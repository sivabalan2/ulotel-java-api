<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="s" uri="/struts-tags"%>

   <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Rate Plan
          </h1>
          <ol class="breadcrumb">
            <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Rate Plan</li>
          </ol>
        </section>
		
        <!-- Main content -->
        <section class="content">
        
        	<div class="row">
            	<div class="col-xs-12" ng-if="rateplans.length>=0">
						<div class="box">
							<div class="box-header">
								<h3 class="box-title">Rate Plan</h3>
							</div>
							
							
							<!-- /.box-header -->
							<div class="box-body table-responsive no-padding" > 
								<table class="table table-hover" id="example1">
									<tr>
										<th>Order</th>
										<th>Rate Plan Type</th>
										<th>Rate Plan Name</th>
										<th>Start Date</th>
										<th>End Date</th>
										<th>Edit</th>
<!-- 										<th>Delete</th> -->
										<th>User Name</th>
									</tr>
									<tr ng-repeat="rp in rateplans">
										<td>{{rp.serialNo}}</td>
										<td>{{rp.ratePlanType}}</td>
										<td>{{rp.ratePlanName}}</td>
										<td>{{rp.startDate}}</td>
										<td>{{rp.endDate}}</td>
										<td>
											<a href="#editmodal" ng-click="getRatePlan(rp.ratePlanId)"  data-toggle="modal"><i  class="fa fa-pencil text-green"></i></a>
										</td>
										<!-- <td>
											<a href="#deletemodal" ng-click="" data-toggle="modal" ><i  class="fa fa-fw fa-trash text-red" ></i></a>
										</td> -->
										<td>{{rp.createdByName}}</td>
									</tr>
									<tr>
									</tr>
								</table>
							</div>
							<!-- /.box-body -->
							
							
							<div class="box-footer clearfix">
								 <ul class="pagination pagination-sm no-margin pull-right">
									<a class="btn btn-primary " href="#addmodal" data-toggle="modal" >Add Rate Plan</a>
								</ul> 
							</div>
							<div class="box-footer clearfix">
								<ul class="pagination pagination-sm"></ul>
								<div class="simple-pagination pull-right" >   
									<p class="simple-pagination__items">Showing {{ratePlanCount[0].count}} out of {{settings.pageLimit}}</p>
									<p><button ng-click="previousPage()" ng-disabled="settings.currentPage <= 0" class="simple-pagination__button simple-pagination__button--prev"> &#10094;          
									</button><span class="simple-pagination__pages">{{settings.currentPage + 1}} of {{getTotalPages()}}</span>
									<button ng-click="nextPage()" ng-disabled="settings.currentPage === (getTotalPages() - 1)" class="simple-pagination__button simple-pagination__button--next">&#10095;</button></p>
									<a href="" ng-click="limited(10)" ng-class="active">{{10}}</a> |
									<span><a href="" ng-click="all(ratePlanCount[0].count)" ng-class="active">All</a></span> 
								</div>
						   </div> 
						</div>
						<!-- /.box -->
					</div>	
         	</div>
          
        </section><!-- /.content -->
      
      	<div class="modal" id="addmodal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" id="btnclose" class="close" data-dismiss="modal"
							aria-hidden="true">x</button>
						<h4 class="modal-title">Add Rate Plan</h4>
					</div>
					 <form method="post" theme="simple" name="ratePlan">
					 <section>
					<div class="modal-body" >
						<div class="form-group col-md-12">
							<label for="planType">Rate Plan Type <span style="color:red">*</span></label>
							<input type="text" class="form-control"  name="planType"  id="planType"  placeholder="Plan Type" ng-model='planType' ng-required="true" autocomplete="off">
						    <span ng-show="ratePlan.planType.$error.pattern" style="color:red">Enter Rate Plan Type !</span>
						</div>
						<div class="form-group col-md-12">
							<label for="planName">Rate Plan Name <span style="color:red">*</span></label>
							<input type="text" class="form-control"  name="planName"  id="planName"  placeholder="Plan Name" ng-model='planName' ng-required="true" autocomplete="off">
						    <span ng-show="ratePlan.planName.$error.pattern" style="color:red">Enter Rate Plan Name !</span>
						</div>
						<div class="form-group col-md-6">
			                <label>Start Date <span style="color:red">*</span></label>
			                    <div class="input-group date">
					              <label class="input-group-addon btn" for="fromDate">
					                   <span class="fa fa-calendar"></span>
					              </label>   
			                	  <input type="text" id="fromDate" class="form-control" name="fromDate"  value="" ng-required="true" autocomplete="off">
			                	</div>
			              </div>
			              <div class="form-group col-md-6">
				                 <label>End Date <span style="color:red">*</span></label>
				
				                 <div class="input-group date">
				                    <label class="input-group-addon btn" for="toDate">
				                    <span class="fa fa-calendar"></span>
				                    </label>   
				                    <input type="text" id="toDate" class="form-control" name="toDate"  value="" ng-required="true" autocomplete="off">
				                </div>
			                </div>
			                 <div class="col-md-6">
			                     <div class="form-group">
				                     <label>Plan Status <span style="color:red">*</span> </label>
				                     <select name="planIsActive" id="planIsActive" class="form-control" ng-required="true" >
					                     <option value="true"  ng-selected ="{{rp.planIsActive}} == 'true'">Active</option>
					                     <option value="false" ng-selected ="{{rp.planIsActive}} == 'false'">InActive</option>
				                     </select>
			                     </div>
		                     </div>
		                     <div class="col-md-6">
			                     <div class="form-group">
				                     <label>Tax Status <span style="color:red">*</span> </label>
				                     <select name="taxIsActive" id="taxIsActive" class="form-control" ng-required="true" >
					                     <option value="true"  ng-selected ="{{rp.taxIsActive}} == 'true'">Active</option>
					                     <option value="false" ng-selected ="{{rp.taxIsActive}} == 'false'">InActive</option>
				                     </select>
			                     </div>
		                     </div>
		                     <div class="form-group col-md-6">
		                   		<label>&nbsp;&nbsp;</label>
						  		<label>&nbsp;&nbsp;</label>
		                     </div>
						<div class="form-group" >
						   <div class="input-group" >
						  		<label>&nbsp;&nbsp;</label>
						  		<label>&nbsp;&nbsp;</label>
						  		<label>&nbsp;&nbsp;</label>
						  		<label>&nbsp;&nbsp;</label>
						  		<input type="hidden" class="form-control"  >
						   </div>

						</div>
					</div>
					</section>
					<section>
						<div class="modal-footer">
							<a href="#" data-dismiss="modal" class="btn btn-danger">Close</a>
							<button ng-click="ratePlan.$valid && addRatePlan()" ng-disabled="ratePlan.$invalid" class="btn btn-primary">Save</button>
						</div>
					</section>
					</form>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dalog -->
		</div>
		<div class="modal" id="editmodal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" id="editbtnclose" class="close" data-dismiss="modal"
							aria-hidden="true">x</button>
						<h4 class="modal-title">Edit Rate Plan</h4>
					</div>
					 	 <form method="post" theme="simple" name="editrateplan">
					 	 <section>
						<div class="modal-body">
							<input type="hidden" value="{{rateplan[0].ratePlanId}}" id="editRatePlanId">
						    <div class="form-group col-md-12">
								<label for="editRatePlanType">Rate Plan Type <span style="color:red">*</span></label>
								<input type="text" class="form-control"  name="editRatePlanType"  id="editRatePlanType" value="{{rateplan[0].ratePlanType}}" ng-model='rateplan[0].ratePlanType'  ng-required="true" autocomplete="off">
							    <span ng-show="editrateplan.editRatePlanType.$error.pattern">Rate Plan Type!</span>
							</div>
						    <div class="form-group col-md-12">
								<label for="editRatePlanName">Rate Plan Name <span style="color:red">*</span></label>
								<input type="text" class="form-control"  name="editRatePlanName" id="editRatePlanName" value="{{rateplan[0].ratePlanName}}"  ng-model='rateplan[0].ratePlanName' ng-required="true" autocomplete="off">
							     <span ng-show="editrateplan.editRatePlanName.$error.pattern" >Rate Plan Name!</span>
							</div>
				            <div class="form-group col-md-6">
			                <label>Start Date <span style="color:red">*</span></label>
			                    <div class="input-group date">
					              <label class="input-group-addon btn" for="editFromDate">
					                   <span class="fa fa-calendar"></span>
					              </label>   
			                	  <input type="text" id="editFromDate" class="form-control" name="editFromDate"  value="{{rateplan[0].startDate}}" ng-required="true" autocomplete="off">
			                	</div>
			              </div>
			              <div class="form-group col-md-6">
			                 <label>End Date <span style="color:red">*</span></label>
			                 <div class="input-group date">
			                    <label class="input-group-addon btn" for="editToDate">
			                    <span class="fa fa-calendar"></span>
			                    </label>   
			                   		 <input type="text" id="editToDate" class="form-control" name="editToDate"  value="{{rateplan[0].endDate}}" ng-required="true" autocomplete="off">
			                </div>
			                </div>    
			                 <div class="col-md-6">
			                     <div class="form-group">
				                     <label>Plan Status <span style="color:red">*</span> </label>
				                     <select name="editPlanIsActive" id="editPlanIsActive" class="form-control" ng-model="rateplan[0].planIsActive" ng-required="true">
					                     <option value="true"  ng-selected ="{{rateplan[0].planIsActive}} == 'true'">Active</option>
					                     <option value="false" ng-selected ="{{rateplan[0].planIsActive}} == 'false'">InActive</option>
				                     </select>
			                     </div>
		                     </div>
		                     <div class="col-md-6">
			                     <div class="form-group">
				                     <label>Tax Status <span style="color:red">*</span> </label>
				                     <select name="editTaxIsActive" id="editTaxIsActive" class="form-control" ng-model="rateplan[0].taxIsActive" ng-required="true">
					                     <option value="true"  ng-selected ="{{rateplan[0].taxIsActive}} == 'true'">Active</option>
					                     <option value="false" ng-selected ="{{rateplan[0].taxIsActive}} == 'false'">InActive</option>
				                     </select>
			                     </div>
		                     </div>
		                    <div class="form-group col-md-6">
		                   		<label>&nbsp;&nbsp;</label>
						  		<label>&nbsp;&nbsp;</label>
		                     </div>
							<div class="form-group" >
						   <div class="input-group" >
						  		<label>&nbsp;&nbsp;</label>
						  		<label>&nbsp;&nbsp;</label>
						  		<label>&nbsp;&nbsp;</label>
						  		<label>&nbsp;&nbsp;</label>
						  		<input type="hidden" class="form-control"  >
						   </div>

						</div>	
						</div>
						</section>
						<section>
							<div class="modal-footer">
								<a href="#" data-dismiss="modal" class="btn btn-danger">Close</a>
								<button ng-click="editrateplan.$valid && editRatePlan()" ng-disabled="editrateplan.$invalid" class="btn btn-primary">Update</button>
							</div>  
						</section>
						</form>
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
		
			<!-- /.modal-dalog -->
		</div>
		
		
  

      
      


<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>

	<script>


 
	
	var app = angular.module('myApp',['ngFileUpload']);
	app.controller('customersCtrl',['$scope',
	             					'Upload',
	            					'$timeout',
	            					'$http', function($scope,Upload,$timeout,$http) {

		
		
		$scope.settings = {
	            currentPage: 0,
	            offset: 0,
	            pageLimit: 10,
	            pageLimits: ['10', '50', '100']
	          };

		 $scope.getTotalPages = function () {
			return Math.ceil($scope.ratePlanCount[0].count / $scope.settings.pageLimit);
		};
			
		 $scope.isCurrentPageLimit = function (value) {
		       return $scope.settings.pageLimit == value;
		 };
		 
		 $scope.previousPage = function () {
			   $scope.settings.currentPage -= 1;
			   var previousOffset = $scope.settings.currentPage * $scope.settings.pageLimit;
		       var url = "get-rate-plan-details?limit="+$scope.settings.pageLimit+"&offset="+previousOffset;
				$http.get(url).success(function(response) {
					//console.log(response);
					$scope.rateplandetails = response.data;
				}); 
		    }; 
		 
     		$scope.nextPage = function () {
		       $scope.settings.currentPage += 1;
		       var nextOffset = $scope.settings.currentPage * $scope.settings.pageLimit;
		       var url = "get-rate-plan-details?limit="+$scope.settings.pageLimit+"&offset="+nextOffset;
				$http.get(url).success(function(response) {
					//console.log(response);
					$scope.rateplandetails = response.data;
				}); 
		     }; 
		       
      			$scope.all = function (total) {
		    	   $scope.settings.pageLimit = total;
			       var nextOffset = 0;
			       var url = "get-rate-plan-details?limit="+total+"&offset="+nextOffset;
					$http.get(url).success(function(response) {
						//console.log(response);
						$scope.rateplandetails = response.data;
					}); 
			    };
			       
      			 $scope.limited = function (limit) {
			    	   $scope.settings.pageLimit = limit;
				       var nextOffset = 0;
				       var url = "get-rate-plan-details?limit="+limit+"&offset="+nextOffset;
						$http.get(url).success(function(response) {
							//console.log(response);
							$scope.rateplandetails = response.data;
					}); 
		       };			

		       $("#fromDate").datepicker({
		            dateFormat: 'MM d, yy',
		          
		            //minDate:  0,
		            onSelect: function (formattedDate) {
		                var date1 = $('#fromDate').datepicker('getDate'); 
		                var date = new Date( Date.parse( date1 ) ); 
		                date.setDate( date.getDate() + 1 );        
		                var newDate = date.toDateString(); 
		                newDate = new Date( Date.parse( newDate ) );   
		                $('#toDate').datepicker("option","minDate",newDate);
		                $timeout(function(){
		                	document.getElementById("fromDate").style.borderColor = "LightGray";
		                });
		            }
		        });

		        $("#toDate").datepicker({
		            dateFormat: 'MM d, yy', 
		          
		            //minDate:  0,
		            onSelect: function (formattedDate) {
		                var date2 = $('#toDate').datepicker('getDate'); 
		                $timeout(function(){
		                	document.getElementById("toDate").style.borderColor = "LightGray";
		                });
		            }
		        }); 
		        
		        $("#editFromDate").datepicker({
		            dateFormat: 'MM d, yy',
		          
		            //minDate:  0,
		            onSelect: function (formattedDate) {
		                var date1 = $('#editFromDate').datepicker('getDate'); 
		                var date = new Date( Date.parse( date1 ) ); 
		                date.setDate( date.getDate() + 1 );        
		                var newDate = date.toDateString(); 
		                newDate = new Date( Date.parse( newDate ) );   
		                $('#editToDate').datepicker("option","minDate",newDate);
		                $timeout(function(){
		                	document.getElementById("editFromDate").style.borderColor = "LightGray";
		                });
		            }
		        });

		        $("#editToDate").datepicker({
		            dateFormat: 'MM d, yy', 
		          
		            //minDate:  0,
		            onSelect: function (formattedDate) {
		                var date2 = $('#editToDate').datepicker('getDate'); 
		                $timeout(function(){
		                	document.getElementById("editToDate").style.borderColor = "LightGray";
		                });
		            }
		        }); 
		
	       	$timeout(function(){
	      	    // $scope.progressbar.complete();
	            $scope.show = true;
	            $("#pre-loader").css("display","none");
	        }, 2000);

			 
			
			$scope.unread = function() {
			//var notifiurl = "http://localhost:8085/collaborative-workflow/unreadnotifications.action";
			var notifiurl = "unreadnotifications.action";
			$http.get(notifiurl).success(function(response) {
				$scope.latestnoti = response.data;
			});
			};

			
			
			$scope.getRatePlanDetails = function() {

				var url = "get-rate-plan-details?limit="+$scope.settings.pageLimit+"&offset="+$scope.settings.offset;
				$http.get(url).success(function(response) {
					$scope.rateplandetails = response.data;
		
				});
			};
			
			
			
	    	 $scope.getRatePlans = function() {
				var url = "get-rate-plan";
				$http.get(url).success(function(response) {
					$scope.rateplans = response.data;
		
				});
			};
			
			$scope.getRatePlan = function(rowid) {
				var url = "get-rate-plan-id?ratePlanId="+rowid;
				$http.get(url).success(function(response) {
					$scope.rateplan = response.data;
		
				});
			};
			
            
			
			$scope.getRatePlanCount = function() {
				var url = "get-rate-plan-count";
				$http.get(url).success(function(response) {
					$scope.ratePlanCount = response.data;
				});
			};
			
			$scope.addRatePlan = function(){
				
				var fdata = "&ratePlanName=" + $('#planName').val()
				+ "&ratePlanType=" + $('#planType').val() 
				+ "&strStartDate=" + $('#fromDate').val() 
				+ "&strEndDate=" + $('#toDate').val()
				+ "&taxIsActive=" + $('#taxIsActive').val();

				$http(
						{
							method : 'POST',
							data : fdata,
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							url : 'add-rate-plan'
						}).then(function successCallback(response) {
							 window.location.reload(); 
				}, function errorCallback(response) {
					
				});

			};
			
			$scope.editRatePlan = function(){
				
				var fdata = "&ratePlanName=" + $('#editRatePlanName').val()
				+ "&ratePlanType=" + $('#editRatePlanType').val() 
				+ "&strStartDate=" + $('#editFromDate').val() 
				+ "&strEndDate=" + $('#editToDate').val()
				+ "&ratePlanId=" + $('#editRatePlanId').val()
				+ "&planIsActive=" + $('#editPlanIsActive').val()
				+ "&taxIsActive=" + $('#editTaxIsActive').val();
				
				
				
				$http(
						{
							method : 'POST',
							data : fdata,
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							url : 'edit-rate-plan-id'
						}).then(function successCallback(response) {
							 window.location.reload(); 
				}, function errorCallback(response) {
					
				});

			};
			
			$scope.getRatePlans();
		
			$scope.getRatePlanCount();
			$scope.getRatePlanDetails();
	}]);


	
		   
		        

		
	</script>
	  <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
			<style type="text/css">
    #ui-datepicker-div
    {
        z-index: 9999999 !important;
    }
</style>
	<link rel="stylesheet" href="dist/css/dataTables.bootstrap.css">
	<script src="js1/upload/ng-file-upload-shim.min.js"></script>
	<script src="js1/upload/ng-file-upload.min.js"></script>
	 <script src="js1/jquery.dataTables.min.js" type="text/javascript"></script>
	 <script src="js1/dataTables.bootstrap.min.js" type="text/javascript"></script>
       