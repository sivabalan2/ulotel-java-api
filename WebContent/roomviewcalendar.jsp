<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="s" uri="/struts-tags"%>
  <link rel="stylesheet" href="plugins/fullcalendar/fullcalendar.min.css">
  <link rel="stylesheet" href="plugins/fullcalendar/fullcalendar.print.css" media="print">

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
        Room View
          </h1>
         <!--  <button  id="show" class="btn-primary" style='display:none;margin:0 auto;'>Modify Search</button>    -->   
        </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
        <div class="box" style="z-index:99999;">
       
          
   
        <div id="container2" >
         <div class="box-header">
			<h3 class="box-title">Room View</h3>
		</div>
          <form name="searchRoom" id="checkedAccommodation">
          <div class="col-sm-12 col-md-12">
			<div class="form-group">
		      <label>Select Accommodation Type :</label>
			
             		<label class="checkbox-inline" ng-repeat="at in accommodations">
       					<input type="checkbox" name="modAccommodations[]" ng-model="modAccommodations" id="modAccommodations" value="{{at.accommodationId}}"  class="flat-red" >&nbsp {{at.accommodationType}}
     				</label>
                
			</div>
			</div>
			<div class="col-sm-12 col-md-12">		</div>
			<div class="box-footer clearfix">
			   <button type="submit" id="hides" onclick="getCalendar()" class="btn btn-primary btngreeen pull-right" style="margin:5px;" >Search</button>
            </div>
          </form>
        </div>
          </div>
      </div>
      <div class="col-lg-12 col-md-12 col-sm-12">  <div id="container" class="table table-responsive"><div id="calendar"></div> </div></div>
    
      

    
  </div>
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
    </section>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
  <%-- <script src="https://code.jquery.com/jquery-1.12.4.js"></script> --%>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="plugins/fullcalendar/fullcalendar.min.js"></script>
<!-- Page specific script -->
<script>
$(document).ready(function () {
    // page is now ready, initialize the calendar...
     var results = [];
     $('#calendar').fullCalendar({
        // put your options and callbacks here
        defaultView: 'month',
       // eventBorderColor: "#de1f1f",
        eventColor: '#77b300',
        eventTextColor:'#fff',
     
   
         header:
        {  
            left: 'prev,next,today',
            center: 'title',
            right: 'month'
        },

        editable: false,
        selectable: true,
        events: dowEvents,
        
                //When u select some space in the calendar do the following:
      

        //When u drop an event in the calendar do the following:
        eventDrop: function (event, delta, revertFunc) {
            //do something when event is dropped at a new location
        },

        //When u resize an event in the calendar do the following:
        eventResize: function (event, delta, revertFunc) {
            //do something when event is resized
        },

 /*        eventRender: function(event, element) {
            $(element).tooltip({title: event.title});             
        },
 */
        //Activating modal for 'when an event is clicked'
        eventClick: function (event) {
        	
//         	alert(event.start);
        	var date = new Date(event.start);
        	var strDate = (date.getMonth() + 1) + '/' + date.getDate() + '/' +  date.getFullYear();
        	//var strDate = new Date(event.start);
        	
        	 $.ajax({
      		   
       		    url: "change-inventory"+"?strDate="+strDate,
     	 		//url: "get-booking-calendar"+ "?chartDate=" +results,
     	 		//url: "get-bookingchart"+ "?chartDate=" +results,
     	 		method: "GET",
     	 		async: true,
     	 		success: function(response) {
     	 			
     	 		//window.location = '/ulopms/reservation';
     	 		
     	 		//alert("success");
     	 			
     	 			}
     	 	});
        	 
            $('#availableRoom').html(event.title);
            $('#availableNight').html(event.description);
        },
        eventRender: function(event, element, view) {
        	 $(element).tooltip({
                 content: event.description
             });
        	$(element).tooltip({title: event.title});  
        	//$(element).tooltip({description: event.description}); 
            var cellheight = $('.fc-widget-content').height();
            var cellheight = $('.fc-widget-content').height();
            $(element).css('height', '20px');
            $(element).css('width', '90px');
            //$(element).css('margin-left', '30px');
            element.find('.fc-time').hide();
            $(element).css('font-size', '1em');
            $(element).css('text-align', 'center');
            $(element).css('line-height', '20px');
           
           
        },
     
       
    }
     )
     
   
 	
     function dowEvents(start, end, tz, callback) {
    	 var checkbox_value = "";
    	
 	 	$("#checkedAccommodation :checkbox").each(function () 
 	 			
 	 			{
 	 		 
 	 	    var ischecked = $(this).is(":checked");
 	 	    
 	 	    if (ischecked) {
 	 	        checkbox_value += $(this).val() + ",";
             
 	 	    }
 	 	});
 	 	
 	 	//alert('mmmmcheckbox_value..'+checkbox_value)
    	  var events = [];
    	  var curr = start;
    	  results = enumerateDaysBetweenDates(start, end);
    	  //alert('mmmmresults..'+results)
    	  $.ajax({
    		  
    	 		url: "get-room-view-calendar"+ "?chartDate=" +results + "&checkedAccommodationId="+checkbox_value,
    	 		method: "GET",
    	 		async: false,
    	 		success: function(response) {
    	 			
    	 			//alert(chartDate);
    	 			events = response.data;
    	 			/* var lim = events.length;
    	 			
    	 			for (var i = 0; i < lim; i++){
    	 				 
    	 					 events[i].start = moment(results[i]);
    	 				
    	 				} */
    	 		}
    	 	});
    	  
    	  
    	  
    	/*  while (curr <= end) {
    	   // if (curr.format('dddd') === 'Monday')
    	    {
    	      events.push({
    	        title: 'Rooms 10',
    	        start: moment(curr)
    	      });
    	    }
    	    curr = curr.add(1, 'day');
    	  }
    	*/
    	
    	  console.log(events);
    	  callback(events);
    	
    	}
     
     
    });






</script>
<script>
     var enumerateDaysBetweenDates = function(startDate, endDate) {
      
      
      var now = startDate,
      dates = [];
      
      
     while (now.isBefore(endDate) || now.isSame(endDate)) {
           // dates.push({title:now.format("YYYY-MM-DD"),start:moment(startDate)});
           dates.push(now.format("YYYY-MM-DD"));
          //  moment().format();               // Jun 12th 17

            now.add('days', 1);
        }
      return dates;
      
  };

 function getCalendar(id){
 
 //$('#calendar').fullCalendar( 'removeEventSource', events);
 //$('#calendar').fullCalendar( 'addEventSource', events);         
 $('#calendar').fullCalendar( 'refetchEvents' );
 
 }
 
 function getCalendar(){
	 
	 $('#calendar').fullCalendar( 'refetchEvents' );
	 }

</script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>

	<script>
var app = angular.module('myApp', ['ngProgress']);
	app.controller('customersCtrl', function($scope, $http,$timeout,ngProgressFactory) {

		 
		
       	$timeout(function(){
      	    // $scope.progressbar.complete();
            $scope.show = true;
            $("#pre-loader").css("display","none");
        }, 2000);

		 
		
		$scope.unread = function() {
		//var notifiurl = "http://localhost:8085/collaborative-workflow/unreadnotifications.action";
		var notifiurl = "unreadnotifications.action";
		$http.get(notifiurl).success(function(response) {
			$scope.latestnoti = response.data;
		});
		};

/* 		$(document).ready(function(){
    	    $("#hide").click(function(){
    	        $("#container2").hide();
    	    });
    	    $("#show").click(function(){
    	        $("#container2").show();
    	    });
    	});
		$("#hide").click(function() {
            //$('#container').css({position: 'relative', bottom: '60px'});
    	    $('html, body').animate({
    	        scrollTop: $("#container").offset().top
    	        
    	    }, 1000);
    	  
    	}); */
	    $scope.getPropertyList = function() {
			 
	        	var userId = $('#adminId').val();
	 			var url = "get-user-properties?userId="+userId;
	 			$http.get(url).success(function(response) {
	 			    
	 				$scope.props = response.data;
	 	
	 			});
	 		};
	 		
	    $scope.getAccommodations = function() {

				var url = "get-accommodations";
				$http.get(url).success(function(response) {
				    //console.log(response);
					$scope.accommodations = response.data;
		
				});
			};
	 		
       
	 		
	 	 $scope.change = function() {
	        	   
// 		        alert($scope.id);
		        
		       	       
		 		};
	 			
	 	
	 		
	    $scope.getPropertyList();
	    
	    $scope.getAccommodations();
	    
	   
		//$scope.unread();
		//
        
		
	});
	
	</script>
		<script>
	var btn = $('#hide,#show').click(function() { // bind click handler to both button
		  $(this).hide(); // hide the clicked button
		  btn.not(this).show(); // show the another button which is hidden
		});
	</script>
	<style>
  #container {
    width: 900px;
    display:block;
    margin:0 auto;
    border: 1px solid #ddd  ;

    height:100%
   
    
}
td.fc-other-month {
   visibility: hidden;
}
	</style>