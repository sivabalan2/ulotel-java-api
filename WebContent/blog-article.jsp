<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!-- <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/themes/south-street/jquery-ui.min.css" />
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
   <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
           
            <small>Articles management</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        
            <li class="active">Articles Management</li>
          </ol>
        </section>
		
        <!-- Main content -->
        <section class="content">
        
        	 <div class="row">
            <div class="col-xs-12">
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#fa-icons" data-toggle="tab">Add Articles</a></li>
                   <li ><a href="#editarticles" data-toggle="tab">View Articles</a></li>
                  <!-- <li ><a href="#tab-confirmation-summary" data-toggle="tab">Confirmation Summary</a></li> -->
                </ul>
                <div class="tab-content">
                  <!-- Font Awesome Icons -->
                  <div class="tab-pane active" id="fa-icons">


    <div class="row">
            <div class="col-md-12">
            <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="glyphicon glyphicon-file">
                            </span>Add Article Title</a>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in">
                        <div class="panel-body">
                           <form name="bookingForm">
                 <div class="form-group col-md-6">
                  <label>Select Category</label>
   <select name="blogCategoryId" id="blogCategoryId" ng-model="blogCategoryId" class="form-control"> 
<!-- <option value="select">select</option> -->
 <option ng-repeat="abc in allblogcategories" value="{{abc.blogCategoryId}}">{{abc.blogCategoryName}}</option>
 </select>
                <!-- /.input group -->
              </div>
                 <div class="form-group col-md-6">
                  <label>Select location</label>
   <select name="blogLocationId" id="blogLocationId"  ng-model="blogLocationId" class="form-control"> 
<!-- <option value="select">select</option> -->
 <option ng-repeat="abl in allbloglocations" value="{{abl.blogLocationId}}">{{abl.blogLocationName}}</option>
 </select>
                <!-- /.input group -->
              </div>
              <div class="form-group col-md-12">
               <label>Article Title</label>
              <input type="text" class="form-control" id="blogArticleTitle" name="blogArticleTitle">      
              </div>
              <div class="form-group col-md-6">
               	<label>Article URL</label>
              	<input type="text" class="form-control" id="blogArticleUrl" name="blogArticleUrl">      
              </div>
             <div class="form-group col-md-6">
							<label for="child_included_rate">Title Image</label>
						<input type="file" class="form-control"   file-input="files" name="photo" id="photo">
						</div>
              <div class="col-md-2">
              <button type="submit" ng-click="addBlogArticleTitle()" class="btn btn-block btn-primary btn-sm" style="margin-top:25px">SAVE</button>
             </div>
             
   
        </form>

                        </div>
                    </div>
                </div>
                          
                
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree"><span class="glyphicon glyphicon-th-list">
                            </span> Add Article Images</a>
                        </h4>
                    </div>
                    <div id="collapseThree" class="panel-collapse collapse">
                        <div class="panel-body">
                                          <form name="bookingForm">
                                            <div class="col-md-4">
                              <div class="box box-solid"  ng-repeat="ai in articleimage"> 
                                 <!-- <div class="box-header with-border">
                                    <h3 class="box-title">					                 
                                       <input type="radio" name="propertyPhotoId" value="{{cp.DT_RowId}}" class="flat-red">
                                    </h3>
                                    <a href="#" ng-click="deletePhoto(cp.DT_RowId)"  data-toggle="modal"><i  class="fa fa-fw fa-trash"></i></a>
                                 </div> -->
                                 <!-- /.box-header -->
                                 <div class="box-body">
                                    <img class="attachment-img " src="get-blog-article-image-stream?blogArticleImageId={{ai.blogArticleImageId}}"  width="100" height="100" alt="attachment image"/>
                                 </div>
                                 <div class="form-group">
                                 <input type="text" id="imagePath" name="imagePath" value="{{ai.imagePath}}">
                                 </div>
                                 <!-- /.box-body -->
                              </div>
                              <!-- /.box -->
                           </div>
                 
                
            <!--   <div class="form-group col-md-12">
               <label>Article Title</label>
              <input type="text" class="form-control" id="articleTitle" name="articleTitle">
      
              </div> -->
              <div class="col-md-3 col-sm-4">
                              <button  ngf-select="upload($file,addedarticleId)" ngf-accept="'image/*'" ngf-pattern="'.jpg,.png,!.gif'" class="btn btn-primary btngreen pull-right">Add Images</button>
                           </div>
             
   
        </form>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"><span class="glyphicon glyphicon-th-list">
                            </span> Add Article Description</a>
                        </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse">
                        <div class="panel-body">
                                          <form name="bookingForm">
                 
                 
             			  <div class="row fontawesome-icon-list">
                              <div class="col-md-12 col-sm-12">                              
                                 <div class="form-group">
                                    <h4>Blog Content : </h4>
                                    <textarea id="blogArticleDescription" name="blogArticleDescription"  class="form-control "  placeholder="Article Description"   ng-required="true">
		                    	
		                    		</textarea>
                                 </div>
                              </div>
                           </div> 
              <div class="col-md-2">
              
             
              <button type="button" ng-click="addBlogArticleDescription(addedarticleId)" class="btn btn-block btn-primary btn-sm" style="margin-top:25px">SAVE</button>
      		</div> 
             
   
        </form>
                        </div>
                    </div>
                </div>
                      
            </div>
        </div>
    </div>
			
        
            </div>
             <div class="tab-pane" id="editarticles" >            
               <div class="form-group col-md-2" > 
				<label>Article Title</label>
				<input type="text" ng-model="search" class="form-control" placeholder="Search">
			  
			  </div>			 
             <!--  <div class="col-sm-2 col-md-2">
	              <div class="form-group">
	              <label>Search</label>
					<label>&nbsp;</label>
	              	<button type="submit" ng-click="getPropertyBookingList()" class="btn btn-danger pull-right btn-block btn-sm">Search</button>
	              	</div>
              </div> --> 
              
              
              <div class="row">
        <div class="col-xs-12">
          <div class="box">
           <!--  <div class="box-header">
              <h3 class="box-title">Results</h3>

              
            </div> -->
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding" style="height:300px;">
  
              <table class="table table-hover">
                <tr>
                  <th style="overflow:hidden;white-space:nowrap;">Article ID</th>
                  <th>Article Title</th>
                  <th>Edit</th>
                  <th>Delete</th>
                  
                </tr>
                 <tr ng-repeat="aba in allblogarticles | filter:search">
                  <td>{{aba.blogArticleId}}</td>
                  <td>{{aba.blogArticleTitle}}</td>
                  <td>
											<a ng-click="getBlogArticle(aba.blogArticleId)"  data-toggle="modal"><i  class="fa fa-pencil text-green"></i></a>
											
										</td>
										<td>
											
											<a href="#deletemodal" ng-click="deleteBlogArticle(aba.blogArticleId)" data-toggle="modal" ><i  class="fa fa-fw fa-trash text-red" ></i></a>
										</td>
                 
                </tr> 
                	
                <tr ng-if="bookinglist.count==0">
                	<td  colspan="8">No Records found
                	</td>
                </tr>
                
             </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
              
              
              <div class="row" >
            <div class="col-md-12">
            <div class="panel-group" id="accordion" >
                <div class="panel panel-default">                
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour"><span class="glyphicon glyphicon-file">
                            </span>Add Article Title</a>
                        </h4>
                    </div>
                    <input type="hidden" value="{{editarticleId}}" id="editBlogArticleId" name="editBlogArticleId">
                    <div id="collapseFour" class="panel-collapse collapse in">
                        <div class="panel-body">
                           <form name="bookingForm">
                 <div class="form-group col-md-6">
                  <label>Select Category</label>
				   <select name="editBlogCategoryId" id="editBlogCategoryId" ng-model="editBlogCategoryId" class="form-control"> 
				<!-- <option value="select">select</option> -->
				 <option ng-repeat="abc in allblogcategories" value="{{abc.blogCategoryId}}" ng-selected ="abc.blogCategoryId == blogarticle[0].blogCategoryId">{{abc.blogCategoryName}}</option>
				 </select>
                <!-- /.input group -->
              </div>
                 <div class="form-group col-md-6">
                  <label>Select location</label>
				   <select name="editBlogLocationId" id="editBlogLocationId"  ng-model="editBlogLocationId" class="form-control"> 
				<!-- <option value="select">select</option> -->
				 <option ng-repeat="abl in allbloglocations" value="{{abl.blogLocationId}}"  ng-selected ="abl.blogLocationId == blogarticle[0].blogLocationId">{{abl.blogLocationName}}</option>
				 </select>
                <!-- /.input group -->
              </div>
              <div class="form-group col-md-12">
               	 <label>Article Title</label>
             	 <input type="text" class="form-control" id="editBlogArticleTitle" value="{{blogarticle[0].blogArticleTitle}}" name="editBlogArticleTitle">      
              </div>
              <div class="form-group col-md-6">
               	 <label>Article URL</label>
             	 <input type="text" class="form-control" id="editBlogArticleUrl" value="{{blogarticle[0].blogArticleUrl}}" name="editBlogArticleUrl">      
              </div>
              <div class="col-md-12">
				         <img class="img-thumbnail"  ngf-select="upload1($file,editarticleId)"  ngf-accept="'image/*'" ngf-pattern="'.jpg,.png,!.gif'" src="get-blog-article-title-image?blogArticleId={{editarticleId}}"  alt="User Avatar" width="100%">
				         </div>
              <div class="col-md-2">
              <button type="submit" ng-click="editBlogArticleTitle()" class="btn btn-block btn-primary btn-sm" style="margin-top:25px">SAVE</button>
             </div>
             
   
        </form>

                        </div>
                    </div>
                </div>
                
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseSix"><span class="glyphicon glyphicon-th-list">
                            </span> Add Article Images</a>
                        </h4>
                    </div>
                    <div id="collapseSix" class="panel-collapse collapse">
                        <div class="panel-body">
                                          <form name="bookingForm">
                                            <div class="col-md-4">
                              <div class="box box-solid"  ng-repeat="ai in articleimage">
                                  <div class="box-header with-border">
                                   <!--  <h3 class="box-title">					                 
                                       <input type="radio" name="propertyPhotoId" value="{{cp.DT_RowId}}" class="flat-red">
                                    </h3> -->
                                    <a href="#" ng-click="deleteArticleImage(ai.blogArticleImageId)"  data-toggle="modal"><i  class="fa fa-fw fa-trash"></i></a>
                                 </div> 
                                 <!-- /.box-header -->
                                 <div class="box-body">
                                    <img class="attachment-img " src="get-blog-article-image-stream?blogArticleImageId={{ai.blogArticleImageId}}"  width="100" height="100" alt="attachment image"/>
                                 </div>
                                 <div class="form-group">
                                 <input type="text" id="imagePath" name="imagePath" value="{{ai.imagePath}}">
                                 </div>
                                 <!-- /.box-body -->
                              </div>
                              <!-- /.box -->
                           </div>
                 
                
            <!--   <div class="form-group col-md-12">
               <label>Article Title</label>
              <input type="text" class="form-control" id="articleTitle" name="articleTitle">
      
              </div> -->
              <div class="col-md-3 col-sm-4">
                              <button  ngf-select="upload($file,editarticleId)" ngf-accept="'image/*'" ngf-pattern="'.jpg,.png,!.gif'" class="btn btn-primary btngreen pull-right">Add Images</button>
                           </div>
             
   
        </form>
                        </div>
                    </div>
                </div>
                
                 <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive"><span class="glyphicon glyphicon-th-list">
                            </span> Add Article Description</a>
                        </h4>
                    </div>
                    <div id="collapseFive" class="panel-collapse collapse">
                        <div class="panel-body">
                                          <form name="bookingForm">
                 
                 
             			 <div class="row fontawesome-icon-list">
                              <div class="col-md-12 col-sm-12">                              
                                 <div class="form-group">
                                    <h4>edit Blog Content : </h4>
                                    <textarea ck-editor  id="editblogArticleDescription" ng-model="blogarticle[0].blogArticleDescription"   class="form-control" placeholder="Article Description" ng-required="true" >
		                    	
		                    	</textarea>		                    		
                                 </div>
                              </div>
                           </div>    
                           <!--  <div class="col-md-12 col-sm-12">
                                 <div class="form-group">
                                    <h4>Hotel Policy : </h4>
                                    <textarea id="propertyHotelPolicy" name="propertyHotelPolicy" ng-model="property[0].propertyHotelPolicy" class="form-control" value="{{property[0].propertyHotelPolicy}}" placeholder="Hotel Policy"   ng-required="true">
		                    		{{property[0].propertyHotelPolicy}}
		                    		</textarea>
                                 </div>
                              </div>  -->                      
              <div class="col-md-2">
             
              <button type="button" ng-click="editBlogArticleDescription(editarticleId)" class="btn btn-block btn-primary btn-sm" style="margin-top:25px">SAVE</button>
      		</div> 
             
   
        </form>
                        </div>
                    </div>
                </div>
                      
            </div>
        </div>
    </div>
              


            </div>
            
            
            
        		 </div>		
				
    
        				
				
    
                        
          
          
        </section><!-- /.content -->
                
                      
                        

                </div><!-- /.tab-content -->
                <!-- guest information begins -->
              	  

              	
              </div><!-- /.nav-tabs-custom -->
            </div><!-- /.col -->
          </div><!-- /.row -->
          </div>
        
        
          
          
        </section><!-- /.content -->
      
    

      </div><!-- /.content-wrapper -->
      


<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>
 <script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script> 
 <script src="https://cdnjs.cloudflare.com/ajax/libs/ng-ckeditor/0.2.1/ng-ckeditor.min.js"></script>
  <%-- <script src="https://cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script> --%>
  <script src="js1/upload/ng-file-upload-shim.min.js"></script>
<script src="js1/upload/ng-file-upload.min.js"></script>
	 <script src="js/jquery-2.1.1.js" type="text/javascript"></script>
 <script>
/*  $('.datepicker').datepicker({
	    dateFormat: 'dd-mm-yy'
	 }); */
 </script>
	<script>
    
	 
	
	var app = angular.module('myApp',['ngFileUpload','ngCkeditor']);
	
	
	app.directive('ckEditor', function () {
	    return {
	        require: '?ngModel',
	        link: function (scope, elm, attr, ngModel) {
	            var ck = CKEDITOR.replace(elm[0]);

	            if (!ngModel) return;
	 
	            ck.on('pasteState', function () {
	                scope.$apply(function () {
	                    ngModel.$setViewValue(ck.getData());
	                });
	            });
	 
	            ngModel.$render = function (value) {
	                ck.setData(ngModel.$viewValue);
	            };
	        }
	    };
	});
	
	app.controller('customersCtrl',['$scope',
	             					'Upload',
	            					'$timeout',
	            					'$http', function($scope,Upload,$timeout,$http) {
		

		 
		
		 
		
		/* ,ngProgressFactory */
			
	 
      
     // CKEDITOR.replace('editblogArticleDescription');
     // $(".textarea").wysihtml5();
		
		$scope.unread = function() {
		//var notifiurl = "http://localhost:8085/collaborative-workflow/unreadnotifications.action";
		var notifiurl = "unreadnotifications.action";
		$http.get(notifiurl).success(function(response) {
			$scope.latestnoti = response.data;
		});
		};

		
	
		$scope.getAllBlogLocations = function() {
			var url = "get-all-blog-locations";
			$http.get(url).success(function(response) {
			    console.log(response);
				$scope.allbloglocations = response.data;
	
			});
			
		};
		
		$scope.getAllBlogCategories = function() {
			var url = "get-all-ulo-blog-categories";
			$http.get(url).success(function(response) {
			    console.log(response);
				$scope.allblogcategories = response.data;
	
			});
			
		};		
	
		var spinner = $('#loadertwo');
		
		$scope.addBlogArticleTitle = function(){
			var file = $scope.files;
			 console.log(file); 
			 
			var locationid = $('#blogLocationId').val();
			var categoryid = $('#blogCategoryId').val();
			
			if(categoryid == 1 && locationid == "? undefined:undefined ?" ){
				
					alert("please select any location")		   
				return;
						
				
			}
			 
			 
			else if(locationid == "? undefined:undefined ?"){
				locationid = "";
			}
			/* else if(locid != "? undefined:undefined ?"){
			    locid = locid;
			} */
			var articleTitle = $scope.getRemoveHtml($('#blogArticleTitle').val());
					
					var fdata = "blogLocationId=" + locationid
						+ "&blogCategoryId=" + categoryid
						+ "&blogArticleTitle=" + articleTitle
						+ "&blogArticleUrl=" + $('#blogArticleUrl').val();
			spinner.show();
					$http(
							{
								method : 'POST',
								data : fdata,
								headers : {
									'Content-Type' : 'application/x-www-form-urlencoded'
								},
								url : 'add-blog-article-title'
							}).then(function successCallback(response) {
								   setTimeout(function(){ spinner.hide(); 
									  }, 1000);
								$scope.addedarticle = response.data;
								$scope.addedarticleId = $scope.addedarticle.data[0].blogArticleId;
								
								
								Upload.upload({
					                url: 'add-article-title-img',
					                enableProgress: true, 
					                data: {myFile: file, 'blogArticleId': $scope.addedarticleId} 
					            }).then(function (resp) {
					            	
					            	
					            });
								   setTimeout(function(){ spinner.hide(); 
									  }, 1000);
								alert("Article title added successfully")
								
					        
					}, function errorCallback(response) {
						   setTimeout(function(){ spinner.hide(); 
							  }, 1000);
					});
			//}
		};
		
		
		$scope.editBlogArticleTitle = function(){
			
			
			var editlocationid = $('#editBlogLocationId').val();
			var editcategoryid = $('#editBlogCategoryId').val();
			
			if(editcategoryid == 1 && editlocationid == "? undefined:undefined ?" ){
				
					alert("please select any location")		   
				return;
			}else if(editlocationid == "? undefined:undefined ?"){
				editlocationid = "";
			}
			/* else if(locid != "? undefined:undefined ?"){
			    locid = locid;
			} */
			var editArticleTitle = $scope.getRemoveHtml($('#editBlogArticleTitle').val());
					
					var fdata = "blogLocationId=" + editlocationid
						+ "&blogCategoryId=" + editcategoryid
						+ "&blogArticleTitle=" + editArticleTitle
						+ "&blogArticleId=" + $('#editBlogArticleId').val()
						+ "&blogArticleUrl=" + $('#editBlogArticleUrl').val();
			spinner.show();
					$http(
							{
								method : 'POST',
								data : fdata,
								headers : {
									'Content-Type' : 'application/x-www-form-urlencoded'
								},
								url : 'edit-blog-article-title'
							}).then(function successCallback(response) {
								   setTimeout(function(){ spinner.hide(); 
									  }, 1000);
								$scope.addedarticle = response.data;
								$scope.addedarticleId = $scope.addedarticle.data[0].blogArticleId;
								
								alert("Article title updated successfully");
					}, function errorCallback(response) {
						   setTimeout(function(){ spinner.hide(); 
							  }, 1000);
					});
			//}
		
		};
		
		$scope.getEscapeHtml =  function(unsafe){
			 return unsafe
	       //.replace(/&/g, "and")
	       //.replace(/</g, "&lt;")&nbsp;
		     .replace(/&rsquo;/g, "'")
			 .replace(/&lsquo;/g, "'")
			 .replace(/&ldquo;/g, "\"")
			 .replace(/&rdquo;/g, "\"")  
			 .replace(/&nbsp;/g, " ")
	         .replace(/&lt;/g, "<")
	       //.replace(/>/g, "&gt;")
	         .replace(/&gt;/g, ">")
	       	 .replace(/&#39;/g, "'")
	       //.replace(/"/g, "'")
	        .replace(/&quot;/g, "\"")
			 .replace(/&amp;/g, "and")
			 .replace(/%/g, "percent")
			 .replace(/&ndash;/g, "-");
	        
			
		};
		
		$scope.getRemoveHtml =  function(unsafe){
			
			 return unsafe.replace(/&/g, "and")
			  .replace(/%/g, "percent");
			
	        
			
		};
		
		$scope.addBlogArticleDescription = function(rowid){
			
			 var testblogArticleDescription= CKEDITOR.instances.blogArticleDescription.getData();
			 //alert(testblogArticleDescription)
			 var changedDescription = $scope.getEscapeHtml(testblogArticleDescription);
			// alert(changedDescription)
			 
			 /* var propertyHotelPolicy= CKEDITOR.instances.propertyHotelPolicy.getData();
   			var propertyStandardPolicy= CKEDITOR.instances.propertyStandardPolicy.getData();
   			var propertyCancellationPolicy= CKEDITOR.instances.propertyCancellationPolicy.getData(); */
   			
   			var fdata="blogArticleDescription=" + changedDescription
   			        + "&blogArticleId=" + rowid
			spinner.show();
   			        $http(
					{
						method : 'POST',
						data : fdata,
						headers : {
							'Content-Type' : 'application/x-www-form-urlencoded'
						},
						url : 'add-blog-article-description'
					}).then(function successCallback(response) {
						$scope.addeddescription = response.data;
						   setTimeout(function(){ spinner.hide(); 
							  }, 1000);
						alert("Article description added successfully")
						
			        
			}, function errorCallback(response) {
				   setTimeout(function(){ spinner.hide(); 
					  }, 1000);
			});
	
};

$scope.editBlogArticleDescription = function(rowid){
	//var testblogArticleDescription = $('#editblogArticleDescription').val();
	//alert(testblogArticleDescription)
	
	 var testblogArticleDescription= CKEDITOR.instances.editblogArticleDescription.getData();
			 //alert(testblogArticleDescription)
			 var changedDescription = $scope.getEscapeHtml(testblogArticleDescription);
			// alert(changedDescription)
			 
	 /* var testblogArticleDescription= CKEDITOR.instances.editblogArticleDescription.getData();
*/
	// var changedDescription = $scope.getRemoveHtml(testblogArticleDescription);
	 /* var propertyHotelPolicy= CKEDITOR.instances.propertyHotelPolicy.getData();
		var propertyStandardPolicy= CKEDITOR.instances.propertyStandardPolicy.getData();
		var propertyCancellationPolicy= CKEDITOR.instances.propertyCancellationPolicy.getData(); */
		
		var fdata="blogArticleDescription=" + changedDescription
		        + "&blogArticleId=" + rowid
          //alert(fdata)
          spinner.show();
	$http(
			{
				method : 'POST',
				data : fdata,
				headers : {
					'Content-Type' : 'application/x-www-form-urlencoded'
				},
				url : 'edit-blog-article-description'
			}).then(function successCallback(response) {
				$scope.editdescription = response.data;
				   setTimeout(function(){ spinner.hide(); 
					  }, 1000);
				alert("Article description updated successfully")
				
	        
	}, function errorCallback(response) {
		   setTimeout(function(){ spinner.hide(); 
			  }, 1000);
	});

};


			$scope.upload = function (file,id) {
				spinner.show();
       Upload.upload({
           url: 'add-blog-article-image',
           enableProgress: true, 
           data: {myFile: file,'blogArticleId':id}
       }).then(function (resp) {
    	   setTimeout(function(){ spinner.hide(); 
 		  }, 1000);
       //	alert("successfully added")
       	$scope.getBlogArticleImages(id);
           console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
           //$scope.getPropertyPhotos();
       }, function (resp) {
    	   setTimeout(function(){ spinner.hide(); 
 		  }, 1000);
    	   console.log('Error status: ' + resp.status);
       }, function (evt) {
       	
           var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
           
           console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
          
       });
      
       
   };

	$scope.upload1 = function (file,id) {
		spinner.show();
Upload.upload({
   url: 'add-article-title-img',
   enableProgress: true, 
   data: {myFile: file,'blogArticleId':id}
}).then(function (resp) {
	   setTimeout(function(){ spinner.hide(); 
		  }, 1000);
	alert("successfully added")
	//$scope.getBlogArticleImages(id);
   console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
   //$scope.getPropertyPhotos();
}, function (evt) {
	
   var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
   
   console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
  
});


};
   
  		 $scope.getBlogArticleImages = function(id) {
			
			var url = 'get-blog-article-images?blogArticleId='+id;
			$http.get(url).success(function(response) {
				//console.log(response);
				$scope.articleimage = response.data;
			
			});
		};

		
		$scope.getAllBlogArticles = function() {
			var url = "get-all-blog-articles";
			$http.get(url).success(function(response) {
			    console.log(response);
				$scope.allblogarticles = response.data;
	
			});
			
		};	
		
			
			
			 $scope.deleteBlogArticle = function(rowid){
					var fdata = "&blogArticleId="+ rowid
					var deleteLocationCheck=confirm("Do you want to delete the property?");
					if(deleteLocationCheck){
						$http(
								{
									method : 'POST',
									data : fdata,
									headers : {
										'Content-Type' : 'application/x-www-form-urlencoded'
									},
									url: 'delete-blog-article'
								}).then(function successCallback(response) {
									window.location.reload();
								   }, function errorCallback(response) {
									
									
								});
					}
					else{
						
					}
					
				};	
				
				 $scope.getBlogArticle = function(id) {
					// CKEDITOR.replace('editblogArticleDescription');
				     // $(".textarea").wysihtml5();
						var url = 'get-blog-article?blogArticleId='+id;
						$http.get(url).success(function(response) {
							//console.log(response);
							$scope.blogarticle = response.data;
							$scope.editarticleId = $scope.blogarticle[0].blogArticleId;
							$scope.getBlogArticleImages(id);
							$('html,body').animate({
						        scrollTop: $("#collapseFour").offset().top},
						        'slow');
						
						    $('html,body').animate({
						        scrollTop: $("#collapseFour").offset().top},
						        'slow');
						
						});
					};
					//$scope.EditBlogArticleDescription = "test booking";
					
					$scope.deleteArticleImage = function(rowid){
						var id = $('#editBlogArticleId').val();
						var fdata = "&blogArticleImageId="+ rowid
						var deleteLocationCheck=confirm("Do you want to delete the Article?");
						if(deleteLocationCheck){
							$http(
									{
										method : 'POST',
										data : fdata,
										headers : {
											'Content-Type' : 'application/x-www-form-urlencoded'
										},
										url: 'delete-blog-article-image'
									}).then(function successCallback(response) {
										$scope.getBlogArticleImages(id);
										//window.location.reload();
									   }, function errorCallback(response) {
										
										
									});
						}
						else{
							
						}
						
					};	
				
				/*  $scope.searchArticleTitle = function(){
					 var fdata = "blogArticleTitle=" + $('#articleTitle').val();
					 /*  var fdata = "&blogArticleId="+ rowid
						var deleteLocationCheck=confirm("Do you want to delete the property?"); */
						//if(deleteLocationCheck){
						/* 	$http(
									{
										method : 'POST',
										data : fdata,
										headers : {
											'Content-Type' : 'application/x-www-form-urlencoded'
										},
										url: 'search-article-title'
									}).then(function successCallback(response) {
										 $scope.searchtitle = response.data;
										/*  $scope.search.users = response.data;
										 var text = '{"array":' + JSON.stringify( $scope.search) + '}';
										 var titl = JSON.stringify($scope.search)
										var tit = $scope.searchtitle.data[0].blogArticleTitle; */
										/* $(".autocomplete").autocomplete({
										    source: tit
										  }); */
										//window.location.reload();
									 /*   }, function errorCallback(response) {
										
										
									}); */
						/* }
						else{
							
						} */
						
				//	};	
		 
		 
          $scope.getPropertyBookingList = function(){
        	  var fdata = "blogArticleTitle=" + $('#articleTitles').val();
    	   
		   var bookingId = parseInt(document.getElementById('filterBookingId').value);
		   var mailFlag = false;
		   var text = '{"array":' + JSON.stringify($scope.bookinglist) + '}';
		   console.log(text);
			var data = JSON.parse(text);
			$http(
				    {
					method : 'POST',
					data : data,
					//data : '{"data":' +JSON.stringify($scope.roomsSelected) + '}',
					dataType: 'json',
					headers : {
						 'Content-Type' : 'application/json; charset=utf-8'
						//'Content-Type' : 'application/x-www-form-urlencoded'
					},
					//url : 'edit-booking-details?bookingId='+bookingId+'&mailFlag='+mailFlag
					//url : 'jsonTest'
				   }).then(function successCallback(response) {
					  // $scope.booked = response.data;
					   alert("Room Blocked successfully");
					  // window.location.reload(); 
				   
				   })
		   
		 };
		
		
		
		$scope.getAllBlogLocations();
		$scope.getAllBlogCategories();
		$scope.getAllBlogArticles();
     
	}]);
	
	app.directive('fileInput', ['$parse', function ($parse) {
	    return {
	        restrict: 'A',
	        link: function (scope, element, attributes) {
	            element.bind('change', function () {
	                $parse(attributes.fileInput)
	                .assign(scope,element[0].files[0])
	                scope.$apply()
	            });
	        }
	    };
	}]);

	
	
	</script>
  <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

	  <!-- iCheck 1.0.1 -->
    <script src="plugins/iCheck/icheck.min.js"></script>
	     <script>
  /* next and previous button */
 $('.nextBtn').click(function(){
  $('.nav-tabs > .active').next('li').find('a').trigger('click');
});

  $('.previousBtn').click(function(){
  $('.nav-tabs > .active').prev('li').find('a').trigger('click');
});
  </script>

  <script>
   $(function () {
      // Replace the <textarea id="editor1"> with a CKEditor
      // instance, using default configuration.
       CKEDITOR.replace('blogArticleDescription');      //bootstrap WYSIHTML5 - text editor
      //$(".textarea").wysihtml5();
      
        //CKEDITOR.replace('EditBlogArticleDescription');
      //$(".textarea").wysihtml5();

     });  
   
 
   
  
</script>

   <style>
 .select2-container--default .select2-selection--multiple .select2-selection__choice {
    background-color: green;
    border: 1px solid #aaa;
    border-radius: 4px;
    cursor: default;
    float: left;
    margin-right: 5px;
    margin-top: 5px;
    padding: 0 5px;
}
 </style>
