<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="s" uri="/struts-tags"%>

   <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <section class="content-header">
          <h1>
            Property Amenities
            <!--<small>Subscription Expiry on {{property[0].subscription[0].expirydate}}</small>-->
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Property</a></li>
            <li class="active">Amenities</li>
          </ol>
        </section>
		
        <!-- Main content -->
        <section class="content">
        
        	 <div class="row">
        	 <div class="col-xs-12">
        	 	<div class="box-body" id="message">
        			
                  </div>
                  </div>
            <div class="col-xs-12">   <!-- ng-repeat=" ac in accommodations " -->
              <div class="nav-tabs-custom" >
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#fa-icons" data-toggle="tab" > Property Amenities </a></li>
       
                  <li ng-repeat="ac in accommodations"><a href="#fa-icons3" ng-click="getAccommodationAmenities(ac.accommodationId)" data-toggle="tab">{{ac.accommodationType}}</a></li>  
                 
                </ul>
                <div class="tab-content">
                  <!-- Font Awesome Icons -->
                  <div class="tab-pane active" id="fa-icons">
                  <form method="post" action="" enctype="multipart/form-data" theme="simple">
                    <s:hidden name="propertyId" value="%{propertyId}"></s:hidden>
                  	
                    <section id="new">
                      <!-- <h4 class="page-header">General Settings</h4> -->
                      
                      <div class="row fontawesome-icon-list">
                      	<div class="col-md-3 col-sm-4" ng-repeat="a in amenities">
	                        	 <div class="form-group" >
	                        	 	<label>
                      					<input type="checkbox" name="checkedAmenity[]"  id="checkedAmenity" value="{{a.DT_RowId}}"  class="flat-red" ng-checked ="a.status == 'true'">&nbsp {{a.amenityName}}
                    				</label>
	                        	 </div>
	                        	 
	                        	
	                        </div>
                        
                        
                      </div>
                    </section>

                    

					
                    

                    <section id="video-player">
                      <h4 class="page-header"></h4>
                      <div class="row fontawesome-icon-list">
                        <div class="col-md-3 col-sm-4"></div>
                        <div class="col-md-3 col-sm-4"></div>
                        <div class="col-md-3 col-sm-4"></div>
                        <div class="col-md-3 col-sm-4">
                        	<button type="button" class="btn btn-primary  pull-right" ng-click="savePropertyAmenities()">Save</button>
                        </div>
                        
                      </div>
                    </section>

                        </form>
              

                </div><!-- /.tab-content -->
              	  
                          	  
              	   <div class="tab-pane" id="fa-icons3">
                  <form id ="form2" name="form2" method="post" action="" enctype="multipart/form-data" theme="simple">
                    <s:hidden name="propertyId" value="%{propertyId}"></s:hidden>
                  	
                    <section id="new">
                      <!-- <h4 class="page-header">General Settings</h4> -->
                      <input type="hidden" name="accommodationId" id="accommodationId" value="">
                      <div class="row fontawesome-icon-list">
                      	<div class="col-md-3 col-sm-4" ng-repeat="accam in accamenities">
	                        	 <div class="form-group" >
	                        	 	<label>
                      					<input type="checkbox" name="checkedAmenity1[]"  id="checkedAmenity1" value="{{accam.DT_RowId}}"  class="flat-red" ng-checked ="accam.status == 'true'">&nbsp {{accam.amenityName}}
                    				</label>
	                        	 </div>
	                        	 
	                        	
	                        </div>
                        
                        
                      </div>
                    </section>

                    

					
                    

                    <section id="video-player" >
                      <h4 class="page-header"></h4>
                       <div class="row fontawesome-icon-list" >
                        <div class="col-md-3 col-sm-4"></div>
                        <div class="col-md-3 col-sm-4"></div>
                        <div class="col-md-3 col-sm-4"></div>
                        <div class="col-md-3 col-sm-4">
                        
                        	<button type="button"  class="btn btn-primary  pull-right" ng-click="saveAccommodationAmenities()">Save</button>
                        
                      </div>
                    </section>

                        </form>
              

                </div><!-- /.tab-content -->
              	  
              	  
              
          
              	  
              
              </div><!-- /.nav-tabs-custom -->
              
             
                        
            </div><!-- /.col -->
          </div><!-- /.row -->
          </div>
        
        	
          
          
        </section><!-- /.content -->
      
  
       
		
		

      </div><!-- /.content-wrapper -->
     


<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>

	<script>

		var app = angular.module('myApp', ['ngProgress']);
		app.controller('customersCtrl', function($scope, $http,$timeout,ngProgressFactory) {

			$("#alert-message").hide();
			//$scope.progressbar = ngProgressFactory.createInstance();
			//$scope.progressbar.setColor("green");
			//$scope.progressbar.start();
	       	$timeout(function(){
	      	    // $scope.progressbar.complete();
	            $scope.show = true;
	            $("#pre-loader").css("display","none");
	        }, 2000);

			 
			
			$scope.unread = function() {
			//var notifiurl = "http://localhost:8085/collaborative-workflow/unreadnotifications.action";
			var notifiurl = "unreadnotifications.action";
			$http.get(notifiurl).success(function(response) {
				$scope.latestnoti = response.data;
			});
			};
			
			$scope.getPropertyAmenities = function() {

				var url = "get-property-amenities";
				$http.get(url).success(function(response) {
					//console.log(response);
					$scope.amenities = response.data;
			
				});
			};
			
		$scope.getAccommodationAmenities = function(rowId) {
					
				//$('#adminId').val()	
				//alert(rowId);
				document.getElementById("accommodationId").value = rowId;
				var url = "get-accommodation-amenities?accommodationId="+rowId;
				$http.get(url).success(function(response) {
					//console.log(response);
					$scope.accamenities = response.data;
			
				});
			}; 
			
			$scope.getAccommodationAmenities1 = function() {
				
				//$('#adminId').val()			
				var accommodationId = $('#accommodationId').val();
	
				//document.getElementById("accommodationId").value = accommodationId;
				var url = "get-accommodation-amenities1?accommodationId="+accommodationId;
				$http.get(url).success(function(response) {
					//console.log(response);
					$scope.accamenities1 = response.data;
			
				});
			}; 
			
			
			
			$scope.savePropertyAmenities = function(){
				
				 var checkbox_value = "";
				    $(":checkbox").each(function () {
				        var ischecked = $(this).is(":checked");
				        if (ischecked) {
				            checkbox_value += $(this).val() + ",";
				        }
				    });
				   
			    
				//alert($('#taskId').val());
				//alert("ds");
				//var checkedValue = $('.flat-red:checked').val();
		        
				var fdata = "checkedAmenity=" + checkbox_value
				+ "&propertyId=1";
				if(checkbox_value == 0){
		
				alert("Please Select Any Amenities");
				}
				else{
				$http(
						{
							method : 'POST',
							data : fdata,
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							url : 'save-property-amenities'
						}).then(function successCallback(response) {
							
							$scope.getPropertyAmenities();
							var alert = ' <div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Alert!</h4>Success Amenities have Been Saved Succesfully.</div>';
				            $('#message').html(alert);
							$timeout(function(){
					      	$('#btnclose').click();
					        }, 2000);
							
							//$('#btnclose').click();
				}, function errorCallback(response) {
					
				  
					// called asynchronously if an error occurs
					// or server returns response with an error status.
				});
				}

			};
			
			$scope.saveAccommodationAmenities = function(id){
				
				
				 var accommodationId = $('#accommodationId').val();
				 //alert(accommodationId);
				 
				 var checkbox_value = "";
				 
				    
				    $("#form2 :checkbox").each(function () {
				        var ischecked = $(this).is(":checked");
				        if (ischecked) {
				            checkbox_value += $(this).val() + ",";
				        }
				    });
				   
				   
				    
				    var fdata = "checkedAmenity1=" + checkbox_value
					+ "&accommodationId=" + accommodationId;
					//alert(fdata);
					if(checkbox_value == 0){
			
					alert("Please Select Any Amenities");
					}
					else{
					$http(
							{
								method : 'POST',
								data : fdata,
								headers : {
									'Content-Type' : 'application/x-www-form-urlencoded'
								},
								url : 'save-accommodation-amenities'
							}).then(function successCallback(response) {
								
								$scope.getAccommodationAmenities1();
								var alert = ' <div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Alert!</h4>Success Amenities have Been Saved Succesfully.</div>';
					            $('#message').html(alert);
								$timeout(function(){
						      	$('#btnclose').click();
						        }, 2000);
								
								//$('#btnclose').click();
					}, function errorCallback(response) {
						
					  
						// called asynchronously if an error occurs
						// or server returns response with an error status.
					});
				//alert($('#taskId').val());
				//alert("ds");
				//var checkedValue = $('.flat-red:checked').val();
					}
				

			};
			
			 $scope.getPropertyList = function() {
				 
		        	
		        	
			        var userId = $('#adminId').val();
		 			var url = "get-user-properties?userId="+userId;
		 			$http.get(url).success(function(response) {
		 			    
		 				$scope.props = response.data;
		 	
		 			});
		 		};
		 		
           $scope.change = function() {
     	   
		        //alert($scope.id);
		        
		        var propertyId = $scope.id;	
     	        var url = "change-user-property?propertyId="+propertyId;
	 			$http.get(url).success(function(response) {
	 				
	 				 window.location = '/ulopms/dashboard'; 
	 				//$scope.change = response.data;
	 	
	 			});
			       
		 		};
		 		
		 		$scope.getAccommodations = function() {
				//	var tabs = [];	
					var url = "get-accommodations";
					$http.get(url).success(function(response) {
						$scope.accommodations = response.data;
									 		/*	var json = JSON.stringify(response.data);
				 		
				 						 tabs = response.data.map(function(a){
				 							   return a.accommodationType;
				 							});   */
				});
				};
		 		
				
		    $scope.getPropertyList();
			
			$scope.getPropertyAmenities();
			
			$scope.getAccommodations();
			
			$scope.getAccommodationAmenities1();
			//$scope.unread();
			
			
			
		});

		//iCheck for checkbox and radio inputs
        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
          checkboxClass: 'icheckbox_minimal-blue',
          radioClass: 'iradio_minimal-blue'
        });
        //Red color scheme for iCheck
        $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
          checkboxClass: 'icheckbox_minimal-red',
          radioClass: 'iradio_minimal-red'
        });
        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
          checkboxClass: 'icheckbox_flat-green',
          radioClass: 'iradio_flat-green'
        });
		
	</script>
	
	 <script src="js/jquery-2.1.1.js" type="text/javascript"></script>
	  <!-- iCheck 1.0.1 -->
    <script src="plugins/iCheck/icheck.min.js"></script>
       