<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="s" uri="/struts-tags"%>
	<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Upload Resume
          </h1>
          <ol class="breadcrumb">
            <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Careers</li>
          </ol>
        </section>
        
        <div  class="row">
			<div class="col-xs-12" >
				<div class="box-body table-responsive no-padding" >
					
					<form method="post" theme="simple" name="addproperty">
					 <section>
						<div class="modal-body" >
							<div class="form-group col-md-6">
								<label>Upload Resume</label>
								<input type="file" class="form-control"   file-input="files" name="photo" id="photo">
								<span style="color:red">upload only(214 * 221)</span>
							</div>
						</div>
					</section>
					<section>
						<div class="modal-footer">
							<a href="#" data-dismiss="modal" class="btn btn-danger">Close</a>
							<button ng-click="addGoogleProperty()" class="btn btn-primary">Save</button>
						</div>
					</section>
					</form>
				</div>
			</div>
		</div>
	        
     </div>
	
   <!-- Content Wrapper. Contains page content -->
            


<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>

	<script>
 
	
	var app = angular.module('myApp',['ngFileUpload']);
	app.controller('customersCtrl',['$scope',
	             					'Upload',
	            					'$timeout',
	            					'$http', function($scope,Upload,$timeout,$http) {

		
		
		$scope.settings = {
	            currentPage: 0,
	            offset: 0,
	            pageLimit: 10,
	            pageLimits: ['10', '50', '100']
	          };

		
	       	$timeout(function(){
	            $scope.show = true;
	            $("#pre-loader").css("display","none");
	        }, 2000);

			 
			
			$scope.unread = function() {
			var notifiurl = "unreadnotifications.action";
			$http.get(notifiurl).success(function(response) {
				$scope.latestnoti = response.data;
			});
			};

			
			
			   
			$scope.addGoogleProperty = function(){
				
				 var file = $scope.files;
				 alert(file); 
				 Upload.upload({
		                url: 'resume-upload',
		                enableProgress: true, 
		                data: {myFile: file} 
		            }).then(function (resp) {
		            	
		            	
		            });
			};
			
	}]);


	app.directive('fileInput', ['$parse', function ($parse) {
	    return {
	        restrict: 'A',
	        link: function (scope, element, attributes) {
	            element.bind('change', function () {
	                $parse(attributes.fileInput)
	                .assign(scope,element[0].files[0])
	                scope.$apply()
	            });
	        }
	    };
	}]);		
		   
		        

		
	</script>
			
	<link rel="stylesheet" href="dist/css/dataTables.bootstrap.css">
	<script src="js1/upload/ng-file-upload-shim.min.js"></script>
	<script src="js1/upload/ng-file-upload.min.js"></script>
	 <script src="js1/jquery.dataTables.min.js" type="text/javascript"></script>
	 <script src="js1/dataTables.bootstrap.min.js" type="text/javascript"></script>
       