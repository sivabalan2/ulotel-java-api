<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="s" uri="/struts-tags"%>

   <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <section class="content-header">
          <h1>
            ULO Reports
            <!--<small>Subscription Expiry on {{property[0].subscription[0].expirydate}}</small>-->
          </h1>
          <ol class="breadcrumb">
            <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Report</a></li>
            <li class="active">ULO Reports</li>
          </ol>
        </section>
 		
 		<!-- Main content -->
    <section class="content">
    <div class="box box-default color-palette-box">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-tag"></i> Search &amp; Filter</h3>
        </div>
        <div class="box-body">
        <form>
          <div class="row">
          	<div class="form-group col-md-3">
			  <label>Select Location :</label>
			  <select  name="reportLocationId" id="reportLocationId" ng-model="reportLocationId" ng-required="true" ng-change="getReportAreas()" class="form-control" >
			  	<option disabled selected value> &nbsp;-- Locations --&nbsp; </option>
			  	<option style="display:none" value="0">All</option>
                 <option ng-repeat="l in locations" value="{{l.googleLocationId}}">{{l.googleLocationDisplayName}}</option>
               </select>
			</div>
            <div class="form-group col-md-3">
			  <label>Select Area :</label>
			  <select name="reportAreaId" ng-model="reportAreaId" id="reportAreaId" ng-required="true" ng-change="getReportAreaProperties()" class="form-control" >
			 	 <option disabled selected value> &nbsp;-- Areas --&nbsp; </option>
			   	<option style="display:none" value="0">All</option>
                 	<option ng-repeat="ar in reportareas" value="{{ar.googleAreaId}}">{{ar.googleAreaDisplayName}}</option>
                 </select>
			</div>  
          </div>
          <div ng-if="reportAreaProperty.length>0">
          	<div class="row">
				<div class="form-group col-md-12" id="reportProperties">
					<label>List of Properties :</label>
				   	<div class="input-group" >
                        <label class="checkbox-inline" ng-repeat="ap in reportAreaProperty">
            				<input type="checkbox" name="checkedProperty[]"  id="checkedProperty" value="{{ap.propertyId}}" ng-model="ap.checked" class="flat-red" ng-click="reportPropertyList(ap.propertyId,ap.propertyName,$index,reportAreaProperty)" >&nbsp {{ap.propertyName}}
          				</label>
				   	</div>
				</div>
			</div>
          </div>
          <div ng-if="reportlist.length>0">
          	<div class="row">
				<div class="form-group col-md-12" id="reportList">
					<label>List of Reports :</label>
				   	<div class="input-group" >
                        <label class="checkbox-inline" ng-repeat="rl in reportlist">
            				<input type="checkbox" name="checkedReports[]"  id="checkedReports" ng-model="rl.checked" ng-checked="" ng-click="reportColumnList(rl.reportId,rl.reportName,$index,reportlist)" class="flat-red" >&nbsp {{rl.reportName}}
          				</label>
				   	</div>
				</div>
			</div>
          </div>
          <div id="searchOptionsReport">	
          <div class="row">
          <div class="col-sm-2 col-md-3">
           <div class="form-group">
                <label>From Date :</label>

                <div class="input-group date">
                <label class="input-group-addon btn" for="fromDate">
					<span class="fa fa-calendar"></span>
				</label>
                  <input type="text" class="form-control pull-right" id="fromDate" name="fromDate" value="" onkeypress="return false;" autocomplete="off">
                </div>
              </div>
              
              </div>
              <div class="col-sm-2 col-md-3">
           <div class="form-group">
                <label>To Date :</label>

                <div class="input-group date">
                  <label class="input-group-addon btn" for="toDate">
					<span class="fa fa-calendar"></span>
				</label>
                  <input type="text" class="form-control pull-right" id="toDate" name="toDate" value="" onkeypress="return false;" autocomplete="off">
                </div>
                <!-- /.input group -->
              </div>
              
              </div>
              <div class="col-sm-2 col-md-2" id="divAccommodations">
              <div class="form-group">
                <label>Accommodation Type :</label>
                <select class="form-control select2" style="width: 100%;" id="accommodationId" name="accommodationId">
                  <option selected="selected" value="0">All</option>
                  <option ng-repeat="acc in accommodations" value="{{acc.accommodationId}}">{{acc.accommodationType}}</option>
                </select>
              </div>
              </div>
              <div class="form-group col-md-2" id="divBookingId">
				<label>Booking Id</label>
				<input type="text"  name="filterBookingId" class="form-control" id="filterBookingId" ng-pattern="/^[0-9]/" maxlength="10" placeholder="Booking Id" autocomplete="off">
			  </div>
              
          </div>
           <div class="row">
	          	<div class="form-group col-md-2" id="divBookingDate">
					<input type="radio" name="filterDate" id="filterBookingDate" value="BookingDate" placeholder="Booking Date">
					<label>Booking Date</label>
				</div>
				<div class="form-group col-md-2" id="divCheckInDate">
					<input type="radio" name="filterDate" id="filterCheckInDate" value="CheckInDate" placeholder="Check In Date">
					<label>Check In Date</label>
				</div>
				<div class="form-group col-md-2" id="divCheckOutDate">
					<input type="radio"  name="filterDate" id="filterCheckOutDate" value="CheckOutDate" placeholder="Check Out Date">
					<label>Check Out Date</label>
				</div>
				<div class="form-group col-md-2" id="divStayInfoDate">
					<input type="radio"  name="filterDate" id="filterStayInfo" value="CheckStayInfo" placeholder="Stay Info">
					<label>Stay Info</label>
				</div>
				
          </div> 
        </div>
        <div class="row" id="viewDownloadId">
        	<div class="col-sm-2 col-md-2">
              	<div class="form-group">
<!--               <label>Search</label> -->
				<label>&nbsp;</label>
              	<button type="submit" ng-click="getRecordList()" class="btn btn-danger pull-right btn-block btn-sm">View</button>
              	</div>
              </div>
              
				<div class="col-sm-2 col-md-2">
              	<div class="form-group">
<!--               	<label>Download</label> -->
				<label>&nbsp;</label>
				<button class="btn btn-danger pull-right btn-block btn-sm" id="mydownload" ng-csv="bookingrecords" filename="{{fileName}}.csv"
				 csv-header="getHeader()" field-separator="," decimal-separator="." >Download</button>
              	</div>
              </div>
        	
        	  <div class="col-sm-2 col-md-2" id="searchRecordId"> 
				<label>Search :</label>
				<input type="text" ng-model="search" class="form-control" placeholder="Search">
			  
			  </div>
        </div>
          <!-- /.row -->
          </form>
        </div>
        <!-- /.box-body -->
      </div>
      
      
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Results</h3>

              
            </div>
            <!-- /.box-header -->
            <div  class="box-body table-responsive no-padding" style="height:300px;" ng-if="bookingrecords.length==0">
            	<table class="table table-hover">
               <tr>
<!--                 	<th><label id="remarksId" class="flat-red"></label> </th> -->
					<th class="flat-red">No records found</th>
                </tr>
                </table>
            </div>
            
            <div class="box-body table-responsive no-padding" style="height:300px;" ng-if="bookingrecords.length>0">
              <table class="table table-hover table-striped">
                <tr role="row">
                  <th ng-repeat="columns in reportColumns" style="overflow:hidden;white-space:nowrap;">{{columns.columnName}}</th>
                </tr>
                <tr ng-repeat="(key,value) in bookingrecords | filter:search">
                  <td ng-repeat="(key,value) in value" >{{value}}</td>
                </tr>
               
                
             </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
      
    </section>
 		
 		
</div>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>
<!-- csv download script start -->
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.15/angular-sanitize.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ng-csv/0.3.6/ng-csv.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.0rc1/angular-route.min.js"></script>
<!-- csv download script end -->
	 <script src="js/jquery-2.1.1.js" type="text/javascript"></script>

	<script>
	var app = angular.module('myApp', ['ngProgress','ngSanitize', 'ngCsv']);
	app.controller('customersCtrl', function($scope, $http,$timeout,ngProgressFactory) {
		
		  $("#fromDate").datepicker({
		      dateFormat: 'MM d, yy',
		    
		      onSelect: function (formattedDate) {
		          var date1 = $('#fromDate').datepicker('getDate'); 
		          var date = new Date( Date.parse( date1 ) ); 
		         // date.setDate( date.getDate() + 1 );        
		          var newDate = date.toDateString(); 
		          newDate = new Date( Date.parse( newDate ) );   
		          $('#toDate').datepicker("option","minDate",newDate);
		          $timeout(function(){
		        	  document.getElementById("fromDate").style.borderColor = "LightGray";
		          });
		      }
		  });

		  $("#toDate").datepicker({
		      dateFormat: 'MM d, yy',
		   
		      onSelect: function (formattedDate) {
		          var date2 = $('#toDate').datepicker('getDate'); 
		          $timeout(function(){
		        	  document.getElementById("toDate").style.borderColor = "LightGray";
		          });
		      }
		  }); 
		  var spinner = $('#loadertwo');
		  $scope.fileName="uloreport";
		  $scope.listcolumns=[];
		  $scope.getRecordList = function(){
				var propertyId=$scope.propertyId;
				var reportName=$scope.reportName;
				var filterDateName="",filterBookingId="", displayReport="";
				if(document.getElementById('filterBookingDate').checked){
					filterDateName='BookingDate';
				}
				if(document.getElementById('filterCheckInDate').checked){
					filterDateName='CheckInDate';
				}
				if(document.getElementById('filterCheckOutDate').checked){
					filterDateName='CheckOutDate';
				}
				if(document.getElementById('filterStayInfo').checked){
					filterDateName='StayInfo';
				}
				
				
				filterBookingId=document.getElementById('filterBookingId').value;
				if(filterBookingId=="" || filterBookingId=="0"){
					filterBookingId="0";
					displayReport="DateFilter";
				}else{
					displayReport="BookingIdFilter";
				}
				
				if(reportName=="REVENUE REPORTS"){
					displayReport='Revenue';
				}
				if(reportName=="USER REPORTS"){
					displayReport='Users';
				}
				
				if(displayReport=="DateFilter"){
					
					
					if(document.getElementById('fromDate').value==""){
			 			 document.getElementById('fromDate').focus();
			 			 document.getElementById("fromDate").style.borderColor = "red";
			 		 }else if(document.getElementById('toDate').value==""){
			 			 document.getElementById('toDate').focus();
			 			document.getElementById("toDate").style.borderColor = "red";
			 		 }else if(document.getElementById('fromDate').value!="" && document.getElementById('toDate').value!=""){

			 			var fdata = "strFromDate=" + $('#fromDate').val()+ "&strToDate=" + $('#toDate').val()+ "&accommodationId="
						+$('#accommodationId').val()+"&filterDateName="+filterDateName+'&filterBookingId='+filterBookingId+"&propertyId="+propertyId+"&reportName="+reportName;
			 			spinner.show();
						$http(
								{
									method : 'POST',
									data : fdata,
									headers : {
										'Content-Type' : 'application/x-www-form-urlencoded'
									},
									url : 'booking-records'
								}).success(function(response) {
									//
									var accommid=$('#accommodationId').val();
									$scope.bookingrecords = response.data;
									if($scope.bookingrecords.length== 0){
										document.getElementById("mydownload").disabled=true;
										$('#searchRecordId').hide();
										if(accommid==0){
											$('#remarksId').text("No records found in selected date");
										}else{
											$('#remarksId').text("No records found in category on selected date");	
										}
										
									}else if($scope.bookingrecords.length > 0){
										
										document.getElementById("mydownload").disabled=false;
										$('#searchRecordId').show();
									}
									  setTimeout(function(){ spinner.hide(); 
									   }, 1000);
									 	
								});
			 		 }
				}else if(displayReport=="BookingIdFilter"){
					var fdata = '&filterBookingId='+filterBookingId+"&propertyId="+propertyId+"&reportName="+reportName;
					spinner.show();
					$http(
						{
							method : 'POST',
							data : fdata,
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							url : 'booking-records'
						}).success(function(response) {
							
							$scope.bookingrecords = response.data;
							if($scope.bookingrecords.length>0){
								document.getElementById("mydownload").disabled=false;
								$('#searchRecordId').show();
							}else if($scope.bookingrecords.length==0){
								document.getElementById("mydownload").disabled=true;
								$('#remarksId').text("No records found in entered booking id");
								$('#searchRecordId').hide();
							}
							  setTimeout(function(){ spinner.hide(); 
							   }, 1000);
						});
				}else if(displayReport=="Users"){
					if(document.getElementById('fromDate').value==""){
			 			 document.getElementById('fromDate').focus();
			 			 document.getElementById("fromDate").style.borderColor = "red";
			 		 }else if(document.getElementById('toDate').value==""){
			 			 document.getElementById('toDate').focus();
			 			document.getElementById("toDate").style.borderColor = "red";
			 		 }else if(document.getElementById('fromDate').value!="" && document.getElementById('toDate').value!=""){

			 			var fdata = "strFromDate=" + $('#fromDate').val()+ "&strToDate=" + $('#toDate').val()+"&reportName="+reportName;
			 			spinner.show();
						$http(
								{
									method : 'POST',
									data : fdata,
									headers : {
										'Content-Type' : 'application/x-www-form-urlencoded'
									},
									url : 'booking-records'
								}).success(function(response) {
									
									$scope.bookingrecords = response.data;
									if($scope.bookingrecords.length>0){
										document.getElementById("mydownload").disabled=false;
										$('#searchRecordId').show();
									}else if($scope.bookingrecords.length==0){
										document.getElementById("mydownload").disabled=true;
										$('#remarksId').text("No records found in selected date");
										$('#searchRecordId').hide();
									}
									   setTimeout(function(){ spinner.hide(); 
									   }, 1000);

								});
			 		 }
				}else if(displayReport=="Revenue"){
					if(document.getElementById('fromDate').value==""){
			 			 document.getElementById('fromDate').focus();
			 			 document.getElementById("fromDate").style.borderColor = "red";
			 		 }else if(document.getElementById('toDate').value==""){
			 			 document.getElementById('toDate').focus();
			 			document.getElementById("toDate").style.borderColor = "red";
			 		 }else if(document.getElementById('fromDate').value!="" && document.getElementById('toDate').value!=""){

			 			var fdata = "strFromDate=" + $('#fromDate').val()+ "&strToDate=" + $('#toDate').val()+ "&accommodationId="
						+$('#accommodationId').val()+"&propertyId="+propertyId+"&reportName="+reportName;
			 			spinner.show();
						$http(
								{
									method : 'POST',
									data : fdata,
									headers : {
										'Content-Type' : 'application/x-www-form-urlencoded'
									},
									url : 'booking-records'
								}).success(function(response) {
									
									$scope.bookingrecords = response.data;
									if($scope.bookingrecords.length>0){
										document.getElementById("mydownload").disabled=false;	
										$('#searchRecordId').show();
									}else if($scope.bookingrecords.length==0){
										document.getElementById("mydownload").disabled=true;
									}
								});
						   setTimeout(function(){ spinner.hide(); 
						   }, 1000);

			 		 }
				}
			};
			
			$scope.getLocations = function() {

				var url = "report-google-locations";
				$http.get(url).success(function(response) {
					$scope.locations = response.data;
		
				});
			};
			
			$scope.getReportAreas = function(){
				$scope.bookingrecords =["data"];
   				$scope.reportAreaProperty=[];
   				$scope.reportlist=[];
   				$scope.reportColumns =[];
   				$('#searchOptionsReport').hide();
   				$('#viewDownloadId').hide();
   				
   				var locationId=$('#reportLocationId').val();
   				var url ="report-google-location-areas?areaLocationId="+locationId;
   				$http.get(url).success(function(response) {
   					$scope.reportareas = response.data;
   				});
   			};
			
   			$scope.getReportAreaProperties = function() {
   				$scope.reportColumns =[];
   	 			var areaId=$('#reportAreaId').val();
   	 			var url="report-google-area-properties?googleAreaId="+areaId;
   	 			//alert(url)
   	 			$http.get(url).success(function(response) {
					$scope.reportAreaProperty = response.data;
		
				});
   	 		};
			$scope.getAccommodations = function() {
				var propertyId=$scope.propertyId;
				
				var url = "ulo-reports-property-accommodations?propertyId="+propertyId;
				$http.get(url).success(function(response) {
				    //console.log(response);
					$scope.accommodations = response.data;
		
				});
			};
			
			$scope.getListReports = function(){
   				var url ="list-reports";
   				$http.get(url).success(function(response) {
   					$scope.reportlist = response.data;
   				});
   			};
   			
   			$scope.reportPropertyList= function(id,name,indx,properties){
   				$scope.bookingrecords =["data"];
   				$scope.reportlist=[];
   				$scope.reportColumns =[];
   				angular.forEach(properties, function(ap,index){
   					
   					if(indx!=index){
   						ap.checked=false;
   					}
   				});
   				

   				$scope.propertyId=id;
   				$scope.getListReports();
   				$scope.getAccommodations();
   				$('#searchOptionsReport').hide();
   				$('#viewDownloadId').hide();
//    				document.getElementById("mydownload").disabled=true;
   			};
   			
   			$scope.reportColumnList = function(id,name,indx,columns){
   				angular.forEach(columns, function(rl,index){
   					if(indx!=index){
   						rl.checked=false;
   					}
   				});
   				$('#searchOptionsReport').show();
   				$('#viewDownloadId').show();
   				if(name=="REVENUE REPORTS"){
   					$('#divAccommodations').show();
   	   				$('#divBookingId').hide();
   	   				$('#divBookingDate').hide();
   	   				$('#divCheckInDate').hide();
   	   				$('#divCheckOutDate').hide();
   	   				$('#divStayInfoDate').hide();
//    	   				$('#reportProperties').show();
   	   				document.getElementById("filterBookingDate").checked=false;
   	   				document.getElementById("filterBookingId").value="";
   	   				$scope.fileName="revenuereport";
   				}else if(name=="USER REPORTS"){
   					$('#divAccommodations').hide();
   	   				$('#divBookingId').hide();
   	   				$('#divBookingDate').hide();
   	   				$('#divCheckInDate').hide();
   	   				$('#divCheckOutDate').hide();
   	   				$('#divStayInfoDate').hide();
//    	   				$('#reportProperties').hide();
   	   				document.getElementById("filterBookingDate").checked=false;
   	   				document.getElementById("filterBookingId").value="";
   	   				$scope.fileName="guestuserreport";
   				}else{
   					$('#divAccommodations').show();
   	   				$('#divBookingId').show();
   	   				$('#divBookingDate').show();
   	   				$('#divCheckInDate').show();
   	   				$('#divCheckOutDate').show();
   	   				$('#divStayInfoDate').show();
//    	   				$('#reportProperties').show();
   	   				document.getElementById("filterBookingDate").checked=true;
   				}
   				
   				if(name=="CANCELLATION REPORTS"){
   					$scope.fileName="bookingcancellationreport";
   				}
				if(name=="RESORT BOOKING REPORTS"){
					$scope.fileName="resortbookingreport";					
				}
				if(name=="ULO BOOKING REPORTS"){
					$scope.fileName="ulobookingreport";	
				}
				if(name=="HOTEL BOOKING REPORTS"){
					$scope.fileName="hotelbookingreport";	
				}
				document.getElementById("mydownload").disabled=true;
   				$scope.reportId=id;
   				$scope.reportName=name;
   				
   				var url ="list-report-columns?reportId="+id+"&reportName="+name;
   				$http.get(url).success(function(response) {
   					$scope.reportColumns = response.data;
   					$scope.listcolumns=[];
   					for(var i=0; i<$scope.reportColumns.length;i++){
   						var columns=$scope.reportColumns[i].columnName;
   						$scope.listcolumns.push(columns);
   					}
   					$scope.bookingrecords=["data"];
   					$scope.getHeader();
   					
   				});
   			};
			
			$scope.getLocations();
			$scope.getHeader=function(){
				
				return $scope.listcolumns;
			};
			$('#reportList').hide();
			$('#searchOptionsReport').hide();
			$('#viewDownloadId').hide();
			document.getElementById("mydownload").disabled=true;
			$('#searchRecordId').hide();
			
	});
		</script>
		
		<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
		  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
		  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		  <script src="plugins/datepicker/bootstrap-datepicker.js"></script>
		  
		
