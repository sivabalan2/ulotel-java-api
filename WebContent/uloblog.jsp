
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="robots" content="all,follow">
      <meta name="googlebot" content="index,follow,snippet,archive">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Ulo Hotels Travel Blog</title>
      <meta name="keywords" content="">
      <!-- Bootstrap and Font Awesome css -->
      <link rel="stylesheet" href="blog/uloblog/css/font-awesome.css">
      <link rel="stylesheet" href="blog/uloblog/css/bootstrap.css">
      <!-- Css animations  -->
      <link href="blog/uloblog/css/animate.css" rel="stylesheet">
      <!-- Theme stylesheet, if possible do not edit this stylesheet -->
      <!-- <link href="uloblog/css/style.default.css" rel="stylesheet" id="theme-stylesheet"> -->
      <!-- Custom stylesheet - for your changes -->
      <link href="blog/uloblog/css/custom.css" rel="stylesheet">
      <!-- Favicon and apple touch icons-->
      <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" />
      <link rel="icon" href="blog/uloblog/images/favicon.ico" type="image/gif" />
      <!-- owl carousel css -->
      <link href="blog/uloblog/css/owl.carousel.css" rel="stylesheet">
      <link href="blog/uloblog/css/owl.theme.css" rel="stylesheet">
      
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PHHZW8M');</script>
<!-- End Google Tag Manager -->
      
   </head>
   <body ng-app="myApp" ng-controller="customersCtrl">
      <!--  blog header begins  -->
      <div id="loader">
         <div class="loader-container">
            <h3 class="loader-back-text"><img src="ulowebsite/images/loader.gif" alt="budget hotels in chennai" class="loader"></h3>
         </div>
      </div>
      <section class="blogheader">
         <div class="container">
            <div clas="row">
               <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                  <div class="bloglogo">
                     <a href="blog" class="bloglogolink"><img src="blog/uloblog/images/uloblog.png" alt="Welcome to ulo blog" class="img-responsive" /></a>
                  </div>
               </div>
               <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                  <div class="hotellink" ><a target="_blank" href="https://www.ulohotels.com/">Book Budget Hotels</a></div>
               </div>
               <div class="blogmenu">
                  <div class="hidden-lg hidden-md col-sm-12 col-xs-12">
                     <div class="social"> 
                        <a href="https://www.facebook.com/Ulohotels/" class="email" data-animate-hover="pulse" target="_blank"><i class="fa fa-facebook"></i></a>
                        <a href="https://twitter.com/Ulohotels" class="external facebook" target="_blank" data-animate-hover="pulse"><i class="fa fa-twitter"></i></a>
                        <a href="https://plus.google.com/u/0/+ULOHOTELS" class="external gplus" target="_blank" data-animate-hover="pulse"><i class="fa fa-google-plus"></i></a>
                        <a href="https://in.pinterest.com/ulohotels/pins/" class="external twitter" target="_blank" data-animate-hover="pulse"><i class="fa fa-pinterest"></i></a>
                        <a href="https://www.instagram.com/ulohotels/?hl=en" class="external twitter" target="_blank" data-animate-hover="pulse"><i class="fa fa-instagram"></i></a>
                     </div>
                  </div>
                  <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                     <div class="navbar navbar-default yamm" role="navigation" id="navbar">
                        <div class="container">
                           <div class="navbar-header">
                              <div class="navbar-buttons">
                                 <button type="button" class="navbar-toggle btn-template-main" data-toggle="collapse" data-target="#navigation">
                                 <span class="sr-only">Toggle navigation</span>
                                 <i class="fa fa-align-justify"></i>
                                 </button>
                              </div>
                           </div>
                           <!--/.navbar-header -->
                           <div class="navbar-collapse collapse" id="navigation">
                              <ul class="nav navbar-nav navbar-left" >
                                 <li class="dropdown active ">
                                    <a href="blog" class="dropdown-toggle" >Home </a>
                                 </li>
                                 <li class="dropdown use-yamm yamm-fw" ng-repeat="abc in allblogcategories">
                                    <a href="{{abc.blogNamespace}}/{{abc.blogCategoryUrl}}" class="dropdown-toggle"  >{{abc.blogCategoryName}}</a>  <!-- ng-click="getBlogCategoryArticles(abc.blogCategoryId)" -->
                                 </li>
                              </ul>
                           </div>
                        </div>
                     </div>
                     <!-- /#navbar -->
                  </div>
                  <div class="col-lg-3 col-md-3 hidden-sm hidden-xs">
                     <div class="social"> 
                        <a href="https://www.facebook.com/Ulohotels/" class="email" data-animate-hover="pulse" target="_blank"><i class="fa fa-facebook"></i></a>
                        <a href="https://twitter.com/Ulohotels" class="external facebook" target="_blank" data-animate-hover="pulse"><i class="fa fa-twitter"></i></a>
                        <a href="https://plus.google.com/u/0/+ULOHOTELS" class="external gplus" target="_blank" data-animate-hover="pulse"><i class="fa fa-google-plus"></i></a>
                        <a href="https://in.pinterest.com/ulohotels/pins/" class="external twitter" target="_blank" data-animate-hover="pulse"><i class="fa fa-pinterest"></i></a>
                        <a href="https://www.instagram.com/ulohotels/?hl=en" class="external twitter" target="_blank" data-animate-hover="pulse"><i class="fa fa-instagram"></i></a>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- row ends -->
         </div>
      </section>
      <!--  blog header Ends  -->
      <!--  blog home carousal begins  -->
      <section class="homecarousal">
         <div class="container">
            <div class="row">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"">
                  <!-- Carousel -->
                  <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" >
                     <!-- Indicators -->
                     <!-- Wrapper for slides -->
                     <div class="carousel-inner">
                        <div class="item" ng-class="{active:!$index}" ng-repeat="aba in allblogarticles track by $index">
                           <img ng-src="get-blog-article-title-image?blogArticleId={{aba.blogArticleId}}"   class="img-responsive">  
                           <div class="header-text hidden-xs">
                              <div class="col-md-12 text-center">
                                 <a href="{{aba.blogNamespace}}/{{aba.blogArticleUrl}}" >
                                    <h2>
                                       <span>{{aba.blogArticleTitle}}</span>
                                    </h2>
                                 </a>
                              </div>
                           </div>
                           <!-- /header-text -->
                        </div>
                     </div>
                     <%-- <div class="carousel-inner">
                        <div class="item active">
                        	<img src="get-blog-article-title-image?blogArticleId={{aba.blogArticleId}}" class="img-responsive" alt="First slide">
                                     <!-- Static Header -->
                                     <div class="header-text hidden-xs">
                                         <div class="col-md-12 text-center">
                                            <a href="" ><h2>
                                             	<span>welcome to ulo hotels blog which u gs</span>
                                             </h2></a>
                                            </div>
                                     </div><!-- /header-text -->
                        </div>
                        </div> --%>
                     <!-- Controls -->
                     <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                     <span class="glyphicon glyphicon-chevron-lefts">
                     <img src="blog/uloblog/images/slider/iconleft.png" alt="ulohotels-right"></span>
                     </a>
                     <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                     <span class="glyphicon glyphicon-chevron-rights">
                     <img src="blog/uloblog/images/slider/iconright.png" alt="ulohotels-right"></span>
                     </a>
                  </div>
                  <!-- /carousel -->
               </div>
            </div>
         </div>
      </section>
      <!--  blog home carousal Ends  -->
      <!--  blog home content list Begins  -->
      <section class="bloglist" >
         <div class="container">
            <div class="row">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blogbox" ng-repeat="aba in allblogarticles | orderBy : '-createdDate'">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                     <a href="{{aba.blogNamespace}}/{{aba.blogArticleUrl}}">
                        <h3>{{aba.blogArticleTitle}}</h3>
                     </a>
                     <span class="publishdate">{{aba.createdDate}}</span>
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12"">
                     <img src="get-blog-article-title-image?blogArticleId={{aba.blogArticleId}}" class="img-responsive imagelist">
                  </div>
                  <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12"">
                     <p> {{aba.blogArticleDescription}}</p>
                     <%-- <%= request.getAttribute("description")%> --%>
                  </div>
               </div>
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pageset">
                  <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
<!--                      <button ng-click="nextPage()" ng-disabled="settings.currentPage === (getTotalPages()   - 1)" class=" pull-right btn-success" > Next &#10095;</button> -->
<%--                      <button ng-click="previousPage()" ng-disabled="settings.currentPage <= 0" class="pull-right btn-success"> &#10094; Previous          --%>
					<button ng-click="nextPage()"  ng-if="settings.currentPage < (getTotalPages() -1 )"  class=" pull-right btn-success" > Next  &#10095;</button><!-- ng-disabled="settings.currentPage === (getTotalPages()   - 1)" -->
                     <button ng-click="previousPage()" ng-if="settings.currentPage > 0" class="pull-right btn-success"> &#10094; Previous         
                     </button>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!--  blog home  list ends  -->
      <!--  blog home destinations list brgins  -->
      <section class="blogdestination">
         <div class="container">
            <div class="row">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blogbox fiveshow">
                  <h3 class="text-center">Destinations</h3>
                  <h5 class="text-center hidden"><a>View All</a></h5>
                  <!-- dest list -->
                  <div class="bloglistfive ">
        
                     <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6" ng-repeat="abl in allbloglocations">
                        <a href="{{abl.blogNamespace}}/{{abl.blogLocationUrl}}"><img src="get-ulo-blog-location-image?blogLocationId={{abl.blogLocationId}}" class="img-responsive destlist"></a>
                        <a href="{{abl.blogNamespace}}/{{abl.blogLocationUrl}}"><h3 class="centered">{{abl.blogLocationName}}</h3></a>
                     </div>
                   
                  </div>
                  <!-- dest list -->
               </div>
            </div>
         </div>
      </section>
      <!--  blog home destinations list Ends   -->
      <!-- blog footer begins ng-init="moveSlide();"-->
      <section class="blogfooter">
         <div class="container">
            <div class="row">
               <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                  <div class="col-lg-4 col-md-4 col-xs-12 col-sm-12 footerlogo">
                     <a href="blog" class="bloglogolink"><img src="blog/uloblog/images/uloblog.png" alt="Welcome to ulo blog" class="img-responsive" /></a>
                     <p>Are you a frequent traveler who loves to explore new places?. Then you have come to the perfect place! The Ulo Hotels blog offers you the best insights about your favorite destinations from useful travel hacks to planning your trip itinerary. We hope this information will be helpful to make your trip spectacular.</p>
                  </div>
                  <div class="col-lg-5 col-sm-12 col-md-5 col-xs-12 popularlist">
                     <h4 class="text-center">Our Popular Post</h4>
                     <div class="col-lg-12" ng-repeat="aba in allblogarticles | limitTo:2">
                        <div class="col-lg-4 col-sm-12 col-md-4 col-xs-12">
                           <a href="{{aba.blogNamespace}}/{{aba.blogArticleUrl}}"><img src="get-blog-article-title-image?blogArticleId={{aba.blogArticleId}}" ></a>
                        </div>
                        <div class="col-lg-8 col-sm-12 col-md-8 col-xs-12">
                           <p>{{aba.blogArticleTitle}}</p>
                           <span class="publishdate">{{aba.createdDate}}</span>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-3 col-sm-12 col-md-3 col-xs-12 footersocial">
                     <h4 class="text-center">Social Medium</h4>
                     <ul>
                        <li><a href="https://www.facebook.com/Ulohotels/" target="_blank"><i class="fa fa-facebook"></i>Facebook</a></li>
                        <li><a href="https://twitter.com/Ulohotels" target="_blank"><i class="fa fa-twitter"></i>Twitter</a></li>
                        <li><a href="https://plus.google.com/u/0/+ULOHOTELS" target="_blank"><i class="fa fa-google-plus"></i>Google Plus</a></li>
                        <li><a href="https://in.pinterest.com/ulohotels/pins/" target="_blank"><i class="fa fa-pinterest"></i>Pinterest</a></li>
                        <li><a href="https://www.instagram.com/ulohotels/?hl=en" target="_blank"><i class="fa fa-instagram "></i>Instagram</a></li>
                        <li><a href="https://www.ulohotels.com/blog/coupon-partners" target="_blank"><i class="fa fa-tags"></i>Coupon Partners</a></li>
                     </ul> 
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!--  blog footer ends -->
      <!-- #### JAVASCRIPT FILES ### -->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
      <script src="blog/uloblog/js/bootstrap.min.js"></script>
      <script src="blog/uloblog/js/jquery.cookie.js"></script>
      <script src="blog/uloblog/js/waypoints.min.js"></script>
      <script src="blog/uloblog/js/jquery.counterup.min.js"></script>
      <script src="blog/uloblog/js/jquery.parallax-1.1.3.js"></script>
      <script src="blog/uloblog/js/front.js"></script>
      <!-- owl carousel -->
      <script src="blog/uloblog/js/owl.carousel.min.js"></script>
      <script type="text/javascript" src="blog/uloblog/js/blogulo.js"></script>
      <script src="blog/uloblog/js/angularjs/angular.min.js" type="text/javascript"></script>
      <script src="blog/uloblog/js/ngprogress.js" type="text/javascript"></script>
      <script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script> 
      <script>
         var app = angular.module('myApp', ['ngProgress']);
         app.controller('customersCtrl', function ($scope, $http, $timeout, ngProgressFactory) {
          
          $scope.settings = {
                  currentPage: 0,
                  offset: 0,
                  pageLimit: 5,
                  pageLimits: ['10', '50', '100']
                };
         
         $scope.getTotalPages = function () {
         return Math.ceil($scope.allblogarticles[0].count / $scope.settings.pageLimit);
         }; 
         
         $scope.isCurrentPageLimit = function (value) {
              return $scope.settings.pageLimit == value;
         };
         
         $scope.previousPage = function () {
           $scope.settings.currentPage -= 1;
           var previousOffset = $scope.settings.currentPage * $scope.settings.pageLimit;
              var url = "get-all-ulo-blog-articles?limit="+$scope.settings.pageLimit+"&offset="+previousOffset;
         $http.get(url).success(function(response) {
         	//console.log(response);
         	$scope.allblogarticles = response.data;
         }); 
           }; 
         
          		$scope.nextPage = function () {
              $scope.settings.currentPage += 1;
              var nextOffset = $scope.settings.currentPage * $scope.settings.pageLimit;
              var url = "get-all-ulo-blog-articles?limit="+$scope.settings.pageLimit+"&offset="+nextOffset;
         $http.get(url).success(function(response) {
         	//console.log(response);
         	$scope.allblogarticles = response.data;
         }); 
            };      		       
           			
          
          $scope.getAllBlogLocations = function() {
         var url = "get-all-ulo-blog-locations";
         $http.get(url).success(function(response) {
            console.log(response);
         $scope.allbloglocations = response.data;
         
         });
         
         };
         
         $scope.getAllBlogCategories = function() {
         var url = "get-all-ulo-blog-categories";
         $http.get(url).success(function(response) {
            console.log(response);
         $scope.allblogcategories = response.data;
         
         });
         
         };	
         
         $scope.getAllBlogArticles = function() {
         var url = "get-all-ulo-blog-articles?limit="+$scope.settings.pageLimit+"&offset="+$scope.settings.offset;
         $http.get(url).success(function(response) {
           console.log(response);
         $scope.allblogarticles = response.data;
         
         });
         
         };	
         
         
         /* 
         	$scope.getBlogCategoryArticles = function(id) {
	         var url = "get-blog-category-articles?blogCategoryId="+id;
	         $http.get(url).success(function(response) {
	           console.log(response);
	         $scope.allblogcatarticles = response.data;
	         
	         });
         
         };	 */
         $scope.myInterval = 5000;
         $scope.moveSlide = function(indx){
        	 alert(indx);
        	 // Instantiate the Bootstrap carousel
             $('.multi-item-carousel').carousel({
               interval: false
             });

             // for every slide in carousel, copy the next slide's item in the slide.
             // Do the same for the next, next item.
             $('.multi-item-carousel .item').each(function(){
               var next = $(this).next();
               if (!next.length) {
                 next = $(this).siblings(':first');
               }
               next.children(':first-child').clone().appendTo($(this));
               
               if (next.next().length>0) {
                 next.next().children(':first-child').clone().appendTo($(this));
               } else {
               	$(this).siblings(':first').children(':first-child').clone().appendTo($(this));
               }
             });
         };
         
      
         $scope.getAllBlogLocations();
         
         $scope.getAllBlogCategories();
         
         $scope.getAllBlogArticles();
         
         
         });
         
      </script>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PHHZW8M"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
   </body>
</html>