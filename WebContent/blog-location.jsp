<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="s" uri="/struts-tags"%>

   <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Blog Location
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
   
            <li class="active">Blog Location</li>
          </ol>
        </section>
		
        <!-- Main content -->
        <section class="content">
        
        	<div class="row">
            	<div class="col-xs-12" > <!-- ng-if="properties.length>=0" -->
						<div class="box">
							<div class="box-header">
								<h3 class="box-title">Blog Location</h3>
							</div>
							
							
							<!-- /.box-header -->
							<div class="box-body table-responsive no-padding" > 
								<table class="table table-hover" id="example1">
									<tr>
										<th>Order</th>
										<th>Location Name</th>
										<th>Edit</th>
										<th>Delete</th>
									
									</tr>
									<tr ng-repeat="bl in bloglocations">
										<td>{{bl.serialNo}}</td>
										<td>{{bl.blogLocationName}}</td>
										<td>
											<a href="#editmodal" ng-click="getBlogLocation(bl.blogLocationId)"  data-toggle="modal"><i  class="fa fa-pencil text-green"></i></a>
										</td>
										<td>
											<a href="#deletemodal" ng-click="deleteBlogLocation(bl.blogLocationId)" data-toggle="modal" ><i  class="fa fa-fw fa-trash text-red" ></i></a>
										</td>
									</tr>
									<tr>
									</tr>
								</table>
							</div>
							<!-- /.box-body -->
							
							
							<div class="box-footer clearfix">
								 <ul class="pagination pagination-sm no-margin pull-right">
									<a class="btn btn-primary " href="#addmodal" data-toggle="modal" >Add Location</a>
								</ul> 
							</div>
							<div class="box-footer clearfix">
								<ul class="pagination pagination-sm"></ul>
								<div class="simple-pagination pull-right" >   
									<p class="simple-pagination__items">Showing {{blogLocationCount[0].count}} out of {{settings.pageLimit}}</p>
									<p><button ng-click="previousPage()" ng-disabled="settings.currentPage <= 0" class="simple-pagination__button simple-pagination__button--prev"> &#10094;          
									</button><span class="simple-pagination__pages">{{settings.currentPage + 1}} of {{getTotalPages()}}</span>
									<button ng-click="nextPage()" ng-disabled="settings.currentPage === (getTotalPages() - 1)" class="simple-pagination__button simple-pagination__button--next">&#10095;</button></p>
									<a href="" ng-click="limited(10)" ng-class="active">{{10}}</a> |
									<span><a href="" ng-click="all(blogLocationCount[0].count)" ng-class="active">All</a></span> 
								</div>
						   </div> 
						</div>
						<!-- /.box -->
					</div>	
         	</div>
          
        </section><!-- /.content -->
      	<div class="modal" id="addmodal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" id="btnclose" class="close" data-dismiss="modal"
							aria-hidden="true">x</button>
						<h4 class="modal-title">Add BlogLocation</h4>
					</div>
					 <form method="post" theme="simple" name="addbloglocation">
					 <section>
					<div class="modal-body" >
						<div class="form-group col-md-12">
						  <label>Location Name<span style="color:red">*</span></label>
						  <input type="text" class="form-control"  name="locationName"  id="locationName"  placeholder="Location Name" value="" ng-model='locationName' ng-pattern="^/^[A-Za-z\d\s]+$/" ng-required="true" maxlength="250">						  
		                  <span ng-show="addbloglocation.locationName.$error.pattern" style="color:red">Not a Category Name!</span>
						</div>
						<div class="form-group col-md-12">
							<label for="locationContent">Location Content<span style="color:red">*</span></label>
							<textarea class="form-control"  name="locationContent"  id="locationContent"  placeholder="Location Content" value="" ng-model='locationContent' ng-pattern="^/^[A-Za-z\d\s]+$/" ng-required="true" maxlength="10485760"></textarea>
						    <span ng-show="addbloglocation.locationContent.$error.pattern" style="color:red">Not a Location Name!</span>
						</div>
						<div class="form-group col-md-12">
						  <label>Location URL<span style="color:red">*</span></label>
						  <input type="text" class="form-control"  name="blogLocationUrl"  id="blogLocationUrl"  placeholder="Location URL" value="" ng-model='locationName' ng-pattern="^/^[A-Za-z\d\s]+$/" ng-required="true" maxlength="250">						  
		                  <span ng-show="addbloglocation.blogLocationUrl.$error.pattern" style="color:red">Not a Category Name!</span>
						</div>
						
						<div class="form-group col-md-6">
							<label>Location Image</label>
							<input type="file" class="form-control"   file-input="files" name="photo" id="photo">
							<span style="color:red">upload only(214 * 221)</span>
<!-- 							<div id="validimage" name="validimage" style="color: red"><label>Not Image Format!!</label></div> -->
						</div>
						<div class="form-group" >
						   <div class="input-group" >
						  		<label>&nbsp;&nbsp;</label>
						  		<label>&nbsp;&nbsp;</label>
						  		<label>&nbsp;&nbsp;</label>
						  		<label>&nbsp;&nbsp;</label>
						  		<input type="hidden" class="form-control"  >
						   </div>

						</div>
					</div>
					</section>
					<section>
						<div class="modal-footer">
							<a href="#" data-dismiss="modal" class="btn btn-danger">Close</a>
							<button ng-click="addbloglocation.$valid && addBlogLocation()" ng-disabled="addbloglocation.$invalid" class="btn btn-primary">Save</button>
						</div>
					</section>
					</form>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dalog -->
		</div>
		<div class="modal" id="editmodal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" id="editbtnclose" class="close" data-dismiss="modal"
							aria-hidden="true">x</button>
						<h4 class="modal-title">Edit BlogLocation</h4>
					</div>
					 	 <form method="post" theme="simple" name="editbloglocation">
					 	 <section>
						<div class="modal-body" ng-repeat="blogloca in bloglocation">
							<input type="hidden" value="{{blogloca.blogLocationId}}" name="editBlogLocationId" id="editBlogLocationId">
						    <div class="form-group col-md-12">
								<label>Location Name<span style="color:red">*</span></label>
								<input type="text" class="form-control"  name="editBlogLocationName"  id="editBlogLocationName"  value="{{blogloca.blogLocationName}}" ng-required="true" >
							    <span ng-show="editbloglocation.editBlogCategoryName.$error.pattern">Not a Location Name!</span>
							</div>
							 <div class="form-group col-md-12">
								<label>Location Content<span style="color:red">*</span></label>
								<textarea class="form-control"  name="editBlogLocationContent"  id="editBlogLocationContent"   ng-required="true" >{{blogloca.blogLocationContent}}</textarea>
							    <span ng-show="editbloglocation.editBlogLocationContent.$error.pattern">Not a Location Name!</span>
							</div>
							<div class="form-group col-md-12">
								<label>Location URL<span style="color:red">*</span></label>
								<input type="text" class="form-control"  name="editBlogLocationUrl"  id="editBlogLocationUrl"  value="{{blogloca.blogLocationUrl}}" ng-required="true" >
							    <span ng-show="editbloglocation.editBlogLocationUrl.$error.pattern">Not a Location Name!</span>
							</div>
							 <div class="form-group col-md-12">
								<label for="child_included_rate">Location Image</label>
								<!-- <input type="file" class="form-control"   file-input="file" name="photo" id="photo"> -->
								<span style="color:red">upload only(214 * 221)</span>
<!-- 								<div id="validimage" name="validimage" style="color: red"><label>Not Image Format!!</label></div> -->
							</div>
							
							<div class="col-md-12">
					         	<img class="img-thumbnail"  ngf-select="upload($file)"  ngf-accept="'image/*'" ngf-pattern="'.jpg,.png,!.gif'" src="get-blog-location-image?blogLocationId={{blogloca.blogLocationId}}"  alt="User Avatar" width="100%">
					        </div> 
								
						</div>
						</section>
						<section>
							<div class="modal-footer">
								<a href="#" data-dismiss="modal" class="btn btn-danger">Close</a>
								<button ng-click="editbloglocation.$valid && editBlogLocation()" ng-disabled="editbloglocation.$invalid" class="btn btn-primary">Update</button>
							</div>  
						</section>
						</form>
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dalog -->
		</div>
		
		
  

      
      


<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>

	<script>


 
	
	var app = angular.module('myApp',['ngFileUpload']);
	app.controller('customersCtrl',['$scope',
	             					'Upload',
	            					'$timeout',
	            					'$http', function($scope,Upload,$timeout,$http) {

		
		
		$scope.settings = {
	            currentPage: 0,
	            offset: 0,
	            pageLimit: 10,
	            pageLimits: ['10', '50', '100']
	          };

		 $scope.getTotalPages = function () {
			return Math.ceil($scope.getBlogLocationCount[0].count / $scope.settings.pageLimit);
		};
			
		 $scope.isCurrentPageLimit = function (value) {
		       return $scope.settings.pageLimit == value;
		 };
		 
		 $scope.previousPage = function () {
			   $scope.settings.currentPage -= 1;
			   var previousOffset = $scope.settings.currentPage * $scope.settings.pageLimit;
		       var url = "get-blog-locations?limit="+$scope.settings.pageLimit+"&offset="+previousOffset;
				$http.get(url).success(function(response) {
					//console.log(response);
					$scope.properties = response.data;
				}); 
		    }; 
		 
     		$scope.nextPage = function () {
		       $scope.settings.currentPage += 1;
		       var nextOffset = $scope.settings.currentPage * $scope.settings.pageLimit;
		       var url = "get-blog-locations?limit="+$scope.settings.pageLimit+"&offset="+nextOffset;
				$http.get(url).success(function(response) {
					//console.log(response);
					$scope.properties = response.data;
				}); 
		     }; 
		       
      			$scope.all = function (total) {
		    	   $scope.settings.pageLimit = total;
			       var nextOffset = 0;
			       var url = "get-blog-locations?limit="+total+"&offset="+nextOffset;
					$http.get(url).success(function(response) {
						//console.log(response);
						$scope.properties = response.data;
					}); 
			    };
			       
      			 $scope.limited = function (limit) {
			    	   $scope.settings.pageLimit = limit;
				       var nextOffset = 0;
				       var url = "get-blog-locations?limit="+limit+"&offset="+nextOffset;
						$http.get(url).success(function(response) {
							//console.log(response);
							$scope.properties = response.data;
					}); 
		       };			

		    
		
	       	$timeout(function(){
	      	    // $scope.progressbar.complete();
	            $scope.show = true;
	            $("#pre-loader").css("display","none");
	        }, 2000);

			 
			
			$scope.unread = function() {
			//var notifiurl = "http://localhost:8085/collaborative-workflow/unreadnotifications.action";
			var notifiurl = "unreadnotifications.action";
			$http.get(notifiurl).success(function(response) {
				$scope.latestnoti = response.data;
			});
			};
			
			
			$scope.getBlogLocations = function() {
				var url = "get-blog-locations?limit="+$scope.settings.pageLimit+"&offset="+$scope.settings.offset;
				$http.get(url).success(function(response) {
				    console.log(response);
					$scope.bloglocations = response.data;
		
				});
				
			};
			
			$scope.getBlogLocation = function(rowid) {
				
				
				var url = "get-blog-location?blogLocationId="+rowid;
				$http.get(url).success(function(response) {
				    console.log(response);
					$scope.bloglocation = response.data;
		
				});
				
			};
			
			$scope.getEscapeHtml =  function(unsafe){
				
				 return unsafe
		         .replace(/&/g, "and")
		         // .replace(/and/g, "&")
		         //.replace(/</g, "&lt;")
		         //.replace(/>/g, "&gt;")
		         //.replace(/'/g, "&#039;")
		         //.replace(/"/g, "'")
		         //.replace(/percent/g, "%");
				  .replace(/%/g, "percent");
		        
				
			};
			var spinner = $('#loadertwo');
			$scope.addBlogLocation = function(){
				
				 var file = $scope.files;
				 console.log(file); 
				// var file = this.files[0];
				 var fileType = file["type"];
				 var ValidImageTypes = ["image/gif", "image/jpeg", "image/png"];
				 if ($.inArray(fileType, ValidImageTypes) < 0) {
					     // invalid file type code goes here.
					     alert('image invalid')
					}
					else{
						var content = $scope.getEscapeHtml($('#locationContent').val());
// 						alert('valid image')
						var fdata = "blogLocationName=" + $('#locationName').val()
							+ "&blogLocationContent=" + content
							+ "&blogLocationUrl=" + $('#blogLocationUrl').val();
							spinner.show();
						$http(
								{
									method : 'POST',
									data : fdata,
									headers : {
										'Content-Type' : 'application/x-www-form-urlencoded'
									},
									url : 'add-blog-location'
								}).then(function successCallback(response) {
									$scope.addedlocation = response.data;
									   setTimeout(function(){ spinner.hide(); 
										  }, 1000);
							        
									var blogLocationId = $scope.addedlocation.data[0].blogLocationId;
									 Upload.upload({
							                url: 'add-blog-location-image',
							                enableProgress: true, 
							                data: {myFile: file, 'blogLocationId': blogLocationId} 
							            }).then(function (resp) {
							            	   setTimeout(function(){ spinner.hide(); 
							     			  }, 1000);
							            	//alert("addes successfully")
							            	window.location.reload(); 
							            });	
						        
						}, function errorCallback(response) {
							
						});
				}
			};
			
			$scope.editBlogLocation = function(){
				
				/* var file = $scope.file;
				alert(file);
				 console.log(file); 
				 var fileType = file["type"];
				 var ValidImageTypes = ["image/gif", "image/jpeg", "image/png"];
				 if ($.inArray(fileType, ValidImageTypes) < 0) {
					     // invalid file type code goes here.
					     alert('image invalid')
					} */
					//else{
						var editContent = $scope.getEscapeHtml($('#editBlogLocationContent').val());
						var fdata = "blogLocationId=" + $('#editBlogLocationId').val()
						+ "&blogLocationName=" + $('#editBlogLocationName').val()
						+ "&blogLocationContent=" + editContent
						+ "&blogLocationUrl=" + $('#editBlogLocationUrl').val();
						
						spinner.show();
						$http(
								{
									method : 'POST',
									data : fdata,
									headers : {
										'Content-Type' : 'application/x-www-form-urlencoded'
									},
									url : 'edit-blog-location'
								}).then(function successCallback(response) {
									 setTimeout(function(){ spinner.hide(); 
					     			  }, 1000);
									window.location.reload(); 
									/* Upload.upload({
						                url: 'edit-blog-catergory-image',
						                enableProgress: true, 
						                data: {myFile: file, 'blogCategoryId': $('#editBlogCategoryId').val()} 
						            }).then(function (resp) {
						            	window.location.reload(); 
						            	
						            }); */
									
									
						}, function errorCallback(response) {
							 setTimeout(function(){ spinner.hide(); 
			     			  }, 1000);
						});
					//}
			};
			
			$scope.upload = function (file) {
	 			
		        	//var accommodationId = document.getElementById("editPropertyAccommodationId").value;
		        	
		            Upload.upload({
		                url: 'edit-blog-location-image',
		                enableProgress: true, 
		                data: {myFile: file, 'blogLocationId': $('#editBlogLocationId').val()} 
		            }).then(function (resp) {
		            	//window.location.reload();
		            	//$scope.getUserPhotos();
		               // console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
		                //$scope.getPropertyPhotos();
		            });
		           
		            
		     };
		     
		     $scope.deleteBlogLocation = function(rowid){
					var fdata = "&blogLocationId="+ rowid
					var deleteLocationCheck=confirm("Do you want to delete the property?");
					if(deleteLocationCheck){
						$http(
								{
									method : 'POST',
									data : fdata,
									headers : {
										'Content-Type' : 'application/x-www-form-urlencoded'
									},
									url: 'delete-blog-location'
								}).then(function successCallback(response) {
									window.location.reload();
								   }, function errorCallback(response) {
									
									
								});
					}
					else{
						
					}
					
				};			
            
	    		
			
			
			$scope.getBlogLocationCount = function() {

				var url = "get-blog-location-count";
				$http.get(url).success(function(response) {
				    //console.log(response);
					$scope.blogLocationCount = response.data;
		            //console.log($scope.accommodationRoomsCount.data[0].size);
				});
			};
			
			$scope.getBlogLocationCount();
			$scope.getBlogLocations();
			
	}]);


	app.directive('fileInput', ['$parse', function ($parse) {
	    return {
	        restrict: 'A',
	        link: function (scope, element, attributes) {
	            element.bind('change', function () {
	                $parse(attributes.fileInput)
	                .assign(scope,element[0].files[0])
	                scope.$apply()
	            });
	        }
	    };
	}]);		
		   
		        

		
	</script>
			
	<link rel="stylesheet" href="dist/css/dataTables.bootstrap.css">
	<script src="js1/upload/ng-file-upload-shim.min.js"></script>
	<script src="js1/upload/ng-file-upload.min.js"></script>
	 <script src="js1/jquery.dataTables.min.js" type="text/javascript"></script>
	 <script src="js1/dataTables.bootstrap.min.js" type="text/javascript"></script>
						
