<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!--  blog home carousal begins  -->
<section class="blogwall">
   <div class="container">
      <div class="row">
         <input type="hidden" name="blogCategoryId" id="blogCategoryId" value="<%= request.getAttribute("blogCategoryId") %>"/> <%-- <%= request.getAttribute("blogArticleTitle")%> session.getAttribute("blogCategoryId") --%>
         <input type="hidden" name="blogLocationId" id="blogLocationId" value="<%= request.getAttribute("blogLocationId")%>"/>
         <% if (request.getAttribute("blogCategoryId") != null){
            %>
         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
            <img src="get-ulo-blog-category-image?blogCategoryId=<%= request.getAttribute("blogCategoryId") %>" alt="ulo budget hotels">
            <div class="header-text hidden-xs">
               <div class="col-md-12 text-center">
                  <a href="" >
                     <h2>
                        <span>{{blogcatarticles[0].blogCategoryName}}</span>
                     </h2>
                  </a>
                  <p>{{blogcatarticles[0].blogCategoryContent}}</p>
               </div>
            </div>
            <!-- /header-text -->
         </div>
         <%} %> 
         <% if (request.getAttribute("blogLocationId") != null){
            %>
         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
            <img src="get-ulo-blog-location-image?blogLocationId=<%= request.getAttribute("blogLocationId")%>" alt="ulo budget hotels">
            <div class="header-text hidden-xs">
               <div class="col-md-12 text-center">
                  <a href="" >
                     <h2>
                        <span>{{blogcatarticles[0].blogLocationName}}</span>
                     </h2>
                  </a>
                  <p>{{blogcatarticles[0].blogLocationContent}}</p>
               </div>
            </div>
            <!-- /header-text -->
         </div>
         <%} %> 
      </div>
   </div>
</section>
<!--  blog home carousal Ends  -->
<!--  blog home content list Begins  -->
<section class="bloglistview">
   <div class="container">
      <div class="row">
         <!--  blog2  -->
         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bloglistview "><h3 class="text-center">View List </h3></div>
         <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" ng-repeat="bca in blogcatarticles | orderBy : '-createdDate'"">
            <input type="hidden" name="blogArticleId" id="blogArticleId" value="{{bca.blogArticleId}}"/>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
               <a href="{{bca.blogArticleUrl}}"> <img src="get-ulo-blog-article-title-image?blogArticleId={{bca.blogArticleId}}" class="img-responsive"></a>
               <a href="{{bca.blogArticleUrl}}">
                  <h3>{{bca.blogArticleTitle}} <br><span class="publishdate">{{bca.createdDate}}</span></h3>
               </a>
               <br>
               <p>{{bca.blogArticleDescription}}</p>
            </div>
         </div>
         <!--  blog2  -->
      </div>
   </div>
</section>
<!--  blog home  list ends  -->
<!--  blog home destinations list brgins  -->
  <section class="blogdestination">
         <div class="container">
            <div class="row">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blogbox fiveshow">
                  <h3 class="text-center">Destinations</h3>
                  <h5 class="text-center hidden"><a>View All</a></h5>
                  <!-- dest list -->
                  <div class="bloglistfive ">
        
                     <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6" ng-repeat="abl in allbloglocations">
                        <a href="{{abl.blogNamespace}}/{{abl.blogLocationUrl}}"><img src="get-ulo-blog-location-image?blogLocationId={{abl.blogLocationId}}" class="img-responsive destlist"></a>
                        <a href="{{abl.blogNamespace}}/{{abl.blogLocationUrl}}"><h3 class="centered">{{abl.blogLocationName}}</h3></a>
                     </div>
                   
                  </div>
                  <!-- dest list -->
               </div>
            </div>
         </div>
      </section>
<!--  blog home destinations list Ends  -->
<script src="uloblog/js/angularjs/angular.min.js" type="text/javascript"></script>
<script src="uloblog/js/ngprogress.js" type="text/javascript"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script> 
<script>
   var app = angular.module('myApp', ['ngProgress']);
   app.controller('customersCtrl', function ($scope, $http, $timeout, ngProgressFactory) {
    
    $scope.settings = {
             currentPage: 0,
             offset: 0,
             pageLimit: 5,
             pageLimits: ['10', '50', '100']
           };
   
    
    
     $scope.getBlogCategoryArticles = function() {
	   	var id = $('#blogCategoryId').val();
   		var url = "get-blog-category-articles?blogCategoryId="+id;
	   $http.get(url).success(function(response) {
	      console.log(response);
	   $scope.blogcatarticles = response.data;
	   
	   });
     };
   
   $scope.getBlogLocationArticles = function() {
      		var id = $('#blogLocationId').val();
   			var url = "get-blog-location-articles?blogLocationId="+id;
   			$http.get(url).success(function(response) {
   			    console.log(response);
   				$scope.blogcatarticles = response.data;
   	
   			});
   
   };	
   
   $scope.getAllBlogLocations = function() {
   var url = "get-all-ulo-blog-locations";
   $http.get(url).success(function(response) {
       console.log(response);
   	$scope.allbloglocations = response.data;
   
   });
   
   };
   
   $scope.getAllBlogArticles = function() {
   var url = "get-all-ulo-blog-articles?limit="+$scope.settings.pageLimit+"&offset="+$scope.settings.offset;
   $http.get(url).success(function(response) {
     console.log(response);
   $scope.allblogarticles = response.data;
   
   });
   
   };
   
   $scope.getAllBlogCategories = function() {
   var url = "get-all-ulo-blog-categories";
   $http.get(url).success(function(response) {
      console.log(response);
   $scope.allblogcategories = response.data;
   
   });
   
   };	     		
   
   
   $scope.getBlogCategoryArticles();
   $scope.getBlogLocationArticles();
   $scope.getAllBlogLocations();
   $scope.getAllBlogCategories();
   $scope.getAllBlogArticles();
   
   });
   
</script>