<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="s" uri="/struts-tags"%>

   <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Blog User Comment
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
   
            <li class="active">Blog User Comment</li>
          </ol>
        </section>
		
        <!-- Main content -->
        <section class="content">
        
        	<div class="row">
            	<div class="col-xs-12" > <!-- ng-if="properties.length>=0" -->
						<div class="box">
							<div class="box-header">
								<h3 class="box-title">Blog User Comment</h3>
							</div>
							
							
							<!-- /.box-header -->
							<div class="box-body table-responsive no-padding" > 
								<table class="table table-hover" id="example1">
									<tr>
										<!-- <th>Comment Id</th> -->
										<th>User Name</th>
										<th>User Email</th>
										<th>User Url</th>
										<th>Comment</th>
										<th>Status</th>
									
									</tr>
									<tr ng-repeat="bac in blogallcomment">
										<!-- <td >{{bac.blogUserCommentId}}</td> -->
										<td>{{bac.blogUserName}}</td>
										<td>{{bac.blogUserEmail}}</td>
										<td>{{bac.blogUserUrl}}
										<td>{{bac.blogUserComment}}</td>
										
										<td>
										<button ng-click="editBlogUserStatus(bac.blogUserCommentId)"  ng-show="bac.status == 'true'"class="btn btn-danger">Approved</button>
									<button ng-click="editBlogUserStatus(bac.blogUserCommentId)"  ng-show="bac.status == 'false'"class="btn btn-primary">Approve</button>		
											<!-- <button ng-click="editBlogUserStatus(bac.blogUserCommentId)"  data-toggle="modal"></button> -->
										</td>
										<!-- <td>
											<a href="#deletemodal" ng-click="deleteBlogCategory(b.blogCategoryId)" data-toggle="modal" ><i  class="fa fa-fw fa-trash text-red" ></i></a>
										</td> -->
									</tr>
									<tr>
									</tr>
								</table>
							</div>
							<!-- /.box-body -->							
							
							
							<div class="box-footer clearfix">
								<ul class="pagination pagination-sm"></ul>
								<div class="simple-pagination pull-right" >   
									<p class="simple-pagination__items">Showing {{blogCommentCount[0].count}} out of {{settings.pageLimit}}</p>
									<p><button ng-click="previousPage()" ng-disabled="settings.currentPage <= 0" class="simple-pagination__button simple-pagination__button--prev"> &#10094;          
									</button><span class="simple-pagination__pages">{{settings.currentPage + 1}} of {{getTotalPages()}}</span>
									<button ng-click="nextPage()" ng-disabled="settings.currentPage === (getTotalPages() - 1)" class="simple-pagination__button simple-pagination__button--next">&#10095;</button></p>
									<a href="" ng-click="limited(10)" ng-class="active">{{10}}</a> |
									<span><a href="" ng-click="all(blogCommentCount[0].count)" ng-class="active">All</a></span> 
								</div>
						   </div> 
						</div>
						<!-- /.box -->
					</div>	
         	</div>
          
        </section><!-- /.content -->     	
		
			<!-- /.modal-dalog -->
		</div>
		
		
  

      
      


<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>

	<script>


 
	
	var app = angular.module('myApp',['ngFileUpload']);
	app.controller('customersCtrl',['$scope',
	             					'Upload',
	            					'$timeout',
	            					'$http', function($scope,Upload,$timeout,$http) {

		
		
		$scope.settings = {
	            currentPage: 0,
	            offset: 0,
	            pageLimit: 10,
	            pageLimits: ['10', '50', '100']
	          };

		 $scope.getTotalPages = function () {
			return Math.ceil($scope.getBlogUserCommentCount[0].count / $scope.settings.pageLimit);
		};
			
		 $scope.isCurrentPageLimit = function (value) {
		       return $scope.settings.pageLimit == value;
		 };
		 
		 $scope.previousPage = function () {
			   $scope.settings.currentPage -= 1;
			   var previousOffset = $scope.settings.currentPage * $scope.settings.pageLimit;
		       var url = "get-blog-all-user-comments?limit="+$scope.settings.pageLimit+"&offset="+previousOffset;
				$http.get(url).success(function(response) {
					//console.log(response);
					$scope.blogallcomment = response.data;
				}); 
		    }; 
		 
     		$scope.nextPage = function () {
		       $scope.settings.currentPage += 1;
		       var nextOffset = $scope.settings.currentPage * $scope.settings.pageLimit;
		       var url = "get-blog-all-user-comments?limit="+$scope.settings.pageLimit+"&offset="+nextOffset;
				$http.get(url).success(function(response) {
					//console.log(response);
					$scope.blogallcomment = response.data;
				}); 
		     }; 
		       
      			$scope.all = function (total) {
		    	   $scope.settings.pageLimit = total;
			       var nextOffset = 0;
			       var url = "get-blog-all-user-comments?limit="+total+"&offset="+nextOffset;
					$http.get(url).success(function(response) {
						//console.log(response);
						$scope.blogallcomment = response.data;
					}); 
			    };
			       
      			 $scope.limited = function (limit) {
			    	   $scope.settings.pageLimit = limit;
				       var nextOffset = 0;
				       var url = "get-blog-all-user-comments?limit="+limit+"&offset="+nextOffset;
						$http.get(url).success(function(response) {
							//console.log(response);
							$scope.blogallcomment = response.data;
					}); 
		       };			

		    
		
	       	$timeout(function(){
	      	    // $scope.progressbar.complete();
	            $scope.show = true;
	            $("#pre-loader").css("display","none");
	        }, 2000);

			 
			
			$scope.unread = function() {
			//var notifiurl = "http://localhost:8085/collaborative-workflow/unreadnotifications.action";
			var notifiurl = "unreadnotifications.action";
			$http.get(notifiurl).success(function(response) {
				$scope.latestnoti = response.data;
			});
			};		
			
			
			
			$scope.getBlogUserComment= function(rowid) {
				
				
				var url = "get-blog-user-comment";
				$http.get(url).success(function(response) {
				    console.log(response);
					$scope.blogcomment = response.data;
				
		
				});
				
			};
			
			$scope.getBlogUserCommentCount = function() {

				var url = "get-blog-user-comment-count";
				$http.get(url).success(function(response) {
				    //console.log(response);
					$scope.blogCommentCount = response.data;
		            //console.log($scope.accommodationRoomsCount.data[0].size);
				});
			};
			
			$scope.getBlogAllUserComments= function() {
				
				
				var url = "get-blog-all-user-comments?limit="+$scope.settings.pageLimit+"&offset="+$scope.settings.offset;
				$http.get(url).success(function(response) {
				    console.log(response);
					$scope.blogallcomment = response.data;
		
				});
				
			};	
			
			
			$scope.editBlogUserStatus= function(id) {
				
				alert(id)
				var url = "edit-blog-user-status?blogUserCommentId="+id;
				$http.get(url).success(function(response) {
				    console.log(response);
					$scope.blogallcomment = response.data;
					window.location.reload(); 
		
				});
				
			};	
			
			$scope.getBlogUserCommentCount();
			$scope.getBlogAllUserComments();
			
	}]);


	app.directive('fileInput', ['$parse', function ($parse) {
	    return {
	        restrict: 'A',
	        link: function (scope, element, attributes) {
	            element.bind('change', function () {
	                $parse(attributes.fileInput)
	                .assign(scope,element[0].files[0])
	                scope.$apply()
	            });
	        }
	    };
	}]);		
		   
		        

		
	</script>
			
	<link rel="stylesheet" href="dist/css/dataTables.bootstrap.css">
	<script src="js1/upload/ng-file-upload-shim.min.js"></script>
	<script src="js1/upload/ng-file-upload.min.js"></script>
	 <script src="js1/jquery.dataTables.min.js" type="text/javascript"></script>
	 <script src="js1/dataTables.bootstrap.min.js" type="text/javascript"></script>
						
