<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="s" uri="/struts-tags"%>

   <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
           Area Content   
          </h1>
          <ol class="breadcrumb">
            <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Area Content</li>
          </ol>
        </section>
        <!-- Main content -->
	<section class="content">
		<div class="row">
		<div class="col-md-12 col-sm-12">
		<form>
			<div class="form-group col-md-6 col-sm-6"  >
				<h4>Select Area :</h4>
				 <select name="googleAreaId" id="googleAreaId"  ng-change="getSeoAreaContent()" ng-model="googleAreaId" class="form-control">
				 <!-- <option value="select">select</option> -->
				 <option ng-repeat="agp in allGoogleAreas" value="{{agp.googleAreaId}}">{{agp.googleAreaName}}</option>
				 </select>
				 
			 </div>
			<div>  <!-- ng-repeat="slc in seoAreaContent" -->
			
				<div class="form-group col-md-12 col-sm-12">
					<h4>MetaTag: </h4>
					<textarea id="title" style="font-size: 14px;font-weight: normal;resize: none;overflow-y: scroll;"
					 name="title" class="form-control" ng-model="title" value="{{seoAreaContent.data[0].title}}"  >
					</textarea>
				</div>
				<div class="form-group col-md-12 col-sm-12">
					<h4>Description: </h4>                                  
					<textarea id="description" style="font-size: 14px;font-weight: normal;resize: none;overflow-y: scroll;"
					 name="description" ng-model="description"  class=" form-control" value="{{seoAreaContent.data[0].description}}">
					</textarea>
				</div>
				<div class="form-group col-md-12 col-sm-12">
					<h4>Script : </h4>                                                                     
					<textarea id="content" style="font-size: 14px;font-weight: normal;resize: none;overflow-y: scroll;" 
					name="content" ng-model="content" class=" form-control" value="{{seoAreaContent.data[0].content}}"   ng-required="true">		                    		
					</textarea>
				</div>
			
			 	<div class="form-group col-md-12 col-sm-12">
				<button type="submit" ng-click="editSeoAreaContent()" class="btn btn-primary btngreeen pull-right" >Save</button>
			</div>
			</div> <!-- NG-repeat ends -->
		</form>
		</div>
		</div>
	</section>                           
 </div>                       
          



<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>

 <script>


 
	
		var app = angular.module('myApp', ['ngProgress']);
		app.controller('customersCtrl', function($scope, $http,$timeout,ngProgressFactory) {

			 $(function(){
				  $('#edit').froalaEditor()
				}); 
			 var spinner = $('#loadertwo');
			
	       	$timeout(function(){
	      	    // $scope.progressbar.complete();
	            $scope.show = true;
	            $("#pre-loader").css("display","none");
	        }, 2000);

			 
			
			$scope.unread = function() {
			//var notifiurl = "http://localhost:8085/collaborative-workflow/unreadnotifications.action";
			var notifiurl = "unreadnotifications.action";
			$http.get(notifiurl).success(function(response) {
				$scope.latestnoti = response.data;
			});
			};

           
			
			
			  $scope.getGoogleAllAreas = function() {
					
					var url = "get-google-all-areas";
					$http.get(url).success(function(response) {
					    //console.log(response);
						$scope.allGoogleAreas = response.data;
			
					});
			   };
				
			$scope.getSeoAreaContent = function(){
				
				var fdata = "googleAreaId=" + $('#googleAreaId').val();
				spinner.show();
				$http(
						{
							method : 'POST',
							data : fdata,
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							url : 'get-seo-area-content'
						}).then(function successCallback(response) {
							$scope.seoAreaContent = response.data;
							$scope.title = $scope.seoAreaContent.data[0].title;
							$scope.description = $scope.seoAreaContent.data[0].description;
							$scope.keywords = $scope.seoAreaContent.data[0].keywords;
							$scope.content = $scope.seoAreaContent.data[0].content;
							$scope.shortDescription = $scope.seoAreaContent.data[0].shortDescription;
							$scope.socialMetaTag = $scope.seoAreaContent.data[0].socialMetaTag;
							$scope.h1 = $scope.seoAreaContent.data[0].h1;
							$scope.h2 = $scope.seoAreaContent.data[0].h2;
							$scope.h3 = $scope.seoAreaContent.data[0].h3;
							$scope.h4 = $scope.seoAreaContent.data[0].h4;   
							   setTimeout(function(){ spinner.hide(); 
							   }, 1000);

				}, function errorCallback(response) {
					   setTimeout(function(){ spinner.hide(); 
					   }, 1000);

					// called asynchronously if an error occurs
					// or server returns response with an error status.
				});

			};
			
			$scope.editSeoAreaContent = function(){
				
				
				var title = $scope.getEscapeHtml($('#title').val());
			    var description = $scope.getEscapeHtml($('#description').val());
			    var content = $('#content').val();
			    var temp = content.replace("+", "plus");
			    var fdata = "title=" + title
				+ "&description=" + description
				+ "&content=" + temp
				+ "&keywords=" + $('#keywords').val()
				+ "&shortDescription=" + $('#shortDescription').val()
				+ "&socialMetaTag=" + $('#socialMetaTag').val()
				+ "&h1=" + $('#h1').val()
				+ "&h2=" + $('#h2').val()
				+ "&h3=" + $('#h3').val()
				+ "&h4=" + $('#h4').val()
				+ "&googleAreaId=" + $('#googleAreaId').val();
				
				
				
				spinner.show();
				//alert(fdata);
				$http(
						{
							method : 'POST',
							data : fdata,
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							url : 'edit-seo-area-content'
						}).then(function successCallback(response) {
							   setTimeout(function(){ spinner.hide(); 
							   }, 1000);

							//alert("Area Content Added Successfully");
							/* var alert = ' <div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Alert!</h4>Success User Updated Succesfully.</div>';
				            $('#editmessage').html(alert);
				            
							$timeout(function(){
							$('#editbtnclose').click();
					        }, 2000); */
							
				}, function errorCallback(response) {
					   setTimeout(function(){ spinner.hide(); 
					   }, 1000);

					// called asynchronously if an error occurs
					// or server returns response with an error status.
				});

			};
			
			
			$scope.getEscapeHtml =  function(unsafe){
				
				 return unsafe
		         .replace(/&/g, "and")
		         // .replace(/and/g, "&")
		         //.replace(/</g, "&lt;")
		         //.replace(/>/g, "&gt;")
		         //.replace(/'/g, "&#039;")
		         //.replace(/"/g, "'")
		         //.replace(/percent/g, "%");
				  .replace(/%/g, "percent");
		        
				
			};
			
			
		    $scope.getGoogleAllAreas();
		  
			
		});

		
	</script>
 
	
	 <script src="js/jquery-2.1.1.js" type="text/javascript"></script>
       