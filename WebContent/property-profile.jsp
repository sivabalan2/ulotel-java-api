<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
   pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://ckeditor.com" prefix="ckeditor" %>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Property Profile
         <!--<small>Subscription Expiry on {{property[0].subscription[0].expirydate}}</small>-->
      </h1>
      <ol class="breadcrumb">
         <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
         <li><a href="#">Property</a></li>
         <li class="active">Profile</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <div class="col-xs-12">
            <div class="box-body" id= "page-header" >
               <div class="alert alert-success alert-dismissable" id="alert-message">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <h4><i class="icon fa fa-check"></i> Alert!</h4>
                  Successfully updated
               </div>
            </div>
         </div>
         <div class="col-xs-12">
            <div class="nav-tabs-custom">
               <ul class="nav nav-tabs">
                  <li class="active"><a href="#fa-icons" data-toggle="tab">Property Info</a></li>
                  <!-- <li ><a href="#tab-subscription" data-toggle="tab">Property Images</a></li> -->
                  <li ng-repeat="pc in photocategory"><a href="#fa-icons3" ng-click="getCategoryPhotos(pc.photoCategoryId)" data-toggle="tab">{{pc.photoCategoryName}}</a></li>
                  <li ><a href="#tab-policy" data-toggle="tab">Property Policy</a></li>
                  <li ><a href="#tab-landmark" data-toggle="tab">Property Landmark</a></li>
               </ul>
               <div class="tab-content">
                  <!-- Font Awesome Icons -->
                  <div class="tab-pane active" id="fa-icons">
                     <form method="post" theme="simple" name="propertyprofile">
                        <s:hidden name="propertyId" id="propertyId" value="%{propertyId}"></s:hidden>
                        <input type="hidden" value ="<%=session.getAttribute("propertyId") %>" id="propertysId" name="propertysId" >
                        <section id="new">
                           <!-- <h4 class="page-header">General Settings</h4> -->
                           <div class="row fontawesome-icon-list">
                              <div class="col-md-3 col-sm-4">
                                 <div class="form-group">
                                    <label>Property Type  <span style="color:red">*</span></label>
                                    <select name="propertyTypeId" id="propertyTypeId"  ng-model="propertyTypeselect" class="form-control" >
                                       <option value="">select</option>
                                       <option ng-repeat="pt in propertytypes" value="{{pt.DT_RowId}}" ng-selected ="property[0].propertyTypeId == pt.DT_RowId">{{pt.propertyTypeName}}</option>
                                    </select>
                                 </div>
                              </div>
                           </div>
                           <div class="row fontawesome-icon-list">
                              <div class="col-md-12 col-sm-12">
                                 <div class="form-group">
                                    <label for="firstname">Description <span style="color:red">*</span> </label>
                                    <textarea id="propertyDescription" name="propertyDescription" ng-model="property[0].propertyDescription" class="form-control"  value="{{property[0].propertyDescription}}"  placeholder="Description"   ng-required="true">
                                    {{property[0].propertyDescription}}
                                    </textarea>
                                 </div>
                              </div>
                           </div>
                        </section>
                        <section id="web-application">
                           <h4 class="page-header">Property Contact Info</h4>
                           <div class="row fontawesome-icon-list">
                              <div class="col-md-3 col-sm-3">
                                 <div class="form-group">
                                    <label for="address">Property Name <span style="color:red">*</span> </label>
                                    <input type="text" class="form-control" name="propertyName"  id="propertyName" value="{{property[0].propertyName}}" placeholder="Name" ng-model='property[0].propertyName' ng-pattern="/^[a-zA-Z0-9]/"  ng-required="true">
                                    <span ng-show="propertyprofile.propertyName.$error.pattern" style="color:red">Not a Valid Property Name!</span>
                                 </div>
                              </div>
                              <%-- <div class="col-md-3 col-sm-4">
                                 <div class="form-group">
                                 <label>Property Phone</label>
                                 <input type="text" class="form-control" name="propertyPhone"  id="propertyPhone" value="{{property[0].propertyPhone}}" placeholder="Phone" ng-model='property[0].propertyPhone' ng-pattern="/^[0-9]/" ng-required="true">
                                 <span ng-show="propertyprofile.propertyPhone.$error.pattern" style="color:red">Not a Valid Property Phone!</span>
                                 </div>
                                 </div> --%>
                               <div class="col-md-3 col-sm-3">
                                 <div class="form-group">
                                    <label>Property Contact <span style="color:red">*</span> </label>	                      			
                                    <input type="text" class="form-control" name="propertyContact"  id=propertyContact value="{{property[0].propertyContact}}" placeholder="Contact" ng-model='property[0].propertyContact' ng-pattern="/^[0-9]/" ng-required="true">
                                    <span ng-show="propertyprofile.propertyContact.$error.pattern" style="color:red">Not a Valid Property Contact!</span>
                                 </div>
                              </div>  
                              
                              <div class="col-md-3 col-sm-3">
                                 <div class="form-group">
                                    <label>Property Email <span style="color:red">*</span> </label>	                      			
                                    <input type="text" class="form-control" name="propertyEmail"  id="propertyEmail" value="{{property[0].propertyEmail}}" placeholder="Email" ng-model='property[0].propertyEmail' ng-pattern="/^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/"  ng-required="true">
                                    <span ng-show="propertyprofile.propertyEmail.$error.pattern" style="color:red">Not a Valid Property Email!</span>
                                 </div>
                              </div>
                           </div>
                        </section>
                        <section id="web-application">
                           <h4 class="page-header">Reservation Manager Contact Info</h4>
                           <div class="row fontawesome-icon-list">
                              <div class="col-md-3 col-sm-3">
                                 <div class="form-group">
                                    <label for="address">Reservation Manager Name <span style="color:red">*</span> </label>
                                    <input type="text" class="form-control" name="reservationManagerName" id="reservationManagerName" value="{{property[0].reservationManagerName}}" placeholder="Reservation Manager Name" ng-model='property[0].reservationManagerName' ng-pattern="/^[a-zA-Z]/" ng-required="true">
                                    <span ng-show="propertyprofile.reservationManagerName.$error.pattern" style="color:red">Not a Valid Reservation Manager Name!</span>
                                 </div>
                              </div>
                              <div class="col-md-3 col-sm-3">
                                 <div class="form-group">
                                    <label>Reservation Manager Contact <span style="color:red">*</span> </label>
                                    <input type="text" class="form-control" name="reservationManagerContact" id="reservationManagerContact" value="{{property[0].reservationManagerContact}}" placeholder="Reservation Manager Contact" ng-model='property[0].reservationManagerContact' ng-pattern="/^[0-9]/" ng-required="true">
                                    <span ng-show="propertyprofile.reservationManagerContact.$error.pattern" style="color:red">Not a Valid Reservation Manager Contact!</span>
                                 </div>
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 <div class="form-group">
                                    <%-- <label>Reservation Manager Email <span style="color:red">*</span></label>
                                    <input type="text" class="form-control" name="reservationManagerEmail" id="reservationManagerEmail" value="{{property[0].reservationManagerEmail}}" placeholder="Reservation Manager Email" ng-model='property[0].reservationManagerEmail' ng-pattern="/^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/" ng-required="true">
                                    <span ng-show="propertyprofile.reservationManagerEmail.$error.pattern" style="color:red">Not a Valid Reservation Manager Email!</span> --%>
                                    <label>Reservation Manager Email <span style="color:red"> * (Use comma(,) for mail id's separation)</span> </label>
                                    <textarea class="form-control" name="reservationManagerEmail" id="reservationManagerEmail" placeholder="Reservation Manager Email"  ng-required="true">{{property[0].reservationManagerEmail}}</textarea>
                                 </div>
                              </div>
                           </div>
                        </section>
                        <section id="web-application">
                           <h4 class="page-header">Hotel Manager Contact Info</h4>
                           <div class="row fontawesome-icon-list">
                              <div class="col-md-3 col-sm-3">
                                 <div class="form-group">
                                    <label for="address">Hotel Manager Name <span style="color:red">*</span> </label>
                                    <input type="text" class="form-control" name="resortManagerName" id="resortManagerName" value="{{property[0].resortManagerName}}" placeholder="Resort Manager Name" ng-model='property[0].resortManagerName' ng-pattern="/^[a-zA-Z]/" ng-required="true">
                                    <span ng-show="propertyprofile.resortManagerName.$error.pattern" style="color:red">Not a Valid Resort Manager Name!</span>
                                 </div>
                              </div>
                              <div class="col-md-3 col-sm-3">
                                 <div class="form-group">
                                    <label>Hotel Manager Contact <span style="color:red">*</span> </label>
                                    <input type="text" class="form-control" name="resortManagerContact" id="resortManagerContact" value="{{property[0].resortManagerContact}}" placeholder="Resort Manager Contact" ng-model='property[0].resortManagerContact' ng-pattern="/^[0-9]/" ng-required="true">
                                    <span ng-show="propertyprofile.resortManagerContact.$error.pattern" style="color:red">Not a Valid Resort Manager Contact!</span>
                                 </div>
                              </div>
                              <div class="col-md-3 col-sm-3">
                                 <div class="form-group">
                                    <label>Hotel Manager Email <span style="color:red">*</span> </label>
                                    <input type="text" class="form-control" name="resortManagerEmail" id="resortManagerEmail" value="{{property[0].resortManagerEmail}}" placeholder="Resort Manager Email" ng-model='property[0].resortManagerEmail' ng-pattern="/^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/" ng-required="true">
                                    <span ng-show="propertyprofile.resortManagerEmail.$error.pattern" style="color:red">Not a Valid Resort Manager Email!</span>
                                 </div>
                              </div>
                              
                           </div>
                        </section>
                        <section id="web-application">
                           <h4 class="page-header">Contract Manager Contact Info</h4>
                           <div class="row fontawesome-icon-list">
                              <div class="col-md-3 col-sm-3">
                                 <div class="form-group">
                                    <label for="address">Contract Manager Name <span style="color:red">*</span> </label>
                                    <input type="text" class="form-control" name="contractManagerName" id="contractManagerName" value="{{property[0].contractManagerName}}" placeholder="Contract Manager Name" ng-model='property[0].contractManagerName' ng-pattern="/^[a-zA-Z]/" ng-required="true">
                                    <span ng-show="propertyprofile.contractManagerName.$error.pattern" style="color:red">Not a Valid Contract Manager Name!</span>
                                 </div>
                              </div>
                              <div class="col-md-3 col-sm-3">
                                 <div class="form-group">
                                    <label>Contract Manager Contact <span style="color:red">*</span> </label>
                                    <input type="text" class="form-control" name="contractManagerContact" id="contractManagerContact" value="{{property[0].contractManagerContact}}" placeholder="Contract Manager Contact" ng-model='property[0].contractManagerContact' ng-pattern="/^[0-9]/" ng-required="true">
                                    <span ng-show="propertyprofile.contractManagerContact.$error.pattern" style="color:red">Not a Valid Contract Manager Contact!</span>
                                 </div>
                              </div>
                              <div class="col-md-3 col-sm-3">
                                 <div class="form-group">
                                    <label>Contract Manager Email <span style="color:red">*</span> </label>
                                    <input type="text" class="form-control" name="contractManagerEmail" id="contractManagerEmail" value="{{property[0].contractManagerEmail}}" placeholder="Contract Manager Email" ng-model='property[0].contractManagerEmail' ng-pattern="/^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/" ng-required="true">
                                    <span ng-show="propertyprofile.contractManagerEmail.$error.pattern" style="color:red">Not a Valid Contract Manager Email!</span>
                                 </div>
                              </div>
                              
                           </div>
                        </section>
                        <section id="web-application">
                           <h4 class="page-header">Revenue Manager Contact Info</h4>
                           <div class="row fontawesome-icon-list">
                              <div class="col-md-3 col-sm-3">
                                 <div class="form-group">
                                    <label for="address">Revenue Manager Name <span style="color:red">*</span> </label>
                                    <input type="text" class="form-control" name="revenueManagerName" id="revenueManagerName" value="{{property[0].revenueManagerName}}" placeholder="Revenue Manager Name" ng-model='property[0].revenueManagerName' ng-pattern="/^[a-zA-Z]/" ng-required="true">
                                    <span ng-show="propertyprofile.revenueManagerName.$error.pattern" style="color:red">Not a Valid Revenue Manager Name!</span>
                                 </div>
                              </div>
                              <div class="col-md-3 col-sm-3">
                                 <div class="form-group">
                                    <label>Revenue Manager Contact <span style="color:red">*</span> </label>
                                    <input type="text" class="form-control" name="revenueManagerContact" id="revenueManagerContact" value="{{property[0].revenueManagerContact}}" placeholder="Revenue Manager Contact" ng-model='property[0].revenueManagerContact' ng-pattern="/^[0-9]/" ng-required="true">
                                    <span ng-show="propertyprofile.revenueManagerContact.$error.pattern" style="color:red">Not a Valid Revenue Manager Contact!</span>
                                 </div>
                              </div>
                              <div class="col-md-6 col-sm-6">
                                 <div class="form-group">
                                    <%-- <label>Revenue Manager Email <span style="color:red">*</span> </label>
                                    <input type="text" class="form-control" name="revenueManagerEmail" id="revenueManagerEmail" value="{{property[0].revenueManagerEmail}}" placeholder="Revenue Manager Email" ng-model='property[0].revenueManagerEmail' ng-pattern="/^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/" ng-required="true">
                                    <span ng-show="propertyprofile.revenueManagerEmail.$error.pattern" style="color:red">Not a Valid Revenue Manager Email!</span> --%>
                                    <label>Revenue Manager Email <span style="color:red"> * (Use comma(,) for mail id's separation)</span> </label>
                                    <textarea class="form-control" name="revenueManagerEmail" id="revenueManagerEmail" placeholder="Revenue Manager Email"  ng-required="true">{{property[0].revenueManagerEmail}}</textarea>
                                 </div>
                              </div>
                              
                           </div>
                        </section>
                        <section id="property-address">
                           <h4 class="page-header">Property Address</h4>
                           <div class="row fontawesome-icon-list">
                              <div class="col-md-3 col-sm-4">
                                 <div class="form-group">
                                    <label for="countryCode">Country <span style="color:red">*</span></label>
                                    <select name="countryCode" id="countryCode" ng-model="property[0].countryCode" value="{{property[0].countryCode}}" class="form-control" ng-required="true">
                                       <option value="select">select</option>
                                       <option ng-repeat="c in countries" value="{{c.countryCode}}"  ng-selected ="property[0].countryCode == c.countryCode">{{c.countryName}}</option>
                                    </select>
                                 </div>
                              </div>
                              <div class="col-md-3 col-sm-4">
                                 <div class="form-group">
                                    <label>Address 1 <span style="color:red">*</span></label>	                      			
                                    <input type="text" class="form-control" name="address1"  id="address1" value="{{property[0].address1}}" placeholder="Address 1" ng-model='property[0].address1' ng-pattern="/^[a-zA-Z0-9]+(?:\,)*/" ng-required="true">
                                    <span ng-show="propertyprofile.address1.$error.pattern" style="color:red">Not a Valid Alphabet!</span>
                                 </div>
                              </div>
                              <div class="col-md-3 col-sm-4">
                                 <div class="form-group">
                                    <label>Address 2 <span style="color:red">*</span></label>                   			
                                    <input type="text" class="form-control" name="address2"  id="address2" value="{{property[0].address2}}" placeholder="Address 2" ng-model='property[0].address2' ng-pattern="/^[a-zA-Z0-9]+(?:\,)*/" ng-required="true">
                                    <span ng-show="propertyprofile.address2.$error.pattern" style="color:red">Not a Valid Alphabet!</span>
                                 </div>
                              </div>
                              <div class="col-md-3 col-sm-4">
                                 <div class="form-group">
                                    <label>City <span style="color:red">*</span></label>	                      			
                                    <input type="text" class="form-control" name="city"  id="city" value="{{property[0].city}}" placeholder="City" ng-model='property[0].city' ng-pattern="/^[a-zA-Z]/" ng-required="true">
                                    <span ng-show="propertyprofile.city.$error.pattern" style="color:red">Not a Valid Alphabet!</span>
                                 </div>
                              </div>
                              <div class="col-md-3 col-sm-4">
                                 <div class="form-group">
                                    <label>State <span style="color:red">*</span></label>
                                    <select name="stateCode" id="stateCode" ng-model="property[0].stateCode" value="{{property[0].stateCode}}" class="form-control" ng-required="true">
                                       <option value="select">select</option>
                                       <option ng-repeat="s in states" value="{{s.stateCode}}" ng-selected ="property[0].stateCode == s.stateCode">{{s.stateName}}</option>
                                    </select>
                                 </div>
                              </div>
                              <div class="col-md-3 col-sm-4">
                                 <div class="form-group">
                                    <label>Zip Code <span style="color:red">*</span> </label>	                      			
                                    <input type="text" class="form-control" name="zipCode"  id="zipCode" value="{{property[0].zipCode}}" placeholder="Zip Code" ng-model='property[0].zipCode' ng-pattern="/^[0-9]/" ng-required="true">
                                    <span ng-show="propertyprofile.zipCode.$error.pattern" style="color:red">Not a Valid Numeric!</span>
                                 </div>
                              </div>
                              <div class="col-md-3 col-sm-4">
                                 <div class="form-group">
                                    <label>Latitude <span style="color:red">*</span> </label>	                      			
                                    <input type="text" class="form-control" name="latitude"  id="latitude" value="{{property[0].latitude}}" placeholder="Latitude" ng-model='property[0].latitude' ng-pattern="/^[0-9]/" ng-required="true" >
                                    <span ng-show="propertyprofile.latitude.$error.pattern" style="color:red">Not a Valid Numeric!</span>
                                 </div>
                              </div>
                              <div class="col-md-3 col-sm-4">
                                 <div class="form-group">
                                    <label>Longitude <span style="color:red">*</span></label>	                      			
                                    <input type="text" class="form-control" name="longitude"  id="longitude" value="{{property[0].longitude}}" placeholder="Longitude" ng-model='property[0].longitude' ng-pattern="/^[0-9]/" ng-required="true">
                                    <span ng-show="propertyprofile.longitude.$error.pattern" style="color:red">Not a Valid Numeric!</span>
                                 </div>
                              </div>
                           </div>
                        </section>
                        <section id="property-map">
                           <h4 class="page-header">Property Tax</h4>
                           <div class="row">
                              <div class="col-md-12">
                 
		                     <div class="form-group col-md-3">
			                     <label>Tax Valid From <span style="color:red">*</span> </label>
			                     <div class="input-group date">
				                     <label class="input-group-addon btn" for="taxValidFrom">
				                     	<span class="fa fa-calendar"></span>
				                     </label>   
				                     <input type="text" id="taxValidFrom" type="text" class="form-control" name="taxValidFrom"  value="{{property[0].taxValidFrom}}">
			                     </div>
		                     <!-- /.input group -->
		                     </div>
		                     <div class="form-group col-md-3">
			                     <label>Tax Valid To <span style="color:red">*</span> </label>
			                     <div class="input-group date">
				                     <label class="input-group-addon btn" for="taxValidTo">
				                    	 <span class="fa fa-calendar"></span>
				                     </label>   
			                     <input type="text" id="taxValidTo" type="text" class="form-control" name="taxValidTo"  value="{{property[0].taxValidTo}}">
			                     </div>
		                     <!-- /.input group -->
		                     </div>
		                     <!--  <input type="hidden" ng-value="property[0].taxIsActive == 'true' ? true : false"> -->
		                     <div class="col-md-3">
			                     <div class="form-group">
				                     <label>Tax Status <span style="color:red">*</span> </label>
				                     <select name="taxIsActive" id="taxIsActive"   class="form-control" ng-required="true">
					                     <option value="true"  ng-selected ="property[0].taxIsActive == 'true'">Active</option>
					                     <option value="false" ng-selected ="property[0].taxIsActive == 'false'">InActive</option>
				                     <!-- <option ng-repeat="pt in propertytypes" value="{{pt.DT_RowId}}" ng-selected ="property[0].propertyTypeId == pt.DT_RowId">{{pt.propertyTypeName}}</option> -->
				                     </select>
			                     </div>
		                     </div>
		                     <div class="col-md-3">
		                         <div class="form-group">
		                            <label>Property Commission <span style="color:red">*</span> </label>
		                            <input type="text" class="form-control" name="propertyCommission" id="propertyCommission" value="{{property[0].propertyCommission}}" placeholder="Resort Manager Contact" ng-model='property[0].propertyCommission' ng-pattern="/^[0-9]/" ng-required="true">
		                            <span ng-show="propertyprofile.propertyCommission.$error.pattern" style="color:red">Not a Valid Percentage!</span>
		                         </div>
		                      </div>
		                      <div class="col-md-3">
		                         <div class="form-group">
		                            <label>Property Place Id <span style="color:red">*</span> </label>
		                            <input type="text" class="form-control" name="placeId" id="placeId" value="{{property[0].placeId}}" placeholder="Place Id" ng-model='property[0].placeId' ng-pattern="/^[a-zA-Z0-9]+(?:\,)*/" ng-required="true">
		                            <span ng-show="propertyprofile.placeId.$error.pattern" style="color:red">Not a Valid Name!</span>
		                         </div>
		                      </div>
		                      <div class="col-md-3">
		                         <div class="form-group">
		                            <label>Property GST No <span style="color:red">*</span> </label>
		                            <input type="text" class="form-control" name="propertyGstNo" id="propertyGstNo" value="{{property[0].propertyGstNo}}" placeholder="Property Gst No" ng-model='property[0].propertyGstNo' ng-pattern="/^[a-zA-Z0-9]+(?:\,)*/" ng-required="true">
		                            <span ng-show="propertyprofile.propertyGstNo.$error.pattern" style="color:red">Not a Valid Name!</span>
		                         </div>
		                      </div>
		                       <div class="col-md-6 col-sm-6">
                                 <div class="form-group">
                                    <label>Property Finance Email <span style="color:red"> * (Use comma(,) for mail id's separation)</span> </label>
                                    <textarea class="form-control" name="propertyFinanceEmail" id="propertyFinanceEmail" placeholder="Property Finance Email"  ng-required="true">{{property[0].propertyFinanceEmail}}</textarea>
                                 </div>
                              </div>
                             <div class="col-md-3">
		                         <div class="form-group">
		                            <label>Route Map <span style="color:red">*</span> </label>
		                            <textarea  class="form-control" name="routeMap" id="routeMap"  placeholder="Route Map"   ng-required="true">{{property[0].routeMap}}</textarea>
		                            <span ng-show="propertyprofile.routeMap.$error.pattern" style="color:red">Not a Valid Name!</span>
		                         </div>
		                      </div>  
		                      <div class="col-md-3">
			                     <div class="form-group">
				                     <label>Pay At Hotel Status<span style="color:red">*</span> </label>
				                     <select name="payAtHotelStatus" id="payAtHotelStatus"   class="form-control" ng-required="true">
					                     <option value="true"  ng-selected ="property[0].payAtHotelStatus == 'true'">Active</option>
					                     <option value="false" ng-selected ="property[0].payAtHotelStatus == 'false'">InActive</option>
				                     <!-- <option ng-repeat="pt in propertytypes" value="{{pt.DT_RowId}}" ng-selected ="property[0].propertyTypeId == pt.DT_RowId">{{pt.propertyTypeName}}</option> -->
				                     </select>
			                     </div>
		                     </div>
		                     <div class="col-md-3">
			                     <div class="form-group">
				                     <label>Couple Status <span style="color:red">*</span> </label>
				                     <select name="coupleIsActive" id="coupleIsActive"   class="form-control" ng-required="true">
					                     <option value="true"  ng-selected ="property[0].coupleStatus == 'true'">Active</option>
					                     <option value="false" ng-selected ="property[0].coupleStatus == 'false'">InActive</option>
				                     </select>
			                     </div>
		                     </div>
		                     <div class="col-md-3">
			                     <div class="form-group">
				                     <label>Breakfast Status <span style="color:red">*</span> </label>
				                     <select name="breakfastIsActive" id="breakfastIsActive"   class="form-control" ng-required="true">
					                     <option value="true"  ng-selected ="property[0].breakfastStatus == 'true'">Active</option>
					                     <option value="false" ng-selected ="property[0].breakfastStatus == 'false'">InActive</option>
				                     </select>
			                     </div>
		                     </div>
		                     <div class="col-md-3">
			                     <div class="form-group">
				                     <label>Hotel Status <span style="color:red">*</span> </label>
				                     <select name="hotelIsActive" id="hotelIsActive"   class="form-control" ng-required="true">
					                     <option value="true"  ng-selected ="property[0].hotelStatus == 'true'">Active</option>
					                     <option value="false" ng-selected ="property[0].hotelStatus == 'false'">InActive</option>
				                     </select>
			                     </div>
		                     </div>
		                      <div class="col-md-3">
		                         <div class="form-group">
		                            <label>Property Review Link <span style="color:red">*</span> </label>
		                            <input type="text" class="form-control" name="propertyReviewLink" id="propertyReviewLink" value="{{property[0].propertyReviewLink}}" placeholder="Property Review Link" ng-model='property[0].propertyReviewLink'  ng-pattern="/^[a-zA-Z0-9]+(?:\,)*/" ng-required="true">
		                            <span ng-show="propertyprofile.propertyReviewLink.$error.pattern" style="color:red">Not a Valid Name!</span>
		                         </div>
		                      </div>                            
	                     </div>
                     
                     </div>
                     </section>
                     <section id="property-map">
                        <h4 class="page-header">Property Address</h4>
                        <div class="row fontawesome-icon-list">
                           <div class="col-md-12 col-sm-4">
                              <div id="map" style="width:100%; height:400px;"></div>
                           </div>
                        </div>
                     </section>
                     <section id="video-player">
                        <h4 class="page-header"></h4>
                        <div class="row fontawesome-icon-list">
                           <div class="col-md-3 col-sm-4"></div>
                           <div class="col-md-3 col-sm-4"></div>
                           <div class="col-md-3 col-sm-4"></div>
                           <div class="col-md-3 col-sm-4">
                              <button type="submit" ng-click="editProperty()" class="btn btn-primary btngreeen pull-right" ng-disabled="propertyprofile.$invalid"  onclick ="goToByScroll('page-header');">Save</button>
                           </div>
                        </div>
                     </section>
                     </form>
                  </div>
                  <!-- /.tab-content -->
                  <div class="tab-pane" id="tab-policy">
                     <form method="post" theme="simple" name="propertyPolicy">
                        <section id="new">
                           <div class="row fontawesome-icon-list">
                              <div class="col-md-12 col-sm-12">
                                 <div class="form-group">
                                    <h4>Hotel Policy : </h4>
                                    <textarea id="propertyHotelPolicy" name="propertyHotelPolicy" ng-model="property[0].propertyHotelPolicy" class="form-control" value="{{property[0].propertyHotelPolicy}}" placeholder="Hotel Policy"   ng-required="true">
		                    		{{property[0].propertyHotelPolicy}}
		                    		</textarea>
                                 </div>
                              </div>
                           </div>
                          <!--  <div class="row fontawesome-icon-list">
                              <div class="col-md-12 col-sm-12">
                                 <div class="form-group">
                                    <h4>Standard Policy : </h4>
                                    		                      			<label for="propertyPolicy">Standard Policy</label>
                                    <textarea id="propertyStandardPolicy" ng-model="property[0].propertyStandardPolicy" name="propertyStandardPolicy" class="form-control" value="{{property[0].propertyStandardPolicy}}"  placeholder="Standard Policy" ng-required="true">
		                    		{{property[0].propertyStandardPolicy}}
		                    		</textarea>
                                 </div>
                              </div>
                           </div>
                           <div class="row fontawesome-icon-list">
                              <div class="col-md-12 col-sm-12">
                                 <div class="form-group">
                                    <h4>Cancellation Policy : </h4>
                                    		                      			<label for="propertyPolicy">Cancellation Policy</label>
                                    <textarea  id="propertyCancellationPolicy" ng-model="property[0].propertyCancellationPolicy" name="propertyCancellationPolicy" class="form-control" value="{{property[0].propertyCancellationPolicy}}"  placeholder="Cancellation Policy" ng-required="true">
		                    		{{property[0].propertyCancellationPolicy}}
		                    		</textarea>
                                 </div>
                              </div>
                           </div>
                            <div class="row fontawesome-icon-list">
                              <div class="col-md-12 col-sm-12">
                                 <div class="form-group">
                                    <h4>Child Policy : </h4>
                                    		                      			<label for="propertyPolicy">Cancellation Policy</label>
                                    <textarea  id="propertyChildPolicy" ng-model="property[0].propertyChildPolicy" name="propertyChildPolicy" class="form-control" value="{{property[0].propertyChildPolicy}}"  placeholder="Child Policy" ng-required="true">
		                    		{{property[0].propertyChildPolicy}}
		                    		</textarea>
                                 </div>
                              </div>
                           </div> -->
                        </section>
                        <section>
                           <h4 class="page-header"></h4>
                           <div class="row fontawesome-icon-list">
                              <div class="col-md-3 col-sm-4"></div>
                              <div class="col-md-3 col-sm-4"></div>
                              <div class="col-md-3 col-sm-4"></div>
                              <div class="col-md-3 col-sm-4">
                                 <button type="submit" ng-click="editPropertyPolicy()" class="btn btn-primary btngreeen pull-right"  onclick ="goToByScroll('page-header');">Save</button>
                              </div>
                           </div>
                        </section>
                     </form>
                  </div>
                  
                   <!-- /.property-landmark -->
                  <div class="tab-pane" id="tab-landmark">
                  <div > 
                     <form method="post" id="propertyLandmark" theme="simple" name="propertyLandmark">
                       <section id="new">
                          <div class="col-md-6 col-sm-4" > 
	                          <label for="propertyLandmark1">Property Landmark 1 &nbsp;</label>
	                          <input type ="text" value="{{propLandmark[0].propertyLandmark1}}" id="landmark1" name="landmark1"/>
                          </div>
                          <div  class="col-md-6 col-sm-4">
	                          <label for="kilometers1">Kilometers &nbsp;</label> 
	                          <input type ="text" value="{{propLandmark[0].kilometers1}}" id="kilom1" name="kilom1"/>
                          </div>
                          <div class="col-md-6 col-sm-4">
	                          <label for="propertyLandmark2">Property Landmark 2 &nbsp;</label>
	                          <input type ="text" value="{{propLandmark[0].propertyLandmark2}}" id="landmark2" name="landmark2"/>
                          </div>
                          <div class="col-md-6 col-sm-4">
	                          <label for="kilometers2">Kilometers &nbsp;</label>
	                          <input type ="text" value="{{propLandmark[0].kilometers2}}" id="kilom2" name="kilom2"/>
                          </div>
                          <div class="col-md-6 col-sm-4">
                          	<label for="propertyLandmark3">Property Landmark 3 &nbsp;</label>
                          	<input type ="text" value="{{propLandmark[0].propertyLandmark3}}" id="landmark3" name="landmark3"/>
                          </div>
                          <div class="col-md-6 col-sm-4">
	                          <label for="kilometers3">Kilometers &nbsp;</label>
	                          <input type ="text" value="{{propLandmark[0].kilometers3}}" id="kilom3" name="kilom3"/>
                          </div>
                          <div class="col-md-6 col-sm-4"> 
	                          <label for="propertyLandmark4">Property Landmark 4 &nbsp;</label>
	                          <input type ="text" value="{{propLandmark[0].propertyLandmark4}}" id="landmark4" name="landmark4"/>
                          </div>
                          <div class="col-md-6 col-sm-4">
	                          <label for="kilometers4">Kilometers  &nbsp;</label>
	                          <input type ="text" value="{{propLandmark[0].kilometers4}}" id="kilom4" name="kilom4"/>
                          </div>
                          <div class="col-md-6 col-sm-4">
	                          <label for="propertyLandmark5">Property Landmark 5 &nbsp;</label>
	                          <input type ="text" value="{{propLandmark[0].propertyLandmark5}}" id="landmark5" name="landmark5"/>
                          </div>
                          <div class="col-md-6 col-sm-4">
	                          <label for="kilometers5">Kilometers &nbsp;</label>
	                          <input type ="text" value="{{propLandmark[0].kilometers5}}" id="kilom5" name="kilom5"/>
	                      </div>
                          <div class="col-md-6 col-sm-4">
	                          <label for="propertyLandmark6">Property Landmark 6 &nbsp;</label>
	                          <input type ="text" value="{{propLandmark[0].propertyLandmark6}}" id="landmark6" name="landmark6"/>
	                      </div>
                          <div class="col-md-6 col-sm-4">
	                          <label for="kilometers6">Kilometers &nbsp;</label>
	                          <input type ="text" value="{{propLandmark[0].kilometers6}}" id="kilom6" name="kilom6"/> 
	                      </div>
                          <div class="col-md-6 col-sm-4">
	                          <label for="propertyLandmark7">Property Landmark 7 &nbsp;</label>
	                          <input type ="text" value="{{propLandmark[0].propertyLandmark7}}" id="landmark7" name="landmark7"/>
	                      </div>
                          <div class="col-md-6 col-sm-4">
	                          <label for="kilometers7">Kilometers &nbsp;</label>
	                          <input type ="text" value="{{propLandmark[0].kilometers7}}" id="kilom7" name="kilom7"/>
	                      </div>
                          <div class="col-md-6 col-sm-4">
	                          <label for="propertyLandmark8">Property Landmark 8 &nbsp;</label>
	                          <input type ="text" value="{{propLandmark[0].propertyLandmark8}}" id="landmark8" name="landmark8"/>
	                      </div>
                          <div class="col-md-6 col-sm-4">
	                          <label for="kilometers8">Kilometers &nbsp;</label>
	                          <input type ="text" value="{{propLandmark[0].kilometers8}}" id="kilom8" name="kilom8"/>
	                      </div>
                          <div class="col-md-6 col-sm-4">
	                          <label for="propertyLandmark9">Property Landmark 9 &nbsp;</label>
	                          <input type ="text" value="{{propLandmark[0].propertyLandmark9}}" id="landmark9" name="landmark9"/>
	                      </div>
                          <div class="col-md-6 col-sm-4">
	                          <label for="kilometers9">Kilometers &nbsp;</label>
	                          <input type ="text" value="{{propLandmark[0].kilometers9}}" id="kilom9" name="kilom9"/>
	                      </div>
                          <div class="col-md-6 col-sm-4">
	                          <label for="propertyLandmark10">Property Landmark 10</label>
	                          <input type ="text" value="{{propLandmark[0].propertyLandmark10}}" id="landmark10" name="landmark10"/>
	                      </div>
                          <div class="col-md-6 col-sm-4">
	                          <label for="kilometers10">Kilometers &nbsp;</label>
	                          <input type ="text" value="{{propLandmark[0].kilometers10}}" id="kilom10" name="kilom10"/>
                          </div>
                          
                           
                        </section>
                        <section>
                           <div class="row fontawesome-icon-list">
                           		
                              <div class="col-md-12 col-sm-12">
                              <h4 class="page-header"></h4>
                                 <button type="submit" ng-click="editPropertyLandmark()" class="btn btn-primary btngreeen pull-right" onclick ="goToByScroll('page-header');">Save</button>
                              </div>
                           </div>
                        </section>
                     </form>
                     </div>
                  </div>
   
                  <div class="tab-pane" id="fa-icons3">
                     <section id="new">
                        <input type="hidden" id="photoCategoryId" name="photoCategoryId" value=""/>
                        <!-- <h4 class="page-header">66 New Icons in 4.4</h4> -->
                        <div class="row fontawesome-icon-list">
                           <div class="col-md-4" ng-repeat="cp in catphotos">
                              <div class="box box-solid">
                                 <div class="box-header with-border">
                                    <h3 class="box-title">					                 
                                       <input type="radio" name="propertyPhotoId" value="{{cp.DT_RowId}}" class="flat-red">
                                    </h3>
                                    <a href="#" ng-click="deletePhoto(cp.DT_RowId)"  data-toggle="modal"><i  class="fa fa-fw fa-trash"></i></a>
                                 </div>
                                 <!-- /.box-header -->
                                 <div class="box-body">
                                    <img class="attachment-img " src="get-property-image-stream?propertyPhotoId={{cp.DT_RowId}}"  width="100" height="100" alt="attachment image"/>
                                 </div>
                                 <!-- /.box-body -->
                              </div>
                              <!-- /.box -->
                           </div>
                           <!-- ./col -->
                        </div>
                     </section>
                     <section id="video-player">
                        <h4 class="page-header"></h4>
                        <div class="row fontawesome-icon-list">
                           <div class="col-md-3 col-sm-4"></div>
                           <div class="col-md-3 col-sm-4"></div>
                           <div class="col-md-3 col-sm-4"></div>
                           <div class="col-md-3 col-sm-4">
                              <button  ngf-select="upload($file,cp.photoCategoryId)" ngf-accept="'image/*'" ngf-pattern="'.jpg,.png,!.gif'" class="btn btn-primary btngreen pull-right">Add Images</button>
                           </div>
                        </div>
                     </section>
                  </div>
      
               </div>
            </div>
            <!-- /.nav-tabs-custom -->
         </div>
         <!-- /.col -->
      </div>
      <!-- /.row -->
      <div class="modal" id="deletemodal">
         <div class="modal-dialog">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" id="deletebtnclose" class="close" data-dismiss="modal"
                     aria-hidden="true">x</button>
                  <h4 class="modal-title">Delete Message</h4>
               </div>
               <div class="modal-body">
                  <div class="alert alert-success alert-dismissible">
                     <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                     <h4><i class="icon fa fa-check"></i>Your Photo has been deleted Sucessfully </h4>
                  </div>
               </div>
               <div class="modal-footer">
                  <a href="#" data-dismiss="modal" class="btn">Close</a>
               </div>
            </div>
            <!-- /.modal-content -->
         </div>
         <!-- /.modal-dalog -->
      </div>
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>
<script>
   var app = angular.module('myApp',['ngFileUpload']);
   app.controller('customersCtrl',['$scope',
                					'Upload',
               					'$timeout',
               					'$http', function($scope,Upload,$timeout,$http) {
   
   	$("#alert-message").hide();
   	
   	//$scope.progressbar = ngProgressFactory.createInstance();
   	//$scope.progressbar.setColor("green");
       //$scope.progressbar.start();
   	
         	$timeout(function(){
        	    // $scope.progressbar.complete();
              $scope.show = true;
              $("#pre-loader").css("display","none");
          }, 2000);
         	
         	$scope.unread = function() {
   		var notifiurl = "unreadnotifications.action";
   		$http.get(notifiurl).success(function(response) {
   			$scope.latestnoti = response.data;
   		});
   		};
   
   		$("#taxValidFrom").datepicker({
            dateFormat: 'MM d, yy',
            minDate:  0,
            onSelect: function (formattedDate) {
                var date1 = $('#taxValidFrom').datepicker('getDate'); 
                var date = new Date( Date.parse( date1 ) ); 
                date.setDate( date.getDate() );        
                var newDate = date.toDateString(); 
                newDate = new Date( Date.parse( newDate ) );   
                $('#taxValidTo').datepicker("option","minDate",newDate);
                $timeout(function(){
                  //scope.checkIn = formattedDate;
                });
            }
        });
   		
   		$("#taxValidTo").datepicker({
            dateFormat: 'MM d, yy',
            minDate:  0,
            onSelect: function (formattedDate) {
                var date2 = $('#taxValidTo').datepicker('getDate'); 
                $timeout(function(){
                  //scope.checkOut = formattedDate;
                });
            }
        });
   		
   		$scope.getProperty = function() {
   			
   			var url = "get-property";
   			$http.get(url).success(function(response) {
   				//console.log(response);
   				//alert(JSON.stringify(response.data));
   				$scope.property = response.data;
   			
   			});
   		};
   		
   		$scope.getStates = function() {
   			
   			var url = "get-states";
   			$http.get(url).success(function(response) {
   				//console.log(response);
   				//alert(JSON.stringify(response.data));
   				$scope.states = response.data;
   			
   			});
   		};
   		
   		$scope.getCountries = function() {
   			
   			var url = "get-countries";
   			$http.get(url).success(function(response) {
   				//console.log(response);
   				//alert(JSON.stringify(response.data));
   				$scope.countries = response.data;
   			
   			});
   		};
   		
   		$scope.getPropertyPhotos = function() {
   			
   			var url = "get-property-photos";
   			$http.get(url).success(function(response) {
   				//console.log(response);
   				//alert(JSON.stringify(response.data));
   				$scope.photos = response.data;
   			
   			});
   		};
   		
   		$scope.deletePhoto = function(rowid) {
   			
   			
   			var url = "delete-property-photo?propertyPhotoId="+rowid;
   			$http.get(url).success(function(response) {
   			    //console.log(response);
   			    $scope.getPropertyPhotos();
   			    $('#deletemodal').modal('toggle');
   			    $timeout(function(){
   			      	$('#deletebtnclose').click();
   			    }, 2000);
   				
   	
   			});
   			
   		};
   		
   		$scope.getEscapeHtml =  function(unsafe){
			
			 return unsafe
	         //.replace(/&/g, "and")
	          .replace(/&amp;/g, "and")
	         .replace(/&nbsp;/g, " ")
	         .replace(/&lt;/g, "<")
	         .replace(/&gt;/g, ">")
	         .replace(/&quot;/g, "\"")
	         //.replace(/>/g, "&gt;")
	         //.replace(/'/g, "&#039;")
	         //.replace(/"/g, "'")
			 .replace(/%/g, "percent");
	        
			
		};
   		
   		$scope.editPropertyPolicy = function() {
   			
   		 var hotelPolicy= CKEDITOR.instances.propertyHotelPolicy.getData();
   		 var ChangeHotelPolicy = $scope.getEscapeHtml(hotelPolicy);
   			/* var propertyHotelPolicyj= CKEDITOR.instances.propertyHotelPolicy.getData();
   			var propertyStandardPolicy= CKEDITOR.instances.propertyStandardPolicy.getData();
   			var propertyCancellationPolicy= CKEDITOR.instances.propertyCancellationPolicy.getData(); */
   			
   			var fdata="&propertyHotelPolicy="+ ChangeHotelPolicy
   			/* + "&propertyStandardPolicy=" +$('#propertyStandardPolicy').val()
   			+"&propertyCancellationPolicy="+ $('#propertyCancellationPolicy').val()
   				+ "&propertyChildPolicy=" +$('#propertyChildPolicy').val(); */
   			
   			$http(
   					{
   						method : 'POST',
   						data : fdata,
   						headers : {
   							'Content-Type' : 'application/x-www-form-urlencoded'
   						},
   						url : 'edit-property-policy'
   					}).then(function successCallback(response) {
   						
   						 
   						 $scope.getProperty();
   						$("#alert-message").show();
   				
   			}, function errorCallback(response) {
   	
   			}); 
   		}
   		
   		$scope.getRemoveHtml =  function(unsafe){
			 return unsafe
	       //.replace(/&/g, "and")
	       //.replace(/</g, "&lt;")&nbsp;
			 .replace(/&/g, "and")
			 .replace(/%/g, "percent");
	        
			
		};
   		
   		$scope.editProperty = function() {
   	
   			var description = $('#propertyDescription').val();
      		 var changedDescription = $scope.getRemoveHtml(description);
   			var fdata="&address1="+ $('#address1').val()
   					+ "&address2=" + $('#address2').val()
   					+"&city="+ $('#city').val()
   					+"&latitude="+$('#latitude').val()
   					+"&longitude="+$('#longitude').val()
   					+"&placeId="+$('#placeId').val()
   					+"&propertyTypeId=" + $('#propertyTypeId').val()
   					+"&propertyContact="+$('#propertyContact').val()
   					+"&propertyDescription="+ changedDescription
   					+"&propertyEmail="+$('#propertyEmail').val()
   					+"&propertyLogo="+$('#propertyLogo').val()
   					+"&propertyName="+$('#propertyName').val()
   					+"&resortManagerName="+$('#resortManagerName').val()
   					+"&resortManagerEmail="+$('#resortManagerEmail').val()
   					+"&resortManagerContact="+$('#resortManagerContact').val()
   					+"&reservationManagerName="+$('#reservationManagerName').val()
   					+"&reservationManagerEmail="+$('#reservationManagerEmail').val()
   					+"&reservationManagerContact="+$('#reservationManagerContact').val()
   					+"&contractManagerName="+$('#contractManagerName').val()
   					+"&contractManagerEmail="+$('#contractManagerEmail').val()
   					+"&contractManagerContact="+$('#contractManagerContact').val()
   					+"&revenueManagerName="+$('#revenueManagerName').val()
   					+"&revenueManagerEmail="+$('#revenueManagerEmail').val()
   					+"&revenueManagerContact="+$('#revenueManagerContact').val()
   					//+"&propertyPhone="+$('#propertyPhone').val()
   					+"&zipCode="+$('#zipCode').val() 
   					+"&countryCode="+$('#countryCode').val()
   					+"&strTaxValidFrom="+$('#taxValidFrom').val()
   					+"&strTaxValidTo="+$('#taxValidTo').val()
   					+"&taxIsActive="+$('#taxIsActive').val()
   					+"&propertyCommission="+$('#propertyCommission').val()
   					+"&payAtHotelStatus="+$('#payAtHotelStatus').val()
   					+"&hotelStatus="+$('#hotelIsActive').val()
   					+"&coupleStatus="+$('#coupleIsActive').val()
   					+"&breakfastStatus="+$('#breakfastIsActive').val()
   					+"&stateCode="+$('#stateCode').val()
					+"&propertyGstNo="+$('#propertyGstNo').val()
   					+"&propertyFinanceEmail="+$('#propertyFinanceEmail').val()
   					+"&routeMap="+$('#routeMap').val()
   					+"&propertyReviewLink="+$('#propertyReviewLink').val();
   			
   			var content = document.getElementById("propertyFinanceEmail").value;

 		   if(content.length<1)
 		   {
 		        window.alert ("Please Enter Property Finance Email ");
 		        document.getElementById('propertyFinanceEmail').focus();
 		
 		   }
 	
   			var propertyType = $('#propertyTypeId').val();
   			var propertydescrip= $('#propertyDescription').val();
   			
			//alert(test);
			if (propertyType == ''){
				
				 alert("please select Property Type ");
			}
			else if (propertydescrip == ''){
			
				 alert("please select Property Description ");
			}
               
               else{

   			
   		$http(
   					{
   						method : 'POST',
   						data : fdata,
   						headers : {
   							'Content-Type' : 'application/x-www-form-urlencoded'
   						},
   						url : 'edit-property'
   					}).then(function successCallback(response) {
   						
   						 
   						 $scope.getProperty();
   						$("#alert-message").show();
   						window.location.reload();
   				
   			}, function errorCallback(response) {

   			}); 
               }
   
   		};
   		
   		
   		 $scope.getPhotoCategory = function() {
   				
   				var url = "get-Photo-category";
   				$http.get(url).success(function(response) {
   					//console.log(response);
   					//alert(JSON.stringify(response.data));
   					$scope.photocategory = response.data;
   				
   				});
   			};
   			
   		
   		$scope.getCategoryPhotos = function(photoCategoryId) {
   	
   			document.getElementById("photoCategoryId").value = photoCategoryId;
   			 var url = "get-category-photos?photoCategoryId="+photoCategoryId							
   			$http.get(url).success(function(response) {
   				//console.log(response);
   				$scope.catphotos = response.data;
   				//window.location.reload();
   		
   			}); 
   		};
   		
   			$scope.getCategoryPhotos1 = function() {
   			
   			//$('#adminId').val()	
   			//alert(photoCategoryId);
   			var  photoCategoryId = document.getElementById("photoCategoryId").value;
   			//alert( photoCategoryId);
   			 var url = "get-category-photos?photoCategoryId="+photoCategoryId	
   					// alert(url);
   			$http.get(url).success(function(response) {
   				//console.log(response);
   				$scope.catphotos = response.data;
   				//window.location.reload();
   		
   			}); 
   		};
   		
   		
    	
   		$scope.getPropertyTypes = function() {
   			
   			var url = "get-property-types";
   			$http.get(url).success(function(response) {
   				//console.log(response);
   				//alert(JSON.stringify(response.data));
   				$scope.propertytypes = response.data;
   			
   			});
   		};
                 
   		 $scope.getPropertyList = function() {
   			 
   	        	
   	        	
   		        var userId = $('#adminId').val();
   	 			var url = "get-user-properties?userId="+userId;
   	 			$http.get(url).success(function(response) {
   	 			    
   	 				$scope.props = response.data;
   	 	
   	 			});
   	 		};
   	 		
                $scope.change = function() {
          	   
   	        //alert($scope.id);
   	        
   	        var propertyId = $scope.id;	
          	    var url = "change-user-property?propertyId="+propertyId;
    			$http.get(url).success(function(response) {
    				
    				 window.location = '/ulopms/dashboard'; 
    				//$scope.change = response.data;
    	
    			});
   		       
   	 		};
   	 		
   			
		$scope.editPropertyLandmark = function() {
   			var fdata="&propertyLandmark1="+ $('#landmark1').val()
   					+ "&propertyLandmark2=" + $('#landmark2').val()
   					+"&propertyLandmark3="+ $('#landmark3').val()
   					+"&propertyLandmark4="+$('#landmark4').val()
   					+"&propertyLandmark5="+$('#landmark5').val()
   					+"&propertyLandmark6="+$('#landmark6').val()
   					+"&propertyLandmark7="+$('#landmark7').val()
   					+"&propertyLandmark8="+$('#landmark8').val()
   					+"&propertyLandmark9="+$('#landmark9').val()
   					+"&propertyLandmark10="+$('#landmark10').val()   					
   					+"&kilometers1=" +$('#kilom1').val()
   					+"&kilometers2="+$('#kilom2').val()
   					+"&kilometers3="+$('#kilom3').val()
   					+"&kilometers4="+$('#kilom4').val()
   					+"&kilometers5="+$('#kilom5').val()
   					+"&kilometers6="+$('#kilom6').val()
   					+"&kilometers7="+$('#kilom7').val()
   					+"&kilometers8="+$('#kilom8').val()
   					+"&kilometers9="+$('#kilom9').val()
   					+"&kilometers10="+$('#kilom10').val()
   			
   			
	   		$http(
	   					{
	   						method : 'POST',
	   						data : fdata,
	   						headers : {
	   							'Content-Type' : 'application/x-www-form-urlencoded'
	   						},
	   						url : 'edit-property-landmark'
	   					}).then(function successCallback(response) {
	   						
	   						 
	   						 $scope.getProperty();
	   						$("#alert-message").show();
	   						window.location.reload();
	   				
	   			}, function errorCallback(response) {
	
	   			}); 
              
   
   		};
   		
   		$scope.getPropertyLandmark = function() {
   			var propertyid =  $('#propertysId').val();
   			//alert("prop"+propertyid);
   			var url = "get-property-landmark?propertyId="+propertyid
	 			$http.get(url).success(function(response) {
	 				$scope.propLandmark = response.data;
	 	
	 			});
	 		};
   	 		
   		
	 		$scope.getPropertyLandmark();
	 		
   	    	$scope.getPropertyList();
   		
   	    	$scope.getPhotoCategory();  
   	    	
            $scope.getProperty();
   		
   			$scope.getStates();
   		
   			$scope.getCountries();
   		
   			$scope.getPropertyTypes();
   		
   			$scope.getPropertyPhotos();

           $scope.upload = function (file) {
           	//alert("file"+file);
           	var id = document.getElementById("photoCategoryId").value;
           	//alert("id"+id);
           	//alert("id"+photoCategoryId);	
               Upload.upload({
                   url: 'add-property-photo',
                   enableProgress: true, 
                   data: {myFile: file,'photoCategoryId':id}
               }).then(function (resp) {
               	
               	$scope.getCategoryPhotos1();
                   console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
                   //$scope.getPropertyPhotos();
               }, function (resp) {
                   console.log('Error status: ' + resp.status);
               }, function (evt) {
               	
                   var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                   
                   console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
                  
               });
              
               
           };
           
   	
   	//$scope.unread();
   	
   
   }]);
   
   
</script>
<script src="js1/upload/ng-file-upload-shim.min.js"></script>
<script src="js1/upload/ng-file-upload.min.js"></script>
<script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.js"></script>

<script>
   $(function () {
      // Replace the <textarea id="editor1"> with a CKEditor
      // instance, using default configuration.
     
      
     CKEDITOR.replace('propertyHotelPolicy');
      //bootstrap WYSIHTML5 - text editor
      $(".textarea").wysihtml5();
      
    /*   CKEDITOR.replace('propertyStandardPolicy');
      //bootstrap WYSIHTML5 - text editor
      $(".textarea").wysihtml5();
      
     CKEDITOR.replace('propertyCancellationPolicy');
     // bootstrap WYSIHTML5 - text editor
      $(".textarea").wysihtml5();
     
      CKEDITOR.replace('editor1');
      //bootstrap WYSIHTML5 - text editor
      $(".textarea").wysihtml5();
      
      CKEDITOR.replace('propertyDescription');
      //bootstrap WYSIHTML5 - text editor
      $(".textarea").wysihtml5(); */
    });
   
  
</script>
<script async defer
   src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDg9dqRsKwRWXGdTKsUNHTHhfQqdSyip9k&callback=initMap"></script>
<!--<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>-->
<script type="text/javascript">
   var markers = [
       {
           "title": 'ulohotels',
           "lat": '13.05406559587872',
           "lng": '80.19280683201782',
           "description": 'ULO Hotels is a trusted and fast growing hotel network with over 10 partners. We use the most advanced technology to offer best value promotions and hotel rates to business and leisure travelers.'
       }
   ];
   window.onload = function () {
       var mapOptions = {
           center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
           zoom: 0,
           mapTypeId: google.maps.MapTypeId.ROADMAP
       };
       var infoWindow = new google.maps.InfoWindow();
       var latlngbounds = new google.maps.LatLngBounds();
       var geocoder = geocoder = new google.maps.Geocoder();
       var map = new google.maps.Map(document.getElementById("map"), mapOptions);
       for (var i = 0; i < markers.length; i++) {
           var data = markers[i]
           var myLatlng = new google.maps.LatLng(data.lat, data.lng);
           var marker = new google.maps.Marker({
               position: myLatlng,
               map: map,
               title: data.title,
               draggable: true,
               animation: google.maps.Animation.DROP
           });
           (function (marker, data) {
               google.maps.event.addListener(marker, "click", function (e) {
                   infoWindow.setContent(data.description);
                   infoWindow.open(map, marker);
               });
               google.maps.event.addListener(marker, "dragend", function (e) {
                   var lat, lng, address;
                   geocoder.geocode({ 'latLng': marker.getPosition() }, function (results, status) {
                       if (status == google.maps.GeocoderStatus.OK) {
                           lat = marker.getPosition().lat();
                           lng = marker.getPosition().lng();
                           address = results[0].formatted_address;
                           $('#latitude').val(lat);
                           $('#longitude').val(lng);
                           
                           //alert("Latitude: " + lat + "\nLongitude: " + lng + "\nAddress: " + address);
                       }
                   });
               });
           })(marker, data);
           latlngbounds.extend(marker.position);
       }
       var bounds = new google.maps.LatLngBounds();
       map.setCenter(latlngbounds.getCenter());
       map.fitBounds(latlngbounds);
   }
</script>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
   $( function() {
//      $("#taxValidFrom").datepicker({minDate:0});
//      $("#taxValidTo").datepicker({minDate:0});
     $("#checkin" ).datepicker({minDate:0});
     $("#checkout" ).datepicker({minDate:0});
     $("#expiryDate" ).datepicker({minDate:0});
   });
</script> 
<script>
   function goToByScroll(id){
      
       // Reove "link" from the ID
     id = id.replace("link", "");
       // Scroll
     $('html,body').animate({
         scrollTop: $("#"+id).offset().top},
         'slow');
   }
   
</script>