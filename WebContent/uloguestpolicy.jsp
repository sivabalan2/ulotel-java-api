<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
   pageEncoding="ISO-8859-1"%>
<section id="innerPageContent">
   <!-- Tab Header -->
   <!-- Tab Header -->
   <div class="container tabBG">
      <div class="row">
         <div class="col-sm-12">
            <!-- Overview Tab -->
            <div class="tabContent" id="overviewTab" style="display:block;">
               <h1 class="heading1">GUEST POLICY</h1>
               <div class="textContent">
<p>The minimum age of primary guest should be 18. </p>
<p>It is mandatory to produce ID proofs during check-in (Pan Card is not accepted as ID Proof). </p>
<p>Charges are applicable for the extra mattress. </p>
<p>Pets are not allowed inside the hotel. </p>
<p>If any action by the guest is regarded inappropriate and is brought to the notice of hotel, the hotel retains the right, after the claims have been investigated, to take necessary action against the guest. </p>
<p>Some destinations may have different policies for certain times during the year. </p>
<p>Guests will be accountable for any damage, except general wear and tear to Hotel asset. </p>
<p>Some policies are booking specific and are informed to the guest during the booking. </p>
<p>Total Tariff is the indicative price at which rooms are available in the other websites/ nearby locality and does not definitely denote the price of our hotel. The discount is displayed with reference to indicative rate. </p>

                  <H2>BOOKING EXTENSION POLICY</H2>
                  <br>
<p>Continuation of stay would be offered on current room prices, subject to room availability. </p>
<p>Current room prices can vary from the prices at which the rooms were reserved. </p>

                  <H2>CANCELLATION POLICIES</H2>
                  <br>
                  <p>If the booking is cancelled 3 days prior to the check-in date, Rs.300 will be charged from the total amount and the remaining amount will be refunded within 10 to 12 working days.</p>
                  <p>If the booking is cancelled 2 days prior to the check-in date, 25% from the total amount will be charged as a cancellation fee.</p>
                  <p>If the booking is cancelled one day prior to the check-in date, no refund is applicable. No refund is applicable for cancellations done on long weekends, festival and On-Season times.</p>


                  <H2>PAY AT HOTEL</H2>
                  <br>
<p>Guest needs to pay 20% of the booking amount on our website as an advance and pay the remaining amount at the hotel. </p>
                  <H2>EARLY CHECK-IN</H2>
                  <br>
<p>Our standard check-in time is 10:00 A.M. </p>
<p>Flexible check-in is applicable on weekdays. </p>
<p>Early check-in is subject to room availability during festivals and weekend times. </p>

                  <H2>LATE CHECKOUT</H2>
                  <br>
  <p>Our standard checkout time is 10:00 A.M. </p>
<p>Late checkout is subject to room availability. </p>
<p>Late checkout is complimentary between 10:00 A.M and 12:00 P.M. </p>
<p>For checkout between 12:00 P.M and 04:00 P.M, 30% charge is applicable as per room prices of next day. </p>
<p>For checkout after 04:00 P.M, 100% charge is applicable as per room prices of next day. </p>
                  <H2>HOTEL SPECIFIC POLICIES</H2>
                  <br>
<p>The amenities specific for each hotel are available on our website. Guests are recommended to refer to it while booking. </p>
<p>The air conditioning facility may not be available at hotels in hill stations because of the climatic condition of the region. </p>
<p>Wi-Fi connectivity may not be available at hotels in remote locations because of the unavailability of the network in those locations. </p>
<p>In certain hotels, unmarried couples are not allowed. The room can be refused to couples if required identification proof is not provided during check-in time. </p>
<p>Hotels may refuse check-in to guests presenting ID proof of the same location as the hotel itself. </p>
<p>Entry of visitors may not be allowed in some hotels. Kindly confirm with the hotel prior inviting visitors to the rooms. </p>

                  <H2>CHILD POLICY</H2>
                  <br>
<p>No charge for an additional mattress for a child under 5 years of age. </p>
<p>Additional charges are applicable for an extra mattress for child above 5 years of age. </p>
<p>Children above the age of 11 years are considered as adults only. </p>
<h2>OCCUPANCY POLICY</h2>
  <br>
<p>The maximum number of guests in a room depends upon the room category and the hotel. </p>


               </div>

            </div>
         </div>
      </div>
   </div>
</section>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script src="js1/ngprogress.js" type="text/javascript"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script> 
<script>
   var app = angular.module('myApp', ['ngProgress']);
   app.controller('customersCtrl', function ($scope, $http, $timeout, ngProgressFactory) {
    $scope.userData = [];
   
    	$scope.login = function () {
   
   
    		var fdata = "username=" + $('#username').val() +
    			"&password=" + $('#password').val();
    		//alert(fdata);
   
    		$http({
    			method: 'POST',
    			data: fdata,
    			headers: {
    				'Content-Type': 'application/x-www-form-urlencoded'
    			},
    			url: 'ulologin'
   
    		}).then(function successCallback(response) {
    			$scope.userData = response.data;
    			console.log($scope.userData);
    			var username = $scope.userData.data[0].userName;
    			var email = $scope.userData.data[0].emailId;
    			var phone = $scope.userData.data[0].phone;
   
    			$('#loginModal').modal('toggle');
    			$('#logSec').html("<div class='mnuContant'><a href='mailto:support@ulohotels.com' title='support@ulohotels.com'><i class='fa fa-envelope'></i><span class='visibleLG'>support@ulohotels.com</span></a><a href='tel:9543592593' title=''+91 95435 92593'><i class='fa fa-phone'></i><span class='visibleLG'>+91 95435 92593</span></a></div><div class='mnuProfile'><div class='mpAvatar hidden-xs' style='background:url(ulowebsite/images/avatar.png);'></div><div class='mpUsername'><a href='javascript:showpro();' class=' mnuProfileOpen'>Hi admin<i class='fa fa-angle-down'></i></a></div><div class='mpSubMenu' id='showmenu' ><ul><li><a href='ulouserprofile'>Profile</a></li><li><a href='ulouserprofile'>Booking History</a></li><li><a href='ulologout'>Log out</a></li></ul></div></div>");
    		}, function errorCallback(response) {
   
    			var alert = ' <div class="alert alert-danger alert-dismissible" style="color:red">Invalid username or password.</div>';
    			$('#loginerror').html(alert);
    		    $(function() {
    	          // setTimeout() function will be fired after page is loaded
    	          // it will wait for 5 sec. and then will fire
    	          // $("#successMessage").hide() function
    	          setTimeout(function() {
    	              $("#loginerror").hide('blind', {}, 100)
    	          }, 5000);
    	      });
    		});
   
    	};
   
    	$scope.signup = function () {
   
   
    		var fdata = "userName=" + $('#userName').val() +
    			"&emailId=" + $('#emailId').val() +
    			"&roleId=" + $('#roleId').val() +
    			"&accessRightsId=" + $('#accessRightsId').val() +
    			"&password=" + $('#signpassword').val();
    		//alert(fdata);
   
    		$http(
   
    			{
    				method: 'POST',
    				data: fdata,
    				headers: {
    					'Content-Type': 'application/x-www-form-urlencoded'
    				},
    				url: 'ulosignup'
    			}).then(function successCallback(response) {
   
   
    			if (response.data == "") {
   
    				var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:red">Sorry This Email Id already exist.</div>';
    				$('#signuperror').html(alertmsg);
   
   
    			} else {
   
    				$scope.userData = response.data;
    				var username = $scope.userData.data[0].userName;
    				var email = $scope.userData.data[0].emailId;
    				var phone = $scope.userData.data[0].phone;
   
   
    				$('#registerModal').modal('toggle');
   
    				$('#logSec').html("<div class='mnuContant'><a href='mailto:support@ulohotels.com' title='support@ulohotels.com'><i class='fa fa-envelope'></i><span class='visibleLG'>support@ulohotels.com</span></a><a href='tel:9543592593' title=''+91 95435 92593'><i class='fa fa-phone'></i><span class='visibleLG'>+91 95435 92593</span></a></div><div class='mnuProfile'><div class='mpAvatar hidden-xs' style='background:url(ulowebsite/images/avatar.png);'></div><div class='mpUsername'><a href='javascript:showpro();' class=' mnuProfileOpen'>Hi admin<i class='fa fa-angle-down'></i></a></div><div class='mpSubMenu' id='showmenu' ><ul><li><a href='ulouserprofile'>Profile</a></li><li><a href='ulouserprofile'>Booking History</a></li><li><a href='ulologout'>Log out</a></li></ul></div></div>");
    			}
   
    		}, function errorCallback(response) {
   
   
    			var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:red">This Email ID Already Exist</div>';
    			$('#signuperror').html(alertmsg);
   
    			// or server returns response with an error status.
    		});
   
    	};
   
   
    	$scope.passwordRequest = function () {
   
   
    		var fdata = "emailId=" + $('#forgetEmailId').val();
   
   
    		$http({
    			method: 'POST',
    			data: fdata,
    			headers: {
    				'Content-Type': 'application/x-www-form-urlencoded'
    			},
    			url: 'ulosendpassword'
    		}).then(function successCallback(response) {
   
    			var alertmsg = ' <div class="alert alert-success alert-dismissible" style="color:red">Password Sent Sucessfully.</div>';
   
    			$('#forgetmessage').html(alertmsg);
   
   
    		}, function errorCallback(response) {
   
   
    			var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:red">Your Email ID does not Registred Yet</div>';
    			$('#forgetmessage').html(alertmsg);
   
   
    		});
   
    	};
   
    	$('#forgetPanel').hide();
    	$scope.showForget = function () {
   
    		$('#loginPanel').hide();
    		$('#forgetPanel').show();
   
   
    	};
   
    	$scope.showLogin = function () {
   
    		$('#forgetPanel').hide();
    		$('#loginPanel').show();
   
   
    	};
   
   $timeout(function () {
     	$('#btnclose').click();
     }, 1000);
     $timeout(function () {
     	$('#btncloses').click();
     }, 1000);
   
   });
   
   
</script>
