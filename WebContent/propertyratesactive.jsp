<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="s" uri="/struts-tags"%>

   <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Active Smart Price
            <!--<small>Subscription Expiry on {{property[0].subscription[0].expirydate}}</small>-->
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Rate Management</a></li>
            <li class="active">Smart Price</li>
          </ol>
        </section>
		
        <!-- Main content -->
        <section class="content">
       
        	
        	<div class="row">
       
            <div class="col-xs-12">
              <div class="nav-tabs-custom">
              	<ul class="nav nav-tabs">
                  <li class="active"><a href="#fa-icons" data-toggle="tab">Active Smart Prices</a></li>
                  <li><a href="#tab-rates" data-toggle="tab">Active Property Rates</a></li>
                </ul>
             <div class="tab-content">
                  <!-- Font Awesome Icons -->
                 <div class="tab-pane active" id="fa-icons">
                  <form method="post" theme="simple" name="activesmartprice">
                  <section class="content">
                  <div class="box-body table-responsive no-padding" > 
						<table class="table table-hover" id="example1">
						<thead>
						<tr>
							<th>Order</th>
						    <th>Start</th>
						   <th>End</th>
<!-- 						   <th>Amount</th> -->
						   <th>Accommodation Type</th>
<!-- 							<th>View</th>  -->
							<th>Delete</th>
							</tr>
						</thead>
						<tbody class="sortable">
								<tr ng-repeat="s in smartprices track by $index"  >
								<td>{{s.serialNo}}</td>
							    <td>{{s.startDate}}</td>
								<td>{{s.endDate}}</td>
<!-- 								<td>{{s.baseAmount}}</td>	 -->
								<td>{{s.accommodationType}}</td>									
								
								<!--  <td>
									<a href="#viewmodal" ng-click="getActive(s.propertyRateId)"  data-toggle="modal"><i  class="fa fa-pencil text-green"></i></a>
								
								</td>  -->
									<td>
									
									<a href="#" ng-click="deletedSmartPrice(s.propertyRateId)"  data-toggle="modal"><i  class="fa fa-fw fa-trash text-red"></i></a>
								</td> 
							</tr>
						
						</tbody>
					
						</table>
					</div>
					</section>
                  </form>
                  </div>
                  <!-- Font Awesome Icons -->
                 <div class="tab-pane" id="tab-rates">
                  <form method="post" theme="simple" name="activerates">
                  <section class="content">
                  <div class="box-body table-responsive no-padding" > 
						<table class="table table-hover" id="example1">
						<thead>
						<tr>
							<th>Order</th>
						    <th>Start</th>
						   <th>End</th>
						   <th>Amount</th>
						   <th>Accommodation Type</th>
						   <th>Source Type</th> 
							<th>Delete</th>
							</tr>
						</thead>
						<tbody class="sortable">
								<tr ng-repeat="r in rateprices track by $index"  >
								<td>{{r.serialNo}}</td>
							    <td>{{r.startDate}}</td>
								<td>{{r.endDate}}</td>
								<td>{{r.baseAmount}}</td>
								<td>{{r.accommodationType}}</td>											
								<td>{{r.sourceType}}</td>
								<!--  <td>
									<a href="#viewmodal" ng-click="getActive(rates.propertyRateId)"  data-toggle="modal"><i  class="fa fa-pencil text-green"></i></a>
								
								</td>  -->
									<td>
									
									<a href="#" ng-click="deletedRates(r.propertyRateId)"  data-toggle="modal"><i  class="fa fa-fw fa-trash text-red"></i></a>
								</td> 
							</tr>
						
						</tbody>
					
						</table>
					</div> 
					</section>
                  </form>
                  </div>
                  
                  </div>
           
                  
                  
              </div>
              </div><!-- /.nav-tabs-custom -->
            </div><!-- /.col -->
            </section><!-- /.content -->
          </div><!-- /.row -->

<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="plugins/fullcalendar/fullcalendar.min.js"></script>
  <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <style type="text/css">
    #ui-datepicker-div
    {
        z-index: 9999999 !important;
    }
</style>
	<script>
	 
	
	var app = angular.module('myApp', ['ngProgress']);
	app.controller('customersCtrl', function($scope, $http,$timeout,ngProgressFactory) {
     
		
          
          var sortableEle;
              sortableEle = $('.sortable').sortable({
              start: $scope.dragStart,
              update: $scope.dragEnd
              
          });
             

		
       	$timeout(function(){
      	    // $scope.progressbar.complete();
            $scope.show = true;
            $("#pre-loader").css("display","none");
        }, 2000);

		 
		
		$scope.unread = function() {
		//var notifiurl = "http://localhost:8085/collaborative-workflow/unreadnotifications.action";
		var notifiurl = "unreadnotifications.action";
		$http.get(notifiurl).success(function(response) {
			$scope.latestnoti = response.data;
		});
		};
	      
			
			
			
		$scope.deletedSmartPrice=function(rowid){
			var fdata = "&updatePropertyRateId="+ rowid
			var deleteSmartPriceCheck=confirm("Do u want to delete the smart price?");
			if(deleteSmartPriceCheck){
				$http(
						{
							method : 'POST',
							data : fdata,
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							url: 'get-inactive-smart-price'
						}).then(function successCallback(response) {
							window.location.reload();
						   }, function errorCallback(response) {
							
							
						});
			}
			else{
				
			}
			
		};
		
		$scope.deletedRates=function(rowid){
			var fdata = "&updatePropertyRateId="+ rowid
			var deleteSmartPriceCheck=confirm("Do u want to delete the manage rates?");
			if(deleteSmartPriceCheck){
				$http(
						{
							method : 'POST',
							data : fdata,
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							url: 'get-inactive-property-rates'
						}).then(function successCallback(response) {
							window.location.reload();
						   }, function errorCallback(response) {
							
							
						});
			}
			else{
				
			}
			
		};
			//deletedRates
		
		$scope.getSmartPrices = function() {

			var url = "get-smart-prices";
			$http.get(url).success(function(response) {
			    console.log(response);
				$scope.smartprices = response.data;
	
			});
		};
		
		$scope.getPropertyRates = function() {
			var url = "get-property-rates";
			$http.get(url).success(function(response) {
			    console.log(response);
				$scope.rateprices = response.data;
	
			});
		};
			
			
		$scope.getSmartPrices();
		$scope.getPropertyRates();
		
	});





	</script>
   