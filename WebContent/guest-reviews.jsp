<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="s" uri="/struts-tags"%>

   <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Guest Review
          </h1>
          <ol class="breadcrumb">
            <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Guest Review</li>
          </ol>
        </section>
		
        <!-- Main content -->
        <section class="content">
        
        	<div class="row">
            	<div class="col-xs-12" >
						<div class="box">
							<div class="box-header">
								<h3 class="box-title">Guest Review</h3>
							</div>
							
							
							<!-- /.box-header -->
							<div class="box-body table-responsive no-padding" > 
								 <div class="form-group col-md-2 right" > 
									<label>Search</label>
									<input type="text" ng-model="search" class="form-control" placeholder="Search">
								  
								  </div>
								<table class="table table-hover" id="example1">
									<tr>
										<th>Order</th>
										<th>Property Name</th>
										<th>Guest Name</th>
										<th>Review Count</th>
										<th>Edit</th>
										<th>Delete</th>
										<th>User Name</th>
									</tr>
									<tr ng-repeat="gr in guestreview | filter:search">
										<td>{{gr.serialNo}}</td>
										<td>{{gr.reviewPropertyName}}</td>
										<td>{{gr.guestName}}</td>
										<td>{{gr.reviewCount}}</td>
										<td>
											<a href="#editmodal" ng-click="getGuestReview(gr.guestReviewId,gr.reviewPropertyId)"  data-toggle="modal"><i  class="fa fa-pencil text-green"></i></a>
										</td>
										<td>
											<a href="#deletemodal" ng-click="deleteGuestReview(gr.guestReviewId)" data-toggle="modal" ><i  class="fa fa-fw fa-trash text-red" ></i></a>
										</td>
										<td>{{gr.createdBy}}</td>
									</tr>
									<tr>
									</tr>
								</table>
							</div>
							<!-- /.box-body -->
							
							
							<div class="box-footer clearfix">
								 <ul class="pagination pagination-sm no-margin pull-right">
									<a class="btn btn-primary " href="#addmodal" data-toggle="modal" >Add Property Review</a>
								</ul> 
							</div>
							<div class="box-footer clearfix">
								<ul class="pagination pagination-sm"></ul>
								<div class="simple-pagination pull-right" >   
									<p class="simple-pagination__items">Showing {{guestReviewCount[0].count}} out of {{settings.pageLimit}}</p>
									<p><button ng-click="previousPage()" ng-disabled="settings.currentPage <= 0" class="simple-pagination__button simple-pagination__button--prev"> &#10094;          
									</button><span class="simple-pagination__pages">{{settings.currentPage + 1}} of {{getTotalPages()}}</span>
									<button ng-click="nextPage()" ng-disabled="settings.currentPage === (getTotalPages() - 1)" class="simple-pagination__button simple-pagination__button--next">&#10095;</button></p>
									<a href="" ng-click="limited(10)" ng-class="active">{{10}}</a> |
									<span><a href="" ng-click="all(guestReviewCount[0].count)" ng-class="active">All</a></span> 
								</div>
						   </div> 
						</div>
						<!-- /.box -->
					</div>	
         	</div>
          
        </section><!-- /.content -->
      	<input type="hidden" value ="<%=session.getAttribute("propertyId") %>" id="propertyId" name="propertyId" >
      	<div class="modal" id="addmodal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" id="btnclose" class="close" data-dismiss="modal"
							aria-hidden="true">x</button>
						<h4 class="modal-title">Add Guest Review</h4>
					</div>
					<form  name="addguestreview">
					<div class="modal-body" >
						<div class="form-group col-md-12">
						  <label>Select Property<span style="color:red">*</span></label>
						  <select  name="reviewPropertyId" id="reviewPropertyId" ng-model="reviewPropertyId" ng-required="true" class="form-control" >
							  <option style="display:none" value="0" >Select Your Property</option>
			                  <option ng-repeat="p in properties" value="{{p.DT_RowId}}" >{{p.propertyName}}</option>
		                  </select>
						</div>
						<div class="form-group col-md-12">
						  <label>Select Reviewer<span style="color:red">*</span></label>
						  <select  name="reviewerId" id="reviewerId" ng-model="reviewerId" ng-required="true" class="form-control" >
							  <option style="display:none" value="0" >Select Your Property</option>
			                  <option ng-repeat="r in reviewer" value="{{r.reviewerId}}" >{{r.reviewerName}}</option>
		                  </select>
						</div>
						<div class="form-group col-md-12">
							<label>Review Guest Name<span style="color:red">*</span></label>
							<input type="text" class="form-control"  name="reviewGuest"  id="reviewGuest" ng-model="reviewGuest" placeholder="Review Guest" ng-required="true">
						    <span ng-show="addguestreview.reviewGuest.$error.pattern" style="color:red">Not a Review Count!</span>
						</div>
						<div class="form-group col-md-12">
                             <div class="form-group">
                                <label for="firstname">Review Description <span style="color:red">*</span> </label>
                                <textarea id="reviewDescription" name="reviewDescription" ng-model="reviewDescription" class="form-control"  value=""  placeholder="Review Description"   ng-required="true">
                                </textarea>
                             </div>
                         </div>
						<div class="form-group col-md-12">
						  <label>Review Count<span style="color:red">*</span></label>
						  <select  name="reviewCount" id="reviewCount" ng-required="true" ng-model="reviewCount" class="form-control" >
							  <option value="0">Select Star Count</option>
			                  <option value="1">1</option>
			                  <option value="2">2</option>
			                  <option value="3">3</option>
			                  <option value="4">4</option>
			                  <option value="5">5</option>
		                  </select>
						</div>
						
						
						<div class="form-group" >
						   <div class="input-group" >
						  		<label>&nbsp;&nbsp;</label>
						  		<label>&nbsp;&nbsp;</label>
						  		<label>&nbsp;&nbsp;</label>
						  		<label>&nbsp;&nbsp;</label>
						  		<input type="hidden" class="form-control">
						   </div>

						</div>
					</div>
					
						<div class="modal-footer">
							<a href="#" data-dismiss="modal" class="btn btn-danger">Close</a>
							<button type="submit" ng-click="addGuestReviewDetails()"  class="btn btn-primary">Save</button>
						</div>
				
					</form>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dalog -->
		</div>
		<div class="modal" id="editmodal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" id="editbtnclose" class="close" data-dismiss="modal"
							aria-hidden="true">x</button>
						<h4 class="modal-title">Edit Property Review</h4>
					</div>
					 	 <form method="post" theme="simple" name="editreview">
					 	 <section>
						<div class="modal-body">
							<input type="hidden" value="{{reviews[0].guestReviewId}}" id="editGuestReviewId" name="editGuestReviewId">
						    <input type="hidden" value="{{reviews[0].reviewPropertyId}}" id="editReviewPropertyId" name="editReviewPropertyId">
						    <div class="form-group col-md-12">
							  <label>Select Property<span style="color:red">*</span></label>
							  <select  name="editReviewPropertyId" id="editReviewPropertyId" ng-model="reviews[0].reviewPropertyId" ng-required="true" class="form-control" >
								  <option style="display:none" value="0" >Select Your Property</option>
				                  <option ng-repeat="p in properties" value="{{p.DT_RowId}}" ng-selected="reviews[0].reviewPropertyId==p.DT_RowId" >{{p.propertyName}}</option>
			                  </select>
							</div>
							<div class="form-group col-md-12">
							  <label>Select Reviewer<span style="color:red">*</span></label>
							  <select  name="editReviewerId" id="editReviewerId" ng-model="reviews[0].reviewerId" ng-required="true" class="form-control" >
								  <option style="display:none" value="0" >Select Your Property</option>
				                  <option ng-repeat="r in reviewer" value="{{r.reviewerId}}" ng-selected="reviews[0].reviewerId==r.reviewerId" >{{r.reviewerName}}</option>
			                  </select>
							</div>
							<div class="form-group col-md-12">
								<label>Review Guest Name<span style="color:red">*</span></label>
								<input type="text" class="form-control"  name="editReviewGuest" value="{{reviews[0].guestName}}" id="editReviewGuest" ng-model="reviews[0].guestName" placeholder="Review Guest" ng-required="true">
							    <span ng-show="editreview.editReviewGuest.$error.pattern" style="color:red">Not a Review Count!</span>
							</div>
							<div class="form-group col-md-12">
	                             <div class="form-group">
	                                <label for="firstname">Review Description <span style="color:red">*</span> </label>
	                                <textarea id="editReviewDescription" name="editReviewDescription" ng-model="reviews[0].reviewDescription" class="form-control"  value="{{reviews[0].reviewDescription}}"  placeholder="Review Description"   ng-required="true">
	                                {{reviews[0].reviewDescription}}
	                                </textarea>
	                             </div>
	                         </div>
							
						    <div class="form-group col-md-12">
								 <label>Star Count<span style="color:red">*</span></label>
								 <select  name="editReviewCount" id="editReviewCount" ng-model="editReviewCount" ng-required="true" class="form-control" >
									  <option style="display:none" value="0">Select Star Count</option>
					                  <option value="1" ng-selected ="reviews[0].reviewCount == '1'">1</option>
					                  <option value="2" ng-selected ="reviews[0].reviewCount == '2'">2</option>
					                  <option value="3" ng-selected ="reviews[0].reviewCount == '3'">3</option>
					                  <option value="4" ng-selected ="reviews[0].reviewCount == '4'">4</option>
					                  <option value="5" ng-selected ="reviews[0].reviewCount == '5'">5</option>
				                  </select>
							</div>
							<div class="form-group" >
						   <div class="input-group" >
						  		<label>&nbsp;&nbsp;</label>
						  		<label>&nbsp;&nbsp;</label>
						  		<label>&nbsp;&nbsp;</label>
						  		<label>&nbsp;&nbsp;</label>
						  		<input type="hidden" class="form-control">
						   </div>

						</div>
								
						</div>
						</section>
						<section>
							<div class="modal-footer">
								<a href="#" data-dismiss="modal" class="btn btn-danger">Close</a>
								<button ng-click="editGuestReviewDetails()" class="btn btn-primary">Update</button>
							</div>  
						</section>
						</form>
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dalog -->
		</div>
		
		
  

      
      


<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>

	<script>


 
	
	var app = angular.module('myApp',['ngFileUpload']);
	app.controller('customersCtrl',['$scope',
	             					'Upload',
	            					'$timeout',
	            					'$http', function($scope,Upload,$timeout,$http) {

		
		
		$scope.settings = {
	            currentPage: 0,
	            offset: 0,
	            pageLimit: 10,
	            pageLimits: ['10', '50', '100']
	          };

		 $scope.getTotalPages = function () {
			return Math.ceil($scope.guestReviewCount[0].count / $scope.settings.pageLimit);
		};
			
		 $scope.isCurrentPageLimit = function (value) {
		       return $scope.settings.pageLimit == value;
		 };
		 
		 $scope.previousPage = function () {
			   $scope.settings.currentPage -= 1;
			   var previousOffset = $scope.settings.currentPage * $scope.settings.pageLimit;
		       var url = "get-guest-review?limit="+$scope.settings.pageLimit+"&offset="+previousOffset;
				$http.get(url).success(function(response) {
					//console.log(response);
					$scope.guestreview = response.data;
				}); 
		    }; 
		 
     		$scope.nextPage = function () {
		       $scope.settings.currentPage += 1;
		       var nextOffset = $scope.settings.currentPage * $scope.settings.pageLimit;
		       var url = "get-guest-review?limit="+$scope.settings.pageLimit+"&offset="+nextOffset;
				$http.get(url).success(function(response) {
					//console.log(response);
					$scope.guestreview = response.data;
				}); 
		     }; 
		       
      			$scope.all = function (total) {
		    	   $scope.settings.pageLimit = total;
			       var nextOffset = 0;
			       var url = "get-guest-review?limit="+total+"&offset="+nextOffset;
					$http.get(url).success(function(response) {
						//console.log(response);
						$scope.guestreview = response.data;
					}); 
			    };
			       
      			 $scope.limited = function (limit) {
			    	   $scope.settings.pageLimit = limit;
				       var nextOffset = 0;
				       var url = "get-guest-review?limit="+limit+"&offset="+nextOffset;
						$http.get(url).success(function(response) {
							//console.log(response);
							$scope.guestreview = response.data;
					}); 
		       };			
		
	       	$timeout(function(){
	      	    // $scope.progressbar.complete();
	            $scope.show = true;
	            $("#pre-loader").css("display","none");
	        }, 2000);

			$scope.unread = function() {
			//var notifiurl = "http://localhost:8085/collaborative-workflow/unreadnotifications.action";
			var notifiurl = "unreadnotifications.action";
			$http.get(notifiurl).success(function(response) {
				$scope.latestnoti = response.data;
			});
			};
			
			$scope.getGuestReviews = function() {

				var url = "get-guest-review?limit="+$scope.settings.pageLimit+"&offset="+$scope.settings.offset;
				$http.get(url).success(function(response) {
				    console.log(response);
					$scope.guestreview = response.data;
		
				});
			};
			
			$scope.getGuestReview = function(selectedRowId,selectedPropertyId) {

				var url = "get-guest-review-id?guestReviewId="+selectedRowId+"&reviewPropertyId="+selectedPropertyId;
				$http.get(url).success(function(response) {
					$scope.reviews = response.data;
		
				});
			};
			
			$scope.getProperties=function(){
				var url = "get-all-property";
				$http.get(url).success(function(response) {
					$scope.properties = response.data;
		
				});
			};
			
			$scope.deleteGuestReview=function(rowid){
				var fdata = "&guestReviewId="+ rowid
				var deleteLocationCheck=confirm("Do you want to delete the guest review?");
				if(deleteLocationCheck){
					$http(
							{
								method : 'POST',
								data : fdata,
								headers : {
									'Content-Type' : 'application/x-www-form-urlencoded'
								},
								url: 'delete-guest-review'
							}).then(function successCallback(response) {
								window.location.reload();
							   }, function errorCallback(response) {
								
								
							});
				}
				else{
					
				}
				
			};
			
			$scope.getGuestReviewCount = function() {

				var url = "get-guest-review-count";
				$http.get(url).success(function(response) {
				    //console.log(response);
					$scope.guestReviewCount = response.data;
				});
			};
			
			$scope.getReviewer = function(){
				var url = "get-reviewer";
				$http.get(url).success(function(response) {
				    //console.log(response);
					$scope.reviewer = response.data;
				});
			};
			var spinner = $('#loadertwo');
			$scope.addGuestReviewDetails=function(){
				var fdata="&reviewPropertyId="+$('#reviewPropertyId').val()
				+ "&reviewerId="+$('#reviewerId').val()
				+ "&reviewGuest="+$('#reviewGuest').val()
				+ "&reviewDescription="+$('#reviewDescription').val()
				+ "&guestReviewsCount="+$('#reviewCount').val()
				
				spinner.show();
				$http(
						{
							method : 'POST',
							data : fdata,
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							url : 'add-guest-review'
						}).then(function successCallback(response) {
							setTimeout(function(){ spinner.hide(); 
							  }, 1000);
							 window.location.reload(); 
							 $scope.getGuestReviews();
				}, function errorCallback(response) {
					setTimeout(function(){ spinner.hide(); 
					  }, 1000);
				});
				
			};
			
			$scope.editGuestReviewDetails=function(){
				
				var fdata="&reviewPropertyId="+$('#editReviewPropertyId').val()
				+ "&guestReviewId="+$('#editGuestReviewId').val()
				+ "&reviewerId="+$('#editReviewerId').val()
				+ "&reviewGuest="+$('#editReviewGuest').val()
				+ "&reviewDescription="+$('#editReviewDescription').val()
				+ "&guestReviewsCount="+$('#editReviewCount').val()
				
				spinner.show();
				
				$http(
						{
							method : 'POST',
							data : fdata,
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							url : 'edit-guest-review'
						}).then(function successCallback(response) {
							setTimeout(function(){ spinner.hide(); 
							  }, 1000); 
							window.location.reload(); 
				}, function errorCallback(response) {
					
				});
				
			};
			
			
			$scope.getGuestReviews();
			$scope.getGuestReviewCount();
			$scope.getProperties();
			$scope.getReviewer();
			
	}]);


	app.directive('fileInput', ['$parse', function ($parse) {
	    return {
	        restrict: 'A',
	        link: function (scope, element, attributes) {
	            element.bind('change', function () {
	                $parse(attributes.fileInput)
	                .assign(scope,element[0].files[0])
	                scope.$apply()
	            });
	        }
	    };
	}]);		
		   
		        

		
	</script>
			
	<link rel="stylesheet" href="dist/css/dataTables.bootstrap.css">
	<script src="js1/upload/ng-file-upload-shim.min.js"></script>
	<script src="js1/upload/ng-file-upload.min.js"></script>
	 <script src="js1/jquery.dataTables.min.js" type="text/javascript"></script>
	 <script src="js1/dataTables.bootstrap.min.js" type="text/javascript"></script>
       