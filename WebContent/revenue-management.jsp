<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
   pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="s" uri="/struts-tags"%>
<style>
.canvasjs-chart-credit{display:none!important;}
.btnpd{margin:5px;}
/* .avabg{background-color:#fff;padding:5px;} */
.invcal{background-color:#05253A;color:#fff;padding:5px;border:none;}
.rmname{color:#fff;}
.box-titles{display: inline-block; font-size: 18px; margin: 0; }
.box-title {color:#fff;}
.totalrooms{background: #88ba41 !important; border-color: #5cb85c !important;text-align:center;width:50%;padding:5px;float:left;}
.partnerAvailable{background: #FFFF00  !important; border-color: #5cb85c !important;text-align:center;width:100%;height:0%;padding:5px;float:left;color: #000000}
.soldtext{background-color: #d9534f; border-color: #d43f3a;text-align:center;width:50%;padding:5px;float:right;}
.btn-primary {background: #a9e006 !important ;border-color:#a9e006 !important;color: #ffffff !important;margin:5px;padding:5px;}
.invcal .btn-primary {background:#FFA812 !important; border-color: #FFA812 !important;color: #ffffff !important;margin:5px;padding:5px;font-weight:normal;}
.btndet .totaloccupancy{background:#88ba41 !important;padding:5px;border:none;width:5%;height:5%;}
.btndet .currentoccupancy{background:#ffffff !important;padding:5px;border:none;width:5%;height:5%;}
.btndet .soldoccupancy{background:#d43f3a !important;;padding:5px;border:none;width:5%;height:5%;border:1px solid #fff;}
.btndet .partnerBlock{background:#FFFF00 !important;;padding:5px;border:none;width:5%;height:5%;border:1px solid #fff;}
.btndet label{color:#ffffff !important;font-weight:normal!important;display:block;}
.btnsaveprice{background-color:#05253A ;border-color:#05253A ;}
.chartlayout{width:100%;height:500px;}
</style>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- text json -->
   <!-- Content Header (Page header) -->

   <section class="content-header">
      <h1>
         Room Management
      </h1>
 
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <div class="col-xs-12">
            <div class="nav-tabs-custom">
               <ul class="nav nav-tabs">
                  <li class="active"><a href="#fa-icons-offer" data-toggle="tab">Room Management</a></li>
                  <li><a href="#tab-price" data-toggle="tab">Price Management</a></li>
<!--                   <li><a href="#tab-max-min-price" data-toggle="tab">Automatic Pricing</a></li> -->
                  <li><a href="#tab-offer" data-toggle="tab">Offer Management</a></li>
                  <li><a href="#tab-chart" data-toggle="tab">Revenue Chart</a></li>
<!--                   <li><a href="#tab-block-report" data-toggle="tab">Partner Inventory Block Report</a></li> -->
                <!--   <li><a href="#tab-logs" data-toggle="tab">Logs</a></li> -->
               </ul>
               
               <div class="tab-content">
                  <!-- Font Awesome Icons -->
                  <div class="tab-pane active" id="fa-icons-offer">
                     <section class="content">
				    <div class="box box-default color-palette-box">
				        <div class="box-header ">
				          <h3 class="box-titles"><i class="fa fa-tag"></i> Room Update</h3>
				        </div>
				        <div class="box-body">
				                <form name="searchRate">
						            <div class="col-sm-3 col-md-3">
						               <div class="form-group">
						                  <label>Select Accommodation Type :</label>
						                  <select name="propertyAccommodationId" id="propertyAccommodationId" value="" ng-model="propertyAccommodationId" class="form-control" ng-required="true">
						                     <option value="">Choose Accommodation</option>
						                     <option ng-repeat="at in accommodations" value="{{at.accommodationId}}">{{at.accommodationType}}</option>
						                  </select>
						               </div>
						            </div>
						            <div class="col-sm-3 col-md-3">
						               <div class=" form-group">
						                  <label>Start Date :</label>
						                  <div class="input-group date">
						                     <label class="input-group-addon btn" for="StartDate">
						                     <span class="fa fa-calendar"></span>
						                     </label>   
						                     <input type="text"  id="StartDate" class="form-control" name="StartDate"  value="" ng-required="true" autocomplete="off">
						                  </div>
						               </div>
						            </div>
						            <div class="col-sm-3 col-md-3">
						               <div class="form-group">
						                  <label >End Date :</label>
						                  <div class="input-group date">
						                     <label class="input-group-addon btn" for="endDate">
						                     <span class="fa fa-calendar"></span>
						                     </label>   
						                     <input type="text" id="endDate" class="form-control" name="endDate" value="" ng-required="true" autocomplete="off"> 
						                  </div>
						               </div>
						            </div>
						            <div class="col-sm-3 col-md-3">
						            	              
						            <label >Search or Modify:</label>
						             <div class="form-group">
						             <button type="submit" ng-click="getBulkBlockInventory()"  class="btn btn-primary "  ><i class="fa fa-search-plus" aria-hidden="true"></i> Search</button> <!-- ng-disabled="searchRate.$invalid" -->
						               <a class="btn btn-danger " href="#bulkupdate" ng-click=""  data-toggle="modal"  >Bulk Update</a>
						            
						               </div>
						            </div>
						             <div class="col-sm-12 col-md-12">
						               <span id="roompromoalert"></span>
						               </div>
						        
						         </form>
						        </div>
						        <!-- /.box-body -->
						              
						      
						      <div class="row avabg">
						        <div class="col-xs-12 ">
						          <div class="box invcal">
						            <div class="box-header">
						              <h3 class="box-title">Available Inventory</h3>
						
						              
						            </div>
									      <div class="row">
									         <div class="col-md-12 " >
									                  <div class="row ">
									               <div class="col-md-6 text-center"><button class="btnnextdays" ng-click="datePrevious()"><i class="fa fa-arrow-left" aria-hidden="true"></i> Previous 10 Days {{roomCount.roomName }}</button></div>
									               <div class="col-md-6 text-center"><button  class="btnnextdays" ng-click="dateNext()">Next 10 Days <i class="fa fa-arrow-right" aria-hidden="true"></i></button></div>
									        </div>
									            <div class="table-responsive " ng-repeat="rc in blockinventory |limitTo :1"> 
					                              <table width="100%" class="table table-bordered ">
					                                    <tbody>
												         <tr>
												            <td ><span class="font14 roomtype-text">Room Type</span></td>
												             <td ng-repeat="dd in rc.roomAvailable | limitTo:filterLimit track by $index" class="text-center" id="tlastchild{{$index}}"> {{dd.date}}</td>
												        </tr>
				                                        <tr  ng-repeat="rc in blockinventory" >
				                                            <td >
				                                                <div class="pad_left_n col-md-7 col-xs-11">
				                                                    <h5 class="text-left rmname">{{ rc.accommodationType }}</h5></div>
				                             
				                                            </td>
				                                            <td ng-repeat="dd in rc.roomAvailable | limitTo:filterLimit track by $index">
				                                            
				                                            <input type="text" id="roomId{{$index}}" style="text-align:center;width:100%;height:0%" name="roomId{{$index}}" ng-change="singleBlockInventory(dd.available,dd.date,rc.accommodationId,dd.totalRooms,dd.totalAvailable,dd.soldRooms,$index)" ng-model="dd.available"  ng-model-onblur class="form-control avatext"/>
				                                             <div class="roomview"><span ng-disabled="true" class="soldtext">{{dd.sold}}</span><span class="totalrooms">{{dd.totalRoomCount}}</span><span class="partnerAvailable" title="last update:  {{dd.blockInventoryDate}}">{{dd.blockInventoryCount}}</span></div>
				                                           	 <input type="text" ng-model="dd.occupancy" style="background-color:#05253A ;border-top-color:#05253A ;text-align:center;color:white;width:100%;height:0%" ng-model-onblur class="form-control avatext " readonly/>	
				                                            <%-- <span class="soldbtn"><button class="btn-danger">{{dd.sold}}</button></span> --%>
				                                            </td>
				                                          
				                                        </tr>
						                                   
						                               </tbody>
						                                </table>
										              <div class="col-md-12 text-center"><button  class="btn-primary pull-right btnsaveprice" id="disableUR" ng-click="updateRooms()"><i class="fa fa-floppy-o" aria-hidden="true"></i> {{updateRoomsinv}}</button></div>
										                         <div class="row btndet">
									        <div class="col-md-3 "><button class="currentoccupancy"></button><label>Current Occupancy</label></div>
									        <div class="col-md-3 "><button class="totaloccupancy"></button><label>Total Occupancy</label></div>
									             <div class="col-md-3 "><button class="soldoccupancy"></button><label>Sold out</label></div>
									             <div class="col-md-3 "><button class="partnerBlock"></button><label>Partner Block</label></div>
									            </div>                  
										            </div>
									          </div>
									      </div>
						            <!-- /.box-body -->
						          </div>
						          <!-- /.box -->
						        </div>
						      </div>
						      
						      </div>
		
		    		</section>
		    		<div class="modal" id="bulkupdate">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" id="btnclose" class="close" data-dismiss="modal"
										aria-hidden="true">x</button>
									<h4 class="modal-title">Bulk Update</h4>
								</div>
							 <form name="addBulkBlockForm" id="addBulkBlock">
								<div class="modal-body" >
		                          <div class="form-group col-md-12">
							      <label>Select Accommodation Type :</label>
									<select  name="accommodationId" id="accommodationId" ng-model="accommodationId" value="" class="form-control" ng-required="true">
									<option style="display:none" value="">Select Your  Accommodation</option>
			                       	<option ng-repeat="at in accommodations" value="{{at.accommodationId}}">{{at.accommodationType}}</option>
			                        </select>
									 </div>
									<div class=" form-group col-md-4">
										<label>Start Date :</label>
										 <div class="input-group date" >
						                   <label class="input-group-addon btn" for="bookCheckin">
						                   <span class="fa fa-calendar"></span>
						                  </label>   
						                  <input type="text" id="bookCheckin" class="form-control" name="bookCheckin" value="" ng-required="true" autocomplete="off">
						                </div>
									</div>
													
									<div class="form-group col-md-4">
										<label >End Date :</label>
									      <div class="input-group date">
						                   <label class="input-group-addon btn" for="bookCheckout">
						                   <span class="fa fa-calendar"></span>
						             	 </label>   
						                  <input type="text" id="bookCheckout" class="form-control" name="bookCheckout"  value="" ng-required="true" autocomplete="off"> 
						                </div>
									</div>
									<div class="form-group col-md-4">
										<label >Inventory Available :</label>
									      <div class="input-group date">
						             
						                  <input type="text" id="blockInventoryCount" class="form-control" name="blockInventoryCount"  value="" ng-required="true" autocomplete="off"> 
						
						                </div>
									     
									</div>
			
					
								<div class="modal-footer">
								
								<span id="roominventorypromoalert"></span>
			<!-- 						<a href="#" data-dismiss="modal" class="btn">Close</a> -->
									<a href="#" data-dismiss="modal" class="btn btn-danger">Close</a>
									<button ng-click="addBulkBlockForm.$valid && verifyBlockInventory()" ng-disabled="addBulkBlockForm.$invalid" class="btn btn-primary">{{bulkroomupdate}}</button>
								</div>
							</div>
							</form>
							</div>
					</div>
				<!-- /.modal-content -->
			</div>
                  </div>
                  <div class="tab-pane " id="tab-logs">
                  	 <form method="post" theme="simple" name="offers">
                        <section id="new">
	                   		<ul class="nav nav-tabs">
			                  <li class="active"><a href="#fa-icons-logs-inventory" data-toggle="tab">Inventory</a></li>
			                  <li ><a href="#tab-logs-price" data-toggle="tab">Price</a></li>
			                  <li ><a href="#tab-logs-promotion" data-toggle="tab">Promotion</a></li>
			                </ul>
                        </section>
                     </form>
                     <div class="tab-content">
	                     	<div class="tab-pane active" id="fa-icons-logs-inventory">
		                  	<section class="content">
							    <div class="box box-default color-palette-box">
							    	<div class="box-header ">
							    		<div class="form-group col-md-4"><h3 class="box-titles">Inventory Logs</h3></div>
							    		<div class="form-group col-md-4"></div>
							          	<div class="form-group col-md-4">
											  <label>Select Property :</label>
											  <select  name="inventoryPropertyId" id="inventoryPropertyId" ng-model="inventoryPropertyId" ng-required="true" ng-change="getAllLogInventoryProperty()" class="form-control" >
											  <option disabled selected value> &nbsp;-- Property --&nbsp; </option>
		<!-- 									  <option style="display:none" value="0">All</option> -->
							                  <option ng-repeat="pp in activepromotionproperty" value="{{pp.propertyId}}">{{pp.propertyName}}</option>
							                  </select>
										</div>
							        </div>
							        <form method="post" theme="simple" name="inventoryLogsForm">
							        	<section class="content">
			                  			<div class="box-body table-responsive no-padding" > 
											<table class="table table-hover" id="example1">
												<thead>
												<tr>
													<th>Order</th>
													<th>Inventory Id</th>
												    <th>Blocked Date</th>
												   	<th>Accommodation Type</th>
												   	<th>Room Inventory</th>
												   	<th>Created Date</th>
												   	<th>Created By</th>
												</tr>
												</thead>
												<tbody class="sortable">
													<tr ng-repeat="li in allInventoryLogs track by $index"  >
														<td>{{li.serialNo}}</td>
														<td>{{li.inventoryId}}</td>
														<td>{{li.blockedDate}}</td>
														<td>{{li.accommodationType}}</td>
														<td>{{li.inventoryCount}}</td>
														<td>{{li.createdDate}}</td>
														<td>{{li.createdBy}}</td>										
														 
													</tr>
												</tbody>
											</table>
										</div>
						                </section>
			                  		</form>
							    </div>
							    </section>
		                  </div>
		                  <div class="tab-pane" id="tab-logs-price">
		                  	<section class="content">
							    <div class="box box-default color-palette-box">
							    	<div class="box-header ">
							    		<div class="form-group col-md-4"><h3 class="box-titles">Pricing Logs</h3></div>
							    		<div class="form-group col-md-4"></div>
							          	<div class="form-group col-md-4">
											  <label>Select Property :</label>
											  <select  name="ratePropertyId" id="ratePropertyId" ng-model="ratePropertyId" ng-required="true" ng-change="getAllLogRatesProperty()" class="form-control" >
											  <option disabled selected value> &nbsp;-- Property --&nbsp; </option>
		<!-- 									  <option style="display:none" value="0">All</option> -->
							                  <option ng-repeat="pp in activepromotionproperty" value="{{pp.propertyId}}">{{pp.propertyName}}</option>
							                  </select>
										</div>
							        </div>
							        <form method="post" theme="simple" name="priceLogsForm">
							        	<section class="content">
			                  			<div class="box-body table-responsive no-padding"> 
											<table class="table table-hover" id="example1">
												<thead>
												<tr>
													<th>Order</th>
													<th>Rate Id</th>
												    <th>Start Date</th>
												   	<th>End Date</th>
												   	<th>Accommodation Type</th>
												   	<th>Tariff</th>
												   	<th>Source Type</th>
												   	<th>Created Date</th>
												   	<th>Created By</th>
												</tr>
												</thead>
												<tbody class="sortable">
													<tr ng-repeat="rl in allRateLogs track by $index"  >
														<td>{{rl.serialNo}}</td>
														<td>{{rl.propertyRateId}}</td>
													    <td>{{rl.startDate}}</td>
														<td>{{rl.endDate}}</td>
														<td>{{rl.accommodationType}}</td>
														<td>{{rl.baseAmount}}</td>
														<td>{{rl.sourceType}}</td>										
														<td>{{rl.createdDate}}</td>
														<td>{{rl.createdBy}}</td>
													</tr>
												</tbody>
											</table>
										</div>
						                </section>
			                  		</form>
							    </div>
							    </section>
		                  </div>
		                  <div class="tab-pane" id="tab-logs-promotion">
		                  	<section class="content">
							    <div class="box box-default color-palette-box">
							    	<div class="box-header ">
							    		<div class="form-group col-md-4"><h3 class="box-titles">Promotion Logs</h3></div>
							    		<div class="form-group col-md-4"></div>
							          	<div class="form-group col-md-4">
											  <label>Select Property :</label>
											  <select  name="promotionPropertyId" id="promotionPropertyId" ng-model="promotionPropertyId" ng-required="true" ng-change="getAllLogPromotionProperty()" class="form-control" >
											  <option disabled selected value> &nbsp;-- Property --&nbsp; </option>
		<!-- 									  <option style="display:none" value="0">All</option> -->
							                  <option ng-repeat="pp in activepromotionproperty" value="{{pp.propertyId}}">{{pp.propertyName}}</option>
							                  </select>
										</div>
							        </div>
							        <form method="post" theme="simple" name="promotionLogsForm">
							        	<section class="content">
			                  			<div class="box-body table-responsive no-padding" > 
											<table class="table table-hover" id="example1">
												<thead>
												<tr>
													<th>Order</th>
													<th>Promotion Id</th>
												    <th>Start Date</th>
												   	<th>End Date</th>
												   	<th>Discount</th>
												   	<th>Created Date</th>
												   	<th>Created By</th>
												</tr>
												</thead>
												<tbody class="sortable">
													<tr ng-repeat="pl in allPromotionLogs track by $index"  >
														<td>{{pl.serialno}}</td>
														<td>{{pl.promotionId}}</td>
													    <td>{{pl.startDate}}</td>
														<td>{{pl.endDate}}</td>
														<td>{{pl.promotionName}}</td>
														<td>{{pl.createdDate}}</td>
														<td>{{pl.createdBy}}</td>										
													</tr>
												</tbody>
											</table>
										</div>
						                </section>
			                  		</form>
							    </div>
							    </section>
		                  </div>
                     </div>
                  </div>
                  <!-- s -->
                  <div class="tab-pane " id="tab-offer">
                  		<form method="post" theme="simple" name="offers">
                        <section id="new">
	                   		<ul class="nav nav-tabs">
<!-- 			                  <li class="active"><a href="#fa-icons-promotion" data-toggle="tab">Last Minute Promotions</a></li> -->
			                  <li class="active"><a href="#tab-flat" data-toggle="tab">Flat Promotions</a></li>
			                  <li ><a href="#tab-early" data-toggle="tab">Early Bird Promotions</a></li>
<!-- 			                  <li ><a href="#tab-all-active" data-toggle="tab">List of Promotions</a></li> -->
			                  <li ><a href="#tab-active-promotion" data-toggle="tab">Active Promotions</a></li>
			                </ul>
                        </section>
                     </form>
                     <div class="tab-content">
                     <div class="tab-pane" id="fa-icons-promotion">
	                     <section class="content">
						    <div class="box box-default color-palette-box">
						        <div class="box-header ">
						          <h3 class="box-titles">Last Minute Promotion</h3>
						        </div>
		                     	<form method="post" theme="simple" id="lastMinutePromotionForm" name="lastMinutePromotionForm" >
				                  	<section class="content">
				                  			<div class="form-group col-md-3">
									  <label>Select Property :</label>
									  <select  name="lastPropertyId" id="lastPropertyId" ng-model="lastPropertyId" ng-required="true" ng-change="getPropertyAccommodations()" class="form-control" >
									  <option disabled selected value> &nbsp;-- Property --&nbsp; </option>
<!-- 									  <option style="display:none" value="0">All</option> -->
					                  <option ng-repeat="pp in promotionproperty" value="{{pp.propertyId}}">{{pp.propertyName}}</option>
					                  </select>
									</div>
				                  	<div class=" form-group col-md-3">
										 <label>Start Date :</label>
										 <div class="input-group date">
						                     <label class="input-group-addon btn" for="lastStartDate">
						                     <span class="fa fa-calendar"></span>
						                     </label>   
						                     <input type="text" id="lastStartDate" class="form-control" name="lastStartDate"  value="" onkeypress="return false;" ng-required="true" placeholder="Start Date" autocomplete="off">
					                	</div>
									</div>
									<div class="form-group col-md-3">
										<label >End Date :</label>
									    <div class="input-group date">
					                	  	<label class="input-group-addon btn" for="lastEndDate">
					                   		<span class="fa fa-calendar"></span>
					              			</label>   
					                  		<input type="text" id="lastEndDate" class="form-control" name="lastEndDate"  value="" onkeypress="return false;" ng-required="true" placeholder="End Date" autocomplete="off"> 
					    	            </div>
									</div>
								
									<div class="form-group col-md-3">
										<label>Promotion Name :</label>
										<input type="text" class="form-control"  name="lastPromotionName"  value=""  ng-model='lastPromotionName' id="lastPromotionName" ng-required="true" placeholder="Promotion Name">
										<span ng-show="lastMinutePromotionForm.lastPromotionName.$error.pattern" style="color:red">Not a valid Name!</span>
									</div>
									<div class="form-group col-md-3">
										  <label>Select Hours Range :</label>
										  <select name="lastHoursRangeId" ng-model="lastHoursRangeId" id="lastHoursRangeId" ng-required="true" ng-change="" class="form-control" >
										  	<option disabled selected value> -- Ranges -- </option>
<!-- 										   	<option value="All">All</option> -->
						                  	<option value="0-8">Before Check-In 8 Hours</option>
						                  	<option value="8-16">Before Check-In between 16 to 8 Hours</option>
						                  	<option value="16-24">Before Check-In between 24 to 16 Hours</option>
						                  	<option value="24-32">Before Check-In between 32 to 24 Hours</option>
						                  	<option value="32-40">Before Check-In between 40 to 32 Hours</option>
						                  	<option value="40-48">Before Check-In between 48 to 40 Hours</option>
						                  </select>
									</div>
									
								
									<div class="form-group col-md-3">
										  <label>Select Discount Type :&nbsp;</label>
										  <br>
										  <input type="radio" name="lastType" ng-checked="false" id="lastType" value="Percent" ng-click="selectLastDiscount('percent')"> &nbsp; Percentage &nbsp;
										  <input type="radio" name="lastType" ng-checked="false" id="lastType" value="INR" ng-click="selectLastDiscount('inr')"> &nbsp; INR &nbsp; 
									</div>
									<div class="form-group col-md-3" id="lastPercentId">
										<label>Discount :</label>
										<input type="number" class="form-control"  name="lastPercentAmount"  id="lastPercentAmount" ng-model="lastPercentAmount" ng-pattern="/^[0-9]/" placeholder="Percentage">
										<span ng-show="lastMinutePromotionForm.lastPercentAmount.$error.pattern" style="color:red">Not a valid Number!</span>
									</div>
									<div class="form-group col-md-3" id="lastInrId">
										<label>Discount :</label>
										<input type="text" class="form-control"  name="lastInrAmount"  id="lastInrAmount" ng-model="lastInrAmount" ng-pattern="/^[0-9]/" placeholder="INR">
										<span ng-show="lastMinutePromotionForm.lastInrAmount.$error.pattern" style="color:red">Not a valid Number!</span>
									</div>
									<!-- <div ng-if="accommodations.length>0">
										
									</div> -->
									<div class="form-group col-md-12" id="lastcheckedaccommodations">
											<label>Accommodation Type :</label>
										   <div class="input-group" >
				
										   <label class="checkbox-inline"><input type="checkbox"  id="accommodationCheckAll" name="accommodationCheckAll" value="0">ALL</label>
				                              <label class="checkbox-inline" ng-repeat="at in promotionaccommodations">
				                     			 <input type="checkbox" name="checkedAccommodation[]"  id="checkedAccommodation" value="{{at.accommodationId}}"  class="flat-red" >&nbsp {{at.accommodationType}}
				                   			  </label>
				                    				
										   </div>
										</div>
									
									<div class="form-group col-md-12" id="lastcheckeddays">
											<label>Days of week :</label>
										   <div class="input-group" >
				
										   <label class="checkbox-inline"><input type="checkbox"  id="lastCheckAll" name="lastCheckAll" value="0">ALL</label>
				                              <label class="checkbox-inline" ng-repeat="d in days">
				                      				<input type="checkbox" name="checkedDay[]"  id="checkedDay" value="{{d.dayId}}"  class="flat-red" >&nbsp {{d.daysOfWeek}}
				                    			</label>
				                    				
										   </div>
									</div>
									
					             
					                      <div class="row fontawesome-icon-list">
					                      	<div class="col-md-12 col-sm-12">
					                           <div id="lastpromoalert"></div>
					                         </div>
					                        <div class="col-md-3 col-sm-4"></div>
					                        <div class="col-md-3 col-sm-4"></div>
					                        <div class="col-md-3 col-sm-4"></div>
					                        <div class="col-md-3 col-sm-4">
					                        	<button type="submit" ng-click="lastMinutePromotionForm.$valid && addGetLastMinutePromotions()" id="lastminute" ng-disabled="lastMinutePromotionForm.$invalid" class="btn btn-primary btngreeen pull-right">{{lastminutenote}}</button>
					                        </div>
					                     </div>
					               </section>
				                 </form>
				              </div>
	                     </section>
	                 </div>
	                 <div class="tab-pane active" id="tab-flat">
	                 	<section class="content">
						    <div class="box box-default color-palette-box">
						    	<div class="box-header ">
						          <h3 class="box-titles">Flat Promotion</h3>
						        </div>
			                  	<form method="post" theme="simple" name="flatPromotionForm" id="flatPromotionForm">
			                  	<section class="content">
				                  	<div class=" form-group col-md-3">
										 <label>Start Date :</label>
										 <div class="input-group date">
						                     <label class="input-group-addon btn" for="flatStartDate">
						                     <span class="fa fa-calendar"></span>
						                     </label>   
						                     <input type="text" id="flatStartDate" class="form-control" name="flatStartDate"  value="" onkeypress="return false;" ng-required="true" placeholder="Start Date" autocomplete="off">
					                	</div>
									</div>
									<div class="form-group col-md-3">
										<label >End Date :</label>
									    <div class="input-group date">
					                	  	<label class="input-group-addon btn" for="flatEndDate">
					                   		<span class="fa fa-calendar"></span>
					              			</label>   
					                  		<input type="text" id="flatEndDate" class="form-control" name="flatEndDate"  value="" onkeypress="return false;" ng-required="true" placeholder="End Date" autocomplete="off"> 
					    	            </div>
									</div>
									<div class="form-group col-md-3">
										<label>Promotion Name :</label>
										<input type="text" class="form-control"  name="flatPromotionName"  value=""  ng-model='flatPromotionName' id="flatPromotionName" ng-required="true" placeholder="Promotion Name">
										<span ng-show="flatPromotionForm.flatPromotionName.$error.pattern" style="color:red">Not a valid Name!</span>
									</div>
									
									<div class="form-group col-md-3">
									  <label>City :</label>
									  <select  name="flatLocationId" id="flatLocationId" ng-model="flatLocationId" ng-required="true" ng-change="getFlatAreas()" class="form-control" >
									  <option disabled selected value> &nbsp;-- Select City --&nbsp; </option>
									  <option style="display:none" value="0">All</option>
					                  <option ng-repeat="l in locations" value="{{l.googleLocationId}}">{{l.googleLocationDisplayName}}</option>
					                  </select>
									</div>
									<div class="form-group col-md-3">
										  <label>Area :</label>
										  <select name="flatAreaId" ng-model="flatAreaId" id="flatAreaId" ng-required="true" ng-change="getFlatAreaProperties()" class="form-control" >
										 	 <option disabled selected value> &nbsp;-- Select Area --&nbsp; </option>
										   	<option style="display:none" value="0">All</option>
						                  	<option ng-repeat="ar in flatareas" value="{{ar.DT_RowId}}">{{ar.googleAreaDisplayName}}</option>
						                  </select>
									</div>
									<div class="form-group col-md-3">
										  <label>Select Discount Type :&nbsp;</label>
										  <br>
										  <input type="radio"  name="flatType" ng-checked="false" id="flatType" value="Percent" ng-click="selectFlatDiscount('percent')"> &nbsp; Percentage &nbsp;
										  <input type="radio"  name="flatType" ng-checked="false" id="flatType" value="INR" ng-click="selectFlatDiscount('inr')"> &nbsp; INR &nbsp; 
									</div>
									<div class="form-group col-md-3" id="flatPercentId">
										<label>Discount :</label>
										<input type="number" class="form-control"  name="flatPercentAmount"  id="flatPercentAmount" ng-model="flatPercentAmount" ng-pattern="/^[0-9]/" placeholder="Percentage">
										<span ng-show="flatPromotionForm.flatPercentAmount.$error.pattern" style="color:red">Not a valid Number!</span>
									</div>
									<div class="form-group col-md-3" id="flatInrId">
										<label>Discount :</label>
										<input type="text" class="form-control"  name="flatInrAmount"  id="flatInrAmount" ng-model="flatInrAmount" ng-pattern="/^[0-9]/" placeholder="INR">
										<span ng-show="flatPromotionForm.flatInrAmount.$error.pattern" style="color:red">Not a valid Number!</span>
									</div>
									
									<div ng-if="flatAreaProperty.length>0">
										<div class="form-group col-md-12" id="flatProperties">
											<label>List of Properties :</label>
										   	<div class="input-group" >
<!-- 											   	<label class="checkbox-inline"><input type="checkbox"  id="flatPropertyAll" name="flatPropertyAll" value="0">ALL</label> -->
					                           <label class="checkbox-inline" ng-repeat="ap in flatAreaProperty">
					                 					<input type="checkbox" name="checkedProperty[]"  id="checkedProperty" value="{{ap.propertyId}}"  class="flat-red" >&nbsp {{ap.propertyName}}
					               				</label>
										   	</div>
										</div>
									</div>
									<div class="form-group col-md-12" id="flatcheckeddays">
											<label>Days of week :</label>
										   <div class="input-group" >
				
										   	<label class="checkbox-inline"><input type="checkbox"  id="flatCheckAll" name="flatCheckAll" ng-model="selectedFlatAllDays" ng-click="selectFlatAllDays()" value="0">ALL</label>
				                         
				                           <label class="checkbox-inline" ng-repeat="fd in flatdays">
				                 					<input type="checkbox" name="checkedDay[]"  id="checkedDay" value="{{fd.dayId}}"  ng-model="fd.Selected" ng-click="checkIfFlatAllSelectedDays()" class="flat-red" >&nbsp {{fd.daysOfWeek}}
				               				</label>
				               			
										   </div>
				
									</div>
						
				                    <div class="row fontawesome-icon-list">
				                    	<div class="col-md-12 col-sm-12">
				                           <div id="flatpromoalert"></div>
				                         </div>
				                        <div class="col-md-3 col-sm-4"></div>
				                        <div class="col-md-3 col-sm-4"></div>
				                        <div class="col-md-3 col-sm-4"></div>
				                        <div class="col-md-3 col-sm-4">
				                        	<button type="submit" ng-click="flatPromotionForm.$valid && addGetFlatPromotions()" id="flatpromo" ng-disabled="flatPromotionForm.$invalid" class="btn btn-primary btngreeen pull-right">{{flatpromonote}}</button>
				                        </div>
				                     </div>
			             			</section>
		                 		 </form>
		                 	</div>
		                 </section>
	                  </div>
	                  <div class="tab-pane" id="tab-early">
	                  		<section class="content">
						    <div class="box box-default color-palette-box">
						    	<div class="box-header ">
						          <h3 class="box-titles">Early Bird Promotion</h3>
						        </div>
				                  	<form method="post" theme="simple" name="earlyPromotionForm" id="earlyPromotionForm">
				                  	<section class="content">
									<div class=" form-group col-md-3">
										 <label>Booking Period Start :</label>
										 <div class="input-group date">
						                     <label class="input-group-addon btn" for="earlyBirdBookingStartDate">
						                     <span class="fa fa-calendar"></span>
						                     </label>   
						                     <input type="text" id="earlyBirdBookingStartDate" class="form-control" name="earlyBirdBookingStartDate"  value="" onkeypress="return false;" ng-required="true" placeholder="Start Date" autocomplete="off">
					                	</div>
									</div>
									<div class="form-group col-md-3">
										<label >Booking Period End :</label>
									    <div class="input-group date">
					                	  	<label class="input-group-addon btn" for="earlyBirdBookingEndDate">
					                   		<span class="fa fa-calendar"></span>
					              			</label>   
					                  		<input type="text" id="earlyBirdBookingEndDate" class="form-control" name="earlyBirdBookingEndDate"  value="" onkeypress="return false;" ng-required="true" placeholder="End Date" autocomplete="off"> 
					    	            </div>
									</div>
									<div class=" form-group col-md-3">
										 <label>Stay Period Start :</label>
										 <div class="input-group date">
						                     <label class="input-group-addon btn" for="earlyBirdStayStartDate">
						                     <span class="fa fa-calendar"></span>
						                     </label>   
						                     <input type="text" id="earlyBirdStayStartDate" class="form-control" name="earlyBirdStayStartDate"  value="" onkeypress="return false;" ng-required="true" placeholder="Start Date" autocomplete="off">
					                	</div>
									</div>
									<div class="form-group col-md-3">
										<label >Stay Period End :</label>
									    <div class="input-group date">
					                	  	<label class="input-group-addon btn" for="earlyBirdStayEndDate">
					                   		<span class="fa fa-calendar"></span>
					              			</label>   
					                  		<input type="text" id="earlyBirdStayEndDate" class="form-control" name="earlyBirdStayEndDate"  value="" onkeypress="return false;" ng-required="true" placeholder="End Date" autocomplete="off"> 
					    	            </div>
									</div>
									<div class="form-group col-md-3">
										<label>Promotion Name :</label>
										<input type="text" class="form-control"  name="earlyPromotionName"  value=""  ng-model='earlyPromotionName' id="earlyPromotionName" ng-required="true" placeholder="Promotion Name">
										<span ng-show="earlyPromotionForm.earlyPromotionName.$error.pattern" style="color:red">Not a valid Name!</span>
									</div>
									<div class="form-group col-md-3">
									  <label>City :</label>
									  <select  name="earlyLocationId" id="earlyLocationId" ng-model="earlyLocationId" ng-required="true" ng-change="getEarlyAreas()" class="form-control" >
									  <option disabled selected value> &nbsp;-- Select City --&nbsp; </option>
									  <option style="display:none" value="0">All</option>
					                  <option ng-repeat="l in locations" value="{{l.googleLocationId}}">{{l.googleLocationDisplayName}}</option>
					                  </select>
									</div>
									<div class="form-group col-md-3">
										  <label>Area :</label>
										  <select name="earlyAreaId" ng-model="earlyAreaId" id="earlyAreaId" ng-required="true" ng-change="getEarlyAreaProperties()" class="form-control" >
										  	<option disabled selected value> &nbsp;--Select Area --&nbsp; </option>
										   	<option style="display:none" value="0">All</option>
						                  	<option ng-repeat="ar in earlyareas" value="{{ar.DT_RowId}}">{{ar.googleAreaDisplayName}}</option>
						                  </select>
									</div>
									
									
									<div class="form-group col-md-4">
										  <label>Select Discount Type :&nbsp;</label>
										  <input type="radio"  name="earlyType" ng-checked="false" id="earlyType" value="Percent" ng-click="selectEarlyDiscount('percent')"> &nbsp; Percentage &nbsp;
										  <input type="radio"  name="earlyType" ng-checked="false" id="earlyType" value="INR" ng-click="selectEarlyDiscount('inr')"> &nbsp; INR &nbsp; 
									</div>
									<div class="form-group col-md-2" id="earlyPercentId">
										<label>Discount :</label>
										<input type="number" class="form-control"  name="earlyPercentAmount" id="earlyPercentAmount" ng-model="earlyPercentAmount" ng-pattern="/^[0-9]/" placeholder="Percentage">
										<span ng-show="earlyPromotionForm.earlyPercentAmount.$error.pattern" style="color:red">Not a valid Number!</span>
									</div>
									<div class="form-group col-md-2" id="earlyInrId">
										<label>Discount :</label>
										<input type="text" class="form-control"  name="earlyInrAmount"  id="earlyInrAmount" ng-model="earlyInrAmount" ng-pattern="/^[0-9]/" placeholder="INR">
										<span ng-show="earlyPromotionForm.earlyInrAmount.$error.pattern" style="color:red">Not a valid Number!</span>
									</div>
									<div ng-if="earlyAreaProperty.length>0">
										<div class="form-group col-md-12" id="earlyProperties">
											<label>List of Properties :</label>
										    <div class="input-group" >
<!-- 											   	<label class="checkbox-inline"><input type="checkbox"  id="earlyPropertyAll" name="earlyPropertyAll" value="0">ALL</label> -->
					                           <label class="checkbox-inline" ng-repeat="ep in earlyAreaProperty">
					                 					<input type="checkbox" name="checkedProperty[]"  id="checkedProperty" value="{{ep.propertyId}}"  class="flat-red" >&nbsp {{ep.propertyName}}
					               				</label>
										    </div>
										</div>
									</div>
									
									<div class="form-group col-md-12" id="earlycheckeddays">
										<label>Days of week :</label>
										<div class="input-group" >
											<label class="checkbox-inline"><input type="checkbox"  id="earlyCheckAll" name="earlyCheckAll" ng-model="selectedEarlyAllDays" ng-click="selectEarlyAllDays()" value="0">ALL</label>
					                    	<label class="checkbox-inline" ng-repeat="ed in earlydays">
					                 		    <input type="checkbox" name="earlyCheckedDay[]"  id="earlyCheckedDay" value="{{ed.dayId}}" ng-model="ed.Selected" ng-click="checkIfEarlyAllSelectedDays()"  class="flat-red" >&nbsp {{ed.daysOfWeek}}
					               			</label>
										</div>
									</div>

						
				                      <div class="row fontawesome-icon-list">
				                      	<div class="col-md-12 col-sm-12">
				                           <div id="earlypromoalert"></div>
				                         </div>
				                        <div class="col-md-3 col-sm-4"></div>
				                        <div class="col-md-3 col-sm-4"></div>
				                        <div class="col-md-3 col-sm-4"></div>
				                        <div class="col-md-3 col-sm-4">
				                        	<button type="submit" ng-click="earlyPromotionForm.$valid && addGetEarlyPromotions()" id="earlypromo" ng-disabled="earlyPromotionForm.$invalid" class="btn btn-primary btngreeen pull-right">Save</button>
				                        </div>
				                     </div>
				                     </section>
				                  </form>
				            	</div>
				            </section>
	                  </div>
	                  
	                  
	                   <!-- <div class="tab-pane" id="tab-all-active">
		                  <form method="post" theme="simple" name="allPromotionsForm">
		                  </form>
	                  </div> -->
	                  <div class="tab-pane" id="tab-active-promotion">
	                  	<section class="content">
						    <div class="box box-default color-palette-box">
						    	<div class="box-header ">
						    		<div class="form-group col-md-4"><h3 class="box-titles">Active Promotions</h3></div>
						    		<div class="form-group col-md-4"></div>
						          	<div class="form-group col-md-4">
										  <label>Select Property :</label>
										  <select  name="activePropertyId" id="activePropertyId" ng-model="activePropertyId" ng-required="true" ng-change="getActivePromotionProperty()" class="form-control" >
										  <option disabled selected value> &nbsp;-- Property --&nbsp; </option>
	<!-- 									  <option style="display:none" value="0">All</option> -->
						                  <option ng-repeat="pp in activepromotionproperty" value="{{pp.propertyId}}">{{pp.propertyName}}</option>
						                  </select>
									</div>
						        </div>
						        <form method="post" theme="simple" name="activePromotionForm">
						        	<section class="content">
		                  			<div class="box-body table-responsive no-padding" id="activecheckedpromotions" > 
										<table class="table table-hover" id="example1">
											<thead>
											<tr>
												<th>Order</th>
											    <th>Start Date</th>
											   	<th>End Date</th>
											   	<th>Promotion Name</th>
											   	<th>Active /Inactive</th>
											</tr>
											</thead>
											<tbody class="sortable">
												<tr ng-repeat="p in allPromotions track by $index"  >
													<td>{{p.serialno}}</td>
												    <td>{{p.startDate}}</td>
													<td>{{p.endDate}}</td>
													<td>{{p.promotionName}}</td>										
													<td><input type="checkbox" name="checkedPromotions[]"  id="checkedPromotions" value="{{p.promotionId}}" ng-checked ="p.status == 'true'"></td>
													 
												</tr>
											</tbody>
										</table>
									</div>
									<div class="row fontawesome-icon-list">
										<div class="col-md-12 col-sm-12">
				                           <div id="promoalert"></div>
				                         </div>
				                        <div class="col-md-3 col-sm-4"></div>
				                        <div class="col-md-3 col-sm-4"></div>
				                        <div class="col-md-3 col-sm-4"></div>
				                        <div class="col-md-3 col-sm-4">
				                        	<button type="submit" ng-click="getUpdatePromotions()" id="updatePromotions" name="updatePromotions" class="btn btn-primary btngreeen pull-right">{{promoactivatebtn}}</button>
				                        </div>
				                        
					                </div>
					                </section>
		                  		</form>
						    </div>
						    </section>
	                  </div>
                  </div>
                  </div>
                  <div class="tab-pane" id="tab-chart">
                  		<div class="tab-pane" >
		                     <section class="content">
		                     <div class="box box-default color-palette-box chartlayout" >
		                     	<div class="box-header ">
						          <h3 class="box-titles">Revenue Chart</h3>
						        </div>
						        <div class="box-body" >
						        	<div class="col-sm-12 col-md-12" >
						        		<form name="searchChart">
								            <div class="col-sm-3 col-md-3">
								               <div class="form-group">
								                  <label>Select Accommodation Type :</label>
								                  <select name="chartPropertyAccommodationId" id="chartPropertyAccommodationId" value="" ng-model="chartPropertyAccommodationId" class="form-control" ng-required="true">
								                     <option value="">Choose Accommodation</option>
								                     <option ng-repeat="at in accommodations" value="{{at.accommodationId}}">{{at.accommodationType}}</option>
								                  </select>
								               </div>
								            </div>
								            <div class="col-sm-3 col-md-3">
								               <div class=" form-group">
								                  <label>Start Date :</label>
								                  <div class="input-group date">
								                     <label class="input-group-addon btn" for="chartStartDate">
								                     <span class="fa fa-calendar"></span>
								                     </label>   
								                     <input type="text"  id="chartStartDate" class="form-control" name="chartStartDate"  value="" ng-required="true" autocomplete="off">
								                  </div>
								               </div>
								            </div>
								            <div class="col-sm-3 col-md-3">
									               <div class="form-group">
									                  <label >End Date :</label>
									                  <div class="input-group date">
									                     <label class="input-group-addon btn" for="chartEndDate">
									                     <span class="fa fa-calendar"></span>
									                     </label>   
									                     <input type="text" id="chartEndDate" class="form-control" name="chartEndDate" value="" ng-required="true" autocomplete="off"> 
									                  </div>
									               </div>
								               </div>
								               <div class="col-sm-3 col-md-3">
									            	<label >Search:</label>
									             	<div class="form-group">
									             		<button type="submit" id="searchRevenueChart" name="searchRevenueChart" class="btn btn-primary "  ><i class="fa fa-search-plus" aria-hidden="true"></i> Search</button> <!-- ng-disabled="searchRate.$invalid" -->
									               </div>
									            </div>
								             <div class="col-sm-12 col-md-12" >
								             	<span id="chartalert"></span>
								             </div>
								        	 	
								         </form>
						        	</div>
						        	<div class="col-sm-12 col-md-12" >
						        		
						        		<div id="chartContainer" style="height: 300px; width: 100%;"></div>
						        	</div>
			                    	
		                    	</div>
		                     </div>
		                     </section>
		                </div>
		                
                  </div>
                  <div class="tab-pane" id="tab-block-report">
                  		<div class="tab-pane" >
		                     <section class="content">
		                     <div class="box box-default color-palette-box chartlayout" >
		                     	<div class="box-header ">
						          <h3 class="box-titles">Partner Block Report</h3>
						        </div>
						        <div class="box-body" >
						        	<div class="col-sm-12 col-md-12" >
						        		<form name="searchChart">
						        		<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
								               <div class="form-group">
								                  <label>Select Accommodation Type :</label>
								                  <select name="blockAccommodationId" id="blockAccommodationId" value="" ng-model="blockAccommodationId" class="form-control" ng-required="true">
								                     <option value="">Choose Accommodation</option>
								                     <option ng-repeat="at in accommodations" value="{{at.accommodationId}}">{{at.accommodationType}}</option>
								                  </select>
								               </div>
								         </div>
								         <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                  							<h3>
                     						  <div class="input-group add-on dsearch">
                        					    <input type="text" ng-model="search" class="dbsearch form-control" placeholder="Search">
                       						    <div class="input-group-btn">
                         					    <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                        					    </div>
                     						  </div>
                  							</h3>
               							 </div>
                     
                     <div class="table table-responsive" ng-repeat="bc in partnerBlockReport">
                  	 <table class="table  table-bordered">
                     <thead>
                        <tr>
                           <th>inventory Id</th>
                           <th>Inventory Count</th>
                           <th>Blocked Date</th>
                           <th>Created Date</th>
                        </tr>
                     </thead>
                     <tbody>
                        <!--   <div ng-repeat="bc in partnerBlockReport"> -->
                        <tr ng-repeat="bcc in bc.roomAvailable | filter:search track by $index ">
                           <td ><span class="dbbkid">{{bcc.inventoryId}}</span></td>
                           <td >{{bcc.availableCount}}</td>
                           <td> {{bcc.blockedDate}}</td>
                           <td>{{bcc.createdDate}}</td>
                           <!-- customer full detail modal-->
                        </tr>
                        <!-- </div>  -->
                     </tbody>
                  </table>
               </div>
								        	 	
								         </form>
						        	</div>
						        	
			                    	
		                    	</div>
		                     </div>
		                     </section>
		                </div>
		                
                  </div>
                  <!-- t -->
                  <div class="tab-pane " id="tab-max-min-price">
                  		<div class="tab-pane active" id="fa-icons-offer">
		                     <section class="content">
						    <div class="box box-default color-palette-box">
						        <div class="box-header ">
						          <h3 class="box-titles">Minimum Maximum Price</h3>
						        </div>
						        <div class="box-body">
						                <form name="searchRate">
								            <div class="col-sm-4 col-md-3">
								               <div class="form-group">
								                  <label>Select Accommodation Type :</label>
								                  <select name="mmratePropertyAccommodationId" id="mmratePropertyAccommodationId" value="" ng-model="mmratePropertyAccommodationId" class="form-control" ng-required="true">
								                     <option value="">Choose Accommodation</option>
								                     <option ng-repeat="at in accommodations" value="{{at.accommodationId}}">{{at.accommodationType}}</option>
								                  </select>
								               </div>
								            </div>
								            <div class="col-sm-4 col-md-3">
								               <div class=" form-group">
								                  <label>Start Date :</label>
								                  <div class="input-group date">
								                     <label class="input-group-addon btn" for="mmrateStartDate">
								                     <span class="fa fa-calendar"></span>
								                     </label>   
								                     <input type="text"  id="mmrateStartDate" class="form-control" name="mmrateStartDate"  value="" ng-required="true" autocomplete="off">
								                  </div>
								               </div>
								            </div>
								            <div class="col-sm-4 col-md-3">
								               <div class="form-group">
								                  <label >End Date :</label>
								                  <div class="input-group date">
								                     <label class="input-group-addon btn" for="rateEndDate">
								                     <span class="fa fa-calendar"></span>
								                     </label>   
								                     <input type="text" id="mmrateEndDate" class="form-control" name="mmrateEndDate" value="" ng-required="true" autocomplete="off"> 
								                  </div>
								               </div>
								            </div>
											<%-- <div class="col-sm-4 col-md-3">
												<div class="form-group">
													<label>Source Type</label>
													<select  class="form-control" name="sourceTypeId" ng-model="sourceTypeId" id="sourceTypeId" value="" class="form-control " ng-required="true">
													<option value="">Select Source Type</option>
													<option ng-repeat="sr in sourceTypes" value="{{sr.sourceTypeId}}" >{{sr.sourceType}}</option>
													</select>
												</div>
												<input type="hidden" id="sourceTypeId" name="sourceTypeId" class="form-control" value = "">
											</div> --%>
								             <div class="col-sm-12 col-md-12" >
								             	<span id="mmratepromoalert"></span>
								                <button type="submit" ng-click="getMMRateSearch()"  class="btn btn-primary btngreeen pull-right btnpd"  >Search</button> <!-- ng-disabled="searchRate.$invalid" -->
								                <a class="btn btn-danger pull-right btnpd" href="#mmaddmodal" ng-click=""  data-toggle="modal"  >Modify Price</a>
								             </div>
								        	 	
								         </form>
								        </div>
								        <!-- /.box-body -->
								              
								      
								      <div class="row avabg">
								        <div class="col-xs-12 ">
								          <div class="box invcal">
								            <div class="box-header">
								              <h3 class="box-title">Revenue Price</h3>
								
								              
								            </div>
											      <div class="row">
											         <div class="col-md-12 " >
											                  <div class="row ">
											               <div class="col-md-6 text-center"><button class="btnnextdays" ng-click="mmrateDatePrevious()">Previous 10 Days {{roomCount.roomName }}</button></div>
											               <div class="col-md-6 text-center"><button  class="btnnextdays" ng-click="mmrateDateNext()">Next 10 Days</button></div>
											            </div>
											            <div class="table-responsive " ng-repeat="rc in mmratecontrol | limitTo :1"> 
							                              <table width="100%" class="table table-bordered ">
							                                    <tbody>
														         <tr>
														            <td ><span class="font14 roomtype-text">Room Type</span></td>
														             <td ng-repeat="dd in rc.rates | limitTo:filterLimit track by $index" class="text-center" id="tlastchild{{$index}}"> {{dd.date | date:'MMMM dd, yyyy' }}</td>
														        </tr>
						                                        <tr  ng-repeat="rc in mmratecontrol" >
						                                            <td>
						                                                <div class="pad_left_n col-md-7 col-xs-11">
						                                                    <h5 class="text-left rmname">{{ rc.accommodationType }}</h5>
						                                                </div>
						                                            </td>
						                                            <td ng-repeat="dd in rc.rates | limitTo:filterLimit track by $index">
						                                            
																	<input type="text" id="minPrice{{$index}}" name="minPrice{{$index}}" ng-change="singleBlockMMRates(dd.minimumBaseAmount,dd.maximumBaseAmount,dd.date,rc.accommodationId,rc.sourceTypeId,dd.adult,dd.child,rc.netRateRevenue,dd.minimumAmount,dd.maximumAmount,dd.sellRate,dd.otaRate,dd.occupancy,dd.epAmount,dd.cpAmount,dd.ratePlanAmount,dd.ratePlanSymbol,true,$index)" ng-model="dd.minimumBaseAmount"  ng-model-onblur class="form-control avatext"/>
																	<input type="text" id="maxPrice{{$index}}" name="maxPrice{{$index}}" ng-change="singleBlockMMRates(dd.minimumBaseAmount,dd.maximumBaseAmount,dd.date,rc.accommodationId,rc.sourceTypeId,dd.adult,dd.child,rc.netRateRevenue,dd.minimumAmount,dd.maximumAmount,dd.sellRate,dd.otaRate,dd.occupancy,dd.epAmount,dd.cpAmount,dd.ratePlanAmount,dd.ratePlanSymbol,false,$index)" ng-model="dd.maximumBaseAmount"  ng-model-onblur class="form-control avatext"/>
						                                            <input type="text" id="adult{{$index}}" name="adult{{$index}}" ng-change="singleBlockMMRates(dd.minimumBaseAmount,dd.maximumBaseAmount,dd.date,rc.accommodationId,rc.sourceTypeId,dd.adult,dd.child,rc.netRateRevenue,dd.minimumAmount,dd.maximumAmount,dd.sellRate,dd.otaRate,dd.occupancy,dd.epAmount,dd.cpAmount,dd.ratePlanAmount,dd.ratePlanSymbol,true,$index)" ng-model="dd.adult"  ng-model-onblur class="form-control avatext"/>
						                                            <input type="text" id="child{{$index}}" name="child{{$index}}" ng-change="singleBlockMMRates(dd.minimumBaseAmount,dd.maximumBaseAmount,dd.date,rc.accommodationId,rc.sourceTypeId,dd.adult,dd.child,rc.netRateRevenue,dd.minimumAmount,dd.maximumAmount,dd.sellRate,dd.otaRate,dd.occupancy,dd.epAmount,dd.cpAmount,dd.ratePlanAmount,dd.ratePlanSymbol,true,$index)" ng-model="dd.child"  ng-model-onblur class="form-control avatext"/>
						                                            </td>
						                                          
						                                        </tr>
						                                        <tr  ng-repeat="rc in mmratecontrol" >
						                                            <td>
						                                                <div class="pad_left_n col-md-7 col-xs-11">
						                                                    <h5 class="text-left rmname">CP</h5>
						                                                </div>
						                                            </td>
						                                            <td ng-repeat="dd in rc.rates | limitTo:filterLimit track by $index">
						                                            	<input type="text" id="mmsellRateTariff{{$index}}" name="mmsellRateTariff{{$index}}" ng-model="dd.sellRate" ng-model-onblur class="form-control avatext" readonly/>
						                                            </td>
						                                        </tr>
						                                        <tr  ng-repeat="rc in mmratecontrol" >
						                                            <td>
						                                                <div class="pad_left_n col-md-7 col-xs-11">
						                                                    <h5 class="text-left rmname">EP</h5>
						                                                </div>
						                                            </td>
						                                            <td ng-repeat="dd in rc.rates | limitTo:filterLimit track by $index">
						                                            	<input type="text" id="mmepRateTariff{{$index}}" name="mmepRateTariff{{$index}}" ng-model="dd.epAmount" ng-model-onblur class="form-control avatext" readonly/>
						                                            </td>
						                                        </tr>
																 <tr  ng-repeat="rc in mmratecontrol" >
						                                            <td>
						                                                <div class="pad_left_n col-md-7 col-xs-11">
						                                                    <h5 class="text-left rmname">OTA</h5>
						                                                </div>
						                                            </td>
						                                            <td ng-repeat="dd in rc.rates | limitTo:filterLimit track by $index">
						                                            	<input type="text" id="mmotaRateTariff{{$index}}" name="mmotaRateTariff{{$index}}" ng-model="dd.otaRate" ng-model-onblur class="form-control avatext" readonly/>
						                                            </td>
						                                        </tr>
						                                        
						                                        									                                   
								                               </tbody>
								                                </table>
												              <div class="col-md-12 text-center"><button  class="btn-primary pull-right" id="uppricetext" ng-click="updateMMRates()">{{uppricetext}}</button></div>
												                                
												            </div>
											          </div>
											      </div>
								            <!-- /.box-body -->
								          </div>
								          <!-- /.box -->
								        </div>
								      </div>
								      
								      </div>
				
				    		</section>
				    		<div class="modal" id="mmaddmodal">
							<div class="modal-dialog">
								<div class="modal-content">	
									<div class="modal-header">
										<button type="button" id="btnclose" class="close" data-dismiss="modal"
											aria-hidden="true">x</button>
										<h4 class="modal-title">Add Rates</h4>
									</div>
									 <form name="addMMForm" id="addRate">
									<div class="modal-body">
									
				                          <div class="form-group col-md-12">
									      <label>Select Accommodation Type :</label>
											<select  name="modMMPropertyAccommodationId" id="modMMPropertyAccommodationId" ng-model="modMMPropertyAccommodationId" value="" class="form-control" ng-required="true" >
											<option style="display:none" value="">Select Your  Accommodation</option>
					                       	<option ng-repeat="at in accommodations" value="{{at.accommodationId}}">{{at.accommodationType}}</option>
					                        </select>
										 </div>
										
										<!-- <div class="form-group col-md-12" id="checkedsources">
											<label>Source :</label>
											<div class="input-group col-md-12" >
											<div>
											<label class="checkbox-inline"><input type="checkbox"  id="checkSourceAll"  name="checkSourceAll" value="0">All</label>
					                           <label ng-repeat="sr in sources"  class="checkbox-inline">
			                      					<input type="checkbox" name="modSources[]"  id="modSources[]" value="{{sr.sourceId}}"  >&nbsp {{sr.sourceName}}
			                    				</label>
				                    		</div></div>
										</div> -->
										 <!-- <div class="form-group col-md-12" id="checkedsourcetypes">
											<label>Source :</label>
											<div class="input-group col-md-12" >
											<div>
											<label class="checkbox-inline"><input type="checkbox"  id="checkSourceTypeAll"  name="checkSourceTypeAll" value="0">All</label>
					                           <label ng-repeat="sr in sourceTypes"  class="checkbox-inline">
			                      					<input type="checkbox" name="modSourceTypes[]"  id="modSourceTypes[]" value="{{sr.sourceTypeId}}"  >&nbsp {{sr.sourceType}}
			                    				</label>
				                    		</div></div>
										</div> -->
										<div class=" form-group col-md-6">
											<label>Start Date :</label>
											 <div class="input-group date">
								                  <label class="input-group-addon btn" for="modMMStartDate">
								                  <span class="fa fa-calendar"></span>
								                  </label>   
								                  <input type="text" id="modMMStartDate" class="form-control" name="modMMStartDate"  value="" ng-required="true"  autocomplete="off">
						                	 </div>
										</div>
										
										<div class="form-group col-md-6">
											<label >End Date :</label>
											   <div class="input-group date">
								                   <label class="input-group-addon btn" for="modMMEndDate">
								                   <span class="fa fa-calendar"></span>
								              		</label>   
								                   <input type="text" id="modMMEndDate" class="form-control" name="modMMEndDate"  value="" ng-required="true"  autocomplete="off"> 
							                   </div>
										</div>
										<div class="form-group col-md-6">
											<label>Minimum Price</label>
											<input type="text" class="form-control"  name="minamount"  id="minamount" ng-model="minamount" ng-pattern="/^[0-9]/" ng-required="true" placeholder="Minimum Price">
										    <span ng-show="addMMForm.minamount.$error.pattern" style="color:red">please enter numeric</span>
										</div>
										<div class="form-group col-md-6">
											<label>Maximum Price</label>
											<input type="text" class="form-control"  name="maxamount"  id="maxamount" ng-model="maxamount" ng-pattern="/^[0-9]/" ng-required="true" placeholder="Maximum Price">
										    <span ng-show="addMMForm.maxamount.$error.pattern" style="color:red">please enter numeric</span>
										</div>
										<div class="form-group col-md-6">
											<label>Extra Adult</label>
											<input type="text" class="form-control"  name="extraMMAdult"  id="extraMMAdult"  placeholder="Extra Adult Rate" ng-model='extraMMAdult' ng-pattern="/^[0-9]/" ng-required="true">
										    <span ng-show="addMMForm.extraMMAdult.$error.pattern" style="color:red">please enter numeric</span>
										      
										</div>
										<div class="form-group col-md-6">
											<label>Extra Child</label>
											<input type="text" class="form-control"  name="extraMMChild"  id="extraMMChild"  placeholder="Extra Child Rate" ng-model='extraMMChild' ng-pattern="/^[0-9]/" ng-required="true">
										    <span ng-show="addMMForm.extraMMChild.$error.pattern" style="color:red">please enter numeric</span>
										</div>
										<div class="form-group col-md-6">
											<label>Extra Infant</label>
											<input type="text" class="form-control"  name="extraMMInfant"  id="extraMMInfant"  placeholder="Extra Infant Rate" ng-model='extraInfant' ng-pattern="/^[0-9]/" ng-required="true">
										    <span ng-show="addMMForm.extraMMInfant.$error.pattern" style="color:red">please enter numeric</span>
										</div>
										<div class="form-group col-md-12" id="mmratecheckeddays">
											<label>Days of week :</label>
										   <div class="input-group" >
				                          	<div>
										   		<label class="checkbox-inline"><input type="checkbox"  id="mmrateCheckAll"  name="mmrateCheckAll" ng-model="selectedRateAllDays" ng-click="selectRateAllDays()" value="0" >All Days</label>
						                        <label ng-repeat="rd in ratedays"  class="checkbox-inline">
				                      				<input type="checkbox" name="mmcheckedDay[]"  id="mmcheckedDay" value="{{rd.dayId}}" ng-model="rd.Selected" ng-click="checkIfRateAllSelectedDays()" class="flat-red" >&nbsp {{rd.daysOfWeek}}
				                    			</label>
				                    		</div>
										   </div>
										</div>
									
										<div class="form-group" >
										   <div class="input-group" >
										  		<label>&nbsp;&nbsp;</label>
										  		<label>&nbsp;&nbsp;</label>
										   </div>
										</div>

									<div class="modal-footer">
										<span id="modifymmratepromoalert"></span>
										<a href="#" data-dismiss="modal" class="btn btn-danger">Close</a>
										<button ng-click="addMMForm.$valid && updateMMPrice()" id="loading-example-btn" ng-disabled="addMMForm.$invalid" class="btn btn-primary">Save</button>
									</div>
									</div>
									</form>
									</div>
								</div>
							<!-- /.modal-content -->
						</div>
						
                  </div>
                  </div>
                   <div class="tab-pane " id="tab-price">
                  		<div class="tab-pane active" id="fa-icons-offer">
		                     <section class="content">
						    <div class="box box-default color-palette-box">
						        <div class="box-header ">
						          <h3 class="box-titles">Price Update</h3>
						        </div>
						        <div class="box-body">
						                <form name="searchRate">
								            <div class="col-sm-3 col-md-3">
								               <div class="form-group">
								                  <label>Select Accommodation Type :</label>
								                  <select name="ratePropertyAccommodationId" id="ratePropertyAccommodationId" value="" ng-model="ratePropertyAccommodationId" class="form-control" ng-required="true">
								                     <option value="">Choose Accommodation</option>
								                     <option ng-repeat="at in accommodations" value="{{at.accommodationId}}">{{at.accommodationType}}</option>
								                  </select>
								               </div>
								            </div>
								            <div class="col-sm-3 col-md-3">
								               <div class=" form-group">
								                  <label>Start Date :</label>
								                  <div class="input-group date">
								                     <label class="input-group-addon btn" for="rateStartDate">
								                     <span class="fa fa-calendar"></span>
								                     </label>   
								                     <input type="text"  id="rateStartDate" class="form-control" name="rateStartDate"  value="" ng-required="true" autocomplete="off">
								                  </div>
								               </div>
								            </div>
								            <div class="col-sm-3 col-md-3">
								               <div class="form-group">
								                  <label >End Date :</label>
								                  <div class="input-group date">
								                     <label class="input-group-addon btn" for="rateEndDate">
								                     <span class="fa fa-calendar"></span>
								                     </label>   
								                     <input type="text" id="rateEndDate" class="form-control" name="rateEndDate" value="" ng-required="true" autocomplete="off"> 
								                  </div>
								               </div>
								            </div>
								            <%-- <div class="col-sm-4 col-md-3">
												<div class="form-group">
													<label>Source</label>
													<select  class="form-control" name="sourceId" ng-model="sourceId" id="sourceId" value="" class="form-control " ng-required="true">
													<option value="">Select Source</option>
													<option ng-repeat="sr in sources" value="{{sr.sourceId}}" >{{sr.sourceName}}</option>
													</select>
												</div>
												<input type="hidden" id="sourceId" name="sourceId" class="form-control" value = "" placeholder="Source Id">
											</div> --%>
											<%-- <div class="col-sm-4 col-md-3">
												<div class="form-group">
													<label>Source Type</label>
													<select  class="form-control" name="sourceTypeId" ng-model="sourceTypeId" id="sourceTypeId" value="" class="form-control " ng-required="true">
													<option value="">Select Source Type</option>
													<option ng-repeat="sr in sourceTypes" value="{{sr.sourceTypeId}}" >{{sr.sourceType}}</option>
													</select>
												</div>
												<input type="hidden" id="sourceTypeId" name="sourceTypeId" class="form-control" value = "">
											</div> --%>
								            <div class="col-sm-3 col-md-3" >
									            <label >Search or Modify:</label>
									            <div class="form-group">
									            <button type="submit" ng-click="getRateSearch()"  class="btn btn-primary"  ><i class="fa fa-search-plus" aria-hidden="true"></i> Search</button> <!-- ng-disabled="searchRate.$invalid" -->
								                <a class="btn btn-danger" href="#addmodal" ng-click=""  data-toggle="modal"  >Modify Price</a>
									            </div>
								             </div> 
								        	 <div class="col-sm-12 col-md-12">
								        	 <span id="sellratepromoalert"></span>
								        	 </div>	
								         </form>
								        </div>
								        <!-- /.box-body -->
								              
								      
								      <div class="row avabg">
								        <div class="col-xs-12 ">
								          <div class="box invcal">
								            <div class="box-header">
								              <h3 class="box-title">Manage Price</h3>
								
								              
								            </div>
											      <div class="row">
											         <div class="col-md-12 " >
											                  <div class="row ">
											               <div class="col-md-6 text-center"><button class="btnnextdays" ng-click="rateDatePrevious()">Previous 10 Days {{roomCount.roomName }}</button></div>
											               <div class="col-md-6 text-center"><button  class="btnnextdays" ng-click="rateDateNext()">Next 10 Days</button></div>
											            </div>
											            <div class="table-responsive " ng-repeat="rc in ratecontrol | limitTo :1"> 
							                              <table width="100%" class="table table-bordered ">
							                                    <tbody>
														         <tr>
														            <td ><span class="font14 roomtype-text">Room Type</span></td>
														            <td ><span class="font14 roomtype-text">Sell Rate</span></td>
														             <td ng-repeat="dd in rc.rates | limitTo:filterLimit track by $index" class="text-center" id="tlastchild{{$index}}"> {{dd.date | date:'MMMM dd, yyyy' }}</td>
														        </tr>
						                                        <tr  ng-repeat="rc in ratecontrol" >
						                                            <td>
						                                            	<div class="pad_left_n col-md-7 col-xs-11">
						                                                    <h5 class="text-left rmname">ULO WEBSITE</h5>
						                                                </div>
						                                                <div class="pad_left_n col-md-7 col-xs-11">
						                                                    <h5 class="text-left rmname">{{ rc.accommodationType }}</h5>
						                                                </div>
						                                            </td>
						                                            <td>
						                                                <div class="pad_left_n col-md-7 col-xs-11">
						                                                    <h5 class="text-left rmname">Tariff</h5>
						                                                    <h1></h1>
						                                                    <h5 class="text-left rmname">Adult</h5>
						                                                    <h1></h1>
						                                                    <h5 class="text-left rmname">Child</h5>
						                                                </div>
						                                            </td>
						                                            <td ng-repeat="dd in rc.rates | limitTo:filterLimit track by $index">
						                                            
<!-- 						                                            <input type="text" ng-change="singleBlockRates(dd.tariff,dd.date,rc.accommodationId,rc.sourceTypeId)" ng-model="dd.tariff"  ng-model-onblur class="form-control avatext"/> -->
																	<input type="text" id="tariff{{$index}}" name="tariff{{$index}}" ng-change="singleRates(dd.date,rc.accommodationId,dd.adult,dd.child,dd.tariff,dd.sellrate,dd.netrate,dd.netadult,dd.netchild,'tariff',$index)" ng-model="dd.tariff"  ng-model-onblur class="form-control avatext" readonly/>
						                                            <input type="text" id="adult{{$index}}" name="adult{{$index}}" ng-change="singleRates(dd.date,rc.accommodationId,dd.adult,dd.child,dd.tariff,dd.sellrate,dd.netrate,dd.netadult,dd.netchild,'adult',$index)" ng-model="dd.adult"  ng-model-onblur class="form-control avatext" readonly/>
						                                            <input type="text" id="child{{$index}}" name="child{{$index}}" ng-change="singleRates(dd.date,rc.accommodationId,dd.adult,dd.child,dd.tariff,dd.sellrate,dd.netrate,dd.netadult,dd.netchild,'child',$index)" ng-model="dd.child"  ng-model-onblur class="form-control avatext" readonly/>
						                                            </td>
						                                          
						                                        </tr>
																<tr>
						                                            <td>
						                                                <div class="pad_left_n col-md-7 col-xs-11">
						                                                    <h5 class="text-left rmname">OTA CHANNEL</h5>
						                                                </div>
						                                                <div class="pad_left_n col-md-7 col-xs-11">
						                                                    <h5 class="text-left rmname">{{rc.accommodationType}}</h5>
						                                                </div>
						                                            </td>
						                                            <td>
						                                                <div class="pad_left_n col-md-7 col-xs-11">
						                                                    <h5 class="text-left rmname">Tariff</h5>
						                                                    <h1></h1>
						                                                    <h5 class="text-left rmname">Adult</h5>
						                                                    <h1></h1>
						                                                    <h5 class="text-left rmname">Child</h5>
						                                                </div>
						                                            </td>
						                                            <td ng-repeat="dd in rc.rates | limitTo:filterLimit track by $index">
						                                            	<input type="text" id="otaRate{{$index}}" name="otaRate{{$index}}" ng-model="dd.otatariff" ng-model-onblur class="form-control avatext" readonly/>
						                                            	<input type="text" id="otaAdult{{$index}}" name="otaAdult{{$index}}" ng-model="dd.otaadult" ng-model-onblur class="form-control avatext" readonly/>
						                                            	<input type="text" id="otaChild{{$index}}" name="otaChild{{$index}}" ng-model="dd.otachild" ng-model-onblur class="form-control avatext" readonly/>
						                                            </td>
						                                        </tr> 										                                   
								                               </tbody>
								                                </table>
												              <div class="col-md-12 text-center"><button  class="btn-primary pull-right" id="uppricetext" ng-click="updateRates()">{{uppricetext}}</button></div>
												                                
												            </div>
											          </div>
											      </div>
								            <!-- /.box-body -->
								          </div>
								          <!-- /.box -->
								        </div>
								      </div>
								      
								      </div>
				
				    		</section>
				    		<div class="modal" id="addmodal">
							<div class="modal-dialog">
								<div class="modal-content">	
									<div class="modal-header">
										<button type="button" id="btnclose" class="close" data-dismiss="modal"
											aria-hidden="true">x</button>
										<h4 class="modal-title">Add Rates</h4>
									</div>
									 <form name="addForm" id="addRate">
									<div class="modal-body">
									
				                          <div class="form-group col-md-12">
									      <label>Select Accommodation Type :</label>
											<select  name="modPropertyAccommodationId" id="modPropertyAccommodationId" ng-model="modPropertyAccommodationId" value="" class="form-control" ng-required="true" >
											<option style="display:none" value="">Select Your  Accommodation</option>
					                       	<option ng-repeat="at in accommodations" value="{{at.accommodationId}}">{{at.accommodationType}}</option>
					                        </select>
										 </div>
										
										<!-- <div class="form-group col-md-12" id="checkedsources">
											<label>Source :</label>
											<div class="input-group col-md-12" >
											<div>
											<label class="checkbox-inline"><input type="checkbox"  id="checkSourceAll"  name="checkSourceAll" value="0">All</label>
					                           <label ng-repeat="sr in sources"  class="checkbox-inline">
			                      					<input type="checkbox" name="modSources[]"  id="modSources[]" value="{{sr.sourceId}}"  >&nbsp {{sr.sourceName}}
			                    				</label>
				                    		</div></div>
										</div> -->
										 <!-- <div class="form-group col-md-12" id="checkedsourcetypes">
											<label>Source :</label>
											<div class="input-group col-md-12" >
											<div>
											<label class="checkbox-inline"><input type="checkbox"  id="checkSourceTypeAll"  name="checkSourceTypeAll" value="0">All</label>
					                           <label ng-repeat="sr in sourceTypes"  class="checkbox-inline">
			                      					<input type="checkbox" name="modSourceTypes[]"  id="modSourceTypes[]" value="{{sr.sourceTypeId}}"  >&nbsp {{sr.sourceType}}
			                    				</label>
				                    		</div></div>
										</div> -->
										<div class=" form-group col-md-6">
											<label>Start Date :</label>
											 <div class="input-group date">
								                  <label class="input-group-addon btn" for="modStartDate">
								                  <span class="fa fa-calendar"></span>
								                  </label>   
								                  <input type="text" id="modStartDate" class="form-control" name="modStartDate"  value="" ng-required="true"  autocomplete="off">
						                	 </div>
										</div>
										
										<div class="form-group col-md-6">
											<label >End Date :</label>
											   <div class="input-group date">
								                   <label class="input-group-addon btn" for="modEndDate">
								                   <span class="fa fa-calendar"></span>
								              		</label>   
								                   <input type="text" id="modEndDate" class="form-control" name="modEndDate"  value="" ng-required="true"  autocomplete="off"> 
							                   </div>
										</div>
										<div class="form-group col-md-6">
											<label>Tariff</label>
											<input type="text" class="form-control"  name="amount"  id="amount" ng-model="amount" ng-pattern="/^[0-9]/" ng-required="true" placeholder="Tariff Per Night">
										    <span ng-show="addForm.amount.$error.pattern" style="color:red">please enter numeric</span>
										</div>
									
										<div class="form-group col-md-6">
											<label for="child_included_rate">Extra Adult</label>
											<input type="text" class="form-control"  name="extraAdult"  id="extraAdult"  placeholder="Extra Adult Rate" ng-model='extraAdult' ng-pattern="/^[0-9]/" ng-required="true">
										    <span ng-show="addForm.extraAdult.$error.pattern" style="color:red">please enter numeric</span>
										      
										</div>
										<div class="form-group col-md-6">
											<label for="child_included_rate">Extra Child</label>
											<input type="text" class="form-control"  name="extraChild"  id="extraChild"  placeholder="Extra Child Rate" ng-model='extraChild' ng-pattern="/^[0-9]/" ng-required="true">
										    <span ng-show="addForm.extraChild.$error.pattern" style="color:red">please enter numeric</span>
										</div>
										<div class="form-group col-md-6">
											<label>Extra Infant</label>
											<input type="text" class="form-control"  name="extraInfant"  id="extraInfant"  placeholder="Extra Infant Rate" value="0" ng-pattern="/^[0-9]/" ng-required="true" readonly>
										</div>
										<div class="form-group col-md-12" id="ratecheckeddays">
											<label>Days of week :</label>
										   <div class="input-group" >
				                          	<div>
										   		<label class="checkbox-inline"><input type="checkbox"  id="rateCheckAll"  name="rateCheckAll"  ng-model="selectedRateAllDays" ng-click="selectRateAllDays()" value="0">All Days</label>
						                        <label ng-repeat="rd in ratedays"  class="checkbox-inline">
				                      				<input type="checkbox" name="checkedDay[]"  id="checkedDay" value="{{rd.dayId}}"  ng-model="rd.Selected" ng-click="checkIfRateAllSelectedDays()" class="flat-red" >&nbsp {{rd.daysOfWeek}}
				                    			</label>
				                    		</div>
										   </div>
										</div>
									
										<div class="form-group" >
										   <div class="input-group" >
										  		<label>&nbsp;&nbsp;</label>
										  		<label>&nbsp;&nbsp;</label>
										   </div>
										</div>

									<div class="modal-footer">
										<span id="sellratepromoalert"></span>
										<a href="#" data-dismiss="modal" class="btn btn-danger">Close</a>
										<button ng-click="addForm.$valid && updatePrice()" id="loading-example-btn" ng-disabled="addForm.$invalid" class="btn btn-primary">Save</button>
									</div>
									</div>
									</form>
									</div>
								</div>
							<!-- /.modal-content -->
						</div>
						
                  </div>
                  </div>
                  </div>
                  
               </div>
              
            </div>
            <!-- /.nav-tabs-custom -->
         </div>
         <!-- /.col -->
      </div>
      <!-- /.row -->
      
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="plugins/fullcalendar/fullcalendar.min.js"></script>
<!-- Page specific script -->
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script src="dist/js/pages/canvasjs.min.js"></script>
<script type="text/javascript"  src="contents/js/ngprogress.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>
<script>
   var app = angular.module('myApp', ['ngProgress','ui.bootstrap']);
   	app.controller('customersCtrl', function($scope, $http,$timeout,ngProgressFactory) {
   		$scope.progressbar = ngProgressFactory.createInstance();
   		var spinner = $('#loadertwo');
     	$timeout(function(){
         	    // $scope.progressbar.complete();
               $scope.show = true;
               $("#pre-loader").css("display","none");
           }, 2000);
     	
          
          	$scope.datePrevious = function(){
          		
         		 var result = new Date(startDate);
         		result.setDate(result.getDate() - 10);
         		var months = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
         		var month = months[result.getMonth()];
         		var year = result.getFullYear();
         		var dates = month  + " " + result.getDate() + "," + year;

         		var fdata = "&startBlockDate=" + dates
      			+ "&endBlockDate=" + startDate
      			+ "&propertyAccommodationId=" + $('#propertyAccommodationId').val()
             	 	spinner.show();
             	  $http(
               		   {
               			   method : 'POST',
               			   data : fdata,
               			   headers : {
               			   'Content-Type' : 'application/x-www-form-urlencoded'
               			   },
               			 url : 'get-bulk-block-inventory'
               			   }).success(function(response) {
               			   
               			   $scope.blockinventory = response.data;
                 	
               		      setTimeout(function(){ 
      			        	
     			        	  spinner.hide();
     			      	   lastDate = $scope.blockinventory[0].roomAvailable[$scope.blockinventory[0].roomAvailable.length-1].date;
               			   startDate = $scope.blockinventory[0].roomAvailable[0].date;
  			         
     			          }, 1000);
               		   });
          	};
        	$scope.dateNext = function(){
          		var result = new Date(lastDate);
          		result.setDate(result.getDate() + 10);
          		var months = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
          		var month = months[result.getMonth()];
          		var year = result.getFullYear();
          		var dates = month  + " " + result.getDate() + "," + year;

          		var fdata = "&startBlockDate=" + lastDate
       			+ "&endBlockDate=" + dates
       			+ "&propertyAccommodationId=" + $('#propertyAccommodationId').val();
              	 	spinner.show();
              	  $http(
                		   {
                			   method : 'POST',
                			   data : fdata,
                			   headers : {
                			   'Content-Type' : 'application/x-www-form-urlencoded'
                			   },
                			 url : 'get-bulk-block-inventory'
                			   }).success(function(response) {
                			   
                			   $scope.blockinventory = response.data;
                  			   
                			   setTimeout(function(){ 
             			        	
          			        	  spinner.hide();
          			        	lastDate = $scope.blockinventory[0].roomAvailable[$scope.blockinventory[0].roomAvailable.length-1].date;
                 			   startDate = $scope.blockinventory[0].roomAvailable[0].date;
          			          }, 1000);
                			   
                			   
                			   });
          	};
          	
 			$scope.editPlan = function(id,type,name,tariff,status,accommId,indx,symbolType){
				 
		         $scope.rateplanlist[indx].ratePlanId=id;
		         $scope.rateplanlist[indx].ratePlanType = type; 
		         $scope.rateplanlist[indx].ratePlanTariff = tariff; 
		         $scope.rateplanlist[indx].ratePlanSymbolType= symbolType;
		         $scope.rateplanlist[indx].planIsActive = status;
		         $scope.rateplanlist[indx].ratePlanName = name; 
		         $scope.rateplanlist[indx].accommodationId=accommId;
			};
          	
          	$scope.updateRatePlan = function(){

				
				var text =  '{"array":' + JSON.stringify($scope.rateplanlist) + '}';
				var fdata = JSON.parse(text);		
				
				$http(
						{
							method : 'POST',
							data : fdata,
							headers : {
								'Content-Type': 'application/json; charset=utf-8'
							},
							url : 'edit-rate-plan'
						}).then(function successCallback(response) {
							 $("#ratepromoalert").hide();
							  var alertmsg = ' <div class="alert" style="color:green;text-align:center;">rate plan updated.</div>';
					            $('#ratepromoalert').html(alertmsg);
					            $("#ratepromoalert").fadeTo(2000, 500).slideUp(500, function(){
					                $("#ratepromoalert").slideUp(500);
					            });
					            $('#editrateplan').modal('toggle');
					              
				}, function errorCallback(response) {
					$("#ratepromoalert").hide();
					  var alertmsg = ' <div class="alert" style="color:green;text-align:center;">process failed, please try again.</div>';
			            $('#ratepromoalert').html(alertmsg);
			            $("#ratepromoalert").fadeTo(2000, 500).slideUp(500, function(){
			                $("#ratepromoalert").slideUp(500);
			            });
			            $('#editrateplan').modal('toggle');
				});
        	}; 
          	$scope.updatePrice = function(){
  	    	  $("#loading-example-btn").prop('disabled', true);
  	    	 
  				/* var checkbox_sources = "";
  			    $("#checkedsources :checkbox").each(function () {
  			        var ischecked = $(this).is(":checked");
  			        if (ischecked) {
  			            checkbox_sources += $(this).val() + ",";
  			        }
  			    }); */
  			  /* var checkbox_source_types = "";
  			    $("#checkedsourcetypes :checkbox").each(function () {
  			        var ischecked = $(this).is(":checked");
  			        if (ischecked) {
  			        	checkbox_source_types += $(this).val() + ",";
  			        }
  			    }); */
  			    
  			    var checkbox_value = "";
  				    $("#ratecheckeddays :checkbox").each(function () {
  				        var ischecked = $(this).is(":checked");
  				        if (ischecked) {
  				            checkbox_value += $(this).val() + ",";
  				        }
  				    });
  				    
  		     	
  		     	
  		     	var fdata = "checkedDays=" + checkbox_value
  		     	+ "&propertyAccommodationId=" + $('#modPropertyAccommodationId').val()
  		     	+ "&baseAmount=" + $('#amount').val()
//   		     	+ "&modSourceTypes=" + checkbox_source_types
  				+ "&strModStartDate=" + $('#modStartDate').val()
  				+ "&strModEndDate=" + $('#modEndDate').val()
  				+ "&extraChild=" + $('#extraChild').val()
  				+ "&extraAdult=" + $('#extraAdult').val()
  				+ "&extraInfant=" + $('#extraInfant').val()
  				
				/* var sourcetypeslength=$('[name="modSourceTypes[]"]:checked').length;
  		     	var sourcetypes=$scope.sourceTypes.length;
  		     	 */
  		     	 
  				var checkedDayslength = $('[name="checkedDay[]"]:checked').length;
  		     	
  		     	var checkedAll=$('[name="rateCheckAll"]:checked').length;
//   		     	var checkedSourceTypeAll=$('[name="checkSourceTypeAll"]:checked').length;
  		     	
  		     	var isChecked=false;
  				if(document.getElementById('modStartDate').value==""){
  		 			 document.getElementById('modStartDate').focus();
  		 			document.getElementById("modStartDate").style.borderColor = "red";
  		 		 }else if(document.getElementById('modEndDate').value==""){
  		 			 document.getElementById('modEndDate').focus();
  		 			document.getElementById("modEndDate").style.borderColor = "red";
  		 		 }else if(document.getElementById('modStartDate').value!="" && document.getElementById('modEndDate').value!=""){
  				
  						if(checkedDayslength>0 || checkedAll>0 ){
  							if( checkedDayslength == 7 && checkedAll>0){
  								isChecked=true;
  							}else if(checkedDayslength>0 && checkedDayslength<=6 && checkedAll==0){
  								isChecked=true;
  							} else{
  								$("#sellratepromoalert").hide();
 							   var alertmsg = ' <div class="alert" style="color:green;text-align:center;">please tick all to continue</div>';
 					            $('#sellratepromoalert').html(alertmsg);
 					            $("#sellratepromoalert").fadeTo(2000, 500).slideUp(500, function(){
 					              
 					                 });
 					           $("#loading-example-btn").prop('disabled', false);
  							}
  							spinner.show();
  							if(isChecked){
  						$http(
  								{
  									method : 'POST',
  									data : fdata,
  									headers : {
  										'Content-Type' : 'application/x-www-form-urlencoded'
  									},
  									url : 'add-rate'
  								}).then(function successCallback(response) {
  									
  									var gdata = "checkedDays=" + checkbox_value
  							     	    + "&baseAmount=" + $('#amount').val()
  							     	    + "&strModEndDate=" + $('#modStartDate').val()
  						                + "&strModEndDate=" + $('#modEndDate').val()
  						                + "&extraChild=" + $('#extraChild').val()
  										+ "&extraAdult=" + $('#extraAdult').val()
  										+ "&extraInfant=" + $('#extraInfant').val()
  									
  									$http(
  								    {
  									method : 'POST',
  									data : gdata,
  									//data : '{"data":' +JSON.stringify($scope.roomsSelected) + '}',
  									
  									headers : {
  										'Content-Type' : 'application/x-www-form-urlencoded'
  										//'Content-Type' : 'application/x-www-form-urlencoded'
  									},
  									url : 'add-rate-details'
  								   }).then(function successCallback(response) {
                                    //  window.location.reload();  
  									   setTimeout(function(){ spinner.hide(); 
  								       $('#addmodal').modal('toggle');
  	  								  $("#sellratepromoalert").hide();
  	  								   var alertmsg = ' <div class="alert" style="color:green;text-align:center;">rate added successfully</div>';
  	  						            $('#sellratepromoalert').html(alertmsg);
  	  						            $("#sellratepromoalert").fadeTo(2000, 500).slideUp(500, function(){
  	  						              
  	  						                 });
                                       }, 3000);
                             
  						          $scope.getAllLogRates();
  								    $("#loading-example-btn").prop('disabled', false); 
  								   }, function errorCallback(response) {
  									 setTimeout(function(){ 
  										 spinner.hide();
  										 $('#addmodal').modal('toggle');
  	  	  								  $("#sellratepromoalert").hide();
  	  	  								   var alertmsg = ' <div class="alert" style="color:green;text-align:center;">process failed, please try again.</div>';
  	  	  						            $('#sellratepromoalert').html(alertmsg);
  	  	  						            $("#sellratepromoalert").fadeTo(2000, 500).slideUp(500, function(){
  	  	  						              
  	  	  						                 });
  	  	  						        $("#loading-example-btn").prop('disabled', false);
                                     }, 3000);

  							
  								});
  							
  							}, function errorCallback(response) {
  								$('#addmodal').modal('toggle');
  								setTimeout(function(){
  									spinner.hide(); 
  							  $("#sellratepromoalert").hide();
							   var alertmsg = ' <div class="alert" style="color:green;text-align:center;">process failed, please try again.</div>';
					            $('#sellratepromoalert').html(alertmsg);
					            $("#sellratepromoalert").fadeTo(2000, 500).slideUp(500, function(){
					              
					                 });
					            $("#loading-example-btn").prop('disabled', false);
                                }, 3000);

								
  							});
  						}
  						}else{
  							$("#sellratepromoalert").hide();
							   var alertmsg = ' <div class="alert" style="color:green;text-align:center;">please check atleast one value</div>';
					            $('#sellratepromoalert').html(alertmsg);
					            $("#sellratepromoalert").fadeTo(2000, 500).slideUp(500, function(){
					              
					                 });
					            $("#loading-example-btn").prop('disabled', false);
  						}
  		     	
  		 		 }
  			};
          	
  			$scope.updateMMPrice = function(){
    	    	  $("#loading-example-btn").prop('disabled', true);
    	    	
    			    
    			    var checkbox_value = "";
    				    $("#mmratecheckeddays :checkbox").each(function () {
    				        var ischecked = $(this).is(":checked");
    				        if (ischecked) {
    				            checkbox_value += $(this).val() + ",";
    				        }
    				    });
    				    
    		     	
    		     	
    		     	var fdata = "checkedDays=" + checkbox_value
    		     	+ "&propertyAccommodationId=" + $('#modMMPropertyAccommodationId').val()
    		     	+ "&minimumAmount=" + $('#minamount').val()
    		     	+ "&maximumAmount=" + $('#maxamount').val()
    		     	+ "&sourceTypeId=" + 1
    				+ "&strModStartDate=" + $('#modMMStartDate').val()
    				+ "&strModEndDate=" + $('#modMMEndDate').val()
    				+ "&extraChild=" + $('#extraMMChild').val()
    				+ "&extraAdult=" + $('#extraMMAdult').val()
    				+ "&extraInfant=" + $('#extraMMInfant').val()
    				
    				var checkedDayslength = $('[name="mmcheckedDay[]"]:checked').length;
    		     	
    		     	var checkedAll=$('[name="mmrateCheckAll"]:checked').length;
    		     	
    		     	var isChecked=false;
    				if(document.getElementById('modMMStartDate').value==""){
    		 			 document.getElementById('modMMStartDate').focus();
    		 			document.getElementById("modMMStartDate").style.borderColor = "red";
    		 		 }else if(document.getElementById('modMMEndDate').value==""){
    		 			 document.getElementById('modMMEndDate').focus();
    		 			document.getElementById("modMMEndDate").style.borderColor = "red";
    		 		 }else if(document.getElementById('modMMStartDate').value!="" && document.getElementById('modMMEndDate').value!=""){
    				
    						if(checkedDayslength>0 || checkedAll>0 ){
    							if( checkedDayslength == 7 && checkedAll>0){
    								isChecked=true;
    							}else if(checkedDayslength>0 && checkedDayslength<=6 && checkedAll==0){
    								isChecked=true;
    							} else{
    								$("#modifymmratepromoalert").hide();
   							   var alertmsg = ' <div class="alert" style="color:green;text-align:center;">please tick all to continue</div>';
   					            $('#modifymmratepromoalert').html(alertmsg);
   					            $("#modifymmratepromoalert").fadeTo(2000, 500).slideUp(500, function(){
   					              
   					                 });
   					           $("#loading-example-btn").prop('disabled', false);
    							}
    							spinner.show();
    							
    							if(isChecked){
    						$http(
    								{
    									method : 'POST',
    									data : fdata,
    									headers : {
    										'Content-Type' : 'application/x-www-form-urlencoded'
    									},
    									url : 'add-min-max-rate'
    								}).then(function successCallback(response) {
    									
    									var gdata = "checkedDays=" + checkbox_value
    				    		     	+ "&minimumAmount=" + $('#minamount').val()
    				    		     	+ "&maximumAmount=" + $('#maxamount').val()
    				    		     	+ "&baseAmount=" + $('#minamount').val()
    				    				+ "&strModStartDate=" + $('#modMMStartDate').val()
    				    				+ "&strModEndDate=" + $('#modMMEndDate').val()
    				    				+ "&extraChild=" + $('#extraMMChild').val()
    				    				+ "&extraAdult=" + $('#extraMMAdult').val()
    				    				+ "&extraInfant=" + $('#extraMMInfant').val()
    				    				
    									
    									$http(
    								    {
    									method : 'POST',
    									data : gdata,
    									//data : '{"data":' +JSON.stringify($scope.roomsSelected) + '}',
    									
    									headers : {
    										'Content-Type' : 'application/x-www-form-urlencoded'
    										//'Content-Type' : 'application/x-www-form-urlencoded'
    									},
    									url : 'add-min-max-rate-details'
    								   }).then(function successCallback(response) {
                                      //  window.location.reload();  
    									   setTimeout(function(){ spinner.hide(); 
    								       $('#mmaddmodal').modal('toggle');
    	  								  $("#mmratepromoalert").hide();
    	  								   var alertmsg = ' <div class="alert" style="color:green;text-align:center;">rate added successfully</div>';
    	  						            $('#mmratepromoalert').html(alertmsg);
    	  						            $("#mmratepromoalert").fadeTo(2000, 500).slideUp(500, function(){
    	  						              
    	  						                 });
                                         }, 3000);
                               
    						          $scope.getAllLogRates();
    								    $("#loading-example-btn").prop('disabled', false); 
    								   }, function errorCallback(response) {
    									 setTimeout(function(){ 
    										 spinner.hide();
    										 $('#mmaddmodal').modal('toggle');
    	  	  								  $("#mmratepromoalert").hide();
    	  	  								   var alertmsg = ' <div class="alert" style="color:green;text-align:center;">process failed, please try again.</div>';
    	  	  						            $('#mmratepromoalert').html(alertmsg);
    	  	  						            $("#mmratepromoalert").fadeTo(2000, 500).slideUp(500, function(){
    	  	  						              
    	  	  						                 });
    	  	  						        $("#loading-example-btn").prop('disabled', false);
                                       }, 3000);

    							
    								});
    							
    							}, function errorCallback(response) {
    								$('#mmaddmodal').modal('toggle');
    								setTimeout(function(){
    									spinner.hide(); 
    							  $("#mmratepromoalert").hide();
  							   var alertmsg = ' <div class="alert" style="color:green;text-align:center;">process failed, please try again.</div>';
  					            $('#mmratepromoalert').html(alertmsg);
  					            $("#mmratepromoalert").fadeTo(2000, 500).slideUp(500, function(){
  					              
  					                 });
  					            $("#loading-example-btn").prop('disabled', false);
                                  }, 3000);

  								
    							});
    						}
    						}else if(checkedDayslength==0){
    							$("#modifymmratepromoalert").hide();
  							   var alertmsg = ' <div class="alert" style="color:green;text-align:center;">please check atleast one value from days</div>';
  					            $('#modifymmratepromoalert').html(alertmsg);
  					            $("#modifymmratepromoalert").fadeTo(2000, 500).slideUp(500, function(){
  					              
  					                 });
  					            $("#loading-example-btn").prop('disabled', false);
    						}else{
    							$("#modifymmratepromoalert").hide();
  							   var alertmsg = ' <div class="alert" style="color:green;text-align:center;">please check atleast one value</div>';
  					            $('#modifymmratepromoalert').html(alertmsg);
  					            $("#modifymmratepromoalert").fadeTo(2000, 500).slideUp(500, function(){
  					              
  					                 });
  					            $("#loading-example-btn").prop('disabled', false);
    						}
    		     	
    		 		 }
    			};
    			
         	$scope.rateDatePrevious = function(){
        		 var result = new Date(rateStartDate);
        		result.setDate(result.getDate() - 10);
        		var months = ["January","February","March","April","May","June","July","August","September","October","November","December"];
        		var month = months[result.getMonth()];
        		var year = result.getFullYear();
        		var dates = month  + " " + result.getDate() + ", " + year;
				
        		var fdata = "&strStartDate=" + dates
     			+ "&strEndDate=" + rateStartDate
     			+ "&accommodationId=" + $('#ratePropertyAccommodationId').val()
     			+ "&sourceTypeId="+ 1;
        		spinner.show();
            	  $http(
              		   {
              			   method : 'POST',
              			   data : fdata,
              			   headers : {
              			   'Content-Type' : 'application/x-www-form-urlencoded'
              			   },
              			 url : 'get-revenue-rate-details'
              			   }).success(function(response) {
              			   
              				 $scope.ratecontrol = response.data;
               			   	rateLastDate = $scope.ratecontrol[0].rates[$scope.ratecontrol[0].rates.length-1].date;
             			 	rateStartDate = $scope.ratecontrol[0].rates[0].date;
             			 	spinner.hide();
              		   });
         	};
         	
       	$scope.rateDateNext = function(){
         		 var result = new Date(rateLastDate);
         		result.setDate(result.getDate() + 10);
         		var months = ["January","February","March","April","May","June","July","August","September","October","November","December"];
         		var month = months[result.getMonth()];
         		var year = result.getFullYear();
         		var dates = month  + " " + result.getDate() + ", " + year;
				
         		var fdata = "&strStartDate=" + rateLastDate
      			+ "&strEndDate=" + dates
      			+ "&accommodationId=" + $('#ratePropertyAccommodationId').val()
      			+ "&sourceTypeId="+ 1;
             	spinner.show();
             	  $http(
               		   {
               			   method : 'POST',
               			   data : fdata,
               			   headers : {
               			   'Content-Type' : 'application/x-www-form-urlencoded'
               			   },
               			 url : 'get-revenue-rate-details'
               			   }).success(function(response) {
               				$scope.ratecontrol = response.data;
              			   	rateLastDate = $scope.ratecontrol[0].rates[$scope.ratecontrol[0].rates.length-1].date;
            			 	rateStartDate = $scope.ratecontrol[0].rates[0].date;               			   
            			 	spinner.hide();
               		   });
         	};
         	
         	$scope.mmrateDatePrevious = function(){
       		 var result = new Date(mmrateStartDate);
       		result.setDate(result.getDate() - 10);
       		var months = ["January","February","March","April","May","June","July","August","September","October","November","December"];
       		var month = months[result.getMonth()];
       		var year = result.getFullYear();
       		var dates = month  + " " + result.getDate() + ", " + year;
				
       		var fdata = "&strStartDate=" + dates
    			+ "&strEndDate=" + mmrateStartDate
    			+ "&accommodationId=" + $('#mmratePropertyAccommodationId').val()
    			+ "&sourceTypeId="+ 1;
       		
       		spinner.show();
           	  $http(
             		   {
             			   method : 'POST',
             			   data : fdata,
             			   headers : {
             			   'Content-Type' : 'application/x-www-form-urlencoded'
             			   },
             			 url : 'get-min-max-revenue-rate-details'
             			   }).success(function(response) {
             			   
             				 $scope.mmratecontrol = response.data;
              			   	mmrateLastDate = $scope.mmratecontrol[0].rates[$scope.mmratecontrol[0].rates.length-1].date;
            			 	mmrateStartDate = $scope.mmratecontrol[0].rates[0].date;
            			 	spinner.hide();
             		   });
        	};
        	
      	$scope.mmrateDateNext = function(){
        		 var result = new Date(mmrateLastDate);
        		result.setDate(result.getDate() + 10);
        		var months = ["January","February","March","April","May","June","July","August","September","October","November","December"];
        		var month = months[result.getMonth()];
        		var year = result.getFullYear();
        		var dates = month  + " " + result.getDate() + ", " + year;
				
        		var fdata = "&strStartDate=" + mmrateLastDate
     			+ "&strEndDate=" + dates
     			+ "&accommodationId=" + $('#mmratePropertyAccommodationId').val()
     			+ "&sourceTypeId="+ 1;
            	spinner.show();
            	  $http(
              		   {
              			   method : 'POST',
              			   data : fdata,
              			   headers : {
              			   'Content-Type' : 'application/x-www-form-urlencoded'
              			   },
              			 url : 'get-min-max-revenue-rate-details'
              			   }).success(function(response) {
              				$scope.mmratecontrol = response.data;
             			   	mmrateLastDate = $scope.mmratecontrol[0].rates[$scope.mmratecontrol[0].rates.length-1].date;
	           			 	mmrateStartDate = $scope.mmratecontrol[0].rates[0].date;               			   
	           			 	spinner.hide();
              		   });
        	};
        	/* $scope.singleBlockInventory = function(available,date,id){
          	 var newData = {'startBlockDate':date,'inventoryCount':available,'propertyAccommodationId':id};
         	  $scope.roomsInfo.push(newData);
          	};
          	
          	$scope.singleBlockRates = function(tariff,date,id,sourceId){
           	 	var newData = {'startBlockDate':date,'baseAmount':tariff,'propertyAccommodationId':id,'sourceId':sourceId};
          	  	$scope.rateInfo.push(newData);
           	}; */
           	
         	$scope.singleBlockInventory = function(available,date,id,totalRooms,totalAvailable,soldRooms,indx){
        		var availRoom=parseInt(available);
        		var totalAvail=parseInt(totalRooms);
        		var soldRoom=parseInt(soldRooms);
        		var availableRooms=totalAvail-soldRoom;
        		if(availRoom<=parseInt(availableRooms)){
        			var newData = {'startBlockDate':date,'inventoryCount':available,'propertyAccommodationId':id};
               	  	$scope.roomsInfo.push(newData);
               	 /* $("#roompromoalert").hide();
				  var alertmsg = ' <div class="alert" style="color:green;text-align:center;">Inventory changed for '+date+'.</div>';
		            $('#roompromoalert').html(alertmsg);
		            $("#roompromoalert").fadeTo(2000, 500).slideUp(500, function(){
		                $("#roompromoalert").slideUp(500);
		            }); */
        		}else if(availRoom>parseInt(availableRooms)){
        			$scope.blockinventory[0].roomAvailable[indx].available=totalAvailable;
		            document.getElementById('roomId'+indx).value=totalAvailable;
        			$("#roompromoalert").hide();
					  var alertmsg = ' <div class="alert" style="color:green;text-align:center;">Maximum inventory updated.</div>';
			            $('#roompromoalert').html(alertmsg);
			            $("#roompromoalert").fadeTo(2000, 500).slideUp(500, function(){
			                $("#roompromoalert").slideUp(500);
			            });
			            
        			return;
        		}
          	 
          	};
          	
          	$scope.singleRates = function(date,id,adult,child,tariff,sellrate,netrate,netadult,netchild,type,indx){
          		
          		var tariffAmount=parseFloat(tariff);
          		var netAmount=parseFloat(netrate);
          		var adultAmount=parseFloat(adult);
          		var childAmount=parseFloat(child);
          		var netAdult=parseFloat(netadult);
          		var netChild=parseFloat(netchild);
          		          		
          		var sourceTypeId=1;
          		
          		var otarate=0,otaadult=0,otachild=0;
          		if(type=='tariff'){
          			
          			$scope.validate = true;
          			if(tariffAmount>=netAmount){
              			otarate=tariffAmount+tariffAmount*20/100;
              			document.getElementById('tariff'+indx).value=tariffAmount;
    		            document.getElementById('tariff'+indx).style.backgroundColor ="#ffffff";
    		            //document.getElementById('tariff'+indx).readOnly=true;
    		            document.getElementById('otaRate'+indx).value=Math.round(otarate);
    		            var newData = {'startBlockDate':date,'baseAmount':tariff,'propertyAccommodationId':id,'sourceTypeId':sourceTypeId,'rateExtraAdult':adult,'rateExtraChild':child};
//                   	  	$scope.rateInfo.push(newData);
              			angular.forEach($scope.rateInfo, function(value, key) {
            				if(value.startBlockDate == date){
            					value.baseAmount = available;
            					$scope.validate = false;
                      		 }
                      	});
            			if($scope.validate == true){
            				$scope.rateInfo.push(newData);
            				
            			}
              		}else if(tariffAmount<netAmount){
              			var enable=confirm("Sell Rate is "+tariffAmount+" less than Net Rate of "+netAmount);
              			if(enable){
              				otarate=netAmount+netAmount*20/100;
              				document.getElementById('tariff'+indx).value=netAmount;
              				document.getElementById('tariff'+indx).style.backgroundColor ="red";
//               				document.getElementById('tariff'+indx).readOnly=true;
    		            	document.getElementById('otaRate'+indx).value=Math.round(otarate);
    		            	var newData = {'startBlockDate':date,'baseAmount':netAmount,'propertyAccommodationId':id,'sourceTypeId':sourceTypeId,'rateExtraAdult':adult,'rateExtraChild':child};
//                       	  	$scope.rateInfo.push(newData);
              				angular.forEach($scope.rateInfo, function(value, key) {
                				if(value.startBlockDate == date){
                					value.baseAmount = available;
                					$scope.validate = false;
                          		 }
                          	});
                			if($scope.validate == true){
                				$scope.rateInfo.push(newData);
                				
                			}
              			}else{
              				otarate=tariffAmount+tariffAmount*20/100;
              				$scope.ratecontrol[0].rates[indx].tariff=tariffAmount;
        		            document.getElementById('tariff'+indx).value=tariffAmount;
        		            document.getElementById('tariff'+indx).style.backgroundColor ="#ffffff";
        		            document.getElementById('otaRate'+indx).value=Math.round(otarate);
//         		            document.getElementById('sellRateTariff'+indx).readOnly=true;
                  			 $("#sellratepromoalert").hide();
                  			var alertmsg = ' <div class="alert" style="color:green;text-align:center;">Rate under net rate.</div>';
            	            $('#sellratepromoalert').html(alertmsg);
            	            $("#sellratepromoalert").fadeTo(2000, 500).slideUp(500, function(){
            	                $("#sellratepromoalert").slideUp(500);
            	            });
              			}	
	          		}
          		}
				if(type=='adult'){
					
					$scope.validate = true;
					if(adultAmount>=netAdult){
                  		otaadult=adultAmount+adultAmount*20/100;
                  		document.getElementById('adult'+indx).value=adult;
    		            document.getElementById('adult'+indx).style.backgroundColor ="#ffffff";
    		            //document.getElementById('tariff'+indx).readOnly=true;
    	            	document.getElementById('otaAdult'+indx).value=Math.round(otaadult);
    	            	var newData = {'startBlockDate':date,'baseAmount':tariff,'propertyAccommodationId':id,'sourceTypeId':sourceTypeId,'rateExtraAdult':adult,'rateExtraChild':child};
//                   	  	$scope.rateInfo.push(newData);
              			angular.forEach($scope.rateInfo, function(value, key) {
            				if(value.startBlockDate == date){
            					value.rateExtraAdult = adult;
            					$scope.validate = false;
                      		 }
                      	});
            			if($scope.validate == true){
            				$scope.rateInfo.push(newData);
            				
            			}
              		}else if(adultAmount<netAdult){
              			var enable=confirm("Sell Adult Rate is "+adultAmount+" less than Net Rate of "+netAdult);
              			if(enable){
                      		otaadult=netAdult+netAdult*20/100;
                      		document.getElementById('adult'+indx).value=netAdult;
              				document.getElementById('adult'+indx).style.backgroundColor ="red";
//               				document.getElementById('tariff'+indx).readOnly=true;
    		            	document.getElementById('adult'+indx).style.backgroundColor ="red";
    		            	document.getElementById('otaAdult'+indx).value=Math.round(otaadult);
    		            	var newData = {'startBlockDate':date,'baseAmount':tariff,'propertyAccommodationId':id,'sourceTypeId':sourceTypeId,'rateExtraAdult':netAdult,'rateExtraChild':child};
                      	  	//$scope.rateInfo.push(newData);
              				angular.forEach($scope.rateInfo, function(value, key) {
                				if(value.startBlockDate == date){
                					value.rateExtraAdult = netAdult;
                					$scope.validate = false;
                          		 }
                          	});
                			if($scope.validate == true){
                				$scope.rateInfo.push(newData);
                				
                			}
              			}else{
                      		otaadult=adultAmount+adultAmount*20/100;
              				$scope.ratecontrol[0].rates[indx].adult=adultAmount;
        		            document.getElementById('adult'+indx).value=adult;
        		            document.getElementById('adult'+indx).style.backgroundColor ="#ffffff";
    		            	document.getElementById('otaAdult'+indx).value=Math.round(otaadult);
//         		            document.getElementById('sellRateTariff'+indx).readOnly=true;
                  			 $("#sellratepromoalert").hide();
                  			var alertmsg = ' <div class="alert" style="color:green;text-align:center;">Rate under net adult rate.</div>';
            	            $('#sellratepromoalert').html(alertmsg);
            	            $("#sellratepromoalert").fadeTo(2000, 500).slideUp(500, function(){
            	                $("#sellratepromoalert").slideUp(500);
            	            });
              			}
          			}
				}
				if(type=='child'){
					
					$scope.validate = true;
					if(childAmount>=netChild){
                  		otachild=childAmount+childAmount*20/100;
                  		document.getElementById('child'+indx).value=child;
    		            document.getElementById('child'+indx).style.backgroundColor ="#ffffff";
    		            //document.getElementById('tariff'+indx).readOnly=true;
    	            	document.getElementById('otaChild'+indx).value=Math.round(otachild);
    	            	var newData = {'startBlockDate':date,'baseAmount':tariff,'propertyAccommodationId':id,'sourceTypeId':sourceTypeId,'rateExtraAdult':adult,'rateExtraChild':child};
                  	  	//$scope.rateInfo.push(newData);
              			angular.forEach($scope.rateInfo, function(value, key) {
            				if(value.startBlockDate == date){
            					value.rateExtraChild = child;
            					$scope.validate = false;
                      		 }
                      	});
            			if($scope.validate == true){
            				$scope.rateInfo.push(newData);
            				
            			}
              		}else if(childAmount<netChild){
              			var enable=confirm("Sell Child Rate is "+childAmount+" less than Net Rate of "+netChild);
              			if(enable){
                      		otachild=netChild+netChild*20/100;
    		            	document.getElementById('child'+indx).value=netChild;
              				document.getElementById('child'+indx).style.backgroundColor ="red";
//               				document.getElementById('tariff'+indx).readOnly=true;
    		            	document.getElementById('otaChild'+indx).value=Math.round(otachild);
    		            	var newData = {'startBlockDate':date,'baseAmount':tariff,'propertyAccommodationId':id,'sourceTypeId':sourceTypeId,'rateExtraAdult':adult,'rateExtraChild':netChild};
                      	  	//$scope.rateInfo.push(newData);
              				angular.forEach($scope.rateInfo, function(value, key) {
                				if(value.startBlockDate == date){
                					value.rateExtraChild =netChild;
                					$scope.validate = false;
                          		 }
                          	});
                			if($scope.validate == true){
                				$scope.rateInfo.push(newData);
                				
                			}
              			}else{
                      		otachild=childAmount+childAmount*20/100;
              				$scope.ratecontrol[0].rates[indx].child=netChild;
              				document.getElementById('child'+indx).value=child;
        		            document.getElementById('child'+indx).style.backgroundColor ="#ffffff";
    		            	document.getElementById('otaChild'+indx).value=Math.round(otachild);
//         		            document.getElementById('sellRateTariff'+indx).readOnly=true;
                  			 $("#sellratepromoalert").hide();
                  			var alertmsg = ' <div class="alert" style="color:green;text-align:center;">Rate under net rate.</div>';
            	            $('#sellratepromoalert').html(alertmsg);
            	            $("#sellratepromoalert").fadeTo(2000, 500).slideUp(500, function(){
            	                $("#sellratepromoalert").slideUp(500);
            	            });
              			}
					}
          		
          		}

           	};
           	
           	$scope.singleBlockRates = function(tariff,date,id,sourceTypeId,adult,child,amount,netamount,firstpercent,firstinr,secondpercent,secondinr,coupons,indx){
          		var tariffAmount=parseFloat(tariff);
          		var netAmount=parseFloat(netamount);
          		var firstPercent=parseFloat(firstpercent);
          		var firstInr=parseFloat(firstinr);
          		var secondPercent=parseFloat(secondpercent);
          		var secondInr=parseFloat(secondinr);
          		var coupons=parseFloat(coupons);
          		
          		var tariffPrice=0;
          		if(firstPercent!=0){
          			tariffPrice=tariffAmount-(tariffAmount*firstPercent/100);
          		}
          		if(firstInr!=0){
          			tariffPrice=tariffAmount-firstInr;
          		}
          		if(secondPercent!=0){
          			tariffPrice=tariffPrice-(tariffPrice*secondPercent/100);
          		}
          		if(secondInr!=0){
          			tariffPrice=tariffPrice-secondInr;
          		}
          		if(coupons!=0){	
          			tariffPrice=tariffPrice-(tariffPrice*coupons/100);
          		}
          		
          		
          		if(tariffPrice>=netAmount){
          			document.getElementById('sellRateTariff'+indx).value=tariffPrice;
		            document.getElementById('sellRateTariff'+indx).style.backgroundColor ="#ffffff";
		            document.getElementById('sellRateTariff'+indx).readOnly=true;
          			var newData = {'startBlockDate':date,'baseAmount':tariff,'propertyAccommodationId':id,'sourceTypeId':sourceTypeId,'rateExtraAdult':adult,'rateExtraChild':child};
              	  	$scope.rateInfo.push(newData);	
          		}else if(tariffPrice<netAmount){
          			var enable=confirm("Sell Rate is "+tariffPrice+" less than Net Rate of "+netAmount);
          			if(enable){
          				document.getElementById('sellRateTariff'+indx).value=tariffPrice;
          				document.getElementById('sellRateTariff'+indx).style.backgroundColor ="red";
          				document.getElementById('sellRateTariff'+indx).readOnly=true;
          				var newData = {'startBlockDate':date,'baseAmount':tariff,'propertyAccommodationId':id,'sourceTypeId':sourceTypeId,'rateExtraAdult':adult,'rateExtraChild':child};
                  	  	$scope.rateInfo.push(newData);
                  	  
          			}else{
          				$scope.ratecontrol[0].rates[indx].tariff=amount;
    		            document.getElementById('tariff'+indx).value=amount;
    		            document.getElementById('sellRateTariff'+indx).value=sellRateTariff;
    		            document.getElementById('sellRateTariff'+indx).style.backgroundColor ="#ffffff";
    		            document.getElementById('sellRateTariff'+indx).readOnly=true;
              			 $("#mmratepromoalert").hide();
              			var alertmsg = ' <div class="alert" style="color:green;text-align:center;">Rate under net rate.</div>';
        	            $('#mmratepromoalert').html(alertmsg);
        	            $("#mmratepromoalert").fadeTo(2000, 500).slideUp(500, function(){
        	                $("#mmratepromoalert").slideUp(500);
        	            });
          			}
          			
    	            
          		}
           	 	
          	  /* $("#ratepromoalert").hide();
			  var alertmsg = ' <div class="alert" style="color:green;text-align:center;">Price changed for '+date+'.</div>';
	            $('#ratepromoalert').html(alertmsg);
	            $("#ratepromoalert").fadeTo(2000, 500).slideUp(500, function(){
	                $("#ratepromoalert").slideUp(500);
	            }); */
           	};
           	
           	$scope.singleBlockMMRates = function(minBase,maxBase,date,id,sourceTypeId,adult,child,netAmount,
           			minAmount,maxAmount,sellrate,otarate,occupancy,epAmount,cpAmount,ratePlanTariff,ratePlanSymbol,otaenable,indx){
          		var minbase=parseFloat(minBase);
          		var maxbase=parseFloat(maxBase);
          		var minamount=parseFloat(minAmount);
          		var maxamount=parseFloat(maxAmount);
          		var netamount=parseFloat(netAmount);
          		var rpAmount=parseFloat(ratePlanTariff);
          		var sellRateAmount=0;
          		var otaRateAmount=0;
          		var epAmount=0;
          		var occupancy=parseInt(occupancy);
          		if(occupancy==0){
          			sellRateAmount=minbase;
          		}else if(occupancy>0 && occupancy<=30){
          			sellRateAmount=minbase+(minbase*5/100);
          		}else if(occupancy>=31 && occupancy<=60){
          			sellRateAmount=minbase+(minbase*10/100)
          		}else if(occupancy>=61 && occupancy<=80){
          			sellRateAmount=minbase+(minbase*15/100)
          		}else if(occupancy>=81 && occupancy<100){
          			sellRateAmount=maxbase;
          		}
          		otaRateAmount=sellRateAmount+(sellRateAmount*15/100);
          		
          		if(ratePlanSymbol=="plus"){
          			epAmount=sellRateAmount+rpAmount;
          		}else if(ratePlanSymbol=="minus"){
          			epAmount=sellRateAmount-rpAmount;
          		}
          		
          		document.getElementById('mmsellRateTariff'+indx).value=sellRateAmount;
          		document.getElementById('mmotaRateTariff'+indx).value=otaRateAmount;
          		document.getElementById('mmepRateTariff'+indx).value=epAmount;
          		
          		var newData = {'startBlockDate':date,'baseAmount':minbase,'minimumAmount':minbase,'maximumAmount':maxbase,
          				'propertyAccommodationId':id,'sourceTypeId':sourceTypeId,'rateExtraAdult':adult,'rateExtraChild':child,
          				'sellRateAmount':sellRateAmount,'otaRateAmount':otaRateAmount,'cpAmount':cpAmount,'epAmount':epAmount,'otaEnable':otaenable};
          	  	$scope.mmrateInfo.push(newData);
          	  	
          		
           	 	
          	  
           	};
           	
          	var lastDate = 0;
          	var startDate = 0;
          	
          	var rateLastDate = 0;
          	var rateStartDate = 0;
          	var mmrateLastDate=0;
          	var mmrateStartDate=0;
          	
          	$scope.getRateSearch = function(){
          		
          		var accommodationId = $('#ratePropertyAccommodationId').val();
    	    	 if(accommodationId>0){
    	    		 if(document.getElementById('rateStartDate').value==""){
    		 			 document.getElementById('rateStartDate').focus();
    		 			document.getElementById("rateStartDate").style.borderColor = "red";
    		 		 }else if(document.getElementById('rateEndDate').value==""){
    		 			 document.getElementById('rateEndDate').focus();
    		 			document.getElementById("rateEndDate").style.borderColor = "red";
    		 		 }else if(document.getElementById('rateStartDate').value!="" && document.getElementById('rateEndDate').value!=""){
    		 			 
    		 		 
    		 			var fdata = "&strStartDate=" + $('#rateStartDate').val()
    	       			+ "&strEndDate=" + $('#rateEndDate').val()
    	       			+ "&accommodationId=" + $('#ratePropertyAccommodationId').val()
    	       			+ "&sourceTypeId="+1;
						spinner.show();
    		 			$http(
    	                		   {   
    	                			   	   
    	                			   method : 'POST',
    	                			   data : fdata,            			  
    	                			   headers : {
    	                			   'Content-Type' : 'application/x-www-form-urlencoded'
    	                			   },
    	                			  url : 'get-revenue-rate-details'   
    	                			   }).success(function(response) {
    	                			   
    	                			  	$scope.ratecontrol = response.data;
    	                			    setTimeout(function(){ spinner.hide(); 
    	                			    rateLastDate = $scope.ratecontrol[0].rates[$scope.ratecontrol[0].rates.length-1].date;
    	                			 	rateStartDate = $scope.ratecontrol[0].rates[0].date;
    	                			    }, 1000);
    	                 			   
    	                  			   	
    	                		   
    	                		   });
    		 		 }
            		
            	}
          	};
          		
			$scope.getMMRateSearch = function(){
          		
          		var accommodationId = $('#mmratePropertyAccommodationId').val();
    	    	 if(accommodationId>0){
    	    		 if(document.getElementById('mmrateStartDate').value==""){
    		 			 document.getElementById('mmrateStartDate').focus();
    		 			document.getElementById("mmrateStartDate").style.borderColor = "red";
    		 		 }else if(document.getElementById('mmrateEndDate').value==""){
    		 			 document.getElementById('mmrateEndDate').focus();
    		 			document.getElementById("mmrateEndDate").style.borderColor = "red";
    		 		 }else if(document.getElementById('mmrateStartDate').value!="" && document.getElementById('mmrateEndDate').value!=""){
    		 			 
    		 		 
    		 			var fdata = "&strStartDate=" + $('#mmrateStartDate').val()
    	       			+ "&strEndDate=" + $('#mmrateEndDate').val()
    	       			+ "&accommodationId=" + $('#mmratePropertyAccommodationId').val()
    	       			+ "&sourceTypeId="+1;
						spinner.show();
    		 			$http(
    	                		   {   
    	                			   	   
    	                			   method : 'POST',
    	                			   data : fdata,            			  
    	                			   headers : {
    	                			   'Content-Type' : 'application/x-www-form-urlencoded'
    	                			   },
    	                			  url : 'get-min-max-revenue-rate-details'   
    	                			   }).success(function(response) {
    	                			   
    	                			  	$scope.mmratecontrol = response.data;
    	                			    setTimeout(function(){ spinner.hide(); 
    	                			    mmrateLastDate = $scope.mmratecontrol[0].rates[$scope.mmratecontrol[0].rates.length-1].date;
    	                			 	mmrateStartDate = $scope.mmratecontrol[0].rates[0].date;
    	                			    }, 1000);
    	                 			   
    	                  			   	
    	                		   
    	                		   });
    		 		 }
            		
            	}
          	};	
          	
          	
        	$scope.getBulkBlockInventory = function() {
          	 	var fdata = "&startBlockDate=" + $('#StartDate').val()
   			+ "&endBlockDate=" + $('#endDate').val()
   			+ "&propertyAccommodationId=" + $('#propertyAccommodationId').val();
   		
          	 	spinner.show();
          	 	
          	  $http(
            		   {   
            			   	   
            			   method : 'POST',
            			   data : fdata,            			  
            			   headers : {
            			   'Content-Type' : 'application/x-www-form-urlencoded'
            			   },
            			  url : 'get-bulk-block-inventory'   /*   get-ota-booked-details */
            			   }).success(function(response) {
            			   
            			  
            			   $scope.progressbar.complete();
            			   //spinner.hide()
            			        setTimeout(function(){ 
            			        	
         			        	  spinner.hide();
         			        	
         						   lastDate = $scope.blockinventory[0].roomAvailable[$scope.blockinventory[0].roomAvailable.length-1].date;
                   			 	startDate = $scope.blockinventory[0].roomAvailable[0].date;
      			         
         			          }, 2000);
            			        $scope.blockinventory = response.data;
            		   
            		   });
          		
          	};
          	$scope.bulkroomupdate = "Update";
          	$scope.progressbar.complete();
          	$scope.verifyBlockInventory = function() {
          	 	var fdata = "&startBlockDate=" + $('#bookCheckin').val()
   			+ "&endBlockDate=" + $('#bookCheckout').val()
   			+ "&propertyAccommodationId=" + $('#accommodationId').val();
          	   var inventoryCount=$('#blockInventoryCount').val();
          	 $scope.bulkroomupdate = "Please Wait";
          	$scope.progressbar.start();
          	spinner.show();
          	  $http(
            		   {   
            			   	   
            			   method : 'POST',
            			   data : fdata,            			  
            			   headers : {
            			   'Content-Type' : 'application/x-www-form-urlencoded'
            			   },
            			  url : 'get-bulk-block-inventory'   /*   get-ota-booked-details */
            			   }).success(function(response) {
            				   $scope.blockroominventory = response.data;
            			
            				 
            				   setTimeout(function(){ spinner.hide(); 
            				   $scope.progressbar.complete();
            				   $scope.bulkroomupdate = "Update";
              				 }, 3000);
            				   
            				   var blnInventory=false;
            				   var blnEnable=true;
            				   
	            			   for(var i=0;i<$scope.blockroominventory[0].roomAvailable.length;i++){
	            				   if(blnEnable){
	            					   var availRoom=$scope.blockroominventory[0].roomAvailable[i].totalRoomCount;
		            				   var totalAvail=$scope.blockroominventory[0].roomAvailable[i].totalAvailable;
		            				   var soldRoom=$scope.blockroominventory[0].roomAvailable[i].sold;
		            				   var available=$scope.blockroominventory[0].roomAvailable[i].available;
		            				   var date = $scope.blockroominventory[0].roomAvailable[i].date;
		            				   var availableRooms=availRoom-soldRoom;
		            				  
		            				   if(parseInt(inventoryCount)<=parseInt(availableRooms)){
		            					   blnInventory=true;
		            					   blnEnable=true;
			                       		}else if(availRoom>parseInt(availableRooms)){
			                       			blnInventory=false;
			                       			blnEnable=false;
			                       		}  
	            				   }
	            				   
	            			   }
	            			   
	            			   if(blnInventory){
	            				   $scope.blockInventory();
	            			   }else{
	            				   
	            				   setTimeout(function(){ spinner.hide(); 
	            				   $scope.progressbar.complete();
	            				   $scope.bulkroomupdate = "Update";
	            				   $("#roominventorypromoalert").hide();
	              					  var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:green;text-align:center;">please check the inventory on '+date+'.</div>';
	              			            $('#roominventorypromoalert').html(alertmsg);
	              			            $("#roominventorypromoalert").fadeTo(2000, 500).slideUp(500, function(){
	              			                $("#roominventorypromoalert").slideUp(500);
	              			            });
	              			          $scope.bulkroomupdate = "Update";
	              			        $scope.progressbar.complete();
	              			
	              				 }, 3000);
	            			
	            			   }
            		   });
          		
          	};
          	
          	$scope.blockInventory = function(){
          		
         	   var fdata = "startBlockDate=" + $('#bookCheckin').val()
            	+ "&endBlockDate=" + $('#bookCheckout').val()
         	+ "&inventoryCount=" + $('#blockInventoryCount').val()
            	+ "&propertyAccommodationId=" + $('#accommodationId').val();
         	  spinner.show();
         		   $http(
         		   {
         			   method : 'POST',
         			   data : fdata,
         			   headers : {
         			   'Content-Type' : 'application/x-www-form-urlencoded'
         			   },
         			  url : 'block-room-inventory'
         			   }).then(function successCallback(response) {
         				  $scope.bulkroomupdate = "Update";
         				 $('#bulkupdate').modal('toggle');
         			   $scope.blockrooms = response.data;
         			  $("#roompromoalert").hide();
					  var alertmsg = ' <div class="alert" style="color:green;text-align:center;">room inventory updated.</div>';
			            $('#roompromoalert').html(alertmsg);
			            $("#roompromoalert").fadeTo(2000, 500).slideUp(500, function(){
			                $("#roompromoalert").slideUp(500);
			            });
			            $scope.getAllLogInventory();
         			  }, function errorCallback(response) {
         				 $('#bulkupdate').modal('toggle');
         				   setTimeout(function(){ spinner.hide(); 
         					 $("#roompromoalert").hide();
              				
   	   					  var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:green;text-align:center;">process failed, please try again.</div>';
   	   			            $('#roompromoalert').html(alertmsg);
   	   			            $("#roompromoalert").fadeTo(2000, 500).slideUp(500, function(){
   	   			                $("#roompromoalert").slideUp(500);
   	   			            });
   	   			      }, 3000);
         			
     					});
         	
            };
            
            
            
            
            $("#rateStartDate").datepicker({
	            dateFormat: 'MM d, yy',
	            minDate:  0,
	            onSelect: function (formattedDate) {
	                var date1 = $('#rateStartDate').datepicker('getDate'); 
	                var date = new Date( Date.parse( date1 ) ); 
	                date.setDate( date.getDate() );        
	                var newDate = date.toDateString(); 
	                newDate = new Date( Date.parse( newDate ) );   
	                $('#rateEndDate').datepicker("option","minDate",newDate);
	                $timeout(function(){
	                	document.getElementById("rateStartDate").style.borderColor = "LightGray";
	                });
	            }
	        });
	      
	      $("#rateEndDate").datepicker({
	            dateFormat: 'MM d, yy',
	            minDate:  0,
	            onSelect: function (formattedDate) {
	                var date2 = $('#rateEndDate').datepicker('getDate'); 
	                $timeout(function(){
	                	document.getElementById("rateEndDate").style.borderColor = "LightGray";
	                });
	            }
	        });
	      
	      $("#mmrateStartDate").datepicker({
	            dateFormat: 'MM d, yy',
	            minDate:  0,
	            onSelect: function (formattedDate) {
	                var date1 = $('#mmrateStartDate').datepicker('getDate'); 
	                var date = new Date( Date.parse( date1 ) ); 
	                date.setDate( date.getDate() );        
	                var newDate = date.toDateString(); 
	                newDate = new Date( Date.parse( newDate ) );   
	                $('#mmrateEndDate').datepicker("option","minDate",newDate);
	                $timeout(function(){
	                	document.getElementById("mmrateStartDate").style.borderColor = "LightGray";
	                });
	            }
	        });
	      
	      $("#mmrateEndDate").datepicker({
	            dateFormat: 'MM d, yy',
	            minDate:  0,
	            onSelect: function (formattedDate) {
	                var date2 = $('#mmrateEndDate').datepicker('getDate'); 
	                $timeout(function(){
	                	document.getElementById("mmrateEndDate").style.borderColor = "LightGray";
	                });
	            }
	        });
	      
	      $("#chartStartDate").datepicker({
	            dateFormat: 'MM d, yy',
	            
	            onSelect: function (formattedDate) {
	                var date1 = $('#chartStartDate').datepicker('getDate'); 
	                var date = new Date( Date.parse( date1 ) ); 
	              //  date.setDate( date.getDate() );        
	                var newDate = date.toDateString(); 
	                newDate = new Date( Date.parse( newDate ) );   
	                $('#chartEndDate').datepicker("option","minDate",newDate);
	                $timeout(function(){
	                	document.getElementById("chartStartDate").style.borderColor = "LightGray";
	                });
	            }
	        });
	      
	      $("#chartEndDate").datepicker({
	            dateFormat: 'MM d, yy',
	            
	            onSelect: function (formattedDate) {
	                var date2 = $('#chartEndDate').datepicker('getDate'); 
	                $timeout(function(){
	                	document.getElementById("chartEndDate").style.borderColor = "LightGray";
	                });
	            }
	        });
	      
	      $("#earlyPromoDate").datepicker({
	            dateFormat: 'MM d, yy',
	            minDate:  0,
	            onSelect: function (formattedDate) {
	                var date1 = $('#earlyPromoDate').datepicker('getDate'); 
	                var date = new Date( Date.parse( date1 ) ); 
	                date.setDate( date.getDate() );        
	                var newDate = date.toDateString(); 
	                newDate = new Date( Date.parse( newDate ) );   
	                $timeout(function(){
	                	document.getElementById("earlyPromoDate").style.borderColor = "LightGray";
	                });
	            }
	        });
	      
            $("#lastStartDate").datepicker({
	            dateFormat: 'MM d, yy',
	            minDate:  0,
	            onSelect: function (formattedDate) {
	                var date1 = $('#lastStartDate').datepicker('getDate'); 
	                var date = new Date( Date.parse( date1 ) ); 
	                date.setDate( date.getDate() );        
	                var newDate = date.toDateString(); 
	                newDate = new Date( Date.parse( newDate ) );   
	                $('#lastEndDate').datepicker("option","minDate",newDate);
	                $timeout(function(){
	                	document.getElementById("lastStartDate").style.borderColor = "LightGray";
	                });
	            }
	        });
	      
	      $("#lastEndDate").datepicker({
	            dateFormat: 'MM d, yy',
	            minDate:  0,
	            onSelect: function (formattedDate) {
	                var date2 = $('#lastEndDate').datepicker('getDate'); 
	                $timeout(function(){
	                	document.getElementById("lastEndDate").style.borderColor = "LightGray";
	                });
	            }
	        });
	      
            $("#earlyBirdStayStartDate").datepicker({
	            dateFormat: 'MM d, yy',
	            minDate:  0,
	            onSelect: function (formattedDate) {
	                var date1 = $('#earlyBirdStayStartDate').datepicker('getDate'); 
	                var date = new Date( Date.parse( date1 ) ); 
	                date.setDate( date.getDate() );        
	                var newDate = date.toDateString(); 
	                newDate = new Date( Date.parse( newDate ) );   
	                $('#earlyBirdStayStartDate').datepicker("option","minDate",newDate);
	                $timeout(function(){
	                	document.getElementById("earlyBirdStayStartDate").style.borderColor = "LightGray";
	                });
	            }
	        });
	      
	      $("#earlyBirdStayEndDate").datepicker({
	            dateFormat: 'MM d, yy',
	            minDate:  0,
	            onSelect: function (formattedDate) {
	                var date2 = $('#earlyBirdStayEndDate').datepicker('getDate'); 
	                $timeout(function(){
	                	document.getElementById("earlyBirdStayEndDate").style.borderColor = "LightGray";
	                });
	            }
	        });
	      
	      
	      $("#earlyBirdBookingStartDate").datepicker({
	            dateFormat: 'MM d, yy',
	            minDate:  0,
	            onSelect: function (formattedDate) {
	                var date1 = $('#earlyBirdBookingStartDate').datepicker('getDate'); 
	                var date = new Date( Date.parse( date1 ) ); 
	                date.setDate( date.getDate() );        
	                var newDate = date.toDateString(); 
	                newDate = new Date( Date.parse( newDate ) );   
	                $('#earlyBirdBookingStartDate').datepicker("option","minDate",newDate);
	                $timeout(function(){
	                	document.getElementById("earlyBirdBookingStartDate").style.borderColor = "LightGray";
	                });
	            }
	        });
	      
	      $("#earlyBirdBookingEndDate").datepicker({
	            dateFormat: 'MM d, yy',
	            minDate:  0,
	            onSelect: function (formattedDate) {
	                var date2 = $('#earlyBirdBookingEndDate').datepicker('getDate'); 
	                $timeout(function(){
	                	document.getElementById("earlyBirdBookingEndDate").style.borderColor = "LightGray";
	                });
	            }
	        });
	      
	      $("#flatStartDate").datepicker({
	            dateFormat: 'MM d, yy',
	            minDate:  0,
	            onSelect: function (formattedDate) {
	                var date1 = $('#flatStartDate').datepicker('getDate'); 
	                var date = new Date( Date.parse( date1 ) ); 
	                date.setDate( date.getDate() );        
	                var newDate = date.toDateString(); 
	                newDate = new Date( Date.parse( newDate ) );   
	                $('#flatEndDate').datepicker("option","minDate",newDate);
	                $timeout(function(){
	                	document.getElementById("flatStartDate").style.borderColor = "LightGray";
	                });
	            }
	        });
	      
	      $("#flatEndDate").datepicker({
	            dateFormat: 'MM d, yy',
	            minDate:  0,
	            onSelect: function (formattedDate) {
	                var date2 = $('#flatEndDate').datepicker('getDate'); 
	                $timeout(function(){
	                	document.getElementById("flatEndDate").style.borderColor = "LightGray";
	                });
	            }
	        });
	      
	      $("#modStartDate").datepicker({
	            dateFormat: 'MM d, yy',
	            minDate:  0,
	            onSelect: function (formattedDate) {
	                var date1 = $('#modStartDate').datepicker('getDate'); 
	                var date = new Date( Date.parse( date1 ) ); 
	                date.setDate( date.getDate());        
	                var newDate = date.toDateString(); 
	                newDate = new Date( Date.parse( newDate ) );   
	                $('#modEndDate').datepicker("option","minDate",newDate);
	                $timeout(function(){
	                	document.getElementById("modStartDate").style.borderColor = "LightGray";
	                });
	            }
	        });
	      
	      $("#modEndDate").datepicker({
	            dateFormat: 'MM d, yy',
	            minDate:  0,
	            onSelect: function (formattedDate) {
	                var date2 = $('#modEndDate').datepicker('getDate'); 
	                $timeout(function(){
	                	document.getElementById("modEndDate").style.borderColor = "LightGray";
	                });
	            }
	        });
	      
	      $("#modMMStartDate").datepicker({
	            dateFormat: 'MM d, yy',
	            minDate:  0,
	            onSelect: function (formattedDate) {
	                var date1 = $('#modMMStartDate').datepicker('getDate'); 
	                var date = new Date( Date.parse( date1 ) ); 
	                date.setDate( date.getDate());        
	                var newDate = date.toDateString(); 
	                newDate = new Date( Date.parse( newDate ) );   
	                $('#modMMEndDate').datepicker("option","minDate",newDate);
	                $timeout(function(){
	                	document.getElementById("modMMStartDate").style.borderColor = "LightGray";
	                });
	            }
	        });
	      
	      $("#modMMEndDate").datepicker({
	            dateFormat: 'MM d, yy',
	            minDate:  0,
	            onSelect: function (formattedDate) {
	                var date2 = $('#modMMEndDate').datepicker('getDate'); 
	                $timeout(function(){
	                	document.getElementById("modMMEndDate").style.borderColor = "LightGray";
	                });
	            }
	        });
	      
	      	
	      	
	      	$("#rateCheckAll").click(function () {
			     $("#ratecheckeddays :checkbox").not(this).prop('checked', this.checked);
			 });
	      	$("#mmrateCheckAll").click(function () {
			     $("#mmratecheckeddays :checkbox").not(this).prop('checked', this.checked);
			 });
			 $("#checkSourceAll").click(function () {
			     $("#checkedsources :checkbox").not(this).prop('checked', this.checked);
			 });
			 
            $("#lastCheckAll").click(function () {
			     $("#lastcheckeddays :checkbox").not(this).prop('checked', this.checked);
			    
			 });
            
            
            
            $("#checkSourceTypeAll").click(function () {
			     $("#checkedsourcetypes :checkbox").not(this).prop('checked', this.checked);
			 });

             $("#accommodationCheckAll").click(function () {
 			     $("#lastcheckedaccommodations :checkbox").not(this).prop('checked', this.checked);
 			    
 			 });
             
             $("#flatPropertyAll").click(function () {
			     $("#flatProperties :checkbox").not(this).prop('checked', this.checked);
			    
			 });
             
             $("#flatCheckAll").click(function () {
 			     $("#flatcheckeddays :checkbox").not(this).prop('checked', this.checked);
 			    
 			 });
             
             $("#earlyPropertyAll").click(function () {
			     $("#earlyProperties :checkbox").not(this).prop('checked', this.checked);
			    
			 });
             
             $("#earlyCheckAll").click(function () {
 			     $("#earlycheckeddays :checkbox").not(this).prop('checked', this.checked);
 			    
 			 });
             
            $("#earlyLocationId").prop("selectedIndex", -1);
             $("#earlyAreaId").prop("selectedIndex", -1);
             $("#flatLocationId").prop("selectedIndex", -1);
             $("#flatAreaId").prop("selectedIndex", -1); 
             
            /*  document.getElementById("earlyLocationId").selectedIndex = -1;
             document.getElementById("earlyAreaId").selectedIndex = -1;
             document.getElementById("flatLocationId").selectedIndex = -1;
             document.getElementById("flatAreaId").selectedIndex = -1; */
             
             $('#lastInrId').hide();
             $('#lastPercentId').hide();
             $('#flatPercentId').hide();
      		 $('#flatInrId').hide();
      		 $('#earlyPercentId').hide();
      		 $('#earlyInrId').hide();
             
             $scope.selectLastDiscount= function(type){
            	if(type=='percent'){
            		$('#lastPercentId').show();
            		$('#lastInrId').hide();
            		$('#lastInrId').val(0);
            	}else if(type=='inr'){
            		$('#lastPercentId').hide();
            		$('#lastPercentId').val(0);
            		$('#lastInrId').show();
            		
            	}
             };
             
             $scope.selectFlatDiscount= function(type){
             	if(type=='percent'){
             		$('#flatPercentId').show();
             		$('#flatInrId').hide();
             		$('#lastInrId').val(0);
             	}else if(type=='inr'){
             		$('#flatPercentId').hide();
             		$('#flatInrId').show();
             		$('#lastPercentId').val(0);
             	}
              };
              
              $scope.selectEarlyDiscount= function(type){
              	if(type=='percent'){
              		$('#earlyPercentId').show();
              		$('#earlyInrId').val(0);
              		$('#earlyInrId').hide();
              	}else if(type=='inr'){
              		$('#earlyPercentId').hide();
              		$('#earlyPercentId').val(0);
              		$('#earlyInrId').show();
              	}
               };
             
          	$scope.filterLimit = 10;
          	 $scope.roomsInfo = [];
          	 $scope.rateInfo=[];
          	$scope.mmrateInfo=[];
                	$scope.updateRoomsinv = "Save Changes";
		         	$scope.updateRooms = function(){
		         		
		         		spinner.show();
		         		
		         		var text = '{"array":' + JSON.stringify($scope.roomsInfo) + '}';
		         		var fdata = JSON.parse(text);
		         		$scope.updateRoomsinv = "Please Wait...";
		         	    $('#disableUR').addClass("dnearby");
		         		$scope.progressbar.start();
                		   $http(
                		   { 
                			   		   
                			   method : 'POST',
                			   data : fdata,
                			   dataType: 'json',
                			   headers : {
                				   'Content-Type': 'application/json; charset=utf-8'
                			   },
                			  url : "single-block-room-inventory"
                			   }).then(function successCallback(response) {
                			   
                			   $scope.singleblockroom = response.data;
                		
         			           
         			          //spinner.hide();
         			           $scope.updateRoomsinv = "Save Changes";
         			          setTimeout(function(){ 
         			        	  spinner.hide();
         			  	   $("#roompromoalert").hide();
      					  var alertmsg = ' <div class="alert alert-success alert-dismissible" style="color:green;text-align:center;">Room inventory updated Successfully.</div>';
      			            $('#roompromoalert').html(alertmsg);
      			            $("#roompromoalert").fadeTo(2000, 500).slideUp(500, function(){
      			                $("#roompromoalert").slideUp(500);
      			            });    
      			         
         			          }, 3000);
         			          
         			      	$scope.progressbar.complete();
         			           $scope.getAllLogInventory();
                			 }, function errorCallback(response) {
                				 setTimeout(function(){ spinner.hide(); 
                				 $("#roompromoalert").hide();
              					  var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:green;text-align:center;">Process failed, Please try again.</div>';
              			            $('#roompromoalert').html(alertmsg);
              			            $("#roompromoalert").fadeTo(2000, 500).slideUp(500, function(){
              			                $("#roompromoalert").slideUp(500);
              			            });
              				         $scope.updateRoomsinv = "Save Changes";
                        			  	$scope.progressbar.complete();
                				 }, 3000);
                			
           			         //spinner.hide();
           			         
           		
         					});
                		   
                    	};
                    	
                    	$scope.uppricetext = "Save Changes";
                    	$scope.updateRates = function(){
                    		 var text = '{"array":' + JSON.stringify($scope.rateInfo) + '}';
                    		var fdata = JSON.parse(text);
                    		
                    		$scope.progressbar.start();
                    		$scope.uppricetext = "Please Wait...";
                    		$("#uppricetext").prop('disabled', true);
                           		   $http(
                           		   { 
                           			   		   
                           			   method : 'POST',
                           			   data : fdata,
                           			   dataType: 'json',
                           			   headers : {
                           				   'Content-Type': 'application/json; charset=utf-8'
                           			   },
                           			  url : "add-single-rate"
                           			   }).then(function successCallback(response)  {
                           			   
                           			   $scope.singleratecontrol = response.data;
		                           			$("#ratepromoalert").hide();
		       							  var alertmsg = ' <div class="alert alert-success alert-dismissible" style="color:green;text-align:center;">Rate successfully  updated.</div>';
		       					            $('#ratepromoalert').html(alertmsg);
		       					            $("#ratepromoalert").fadeTo(2000, 500).slideUp(500, function(){
		       					                $("#ratepromoalert").slideUp(500);
		       					            });
		       					         	$scope.getRateSearch();
		       					            $scope.getAllLogRates();
		       					     	$scope.progressbar.complete();
		       					     $scope.uppricetext = "Save Changes";
		       					  $("#uppricetext").prop('disabled', false); 	 
                           			 }, function errorCallback(response) {
	                           				$("#ratepromoalert").hide();
	           							  var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:green;text-align:center;">Process failed, Please try again.</div>';
	           					            $('#ratepromoalert').html(alertmsg);
	           					            $("#ratepromoalert").fadeTo(2000, 500).slideUp(500, function(){
	           					                $("#ratepromoalert").slideUp(500);
	           					            });
	           					     	$scope.progressbar.complete();
	           					     $scope.uppricetext = "Save Changes";
	           					  $("#uppricetext").prop('disabled', false);
                    					});
                           		   
                               	};
                               	
                       	$scope.updateMMRates = function(){
                   		 var text = '{"array":' + JSON.stringify($scope.mmrateInfo) + '}';
                   		 
                   		var fdata = JSON.parse(text);
                   		$scope.progressbar.start();
                   		$scope.uppricetext = "Please Wait...";
                   		$("#uppricetext").prop('disabled', true);
                          		   $http(
                          		   { 
                          			   		   
                          			   method : 'POST',
                          			   data : fdata,
                          			   dataType: 'json',
                          			   headers : {
                          				   'Content-Type': 'application/json; charset=utf-8'
                          			   },
                          			  url : "add-min-max-single-rate"
                          			   }).then(function successCallback(response)  {
                          			   
                          			   $scope.singleratecontrol = response.data;
	                           			$("#ratepromoalert").hide();
	       							  var alertmsg = ' <div class="alert alert-success alert-dismissible" style="color:green;text-align:center;">Rate successfully  updated.</div>';
	       					            $('#ratepromoalert').html(alertmsg);
	       					            $("#ratepromoalert").fadeTo(2000, 500).slideUp(500, function(){
	       					                $("#ratepromoalert").slideUp(500);
	       					            });
	       					         	$scope.getMMRateSearch();
	       					            $scope.getAllLogRates();
	       					     	$scope.progressbar.complete();
	       					     $scope.uppricetext = "Save Changes";
	       					  $("#uppricetext").prop('disabled', false); 	 
                          			 }, function errorCallback(response) {
                           				$("#ratepromoalert").hide();
           							  var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:green;text-align:center;">Process failed, Please try again.</div>';
           					            $('#ratepromoalert').html(alertmsg);
           					            $("#ratepromoalert").fadeTo(2000, 500).slideUp(500, function(){
           					                $("#ratepromoalert").slideUp(500);
           					            });
           					     	$scope.progressbar.complete();
           					     $scope.uppricetext = "Save Changes";
           					  $("#uppricetext").prop('disabled', false);
                   					});
                          		   
                          };  	
          	
                    	$("#StartDate").datepicker({
                            dateFormat: 'MM d, yy',
                                   minDate:  0,
                            onSelect: function (formattedDate) {
                                var date1 = $('#StartDate').datepicker('getDate'); 
                                var date = new Date( Date.parse( date1 ) ); 
                               // date.setDate( date.getDate() + 1 );        
                                var newDate = date.toDateString(); 
                                newDate = new Date( Date.parse( newDate ) );   
                                $('#endDate').datepicker("option","minDate",newDate);
                                $timeout(function(){
                                  //scope.checkIn = formattedDate;
                              	  document.getElementById("StartDate").style.borderColor = "LightGray";
                                });
                            }
                        });
           
                    	
  		         $("#endDate").datepicker({
  		               dateFormat: 'MM d,yy',
  		               minDate:  0,
  		               onSelect: function (formattedDate) {
  		                   var date2 = $('#endDate').datepicker('getDate'); 
  		                   $timeout(function(){
  		                     //scope.checkOut = formattedDate;
  		                   	document.getElementById("endDate").style.borderColor = "LightGray";
  		                   });
  		               }
  		           });
         
         $("#bookCheckin").datepicker({
             dateFormat: 'M d,yy',
             minDate:  0,
             onSelect: function (dateText, inst) {
                 var date1 = $('#bookCheckin').datepicker('getDate'); 
                 var date = new Date( Date.parse( date1 ) ); 
                 date.setDate( date.getDate());        
                 var newDate = date.toDateString(); 
                 var d = new Date(dateText);
 	            //$('#calendar').fullCalendar('gotoDate', d);
                 newDate = new Date( Date.parse( newDate ) );   
                 $('#bookCheckout').datepicker("option","minDate",newDate);
                 $timeout(function(){
                   //scope.checkIn = formattedDate;
                 	document.getElementById("bookCheckin").style.borderColor = "LightGray";
                 });
             }
         });
       
       $("#bookCheckout").datepicker({
             dateFormat: 'M d,yy',
             minDate:  0,
             onSelect: function (formattedDate) {
                 var date2 = $('#bookCheckout').datepicker('getDate'); 
                 $timeout(function(){
                   //scope.checkOut = formattedDate;
                 	document.getElementById("bookCheckout").style.borderColor = "LightGray";
                 });
             }
         });
   		
   		$scope.unread = function() {
   		var notifiurl = "unreadnotifications.action";
   		$http.get(notifiurl).success(function(response) {
   			$scope.latestnoti = response.data;
   		});
   		};
   
   	 $scope.getPropertyAccommodations = function() {
   				var rowid=$('#lastPropertyId').val();	
   				var url = "get-property-accommodations?propertyId="+rowid;
   				$http.get(url).success(function(response) {
   					$scope.promotionaccommodations = response.data;
   		
   				});
   			};
   	 		
        
        $scope.getAccommodations = function() {
        	   
				var url = "get-accommodations";
				$http.get(url).success(function(response) {
				    //console.log(response);
					$scope.accommodations = response.data;
		
				});
			};
   	 		
			$scope.getAccommodationProperty = function() {
	        	   
				var url = "get-accommodations";
				$http.get(url).success(function(response) {
				    //console.log(response);
					$scope.promotionaccommodations = response.data;
		
				});
			};
	   	 	 $scope.change = function() {
	   		       	       
	   		 };
	   		 
	   		$scope.lastminutenote = "Save";
	   		$scope.addGetLastMinutePromotions = function(){
	   		 $("#lastminute").prop('disabled', true);
	   			var lastType=$('input[name=lastType]:checked').val();
	   			
	   			if(lastType=='Percent'){
	   				$('#lastInrAmount').val(0);
	   			}else if(lastType=='INR'){
	   				$('#lastPercentAmount').val(0);
	   			}else{
	   				$("#lastpromoalert").hide();
					   var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:green;text-align:center;">Please select discount type to continue!!</div>';
			            $('#lastpromoalert').html(alertmsg);
			            $("#lastpromoalert").fadeTo(2000, 500).slideUp(500, function(){
			              
			                 });
	   				$("#lastminute").prop('disabled', false); 
	   				return;
	   			}
		    	 
				    var checkbox_days_value = "";
				    var checkbox_accommodation_value="";
					    $("#lastcheckeddays :checkbox").each(function () {
					        var ischecked = $(this).is(":checked");
					        if (ischecked) {
					        	checkbox_days_value += $(this).val() + ",";
					        }
					    });
					    
					    $("#lastcheckedaccommodations :checkbox").each(function () {
					        var ischecked = $(this).is(":checked");
					        if (ischecked) {
					        	checkbox_accommodation_value += $(this).val() + ",";
					        }
					    });
				    
					    
			     	
			     	var fdata = "checkedDays=" + checkbox_days_value
			     	+ "&checkedAccommodations=" + checkbox_accommodation_value
			     	+ "&propertyId="+$('#lastPropertyId').val()
					+ "&strStartDate=" + $('#lastStartDate').val()
					+ "&strEndDate=" + $('#lastEndDate').val()
					+ "&discountType=" + lastType
					+ "&promotionName=" + $('#lastPromotionName').val()
					+ "&percentage="+ $('#lastPercentAmount').val()
					+ "&promotionInr="+ $('#lastInrAmount').val()
					+ "&hoursRange="+$('#lastHoursRangeId').val()
					+ "&promotionPropertyId="+$('#lastPropertyId').val()
					+ "&promotionType=L";
			     	
			     	
					var checkedDayslength = $('[name="checkedDay[]"]:checked').length;
			     	var checkedAll=$('[name="lastCheckAll"]:checked').length;
					
			     	var checkedAccommodationlength = $('[name="checkedAccommodation[]"]:checked').length;
			     	var checkedAccommodationAll=$('[name="accommodationCheckAll"]:checked').length;
			     	
					var isCheckedDays=false;
			     	var isCheckedCategory=false;
					if(document.getElementById('lastStartDate').value==""){
			 			 document.getElementById('lastStartDate').focus();
			 			 document.getElementById("lastStartDate").style.borderColor = "red";
			 		 }else if(document.getElementById('lastEndDate').value==""){
			 			 document.getElementById('lastEndDate').focus();
			 			 document.getElementById("lastEndDate").style.borderColor = "red";
			 		 }else if(document.getElementById('lastStartDate').value!="" && document.getElementById('lastEndDate').value!=""){
			 		
			 			 
						if((checkedDayslength>0 || checkedAll>0)){
							if(( checkedDayslength == 7 && checkedAll>0)){
								isCheckedDays=true;
							}else if(checkedDayslength>0 && checkedDayslength<=6 && checkedAll==0){
								isCheckedDays=true;
							} else{
								$("#lastpromoalert").hide();
								   var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:green;text-align:center;">Please select all days to continue!!</div>';
						            $('#lastpromoalert').html(alertmsg);
						            $("#lastpromoalert").fadeTo(2000, 500).slideUp(500, function(){
						              
						                 });
								$("#lastminute").prop('disabled', false); 
							}
						}else{
							$("#lastpromoalert").hide();
							   var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:green;text-align:center;">Please select days to continue!!</div>';
					            $('#lastpromoalert').html(alertmsg);
					            $("#lastpromoalert").fadeTo(2000, 500).slideUp(500, function(){
					              
					                 });
							$("#lastminute").prop('disabled', false); 
						}
						
						if(checkedAccommodationlength>0 || checkedAccommodationAll>0){
							isCheckedCategory=true;
						}else{
							$("#lastpromoalert").hide();
							   var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:green;text-align:center;">Please select accommodations to continue!!</div>';
					            $('#lastpromoalert').html(alertmsg);
					            $("#lastpromoalert").fadeTo(2000, 500).slideUp(500, function(){
					              
					                 });
							$("#lastminute").prop('disabled', false); 
						}
						
						if(isCheckedDays && isCheckedCategory){
							spinner.show();
							$http(
									{
										method : 'POST',
										data : fdata,
										headers : {
											'Content-Type' : 'application/x-www-form-urlencoded'
										},
										url: 'add-promotions'
										
									}).then(function successCallback(response) {
										
										var promotionchk=response.data.data[0].check;
										
										if(promotionchk=="true"){
//	 										window.location.reload();
											$("#lastpromoalert").hide();
											   var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:green;text-align:center;">Already have an Promotions.</div>';
									            $('#lastpromoalert').html(alertmsg);
									            $("#lastpromoalert").fadeTo(2000, 500).slideUp(500, function(){
									              
									                 });
										}
										else if(promotionchk=="false"){
										
											var gdata = "checkedDays=" + checkbox_days_value
									     	+ "&checkedAccommodations=" + checkbox_accommodation_value
											+ "&strStartDate=" + $('#lastStartDate').val()
											+ "&strEndDate=" + $('#lastEndDate').val()
											+ "&discountType=" + lastType
											+ "&promotionName=" + $('#lastPromotionName').val()
											+ "&percentage="+ $('#lastPercentAmount').val()
											+ "&promotionInr="+ $('#lastInrAmount').val()
											+ "&hoursRange="+$('#lastHoursRangeId').val()
											+ "&promotionPropertyId="+$('#lastPropertyId').val()
											+ "&promotionType=L";
											$scope.lastminutenote = "Please Wait...";
											spinner.show();
										$http(
									    {
										method : 'POST',
										data : gdata,
										//data : '{"data":' +JSON.stringify($scope.roomsSelected) + '}',
										
										headers : {
											'Content-Type' : 'application/x-www-form-urlencoded'
											//'Content-Type' : 'application/x-www-form-urlencoded'
										},
										url: 'add-promotion-details'
									   }).then(function successCallback(response) {
										   setTimeout(function(){
											   
											   $("#lastpromoalert").hide();
											   var alertmsg = ' <div class="alert alert-success alert-dismissible" style="color:green;text-align:center;">Last minute promotion saved successfully.</div>';
									            $('#lastpromoalert').html(alertmsg);
									            $("#lastpromoalert").fadeTo(2000, 500).slideUp(500, function(){
									              
									                 });
										    $("#lastminute").prop('disabled', false);
										    $scope.getAllPromotions();
										    $scope.lastminutenote = "Save";
										    $scope.progressbar.complete();
										    spinner.hide();
										   }, 1000);
									
									    document.getElementById("lastMinutePromotionForm").reset();
									    $scope.getAllLogPromotions();
									   }, function errorCallback(response) {
										   setTimeout(function(){
											   $("#lastpromoalert").hide();
											   var alertmsg = ' <div class="alert alert-success alert-dismissible" style="color:green;text-align:center;">Process failed , Please try again</div>';
									            $('#lastpromoalert').html(alertmsg);
									            $("#lastpromoalert").fadeTo(2000, 500).slideUp(500, function(){
									              
									                 });
									            $scope.progressbar.complete();
									            $scope.lastminutenote = "Save";
									            spinner.hide();
										   }, 2000);
										   
										   $("#lastminute").prop('disabled', false);
										  document.getElementById("lastMinutePromotionForm").reset();
										
									});
										
								}		
										
							}, function errorCallback(response) {
								   setTimeout(function(){ spinner.hide(); 
									$("#lastpromoalert").hide();
									   var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:green;text-align:center;">process failed please try again</div>';
							            $('#lastpromoalert').html(alertmsg);
							            $("#lastpromoalert").fadeTo(2000, 500).slideUp(500, function(){
							              
							                 });
							            $scope.progressbar.complete();
							            $scope.lastminutenote = "Save";
							            spinner.hide();
								   }, 2000);
					
								$("#lastminute").prop('disabled', false);
								document.getElementById("lastMinutePromotionForm").reset();
								
							});
						}
			 		 }

				};
				$scope.flatpromonote = "Save";
				$scope.addGetFlatPromotions = function(){
					$("#flatpromo").prop('disabled', true);
					
					var flatType=$('input[name=flatType]:checked').val();
		   			if(flatType=='Percent'){
		   				$('#flatInrAmount').val(0);
		   			}else if(flatType=='INR'){
		   				$('#flatPercentAmount').val(0);
		   			}else{
		   				$("#flatpromoalert").hide();
						   var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:green;text-align:center;">Please select discount type to continue!!</div>';
				            $('#flatpromoalert').html(alertmsg);
				            $("#flatpromoalert").fadeTo(2000, 500).slideUp(500, function(){
				              
				                 });
		   				$("#flatpromo").prop('disabled', false); 
		   				return;
		   			}
		   			var isChecked=false;
				    var checkbox_days_value = "";
					    $("#flatcheckeddays :checkbox").each(function () {
					        var ischecked = $(this).is(":checked");
					        if (ischecked) {
					        	checkbox_days_value += $(this).val() + ",";
					        }
					    });
					
					    var checkbox_property_value = "";
					    $("#flatProperties :checkbox").each(function () {
					        var ischecked = $(this).is(":checked");
					        if (ischecked) {
					        	checkbox_property_value += $(this).val() + ",";
					        }
					    });    
			     	
			     	var fdata = "checkedDays=" + checkbox_days_value
			     	+ "&checkedProperty=" + checkbox_property_value
					+ "&strStartDate=" + $('#flatStartDate').val()
					+ "&strEndDate=" + $('#flatEndDate').val()
					+ "&discountType=" + flatType
					+ "&promotionName=" + $('#flatPromotionName').val()
					+ "&percentage="+ $('#flatPercentAmount').val()
					+ "&promotionInr="+ $('#flatInrAmount').val()
					+ "&areaId="+ $('#flatAreaId').val()
					+ "&promotionType=F";
					
					var checkedDayslength = $('[name="checkedDay[]"]:checked').length;
			     	var checkedAll=$('[name="flatCheckAll"]:checked').length;
					
			     	var checkedPropertylength = $('[name="checkedProperty[]"]:checked').length;
			     	var checkedPropertyAll=$('[name="flatPropertyAll"]:checked').length;
			     	
			     	if(checkedPropertylength>0 || checkedPropertyAll>0){
			     		isChecked=true;
			     	}else{
			     		$("#flatpromoalert").hide();
						   var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:green;text-align:center;">Please select property to continue!!</div>';
				            $('#flatpromoalert').html(alertmsg);
				            $("#flatpromoalert").fadeTo(2000, 500).slideUp(500, function(){
				              
				                 });
		   				$("#flatpromo").prop('disabled', false); 
		   				return;
			     	}
					
					
					if(document.getElementById('flatStartDate').value==""){
			 			 document.getElementById('flatStartDate').focus();
			 			document.getElementById("flatStartDate").style.borderColor = "red";
			 		 }else if(document.getElementById('flatEndDate').value==""){
			 			 document.getElementById('flatEndDate').focus();
			 			document.getElementById("flatEndDate").style.borderColor = "red";
			 		 }else if(document.getElementById('flatStartDate').value!="" && document.getElementById('flatEndDate').value!=""){
			 			
							if(checkedDayslength>0 || checkedAll>0 ){
								if(( checkedDayslength == 7 && checkedAll>0)){
									isChecked=true;
								}else if(checkedDayslength>0 && checkedDayslength<=6 && checkedAll==0){
									isChecked=true;
								} else{
									$("#flatpromoalert").hide();
									   var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:green;text-align:center;">Please select all days to continue!!</div>';
							            $('#flatpromoalert').html(alertmsg);
							            $("#flatpromoalert").fadeTo(2000, 500).slideUp(500, function(){
							              
							                 });
								}
								
							if(isChecked){
								$scope.flatpromonote = "Please Wait...";
								$scope.progressbar.start();
								spinner.show();
							$http(
									{
										method : 'POST',
										data : fdata,
										headers : {
											'Content-Type' : 'application/x-www-form-urlencoded'
										},
										url: 'add-promotions'
									}).then(function successCallback(response) {
										var promotionchk=response.data.data[0].check;
										
										if(promotionchk=="true"){
			// 								window.location.reload();
											$("#flatpromoalert").hide();
											   var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:green;text-align:center;">Already have an Promotions.</div>';
									            $('#flatpromoalert').html(alertmsg);
									            $("#flatpromoalert").fadeTo(2000, 500).slideUp(500, function(){
									              
									                 });
										}
										else if(promotionchk=="false"){
											var gdata = "checkedDays=" + checkbox_days_value
											+ "&checkedProperty=" + checkbox_property_value
											+ "&strStartDate=" + $('#flatStartDate').val()
											+ "&strEndDate=" + $('#flatEndDate').val()
											+ "&discountType=" + flatType
											+ "&PromotionName=" + $('#flatPromotionName').val()
											+ "&Percentage="+ $('#flatPercentAmount').val()
											+ "&promotionInr="+ $('#flatInrAmount').val()
											+ "&promotionType=F";
											spinner.show();
											$http(
										    {
											method : 'POST',
											data : gdata,
											//data : '{"data":' +JSON.stringify($scope.roomsSelected) + '}',
											
											headers : {
												'Content-Type' : 'application/x-www-form-urlencoded'
												//'Content-Type' : 'application/x-www-form-urlencoded'
											},
											url: 'add-promotion-details'
										   }).then(function successCallback(response) {
											   $("#flatpromoalert").hide();
											   var alertmsg = ' <div class="alert alert-success alert-dismissible" style="color:green;text-align:center;">Flat promotion saved successfully.</div>';
									            $('#flatpromoalert').html(alertmsg);
									            $("#flatpromoalert").fadeTo(2000, 500).slideUp(500, function(){
									              
									                 });
										    $("#flatpromo").prop('disabled', false);
										    $scope.flatpromonote = "Save";
											$scope.progressbar.complete();
											 setTimeout(function(){ spinner.hide(); 
											  }, 1000);
										    $scope.getAllPromotions();
										    document.getElementById("flatPromotionForm").reset();
										    $scope.getAllLogPromotions();
										   }, function errorCallback(response) {
											   $("#flatpromoalert").hide();
											   var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:green;text-align:center;">Process failed, please try again</div>';
									            $('#flatpromoalert').html(alertmsg);
									            $("#flatpromoalert").fadeTo(2000, 500).slideUp(500, function(){
									              
									                 });
									            $scope.flatpromonote = "Save";
												$scope.progressbar.complete();
												 setTimeout(function(){ spinner.hide(); 
												  }, 1000);
											   $("#flatpromo").prop('disabled', false);
											   document.getElementById("flatPromotionForm").reset();
										});
									}
										
							}, function errorCallback(response) {
								$("#flatpromoalert").hide();
								   var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:green;text-align:center;">Process failed , please try again</div>';
						            $('#flatpromoalert').html(alertmsg);
						            $("#flatpromoalert").fadeTo(2000, 500).slideUp(500, function(){
						              
						                 });
								 $("#flatpromo").prop('disabled', false);
								 document.getElementById("flatPromotionForm").reset();
								
							});
							
							}
						}else {
							$("#flatpromoalert").hide();
							   var alertmsg = ' <div class="alert" style="color:green;text-align:center;">please select days to continue!!</div>';
					            $('#flatpromoalert').html(alertmsg);
					            $("#flatpromoalert").fadeTo(2000, 500).slideUp(500, function(){
					              
					                 });
						}
			 		 }

				};
				
				$scope.addGetEarlyPromotions = function(){
					$("#earlypromo").prop('disabled', true);
					
					var earlyType=$('input[name=earlyType]:checked').val();
		   			if(earlyType=='Percent'){
		   				$('#earlyInrAmount').val(0);
		   			}else if(earlyType=='INR'){
		   				$('#earlyPercentAmount').val(0);
		   			}else{
		   				$("#earlypromoalert").hide();
						   var alertmsg = ' <div class="alert" style="color:green;text-align:center;">please select discount type to continue!!</div>';
				            $('#earlypromoalert').html(alertmsg);
				            $("#earlypromoalert").fadeTo(2000, 500).slideUp(500, function(){
				              
				                 });
		   				$("#earlypromo").prop('disabled', false); 
		   				return;
		   			}
		   			
				    var checkbox_days_value = "";
					    $("#earlycheckeddays :checkbox").each(function () {
					        var ischecked = $(this).is(":checked");
					        if (ischecked) {
					        	checkbox_days_value += $(this).val() + ",";
					        }
					    });
					  
					    var checkbox_property_value = "";
					    $("#earlyProperties :checkbox").each(function () {
					        var ischecked = $(this).is(":checked");
					        if (ischecked) {
					        	checkbox_property_value += $(this).val() + ",";
					        }
					    });    
			     	
			     	var fdata = "checkedDays=" + checkbox_days_value
			     	+ "&checkedProperty=" + checkbox_property_value
					+ "&stayStartDate=" + $('#earlyBirdStayStartDate').val()
					+ "&stayEndDate=" + $('#earlyBirdStayEndDate').val()
					+ "&strStartDate=" + $('#earlyBirdBookingStartDate').val()
					+ "&strEndDate=" + $('#earlyBirdBookingEndDate').val()
					+ "&discountType=" + earlyType
					+ "&promotionName=" + $('#earlyPromotionName').val()
					+ "&percentage="+ $('#earlyPercentAmount').val()
					+ "&promotionInr="+ $('#earlyInrAmount').val()
					+ "&areaId="+ $('#earlyAreaId').val()
					+ "&promotionType=E";
					
					
					
					var checkedDayslength = $('[name="checkedDay[]"]:checked').length;
			     	var checkedAll=$('[name="earlyCheckAll"]:checked').length;
			     	
			     	var checkedPropertylength = $('[name="checkedProperty[]"]:checked').length;
			     	var checkedPropertyAll=$('[name="earlyPropertyAll"]:checked').length;
					
					var isChecked=false;
					
			     	if(checkedPropertylength>0 || checkedPropertyAll>0){
			     		isChecked=true;
			     	}else{
			     		$("#earlypromoalert").hide();
						   var alertmsg = ' <div class="alert" style="color:green;text-align:center;">please select property to continue!!</div>';
				            $('#earlypromoalert').html(alertmsg);
				            $("#earlypromoalert").fadeTo(2000, 500).slideUp(500, function(){
				              
				                 });
		   				$("#earlypromo").prop('disabled', false); 
		   				return;
			     	}
					
					if(document.getElementById('earlyBirdStayStartDate').value==""){
			 			 document.getElementById('earlyBirdStayStartDate').focus();
			 			document.getElementById("earlyBirdStayStartDate").style.borderColor = "red";
			 		 }else if(document.getElementById('earlyBirdStayEndDate').value==""){
			 			 document.getElementById('earlyBirdStayEndDate').focus();
			 			document.getElementById("earlyBirdStayEndDate").style.borderColor = "red";
			 		 }else if(document.getElementById('earlyBirdBookingStartDate').value==""){
			 			 document.getElementById('earlyBirdBookingStartDate').focus();
				 		 document.getElementById("earlyBirdBookingStartDate").style.borderColor = "red";
				 	 }else if(document.getElementById('earlyBirdBookingEndDate').value==""){
			 			 document.getElementById('earlyBirdBookingEndDate').focus();
				 		 document.getElementById("earlyBirdBookingEndDate").style.borderColor = "red";
				 	 }else if(document.getElementById('earlyBirdStayStartDate').value!="" && document.getElementById('earlyBirdStayEndDate').value!="" && document.getElementById('earlyBirdBookingStartDate').value!="" && document.getElementById('earlyBirdBookingEndDate').value!=""){
			 			
							if(checkedDayslength>0 || checkedAll>0 ){
								if(( checkedDayslength == 7 && checkedAll>0)){
									isChecked=true;
								}else if(checkedDayslength>0 && checkedDayslength<=6 && checkedAll==0){
									isChecked=true;
								} else{
									$("#earlypromoalert").hide();
									   var alertmsg = ' <div class="alert" style="color:green;text-align:center;">please select all days to continue!!</div>';
							            $('#earlypromoalert').html(alertmsg);
							            $("#earlypromoalert").fadeTo(2000, 500).slideUp(500, function(){
							              
							                 });
								}
								
							if(isChecked){
							spinner.show();
							
							$http(
									{
										method : 'POST',
										data : fdata,
										headers : {
											'Content-Type' : 'application/x-www-form-urlencoded'
										},
										url: 'add-promotions'
									}).then(function successCallback(response) {
										var promotionchk=response.data.data[0].check;
										
										if(promotionchk=="true"){
			// 								window.location.reload();
											$("#earlypromoalert").hide();
											   var alertmsg = ' <div class="alert" style="color:green;text-align:center;">already have an Promotions.</div>';
									            $('#earlypromoalert').html(alertmsg);
									            $("#earlypromoalert").fadeTo(2000, 500).slideUp(500, function(){
									              
									                 });
										}
										else if(promotionchk=="false"){
											var gdata = "checkedDays=" + checkbox_days_value
											+ "&checkedProperty=" + checkbox_property_value
											+ "&strStartDate=" + $('#earlyStartDate').val()
											+ "&strEndDate=" + $('#earlyEndDate').val()
											+ "&discountType=" + earlyType
											+ "&PromotionName=" + $('#earlyPromotionName').val()
											+ "&Percentage="+ $('#earlyPercentAmount').val()
											+ "&promotionInr="+ $('#earlyInrAmount').val()
											+ "&promotionType=E";
											spinner.show();
											$http(
										    {
											method : 'POST',
											data : gdata,
											//data : '{"data":' +JSON.stringify($scope.roomsSelected) + '}',
											
											headers : {
												'Content-Type' : 'application/x-www-form-urlencoded'
												//'Content-Type' : 'application/x-www-form-urlencoded'
											},
											url: 'add-promotion-details'
										   }).then(function successCallback(response) {
											   $("#earlypromoalert").hide();
											   var alertmsg = ' <div class="alert" style="color:green;text-align:center;">early bird promotion saved successfully.</div>';
									            $('#earlypromoalert').html(alertmsg);
									            $("#earlypromoalert").fadeTo(2000, 500).slideUp(500, function(){
									              
									                 });
									            setTimeout(function(){ spinner.hide(); 
									            }, 1000);
										    $("#earlypromo").prop('disabled', false);
										    $scope.getAllPromotions();
										    document.getElementById("earlyPromotionForm").reset();
										   }, function errorCallback(response) {
											   $("#earlypromoalert").hide();
											   var alertmsg = ' <div class="alert" style="color:green;text-align:center;">process failed please try again</div>';
									            $('#earlypromoalert').html(alertmsg);
									            $("#earlypromoalert").fadeTo(2000, 500).slideUp(500, function(){
									              
									                 });
									            setTimeout(function(){ spinner.hide(); 
									            }, 1000);
												$("#earlypromo").prop('disabled', false);
												document.getElementById("earlyPromotionForm").reset();
										});
									}
										
							}, function errorCallback(response) {
								$("#earlypromoalert").hide();
								   var alertmsg = ' <div class="alert" style="color:green;text-align:center;">process failed please try again</div>';
						            $('#earlypromoalert').html(alertmsg);
						            $("#earlypromoalert").fadeTo(2000, 500).slideUp(500, function(){
						              
						                 });
						            setTimeout(function(){ spinner.hide(); 
						            }, 1000);
									$("#earlypromo").prop('disabled', false);
									document.getElementById("earlyPromotionForm").reset();								
								
							});
							
							}
						}else {
							$("#earlypromoalert").hide();
							   var alertmsg = ' <div class="alert" style="color:green;text-align:center;">please select days to continue!!</div>';
					            $('#earlypromoalert').html(alertmsg);
					            $("#earlypromoalert").fadeTo(2000, 500).slideUp(500, function(){
					              
					                 });
					            setTimeout(function(){ spinner.hide(); 
					            }, 1000);
					            $("#earlypromo").prop('disabled', false);
						}
			 		 }

				};
				
   	 		$scope.getFlatAreaProperties = function() {
   	 			var areaId=$('#flatAreaId').val();
   	 			var url="get-google-area-properties?googleAreaId="+areaId;
   	 			$http.get(url).success(function(response) {
					$scope.flatAreaProperty = response.data;
		
				});
   	 		};
   	 		
   	 		$scope.getEarlyAreaProperties = function() {
		 			var areaId=$('#earlyAreaId').val();
		 			var url="get-google-area-properties?googleAreaId="+areaId;
		 			$http.get(url).success(function(response) {
					$scope.earlyAreaProperty = response.data;
		
				});
	 		};
   	 		
   		 	$scope.getDay = function() {
   				var url = "get-day";
   				$http.get(url).success(function(response) {
   					$scope.ratedays = response.data;
   		
   				});
   			};	
   			
   			$scope.getFlatDay = function() {
   				var url = "get-day";
   				$http.get(url).success(function(response) {
   					$scope.flatdays = response.data;
   		
   				});
   			};	
   			$scope.getEarlyDay = function() {
   				var url = "get-day";
   				$http.get(url).success(function(response) {
   					$scope.earlydays = response.data;
   		
   				});
   			};	
   			
   			$scope.getFlatAreas = function(){
   				var locationId=$('#flatLocationId').val();
   				var url ="get-google-location-areas?areaLocationId="+locationId;
   				$http.get(url).success(function(response) {
   					$scope.flatareas = response.data;
   				});
   			};
   			
   			$scope.getEarlyAreas = function(){
   				var locationId=$('#earlyLocationId').val();
   				var url ="get-google-location-areas?areaLocationId="+locationId;
   				$http.get(url).success(function(response) {
   					$scope.earlyareas = response.data;
   				});
   			};
   			$scope.getLocations = function() {

				var url = "get-ulo-google-locations";
				$http.get(url).success(function(response) {
					$scope.locations = response.data;
		
				});
			};
			
			$scope.getAllPromotions = function() {

				var url = "get-all-type-promotions";
				$http.get(url).success(function(response) {
					$scope.allPromotions = response.data;
		
				});
			};
			
			$scope.getActivePromotionProperty = function(){
				var promoPropertyId=$('#activePropertyId').val();
				var url = "get-property-promotions?propertyId="+promoPropertyId;
				$http.get(url).success(function(response) {
					$scope.allPromotions = response.data;
		
				});
			};
			
			$scope.getAllLogPromotions = function() {

				var url = "get-all-log-promotions";
				$http.get(url).success(function(response) {
					$scope.allPromotionLogs = response.data;
				});
			};
			
			$scope.getAllLogPromotionProperty = function(){
				var promoPropertyId=$('#promotionPropertyId').val();
				var url = "get-log-property-promotions?propertyId="+promoPropertyId;
				$http.get(url).success(function(response) {
					$scope.allPromotionLogs = response.data;
				});
			};
			
			$scope.getAllLogRates = function() {

				var url = "get-all-log-rates";
				$http.get(url).success(function(response) {
					$scope.allRateLogs = response.data;
				});
			};
			
			$scope.getAllLogRatesProperty = function(){
				var ratePropertyId=$('#ratePropertyId').val();
				var url = "get-log-property-rates?propertyId="+ratePropertyId;
				$http.get(url).success(function(response) {
					$scope.allRateLogs = response.data;
				});
			};
			
			$scope.getAllLogInventory = function() {

				var url = "get-all-log-inventory";
				$http.get(url).success(function(response) {
					$scope.allInventoryLogs = response.data;
				});
			};
			
			$scope.getAllLogInventoryProperty = function(){
				var inventoryPropertyId=$('#inventoryPropertyId').val();
				var url = "get-log-property-inventory?propertyId="+inventoryPropertyId;
				$http.get(url).success(function(response) {
					$scope.allInventoryLogs = response.data;
				});
			};
			
			
			$scope.getPromotionProperty = function() {

				var url = "get-promotion-property";
				$http.get(url).success(function(response) {
					$scope.promotionproperty = response.data;
		
				});
			};
			
			
			
			$scope.getActiveProperty = function() {

				var url = "get-active-promotion-property";
				$http.get(url).success(function(response) {
					$scope.activepromotionproperty = response.data;
		
				});
			};
			
			$scope.promoactivatebtn = "Active";
			$scope.getUpdatePromotions =function(){
				$scope.promoactivatebtn = "Please Wait...";
				$scope.progressbar.start();
				var propertyId=$('#activePropertyId').val();
				if(!propertyId){
					$("#promoalert").hide();
					   var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:green;text-align:center;">Please select the property</div>';
			            $('#promoalert').html(alertmsg);
			            $("#promoalert").fadeTo(2000, 500).slideUp(500, function(){
			              
			                 });
			            $scope.promoactivatebtn = "Active";
			            $scope.progressbar.complete();
					return;
				}
				var checkbox_promotions_ids = "";
			    $("#activecheckedpromotions :checkbox").each(function () {
			        var ischecked = $(this).is(":checked");
			        if (ischecked) {
			        	checkbox_promotions_ids += $(this).val() + ",";
			        }
			    });
			    
			  var promoIdLength=checkbox_promotions_ids.length;
			  if(promoIdLength==0){
				  checkbox_promotions_ids=0;
			  }
				var fdata = "updatePromotionIds="+ checkbox_promotions_ids+"&propertyId="+propertyId;
				$scope.promoactivatebtn = "Please Wait...";
				$scope.progressbar.start();
				spinner.show();
// 				var fdata = "&updatePromotionId="+ promoId;
				$http(
						{
							method : 'POST',
							data : fdata,
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							url: 'update-double-promotions'
						}).then(function successCallback(response) {
							var promotionchk=response.data.data[0].check;
							if(promotionchk=="true"){
								 $("#promoalert").hide();
								   var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:green;text-align:center;">Activate only two type of promotion.</div>';
						            $('#promoalert').html(alertmsg);
						            $("#promoalert").fadeTo(2000, 500).slideUp(500, function(){
						              
						                 });
						            setTimeout(function(){ spinner.hide(); 
						            }, 1000);
						            $scope.promoactivatebtn = "Active";
						            $scope.progressbar.complete();
							}else if(promotionchk=="false"){
								 $("#promoalert").hide();
								   var alertmsg = ' <div class="alert alert-success alert-dismissible" style="color:green;text-align:center;">Promotion activated successfully.</div>';
						            $('#promoalert').html(alertmsg);
						            $("#promoalert").fadeTo(2000, 500).slideUp(500, function(){
						              
						                 });
						            $scope.promoactivatebtn = "Active";
						            $scope.progressbar.complete();
						            setTimeout(function(){ spinner.hide(); 
						            }, 1000);
							}
							
						   }, function errorCallback(response) {
							   setTimeout(function(){ spinner.hide(); 
							   }, 1000);
							   $scope.promoactivatebtn = "Active";
						});
				
			};
			
			$scope.getRatePlanList = function(rowId) {
				var url = "get-rate-plan-reveune-list?accommodationId="+rowId;
				$http.get(url).success(function(response) {
					$scope.rateplanlist = response.data;
				});
			};
			
			$scope.getSources = function() {

				var url = "get-sources";
				$http.get(url).success(function(response) {
				    //console.log(response);
					$scope.sources = response.data;
		
				});
			};
			
			$scope.getAllPartnerBlock = function() {

				var url = "get-all-partner-block";
				$http.get(url).success(function(response) {
				    //console.log(response);
					$scope.partnerBlockReport = response.data;
		
				});
			};
			
			$scope.getSourceTypes = function() {

				var url = "get-source-types";
				$http.get(url).success(function(response) {
				    //console.log(response);
					$scope.sourceTypes = response.data;
		
				});
			};
			
			$scope.selectAll = function() {
		          angular.forEach($scope.days, function(d) {
		            d.Selected = $scope.selectedAll;
		          });
		        };

		        // use the array "every" function to test if ALL items are checked
		        $scope.checkIfAllSelected = function() {
		          $scope.selectedAll = $scope.days.every(function(d) {
		            return d.Selected == true;
		          })
		        }; 
		        
		        /* Days Types checkbox */
		         $scope.selectRateAllDays = function() {
		          angular.forEach($scope.ratedays, function(rd) {
		            rd.Selected = $scope.selectedRateAllDays;
		          });
		        };

		        // use the array "every" function to test if ALL items are checked
		        $scope.checkIfRateAllSelectedDays = function() {
		          $scope.selectedRateAllDays = $scope.ratedays.every(function(rd) {
		            return rd.Selected == true;
		          })
		        };
		           $scope.selectFlatAllDays = function() {
		          angular.forEach($scope.flatdays, function(fd) {
		            fd.Selected = $scope.selectedFlatAllDays;
		          });
		        };

		        // use the array "every" function to test if ALL items are checked
		        $scope.checkIfFlatAllSelectedDays = function() {
		          $scope.selectedFlatAllDays = $scope.flatdays.every(function(fd) {
		            return fd.Selected == true;
		          })
		        };
		        
		          $scope.selectEarlyAllDays = function() {
			          angular.forEach($scope.earlydays, function(ed) {
			            ed.Selected = $scope.selectedEarlyAllDays;
			          });
			        };

			        // use the array "every" function to test if ALL items are checked
			        $scope.checkIfEarlyAllSelectedDays = function() {
			          $scope.selectedEarlyAllDays = $scope.earlydays.every(function(ed) {
			            return ed.Selected == true;
			          })
			        };
		        
			$scope.getSourceTypes();
			$scope.getSources();
			$scope.getAllPromotions();
			$scope.getLocations();
   			$scope.getDay();
   			$scope.getEarlyDay();
   			$scope.getFlatDay();
   	   	 	$scope.getAccommodations();
   	   	    $scope.getAccommodationProperty();
   	    	$scope.getPromotionProperty();
   	    	$scope.getActiveProperty();
   	    	$scope.getAllLogPromotions();
   	    	$scope.getAllLogRates();
   	    	$scope.getAllLogInventory();
   	    	$scope.getAllPartnerBlock();
   	});
   	
   	app.directive('ngModelOnblur', function() {
   	    return {
   	        restrict: 'A',
   	        require: 'ngModel',
   	        priority: 1, // needed for angular 1.2.x
   	        link: function(scope, elm, attr, ngModelCtrl) {
   	            if (attr.type === 'radio' || attr.type === 'checkbox') return;

   	            elm.unbind('input').unbind('keydown').unbind('change');
   	            elm.bind('blur', function() {
   	                scope.$apply(function() {
   	                    ngModelCtrl.$setViewValue(elm.val());
   	                });         
   	            });
   	        }
   	    };
   	});
</script>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="dist/js/pages/revenue-chart.js"></script>
  <style type="text/css">
    #ui-datepicker-div
    {
        z-index: 9999999 !important;
    }
</style>
<style type="text/css">

</style>
<script src="http://code.jquery.com/jquery.js"></script>
<script>

</script>
