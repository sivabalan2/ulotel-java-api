<section id="innerPageContent">
   <!-- Tab Header -->
   <div class="headingBar">
      <div class="container">
         <div class="row">
            <div class="col-sm-12">
               <ul class="tabMenu nav nav-tabs">
                  <li>
                     <a id="team" href="javascript:void(0)">
                     <i class="fa fa-users"></i><br />
                     <span>Team</span>
                     </a>
                  </li>
               </ul>
            </div>
         </div>
      </div>
   </div>
   <!-- Tab Header -->
   <div class="container tabBG">
      <div class="row">
         <div class="col-sm-12">
            <!-- Our Team Tab -->
            <div class="tabContent" id="teamTab" >
               <h1 class="heading1">Our <span>Team</span></h1>
               <div class="row">
                  <div class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
                     <div class="teamCEO boxShadow">
                        <div class="image"><img src="ulowebsite/images/team/viswa.png" class="img-circle" /></div>
                        <div class="details">
                           <span><strong>Viswanathan M</strong><br />CEO</span>
                           <p>Has 6+ years in the Travel and Tourism Sector & handles Operations, marketing, strategy, financing, human resources and more.</p>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="row ourTeam">
                  <div class="col-sm-4">
                     <div class="teamMembers boxShadow">
                        <div class="image">
                           <img src="ulowebsite/images/team/muruga.png" class="img-circle" />
                        </div>
                        <div class="details">
                           <span><strong>Muruga j</strong><br />Chief Mentor</span>
                           <p>Has 2 decades experience on multiple technical domains. He values teamwork and is a guiding star of the entire team.</p>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-4">
                     <div class="teamMembers boxShadow">
                        <div class="image">
                           <img src="ulowebsite/images/team/vino.png" class="img-circle" />
                        </div>
                        <div class="details">
                           <span><strong>Vinodhini</strong><br />Head-Operation</span>
                           <P>Holds a degree in Commerce and takes care of the Operations team. She experiments with new ideas to satisfy guests. </P>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-4">
                     <div class="teamMembers boxShadow">
                        <div class="image">
                           <img src="ulowebsite/images/team/gopi.png" class="img-circle" />
                        </div>
                        <div class="details">
                           <span><strong>Gopinath</strong><br />Senior Developer</span>
                           <P>Has a Master degree in Computer Science and heads the Web Development Team. He is a pro at coding. </P>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row ourTeam">
                  <div class="col-sm-4">
                     <div class="teamMembers boxShadow">
                        <div class="image">
                           <img src="ulowebsite/images/team/pravin.png" class="img-circle" />
                        </div>
                        <div class="details">
                           <span><strong>Pravin</strong><br />SEO Analyst</span>
                           <P>Holds a Master degree in Computer Applications and heads the Digital Marketing Team. He plans & creates marketing strategies. </P>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-4">
                     <div class="teamMembers boxShadow">
                        <div class="image">
                           <img src="ulowebsite/images/team/dwarakesh.png" class="img-circle" />
                        </div>
                        <div class="details">
                           <span><strong>Dwarakesh</strong><br />Web Developer</span>
                           <P>Has a degree in Information Technology with a flair of new computer languages. He is responsible for the software Ulo uses</P>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-4">
                     <div class="teamMembers boxShadow">
                        <div class="image">
                           <img src="ulowebsite/images/team/siva.png" class="img-circle" />
                        </div>
                        <div class="details">
                           <span><strong>SivaBalan</strong><br />Web Developer</span>
                           <P>Graduated with a degree in Computer Science and does effective management of reservations to increase the occupancy and revenue. </P>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row ourTeam">
                  <div class="col-sm-4">
                     <div class="teamMembers boxShadow">
                        <div class="image">
                           <img src="ulowebsite/images/team/vikram.png" class="img-circle" />
                        </div>
                        <div class="details">
                           <span><strong>Vikram</strong><br />Reservation Manager</span>
                           <P>Graduated with a degree in Computer Science and does effective management of reservations to increase the occupancy and revenue. </P>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-4">
                     <div class="teamMembers boxShadow">
                        <div class="image">
                           <img src="ulowebsite/images/team/naga.png" class="img-circle" />
                        </div>
                        <div class="details">
                           <span><strong>Nagalingam</strong><br />Contract Manager</span>
                           <P> A Computer Science graduate, he takes care of the logistics related to operations and scaling up in Ulo Hotels. </P>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-4">
                     <div class="teamMembers boxShadow">
                        <div class="image">
                           <img src="ulowebsite/images/team/rahul.png" class="img-circle" />
                        </div>
                        <div class="details">
                           <span><strong>Rahul</strong><br />Digital Marketing Executive</span>
                           <P>Graduated with a degree in Information Technology, he is responsible for making sure Ulo reaches the target audience. </P>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row ourTeam">
                  <div class="col-sm-4">
                     <div class="teamMembers boxShadow">
                        <div class="image">
                           <img src="ulowebsite/images/team/bala.png" class="img-circle" />
                        </div>
                        <div class="details">
                           <span><strong>Bala Murugan</strong><br />Creative Designer</span>
                           <P>Graduated with a degree in Computer Science and does effective management of reservations to increase the occupancy and revenue. </P>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-4">
                     <div class="teamMembers boxShadow">
                        <div class="image">
                           <img src="ulowebsite/images/team/abinaya.png" class="img-circle" />
                        </div>
                        <div class="details">
                           <span><strong>Abinaya</strong><br />Content Writer & Media Coordinator</span>
                           <P> A Computer Science graduate, he takes care of the logistics related to operations and scaling up in Ulo Hotels. </P>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-4">
                     <div class="teamMembers boxShadow">
                        <div class="image">
                           <img src="ulowebsite/images/team/mohan.png" class="img-circle" />
                        </div>
                        <div class="details">
                           <span><strong>Mohanraj</strong><br />Senior Developer</span>
                           <P>Graduated with a degree in Information Technology, he is responsible for making sure Ulo reaches the target audience. </P>
                        </div>
                     </div>
                  </div>
               </div>
            
               <div class="row ourTeam">
                  <div class="col-sm-4">
                     <div class="teamMembers boxShadow">
                        <div class="image">
                           <img src="ulowebsite/images/team/sajith.png" class="img-circle" />
                        </div>
                        <div class="details">
                           <span><strong>Sajith</strong><br />Revenue & Price Manager</span>
                           <P>With 6+ years experience in Management, he analyses the reservation charts, follows up with the guests & manages the price strategy</P>
                        </div>
                     </div>
                  </div>
                  <div class="col-sm-4">
                     <div class="teamMembers boxShadow">
                        <div class="image">
                           <img src="ulowebsite/images/team/maheswari.png" class="img-circle" />
                        </div>
                        <div class="details">
                           <span><strong>Maheshwari</strong><br />Guest Support Executive</span>
                           <P>With a degree in Computer Science, she works with the reservation team to bring more revenue and Occupancy to Ulo. </P>
                        </div>
                     </div>
                  </div>
                       <div class="col-sm-4">
                     <div class="teamMembers boxShadow">
                        <div class="image">
                           <img src="ulowebsite/images/team/rajii.png" class="img-circle" />
                        </div>
                        <div class="details">
                           <span><strong>Rajalakshmi</strong><br />Guest Support Executive</span>
                           <P>Graduated with a degree in Aeronautical Engineering, she interacts with the guests, gives them suggestions about choosing their destinations. </P>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- //Our Team Tab -->
         </div>
      </div>
   </div>
</section>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script src="js1/ngprogress.js" type="text/javascript"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script> 
<script>
   var app = angular.module('myApp', ['ngProgress']);
   app.controller('customersCtrl', function ($scope, $http, $timeout, ngProgressFactory) {
    $scope.userData = [];
   
    	$scope.login = function () {
   
   
    		var fdata = "username=" + $('#username').val() +
    			"&password=" + $('#password').val();
    		//alert(fdata);
   
    		$http({
    			method: 'POST',
    			data: fdata,
    			headers: {
    				'Content-Type': 'application/x-www-form-urlencoded'
    			},
    			url: 'ulologin'
   
    		}).then(function successCallback(response) {
    			$scope.userData = response.data;
    			console.log($scope.userData);
    			var username = $scope.userData.data[0].userName;
    			var email = $scope.userData.data[0].emailId;
    			var phone = $scope.userData.data[0].phone;
   
    			$('#loginModal').modal('toggle');
    			$('#logSec').html("<div class='mnuContant'><a href='mailto:support@ulohotels.com' title='support@ulohotels.com'><i class='fa fa-envelope'></i><span class='visibleLG'>support@ulohotels.com</span></a><a href='tel:9543592593' title=''+91 95435 92593'><i class='fa fa-phone'></i><span class='visibleLG'>+91 95435 92593</span></a></div><div class='mnuProfile'><div class='mpAvatar hidden-xs' style='background:url(ulowebsite/images/avatar.png);'></div><div class='mpUsername'><a href='javascript:showpro();' class=' mnuProfileOpen'>Hi admin<i class='fa fa-angle-down'></i></a></div><div class='mpSubMenu' id='showmenu' ><ul><li><a href='ulouserprofile'>Profile</a></li><li><a href='ulouserprofile'>Booking History</a></li><li><a href='ulologout'>Log out</a></li></ul></div></div>");
    		}, function errorCallback(response) {
   
    			var alert = ' <div class="alert alert-danger alert-dismissible" style="color:red">Invalid username or password.</div>';
    			$('#loginerror').html(alert);
    		    $(function() {
    	          // setTimeout() function will be fired after page is loaded
    	          // it will wait for 5 sec. and then will fire
    	          // $("#successMessage").hide() function
    	          setTimeout(function() {
    	              $("#loginerror").hide('blind', {}, 100)
    	          }, 5000);
    	      });
    		});
   
    	};
   
    	$scope.signup = function () {
   
   
    		var fdata = "userName=" + $('#userName').val() +
    			"&phone=" + $('#mobilePhone').val() +
    			"&emailId=" + $('#emailId').val() +
    			"&roleId=" + $('#roleId').val() +
    			"&accessRightsId=" + $('#accessRightsId').val() +
    			"&password=" + $('#signpassword').val();
    		//alert(fdata);
   
    		$http(
   
    			{
    				method: 'POST',
    				data: fdata,
    				headers: {
    					'Content-Type': 'application/x-www-form-urlencoded'
    				},
    				url: 'ulosignup'
    			}).then(function successCallback(response) {
   
   
    			if (response.data == "") {
   
    				var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:red">Sorry This Email Id already exist.</div>';
    				$('#signuperror').html(alertmsg);
   
   
    			} else {
   
    				$scope.userData = response.data;
    				var username = $scope.userData.data[0].userName;
    				var email = $scope.userData.data[0].emailId;
    				var phone = $scope.userData.data[0].phone;
   
   
    				$('#registerModal').modal('toggle');
   
    				$('#logSec').html("<div class='mnuContant'><a href='mailto:support@ulohotels.com' title='support@ulohotels.com'><i class='fa fa-envelope'></i><span class='visibleLG'>support@ulohotels.com</span></a><a href='tel:9543592593' title=''+91 95435 92593'><i class='fa fa-phone'></i><span class='visibleLG'>+91 95435 92593</span></a></div><div class='mnuProfile'><div class='mpAvatar hidden-xs' style='background:url(ulowebsite/images/avatar.png);'></div><div class='mpUsername'><a href='javascript:showpro();' class=' mnuProfileOpen'>Hi admin<i class='fa fa-angle-down'></i></a></div><div class='mpSubMenu' id='showmenu' ><ul><li><a href='ulouserprofile'>Profile</a></li><li><a href='ulouserprofile'>Booking History</a></li><li><a href='ulologout'>Log out</a></li></ul></div></div>");
    			}
   
    		}, function errorCallback(response) {
   
   
    			var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:red">This Email ID Already Exist</div>';
    			$('#signuperror').html(alertmsg);
   
    			// or server returns response with an error status.
    		});
   
    	};
   
   
    	$scope.passwordRequest = function () {
   
   
    		var fdata = "emailId=" + $('#forgetEmailId').val();
   
   
    		$http({
    			method: 'POST',
    			data: fdata,
    			headers: {
    				'Content-Type': 'application/x-www-form-urlencoded'
    			},
    			url: 'ulosendpassword'
    		}).then(function successCallback(response) {
   
    			var alertmsg = ' <div class="alert alert-success alert-dismissible" style="color:red">Password Sent Sucessfully.</div>';
   
    			$('#forgetmessage').html(alertmsg);
   
   
    		}, function errorCallback(response) {
   
   
    			var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:red">Your Email ID does not Registred Yet</div>';
    			$('#forgetmessage').html(alertmsg);
   
   
    		});
   
    	};
   
    	$('#forgetPanel').hide();
    	$scope.showForget = function () {
   
    		$('#loginPanel').hide();
    		$('#forgetPanel').show();
   
   
    	};
   
    	$scope.showLogin = function () {
   
    		$('#forgetPanel').hide();
    		$('#loginPanel').show();
   
   
    	};
   
   $timeout(function () {
     	$('#btnclose').click();
     }, 1000);
     $timeout(function () {
     	$('#btncloses').click();
     }, 1000);
   
   });
   
   
</script>