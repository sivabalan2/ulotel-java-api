<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="s" uri="/struts-tags"%>

   <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <section class="content-header">
          <h1>
           Hotel Booking Report
            <!--<small>Subscription Expiry on {{property[0].subscription[0].expirydate}}</small>-->
          </h1>
          <ol class="breadcrumb">
            <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Report</a></li>
            <li class="active">Hotel Booking Report</li>
          </ol>
        </section>
 		
 		<!-- Main content -->
    <section class="content">
    <div class="box box-default color-palette-box">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-tag"></i> Search &amp; Filter</h3>
        </div>
        <div class="box-body">
        <form>
          <div class="row">
          <div class="col-sm-2 col-md-3">
           <div class="form-group">
                <label>From Date:</label>

                <div class="input-group date">
                <label class="input-group-addon btn" for="fromDate">
					<span class="fa fa-calendar"></span>
				</label>
                  <input type="text" class="form-control pull-right" id="fromDate" name="fromDate" value="" onkeypress="return false;" autocomplete="off">
                </div>
              </div>
              
              </div>
              <div class="col-sm-2 col-md-3">
           <div class="form-group">
                <label>To Date:</label>

                <div class="input-group date">
                  <label class="input-group-addon btn" for="toDate">
					<span class="fa fa-calendar"></span>
				</label>
                  <input type="text" class="form-control pull-right" id="toDate" name="toDate" value="" onkeypress="return false;" autocomplete="off">
                </div>
                <!-- /.input group -->
              </div>
              
              </div>
              <div class="col-sm-2 col-md-2">
              <div class="form-group">
                <label>Accommodation Type</label>
                <select class="form-control select2" style="width: 100%;" id="accommodationId" name="accommodationId">
                  <option selected="selected" value="0">All</option>
                  <option ng-repeat="acc in accommodations" value="{{acc.accommodationId}}">{{acc.accommodationType}}</option>
                </select>
              </div>
              </div>
              
              <div class="form-group col-md-2">
				<label>Booking Id</label>
				<input type="text"  name="filterBookingId" class="form-control" id="filterBookingId" ng-pattern="/^[0-9]/" maxlength="10" placeholder="Booking Id">
			  </div>
			  
              <div class="col-sm-2 col-md-2">
              
              <div class="form-group">
<!--               <label>Search</label> -->
				<label>&nbsp;</label>
              	<button type="submit" ng-click="getResortBookingList()" class="btn btn-danger pull-right btn-block btn-sm">Search</button>
              	</div>
              </div>
              
              
          </div>
           <div class="row">
	          	<div class="form-group col-md-2">
					<input type="radio" name="filterDate" id="filterBookingDate" value="BookingDate" placeholder="Booking Date" checked>
					<label>Booking Date</label>
				</div>
				<div class="form-group col-md-2">
					<input type="radio" name="filterDate" id="filterCheckInDate" value="CheckInDate" placeholder="Check In Date">
					<label>Check In Date</label>
				</div>
				<div class="form-group col-md-2">
					<input type="radio"  name="filterDate" id="filterCheckOutDate" value="CheckOutDate" placeholder="Check Out Date">
					<label>Check Out Date</label>
				</div>
				<div class="form-group col-md-2">
					<input type="radio"  name="filterDate" id="filterStayInfo" value="CheckStayInfo" placeholder="Stay Info">
					<label>Stay Info</label>
				</div>
				<div class="col-sm-2 col-md-2">
              	<div class="form-group">
<!--               	<label>Download</label> -->
<!-- 				<label>&nbsp;</label> -->
<!--               	<button type="button" ng-click="getExportBookingList()" class="btn btn-danger pull-right btn-block btn-sm">Download</button> -->
				<button class="btn btn-danger pull-right btn-block btn-sm" ng-csv="resortbookinglist" filename="resortbookingreport.csv" 
				csv-header="['Booking Id','Guest Name','Mobile Number','Email Id','Booking Date','Check-In','Check-Out','Accommodation Type',
				'Rooms','Nights','Room Nights','No. of Adult','No. of Child','Actual Amount','Hotel Amount','Tax','Amount','Varying Amount','Status']" field-separator="," decimal-separator=".">Export to CSV</button>
              	</div>
              </div>
				
          </div> 
        
          <!-- /.row -->
          </form>
        </div>
        <!-- /.box-body -->
      </div>
      
      
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Results</h3>

              
            </div>
            <!-- /.box-header -->
            <div  class="box-body table-responsive no-padding" style="height:300px;"  ng-if="resortbookinglist.length==0">
            	<table class="table table-hover">
               <tr>
                	<td  colspan="8">No Records found
                	</td>
                </tr>
                </table>
            </div>
            <div class="box-body table-responsive no-padding" style="height:300px;" ng-if="resortbookinglist.length>0">
              <table class="table table-hover ">
                <tr>
                  <th style="overflow:hidden;white-space:nowrap;">Booking ID</th>
                  <th>Guest</th>
                  <th>Mobile</th>
                  <th>Email</th>
                  <th style="overflow:hidden;white-space:nowrap;">Booking Date</th>
                  <th style="overflow:hidden;white-space:nowrap;">Check-In</th>
                  <th style="overflow:hidden;white-space:nowrap;">Check-Out</th>
                  <th>Accommodations</th>
                  <th>Rooms</th>
                  <th>Nights</th>
                  <th style="overflow:hidden;white-space:nowrap;">Room Nights</th>
                  <th>Adult</th>
                  <th>Child</th>
                  <th>Actual Amount</th>
                  <th>Hotel Amount</th>
                  <th>Tax</th>
                  <th>Amount</th>
                  <th>Varying Amount</th>
                  <th>Status</th>
                </tr>
                <tr ng-repeat="book in resortbookinglist">
                  <td>{{book.bookingId}}</td>
                  <td>{{book.guestName}}</td>
                  <td>{{book.mobileNumber}}</td>
                  <td>{{book.emailId}}</td>
                  <td style="overflow:hidden;white-space:nowrap;">{{book.bookingDate}}</td>
                  <td style="overflow:hidden;white-space:nowrap;">{{book.checkIn}}</td>
                  <td style="overflow:hidden;white-space:nowrap;">{{book.checkOut}}</td>
                  <td>{{book.accommodationType}}</td>
                  <td>{{book.rooms}}</td>
                  <td>{{book.diffDays}}</td>
                  <td>{{book.roomNights}}</td>
                  <td>{{book.adultCount}}</td>
                  <td>{{book.childCount}}</td>
                  <td>{{book.actualAmount}}</td>
                  <td>{{book.resortOfferAmount}}</td>
                  <td>{{book.taxAmount}}</td>
                  <td>{{book.totalAmount}}</td>
                  <td>{{book.actualTotalAmount}}</td>
                  <td>{{book.status}}</td>
                </tr>
                	
                
                
             </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
      
    </section>
 		
 		
</div>
    <input type="hidden" value ="<%=session.getAttribute("propertyId") %>" id="propertyId" name="propertyId" >
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>
<!-- csv download script start -->
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.15/angular-sanitize.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ng-csv/0.3.6/ng-csv.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.0rc1/angular-route.min.js"></script>
<!-- csv download script end -->
	 <script src="js/jquery-2.1.1.js" type="text/javascript"></script>

	<script>
	var app = angular.module('myApp', ['ngProgress','ngSanitize', 'ngCsv']);
	app.controller('customersCtrl', function($scope, $http,$timeout,ngProgressFactory) {
		
		  $("#fromDate").datepicker({
		      dateFormat: 'MM d, yy',
		    
		      onSelect: function (formattedDate) {
		          var date1 = $('#fromDate').datepicker('getDate'); 
		          var date = new Date( Date.parse( date1 ) ); 
		         // date.setDate( date.getDate() + 1 );        
		          var newDate = date.toDateString(); 
		          newDate = new Date( Date.parse( newDate ) );   
		          $('#toDate').datepicker("option","minDate",newDate);
		          $timeout(function(){
		        	  document.getElementById("fromDate").style.borderColor = "LightGray";
		          });
		      }
		  });

		  $("#toDate").datepicker({
		      dateFormat: 'MM d, yy',
		   
		      onSelect: function (formattedDate) {
		          var date2 = $('#toDate').datepicker('getDate'); 
		          $timeout(function(){
		        	  document.getElementById("toDate").style.borderColor = "LightGray";
		          });
		      }
		  }); 
		
		
		/*  
		$scope.getResortBookingList = function(){
				
				var fdata = "strFromDate=" + $('#fromDate').val()+ "&strToDate=" + $('#toDate').val()+ "&accommodationId="+$('#accommodationId').val();
				if(document.getElementById('fromDate').value==""){
		 			 document.getElementById('fromDate').focus();
		 			 document.getElementById("fromDate").style.borderColor = "red";
		 		 }else if(document.getElementById('toDate').value==""){
		 			 document.getElementById('toDate').focus();
		 			document.getElementById("toDate").style.borderColor = "red";
		 		 }else if(document.getElementById('fromDate').value!="" && document.getElementById('toDate').value!=""){
	    			    		
					$http(
							{
								method : 'POST',
								data : fdata,
								headers : {
									'Content-Type' : 'application/x-www-form-urlencoded'
								},
								url : 'resort-booking'
							}).success(function(response) {
								
								$scope.resortbookinglist = response.data;
								
							});
		 		 }
			}; */
			
		  $scope.getResortBookingList = function(){
				var filterDateName="",filterBookingId="", displayReport="";
				if(document.getElementById('filterBookingDate').checked){
					filterDateName='BookingDate';
				}
				if(document.getElementById('filterCheckInDate').checked){
					filterDateName='CheckInDate';
				}
				if(document.getElementById('filterCheckOutDate').checked){
					filterDateName='CheckOutDate';
				}
				if(document.getElementById('filterStayInfo').checked){
					filterDateName='StayInfo';
				}
				filterBookingId=document.getElementById('filterBookingId').value;
				if(filterBookingId==""){
					filterBookingId="0";
					displayReport="DateFilter";
				}else{
					displayReport="BookingIdFilter";
				}
				if(displayReport=="DateFilter"){
					
					var fdata = "strFromDate=" + $('#fromDate').val()+ "&strToDate=" + $('#toDate').val()
					+ "&accommodationId="+$('#accommodationId').val()+"&filterDateName="+filterDateName
					+'&filterBookingId='+filterBookingId+"&propertyId="+$('#propertyId').val();
					if(document.getElementById('fromDate').value==""){
			 			 document.getElementById('fromDate').focus();
			 			 document.getElementById("fromDate").style.borderColor = "red";
			 		 }else if(document.getElementById('toDate').value==""){
			 			 document.getElementById('toDate').focus();
			 			document.getElementById("toDate").style.borderColor = "red";
			 		 }else if(document.getElementById('fromDate').value!="" && document.getElementById('toDate').value!=""){
		    			    		
						$http(
								{
									method : 'POST',
									data : fdata,
									headers : {
										'Content-Type' : 'application/x-www-form-urlencoded'
									},
									url : 'resort-booking'
								}).success(function(response) {
									
									$scope.resortbookinglist = response.data;
									
								});
			 		 }
				}else if(displayReport=="BookingIdFilter"){
					var fdata = '&filterBookingId='+filterBookingId+"&propertyId="+$('#propertyId').val();	 
					$http(
							{
								method : 'POST',
								data : fdata,
								headers : {
									'Content-Type' : 'application/x-www-form-urlencoded'
								},
								url : 'resort-booking'
							}).success(function(response) {
								
								$scope.resortbookinglist = response.data;
								
							});
					}
			};
			
			var propertyId = $('#propertyId').val(); 	
			$scope.getAccommodations = function() {

				var url = "get-front-accommodations?propertyId="+propertyId;
				$http.get(url).success(function(response) {
				    //console.log(response);
					$scope.accommodations = response.data;
		
				});
			};
			
			
			$scope.getAccommodations();
			
			
			
		
	});
		</script>
		
		<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
		  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
		  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		  <script src="plugins/datepicker/bootstrap-datepicker.js"></script>
		  
		  <script>
 
//Date picker

  

  </script>
