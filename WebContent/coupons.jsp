<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!-- <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/themes/south-street/jquery-ui.min.css" />
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <small>Create Coupon</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Create Coupon</li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="nav-tabs-custom">
          <ul class="nav nav-tabs">
            <li class="active"><a href="#fa-icons" data-toggle="tab">Create Coupon</a></li>
            <li class=""><a href="#fa-iconstwo" data-toggle="tab">View Coupon</a></li>
          </ul>
          <div class="tab-content">
            <!-- Font Awesome Icons -->
            <div class="tab-pane active" id="fa-icons">
            <!-- create coupon begins-->
              <section class="content">
                <div class="row">
                 <form name="createcoupon" id="createcoupon">
                 <div class="col-sm-4 form-group">
                 	<label>Coupon Applicable</label>
				    <select name="couponType" data-allow-clear="true" ng-model="couponType" id="couponType" ng-change="getLocationDiv(couponType)" class="form-control"  ng-required="true" autocomplete="off">
				    <option disabled selected value> &nbsp;-- Coupon Type --&nbsp; </option>
                   <option value="UW">Own Channel</option>
                   <option value="RD">City/Area Wise</option>
                   <option value="OW">Affiliate Website</option>
	               </select>
                 </div>
                  <div class="col-sm-4 form-group" id="locationdiv">
                 	<label>Select City/Area</label>
		               <select  name="selectedLocationUrl" id="selectedLocationUrl" ng-model="selectedLocationUrl" class="form-control" autocomplete="off">
						  <option disabled selected value> &nbsp;-- Select City/Area --&nbsp; </option>
						  <option value="ulo-all">All</option>
		                  <option ng-repeat="l in locations" value="{{l.locationUrl}}">{{l.locationDisplayName}}</option>
		              </select>
                 </div>
                 
                 <div class="col-sm-4 form-group">
                  <label>Coupon Name<span style="color:red">*</span></label>
                  <input type="text" class="form-control"  name="couponName"  id="couponName"  placeholder="coupon name" ng-model='couponName' ng-pattern="^/^[A-Za-z\d\s]+$/" ng-required="true" maxlength="50" autocomplete="off">
				  <span ng-show="createcoupon.couponName.$error.pattern" style="color:red">Enter The Valid Name</span>
                 </div>
                  <div class="col-sm-4 form-group">
                  <label>Coupon Code<span style="color:red">*</span></label>
                  <input type="text" class="form-control"  name="couponCode"  id="couponCode"  placeholder="coupon code" ng-model='couponCode' ng-pattern="/^[A-z\d_@.#$=!%^)(\]:\*;\?\/\,}{'\|<>\[&\+-]*$/" ng-required="true" maxlength="50" autocomplete="off">
				  <span ng-show="createcoupon.couponCode.$error.pattern" style="color:red">Enter The Coupon Code</span>
                 </div>
                 <div class="col-sm-4 form-group">
                 	<label>Discount Type</label>
				    <select name="discountType" data-allow-clear="true" ng-model="discountType" id="discountType" value="" class="form-control" ng-required="true" autocomplete="off">
				    <option disabled selected value> &nbsp;-- Discount Type--&nbsp; </option>
                   <option value="Percent">Percentage</option>
                   <option value="Inr">Flat</option>
	               </select>
                 </div>
                  <div class="col-sm-4 form-group">
                  <label>Coupon Percentage / Amount<span style="color:red">*</span></label>
                  <input type="text" class="form-control"  name="couponDiscount"  id="couponDiscount"  placeholder="coupon discount" ng-model='couponDiscount' ng-pattern="/^[0-9]*$/"  ng-required="true" maxlength="50" autocomplete="off">
				  <span ng-show="createcoupon.couponDiscount.$error.pattern" style="color:red">Enter The Coupon Value</span>
                 </div>
                  <div class="col-sm-4 form-group">
                  <label>Max Discount Amount<span style="color:red">*</span></label>
                  <input type="text" class="form-control"  name="maxDiscount"  id="maxDiscount"  placeholder="discount amount" autocomplete="off" ng-model='maxDiscount' ng-pattern="/^[0-9]*$/"  ng-required="true" maxlength="50">
				  <span ng-show="createcoupon.maxDiscount.$error.pattern" style="color:red">Enter The Max Discount</span>
                 </div>
                 <div class="col-sm-4 form-group">
                  <label>Number Of Coupon<span style="color:red">*</span></label>
                  <input type="text" class="form-control"  name="couponCount"  id="couponCount"  placeholder="No of coupons" ng-model='couponCount' autocomplete="off" ng-pattern="/^[0-9]*$/"  ng-required="true" maxlength="50">
				  <span ng-show="createcoupon.couponCount.$error.pattern" style="color:red">Enter The Coupon Count</span>
                 </div>
                 <div class="form-group col-sm-4">
                  <label>Valid from</label>
                   <div class="input-group date">
                   <label class="input-group-addon btn" for="validFrom">
                   <span class="fa fa-calendar"></span>
                   </label>   
                   <input type="text" id="validFrom" class="form-control" name="validFrom"  value="" ng-required="true" autocomplete="off">
                  </div>
                 </div>
                  <div class="form-group col-sm-4">
                  <label>Valid To</label>
                   <div class="input-group date">
                   <label class="input-group-addon btn" for="validTo">
                   <span class="fa fa-calendar"></span>
                   </label>   
                   <input type="text" id="validTo" class="form-control" name="validTo"  value="" ng-required="true" autocomplete="off">
                  </div>
                 </div>
                  <div class="col-sm-4 form-group">
                 	<label>Select Restriction</label>
				    <select name="isSignup" data-allow-clear="true" ng-model="isSignup" id="isSignup" value="" class="form-control" ng-required="true" autocomplete="off" >
				    <option disabled selected value> &nbsp;-- Selected Users --&nbsp; </option>
                   <option value="true">First Time Users</option>
                   <option value="false">All Users</option>
                    </select>
                 </div>
                  <div class="form-group col-sm-12">
                  <div id="messagealert"></div>
                  <button type="submit" class="btn btn-primary pull-right" ng-disabled="createcoupon.$invalid" ng-click="createcoupon.$valid && addCoupon()">Save Coupon</button>
                  </div>
                 </form>
                </div>
             </section>
              <!-- create coupon ends-->
            </div>
            <!-- /.tab-content -->
            <div class="tab-pane " id="fa-iconstwo">
              <section class="content">
                <div class="row">
                  <h4>Coupon List</h4>
                  	<div class="table-responsive no-padding"  > 
					 <table class="table table-bordered" id="example1">
					 <tr>
					 <th>S.No</th>
					 <th>Property Name</th>
					 <th>Coupon Name</th>
					 <th>Coupon Code</th>
					 <th>Valid From</th>
					 <th>Valid To</th>
					 <th>Edit</th>
					 <th>Delete</th>
					 </tr>
					 <tr ng-repeat="c in coupons">
					 <td>{{c.serialNo}}</td>
					 <td>{{c.propertyName}}</td>
					 <td>{{c.discountName}}</td>
					 <td>{{c.discountCode}}</td>
					 <td>{{c.startDate}}</td>
					 <td>{{c.endDate}}</td>
					 <td><a href="#editCouponModel" ng-click="getDiscount(c.discountId)" data-toggle="modal"><i class="fa fa-pencil text-green"></i></a></td>
					 <td><a href="#" ng-click="deleteCoupons(c.discountId)" data-toggle="modal"><i class="fa fa-fw fa-trash text-red"></i></a></td>
					 </tr>
					 </table>
					</div>
                </div>
              <!-- edit coupon form -->
  <!-- Modal -->
<div id="editCouponModel" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit Coupon</h4>
      </div>
      <div class="modal-body">
        <div class="row">
        <form name="editcreatecoupon">
        		<input type="hidden" class="form-control" name="editPropertyDiscountId" id="editPropertyDiscountId" value="{{selectcoupons[0].discountId}}">
         		<div class="col-sm-4 form-group">
                 	<label>Coupon Applicable</label>
				    <select name="editCouponType" data-allow-clear="true"  data-placeholder="Coupon Type" ng-model="editCouponType" autocomplete="off" id="editCouponType" value="" class="form-control"  >
                   <option value="UW" ng-selected="selectcoupons[0].couponType=='UW'">Own Channel</option>
                   <option value="RD" ng-selected="selectcoupons[0].couponType=='RD'">Area/City Wise</option>
                   <option value="OW" ng-selected="selectcoupons[0].couponType=='OW'">Affiliate Website</option>
	               </select>
                 </div>
                  <div class="col-sm-4 form-group">
                 	<label>Select City/Area</label>
				    <select name="editSelectLocationUrl" data-allow-clear="true" ng-model="selectcoupons[0].locationUrl" autocomplete="off" id="editSelectLocationUrl" class="form-control"  >
						  <option style="display:none" value="0">All</option>
		                  <option ng-repeat="l in locations" value="{{l.locationUrl}}" ng-selected="l.locationUrl==selectcoupons[0].locationUrl">{{l.locationDisplayName}}</option>
	               </select>
                 </div>
                 <div class="col-sm-4 form-group">
                  <label>Coupon Name<span style="color:red">*</span></label>
                  <input type="text" class="form-control"  name="editCouponName"  id="editCouponName" ng-model='selectcoupons[0].discountName' autocomplete="off" ng-pattern="^/^[A-Za-z\d\s]+$/" ng-required="true" maxlength="50">
				  <span ng-show="editcreatecoupon.editCouponName.$error.pattern" style="color:red">Enter The Valid Name</span>
                 </div>
                 <div class="col-sm-12 form-group">
                 </div>
                  <div class="col-sm-4 form-group">
                  <label>Coupon Code<span style="color:red">*</span></label>
                  <input type="text" class="form-control"  name="editCouponCode"  id="editCouponCode"  autocomplete="off" ng-model='selectcoupons[0].discountCode' ng-pattern="/^[A-z\d_@.#$=!%^)(\]:\*;\?\/\,}{'\|<>\[&\+-]*$/" ng-required="true" maxlength="50">
				  <span ng-show="editcreatecoupon.editCouponCode.$error.pattern" style="color:red">Enter The Coupon Code</span>
                 </div>
                 <div class="col-sm-4 form-group">
                 	<label>Discount Type</label>
				    <select name="editDiscountType" data-allow-clear="true" autocomplete="off" ng-model="editDiscountType" id="editDiscountType" class="form-control"  >
                   <option value="Percent" ng-selected="selectcoupons[0].discountType=='Percent'">Percentage</option>
                   <option value="Inr" ng-selected="selectcoupons[0].discountType=='INR'">Flat</option>
	               </select>
                 </div>
                  <div class="col-sm-4 form-group">
                  <label>Coupon Percentage / Amount<span style="color:red">*</span></label>
                  <input type="text" class="form-control"  name="editCouponDiscount"  id="editCouponDiscount" autocomplete="off" ng-model='selectcoupons[0].discountAmount' ng-pattern="/^[0-9]*$/"  ng-required="true" maxlength="50">
				  <span ng-show="editcreatecoupon.editCouponDiscount.$error.pattern" style="color:red">Enter The Coupon Value</span>
                 </div>
                 <div class="col-sm-12 form-group">
                 </div>
                  <div class="col-sm-4 form-group">
                  <label>Max Discount Amount<span style="color:red">*</span></label>
                  <input type="text" class="form-control"  name="editMaxDiscount"  id="editMaxDiscount" autocomplete="off" placeholder="discount amount" ng-model='selectcoupons[0].discountMaximumAmount' ng-pattern="/^[0-9]*$/"  ng-required="true" maxlength="50">
				  <span ng-show="editcreatecoupon.editMaxDiscount.$error.pattern" style="color:red">Enter The Max Discount</span>
                 </div>
                 <div class="col-sm-4 form-group">
                  <label>Number Of Coupon<span style="color:red">*</span></label>
                  <input type="text" class="form-control"  name="editCouponCount"  id="editCouponCount"  placeholder="No of coupons" ng-model='selectcoupons[0].discountUnits' ng-pattern="/^[0-9]*$/"  ng-required="true" maxlength="50" autocomplete="off">
				  <span ng-show="editcreatecoupon.editCouponCount.$error.pattern" style="color:red">Enter The Coupon Count</span>
                 </div>
                 <div class="form-group col-sm-4">
                  <label>Valid from</label>
                   <div class="input-group date">
                   <label class="input-group-addon btn" for="validFrom">
                   <span class="fa fa-calendar"></span>
                   </label>   
                   <input type="text" id="editValidFrom" class="form-control" name="editValidFrom" ng-model="selectcoupons[0].startDate"  value="" ng-required="true" autocomplete="off">
                  </div>
                 </div>
                 <div class="col-sm-12 form-group">
                 </div>
                  <div class="form-group col-sm-4">
                  <label>Valid To</label>
                   <div class="input-group date">
                   <label class="input-group-addon btn" for="validTo">
                   <span class="fa fa-calendar"></span>
                   </label>   
                   <input type="text" id="editValidTo" class="form-control" name="editValidTo" ng-model="selectcoupons[0].endDate" value="" ng-required="true" autocomplete="off">
                  </div>
                 </div>
                  <div class="col-sm-4 form-group">
                 	<label>Select Restriction</label>
				    <select name="editIsSignup" data-allow-clear="true" ng-model="selectcoupons[0].isSignup" id="editIsSignup" value="" class="form-control" autocomplete="off">
                   <option value="true" ng-selected="selectcoupons[0].isSignup==true" >First Time Users</option>
                   <option value="false" ng-selected="selectcoupons[0].isSignup==false">All Users</option>
                    </select>
                 </div>
           
        </form>
        </div>
      </div>
      <div class="modal-footer">
         <div id="editmessagealert"></div>
                  <button type="submit" class="btn btn-primary pull-right" ng-disabled="editcreatecoupon.$invalid" ng-click="editCoupon();">Save Coupon</button>
          </div>
    </div>

  </div>
</div>
  <!-- Edit coupon Form Ends -->
              </section>
              <!-- /.content -->
            </div>
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
  </section>

  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>
<script src="js/jquery-2.1.1.js" type="text/javascript"></script>
<script>
    
	 
	
	var app = angular.module('myApp', ['ngProgress']);
	app.controller('customersCtrl', function($scope, $http,$timeout,ngProgressFactory) {
		
		var spinner = $('#loadertwo');
		$("#validFrom").datepicker({
			dateFormat: 'MM d, yy',

			//minDate:  0,
			onSelect: function (formattedDate) {
				var date1 = $('#validFrom').datepicker('getDate');
				var date = new Date(Date.parse(date1));
				date.setDate(date.getDate() + 1);
				var newDate = date.toDateString();
				newDate = new Date(Date.parse(newDate));
				$('#validTo').datepicker("option", "minDate", newDate);
				$timeout(function () {
					document.getElementById("validFrom").style.borderColor = "LightGray";
				});
			}
		});

		$("#validTo").datepicker({
			dateFormat: 'MM d, yy',

			//minDate:  0,
			onSelect: function (formattedDate) {
				var date2 = $('#validTo').datepicker('getDate');
				$timeout(function () {
					document.getElementById("validTo").style.borderColor = "LightGray";
				});
			}
		});
		 
	    $scope.addCoupon = function() {
	    	   
	           var fdata = "&discountName=" + $('#couponName').val() + "&strStartDate=" + $('#validFrom').val() 
	           + "&strEndDate=" + $('#validTo').val() + "&discountType=" + $('#discountType').val()
	           + "&isSignup=" + $('#isSignup').val() + "&discountUnits=" + $('#couponCount').val()
	           + "&couponDiscount=" + $('#couponDiscount').val() + "&couponCode=" + $('#couponCode').val() 
	           + "&couponType=" + $('#couponType').val() + "&locationUrl="+$('#selectedLocationUrl').val()
	           +"&maximumDiscount="+$('#maxDiscount').val();
	           
	           if(document.getElementById('validFrom').value==""){
	        		 document.getElementById('validFrom').focus();
	        		 document.getElementById("validFrom").style.borderColor = "red";
	        	 }else if(document.getElementById('validTo').value==""){
	        		 document.getElementById('validTo').focus();
	        		document.getElementById("validTo").style.borderColor = "red";
	        	 }else if(document.getElementById('validFrom').value!="" && document.getElementById('validTo').value!=""){
	             spinner.show();
		           $http({
		               method: 'POST',
		               data: fdata,
		               headers: {
		                   'Content-Type': 'application/x-www-form-urlencoded'
		               },
		               url: 'add-coupon'
		           }).then(function successCallback(response) {
		               document.getElementById("createcoupon").reset();
		               setTimeout(function(){ spinner.hide(); 
	            	   }, 1000);
	            	   var alertmsg = ' <div class="alert" style="color:green;text-align:center;">saved successfully</div>';
			            $('#messagealert').html(alertmsg);
			            $("#messagealert").fadeTo(2000, 500).slideUp(500, function(){
			              
			                 });
	   
	                   $scope.getDiscounts();
		   
		               //window.location.reload();
		           }, function errorCallback(response) {
		        	   setTimeout(function(){ spinner.hide(); 
	            	   }, 1000);
	            	   var alertmsg = ' <div class="alert" style="color:green;text-align:center;">process failed, please try again.</div>';
			            $('#messagealert').html(alertmsg);
			            $("#messagealert").fadeTo(2000, 500).slideUp(500, function(){
			              
			                 });
	   
	                   $scope.getDiscounts();   
		           });
	        	 }
	       };
	       
			$scope.editCoupon = function() {
	    	   
	           var fdata = "&discountName=" + $('#editCouponName').val() + "&strStartDate=" + $('#editValidFrom').val() 
	           + "&strEndDate=" + $('#editValidTo').val() + "&discountType=" + $('#editDiscountType').val()
	           + "&isSignup=" + $('#editIsSignup').val() + "&discountUnits=" + $('#editCouponCount').val()
	           + "&couponDiscount=" + $('#editCouponDiscount').val() + "&couponCode=" + $('#editCouponCode').val() 
	           + "&couponType=" + $('#editCouponType').val() + "&locationUrl="+$('#editSelectLocationUrl').val()
	           +"&maximumDiscount="+$('#editMaxDiscount').val()+"&propertyDiscountId="+$('#editPropertyDiscountId').val();
	           
	           if(document.getElementById('editValidFrom').value==""){
	        		 document.getElementById('editValidFrom').focus();
	        		 document.getElementById("editValidFrom").style.borderColor = "red";
	        	 }else if(document.getElementById('editValidTo').value==""){
	        		 document.getElementById('editValidTo').focus();
	        		document.getElementById("editValidTo").style.borderColor = "red";
	        	 }else if(document.getElementById('editValidFrom').value!="" && document.getElementById('editValidTo').value!=""){
	             spinner.show();
		           $http({
		               method: 'POST',
		               data: fdata,
		               headers: {
		                   'Content-Type': 'application/x-www-form-urlencoded'
		               },
		               url: 'edit-coupon'
		           }).then(function successCallback(response) {
		               setTimeout(function(){ spinner.hide(); 
	            	   }, 1000);
		               var alertmsg = ' <div class="alert" style="color:green;text-align:center;">saved successfully</div>';
			            $('#editmessagealert').html(alertmsg);
			            $("#editmessagealert").fadeTo(2000, 500).slideUp(500, function(){
			              
			                 });
	                   $('#editCouponModel').modal('toggle');
	                   $scope.getDiscounts();
		           }, function errorCallback(response) {
		        	   setTimeout(function(){ spinner.hide(); 
	            	   }, 1000);
		        	   
		        	   var alertmsg = ' <div class="alert" style="color:green;text-align:center;">process failed, please try again.</div>';
			            $('#editmessagealert').html(alertmsg);
			            $("#editmessagealert").fadeTo(2000, 500).slideUp(500, function(){
			              
			                 });
	                   $('#editCouponModel').modal('toggle');
	                   $scope.getDiscounts();
		           });
	        	 }
	       };
	       
	       
	
		$scope.getDiscounts = function() {
			   
	           var url = "get-coupons";
	           $http.get(url).success(function(response) {
	               $scope.coupons = response.data;
	   
	           });
	       };
	   
       $scope.getDiscount = function(rowid) {
   
           var url = "get-coupon?propertyDiscountId=" + rowid;
           $http.get(url).success(function(response) {
               $scope.selectcoupons = response.data;
           });
       };
     
       $scope.deleteCoupons = function(rowid) {
    	   var msg=confirm("Do you want to delete the coupons?");
    	   if(msg){
    		   var url = "delete-coupons?propertyDiscountId=" + rowid;
               $http.get(url).success(function(response) {
               		$scope.getDiscounts();
               });   
    	   }
           
       };
       
       $scope.getLocationAreas = function(){
    	 var url="get-discount-location-areas"
   		 $http.get(url).success(function(response) {
                $scope.locations = response.data;
            });
       };
       
       $scope.getDiscounts();
       $scope.getLocationAreas();
       $('#locationdiv').hide();
       $scope.getLocationDiv= function(type){
    	   if(type=="RD"){
    		   $('#locationdiv').show();
    		   
    	   }else{
    		   $('#selectedLocationUrl').val('ulo-all-hotels');
    		   $('#locationdiv').hide();
    		   
    	   }
    	   
       };
    
		
	});

	</script>
  <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="plugins/iCheck/icheck.min.js"></script>
<script>
  /* next and previous button */
 $('.nextBtn').click(function(){
  $('.nav-tabs > .active').next('li').find('a').trigger('click');
});

  $('.previousBtn').click(function(){
  $('.nav-tabs > .active').prev('li').find('a').trigger('click');
});
  </script>
