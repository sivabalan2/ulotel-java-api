<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!--  blog header Ends  -->
<!--  blog home carousal begins  -->
<section class="promocode">
<div class="container">
<div class="row">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
 <h3 class="text-center">Our Coupon Partners</h3>
</div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
<a href="https://www.dealsshutter.com/ulohotels-coupons" target="_blank"><img src="uloblog/images/coupons/dealsshutter.jpg "alt="ulo free promo code" class="img-responsive" ></a>
</div>
<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
<a href="https://dealspricer.in/offers/ulohotels-coupons" target="_blank"><img src="uloblog/images/coupons/dealspricer.jpg" alt="ulo free promo code" class="img-responsive"></a>
</div>
<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
<a href="https://www.maddycoupons.in/stores/ulohotels-coupons/" target="_blank"><img src="uloblog/images/coupons/maddycoupons.jpg" alt="ulo free promo code" class="img-responsive" ></a>
</div>
<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
<a href="https://www.janatohmana.in/store/ulo-hotels-coupons/" target="_blank"><img src="uloblog/images/coupons/janatohmana.jpg" alt="ulo free promo code" class="img-responsive"></a>
</div>
<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
<a href="https://www.coupons4sure.com/shop/ulo-hotels/" target="_blank"><img src="uloblog/images/coupons/coupons4sure.jpg" alt="ulo free promo code" class="img-responsive"></a>
</div>
<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
<a href="https://www.couponmoto.com/ulohotels-coupons" target="_blank"><img src="uloblog/images/coupons/couponmoto.jpg" alt="ulo free promo code" class="img-responsive"></a>
</div>
<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
<a href="https://www.fabpromocodes.in/" target="_blank"><img src="uloblog/images/coupons/fab-promo-codes.jpg" alt="ulo fab promo code" class="img-responsive"></a>
</div>
</div>
</div>
</div>
</section>
<!--  blog home destinations list brgins  -->
<section class="blogdestination">
   <div class="container">
      <div class="row">
         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 blogbox">
            <h3 class="text-center">Destinations</h3>
            <h5 class="text-center hidden"><a>View All</a></h5>
            <!-- dest list -->
            <div class="bloglistfive ">
            
               <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12" ng-repeat="abl in allbloglocations">
                  <a href="{{abl.blogLocationUrl}}"><img src="get-ulo-blog-location-image?blogLocationId={{abl.blogLocationId}}" class="img-responsive destlist"></a>
                  <h3 class="centered">{{abl.blogLocationName}}</h3>
               </div>
              
            </div>
            <!-- dest list -->
         </div>
      </div>
   </div>
</section>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jsSocials/1.5.0/jssocials.min.js"></script>
<script src="uloblog/js/angularjs/angular.min.js" type="text/javascript"></script>
<script src="uloblog/js/ngprogress.js" type="text/javascript"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script> 
<script>
   var app = angular.module('myApp', ['ngProgress']);
   app.controller('customersCtrl', function ($scope, $http, $timeout, ngProgressFactory) {
   

        
        $scope.settings = {
               currentPage: 0,
               offset: 0,
               pageLimit: 5,
               pageLimits: ['10', '50', '100']
             };
     
     
     $scope.getUloBlogArticles = function() {
   	  
   	  //alert("enter")
   	  var id = $('#blogArticleId').val() 
   	  //alert(id)
   var url = "get-ulo-blog-articles?blogArticleId="+id;
   		$http.get(url).success(function(response) {
   	$scope.blogarticlecontent = response.data;  
   	
   
   });
   
   };  
   
   $scope.sendBlogUserComment = function () {
   //alert("send enter")
     
     var fdata = "blogUserComment=" + $('#blogUserMessage').val()+
     "&blogUserEmail=" + $('#blogUserEmail').val() +
     "&blogUserUrl=" + $('#blogUserUrl').val()+
     "&blogUserName=" + $('#blogUserName').val() +
   "&blogArticleId=" + $('#blogArticleId').val();
     //alert(fdata)
     $http({
     method: 'POST',
     data: fdata,
     headers: {
     	'Content-Type': 'application/x-www-form-urlencoded'
     },
     url: 'send-blog-user-comment'
     }).success(function (response) {
     $scope.sendcommment = response.data;
   var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:red">Thank You For Posting Your Comments</div>';
   $('#commentmessage').html(alertmsg);
   window.setTimeout(function() {
	    $("#commentmessage").fadeTo(200, 0).slideUp(200, function(){
	        $(this).remove(); 
	    });
	}, 4000);
     //window.location.reload()
     $('#replayForm')[0].reset(); // To reset form fields$('#form')[0].reset(); // To reset form fields
     });
     };
     
   $scope.getAllBlogLocations = function() {
    
   	var url = "get-all-ulo-blog-locations";
   	$http.get(url).success(function(response) {
   	    console.log(response);
   		$scope.allbloglocations = response.data;
   
   	});
   	
   };
   
   $scope.getBlogUserComment= function() {
   
   var id = $('#blogArticleId').val() 
   
   var url = "get-ulo-blog-user-comment?blogArticleId="+id;
   $http.get(url).success(function(response) {
   console.log(response);
   $scope.blogusercomment = response.data;
   
   });
   
   };
   
   $scope.getAllBlogCategories = function() {
   var url = "get-all-ulo-blog-categories";
   $http.get(url).success(function(response) {
      console.log(response);
   $scope.allblogcategories = response.data;
   
   });
   
   };	
   
   $scope.getAllBlogArticles = function() {
   var url = "get-all-ulo-blog-articles?limit="+$scope.settings.pageLimit+"&offset="+$scope.settings.offset;
   $http.get(url).success(function(response) {
     console.log(response);
   $scope.allblogarticles = response.data;
   
   });
   
   };
   
   
   //$scope.getUloBlogArticles();
   $scope.getBlogUserComment();
   $scope.getAllBlogArticles();
   $scope.getAllBlogLocations();
   $scope.getAllBlogCategories();
   });
   
</script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials.min.js"></script>

<link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials.css" />

 <script>
        $("#share").jsSocials({
            shares: ["email", "twitter", "facebook", "googleplus", "linkedin", "pinterest", "stumbleupon", "whatsapp"]
        });
    </script>
