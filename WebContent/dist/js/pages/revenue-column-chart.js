var arr =  new Array;
if(document.getElementById('searchRevenueChart').onclick = function() {
	var accommId=$('#chartPropertyAccommodationId').val();
	var fromDate=$('#chartStartDate').val();
	var toDate=$('#chartEndDate').val();
	var noofRooms=0;
	var datapointA=[];
	var datapointB=[];
	$.ajax({
		 	
	 		url: "get-chart-inventory-rates"+ "?accommodationId=" +accommId+'&strFromDate='+fromDate+'&strToDate='+toDate,
	 		method: "GET",
	 		async: false,
	 		success: function(response) {
	 			
	 			var jsonData = response.data;
	 			
	 			noofRooms=parseInt(jsonData[0].roomCount);
	 			for(var  i=0; i<jsonData.length;i++){
	 				datapointA.push({label:jsonData[i].daysmonth, y: parseInt(jsonData[i].occupancy)});
	 			}
	 			
	 			for(var  i=0; i<jsonData.length;i++){
	 				datapointB.push({label:jsonData[i].daysmonth, y: parseFloat(jsonData[i].revenueAmount)});
	 			}
	 		}
	 	});
    

	var chart = new CanvasJS.Chart("chartContainer", {
		animationEnabled: true,
		title:{
			text: "Inventory & Revenue"
		},	
		axisY: {
			minimum:0,
			maximum:noofRooms,
			interval:1,
			title: "Room Inventory",
			titleFontColor: "#4F81BC",
			lineColor: "#4F81BC",
			labelFontColor: "#4F81BC",
			tickColor: "#4F81BC"
		},
		axisY2: {
			minimum:500,
			maximum:10000,
			interval:500,
			title: "Room Revenue",
			titleFontColor: "#C0504E",
			lineColor: "#C0504E",
			labelFontColor: "#C0504E",
			tickColor: "#C0504E"
		},
		toolTip: {
			shared: true
		},
		legend: {
			cursor:"pointer",
			itemclick: toggleDataSeries
		},
		data: [{
			type: "column",
			name: "Inventory",
			legendText: "Inventory",
			showInLegend: true, 
			dataPoints:datapointA
		},
		{
			type: "column",	
			name: "Rates",
			legendText: "Rates",
	 		axisYType: "secondary",
			showInLegend: true,
			dataPoints:datapointB
		}]
	});
	chart.render();

	function toggleDataSeries(e) {
		if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
			e.dataSeries.visible = false;
		}
		else {
			e.dataSeries.visible = true;
		}
		chart.render();
	}  

	    
  }); 
