<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="s" uri="/struts-tags"%>
<link rel="stylesheet" href="plugins/fullcalendar/fullcalendar.min.css">
  <link rel="stylesheet" href="plugins/fullcalendar/fullcalendar.print.css" media="print">
  
   <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
        Rate Manager
          </h1>
          <ol class="breadcrumb">
            <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Rate Manager</li>
          </ol>
        </section>
		
        <!-- Main content -->
        <section class="content">
        
        	<div class="row">
            	<div class="col-xs-12" style="z-index:1;">
						<div class="box">
							<div class="box-header">
								<h3 class="box-title">Rate Manager</h3>
									<button  class="btn-primary" id="show" style='display:none;z-index:99999;'  >Modify Search</button>   
							</div>
							
							<div id="container2" >
							<form name="searchRate">
								<div class="col-sm-4 col-md-3">
								<div class="form-group">
							      <label>Select Accommodation Type :</label>
									<select name="propertyAccommodationId" id="propertyAccommodationId" value="" ng-model="propertyAccommodationId" class="form-control" ng-required="true">
			                       	<option value="">Choose Accommodation</option>
			                       	<option ng-repeat="at in accommodations" value="{{at.accommodationId}}">{{at.accommodationType}}</option>
			                        </select>
								</div>
								<input type="hidden" id="accommId" name="accommId" class="form-control" value = "" placeholder="Accommodation">
								</div>
								<div class="col-sm-4 col-md-3">
							 	<div class=" form-group">
								<label>Start Date :</label>
									 <div class="input-group date">
					                   <label class="input-group-addon btn" for="startDate">
					                   <span class="fa fa-calendar"></span>
					                  </label>   
					                  <input type="text" id="startDate" class="form-control" name="startDate"   value="" ng-required="true"  autocomplete="off">
					                </div>
<!-- 					                <input type="hidden" id="startDate" name="startDate" class="form-control" value = "" placeholder="Start Date"> -->
								</div>
								</div>
								<div class="col-sm-4 col-md-3">
								<div class="form-group">
									<label >End Date :</label>
						      		<div class="input-group date">
					                   <label class="input-group-addon btn" for="endDate">
						                   <span class="fa fa-calendar"></span>
						              	</label>   
						                  <input type="text" id="endDate" class="form-control" name="endDate"  value="" ng-required="true"  autocomplete="off"> 
					                </div>
<!-- 					                <input type="hidden" id="endDate" name="endDate" class="form-control" value = "" placeholder="End Date"> -->
								</div>
								</div>
								<div class="col-sm-4 col-md-3">
								<div class="form-group">
									<label>Source</label>
									<select  class="form-control" name="sourceId" ng-model="sourceId" id="sourceId" value="" class="form-control " ng-required="true">
									<option value="">Select Source</option>
									<option ng-repeat="sr in sources" value="{{sr.sourceId}}" >{{sr.sourceName}}</option>
									</select>
								</div>
								<input type="hidden" id="sourceId" name="sourceId" class="form-control" value = "" placeholder="Source Id">
								</div>
							
								<div class="box-footer clearfix">
					              <button type="submit" id="hide" onclick="getCalendar()" ng-disabled="searchRate.$invalid" class="btn btn-primary btngreeen pull-right" style="margin:5px;" >Search</button>
					              <a class="btn btn-primary pull-right " href="#addmodal" ng-click=""  data-toggle="modal" style="margin:5px;" >Modify</a>
					              <a class="btn btn-primary pull-right " href="#smartpricemodal" ng-click=""  data-toggle="modal" style="margin:5px;" >Smart Price</a>
					            </div>
							</form>
						</div>
						<!-- /.box -->
						
					</div>	
         	</div>
         	<div class="col-lg-12 col-md-12">
         	<div id="container">
         	 <div id="calendar"></div>
         	</div>
         	</div>
          </div>
        </section><!-- /.content -->
      	
      	<div class="modal" id="addmodal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" id="btnclose" class="close" data-dismiss="modal"
							aria-hidden="true">x</button>
						<h4 class="modal-title">Add Rates</h4>
					</div>
					 <form name="addForm" id="addRate">
					<div class="modal-body">
					
                          <div class="form-group col-md-12">
					      <label>Select Accommodation Type :</label>
							<select  name="modPropertyAccommodationId" id="modPropertyAccommodationId" ng-model="modPropertyAccommodationId" value="" class="form-control" ng-required="true" >
							<option style="display:none" value="">Select Your  Accommodation</option>
	                       	<option ng-repeat="at in accommodations" value="{{at.accommodationId}}">{{at.accommodationType}}</option>
	                        </select>
						 </div>
						 
					<%-- 	<div class="form-group col-md-6">
							<label>Rate Name</label>
							<input type="text" class="form-control"  name="rateName"  value="{{r.rateName}}"  ng-model='rateName' ng-pattern= "/^[a-zA-Z0-9]*$/" id="rateName" ng-required="true">
							<span ng-show="addForm.rateName.$error.pattern" style="color:red">Not a valid Name!</span>
						 </div> --%>
						
						<div class="form-group col-md-12" id="checkedsources">
							<label>Source :</label>
<%-- 							<select  class="form-control" name="modSourceId"  id="modSourceId" value="" class="form-control " ng-required="true"> --%>
<!-- 							<option ng-repeat="sr in sources" value="{{sr.sourceId}}" ng-selected ="sr.sourceId == sr.sourceId">{{sr.sourceName}}</option> -->
<%-- 							</select> --%>
							<div class="input-group col-md-12" >
							<div >
							<label class="checkbox-inline"><input type="checkbox"  id="checkSourceAll"  name="checkSourceAll" value="0">All</label>
                           <label ng-repeat="sr in sources"  class="checkbox-inline">
                      					<input type="checkbox" name="modSources[]"  id="modSources[]" value="{{sr.sourceId}}"  >&nbsp {{sr.sourceName}}
                    				</label>
                    				</div></div>
						</div>
						 
						<div class=" form-group col-md-6">
							<label>Start Date :</label>
							 <div class="input-group date">
		                   <label class="input-group-addon btn" for="modStartDate">
		                   <span class="fa fa-calendar"></span>
		                  </label>   
		                  <input type="text" id="modStartDate" class="form-control" name="modStartDate"  value="" ng-required="true"  autocomplete="off">
		 
		                  
		                </div>
						     
						</div>
						
						<div class="form-group col-md-6">
							<label >End Date :</label>
							      <div class="input-group date">
			                   <label class="input-group-addon btn" for="modEndDate">
			                   <span class="fa fa-calendar"></span>
			              		</label>   
			                  <input type="text" id="modEndDate" class="form-control" name="modEndDate"  value="" ng-required="true"  autocomplete="off"> 
			
			                </div>
						     
						</div>
						<div class="form-group col-md-6">
							<label>Tariff</label>
							<input type="text" class="form-control"  name="amount"  id="amount" ng-model="amount" ng-pattern="/^[0-9]/" ng-required="true" placeholder="Tariff Per Night">
						    <span ng-show="addForm.amount.$error.pattern" style="color:red">please enter numeric</span>
						</div>
					
						<div class="form-group col-md-6">
							<label for="child_included_rate">Extra Adult</label>
							<input type="text" class="form-control"  name="extraAdult"  id="extraAdult"  placeholder="Extra Adult Rate" ng-model='extraAdult' ng-pattern="/^[0-9]/" ng-required="true">
						    <span ng-show="addForm.extraAdult.$error.pattern" style="color:red">please enter numeric</span>
						      
						</div>
							<div class="form-group col-md-6">
							<label for="child_included_rate">Extra Child</label>
							<input type="text" class="form-control"  name="extraChild"  id="extraChild"  placeholder="Extra Child Rate" ng-model='extraChild' ng-pattern="/^[0-9]/" ng-required="true">
						    <span ng-show="addForm.extraChild.$error.pattern" style="color:red">please enter numeric</span>
						      
						</div>
					 	<div class="form-group col-md-6">
							<label for="child_included_rate">Extra Infant</label>
							<input type="text" class="form-control"  name="extraInfant"  id="extraInfant"  placeholder="Extra Infant Rate" ng-model='extraInfant' ng-pattern="/^[0-9]/" ng-required="true">
						    <span ng-show="addForm.extraInfant.$error.pattern" style="color:red">please enter numeric</span>
						      
						</div> 
						
						<div class="form-group col-md-12" id="checkeddays">
							<label>Days of week :</label>
						   <div class="input-group" >
                          <div >
						   	<label class="checkbox-inline"><input type="checkbox"  id="checkAll"  name="checkAll" value="0">All Days</label>
                         
                           <label ng-repeat="d in days"  class="checkbox-inline">
                      					<input type="checkbox" name="checkedDay[]"  id="checkedDay" value="{{d.dayId}}"   class="flat-red" >&nbsp {{d.daysOfWeek}}
                    				</label>
                    				</div>
						   </div>

						</div>
					
					<div class="form-group" >
						   <div class="input-group" >
						  		<label>&nbsp;&nbsp;</label>
						  		<label>&nbsp;&nbsp;</label>
						   </div>

						</div>
						
					<div class="modal-footer">
	
						<a href="#" data-dismiss="modal" class="btn btn-danger">Close</a>
						<button ng-click="addForm.$valid && updatePrice()"id="loading-example-btn" ng-disabled="addForm.$invalid" class="btn btn-primary">Save</button>
					</div>
					</div>
					</form>
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dalog -->

		</div>
			<div class="modal" id="smartpricemodal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" id="btnclose" class="close" data-dismiss="modal"
							aria-hidden="true">x</button>
						<h4 class="modal-title">Smart Price</h4>
					</div>
					 <form name="addSmartPriceForm" id="addSmartPriceRate">
					<div class="modal-body" >
                          <div class="form-group col-md-6">
					      <label>Select Accommodation Type :</label>
							<select  name="smartPricePropertyAccommodationId" id="smartPricePropertyAccommodationId" ng-model="smartPricePropertyAccommodationId" value="" class="form-control" ng-required="true">
							<option style="display:none" value="">Select Your  Accommodation</option>
	                       	<option ng-repeat="at in accommodations" value="{{at.accommodationId}}">{{at.accommodationType}}</option>
	                        </select>
						 </div>
						 
						
						 
						<div class=" form-group col-md-6">
							<label>Start Date :</label>
							 <div class="input-group date">
			                   <label class="input-group-addon btn" for="smartPriceStartDate">
			                   <span class="fa fa-calendar"></span>
			                  </label>   
			                  <input type="text" id="smartPriceStartDate" class="form-control" name="smartPriceStartDate" value="" ng-required="true"  autocomplete="off">
			 
			                  
			                </div>
							     
						</div>
						
						<div class="form-group col-md-6">
							<label >End Date :</label>
						      <div class="input-group date">
			                   <label class="input-group-addon btn" for="smartPriceEndDate">
			                   <span class="fa fa-calendar"></span>
			             	 </label>   
			                  <input type="text" id="smartPriceEndDate" class="form-control" name="smartPriceEndDate"  value="" ng-required="true"  autocomplete="off"> 
			
			                </div>
						     
						</div>
						
						
						<div class="form-group" >
						   <div class="input-group" >
						   		<label>&nbsp;&nbsp;</label>
						  		<label>Smart Price:</label>
						  		<label>&nbsp;&nbsp;</label>
								<label class="radio-inline"><input type="radio" name="smartPriceIsActive"  id="smartPriceIsActive" ng-value="true" checked="checked" class="flat-red" >Active</label>
<!-- 								<label class="radio-inline"><input type="radio" name="smartPriceIsActive"  id="smartPriceIsActive" ng-value="false"  class="flat-red" >In-Active</label> -->
						   </div>

						</div>
						
						<div class="form-group" >
						   <div class="input-group" >
						  		<label>&nbsp;&nbsp;</label>
						  		<label>&nbsp;&nbsp;</label>
						   </div>

						</div>
			
					<div class="modal-footer">
<!-- 						<a href="#" data-dismiss="modal" class="btn">Close</a> -->
						<a href="#" data-dismiss="modal" class="btn btn-danger">Close</a>
						<button ng-click="addSmartPriceForm.$valid && updateSmartPrice()" ng-disabled="addSmartPriceForm.$invalid" id="smartRate" class="btn btn-primary">Update</button>
					</div>
					</div>
					</form>
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			</div>
      </div><!-- /.content-wrapper -->
 
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="plugins/fullcalendar/fullcalendar.min.js"></script>

  <style type="text/css">
    #ui-datepicker-div
    {
        z-index: 9999999 !important;
    }
</style>
	<script>
	 
	
	var app = angular.module('myApp', ['ngProgress']);
	app.controller('customersCtrl', function($scope, $http,$timeout,ngProgressFactory) {
     
		$scope.dragStart = function (e, ui) {
            ui.item.data('start', ui.item.index());
          }
          
            $scope.dragEnd = function (e, ui) {
            var start = ui.item.data('start'),
            end = ui.item.index();
          
            $scope.rates.splice(end, 0, $scope.rates.splice(start, 1)[0]);
            $scope.$apply();
            console.log($scope.rates);
            var text = '{"orders":' + JSON.stringify($scope.rates) + '}';
            
			  var data = JSON.parse(text);
				
	             $http(
						{
							method : 'POST',
							data : data,
							//data : '{"data":' +JSON.stringify($scope.roomsSelected) + '}',
							dataType: 'json',
							headers : {
								 'Content-Type' : 'application/json; charset=utf-8'

							},
							
							//url : 'bookingdetails'
							url : 'edit-rate-orders'
							
						}).success(function(response) {
							
							//window.location = '/bookingdetails'; 
							
							$scope.orders = response.data;
							
							//console.log($scope.booked[0].firstName);
							
							
						});
            
          }
          
          var sortableEle;
              sortableEle = $('.sortable').sortable({
              start: $scope.dragStart,
              update: $scope.dragEnd
              
          });
		 $("#checkAll").click(function () {
		     $("#checkeddays :checkbox").not(this).prop('checked', this.checked);
		 });
		 $("#checkSourceAll").click(function () {
		     $("#checkedsources :checkbox").not(this).prop('checked', this.checked);
		 });
		
       	$timeout(function(){
      	    // $scope.progressbar.complete();
            $scope.show = true;
            $("#pre-loader").css("display","none");
        }, 2000);

	/* 	$("#hide").click(function() {
            //$('#container').css({position: 'relative', bottom: '60px'});
    	    $('html, body').animate({
    	        scrollTop: $("#container").offset().top
    	        
    	    }, 1000);
    	  
    	}); */
		
		$scope.unread = function() {
		//var notifiurl = "http://localhost:8085/collaborative-workflow/unreadnotifications.action";
		var notifiurl = "unreadnotifications.action";
		$http.get(notifiurl).success(function(response) {
			$scope.latestnoti = response.data;
		});
		};

				 
	        $("#startDate").datepicker({
	            dateFormat: 'MM d, yy',
	            minDate:  0,
	            onSelect: function (dateText, inst) {
	                var date1 = $('#startDate').datepicker('getDate'); 
	                var date = new Date( Date.parse( date1 ) ); 
	                date.setDate( date.getDate());        
	                var newDate = date.toDateString(); 
	                var d = new Date(dateText);
		            $('#calendar').fullCalendar('gotoDate', d);
	                newDate = new Date( Date.parse( newDate ) );   
	                $('#endDate').datepicker("option","minDate",newDate);
	                $timeout(function(){
	                  //scope.checkIn = formattedDate;
	                	document.getElementById("startDate").style.borderColor = "LightGray";
	                });
	            }
	        });
	      
	      $("#endDate").datepicker({
	            dateFormat: 'MM d, yy',
	            minDate:  0,
	            onSelect: function (formattedDate) {
	                var date2 = $('#endDate').datepicker('getDate'); 
	                $timeout(function(){
	                  //scope.checkOut = formattedDate;
	                	document.getElementById("endDate").style.borderColor = "LightGray";
	                });
	            }
	        });
	      $("#modStartDate").datepicker({
	            dateFormat: 'MM d, yy',
	            minDate:  0,
	            onSelect: function (formattedDate) {
	                var date1 = $('#modStartDate').datepicker('getDate'); 
	                var date = new Date( Date.parse( date1 ) ); 
	                date.setDate( date.getDate());        
	                var newDate = date.toDateString(); 
	                newDate = new Date( Date.parse( newDate ) );   
	                $('#modEndDate').datepicker("option","minDate",newDate);
	                $timeout(function(){
	                	document.getElementById("modStartDate").style.borderColor = "LightGray";
	                });
	            }
	        });
	      
	      $("#modEndDate").datepicker({
	            dateFormat: 'MM d, yy',
	            minDate:  0,
	            onSelect: function (formattedDate) {
	                var date2 = $('#modEndDate').datepicker('getDate'); 
	                $timeout(function(){
	                	document.getElementById("modEndDate").style.borderColor = "LightGray";
	                });
	            }
	        });
	      
	      
	      $("#smartPriceStartDate").datepicker({
	            dateFormat: 'MM d, yy',
	            minDate:  0,
		          onSelect: function (formattedDate) {
	                var date1 = $('#smartPriceStartDate').datepicker('getDate'); 
	                var date = new Date( Date.parse( date1 ) ); 
	                date.setDate( date.getDate() );        
	                var newDate = date.toDateString(); 
	                newDate = new Date( Date.parse( newDate ) );   
	                $('#smartPriceEndDate').datepicker("option","minDate",newDate);
	                $timeout(function(){
	                	document.getElementById("smartPriceStartDate").style.borderColor = "LightGray";
	                });
	            } 
	        });
	      
	      $("#smartPriceEndDate").datepicker({
	            dateFormat: 'MM d, yy',
	            minDate:  0,
	             onSelect: function (formattedDate) {
	                var date2 = $('#smartPriceEndDate').datepicker('getDate'); 
	                $timeout(function(){
	                	document.getElementById("smartPriceEndDate").style.borderColor = "LightGray";
	                });
	            } 
	        });
			//updateSmartPrice
	      $scope.updateSmartPrice = function(){
	    	   $("#smartRate").prop('disabled', true);
		     	var fdata = "&smartPricePropertyAccommodationId=" + $('#smartPricePropertyAccommodationId').val()
				+ "&strSmartPriceStartDate=" + $('#smartPriceStartDate').val()
				+ "&strSmartPriceEndDate=" + $('#smartPriceEndDate').val()
				+ "&smartPriceIsActiveOrNot=" + $('input[name=smartPriceIsActive]:checked').val()

				 if(document.getElementById('smartPriceStartDate').value==""){
		 			 document.getElementById('smartPriceStartDate').focus();
		 			document.getElementById("smartPriceStartDate").style.borderColor = "red";
		 		 }else if(document.getElementById('smartPriceEndDate').value==""){
		 			 document.getElementById('smartPriceEndDate').focus();
		 			document.getElementById("smartPriceEndDate").style.borderColor = "red";
		 		 }else if(document.getElementById('smartPriceStartDate').value!="" && document.getElementById('smartPriceEndDate').value!=""){
		 			 
						var checkSmartPrice=$('input[name=smartPriceIsActive]:checked').val();
				     	var events = [];
						if(checkSmartPrice=='true'){
							$http(
									{
										method : 'POST',
										data : fdata,
										headers : {
											'Content-Type' : 'application/x-www-form-urlencoded'
										},
										url : 'add-smart-price-rate'
									}).then(function successCallback(response) {
										
										var smartpricechk=response.data.data[0].check;
										
										if(smartpricechk=="true"){
											alert('smart price already available for this date');
										}
										else if(smartpricechk=='false'){
											
												var gdata = "&smartPricePropertyAccommodationId=" + $('#smartPricePropertyAccommodationId').val()
												+ "&strSmartPriceStartDate=" + $('#smartPriceStartDate').val()
												+ "&strSmartPriceEndDate=" + $('#smartPriceEndDate').val()
												+ "&smartPriceIsActiveOrNot=" + $('input[name=smartPriceIsActive]:checked').val()
												
											$http(
										    {
												method : 'POST',
												data : gdata,
												//data : '{"data":' +JSON.stringify($scope.roomsSelected) + '}',
												
												headers : {
													'Content-Type' : 'application/x-www-form-urlencoded'
													//'Content-Type' : 'application/x-www-form-urlencoded'
												},
												url : 'add-smart-price-rate-details'
										   }).then(function successCallback(response) {
											   
											window.location.reload();
										    alert("smart price added successfully");
										    $("#smartRate").prop('disabled', false);
										   }, function errorCallback(response) {

										   });
											
										}
										
							}, function errorCallback(response) {
								
							}); 
						}
		 		 }				

			};
			
		  
	      $scope.updatePrice = function(){
	    	  $("#loading-example-btn").prop('disabled', true);
	    	 
				var checkbox_sources = "";
			    $("#checkedsources :checkbox").each(function () {
			        var ischecked = $(this).is(":checked");
			        if (ischecked) {
			            checkbox_sources += $(this).val() + ",";
			        }
			    });
			    
			    
			    var checkbox_value = "";
				    $("#checkeddays :checkbox").each(function () {
				        var ischecked = $(this).is(":checked");
				        if (ischecked) {
				            checkbox_value += $(this).val() + ",";
				        }
				    });
				    
		     	
		     	
		     	var fdata = "checkedDays=" + checkbox_value
		     	+ "&propertyAccommodationId=" + $('#modPropertyAccommodationId').val()
		     	+ "&modSources=" + checkbox_sources
				+ "&strModStartDate=" + $('#modStartDate').val()
				+ "&strModEndDate=" + $('#modEndDate').val()
				+ "&extraChild=" + $('#extraChild').val()
				+ "&extraAdult=" + $('#extraAdult').val()
				+ "&extraInfant=" + $('#extraInfant').val()
				
				var sourceslength=$('[name="modSources[]"]:checked').length;
		     	
				var checkedDayslength = $('[name="checkedDay[]"]:checked').length;
		     	
		     	var checkedAll=$('[name="checkAll"]:checked').length;
		     	var checkedSourceAll=$('[name="checkSourceAll"]:checked').length;
				var isChecked=false;
				if(document.getElementById('modStartDate').value==""){
		 			 document.getElementById('modStartDate').focus();
		 			document.getElementById("modStartDate").style.borderColor = "red";
		 		 }else if(document.getElementById('modEndDate').value==""){
		 			 document.getElementById('modEndDate').focus();
		 			document.getElementById("modEndDate").style.borderColor = "red";
		 		 }else if(document.getElementById('modStartDate').value!="" && document.getElementById('modEndDate').value!=""){
				
						if((checkedDayslength>0 || checkedAll>0 ) && (sourceslength>0 || checkedSourceAll>0)){
							if(( checkedDayslength == 7 && checkedAll>0) && ( sourceslength == 6 && checkedSourceAll>0)){
								isChecked=true;
							}else if(sourceslength>0 && sourceslength<=5 && checkedSourceAll==0){
								isChecked=true;
							}else if(checkedDayslength>0 && checkedDayslength<=6 && checkedAll==0){
								isChecked=true;
							} else{
								alert('please tick all to continue')
							}
							
							if(isChecked){
						$http(
								{
									method : 'POST',
									data : fdata,
									headers : {
										'Content-Type' : 'application/x-www-form-urlencoded'
									},
									url : 'add-rate'
								}).then(function successCallback(response) {
									var smartpricechk=response.data.data[0].check;
									if(smartpricechk=="true"){
										alert('Already smart price active for this date');
									}
									else if(smartpricechk=="false"){
									
									var gdata = "checkedDays=" + checkbox_value
							     	    + "&baseAmount=" + $('#amount').val()
							     	    + "&strModEndDate=" + $('#modStartDate').val()
						                + "&strModEndDate=" + $('#modEndDate').val()
						                + "&extraChild=" + $('#extraChild').val()
										+ "&extraAdult=" + $('#extraAdult').val()
										+ "&extraInfant=" + $('#extraInfant').val()
									
									$http(
								    {
									method : 'POST',
									data : gdata,
									//data : '{"data":' +JSON.stringify($scope.roomsSelected) + '}',
									
									headers : {
										'Content-Type' : 'application/x-www-form-urlencoded'
										//'Content-Type' : 'application/x-www-form-urlencoded'
									},
									url : 'add-rate-details'
								   }).then(function successCallback(response) {
									   window.location.reload();
									
								    alert("rate added successfully");
								    $("#loading-example-btn").prop('disabled', false); 
								   }, function errorCallback(response) {
								});
								}	
							}, function errorCallback(response) {
								
							});
						}
						}else if(sourceslength==0){
							alert('please check atleast one value from sources');
						}else if(checkedDayslength==0){
							alert('please check atleast one value from days');
						}else{
							alert('please check atleast one value');
						}
		     	
		 		 }
			};
			
			  
			
	    $scope.getDays = function(){
				
		       var checkbox_value = "";
		       $(":checkbox").each(function () {
		        var ischecked = $(this).is(":checked");
		        if (ischecked) {
		            checkbox_value += $(this).val() + ",";
		        }
		       });
		      
		       
			    var fdata = "strStartDate=" + $('#startDate').val()
				+ "&strEndDate=" + $('#endDate').val()
				+ "&checkedDays=" + checkbox_value
				
				
			    
				$http(
						{
							method : 'POST',
							data : fdata,
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							url : 'get-days'
						}).then(function successCallback(response) {
							$timeout(function(){
						      	$('#btnclose').click();
						        }, 2000); 
							window.location.reload();
							
				}, function errorCallback(response) {
					
					// called asynchronously if an error occurs
					// or server returns response with an error status.
				});

			};
			
		$scope.getAccommodations = function() {

			var url = "get-accommodations";
			$http.get(url).success(function(response) {
			    //console.log(response);
				$scope.accommodations = response.data;
	
			});
		};
		
		$scope.getRates = function() {

			var url = "get-rates";
			$http.get(url).success(function(response) {
			    console.log(response);
				$scope.rates = response.data;
	
			});
		};
		

		 $scope.getRate = function(rowid) {
				var url = "get-rate?propertyRateId="+rowid;
				$http.get(url).success(function(response) {
					$scope.rate = response.data;
		
				});
			};
			
			 $scope.deleteRates = function(rowid) {
					
					var url = "delete-rates?propertyRateId="+rowid;
					$http.get(url).success(function(response) {
					    //console.log(response);
					    //$scope.getTaxes();
					    // $('#deletemodal').modal('toggle');
					    $timeout(function(){
					      	$('#deletebtnclose').click();
					    }, 2000); 
					    window.location.reload();
			
					});
					
				};
		
		$scope.getSources = function() {

			var url = "get-sources";
			$http.get(url).success(function(response) {
			    //console.log(response);
				$scope.sources = response.data;
	
			});
		};
		
		$scope.getDay = function() {

			var url = "get-day";
			$http.get(url).success(function(response) {
			    //console.log(response);
				$scope.days = response.data;
	
			});
		};
		
		$scope.getSmartPrice = function() {

			var url = "get-smart-price";
			$http.get(url).success(function(response) {
			    //console.log(response);
				$scope.smartprices = response.data;
	
			});
		};
		
	   	$(document).ready(function(){
    	    $("#hide").click(function(){
    	    	if(document.getElementById('startDate').value!="" && document.getElementById('endDate').value!=""){
    	        	$("#container2").hide();
    	    	}
    	    });
    	    $("#show").click(function(){
    	    	if(document.getElementById('startDate').value!="" && document.getElementById('endDate').value!=""){
    	        	$("#container2").show();
    	    	}
    	    });
    	});
		
	  $scope.getPropertyList = function() {
			 
	        	
	        	
		        var userId = $('#adminId').val();
	 			var url = "get-user-properties?userId="+userId;
	 			$http.get(url).success(function(response) {
	 			    
	 				$scope.props = response.data;
	 	
	 			});
	 		};
	 		
       $scope.change = function() {
 	   
	        
	        var propertyId = $scope.id;	
 	        var url = "change-user-property?propertyId="+propertyId;
 			$http.get(url).success(function(response) {
 				
 				 window.location = '/ulopms/dashboard'; 
 				//$scope.change = response.data;
 	
 			});
		       
	 		};
	 		
			
	    $scope.getPropertyList();
	    $scope.getSources();
		$scope.getAccommodations();
		//$scope.getRates();
		$scope.getDay();
		//$scope.unread();
		//
		$scope.getSmartPrice();
		
	});

	$(document).ready(function () {
	    // page is now ready, initialize the calendar...
	     var results = [];
	     $('#calendar').fullCalendar({
	        // put your options and callbacks here
	        defaultView: 'month',
	       // eventBorderColor: "#de1f1f",
	        eventColor: '#77b300',
	        eventTextColor:'#fff',
	       // weekMode:'variable',
	     
	   
	         header:
	        {  
	            left: 'prev,next,today',
	            center: 'title',
	            right: 'month'
	        },

	        editable: false,
	        selectable: true,
	        events: dowEvents,
	        editable: true,
	        allDaySlot: true,
	        selectable: true,
	        selectHelper: true,
	        selectOverlap: false,
	        fixedWeekCount: false,
	        showNonCurrentDates: false,
	        
	        
	                //When u select some space in the calendar do the following:
	      

	        //When u drop an event in the calendar do the following:
	        eventDrop: function (event, delta, revertFunc) {
	            //do something when event is dropped at a new location
	        },

	        //When u resize an event in the calendar do the following:
	        eventResize: function (event, delta, revertFunc) {
	            //do something when event is resized
	        },

	 /*        eventRender: function(event, element) {
	            $(element).tooltip({title: event.title});             
	        },
	 */
	        //Activating modal for 'when an event is clicked'
	        eventClick: function (event) {
	        	/* var date = new Date(event.start);
	        	var strDate = (date.getMonth() + 1) + '/' + date.getDate() + '/' +  date.getFullYear();
	        	var smartpricecheck=confirm("Do u want to disable the Smart Price");
	        	if(smartpricecheck)
	        	{
	        		
	        		$.ajax({
		     	 		url: "get-smart-price-disabled"+ "?resultDate=" +strDate,
		     	 		method: "GET",
		     	 		async: true,
		     	 		success: function(response) {
		     	 			events = response.data;
		     	 			window.location.reload();
		    	 			
		     	 			}
		     	 	});
	        	} 
	        	else if (!smartpricecheck){
	        	}  */
	        	
	        },
	       
	        eventRender: function(event, element, view) {
	        	 $(element).tooltip({
	                 content: event.description
	             });
	        	$(element).tooltip({title: event.title});  
	        	//$(element).tooltip({description: event.description}); 
	            var cellheight = $('.fc-widget-content').height();
	            $(element).css('height', '12%');
	            $(element).css('width', '80px');
	             $(element).css('word-wrap', 'break-word');
	            //$(element).css('margin-left', '30px');
	            element.find('.fc-time').hide();
	            $(element).css('font-size', '9');
	            $(element).css('font-style', 'normal');
	            $(element).css('font-weight', 'bold');
	            $(element).css('font-family', 'sans-serif');
	            //sans-serif
	            $(element).css('text-align', 'center');
	            $(element).css('line-height', '20px');
	           
	        },
	     
	       
	    }
	     )


	   
	     
	  function dowEvents(start, end, tz, callback) {
	    	  //var events = [{title:'Rooms 10',start:'o'},{title:'Gopr',start:'o'}];
	    	 var events = [];
	    	  results = enumerateDaysBetweenDates(start, end);
	    	  
	    	  var accommodationId = $('#propertyAccommodationId').val();
	    	  var startDate = $('#startDate').val();
	    	  var endDate = $('#endDate').val();
	    	  var sourceId = $('#sourceId').val();
	    	  var events = [];
	    	 if(accommodationId>0){
	    		 if(document.getElementById('startDate').value==""){
		 			 document.getElementById('startDate').focus();
		 			document.getElementById("startDate").style.borderColor = "red";
		 		 }else if(document.getElementById('endDate').value==""){
		 			 document.getElementById('endDate').focus();
		 			document.getElementById("endDate").style.borderColor = "red";
		 		 }else if(document.getElementById('startDate').value!="" && document.getElementById('endDate').value!=""){
		 			    	  
			    	  
			    	  $.ajax({
			    	 	
			    	 		url: "get-rate-details"+ "?chartDate=" +results + "&accommodationId="+accommodationId + "&strStartDate="+startDate + "&strEndDate="+endDate + "&sourceId="+sourceId,
			    	 		method: "GET",
			    	 		async: false,
			    	 		success: function(response) {
			    	 			
			    	 			events = response.data;
			    	 			
			    	 			/* var lim = events.length;
			    	 			
			    	 			
			    	 			for (var i = 0; i < lim; i++){
			    	 				     
			    	 					 events[i].start = moment(results[i]);
			    	 				
			    	 				}  */
			    	 			
			    	 			
			    	 		}
			    	 	});
			    	 
			    	  
			    	  console.log(events);
			    	  
			    	  callback(events);
			    	
			    	} 
	    	 }
	    	  
	     }
	     
	    });




	</script>
	<script>
	     var enumerateDaysBetweenDates = function(startDate, endDate) {
	      
	      
	      var now = startDate,
	      dates = [];
	      
	      
	     while (now.isBefore(endDate) || now.isSame(endDate)) {
	           // dates.push({title:now.format("YYYY-MM-DD"),start:moment(startDate)});
	           dates.push(now.format("YYYY-MM-DD"));
	          //  moment().format();               // Jun 12th 17

	            now.add('days', 1);
	        }
	      return dates;
	      
	  };

	 
	 
	 function getCalendar(){
		 if(document.getElementById('startDate').value!="" && document.getElementById('endDate').value!=""){
			 $('#calendar').fullCalendar( 'refetchEvents' );	 
		 }		 
	}
	

	</script>
	
  <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <%-- <script src="https://code.jquery.com/jquery-1.12.4.js"></script> --%>
  	<style>
  #container {
    width: 900px;
    display:block;
    margin:0 auto;
    border: 1px solid #ddd  ;

    height:100%;
   
    z-index: 9999999 !important;
}
td.fc-other-month {
   visibility: hidden;
}
td.fc-other-month {
   visibility: hidden;
}
	</style>
 		<script>
	var btn = $('#hide,#show').click(function() { // bind click handler to both button
		if(document.getElementById('startDate').value!="" && document.getElementById('endDate').value!=""){
		  $(this).hide(); // hide the clicked button
		  btn.not(this).show(); // show the another button which is hidden
		}
		});
	</script>
<%--     <script>
  $( function() {
    $("#startDate" ).datepicker({minDate:0});
    $("#endDate" ).datepicker({minDate:0});
    $("#checkin" ).datepicker({minDate:0});
    $("#checkout" ).datepicker({minDate:0});
  });
  </script> --%>
       