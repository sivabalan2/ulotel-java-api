  <%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
   pageEncoding="ISO-8859-1"%>
   <section id="innerPageContent">
        <div class="headingBar">
             &nbsp;
        </div>
        
        <div class="container tabBG">
            <div class="row">
                <div class="col-sm-12 tabContent">
                      <h1 class="heading1">Contact <span>Us</span></h1>
                      <div class="contactContent">
                        <div class="row">
                            <div class="col-sm-5">
                                <h6>Need Support Query?</h6>
                                <!-- Contact Form -->
                                 <div id="contact_form" class="contact_form">
                                 <div id="message"><s:actionmessage theme="simple" /></div>
                                <form method="post" class="form-horizontal" id="contactform" class="row" action="ulocontact" name="contactform" method="post">
                                    <input type="hidden" name="action" id="action" value="registerSubmit" />
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <input type="text" class="form-control" name="email" id="email"  placeholder="Your Email" name="email" value="" required />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <input type="text" class="form-control" id="phone" placeholder="Your phone no." name="phone" value="" required />
                                        </div>
                                    </div>                    
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <input type="text" class="form-control" name="name" id="name" placeholder="Email" name="contact_email" value="" required />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <textarea rows="5" type="text" class="form-control" name="message" id="message"name="message" value="" required></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <button type="submit" value="SEND" name="submitLogin" id="submitLogin" class="btn greenBtn boxShadow">Submit</button>
                                        </div>
                                    </div> 
                                </form>
                                </div>
                                <!-- //Contact Form -->
                            </div>
                            <!-- Contact Address -->
                            <div class="col-sm-6 col-sm-offset-1 contactAddr">
                                <h6>Contact address</h6>
                                <p>
                                    <span>Office:</span><br />
                                    Ulo Hotels Private Limited<br />
                                   1/4, Plot No: 251, Second Floor, Rajamannar Salai,19th Street,<br />
                                  Vijayaraghavapuram, Saligramam, Chennai, Tamil Nadu 600093
                                </p>
                                <p>
                                    <span>Toll Free:</span><br />
                                    1800 3000 8686
                                </p>
                                <p>
                                    <span>Phone:</span><br />
                                    +044 4856 7649 
                                </p>
                                  <p>
                                    <span>Phone:</span><br />
                                    +044 4856 7639 
                                </p>
                                <p>
                                    <span>Mail: </span><br />
                                    <a href="mailto:support@ulohotels.com">support@ulohotels.com</a>
                                </p>
                            </div>
                            <!-- //Contact Address -->
                        </div>
                        
                        <div class="separator rspSep"></div>
                        
                        <!-- Contact Map -->
                        <div class="row">
                            <div class="col-sm-12">
                               <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15547.584413461323!2d80.2167667!3d13.0422848!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x6121c6e19c9a0375!2sUlo+Hotels!5e0!3m2!1sen!2sin!4v1513588615504" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                            </div>
                        </div>
                        <!-- //Contact Map -->
                      </div>
                </div> 
            </div>
        </div>
        
    </section>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script src="js1/ngprogress.js" type="text/javascript"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script> 
<script>
   var app = angular.module('myApp', ['ngProgress']);
   app.controller('customersCtrl', function ($scope, $http, $timeout, ngProgressFactory) {
    $scope.userData = [];
   
    	$scope.login = function () {
   
   
    		var fdata = "username=" + $('#username').val() +
    			"&password=" + $('#password').val();
    		//alert(fdata);
   
    		$http({
    			method: 'POST',
    			data: fdata,
    			headers: {
    				'Content-Type': 'application/x-www-form-urlencoded'
    			},
    			url: 'ulologin'
   
    		}).then(function successCallback(response) {
    			$scope.userData = response.data;
    			console.log($scope.userData);
    			var username = $scope.userData.data[0].userName;
    			var email = $scope.userData.data[0].emailId;
    			var phone = $scope.userData.data[0].phone;
   
    			$('#loginModal').modal('toggle');
    			$('#logSec').html("<div class='mnuContant'><a href='mailto:support@ulohotels.com' title='support@ulohotels.com'><i class='fa fa-envelope'></i><span class='visibleLG'>support@ulohotels.com</span></a><a href='tel:9543592593' title=''+91 95435 92593'><i class='fa fa-phone'></i><span class='visibleLG'>+91 95435 92593</span></a></div><div class='mnuProfile'><div class='mpAvatar hidden-xs' style='background:url(ulowebsite/images/avatar.png);'></div><div class='mpUsername'><a href='javascript:showpro();' class=' mnuProfileOpen'>Hi admin<i class='fa fa-angle-down'></i></a></div><div class='mpSubMenu' id='showmenu' ><ul><li><a href='ulouserprofile'>Profile</a></li><li><a href='ulouserprofile'>Booking History</a></li><li><a href='ulologout'>Log out</a></li></ul></div></div>");
    		}, function errorCallback(response) {
   
    			var alert = ' <div class="alert alert-danger alert-dismissible" style="color:red">Invalid username or password.</div>';
    			$('#loginerror').html(alert);
    		    $(function() {
    	          // setTimeout() function will be fired after page is loaded
    	          // it will wait for 5 sec. and then will fire
    	          // $("#successMessage").hide() function
    	          setTimeout(function() {
    	              $("#loginerror").hide('blind', {}, 100)
    	          }, 5000);
    	      });
    		});
   
    	};
   
    	$scope.signup = function () {
   
   
    		var fdata = "userName=" + $('#userName').val() +
    			"&phone=" + $('#mobilePhone').val() +
    			"&emailId=" + $('#emailId').val() +
    			"&roleId=" + $('#roleId').val() +
    			"&accessRightsId=" + $('#accessRightsId').val() +
    			"&password=" + $('#signpassword').val();
    		//alert(fdata);
   
    		$http(
   
    			{
    				method: 'POST',
    				data: fdata,
    				headers: {
    					'Content-Type': 'application/x-www-form-urlencoded'
    				},
    				url: 'ulosignup'
    			}).then(function successCallback(response) {
   
   
    			if (response.data == "") {
   
    				var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:red">Sorry This Email Id already exist.</div>';
    				$('#signuperror').html(alertmsg);
   
   
    			} else {
   
    				$scope.userData = response.data;
    				var username = $scope.userData.data[0].userName;
    				var email = $scope.userData.data[0].emailId;
    				var phone = $scope.userData.data[0].phone;
   
   
    				$('#registerModal').modal('toggle');
   
    				$('#logSec').html("<div class='mnuContant'><a href='mailto:support@ulohotels.com' title='support@ulohotels.com'><i class='fa fa-envelope'></i><span class='visibleLG'>support@ulohotels.com</span></a><a href='tel:9543592593' title=''+91 95435 92593'><i class='fa fa-phone'></i><span class='visibleLG'>+91 95435 92593</span></a></div><div class='mnuProfile'><div class='mpAvatar hidden-xs' style='background:url(ulowebsite/images/avatar.png);'></div><div class='mpUsername'><a href='javascript:showpro();' class=' mnuProfileOpen'>Hi admin<i class='fa fa-angle-down'></i></a></div><div class='mpSubMenu' id='showmenu' ><ul><li><a href='ulouserprofile'>Profile</a></li><li><a href='ulouserprofile'>Booking History</a></li><li><a href='ulologout'>Log out</a></li></ul></div></div>");
    			}
   
    		}, function errorCallback(response) {
   
   
    			var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:red">This Email ID Already Exist</div>';
    			$('#signuperror').html(alertmsg);
   
    			// or server returns response with an error status.
    		});
   
    	};
   
   
    	$scope.passwordRequest = function () {
   
   
    		var fdata = "emailId=" + $('#forgetEmailId').val();
   
   
    		$http({
    			method: 'POST',
    			data: fdata,
    			headers: {
    				'Content-Type': 'application/x-www-form-urlencoded'
    			},
    			url: 'ulosendpassword'
    		}).then(function successCallback(response) {
   
    			var alertmsg = ' <div class="alert alert-success alert-dismissible" style="color:red">Password Sent Sucessfully.</div>';
   
    			$('#forgetmessage').html(alertmsg);
   
   
    		}, function errorCallback(response) {
   
   
    			var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:red">Your Email ID does not Registred Yet</div>';
    			$('#forgetmessage').html(alertmsg);
   
   
    		});
   
    	};
   
    	$('#forgetPanel').hide();
    	$scope.showForget = function () {
   
    		$('#loginPanel').hide();
    		$('#forgetPanel').show();
   
   
    	};
   
    	$scope.showLogin = function () {
   
    		$('#forgetPanel').hide();
    		$('#loginPanel').show();
   
   
    	};
   
   $timeout(function () {
     	$('#btnclose').click();
     }, 1000);
     $timeout(function () {
     	$('#btncloses').click();
     }, 1000);
   
   });
   
   
</script>    