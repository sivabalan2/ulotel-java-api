<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<!-- ulotel payment page desktop begins -->
<section class="dpaymentpage hidden-xs hidden-sm" id="dpaymentpage" style="display:none;">
<div class="container">
<div class="row">
<!-- user detail -->

<div class="col-md-6 col-xs-12">
<div class="col-xs-12 dpayuserdetail">
<h4>Guest Info</h4>
<form name="desktopguestform" id="desktopguestform">
<input type="hidden" class="form-control"  name="userId" id="userId" value="<s:property value="#session.uloUserId"/>" placeholder="usrid">
<div class="form-group">
<input type="hidden" class="form-control" ng-model="UserIds" name="userIds" id="userIds"  placeholder="usrid"> <%--  value="<s:property value="#session.uloUserId"/>" --%>
<input type="text" class="form-control pform" name="firstName"  id="firstName" ng-model="username"  value="<%= ((session.getAttribute("uloUserName")==null)?"":session.getAttribute("uloUserName")) %>"  placeholder="Name" ng-required="true" ng-pattern="/^[a-zA-Z\s]*$/" ng-required="true" maxlength="50">  <%-- value="<%= (session.getAttribute("uloUserName")) %>" --%>
<span ng-show="desktopguestform.firstName.$error.pattern" style="color:red">Enter your name </span>
</div>
<div class="form-group">
<input type="text" class="form-control pform" name="mobilePhone1" ng-model="userphone" id="mobilePhone1" value="{{user[0].o_phone}}" placeholder="Mobile No"  ng-pattern="/^[0-9]*$/"  ng-required="true" maxlength="10" ng-minlength="10">
<span ng-show="desktopguestform.mobilePhone1.$error.pattern" style="color:red">Enter the valid mobile number!</span> 
</div>
<div class="form-group">
<input type="email" class="form-control pform" name="guestEmail" ng-model="useremail" id="guestEmail" value="{{user[0].o_email}}" placeholder="Email" ng-required="true" ng-pattern="/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i">
   <span ng-show="desktopguestform.guestEmail.$error.pattern" style="color:red">Enter the email id!</span>
                        
</div>

<div class="form-group">
<h6 class="text-center">I agree <a href="guest-policy" target="_blank">Hotel Policies</a> and <a href="termsandconditions"  target="_blank">Terms and Conditions</a></h6>

</div>
<div class="col-xs-12"  ng-if="username == ''" >
<div class="form-group"> 
<h4 class="text-center">Or</h4>
</div>
<div class="col-xs-6">
<div class="form-group">
<button class="dpgooglebutton" ng-click = "googleLogin();">Google</button>
</div>
</div>
<div class="col-xs-6">
<div class="form-group">
<button  class="dpfbbutton" ng-click ="facebookLogin();">Facebook</button>
</div>
</div>
</div>
<div class="form-group"  ng-if="properties[0].payAtHotelStatus == 'true'">
<button type="submit" class="dpaylaterbutton" ng-click="desktopguestform.$valid && sendPayAtHotelOtp()" >Pay @ Hotel</button>  <!-- data-toggle="modal" data-target="#mypaymodal" -->
</div>
<div class="form-group" ng-repeat="bd in bookingDetails track by $index">
<button type="submit"  class="dpaynowbutton" ng-click="desktopguestform.$valid && dpbooking($index)">{{dpaynotification}}</button>
<h6 ng-if="bd.paynowDiscount !=0" class="text-center">Click Pay Now TO Get {{bd.paynowDiscount}}RS Cashback</h6>
</div>
</form>
</div>
</div>
<!-- paymentdetail -->
<div class="col-md-6 col-xs-12" ng-repeat="bd in bookingDetails">
<div class="col-xs-12  dpayhoteldetail">
<div class="row">
<img src="get-property-thumb?propertyId={{bd.propertyId}}" alt="Property name" width="100%" height="300px" class="hotelpayimage">
<div class="col-xs-12">
<h2 class="phname">{{bd.propertyName}}</h2>
<h5 class="phaddress">{{bd.city}}</h5>
</div>
</div>
<div class="row ">
<div class="col-xs-5">
<label>Check In</label>
<h5 class="dpcheckin">{{bd.paymentArrival}}</h5>
</div>
<div class="col-xs-2">
<img src="contents/images/menu-icon/sep2.png" alt="seacrh" class="dpcheckinimg"/>
</div>
<div class="col-xs-5">
<label>Check Out</label>
<h5 class="dpcheckin">{{bd.paymentDeparture}}</h5>
</div>
</div>
<div class="row payroomdetailbox">
<div class="col-xs-3"><h4>{{bd.accommodationType}}  <span class="roomx">x</span> {{bd.rooms}}</h4></div>
<div class="col-xs-3"><h4>{{getTotalAdult() + getTotalChild()}} Guest </h4></div>
<div class="col-xs-3"><h4>{{bd.diffDays}} Night</h4></div>
<div class="col-xs-3"><h4><i class="fa fa-rupee"></i> {{bd.baseOriginAmount}}</h4></div>
</div>
<!-- <div class="row ">
<div class="col-xs-12"><h4 class="pd">Payment Details</h4></div>
</div> -->
<div class="row ">
<%-- <div class="col-xs-8"><span class="dpreason">Room Night Price</span></div>
<div class="col-xs-4"> <span class="dpreasonrate"><i class="fa fa-rupee"></i>{{bd.baseOriginAmount}}</span> </div>
 --%>
 <div class="col-xs-8" ng-if="bd.promotionName != ''"><span class="dpreason dpreasonoffer">Offer Applied</span></div>
<div class="col-xs-4" ng-if="bd.promotionFirstName != ''"> <span class="dpreasonrate dpreasonoffer">{{bd.promotionFirstName}}</span> </div>
<div class="col-xs-4" ng-if="bd.promotionSecondName != ''"> <span class="dpreasonrate dpreasonoffer">{{bd.promotionSecondName}}</span> </div>
<div class="col-xs-8" ng-if="bd.discountName != 0" ><span class="dpreason  "><span class="dpappliedcoupon">{{bd.discountName}}</span> Coupon Applied</span></div>
<div class="col-xs-4" ng-if="bd.discountName != 0"> <span class="dpreasonrate"> <i class="fa fa-rupee"></i>{{bd.dropedDiscountPrice}}</span> </div>
<div class="col-xs-8"><span class="dpreasonsave dpreasonofferprice">Discounted Price</span></div>
<div class="col-xs-4"> <span class="dpreasonsaverate dpreasonofferprice"> <i class="fa fa-rupee"></i> {{getTotalSaveAmount()}}</span> </div>

<div class="col-xs-8" ng-if="bd.usedRewardPoints != 0 "><span class="dpreason">Ulo Credit Applied <a ng-click="removeUloReward()">Remove</a></span></div>
<div class="col-xs-4" ng-if="bd.usedRewardPoints != 0 "> <span class="dpreasonrate"><i class="fa fa-rupee"></i>{{bd.total}}</span> </div>
<div class="col-xs-12" ng-if="user[0].rewardPoints != null"><span class="dcreditlockpoint" ng-if="bd.usedRewardPoints == 0 ">Use <strong>{{user[0].rewardPoints}}</strong> Credit for ulo booking <a ng-click="applyRewardPoints()">Apply</a></span></div>
<input type="hidden" value="{{user[0].rewardPoints}}" id="userRewardPoints" />
<div class="col-xs-12" ng-if="username == ''"><Span class="dcreditlocktext"><a class="dcreditlocktext" data-toggle="modal" data-target="#myModal">Login To Get ULO Credit</a></Span></div>

</div>
<div class="row listloginbanner"  ng-if="username == ''">
<div class="col-lg-12">
<div ng-repeat="rd in rewardDetails">
<img src="contents/images/detail/gift.png" alt="ulo login">  
Login To Get Your  {{rd.rewardPoint}} Credit 
<a class="deskloginoffer" data-toggle="modal" data-target="#myModal">Login</a>
</div>
</div>
</div>
<div class="row dfinalpaybox">
<div class="col-xs-8"><span class="tp">Total Pay</span><span class="it"> Incl Tax</span></div>
<div class="col-xs-4"><span class="ta"><i class="fa fa-rupee"></i>{{getBookingTotal()}}</span></div>
</div>
<div class="col-xs-12">
<h6><a class="dviewscroll" ng-click="viewTaxbox();">View Details</a></h6>
<div class="row dfinaltaxbox" id="dfinaltaxbox">
<h6>Tax Breakup</h6>
<div class="col-xs-8"><span class="dpreasontax">Tariff</span></div>
<div class="col-xs-4"> <span class="dpreasontaxrate"><i class="fa fa-rupee"></i> {{getFinalTotal()}}</span></div>
<div class="col-xs-8"><span class="dpreasontax">Tax</span></div>
<div class="col-xs-4"><span class="dpreasontaxrate"><i class="fa fa-rupee"></i> {{getFinalTax()}}</span></div>
<div class="col-xs-8"><span class="tp">Total Pay</span><span class="it"> Incl Tax</span></div>
<div class="col-xs-4"><span class="ta"><i class="fa fa-rupee"></i>  {{getBookingTotal()}}</span></div>
</div>
</div>

</div>
</div>

<!-- paymentdetail -->
</div>
</div>
<!-- Modal -->
<!-- <div id="mypaymodal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-xs mypaymodal">

    Modal content
    <div class="modal-content">
      <div class="modal-body">
        <form>
        <div class="form-group">
<button  class="dpaylaterbutton" data-toggle="modal" data-target="#myotpnowmodal" data-dismiss="modal"> Pay @ Hotel</button>
</div>
<div class="form-group">
<button  class="dpaynowbutton">Pay Now</button>
<h6 class="text-center">Click Pay Now TO Get 500RS Cashback</h6>
</div>

        </form>
      </div>
    </div>

  </div>
</div> -->

</section>
<!-- ulotel payment page  desktop ends-->
<div id="myotpnowmodal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-xs myotpnowmodal">

    <!-- Modal content-->
    <div class="modal-content">
    <div class="modal-header"><h4>Verify Your Mobile Number</h4></div>
      <div class="modal-body">
       <form action="" name="mpuserotp">
       <div ng-repeat="pao in payathotelotp">
        <div class="form-group">
                    <span class="usernote">A One Time Password has been sent via SMS To <a class="usernumber"></a>{{pao.phone}}</span>
                  </div>
                  <div class="row">  
                  <div id="smsapplyerror"></div>
                  <div class="form-group col-sm-8">
                	     <input type="hidden" name="smsGuestId" id="smsGuestId" value="{{pao.guestId}}" />            
                	    <input type="text" class="form-control signupuser" id="smsOtp" placeholder="Enter your OTP" ng-required="true">
                </div>
                <div class="col-sm-4">
                   <span class="btn btn-primary signupbtn " id="disablesms" ng-click="verifySms()" ><span ng-show="smsapply == 'Please Wait'"></span>{{smsapply}}</span>
                </div>
 </div>
      <div class="form-group">
                    <span class="usernotetwo">Haven't received the code yet ? <a class="userotpagain" ng-click="reSendPayAtHotelOtp()">Resend OTP</a></span>
                  </div>
  </div>
  </form>
      </div>
    </div>

  </div>
</div>
<!-- payment page for mobile begins -->
<section class="dpaymentpage hidden-md hidden-lg" id="mpaymentpage" style="display:none;">
<div class="container">
<div class="row">

<!-- paymentdetail -->
<div class="col-md-6 col-xs-12 col-sm-12 dpayhoteldetail"  ng-repeat="bd in bookingDetails track by $index">
<div class="col-xs-12 col-sm-12 ">
<div class="row">
<div class="col-xs-4 col-sm-4">
<img src="get-property-thumb?propertyId={{bd.propertyId}}" alt="Property name"  class="finalpayimage">
</div>
<div class="col-xs-8 col-sm-8">
<h2 class="phname">{{bd.propertyName}}</h2>
<h5 class="phaddress">{{bd.city}}</h5>
</div>
</div>
<div class="row">
<div class="col-xs-12 roomdetailsline">
<h5>Booking Details</h5>
</div>

<div class="col-xs-5">
<label>Check In</label>
<h5 class="dpcheckin">{{bd.paymentArrival}}</h5>
</div>
<div class="col-xs-2">
<img src="contents/images/menu-icon/sep2.png" alt="seacrh" class="dpcheckinimg"/>
</div>
<div class="col-xs-5">
<label>Check Out</label>
<h5 class="dpcheckin">{{bd.paymentDeparture}}</h5>
</div>
</div>
<div class="row  proomtypes">
<div class="col-xs-6"><h4>{{bd.accommodationType}}  * {{bd.rooms}}</h4></div>
<div class="col-xs-3"><h4>{{getTotalAdult() + getTotalChild()}} Guest</h4></div>
<div class="col-xs-3"><h4>{{bd.diffDays}} Night</h4></div>
</div>
<div class="row ">
<div class="col-xs-12 roomdetailsline"><h5>Payment Details</h5></div>
<div class="col-xs-8"><span class="dpreason">Total Room Night Price</span></div>
<div class="col-xs-4"> <span class="dpreasonrate"><i class="fa fa-rupee"></i>{{bd.baseOriginAmount}}</span> </div>
<div class="col-xs-8" ng-if="bd.promotionFirstName != ''"><span class="dpreason">{{bd.promotionFirstName}}</span></div>
<div class="col-xs-8" ng-if="bd.promotionSecondName != ''"><span class="dpreason">{{bd.promotionSecondName}}</span></div>
<div class="col-xs-4" ng-if="bd.promotionName != ''"> <span class="dpreasonrate"><i class="fa fa-rupee"></i>{{bd.baseOriginAmount - bd.baseAmount}}</span> </div>
<div class="col-xs-8" ng-if="bd.discountName != 0"><span class="dpreason"><span class="dpappliedcoupon">{{bd.discountName}}</span> Coupon Applied</span></div>
<div class="col-xs-4" ng-if="bd.discountName != 0"> <span class="dpreasonrate"> <i class="fa fa-rupee"></i>{{bd.dropedDiscountPrice}}</span> </div>
<div class="col-xs-8"><span class="dpreasonsave">Discounted Price</span></div>
<div class="col-xs-4"> <span class="dpreasonsaverate"> <i class="fa fa-rupee"></i>{{getTotalSaveAmount()}}</span> </div>

<div class="col-xs-8" ng-if="bd.usedRewardPoints != 0 "><span class="dpreason">Ulo Credit Applied <a ng-click="removeUloReward()">Remove</a></span></div>
<div class="col-xs-4"> <span class="dpreasonrate" ng-if="bd.usedRewardPoints != 0 "><i class="fa fa-rupee"></i>{{bd.dropedRewardPrice}}</span> </div>
<div class="col-xs-12"><span class="dcreditlockpoint" ng-if="user[0].rewardPoints != null">Use <strong>{{user[0].rewardPoints}}</strong> Credit for ulo booking <a ng-click="applyRewardPoints()">Apply</a></div>
<div class="col-xs-12" ng-if="username == ''"> <Span class="dcreditlocktext"><Span class="dcreditlocktext"><a class="dcreditlocktext" data-toggle="modal" data-target="#mymobileModal">Signin To Get ULO Credit</Span></div>

</div>

<div class="row dfinalpaybox">
<div class="col-xs-8"><span class="tp">Total Pay</span><span class="it"> Incl Tax</span></div>
<div class="col-xs-4"><span class="ta"><i class="fa fa-rupee"></i>{{getBookingTotal()}}</span></div>
<div class="col-xs-12">
 <h6><a class="dviewscroll" ng-click="mviewTaxbox();">View Details</a></h6> 
</div>
<div class="col-xs-12 dfinaltaxbox" id="dfinaltaxboxmobile">

<h6>Tax Breakup</h6>
<div class="col-xs-8"><span class="dpreasontax">Tariff</span></div>
<div class="col-xs-4"> <span class="dpreasontaxrate"><i class="fa fa-rupee"></i>{{getFinalTotal()}}</span></div>
<div class="col-xs-8"><span class="dpreasontax">Tax</span></div>
<div class="col-xs-4"><span class="dpreasontaxrate"><i class="fa fa-rupee"></i>{{getFinalTax()}}</span></div>
<div class="col-xs-8"><span class="tp">Total Pay</span><span class="it"> Incl Tax</span></div>
<div class="col-xs-4"><span class="ta"><i class="fa fa-rupee"></i>{{getBookingTotal()}}</span></div>

</div>
</div>



</div>
</div>

<!-- paymentdetail -->
<!-- user detail -->

<div class="col-md-6 col-xs-12 dpayuserdetail">
<div class="col-xs-12 ">
<h4>Guest Info</h4>
<form name="mobileguestform" id="mobileguestform"><input type="hidden" class="form-control" name="userId" id="userId" value="<s:property value="#session.uloUserId"/>" placeholder="usrid">
<div class="form-group">
<input type="hidden" class="form-control" ng-model="UserIds" name="userIds" id="userIds"  placeholder="usrid"> <%--  value="<s:property value="#session.uloUserId"/>" --%>
<input type="text" class="form-control pform" name="mbfirstName" id="mbfirstName" ng-model="username"  value="<%= ((session.getAttribute("uloUserName")==null)?"":session.getAttribute("uloUserName")) %>" placeholder="Name" ng-required="true" ng-pattern="/^[a-zA-Z\s]*$/" ng-required="true" maxlength="50">
<span ng-show="mobileguestform.mbfirstName.$error.pattern" style="color:red">Enter your name </span>
</div>
<div class="form-group">
<input type="text" class="form-control pform" name="mbMobilePhone1" id="mbMobilePhone1" ng-model="userphone" value="{{user[0].o_phone}}" placeholder="Mobile No"  ng-pattern="/^[0-9]*$/"  ng-required="true" maxlength="10" ng-minlength="10">
<span ng-show="mobileguestform.mbMobilePhone1.$error.pattern" style="color:red">Enter the valid mobile number!</span> 
</div>
<div class="form-group">
<input type="email" class="form-control pform" name="mbguestEmail" id="mbguestEmail" ng-model="useremail" value="{{user[0].o_email}}" placeholder="Email" ng-required="true" ng-pattern="/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i">
   <span ng-show="mobileguestform.mbguestEmail.$error.pattern" style="color:red">Enter the email id!</span>
                        
</div>

<div class="form-group">
<h6 class="text-center">I agree <a href="guest-policy" target="_blank">Hotel Policies</a> and <a href="termsandconditions"  target="_blank">Terms and Conditions</a></h6>
</div>
<div class="col-xs-12" ng-if="username == ''">
<div class="form-group">
<h4 class="text-center">Or</h4>
</div>
<div class="col-xs-6">
<div class="form-group">
<button class="dpgooglebutton" ng-click = "googleLoginMobile();">Google</button>
</div>
</div>
<div class="col-xs-6">
<div class="form-group">
<button  class="dpfbbutton" ng-click = "facebookLoginMobile();">Facebook</button>
</div>
</div>
</div>
<div class="mbfixpayment">
<div class="form-group" ng-if="properties[0].payAtHotelStatus == 'true'">
<button type="submit"  class="dpaylaterbutton" ng-click="mobileguestform.$valid && mbsendPayAtHotelOtp()" >Pay @ Hotel</button>  <!-- data-toggle="modal" data-target="#mypaymodal" -->
</div>
<div class="form-group" ng-repeat="bd in bookingDetails track by $index">
<button  type="submit"  class="dpaynowbutton" ng-click="mobileguestform.$valid && mbbooking($index)  ">{{mbpaynotification}}</button>
<h6 ng-if="bd.paynowDiscount != 0" class="text-center">Click Pay Now TO Get {{bd.paynowDiscount}} RS Cashback</h6>
</div>
</div>
</form>
</div>
</div>
</div>
</div>

</section>
<!-- payment page for mobile ends -->
<!-- Ulohotel detail page for mobile begins rock -->
<section class="mobiledetailpage hidden-md hidden-lg hiddex-sm " id="mobiledetailpage" style="display:block;">
<!-- mobile carousal begins -->
<div class="container" >
<div class="row">
<!-- Mobile Carousel begins -->
<!-- Mobile Carousel begins -->
 <img class="imgheight img-responsive" src="get-property-thumb?propertyId={{properties[0].DT_RowId}}">
<a data-toggle="modal" data-target="#mobileimagescroll" class="mlistcallpopup"><img src="contents/images/listpopicon.png"  alt="seacrh" class="dhsep"/></a>
<a href="javascript:void(0)" onclick="goBack()" class="mlistcallback"><img src="contents/images/menu-icon/backarrow.png" alt="ulo contact number"></a>         
<!-- Modal -->
<div id="mobileimagescroll" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content ">
      <div class="modal-header">
        <a class="close closebox" data-dismiss="modal"><img src="contents/images/detail/close.png"></a>
       <a class="close backbox" data-dismiss="modal"><img src="contents/images/detail/back.png"></a>
      </div>
<ul class="nav nav-tabs">
<li ng-repeat="cp in commonPhotos"><a data-toggle="tab active" href="#home{{cp.id}}">{{cp.Name}}</a></li>

<li ng-repeat="ap in accommPhotos"><a data-toggle="tab" href="#home{{ap.accommodationId}}">{{ap.accommodationType}}</a></li>

</ul>

<div class="tab-content" >
 <div ng-repeat="cp in commonPhotos" id="home{{cp.id}}" class="tab-pane fade in active">
 <div ng-repeat="pt in cp.photos">
 <img ng-src="get-property-image-stream?propertyPhotoId={{pt.photos}}">
 </div>
  </div>
  <div ng-repeat="ap in accommPhotos" id="home{{ap.accommodationId}}" class="tab-pane fade in ">
  <div  ng-repeat="pt in ap.photos">
  <img ng-src="get-property-image-stream?propertyPhotoId={{pt.photoPath}}">
  </div>
  </div>
</div>
      
    </div>

  </div>
</div>

<!-- Mobile carousel ends -->
</div>
<!-- Property address -->
<div class="row whiteboard">
<div class="col-xs-12  " ng-repeat="p in properties track by $index">
<h4 class="mname">{{properties[0].propertyName}}</h4>
<h5 class="mlocation"><img src="contents/images/detail/map.png" alt="ulo location">{{properties[0].city}}</h5>
<div ng-repeat="review in p.reviews"><button class="dmrating"><span class="dmratingcount">{{review.starCount}} </span><span class="dmratingtext"> {{review.propertyRating}}</span></button><a class="mviewrclick" id="mtotalscrollbtn" ng-click="reviewscrollbtn()" href="JavaScript:Void(0);">View Review</a></div>
</div>
<!-- Mobile Amenities begins -->
<div class="col-xs-12  ">
<h5>Amenities</h5>
<div class="mobileicondiv mobileicondivnew"  id="mtext" ng-repeat="p in properties track by $index" >
<ul>
    <li ng-repeat="amm in p.amenities | orderBy:'amenityName'"><img ng-src="{{amm.icon}}"></li>
</ul>
</div>
<div class="rows ">
<a id="mmore" class="dmorebtn">View More <i class="fa fa-caret-down"></i></a>
<a id="mless" class="dmorebtn" style="display:none">View Less <i class="fa fa-caret-down"></i></a>
</div>
</div>
<!-- Mobile Amenities ends -->
</div>
<!-- property amenitites -->
<!-- Property Room  types -->
<div class="row whiteboard">
<div class="col-xs-12 ">
<h5 class="dselectcategory"><img src="contents/images/detail/pax.png" alt="ulo location"> YOUR STAY DATE</h5>
<div class="col-xs-12 mobileselectdate">
<div class="col-xs-2">
<img src="contents/images/menu-icon/date2.png" alt="seacrh" class="dhflag"/>
</div>
<span class="col-xs-4 ">
<input type="text" class="dateform" id="mdatepicker0" value="<%= ((request.getAttribute("mbcheckIn2")==null)?session.getAttribute("mbcheckIn2"):request.getAttribute("mbcheckIn2")) %>" onkeypress="return false;" readonly>
<input type="hidden" id="minput1" value="<%= ((request.getAttribute("mbcheckIn1")==null)?session.getAttribute("mbcheckIn1"):request.getAttribute("mbcheckIn1")) %>" size="10">
			<input type="hidden" id="malternate" value="<%= ((request.getAttribute("mbcheckIn")==null)?session.getAttribute("mbcheckIn"):request.getAttribute("mbcheckIn")) %>" size="10">



</span>
<div class="col-xs-2"><img src="contents/images/menu-icon/sep2.png" alt="seacrh" class="dhflag"></div>
<span class="col-xs-4">

<input type="text" class="dateform"  id="mdatepicker1" value="<%= ((request.getAttribute("mbcheckOut2")==null)?session.getAttribute("mbcheckOut2"):request.getAttribute("mbcheckOut2")) %>" onkeypress="return false;" readonly>
<input type="hidden" id="minput2" value="<%= ((request.getAttribute("mbcheckOut1")==null)?session.getAttribute("mbcheckOut1"):request.getAttribute("mbcheckOut1")) %>" size="10">
<input type="hidden" id="malternate1" value="<%= ((request.getAttribute("mbcheckOut")==null)?session.getAttribute("mbcheckOut"):request.getAttribute("mbcheckOut")) %>" size="10">
</span>

</div>

</div>
<div class="col-xs-12">
<div class="mbdatepicker"></div>
</div>
<div ng-repeat="p in properties track by $index">
<div class="col-xs-12 mroomselect ">
<h5 class="dselectcategory"><img src="contents/images/detail/bed.png" alt="ulo location"> SELECT ROOM CATEGORY</h5>
</div>

<div class="col-xs-12  mroomselect">
<select ng-model="mbRoomType" class="mroomdrop form-control" ng-change="getMobileAccommodationAvailability($index,mbRoomType,p.arrivalDate,p.departureDate)">
<option ng-repeat="accom in p.jsonAccommodations" ng-selected ="p.accommodationId == accom.accommodationId" value="{{accom.accommodationId}}">{{accom.accommodationName}}</option>
</select>
</div>
<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
 <div class="droominfo" id="droominfo"><span class="droomone">*{{p.accommodationName}}</span> <span class="droompersoncount">Maximum Occupancy Of {{p.maxOccupancy}} Guest</span></div>
</div>
<div class="col-xs-12 ">
<h5 class="dselectcategory"><img src="contents/images/detail/pax.png" alt="ulo location"> YOUR STAY DETAILS</h5>
</div>
<div class="col-xs-12 mroomperson">
<div class="col-xs-5">
<select ng-init="mbrm.value = 1" ng-model="mbrm.value" class="form-control mrcount" ng-change="updateRoomAvailable($index,mbrm.value,mbad.value,mbch.value,p.available,p.baseAmount,p.minOccupancy,p.maxOccupancy,p.extraAdultAmount,p.extraChildAmount,p.arrivalDate,p.departureDate,p.promotionId,p.totalAmount,p.discountBaseAmount,p.baseActualAmount,p.tax,p.taxPercentage);totalAdultCheck($index,p.maxOccupancy,mbrm.value)">
<option class="droomvalue" ng-repeat="rm in range(1,p.available)" value="{{rm}}"> Room {{rm}} </option>
</select>

</div>
<div class="col-xs-7 mgbox">
<select ng-init="mbad.value = 1" ng-model="mbad.value"  class=" mguestcountbox" ng-change="updateAvailable($index,mbrm.value,mbad.value,mbch.value,p.available,p.baseAmount,p.minOccupancy,p.maxOccupancy,p.extraAdultAmount,p.extraChildAmount,p.arrivalDate,p.departureDate,p.promotionId,p.totalAmount,p.discountBaseAmount,p.baseActualAmount,p.tax,p.taxPercentage);minimumOccupancyCheck($index,p.maxAdult,ad.value)">
<option ng-repeat="rm in range(1,p.maxAdult)" class="dadultvalue" value="{{rm}}">{{rm}} Adult</option>
</select>
<select ng-init="mbch.value = 0"  ng-model="mbch.value" class="mguestcountboxchild" ng-change="updateAvailable9($index,mbrm.value,mbad.value,mbch.value,p.available,p.baseAmount,p.minOccupancy,p.maxOccupancy,p.extraAdultAmount,p.extraChildAmount,p.arrivalDate,p.departureDate,p.promotionId,p.totalAmount,p.discountBaseAmount,p.baseActualAmount,p.tax,p.taxPercentage)">
 <option ng-repeat="rm in range(0,p.maxAdult)" value="{{rm}}">{{rm}} Child</option>
</select>



</div>
</div>
<div class="col-xs-12 ">
<h5 class="dselectcategory">Inclusion </h5>
<div class="dinclusion">
<ul>
    <li><i class="fa fa-check-circle"></i> Accommodations</li>
    <li><i class="fa fa-check-circle"></i> WiFi</li>
        <li><i class="fa fa-check-circle"></i> Toiletries</li>
</ul>
</div>
</div>
<!-- inclusion click -->
<div class="col-xs-12 mselectbox" ng-show="p.available  !== '0' && showloaderpaynow">
<h5>Booking Details </h5>
<div class="dselecteddata">
<span class="dselectedroomdata" >{{p.accommodationName}} X  {{p.diffDays}} Nights {{getAdults() + getChild()}} Guest  </span>
<span class="dselectedstrikeprice "><i class="fa fa-rupee"></i>{{getBaseActualTotal()}}</span><span class="dselectedtotalprice"> <i class="fa fa-rupee"></i>{{getTotal()}}</span> 
</div><!-- selected data -->
</div>
<div class="row" ng-show="showloaderbooknow">
<div class="detailloadbox"></div>
</div>  
<div class="row detailsoldnotes" ng-show="showloaderpaynow">
<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
<button class="btn soldoutbtn" ng-show ="p.available === '0'"> SOLD OUT </button>
</div>
<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
<p ng-show ="p.available === '0'" class="soldoutnote">We are unable to serve on these dates,Please select another date or hotel</p>

</div>
</div>   
<div class="row" ng-show="showloaderbooknow">
<div class="detailloadbox"></div>
</div>    
              
<div class=""  ng-if="username == ''">
<div class="col-xs-12 dofferone" ng-if="username == ''" ng-repeat="rd in rewardDetails">
<img src="contents/images/detail/gift.png" alt="ulo earn credit">To Get Cashback Upto <i class="fa fa-rupee"></i> {{rd.rewardPoint}} <span  ><a class="dofferlogin" data-toggle="modal" data-target="#mymobileModal">Login</a></span>
</div><!-- Offer click -->
</div>
<div ng-if="user[0].rewardPoints != null">
<label class="hide"><input type="checkbox"> Apply Credit</label>
<div class="creditwelcome">
<h5>Congratulations you have earned {{user[0].rewardPoints}} credits </h5>
<h5>please apply the credits during the checkout</h5>
</div>
</div>
<div ng-show="showreward">
<label class="hide"><input type="checkbox"> Apply Credit</label>
<div class="creditwelcome">
<h5>Congratulations you have earned {{user[0].rewardPoints}} credits </h5>
<h5>please apply the credits during the checkout</h5>
</div>
</div>

<div class="row " ng-show="p.available  !== '0' && showloaderpaynow ">
<div class="col-xs-12 mobiledetailfixedbox ">
<div class="dselecteddata">
<span class="dselectedroomdata" >{{p.accommodationName}} X {{getRoomCount()}}   <!-- <a data-toggle="modal" data-target="#guestpopupfill" href="JavaScript:Void(0);"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> --></span>
<span class="dselectedroomdata" >{{p.diffDays}} Nights {{getAdults() + getChild()}} Guest  <span class="dselectedstrikeprice "><i class="fa fa-rupee"></i></span><span class="dselectedtotalprice"> <i class="fa fa-rupee"></i> {{getTotal()}} </span>
</span>
<a  href="#bookingdetails" class="btn mpaybtn" ng-click="quickBook($index,mbrm.value,mbad.value,mbch.value);" ng-show="getTotal() !== 0 ">CONTINUE TO PAY</a>

</div>
</div>
</div>
</div>
<!-- Inclusion click begins -->

<!-- Apply coupon ends -->
<!-- Apply coupon -->
<div class="col-xs-12  dofferzone">
<div class="doffersone">Get Extra Discount For Your Booking <a class="mapplycoupon" data-toggle="modal" data-target="#mcouponmodal">Apply Coupon</a></div>
<div id="mcouponmodal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content dcouponpopup">
      <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title text-center">Offer Zone</h4>
        <img src="contents/images/detail/offer1.png" alt="ulo promocode">
       
      </div>
      <div class="modal-body">
      <form>
      <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
            <input type="hidden" class="form-control couponform" id="mbcode_signup" placeholder=""  name="code_signup" value="" required />
            <input type="hidden" class="form-control couponform" id="mbcode_percentage" placeholder=""  name="code_percentage" value="" required />
            <input type="hidden" class="form-control couponform" id="mbcode_id" placeholder=""  name="code_id" value="" required />
            <input type="text" class="form-control couponform" id="mbcode_input" value="">
      </div>
      <div class="col-lg-4 col-md-4	col-sm-4 col-xs-4" >
       <button type="submit" ng-click="mbapplyDiscount()" class="couponbtn"><span ng-show="mapply == 'APPLYING'"></span>{{mapply}}</button>
      </div>
       <div class="col-lg-12 col-md-12	col-sm-12 col-xs-12" >

<div id="couponerrorone"></div>
        <span class="dpickspan">Click To Apply</span>
      </div>
        <div class="col-lg-12 col-md-12	col-sm-12 col-xs-12" >

<div id="couponerrorone"></div>
   
      </div>
    
       
             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" ng-repeat="d in discounts track by $index">
         <div class="dcouponbox"><span class="dcouponcode" ng-click="mbselectDiscount($index)">{{d.discountName}}</span><span class="dcouponoffer">Flat {{d.discountPercentage}} Offer </span></div>
       </div>
       </div>
         </form>
      </div>
         

      </div>

    </div>

  </div>
</div>

</div>
<!-- room count detail begins -->
<div class="row mguestselect ">

</div>
<!-- date select -->

<!-- Prebooking detais -->
<div class="row">

</div>
<!-- amenities accordion -->
 <div class="row ">
   <div class="hidden-lg hidden-md  mobilescroll demo">
      
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
         <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingTwo">
                <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                  Amenities
                     <i class="more-less fa fa-chevron-down"></i>
                        
                    </a>
                </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">
                <div class="scrollhotelamenity" ng-repeat="p in properties">
                <div class="col-xs-12" ng-repeat="amm in p.amenities | limitTo:10 | orderBy:'amenityName'">
                <div class="col-xs-4"><img ng-src="{{amm.icon}}" alt="{{amm.amenityName}}" class="damenityicon" ></div>
                <div class="col-xs-6"><p>{{amm.amenityName}}</p></div>
                  <div class="col-xs-2"></div>
                  
    
                </div>
      </div> 
                   
                  </div> 
               </div>
            </div>
        </div>
     <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingTwo">
                <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                     hotel policies
                     <i class="more-less fa fa-chevron-down"></i>
                        
                    </a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                <div class="panel-body">
               <div class="hotelContent hotelpolicydetail">
                     <span class="policyhead"></span>
                     <p ><%= request.getAttribute("hotelPolicy") %> </p>
                  </div> 
               </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingThree">
                <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                     Near By 
                     <i class="more-less fa fa-chevron-down"></i>
                    </a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                <div class="panel-body">
                  <div >
                <div class="panel-body landmark" ng-repeat="pl in propLandmark">
                          
                        <div class="col-lg-6 col-md-6">
                           <h5 class="text-center">Landmark</h5>
                           <p ng-hide="pl.kilometers1 == 0"> {{pl.propertyLandmark1 }} - {{pl.kilometers1  | number }} Kms</p>
                           <p ng-hide="pl.kilometers2 == 0"> {{pl.propertyLandmark2}} - {{pl.kilometers2  | number }} Kms</p>
                           <p ng-hide="pl.kilometers3 == 0"> {{pl.propertyLandmark3 }} - {{pl.kilometers3  | number }} Kms</p>
                           <p ng-hide="pl.kilometers4 == 0"> {{pl.propertyLandmark4 }} - {{pl.kilometers4  | number }} Kms</p>
                           <p ng-hide="pl.kilometers5 == 0"> {{pl.propertyLandmark5}} - {{pl.kilometers5  | number }} Kms</p>
                        </div>
                        <div class="col-lg-6 col-md-6">
                           <h5  class="text-center">Attractions</h5>
                           <p ng-hide="pl.kilometers6 == 0"> {{pl.propertyLandmark6 }} - {{pl.kilometers6  | number }} Kms</p>
                           <p ng-hide="pl.kilometers7 == 0"> {{pl.propertyLandmark7 }} - {{pl.kilometers7  | number }} Kms</p>
                           <p ng-hide="pl.kilometers8 == 0"> {{pl.propertyLandmark8 }} - {{pl.kilometers8  | number }} Kms</p>
                           <p ng-hide="pl.kilometers9 == 0"> {{pl.propertyLandmark9 }} - {{pl.kilometers9  | number }} Kms</p>
                           <p ng-hide="pl.kilometers10 == 0"> {{pl.propertyLandmark10 }} - {{pl.kilometers10  | number }} Kms</p>
                        </div>
                     </div>
               </div>
                 </div>
            </div>
        </div>

    </div><!-- panel-group -->
    
   </div>
   </div>
 <!--   map div begins -->
 <div class="row whiteboard maplocationmobile">
  <div class="col-xs-12 "> <div class="col-xs-12 "><h5>Location Map</h5></div></div>
<div class="col-xs-12" >
 <div class="col-xs-8 ">
 
 <input type="hidden" id ="lat" name="lat" value="<s:property value="latitude" />">
<input type="hidden" id ="lng" name="lng" value="<s:property value="longitude" />" >
<a href="https://www.google.com/maps/search/?api=1&query=<s:property value="latitude"/>,<s:property value="longitude" />" target="_blank"><div id="mobilemap" style="height:150px;"></div></a>                      
 
 </div>
 <div class="col-xs-4">
 <a class="mviewmap" href="https://www.google.com/maps/search/?api=1&query=<s:property value="latitude"/>,<s:property value="longitude" />" target="_blank">View Map</a>
 </div>
</div>
 </div>
 <!-- Review Section -->
 <div class="row mtotalreviews " id="mtotalreviews">
 <div class="col-xs-12">
  <div class="col-xs-12 ">
 <h5>Reviews</h5>
 </div>
 </div> 
 <!-- google -->
 <div class="col-xs-12 "  ng-repeat="p in properties" >
 <div class="col-xs-7" ng-repeat="review in p.reviews" >
 <h4 ><img src="contents/images/detail/google.png" alt="google reviews of ulo hotels" width='32' height="32"><span class="dgcount">{{review.reviewCount}} Reviews</span></h4>
 
 </div>
  <div class="col-xs-5" ng-repeat="review in p.reviews" >
<a href="" target="_blank" class="dgreviewbtn"><h4>Write Review <i class="fa fa-edit"></i></h4></a>
 </div>
   
<div class="col-xs-12 mgooglereviewbox">
<div class="carousel slide" data-ride="carousel" id="quote-carousel">
        <!-- Bottom Carousel Indicators -->
        <ol class="carousel-indicators">
     
          <li data-target="#quote-carousel" ng-repeat="gr in p.guestReviews   | filter: { reviewerName: 'Google' } track by $index" data-slide-to="{{$index}}"></li>
      
        </ol>
        <!--   <ol class="carousel-indicators thumbs visible-lg visible-md"   >
                           <li data-target="#carousel-custom" ng-repeat="p in photos track by $index"  data-slide-to="{{$index}}" > <img ng-src="get-property-image-stream?propertyPhotoId={{p.DT_RowId}}"  class="img-responsive"></li>
                        </ol> -->
        <!-- Carousel Slides / Quotes -->
        <div class="carousel-inner reviewboxgoogle">
          <!-- Quote 1 -->
          <div class="item"  ng-class="{active:!$index}" ng-repeat="gr in p.guestReviews   | filter: { reviewerName: 'Google' } track by $index">
            
             <div class="row">
                <div class="col-sm-12">
                <div class="col-sm-6">
                 <img class="img-circle" src="ulowebsite/images/reviews/googleguest.png" height="24" width="24" >
                </div>
                <div class="col-sm-6">
                   <small>{{gr.reviewerGuestName}}</small>
                 <span star-rating rating-value="gr.reviewerCount" max="5" ></span>
               
                </div>
                 
              </div>
                <div class="col-sm-12">
                  <p>"{{gr.reviewerDescription}}"</p>
                 
                </div>
              </div>
         
          </div>
 </div>
 </div>
 </div>
 </div>
 <!-- Trip -->
  <div class="col-xs-12 mtripdiv"  ng-repeat="p in properties" >
 <div class="col-xs-7" ng-repeat="review in p.reviews" >
 <h4 ><img src="contents/images/reviews/trs.png" alt="google reviews of ulo hotels" width='32' height="32"><span class="dgcount">{{review.reviewCount}} Reviews</span></h4>
 
 </div>
  <div class="col-xs-5" ng-repeat="review in p.reviews" >
<a href="" target="_blank" class="tgreviewbtn"><h4>Write Review <i class="fa fa-edit"></i></h4></a>
 </div>
 <div class="col-xs-12 ">
<div class="carousel slide reviewboxtrip" data-ride="carousel" id="quote-carousel">
        <!-- Bottom Carousel Indicators -->
        <ol class="carousel-indicators">
     
          <li data-target="#quote-carousel" ng-repeat="gr in p.guestReviews   | filter: { reviewerName: 'tripadvisor' } track by $index" data-slide-to="{{$index}}"></li>
      
        </ol>
        <!--   <ol class="carousel-indicators thumbs visible-lg visible-md"   >
                           <li data-target="#carousel-custom" ng-repeat="p in photos track by $index"  data-slide-to="{{$index}}" > <img ng-src="get-property-image-stream?propertyPhotoId={{p.DT_RowId}}"  class="img-responsive"></li>
                        </ol> -->
        <!-- Carousel Slides / Quotes -->
        <div class="carousel-inner ">
          <!-- Quote 1 -->
          <div class="item"  ng-class="{active:!$index}" ng-repeat="gr in p.guestReviews   | filter: { reviewerName: 'tripadvisor' } track by $index">
            
             <div class="row">
                <div class="col-sm-12">
                <div class="col-sm-12">
                 <img class="img-circle" src="ulowebsite/images/reviews/tripguest.png" height="24" width="24" >
                </div>
                <div class="col-sm-12">
                   <small class="name">{{gr.reviewerGuestName}}</small>
                   
                 <span star-rating2 rating-value="gr.reviewerCount" max="5" ></span>
               
                </div>
                 
              </div>
                <div class="col-sm-12">
                  <p>"{{gr.reviewerDescription}}"</p>
                 
                </div>
              </div>
         
          </div>
 </div>
 </div>

 </div>
 
 </div>

<!-- mobile carousal begins -->


 </div>
<!-- mobile carousal begins -->
</div>
 </section>
<!-- Ulohotel detail page for mobile ends -->
<!-- ulohotel detail page begins for desktop -->
<section class="desktopdetailpage hidden-xs " style="display:block;" id="desktopdetailpage">
<div class="container">
<div class="row">

<div class="col-lg-12 col-md-12 col-sm-12 hidden-xs breadcrumb">
<a href="index" class="breadcrumblist">Home</a> > <a class="breadcrumblist" href="<%= session.getAttribute("breadCrumAreaUrl") %>?checkIn={{StartDate}}&checkOut={{EndDate}}" ><%= session.getAttribute("breadCrumAreaName") %>  </a> > <a class="breadcrumblistactive" href="#"><%= session.getAttribute("breadCrumPropertyName")%></a>
</div>
</div>
<!-- Property Details  -->
<div class="row hoteldetailwhite">
<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 deskcarousel">
<!-- Image carousal begins -->
<div id='carousel-custom' class='carousel slide' data-ride='carousel'>
    <div class='carousel-outer'>
        <!-- Wrapper for slides -->
        <div class='carousel-inner'>
            <div class='item' ng-class="{active:!$index}"  ng-repeat="p in photos track by $index" >
                <img ng-src='get-property-image-stream?propertyPhotoId={{p.DT_RowId}}' alt='' />
                <span class="imageName">{{p.accommodationType}}</span>
                <span class="imageCount">{{$index+1}}</span>
            </div>
       
        </div>
            
        <!-- Controls -->
        <a class='left carousel-control' href='#carousel-custom' data-slide='prev'>
            <span class='fas fa-chevron-left fa-2x'></span>
        </a>
        <a class='right carousel-control' href='#carousel-custom' data-slide='next'>
            <span class='fas fa-chevron-right fa-2x'></span>
        </a>
    </div>
    
    <!-- Indicators -->
    <ol class='carousel-indicators mCustomScrollbar'>
        <li data-target='#carousel-custom' ng-repeat="p in photos track by $index"  data-slide-to="{{$index}}"><img ng-src='get-property-image-stream?propertyPhotoId={{p.DT_RowId}}' alt='' /></li>
    
    </ol>
</div>


<!-- Image carosual ends --> 
 
</div>
<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12" >
<span class="dname">{{properties[0].propertyName}}</span>

<div class="dselecteddate">
<h5 class="dselectcategorydate"><img src="contents/images/detail/pax.png" alt="ulo location"> YOUR STAY DATE</h5>

<div class="col-sm-12 dselectpicker">
<span class="col-sm-6">
<img src="contents/images/menu-icon/date2.png" alt="seacrh" class="dhflag"/>

<input type="text" class="dateform" id="datepicker0" placeholder="21 July Monday"  value="<%= ((request.getAttribute("checkIn2")==null)?session.getAttribute("checkIn2"):request.getAttribute("checkIn2")) %>" onkeypress="return false;" readonly>
<input type="hidden" id="input1"  value="<%= ((request.getAttribute("checkIn1")==null)?session.getAttribute("checkIn1"):request.getAttribute("checkIn1")) %>" size="10">
			<input type="hidden" id="alternate"  value="<%= ((request.getAttribute("checkIn")==null)?session.getAttribute("checkIn"):request.getAttribute("checkIn")) %>" size="10">
</span>
<span class="col-sm-6 ">

<img src="contents/images/menu-icon/sep2.png" alt="seacrh"  class="dhflag"/ >
<input type="text" id="datepicker1"  class="dateform" placeholder="21 July Monday " value="<%= ((request.getAttribute("checkOut2")==null)?session.getAttribute("checkOut2"):request.getAttribute("checkOut2")) %>" onkeypress="return false;" readonly>
<input type="hidden" id="input2" value="<%= ((request.getAttribute("checkOut1")==null)?session.getAttribute("checkOut1"):request.getAttribute("checkOut1")) %>" size="10">
			<input type="hidden" id="alternate1" value="<%= ((request.getAttribute("checkOut")==null)?session.getAttribute("checkOut"):request.getAttribute("checkOut")) %>" size="10">
</span>

</div>

</div><!-- selected date --><!-- selected date -->
 <div class="col-lg-12">
  <div class="datepicker"></div>
</div>

<div class="selecti " ng-repeat="p in properties track by $index">
<span class="dselectcategoryone"><img src="contents/images/detail/bed.png" alt="ulo location"> SELECT ROOM CATEGORY</span>
  <select  ng-model="roomType" class="form-control selectroom " id="roomType" value="" ng-change="getAccommodationAvailability($index,roomType,p.arrivalDate,p.departureDate)" ng-model="roomType"  >
<!-- <option value="select">{{acc.accommodationType}}</option> -->
<option value="{{accom.accommodationId}}"  ng-repeat="accom in p.jsonAccommodations" ng-selected ="p.accommodationName == accom.accommodationName" >{{accom.accommodationName}}</option> 
</select> 
<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
 <div class="droominfo" id="droominfo"><span class="droomone">*{{p.accommodationName}}</span> <span class="droompersoncount">Maximum Occupancy Of {{p.maxOccupancy}} Guest</span></div>
</div>
<span class="dselectcategoryone"><img src="contents/images/detail/pax.png" alt="ulo location"> YOUR STAY DETAILS</span>
<div class="dpersonclick">
<div class="col-sm-5 droomcountnew">
<div class="col-sm-6">
<label>Room</label>
</div>
<div class="col-sm-6 ">
<select name="rooms"  id="rooms" ng-init="rm.value = 1" ng-model="rm.value" class="selectadultcount form-control " ng-change="updateRoomAvailable($index,rm.value,ad.value,ch.value,p.available,p.baseAmount,p.minOccupancy,p.maxOccupancy,p.extraAdultAmount,p.extraChildAmount,p.arrivalDate,p.departureDate,p.promotionId,p.totalAmount,p.discountBaseAmount,p.baseActualAmount,p.tax,p.taxPercentage);totalAdultCheck($index,p.maxOccupancy,rm.value)">
<option class="droomvalue" ng-repeat="rm in range(1,p.available)" value="{{rm}}">{{rm}}</option>
</select>

</div>
</div>
<div class="col-sm-7 dguestcountnew">
 <div class="col-sm-2">
<label>Adult</label>
  </div>
  <div class="col-sm-4">
  <select  ng-init="ad.value = 1" name="adults" ng-model="ad.value"  id="adults"   class="selectadult  form-control " ng-change="updateAvailable($index,rm.value,ad.value,ch.value,p.available,p.baseAmount,p.minOccupancy,p.maxOccupancy,p.extraAdultAmount,p.extraChildAmount,p.arrivalDate,p.departureDate,p.promotionId,p.totalAmount,p.discountBaseAmount,p.baseActualAmount,p.tax,p.taxPercentage);minimumOccupancyCheck($index,p.maxAdult,ad.value)">
  <option ng-repeat="rm in range(1,p.maxAdult)" class="dadultvalue" value="{{rm}}">{{rm}}</option>
  </select>  
  </div>
   <div class="col-sm-2">
<label>Child</label>
  </div>
  <div class="col-sm-4">
  <select ng-init="ch.value = 0"  ng-model="ch.value" name="child" id="child" class="selectadult form-control" ng-change="updateAvailable9($index,rm.value,ad.value,ch.value,p.available,p.baseAmount,p.minOccupancy,p.maxOccupancy,p.extraAdultAmount,p.extraChildAmount,p.arrivalDate,p.departureDate,p.promotionId,p.totalAmount,p.discountBaseAmount,p.baseActualAmount,p.tax,p.taxPercentage)">
  <option class="dadultvalue" ng-repeat="rm in range(0,p.maxChild)" value="{{rm}}">{{rm}}</option>
  </select>
  </div>
</div>

</div><!-- Person click -->
<div class="dinclusion"> 
<span class="dselectcategory">INCLUSION</span>
<ul>
    <li><i class="fa fa-check-circle"></i> Accommodations</li>
    <li><i class="fa fa-check-circle"></i> WiFi</li>
    <li><i class="fa fa-check-circle"></i> Toiletries</li>
</ul>
</div><!-- inclusion click -->
<%-- <s:if test="#session.uloUserId != null" > </s:if> --%> 
<div ng-if="username == ''">
<div class="dofferone" ng-show="dofferlogin" ng-if="user[0].rewardPoints == null"  ng-repeat="rd in rewardDetails">
<img src="contents/images/detail/gift.png" alt="ulo earn credit">To Get Cashback Upto <i class="fa fa-rupee"></i> {{rd.rewardPoint}} <span  ><a class="dofferlogin" data-toggle="modal" data-target="#myModal">Login</a></span>
</div>
</div><!-- Offer click -->
<div ng-if="user[0].rewardPoints != null">
<label class="hide"><input type="checkbox"> Apply Credit</label>
<div class="creditwelcome">
<h5>Congratulations you have earned {{user[0].rewardPoints}} credits </h5>
<h5>please apply the credits during the checkout</h5>
</div>
</div>
<div ng-show="showreward">
<label class="hide"><input type="checkbox"> Apply Credit</label>
<div class="creditwelcome">
<h5>Congratulations you have earned {{user[0].rewardPoints}} credits </h5>
<h5>please apply the credits during the checkout</h5>
</div>
</div>
<div class="dcouponone">
<button type="button" class="btn dapplybtn" data-toggle="modal" data-target="#dcouponmodal">Apply Coupons</button>
</div><!-- Coupon click -->
<div class="dcouponapplied" ng-if="p.discountName != '0'">
<h3>{{p.discountName}}<span class="dcname">  Coupon Applied*</span></h3>
</div><!-- Coupon Applied -->
<div class="row dselecteddata"  ng-show="p.available  !== '0' && showloaderpaynow">
<div class="col-xs-8"><span class="dselectedroomdata" >{{p.accommodationName}} X {{getRoomCount()}} {{p.diffDays}} Nights {{getAdults() + getChild()}} Guest  </span></div>
<div class="col-xs-4"><span class="dselectedstrikeprice"><i class="fa fa-rupee"></i>{{getBaseActualTotal()}}</span><span class="dselectedtotalprice"> <i class="fa fa-rupee"></i>{{getTotal()}}</span><span class="dtaxnote">Tax Incl</span></div>  <!-- getTotal() -->
<div class="dconfirmbooking" class="dnav">
<a   href="#bookingdetails"><button class="btn dconfirm" ng-click="quickBook($index,rm.value,ad.value,ch.value);" ng-show="getTotal() !== 0 " >CONTINUE TO PAY</button></a>
</div><!-- Booking button click -->
</div><!-- selected data -->
<div class="row" ng-show="showloaderbooknow">
<div class="detailloadbox"></div>
</div>
<!-- soldout notes -->
<div class="row detailsoldnotes" ng-show="showloaderpaynow">
<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
<button class="btn soldoutbtn" ng-show ="p.available === '0'"> SOLD OUT </button>
</div>
<div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
<p ng-show ="p.available === '0'" class="soldoutnote">We are unable to serve on these dates,Please select another date or hotel</p>

</div>
</div>
<!-- sold out section -->
</div>
</div>
</div>
<!-- Property Details  -->
<!-- Property Amenities begins -->
<div class="row">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 dotherhoteldetails">
<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 damenityboxline">
<h4>Amenities</h4>
<div class="damenitybox" id="text"  ng-repeat="p in properties ">
<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" ng-repeat="amm in p.amenities | orderBy:'amenityName'"><img ng-src="{{amm.icon}}" alt="best emenituties" class="damenityicon" ><span class="damenityname">{{amm.amenityName}}</span></div>
</div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
<a id="more" class="dmorebtn">More <i class="fa fa-caret-down"></i></a>
<a id="less" class="dmorebtn" style="display:none">Less <i class="fa fa-caret-down"></i></a>
</div>
</div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 damenityboxline">
<div class="dnearby" id="textone" ng-repeat="pl in propLandmark">
<h4>Near By</h4>

<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
<h5>LANDMARKS</h5>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
<h5 ng-hide="pl.kilometers1 == 0"><span class="landname">{{pl.propertyLandmark1}}</span><span class="landkms">{{pl.kilometers1  | number }}kms</span></h5>
<h5 ng-hide="pl.kilometers2 == 0"><span class="landname">{{pl.propertyLandmark2}}</span><span class="landkms">{{pl.kilometers2  | number }}kms</span></h5>
<h5 ng-hide="pl.kilometers3 == 0"><span class="landname">{{pl.propertyLandmark3}}</span><span class="landkms">{{pl.kilometers3  | number }}kms</span></h5>
<h5 ng-hide="pl.kilometers4 == 0"><span class="landname">{{pl.propertyLandmark4}}</span><span class="landkms">{{pl.kilometers4  | number }}kms</span></h5>
<h5 ng-hide="pl.kilometers5 == 0"><span class="landname">{{pl.propertyLandmark5}}</span><span class="landkms">{{pl.kilometers5  | number }}kms</span></h5>
</div>
</div><!-- location -->
<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 "><!-- Attraction -->
<h5>ATTRACTIONS</h5>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
<h5 ng-hide="pl.kilometers6 == 0"><span class="landname">{{pl.propertyLandmark6}}</span><span class="landkms">{{pl.kilometers6  | number }}kms</span></h5>
<h5 ng-hide="pl.kilometers7 == 0"><span class="landname">{{pl.propertyLandmark7}}</span><span class="landkms">{{pl.kilometers7  | number }}kms</span></h5>
<h5 ng-hide="pl.kilometers8 == 0"><span class="landname">{{pl.propertyLandmark8}}</span><span class="landkms">{{pl.kilometers8  | number }}kms</span></h5>
<h5 ng-hide="pl.kilometers9 == 0"><span class="landname">{{pl.propertyLandmark9}}</span><span class="landkms">{{pl.kilometers9  | number }}kms</span></h5>
<h5 ng-hide="pl.kilometers10 == 0"><span class="landname">{{pl.propertyLandmark10}}</span><span class="landkms">{{pl.kilometers10  | number }}kms</span></h5>

</div>
</div>
</div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
<a id="moreone" class="dmorebtn">More <i class="fa fa-caret-down"></i></a>
<a id="lessone" class="dmorebtn" style="display:none">Less <i class="fa fa-caret-down"></i></a>
</div>
</div>
</div>

<!-- property Amenities ends -->
<!-- Property policies begins -->
<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 dpolicy ">
<h4>Policies</h4>
<span class="policyhead"></span>
                     <p><%= request.getAttribute("hotelPolicy") %> </p>
                     <p><%= request.getAttribute("cancellationPolicy") %></p>
                     <p><%= request.getAttribute("refundPolicy") %></p>
</div>
</div>
<!-- property policy endds -->
<!-- property map begins -->
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
<h4>Location</h4>
<h5>{{properties[0].address1}}, {{properties[0].address2}}, {{properties[0].city}} </h5>
<input type="hidden" id ="lat" name="lat" value="<s:property value="latitude" />">
<input type="hidden" id ="lng" name="lng" value="<s:property value="longitude" />" >
<a href="https://www.google.com/maps/search/?api=1&query=<s:property value="latitude"/>,<s:property value="longitude" />" target="_blank"><div id="map" style="height:250px;"></div></a>                      
</div>

<!-- property map ends -->
<!-- property reviews begins  -->
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 dtotalreviews" ng-repeat="p in properties">
<h4>Reviews</h4>
<!-- google reviews begins -->
<div class="col-lg-12 dgbox">
 <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" ng-repeat="review in p.reviews">
 <h4 ><img src="contents/images/detail/google.png" alt="google reviews of ulo hotels" width='42' height="42"><span class="dgcount">{{review.reviewCount}} Reviews</span></h4>
 <a ng-href="{{review.googleReviewUrl}}" target="_blank" class="dgreviewbtn"><h4>Write Review <i class="fa fa-edit"></i></h4></a>
 </div>
  <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12" >
  <div class="dgooglereviewbox" >
  <div ng-repeat="gr in p.guestReviews  | filter: { reviewerName: 'Google' } track by $index">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
<img class="img-circle guserimage" src="contents/images/reviews/googleguest.png">
<span class="gusername">{{gr.reviewerGuestName}}</span>
 <span star-rating rating-value="gr.reviewerCount" max="5" ></span>
</div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
<span class="greviewext">{{gr.reviewerDescription}}</span>
</div>
</div>
</div>
  </div>
  </div>
<!-- google reviews ends -->
<!-- trip advisior reviews begins -->
<div class="col-lg-12 dtbox">
 <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12" ng-repeat="review in p.reviews">
 <h4 ><img src="contents/images/reviews/trs.png" alt="google reviews of ulo hotels" width='42' height="42"><span class="dtcount">{{review.tripadvisorReviewCount}} Reviews</span></h4>
 <a ng-href="{{review.tripadvisorReviewUrl}}" target="_blank" class="dtreviewbtn"><h4>Write Review <i class="fa fa-edit"></i></h4></a>
 </div>
  <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
  <div class="dtripreviewbox">
  <div ng-repeat="gr in p.guestReviews  | filter: { reviewerName: 'tripadvisor' } track by $index">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
<img class="img-circle tuserimage" src="contents/images/reviews/tripguest.png">
<span class="tusername">{{gr.reviewerGuestName}}</span>
 <span star-rating2 rating-value="gr.reviewerCount" max="5" ></span>
</div>
</div>
</div>
  </div>
  </div>
<!--  trip advisior reviews ends -->

</div>
<!-- property reviews ends -->
</div>
</div>
<!-- Desktop coupon code begins -->

<!-- Modal -->
<div id="dcouponmodal" class="modal fade" role="dialog">
  <div class="modal-dialog dcouponpopup">

    <!-- Modal content-->
    <div class="modal-content ">
      <div class="modal-header">
       
        <h4 class="modal-title text-center">Offer Zone</h4>
        <img src="contents/images/detail/offer1.png" alt="ulo promocode">
      </div>
      <div class="modal-body">
      <form name="dcoupon">
      <div class="row">
            <div class="col-lg-9 col-md-9 col-sm-10 col-xs-10">
            <input type="hidden" class="form-control couponform" id="code_signup" placeholder=""  name="code_signup" value="" required />
            <input type="hidden" class="form-control couponform" id="code_percentage" placeholder=""  name="code_percentage" value="" required />
            <input type="hidden" class="form-control couponform" id="code_id" placeholder=""  name="code_id" value="" required />
            <input type="text" class="form-control couponform" id="code_input" value="" ng-required="true">
      </div>
      <div class="col-lg-3 col-md-3	col-sm-2 col-xs-2" >
       <button type="submit" ng-click="applyDiscount()" class="couponbtn"><span ng-show="dapply == 'APPLYING'"></span>{{dapply}}</button>
      </div>
    <span class="dpick">Pick A Deal</span>
       <span class="dpickspan">Click To Apply</span>
       <div id="couponerrortwo"></div>
             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" ng-repeat="d in discounts track by $index">
         <div class="dcouponbox"><span class="dcouponcode" ng-click="selectDiscount($index)">{{d.discountName}}</span><span class="dcouponoffer">Flat {{d.discountPercentage}} Offer </span></div>
       </div>
         </form>
      </div>
         

      </div>

    </div>

  </div>
</div>
<!-- Desktop Coupon code ends -->
</section>

       <input type="hidden" value ="<%=request.getAttribute("urlPropertyId")==null?session.getAttribute("uloPropertyId"):request.getAttribute("urlPropertyId") %>" id="propertyId" name="propertyId" >
       <input type="hidden" id ="urlPropertyId"  name="urlPropertyId"  value="<%= request.getAttribute("urlPropertyId") %>"  >

<!-- ulohotel detail page ends for desktop -->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
 <script type="text/javascript"  src="contents/js/ngprogress.js"></script>
 <script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script> 
 <script type="text/javascript" src="https://code.jquery.com/jquery-2.2.4.min.js"></script> 
 <script src="https://checkout.razorpay.com/v1/checkout.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.5/js/bootstrap-select.min.js"></script> 
 <script type="text/javascript" src="https://apis.google.com/js/api.js" ng-init="handleGoogleApiLibrary()" ></script>
 <link rel="stylesheet" href="contents/css/jquery-ui.css">
 <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script type="text/javascript" >
var app = angular.module('myApp', ['ngProgress','ui.bootstrap']);
app.controller('customersCtrl', function($scope, $http, $timeout, ngProgressFactory, $location, $filter,$compile) {
	$scope.progressbar = ngProgressFactory.createInstance();
    $scope.progressbar.start();
    $scope.showloaderbooknow = true;
    
	  $scope.dofferlogin = true;

	  /* login and signup start here */
		
	  $scope.handleGoogleApiLibrary = function() {
          gapi.load('client:auth2', {
              callback: function() {
                  gapi.client.init({
                      apiKey: 'AIzaSyAuJwG_DuX3eEXTgsoEY9A8LRvRVfeilFs',
                      clientId: '106759720549-tfe9i1b16ovvpqn8aho6hooiaftt2h4n.apps.googleusercontent.com',
                      scope: 'https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/plus.me'
                  }).then(
                      // On success
                      function(success) {
                          $("#login-button").removeAttr('disabled');
                      },
                      // On error
                      function(error) {
                      }
                  );
              },
              onerror: function() {
                  // Failed to load libraries
              }
          });
      };
      $scope.googleLogin = function() {
    	  
    	  $scope.progressbar.start();
    	  $('#myModal').modal('hide');
	      //$('#mymobileModal').modal('hide');
          gapi.auth2.getAuthInstance().signIn().then(
              function(success) {
                  gapi.client.request({
                      path: 'https://www.googleapis.com/plus/v1/people/me'
                  }).then(
                      function(success) {
                          var user_info = JSON.parse(success.body);
                          var username = user_info.displayName;
                          var email = user_info.emails[0].value;
                          var phone = null;
                  
                          var fdata = "username=" + user_info.displayName +
                              "&emailid=" + user_info.emails[0].value +
                              "&userid=" + user_info.id;
                          $http({
                              method: 'POST',
                              data: fdata,
                              headers: {
                                  'Content-Type': 'application/x-www-form-urlencoded'
                              },
                              url: 'sociallogin'
                          }).then(function successCallback(response) {
                        	  
                        	  $scope.sociallogin = response.data;
                        
                         	   
                        	  $scope.showDprofile = true;
                              $scope.showDlogin = false;
                              $scope.dofferlogin = false;
                              $scope.showreward = false;
                              $scope.username = $scope.sociallogin.data[0].uloUserName;
                              $('#myModal').modal('hide');
                   	          //$('#mymobileModal').modal('toggle');
                              $scope.progressbar.complete();
                          }, function errorCallback(response) {

                          });
                      },
                      // On error
                      function(error) {
                          $("#login-button").removeAttr('disabled');
                          alert('Error : Failed to get user user information');
                      }
                  );
              },
              // On error
              function(error) {
                  $("#login-button").removeAttr('disabled');
                  $('#myModal').modal('toggle');
       	          //$('#mymobileModal').modal('toggle');
                  $scope.progressbar.complete();
              }
          );
      };
      $scope.facebookLogin = function() {
          $scope.authUser();
      };
      $scope.facebookLoginMobile = function() {
          $scope.authUser();
      };
      $scope.authUser = function() {
          FB.login($scope.checkLoginStatus, {
              scope: 'email, user_likes, user_birthday, user_photos'
          });
      };
      $scope.authUserMobile = function() {
          FB.login($scope.checkLoginStatusMobile, {
              scope: 'email, user_likes, user_birthday, user_photos'
          });
      };
      
      
      $scope.checkLoginStatus = function(response) {
    	  $scope.progressbar.start();
    	  $('#myModal').modal('hide');
	      //$('#mymobileModal').modal('hide');
          if (response && response.status == 'connected') {
              console.log('User is authorized');
              FB.api('/me?fields=name,email', function(response) {
                  console.log(response);
                  console.log('Good to see you, ' + response.email + '.');
                  var username = response.name;
                  var email = response.email;
                  var phone = null;
            
                  var fdata = "username=" + response.name +
                      "&emailid=" + response.email +
                      "&userid=" + response.id;
                  $http({
                      method: 'POST',
                      data: fdata,
                      headers: {
                          'Content-Type': 'application/x-www-form-urlencoded'
                      },
                      url: 'sociallogin'
                  }).then(function successCallback(response) {

                	  $scope.facebooklogin = response.data;
                	  $scope.showDprofile = true;
                      $scope.showDlogin = false;
                      $scope.dofferlogin = false;
                      $scope.showreward = false;
                      $scope.username = $scope.facebooklogin.data[0].uloUserName;
                      $scope.progressbar.complete();
                  }, function errorCallback(response) {});
              })
          } else if (response.status === 'not_authorized') {
              // the user is logged in to Facebook, but has not authenticated your app
              console.log('User is not authorized');
              $('#myModal').modal('toggle');
    	      //$('#mymobileModal').modal('hide');
          } else {
              // the user isn't logged in to Facebook.
              console.log('User is not logged into Facebook');
              $('#myModal').modal('toggle');
    	      //$('#mymobileModal').modal('hide');
              $scope.progressbar.complete();

          }
      };
      $scope.checkLoginStatusMobile = function(response) {
    	  $scope.progressbar.start();
    	  //$('#myModal').modal('hide');
	      $('#mymobileModal').modal('hide');
          if (response && response.status == 'connected') {
              console.log('User is authorized');
              FB.api('/me?fields=name,email', function(response) {
                  console.log(response);
                  console.log('Good to see you, ' + response.email + '.');
                  var username = response.name;
                  var email = response.email;
                  var phone = null;
            
                  var fdata = "username=" + response.name +
                      "&emailid=" + response.email +
                      "&userid=" + response.id;
                  $http({
                      method: 'POST',
                      data: fdata,
                      headers: {
                          'Content-Type': 'application/x-www-form-urlencoded'
                      },
                      url: 'sociallogin'
                  }).then(function successCallback(response) {

                	  $scope.facebooklogin = response.data;
                	  $scope.showDprofile = true;
                      $scope.showDlogin = false;
                      $scope.dofferlogin = false;
                      $scope.showreward = false;
                      $scope.username = $scope.facebooklogin.data[0].uloUserName;
                      $scope.progressbar.complete();
                  }, function errorCallback(response) {});
              })
          } else if (response.status === 'not_authorized') {
              // the user is logged in to Facebook, but has not authenticated your app
              console.log('User is not authorized');
              //$('#myModal').modal('toggle');
    	      $('#mymobileModal').modal('hide');
          } else {
              // the user isn't logged in to Facebook.
              console.log('User is not logged into Facebook');
              //$('#myModal').modal('toggle');
    	      $('#mymobileModal').modal('hide');
              $scope.progressbar.complete();

          }
      };
 $scope.googleLoginMobile = function() {
    	  
    	  $scope.progressbar.start();
    	  //$('#myModal').modal('hide');
	      $('#mymobileModal').modal('hide');
          gapi.auth2.getAuthInstance().signIn().then(
              function(success) {
                  gapi.client.request({
                      path: 'https://www.googleapis.com/plus/v1/people/me'
                  }).then(
                      function(success) {
                          var user_info = JSON.parse(success.body);
                          var username = user_info.displayName;
                          var email = user_info.emails[0].value;
                          var phone = null;
                  
                          var fdata = "username=" + user_info.displayName +
                              "&emailid=" + user_info.emails[0].value +
                              "&userid=" + user_info.id;
                          $http({
                              method: 'POST',
                              data: fdata,
                              headers: {
                                  'Content-Type': 'application/x-www-form-urlencoded'
                              },
                              url: 'sociallogin'
                          }).then(function successCallback(response) {
                        	  
                        	  $scope.sociallogin = response.data;
                        
                         	   
                        	  $scope.showDprofile = true;
                              $scope.showDlogin = false;
                              $scope.dofferlogin = false;
                              $scope.showreward = false;
                              $scope.username = $scope.sociallogin.data[0].uloUserName;
                              //$('#myModal').modal('hide');
                   	          //$('#mymobileModal').modal('toggle');
                              $scope.progressbar.complete();
                          }, function errorCallback(response) {

                          });
                      },
                      // On error
                      function(error) {
                          $("#login-button").removeAttr('disabled');
                          alert('Error : Failed to get user user information');
                          $('#mymobileModal').modal('toggle');
                          $scope.progressbar.complete();
                      }
                  );
              },
              // On error
              function(error) {
                  $("#login-button").removeAttr('disabled');
                  //$('#myModal').modal('toggle');
       	          $('#mymobileModal').modal('toggle');
                  $scope.progressbar.complete();
              }
          );
      };
	  $('.userotp').hide();

	  $('.mobileuserotp').hide();
	  $scope.sendMobileUserOtp = function(){
		  
	  }
	  
	  $scope.showDprofile = false;
		$scope.showDlogin = true;
		$scope.showMprofile = false;
		$scope.showMlogin = true;
		$scope.showreward = false;
	    $scope.dsignin = "Submit";
	   
	    var firstname = $('#firstName').val();
		$scope.username = firstname;
    $scope.signup = function() {
  	  

      $scope.dsignin = "Please Wait...";
     
   	  var rewardDetailId = $('#rewardDetailId').val();
   	  if(rewardDetailId != null){
   		  var fdata = "userName=" + $('#userName').val() +
             "&phone=" + $('#mobilePhone').val() +
             "&emailId=" + $('#emailId').val() +
             "&roleId=" + $('#roleId').val() +
             "&accessRightsId=" + $('#accessRightsId').val() +
             //"&password=" + $('#signpassword').val() +
             "&rewardDetailId=" + rewardDetailId;
   	  }
   	  else{ 
         var fdata = "userName=" + $('#userName').val() +
             "&phone=" + $('#mobilePhone').val() +
             "&emailId=" + $('#emailId').val() +
             "&roleId=" + $('#roleId').val() +
             "&accessRightsId=" + $('#accessRightsId').val();
            // "&password=" + $('#signpassword').val();
   	  }
   	 
         $http({
             method: 'POST',
             data: fdata,
             headers: {
                 'Content-Type': 'application/x-www-form-urlencoded'
             },
             url: 'ulosignup'
         }).then(function successCallback(response) {
        	 $scope.signup = response.data;
        	 
        	
      	    var alertmsg = ' <div class="alert alert-success alert-dismissible" style="color:red;text-align:center;">Login Successfull.</div>';
              $('#signuperrors').html(alertmsg);
              $(function() {
                  setTimeout(function() {
                      $("#signuperrors").hide('blind', {}, 100)
                  }, 2000);
              });
            //  $('#loginModal').modal('toggle');
              //location.reload();
             $('#myModal').modal('toggle');

             $scope.dsignin = "Submit";
             $scope.showDprofile = true;
             $scope.showDlogin = false;
             $scope.dofferlogin = false;
             $scope.showreward = true;
             $scope.UserIds = $scope.signup.data[0].userId;
             $scope.getUser();
      	  
        
         }, function errorCallback(response) {
      
        	  $scope.dsignin = "Submit";
        		 $("#signuperror").hide();
                 var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:red;text-align:center;">Mobile Number / Email already Already exist.</div>';
                 
               $('#signuperror').html(alertmsg);
		            $("#signuperror").fadeTo(2000, 500).slideUp(500, function(){
		                $("#signuperror").slideUp(500);
		            });
         });
     };
     $scope.msignin = "Submit";
     $scope.mbsignup = function() {
  	   $scope.msignin = "Please Wait";
    	  var rewardDetailId = $('#mbRewardDetailId').val();
    	  if(rewardDetailId != null){
    		  var fdata = "userName=" + $('#mbUserName').val() +
              "&phone=" + $('#mbMobilePhone').val() +
              "&emailId=" + $('#mbEmailId').val() +
              "&roleId=" + $('#mbRoleId').val() +
              "&accessRightsId=" + $('#mbAccessRightsId').val() +
              //"&password=" + $('#signpassword').val() +
              "&rewardDetailId=" + rewardDetailId;
    	  }
    	  else{ 
          var fdata = "userName=" + $('#mbUserName').val() +
              "&phone=" + $('#mbMobilePhone').val() +
              "&emailId=" + $('#mbEmailId').val() +
              "&roleId=" + $('#mbRoleId').val() +
              "&accessRightsId=" + $('#mbAccessRightsId').val();
             // "&password=" + $('#signpassword').val();
    	  }
          $http({
              method: 'POST',
              data: fdata,
              headers: {
                  'Content-Type': 'application/x-www-form-urlencoded'
              },
              url: 'ulosignup'
          }).then(function successCallback(response) {
        	  $scope.mbsignup = response.data;
          	 $('#mymobileModal').modal('toggle');

               
               $scope.showMprofile = true;
               $scope.showMlogin = false;
               $scope.msignin = "Submit";
               $scope.showreward = true;
               $scope.UserIds = $scope.mbsignup.data[0].userId();
               $scope.getUser();
       
          }, function errorCallback(response) {
        	  $scope.msignin = "Submit";
    
         	 $("#msignuperror").hide();
              var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:red;text-align:center;">Mobile Number / Email already Already exist.</div>';
              
            $('#msignuperror').html(alertmsg);
		            $("#msignuperror").fadeTo(2000, 500).slideUp(500, function(){
		                $("#msignuperror").slideUp(500);
          });
          });
      };  
		
			
     
     $scope.sendUloLoginOtp = function() {
         var fdata = "username="+$('#loginUserName').val();

         $http({
             method: 'POST',
             data: fdata,
             headers: {
                 'Content-Type': 'application/x-www-form-urlencoded'
             },
             url: 'send-ulologin-otp'
         }).then(function successCallback(response) {
              $scope.sendLoginOtp = response.data;
      		$scope.ulouserId = $scope.sendLoginOtp.data[0].uloUserId;
      		$scope.username =  $scope.sendLoginOtp.data[0].userName;
              var alertmsg = ' <div class="alert alert-success alert-dismissible" style="color:red;text-align:center;">OTP Send Successfull.</div>';
              $('#otpsendmsg').html(alertmsg);
              $(function() {
                  setTimeout(function() {
                      $("#otpsendmsg").hide('blind', {}, 100)
                  }, 2000);
                  $('.userotp').show();
          		  $('.userinfo').hide(); 
              });
           },function errorCallback(response) {
           	 $("#loginerror").hide();
             var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:red;text-align:center;">This email/mobile number does not exist.</div>';
             
           $('#loginerror').html(alertmsg);
		            $("#loginerror").fadeTo(2000, 500).slideUp(500, function(){
		                $("#loginerror").slideUp(500);
         });
         });
     };
     
     $scope.mbSendUloLoginOtp = function() {
         var fdata = "username=" + $('#mbLoginUserName').val();

         $http({
             method: 'POST',
             data: fdata,
             headers: {
                 'Content-Type': 'application/x-www-form-urlencoded'
             },
             url: 'send-ulologin-otp'
         }).then(function successCallback(response) {
              $scope.mbSendLoginOtp = response.data;
              $scope.mbulouserId = $scope.mbSendLoginOtp.data[0].uloUserId;
      		$scope.mbusername =  $scope.mbSendLoginOtp.data[0].userName;
              $('.mobileuserotp').show();
    		  $('.mobileuserinfo').hide();
       
         }, function errorCallback(response) {
         	 $("#loginerrormb").hide();
             var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:red;text-align:center;">This email/mobile number does not exist.</div>';
             
           $('#loginerrormb').html(alertmsg);
		            $("#loginerrormb").fadeTo(2000, 500).slideUp(500, function(){
		                $("#loginerrormb").slideUp(500);
         });
      });
     };
     
     $scope.verifyUloLoginOtp = function() {
         var fdata = "userId=" + $('#uloLoginId').val()+  
                      "&loginOtp=" + $('#loginOtp').val();
         $http({
             method: 'POST',
             data: fdata,
             headers: {
                 'Content-Type': 'application/x-www-form-urlencoded'
             },
             url: 'verify-ulologin-otp'
         }).then(function successCallback(response) {
              $scope.verifyLoginOtp = response.data;
              $scope.showDprofile = true;
              $scope.showDlogin = false;
              $scope.dofferlogin = false;
              $scope.UserIds = $scope.verifyLoginOtp.data[0].userId;
              $scope.getUser();
              $('#loginModal').modal('toggle');
              /// location.reload();
             
         }, function errorCallback(response) {
      	 $("#loginerror").hide();
         var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:red;text-align:center;">Invalid username or password.</div>';
         
       $('#loginerror').html(alertmsg);
	            $("#loginerror").fadeTo(2000, 500).slideUp(500, function(){
	                $("#loginerror").slideUp(500);
         });
	            
      });
     };
     
     $scope.mbVerifyUloLoginOtp = function() {
         var fdata = "userId=" + $('#mbUloLoginId').val()+  
                      "&loginOtp=" + $('#mbLoginOtp').val();
         $http({
             method: 'POST',
             data: fdata,
             headers: {
                 'Content-Type': 'application/x-www-form-urlencoded'
             },
             url: 'verify-ulologin-otp'
         }).then(function successCallback(response) {
              $scope.mbVerifyLoginOtp = response.data;
              $scope.UserIds = $scope.mbVerifyLoginOtp.data[0].userId;
                $scope.getUser();
                $('#loginmobileModal').modal('toggle');
               //location.reload();
             
         }, function errorCallback(response) {
          $("#loginerror").hide();
         var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:red;text-align:center;">Invalid username or password.</div>';
         
       $('#loginerror').html(alertmsg);
	            $("#loginerror").fadeTo(2000, 500).slideUp(500, function(){
	                $("#loginerror").slideUp(500);
         });
          
      });
     };
     
     $scope.resendUloLoginOtp = function() {
         var fdata = "userId=" + $('#uloLoginId').val()+  
                      "&username=" + $('#uloLoginName').val();   /* $('#uloLoginName').val() */
       
         $http({
             method: 'POST',
             data: fdata,
             headers: {
                 'Content-Type': 'application/x-www-form-urlencoded'
             },
             url: 'resend-ulologin-otp'
         }).then(function successCallback(response) {
              $scope.resendLoginOtp = response.data;
             
         });
     };
     
     $scope.mbResendUloLoginOtp = function() {
         var fdata = "userId=" + $('#mbUloLoginId').val()+  
                      "&username=" + $('#mbUloLoginName').val();   /* $('#uloLoginName').val() */
         $http({
             method: 'POST',
             data: fdata,
             headers: {
                 'Content-Type': 'application/x-www-form-urlencoded'
             },
             url: 'resend-ulologin-otp'
         }).then(function successCallback(response) {
              $scope.mbResendLoginOtp = response.data;
             
         });
     };
     
     /* login and signup end here */       
    
	
		function goBack() {
		    window.history.back();
		}
		
	$(document).ready(function(){
		 $('#moreone').click(function(){
		        $('#textone').removeClass("dnearby");
		        $("#lessone").show();
		        $(this).hide();
		    });
		    
		    
		    $('#lessone').click(function(){
		        $('#textone').addClass("dnearby");
		        $(this).hide();
		        $("#moreone").show();
		    }); 
	    $('#more').click(function(){
	        $('#text').removeClass("damenitybox");
	        $("#less").show();
	        $(this).hide();
	    });
	    
	    
	    $('#mless').click(function(){
	        $('#mtext').addClass("mobileicondiv");
	        $(this).hide();
	        $("#mmore").show();
	    });
	    $('#mmore').click(function(){
	        $('#mtext').removeClass("mobileicondiv");
	        $("#mless").show();
	        $(this).hide();
	    });
	    
	    
	    $('#less').click(function(){
	    	
	        $('#text').addClass("damenitybox");
	        $('#mtext').addClass("damenitybox");
	        $(this).hide();
	        $("#more").show();
	    });
	})
	
	
	  var propertyId = $('#propertyId').val();
	              var checkIn = $('#datepicker').val();
	              var checkOut = $('#datepicker1').val();
	          	var urlPropertyId=$('#urlPropertyId').val();
	          	
	          	$scope.StartDate=document.getElementById("alternate").value;
		 	    $scope.EndDate=document.getElementById("alternate1").value;
	          	
	            $scope.getProperties = function() {
	            	 $scope.showloaderbooknow = true;
	            	 $scope.showloaderpaynow = false;
	            	var checkedPropertyId;
	            	
	            	if(propertyId!=null && propertyId!=""){
	            		checkedPropertyId=propertyId;
	            	}else{
	            		checkedPropertyId=urlPropertyId;
	            	}

	            	var fdata = "&startDate=" + $('#alternate').val() + 
	                    "&endDate=" + $('#alternate1').val() + 
	                    "&propertyId=" + checkedPropertyId
	                    $http({
	                        method: 'POST',
	                        data: fdata,
	                        headers: {
	                            'Content-Type': 'application/x-www-form-urlencoded'
	                        },
	                        url: "get-new-ulo-front-availability"
	                    }).success(function(response) {
	                    	$scope.properties = response.data;
	                        $scope.progressbar.complete();
	                        $scope.showloaderbooknow = false;
	                        $scope.showloaderpaynow = true;
	                        //console.log(JSON.stringify($scope.properties))
	                    });
	            //	}
	            	
	            };
	            
	            $scope.getMbProperties = function() {
	            	 $scope.showloaderbooknow = true;
	            	 $scope.showloaderpaynow = false;
	            	var checkedPropertyId;
	            	
	            	if(propertyId!=null && propertyId!=""){
	            		checkedPropertyId=propertyId;
	            	}else{
	            		checkedPropertyId=urlPropertyId;
	            	}

	            	var fdata = "&startDate=" + $('#malternate').val() + 
	                    "&endDate=" + $('#malternate1').val() + 
	                    "&propertyId=" + checkedPropertyId
	                    $http({
	                        method: 'POST',
	                        data: fdata,
	                        headers: {
	                            'Content-Type': 'application/x-www-form-urlencoded'
	                        },
	                        url: "get-new-front-availability"
	                    }).success(function(response) {
	                    	$scope.properties = response.data;
	                        $scope.progressbar.complete();
	                        $scope.showloaderbooknow = false;
	                        $scope.showloaderpaynow = true;
	                        //console.log(JSON.stringify($scope.properties))
	                    });
	            //	}
	            	
	            };
	            
	            $scope.range = function(min, max, step) {
	                step = step || 1;
	                var input = [];
	                for (var i = min; i <= max; i += step) input.push(i);
	                return input;
	            };
	            
	            $scope.totalAdultCheck = function(indx, noOfAdults, available) {
	        
	                if (available == 0) {
	                    console.log("Disable");
	                } else {

	                    var noOfAdult = parseInt(available) * parseInt(noOfAdults);
	                    
	                    $scope.properties[indx].maxAdult = noOfAdult;
	                    //$scope.accommodations[indx].maxOccupancy = noOfAdult;
	                    return false;
	                }
	            };
	            $scope.totalAdultCheckmb = function(indx, noOfAdults, available) {
	                if (available == 0) {
	                    console.log("Disable");
	                } else {

	                    var noOfAdult = parseInt(available) * parseInt(noOfAdults);
	                    
	                    $scope.properties[indx].maxAdult = noOfAdult;
	                    //$scope.accommodations[indx].maxOccupancy = noOfAdult;
	                    return false;
	                }
	            };
	            
	            $scope.minimumOccupancyCheck = function(indx, available, noOfAdults) {

	               /*  if (available == 0) {

	                    console.log("Disable");
	                } else { */
	                    var noOfChild = parseInt(available) - parseInt(noOfAdults);
	                    $scope.properties[indx].maxChild = noOfChild;
	                    return false;
	               // }
	            };
	            
	            $scope.updateRoomAvailable = function(indx, room, adult, child, available, baseAmount, minOccupancy, maxOccupancy, extraAdult, extraChild, arrivalDate, departureDate, promotionId,totalAmount,discountBaseAmount,baseActualAmount,tax,taxPercent) {
	            	var adult = parseInt(adult);
	                var child = parseInt(child);
	                var date1 = new Date(arrivalDate);
	                var date2 = new Date(departureDate);
	                var amount = parseFloat(baseAmount);
	                var timeDiff = Math.abs(date2.getTime() - date1.getTime());
	                var room = parseInt(room);
	                var tax = parseInt(tax);
	                var minOccu = parseInt(minOccupancy * room);
	                var maxOccu = parseInt(maxOccupancy * room);
	                var diffDays = parseInt(Math.round(timeDiff / (1000 * 3600 * 24)));
	                var totalOccu = parseInt(adult + child);
	                var total = (room * amount);
	                var roomAmount = parseFloat(total);
	                var adltChd = parseInt(adult + child);
	                var adltChdIft = parseInt(adult + child);
	                var totalActualAmount = parseInt(baseActualAmount);
	                $scope.properties[indx].room = room;
	                $scope.properties[indx].baseActualAmount = totalActualAmount;
	                $scope.properties[indx].adultsCount = adult;
	                $scope.properties[indx].childCount = child;
	                $scope.properties[indx].rooms = room;
	                $scope.properties[indx].maxChild = 0;
	                
	             
	                var roomTax = $scope.properties[indx].roomTax;
	                $scope.properties[indx].totalAmount = roomAmount;
	                $scope.properties[indx].discountBaseAmount = roomAmount;
	              
	                $scope.properties[indx].tax = parseInt(roomTax * room);
                    $scope.properties[indx].taxPercentage = taxPercent;
	               if (totalOccu > maxOccu) {
	                    alert("Runnig out of Maximum Occupancy");
	                    $('#adults').val(minOccu);
	                    $('#child').val(0);
	                    
	                    $scope.properties[indx].adultsCount = minOccu;
	                    $scope.properties[indx].childCount = 0;
	                  
	                } else {
	                	//if(adltChd > minOccu){
	                    if (adltChd > minOccu) {
	                    	
	                        adultsLeft = (adltChd)-(minOccu+childLeft);
	                        var total = (diffDays * adultsLeft * extraAdult + roomAmount)+(childLeft*extraChild);
	                        $scope.properties[indx].totalAmount = total;
	                        var tax = (total*taxPercent/100);
	                        $scope.properties[indx].tax = tax; 
	                        
	                        
	                    } /* else if(child > minOccu){
	                    	var childLeft = (child - minOccu);
	                    	var total = (diffDays * childLeft * extraChild + roomAmount);
	                    	$scope.properties[indx].totalAmount = total;
	                    	var tax = (total*taxPercent/100);
	                        $scope.properties[indx].tax = tax;
	                    	
	                    }   else if (adltChd > minOccu) {
	                    	
	                        var childLeft = (adltChd - minOccu);
	                        var total = (diffDays * childLeft * extraChild + roomAmount);
	                        $scope.properties[indx].totalAmount = total;
	                        var tax = (total*taxPercent/100);
	                        $scope.properties[indx].tax = tax;
	                    }  */
	                    
	                    else if (adltChd <= minOccu) {
	                    	
	                    	  childLeft = 0;	
		                      adultsLeft = 0; 
		                        
		                    }

	                	
	                }
	            };	 
	            
	            $scope.updateAvailable = function(indx, room, adult, child, available, baseAmount, minOccupancy, maxOccupancy, extraAdult, extraChild, arrivalDate, departureDate, promotionId,totalAmount,discountBaseAmount,baseActualAmount,tax,taxPercent) {
	            	//updateAvailable($index,rm.value,ad.value,ch.value,p.available,p.baseAmount,p.minOccupancy,p.maxOccupancy,p.extraAdultAmount,p.extraChildAmount,p.arrivalDate,p.departureDate,p.promotionId,p.totalAmount,p.discountBaseAmount,p.baseActualAmount,p.tax,p.taxPercentage)
	            	var adult = parseInt(adult);
	                var child = parseInt(child);
	                var date1 = new Date(arrivalDate);
	                var date2 = new Date(departureDate);
	                var amount = parseFloat(baseAmount);
	                var timeDiff = Math.abs(date2.getTime() - date1.getTime());
	                var room = parseInt(room);
	                var tax = parseInt(tax);
	                var minOccu = parseInt(minOccupancy * room);
	                var maxOccu = parseInt(maxOccupancy * room);
	                var diffDays = parseInt(Math.round(timeDiff / (1000 * 3600 * 24)));
	                var totalOccu = parseInt(adult + child);
	                var total = (room * amount);
	                var roomAmount = parseFloat(total);
	                var adltChd = parseInt(adult + child);
	                var adltChdIft = parseInt(adult + child);
	                var totalActualAmount = parseInt(baseActualAmount);
	                $scope.properties[indx].room = room;
	                $scope.properties[indx].baseActualAmount = totalActualAmount;
	                $scope.properties[indx].adultsCount = adult;
	                $scope.properties[indx].childCount = child;
	                $scope.properties[indx].rooms = room;
	             
	                var roomTax = $scope.properties[indx].roomTax;
	                $scope.properties[indx].totalAmount = roomAmount;
	                $scope.properties[indx].discountBaseAmount = roomAmount;
	                $scope.properties[indx].tax = parseInt(roomTax * room);
                    $scope.properties[indx].taxPercentage = taxPercent;
                    var childLeft = 0;
	                var adultsLeft = 0;
	                
	                

	                if (totalOccu > maxOccu) {
	                    alert("Runnig out of Maximum Occupancy");
	                    $('#adults').val(minOccu);
	                    $('#child').val(0);
	                    
	                    $scope.properties[indx].adultsCount = minOccu;
	                    $scope.properties[indx].childCount = 0;
	                  
	                } else {
	                	//if(adltChd > minOccu){
	                    if (adltChd > minOccu) {
	                    	
	                        adultsLeft = (adltChd)-(minOccu+childLeft);
	                        var total = (diffDays * adultsLeft * extraAdult + roomAmount)+(childLeft*extraChild);
	                        $scope.properties[indx].totalAmount = total;
	                        $scope.properties[indx].discountBaseAmount = total;
	                        var tax = (total*taxPercent/100);
	                        $scope.properties[indx].tax = tax; 
	                        
	                        
	                    } /* else if(child > minOccu){
	                    	var childLeft = (child - minOccu);
	                    	var total = (diffDays * childLeft * extraChild + roomAmount);
	                    	$scope.properties[indx].totalAmount = total;
	                    	var tax = (total*taxPercent/100);
	                        $scope.properties[indx].tax = tax;
	                    	
	                    }   else if (adltChd > minOccu) {
	                    	
	                        var childLeft = (adltChd - minOccu);
	                        var total = (diffDays * childLeft * extraChild + roomAmount);
	                        $scope.properties[indx].totalAmount = total;
	                        var tax = (total*taxPercent/100);
	                        $scope.properties[indx].tax = tax;
	                    }  */
	                    
	                    else if (adltChd <= minOccu) {
	                    	
	                    	  childLeft = 0;	
		                      adultsLeft = 0; 
		                        
		                    }

	                	
	                }
	            };	 
	            
	            
	            $scope.updateAvailable9 = function(indx, room, adult, child, available, baseAmount, minOccupancy, maxOccupancy, extraAdult, extraChild, arrivalDate, departureDate, promotionId,totalAmount,discountBaseAmount,baseActualAmount,tax,taxPercent) {
	            	
	            	var adult = parseInt(adult);
	                var child = parseInt(child);
	                var date1 = new Date(arrivalDate);
	                var date2 = new Date(departureDate);
	                var amount = parseFloat(baseAmount);
	                var timeDiff = Math.abs(date2.getTime() - date1.getTime());
	                var room = parseInt(room);
	                var tax = parseInt(tax);
	                var minOccu = parseInt(minOccupancy * room);
	                var maxOccu = parseInt(maxOccupancy * room);
	                var diffDays = parseInt(Math.round(timeDiff / (1000 * 3600 * 24)));
	                var totalOccu = parseInt(adult + child);
	                var total = (room * amount);
	                var roomAmount = parseFloat(total);
	                var adltChd = parseInt(adult + child);
	                var adltChdIft = parseInt(adult + child);
	                var totalActualAmount = parseInt(baseActualAmount);
	                $scope.properties[indx].room = room;
	                $scope.properties[indx].baseActualAmount = totalActualAmount;
	                $scope.properties[indx].adultsCount = adult;
	                $scope.properties[indx].childCount = child;
	                $scope.properties[indx].rooms = room;
	             	$scope.properties[indx].maxAdult = maxOccu - child ; 
	                
	                var roomTax = $scope.properties[indx].roomTax;
	                
	                $scope.properties[indx].totalAmount = roomAmount;
	                $scope.properties[indx].discountBaseAmount = roomAmount;
	               $scope.properties[indx].tax = parseInt(roomTax * room);
                    $scope.properties[indx].taxPercentage = taxPercent;
	                var childLeft = 0;
	                var adultsLeft = 0;
	                
	                

	                if (totalOccu > maxOccu) {
	                    alert("Runnig out of Maximum Occupancy");
	                    $('#adults').val(minOccu);
	                    $('#child').val(0);
	                    
	                    $scope.properties[indx].adultsCount = minOccu;
	                    $scope.properties[indx].childCount = 0;
	                  
	                } else {
	                	
	                    if (adltChd > minOccu) {
	                    	
	                        childLeft = (adltChd)-(minOccu+adultsLeft);
	                        console.log($scope.properties[indx]);
	                       // var excessAmount = (diffDays * childLeft * roomAmount) - $scope.properties[indx].totalAmount;
	                    	var total = (diffDays * childLeft * extraChild + roomAmount)+(adultsLeft*extraAdult);
	                        
	                    	$scope.properties[indx].totalAmount = total;
	                    	$scope.properties[indx].discountBaseAmount = total;
	                        var tax = (total*taxPercent/100);
	                        $scope.properties[indx].tax = tax; 
	                        
	                        
	                    }
	                    
	                    else if (adltChd <= minOccu) {
	                    	
                    	  childLeft = 0;	
	                      adultsLeft = 0; 
	                        
	                    }
	                	
	                }
	            };	            
																	
	            
	            $scope.getAccommodationAvailability = function(indx, accommodationId, arrivalDate, departureDate) {
	            	 
	            	
	            	$scope.showloaderbooknow = true;
	            	 $scope.showloaderpaynow = false;
	            	var checkedPropertyId;
	            	if(propertyId!=null && propertyId!=""){
	            		checkedPropertyId=propertyId;
	            	}else{
	            		checkedPropertyId=urlPropertyId;
	            	}

	            	var startDate = $('#alternate').val();
					var endDate = $('#alternate1').val();
	            	
	            	
	                var fdata = "&strStartDate=" + startDate +
	                    "&strEndDate=" + endDate +
	                    "&propertyAccommodationId=" + accommodationId +
	                    "&propertyId=" + checkedPropertyId;
	                $http({
	                    method: 'POST',
	                    data: fdata,
	                    headers: {
	                        'Content-Type': 'application/x-www-form-urlencoded'
	                    },
	                    url: 'get-new-accommodation-availability'
	                }).success(function(response) {
	                	    $('#rooms').val(1);
	                	    $('#adults').val(1);
	                	    $('#child').val(0);
		                   
	                    $scope.propertyaccommodations = response.data;
	                    $scope.properties[indx].baseActualAmount = $scope.propertyaccommodations[0].baseActualAmount;
	                    $scope.properties[indx].accommodationName = $scope.propertyaccommodations[0].accommodationName;
	                    $scope.properties[indx].accommodationId = $scope.propertyaccommodations[0].accommodationId;
	                    $scope.properties[indx].available = $scope.propertyaccommodations[0].available;
	                    $scope.properties[indx].minOccupancy = $scope.propertyaccommodations[0].minOccupancy;
	                    $scope.properties[indx].maxOccupancy = $scope.propertyaccommodations[0].maxOccupancy;
	                    $scope.properties[indx].extraAdultAmount = $scope.propertyaccommodations[0].extraAdultAmount;
	                    $scope.properties[indx].extraChildAmount = $scope.propertyaccommodations[0].extraChildAmount;
// 	                    $scope.properties[indx].promotionName = $scope.propertyaccommodations[0].promotionName;
						$scope.properties[indx].promotionFirstName = $scope.propertyaccommodations[0].promotionFirstName;
// 	                    $scope.properties[indx].promotionId = $scope.propertyaccommodations[0].promotionId;
	                    //$scope.properties[indx].smartPricePercent = $scope.propertyaccommodations[0].smartPricePercent;
	                    //$scope.properties[indx].smartPriceId = $scope.propertyaccommodations[0].smartPriceId;
	                    $scope.properties[indx].baseAmount = $scope.propertyaccommodations[0].baseAmount;
	                    $scope.properties[indx].discountBaseAmount = $scope.properties[indx].baseAmount; 
	                    $scope.properties[indx].totalAmount = $scope.propertyaccommodations[0].totalAmount;
	                    //$scope.properties[indx].bookRooms = $scope.propertyaccommodations[0].bookRooms;
	                    //$scope.properties[indx].getRooms = $scope.propertyaccommodations[0].getRooms;
	                    //$scope.properties[indx].bookNights = $scope.propertyaccommodations[0].bookNights;
	                   // $scope.properties[indx].getNights = $scope.propertyaccommodations[0].getNights;
	                    //$scope.properties[indx].totalActualAmount = $scope.propertyaccommodations[0].totalActualAmount;
	                    $scope.properties[indx].maxAdult = $scope.propertyaccommodations[0].maxAdult;
	                    $scope.properties[indx].maxChild = $scope.propertyaccommodations[0].maxChild;
	                    $scope.properties[indx].roomTax = $scope.propertyaccommodations[0].roomTax;
	                    $scope.properties[indx].adultsCount = $scope.propertyaccommodations[0].adultsCount;
	                    $scope.properties[indx].chiildCount = $scope.propertyaccommodations[0].chiildCount;
	                    $scope.properties[indx].rooms = $scope.propertyaccommodations[0].rooms;
	                    $scope.properties[indx].tax = $scope.propertyaccommodations[0].tax;
	                    $scope.properties[indx].taxPercentage = $scope.propertyaccommodations[0].taxPercentage;
	                    $scope.showloaderbooknow = false;
		            	 $scope.showloaderpaynow = true;
	                });

	            };
	            
 			$scope.getMobileAccommodationAvailability = function(indx, accommodationId, arrivalDate, departureDate) {
	            	 
	            	
	            	$scope.showloaderbooknow = true;
	            	 $scope.showloaderpaynow = false;
	            	var checkedPropertyId;
	            	if(propertyId!=null && propertyId!=""){
	            		checkedPropertyId=propertyId;
	            	}else{
	            		checkedPropertyId=urlPropertyId;
	            	}
	            	
	            		var startDate = $('#malternate').val();
						var endDate = $('#malternate1').val();
	            	
	                var fdata = "&strStartDate=" + startDate +
	                    "&strEndDate=" + endDate +
	                    "&propertyAccommodationId=" + accommodationId +
	                    "&propertyId=" + checkedPropertyId;
	                $http({
	                    method: 'POST',
	                    data: fdata,
	                    headers: {
	                        'Content-Type': 'application/x-www-form-urlencoded'
	                    },
	                    url: 'get-new-accommodation-availability'
	                }).success(function(response) {
	                	    $('#rooms').val(1);
	                	    $('#adults').val(1);
	                	    $('#child').val(0);
		                   
	                    $scope.propertyaccommodations = response.data;
	                    $scope.properties[indx].baseActualAmount = $scope.propertyaccommodations[0].baseActualAmount;
	                    $scope.properties[indx].accommodationName = $scope.propertyaccommodations[0].accommodationName;
	                    $scope.properties[indx].accommodationId = $scope.propertyaccommodations[0].accommodationId;
	                    $scope.properties[indx].available = $scope.propertyaccommodations[0].available;
	                    $scope.properties[indx].minOccupancy = $scope.propertyaccommodations[0].minOccupancy;
	                    $scope.properties[indx].maxOccupancy = $scope.propertyaccommodations[0].maxOccupancy;
	                    $scope.properties[indx].extraAdultAmount = $scope.propertyaccommodations[0].extraAdultAmount;
	                    $scope.properties[indx].extraChildAmount = $scope.propertyaccommodations[0].extraChildAmount;
// 	                    $scope.properties[indx].promotionName = $scope.propertyaccommodations[0].promotionName;
						$scope.properties[indx].promotionFirstName = $scope.propertyaccommodations[0].promotionFirstName;
// 	                    $scope.properties[indx].promotionId = $scope.propertyaccommodations[0].promotionId;
	                    //$scope.properties[indx].smartPricePercent = $scope.propertyaccommodations[0].smartPricePercent;
	                    //$scope.properties[indx].smartPriceId = $scope.propertyaccommodations[0].smartPriceId;
	                    $scope.properties[indx].baseAmount = $scope.propertyaccommodations[0].baseAmount;
	                    $scope.properties[indx].discountBaseAmount = $scope.properties[indx].baseAmount; 
	                    $scope.properties[indx].totalAmount = $scope.propertyaccommodations[0].totalAmount;
	                    //$scope.properties[indx].bookRooms = $scope.propertyaccommodations[0].bookRooms;
	                    //$scope.properties[indx].getRooms = $scope.propertyaccommodations[0].getRooms;
	                    //$scope.properties[indx].bookNights = $scope.propertyaccommodations[0].bookNights;
	                   // $scope.properties[indx].getNights = $scope.propertyaccommodations[0].getNights;
	                    //$scope.properties[indx].totalActualAmount = $scope.propertyaccommodations[0].totalActualAmount;
	                    $scope.properties[indx].maxAdult = $scope.propertyaccommodations[0].maxAdult;
	                    $scope.properties[indx].maxChild = $scope.propertyaccommodations[0].maxChild;
	                    $scope.properties[indx].roomTax = $scope.propertyaccommodations[0].roomTax;
	                    $scope.properties[indx].adultsCount = $scope.propertyaccommodations[0].adultsCount;
	                    $scope.properties[indx].chiildCount = $scope.propertyaccommodations[0].chiildCount;
	                    $scope.properties[indx].rooms = $scope.propertyaccommodations[0].rooms;
	                    $scope.properties[indx].tax = $scope.propertyaccommodations[0].tax;
	                    $scope.properties[indx].taxPercentage = $scope.propertyaccommodations[0].taxPercentage;
	                    $scope.showloaderbooknow = false;
		            	 $scope.showloaderpaynow = true;
	                });

	            };
	            
	           /*  $scope.getMobileAccommodationAvailability = function(indx, accommodationId, arrivalDate, departureDate) {
	            	var checkedPropertyId;
	            	if(propertyId!=null && propertyId!=""){
	            		checkedPropertyId=propertyId;
	            	}else{
	            		checkedPropertyId=urlPropertyId;
	            	}
	            	
	                var fdata = "&strStartDate=" + arrivalDate +
	                    "&strEndDate=" + departureDate +
	                    "&propertyAccommodationId=" + accommodationId +
	                    "&propertyId=" + checkedPropertyId;
	                $http({
	                    method: 'POST',
	                    data: fdata,
	                    headers: {
	                        'Content-Type': 'application/x-www-form-urlencoded'
	                    },
	                    url: 'get-accommodation-availability'
	                }).success(function(response) {
	                    $scope.propertyaccommodations = response.data;
	                    $scope.properties[indx].baseActualAmount = $scope.propertyaccommodations[0].baseActualAmount;
	                    $scope.properties[indx].accommodationName = $scope.propertyaccommodations[0].accommodationName;
	                    $scope.properties[indx].accommodationId = $scope.propertyaccommodations[0].accommodationId;
	                    $scope.properties[indx].available = $scope.propertyaccommodations[0].available;
	                    $scope.properties[indx].minOccupancy = $scope.propertyaccommodations[0].minOccupancy;
	                    $scope.properties[indx].maxOccupancy = $scope.propertyaccommodations[0].maxOccupancy;
	                    $scope.properties[indx].extraAdultAmount = $scope.propertyaccommodations[0].extraAdultAmount;
	                    $scope.properties[indx].extraChildAmount = $scope.propertyaccommodations[0].extraChildAmount;
	                    $scope.properties[indx].promotionName = $scope.propertyaccommodations[0].promotionName;
	                    $scope.properties[indx].promotionId = $scope.propertyaccommodations[0].promotionId;
	                    $scope.properties[indx].smartPricePercent = $scope.propertyaccommodations[0].smartPricePercent;
	                    $scope.properties[indx].smartPriceId = $scope.propertyaccommodations[0].smartPriceId;
	                    $scope.properties[indx].baseAmount = $scope.propertyaccommodations[0].baseAmount;
	                    $scope.properties[indx].totalAmount = $scope.propertyaccommodations[0].totalAmount;
	                    $scope.properties[indx].bookRooms = $scope.propertyaccommodations[0].bookRooms;
	                    $scope.properties[indx].getRooms = $scope.propertyaccommodations[0].getRooms;
	                    $scope.properties[indx].bookNights = $scope.propertyaccommodations[0].bookNights;
	                    $scope.properties[indx].getNights = $scope.propertyaccommodations[0].getNights;
	                    $scope.properties[indx].totalActualAmount = $scope.propertyaccommodations[0].totalActualAmount;
	                    $scope.properties[indx].maxAdult = $scope.propertyaccommodations[0].maxAdult;
	                    $scope.properties[indx].roomTax = $scope.propertyaccommodations[0].roomTax;
	                    $scope.properties[indx].tax = $scope.propertyaccommodations[0].tax;
	                    $scope.properties[indx].taxPercentage = $scope.propertyaccommodations[0].taxPercentage;
	                    console.log($scope.propertyaccommodations);
	                });

	            };
	             */
	            
	            $scope.getCurrentDiscounts = function() {
	                var propertyId = $('#propertyId').val();
	               // var ulosignup = $('#uloSignup').val();
	                var url = "get-current-discounts";
	                $http.get(url).success(function(response) {
	                    $scope.discounts = response.data;
	                });
	            };
	            
	            $scope.getAllCurrentDiscounts = function() {
	                
	                var url = "get-all-current-discounts";
	                $http.get(url).success(function(response) {
	                    $scope.allDiscounts = response.data;
	                });
	            };
	            
	            $scope.selectDiscount = function(indx) {
	                
	            	percent = $scope.discounts[indx].discountPercentage;
	                code = $scope.discounts[indx].discountName;
	                id = $scope.discounts[indx].discountId;
	                signup = $scope.discounts[indx].isSignup;
	                $('#code_percentage').val(percent);
	                $('#code_input').val(code);
	                $('#code_id').val(id);
	                $('#code_signup').val(signup);
	            };
	            
	            $scope.mbselectDiscount = function(indx) {
	                percent = $scope.discounts[indx].discountPercentage;
	                code = $scope.discounts[indx].discountName;
	                id = $scope.discounts[indx].discountId;
	                signup = $scope.discounts[indx].isSignup;
	                $('#mbcode_percentage').val(percent);
	                $('#mbcode_input').val(code);
	                $('#mbcode_id').val(id);
	                $('#mbcode_signup').val(signup);
	            };
	            $scope.dapply = "APPLY";
	            $scope.applyDiscount = function() {
	                $scope.dapply = "APPLYING";
	                var discountPercentage = $('#code_percentage').val();
	                var j = false;
	                if(discountPercentage == ''){
	                	var firstCouponName = $('#code_input').val();
	                	for (var i = 0; i < $scope.allDiscounts.length; i++) {
	                	
	                        if ($scope.allDiscounts[i].discountName === firstCouponName) {
	                            var discountPercentage =  $scope.allDiscounts[i].discountPercentage;
	                            var discountId =  $scope.allDiscounts[i].discountId;
	                           // var ulosignup =  $scope.allDiscounts[i].ulosignup;
	                            var isSignup =  $scope.allDiscounts[i].isSignup;
	                            var CouponName = $('#code_input').val();
	                          j = true;
	                        } 
	                     
	                        }
	                	 if(j == false){
	                           	  $scope.dapply = "APPLY";
	                		  var alertmsg = ' <div class="alert alert-success alert-dismissible" style="color:red;text-align:center;">This coupon is expired.</div>';
	                          $('#couponerrortwo').html(alertmsg);
	                          $(function() {
	                              setTimeout(function() {
	                                  $("#couponerrortwo").hide('blind', {}, 100)
	                              }, 2000);
	                          });
	                          return false;
	                        }
	                }else{
	                var discountId = $('#code_id').val();
	                var discountPercentage = $('#code_percentage').val();
	                var isSignup = $('#code_signup').val();
	                var CouponName = $('#code_input').val();
	                }
	                if (isSignup == "true") {
	                    if (ulosignup == "true") {
	                        $scope.priceDropped = Math.round(((grandTotal) * discountPercentage) / 100);
	                        $scope.tempPrice = Math.round((grandTotal) - (((grandTotal) * discountPercentage) / 100));
	                        $scope.bookingDetails[0].discountId = discountId;

	                        for (var i = 0; i < $scope.properties.length; i++) {
	                            var booked = $scope.properties[i];
	                            booked.total = Math.round((booked.discountBaseAmount) - (((booked.discountBaseAmount) * discountPercentage) / 100));
	                            booked.tax = Math.round(booked.total * booked.taxPercentage / 100);
	                        }
	                        console.log($scope.bookingDetails);
	                        $timeout(function() {
	                        	$scope.dapply = "APPLY";
	                            $('#dcouponmodal').click();
	                            
	                        }, 2000);

	                    }
	                    else {
	                        alert("please signup to apply this code");
	                    }
	                } else {
	                	for(var i = 0; i < $scope.properties.length; i++) {
	                		
	                		var booked = $scope.properties[i];
	                	    $scope.priceDropped = Math.round(((booked.discountBaseAmount) * discountPercentage) / 100);
		 	                $scope.tempPrice = Math.round((booked.discountBaseAmount) - (((booked.discountBaseAmount) * discountPercentage) / 100));
	                        booked.totalAmount = Math.round((booked.discountBaseAmount) - ((booked.discountBaseAmount) * discountPercentage) / 100);
	                        booked.tax = Math.round((booked.totalAmount) * (booked.taxPercentage) / 100);
	                        booked.discountId = discountId;
	                        booked.discountName = CouponName;
	                        booked.discountPercent = discountPercentage;
	                    }
	                    $timeout(function() {
	                    	  $scope.dapply = "APPLY";
	                        $('#dcouponmodal').click();
	                      
	                    }, 2000);
	                    
	                    console.log($scope.properties);
	                }

	            };
	            $scope.mapply = "APPLY";
	            $scope.mbapplyDiscount = function() {
	            	$scope.mapply = "APPLYING";
	            	var j = false;
	                var discountPercentage = $('#mbcode_percentage').val();
	                if(discountPercentage == ''){
	                	var firstCouponName = $('#mbcode_input').val();
	                
	                	for (var i = 0; i < $scope.allDiscounts.length; i++) {
	                        if ($scope.allDiscounts[i].discountName === firstCouponName) {
	                            var discountPercentage =  $scope.allDiscounts[i].discountPercentage;
	                            var discountId =  $scope.allDiscounts[i].discountId;
	                            var ulosignup =  $scope.allDiscounts[i].ulosignup;
	                            var isSignup =  $scope.allDiscounts[i].isSignup;
	                            var CouponName = $('#mbcode_input').val();
	                            //$scope.CouponPercent = discountPercentage;
	                           // j =  true;
	                         
	                          
	                        } 
	                        }
	                	 if(j == false){
	                		 $scope.mapply = "APPLY"; 
	                		 var alertmsg = ' <div class="alert alert-success alert-dismissible" style="color:red;text-align:center;">This coupon is expired.</div>';
	                          $('#couponerrorone').html(alertmsg);
	                          $(function() {
	                              setTimeout(function() {
	                                  $("#couponerrorone").hide('blind', {}, 100)
	                              }, 2000);
	                          });
	                        	return false;
	                        }

	                }else{
	            	
	                var discountPercentage = $('#mbcode_percentage').val();
	                var discountId = $('#mbcode_id').val();
	                var isSignup = $('#mbcode_signup').val();
	                var CouponName = $('#mbcode_input').val();
	                }
	                if (isSignup == "true") {
	                    if (ulosignup == "true") {
	                        $scope.priceDropped = Math.round(((grandTotal) * discountPercentage) / 100);
	                        $scope.tempPrice = Math.round((grandTotal) - (((grandTotal) * discountPercentage) / 100));
	                        $scope.bookingDetails[0].discountId = discountId;

	                        for (var i = 0; i < $scope.properties.length; i++) {
	                            var booked = $scope.properties[i];
	                            booked.total = Math.round((booked.discountBaseAmount) - (((booked.discountBaseAmount) * discountPercentage) / 100));
	                            booked.tax = Math.round(booked.total * booked.taxPercentage / 100);
	                        }
	                        console.log($scope.bookingDetails);
	                        $timeout(function() {
	                        	$scope.mapply = "APPLY";
	                        	$('#mcouponmodal').click();
	                        }, 2000);
	                    } else {
	                        alert("please signup to apply this code");
	                    }
	                } else {
	                	for(var i = 0; i < $scope.properties.length; i++) {
	                		
	                		var booked = $scope.properties[i];
	                	    $scope.priceDropped = Math.round(((booked.discountBaseAmount) * discountPercentage) / 100);
		 	                $scope.tempPrice = Math.round((booked.discountBaseAmount) - (((booked.discountBaseAmount) * discountPercentage) / 100));
	                        booked.totalAmount = Math.round((booked.discountBaseAmount) - ((booked.discountBaseAmount) * discountPercentage) / 100);
	                        booked.tax = Math.round((booked.totalAmount) * (booked.taxPercentage) / 100);
	                        booked.discountId = discountId;
	                        booked.discountName = CouponName;
	                        booked.discountPercent = discountPercentage;
	                    }
	                    $timeout(function() {
	                    	$scope.mapply = "APPLY";
	                        $('#mcouponmodal').click();
	                    }, 2000);
	                    console.log($scope.properties);
	                }

	            };
	            
	            $scope.quickBook = function(indx, roomCount, adultCount, childCount) {
					
	            	$scope.bookingDetails = [];
	                
	                var propertyId = $scope.properties[indx].DT_RowId;	              
	                var propertyName = $scope.properties[indx].propertyName;
	                var arrival = $scope.properties[indx].arrivalDate;
	                var available = $scope.properties[indx].available;
	                var departure = $scope.properties[indx].departureDate;
	                var dt1 = new Date(arrival);
	                var dt2 = new Date(departure);
	                var dd = dt1.getDate()+1;
	                var mm = dt1.getMonth();
	    			//var yy = dt1.getFullYear();
	                var dayName = dt1.getDay();
	                var departuredd = dt2.getDate()+1;
	                var departuremm = dt2.getMonth();
	    			//var yy = dt2.getFullYear();
	                var departuredayName = dt2.getDay();
	                var checkIn = monthNames[mm] + ' ' + dd + '' + weekDays[dayName];
	                var checkOut = monthNames[departuremm] + ' ' + departuredd + '' + weekDays[departuredayName];
	                var paymentArrival = checkIn;
	                var paymentDeparture = checkOut;
	                var accommId = $scope.properties[indx].accommodationId;
	                var type = $scope.properties[indx].accommodationName;
	                var rooms = roomCount;
	                var adultsCount = adultCount;
	                var childCount = childCount;
	                var diffDays = 0;
	                var baseAmount = parseInt($scope.properties[indx].totalAmount);
	                var totalAmount = parseInt($scope.properties[indx].totalAmount);
	                var adultsAllow = 0;
	                var childAllow = 0;
	                var minOccu = $scope.properties[indx].minOccupancy;
	                var extraAdult = parseInt($scope.properties[indx].extraAdultAmount);
	                var extraInfant = 0
	                var extraChild = parseInt($scope.properties[indx].extraChildAmount);
	                var taxes = $scope.properties[indx].tax;
	                var randomNo = Math.random();
	                var discountName = $scope.properties[indx].discountName;
	                var promotionFirstName = $scope.properties[indx].promotionFirstName;
	                	
	                if (promotionFirstName == 'None') {
	                	var promotionfirstName = '';	                	
	                	 promotionFirstName = '';
	                } else {
	                	
	                	var promotionfirstName = promotionFirstName;
	                    var temp = promotionFirstName.replace("%", "PERCENT");

	                    promotionFirstName = temp;
	                } 
	                  
	                var promotionId = $scope.properties[indx].promotionId;
	                var promotionName = promotionFirstName;
	                //var smartPriceId = $scope.properties[indx].smartPriceId;
	                var discountId = $scope.properties[indx].discountId;
	                var discountPercent = $scope.properties[indx].discountPercent;
	                var dropDiscount = Math.round(baseAmount * discountPercent/100)
	                var diffDays = $scope.properties[indx].diffDays;
	                //var bookRooms = $scope.properties[indx].bookRooms;
	                //var getRooms = $scope.properties[indx].getRooms;
	                //var bookNights = $scope.properties[indx].bookNights;
	                //var getNights = $scope.properties[indx].getNights;
	                if($scope.properties[indx].paynowDiscount != 0){
	                	var paynowDiscount = $scope.properties[indx].paynowDiscount;
	                }
	                else{
	                	var paynowDiscount = 0;	
	                }
	                
	                var taxPercentage = $scope.properties[indx].taxPercentage;
	                var advance = 0;
	                var baseTotalAmount = parseInt($scope.properties[indx].baseActualAmount);
	                var minAmount=parseFloat($scope.properties[indx].minimumAmount);
	                var maxAmount=parseFloat($scope.properties[indx].maximumAmount);
	                
	                var newData = {
	                    'indx': indx,
	                    'available': available,
	                    'arrival': arrival,
	                    'departure': departure,
	                    'paymentArrival':paymentArrival,
	                    'paymentDeparture':paymentDeparture,
	                    'accommodationId': accommId,
	                    'sourceId': 1,
	                    'accommodationType': type,
	                    'rooms': rooms,
	                    'diffDays': diffDays,
	                    'baseAmount': baseAmount,
	                    'total': totalAmount,
	                    'adultsCount': adultsCount,
	                    'infantCount': 0,
	                    'childCount': childCount,
	                    'refund': 0,
	                    'statusId': 2,
	                    'tax': taxes,
	                    'randomNo': randomNo,
	                    'promotionId': promotionId,
	                    'promotionName': promotionName,
	                    'promotionFirstName': promotionfirstName,
// 	                    'promotionSecondName': promotionsecondName,
	                    //'smartPriceId': smartPriceId,
	                    'discountId': discountId,
	                    'discountName': discountName,
	                    /* 'bookRooms': bookRooms,
	                    'getRooms': getRooms,
	                    'bookNights': bookNights,
	                    'getNights': getNights, */
	                    'advanceAmount': advance,
	                    'baseOriginAmount': parseInt(baseTotalAmount * rooms) ,
	                    'propertyId':propertyId,
	                    'propertyName':propertyName,
	                    'paynowDiscount':paynowDiscount,
	                    'appliedPayNow':0,
	                    'taxPercentage':taxPercentage,
	                    'dropedDiscountPrice':dropDiscount,
	                    'usedRewardPoints':0,
	                    'dropedRewardPrice':0,
	                    'extraAdult':extraAdult,
	                    'extraChild':extraChild,
	                    'minimumAmount':minAmount,
	                    'maximumAmount':maxAmount,
	                };
	                $scope.bookingDetails.push(newData);
	               /*  var details = "&details=" + '{"array":' + JSON.stringify($scope.bookingDetails) + '}' +
	                   "&propertyId=" + propertyId +
	                   "&propertyName=" + propertyName; */
	               //  console.log(JSON.stringify($scope.bookingDetails))
	              // window.location = '/paymentsuccess';
	        	       document.getElementById("mpaymentpage").style.display="block"; 
                        document.getElementById("mobiledetailpage").style.display="none";
	        	        document.getElementById("dpaymentpage").style.display="block"; 
	        	    	document.getElementById("desktopdetailpage").style.display="none"; 
	        	    	 $('html,body').animate({
	        			        scrollTop: $("#mpaymentpage").offset().top},
	        			        'slow');
	                /* $http({
	                    method: 'POST',
	                    data: details,
	                    headers: {
	                        'Content-Type': 'application/x-www-form-urlencoded'
	                    },
	                    url: 'get-selected-details'
	                }).success(function(response) {
	                	
	                    window.location = '/bookingdetails';
	                });  */
	            };

	            
	            $scope.mbquickBook = function(indx, roomCount, adultCount, childCount) {
	            	

	            	$scope.bookingDetails = [];
	                
	                var propertyId = $scope.properties[indx].DT_RowId;	              
	                var propertyName = $scope.properties[indx].propertyName;
	                var arrival = $scope.properties[indx].arrivalDate;
	                var available = $scope.properties[indx].available;
	                var departure = $scope.properties[indx].departureDate;
	                var dt1 = new Date(arrival);
	                var dt2 = new Date(departure);
	                var dd = dt1.getDate();
	                var mm = dt1.getMonth();
	    			//var yy = dt1.getFullYear();
	                var dayName = dt1.getDay();
	                var dd = dt2.getDate();
	                var mm = dt2.getMonth();
	    			//var yy = dt2.getFullYear();
	                var dayName = dt2.getDay();
	                var checkIn = monthNames[mm] + ' ' + dd + '' + weekDays[dayName];
	                var checkOut = monthNames[mm] + ' ' + dd + '' + weekDays[dayName];
	                var paymentArrival = checkIn;
	                alert(checkIn+"---------"+checkOut)
	                var paymentDeparture = checkOut;
	                var accommId = $scope.properties[indx].accommodationId;
	                var type = $scope.properties[indx].accommodationName;
	                var rooms = roomCount;
	                var adultsCount = adultCount;
	                var childCount = childCount;
	                var diffDays = 0;
	                var baseAmount = $scope.properties[indx].baseAmount;
	                var totalAmount = $scope.properties[indx].totalAmount;
	                var adultsAllow = 0;
	                var childAllow = 0;
	                var minOccu = $scope.properties[indx].minOccupancy;
	                var extraAdult = $scope.properties[indx].extraAdultAmount;
	                var extraInfant = 0
	                var extraChild = $scope.properties[indx].extraChildAmount;
	                var taxes = $scope.properties[indx].tax;
	                var randomNo = Math.random();
	                var discountName = $scope.properties[indx].discountName;
	                var promotionFirstName = $scope.properties[indx].promotionFirstName;
// 	                var promotionSecondName = $scope.properties[indx].promotionSecondName;

	                if (promotionFirstName == 'None') {
	                	var promotionfirstName = '';	                	
	                	 promotionFirstName = '';
	                } else {
	                	
	                	var promotionfirstName = promotionFirstName;
	                    var temp = promotionFirstName.replace("%", "PERCENT");

	                    promotionFirstName = temp;
	                } 
	                 /* if (promotionSecondName == 'None') {
						var promotionsecondName = '';	                	 
	                	 promotionSecondName = '';
	                } else {
	                	var promotionsecondName = promotionSecondName;
	                    var temp = promotionSecondName.replace("%", "PERCENT");
							
	                    promotionSecondName = temp;
	                } */ 
	                var promotionId = $scope.properties[indx].promotionId;
	                var promotionName = promotionFirstName;
	                var smartPriceId = $scope.properties[indx].smartPriceId;
	                var discountId = $scope.properties[indx].discountId;
	                var discountPercent = $scope.properties[indx].discountPercent;
	                var dropDiscount = Math.round(baseAmount * discountPercent/100)
	                var diffDays = $scope.properties[indx].diffDays;
	                var bookRooms = $scope.properties[indx].bookRooms;
	                var getRooms = $scope.properties[indx].getRooms;
	                var bookNights = $scope.properties[indx].bookNights;
	                var getNights = $scope.properties[indx].getNights;
	                var minAmount=$scope.properties[indx].minimumAmount;
	                var maxAmount=$scope.properties[indx].maximumAmount;
	                if($scope.properties[indx].paynowDiscount != 0){
	                	var paynowDiscount = $scope.properties[indx].paynowDiscount;
	                }
	                else{
	                	var paynowDiscount = 0;	
	                }
	                var taxPercentage = $scope.properties[indx].taxPercentage;
	                var advance = 0;
	                var baseTotalAmount = $scope.properties[indx].baseActualAmount;
	                
	                var newData = {
	                    'indx': indx,
	                    'available': available,
	                    'arrival': arrival,
	                    'departure': departure,
	                    'paymentArrival':paymentArrival,
	                    'paymentDeparture':paymentDeparture,
	                    'accommodationId': accommId,
	                    'sourceId': 1,
	                    'accommodationType': type,
	                    'rooms': rooms,
	                    'diffDays': diffDays,
	                    'baseAmount': baseAmount,
	                    'total': totalAmount,
	                    'adultsCount': adultsCount,
	                    'infantCount': 0,
	                    'childCount': childCount,
	                    'refund': 0,
	                    'statusId': 2,
	                    'tax': taxes,
	                    'randomNo': randomNo,
	                    'promotionId': promotionId,
	                    'promotionName': promotionName,
	                    'promotionFirstName': promotionfirstName,
// 	                    'promotionSecondName': promotionsecondName,
	                    'smartPriceId': smartPriceId,
	                    'discountId': discountId,
	                    'discountName': discountName,
	                    'bookRooms': bookRooms,
	                    'getRooms': getRooms,
	                    'bookNights': bookNights,
	                    'getNights': getNights,
	                    'advanceAmount': advance,
	                    'baseOriginAmount': baseTotalAmount,
	                    'propertyId':propertyId,
	                    'propertyName':propertyName,
	                    'paynowDiscount':paynowDiscount,
	                    'appliedPayNow':0,
	                    'taxPercentage':taxPercentage,
	                    'dropedDiscountPrice':dropDiscount,
	                    'usedRewardPoints':0,
	                    'dropedRewardPrice':0,
	                    'extraAdult':extraAdult,
	                    'extraChild':extraChild,
	                    'minimumAmount':minAmount,
	                    'maximumAmount':maxAmount,
	                };
	                
	                
	                $scope.bookingDetails.push(newData); 
	               /*  var details = "&details=" + '{"array":' + JSON.stringify($scope.bookingDetails) + '}' +
	                   "&propertyId=" + propertyId +
	                   "&propertyName=" + propertyName; */
	               //  console.log(JSON.stringify($scope.bookingDetails))
	        	        
	        	        document.getElementById("dpaymentpage").style.display="block"; 
	        	    	document.getElementById("mobiledetailpage").style.display="none"; 
	        	    
	                /* $http({
	                    method: 'POST',
	                    data: details,
	                    headers: {
	                        'Content-Type': 'application/x-www-form-urlencoded'
	                    },
	                    url: 'get-selected-details'
	                }).success(function(response) {
	                	
	                    window.location = '/bookingdetails';
	                });  */
	            };

	            
	            $scope.confirmBooking = function() {
	                $(this).attr('disabled', 'disabled');
	                $scope.progressbar.start();
	                $scope.dpaynotification = "Please wait,Booking is processing..."
	                //$('#loader').show();
	               //$scope.bookingDetails[0].advanceAmount = $scope.getTotal();
	           /*      for (var i = 0; i < $scope.bookingDetails.length; i++) {
	                    var booked = $scope.bookingDetails[i];
	                   	var btotal = parseInt(booked.total);
	                   	var btax = parseInt(booked.tax);
	                    var gtotal = btotal+btax;
	                    var ototal = $scope.getTotal() ;
	                    var atotal = ototal - gtotal;
	                    if(ototal >= atotal){
	                     var aa = ototal-atotal;
	                     booked.advanceAmount = aa;
	                    }
	                } */
	                 var text = '{"array":' + JSON.stringify($scope.bookingDetails) + '}';
	                var data = JSON.parse(text);
	                var sourceid = 1;
	                $http({
	                    method: 'POST',
	                    data: data,
	                    //data : '{"data":' +JSON.stringify($scope.roomsSelected) + '}',
	                    dataType: 'json',
	                    headers: {
	                        'Content-Type': 'application/json; charset=utf-8'
	                    },
	                    url: "add-booking?sourceId=" + sourceid  //+$('#rewardPoints').val()
	                }).then(function successCallback(response) {
	                    var gdata = "firstName=" + $('#firstName').val() +
	                        "&emailId=" + $('#guestEmail').val() +
	                        "&phone=" + $('#mobilePhone1').val()
	                    $http({
	                        method: 'POST',
	                        data: gdata,
	                        headers: {
	                            'Content-Type': 'application/x-www-form-urlencoded'
	                        },
	                        url: 'add-guest'

	                    }).then(function successCallback(response) {
	                       // var specialRequest = $('#specialRequest').val();
	                        var text = '{"array":' + JSON.stringify($scope.bookingDetails) + '}';
	                        var data = JSON.parse(text);
	                        $http({
	                            method: 'POST',
	                            data: data,
	                            dataType: 'json',
	                            headers: {
	                                'Content-Type': 'application/json; charset=utf-8'
	                            },
	                            url: 'add-booking-details?specialRequest='+"Nil"
	                            //url : 'jsonTest'  ?specialRequest=' + specialRequest  +"userRewardPoints="+$('#userRewardPoints').val()
	                        }).then(function successCallback(response) {
	                          //  $(this).removeAttr('disabled');
	                            //$('#loader').hide();
	                            //$scope.booked = response.data;
	                            //console.log($scope.booked.data[0].bookingId);
	                           $scope.dpaynotification = "Pay Now"
	                            window.location = '/paymentsuccess';

	                        }, function errorCallback(response) {});
	        				
	                    }, function errorCallback(response) {});

	                }, function errorCallback(response) {});

	            };
	            
	            
	            $scope.mbconfirmBooking = function() {
	                $(this).attr('disabled', 'disabled');
	                $scope.mbpaynotification = "Please Wait..Booking Processing"
	                //$('#loader').show();
	               //$scope.bookingDetails[0].advanceAmount = $scope.getTotal();
	           /*      for (var i = 0; i < $scope.bookingDetails.length; i++) {
	                    var booked = $scope.bookingDetails[i];
	                   	var btotal = parseInt(booked.total);
	                   	var btax = parseInt(booked.tax);
	                    var gtotal = btotal+btax;
	                    var ototal = $scope.getTotal() ;
	                    var atotal = ototal - gtotal;
	                    if(ototal >= atotal){
	                     var aa = ototal-atotal;
	                     booked.advanceAmount = aa;
	                    }
	                } */
	                 var text = '{"array":' + JSON.stringify($scope.bookingDetails) + '}';
	                var data = JSON.parse(text);
	                var sourceid = 1;
	                $http({
	                    method: 'POST',
	                    data: data,
	                    //data : '{"data":' +JSON.stringify($scope.roomsSelected) + '}',
	                    dataType: 'json',
	                    headers: {
	                        'Content-Type': 'application/json; charset=utf-8'
	                    },
	                    url: "add-booking?sourceId=" + sourceid  //+$('#rewardPoints').val()
	                }).then(function successCallback(response) {
	                    var gdata = "firstName=" + $('#mbfirstName').val() +
	                        "&emailId=" + $('#mbguestEmail').val() +
	                        "&phone=" + $('#mbMobilePhone1').val()
	                    $http({
	                        method: 'POST',
	                        data: gdata,
	                        headers: {
	                            'Content-Type': 'application/x-www-form-urlencoded'
	                        },
	                        url: 'add-guest'

	                    }).then(function successCallback(response) {
	                       // var specialRequest = $('#specialRequest').val();
	                        var text = '{"array":' + JSON.stringify($scope.bookingDetails) + '}';
	                        var data = JSON.parse(text);
	                        $http({
	                            method: 'POST',
	                            data: data,
	                            dataType: 'json',
	                            headers: {
	                                'Content-Type': 'application/json; charset=utf-8'
	                            },
	                            url: 'add-booking-details?specialRequest='+"Nil"
	                            //url : 'jsonTest'  ?specialRequest=' + specialRequest  +"userRewardPoints="+$('#userRewardPoints').val()
	                        }).then(function successCallback(response) {
	                          //  $(this).removeAttr('disabled');
	                            //$('#loader').hide();
	                            //$scope.booked = response.data;
	                            //console.log($scope.booked.data[0].bookingId);
	                           $scope.mbpaynotification = "Payment Done"
	                            window.location = '/paymentsuccess';

	                        }, function errorCallback(response) {});
	        				
	                    }, function errorCallback(response) {});

	                }, function errorCallback(response) {});

	            };
	            
	            $scope.dpaynotification = "Pay Now"
	            $scope.dpbooking = function(indx) {
	            	
	            	$scope.dpaynotification = "Please wait,Booking is processing.."	            
	            	var appliedPayNow = parseInt($scope.bookingDetails[indx].appliedPayNow);
	            	
	            	
	                
                   
	            	if(appliedPayNow == 0){
	            	var total = parseInt($scope.bookingDetails[indx].total);
	            	var paynowAmount = parseInt($scope.bookingDetails[indx].paynowDiscount);
	            	var tax = $scope.bookingDetails[indx].tax;
	            	
	            	var taxpercent = $scope.bookingDetails[indx].taxPercentage;
	            	
	            	var finalAmount = parseInt(total - paynowAmount);
	            	var finalTax = parseInt(finalAmount * taxpercent / 100);
	            	var finalTotalAmount = parseInt(finalAmount + finalTax);
	            	$scope.bookingDetails[indx].total = finalAmount;
	            	$scope.bookingDetails[indx].appliedPayNow = paynowAmount;
	            	$scope.bookingDetails[indx].tax = finalTax;

	            	$scope.bookingDetails[indx].advanceAmount =finalTotalAmount;
	            	}
	            	
	            	else{
	            	
	            		
	            	    finalAmount = $scope.bookingDetails[indx].total ;
	            	    taxpercent = $scope.bookingDetails[indx].taxPercentage;
	            	    finalTax = parseInt(finalAmount * taxpercent / 100);
		            	finalTotalAmount = parseInt(finalAmount + finalTax);

		            	$scope.bookingDetails[indx].advanceAmount =finalTotalAmount;
		            	
	            		
	            	}
	            	/*  if ($scope.tempPrice == 0) {
	                    var payable = $scope.getTotal();
	                } else {
	                    var payable = $scope.getTotal()
	                } */
	             var options = {
// 	                 	"key": "rzp_test_bQQGkpEPtwppNI",
	                    "key": "rzp_live_vuYF9peaDXMoQF",
	                    "amount": finalTotalAmount * 100, // 2000 paise = INR 20
	                    "name": "Ulohotels.com",
	                    "description": "Welcome to Ulohotels",
	                    //"image": "/your_logo.png",
	                    "handler": function(response) {;
	                        $scope.confirmBooking();
	                    },
	                    "prefill": {
	                        "name": $('#firstName').val(),
	                        "email": $('#guestEmail').val(),
	                        "contact": $('#mobilePhone1').val()
	                    },
	                    "notes": {
	                        "address": $('#mobilePhone1').val()
	                    },
	                    "theme": {
	                        "color": "#89b717"
	                    }
	                };
	                var rzp1 = new Razorpay(options);
	                rzp1.open();
	                e.preventDefault();
	            };
	            $scope.mbpaynotification = "Pay Now"
	            $scope.mbbooking = function(indx) {
	            	$scope.progressbar.start();
	            	 $scope.mbpaynotification = "Please Wait..Booking Processing"
	            	var appliedPayNow = $scope.bookingDetails[indx].appliedPayNow;

	            	if(appliedPayNow == 0){
		            	var total = parseInt($scope.bookingDetails[indx].total);
		            	var paynowAmount = $scope.bookingDetails[indx].paynowDiscount;
		            	var tax = $scope.bookingDetails[indx].tax;
		            	var taxpercent = $scope.bookingDetails[indx].taxPercentage;
		            	var finalAmount = parseInt(total - paynowAmount);
		            	var finalTax = parseInt(finalAmount * taxpercent / 100);
		            	var finalTotalAmount = parseInt(finalAmount + finalTax);
		            	$scope.bookingDetails[indx].total = finalAmount;
		            	$scope.bookingDetails[indx].appliedPayNow = paynowAmount;
		            	$scope.bookingDetails[indx].tax = finalTax;
		            	$scope.bookingDetails[indx].advanceAmount =finalTotalAmount;
	            	}else{
	            	
	            		
	            	    finalAmount = $scope.bookingDetails[indx].total ;
	            	    taxpercent = $scope.bookingDetails[indx].taxPercentage;
	            	    finalTax = parseInt(finalAmount * taxpercent / 100);
		            	finalTotalAmount = parseInt(finalAmount + finalTax);
		            	$scope.bookingDetails[indx].advanceAmount =finalTotalAmount;
	            		
	            	}
	            	/*  if ($scope.tempPrice == 0) {
	                    var payable = $scope.getTotal();
	                } else {
	                    var payable = $scope.getTotal()
	                } */
	             var options = {
	                 	//"key": "rzp_test_bQQGkpEPtwppNI",
	                    "key": "rzp_live_vuYF9peaDXMoQF",
	                    "amount": finalTotalAmount * 100, // 2000 paise = INR 20
	                    "name": "Ulohotels.com",
	                    "description": "Welcome to Ulohotels",
	                    //"image": "/your_logo.png",
	                    "handler": function(response) {;
	                        $scope.mbconfirmBooking();
	                    },
	                    "prefill": {
	                        "name": $('#mbfirstName').val(),
	                        "email": $('#mbguestEmail').val(),
	                        "contact": $('#mbMobilePhone1').val()
	                    },
	                    "notes": {
	                        "address": $('#mbMobilePhone1').val()
	                    },
	                    "theme": {
	                        "color": "#89b717"
	                    }
	                };
	                var rzp1 = new Razorpay(options);
	                rzp1.open();
	                e.preventDefault();
	            };
	            
	            
	            $scope.getRoomCount = function() {

	                var total = 0;

	                for (var i = 0; i < $scope.properties.length; i++) {

	                    var accommodation = $scope.properties[i];
	                    total += parseInt(accommodation.rooms);

	                }
	                if (isNaN(total)) total = 1;
	                return total;


	            }

	            $scope.getAdults = function() {
	                var total = 0;
	                for (var i = 0; i < $scope.properties.length; i++) {
	                    var accommodation = $scope.properties[i];
	                    total += parseInt(accommodation.adultsCount);
	                }
	                if (isNaN(total)) total = 1;
	                return total;
	            }
	            $scope.getTotalAdult = function() {
	                var total = 0;
	                for (var i = 0; i < $scope.bookingDetails.length; i++) {
	                    var accommodation = $scope.bookingDetails[i];
	                    total += parseInt(accommodation.adultsCount);
	                }
	                if (isNaN(total)) total = 1;
	                return total;
	            }
	            $scope.getTotalChild = function() {
	                var total = 0;
	                for (var i = 0; i < $scope.bookingDetails.length; i++) {
	                    var accommodation = $scope.bookingDetails[i];
	                    total += parseInt(accommodation.childCount);
	                }
	                if (isNaN(total)) total = 1;
	                return total;
	            }


	            $scope.getChild = function() {
	                var total = 0;
	                for (var i = 0; i < $scope.properties.length; i++) {
	                    var accommodation = $scope.properties[i];
	                    total += parseInt(accommodation.childCount);
	                }
	                if (isNaN(total)) total = 0;
	                return total;

	            }
	            
	            $scope.getTotal = function() {
	            	var total = 0;
	              for (var i = 0; i < $scope.properties.length; i++) {
	            	  		var accommodation = $scope.properties[i];
	                        var totalAmount = parseInt(accommodation.totalAmount);
	                        var tax = parseInt(accommodation.tax);
	                        total = Math.round(totalAmount + tax);
						
	                    }
	              return total;
	                }
	            
	            $scope.getBaseActualTotal = function() {
	            	var total = 0;
	              for (var i = 0; i < $scope.properties.length; i++) {
	            	  		var accommodation = $scope.properties[i];
	                        var baseActualAmount = parseInt(accommodation.baseActualAmount * accommodation.rooms);
	                        var taxPercent = parseInt(accommodation.taxPercentage);
	                        var tax = parseInt(baseActualAmount * taxPercent /100 );
	                        total = Math.round(baseActualAmount + tax);
	                    }
	              return total;
	                }
	            
	            
	            $scope.getBookingTotal = function() {
	            	var total = 0;
	              for (var i = 0; i < $scope.bookingDetails.length; i++) {
	            	  		var accommodation = $scope.bookingDetails[i];
	                        var totalAmount = parseInt(accommodation.total);
	                        var tax = parseInt(accommodation.tax);
	                        total = Math.round(totalAmount + tax);
						
	                    }
	              return total;
	                }
	            
	            $scope.getFinalTotal = function() {
	            	var total = 0;
	              for (var i = 0; i < $scope.bookingDetails.length; i++) {
	            	  		var accommodation = $scope.bookingDetails[i];
	                        var totalAmount = parseInt(accommodation.total);
	                        total = Math.round(totalAmount);
						
	                    }
	              return total;
	                }
	            
	            $scope.getFinalTax = function() {
	            	var total = 0;
	              for (var i = 0; i < $scope.bookingDetails.length; i++) {
	            	  		var accommodation = $scope.bookingDetails[i];
	                        var totalTax = parseInt(accommodation.tax);
	                        total = Math.round(totalTax);
						
	                    }
	              return total;
	                }
	            
	            $scope.getTotalSaveAmount = function() {
	            	var total = 0;
	              for (var i = 0; i < $scope.bookingDetails.length; i++) {
	            	  		var accommodation = $scope.bookingDetails[i];
	            	  		var dropFlat = (accommodation.baseOriginAmount - accommodation.baseAmount)
	            	  		
	                        var dropDiscount = parseInt(accommodation.dropedDiscountPrice);
	            	  		
	            	  		 var dropReward = parseInt(accommodation.dropedRewardPrice);
	                        total = Math.round(dropFlat + dropDiscount + dropReward);
	                    }

	              return total;
	                }
	            
	            
	            $scope.sendPayAtHotelOtp = function() {
	            
	            
	            	
	              	 var gdata = "firstName=" + $('#firstName').val() +
	                   "&emailId=" + $('#guestEmail').val() +
	                   "&phone=" + $('#mobilePhone1').val();

	              	 $http({
	                       method: 'POST',
	                       data: gdata,
	                       headers: {
	                           'Content-Type': 'application/x-www-form-urlencoded'
	                       },
	                       url: "send-payathotel-otp"
	                   }).success(function(response) {
	                       $scope.payathotelotp = response.data;
	                       //console.log($scope.payathotelotp);
	                       $('#myotpnowmodal').modal('show');
	                   });   	
	                
	              };    
	              
	              $scope.mbsendPayAtHotelOtp = function() {
	  	            
	  	            
		            	
		              	 var gdata = "firstName=" + $('#mbfirstName').val() +
		                   "&emailId=" + $('#mbguestEmail').val() +
		                   "&phone=" + $('#mbMobilePhone1').val();

		              	 $http({
		                       method: 'POST',
		                       data: gdata,
		                       headers: {
		                           'Content-Type': 'application/x-www-form-urlencoded'
		                       },
		                       url: "send-payathotel-otp"
		                   }).success(function(response) {
		                       $scope.payathotelotp = response.data;
		                       //console.log($scope.payathotelotp);
		                       $('#myotpnowmodal').modal('show');
		                   });   	
		                
		              };    
	             
	              
	              $scope.reSendPayAtHotelOtp = function() {
	             	 var gdata = "guestId=" + $('#smsGuestId').val();
	             	 $http({
	                      method: 'POST',
	                      data: gdata,
	                      headers: {
	                          'Content-Type': 'application/x-www-form-urlencoded'
	                      },
	                      url: "resend-payathotel-otp"
	                  }).success(function(response) {
	                      $scope.repayathotelotp = response.data;
	                      console.log($scope.repayathotelotp);
	                  });   	
	               
	             };
	             
	           $scope.smsapply = "Verify";
	             $scope.verifySms = function() {
	           	  //$scope.searchButtonSmsText = "APPLYING";
	           	 //$("#loading-example-btn").prop('disabled', true)
	           	 $('#disablesms').prop('disabled', true);
	            	 var indx = 0;
		            	var appliedPayNow =  parseInt($scope.bookingDetails[indx].appliedPayNow);
	   	           	 if(appliedPayNow != 0){
	   	           
	   	           	    var total = parseInt($scope.bookingDetails[indx].total);
	   	            	var tax = $scope.bookingDetails[indx].tax;
	   	            	var taxpercent = $scope.bookingDetails[indx].taxPercentage;
	   	            	var finalAmount = parseInt(total + appliedPayNow);
	   	            	var finalTax = parseInt(finalAmount * taxpercent / 100);
	   	            	var finalTotalAmount = parseInt(finalAmount + finalTax);
	   	            	$scope.bookingDetails[indx].total = finalAmount;
	   	            	$scope.bookingDetails[indx].tax = finalTax;
	   	            	$scope.bookingDetails[indx].appliedPayNow = 0;

	   	            	$scope.bookingDetails[indx].advanceAmount = 0;
	   	           	 }else{
	   	           		$scope.bookingDetails[indx].advanceAmount = 0;
	   	           	 }
	           	
	           	 $scope.smsapply = "Please Wait";  
	           	 var gdata = "guestId=" + $('#smsGuestId').val() +
	           	    "&payAtHotelOtp=" + $('#smsOtp').val() ;
	           	   
	           		 $http({
	           	            method: 'POST',
	           	            data: gdata,
	           	            headers: {
	           	                'Content-Type': 'application/x-www-form-urlencoded'
	           	            },
	           	            url: "verify-sms-otp"
	           	            
	           	        }).then(function successCallback(response) {
	           	        	
	           	         
	           	   
	                 var text = '{"array":' + JSON.stringify($scope.bookingDetails) + '}';
	                 var data = JSON.parse(text);
	                 var sourceid = 1;
	                 $http({
	                     method: 'POST',
	                     data: data,
	                     //data : '{"data":' +JSON.stringify($scope.roomsSelected) + '}',
	                     dataType: 'json',
	                     headers: {
	                         'Content-Type': 'application/json; charset=utf-8'
	                     },
	                     url: "add-booking?sourceId=" + sourceid
	                 }).then(function successCallback(response) {
	                        // var specialRequest = $('#specialRequest').val();
	                         var text = '{"array":' + JSON.stringify($scope.bookingDetails) + '}';

	                         var data = JSON.parse(text);
	                         $http({
	                             method: 'POST',
	                             data: data,
	                             dataType: 'json',
	                             headers: {
	                                 'Content-Type': 'application/json; charset=utf-8'
	                             },
	                             url: 'add-booking-details?specialRequest='+"Nil"
	                             //url : 'jsonTest'
	                         }).then(function successCallback(response) {
	                        	 //$(this).removeAttr('disabled');
	                             //$('#loader').hide();
	                             $scope.booked = response.data;
	                             console.log($scope.booked.data[0].bookingId);
	                             window.location = '/paymentsuccess';
	                             $scope.smsapply = "Verify";

	                         }, function errorCallback(response) {
	                        	 });
	                         
	           				
	                     }, function errorCallback(response) {
	                    	 
	                     });

	                 }, function errorCallback(response) {
	                	 $scope.smsapply = "Verify";
	                	 $('#disablesms').prop('disabled',false);
	             	    //$("#loading-example-btn").prop('disabled', true)
	           	   // $scope.searchButtonSmsText = "APPLY";
	                 var alert = ' <div class="alert alert-danger alert-dismissible" style="color:red;font-size:12px;">Please Enter Valid OTP</div>';
	                 $('#smsapplyerror').html(alert);
	            
	               	  
	               	  
	               	  
	                 });
	           	
	           	    
	           	   };
	            
	             
	             $scope.getRewardDetails = function() {
	 				
	 				var url = "get-reward-details";
	 				$http.get(url).success(function(response) {
	 					$scope.rewardDetails = response.data;
	 		
	 				});
	 		   };
	            
	            
	            $scope.removeUloReward = function(){
	                var element = $('#userRewardPoints').val()
	                //element.classList.add("active");
	                document.getElementById('droominfo').style.display = "block";
	                for(var i=0;i<$scope.bookingDetails.length;i++){
	                var total = parseInt($scope.bookingDetails[i].total);
	            	var tax = parseInt($scope.bookingDetails[i].tax);
	            	var taxpercent = $scope.bookingDetails[i].taxPercentage;
	            	var finalAmount = parseInt(total) + parseInt(element);
	            	var finalTax = parseInt(finalAmount * taxpercent / 100);
	            	var finalTotalAmount = Math.round(finalAmount + finalTax);
	            	$scope.bookingDetails[i].total = finalAmount;
	            	$scope.bookingDetails[i].tax = finalTax;
            		$scope.bookingDetails[i].usedRewardPoints = 0; 
            		$scope.bookingDetails[i].dropedRewardPrice = 0;
	                }
	            }
	            
	            $scope.applyRewardPoints = function(){
	               
	            	var rewardPoint = $('#userRewardPoints').val();
	            	for(var i=0;i<$scope.bookingDetails.length;i++){
	            		
	            		var total = $scope.bookingDetails[i].total;
		            	var tax = $scope.bookingDetails[i].tax;
		            	var taxpercent = $scope.bookingDetails[i].taxPercentage;
		            	var finalAmount = parseInt(total - rewardPoint);
		            	var finalTax = parseInt(finalAmount * taxpercent / 100);
		            	var finalTotalAmount = parseInt(finalAmount + finalTax);
		            	$scope.bookingDetails[i].total = finalAmount;
		            	$scope.bookingDetails[i].tax = finalTax;
	            		$scope.bookingDetails[i].usedRewardPoints = rewardPoint; 
	            		$scope.bookingDetails[i].dropedRewardPrice = rewardPoint;
	            	}
	              
	            }
	            
	            
	            $scope.getUser = function() {
	            	var id = $scope.UserIds;
	            	
	            	var id1 = $('#userId').val();
	            	if(id !=null){
	            		$scope.UserIds = id;
	            	}
	            	else if(id1 != null){
	            		$scope.UserIds = id1;
	            	}
	               
	            	var url = "get-customer?auserId=" + $scope.UserIds;
	                $http.get(url).success(function(response) {
	                    $scope.user = response.data;
	                    $scope.username = $scope.user[0].o_username;
	                    $scope.userphone = $scope.user[0].o_phone;
	                    $scope.useremail = $scope.user[0].o_email;
	        	
	                });
	            };
	 
	    $scope.getProperty = function() {
	    	 var checkedPropertyId;
	       	if(propertyId!=null && propertyId!=""){
	       		checkedPropertyId=propertyId;
	       	}else{
	       		checkedPropertyId=urlPropertyId;
	       	}

	       	var url = "get-ulo-front-property?propertyId=" + checkedPropertyId;
	        $http.get(url).success(function(response) {
	            //console.log(response);
	            $scope.property = response.data;
	            $scope.placeId = $scope.property[0].placeId;
	            console.log($scope.property);

	        });
	    };
	    $scope.getAccommodations = function() {
	    	 var checkedPropertyId;
	      	if(propertyId!=null && propertyId!=""){
	      		checkedPropertyId=propertyId;
	      	}else{
	      		checkedPropertyId=urlPropertyId;
	      	}
	        var url = "get-front-accommodations?propertyId=" + checkedPropertyId;
	        $http.get(url).success(function(response) {
	            //console.log(response);
	            $scope.accommodations = response.data;


	        });
	    };
	    $scope.viewTaxbox = function(){
	        var x = document.getElementById("dfinaltaxbox");
	        if (x.style.display === "none") {
	            x.style.display = "block";
	        } else {
	            x.style.display = "none";
	        }
	    };
	    $scope.mviewTaxbox = function(){
	        var x = document.getElementById("dfinaltaxboxmobile");
	        if (x.style.display === "none") {
	            x.style.display = "block";
	        } else {
	            x.style.display = "none";
	        }
	    };
	    $scope.getPropertyPhotos = function() {
	    	 var checkedPropertyId;
	     	if(propertyId!=null && propertyId!=""){
	     		checkedPropertyId=propertyId;
	     	}else{
	     		checkedPropertyId=urlPropertyId;
	     	}
	        var url = "get-ulo-property-photos?propertyId=" + checkedPropertyId;
	        $http.get(url).success(function(response) {
	            $scope.photos = response.data;

	        });
	    };
	    
	    $scope.getCommonPropertyPhotos = function() {
	    	
	        var propertyId = $('#propertyId').val();
	       
	        var url = "get-ulo-common-property-photos?propertyId=" + propertyId;
	        $http.get(url).success(function(response) {
	            $scope.commonPhotos = response.data;
	           
	        });
	    };
	    
	    
	    $scope.getAccommodationPropertyPhotos = function() {
	    	
	 	  var propertyId = $('#propertyId').val();
	       var url = "get-ulo-accomm-property-photos?propertyId=" + propertyId;
	       $http.get(url).success(function(response) {
	           $scope.accommPhotos = response.data;
	           
	          
	       });
	   };
	   
	    
	    $(".dropdown-menu li a").click(function(){
	    	  var selText = $(this).text();
	    	  $(this).parents('.btn-group').find('.dropdown-toggle').html(selText+' <span class="caret"></span>');
	    	});
	    $scope.getscrollphotosfixed =[{"Name":"Common places",
            "id":"1",
             "photos":[
                        {
                     	   "image": "http://lorempixel.com/400/200/"
                        },
                        {
                      	   "image": "http://lorempixel.com/400/200/"
                         },
                         {
                       	   "image": "http://lorempixel.com/400/200/"
                          },
                          {
                        	   "image": "http://lorempixel.com/400/200/"
                           }
                        ]
             }];
$scope.getCommonPropertyPhotos = function() {
	    	
	        var propertyId = $('#propertyId').val();
	       
	        var url = "get-ulo-common-property-photos?propertyId=" + propertyId;
	        $http.get(url).success(function(response) {
	            //console.log(response);
	            $scope.commonPhotos = response.data;
	           
	        });
	    };
	    
	    
	    $scope.getAccommodationPropertyPhotos = function() {
	    	
	 	  var propertyId = $('#propertyId').val();
	       var url = "get-ulo-accomm-property-photos?propertyId=" + propertyId;
	       $http.get(url).success(function(response) {
	           //console.log(response);
	           $scope.accommPhotos = response.data;
	          
	       });
	   };
	   
	   $scope.getPropertyLandmark = function() {
		       var propertyId = $('#propertyId').val();
		       var urlPropertyId = $('#urlPropertyId').val();
		        // url : "add-booking?sourceId="+sourceid
		    	var checkedPropertyId;
		    	if(propertyId!=null && propertyId!=""){
		    		checkedPropertyId=propertyId;
		    	}else{
		    		checkedPropertyId=urlPropertyId;
		    	}		
		        var url = "get-property-landmark?propertyId=" + checkedPropertyId
		        $http.get(url).success(function(response) {
		            $scope.propLandmark = response.data;

		        });
		    };
		    
		$scope.reviewscrollbtn = function(){
		
		    $('html,body').animate({
		        scrollTop: $("#mtotalreviews").offset().top},
		        'slow');
		    
		};  
		
		$scope.reviewscrollbtn = function(){
			
		    $('html,body').animate({
		        scrollTop: $("#mtotalreviews").offset().top},
		        'slow');
		    
		};
	
	   	 $(".datepicker").datepicker({
	            //dateFormat: 'dd M DD',
				minDate: 0,
				numberOfMonths:2,
				autoclose: false,
	            beforeShowDay: function(date) {
				var date1 = $.datepicker.parseDate($.datepicker._defaults.dateFormat, $("#input1").val());
			    var date2 = $.datepicker.parseDate($.datepicker._defaults.dateFormat, $("#input2").val());
				return [true, date1 && ((date.getTime() == date1.getTime()) || (date2 && date >= date1 && date <= date2)) ? "dp-highlight" : ""]; 
				}, 
				onSelect: function(dateText, inst) {
				    
					var date1 = $.datepicker.parseDate($.datepicker._defaults.dateFormat, $("#input1").val());
					var date2 = $.datepicker.parseDate($.datepicker._defaults.dateFormat, $("#input2").val());
	                var dt1 = new Date(dateText);
				    var dd = dt1.getDate();
	                var mm = dt1.getMonth();
					var yy = dt1.getFullYear();
	                var dayName = dt1.getDay();
					
					
					
					if (!date1 || date2) {
					    
						
						checkIn = monthNames[mm] + ' ' + dd + '' + weekDays[dayName];
						alternate = monthNames[mm] + ' ' + dd + ',' + yy;
						stDate = dateText;
						
						$("#input1").val(dateText);
						$("#datepicker0").val(checkIn);
						$("#alternate").val(alternate);
						$("#input2").val("");
 	//var selectedDate = new Date(dateText);
						
	   					//var msecsInADay = 86400000;
	   			      //var endDate = new Date(selectedDate.getTime() + msecsInADay);
	   		
                    //$(".datepicker").datepicker( "option", "minDate", endDate );
	                    $(this).datepicker();
					} else {

   					    checkOut = monthNames[mm] + ' ' + dd + '' + weekDays[dayName];
   						alternate1 = monthNames[mm] + ' ' + dd + ',' + yy;
                        
   						if ((Date.parse(dateText) < Date.parse(stDate))){
   							
   							
   							$("#input2").val(stDate);
   	   						$("#datepicker1").val(checkIn);
   	   						$("#alternate1").val(alternate);
   							
   							$("#input1").val(dateText);
   	   						$("#datepicker0").val(checkOut);
   	   						$("#alternate").val(alternate1);
   	   						
   	   	                    
   	   						
   	   						$(this).datepicker();
   	   						$('.datepicker').hide('slow');
   	   					$scope.getProperties();	
   	   						
   							
   						}
   						
   						else if((Date.parse(dateText) == Date.parse(stDate))){
   							
   							var nextDay = new Date(dateText);
   							nextDay.setDate(nextDay.getDate()+ 1);
   							var nextDD = nextDay.getDate();
   		   	                var nextMM = nextDay.getMonth();
   		   					var nextYY = nextDay.getFullYear();
   		   	                var nextName = nextDay.getDay();
   							
   		   	                checkOut = monthNames[nextMM] + ' ' + nextDD + '' + weekDays[nextName];
 						    alternate1 = monthNames[nextMM] + ' ' + nextDD + ',' + nextYY;
 						    var input2 =  nextMM+1 + '/' + nextDD + '/' + nextYY;
 						   
   							$("#input2").val(input2);
   	   						$("#datepicker1").val(checkOut);
   	   						$("#alternate1").val(alternate1);
   	   						
   	   					    $(this).datepicker();
	   						$('.datepicker').hide('slow');
	   						$scope.getProperties();	
   						}
   						
   						else {
   							
   							$("#input2").val(dateText);
   	   						$("#datepicker1").val(checkOut);
   	   						$("#alternate1").val(alternate1);
   	   						
   	   	                    $(this).datepicker();
   	   						$('.datepicker').hide('slow');
   	   					$scope.StartDate=document.getElementById("alternate").value;
   			 	      	$scope.EndDate=document.getElementById("alternate1").value;
   	   					$scope.getProperties();	
   						}
						
						
					}
					
				}
			});
	$(".mbdatepicker").datepicker({
        //dateFormat: 'dd M DD',
		minDate: 0,
		numberOfMonths:1,
		autoclose: false,
        beforeShowDay: function(date) {
		var date1 = $.datepicker.parseDate($.datepicker._defaults.dateFormat, $("#minput1").val());
	    var date2 = $.datepicker.parseDate($.datepicker._defaults.dateFormat, $("#minput2").val());
		return [true, date1 && ((date.getTime() == date1.getTime()) || (date2 && date >= date1 && date <= date2)) ? "dp-highlight" : ""]; 
		}, 
		onSelect: function(dateText, inst) {
		    
			var date1 = $.datepicker.parseDate($.datepicker._defaults.dateFormat, $("#minput1").val());
			var date2 = $.datepicker.parseDate($.datepicker._defaults.dateFormat, $("#minput2").val());
            var dt1 = new Date(dateText);
		    var dd = dt1.getDate();
            var mm = dt1.getMonth();
			var yy = dt1.getFullYear();
            var dayName = dt1.getDay();
			
			
			
			if (!date1 || date2) {
			    
				
				checkIn = monthNames[mm] + ' ' + dd + '' + mbweekDays[dayName];
				malternate = monthNames[mm] + ' ' + dd + ',' + yy;
				stDate = dateText;
				
				
				$("#minput1").val(dateText);
				$("#mdatepicker0").val(checkIn);
				$("#malternate").val(malternate);
				$("#minput2").val("");
			
		
            $(".mbdatepicker").datepicker( "option", "minDate", endDate );
                $(this).datepicker();
			} else {
				if ((Date.parse(dateText) < Date.parse(stDate))){
					
					   checkOut = monthNames[mm] + ' ' + dd + '' + mbweekDays[dayName];
		   			   malternate1 = monthNames[mm] + ' ' + dd + ',' + yy;
						
						$("#minput2").val(stDate);
						$("#mdatepicker1").val(checkIn);
						$("#malternate1").val(malternate);
						
						$("#minput1").val(dateText);
						$("#mdatepicker0").val(checkOut);
						$("#malternate").val(malternate1);
						
	                    
						
						$(this).datepicker();
						$('.mbdatepicker').hide('slow');
						$scope.getMbProperties();	
						
						
					}
					
					else if((Date.parse(dateText) == Date.parse(stDate))){
						//checkOut = monthNames[mm] + ' ' + dd + '' + mbweekDays[dayName];
			   			//malternate1 = monthNames[mm] + ' ' + dd + ',' + yy;
			   			
			   			var nextDay = new Date(dateText);
							nextDay.setDate(nextDay.getDate()+ 1);
							var nextDD = nextDay.getDate();
		   	                var nextMM = nextDay.getMonth();
		   					var nextYY = nextDay.getFullYear();
		   	                var nextName = nextDay.getDay();
							
		   	                checkOut = monthNames[nextMM] + ' ' + nextDD + '' + weekDays[nextName];
						    malternate1 = monthNames[nextMM] + ' ' + nextDD + ',' + nextYY;
						    var input2 =  nextMM+1 + '/' + nextDD + '/' + nextYY;
						   
							$("#minput2").val(input2);
	   						$("#mdatepicker1").val(checkOut);
	   						$("#malternate1").val(malternate1);
	   						
	   					    $(this).datepicker();
						    $('.mbdatepicker').hide('slow');
						    $scope.getMbProperties();	
						
						
					  
						
					}
					
					else {
						
						checkOut = monthNames[mm] + ' ' + dd + '' + mbweekDays[dayName];
			   			malternate1 = monthNames[mm] + ' ' + dd + ',' + yy;
						$("#minput2").val(dateText);
						$("#mdatepicker1").val(checkOut);
						$("#malternate1").val(malternate1);
						
	                    $(this).datepicker();
						$('.mbdatepicker').hide('slow');
						$scope.getMbProperties();	
						
					}
			  /*   checkOut = monthNames[mm] + ' ' + dd + '' + mbweekDays[dayName];
			    malternate1 = monthNames[mm] + ' ' + dd + ',' + yy;
				$("#minput2").val(dateText);
				$("#mdatepicker1").val(checkOut);
				$("#malternate1").val(malternate1);
                $(this).datepicker();
				$('.mbdatepicker').hide('slow'); */
				/* 
			    checkOut = monthNames[mm] + ' ' + dd + '' + mbweekDays[dayName];
			    malternate1 = monthNames[mm] + ' ' + dd + ',' + yy;
				$("#minput2").val(dateText);
				$("#mdatepicker1").val(checkOut);
				$("#malternate1").val(malternate1);
                $(this).datepicker();
				$('.mbdatepicker').hide('slow'); */
				
				
				
			}
			
		}
	});	
	
	    $scope.getPropertyPhotos();
	    $scope.getCurrentDiscounts();
	    $scope.getAllCurrentDiscounts();
	    $scope.getRewardDetails();
	    $scope.getUser();
	    $scope.getPropertyLandmark();
	   // $scope.getProperty(); 	
	    $scope.getAccommodationPropertyPhotos();
	    $scope.getCommonPropertyPhotos();
	    $scope.getProperties();
	    $scope.getMbProperties();
// 	    $scope.getCurrentDiscounts();
	   // $scope.getFrontAvailability();
});

app.directive('starRating', function () {

    return {
        restrict: 'A',
        template: '<ul class="rating">' +
            '<li ng-repeat="star in stars" ng-class="star">' +
            '\u2605' +
            '</li>' +
            '</ul>',
        scope: {
            ratingValue: '=',
            max: '='
        },
        link: function (scope, elem, attrs) {
            scope.stars = [];
            for (var i = 0; i < scope.max; i++) {
                scope.stars.push({
                    filled: i < scope.ratingValue
                });
            }
        }
        
    }
});
app.directive('starRating2', function () {
    return {
        restrict: 'A',
        template: '<ul class="rating">' +
            '<li ng-repeat="star in stars" ng-class="star">' +
            '<i class="fa fa-dot-circle-o"></i>' +
            '</li>' +
            '</ul>',
        scope: {
            ratingValue: '=',
            max: '='
        },
        link: function (scope, elem, attrs) {
            scope.stars = [];
            for (var i = 0; i < scope.max; i++) {
                scope.stars.push({
                    filled: i < scope.ratingValue
                });
            }
        }
    }
});
</script>
<script>
function latlong() {

    return new google.maps.LatLng( $("#lat").val(), $("#lng").val() );

}
function initMap() {
     var lat = document.getElementById("lat").value;
     var lng = document.getElementById("lng").value;
      var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 6,
      center: latlong()
    });
      var map2 = new google.maps.Map(document.getElementById('mobilemap'), {
          zoom: 6,
          center: latlong()
        });
}

</script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDg9dqRsKwRWXGdTKsUNHTHhfQqdSyip9k&callback=initMap"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-hashchange/1.3/jquery.ba-hashchange.min.js"></script>
<script>
var detectBackOrForward = function(onBack, onForward) {
	  hashHistory = [window.location.hash];
	  historyLength = window.history.length;

	  return function() {
	    var hash = window.location.hash, length = window.history.length;
	    if (hashHistory.length && historyLength == length) {
	      if (hashHistory[hashHistory.length - 2] == hash) {
	        hashHistory = hashHistory.slice(0, -1);
	        onBack();
	      } else {
	        hashHistory.push(hash);
	        onForward();
	      }
	    } else {
	      hashHistory.push(hash);
	      historyLength = length;
	    }
	  }
	};

	window.addEventListener("hashchange", detectBackOrForward(
	  function() { console.log("back")
		  document.getElementById("dpaymentpage").style.display="none"; 
	  	document.getElementById("desktopdetailpage").style.display="block";   
	  	document.getElementById("mpaymentpage").style.display="none"; 
	  	document.getElementById("mobiledetailpage").style.display="block";
	  },
	  function() { console.log("forward") 

	  }
	));
</script>
<script>
$(window).bind( 'hashchange', function(){
	
    var hash = location.hash;
    
    $('.dnav').each(function(event){
   
        var that = $(this);
        event.preventDefault();
    });
});
  
$(window).trigger( 'hashchange' );
</script>
<script>
    $( "#datepicker0" ).focus(function() {
   	  $('.datepicker').show('slow');
   	});
   $( "#mdatepicker0" ).focus(function() {
   	  $('.mbdatepicker').show('slow');
   	});
  /*  checkout module */
     $( "#datepicker1" ).focus(function() {
   	  $('.datepicker').show('slow');
   	});
   $( "#mdatepicker1" ).focus(function() {
   	  $('.mbdatepicker').show('slow');
   	});
   	 var monthNames = [
   	              "Jan", "Feb", "Mar",
   	              "Apr", "May", "Jun", "Jul",
   	              "Aug", "Sep", "Oct",
   	              "Nov", "Dec"
   	          ];
   			  
   	 var weekDays = [
   	                " Sunday", " Monday", " Tuesday",
   	                " Wednesday", " Thursday", " Friday", " Saturday"
   	                ];
   	 
   	var mbweekDays = [
   	                " Sun", " Mon", " Tue",
   	                " Wed", " Thu", " Fri", " Sat"
   	                ];
   	var monthNos = [01,02,03,04,05,06,07,08,09,10,11,12];
   							
   	 var today = new Date();
   	 var toda_dd = today.getDate();
   	 var toda_mm = today.getMonth();
   	 var toda_yy = today.getFullYear();
   	 var toda_day = today.getDay();
   	 
   	 
   	 
   	 var tomorrow =  new Date();
   	 tomorrow.setDate(tomorrow.getDate() + 1);
   	 var tomo_dd = tomorrow.getDate();
   	 var tomo_mm = tomorrow.getMonth();
   	 var tomo_yy = tomorrow.getFullYear();
   	 var tomo_day = tomorrow.getDay();
   	 
   	 
   	/*var str1  = monthNos[toda_mm] + '/' + toda_dd + '/' + toda_yy;
   	var str2 =  monthNos[tomo_mm] + '/' + tomo_dd + '/' + tomo_yy;
   	$('#input1').val(str1);
   	$('#input2').val(str2);*/
   	 
   	 
     if($('#datepicker0').val() == ''){
    	 
   	 $('#input1').val(monthNos[toda_mm] + '/' + toda_dd + '/' + toda_yy);	
   	 $('#input2').val(monthNos[tomo_mm] + '/' + tomo_dd + '/' + tomo_yy);
   	 $('#datepicker0').val(monthNames[toda_mm] + ' ' + toda_dd + '' + weekDays[toda_day]);	
   	 $('#datepicker1').val(monthNames[tomo_mm] + ' ' + tomo_dd + '' + weekDays[tomo_day]);
   	 $('#alternate').val(monthNames[toda_mm] + ' ' + toda_dd + ',' + toda_yy);
   	 $('#alternate1').val(monthNames[tomo_mm] + ' ' + tomo_dd + ',' + tomo_yy); 
   	 
   	 $('#minput1').val(monthNos[toda_mm] + '/' + toda_dd + '/' + toda_yy);	
   	 $('#minput2').val(monthNos[tomo_mm] + '/' + tomo_dd + '/' + tomo_yy);
   	 $('#mdatepicker0').val(monthNames[toda_mm] + ' ' + toda_dd + '' + mbweekDays[toda_day]);	
   	 $('#mdatepicker1').val(monthNames[tomo_mm] + ' ' + tomo_dd + '' + mbweekDays[tomo_day]);
   	 $('#malternate').val(monthNames[toda_mm] + ' ' + toda_dd + ',' + toda_yy);
   	 $('#malternate1').val(monthNames[tomo_mm] + ' ' + tomo_dd + ',' + tomo_yy); 
   	 
     }
   	


       
    </script>
<%--     <script>
    $("body").click(function (event) {
    	$('.mbdatepicker').hide('slow');
    	$('.datepicker').hide('slow');
    	});
    </script> --%>

<style>
  .datepicker{
      display:none;
      z-index:2;
        cursor: pointer; /* Add a pointer on hover */
    position:absolute;
    left:-90%;
      }
      .mbdatepicker{
      display:none;
           z-index:2;
        cursor: pointer; /* Add a pointer on hover */
    position:absolute;
top:-500%;
left:0%;
      }
      .dp-highlight .ui-state-default {
      background: #a9e006; 
      color: #FFF;
          text-align:center;
      }
          .dp-highlight {
      background: #a9e006; 
      color: #FFF;
      text-align:center;
      }
      .ui-datepicker.ui-datepicker-multi  {
      width: 50% !important;
      margin: 0px 400px auto;
      display:block;
      }
      .ui-datepicker-multi .ui-datepicker-group {
      float:left;
      }
      #datepicker {
      height: 300px;
      overflow-x: scroll;
      }
      #mbdatepicker {
      height: 300px;
      overflow-x: scroll;
      }
      h4 {
    margin: 20px 10px 10px;
}
p {
    margin: 10px;
}

#carousel-example-generic {
    margin: 20px auto;

}
#carousel-custom {
    margin: 0px auto;

}
#carousel-custom .carousel-indicators {
    margin: 10px 0 0;
    overflow: auto;
    position: static;
    text-align: left;
    white-space: nowrap;
    width: 100%;
}
#carousel-custom .carousel-indicators li {
    background-color: transparent;
    -webkit-border-radius: 0;
    border-radius: 0;
    display: inline-block;
    height: auto;
    margin: 0 !important;
    width: auto;
}
#carousel-custom .carousel-indicators li img {
    display: block;
    opacity: 0.5;
    height:60px;
    width:60px;
}
#carousel-custom .carousel-indicators li.active img {
    opacity: 1;
}
#carousel-custom .carousel-indicators li:hover img {
    opacity: 0.75;
}
#carousel-custom .carousel-outer {
    position: relative;
}
#carousel-custom .imageCount{
    background-color: #5d5d5d;
    border: 1px solid #5d5d5d;
    color: #fff;
    padding:5px;
    margin-top: 5px;
    position: absolute;
    bottom: 20px;
    right: 20px;
    font-size: 12px;
    border-radius:100%;
}
#carousel-custom .imageName{
    background-color: #5d5d5d;
    border: 1px solid #5d5d5d;
    color: #fff;
    padding:5px;
    margin-top: 5px;
    position: absolute;
    top: 20px;
    right: 20px;
    font-size: 12px;
}
#mobileimagescroll .modal-dialog {
  width: 100%;
  height: 100%;
  margin: 0;
  padding: 0;
}

#mobile .modal-content {
  height: auto;
  min-height: 100%;
  border-radius: 0;
}
#mcouponmodal .modal-header .close {
    margin-top: 3px;
    margin-right:3px;
}
     @media (min-width: 320px) and (max-width: 767px) {
      .middle-header{
      display:none;
      }
      }
      

 </style>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
