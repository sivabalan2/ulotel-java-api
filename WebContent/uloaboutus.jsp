<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
   pageEncoding="ISO-8859-1"%>
<section id="innerPageContent">
   <!-- Tab Header -->
   <!-- Tab Header -->
   <div class="container tabBG">
      <div class="row">
         <div class="col-sm-12">
            <!-- Overview Tab -->
            <div class="tabContent" id="overviewTab" style="display:block;">
               <h1 class="heading1">Our <span>Story</span></h1>
               <div class="textContent">
                  <p>Ulo Hotels is a New Generation Quality budget Hotel chain. Ulo Hotels get its name from “Ulo”, which denotes accommodation in the Igbo language. We chose this name because we wanted to provide quality budget accommodations for all our guests.</p>
                  <p>We are a tech-enabled company focused on offering great Customer experience in Budget Stays. We in Ulo Hotels have dedicated teams for Reservation, Revenue, Digital Marketing, Contracts, Technology, Quality, and Accounts. All our hotels follow meticulous protocols, monthly quality audits and have a well-trained staff.</p>
                  <p>We operate through the Joint Business Venture Model. We will assist day-to-day operations, enhance quality, boost the internet presence and employ modern technology in all our portfolio properties. We will work on increasing the Occupancy Rate, Revenue, and Quality.</p>
                  <p>We make sure that every guest opting Ulo Hotels is satisfied with our service. Our unique, quality 5Bs services create repetitive customers for Ulo.</p>
                  <div class="separator"></div>
                  <h6>WHY ULO</h6>
                  <div class="listCon">
                     <ul>
                        <li>
                           <p>Budget-friendly Accommodations</p>
                        </li>
                        <li>
                           <p>Top-notch amenities &services</p>
                        </li>
                        <li>
                           <p>Value For Money</p>
                        </li>
                        <li>
                           <p>Amicable Staff</p>
                        </li>
                        <li>
                           <p>Real-time Inventory Management</p>
                        </li>
                        <li>
                           <p>Price Strategy (Based on Trend)</p>
                        </li>
                        <li>
                           <p>Effective Internet Presence</p>
                        </li>
                        <li>
                           <p>YOY (Year on Year) Revenue Growth</p>
                        </li>
                        <li>
                           <p>Review Management</p>
                        </li>
                     </ul>
                  </div>
                  <div class="separator"></div>
                  <h6>COMPETITIVE ADVANTAGES</h6>
                  <p>We ensure all our portfolio properties are maintained to international standards in terms of hygiene, amenities, and services. We assure all our partners,</p>
                  <div class="listCon">
                     <ul>
                        <li>
                           <p>Guaranteed Occupancy</p>
                        </li>
                        <li>
                           <p>Assured Revenue</p>
                        </li>
                        <li>
                           <p>Professional Highly-trained Staff</p>
                        </li>
                        <li>
                           <p>Quality Maintenance</p>
                        </li>
                        <li>
                           <p>Constant Review Management</p>
                        </li>
                     </ul>
                  </div>
                  <div class="separator"></div>
                  <h6>UNIQUENESS</h6>
                  <div class="listCon">
                     <p>We have employed our own Property Management System (PMS) which offers benefits like,</p>
                     <ul>
                        <li>
                           <p>Real-time Booking</p>
                        </li>
                        <li>
                           <p>Instant Revenue Reconciliation</p>
                        </li>
                        <li>
                           <p>Detailed Revenue Forecast</p>
                        </li>
                        <li>
                           <p>Integration with Online/Offline Partners</p>
                        </li>
                        <li>
                           <p>Comprehensive Dashboard and Reporting </p>
                        </li>
                     </ul>
                     <p>We guarantee a growth of over 60% in revenue and occupancy rate through our business model. Our vision is to expand Ulo hotels to Pan India.</p>
                     <p>Give us a try when you plan for a vacation and save money. You surely won’t regret your decision.</p>
                  </div>
               </div>
            </div>
            <!-- //Overview Tab -->
         </div>
      </div>
   </div>
</section>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script src="js1/ngprogress.js" type="text/javascript"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script> 
<script>
   var app = angular.module('myApp', ['ngProgress']);
   app.controller('customersCtrl', function ($scope, $http, $timeout, ngProgressFactory) {
    $scope.userData = [];
   
    	$scope.login = function () {
   
   
    		var fdata = "username=" + $('#username').val() +
    			"&password=" + $('#password').val();
    		//alert(fdata);
   
    		$http({
    			method: 'POST',
    			data: fdata,
    			headers: {
    				'Content-Type': 'application/x-www-form-urlencoded'
    			},
    			url: 'ulologin'
   
    		}).then(function successCallback(response) {
    			$scope.userData = response.data;
    			console.log($scope.userData);
    			var username = $scope.userData.data[0].userName;
    			var email = $scope.userData.data[0].emailId;
    			var phone = $scope.userData.data[0].phone;
   
    			$('#loginModal').modal('toggle');
    			$('#logSec').html("<div class='mnuContant'><a href='mailto:support@ulohotels.com' title='support@ulohotels.com'><i class='fa fa-envelope'></i><span class='visibleLG'>support@ulohotels.com</span></a><a href='tel:9543592593' title=''+91 95435 92593'><i class='fa fa-phone'></i><span class='visibleLG'>+91 95435 92593</span></a></div><div class='mnuProfile'><div class='mpAvatar hidden-xs' style='background:url(ulowebsite/images/avatar.png);'></div><div class='mpUsername'><a href='javascript:showpro();' class=' mnuProfileOpen'>Hi admin<i class='fa fa-angle-down'></i></a></div><div class='mpSubMenu' id='showmenu' ><ul><li><a href='ulouserprofile'>Profile</a></li><li><a href='ulouserprofile'>Booking History</a></li><li><a href='ulologout'>Log out</a></li></ul></div></div>");
    		}, function errorCallback(response) {
   
    			var alert = ' <div class="alert alert-danger alert-dismissible" style="color:red">Invalid username or password.</div>';
    			$('#loginerror').html(alert);
    		    $(function() {
    	          // setTimeout() function will be fired after page is loaded
    	          // it will wait for 5 sec. and then will fire
    	          // $("#successMessage").hide() function
    	          setTimeout(function() {
    	              $("#loginerror").hide('blind', {}, 100)
    	          }, 5000);
    	      });
    		});
   
    	};
   
    	$scope.signup = function () {
   
   
    		var fdata = "userName=" + $('#userName').val() +
    			"&emailId=" + $('#emailId').val() +
    			"&roleId=" + $('#roleId').val() +
    			"&accessRightsId=" + $('#accessRightsId').val() +
    			"&password=" + $('#signpassword').val();
    		//alert(fdata);
   
    		$http(
   
    			{
    				method: 'POST',
    				data: fdata,
    				headers: {
    					'Content-Type': 'application/x-www-form-urlencoded'
    				},
    				url: 'ulosignup'
    			}).then(function successCallback(response) {
   
   
    			if (response.data == "") {
   
    				var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:red">Sorry This Email Id already exist.</div>';
    				$('#signuperror').html(alertmsg);
   
   
    			} else {
   
    				$scope.userData = response.data;
    				var username = $scope.userData.data[0].userName;
    				var email = $scope.userData.data[0].emailId;
    				var phone = $scope.userData.data[0].phone;
   
   
    				$('#registerModal').modal('toggle');
   
    				$('#logSec').html("<div class='mnuContant'><a href='mailto:support@ulohotels.com' title='support@ulohotels.com'><i class='fa fa-envelope'></i><span class='visibleLG'>support@ulohotels.com</span></a><a href='tel:9543592593' title=''+91 95435 92593'><i class='fa fa-phone'></i><span class='visibleLG'>+91 95435 92593</span></a></div><div class='mnuProfile'><div class='mpAvatar hidden-xs' style='background:url(ulowebsite/images/avatar.png);'></div><div class='mpUsername'><a href='javascript:showpro();' class=' mnuProfileOpen'>Hi admin<i class='fa fa-angle-down'></i></a></div><div class='mpSubMenu' id='showmenu' ><ul><li><a href='ulouserprofile'>Profile</a></li><li><a href='ulouserprofile'>Booking History</a></li><li><a href='ulologout'>Log out</a></li></ul></div></div>");
    			}
   
    		}, function errorCallback(response) {
   
   
    			var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:red">This Email ID Already Exist</div>';
    			$('#signuperror').html(alertmsg);
   
    			// or server returns response with an error status.
    		});
   
    	};
   
   
    	$scope.passwordRequest = function () {
   
   
    		var fdata = "emailId=" + $('#forgetEmailId').val();
   
   
    		$http({
    			method: 'POST',
    			data: fdata,
    			headers: {
    				'Content-Type': 'application/x-www-form-urlencoded'
    			},
    			url: 'ulosendpassword'
    		}).then(function successCallback(response) {
   
    			var alertmsg = ' <div class="alert alert-success alert-dismissible" style="color:red">Password Sent Sucessfully.</div>';
   
    			$('#forgetmessage').html(alertmsg);
   
   
    		}, function errorCallback(response) {
   
   
    			var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:red">Your Email ID does not Registred Yet</div>';
    			$('#forgetmessage').html(alertmsg);
   
   
    		});
   
    	};
   
    	$('#forgetPanel').hide();
    	$scope.showForget = function () {
   
    		$('#loginPanel').hide();
    		$('#forgetPanel').show();
   
   
    	};
   
    	$scope.showLogin = function () {
   
    		$('#forgetPanel').hide();
    		$('#loginPanel').show();
   
   
    	};
   
   $timeout(function () {
     	$('#btnclose').click();
     }, 1000);
     $timeout(function () {
     	$('#btncloses').click();
     }, 1000);
   
   });
   
   
</script>