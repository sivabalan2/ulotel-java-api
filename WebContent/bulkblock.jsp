
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="s" uri="/struts-tags"%>
<style>
.btnpd{margin:5px;}
/* .avabg{background-color:#fff;padding:5px;} */
.invcal{background-color:#069;color:#fff;padding:5px;border:none;}
.rmname{color:#fff;}
.box-titles{display: inline-block; font-size: 18px; margin: 0; }
.box-title {color:#fff;}
.totalrooms{background: #88ba41 !important; border-color: #5cb85c !important;text-align:center;width:50%;padding:5px;float:left;}
.soldtext{background-color: #d9534f; border-color: #d43f3a;text-align:center;width:50%;padding:5px;float:right;}
</style>
   <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <section class="content-header">
          <h1>
           Room Inventory
            <!--<small>Subscription Expiry on {{property[0].subscription[0].expirydate}}</small>-->
          </h1>
          <ol class="breadcrumb">
            <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Room Inventory</li>
          </ol>
        </section>
 		
 		<!-- Main content -->
    <section class="content">
    <div class="box box-default color-palette-box">
        <div class="box-header ">
          <h3 class="box-titles"><i class="fa fa-tag"></i> Room Update</h3>
        </div>
        <div class="box-body">
                <form name="searchRate">
            <div class="col-sm-4 col-md-4">
               <div class="form-group">
                  <label>Select Accommodation Type :</label>
                  <select name="propertyAccommodationId" id="propertyAccommodationId" value="" ng-model="propertyAccommodationId" class="form-control" ng-required="true">
                     <option value="">Choose Accommodation</option>
                     <option ng-repeat="at in accommodations" value="{{at.accommodationId}}">{{at.accommodationType}}</option>
                  </select>
               </div>
            </div>
            <div class="col-sm-4 col-md-4">
               <div class=" form-group">
                  <label>Start Date :</label>
                  <div class="input-group date">
                     <label class="input-group-addon btn" for="startDate">
                     <span class="fa fa-calendar"></span>
                     </label>   
                     <input type="text"  id="StartDate" class="form-control" name="StartDate"  value="" ng-required="true" autocomplete="off">
                  </div>
               </div>
            </div>
            <div class="col-sm-4 col-md-4">
               <div class="form-group">
                  <label >End Date :</label>
                  <div class="input-group date">
                     <label class="input-group-addon btn" for="endDate">
                     <span class="fa fa-calendar"></span>
                     </label>   
                     <input type="text" id="endDate" class="form-control" name="endDate" value="" ng-required="true" autocomplete="off"> 
                  </div>
               </div>
            </div>
             <div class="col-sm-12 col-md-12">
               <button type="submit" ng-click="getBulkBlockInventory()"  class="btn btn-primary btngreeen pull-right btnpd"  >Search</button> <!-- ng-disabled="searchRate.$invalid" -->
               <a class="btn btn-danger pull-right btnpd" href="#bulkupdate" ng-click=""  data-toggle="modal"  >Bulk Update</a>
             </div>
        		<div class="col-sm-12 col-md-12">
               		<span id="roompromoalert"></span>
               </div>
         </form>
        </div>
        <!-- /.box-body -->
              
      
      <div class="row avabg">
        <div class="col-xs-12 ">
          <div class="box invcal">
            <div class="box-header">
              <h3 class="box-title">Available Inventory</h3>

              
            </div>
      <div class="row">
         <div class="col-md-12 " >
                  <div class="row ">
               <div class="col-md-6 text-center"><button class="btn-primary" ng-click="datePrevious()">Previous 10 Days {{roomCount.roomName }}</button></div>
               <div class="col-md-6 text-center"><button  class="btn-primary" ng-click="dateNext()">Next 10 Days</button></div>
            </div>
            <div class="table-responsive " ng-repeat="rc in blockinventory |limitTo :1"> 
                              <table width="100%" class="table table-bordered ">
                                    <tbody>
                                            <tr>
									            <td ><span class="font14 roomtype-text">Room Type</span></td>
									             <td ng-repeat="dd in rc.roomAvailable | limitTo:filterLimit track by $index" class="text-center" id="tlastchild{{$index}}"> {{dd.date}}</td>
									        </tr>
                                        <tr  ng-repeat="rc in blockinventory" >
                                            <td >
                                                <div class="pad_left_n col-md-7 col-xs-11">
                                                    <h5 class="text-left rmname">{{ rc.accommodationType }}</h5></div>
                             
                                            </td>
                                            <td ng-repeat="dd in rc.roomAvailable | limitTo:filterLimit track by $index">
											<input type="text" id="roomId{{$index}}" name="roomId{{$index}}" ng-change="singleBlockInventory(dd.available,dd.date,rc.accommodationId,dd.totalRooms,dd.totalAvailable,dd.soldRooms,$index)" ng-model="dd.available"  ng-model-onblur class="form-control avatext"/>
				                            <div class="roomview"><span ng-disabled="true" class="soldtext">{{dd.sold}}</span><span class="totalrooms">{{dd.totalRoomCount}}</span></div>
<!-- 				                            <input type="text" ng-model="dd.occupancy" style="background-color:#05253A ;border-top-color:#05253A ;text-align:center;color:white;width:100%;height:0%" ng-model-onblur class="form-control avatext " readonly/>                                             -->
                                            <%-- <input type="text" ng-change="singleBlockInventory(dd.available,dd.date,rc.accommodationId)" ng-model="dd.available"  ng-model-onblur class="form-control avatext"/>
                                             <div class="roomview"><span ng-disabled="true" class="soldtext">{{dd.sold}}</span><span class="totalrooms">{{dd.totalRoomCount}}</span></div> --%>
                                            <%-- <span class="soldbtn"><button class="btn-danger">{{dd.sold}}</button></span> --%>
                                            </td>
                                          
                                        </tr>
                                   
                                    </tbody>
                                </table>
                                <div class="col-sm-12 col-md-12">
				               		<div class="form-group">
		                              <label>Inventory Remarks</label>
		                              <input type="text" id="singleInventoryRemarks" name="singleInventoryRemarks" class="form-control" ng-required="true"/>
		                           </div>
				               </div>
                               <div class="col-md-6"><button  id="loading-example-btn" class="btn-primary pull-right" ng-click="updateRooms()">Update Inventory</button></div>
                                
            </div>
          </div>
      </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
      
      </div>

    </section>
    
    <div class="modal" id="bulkupdate">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" id="btnclose" class="close" data-dismiss="modal"
							aria-hidden="true">x</button>
						<h4 class="modal-title">Bulk Update</h4>
					</div>
					 <form name="addBlockForm" id="addBlockForm">
					<div class="modal-body" >
                          <div class="form-group col-md-6">
					      <label>Select Accommodation Type :</label>
							<select  name="accommodationId" id="accommodationId" ng-model="accommodationId" value="" class="form-control" ng-required="true">
							<option style="display:none" value="">Select Your  Accommodation</option>
	                       	<option ng-repeat="at in accommodations" value="{{at.accommodationId}}">{{at.accommodationType}}</option>
	                        </select>
						 </div>
						 
						
						 
						<div class=" form-group col-md-6">
							<label>Start Date :</label>
							 <div class="input-group date">
			                   <label class="input-group-addon btn">
			                   <span class="fa fa-calendar"></span>
			                  </label>   
			                  <input type="text" id="bookCheckin" class="form-control" name="bookCheckin" value="" ng-required="true" autocomplete="off">
			 
			                  
			                </div>
							     
						</div>
						
						<div class="form-group col-md-6">
							<label >End Date :</label>
						      <div class="input-group date">
			                   <label class="input-group-addon btn">
			                   <span class="fa fa-calendar"></span>
			             	 </label>   
			                  <input type="text" id="bookCheckout" class="form-control" name="bookCheckout"  value="" ng-required="true" autocomplete="off"> 
			
			                </div>
						     
						</div>
						<div class="form-group col-md-6">
							<label >Inventory Available:</label>
						      <div class="input-group date">
			             
			                  <input type="text" id="blockInventoryCount" class="form-control" name="blockInventoryCount"  value="" ng-required="true" autocomplete="off"> 
			
			                </div>
						     
						</div>
						<div class="col-md-12 col-sm-12">
                           <div class="form-group">
                              <label>Inventory Remarks</label>
                              <input type="text" id="inventoryRemarks" name="inventoryRemarks" class="form-control" ng-required="true" />
                           </div>
                        </div>
			
					<div class="modal-footer">
					<span id="roominventorypromoalert"></span>
<!-- 						<a href="#" data-dismiss="modal" class="btn">Close</a> -->
						<a href="#" data-dismiss="modal" class="btn btn-danger">Close</a>
						<button ng-click="addBlockForm.$valid && blockInventory()" ng-disabled="addBlockForm.$invalid" id="bulkBlock" class="btn btn-primary">Update</button>
					</div>
					</div>
					</form>
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
 		
 		
</div>
   <!-- jQuery 2.2.3 -->
<%-- <script src="https://code.jquery.com/jquery-1.12.4.js"></script> --%>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="plugins/fullcalendar/fullcalendar.min.js"></script>
<!-- Page specific script -->
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>
<script>
   var app = angular.module('myApp', ['ngProgress']);
   	app.controller('customersCtrl', function($scope, $http,$timeout,ngProgressFactory) {
     	$timeout(function(){
         	    // $scope.progressbar.complete();
               $scope.show = true;
               $("#pre-loader").css("display","none");
           }, 2000);
     	
          
          	$scope.datePrevious = function(){
          		
         		 var result = new Date(startDate);
         		result.setDate(result.getDate() - 10);
         		var months = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
         		var month = months[result.getMonth()];
         		var year = result.getFullYear();
         		var dates = month  + " " + result.getDate() + "," + year;

         		var fdata = "&startBlockDate=" + dates
      			+ "&endBlockDate=" + startDate
      			+ "&propertyAccommodationId=" + $('#propertyAccommodationId').val()
             	 	
             	  $http(
               		   {
               			   method : 'POST',
               			   data : fdata,
               			   headers : {
               			   'Content-Type' : 'application/x-www-form-urlencoded'
               			   },
               			 url : 'get-bulk-block-inventory'
               			   }).success(function(response) {
               			   
               			   $scope.blockinventory = response.data;
               			   
                			   
                 			   lastDate = $scope.blockinventory[0].roomAvailable[$scope.blockinventory[0].roomAvailable.length-1].date;
               			   startDate = $scope.blockinventory[0].roomAvailable[0].date;
               			 
               		   
               		   });
          	};
        	$scope.dateNext = function(){
          		 var result = new Date(lastDate);
          		result.setDate(result.getDate() + 10);
          		var months = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
          		var month = months[result.getMonth()];
          		var year = result.getFullYear();
          		var dates = month  + " " + result.getDate() + "," + year;

          		var fdata = "&startBlockDate=" + lastDate
       			+ "&endBlockDate=" + dates
       			+ "&propertyAccommodationId=" + $('#propertyAccommodationId').val()
              	 	
              	  $http(
                		   {
                			   method : 'POST',
                			   data : fdata,
                			   headers : {
                			   'Content-Type' : 'application/x-www-form-urlencoded'
                			   },
                			 url : 'get-bulk-block-inventory'
                			   }).success(function(response) {
                			   
                			   $scope.blockinventory = response.data;
                			   
                 			   
                  			   lastDate = $scope.blockinventory[0].roomAvailable[$scope.blockinventory[0].roomAvailable.length-1].date;
                			   startDate = $scope.blockinventory[0].roomAvailable[0].date;
                			 
                		   
                		   });
          	};
          	
        	/* $scope.singleBlockInventory = function(available,date,id){
          	 
          	  
          	 var newData = {'startBlockDate':date,'inventoryCount':available,'propertyAccommodationId':id};
         	  $scope.roomsInfo.push(newData);
          	}; */
          	
          	$scope.singleBlockInventory = function(available,date,id,totalRooms,totalAvailable,soldRooms,indx){
        		var availRoom=parseInt(available);
        		var totalAvail=parseInt(totalRooms);
        		var soldRoom=parseInt(soldRooms);
        		var availableRooms=totalAvail-soldRoom;
        		if(availRoom<=parseInt(availableRooms)){
        			var newData = {'startBlockDate':date,'inventoryCount':available,'propertyAccommodationId':id};
               	  	$scope.roomsInfo.push(newData);
               	 /* $("#roompromoalert").hide();
				  var alertmsg = ' <div class="alert" style="color:green;text-align:center;">Inventory changed for '+date+'.</div>';
		            $('#roompromoalert').html(alertmsg);
		            $("#roompromoalert").fadeTo(2000, 500).slideUp(500, function(){
		                $("#roompromoalert").slideUp(500);
		            }); */
        		}else if(availRoom>parseInt(availableRooms)){
        			$scope.blockinventory[0].roomAvailable[indx].blockedRooms=totalAvailable;
		            document.getElementById('roomId'+indx).value=totalAvailable;
        			$("#roompromoalert").hide();
					  var alertmsg = ' <div class="alert" style="color:green;text-align:center;">Maximum inventory updated.</div>';
			            $('#roompromoalert').html(alertmsg);
			            $("#roompromoalert").fadeTo(2000, 500).slideUp(500, function(){
			                $("#roompromoalert").slideUp(500);
			            });
			            
        			return;
        		}
          	 
          	};
          	
          	
          	var lastDate = 0;
          	var startDate = 0;
          	
        	$scope.getBulkBlockInventory = function() {
          	 	var fdata = "&startBlockDate=" + $('#StartDate').val()
   			+ "&endBlockDate=" + $('#endDate').val()
   			+ "&propertyAccommodationId=" + $('#propertyAccommodationId').val()
          	 	
          	  $http(
            		   {   
            			   	   
            			   method : 'POST',
            			   data : fdata,            			  
            			   headers : {
            			   'Content-Type' : 'application/x-www-form-urlencoded'
            			   },
            			  url : 'get-bulk-block-inventory'   /*   get-ota-booked-details */
            			   }).success(function(response) {
            			   
            			   $scope.blockinventory = response.data;
            			
             			   
              			   lastDate = $scope.blockinventory[0].roomAvailable[$scope.blockinventory[0].roomAvailable.length-1].date;
            			 	startDate = $scope.blockinventory[0].roomAvailable[0].date;
            		   
            		   });
          		
          	};
          	
          	
          	$scope.blockInventory = function(){
          		
         	   var fdata = "startBlockDate=" + $('#bookCheckin').val()
            	+ "&endBlockDate=" + $('#bookCheckout').val()
         	+ "&inventoryCount=" + $('#blockInventoryCount').val()
            	+ "&propertyAccommodationId=" + $('#accommodationId').val()
            	+"&inventoryRemarks="+$('#inventoryRemarks').val();
         	  	var inventoryRemarks=$('#inventoryRemarks').val();
         	   if(inventoryRemarks.trim().length==0)
     			{
         		  
       			$("#roominventorypromoalert").hide();
				  var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:green;text-align:center;">please check the inventory on '+date+'.</div>';
		            $('#roominventorypromoalert').html(alertmsg);
		            $("#roominventorypromoalert").fadeTo(2000, 500).slideUp(500, function(){
		                $("#roominventorypromoalert").slideUp(500);
		            });
	            
  				return;
     			}
         		   $http(
         		   {
         			   method : 'POST',
         			   data : fdata,
         			   headers : {
         			   'Content-Type' : 'application/x-www-form-urlencoded'
         			   },
         			  url : 'block-room-inventory'
         			   }).success(function(response) {
         			   
         			   $scope.blockrooms = response.data;
         			  $("#roompromoalert").hide();
 					  var alertmsg = ' <div class="alert" style="color:green;text-align:center;">room inventory updated.</div>';
 			            $('#roompromoalert').html(alertmsg);
 			            $("#roompromoalert").fadeTo(2000, 500).slideUp(500, function(){
 			                $("#roompromoalert").slideUp(500);
 			            }); 
         			   
 			           $('#bulkupdate').modal('toggle');
         		   
         			  }).error(function(response) {
         				  
     						$("#roompromoalert").hide();
       					  var alertmsg = ' <div class="alert" style="color:green;text-align:center;">process failed, please try again!! .</div>';
       			            $('#roompromoalert').html(alertmsg);
       			            $("#roompromoalert").fadeTo(2000, 500).slideUp(500, function(){
       			                $("#roompromoalert").slideUp(500);
       			            });
       			         $('#bulkupdate').modal('toggle');
     					});
         	
            };
            
          	$scope.filterLimit = 10;
          	 $scope.roomsInfo = [];
         	$scope.updateRooms = function(){
         		$("#loading-example-btn").prop('disabled', true);
         		var text = '{"array":' + JSON.stringify($scope.roomsInfo) + '}';
         		var fdata = JSON.parse(text);
         		var inventoryRemarks=$('#singleInventoryRemarks').val();
         		if(inventoryRemarks.trim().length==0)
       			{
         			$("#loading-example-btn").prop('disabled', false);
       				$("#roompromoalert").hide();
				  	var alertmsg = ' <div class="alert" style="color:green;text-align:center;">Enter the remarks.</div>';
		            $('#roompromoalert').html(alertmsg);
		            $("#roompromoalert").fadeTo(2000, 500).slideUp(500, function(){
		                $("#roompromoalert").slideUp(500);
		            });
	            
    				return;
       			}
         		
         		
         		
                		   $http(
                		   { 
                			   		   
                			   method : 'POST',
                			   data : fdata,
                			   dataType: 'json',
                			   headers : {
                				   'Content-Type': 'application/json; charset=utf-8'
                			   },
                			  url : "single-block-room-inventory?inventoryRemarks="+inventoryRemarks
                			   }).success(function(response) {
                			   
                			   $scope.singleblockroom = response.data;
                			   $("#loading-example-btn").prop('disabled', false);
                			   
                			   $("#roompromoalert").hide();
         					  var alertmsg = ' <div class="alert" style="color:green;text-align:center;">room inventory updated.</div>';
         			            $('#roompromoalert').html(alertmsg);
         			            $("#roompromoalert").fadeTo(2000, 500).slideUp(500, function(){
         			                $("#roompromoalert").slideUp(500);
         			            });
                			 }).error(function(response) {
            				  
         						$("#loading-example-btn").prop('disabled', false);
         						var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:green;text-align:center;">process failed, please try again.</div>';
       	   			            $('#roompromoalert').html(alertmsg);
       	   			            $("#roompromoalert").fadeTo(2000, 500).slideUp(500, function(){
       	   			                $("#roompromoalert").slideUp(500);
       	   			            });
         					});
                		   
                    	};
          	
                  	$("#StartDate").datepicker({
                          dateFormat: 'MM d, yy',
                                 minDate:  0,
                          onSelect: function (formattedDate) {
                              var date1 = $('#StartDate').datepicker('getDate'); 
                              var date = new Date( Date.parse( date1 ) ); 
                             // date.setDate( date.getDate() + 1 );        
                              var newDate = date.toDateString(); 
                              newDate = new Date( Date.parse( newDate ) );   
                              $('#endDate').datepicker("option","minDate",newDate);
                              $timeout(function(){
                                //scope.checkIn = formattedDate;
                            	  document.getElementById("StartDate").style.borderColor = "LightGray";
                              });
                          }
                      });
         
                  	
		         $("#endDate").datepicker({
		               dateFormat: 'MM d,yy',
		               minDate:  0,
		               onSelect: function (formattedDate) {
		                   var date2 = $('#endDate').datepicker('getDate'); 
		                   $timeout(function(){
		                     //scope.checkOut = formattedDate;
		                   	document.getElementById("endDate").style.borderColor = "LightGray";
		                   });
		               }
		           });
         
         $("#bookCheckin").datepicker({
             dateFormat: 'MM d,yy',
             minDate:  0,
             onSelect: function (dateText, inst) {
                 var date1 = $('#bookCheckin').datepicker('getDate'); 
                 var date = new Date( Date.parse( date1 ) ); 
                 date.setDate( date.getDate());        
                 var newDate = date.toDateString(); 
                 var d = new Date(dateText);
 	           // $('#calendar').fullCalendar('gotoDate', d);
                 newDate = new Date( Date.parse( newDate ) );   
                 $('#bookCheckout').datepicker("option","minDate",newDate);
                 $timeout(function(){
                   //scope.checkIn = formattedDate;
                 	document.getElementById("bookCheckin").style.borderColor = "LightGray";
                 });
             }
         });
       
       $("#bookCheckout").datepicker({
             dateFormat: 'MM d,yy',
             minDate:  0,
             onSelect: function (formattedDate) {
                 var date2 = $('#bookCheckout').datepicker('getDate'); 
                 $timeout(function(){
                   //scope.checkOut = formattedDate;
                 	document.getElementById("bookCheckout").style.borderColor = "LightGray";
                 });
             }
         });
   		
   		$scope.unread = function() {
   		//var notifiurl = "http://localhost:8085/collaborative-workflow/unreadnotifications.action";
   		var notifiurl = "unreadnotifications.action";
   		$http.get(notifiurl).success(function(response) {
   			$scope.latestnoti = response.data;
   		});
   		};
   
   /* 		$(document).ready(function(){
       	    $("#hide").click(function(){
       	        $("#container2").hide();
       	    });
       	    $("#show").click(function(){
       	        $("#container2").show();
       	    });
       	});
   		$("#hide").click(function() {
               //$('#container').css({position: 'relative', bottom: '60px'});
       	    $('html, body').animate({
       	        scrollTop: $("#container").offset().top
       	        
       	    }, 1000);
       	  
       	}); */
   	    $scope.getPropertyList = function() {
   			 
   	        	var userId = $('#adminId').val();
   	 			var url = "get-user-properties?userId="+userId;
   	 			$http.get(url).success(function(response) {
   	 			    
   	 				$scope.props = response.data;
   	 	
   	 			});
   	 		};
   	 		
   	    $scope.getAccommodations = function() {
   
   				var url = "get-accommodations";
   				$http.get(url).success(function(response) {
   				    //console.log(response);
   					$scope.accommodations = response.data;
   		
   				});
   			};
   	 		
          
   	 		
   	 	 $scope.change = function() {
   	        	   
   // 		        alert($scope.id);
   		        
   		       	       
   		 		};
   	 			
   	 	
   	 		
   	    $scope.getPropertyList();
   	    
   	    $scope.getAccommodations();
   	    
   	   
   		//$scope.unread();
   		//
           
   		
   	});
   	
   	app.directive('ngModelOnblur', function() {
   	    return {
   	        restrict: 'A',
   	        require: 'ngModel',
   	        priority: 1, // needed for angular 1.2.x
   	        link: function(scope, elm, attr, ngModelCtrl) {
   	            if (attr.type === 'radio' || attr.type === 'checkbox') return;

   	            elm.unbind('input').unbind('keydown').unbind('change');
   	            elm.bind('blur', function() {
   	                scope.$apply(function() {
   	                    ngModelCtrl.$setViewValue(elm.val());
   	                });         
   	            });
   	        }
   	    };
   	});
</script>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

  <style type="text/css">
    #ui-datepicker-div
    {
        z-index: 9999999 !important;
    }

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="s" uri="/struts-tags"%>
<style>
.btnpd{margin:5px;}
/* .avabg{background-color:#fff;padding:5px;} */
.invcal{background-color:#069;color:#fff;padding:5px;border:none;}
.rmname{color:#fff;}
.box-titles{display: inline-block; font-size: 18px; margin: 0; }
.box-title {color:#fff;}
.totalrooms{background: #88ba41 !important; border-color: #5cb85c !important;text-align:center;width:50%;padding:5px;float:left;}
.soldtext{background-color: #d9534f; border-color: #d43f3a;text-align:center;width:50%;padding:5px;float:right;}
</style>
   <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <section class="content-header">
          <h1>
           Room Inventory
            <!--<small>Subscription Expiry on {{property[0].subscription[0].expirydate}}</small>-->
          </h1>
          <ol class="breadcrumb">
            <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Room Inventory</li>
          </ol>
        </section>
 		
 		<!-- Main content -->
    <section class="content">
    <div class="box box-default color-palette-box">
        <div class="box-header ">
          <h3 class="box-titles"><i class="fa fa-tag"></i> Room Update</h3>
        </div>
        <div class="box-body">
                <form name="searchRate">
            <div class="col-sm-4 col-md-4">
               <div class="form-group">
                  <label>Select Accommodation Type :</label>
                  <select name="propertyAccommodationId" id="propertyAccommodationId" value="" ng-model="propertyAccommodationId" class="form-control" ng-required="true">
                     <option value="">Choose Accommodation</option>
                     <option ng-repeat="at in accommodations" value="{{at.accommodationId}}">{{at.accommodationType}}</option>
                  </select>
               </div>
            </div>
            <div class="col-sm-4 col-md-4">
               <div class=" form-group">
                  <label>Start Date :</label>
                  <div class="input-group date">
                     <label class="input-group-addon btn" for="startDate">
                     <span class="fa fa-calendar"></span>
                     </label>   
                     <input type="text"  id="StartDate" class="form-control" name="StartDate"  value="" ng-required="true" autocomplete="off">
                  </div>
               </div>
            </div>
            <div class="col-sm-4 col-md-4">
               <div class="form-group">
                  <label >End Date :</label>
                  <div class="input-group date">
                     <label class="input-group-addon btn" for="endDate">
                     <span class="fa fa-calendar"></span>
                     </label>   
                     <input type="text" id="endDate" class="form-control" name="endDate" value="" ng-required="true" autocomplete="off"> 
                  </div>
               </div>
            </div>
             <div class="col-sm-12 col-md-12">
               <button type="submit" ng-click="getBulkBlockInventory()"  class="btn btn-primary btngreeen pull-right btnpd"  >Search</button> <!-- ng-disabled="searchRate.$invalid" -->
               <a class="btn btn-danger pull-right btnpd" href="#bulkupdate" ng-click=""  data-toggle="modal"  >Bulk Update</a>
             </div>
        		<div class="col-sm-12 col-md-12">
               		<span id="roompromoalert"></span>
               </div>
         </form>
        </div>
        <!-- /.box-body -->
              
      
      <div class="row avabg">
        <div class="col-xs-12 ">
          <div class="box invcal">
            <div class="box-header">
              <h3 class="box-title">Available Inventory</h3>

              
            </div>
      <div class="row">
         <div class="col-md-12 " >
                  <div class="row ">
               <div class="col-md-6 text-center"><button class="btn-primary" ng-click="datePrevious()">Previous 10 Days {{roomCount.roomName }}</button></div>
               <div class="col-md-6 text-center"><button  class="btn-primary" ng-click="dateNext()">Next 10 Days</button></div>
            </div>
            <div class="table-responsive " ng-repeat="rc in blockinventory |limitTo :1"> 
                              <table width="100%" class="table table-bordered ">
                                    <tbody>
                                            <tr>
									            <td ><span class="font14 roomtype-text">Room Type</span></td>
									             <td ng-repeat="dd in rc.roomAvailable | limitTo:filterLimit track by $index" class="text-center" id="tlastchild{{$index}}"> {{dd.date}}</td>
									        </tr>
                                        <tr  ng-repeat="rc in blockinventory" >
                                            <td >
                                                <div class="pad_left_n col-md-7 col-xs-11">
                                                    <h5 class="text-left rmname">{{ rc.accommodationType }}</h5></div>
                             
                                            </td>
                                            <td ng-repeat="dd in rc.roomAvailable | limitTo:filterLimit track by $index">
											<input type="text" id="roomId{{$index}}" name="roomId{{$index}}" ng-change="singleBlockInventory(dd.available,dd.date,rc.accommodationId,dd.totalRooms,dd.totalAvailable,dd.soldRooms,$index)" ng-model="dd.available"  ng-model-onblur class="form-control avatext"/>
				                            <div class="roomview"><span ng-disabled="true" class="soldtext">{{dd.sold}}</span><span class="totalrooms">{{dd.totalRoomCount}}</span></div>
<!-- 				                            <input type="text" ng-model="dd.occupancy" style="background-color:#05253A ;border-top-color:#05253A ;text-align:center;color:white;width:100%;height:0%" ng-model-onblur class="form-control avatext " readonly/>                                             -->
                                            <%-- <input type="text" ng-change="singleBlockInventory(dd.available,dd.date,rc.accommodationId)" ng-model="dd.available"  ng-model-onblur class="form-control avatext"/>
                                             <div class="roomview"><span ng-disabled="true" class="soldtext">{{dd.sold}}</span><span class="totalrooms">{{dd.totalRoomCount}}</span></div> --%>
                                            <%-- <span class="soldbtn"><button class="btn-danger">{{dd.sold}}</button></span> --%>
                                            </td>
                                          
                                        </tr>
                                   
                                    </tbody>
                                </table>
                                <div class="col-sm-12 col-md-12">
				               		<div class="form-group">
		                              <label>Inventory Remarks</label>
		                              <input type="text" id="singleInventoryRemarks" name="singleInventoryRemarks" class="form-control" ng-required="true"/>
		                           </div>
				               </div>
                               <div class="col-md-6"><button  id="loading-example-btn" class="btn-primary pull-right" ng-click="updateRooms()">Update Inventory</button></div>
                                
            </div>
          </div>
      </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
      
      </div>

    </section>
    
    <div class="modal" id="bulkupdate">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" id="btnclose" class="close" data-dismiss="modal"
							aria-hidden="true">x</button>
						<h4 class="modal-title">Bulk Update</h4>
					</div>
					 <form name="addBlockForm" id="addBlockForm">
					<div class="modal-body" >
                          <div class="form-group col-md-6">
					      <label>Select Accommodation Type :</label>
							<select  name="accommodationId" id="accommodationId" ng-model="accommodationId" value="" class="form-control" ng-required="true">
							<option style="display:none" value="">Select Your  Accommodation</option>
	                       	<option ng-repeat="at in accommodations" value="{{at.accommodationId}}">{{at.accommodationType}}</option>
	                        </select>
						 </div>
						 
						
						 
						<div class=" form-group col-md-6">
							<label>Start Date :</label>
							 <div class="input-group date">
			                   <label class="input-group-addon btn">
			                   <span class="fa fa-calendar"></span>
			                  </label>   
			                  <input type="text" id="bookCheckin" class="form-control" name="bookCheckin" value="" ng-required="true" autocomplete="off">
			 
			                  
			                </div>
							     
						</div>
						
						<div class="form-group col-md-6">
							<label >End Date :</label>
						      <div class="input-group date">
			                   <label class="input-group-addon btn">
			                   <span class="fa fa-calendar"></span>
			             	 </label>   
			                  <input type="text" id="bookCheckout" class="form-control" name="bookCheckout"  value="" ng-required="true" autocomplete="off"> 
			
			                </div>
						     
						</div>
						<div class="form-group col-md-6">
							<label >Inventory Available:</label>
						      <div class="input-group date">
			             
			                  <input type="text" id="blockInventoryCount" class="form-control" name="blockInventoryCount"  value="" ng-required="true" autocomplete="off"> 
			
			                </div>
						     
						</div>
						<div class="col-md-12 col-sm-12">
                           <div class="form-group">
                              <label>Inventory Remarks</label>
                              <input type="text" id="inventoryRemarks" name="inventoryRemarks" class="form-control" ng-required="true" />
                           </div>
                        </div>
			
					<div class="modal-footer">
					<span id="roominventorypromoalert"></span>
<!-- 						<a href="#" data-dismiss="modal" class="btn">Close</a> -->
						<a href="#" data-dismiss="modal" class="btn btn-danger">Close</a>
						<button ng-click="addBlockForm.$valid && blockInventory()" ng-disabled="addBlockForm.$invalid" id="bulkBlock" class="btn btn-primary">Update</button>
					</div>
					</div>
					</form>
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
 		
 		
</div>
   <!-- jQuery 2.2.3 -->
<%-- <script src="https://code.jquery.com/jquery-1.12.4.js"></script> --%>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="plugins/fullcalendar/fullcalendar.min.js"></script>
<!-- Page specific script -->
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>
<script>
   var app = angular.module('myApp', ['ngProgress']);
   	app.controller('customersCtrl', function($scope, $http,$timeout,ngProgressFactory) {
     	$timeout(function(){
         	    // $scope.progressbar.complete();
               $scope.show = true;
               $("#pre-loader").css("display","none");
           }, 2000);
     	
          
          	$scope.datePrevious = function(){
          		
         		 var result = new Date(startDate);
         		result.setDate(result.getDate() - 10);
         		var months = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
         		var month = months[result.getMonth()];
         		var year = result.getFullYear();
         		var dates = month  + " " + result.getDate() + "," + year;

         		var fdata = "&startBlockDate=" + dates
      			+ "&endBlockDate=" + startDate
      			+ "&propertyAccommodationId=" + $('#propertyAccommodationId').val()
             	 	
             	  $http(
               		   {
               			   method : 'POST',
               			   data : fdata,
               			   headers : {
               			   'Content-Type' : 'application/x-www-form-urlencoded'
               			   },
               			 url : 'get-bulk-block-inventory'
               			   }).success(function(response) {
               			   
               			   $scope.blockinventory = response.data;
               			   
                			   
                 			   lastDate = $scope.blockinventory[0].roomAvailable[$scope.blockinventory[0].roomAvailable.length-1].date;
               			   startDate = $scope.blockinventory[0].roomAvailable[0].date;
               			 
               		   
               		   });
          	};
        	$scope.dateNext = function(){
          		 var result = new Date(lastDate);
          		result.setDate(result.getDate() + 10);
          		var months = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
          		var month = months[result.getMonth()];
          		var year = result.getFullYear();
          		var dates = month  + " " + result.getDate() + "," + year;

          		var fdata = "&startBlockDate=" + lastDate
       			+ "&endBlockDate=" + dates
       			+ "&propertyAccommodationId=" + $('#propertyAccommodationId').val()
              	 	
              	  $http(
                		   {
                			   method : 'POST',
                			   data : fdata,
                			   headers : {
                			   'Content-Type' : 'application/x-www-form-urlencoded'
                			   },
                			 url : 'get-bulk-block-inventory'
                			   }).success(function(response) {
                			   
                			   $scope.blockinventory = response.data;
                			   
                 			   
                  			   lastDate = $scope.blockinventory[0].roomAvailable[$scope.blockinventory[0].roomAvailable.length-1].date;
                			   startDate = $scope.blockinventory[0].roomAvailable[0].date;
                			 
                		   
                		   });
          	};
          	
        	/* $scope.singleBlockInventory = function(available,date,id){
          	 
          	  
          	 var newData = {'startBlockDate':date,'inventoryCount':available,'propertyAccommodationId':id};
         	  $scope.roomsInfo.push(newData);
          	}; */
          	
          	$scope.singleBlockInventory = function(available,date,id,totalRooms,totalAvailable,soldRooms,indx){
        		var availRoom=parseInt(available);
        		var totalAvail=parseInt(totalRooms);
        		var soldRoom=parseInt(soldRooms);
        		var availableRooms=totalAvail-soldRoom;
        		if(availRoom<=parseInt(availableRooms)){
        			var newData = {'startBlockDate':date,'inventoryCount':available,'propertyAccommodationId':id};
               	  	$scope.roomsInfo.push(newData);
               	 /* $("#roompromoalert").hide();
				  var alertmsg = ' <div class="alert" style="color:green;text-align:center;">Inventory changed for '+date+'.</div>';
		            $('#roompromoalert').html(alertmsg);
		            $("#roompromoalert").fadeTo(2000, 500).slideUp(500, function(){
		                $("#roompromoalert").slideUp(500);
		            }); */
        		}else if(availRoom>parseInt(availableRooms)){
        			$scope.blockinventory[0].roomAvailable[indx].blockedRooms=totalAvailable;
		            document.getElementById('roomId'+indx).value=totalAvailable;
        			$("#roompromoalert").hide();
					  var alertmsg = ' <div class="alert" style="color:green;text-align:center;">Maximum inventory updated.</div>';
			            $('#roompromoalert').html(alertmsg);
			            $("#roompromoalert").fadeTo(2000, 500).slideUp(500, function(){
			                $("#roompromoalert").slideUp(500);
			            });
			            
        			return;
        		}
          	 
          	};
          	
          	
          	var lastDate = 0;
          	var startDate = 0;
          	
        	$scope.getBulkBlockInventory = function() {
          	 	var fdata = "&startBlockDate=" + $('#StartDate').val()
   			+ "&endBlockDate=" + $('#endDate').val()
   			+ "&propertyAccommodationId=" + $('#propertyAccommodationId').val()
          	 	
          	  $http(
            		   {   
            			   	   
            			   method : 'POST',
            			   data : fdata,            			  
            			   headers : {
            			   'Content-Type' : 'application/x-www-form-urlencoded'
            			   },
            			  url : 'get-bulk-block-inventory'   /*   get-ota-booked-details */
            			   }).success(function(response) {
            			   
            			   $scope.blockinventory = response.data;
            			
             			   
              			   lastDate = $scope.blockinventory[0].roomAvailable[$scope.blockinventory[0].roomAvailable.length-1].date;
            			 	startDate = $scope.blockinventory[0].roomAvailable[0].date;
            		   
            		   });
          		
          	};
          	
          	
          	$scope.blockInventory = function(){
          		
         	   var fdata = "startBlockDate=" + $('#bookCheckin').val()
            	+ "&endBlockDate=" + $('#bookCheckout').val()
         	+ "&inventoryCount=" + $('#blockInventoryCount').val()
            	+ "&propertyAccommodationId=" + $('#accommodationId').val()
            	+"&inventoryRemarks="+$('#inventoryRemarks').val();
         	  	var inventoryRemarks=$('#inventoryRemarks').val();
         	   if(inventoryRemarks.trim().length==0)
     			{
         		  
       			$("#roominventorypromoalert").hide();
				  var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:green;text-align:center;">please check the inventory on '+date+'.</div>';
		            $('#roominventorypromoalert').html(alertmsg);
		            $("#roominventorypromoalert").fadeTo(2000, 500).slideUp(500, function(){
		                $("#roominventorypromoalert").slideUp(500);
		            });
	            
  				return;
     			}
         		   $http(
         		   {
         			   method : 'POST',
         			   data : fdata,
         			   headers : {
         			   'Content-Type' : 'application/x-www-form-urlencoded'
         			   },
         			  url : 'block-room-inventory'
         			   }).success(function(response) {
         			   
         			   $scope.blockrooms = response.data;
         			  $("#roompromoalert").hide();
 					  var alertmsg = ' <div class="alert" style="color:green;text-align:center;">room inventory updated.</div>';
 			            $('#roompromoalert').html(alertmsg);
 			            $("#roompromoalert").fadeTo(2000, 500).slideUp(500, function(){
 			                $("#roompromoalert").slideUp(500);
 			            }); 
         			   
 			           $('#bulkupdate').modal('toggle');
         		   
         			  }).error(function(response) {
         				  
     						$("#roompromoalert").hide();
       					  var alertmsg = ' <div class="alert" style="color:green;text-align:center;">process failed, please try again!! .</div>';
       			            $('#roompromoalert').html(alertmsg);
       			            $("#roompromoalert").fadeTo(2000, 500).slideUp(500, function(){
       			                $("#roompromoalert").slideUp(500);
       			            });
       			         $('#bulkupdate').modal('toggle');
     					});
         	
            };
            
          	$scope.filterLimit = 10;
          	 $scope.roomsInfo = [];
         	$scope.updateRooms = function(){
         		$("#loading-example-btn").prop('disabled', true);
         		var text = '{"array":' + JSON.stringify($scope.roomsInfo) + '}';
         		var fdata = JSON.parse(text);
         		var inventoryRemarks=$('#singleInventoryRemarks').val();
         		if(inventoryRemarks.trim().length==0)
       			{
         			$("#loading-example-btn").prop('disabled', false);
       				$("#roompromoalert").hide();
				  	var alertmsg = ' <div class="alert" style="color:green;text-align:center;">Enter the remarks.</div>';
		            $('#roompromoalert').html(alertmsg);
		            $("#roompromoalert").fadeTo(2000, 500).slideUp(500, function(){
		                $("#roompromoalert").slideUp(500);
		            });
	            
    				return;
       			}
         		
         		
         		
                		   $http(
                		   { 
                			   		   
                			   method : 'POST',
                			   data : fdata,
                			   dataType: 'json',
                			   headers : {
                				   'Content-Type': 'application/json; charset=utf-8'
                			   },
                			  url : "single-block-room-inventory?inventoryRemarks="+inventoryRemarks
                			   }).success(function(response) {
                			   
                			   $scope.singleblockroom = response.data;
                			   $("#loading-example-btn").prop('disabled', false);
                			   
                			   $("#roompromoalert").hide();
         					  var alertmsg = ' <div class="alert" style="color:green;text-align:center;">room inventory updated.</div>';
         			            $('#roompromoalert').html(alertmsg);
         			            $("#roompromoalert").fadeTo(2000, 500).slideUp(500, function(){
         			                $("#roompromoalert").slideUp(500);
         			            });
                			 }).error(function(response) {
            				  
         						$("#loading-example-btn").prop('disabled', false);
         						var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:green;text-align:center;">process failed, please try again.</div>';
       	   			            $('#roompromoalert').html(alertmsg);
       	   			            $("#roompromoalert").fadeTo(2000, 500).slideUp(500, function(){
       	   			                $("#roompromoalert").slideUp(500);
       	   			            });
         					});
                		   
                    	};
          	
                  	$("#StartDate").datepicker({
                          dateFormat: 'MM d, yy',
                                 minDate:  0,
                          onSelect: function (formattedDate) {
                              var date1 = $('#StartDate').datepicker('getDate'); 
                              var date = new Date( Date.parse( date1 ) ); 
                             // date.setDate( date.getDate() + 1 );        
                              var newDate = date.toDateString(); 
                              newDate = new Date( Date.parse( newDate ) );   
                              $('#endDate').datepicker("option","minDate",newDate);
                              $timeout(function(){
                                //scope.checkIn = formattedDate;
                            	  document.getElementById("StartDate").style.borderColor = "LightGray";
                              });
                          }
                      });
         
                  	
		         $("#endDate").datepicker({
		               dateFormat: 'MM d,yy',
		               minDate:  0,
		               onSelect: function (formattedDate) {
		                   var date2 = $('#endDate').datepicker('getDate'); 
		                   $timeout(function(){
		                     //scope.checkOut = formattedDate;
		                   	document.getElementById("endDate").style.borderColor = "LightGray";
		                   });
		               }
		           });
         
         $("#bookCheckin").datepicker({
             dateFormat: 'MM d,yy',
             minDate:  0,
             onSelect: function (dateText, inst) {
                 var date1 = $('#bookCheckin').datepicker('getDate'); 
                 var date = new Date( Date.parse( date1 ) ); 
                 date.setDate( date.getDate());        
                 var newDate = date.toDateString(); 
                 var d = new Date(dateText);
 	           // $('#calendar').fullCalendar('gotoDate', d);
                 newDate = new Date( Date.parse( newDate ) );   
                 $('#bookCheckout').datepicker("option","minDate",newDate);
                 $timeout(function(){
                   //scope.checkIn = formattedDate;
                 	document.getElementById("bookCheckin").style.borderColor = "LightGray";
                 });
             }
         });
       
       $("#bookCheckout").datepicker({
             dateFormat: 'MM d,yy',
             minDate:  0,
             onSelect: function (formattedDate) {
                 var date2 = $('#bookCheckout').datepicker('getDate'); 
                 $timeout(function(){
                   //scope.checkOut = formattedDate;
                 	document.getElementById("bookCheckout").style.borderColor = "LightGray";
                 });
             }
         });
   		
   		$scope.unread = function() {
   		//var notifiurl = "http://localhost:8085/collaborative-workflow/unreadnotifications.action";
   		var notifiurl = "unreadnotifications.action";
   		$http.get(notifiurl).success(function(response) {
   			$scope.latestnoti = response.data;
   		});
   		};
   
   /* 		$(document).ready(function(){
       	    $("#hide").click(function(){
       	        $("#container2").hide();
       	    });
       	    $("#show").click(function(){
       	        $("#container2").show();
       	    });
       	});
   		$("#hide").click(function() {
               //$('#container').css({position: 'relative', bottom: '60px'});
       	    $('html, body').animate({
       	        scrollTop: $("#container").offset().top
       	        
       	    }, 1000);
       	  
       	}); */
   	    $scope.getPropertyList = function() {
   			 
   	        	var userId = $('#adminId').val();
   	 			var url = "get-user-properties?userId="+userId;
   	 			$http.get(url).success(function(response) {
   	 			    
   	 				$scope.props = response.data;
   	 	
   	 			});
   	 		};
   	 		
   	    $scope.getAccommodations = function() {
   
   				var url = "get-accommodations";
   				$http.get(url).success(function(response) {
   				    //console.log(response);
   					$scope.accommodations = response.data;
   		
   				});
   			};
   	 		
          
   	 		
   	 	 $scope.change = function() {
   	        	   
   // 		        alert($scope.id);
   		        
   		       	       
   		 		};
   	 			
   	 	
   	 		
   	    $scope.getPropertyList();
   	    
   	    $scope.getAccommodations();
   	    
   	   
   		//$scope.unread();
   		//
           
   		
   	});
   	
   	app.directive('ngModelOnblur', function() {
   	    return {
   	        restrict: 'A',
   	        require: 'ngModel',
   	        priority: 1, // needed for angular 1.2.x
   	        link: function(scope, elm, attr, ngModelCtrl) {
   	            if (attr.type === 'radio' || attr.type === 'checkbox') return;

   	            elm.unbind('input').unbind('keydown').unbind('change');
   	            elm.bind('blur', function() {
   	                scope.$apply(function() {
   	                    ngModelCtrl.$setViewValue(elm.val());
   	                });         
   	            });
   	        }
   	    };
   	});
</script>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

  <style type="text/css">
    #ui-datepicker-div
    {
        z-index: 9999999 !important;
    }

</style>
