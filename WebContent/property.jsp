<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="s" uri="/struts-tags"%>

   <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Property
          </h1>
          <ol class="breadcrumb">
            <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Property</li>
          </ol>
        </section>
		
        <!-- Main content -->
        <section class="content">
        
        	<div class="row">
            	<div class="col-xs-12" ng-if="properties.length>=0">
						<div class="box">
							<div class="box-header">
								<h3 class="box-title">Property </h3>
							</div>
							
							
							<!-- /.box-header -->
							<div class="box-body table-responsive no-padding" > 
								<table class="table table-hover" id="example1">
									<tr>
										<th>Order</th>
										<th>Location </th>
										<th>Property Name</th>
										<th>Edit</th>
										<th>Delete</th>
										<th>User Name</th>
									</tr>
									<tr ng-repeat="p in properties">
										<td>{{p.serialNo}}</td>
										<td>{{p.googleLocationName}}</td>
										<td><span style="color:{{p.isActive}};">{{p.propertyName}}</span></td>
										<td>
											<a href="#editmodal" ng-click="getProperty(p.DT_RowId,p.googleLocationId)"  data-toggle="modal"><i  class="fa fa-pencil text-green"></i></a>
										</td>
										<td>
											<a href="#deletemodal" ng-click="deleteProperty(p.DT_RowId)" data-toggle="modal" ><i  class="fa fa-fw fa-trash text-red" ></i></a>
										</td>
										<td>{{p.createdBy}}</td>
									</tr>
									<tr>
									</tr>
								</table>
							</div>
							<!-- /.box-body -->
							
							
							<div class="box-footer clearfix">
								 <ul class="pagination pagination-sm no-margin pull-right">
									<a class="btn btn-primary " href="#addmodal" data-toggle="modal" >Add Property</a>
								</ul> 
							</div>
							<div class="box-footer clearfix">
								<ul class="pagination pagination-sm"></ul>
								<div class="simple-pagination pull-right" >   
									<p class="simple-pagination__items">Showing {{propertyCount[0].count}} out of {{settings.pageLimit}}</p>
									<p><button ng-click="previousPage()" ng-disabled="settings.currentPage <= 0" class="simple-pagination__button simple-pagination__button--prev"> &#10094;          
									</button><span class="simple-pagination__pages">{{settings.currentPage + 1}} of {{getTotalPages()}}</span>
									<button ng-click="nextPage()" ng-disabled="settings.currentPage === (getTotalPages() - 1)" class="simple-pagination__button simple-pagination__button--next">&#10095;</button></p>
									<a href="" ng-click="limited(10)" ng-class="active">{{10}}</a> |
									<span><a href="" ng-click="all(propertyCount[0].count)" ng-class="active">All</a></span> 
								</div>
						   </div> 
						</div>
						<!-- /.box -->
					</div>	
         	</div>
          
        </section><!-- /.content -->
      	<input type="hidden" value ="<%=session.getAttribute("propertyId") %>" id="locationId" name="locationId" >
      	<div class="modal" id="addmodal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" id="btnclose" class="close" data-dismiss="modal"
							aria-hidden="true">x</button>
						<h4 class="modal-title">Add Property</h4>
					</div>
					 <form method="post" theme="simple" name="addproperty">
					 <section>
					<div class="modal-body" >
						<div class="form-group col-md-12">
						  <label>Select Location<span style="color:red">*</span></label>
						  <select  name="googleLocationId" id="googleLocationId" ng-model="googleLocationId" ng-required="true" ng-change="getGoogleAreas()" class="form-control" >
						  <option style="display:none" value="0">Select Your Location</option>
		                  <option ng-repeat="gl in googleLocations" value="{{gl.googleLocationId}}"  >{{gl.googleLocationDisplayName}}</option>
		                  </select>
						</div>
						<div class="form-group col-md-12">
						  <label>Select Location Type<span style="color:red">*</span></label>
						  <select  name="propertyLocationTypeId" id="propertyLocationTypeId" ng-model="propertyLocationTypeId" ng-required="true" class="form-control" >
						  <option style="display:none" value="0">Select Your Location Type</option>
		                  <option ng-repeat="lt in locationTypes" value="{{lt.DT_RowId}}"  >{{lt.locationTypeName}}</option>
		                  </select>
						</div>
						<div class="form-group col-md-12">
							<label for="propertyName">Property Name<span style="color:red">*</span></label>
							<input type="text" class="form-control"  name="propertyName"  id="propertyName"  placeholder="Property Name" value="" ng-model='propertyName' ng-pattern="^/^[A-Za-z\d\s]+$/" ng-required="true">
						    <span ng-show="addproperty.propertyName.$error.pattern" style="color:red">Not a Property Name!</span>
						</div>
						<div class="form-group col-md-12">
							<label for="propertyUrl">Property URL<span style="color:red">*</span></label>
							<input type="text" class="form-control"  name="propertyUrl"  id="propertyUrl"  placeholder="Property URL" value="" ng-model='propertyUrl' ng-pattern="^/^[A-Za-z\d\s]+$/" ng-required="true">
						    <span ng-show="addproperty.propertyUrl.$error.pattern" style="color:red">Not a Property URL!</span>
						</div>
						<div class="form-group col-md-12" ng-if="areas.length>0" id="newAreas">
						<label>Location Areas<span style="color:red">*</span></label>
							<div>
                           		<label ng-repeat="a in areas"  class="checkbox-inline">
                      				<input type="checkbox" name="areas[]"  id="areas[]" value="{{a.DT_RowId}}"  >&nbsp {{a.googleAreaDisplayName}}
                    			</label>
                    		</div>
                    	</div>
						<div class="form-group col-md-6">
							<label>Property Image</label>
							<input type="file" class="form-control"   file-input="files" name="photo" id="photo">
							<span style="color:red">upload only(214 * 221)</span>
<!-- 							<div id="validimage" name="validimage" style="color: red"><label>Not Image Format!!</label></div> -->
						</div>
						<div class="form-group" >
						   <div class="input-group" >
						  		<label>&nbsp;&nbsp;</label>
						  		<label>&nbsp;&nbsp;</label>
						  		<label>&nbsp;&nbsp;</label>
						  		<label>&nbsp;&nbsp;</label>
						  		<input type="hidden" class="form-control"  >
						   </div>

						</div>
					</div>
					</section>
					<section>
						<div class="modal-footer">
							<a href="#" data-dismiss="modal" class="btn btn-danger">Close</a>
							<button ng-click="addproperty.$valid && addGoogleProperty()" ng-disabled="addproperty.$invalid" class="btn btn-primary">Save</button>
						</div>
					</section>
					</form>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dalog -->
		</div>
		<div class="modal" id="editmodal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" id="editbtnclose" class="close" data-dismiss="modal"
							aria-hidden="true">x</button>
						<h4 class="modal-title">Edit Property</h4>
					</div>
					 	 <form method="post" theme="simple" name="editproperty">
					 	 <section>
						<div class="modal-body">
							<input type="hidden" value="{{property[0].propertyId}}" id="editPropertyId">
						    <!-- <input type="hidden" value="{{property[0].googleLocationId}}" id="editGoogleLocationId"> -->
						    <div class="form-group col-md-12">
								<label>Property Name<span style="color:red">*</span></label>
								<input type="text" class="form-control"  name="editPropertyName"  id="editPropertyName"  value="{{property[0].propertyName}}" ng-required="true">
							    <span ng-show="editproperty.editPropertyName.$error.pattern">Not a Property Name!</span>
							</div>
							<div class="form-group col-md-12">
						  <label>Select Location<span style="color:red">*</span></label>
						  <select  name="editGoogleLocationId" id="editGoogleLocationId" ng-model="editGoogleLocationId" ng-change="getEditGoogleAreas()" class="form-control" >
						  <option  value="0" ng-selected="property[0].googleLocationId == '' ">Select Your Location</option>
		                  <option ng-repeat="gl in googleLocations" value="{{gl.googleLocationId}}" ng-selected="gl.googleLocationId == property[0].googleLocationId" >{{gl.googleLocationDisplayName}}</option>
		                  </select>
						</div>
						<div class="form-group col-md-12">
						  <label>Select Location Type<span style="color:red">*</span></label>
						  <select  name="editLocationTypeId" id="editLocationTypeId" ng-model="editLocationTypeId" class="form-control" >
						  <option  value="0" ng-selected="property[0].locationTypeId == ''">Select Your Location Type</option>
		                  <option ng-repeat="lt in locationTypes" value="{{lt.DT_RowId}}" ng-selected="lt.DT_RowId == property[0].locationTypeId" >{{lt.locationTypeName}}</option>
		                  </select>
						</div>
							<div class="form-group col-md-12">
								<label>Property URL<span style="color:red">*</span></label>
								<input type="text" class="form-control"  name="editPropertyUrl"  id="editPropertyUrl"  value="{{property[0].propertyUrl}}" ng-required="true">
							    <span ng-show="editproperty.editPropertyUrl.$error.pattern">Not a Property URL!</span>
							</div>
							<div class="form-group col-md-12" ng-if="property[0].areas.length>=0" id="editAreas">
								<label>Location Areas<span style="color:red">*</span></label>
								<div>
	                           		<label ng-repeat="area in property[0].areas "  class="checkbox-inline">
	                      					<input type="checkbox" name="modAreas[]"  id="modAreas[]" value="{{area.googleAreaId}}"  ng-checked ="area.status == 'true'" >&nbsp {{area.googleAreaDisplayName}}
	                    			</label>
                    			</div>
	                    	</div>
	                    	
							 <div class="form-group col-md-12">
								<label for="child_included_rate">Property Image</label>
								<input type="file" class="form-control"   file-input="files" name="photo" id="photo">
								<span style="color:red">upload only(214 * 221)</span>
<!-- 								<div id="validimage" name="validimage" style="color: red"><label>Not Image Format!!</label></div> -->
							</div>
							 
							<div class="col-md-12">
					         	<img class="img-thumbnail" id="photoPath" file-input="files" ngf-select="upload($file)"  
					         	ngf-accept="'image/*'" ngf-pattern="'.jpg,.png,!.gif'" 
					         	src="{{property[0].photoPath}}"  alt="User Avatar" width="100%">
					        </div>
								
						</div>
						</section>
						<section>
							<div class="modal-footer">
								<a href="#" data-dismiss="modal" class="btn btn-danger">Close</a>
								<button ng-click="editproperty.$valid && editGoogleProperty()" ng-disabled="editproperty.$invalid" class="btn btn-primary">Update</button>
							</div>  
						</section>
						</form>
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dalog -->
		</div>
      


<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>

	<script>
 
	
	var app = angular.module('myApp',['ngFileUpload']);
	app.controller('customersCtrl',['$scope',
	             					'Upload',
	            					'$timeout',
	            					'$http', function($scope,Upload,$timeout,$http) {

		
		
		$scope.settings = {
	            currentPage: 0,
	            offset: 0,
	            pageLimit: 10,
	            pageLimits: ['10', '50', '100']
	          };

		 $scope.getTotalPages = function () {
			return Math.ceil($scope.propertyCount[0].count / $scope.settings.pageLimit);
		};
			
		 $scope.isCurrentPageLimit = function (value) {
		       return $scope.settings.pageLimit == value;
		 };
		 
		 $scope.previousPage = function () {
			   $scope.settings.currentPage -= 1;
			   var previousOffset = $scope.settings.currentPage * $scope.settings.pageLimit;
		       var url = "get-google-properties?limit="+$scope.settings.pageLimit+"&offset="+previousOffset;
				$http.get(url).success(function(response) {
					//console.log(response);
					$scope.properties = response.data;
				}); 
		    }; 
		 
     		$scope.nextPage = function () {
		       $scope.settings.currentPage += 1;
		       var nextOffset = $scope.settings.currentPage * $scope.settings.pageLimit;
		       var url = "get-google-properties?limit="+$scope.settings.pageLimit+"&offset="+nextOffset;
				$http.get(url).success(function(response) {
					//console.log(response);
					$scope.properties = response.data;
				}); 
		     }; 
		       
      			$scope.all = function (total) {
		    	   $scope.settings.pageLimit = total;
			       var nextOffset = 0;
			       var url = "get-google-properties?limit="+total+"&offset="+nextOffset;
					$http.get(url).success(function(response) {
						//console.log(response);
						$scope.properties = response.data;
					}); 
			    };
			       
      			 $scope.limited = function (limit) {
			    	   $scope.settings.pageLimit = limit;
				       var nextOffset = 0;
				       var url = "get-google-properties?limit="+limit+"&offset="+nextOffset;
						$http.get(url).success(function(response) {
							//console.log(response);
							$scope.properties = response.data;
					}); 
		       };			

		    
		
	       	$timeout(function(){
	      	    // $scope.progressbar.complete();
	            $scope.show = true;
	            $("#pre-loader").css("display","none");
	        }, 2000);

			 
			
			$scope.unread = function() {
			//var notifiurl = "http://localhost:8085/collaborative-workflow/unreadnotifications.action";
			var notifiurl = "unreadnotifications.action";
			$http.get(notifiurl).success(function(response) {
				$scope.latestnoti = response.data;
			});
			};

			
			
			$scope.getProperties = function() {

				var url = "get-google-properties?limit="+$scope.settings.pageLimit+"&offset="+$scope.settings.offset;
				$http.get(url).success(function(response) {
				    console.log(response);
					$scope.properties = response.data;
		
				});
			};
			
			var spinner = $('#loadertwo');
			   
			$scope.addGoogleProperty = function(){
				
				var checkbox_value = "";
			    $("#newAreas :checkbox").each(function () {
			        var ischecked = $(this).is(":checked");
			        if (ischecked) {
			            checkbox_value += $(this).val() + ",";
			        }
			    });
			   	
				 var file = $scope.files;
				 console.log(file); 
				// var file = this.files[0];
				 var fileType = file["type"];
				 var ValidImageTypes = ["image/gif", "image/jpeg", "image/png"];
				 if ($.inArray(fileType, ValidImageTypes) < 0) {
					     // invalid file type code goes here.
					     alert('image invalid')
					}
					else{
						var fdata = "googleLocationId=" + $('#googleLocationId').val()
							+ "&propertyName=" + $('#propertyName').val()
							+ "&propertyUrl="+$('#propertyUrl').val()
							+ "&locationTypeId="+$('#propertyLocationTypeId').val()
							+ "&checkedAreas=" +checkbox_value; 
						spinner.show();
						$http(
								{
									method : 'POST',
									data : fdata,
									headers : {
										'Content-Type' : 'application/x-www-form-urlencoded'
									},
									url : 'add-new-google-property'
								}).then(function successCallback(response) {
									$scope.addedproperty = response.data;
								setTimeout(function(){ spinner.hide(); 
									  }, 1000);
									console.log($scope.addedproperty.data[0].locationPropertyId);
							        
									var locationPropertyId = $scope.addedproperty.data[0].locationPropertyId;
									 Upload.upload({
							                url: 'propertypicupdate',
							                enableProgress: true, 
							                data: {myFile: file, 'locationPropertyId': locationPropertyId} 
							            }).then(function (resp) {
							            	
							            	
							            });
									 
								var gdata="locationPropertyId=" +locationPropertyId;
										$http(
											    {
												method : 'POST',
												data : gdata,
												headers : {
													'Content-Type' : 'application/x-www-form-urlencoded'
													//'Content-Type' : 'application/x-www-form-urlencoded'
												},
											    url: 'add-new-property-user'
											   }).then(function successCallback(response) {
												   setTimeout(function(){ spinner.hide(); 
													  }, 1000);
												   // window.location.reload();
											   }, function errorCallback(response) {
											});
										
									$scope.getProperty();
									var alertmsg = ' <div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Alert!</h4>Success Item Added Succesfully.</div>';
						            $('#message').html(alertmsg);
						            window.location.reload(); 
									$timeout(function(){
							      	$('#btnclose').click();
							        }, 2000); 
						        	
						        
						}, function errorCallback(response) {
							
						});
				}
			};
			
			$scope.editGoogleProperty = function(){
				/* var elem = document.getElementById('photoPath');
				alert(elem)
				  if(elem.getAttribute('src') == ""){
					  alert("please choose Image")
				  } */
				
				var file = $scope.files;	
				 console.log(file); 
				 var fileType = file["type"];
				 var ValidImageTypes = ["image/gif", "image/jpeg", "image/png"];
				 if ($.inArray(fileType, ValidImageTypes) < 0) {
					     // invalid file type code goes here.
					     alert('image invalid')
					}
					else{
						
						var checkbox_value = "";
					    $("#editAreas :checkbox").each(function () {
					        var ischecked = $(this).is(":checked");
					        if (ischecked) {
					            checkbox_value += $(this).val() + ",";
					        }
					    });
					    
						var fdata = "googleLocationId=" + $('#editGoogleLocationId').val()
						+ "&propertyName=" + $('#editPropertyName').val()
						+ "&propertyId=" + $('#editPropertyId').val()
						+ "&propertyUrl="+$('#editPropertyUrl').val()
						+ "&locationTypeId="+$('#editLocationTypeId').val()  
						+ "&checkedAreas=" +checkbox_value; 
						spinner.show();
						$http(
								{
									method : 'POST',
									data : fdata,
									headers : {
										'Content-Type' : 'application/x-www-form-urlencoded'
									},
									url : 'edit-google-property'
								}).then(function successCallback(response) {
									Upload.upload({
						                url: 'propertypicupdate',
						                enableProgress: true, 
						                data: {myFile: file, 'locationPropertyId': $('#editPropertyId').val()} 
						            }).then(function (resp) {
						            	setTimeout(function(){ spinner.hide(); 
										  }, 1000);
						            	window.location.reload(); 
						            	
						            });
									
									
						}, function errorCallback(response) {
							
						});
					}
			};
			
            
			 $scope.getGoogleLocations = function() {
					var url = "get-area-google-locations";
					$http.get(url).success(function(response) {
					    console.log(response);
						$scope.googleLocations = response.data;
			
					});
				};
				
				$scope.getLocationTypes = function() {

				var url = "get-location-types";
				$http.get(url).success(function(response) {
				    console.log(response);
					$scope.locationTypes = response.data;
		
				});
			};
			
            
			$scope.getProperty = function(rowid,locid) {
				var url = "get-property-detail?propertyId="+rowid+"&googleLocationId="+locid;
				$http.get(url).success(function(response) {
				   // console.log(response);
					$scope.property = response.data;
		
				});
				
			};
			
            			
			$scope.deleteProperty=function(rowid){
				var fdata = "&propertyId="+ rowid
				var deleteLocationCheck=confirm("Do you want to delete the property?");
				if(deleteLocationCheck){
					$http(
							{
								method : 'POST',
								data : fdata,
								headers : {
									'Content-Type' : 'application/x-www-form-urlencoded'
								},
								url: 'delete-property'
							}).then(function successCallback(response) {
								window.location.reload();
							   }, function errorCallback(response) {
								
								
							});
				}
				else{
					
				}
				
			};
			
			$scope.getPropertyCount = function() {

				var url = "get-property-count";
				$http.get(url).success(function(response) {
				    //console.log(response);
					$scope.propertyCount = response.data;
		            //console.log($scope.accommodationRoomsCount.data[0].size);
				});
			};
			
			$scope.getGoogleAreas = function() {
				var areaLocationId=$('#googleLocationId').val();
				var url = "get-google-location-areas?areaLocationId="+areaLocationId;
				$http.get(url).success(function(response) {
					$scope.areas = response.data;
				});
			};
			
			$scope.getEditGoogleAreas = function() {
				var areaLocationId=$('#editGoogleLocationId').val();
				var url = "get-google-location-areas?areaLocationId="+areaLocationId;
				$http.get(url).success(function(response) {
					$scope.areas = response.data;
					$scope.property[0].areas = $scope.areas;
				});
			};
			
			$scope.getProperties();
			$scope.getGoogleLocations();
			$scope.getPropertyCount();
			$scope.getLocationTypes();
			
	}]);


	app.directive('fileInput', ['$parse', function ($parse) {
	    return {
	        restrict: 'A',
	        link: function (scope, element, attributes) {
	            element.bind('change', function () {
	                $parse(attributes.fileInput)
	                .assign(scope,element[0].files[0])
	                scope.$apply()
	            });
	        }
	    };
	}]);		
		   
		        

		
	</script>
			
	<link rel="stylesheet" href="dist/css/dataTables.bootstrap.css">
	<script src="js1/upload/ng-file-upload-shim.min.js"></script>
	<script src="js1/upload/ng-file-upload.min.js"></script>
	 <script src="js1/jquery.dataTables.min.js" type="text/javascript"></script>
	 <script src="js1/dataTables.bootstrap.min.js" type="text/javascript"></script>
       