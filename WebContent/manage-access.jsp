<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="s" uri="/struts-tags"%>

   <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <section class="content-header">
          <h1>
            Manage Access
            <!--<small>Subscription Expiry on {{property[0].subscription[0].expirydate}}</small>-->
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Setting</a></li>
            <li class="active">Manage-Access</li>
          </ol>
        </section>
		
        <!-- Main content -->
        <section class="content">
        
        	 <div class="row">
        	 <div class="col-xs-12">
        	 	<div class="box-body" id="message">
        			
                  </div>
                  </div>
            <div class="col-xs-12">   <!-- ng-repeat=" ac in accommodations " -->
              <div class="nav-tabs-custom" >
                <ul class="nav nav-tabs">
<!--        			 <li class="active"><a href="#fa-icons" data-toggle="tab" > Admin</a></li> -->
                  <li ng-repeat="rl in role"><a href="#fa-icons3" ng-click="getRoleAccess(rl.DT_RowId)" data-toggle="tab">{{rl.roleName}}</a></li>  
                 
                </ul>
                <div class="tab-content">
              	   <div class="tab-pane " id="fa-icons3">
                  <form id ="form" name="form" method="post" action="" enctype="multipart/form-data" theme="simple">
                    <s:hidden name="propertyId" value="%{propertyId}"></s:hidden>
                  	
                    <section id="new">
                      <!-- <h4 class="page-header">General Settings</h4> -->
                      <input type="hidden" name="roleId" id="roleId" value="">
                      <div class="row fontawesome-icon-list">
                      	<div class="col-md-3 col-sm-4" ng-repeat="ra in roleaccess">
	                        	 <div class="form-group" >
	                        	 	<label>
                      					<input type="checkbox" ng-class="active" name="checkedAmenity[]"  id="checkedAmenity" value="{{ra.accessRightsId}}"  class="flat-red" ng-checked ="ra.status == 'true'" >&nbsp {{ra.displayName}}
                    				</label>
                    				
	                        	 </div>
	                        	 
	                        	
	                        </div>
                        
                        
                      </div>
                    </section>

                    

					
                    

                    <section id="video-player" >
                      <h4 class="page-header"></h4>
                       <div class="row fontawesome-icon-list" >
                        <div class="col-md-3 col-sm-4"></div>
                        <div class="col-md-3 col-sm-4"></div>
                        <div class="col-md-3 col-sm-4"></div>
                        <div class="col-md-3 col-sm-4">
                        
                        	<button type="button"  class="btn btn-primary  pull-right" ng-click="saveRoleAccess()">Save</button>
                        
                      </div>
                    </section>

                        </form>
              

                </div><!-- /.tab-content -->
              	  
              	  
              
          
              	  
              
              </div><!-- /.nav-tabs-custom -->
              
             
                        
            </div><!-- /.col -->
          </div><!-- /.row -->
          </div>
        
        	
          
          
        </section><!-- /.content -->
      
  
       
		
		

      </div><!-- /.content-wrapper -->
     


<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>

	<script>

		var app = angular.module('myApp', ['ngProgress']);
		app.controller('customersCtrl', function($scope, $http,$timeout,ngProgressFactory) {

			$("#alert-message").hide();
			//$scope.progressbar = ngProgressFactory.createInstance();
			//$scope.progressbar.setColor("green");
			//$scope.progressbar.start();
	       	$timeout(function(){
	      	    // $scope.progressbar.complete();
	            $scope.show = true;
	            $("#pre-loader").css("display","none");
	        }, 2000);

			 
			
			$scope.unread = function() {
			//var notifiurl = "http://localhost:8085/collaborative-workflow/unreadnotifications.action";
			var notifiurl = "unreadnotifications.action";
			$http.get(notifiurl).success(function(response) {
				$scope.latestnoti = response.data;
			});
			};
		
			var spinner = $('#loadertwo');
			$scope.saveRoleAccess = function(){
				
				
				 var roleId = $('#roleId').val();
				 
				 var checkbox_value = "";
				 
				    
				    $("#form :checkbox").each(function () {
				        var ischecked = $(this).is(":checked");
				        if (ischecked) {
				            checkbox_value += $(this).val() + ",";
				        }
				    });
				   
				   
				    
				    var fdata = "checkedRoleAccess=" + checkbox_value
					+ "&roleId=" + roleId;
					if(checkbox_value == 0)
			
					alert("please select any one menu to continue?");
					spinner.show();
					$http(
							{
								method : 'POST',
								data : fdata,
								headers : {
									'Content-Type' : 'application/x-www-form-urlencoded'
								},
								url : 'save-role-access'
							}).then(function successCallback(response) {
								   setTimeout(function(){ spinner.hide(); 
								   	  }, 1000);
								//$scope.getRoleAccess();
								//$scope.getAccommodationAmenities1();
								var alert = ' <div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Alert!</h4>Success User Access have Been Provided Succesfully.</div>';
					            $('#message').html(alert);
								$timeout(function(){
						      	$('#btnclose').click();
						        }, 2000);
								
								//$('#btnclose').click();
					}, function errorCallback(response) {
						   setTimeout(function(){ spinner.hide(); 
						   	  }, 1000);
					  
						// called asynchronously if an error occurs
						// or server returns response with an error status.
					});
				//alert($('#taskId').val());
				//alert("ds");
				//var checkedValue = $('.flat-red:checked').val();
		        
				

			};
			
			
		 		
				$scope.getRoles = function() {
			        
		 			var url = "get-roles";
		 			$http.get(url).success(function(response) {		 			    
		 				$scope.role = response.data;
		 	
		 			});
		 		};
		 		
				/* $scope.getAccessRights = function() {
			        
		 			var url = "get-access-rights";
		 			$http.get(url).success(function(response) {		 			    
		 				$scope.accessrights = response.data;
		 	
		 			});
		 		}; */
		 		
			$scope.getRoleAccess = function(rowId) {
				
				//alert(rowId);
				document.getElementById("roleId").value = rowId;
			        
		 			var url = "get-role-access?roleId="+rowId;
		 			$http.get(url).success(function(response) {		 			    
		 				$scope.roleaccess = response.data;
		 	
		 			});
		 		};
		 		
           $scope.change = function() {
     	   
		        //alert($scope.id);
		        
		        var propertyId = $scope.id;	
     	        var url = "change-user-property?propertyId="+propertyId;
	 			$http.get(url).success(function(response) {
	 				
	 				 window.location = '/ulopms/dashboard'; 
	 				//$scope.change = response.data;
	 	
	 			});
			       
		 		};	 		
		 		
		 				
		    			
			$scope.getRoles();
			
			
			
			
			//$scope.unread();
			
			
			
		});

		//iCheck for checkbox and radio inputs
        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
          checkboxClass: 'icheckbox_minimal-blue',
          radioClass: 'iradio_minimal-blue'
        });
        //Red color scheme for iCheck
        $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
          checkboxClass: 'icheckbox_minimal-red',
          radioClass: 'iradio_minimal-red'
        });
        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
          checkboxClass: 'icheckbox_flat-green',
          radioClass: 'iradio_flat-green'
        });
		
	</script>
	
	 <script src="js/jquery-2.1.1.js" type="text/javascript"></script>
	  <!-- iCheck 1.0.1 -->
    <script src="plugins/iCheck/icheck.min.js"></script>
       