<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
   pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="s" uri="/struts-tags"%>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/themes/south-street/jquery-ui.min.css" />
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<style>
   table.table-bordered > tbody > tr > th {
   //border:1px solid #3c8dbc ;
   }
   table.table-bordered  {
   // border:1px solid #3c8dbc ;
   /*     margin-top:20px; */
   }
   table.table-bordered > thead > tr > th{
   //border:1px solid #3c8dbc ;
   }
   table.table-bordered > tbody > tr > td{
   //border:1px solid #3c8dbc ;
   }
   .tablesuccess{
   background-color: #092f53 !important;
   color:#ffffff !important;
   text-align:center;
   }
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Hotel Direct
      </h1>
      <ol class="breadcrumb">
         <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
         <li class="active">Hotel Direct</li>
      </ol>
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <div class="col-xs-12">
            <div class="nav-tabs-custom">
               <ul class="nav nav-tabs">
                  <li class="active"><a href="#fa-icons" data-toggle="tab">Availability</a></li>
                  <li ><a href="#tab-guest-information" data-toggle="tab">Guest Information</a></li>
                  <li ><a href="#tab-confirmation-summary" data-toggle="tab">Confirmation Summary</a></li>
               </ul>
               <div class="tab-content">
                  <!-- Font Awesome Icons -->
                  <div class="tab-pane active" id="fa-icons">
                     <section class="content">
                        <div class="row">
                           <form name="bookingForm">
                              <div class="form-group col-md-3">
                                 <label>Check-in:</label>
                                 <div class="input-group date">
                                    <label class="input-group-addon btn" for="bookCheckin">
                                    <span class="fa fa-calendar"></span>
                                    </label>   
                                    <input type="text" id="bookCheckin" type="text" class="form-control" name="bookCheckin"  value="" ng-required="true" onkeydown="return false" autocomplete="off">
                                 </div>
                                 <!-- /.input group -->
                              </div>
                              <div class="form-group col-md-3">
                                 <label>Check-out:</label>
                                 <div class="input-group date">
                                    <label class="input-group-addon btn" for="bookCheckout">
                                    <span class="fa fa-calendar"></span>
                                    </label>   
                                    <input type="text" id="bookCheckout" type="text" class="form-control" name="bookCheckout"  value="" ng-required="true"  onkeydown="return false" autocomplete="off">
                                 </div>
                                 <!-- /.input group -->
                              </div>
                              <div class="col-md-2">
                                 <button type="submit" ng-click="getAvailability()" class="btn btn-block btn-primary btn-sm" style="margin-top:25px">Search</button>
                              </div>
                           </form>
                        </div>
                        <div class="table-responsive no-padding"  >
                           <table class="table table-bordered" id="example1">
                              <h4>Availability</h4>
                              <tr class="tablesuccess">
                                 <th>Room Category</th>
                                 <th>Starting from</th>
                                 <th>Arrival Date</th>
                                 <th>Departure date </th>
                                 <th>Available</th>
                                 <th>Max Person </th>
                                 <!-- <th>Select Rooms</th> -->
                                 <th>Action</th>
                              </tr>
                              <tr ng-repeat="av in availablities track by $index">
                                 <td ng-model="accommodationType">{{av.accommodationType}}</td>
                                 <td ng-model="baseAmount">{{av.baseAmount}}</td>
                                 <td>{{av.arrivalDate}}</td>
                                 <td>{{av.departureDate}}</td>
                                 <td ng-model="rooms">{{av.available}}</td>
                                 <!--   //<td ng-model="rooms">{{av.noOfUnits}}</td> --> 
                                 <td ng-model="rooms"> Max {{av.maxOccupancy}} Person(s)</td>
                                 <!-- <td ng-model="rooms">{{av.rooms}}</td> -->
                                 <!--<td><select ng-model="r.value" ng-change="update(r.value,$index)"><option ng-repeat="r in range(1,1)" value="{{r}}">{{r}}</option></select></td>
                                    <!--  Selected Value is:{{rValue}}-->
                                 <!-- shiva add addrow(av.tax) -->
                                 <td>
                                    <a type="button" class= "btn btn-danger btn-style btn-sm" ng-show="av.available == 0 ">sold out</a>
                                    <button type="button" ng-hide="av.available == 0 " class="btn btn-success btn-style btn-sm"  class="btn btn-success btn-style" id="btn{{av.accommodationId}}"  ng-click="selectType(av.accommodationId,$index)" >SELECT ROOM</button>
                                    <input type='checkbox' name='type[]' style="visibility: hidden;"  value='{{av.accommodationId}}' id="{{av.accommodationId}}"  ng-click="manageTypes(av.accommodationId);"/>
                                 </td>
                                 <!-- <td><a class="btn btn-xs btn-primary" href="#addmodal" ng-disabled="av.available == 0"  ng-click="addRow(av.accommodationId,av.accommodationType,av.baseAmount,av.arrivalDate,av.departureDate,av.rooms,av.noOfAdults,av.noOfChild,av.minOccupancy,av.extraAdult,av.extraChild,av.tax,av.available,$index)"  data-toggle="modal" min="0" >Add</a></td> -->
                              </tr>
                              <tr>
                     
                              </tr>
                           </table>
                        </div>
                        <div class="table-responsive no-padding"  >
                           <table class="table table-bordered " id="finalData" >
                              <tr class="tablesuccess">
                                 <th style="white-space: nowrap; overflow: hidden;">Room Category</th>
                                 <th>Rooms</th>
                                 <th>Adult</th>
                                 <th>Child</th>
                                 <th style="white-space: nowrap; overflow: hidden;">Discount Amount</th>
                                <!--  <th>Infant</th> -->
                                 <!-- shiva add <th>tax</th> -->
                                 <th>Total</th>
                                 <th style="white-space: nowrap; overflow: hidden;">Tax Per</th>
                                 <th>Tax</th>
                                 <th>Total</th>
                              </tr>
                              <tr id="tr-{{$index}}" ng-repeat="rs in roomsSelected track by $index">
                                 <td>{{rs.accommodationType}}</td>
                                 <td width="20%">
                                    <div class="input-group">
                                       <span class="input-group-btn">
                                       <button type="button" class="btn btn-danger btn-number"  ng-click="minus($index,'rooms',rs.arrival,rs.departure,rs.minOccu,rs.maxOccu,rs.extraAdult,rs.extraInfant,rs.extraChild,rs.baseAmount,rs.totalBaseAmount,rs.diffDays,rs.taxPercentage)" ng-disabled="addroom <=0">
                                       <span class="glyphicon glyphicon-minus"></span>
                                       </button>
                                       </span>
                                       <input type="text" name="rooms"  class="form-control input-number rooms{{$index}}" value="1" min="1" max="10">
                                       <span class="input-group-btn">
                                       <button type="button" class="btn btn-success btn-number" ng-click="plus($index,'rooms',rs.available,rs.arrival,rs.departure,rs.minOccu,rs.maxOccu,rs.extraAdult,rs.extraInfant,rs.extraChild,rs.baseAmount,rs.totalBaseAmount,rs.diffDays,rs.taxPercentage)"  data-type="plus" data-field="quant">
                                       <span class="glyphicon glyphicon-plus"></span>
                                       </button>
                                       </span>
                                    </div>
                                 </td>
                                 <td width="20%">
                                    <div class="input-group">
                                       <span class="input-group-btn">
                                       <button type="button" class="btn btn-danger btn-number"  ng-click="minus($index,'adult',rs.arrival,rs.departure,rs.minOccu,rs.maxOccu,rs.extraAdult,rs.extraInfant,rs.extraChild,rs.baseAmount,rs.totalBaseAmount,rs.diffDays,rs.taxPercentage)" ng-disabled="addroom <=0">
                                       <span class="glyphicon glyphicon-minus"></span>
                                       </button>
                                       </span>
                                       <input type="text" name="adult"  class="form-control input-number adult{{$index}}" value="1" min="1" max="10">
                                       <span class="input-group-btn">
                                       <button type="button" class="btn btn-success btn-number"     ng-click="plus($index,'adult',100,rs.arrival,rs.departure,rs.minOccu,rs.maxOccu,rs.extraAdult,rs.extraInfant,rs.extraChild,rs.baseAmount,rs.totalBaseAmount,rs.diffDays,rs.taxPercentage)"  data-type="plus" data-field="quant">
                                       <span class="glyphicon glyphicon-plus"></span>
                                       </button>
                                       </span>
                                    </div>
                                 </td>
                                 <td width="20%">
                                    <div class="input-group">
                                       <span class="input-group-btn">
                                       <button type="button" class="btn btn-danger btn-number"  ng-click="minus($index,'child',rs.arrival,rs.departure,rs.minOccu,rs.maxOccu,rs.extraAdult,rs.extraInfant,rs.extraChild,rs.baseAmount,rs.totalBaseAmount,rs.diffDays,rs.taxPercentage)" ng-disabled="addroom <=0">
                                       <span class="glyphicon glyphicon-minus"></span>
                                       </button>
                                       </span>
                                       <input type="text" name="child"  class="form-control input-number child{{$index}}" value="0" min="1" max="10">
                                       <span class="input-group-btn">
                                       <button type="button" class="btn btn-success btn-number" ng-click="plus($index,'child',50,rs.arrival,rs.departure,rs.minOccu,rs.maxOccu,rs.extraAdult,rs.extraInfant,rs.extraChild,rs.baseAmount,rs.totalBaseAmount,rs.diffDays,rs.taxPercentage)"  data-type="plus" data-field="quant">
                                       <span class="glyphicon glyphicon-plus"></span>
                                       </button>
                                       </span>
                                    </div>
                                 </td>
                                 <td>
                                  <input type="text" name="resortDiscount" id="resortDiscount"  class="form-control input-number resortDiscount{{$index}}" ng-blur="editResortPrice($index,'resortDiscount')" value="" >
                                 </td>
                                 <td width="20%" style="display:none">
                                    <div class="input-group">
                                       <span class="input-group-btn">
                                       <button type="button" class="btn btn-danger btn-number"  ng-click="minus($index,'infant',rs.arrival,rs.departure,rs.minOccu,rs.maxOccu,rs.extraAdult,rs.extraInfant,rs.extraChild,rs.baseAmount,rs.totalBaseAmount,rs.diffDays,rs.taxPercentage)" ng-disabled="addroom <=0">
                                       <span class="glyphicon glyphicon-minus"></span>
                                       </button>
                                       </span>
                                       <input type="text" name="infant"  class="form-control input-number infant{{$index}}" value="0" min="1" max="10">
                                       <span class="input-group-btn">
                                       <button type="button" class="btn btn-success btn-number" ng-click="plus($index,'infant',10,rs.arrival,rs.departure,rs.minOccu,rs.maxOccu,rs.extraAdult,rs.extraInfant,rs.extraChild,rs.baseAmount,rs.totalBaseAmount,rs.diffDays,rs.taxPercentage)"  data-type="plus" data-field="quant">
                                       <span class="glyphicon glyphicon-plus"></span>
                                       </button>
                                       </span>
                                    </div>
                                 </td>
                                 <!-- shiva add <td>rs.tax</td> -->
                                 <td>{{rs.total}}</td>
                                 <td width="10%">
                                 <select name="taxPercentage" id="taxPercentage" ng-change="editResortTax($index,'taxPercentage')"  ng-model="taxPercentage" class="form-control input-number taxPercentage{{$index}}"> 
                                    <option value="selected">select</option>
                                    <option ng-repeat="t in taxes" value="{{t.taxPercantage}}" >{{t.taxPercantage | number}}</option> 
                                 </select>
                                 </td>
                                 <td>{{rs.tax}}</td>
                                 <td>{{rs.totalBaseAmount}} </td>
                              </tr>
                              <tr>
                                 <!-- <td></td>
                                    <td></td> -->
                                 <td style="border:none!important"></td>
                                 <td style="border:none!important"></td>
                                 <td style="border:none!important"></td>
                                 <td style="border:none!important"></td>
                                 <td style="border:none!important"></td>
                                
                                <!--  <td style="border:none!important"></td> -->
                                 <td width="15%"> 
                                  <td style="border:none!important"></td> 
                           
                                 </td>
                                 <td class="danger" width="10%">Sub Total:</td>
                                 <td class="danger"  id="totalAmount" >{{ getTotal()}} </td>
                                 <!--   <td class="danger" id="totalRooms">{{ getRoomsTotal() }} </td>
                                    <td><input type="hidden" value="0" id="tax"></td>
                                    <td><input type="hidden" value="0" id="securityDeposit"></td>-->
                                 <input type="hidden" id="bookingId" value="<s:property value="#session.bookingId" />" name="">
                              </tr>
                           </table>
                           <button class="btn btn-primary nextBtn pull-right" type="button">Next</button>
                        </div>
                     </section>
                     <!-- /.content -->
                  </div>
                  <!-- /.tab-content -->
                  <!-- guest information begins -->
                  <div class="tab-pane" id="tab-guest-information">
                     <form name="addguestinformation">
                        <div class="modal-body" >
                           <div class="row">
                              <div id="message"></div>
                              <div class="form-group col-sm-4">
                                 <label for="firstName">First Name <span style="color:red">*</span></label>
                                 <input type="text" class="form-control"  name="firstName"  id="firstName"  placeholder="First Name" ng-model='firstName' ng-pattern="/^[a-zA-Z\s]*$/" ng-required="true" maxlength="50">
                                 <span ng-show="addguestinformation.firstName.$error.pattern" style="color:red">Enter The Valid First Name</span>
                              </div>
                              <div class="form-group col-sm-4">
                                 <label for="lastName">Last Name <span style="color:red">*</span></label>
                                 <input type="text" class="form-control"  name="lastName"  id="lastName"  placeholder="Last Name" ng-model='lastName' ng-pattern="/^[a-zA-Z\s]*$/" ng-required="true" maxlength="50">
                                 <span ng-show="addguestinformation.lastName.$error.pattern" style="color:red">Enter The Valid Second Name!</span>
                              </div>
                              <div class="form-group col-sm-4">
                                 <label for="guestEmail">E-mail <span style="color:red">*</span></label>
                                 <input type="email" class="form-control"  name="guestEmail"  id="guestEmail"  placeholder="Enter Your Email" ng-model='guestEmail' ng-pattern="/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i" ng-required="true" maxlength="50">
                                 <span ng-show="addguestinformation.guestEmail.$error.pattern" style="color:red">Enter The Valid Email ID!</span>
                              </div>
                              <div class="form-group col-sm-4">
                                 <label for="mobile_phone">Mobile Phone <span style="color:red">*</span></label>
                                 <input type="text" class="form-control"  name="mobilePhone"  id="mobilePhone"  placeholder="Enter The Mobile Number" ng-model='mobilePhone' ng-pattern="/^[0-9]*$/" ng-required="true" maxlength="20">
                                 <span ng-show="addguestinformation.mobilePhone.$error.pattern" style="color:red">Enter the Valid Mobile Number!</span> 
                              </div>
                              <div class="form-group col-sm-4">
                                 <label for="guestCity">City <span style="color:red">*</span></label>
                                 <input type="text" class="form-control"  name="guestCity"  id="guestCity"  placeholder="City" ng-model='guestCity' ng-pattern="/^[a-zA-Z\s]*$/" ng-required="true" maxlength="50">
                                 <span ng-show="addguestinformation.guestCity.$error.pattern" style="color:red">Enter The City</span>     
                              </div>
                              <div class="form-group col-sm-4">
                                 <label for="guestZipcode">Postal Zipcode <span style="color:red">*</span></label>
                                 <input type="text" class="form-control"  name="guestZipcode" id="guestZipCode" placeholder="Zip Code" ng-model='guestZipcode' ng-pattern="/^[0-9]/" ng-required="true" maxlength="10">
                                 <span ng-show="addguestinformation.guestZipcode.$error.pattern" style="color:red">Enter The Valid POstal Code</span>
                              </div>
                              <div class="form-group col-sm-4">
                                 <label for="guestcountrycode">Country</label>
                                 <select name="countryCode" id="countryCode" value="{{property[0].countryCode}}" class="form-control">
                                    <option value="select">select</option>
                                    <option ng-repeat="c in countries" value="{{c.countryCode}}"  ng-selected ="property[0].countryCode == c.countryCode">{{c.countryName}}</option>
                                 </select>
                              </div>
                              <div class="form-group col-sm-4">
                                 <label>State</label>
                                 <select name="stateCode" id="stateCode" value="{{property[0].stateCode}}" class="form-control">
                                    <option value="select">select</option>
                                    <option ng-repeat="s in states" value="{{s.stateCode}}" ng-selected ="property[0].stateCode == s.stateCode">{{s.stateName}}</option>
                                 </select>
                              </div>
                              <div class="form-group col-sm-4">
                                 <label>Source</label>
                                 <select  class="form-control" name="sourceId"  id="sourceId" value="" class="form-control " ng-required="true">
                                    <option ng-repeat="sr in sources" value="{{sr.sourceId}}" ng-selected ="sr.sourceId == r.sourceId">{{sr.sourceName}}</option>
                                 </select>
                              </div>
                              <div class="form-group col-sm-4">
                                 <label for="firstName">Special Request <span style="color:red"></span></label>
                                 <input type="text" class="form-control"  name="specialRequest"  id="specialRequest"  placeholder="Special Request" ng-model='specialRequest' ng-pattern="/^[a-zA-Z\s]*$/" ng-required="true" maxlength="50">
                                 <span ng-show="addguestinformation.specialRequest.$error.pattern" style="color:red">Enter The Valid First Name</span>
                              </div>
                              <%-- <div class="form-group col-sm-4">
                                 <label for="guestcountrycode">Sub Total</label>
                                 <input type="text" value="{{ getTempTotal()}}" id="txtSubTotal" name="txtSubTotal" class="form-control" readonly>
                              </div>
                              <div class="form-group col-sm-4">
                                 <label>Discount</label>
                                 <select name="discount" id="discount"  ng-change="discountedGrossTotal()" ng-model="discount" class="form-control">
                                    <option value="select">select</option>
                                    <option ng-repeat="rd in resortdiscount" value="{{rd.discountId}}"  >{{rd.discountName}}</option>
                                 </select>
                              </div>
                              <div class="form-group col-sm-4">
                                 <label>Grand Total</label>
                                 <input type="text" value="{{ getTotal()}}" id="txtGrandTotal"  name ="txtGrandTotal" class="form-control" readonly >
                              </div> --%>
                              <!--   <div class="form-group col-sm-4">
                                 <input type="hidden" class="form-control"  name="paymentLink"  id="paymentLink"  placeholder="Enter The Payment Link">
                                     <br>
                                     <div ng-repeat="invo in invoice">
                                     <input type="hidden" class="form-control"  name="Link" value="{{invo.id}}" id="linkId" >
                                      </div>
                                     <button class="btn btn-primary pull-left" type="button"  ng-click="sendLink()">Send Link</button> 
                                      <button class="btn btn-primary pull-left" type="button"  ng-click="goLink()">get status</button> 
                                     </div> -->
                                  
                              <div class="col-sm-12 col-lg-12 col-md-12">
                                 <div ng-repeat="invo in invoice">
                                    <input type="hidden" class="form-control"  name="Link" value="{{invo.id}}" id="linkId" >
                                 </div>
                                 <!--  <input type="text" placeholder="Total"  class="form-control"> -->
                                 <button   href="#addmodal" ng-click=""  data-toggle="modal" class="btn btn-primary  text-center">Create Payment Link</button>
                                 <button class="btn btn-primary " type="button"  ng-click="goLink()">get status</button>
                                 <button ng-click="confirmBooking()" ng-disabled="addguestinformation.$invalid" class="btn btn-primary nextBtn">Pay At Hotel</button>
                              </div>
                           </div>
                        </div>
                        <div class="modal-footer">
                           <button class="btn btn-danger previousBtn pull-left " type="button" >Previous</button>  
                           <button class="btn btn-primary nextBtn" disabled>Next</button> 
                        </div>
                     </form>
                     <div class="modal" id="addmodal">
                        <div class="modal-dialog">
                           <div class="modal-content">
                              <div class="modal-header">
                                 <button type="button" id="btnclose" class="close" data-dismiss="modal"
                                    aria-hidden="true">x</button>
                                 <h4 class="modal-title">Create Payment Link</h4>
                              </div>
                              <form name="addItems">
                                 <div class="modal-bodys" >
                                    <div class="form-group col-md-12">
                                       <label>Amount: </label>
                                       <input type="text"  id="totalAmounts"   value="{{getTotal()}}"  name ="totalAmounts" class="form-control" >
                                    </div>
                                    <div class="form-group col-md-12">
                                       <label>Summary:</label>
                                       <textarea  class="form-control"  name="itemDescription"  id="itemDescription" value=""  placeholder="Description" required maxlength="250"></textarea>
                                    </div>
                                    <div class="form-group col-md-6">
                                       <label>Expiry Date:</label>
                                       <input type="text" class="form-control"  name="expiryDate"  id="expiryDate"  value="" placeholder="Select Date" required maxlength="250">
                                    </div>
                                    <div class="form-group col-md-6">
                                       <label>Customer Phone:</label>
                                       <input type="text" class="form-control"  id="itemMobile"  name="itemMobile"   placeholder="Mobile Number"  value="{{mobilePhone}}"  ng-pattern="/^[0-9]*$/" ng-required="true" maxlength="20">
                                       <span ng-show="addItems.itemMobile.$error.pattern" style="color:red">Not a valid Number !</span>
                                    </div>
                                    <div class="form-group col-md-12">
                                       <label>Customer Email:</label>
                                       <input type="text" class="form-control"  name="itemEmail"  id="itemEmail"  placeholder="E-mail" value="{{guestEmail}}" ng-pattern="/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i" ng-required="true" maxlength="50">
                                       <span ng-show="addItems.itemEmail.$error.pattern" style="color:red">Not a valid Email id !</span>
                                    </div>
                                 </div>
                                 <div class="modal-footer">
                                    <a href="#" data-dismiss="modal" class="btn btn-danger">Close</a>
                                    <button ng-click="createLink()" ng-disabled="addItems.$invalid" class="btn btn-primary">Send Payment Link</button>
                                 </div>
                              </form>
                           </div>
                           <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dalog -->
                     </div>
              
                  </div>
                  <!-- guest information begins -->
                 <div class="tab-pane" id="tab-confirmation-summary">
                     <div class="row" id="printer">
                        <form name="guestConfirmation">
                           <div class="modal-body">
                              <div id="message"></div>
                               <div class="invoice-box" style="max-width: 800px; margin: auto; padding: 30px; border: 1px solid #E5E5E5; box-shadow: 0 0 10px rgba(0, 0, 0, .15); font-size: 16px; line-height: 24px; font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; color: #555;">
         <table style="width: 100%; line-height: inherit;text-align: left;" cellpadding="0" cellspacing="0">
            <tr>
               <td class="title">
                  <a href="https://www.ulohotels.com" target="_blank">
                  <img alt="Logo" src="https://www.ulohotels.com/ulowebsite/images/ulo_logo2.png" style="display:block;font-family:Helvetica,Arial,sans-serif;color:#fff;font-size:16px;float:left;" border="0">
                  </a>
                  </br>
               </td>
               <td>
                  <h3 style="text-align:right;padding:5px;"><span style="color:#86B940">Booking</span><span style="color:#145E8F"> Voucher</span><br>
                     <span  style="color:#000;font-size:12px;">Voucher No&nbsp;:&nbsp;{{booked.data[0].bookingid}}<br>
                     Booking Id&nbsp;:&nbsp;{{booked.data[0].bookingid}}<br>
                     Booking Date&nbsp;:&nbsp;{{booked.data[0].timeHours}}</span>
                  </h3>
               </td>
            </tr>
         </table>
         <table>
            <span >
               <hr style="height:2px; border:none; color:#000; background-color:#115E92;margin-top:10px;">
            </span>
            <h4 style="color:#000;font-style:italic;line-height: 0px;font-size:14px;">Dear {{booked.data[0].guestname}},</h4>
            <span style="text-align:justify;font-style:italic;font-size:13px;">Thank you for choosing Ulo Hotels, Your booking is confirmed. Carry this voucher to the hotel. All booking details are provided below. Incase of any queries, Please revert back to <a href="mailto:support@ulohotels.com" target="_top">support@ulohotels.com</a></span>
            <h4 style="color:#125E90;font-style:italic;line-height: 0px;font-size:16px;">Booking Details</h4>
            <span >
               <hr style="height:2px; border:none; color:#000; background-color:#115E92;margin-top:10px;">
            </span>
         </table>
         <table  class="responsive-table"  style="border:1px solid #d6d4d4;padding:5px;" >
            <tr >
               <td style="font-size:14px;font-family:Helvetica,Arial,sans-serif;color:#727272;padding-bottom:0px;font-weight:bold;">Booking Id</td>
               <td  style="text-align:left;"><b>:</b>&nbsp;{{booked.data[0].bookingid}}</td>
            </tr>
            <tr>
               <td style="font-size:14px;font-family:Helvetica,Arial,sans-serif;color:#727272;padding-bottom:0px;font-weight:bold;">Name Of Guest</td>
               <td  style="text-align:left;"><b>:</b>&nbsp;{{booked.data[0].guestname}}</td>
            </tr>
            <tr >
               <td style="font-size:14px;font-family:Helvetica,Arial,sans-serif;color:#727272;padding-bottom:0px;font-weight:bold;">Email</td>
               <td  style="text-align:left;"><b>:</b>&nbsp;{{booked.data[0].emailId}}</td>
            </tr>
            <tr>
               <td style="font-size:14px;font-family:Helvetica,Arial,sans-serif;color:#727272;padding-bottom:0px;font-weight:bold;">Mobile</td>
               <td  style="text-align:left;"><b>:</b>&nbsp;{{booked.data[0].mobile}}</td>
            </tr>
            <tr>
               <td colspan="2" style="font-size:15px;font-family:Helvetica,Arial,sans-serif;color:#000;padding-bottom:0px;font-weight:600;text-decoration:underline;">Room Details</td>
            </tr>
			   <tr >
               <td style="font-size:14px;font-family:Helvetica,Arial,sans-serif;color:#727272;padding-bottom:0px;font-weight:bold;">Hotel Name</td>
               <td  style="text-align:left;"><b>:</b>&nbsp;{{booked.data[0].propertyName}}</td>
            </tr>
            <tr >
               <td style="font-size:14px;font-family:Helvetica,Arial,sans-serif;color:#727272;padding-bottom:0px;font-weight:bold;">Source</td>
               <td  style="text-align:left;"><b>:</b>&nbsp;{{booked.data[0].sourcename}}</td>
            </tr>
            <tr>
               <td style="font-size:14px;font-family:Helvetica,Arial,sans-serif;color:#727272;padding-bottom:0px;font-weight:bold;">Check-In</td>
               <td  style="text-align:left;"><b>:</b>&nbsp;{{booked.data[0].checkIn}}</td>
            </tr>
            <tr>
               <td style="font-size:14px;font-family:Helvetica,Arial,sans-serif;color:#727272;padding-bottom:0px;font-weight:bold;">Check-Out</td>
               <td  style="text-align:left;"><b>:</b>&nbsp;{{booked.data[0].checkOut}} </td>
            </tr>
            <tr>
               <td style="font-size:14px;font-family:Helvetica,Arial,sans-serif;color:#727272;padding-bottom:0px;font-weight:bold;">No-Of-Nights</td>
               <td  style="text-align:left;"><b>:</b>&nbsp;{{booked.data[0].days}} Nights</td>
            </tr>
            <!-- <tr>
               <td style="font-size:14px;font-family:Helvetica,Arial,sans-serif;color:#727272;padding-bottom:0px;font-weight:bold;white-space: nowrap; overflow: hidden;">Room Category:</td>
               <td  style="text-align:left;">{{booked.data[0].accommodationType}},{{booked.data[1].accommodationType}},{{booked.data[2].accommodationType}},
               {{booked.data[3].accommodationType}},{{booked.data[4].accommodationType}},{{booked.data[5].accommodationType}},
               {{booked.data[6].accommodationType}},{{booked.data[7].accommodationType}}</td>
            </tr> -->
            <tr>
               <td style="font-size:14px;font-family:Helvetica,Arial,sans-serif;color:#727272;padding-bottom:0px;font-weight:bold;">No-Of-Rooms</td>
               <td  style="text-align:left;"><b>:</b>&nbsp;{{booked.data[0].rooms}}</td>
            </tr>
            <tr>
               <td style="font-size:14px;font-family:Helvetica,Arial,sans-serif;color:#727272;padding-bottom:0px;font-weight:bold;">Adult</td>
               <td  style="text-align:left;"><b>:</b>&nbsp;{{booked.data[0].adults}}</td>
            </tr>
            <tr>
               <td style="font-size:14px;font-family:Helvetica,Arial,sans-serif;color:#727272;padding-bottom:0px;font-weight:bold;">Child</td>
               <td  style="text-align:left;"><b>:</b>&nbsp;{{booked.data[0].child}}</td>
            </tr>
            <tr>
               <td style="font-size:14px;font-family:Helvetica,Arial,sans-serif;color:#727272;padding-bottom:0px;font-weight:bold;">BreakFast</td>
               <td  style="text-align:left;"><b>:</b>&nbsp;Yes</td>
            </tr>
            <tr>
               <td style="font-size:14px;font-family:Helvetica,Arial,sans-serif;color:#727272;padding-bottom:0px;font-weight:bold;white-space: nowrap; overflow: hidden;">Offers Applied</td>
               <td  style="text-align:left;"><b>:</b>&nbsp;{{booked.data[0].promotionName}}</td>
            </tr>
            <tr>
               <td style="font-size:14px;font-family:Helvetica,Arial,sans-serif;color:#727272;padding-bottom:0px;font-weight:bold;white-space: nowrap; overflow: hidden;">Coupon Applied</td>
               <td style="text-align:left;"><b>:</b>&nbsp;{{booked.data[0].propertyDiscountName}}</td>
            </tr>
            <tr>
               <td style="font-size:14px;font-family:Helvetica,Arial,sans-serif;color:#727272;padding-bottom:0px;font-weight:bold;white-space: nowrap; overflow: hidden;">Payment Status</td>
               <td  style="text-align:left;"><b>:</b>&nbsp;Paid</td>
            </tr>
            <tr>
               <td style="font-size:14px;font-family:Helvetica,Arial,sans-serif;color:#727272;padding-bottom:0px;font-weight:bold;">Special Request</td>
               <td  style="text-align:left;"><b>:</b>&nbsp;{{booked.data[0].specialrequest}}</td>
            </tr>
         </table>
         <h4 style="color:#125E90;font-style:italic;line-height: 0px;font-size:16px;">Payment Details</h4>
         <span >
            <hr style="height:2px; border:none; color:#000; background-color:#115E92;margin-top:10px;">
         </span>
         <div class="invoice" style="padding:10px;">
            <table width="100%"  style=" border-collapse: collapse;border:1px solid #d6d4d4;border-bottom:none;" cellpadding="0" cellspacing="0" class="responsive-table">
               <tr style="border-bottom:1px solid #d6d4d4;padding:5px;" >
                  <td style="text-align:center;font-size:13px;font-weight:600;padding:2px;">S.NO</td>
                  <td style="text-align:center;font-size:13px;font-weight:600;padding:2px;">Categories</td>
                  <td style="text-align:center;font-size:13px;font-weight:600;padding:2px;">Price</td>
                  <td style="text-align:center;font-size:13px;font-weight:600;padding:2px;">Tax</td>
                  <td style="text-align:center;font-size:13px;font-weight:600;padding:2px;">Total Amount</td>
               </tr>
			   <!-- <#list accommodationBooked as booked> -->              
               <tr ng-repeat = "types in booked.data[0].types">
                  <td style="text-align:center;">{{1}}</td>
                  <td style="text-align:center;color:#000;font-weight:600;padding:2px;"><span >{{types.rooms}} {{types.accommodationType}} </span><span style="font-size:12px;color:#727272;">({{types.adultCount}} Adult / {{types.childCount}} child)</span></td>
                  <td style="text-align:center;color:#000;font-weight:600;padding:2px;">{{types.amount}}</td>
                  <td style="text-align:center;color:#000;font-weight:600;padding:2px;">{{types.tax}}</td>
                  <td style="text-align:center;color:#000;font-weight:600;padding:2px;">{{types.total}}</td>
               </tr >
               <tr ng-repeat = "types in booked.data[1].types">
                  <td style="text-align:center;">{{2}}</td>
                  <td style="text-align:center;color:#000;font-weight:600;padding:2px;"><span >{{types.rooms}} {{types.accommodationType}} </span><span style="font-size:12px;color:#727272;">({{types.adultCount}} Adult / {{types.childCount}} child)</span></td>
                  <td style="text-align:center;color:#000;font-weight:600;padding:2px;">{{types.amount}}</td>
                  <td style="text-align:center;color:#000;font-weight:600;padding:2px;">{{types.tax}}</td>
                  <td style="text-align:center;color:#000;font-weight:600;padding:2px;">{{types.total}}</td>
               </tr >
                <tr ng-repeat = "types in booked.data[2].types">
                  <td style="text-align:center;">{{3}}</td>
                  <td style="text-align:center;color:#000;font-weight:600;padding:2px;"><span >{{types.rooms}} {{types.accommodationType}} </span><span style="font-size:12px;color:#727272;">({{types.adultCount}} Adult / {{types.childCount}} child)</span></td>
                  <td style="text-align:center;color:#000;font-weight:600;padding:2px;">{{types.amount}}</td>
                  <td style="text-align:center;color:#000;font-weight:600;padding:2px;">{{types.tax}}</td>
                  <td style="text-align:center;color:#000;font-weight:600;padding:2px;">{{types.total}}</td>
               </tr >
               <tr ng-repeat = "types in booked.data[3].types">
                  <td style="text-align:center;">{{4}}</td>
                  <td style="text-align:center;color:#000;font-weight:600;padding:2px;"><span >{{types.rooms}} {{types.accommodationType}} </span><span style="font-size:12px;color:#727272;">({{types.adultCount}} Adult / {{types.childCount}} child)</span></td>
                  <td style="text-align:center;color:#000;font-weight:600;padding:2px;">{{types.amount}}</td>
                  <td style="text-align:center;color:#000;font-weight:600;padding:2px;">{{types.tax}}</td>
                  <td style="text-align:center;color:#000;font-weight:600;padding:2px;">{{types.total}}</td>
               </tr >
               <tr ng-repeat = "types in booked.data[4].types">
                  <td style="text-align:center;">{{5}}</td>
                  <td style="text-align:center;color:#000;font-weight:600;padding:2px;"><span >{{types.rooms}} {{types.accommodationType}} </span><span style="font-size:12px;color:#727272;">({{types.adultCount}} Adult / {{types.childCount}} child)</span></td>
                  <td style="text-align:center;color:#000;font-weight:600;padding:2px;">{{types.amount}}</td>
                  <td style="text-align:center;color:#000;font-weight:600;padding:2px;">{{types.tax}}</td>
                  <td style="text-align:center;color:#000;font-weight:600;padding:2px;">{{types.total}}</td>
               </tr >
               <tr ng-repeat = "types in booked.data[5].types">
                  <td style="text-align:center;">{{6}}</td>
                  <td style="text-align:center;color:#000;font-weight:600;padding:2px;"><span >{{types.rooms}} {{types.accommodationType}} </span><span style="font-size:12px;color:#727272;">({{types.adultCount}} Adult / {{types.childCount}} child)</span></td>
                  <td style="text-align:center;color:#000;font-weight:600;padding:2px;">{{types.amount}}</td>
                  <td style="text-align:center;color:#000;font-weight:600;padding:2px;">{{types.tax}}</td>
                  <td style="text-align:center;color:#000;font-weight:600;padding:2px;">{{types.total}}</td>
               </tr >
               <tr ng-repeat = "types in booked.data[6].types">
                  <td style="text-align:center;">{{7}}</td>
                  <td style="text-align:center;color:#000;font-weight:600;padding:2px;"><span >{{types.rooms}} {{types.accommodationType}} </span><span style="font-size:12px;color:#727272;">({{types.adultCount}} Adult / {{types.childCount}} child)</span></td>
                  <td style="text-align:center;color:#000;font-weight:600;padding:2px;">{{types.amount}}</td>
                  <td style="text-align:center;color:#000;font-weight:600;padding:2px;">{{types.tax}}</td>
                  <td style="text-align:center;color:#000;font-weight:600;padding:2px;">{{types.total}}</td>
               </tr >
               <tr ng-repeat = "types in booked.data[7].types">
                  <td style="text-align:center;">{{8}}</td>
                  <td style="text-align:center;color:#000;font-weight:600;padding:2px;"><span >{{types.rooms}} {{types.accommodationType}} </span><span style="font-size:12px;color:#727272;">({{types.adultCount}} Adult / {{types.childCount}} child)</span></td>
                  <td style="text-align:center;color:#000;font-weight:600;padding:2px;">{{types.amount}}</td>
                  <td style="text-align:center;color:#000;font-weight:600;padding:2px;">{{types.tax}}</td>
                  <td style="text-align:center;color:#000;font-weight:600;padding:2px;">{{types.total}}</td>
               </tr >
               <!-- </#list> -->
               <tbody>
                  <tr  style="border-top:1px solid #d6d4d4;">
                     <td>&nbsp;</td>
                     <td>&nbsp;</td>
                     <td>&nbsp;</td>
                     <td>&nbsp;</td>
                     <td style="text-align:center;color:#000;font-weight:600;background-color:#F7F183;font-size:15px;padding:5px;">Total Amount&nbsp;:&nbsp;{{booked.data[0].totalamount}} </td>
                  </tr>
               </tbody>
            </table>
            <table width="100%"  style=" border-collapse:collapse;border:1px solid #d6d4d4;" cellpadding="0" cellspacing="0" class="responsive-table">
               <tbody>
                  <tr >
                     <td style="text-align:left;color:#000;font-size:13px;background-color:#E5E5E5;font-weight:normal;padding:5px">GST&nbsp;:&nbsp;33AABCU9979Q2Z8 <br>CIN&nbsp;:&nbsp;U55100TN2016PTC113073 <br>PAN&nbsp;:&nbsp;AABCU9979Q <br>Service Category-Hotel Service</td>
                     <td >&nbsp;</td>
                     <td >&nbsp;</td>
                     <td >&nbsp;</td>
                     <%-- <td style="text-align:center;color:#145E8F;font-weight:600;padding:5px;font-size:15px;">Advance:15000 <br> <span style="text-align:center;color:#FF0000;font-weight:600;white-space: nowrap; overflow: hidden;font-size:15px;">Hotel Pay:15000</span></td> --%>
                  </tr>
               </tbody>
            </table>
         </div>
         <h4 style="color:#125E90;font-style:italic;line-height: 0px;font-size:15px;padding:5px;">Contact Details</h4>
         <span >
            <hr style="height:2px; border:none; color:#000; background-color:#115E92;margin-top:10px;">
         </span>
         <p style="padding:5px;color:#727272;font-weight:bold;font-size:14px;text-decoration:underline;text-align:left;">Property Details:</p>
         <table width="100%"  style=" border-collapse: collapse;border:0px solid #d6d4d4;" cellpadding="0" cellspacing="0" class="responsive-table">
            <tr>
               <td style="color:#727272;font-weight:normal;text-align:left;font-size:12px;padding:2px;" >{{booked.data[0].propertyName}}<br>{{booked.data[0].address1}} , <br> {{booked.data[0].address2}}<br> {{booked.data[0].address2}}</td>
               <td style="color:#727272;font-weight:normal;text-align:left;border-left: 1px solid #d6d4d4;font-size:12px;padding:2px; " ><span style="font-weight:600;">&nbsp;Contact:</span> {{booked.data[0].phoneNumber}}<br><span style="font-weight:600;">&nbsp;Mail:</span> {{booked.data[0].propertyEmail}}</td>
               <td style="color:#145E8F;font-weight:normal;text-align:left;text-decoration:underline;border-left: 1px solid #d6d4d4;padding:2px;;font-size:12px;white-space: nowrap; overflow: hidden;" ><a href="https://www.google.com/maps/?q=13.0423,80.2168" target="_blank">&nbsp;View Map<img src="https://www.ulohotels.com/ulowebsite/images/invoice/invoicemap.png" alt="gallery"></a><br><a href="" target="_blank">&nbsp;Photo Gallery</a></td>
            </tr>
         </table>
		   <span >
            <hr style="height:1px; border:none; color:#000; background-color:#115E92;margin-top:5px;">
         </span>
         <p style="padding:5px;color:#727272;font-weight:bold;font-size:14px;text-decoration:underline;text-align:left;">Reservation Details:</p>
         <table name=bordertable width="100%" cellpadding="0" cellspacing="0" style=" border-collapse: collapse;border:none;padding:5px;">
            <tr>
               <td style="color:#727272;font-weight:normal;text-align:left;font-size:12px;"><span style="font-weight:600;">Contact:</span> {{booked.data[0].reservationManagerContact}}<br><span style="font-weight:600;">Mail:</span> {{booked.data[0].reservationManagerEmail}}</td>
            </tr>
         </table>
         <h4 style="color:#125E90;font-style:italic;line-height: 0px;font-size:15px;padding:5px;">Note:</h4>
         <span >
            <hr style="height:2px; border:none; color:#000; background-color:#115E92;margin-top:5px;">
         </span>
         <span style="font-size:12px;">The Check-In and Check-out Time is 10:00 AM <br>Free cancellation before 72 Hrs, If cancelled before 48 Hrs, 20 percentage has been deducted from booking amount, and in-case cancelled check-in date before 48 Hrs non refundable.<br> Cancellation refund process takes 10 to 12 Working Days.<br>To know more read the hotel policies to click <a href="https://www.ulohotels.com/termsandconditions" target="_blank" >Terms And Conditions</a> </span>
         <p style="font-size:12px;text-align:center;font-weight:bold;">* This Computer Generated Voucher,Here No Signature Required.</p>
         <p style="font-size:12px;background-color:#E5E5E5;padding:5px;"><span><img src="https://www.ulohotels.com/ulowebsite/images/invoice/footmail.png" align="top"> support@ulohotels.com</span><img src="https://www.ulohotels.com/ulowebsite/images/invoice/footcall.png" align="top">9543592593<img src="https://www.ulohotels.com/ulowebsite/images/invoice/footweb.png" align="top" >www.ulohotels.com</p>
      </div>
</div>

                              </div>
                                    <div class="modal-footer">
                              <div class="col-lg-12"></div>
                              <button class="btn btn-danger previousBtn pull-left" type="button" >Previous</button>
                              <button  ng-disabled="guestConfirmation.$invalid" class="btn btn-primary nextBtn" onclick="javascript:printDiv('printer')">Print</button>
                        <!--       <button class="btn btn-primary nextBtn pull-right" type="button"  ng-click="getBookedDetails()">Book Now</button> -->
                           </div>
                           </div>
               </div>
               <!-- /.nav-tabs-custom -->
            </div>
            <!-- /.col -->
         </div>
         <!-- /.row -->
      </div>
   </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- <button id="rzp-button1">Pay</button> -->
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script>
   var link = {
   	
   	  "customer": {
   	    "name": "Test",
   	    "email": "test@test.com",
   	    "contact": "9999999999"
   	  },
   	  
   	  "type": "link",
   	  "view_less": 1,
   	  "amount": 2000,
   	  "currency": "INR",
   	  "description": "Welcome to Ulohotels",
//    	  "key": "rzp_test_bQQGkpEPtwppNI",
     "key": "rzp_live_vuYF9peaDXMoQF",
   	   "name": "Merchant Name",
   	   
   	  "image": "/your_logo.png",
   	  "expire_by": 1493630556
   	
   	
   	
   };
   var options = {
//    	"key": "rzp_test_bQQGkpEPtwppNI",
		"key": "rzp_live_vuYF9peaDXMoQF",
       "amount": "2000", // 2000 paise = INR 20
       "name": "Merchant Name",
       "description": "Purchase Description",
       "image": "/your_logo.png",
       "handler": function (response){
           alert(response.razorpay_payment_id);
       },
       "prefill": {
           "name": "Harshil Mathur",
           "email": "harshil@razorpay.com"
       },
       "notes": {
           "address": "Hello World"
       },
       "theme": {
           "color": "#F37254"
       }
   };
   var rzp1 = new Razorpay(link);
   
   document.getElementById('rzp-button1').onclick = function(e){
       rzp1.open();
       e.preventDefault();
   }
</script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>
<script src="js/jquery-2.1.1.js" type="text/javascript"></script>
<script>
   $('.datepicker').datepicker({
       dateFormat: 'dd-mm-yy'
   });
</script>
<script>
   var app = angular.module('myApp', ['ngProgress']);
   app.controller('customersCtrl', function($scope, $http,$timeout,ngProgressFactory,$filter) {
   
	   var spinner = $('#loadertwo');
   
         	$timeout(function(){
        	    // $scope.progressbar.complete();
              $scope.show = true;
              $("#pre-loader").css("display","none");
          }, 2000);
   
          $("#bookCheckin").datepicker({
              dateFormat: 'MM d, yy',
            
//               minDate:  0,
              onSelect: function (formattedDate) {
                  var date1 = $('#bookCheckin').datepicker('getDate'); 
                  var date = new Date( Date.parse( date1 ) ); 
                  date.setDate( date.getDate() + 1 );        
                  var newDate = date.toDateString(); 
                  newDate = new Date( Date.parse( newDate ) );   
                  $('#bookCheckout').datepicker("option","minDate",newDate);
                  $timeout(function(){
                    //scope.checkIn = formattedDate;
                	  document.getElementById("bookCheckin").style.borderColor = "LightGray";
                  });
              }
          });
   
          $("#bookCheckout").datepicker({
              dateFormat: 'MM d, yy',
            
//               minDate:  0,
              onSelect: function (formattedDate) {
                  var date2 = $('#bookCheckout').datepicker('getDate'); 
                  $timeout(function(){
                    //scope.checkOut = formattedDate;
                	  document.getElementById("bookCheckout").style.borderColor = "LightGray";
                  });
              }
          }); 
   
   $scope.stdPrice =  0;
   $scope.unread = function() {
   //var notifiurl = "http://localhost:8085/collaborative-workflow/unreadnotifications.action";
   var notifiurl = "unreadnotifications.action";
   $http.get(notifiurl).success(function(response) {
   $scope.latestnoti = response.data;
   });
   };
   
   
   
    $scope.getAvailability = function(){
    	
    	
    	$("#example1 :checkbox").each(function () { 
	        var ischecked = $(this).is(":checked");
	        if (ischecked) {
	        	
	        	//alert($(this).val());
	        	
	            checkbox_value = $(this).val() + ",";
	            
	            //alert("check"+checkbox_value);
	            
	            var text = document.getElementById("btn"+$(this).val()).firstChild;
	            //alert("test"+text)
                //text.data = text.data == "SELECT ROOM" ? "Selected" : "SELECT ROOM";
                 
                if(document.getElementById($(this).val()).checked){
                
                
                document.getElementById($(this).val()).checked = false;
                
                text.data = "SELECT ROOM";
                document.getElementById("btn"+$(this).val()).classList.remove("actives");
               // $scope.manageTypes(id);
                
                }
                
	            //alert(checkbox_value);
	        }
	    });
    	
   
	   var fdata = "&strStartDate=" + $('#bookCheckin').val()
   	+ "&strEndDate=" + $('#bookCheckout').val();
	   
   if(document.getElementById('bookCheckin').value==""){
		 document.getElementById('bookCheckin').focus();
		 document.getElementById("bookCheckin").style.borderColor = "red";
	 }else if(document.getElementById('bookCheckout').value==""){
		 document.getElementById('bookCheckout').focus();
		document.getElementById("bookCheckout").style.borderColor = "red";
	 }else if(document.getElementById('bookCheckin').value!="" && document.getElementById('bookCheckout').value!=""){
		 spinner.show();
		   $http(
		   {
			   method : 'POST',
			   data : fdata,
			   headers : {
			   'Content-Type' : 'application/x-www-form-urlencoded'
			   },
			   url : 'get-new-offline-room-availability'
			   }).success(function(response) {
			   
			   $scope.availablities = response.data;
			   console.log($scope.availablities);
			   spinner.hide();
			   if( $scope.roomsSelected != ''){
	        		//alert("enter");
	        		//alert("length"+$scope.roomsSelected.length)
	        		$scope.roomsSelected.splice(0,$scope.roomsSelected.length);
	        		 
	        	}
		   
		   });
	 }
   
   };
   
   
   
   
   $scope.plus = function(indx,className,limit,arrivalDate,departureDate,minOccupancy,maxOccupancy,extraAdult,extraInfant,extraChild,baseAmount,totalBaseAmount,diffDays,taxPercentage) {
   		var currentVal =  parseInt($('.'+className+indx).val());
         	   // If is not undefined
                if (!isNaN(currentVal)) {
               	 
               	if(currentVal >= limit){
               	
               	$('.'+className+indx).val(currentVal);
               	//alert("Sorry You Are Exceeding The Limit");
               	 
                }
               	else{
                    // Increment
                    $('.'+className+indx).val(currentVal + 1);
                    
               	}
                } 
         	   
         	   
               else {
                    // Otherwise put a 0 there
                    $('.'+className+indx).val(0);
                }
         	   
                var rooms = parseInt($('.rooms'+indx).val());
            var adult = parseInt($('.adult'+indx).val());
            var child = parseInt($('.child'+indx).val());
            var infant = parseInt($('.infant'+indx).val());
            
            
            var diffDays = parseInt(diffDays);
          
            var minOccu = parseInt(minOccupancy*rooms);
            
            var maxOccu = parseInt(maxOccupancy*rooms);
            //alert("max"+maxOccu)
            
            var extraAdult = parseInt(extraAdult);
            var extraInfant = parseInt(extraInfant);
            var extraChild = parseInt(extraChild);
            var baseAmount= parseFloat(baseAmount);
            var totalOccu = parseInt(adult+child+infant);
            //alert("total"+totalOccu)
            var total = (rooms*baseAmount);
            var roomAmount = parseFloat(total);
            var adltChd = parseInt(adult+child); 
            
            var adltChdIft = parseInt(adult+child+infant); 
            $scope.roomsSelected[indx].rooms = rooms;
            $scope.roomsSelected[indx].total = $scope.roomsSelected[indx].baseAmount * rooms;
            $scope.roomsSelected[indx].tempTotal = $scope.roomsSelected[indx].baseAmount * rooms;
            $scope.roomsSelected[indx].adultsCount = adult;
            $scope.roomsSelected[indx].childCount = child;
            $scope.roomsSelected[indx].infantCount = infant;
           
            
            
            
            if(totalOccu > maxOccu){
            
           	alert("Running out of Maximum Occupancy");
           	 
           	$('.adult'+indx).val(minOccu);
           	$('.child'+indx).val(0);
           	$('.infant'+indx).val(0);
           	 
            $scope.roomsSelected[indx].adultsCount = 1;
            $scope.roomsSelected[indx].childCount = 0;
            $scope.roomsSelected[indx].infantCount = 0;
            $scope.roomsSelected[indx].total = $scope.roomsSelected[indx].baseAmount * rooms;
            $scope.roomsSelected[indx].tempTotal = $scope.roomsSelected[indx].baseAmount * rooms;
           	//finTotal = 0;
           	// $(this).val(currentVal-1);
           	//$scope.roomsSelected[indx].adultsCount = currentVal-1;
           
            }
            
            else{
            if(adult>minOccu){
           	var adultsLeft = (adult-minOccu);
          	$scope.roomsSelected[indx].total =  (adultsLeft * extraAdult * diffDays + roomAmount);
           	$scope.roomsSelected[indx].tempTotal =  (adultsLeft * extraAdult * diffDays + roomAmount);
           	//alert("adult" +adultsLeft);
            }
            
            else if(child>minOccu){
           	var childLeft = (child-minOccu);
           	$scope.roomsSelected[indx].total =  ( childLeft * extraChild * diffDays + roomAmount);
        	$scope.roomsSelected[indx].tempTotal =  ( childLeft * extraChild * diffDays+ roomAmount);
           	// alert("child" +childLeft);
            }
            
            else if(infant>minOccu){
           	var infantLeft = (infant-minOccu);
           	$scope.roomsSelected[indx].total =  (infantLeft * extraInfant * diffDays + roomAmount);
           	$scope.roomsSelected[indx].tempTotal =  ( infantLeft * extraInfant * diffDays + roomAmount);
           	//alert("infant" +infantLeft);
            }
                       
            else if((adult+child) > minOccu){
           	var childLeft = (adltChd-minOccu);
           	$scope.roomsSelected[indx].total =  ( childLeft * extraChild * diffDays + roomAmount);
        	$scope.roomsSelected[indx].tempTotal =  (childLeft * extraChild * diffDays + roomAmount);
           	// alert("child" +childLeft);
            }
                       
            else if((adult+child+infant) > minOccu && infant >0){
           	var infantLeft = (adltChdIft-minOccu);
           	$scope.roomsSelected[indx].total =  (infantLeft * extraInfant  * diffDays + roomAmount);
           	$scope.roomsSelected[indx].tempTotal =  ( infantLeft * extraInfant * diffDays + roomAmount);
            }
            
            }
            var finTotal = $scope.roomsSelected[indx].total;
            //alert("final"+finTotal)
            
             $scope.roomsSelected[indx].tax =  Math.round(taxPercentage / 100 * finTotal)  ;
            //alert("taxes"+ $scope.roomsSelected[indx].tax)
              /* if(finTotal < 1000){
                        //alert("dsfsd");
                        var percent = 0;
                        var taxes = 0/100*finTotal* diffDays;
                        $scope.roomsSelected[indx].tax = taxes; 
                        //alert(taxes);
                    }
                    else if(finTotal >= 1000 && finTotal <= 2500){
                        //alert("dsfsd");
                        var percent = 12;
                        //var taxes = 12/100*amount* diffDays;
                        var taxes = Math.round(percent / 100 * finTotal) * diffDays;
                        $scope.roomsSelected[indx].tax = taxes; 
                        //alert(taxes);
                    }
                    else if(finTotal >= 2500 && finTotal <= 7000){
                        //alert("dsfsd");
                        var percent = 18;
                        var taxes = Math.round(percent / 100 * finTotal) * diffDays;
                        $scope.roomsSelected[indx].tax = taxes; 
                        //alert(taxes);
                    }
                    else if(finTotal >= 7000 && finTotal <= 7000000){
                        //alert("dsfsd");
                        var percent = 28;
                        //var taxes = Math.round(percent)/100 *amount* diffDays;
                       var taxes = Math.round(percent / 100 * finTotal) * diffDays;
                       $scope.roomsSelected[indx].tax = taxes; 
                        //alert( taxes);
                    } */
              
              $scope.roomsSelected[indx].totalBaseAmount = $scope.roomsSelected[indx].tax +  $scope.roomsSelected[indx].total;
                   // alert("totalbase"+ $scope.roomsSelected[indx].totalBaseAmount)
   }
   	
   	
                $scope.minus = function(indx,className,arrivalDate,departureDate,minOccupancy,maxOccupancy,extraAdult,extraInfant,extraChild,baseAmount,totalBaseAmount,diffDays,taxPercentage) {
   
			   	var currentVal =  parseInt($('.'+className+indx).val());
			   	var limit = 0;
         	   // If is not undefined
                if (!isNaN(currentVal)) {
                    // Increment
                   
                    if(currentVal <= limit){
               	
               	$('.'+className+indx).val(currentVal);
               	
               	 
                }
               	else{
                    // Increment
               	   $('.'+className+indx).val(currentVal - 1);
                    
               	}
                    
                } else {
                    // Otherwise put a 0 there
                    $('.'+className+indx).val(0);
                }
         	   
         	   
                var rooms = parseInt($('.rooms'+indx).val());
            var adult = parseInt($('.adult'+indx).val());
            var child = parseInt($('.child'+indx).val());
            var infant = parseInt($('.infant'+indx).val());
           
            var diffDays = parseInt(diffDays);
            var minOccu = parseInt(minOccupancy*rooms);
            var maxOccu = parseInt(maxOccupancy*rooms);
            var extraAdult = parseInt(extraAdult);
            var extraInfant = parseInt(extraInfant);
            var extraChild = parseInt(extraChild);
            var baseAmount = parseFloat(baseAmount);
            var totalOccu = parseInt(adult+child+infant);
            var total = (rooms*baseAmount);
            var roomAmount = parseFloat(total);
            var adltChd = parseInt(adult+child); 
            
            var adltChdIft = parseInt(adult+child+infant); 
           // alert(adltChdIft);
            $scope.roomsSelected[indx].rooms = rooms;
            $scope.roomsSelected[indx].total = $scope.roomsSelected[indx].baseAmount * rooms;
            $scope.roomsSelected[indx].tempTotal = $scope.roomsSelected[indx].baseAmount * rooms;
            $scope.roomsSelected[indx].adultsCount = adult;
            $scope.roomsSelected[indx].childCount = child;
            $scope.roomsSelected[indx].infantCount = infant;
            var finTotal = $scope.roomsSelected[indx].total;
           
            if(totalOccu > maxOccu){
            
           	alert("Running out of Maximum Occupancy");
           	 
           	$('.adult'+indx).val(1);
           	$('.child'+indx).val(0);
           	$('.infant'+indx).val(0);
           	 
           	$scope.roomsSelected[indx].adultsCount = 1;
            $scope.roomsSelected[indx].childCount = 0;
            $scope.roomsSelected[indx].infantCount = 0;
            $scope.roomsSelected[indx].total = $scope.roomsSelected[indx].baseAmount * rooms;
            $scope.roomsSelected[indx].tempTotal = $scope.roomsSelected[indx].baseAmount * rooms;
           	//finTotal = 0;
           	// $(this).val(currentVal-1);
           	//$scope.roomsSelected[indx].adultsCount = currentVal-1;
           
            }
            
            else{
          
            if(adult>minOccu){
           	 
           	var adultsLeft = (adult-minOccu);
           	$scope.roomsSelected[indx].total =  (adultsLeft * extraAdult * diffDays + roomAmount);
           	$scope.roomsSelected[indx].tempTotal =  (adultsLeft * extraAdult * diffDays + roomAmount);
           	//alert("adult" +adultsLeft);
            }
            
                  if(child>minOccu){
           	 
           	var childLeft = (child-minOccu);
           	$scope.roomsSelected[indx].total =  (childLeft * extraChild * diffDays + roomAmount);
           	$scope.roomsSelected[indx].tempTotal =  (childLeft * extraChild * diffDays + roomAmount);
           	// alert("child" +childLeft);
            }
            
                       if(infant>minOccu){
           	 
           	var infantLeft = (infant-minOccu);
           	$scope.roomsSelected[indx].total =  ( infantLeft * extraInfant * diffDays + roomAmount);
           	$scope.roomsSelected[indx].tempTotal =  (infantLeft * extraInfant * diffDays + roomAmount);
           	//alert("infant" +infantLeft);
            }
                       
                       if((adult+child) > minOccu){
           	 
           	var childLeft = (adltChd-minOccu);
           	$scope.roomsSelected[indx].total =  (childLeft * extraChild * diffDays + roomAmount);
           	// alert("child" +childLeft);
            }
                       
                       if((adult+child+infant) > minOccu && infant >0){
           	 
           	var infantLeft = (adltChdIft-minOccu);
           	$scope.roomsSelected[indx].total =  (infantLeft * extraInfant * diffDays + roomAmount);
           	$scope.roomsSelected[indx].tempTotal =  ( infantLeft * extraInfant * diffDays + roomAmount);
           	//alert("infant" +infantLeft);
            }
            
            }
            $scope.roomsSelected[indx].tax =  Math.round(taxPercentage / 100 * $scope.roomsSelected[indx].total);
           // alert("taxes"+ $scope.roomsSelected[indx].tax)
                        //alert($scope.roomsSelected[indx].total);
            
             
           //  var taxy = $scope.roomsSelected[indx].tax;
             
            /*  if(finTotal < 1000){
                        //alert("dsfsd");
                        var percent = 0;
                        var taxes = 0/100*finTotal* diffDays;
                        $scope.roomsSelected[indx].tax = taxes; 
                        //alert(taxes);
                    }
                    else if(finTotal >= 1000 && finTotal <= 2500){
                        //alert("dsfsd");
                        var percent = 12;
                        //var taxes = 12/100*amount* diffDays;
                        var taxes = Math.round(percent / 100 * finTotal) * diffDays;
                        $scope.roomsSelected[indx].tax = taxes; 
                        //alert(taxes);
                    }
                    else if(finTotal >= 2500 && finTotal <= 7000){
                        //alert("dsfsd");
                        var percent = 18;
                        var taxes = Math.round(percent / 100 * finTotal) * diffDays;
                        $scope.roomsSelected[indx].tax = taxes; 
                        //alert(taxes);
                    }
                    else if(finTotal >= 7000 && finTotal <= 7000000){
                        //alert("dsfsd");
                        var percent = 28;
                        //var taxes = Math.round(percent)/100 *amount* diffDays;
                       var taxes = Math.round(percent / 100 * finTotal) * diffDays;
                       $scope.roomsSelected[indx].tax = taxes; 
                        //alert( taxes);
                    } */
             $scope.roomsSelected[indx].totalBaseAmount = $scope.roomsSelected[indx].tax +  $scope.roomsSelected[indx].total;
          
                
         	   
   }
   
   
     $scope.selectType = function(id,indx){
        	    
                
                //alert(btn.value);
               // var  a = document.getElementById(btn).hide();
                //alert(id);
                //var x = document.getElementById("btn"+id).style.display='none'; 
                
                var text = document.getElementById("btn"+id).firstChild;
                //text.data = text.data == "SELECT ROOM" ? "Selected" : "SELECT ROOM";
                 
                if(document.getElementById(id).checked){
                
                
                document.getElementById(id).checked = false;
                
                text.data = "SELECT ROOM";
                document.getElementById("btn"+id).classList.remove("actives");
                $scope.manageTypes(id);
                
                }
                
                else{
                    
                text.data = "SELECTED";  
                document.getElementById(id).checked = true;   
                document.getElementById("btn"+id).classList.add("actives");
                $scope.manageTypes(id,indx);
                
                }
                //alert("hai");
                  
                }
     
        $scope.manageTypes = function(id,indx){
   
        if(document.getElementById(id).checked){
        
        
        //var accId = parseInt($scope.accommodations[indx].accommodationId);
        //var available = parseInt($scope.accommodations[indx].available);
       // var amount = parseFloat($scope.accommodations[indx].baseAmount);
        
       // var chkval = document.getElementById(id).value;
       // var newType = {'accommodationId':accId ,'baseAmount':amount,'available':available};
        
         //$scope.typeSelected.push(newType);
        
         var dt = $scope.availablities[indx];
         $scope.addRow(dt.accommodationId,dt.accommodationType,dt.baseAmount,dt.arrivalDate, dt.departureDate,dt.rooms,
        		 dt.noOfAdults,dt.noOfChild,dt.minOccupancy,dt.maxOccupancy,dt.extraAdult,dt.extraInfant,dt.extraChild,
        		 dt.available,indx,dt.tax,dt.diffDays,dt.taxPercentage,dt.propertyId,dt.minimumAmount,dt.maximumAmount);
        
         // $scope.addRow(dt.accommodationId,dt.accommodationType,dt.baseAmount,dt.arrivalDate, dt.departureDate,dt.rooms,dt.noOfAdults,dt.noOfChild,dt.minOccupancy,dt.extraAdult,dt.extraInfant,dt.extraChild,dt.available,indx,dt.tax);    
         
         //avai
         console.log($scope.roomSelected);
        
      } 
        
     else{
        
       //$scope.typeSelected.splice(id);
        $scope.cleaner($scope.roomsSelected,id);
        //var chkval = document.getElementById(id).value;
        //alert(chkval);
      }
   
        
            };
            
            
            $scope.cleaner = function(arr,id){
              //console.log(id);
          	console.log(arr.length);
          	
              	for (var i = 0; i < arr.length; i++) {
              	        var cur = arr[i];
              	        
              	        if (cur.accommodationId == id) {
              	            arr.splice(i, 1);
              	            break;
              	        }
              	    }
              	 
              };
   
         $scope.confirmBooking = function(){
   
   //alert("sdghgd");
   //alert($('#propertyItemCategoryId').val());
     
   
   /*var fdata = "arrivalDate=" + $('#bookCheckin').val()
   + "&departureDate=" + $('#bookCheckout').val()
   + "&rooms=" + $("#totalRooms").text().replace(/\s/g,'')
   + "&totalTax=" + $('#tax').val()
   + "&securityDeposit=" + $('#securityDeposit').val()*/
   
   var text = '{"array":' + JSON.stringify($scope.roomsSelected) + '}';
  // alert("text"+text)
   var data = JSON.parse(text);
   var sourceid =  $('#sourceId').val();
   var discountid =  $('#discount').val(); 
   var totalAmounts =  $('#totalAmounts').val();
   $http(
   {
   method : 'POST',
   data : data,
   //data : '{"data":' +JSON.stringify($scope.roomsSelected) + '}',
   dataType: 'json',
   headers : {
   'Content-Type' : 'application/json; charset=utf-8'
   
   },
   url : "add-booking?sourceId="+sourceid
   
   
   }).then(function successCallback(response) {
   
   
   var gdata = "firstName=" + $('#firstName').val()
   + "&lastName=" + $('#lastName').val()
   + "&emailId=" + $('#guestEmail').val()
   + "&phone=" + $('#mobilePhone').val()
   + "&landline=" + $('#landlinePhone').val()
   + "&address1=" + $('#firstAddress').val()
   + "&address1=" + $('#secondAddress').val()
   + "&city=" + $('#guestCity').val()
   + "&zipCode=" + $('#guestZipCode').val()
   + "&countryCode=" + $('#countryCode').val()
   + "&stateCode=" + $('#stateCode').val()
   
   $http(
   {
   method : 'POST',
   data : gdata,
   headers : {
   'Content-Type' : 'application/x-www-form-urlencoded'
   },
   url : 'add-guest'
   
   }).then(function successCallback(response) {
   
   /*var bdata = "accommodationId=" + $('#accommodationId').val()
   + "&adultCount=" + $('#adults').val()
   + "&childCount=" + $('#child').val()
   + "&amount=" + $('#price').val()
   + "&refund=0
   + "&statusId=2"
   + "&tax=0"*/
       console.log($scope.roomsSelected);
       //angular.toJson
       //var bdata ="jsonData=" + '{"roomsbooked":' +JSON.stringify($scope.roomsSelected) + '}';
       //var bdata = '{"data":' +$scope.roomsSelected + '}';
       //var bdata = "jsonData=" + JSON.stringify($scope.roomsSelected);
       // var bdata = angular.toJson($scope.roomsSelected);
   //var bdata = $scope.roomsSelected;s
   
   //var bdata = '{"data":[{"accommodationId":9,"accommodationType":"luxury"}]}';
   //var data = {data: {jsonKey1 : 'one', jsonKey2: 'two',jsonKey5: 2, jsonKey6: '2'}};
   var text = '{"array":' + JSON.stringify($scope.roomsSelected) + '}';
   //alert(text);
   var data = JSON.parse(text);
   //var data = $scope.roomsSelected;
   var discountid =  $('#discount').val(); 
   var totalAmounts =  $('#totalAmounts').val();
   var specialRequest =  $('#specialRequest').val();
   var sourceid =  $('#sourceId').val();
   var mailSource = true;
   //alert("discountid"+discountid);
   $http(
       {
   method : 'POST',
   data : data,
   //data : '{"data":' +JSON.stringify($scope.roomsSelected) + '}',
   dataType: 'json',
   headers : {
   'Content-Type' : 'application/json; charset=utf-8'
   //'Content-Type' : 'application/x-www-form-urlencoded'
   },
   url : "add-booking-details?txtGrandTotal="+totalAmounts+'&specialRequest='+specialRequest+'&mailSource='+mailSource+'&sourceId='+sourceid
   //url : 'jsonTest'
      }).then(function successCallback(response) {
   
   
       // alert("booking has been done sucessfully");	
        $scope.booked = response.data;
        //alert(JSON.stringify($scope.booked));
        console.log($scope.booked.data[0].bookingId);
     
      }, function errorCallback(response) {
    	  alert('process failed please try again!! ')
   });
   
   }, function errorCallback(response) {
	   alert('process failed please try again!! ')   
   });
   
   }, function errorCallback(response) {
	   alert('process failed please try again!! ')
   });
   
   };
   
   
   $scope.range = function(min, max, step){
       step = step || 1;
       var input = [];
       for (var i = min; i <= max; i += step) input.push(i);
       return input;
   };
   
   $scope.getStates = function() {
               
               var url = "get-states";
               $http.get(url).success(function(response) {
                   //console.log(response);
                   //alert(JSON.stringify(response.data));
                   $scope.states = response.data;
               
               });
           };
           
           $scope.getCountries = function() {
               
               var url = "get-countries";
               $http.get(url).success(function(response) {
                   //console.log(response);
                   //alert(JSON.stringify(response.data));
                   $scope.countries = response.data;
               
               });
           };
           
           $scope.getResortDiscount = function() {
          	
          	 var url = "get-resort-discount";
               $http.get(url).success(function(response) {
                   //console.log(response);
                   //alert(JSON.stringify(response.data));
                   $scope.resortdiscount = response.data;
               
               });
          	 
           }
           
           $scope.editResortPrice = function(indx,className) {
        	   var currentVal =  parseInt($('.'+className+indx).val());
        	  if(isNaN(currentVal)){
        		  $scope.roomsSelected[indx].total =  Math.round($scope.roomsSelected[indx].baseAmount  * $scope.roomsSelected[indx].rooms);
        		  
        	  	}
        	  
              else{
          	  	 $scope.roomsSelected[indx].total =  Math.round($scope.roomsSelected[indx].tempTotal - currentVal);
          	    
             	 }
        	
          	  	$scope.roomsSelected[indx].tax = Math.round($scope.roomsSelected[indx].total * $scope.roomsSelected[indx].taxPercentage / 100);
          		$scope.roomsSelected[indx].totalBaseAmount = Math.round($scope.roomsSelected[indx].tax +  $scope.roomsSelected[indx].total);  
            	 
             }
           
           
				$scope.editResortTax = function(indx,className) {
        	   
        	   var currentVal =  parseInt($('.'+className+indx).val());
        	   if(isNaN(currentVal)){
        		   $scope.roomsSelected[indx].tax = $scope.roomsSelected[indx].tax; 
        		   $scope.roomsSelected[indx].totalBaseAmount = Math.round($scope.roomsSelected[indx].tax +  $scope.roomsSelected[indx].total);
        	   }
        	   else{
        	   //var totalAmount = Math.round($scope.roomsSelected[indx].baseAmount  * $scope.roomsSelected[indx].rooms);
          	  	$scope.roomsSelected[indx].tax =  Math.round($scope.roomsSelected[indx].total * currentVal /100);
          	  	//alert( $scope.roomsSelected[indx].tax)
          		$scope.roomsSelected[indx].totalBaseAmount = Math.round($scope.roomsSelected[indx].tax +  $scope.roomsSelected[indx].total);
                
        	   }
             }
           
           
          
           $scope.roomsSelected = [];
           // shiva add addrom tax
           
           $scope.addRow = function(id,type,baseAmount,arrival, departure,rooms,adultsAllow,childAllow,minOccu,maxOccu,
        		   extraAdult,extraInfant,extraChild,available,indx,tax,diffDays,taxPercentage,propertyId,minAmount,maxAmount){	
           	
          	if(available == 0){
          	console.log("Disable");
                   }
                   else{
          	 
           var type = type.replace(/\s/g,'');
           var id = parseInt(id);
           var minOccu = parseInt(minOccu);
           var maxOccu = parseInt(maxOccu);
           var extraAdult = parseInt(extraAdult);
           var extraInfant = parseInt(extraInfant);
           var extraChild = parseInt(extraChild);
           
           var adultsAllow = parseInt(adultsAllow);
           var childAllow = parseInt(childAllow);
          
           var amount = parseFloat(baseAmount);
           var taxPercentage = parseFloat(taxPercentage);
           var rooms = parseInt(rooms);
           var diffDays=parseInt(diffDays);
           var taxes = parseInt(tax * diffDays);
           //alert("tax"+taxes)
          /*  var date1 = new Date(arrival);
           var date2 = new Date(departure); */
           
          // var a = arrival.split(/[^0-9]/);
           //for (i=0;i<a.length;i++) { alert(a[i]); }
           //var d= new Date (a[0],a[1]-1,a[2],a[3],a[4],a[5] );
         //  alert(s+ " "+d);
         
           //alert(arrival);
           //alert(date1);
           
          /*  var a1 = arrival.split(/[^0-9]/);
           var d1= new Date (a1[0],a1[1]-1,a1[2],a1[3],a1[4],a1[5] );
           
           var a2 = departure.split(/[^0-9]/);
           var d2= new Date (a2[0],a2[1]-1,a2[2],a2[3],a2[4],a2[5] );
         
           var timeDiff = Math.abs(d2.getTime() - d1.getTime());
         
           var diffDays = parseInt(Math.round(timeDiff / (1000 * 3600 * 24))); */
           var randomNo = Math.random();
           var baseAmountTax = amount / diffDays;
          /*  if(baseAmountTax < 1000){
               //alert("dsfsd");
               var percent = 0;
                = 0/100*baseAmountTax* diffDays;
               //alert(taxes);
           }
           else if(baseAmountTax >= 1000 && baseAmountTax <= 2500){
               //alert("dsfsd");
               var percent = 12;
               //var taxes = 12/100*amount* diffDays;
               var taxes = Math.round(percent / 100 * baseAmountTax) * diffDays;
               //alert(taxes);
           }
           else if(baseAmountTax >= 2500 && baseAmountTax <= 7000){
               //alert("dsfsd");
               var percent = 18;
               var taxes = Math.round(percent / 100 * baseAmountTax) * diffDays;
               //alert(taxes);
           }
           else if(amount >= 7000 && baseAmountTax <= 7000000){
               //alert("dsfsd");
               var percent = 28;
               //var taxes = Math.round(percent)/100 *amount* diffDays;
              var taxes = Math.round(percent / 100 * baseAmountTax) * diffDays;
               //alert( taxes);
           } */
//            var total = (diffDays*rooms*amount);
			var total = (amount);
			var tempTotal = (amount);
			var totalBaseAmount = total + taxes;
			var minimumAmount=parseFloat(minAmount);
			var maximumAmount=parseFloat(maxAmount);
			//alert("t.b.a"+totalBaseAmount)
           //shiva update newData diffDays only
           var newData = {'arrival':arrival,'indx':indx,'available':available,'departure':departure,'accommodationId':id,'sourceId':1,
				'accommodationType':type, 'rooms': rooms,'diffDays':diffDays, 'baseAmount':amount, 'total':total ,'adultsAllow':adultsAllow,'childAllow':childAllow,
				'minOccu':minOccu,'maxOccu':maxOccu,'extraAdult':extraAdult,'extraInfant':extraInfant,'extraChild':extraChild,'adultsCount':1,
				'infantCount':0,'childCount':0,'refund':0,'statusId':2,'tax':taxes,'randomNo':randomNo,'totalBaseAmount':totalBaseAmount,
				'taxPercentage':taxPercentage,'discountId':35,'propertyId':propertyId,
				'tempTotal':tempTotal,'minimumAmount':minimumAmount,'maximumAmount':maximumAmount};
           
          	   $scope.roomsSelected.push(newData);
          	   console.log($scope.roomsSelected);
          	   // $scope.availablities[indx].available = $scope.availablities[indx].available - 1 ;
                   }
          	};
          	
              $scope.getTotal = function(){
              	
          	    var total = 0;
          	    var tax = 0; 
          	    for(var i = 0; i < $scope.roomsSelected.length; i++){
          	        var accommodation = $scope.roomsSelected[i];
          	        tax += (accommodation.tax);
          	        total += (accommodation.total);
          	    }
          
          	    var all = (total+tax);
          	    return all;
          	}  	
            
              $scope.getTempTotal = function(){
                	
            	    var total = 0;
            	    var tax = 0;
            	    for(var i = 0; i < $scope.roomsSelected.length; i++){
            	        var accommodation = $scope.roomsSelected[i];
            	        tax += (accommodation.tax);
            	        total += (accommodation.totalBaseAmount);
            	    }
            
            	    var all = (total);
            	    return all;
            	}  
	          								
   
   
   $scope.createLink = function(){
   
   /*var link = 	{
     "customer": {
       "name": "Test",
       "email": "test@test.com",
       "contact": "9999999999"
     },
     "type": "link",
     "view_less": 1,
     "amount": 2000,
     "currency": "INR",
     "description": "Payment link for this purpose - xyz.",
     "expire_by": 1493630556
   };*/
   
   //alert("create link");
   //document.getElementById("totalAmounts").value = document.getElementById("txtGrandTotal").value
   
      var type = "link";
      var view_less = 1;
      var Amount = $('#totalAmounts').val();
      var amount = Amount * 100;
      var currency = "INR";
      var description = "Payment Link";
      var expire_by = Math.floor(new Date($('#expiryDate').val()).getTime()/1000);
     // var expire_by = 1507833000;
        //1507833000000
      var name = $('#firstName').val();
      var email =  $('#itemEmail').val();
      var phone =  $('#itemMobile').val();
     
     
      
      
      //alert(Math.floor(expire_by/1000));
      /*var name = "gopinath";
      var email = "gopinath@ulohotels.com";
      var phone = "8870088876";*/
      
      
      var pdata = "link=" + $('#guestEmail').val()
      + "&email=" + $('#guestEmail').val()
      + "&name=" + $('#firstName').val();
      
      
     /* var total = $('#gopi').val();
      var name = $('#firstName').val();
      var email =  $('#itemEmail').val();
      var phone =  $('#itemMobile').val();
      var description = $('#itemDescription').val();
      var expire_by = $('#expiryDate').val();*/
      
      
   
   var idata = "type=" + type
                  +"&view_less=" + view_less
                  + "&amount=" + amount
                  + "&name=" + name
                  + "&email=" + email
                  + "&phone=" + phone
                  + "&currency=" + currency
               + "&description=" + description
               + "&expire_by=" + expire_by;
   
   
   
               
      $http(
   {
   method : 'POST',
   data : idata,
   
   headers : {
   'Content-Type' : 'application/x-www-form-urlencoded'
   },
   url : 'create-payment-link'
   
   }).success(function(response) {
   
   $scope.invoice = response.data;
   
   alert("link sent successfully");
   //console.log($scope.booked[0].firstName);
   $timeout(function(){
    	$('#btnclose').click();
  }, 1000);
   
   });
   
   };	
   
          
   
   $scope.goLink = function(){
   
   //alert("hai");
   //alert($('#bookingId').val());
   var invoiceId = $('#linkId').val();
   //var invoiceId = 'inv_8oJzL37diCruyV';
   var fdata = "invoiceId=" + invoiceId; 
   
               //alert(bookingId);
   //alert("payid" +invoiceId);
   
   $http(
   {
   method : 'POST',
   data : fdata,
   headers : {
   'Content-Type' : 'application/x-www-form-urlencoded'
   },
   url : 'get-invoice-status'
   }).success(function(response) {
   
   $scope.linku = response.data;
   //console.log($scope.booked[0].firstName);
   //alert($scope.linku[0].amount_paid);
           if($scope.linku[0].amount_paid == 0){
           	alert("Payment Pending");
           }else
           	{
           	alert("Payment Success");
           	}
   
   });
   
   
   };
   
   $scope.getTaxes = function() {

		var url = "get-taxes";
		$http.get(url).success(function(response) {
		    //console.log(response);
			$scope.taxes = response.data;
		});
	};
   	
   
   
   $scope.getSources = function() {
   
   var url = "get-resort-sources";
   $http.get(url).success(function(response) {
       //console.log(response);
   $scope.sources = response.data;
   
    });
   };
   
   /* $scope.discountedGrossTotal = function() {
   	//alert("enter"+ $scope.discount);
   	//$scope.manageType($scope.discount,$scope.resortdiscount,$scope.roomsSelected);
   	 // $scope.stdPrice =  $scope.getTotal();
   	   
   	 // alert($scope.stdPrice);
   	  
   	  //alert("asdas");
   	  var discountOption = document.getElementById("discount").value;
      var subTotal = document.getElementById("txtSubTotal").value;
      console.log($scope.resortdiscount)
      var did = $scope.discount;
      var obj = $filter('filter')($scope.resortdiscount, {discountId: did}, true)[0];
      var discountPercentage = obj.discountPercentage;
      console.log($scope.roomsSelected);
      for(var j=0; j < $scope.roomsSelected.length;j++){
     		var rs = $scope.roomsSelected[j];
     		var tempTotal = rs.baseAmount * rs.rooms;
     		rs.total = Math.round(tempTotal - (tempTotal * discountPercentage)/100);
     		//alert("total"+rs.total)
     		rs.tax = Math.round((rs.total* rs.taxPercentage)  /100);
     		//alert("tax"+rs.tax)
     		 document.getElementById("totalAmounts").value = $scope.getTotal();
     		// $scope.roomsSelected[indx].total = 100;
     	   //alert("final"+$scope.roomsSelected.total) 
     	 //  alert(JSON.stringify($scope.roomsSelected));
     	}
       console.log( obj.discountPercentage);
      
      //var discountOption = document.getElementById("discount").value;
    
  //	document.getElementById("totalAmounts").value = document.getElementById("txtGrandTotal").value; 
  		
   	}; */
   	
   	   
      /*   $scope.manageType = function(id,arr,rs){
           
            //var subTotal = document.getElementById("txtSubTotal").value;
            alert("json"+JSON.stringify(rs));
             //$scope.roomsSelected
       	 for (var i = 0; i < arr.length; i++) {
       	        var cur = arr[i];
       	        //alert("name"+ cur.discountPercentage);
       	        if (cur.discountId == id) {
       	        	
       	        	for(var j=0; j <rs.length;j++){
       	        		var rss= rs[j];
       	        		alert("rss"+rss.baseAmount);
       	        		$scope.roomsSelected.total = (rss.baseAmount - (rss.baseAmount * cur.discountPercentage)/100);
       	        		// $scope.roomsSelected[indx].total = 100;
       	        	alert("final"+$scope.roomsSelected.total) 
       	        	alert(JSON.stringify($scope.roomsSelected));
       	        	}
       	        	//alert("success");
       	        	//  $scope.roomsSelected[0].discountId = id;
       	        	//$scope.roomsSelected[0].total = ($scope.roomsSelected[0].baseAmount - ($scope.roomsSelected[0].baseAmount * cur.discountPercentage)/100);
       	        	
       	        	 var discountOption = document.getElementById("discount").value;
       	         		var subTotal = document.getElementById("txtSubTotal").value;
       	         	
                     document.getElementById("txtGrandTotal").value = Math.round(subTotal - ((subTotal * cur.discountPercentage)/100));
       	         	document.getElementById("totalAmounts").value = document.getElementById("txtGrandTotal").value; 
       	         	break;
       	        }
       	 }
       	 
       	 
       	    }; */
   	 
   	      $scope.getSources();
   
  		  $scope.getStates();
          
          $scope.getCountries();
          
          $scope.getResortDiscount();
          
          $scope.getTaxes();
          
          
          
          //console.log($scope.roomSelected);
          //$scope.getBookedDetails();
   //$scope.unread();
   //
          
   
   });
   
</script>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
   $( function() {
   /*   $("#bookCheckin" ).datepicker({minDate:0});
     $("#bookCheckout" ).datepicker({minDate:0}); */
     $("#checkin" ).datepicker({minDate:0});
     $("#checkout" ).datepicker({minDate:0});
     $("#expiryDate" ).datepicker({minDate:1});
   });
   
</script>
<!-- iCheck 1.0.1 -->
<script src="plugins/iCheck/icheck.min.js"></script>
<script>
   /* next and previous button */
   $('.nextBtn').click(function(){
   $('.nav-tabs > .active').next('li').find('a').trigger('click');
   });
   
   $('.previousBtn').click(function(){
   $('.nav-tabs > .active').prev('li').find('a').trigger('click');
   });
</script>
  <script language="javascript" type="text/javascript">
        function printDiv(divID) {
            //Get the HTML of div
            var divElements = document.getElementById(divID).innerHTML;
            //Get the HTML of whole page
            var oldPage = document.body.innerHTML;

            //Reset the page's HTML with div's HTML only
            document.body.innerHTML = 
              "<html><head><title></title></head><body>" + 
              divElements + "</body>";

            //Print Page
            window.print();

            //Restore orignal HTML
            document.body.innerHTML = oldPage;

          
        }
    </script>
