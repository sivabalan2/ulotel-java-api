<!doctype html>
<html amp lang="en">

<head>
    <meta charset="utf-8" />

    <meta name="viewport" content="width=device-width,minimum-scale=1">
<title>Book Hotels In Kodaikanal, Flat 1000 Offer with Free Breakfast</title> 
<meta name="description" content="Book hotels in Kodaikanal and get Free Breakfast, Free WiFi, AC Rooms, LCD TV and Comfortable Beds. Reserve best budget Kodaikanal hotels.">
<meta name="keywords" content="kodaikanal hotels, budget hotels in kodaikanal, kodaikanal stay, kodaikanal room booking, kodaikanal hotels rates below 1000"/> 
<meta property="og:site_name" content="ULO Hotels" />
<meta property="og:image" content="https://www.ulohotels.com/ulowebsite/images/ulo_logo.jpg"> 
<meta property="og:description" content="Book hotels in Kodaikanal and get Free Breakfast, Free WiFi, AC Rooms, LCD TV and Comfortable Beds. Reserve best budget Kodaikanal hotels." />
<meta property="og:title" content="Book Hotels In Kodaikanal, Flat 1000 Offer with Free Breakfast" /> 
<meta property="og:publisher" content="https://www.facebook.com/Ulohotels" /> 
<meta property="og:url" content=" https://www.ulohotels.com/kodaikanal-hotels" />
    <link rel="canonical" href="https://www.ulohotels.com/kodaikanal-hotels">
    <meta content="origin" name="referrer" />
    <meta name="google" content="notranslate">
    <meta http-equiv="Content-Language" content="en">
    <meta name="amp-google-client-id-api" content="googleanalytics">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
   <style amp-boilerplate>
			body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}
		</style>
		<noscript>
         <style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style>
      </noscript>
    <style amp-custom>
        .link {
            text-decoration: none;
        }
        
        .hide {
            display: none;
        }
        
        .fwidth {
            width: 100%;
        }
        
        .header {
            -webkit-align-items: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-flex-direction: row;
            -ms-flex-direction: row;
            flex-direction: row;
            -webkit-box-pack: justify;
            -webkit-justify-content: space-between;
            -ms-flex-pack: justify;
            justify-content: space-between;
        }
        
        .socialreview {
            -webkit-align-items: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-flex-direction: row;
            -ms-flex-direction: row;
            flex-direction: row;
        }
        
        .hoteldetail {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-flex-direction: row;
            -ms-flex-direction: row;
            flex-direction: row;
            -webkit-box-pack: justify;
            -webkit-justify-content: space-between;
            -ms-flex-pack: justify;
            justify-content: space-between;
        }
        
        .footbox {
            -webkit-align-items: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-flex-direction: row;
            -ms-flex-direction: row;
            flex-direction: row;
            -webkit-box-pack: center;
            -webkit-justify-content: center;
            -ms-flex-pack: center;
            justify-content: center;
        }
        
        html {
            box-sizing: border-box;
        }
        
        body {
            background: #f1f1f1;
            color: #4a4a4a;
            font-size: 14px;
            font-weight: 400;
            font-family: 'Open Sans', sans-serif;
            margin: 0;
        }
        
        *,
        *::after,
        *::before {
            line-height: 1.2;
            box-sizing: inherit;
        }
        
        body {
            background: #ffffff;
        }
        /* sc-component-id:  */
        
        .h1tag {
            margin: 12px;
        }
        
        .coupad {
            padding: 16px;
        }
        
        .reviewpad {
            padding: 0px 0px 0px 8px;
        }
        
        .hdpad {
            margin: 16px 0px;
            padding: 0px 16px;
        }
        
        .addpad {
            margin: 8px 0px 8px 0px;
        }
        
        .seotag {
            margin: 24px 16px 32px 16px;
        }
        
        #logoid {
            padding-left: 30px;
        }
        
        .headtxt {
            color: #aeaeae;
            font-size: 14px;
            overflow: hidden;
            white-space: nowrap;
            text-overflow: ellipsis;
            padding-right: 30px;
            padding-top: 5px;
            margin: 0;
        }
        
        .locname {
            font-size: 16px;
            padding: 0;
            margin: 0;
            font-weight: 600;
        }
        
        .offname {
            color: #ffffff;
            font-size: 14px;
            font-weight: 500;
            padding: 0;
            margin: 0;
        }
        
        .ropen {
            color: #4a4a4a;
            font-size: 14px;
            padding: 0;
            margin: 0;
        }
        
        .hname {
            color: #693;
            font-size: 16px;
            font-weight: 600;
            padding: 0;
            margin: 0;
        }
        
        .addsec {
            color: #727171;
            font-size: 16px;
            font-weight: 600;
            padding: 0;
            margin: 0;
        }
        
        .addcolor {
            color: #727171;
            font-size: 14px;
            font-weight: 400;
            padding: 0;
            margin: 0;
        }
        
        .retext {
            color: #727171;
            font-size: 14px;
            padding: 0;
            margin: 0;
        }
        
        .hprice {
            color: #069;
            font-size: 28px;
            font-weight: 600;
            padding: 0;
            margin: 0;
        }
        
        .seoh1 {
            color: #4a4a4a;
            font-size: 18px;
            font-weight: 500;
            padding: 0;
            margin: 0;
        }
        
        .lmore {
            color: #5768e9;
            font-size: 14px;
            padding: 0;
            margin: 0;
        }
        
        .foottext {
            color: #4a4a4a;
            font-size: 12px;
            font-weight: 600;
            padding: 5px;
            margin: 0;
            text-align: center;
        }
        
        .footer {
            bottom: 0;
            z-index: 20;
            margin: 5px;
        }
        
        .offbox {
            border-width: 1px;
            border-style: solid;
            border-radius: 2px;
            border-color: #699;
            background-color: #699;
        }
        
        .footstyle {
            border-width: 1px;
            border-style: solid;
            border-radius: 2px;
            border-color: #ffffff;
            background-color: #ffffff;
            text-align: center;
        }
        
        .rollscale {
            -webkit-filter: grayscale(0);
            filter: grayscale(0);
            border-radius: 2px;
        }
        
        .seobox > div {
            height: 100px;
            overflow: hidden;
        }
        
        .seobox > label::before {
            content: 'Show More';
        }
        
        .seobox #seopara {
            display: none;
        }
        
        .seobox #seopara:checked + div {
            height: auto;
        }
        
        .seobox #seopara:checked ~ label::before {
            content: 'Show Less';
        }
        /* sc-component-id:  */
        
        .carimg .amp-carousel-button {
            display: none;
        }
        
        .hline {
            border-width: 1px 0 0;
            border-color: #f1f1f1;
            border-style: solid;
            background: rgba(0, 0, 0, 0);
            margin: 0;
        }
        
        .headercontact {
            color: #000;
            text-decoration: none;
            font-weight: 400;
            margin-top: 5px;
            fon-size: 14px;
            text-align: center;
            display: block;
        }
        
        .selfintro {
            font-size: 12px;
            pading: 10px;
        }
        
        .offcode {
            border: 1px solid #fff;
            padding: 5px;
        }
        
        .bookbtn {
            color: #fff;
            background: #693;
            padding: 7px;
            font-size: 12px;
            border: none;
            margin: 5px;
        }
        
        .hotellist {
            box-shadow: 5px 5px #e6e7e8;
        }
    </style>
    <script async src="https://cdn.ampproject.org/v0.js"></script>
    <script async custom-element="amp-install-serviceworker" src="https://cdn.ampproject.org/v0/amp-install-serviceworker-0.1.js"></script>
    <script async custom-element="amp-analytics" src="https://cdn.ampproject.org/v0/amp-analytics-0.1.js"></script>
    <script async custom-element="amp-lightbox-gallery" src="https://cdn.ampproject.org/v0/amp-lightbox-gallery-0.1.js"></script>
    <script async custom-element="amp-carousel" src="https://cdn.ampproject.org/v0/amp-carousel-0.1.js"></script>
    <script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>
    <script async custom-template="amp-mustache" src="https://cdn.ampproject.org/v0/amp-mustache-0.2.js"></script>
    <script async custom-element="amp-list" src="https://cdn.ampproject.org/v0/amp-list-0.1.js"></script>
    <script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>
</head>

<body>
    <div>
        <div class="  " id="wrapper">
            <div class="   ">
                <div id="logosec">
                    <div class="   header  ">
                        <a id="logoid" href="https://www.ulohotels.com" class="   ">
                            <amp-img class="" src="amp/logo/ulo4.png" alt="logo" width="120" height="40">
                            </amp-img>
                        </a>
                        <div class="headtxt">
                            <a href="tel:9543592593" title="+91 95435 92593" class="headercontact"><span >Call @ 9543 592 593</span></a>
                        </div>
                    </div>
                    <div id="cousec">
                    
                        <div class=" h1tag  ">
                            <div class=" locname">KODAIKANAL HOTELS AND RESORTS</div>
                        </div>
                        <!-- woodys -->
                              <div class="hotellist">
                            <a class="link" href="ulo-woodys">
                                <div class="    ">
                                    <amp-carousel width="auto" height="150px" layout="fixed-height" controls="" class=" carimg" type="carousel">
                                        <div class="   "></div>
                                        <amp-img class=" rollscale" src="amp/ulo-woodys/1.png" alt="ULO KODAI MERIDIAN" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-woodys/2.png" alt="ULO KODAI MERIDIAN" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-woodys/3.png" alt="ULO KODAI MERIDIAN" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-woodys/4.png" alt="ULO KODAI MERIDIAN" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-woodys/5.png" alt="ULO KODAI MERIDIAN" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-woodys/6.png" alt="ULO KODAI MERIDIAN" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-woodys/7.png" alt="ULO KODAI MERIDIAN" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-woodys/8.png" alt="ULO KODAI MERIDIAN" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-woodys/9.png" alt="ULO KODAI MERIDIAN" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-woodys/10.png" alt="ULO KODAI MERIDIAN" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-woodys/11.png" alt="ULO KODAI MERIDIAN" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-woodys/12.png" alt="ULO KODAI MERIDIAN" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-woodys/13.png" alt="ULO KODAI MERIDIAN" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-woodys/14.png" alt="ULO KODAI MERIDIAN" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-woodys/15.png" alt="ULO KODAI MERIDIAN" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-woodys/16.png" alt="ULO KODAI MERIDIAN" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-woodys/17.png" alt="ULO KODAI MERIDIAN" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-woodys/18.png" alt="ULO KODAI MERIDIAN" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-woodys/19.png" alt="ULO KODAI MERIDIAN" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-woodys/20.png" alt="ULO KODAI MERIDIAN" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-woodys/21.png" alt="ULO KODAI MERIDIAN" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-woodys/22.png" alt="ULO KODAI MERIDIAN" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-woodys/23.png" alt="ULO KODAI MERIDIAN" width="200" height="150">
                                        </amp-img>
                               
                                    </amp-carousel>
                                </div>
                                <div class=" hdpad  ">
                                    <div class=" hoteldetail  ">
                                        <div class=" ">
                                            <meta itemProp="telephone" content="+91-9543592593" />
                                            <meta itemProp="email" content="reservations@ulohotels.com" />
                                            <div class="   hname">ULO WOODYS</div>
                                            <div class=" addsec">
                                                <meta itemProp="addressLocality" content="NO.32, AGA ABBAS ALI RD, YELLAPPA CHETTY LAYOUT" />
                                                <meta itemProp="addressRegion" content="bangalore" />
                                                <div class=" addpad">
                                                    <div class=" addcolor">KODAIKANAL</div>
                                                </div>
                                            </div>
                                            <div class=" socialreview  ">
                                                <meta itemProp="ratingValue" content="3.5" />
                                                <meta itemProp="ratingCount" content="275" />
                                                <meta itemProp="bestRating" content="5" />
                                                <!-- Hotel-Rating -->
                                                <span class="make_flex">
                                          <svg height="10px" version="1.1" viewBox="0 0 11 10" width="11px" >
                                             <polygon fill="#e57520" fill-rule="evenodd" points="5.33950617 7.65957447 2.13580247 10 3.2037037 6.38297872 0 3.61702128 3.84444444 3.61702128 5.33950617 0 6.62098765 3.61702128 10.6790123 3.61702128 7.47530864 6.38297872 8.54320988 10" stroke="none"></polygon>
                                          </svg>
                                          <svg height="10px" version="1.1" viewBox="0 0 11 10" width="11px" >
                                             <polygon fill="#e57520" fill-rule="evenodd" points="5.33950617 7.65957447 2.13580247 10 3.2037037 6.38297872 0 3.61702128 3.84444444 3.61702128 5.33950617 0 6.62098765 3.61702128 10.6790123 3.61702128 7.47530864 6.38297872 8.54320988 10" stroke="none"></polygon>
                                          </svg>
                                          <svg height="10px" version="1.1" viewBox="0 0 11 10" width="11px" >
                                             <polygon fill="#e57520" fill-rule="evenodd" points="5.33950617 7.65957447 2.13580247 10 3.2037037 6.38297872 0 3.61702128 3.84444444 3.61702128 5.33950617 0 6.62098765 3.61702128 10.6790123 3.61702128 7.47530864 6.38297872 8.54320988 10" stroke="none"></polygon>
                                          </svg>
                                          <svg height="10px" version="1.1" viewBox="0 0 11 10" width="11px" >
                                             <polygon fill="#e57520" fill-rule="evenodd" points="5.33950617 7.65957447 2.13580247 10 3.2037037 6.38297872 0 3.61702128 3.84444444 3.61702128 5.33950617 0 6.62098765 3.61702128 10.6790123 3.61702128 7.47530864 6.38297872 8.54320988 10" stroke="none"></polygon>
                                          </svg>
                                          <svg height="10px" version="1.1" viewBox="0 0 11 10" width="11px" >
                                             <polygon fill="#b7b7b7" fill-rule="evenodd" points="5.33950617 7.65957447 2.13580247 10 3.2037037 6.38297872 0 3.61702128 3.84444444 3.61702128 5.33950617 0 6.62098765 3.61702128 10.6790123 3.61702128 7.47530864 6.38297872 8.54320988 10" stroke="none"></polygon>
                                          </svg>
                                       </span>
                                                <!-- /Hotel-Rating -->
                                                <div class=" reviewpad  ">
                                                    <div class=" ropen">
                                                    </div>
                                                </div>
                                                <div class=" retext"></div>
                                                <div class=" retext"><span>    37 Google Reviews</span></div>
                                            </div>
                                        </div>
                                        <div class=" ">
                                            <div>
                                                <div class=" hprice">
                                                    &#8377; 1800
                                                </div>
                                                <a class="link" href="ulo-woodys">
                                                    <button class="bookbtn">SELECT ROOM</button>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <hr class="hline" />
                        </div>
                        <!-- woodys -->
                        <!-- one -->
                                     <div class="hotellist">
                            <a class="link" href="ulo-kodai-meridian">
                                <div class="    ">
                                    <amp-carousel width="auto" height="150px" layout="fixed-height" controls="" class=" carimg" type="carousel">
                                        <div class="   "></div>
                                        <amp-img class=" rollscale" src="amp/ulokodai/1.png" alt="ULO KODAI MERIDIAN" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulokodai/2.png" alt="ULO KODAI MERIDIAN" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulokodai/3.png" alt="ULO KODAI MERIDIAN" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulokodai/4.png" alt="ULO KODAI MERIDIAN" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulokodai/5.png" alt="ULO KODAI MERIDIAN" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulokodai/6.png" alt="ULO KODAI MERIDIAN" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulokodai/7.png" alt="ULO KODAI MERIDIAN" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulokodai/8.png" alt="ULO KODAI MERIDIAN" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulokodai/9.png" alt="ULO KODAI MERIDIAN" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulokodai/10.png" alt="ULO KODAI MERIDIAN" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulokodai/11.png" alt="ULO KODAI MERIDIAN" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulokodai/12.png" alt="ULO KODAI MERIDIAN" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulokodai/13.png" alt="ULO KODAI MERIDIAN" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulokodai/14.png" alt="ULO KODAI MERIDIAN" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulokodai/15.png" alt="ULO KODAI MERIDIAN" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulokodai/16.png" alt="ULO KODAI MERIDIAN" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulokodai/17.png" alt="ULO KODAI MERIDIAN" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulokodai/18.png" alt="ULO KODAI MERIDIAN" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulokodai/19.png" alt="ULO KODAI MERIDIAN" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulokodai/20.png" alt="ULO KODAI MERIDIAN" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulokodai/21.png" alt="ULO KODAI MERIDIAN" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulokodai/22.png" alt="ULO KODAI MERIDIAN" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulokodai/23.png" alt="ULO KODAI MERIDIAN" width="200" height="150">
                                        </amp-img>
                               
                                    </amp-carousel>
                                </div>
                                <div class=" hdpad  ">
                                    <div class=" hoteldetail  ">
                                        <div class=" ">
                                            <meta itemProp="telephone" content="+91-9543592593" />
                                            <meta itemProp="email" content="reservations@ulohotels.com" />
                                            <div class="   hname">ULO KODAI MERIDIAN</div>
                                            <div class=" addsec">
                                                <meta itemProp="addressLocality" content="NO.32, AGA ABBAS ALI RD, YELLAPPA CHETTY LAYOUT" />
                                                <meta itemProp="addressRegion" content="bangalore" />
                                                <div class=" addpad">
                                                    <div class=" addcolor">KODAIKANAL</div>
                                                </div>
                                            </div>
                                            <div class=" socialreview  ">
                                                <meta itemProp="ratingValue" content="3.5" />
                                                <meta itemProp="ratingCount" content="275" />
                                                <meta itemProp="bestRating" content="5" />
                                                <!-- Hotel-Rating -->
                                                <span class="make_flex">
                                          <svg height="10px" version="1.1" viewBox="0 0 11 10" width="11px" >
                                             <polygon fill="#e57520" fill-rule="evenodd" points="5.33950617 7.65957447 2.13580247 10 3.2037037 6.38297872 0 3.61702128 3.84444444 3.61702128 5.33950617 0 6.62098765 3.61702128 10.6790123 3.61702128 7.47530864 6.38297872 8.54320988 10" stroke="none"></polygon>
                                          </svg>
                                          <svg height="10px" version="1.1" viewBox="0 0 11 10" width="11px" >
                                             <polygon fill="#e57520" fill-rule="evenodd" points="5.33950617 7.65957447 2.13580247 10 3.2037037 6.38297872 0 3.61702128 3.84444444 3.61702128 5.33950617 0 6.62098765 3.61702128 10.6790123 3.61702128 7.47530864 6.38297872 8.54320988 10" stroke="none"></polygon>
                                          </svg>
                                          <svg height="10px" version="1.1" viewBox="0 0 11 10" width="11px" >
                                             <polygon fill="#e57520" fill-rule="evenodd" points="5.33950617 7.65957447 2.13580247 10 3.2037037 6.38297872 0 3.61702128 3.84444444 3.61702128 5.33950617 0 6.62098765 3.61702128 10.6790123 3.61702128 7.47530864 6.38297872 8.54320988 10" stroke="none"></polygon>
                                          </svg>
                                          <svg height="10px" version="1.1" viewBox="0 0 11 10" width="11px" >
                                             <polygon fill="#e57520" fill-rule="evenodd" points="5.33950617 7.65957447 2.13580247 10 3.2037037 6.38297872 0 3.61702128 3.84444444 3.61702128 5.33950617 0 6.62098765 3.61702128 10.6790123 3.61702128 7.47530864 6.38297872 8.54320988 10" stroke="none"></polygon>
                                          </svg>
                                          <svg height="10px" version="1.1" viewBox="0 0 11 10" width="11px" >
                                             <polygon fill="#b7b7b7" fill-rule="evenodd" points="5.33950617 7.65957447 2.13580247 10 3.2037037 6.38297872 0 3.61702128 3.84444444 3.61702128 5.33950617 0 6.62098765 3.61702128 10.6790123 3.61702128 7.47530864 6.38297872 8.54320988 10" stroke="none"></polygon>
                                          </svg>
                                       </span>
                                                <!-- /Hotel-Rating -->
                                                <div class=" reviewpad  ">
                                                    <div class=" ropen">
                                                    </div>
                                                </div>
                                                <div class=" retext"></div>
                                                <div class=" retext"><span>    130 Google Reviews</span></div>
                                            </div>
                                        </div>
                                        <div class=" ">
                                            <div>
                                                <div class=" hprice">
                                                    &#8377; 2000
                                                </div>
                                                <a class="link" href="ulo-kodai-meridian">
                                                    <button class="bookbtn">SELECT ROOM</button>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <hr class="hline" />
                        </div>
                        <!-- one  -->
                        <div class="hotellist">
                            <a class="link" href="ulo-green-hills-resort">
                                <div class="    ">
                                    <amp-carousel width="auto" height="150px" layout="fixed-height" controls="" class=" carimg" type="carousel">
                                        <div class="   "></div>
                                        <amp-img class=" rollscale" src="amp/ulo-green-hills-resort/1.png" alt="ulo-green-hills-resort" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-green-hills-resort/2.png" alt="ulo-green-hills-resort" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-green-hills-resort/3.png" alt="ulo-green-hills-resort" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-green-hills-resort/4.png" alt="ulo-green-hills-resort" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-green-hills-resort/5.png" alt="ulo-green-hills-resort" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-green-hills-resort/6.png" alt="ulo-green-hills-resort" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-green-hills-resort/7.png" alt="ulo-green-hills-resort" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-green-hills-resort/8.png" alt="ulo-green-hills-resort" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-green-hills-resort/9.png" alt="ulo-green-hills-resort" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-green-hills-resort/10.png" alt="ulo-green-hills-resort" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-green-hills-resort/11.png" alt="ulo-green-hills-resort" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-green-hills-resort/12.png" alt="ulo-green-hills-resort" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-green-hills-resort/13.png" alt="ulo-green-hills-resort" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-green-hills-resort/14.png" alt="ulo-green-hills-resort" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-green-hills-resort/15.png" alt="ulo-green-hills-resort" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-green-hills-resort/16.png" alt="ulo-green-hills-resort" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-green-hills-resort/17.png" alt="ulo-green-hills-resort" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-green-hills-resort/18.png" alt="ulo-green-hills-resort" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-green-hills-resort/19.png" alt="ulo-green-hills-resort" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-green-hills-resort/20.png" alt="ulo-green-hills-resort" width="200" height="150">
                                        </amp-img>
                                      
                                    </amp-carousel>
                                </div>
                                <div class=" hdpad  ">
                                    <div class=" hoteldetail  ">
                                        <div class=" ">
                                            <meta itemProp="telephone" content="+91-9543592593" />
                                            <meta itemProp="email" content="reservations@ulohotels.com" />
                                            <div class="   hname">ULO GREEN HILLS RESORT</div>
                                            <div class=" addsec">
                                                <meta itemProp="addressLocality" content="NO.32, AGA ABBAS ALI RD, YELLAPPA CHETTY LAYOUT" />
                                                <meta itemProp="addressRegion" content="bangalore" />
                                                <div class=" addpad">
                                                    <div class=" addcolor">KODAIKANAL</div>
                                                </div>
                                            </div>
                                            <div class=" socialreview  ">
                                                <meta itemProp="ratingValue" content="3.5" />
                                                <meta itemProp="ratingCount" content="275" />
                                                <meta itemProp="bestRating" content="5" />
                                                <!-- Hotel-Rating -->
                                                <span class="make_flex">
                                          <svg height="10px" version="1.1" viewBox="0 0 11 10" width="11px" >
                                             <polygon fill="#e57520" fill-rule="evenodd" points="5.33950617 7.65957447 2.13580247 10 3.2037037 6.38297872 0 3.61702128 3.84444444 3.61702128 5.33950617 0 6.62098765 3.61702128 10.6790123 3.61702128 7.47530864 6.38297872 8.54320988 10" stroke="none"></polygon>
                                          </svg>
                                          <svg height="10px" version="1.1" viewBox="0 0 11 10" width="11px" >
                                             <polygon fill="#e57520" fill-rule="evenodd" points="5.33950617 7.65957447 2.13580247 10 3.2037037 6.38297872 0 3.61702128 3.84444444 3.61702128 5.33950617 0 6.62098765 3.61702128 10.6790123 3.61702128 7.47530864 6.38297872 8.54320988 10" stroke="none"></polygon>
                                          </svg>
                                          <svg height="10px" version="1.1" viewBox="0 0 11 10" width="11px" >
                                             <polygon fill="#e57520" fill-rule="evenodd" points="5.33950617 7.65957447 2.13580247 10 3.2037037 6.38297872 0 3.61702128 3.84444444 3.61702128 5.33950617 0 6.62098765 3.61702128 10.6790123 3.61702128 7.47530864 6.38297872 8.54320988 10" stroke="none"></polygon>
                                          </svg>
                                          <svg height="10px" version="1.1" viewBox="0 0 11 10" width="11px" >
                                             <polygon fill="#e57520" fill-rule="evenodd" points="5.33950617 7.65957447 2.13580247 10 3.2037037 6.38297872 0 3.61702128 3.84444444 3.61702128 5.33950617 0 6.62098765 3.61702128 10.6790123 3.61702128 7.47530864 6.38297872 8.54320988 10" stroke="none"></polygon>
                                          </svg>
                                          <svg height="10px" version="1.1" viewBox="0 0 11 10" width="11px" >
                                             <polygon fill="#b7b7b7" fill-rule="evenodd" points="5.33950617 7.65957447 2.13580247 10 3.2037037 6.38297872 0 3.61702128 3.84444444 3.61702128 5.33950617 0 6.62098765 3.61702128 10.6790123 3.61702128 7.47530864 6.38297872 8.54320988 10" stroke="none"></polygon>
                                          </svg>
                                       </span>
                                                <!-- /Hotel-Rating -->
                                                <div class=" reviewpad  ">
                                                    <div class=" ropen">
                                                    </div>
                                                </div>
                                                <div class=" retext"></div>
                                                <div class=" retext"><span>    80 Google Reviews</span></div>
                                            </div>
                                        </div>
                                        <div class=" ">
                                            <div>
                                                <div class=" hprice">
                                                    &#8377; 1200
                                                </div>
                                                <a class="link" href="ulo-green-hills-resort">
                                                    <button class="bookbtn">SELECT ROOM</button>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <hr class="hline" />
                        </div>

                        <div class=" seotag  ">
                            <div class=" seoh1">KODAIKANAL HOTELS AND RESORTS</div>
                            <div class="seobox">
                                <input type="checkbox" id="seopara" />
                                <div>
                                    Popularly known as the Princess of Hill Stations, Kodaikanal is a popular hill station in the Dindugal district of Tamil Nadu. The word Kodaikanal means a place to see in summer. It is located at an altitude of 2,133 meters above sea level. The hillsides are sprinkled with grasslands and meadows. It is popular for its flourishing flora like eucalyptus, cypress, and acacia. Due to its astonishing backdrop, it is used for film shootings as well. The chief places of interest in Kodaikanal are Kodaikanal Lake, Bryant Park, Coakers Walk, Kudhanthai Velappar Temple and Pine Forests. It is frequently visited by honeymooners, family and group vacationers.
                                    <br> The weather is pleasant and cool throughout the year due to the high elevation of the place. Monsoon is the perfect time to visit Kodaikanal as the climate is breezy and usually stays between 20 degree Celsius to 30 degree Celsius. Even during the summertime, the temperature ranges between 24 to 38 degree Celsius. This is the best time to indulge in cycling, trekking, boating, sightseeing and annual festivals. In the winter season, sometimes the temperature falls to freezing point and beyond at night time. September to May is the ideal time to visit Kodaikanal. The backdrop is colorful during these months.
                                    <br> Despite the influence of tourists, Kodaikanal has managed to maintain its old traditions and culture. The festivals celebrated in Kodaikanal are Natyanjali Festival, Chithirai Festival, Pongal, Summer Festival and Tea and Tourism Festival. The Natyanjali Festival is conducted in the month of February or March each year. On this special day, dancers from every part of India gather here to pay tribute to Nataraja (God of Dances). Named after the Tamil month Chithirai, Chithirai Festival is celebrated every year in April month. It is celebrated for 10 long days with a procession of Lord Vishnu. Pongal is celebrated with great zeal in Kodaikanal. It is a harvest festival observed in the honor of farmers and farm animals.Know more about the culture, history, and lifestyle of the local people in Kodaikanal by staying at <a href="https://www.ulohotels.com/kodaikanal-hotels-and-resorts">budget hotels in Kodaikanal</a>.
                                    <br> <b>ULO HOTELS IN KODAIKANAL:</b> If you are searching for good accommodations, there are many <a href="https://www.ulohotels.com/kodaikanal-hotels-and-resorts">hotels in Kodaikanal</a> which you can book at reasonable rates. The different accommodation types you can see in Kodaikanal are hotels, cottages, resorts, bungalows, dormitories, and homestays. The <a href="https://www.ulohotels.com/kodaikanal-hotels-and-resorts">resorts in Kodaikanal</a> are the best choice for all types of guest like solo travelers, group trippers, families, female vacationers, and couples. For budget travelers, there are many <a href="https://www.ulohotels.com/kodaikanal-hotels-and-resorts">cheap hotels in Kodaikanal</a> that you can book at the best prices. If you are traveling as a couple, book hotels in Kodaikanal for honeymoon couples. If you are a nature enthusiast, Ulo Hotels offers hotels in Kodaikanal near lake set in an alluring backdrop. It is the best choice for enjoying the astounding beauty of Kodaikanal.
                                    <br> <b>WHY CHOOSE A ULO IN KODAIKANAL:</b> The first confusion that arises in everyone mind is to find a quality accommodation in Kodaikanal. If you are looking for the <a href="https://www.ulohotels.com/kodaikanal-hotels-and-resorts">best hotels in Kodaikanal</a>, book an Ulo Hotel. Ulo Hotels provides first-class amenities and services like 24*7 Help Desk, Complimentary Breakfast, Welcome Drink, Free Wi-Fi, AC Rooms, LCD TV, Hygienic Bathrooms with Toiletries Kit, Clean and Comfortable Beds. Other additional amenities and services offered across different Kodaikanal hotels are Parking Space, Travel Desk and Laundry Services. All Ulo Hotels are located in main locations with well-connected transport routes. Ulo Hotels boasts its unique 5 Bs services like Best In Guest Service and Care, Bed Clean and Comfortable, Budget Friendly and Value, Breakfast Healthy and Regional and Bathroom Hygienic and Functional. You can view the room types and prices on Ulo Hotels official website.
                                    <br>
                                    <h2><center>THINGS TO DO IN KODAIKANAL:</center></h2> Places to Stay in Kodaikanal: Prior booking Kodaikanal resorts, check basic criteria like the location of the resort, prices, facilities, and services. We provide budget-friendly accommodations for all our guests without compromising the overall quality. If you want to stay in a peaceful and romantic setting with your loved ones, opt for resorts in Kodaikanal for unmarried couples. If you prefer accommodations near major tourist attractions, book hotels in Kodaikanal near bus stand. If you are looking for a quiet weekend retreat with your family, choose our best <a href="https://www.ulohotels.com/kodaikanal-hotels-and-resorts">cottages in Kodaikanal</a>.
                                    <br> <b>Attractions in Kodaikanal:</b> Kodaikanal has many beautiful natural attractions that are enjoyed by all tourists. It is a famous romantic destination for honeymoon couples. Kodaikanal Lake is a man-made, star-shaped lake and is regarded as Kodaikanal most popular tourist spot. You can hire rowboats and pedal boats at the Kodaikanal Boat Club. You can also rent bicycles and horses available near the lake. Bryant Park is named after the forest officer H.D.Bryant, who built it in 1908. It is a charming botanical garden with different species of trees and shrubs. Donot miss the Flower Shows organized in summertime every year. Built by Lt.Coaker in 1872, Coakers Walk is a one-kilometer pedestrian path. You can observe a rare phenomenon called Brock spectre, where you can see your shadow on the clouds with a rainbow halo. Other than these places there are other tourist spots like Guna caves, Kodaikanal Solar Observatory, Silver Cascade Falls, Dolphin Nose and Kurinji Andavar Murugan Temple.
                                    <br> <b>Food and Shopping:</b> Kodaikanal delights the tourists with several interesting options for shopping. Your trip cannot be considered complete without shopping and trying the local delicacies of Kodaikanal. The street shops are filled with a wide variety of handmade articles, woolen materials, embroidery clothes, walnut wood articles, leather items and jewelry. The best buys in Kodaikanal are homemade chocolates, cheese, muffins, herbal tea, homegrown honey, and marshmallows. You can buy fresh fruits and vegetables at reasonable rates. Other than this, you can find stalls for medicinal herbs and oils as well. You can shop more with even a moderate budget. It is recommended to bargain before buying any product in Kodaikanal. If you are a food lover, then Kodaikanal is definitely a heaven for you. The restaurants in Kodaikanal offer different cuisines like South Indian, Tibetian, Continental and Grill. Donot forget to try famous local items like fried momos, thupka, chicken pot pie and pastry varieties.Do visit Kodaikanal during the seasonal time, to enjoy its panoramic beauty to the fullest </div>
                                <label class=" lmore" for="seopara"></label>
                            </div>
                        </div>
                    </div>
                    <div class="footer     footstyle  ">
                        <a class="link" href="https://www.ulohotels.com">
                            <div class=" footbox  ">
                                <div class=" foottext">
                                    Ulohotels Private Limited 2018-2019
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <amp-install-serviceworker src="/serviceWorker.js" data-iframe-src="https://www.ulohotels.com/" layout="nodisplay">
    </amp-install-serviceworker>
   <amp-analytics config="https://www.googletagmanager.com/amp.json?id=GTM-NXVPRL8&gtm.url=SOURCE_URL" data-credentials="include"></amp-analytics>

</body>

</html>