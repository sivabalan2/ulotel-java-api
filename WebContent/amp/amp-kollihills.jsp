<!doctype html>
<html amp lang="en">

<head>
    <meta charset="utf-8" />

    <meta name="viewport" content="width=device-width,minimum-scale=1">
 <title>Book Hotels In Kolli Hills, Flat 1000 Offer with Free Breakfast</title> 
<meta name="description" content="Book hotels in kolli hills and get Free Breakfast, Free WiFi, AC Rooms, LCD TV and Comfortable Beds. Reserve best budget kolli hills hotels."> 
<meta name="keywords" content="kolli hills resort, kolli hills hotels, rooms in kolli hills, kolli hills accommodation, budget hotels in kolli hills"/> 
<meta property="og:site_name" content="ULO Hotels" /> 
<meta property="og:description" content="Book hotels in kolli hills and get Free Breakfast, Free WiFi, AC Rooms, LCD TV and Comfortable Beds. Reserve best budget kolli hills hotels." /> 
<meta property="og:title" content="Book Hotels In Kolli Hills, Flat 1000 Offer with Free Breakfast" /> 
<meta property="og:publisher" content="https://www.facebook.com/Ulohotels" /> 
<meta property="og:url" content=" https://www.ulohotels.com/kolli-hills-resorts " />
    <link rel="canonical" href="https://www.ulohotels.com/kolli-hills-resorts">
    <meta content="origin" name="referrer" />
    <meta name="google" content="notranslate">
    <meta http-equiv="Content-Language" content="en">
    <meta name="amp-google-client-id-api" content="googleanalytics">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
<style amp-boilerplate>
			body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}
		</style>
		<noscript>
         <style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style>
      </noscript>
    <style amp-custom>
        .link {
            text-decoration: none;
        }
        
        .hide {
            display: none;
        }
        
        .fwidth {
            width: 100%;
        }
        
        .header {
            -webkit-align-items: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-flex-direction: row;
            -ms-flex-direction: row;
            flex-direction: row;
            -webkit-box-pack: justify;
            -webkit-justify-content: space-between;
            -ms-flex-pack: justify;
            justify-content: space-between;
        }
        
        .socialreview {
            -webkit-align-items: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-flex-direction: row;
            -ms-flex-direction: row;
            flex-direction: row;
        }
        
        .hoteldetail {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-flex-direction: row;
            -ms-flex-direction: row;
            flex-direction: row;
            -webkit-box-pack: justify;
            -webkit-justify-content: space-between;
            -ms-flex-pack: justify;
            justify-content: space-between;
        }
        
        .footbox {
            -webkit-align-items: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-flex-direction: row;
            -ms-flex-direction: row;
            flex-direction: row;
            -webkit-box-pack: center;
            -webkit-justify-content: center;
            -ms-flex-pack: center;
            justify-content: center;
        }
        
        html {
            box-sizing: border-box;
        }
        
        body {
            background: #f1f1f1;
            color: #4a4a4a;
            font-size: 14px;
            font-weight: 400;
            font-family: 'Open Sans', sans-serif;
            margin: 0;
        }
        
        *,
        *::after,
        *::before {
            line-height: 1.2;
            box-sizing: inherit;
        }
        
        body {
            background: #ffffff;
        }
        /* sc-component-id:  */
        
        .h1tag {
            margin: 12px;
        }
        
        .coupad {
            padding: 16px;
        }
        
        .reviewpad {
            padding: 0px 0px 0px 8px;
        }
        
        .hdpad {
            margin: 16px 0px;
            padding: 0px 16px;
        }
        
        .addpad {
            margin: 8px 0px 8px 0px;
        }
        
        .seotag {
            margin: 24px 16px 32px 16px;
        }
        
        #logoid {
            padding-left: 30px;
        }
        
        .headtxt {
            color: #aeaeae;
            font-size: 14px;
            overflow: hidden;
            white-space: nowrap;
            text-overflow: ellipsis;
            padding-right: 30px;
            padding-top: 5px;
            margin: 0;
        }
        
        .locname {
            font-size: 16px;
            padding: 0;
            margin: 0;
            font-weight: 600;
        }
        
        .offname {
            color: #ffffff;
            font-size: 14px;
            font-weight: 500;
            padding: 0;
            margin: 0;
        }
        
        .ropen {
            color: #4a4a4a;
            font-size: 14px;
            padding: 0;
            margin: 0;
        }
        
        .hname {
            color: #693;
            font-size: 16px;
            font-weight: 600;
            padding: 0;
            margin: 0;
        }
        
        .addsec {
            color: #727171;
            font-size: 16px;
            font-weight: 600;
            padding: 0;
            margin: 0;
        }
        
        .addcolor {
            color: #727171;
            font-size: 14px;
            font-weight: 400;
            padding: 0;
            margin: 0;
        }
        
        .retext {
            color: #727171;
            font-size: 14px;
            padding: 0;
            margin: 0;
        }
        
        .hprice {
            color: #069;
            font-size: 28px;
            font-weight: 600;
            padding: 0;
            margin: 0;
        }
        
        .seoh1 {
            color: #4a4a4a;
            font-size: 18px;
            font-weight: 500;
            padding: 0;
            margin: 0;
        }
        
        .lmore {
            color: #5768e9;
            font-size: 14px;
            padding: 0;
            margin: 0;
        }
        
        .foottext {
            color: #4a4a4a;
            font-size: 12px;
            font-weight: 600;
            padding: 5px;
            margin: 0;
            text-align: center;
        }
        
        .footer {
            bottom: 0;
            z-index: 20;
            margin: 5px;
        }
        
        .offbox {
            border-width: 1px;
            border-style: solid;
            border-radius: 2px;
            border-color: #699;
            background-color: #699;
        }
        
        .footstyle {
            border-width: 1px;
            border-style: solid;
            border-radius: 2px;
            border-color: #ffffff;
            background-color: #ffffff;
            text-align: center;
        }
        
        .rollscale {
            -webkit-filter: grayscale(0);
            filter: grayscale(0);
            border-radius: 2px;
        }
        
        .seobox > div {
            height: 100px;
            overflow: hidden;
        }
        
        .seobox > label::before {
            content: 'Show More';
        }
        
        .seobox #seopara {
            display: none;
        }
        
        .seobox #seopara:checked + div {
            height: auto;
        }
        
        .seobox #seopara:checked ~ label::before {
            content: 'Show Less';
        }
        /* sc-component-id:  */
        
        .carimg .amp-carousel-button {
            display: none;
        }
        
        .hline {
            border-width: 1px 0 0;
            border-color: #f1f1f1;
            border-style: solid;
            background: rgba(0, 0, 0, 0);
            margin: 0;
        }
        
        .headercontact {
            color: #000;
            text-decoration: none;
            font-weight: 400;
            margin-top: 5px;
            fon-size: 14px;
            text-align: center;
            display: block;
        }
        
        .selfintro {
            font-size: 12px;
            pading: 10px;
        }
        
        .offcode {
            border: 1px solid #fff;
            padding: 5px;
        }
        
        .bookbtn {
            color: #fff;
            background: #693;
            padding: 7px;
            font-size: 12px;
            border: none;
            margin: 5px;
        }
        
        .hotellist {
            box-shadow: 5px 5px #e6e7e8;
        }
    </style>
    <script async src="https://cdn.ampproject.org/v0.js"></script>
    <script async custom-element="amp-install-serviceworker" src="https://cdn.ampproject.org/v0/amp-install-serviceworker-0.1.js"></script>
    <script async custom-element="amp-analytics" src="https://cdn.ampproject.org/v0/amp-analytics-0.1.js"></script>
    <script async custom-element="amp-lightbox-gallery" src="https://cdn.ampproject.org/v0/amp-lightbox-gallery-0.1.js"></script>
    <script async custom-element="amp-carousel" src="https://cdn.ampproject.org/v0/amp-carousel-0.1.js"></script>
    <script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>
    <script async custom-template="amp-mustache" src="https://cdn.ampproject.org/v0/amp-mustache-0.2.js"></script>
    <script async custom-element="amp-list" src="https://cdn.ampproject.org/v0/amp-list-0.1.js"></script>
    <script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>
</head>

<body>
    <div>
        <div class="  " id="wrapper">
            <div class="   ">
                <div id="logosec">
                    <div class="   header  ">
                        <a id="logoid" href="https://www.ulohotels.com" class="   ">
                            <amp-img class="" src="amp/logo/ulo4.png" alt="logo" width="120" height="40">
                            </amp-img>
                        </a>
                        <div class="headtxt">
                            <a href="tel:9543592593" title="+91 95435 92593" class="headercontact"><span >Call @ 9543 592 593</span></a>
                        </div>
                    </div>
                    <div id="cousec">
                        <div class=" h1tag  ">
                            <div>
                          
                                </div>
                            </div>
                        </div>
                        <div class=" h1tag  ">
                            <div class=" locname">KOLLI HILLS HOTELS</div>
                        </div>
                        <div class="hotellist">
                            <a class="link" href="ulo-hill-palace">
                                <div class="    ">
                                    <amp-carousel width="auto" height="150px" layout="fixed-height" controls="" class=" carimg" type="carousel">
                                        <div class="   "></div>
                                        <amp-img class=" rollscale" src="amp/ulo-hill-palace/1.png" alt="ulo-hill-palace" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-hill-palace/2.png" alt="ulo-hill-palace" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-hill-palace/3.png" alt="ulo-hill-palace" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-hill-palace/4.png" alt="ulo-hill-palace" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-hill-palace/5.png" alt="ulo-hill-palace" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-hill-palace/6.png" alt="ulo-hill-palace" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-hill-palace/7.png" alt="ulo-hill-palace" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-hill-palace/8.png" alt="ulo-hill-palace" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-hill-palace/9.png" alt="ulo-hill-palace" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-hill-palace/10.png" alt="ulo-hill-palace" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-hill-palace/11.png" alt="ulo-hill-palace" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-hill-palace/12.png" alt="ulo-hill-palace" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-hill-palace/13.png" alt="ulo-hill-palace" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-hill-palace/14.png" alt="ulo-hill-palace" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-hill-palace/15.png" alt="ulo-hill-palace" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-hill-palace/16.png" alt="ulo-hill-palace" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-hill-palace/17.png" alt="ulo-hill-palace" width="200" height="150">
                                        </amp-img>
                                    </amp-carousel>
                                </div>
                                <div class=" hdpad  ">
                                    <div class=" hoteldetail  ">
                                        <div class=" ">
                                            <meta itemProp="telephone" content="+91-9543592593" />
                                            <meta itemProp="email" content="reservations@ulohotels.com" />
                                            <div class="   hname">ULO HILL PALACE</div>
                                            <div class=" addsec">
                                                <meta itemProp="addressLocality" content="NO.32, AGA ABBAS ALI RD, YELLAPPA CHETTY LAYOUT" />
                                                <meta itemProp="addressRegion" content="bangalore" />
                                                <div class=" addpad">
                                                    <div class=" addcolor">KOLLI HILLS</div>
                                                </div>
                                            </div>
                                            <div class=" socialreview  ">
                                                <meta itemProp="ratingValue" content="3.5" />
                                                <meta itemProp="ratingCount" content="275" />
                                                <meta itemProp="bestRating" content="5" />
                                                <!-- Hotel-Rating -->
                                                <span class="make_flex">
                                          <svg height="10px" version="1.1" viewBox="0 0 11 10" width="11px" >
                                             <polygon fill="#e57520" fill-rule="evenodd" points="5.33950617 7.65957447 2.13580247 10 3.2037037 6.38297872 0 3.61702128 3.84444444 3.61702128 5.33950617 0 6.62098765 3.61702128 10.6790123 3.61702128 7.47530864 6.38297872 8.54320988 10" stroke="none"></polygon>
                                          </svg>
                                          <svg height="10px" version="1.1" viewBox="0 0 11 10" width="11px" >
                                             <polygon fill="#e57520" fill-rule="evenodd" points="5.33950617 7.65957447 2.13580247 10 3.2037037 6.38297872 0 3.61702128 3.84444444 3.61702128 5.33950617 0 6.62098765 3.61702128 10.6790123 3.61702128 7.47530864 6.38297872 8.54320988 10" stroke="none"></polygon>
                                          </svg>
                                          <svg height="10px" version="1.1" viewBox="0 0 11 10" width="11px" >
                                             <polygon fill="#e57520" fill-rule="evenodd" points="5.33950617 7.65957447 2.13580247 10 3.2037037 6.38297872 0 3.61702128 3.84444444 3.61702128 5.33950617 0 6.62098765 3.61702128 10.6790123 3.61702128 7.47530864 6.38297872 8.54320988 10" stroke="none"></polygon>
                                          </svg>
                                          <svg height="10px" version="1.1" viewBox="0 0 11 10" width="11px" >
                                             <polygon fill="#b7b7b7" fill-rule="evenodd" points="5.33950617 7.65957447 2.13580247 10 3.2037037 6.38297872 0 3.61702128 3.84444444 3.61702128 5.33950617 0 6.62098765 3.61702128 10.6790123 3.61702128 7.47530864 6.38297872 8.54320988 10" stroke="none"></polygon>
                                          </svg>
                                          <svg height="10px" version="1.1" viewBox="0 0 11 10" width="11px" >
                                             <polygon fill="#b7b7b7" fill-rule="evenodd" points="5.33950617 7.65957447 2.13580247 10 3.2037037 6.38297872 0 3.61702128 3.84444444 3.61702128 5.33950617 0 6.62098765 3.61702128 10.6790123 3.61702128 7.47530864 6.38297872 8.54320988 10" stroke="none"></polygon>
                                          </svg>
                                       </span>
                                                <!-- /Hotel-Rating -->
                                                <div class=" reviewpad  ">
                                                    <div class=" ropen">
                                                    </div>
                                                </div>
                                                <div class=" retext"></div>
                                                <div class=" retext"><span>    48 Google Reviews</span></div>
                                            </div>
                                        </div>
                                        <div class=" ">
                                            <div>
                                                <div class=" hprice">
                                                    &#8377; 2200
                                                </div>
                                                <a class="link" href="ulo-hill-palace">
                                                    <button class="bookbtn">SELECT ROOM</button>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <hr class="hline" />
                        </div>
                        <div class=" seotag  ">
                            <div class=" seoh1">KOLLI HILLS HOTELS AND RESORTS</div>
                            <div class="seobox">
                                <input type="checkbox" id="seopara" />
                                <div>
                                    Set at an altitude of 1300 meters above sea level, Kolli Hills (Kolli Hills) is a hill station in Namakkal, Tamil Nadu. It is a small mountain range and is the part of the Eastern Ghats. Kolli Hills is a pilgrimage destination attracting a multitude of devotees each year. Kolli Hills got its name from the local deity Goddess Ettukkai Amman (Kolli Pavai). According to the local saying, Goddess Kolli Pavai chased away the demons troubling the sadhus with her mystical smile. It is a less explored terrain when compared to other hill stations in South India.
                                    <br> Kolli Hills is a haven for nature enthusiasts, trekking clubs, hikers and meditation practitioners. It has a rich collection of unique, exotic flora and fauna. The lush green verdure is a treat to watch during the spring and monsoon season. Kolli Hills is an all-season destination as the climate is pleasant all around the year. You will not feel the heat of the sun even during the summer season. We recommend tourists to carry winter clothes during their trip to Kolli Hills. The best time to visit Kolli Hills is during the months of March to June.
                                    <br> The chief festival celebrated in Kolli hills is the Valvil Ori festival. It is celebrated every August to honor the glory of King Valvil ori who was the ruler of Kolli Hills in 200 AD. He is famous for his generosity and archery skills. The State Government officials will arrange folk dances, cultural programs and skits highlighting the life of Valvil Ori. The trip to Kolli Hills will definitely a life-changing experience for all.If you want to know more about the culture and lifestyle of the tribal folks, there are many <a href="https://www.ulohotels.com/kollihills-hotels-and-resorts">budget hotels in Kolli Hills</a> where you can stay at reasonable prices.
                                    <br> <b>ULO HOTELS IN KOLLI HILLS:</b> If you are searching for your best Ulo Hotels, there are many <a href="https://www.ulohotels.com/kollihills-hotels-and-resorts">best hotels in Kolli Hills</a>, which you can book within your budget. If you want to look for hotels or resorts in Kolli Hills, search as Kolli Hills accommodation on the internet. Ulo Hotels is the best accommodation choice for families, couples, solo travelers, female trippers and group vacationers who are searching for <a href="https://www.ulohotels.com/kollihills-hotels-and-resorts">hotels in Kolli Hills</a> at affordable prices. You can directly book Kolli Hills hotels on our website with best offers and deals. So why worry? Choose Kollimalai resorts from Ulo that are suitable for all budget types.
                                    <br> <b>WHY CHOOSE A ULO IN KOLLI HILLS:</b> The first thing that comes to our mind while planning for a trip is finding quality stays within the desired budget. If you are searching for <a href="https://www.ulohotels.com/kollihills-hotels-and-resorts">cheap hotels in Kolli hills</a>, choose an Ulo Hotel. Ulo Hotels provides best quality amenities and services like Complimentary Breakfast, Free Wi-Fi, Air-conditioned Rooms, Welcome Drink, LCD TV with cable connection, Clean Bathrooms with Toiletries Kit, Clean and Comfortable Beds. Special facilities and services offered across the <a href="https://www.ulohotels.com/kollihills-hotels-and-resorts">resorts in Kolli Hills</a> are Parking Space, Ayurvedic Spa, and Laundry Services. All our cottages in Kolli Hills are situated in main locations with easy transportation services. We are known for our quality 5 Bs services like Budget Friendly and Value, Breakfast Healthy and Regional, Best In Guest Service and Care, Bed Clean and Comfortable and Bathroom Hygienic and Functional. You can view the hotels in Kolli Hills with rates and room prices on our Ulo Hotels official website.
                                    <br>
                                    <h2><center>THINGS TO DO IN KOLLI HILLS</center></h2> <b>Places to Stay in Kolli Hills:</b> When searching for Kollimalai hotels, check its cost, locations, facilities, and services before booking your rooms. Ulo Hotels offers hotels and resorts that are suitable for all sorts of vacationers. If you want to stay in a peaceful setting with your loved ones, choose our Kollimalai cottages. If you are looking for luxury accommodations, you can opt for our 2 or 3-star hotels in Kolli Hills. If you are staying with your entire family, book Kolli Hills resorts in the prime location with modish amenities and services. Ulo Hotels offers special offers and discounts for repetitive guests. If you want to stay away from the hustle-bustle daily routine, then book our <a href="https://www.ulohotels.com/kollihills-hotels-and-resorts">cottages in Kolli Hills</a> that are set in a peaceful backdrop. Kolli Hills is definitely a paradise for people who love exploring new places.
                                    <br> <b>Attractions in Kolli Hills:</b> Kolli Hills has many beautiful sightseeing places that will leave everyone spellbound. It is an ideal destination spot for people who love nature and adventure. As the climate is pleasant throughout the year, you can visit Kolli Hills whenever you get free time. You can see all the tourist spots within 2 or 3 days time. Kolli Hills houses two alluring waterfalls namely Agaya Gangai Falls and Mini Falls. The path to the Agaya Gangai Falls is a famous trekking route which is sprinkled with lush verdure. If you want to learn more about the history of Kolli Hills, your next stop should be Selur Nadu. It depicts the good deeds committed by the benevolent King Pari. The Boat House in Vasalurpatty, Kolli Hills is a famous picnic spot. Enjoy riding motor boats and paddle boats with your loved ones on this serene lake. Other than these spots, Kolli Hills also has other attractions like Arapaleeshwarar Temple, Botanical Garden, Seekuparai, Siddhar Caves and Tampcol Medicinal Farm.
                                    <br> <b>Food and Shopping:</b> Your trip is not complete without buying the best stuff from Kolli Hills. Wild Honey, Jackfruit, Spices, Coffee, and Honey are the must buy things from Kollimalai. The Shandai or Bi-weekly market is held every Wednesdays and Saturdays where you can buy the local produce at reasonable rates. The shops are generally open from 9 am to 6 pm. Do try the fresh fruits like pineapple, jackfruit, and tapioca sold in the streets by the local vendors. Donot forget to taste the local favorite, Naatu Kozhi Varuval at any eateries in Kolli Hills.You will surely enjoy your trip to Kolli Hills as it is a paradise on earth with beautiful places to see and relish.
                                </div>
                                <label class=" lmore" for="seopara"></label>
                            </div>
                        </div>
                    </div>
                    <div class="footer     footstyle  ">
                        <a class="link" href="https://www.ulohotels.com">
                            <div class=" footbox  ">
                                <div class=" foottext">
                                    Ulohotels Private Limited 2018-2019
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <amp-install-serviceworker src="/serviceWorker.js" data-iframe-src="https://www.ulohotels.com/" layout="nodisplay">
    </amp-install-serviceworker>
<amp-analytics config="https://www.googletagmanager.com/amp.json?id=GTM-NXVPRL8&gtm.url=SOURCE_URL" data-credentials="include"></amp-analytics>

</body>

</html>