<!doctype html>
<html amp lang="en">

<head>
    <meta charset="utf-8" />

    <meta name="viewport" content="width=device-width,minimum-scale=1">
 <title>Hotels in Ooty, Get Upto 50% OFF | Free Breakfast</title> <meta name="description" content="Book budget hotels in Ooty also get &#10003;Free Breakfast &#10003;Free WiFi &#10003;Free Toiletries & &#10003;Free Cancellation in Ooty hotels."> <link rel="canonical" href="https://www.ulohotels.com/ooty-hotels" /> <meta name="robots" content="index, follow"> <meta name="google" content="notranslate">
<meta http-equiv="Content-Language" content="en"> <meta content="origin" name="referrer" />
    <meta name="google" content="notranslate">
    <meta http-equiv="Content-Language" content="en">
    <meta name="amp-google-client-id-api" content="googleanalytics">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
<style amp-boilerplate>
			body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}
		</style>
		<noscript>
         <style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style>
      </noscript>
    <style amp-custom>
        .link {
            text-decoration: none;
        }
        
        .hide {
            display: none;
        }
        
        .fwidth {
            width: 100%;
        }
        
        .header {
            -webkit-align-items: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-flex-direction: row;
            -ms-flex-direction: row;
            flex-direction: row;
            -webkit-box-pack: justify;
            -webkit-justify-content: space-between;
            -ms-flex-pack: justify;
            justify-content: space-between;
        }
        
        .socialreview {
            -webkit-align-items: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-flex-direction: row;
            -ms-flex-direction: row;
            flex-direction: row;
        }
        
        .hoteldetail {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-flex-direction: row;
            -ms-flex-direction: row;
            flex-direction: row;
            -webkit-box-pack: justify;
            -webkit-justify-content: space-between;
            -ms-flex-pack: justify;
            justify-content: space-between;
        }
        
        .footbox {
            -webkit-align-items: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-flex-direction: row;
            -ms-flex-direction: row;
            flex-direction: row;
            -webkit-box-pack: center;
            -webkit-justify-content: center;
            -ms-flex-pack: center;
            justify-content: center;
        }
        
        html {
            box-sizing: border-box;
        }
        
        body {
            background: #f1f1f1;
            color: #4a4a4a;
            font-size: 14px;
            font-weight: 400;
            font-family: 'Open Sans', sans-serif;
            margin: 0;
        }
        
        *,
        *::after,
        *::before {
            line-height: 1.2;
            box-sizing: inherit;
        }
        
        body {
            background: #ffffff;
        }
        /* sc-component-id:  */
        
        .h1tag {
            margin: 12px;
        }
        
        .coupad {
            padding: 16px;
        }
        
        .reviewpad {
            padding: 0px 0px 0px 8px;
        }
        
        .hdpad {
            margin: 16px 0px;
            padding: 0px 16px;
        }
        
        .addpad {
            margin: 8px 0px 8px 0px;
        }
        
        .seotag {
            margin: 24px 16px 32px 16px;
        }
        
        #logoid {
            padding-left: 30px;
        }
        
        .headtxt {
            color: #aeaeae;
            font-size: 14px;
            overflow: hidden;
            white-space: nowrap;
            text-overflow: ellipsis;
            padding-right: 30px;
            padding-top: 5px;
            margin: 0;
        }
        
        .locname {
            font-size: 16px;
            padding: 0;
            margin: 0;
            font-weight: 600;
        }
        
        .offname {
            color: #ffffff;
            font-size: 14px;
            font-weight: 500;
            padding: 0;
            margin: 0;
        }
        
        .ropen {
            color: #4a4a4a;
            font-size: 14px;
            padding: 0;
            margin: 0;
        }
        
        .hname {
            color: #693;
            font-size: 16px;
            font-weight: 600;
            padding: 0;
            margin: 0;
        }
        
        .addsec {
            color: #727171;
            font-size: 16px;
            font-weight: 600;
            padding: 0;
            margin: 0;
        }
        
        .addcolor {
            color: #727171;
            font-size: 14px;
            font-weight: 400;
            padding: 0;
            margin: 0;
        }
        
        .retext {
            color: #727171;
            font-size: 14px;
            padding: 0;
            margin: 0;
        }
        
        .hprice {
            color: #069;
            font-size: 28px;
            font-weight: 600;
            padding: 0;
            margin: 0;
        }
        
        .seoh1 {
            color: #4a4a4a;
            font-size: 18px;
            font-weight: 500;
            padding: 0;
            margin: 0;
        }
        
        .lmore {
            color: #5768e9;
            font-size: 14px;
            padding: 0;
            margin: 0;
        }
        
        .foottext {
            color: #4a4a4a;
            font-size: 12px;
            font-weight: 600;
            padding: 5px;
            margin: 0;
            text-align: center;
        }
        
        .footer {
            bottom: 0;
            z-index: 20;
            margin: 5px;
        }
        
        .offbox {
            border-width: 1px;
            border-style: solid;
            border-radius: 2px;
            border-color: #699;
            background-color: #699;
        }
        
        .footstyle {
            border-width: 1px;
            border-style: solid;
            border-radius: 2px;
            border-color: #ffffff;
            background-color: #ffffff;
            text-align: center;
        }
        
        .rollscale {
            -webkit-filter: grayscale(0);
            filter: grayscale(0);
            border-radius: 2px;
        }
        
        .seobox > div {
            height: 100px;
            overflow: hidden;
        }
        
        .seobox > label::before {
            content: 'Show More';
        }
        
        .seobox #seopara {
            display: none;
        }
        
        .seobox #seopara:checked + div {
            height: auto;
        }
        
        .seobox #seopara:checked ~ label::before {
            content: 'Show Less';
        }
        /* sc-component-id:  */
        
        .carimg .amp-carousel-button {
            display: none;
        }
        
        .hline {
            border-width: 1px 0 0;
            border-color: #f1f1f1;
            border-style: solid;
            background: rgba(0, 0, 0, 0);
            margin: 0;
        }
        
        .headercontact {
            color: #000;
            text-decoration: none;
            font-weight: 400;
            margin-top: 5px;
            fon-size: 14px;
            text-align: center;
            display: block;
        }
        
        .selfintro {
            font-size: 12px;
            pading: 10px;
        }
        
        .offcode {
            border: 1px solid #fff;
            padding: 5px;
        }
        
        .bookbtn {
            color: #fff;
            background: #693;
            padding: 7px;
            font-size: 12px;
            border: none;
            margin: 5px;
        }
        
        .hotellist {
            box-shadow: 5px 5px #e6e7e8;
        }
    </style>
    <script async src="https://cdn.ampproject.org/v0.js"></script>
    <script async custom-element="amp-install-serviceworker" src="https://cdn.ampproject.org/v0/amp-install-serviceworker-0.1.js"></script>
    <script async custom-element="amp-analytics" src="https://cdn.ampproject.org/v0/amp-analytics-0.1.js"></script>
    <script async custom-element="amp-lightbox-gallery" src="https://cdn.ampproject.org/v0/amp-lightbox-gallery-0.1.js"></script>
    <script async custom-element="amp-carousel" src="https://cdn.ampproject.org/v0/amp-carousel-0.1.js"></script>
    <script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>
    <script async custom-template="amp-mustache" src="https://cdn.ampproject.org/v0/amp-mustache-0.1.js"></script>
    <script async custom-element="amp-list" src="https://cdn.ampproject.org/v0/amp-list-0.1.js"></script>
    <script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>
</head>

<body>
    <div>
        <div class="  " id="wrapper">
            <div class="   ">
                <div id="logosec">
                    <div class="   header  ">
                        <a id="logoid" href="https://www.ulohotels.com" class="   ">
                            <amp-img class="" src="amp/logo/ulo4.png" alt="logo" width="120" height="40">
                            </amp-img>
                        </a>
                        <div class="headtxt">
                            <a href="tel:9543592593" title="+91 95435 92593" class="headercontact"><span >Call @ 9543 592 593</span></a>
                        </div>
                    </div>
                    <div id="cousec">
                        <div class=" h1tag  ">
                            <div>
                                <div class=" coupad  offbox  ">
                                    <div class=" ">
                                        <div class=" offname">Use this code <span class="offcode">SUMMER10</span> To Get 10 % OFF </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class=" h1tag  ">
                            <div class=" locname">COORG HOTELS AND RESORTS</div>
                        </div>
                        <div class="hotellist">
                            <a class="link" href="ulo-riviera-the-spring-arc">
                                <div class="    ">
                                    <amp-carousel width="auto" height="150px" layout="fixed-height" controls="" class=" carimg" type="carousel">
                                        <div class="   "></div>
                                        <amp-img class=" rollscale" src="amp/ulo-rivera/1.png" alt="ulo-rivera" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-rivera/2.png" alt="ulo-rivera" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-rivera/3.png" alt="ulo-rivera" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-rivera/4.png" alt="ulo-rivera" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-rivera/5.png" alt="ulo-rivera" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-rivera/6.png" alt="ulo-rivera" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-rivera/7.png" alt="ulo-rivera" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-rivera/8.png" alt="ulo-rivera" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-rivera/9.png" alt="ulo-rivera" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-rivera/10.png" alt="ulo-rivera" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-rivera/11.png" alt="ulo-rivera" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-rivera/12.png" alt="ulo-rivera" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-rivera/13.png" alt="ulo-rivera" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-rivera/14.png" alt="ulo-rivera" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-rivera/15.png" alt="ulo-rivera" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-rivera/16.png" alt="ulo-rivera" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-rivera/17.png" alt="ulo-rivera" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-rivera/18.png" alt="ulo-rivera" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ulo-rivera/19.png" alt="ulo-rivera" width="200" height="150">
                                        </amp-img>
                                    </amp-carousel>
                                </div>
                                <div class=" hdpad  ">
                                    <div class=" hoteldetail  ">
                                        <div class=" ">
                                            <meta itemProp="telephone" content="+91-9543592593" />
                                            <meta itemProp="email" content="reservations@ulohotels.com" />
                                            <div class="   hname">ULO RIVERA SPRING ARC </div>
                                            <div class=" addsec">
                                                <meta itemProp="addressLocality" content="NO.32, AGA ABBAS ALI RD, YELLAPPA CHETTY LAYOUT" />
                                                <meta itemProp="addressRegion" content="bangalore" />
                                                <div class=" addpad">
                                                    <div class=" addcolor">MADIKERI,COORG</div>
                                                </div>
                                            </div>
                                            <div class=" socialreview  ">
                                                <meta itemProp="ratingValue" content="3.5" />
                                                <meta itemProp="ratingCount" content="275" />
                                                <meta itemProp="bestRating" content="5" />
                                                <!-- Hotel-Rating -->
                                                <span class="make_flex">
                                          <svg height="10px" version="1.1" viewBox="0 0 11 10" width="11px" >
                                             <polygon fill="#e57520" fill-rule="evenodd" points="5.33950617 7.65957447 2.13580247 10 3.2037037 6.38297872 0 3.61702128 3.84444444 3.61702128 5.33950617 0 6.62098765 3.61702128 10.6790123 3.61702128 7.47530864 6.38297872 8.54320988 10" stroke="none"></polygon>
                                          </svg>
                                          <svg height="10px" version="1.1" viewBox="0 0 11 10" width="11px" >
                                             <polygon fill="#e57520" fill-rule="evenodd" points="5.33950617 7.65957447 2.13580247 10 3.2037037 6.38297872 0 3.61702128 3.84444444 3.61702128 5.33950617 0 6.62098765 3.61702128 10.6790123 3.61702128 7.47530864 6.38297872 8.54320988 10" stroke="none"></polygon>
                                          </svg>
                                          <svg height="10px" version="1.1" viewBox="0 0 11 10" width="11px" >
                                             <polygon fill="#e57520" fill-rule="evenodd" points="5.33950617 7.65957447 2.13580247 10 3.2037037 6.38297872 0 3.61702128 3.84444444 3.61702128 5.33950617 0 6.62098765 3.61702128 10.6790123 3.61702128 7.47530864 6.38297872 8.54320988 10" stroke="none"></polygon>
                                          </svg>
                                          <svg height="10px" version="1.1" viewBox="0 0 11 10" width="11px" >
                                             <polygon fill="#e57520" fill-rule="evenodd" points="5.33950617 7.65957447 2.13580247 10 3.2037037 6.38297872 0 3.61702128 3.84444444 3.61702128 5.33950617 0 6.62098765 3.61702128 10.6790123 3.61702128 7.47530864 6.38297872 8.54320988 10" stroke="none"></polygon>
                                          </svg>
                                          <svg height="10px" version="1.1" viewBox="0 0 11 10" width="11px" >
                                             <polygon fill="#b7b7b7" fill-rule="evenodd" points="5.33950617 7.65957447 2.13580247 10 3.2037037 6.38297872 0 3.61702128 3.84444444 3.61702128 5.33950617 0 6.62098765 3.61702128 10.6790123 3.61702128 7.47530864 6.38297872 8.54320988 10" stroke="none"></polygon>
                                          </svg>
                                       </span>
                                                <!-- /Hotel-Rating -->
                                                <div class=" reviewpad  ">
                                                    <div class=" ropen">
                                                    </div>
                                                </div>
                                                <div class=" retext"></div>
                                                <div class=" retext"><span>    88 Google Reviews</span></div>
                                            </div>
                                        </div>
                                        <div class=" ">
                                            <div>
                                                <div class=" hprice">
                                                    &#8377; 1500
                                                </div>
                                                <a class="link" href="ulo-riviera-the-spring-arc">
                                                    <button class="bookbtn">SELECT ROOM</button>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <hr class="hline" />
                        </div>

                        <div class=" seotag  ">
                            <div class=" seoh1">COORG HOTELS AND RESORTS</div>
                            <div class="seobox">
                                <input type="checkbox" id="seopara" />
                                <div>
                                    Commonly known as Kodagu, Coorg is set at an altitude of 120 meters above sea level. It is a hilly district in Karnataka and covers an area of 4,102 square kilometers. It is a paradise for all nature lovers and adventure freaks. Coorg is famous for its flourishing greenery and exotic fauna. The dominant community in Coorg is the Kodavas. The Kodavas are notable for their amicable hospitality. It is rated as one of the highly visited tourist places in India. The popular tourist spots in Coorg include Abbey Falls, Talacauvery, Dubare Elephant Camp, Nagarhole National Park and Namdroling Monastery.
                                    <br> The climate of Coorg is Tropical Monsoon, so the weather is pleasant around the year. If you want to enjoy the splendid greenery, then visit Coorg during Monsoons. It is also the best season for the adventurous people who love river rafting and boating. The entire town is filled with the aroma of coffee blossoms in the summertime. You can plan your vacation any time of the year without waiting for seasonal times. If you plan your trip in the summer season, pack light cotton clothing. If you plan to visit Coorg in winter time, pack some woolen clothes and apparel.
                                    <br> The Kalipoud (Kailmurtha) is celebrated every year on 3 September to signify the completion of rice crop planting. The weapons used to safeguard the crops from the wild animals are kept in the prayer room and decorated with flowers. It is then worshipped by the entire townspeople. Puttari, also known as Rice Harvest Festival takes place in late November or early December each year. The whole family assembles in their common family house and decorate their house with flowers. They travel to their paddy field with a lamp and cut the rice plants and stack them. They take this to their homes and offer it to the Gods. The entire village gathers together for a communal dinner.
                                    <br> If you want to learn and experience more about the culture and history of the Kodavas, you can stay at any of the <a href="https://www.ulohotels.com/coorg-hotels-and- resorts">budget hotels in Coorg</a> at reasonable prices.
                                    <br> <b>ULO HOTELS IN COORG:</b> If you are looking for good accommodations, there are many <a href="https://www.ulohotels.com/coorg-hotels-and-resorts">best hotels in Coorg</a> which you can book at best rates with attractive deals and offers. The common accommodation types you can find here are 3-star hotels, cottages, resorts, dormitories, and homestays. The Ulo <a href="https://www.ulohotels.com/coorg-hotels-and-resorts">hotels in Coorg</a> are the best stay option for all sorts of travelers like solo, group, families, female and couples. If you are a budget traveler, there are many <a href="https://www.ulohotels.com/coorg-hotels-and-resorts">cheap hotels in Coorg</a> that you can reserve at reasonable prices. If you are a solo traveler, book Coorg homestays which are value for money. For all nature lovers out there, Ulo Hotels offers nature stays near coffee estates and jungle stays near the lush Coorg forests. It is the perfect choice for enjoying the complete beauty of Coorg.
                                    <br> <b>WHY CHOOSE A ULO IN COORG:</b> After planning a vacation the next thing every traveler look for is finding a quality hotel in Coorg. When looking for the best hotels in Coorg, choose an Ulo Hotel. Ulo Hotels offers top-class amenities and services like 24*and Help Desk, Complimentary Breakfast, Welcome Drink, Free Wi-Fi, Air-conditioned Rooms, Flat-screen TV, Bathrooms equipped with Toiletries Kit, Clean and Comfortable Beds. Other amenities and services provided across different Coorg hotels are Lockers, Valet Parking, Spa, Bar, Travel Desk and Laundry Services. All Ulo Hotels are set in prime locations with well-connected transport routes. Ulo Hotels are known for quality 5 Bs services like Best In Guest Service and Care, Bed Clean and Comfortable, Budget Friendly and Value, Breakfast Healthy and Regional and Bathroom Hygienic and Functional. You can find the Coorg resorts list with room types and prices on Ulo Hotels website.
                                    <br>
                                    <h2><center>THINGS TO DO IN COORG</center></h2> Places to Stay in Coorg: Before booking <a href="https://www.ulohotels.com/coorg-hotels-and-resorts">resorts in Coorg</a>, consider criteria like location, rates, amenities, and services. Ulo Hotels offers pocket-friendly stays for all vacationers without compromising service quality. If you want to stay in a serene backdrop with your loved ones, opt for an estate stay in Coorg. If you prefer home-style accommodations and food, book homestay in Madikeri. If you are looking for an accommodation in a prime location with best amenities and services, choose Kodagu resorts. If you are looking for a romantic weekend retreat with your better-half, choose our best resorts in Coorg. If you want to spend a quiet weekend with your family, you can reserve <a href="https://www.ulohotels.com/coorg-hotels-and- resorts">cottages in Coorg</a> from Ulo.
                                    <br> <b>Attractions in Coorg:</b> The visit to Coorg is incomplete without visiting the chief attractions which display the heritage and culture of Coorg. Being the origin of the Kaveri River, Talacauvery is the most visited spot in Coorg. There is a temple on the riverbanks dedicated to Lord Brahma. Dubare Elephant Camp is widely liked by tourists for its elephant training camp. You can enjoy watching the daily routines of the elephants and feed them jaggery and fruits. The Abbey Falls is a scenic waterfall with a picturesque background. The ideal time to visit this falls is during monsoon when the vista is breathtaking. Nagarhole National Park is one of the best-known wildlife parks in India. It boasts of its rich and diverse flora and fauna collection. Namdroling Monastery, also known as Golden Temple is a famous Tibetian Temple. It has 18 meters high gold-plated Buddha and filled with paintings depicting the Tibetian culture and Buddha life.
                                    <br> <b>Food and Shopping:</b> Coorg is famous for its authentic Kodava cuisine. The local specialty is Sanakki which is fragrant and flavorful rice. Other local delicacies are Akki Roti, Baimbale Curry, Pulvas, Pattus, Kumm Curry and Cheke Curry. Coorg is also famous for its exotic pickles of fish, pork, mushrooms and bamboo shoots. You can find delicacies from different cuisines like South Indian, Chinese, Continental, and Tibetian Cuisines. One of the best things about touring Coorg is local markets that sell unique items from the city. The best buys in Coorg are wild honey, coffee and aromatic spices (oregano, pepper, basil, cardamom and much more). Also, buy Kachampuli which is used as a flavor enhancer for meat dishes. Your trip is incomplete without buying a silk saree as a souvenir for your loved ones. Coorg is a must visit place for every traveler who is into adventure and nature. The trip to Coorg will be definitely filled with loving memories.
                                </div>
                                <label class=" lmore" for="seopara"></label>
                            </div>
                        </div>
                    </div>
                    <div class="footer     footstyle  ">
                        <a class="link" href="https://www.ulohotels.com">
                            <div class=" footbox  ">
                                <div class=" foottext">
                                    Ulohotels Private Limited 2018-2019
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <amp-install-serviceworker src="/serviceWorker.js" data-iframe-src="https://www.ulohotels.com/" layout="nodisplay">
    </amp-install-serviceworker>
<amp-analytics config="https://www.googletagmanager.com/amp.json?id=GTM-NXVPRL8&gtm.url=SOURCE_URL" data-credentials="include"></amp-analytics>

</body>

</html>