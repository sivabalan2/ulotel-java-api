<!doctype html>
<html amp lang="en">

<head>
    <meta charset="utf-8" />

    <meta name="viewport" content="width=device-width,minimum-scale=1">
 <title>Hotels in Ooty, Get Upto 50% OFF | Free Breakfast</title>
  <meta name="description" content="Book budget hotels in Ooty also get &#10003;Free Breakfast &#10003;Free WiFi &#10003;Free Toiletries & &#10003;Free Cancellation in Ooty hotels."> 
  <link rel="canonical" href="https://www.ulohotels.com/ooty-hotels" /> 
  <meta name="robots" content="index, follow"> <meta name="google" content="notranslate">
<meta http-equiv="Content-Language" content="en"> <meta content="origin" name="referrer" />
    <meta name="google" content="notranslate">
    <meta http-equiv="Content-Language" content="en">
    <meta name="amp-google-client-id-api" content="googleanalytics">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
<style amp-boilerplate>
			body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}
		</style>
		<noscript>
         <style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style>
      </noscript>
    <style amp-custom>
        .link {
            text-decoration: none;
        }
        
        .hide {
            display: none;
        }
        
        .fwidth {
            width: 100%;
        }
        
        .header {
            -webkit-align-items: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-flex-direction: row;
            -ms-flex-direction: row;
            flex-direction: row;
            -webkit-box-pack: justify;
            -webkit-justify-content: space-between;
            -ms-flex-pack: justify;
            justify-content: space-between;
        }
        
        .socialreview {
            -webkit-align-items: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-flex-direction: row;
            -ms-flex-direction: row;
            flex-direction: row;
        }
        
        .hoteldetail {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-flex-direction: row;
            -ms-flex-direction: row;
            flex-direction: row;
            -webkit-box-pack: justify;
            -webkit-justify-content: space-between;
            -ms-flex-pack: justify;
            justify-content: space-between;
        }
        
        .footbox {
            -webkit-align-items: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            -webkit-flex-direction: row;
            -ms-flex-direction: row;
            flex-direction: row;
            -webkit-box-pack: center;
            -webkit-justify-content: center;
            -ms-flex-pack: center;
            justify-content: center;
        }
        
        html {
            box-sizing: border-box;
        }
        
        body {
            background: #f1f1f1;
            color: #4a4a4a;
            font-size: 14px;
            font-weight: 400;
            font-family: 'Open Sans', sans-serif;
            margin: 0;
        }
        
        *,
        *::after,
        *::before {
            line-height: 1.2;
            box-sizing: inherit;
        }
        
        body {
            background: #ffffff;
        }
        /* sc-component-id:  */
        
        .h1tag {
            margin: 12px;
        }
        
        .coupad {
            padding: 16px;
        }
        
        .reviewpad {
            padding: 0px 0px 0px 8px;
        }
        
        .hdpad {
            margin: 16px 0px;
            padding: 0px 16px;
        }
        
        .addpad {
            margin: 8px 0px 8px 0px;
        }
        
        .seotag {
            margin: 24px 16px 32px 16px;
        }
        
        #logoid {
            padding-left: 30px;
        }
        
        .headtxt {
            color: #aeaeae;
            font-size: 14px;
            overflow: hidden;
            white-space: nowrap;
            text-overflow: ellipsis;
            padding-right: 30px;
            padding-top: 5px;
            margin: 0;
        }
        
        .locname {
            font-size: 16px;
            padding: 0;
            margin: 0;
            font-weight: 600;
        }
        
        .offname {
            color: #ffffff;
            font-size: 14px;
            font-weight: 500;
            padding: 0;
            margin: 0;
        }
        
        .ropen {
            color: #4a4a4a;
            font-size: 14px;
            padding: 0;
            margin: 0;
        }
        
        .hname {
            color: #693;
            font-size: 16px;
            font-weight: 600;
            padding: 0;
            margin: 0;
        }
        
        .addsec {
            color: #727171;
            font-size: 16px;
            font-weight: 600;
            padding: 0;
            margin: 0;
        }
        
        .addcolor {
            color: #727171;
            font-size: 14px;
            font-weight: 400;
            padding: 0;
            margin: 0;
        }
        
        .retext {
            color: #727171;
            font-size: 14px;
            padding: 0;
            margin: 0;
        }
        
        .hprice {
            color: #069;
            font-size: 28px;
            font-weight: 600;
            padding: 0;
            margin: 0;
        }
        
        .seoh1 {
            color: #4a4a4a;
            font-size: 18px;
            font-weight: 500;
            padding: 0;
            margin: 0;
        }
        
        .lmore {
            color: #5768e9;
            font-size: 14px;
            padding: 0;
            margin: 0;
        }
        
        .foottext {
            color: #4a4a4a;
            font-size: 12px;
            font-weight: 600;
            padding: 5px;
            margin: 0;
            text-align: center;
        }
        
        .footer {
            bottom: 0;
            z-index: 20;
            margin: 5px;
        }
        
        .offbox {
            border-width: 1px;
            border-style: solid;
            border-radius: 2px;
            border-color: #699;
            background-color: #699;
        }
        
        .footstyle {
            border-width: 1px;
            border-style: solid;
            border-radius: 2px;
            border-color: #ffffff;
            background-color: #ffffff;
            text-align: center;
        }
        
        .rollscale {
            -webkit-filter: grayscale(0);
            filter: grayscale(0);
            border-radius: 2px;
        }
        
        .seobox > div {
            height: 100px;
            overflow: hidden;
        }
        
        .seobox > label::before {
            content: 'Show More';
        }
        
        .seobox #seopara {
            display: none;
        }
        
        .seobox #seopara:checked + div {
            height: auto;
        }
        
        .seobox #seopara:checked ~ label::before {
            content: 'Show Less';
        }
        /* sc-component-id:  */
        
        .carimg .amp-carousel-button {
            display: none;
        }
        
        .hline {
            border-width: 1px 0 0;
            border-color: #f1f1f1;
            border-style: solid;
            background: rgba(0, 0, 0, 0);
            margin: 0;
        }
        
        .headercontact {
            color: #000;
            text-decoration: none;
            font-weight: 400;
            margin-top: 5px;
            fon-size: 14px;
            text-align: center;
            display: block;
        }
        
        .selfintro {
            font-size: 12px;
            pading: 10px;
        }
        
        .offcode {
            border: 1px solid #fff;
            padding: 5px;
        }
        
        .bookbtn {
            color: #fff;
            background: #693;
            padding: 7px;
            font-size: 12px;
            border: none;
            margin: 5px;
        }
        
        .hotellist {
            box-shadow: 5px 5px #e6e7e8;
        }
    </style>
    <script async src="https://cdn.ampproject.org/v0.js"></script>
    <script async custom-element="amp-install-serviceworker" src="https://cdn.ampproject.org/v0/amp-install-serviceworker-0.1.js"></script>
    <script async custom-element="amp-analytics" src="https://cdn.ampproject.org/v0/amp-analytics-0.1.js"></script>
    <script async custom-element="amp-lightbox-gallery" src="https://cdn.ampproject.org/v0/amp-lightbox-gallery-0.1.js"></script>
    <script async custom-element="amp-carousel" src="https://cdn.ampproject.org/v0/amp-carousel-0.1.js"></script>
    <script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>
    <script async custom-template="amp-mustache" src="https://cdn.ampproject.org/v0/amp-mustache-0.2.js"></script>
    <script async custom-element="amp-list" src="https://cdn.ampproject.org/v0/amp-list-0.1.js"></script>
    <script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>
</head>

<body>
    <div>
        <div class="  " id="wrapper">
            <div class="   ">
                <div id="logosec">
                    <div class="   header  ">
                        <a id="logoid" href="https://www.ulohotels.com" class="   ">
                            <amp-img class="" src="amp/logo/ulo4.png" alt="logo" width="120" height="40">
                            </amp-img>
                        </a>
                        <div class="headtxt">
                            <a href="tel:9543592593" title="+91 95435 92593" class="headercontact"><span >Call @ 9543 592 593</span></a>
                        </div>
                    </div>
                    <div id="cousec">
                        <div class=" h1tag  ">
                            <div>
                            
                            </div>
                        </div>
                        <div class=" h1tag  ">
                            <div class=" locname">OOTY HOTELS </div>
                        </div>
                        <div class="hotellist">
                            <a class="link" href="ulo-armster-palace">
                                <div class="    ">
                                    <amp-carousel width="auto" height="150px" layout="fixed-height" controls="" class=" carimg" type="carousel">
                                        <div class="   "></div>
                                        <amp-img class=" rollscale" src="amp/ootyarm/1.png" alt="ootyarm" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ootyarm/2.png" alt="ootyarm" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ootyarm/3.png" alt="ootyarm" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ootyarm/4.png" alt="ootyarm" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ootyarm/5.png" alt="ootyarm" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ootyarm/6.png" alt="ootyarm" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ootyarm/7.png" alt="ootyarm" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ootyarm/8.png" alt="ootyarm" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ootyarm/9.png" alt="ootyarm" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ootyarm/10.png" alt="ootyarm" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ootyarm/11.png" alt="ootyarm" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ootyarm/12.png" alt="ootyarm" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ootyarm/13.png" alt="ootyarm" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ootyarm/14.png" alt="ootyarm" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ootyarm/15.png" alt="ootyarm" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ootyarm/16.png" alt="ootyarm" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ootyarm/17.png" alt="ootyarm" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ootyarm/18.png" alt="ootyarm" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ootyarm/19.png" alt="ootyarm" width="200" height="150">
                                        </amp-img>
                                    </amp-carousel>
                                </div>
                                <div class=" hdpad  ">
                                    <div class=" hoteldetail  ">
                                        <div class=" ">
                                            <meta itemProp="telephone" content="+91-9543592593" />
                                            <meta itemProp="email" content="reservations@ulohotels.com" />
                                            <div class="   hname">ULO ARMSTER PALACE</div>
                                            <div class=" addsec">
                                                <meta itemProp="addressLocality" content="NO.32, AGA ABBAS ALI RD, YELLAPPA CHETTY LAYOUT" />
                                                <meta itemProp="addressRegion" content="bangalore" />
                                                <div class=" addpad">
                                                    <div class=" addcolor">OOTY</div>
                                                </div>
                                            </div>
                                            <div class=" socialreview  ">
                                                <meta itemProp="ratingValue" content="3.5" />
                                                <meta itemProp="ratingCount" content="275" />
                                                <meta itemProp="bestRating" content="5" />
                                                <!-- Hotel-Rating -->
                                                <span class="make_flex">
                                          <svg height="10px" version="1.1" viewBox="0 0 11 10" width="11px" >
                                             <polygon fill="#e57520" fill-rule="evenodd" points="5.33950617 7.65957447 2.13580247 10 3.2037037 6.38297872 0 3.61702128 3.84444444 3.61702128 5.33950617 0 6.62098765 3.61702128 10.6790123 3.61702128 7.47530864 6.38297872 8.54320988 10" stroke="none"></polygon>
                                          </svg>
                                          <svg height="10px" version="1.1" viewBox="0 0 11 10" width="11px" >
                                             <polygon fill="#e57520" fill-rule="evenodd" points="5.33950617 7.65957447 2.13580247 10 3.2037037 6.38297872 0 3.61702128 3.84444444 3.61702128 5.33950617 0 6.62098765 3.61702128 10.6790123 3.61702128 7.47530864 6.38297872 8.54320988 10" stroke="none"></polygon>
                                          </svg>
                                          <svg height="10px" version="1.1" viewBox="0 0 11 10" width="11px" >
                                             <polygon fill="#e57520" fill-rule="evenodd" points="5.33950617 7.65957447 2.13580247 10 3.2037037 6.38297872 0 3.61702128 3.84444444 3.61702128 5.33950617 0 6.62098765 3.61702128 10.6790123 3.61702128 7.47530864 6.38297872 8.54320988 10" stroke="none"></polygon>
                                          </svg>
                                          <svg height="10px" version="1.1" viewBox="0 0 11 10" width="11px" >
                                             <polygon fill="#e57520" fill-rule="evenodd" points="5.33950617 7.65957447 2.13580247 10 3.2037037 6.38297872 0 3.61702128 3.84444444 3.61702128 5.33950617 0 6.62098765 3.61702128 10.6790123 3.61702128 7.47530864 6.38297872 8.54320988 10" stroke="none"></polygon>
                                          </svg>
                                          <svg height="10px" version="1.1" viewBox="0 0 11 10" width="11px" >
                                             <polygon fill="#b7b7b7" fill-rule="evenodd" points="5.33950617 7.65957447 2.13580247 10 3.2037037 6.38297872 0 3.61702128 3.84444444 3.61702128 5.33950617 0 6.62098765 3.61702128 10.6790123 3.61702128 7.47530864 6.38297872 8.54320988 10" stroke="none"></polygon>
                                          </svg>
                                       </span>
                                                <!-- /Hotel-Rating -->
                                                <div class=" reviewpad  ">
                                                    <div class=" ropen">
                                                    </div>
                                                </div>
                                                <div class=" retext"></div>
                                                <div class=" retext"><span>    88 Google Reviews</span></div>
                                            </div>
                                        </div>
                                        <div class=" ">
                                            <div>
                                                <div class=" hprice">
                                                    &#8377; 1800
                                                </div>
                                                <a class="link" href="ulo-armster-palace">
                                                    <button class="bookbtn">SELECT ROOM</button>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <hr class="hline" />
                        </div>
    <!--   second hotels -->     <div class="hotellist">
                            <a class="link" href="ulo-drizzle-drop-inn">
                                <div class="    ">
                                    <amp-carousel width="auto" height="150px" layout="fixed-height" controls="" class=" carimg" type="carousel">
                                        <div class="   "></div>
                                        <amp-img class=" rollscale" src="amp/ootydri/1.png" alt="ootydri" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ootydri/2.png" alt="ootydri" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ootydri/3.png" alt="ootydri" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ootydri/4.png" alt="ootydri" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ootydri/5.png" alt="ootydri" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ootydri/6.png" alt="ootydri" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ootydri/7.png" alt="ootydri" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ootydri/8.png" alt="ootydri" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ootydri/9.png" alt="ootydri" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ootydri/10.png" alt="ootydri" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ootydri/11.png" alt="ootydri" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ootydri/12.png" alt="ootydri" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ootydri/13.png" alt="ootydri" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ootydri/14.png" alt="ootydri" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ootydri/15.png" alt="ootydri" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ootydri/16.png" alt="ootydri" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ootydri/17.png" alt="ootydri" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ootydri/18.png" alt="ootydri" width="200" height="150">
                                        </amp-img>
                                        <amp-img class=" rollscale" src="amp/ootydri/19.png" alt="ootydri" width="200" height="150">
                                        </amp-img>
                                    </amp-carousel>
                                </div>
                                <div class=" hdpad  ">
                                    <div class=" hoteldetail  ">
                                        <div class=" ">
                                            <meta itemProp="telephone" content="+91-9543592593" />
                                            <meta itemProp="email" content="reservations@ulohotels.com" />
                                            <div class="   hname">ULO DRIZZLE DROP INN</div>
                                            <div class=" addsec">
                                                <meta itemProp="addressLocality" content="NO.32, AGA ABBAS ALI RD, YELLAPPA CHETTY LAYOUT" />
                                                <meta itemProp="addressRegion" content="bangalore" />
                                                <div class=" addpad">
                                                    <div class=" addcolor">OOTY</div>
                                                </div>
                                            </div>
                                            <div class=" socialreview  ">
                                                <meta itemProp="ratingValue" content="3.5" />
                                                <meta itemProp="ratingCount" content="275" />
                                                <meta itemProp="bestRating" content="5" />
                                                <!-- Hotel-Rating -->
                                                <span class="make_flex">
                                          <svg height="10px" version="1.1" viewBox="0 0 11 10" width="11px" >
                                             <polygon fill="#e57520" fill-rule="evenodd" points="5.33950617 7.65957447 2.13580247 10 3.2037037 6.38297872 0 3.61702128 3.84444444 3.61702128 5.33950617 0 6.62098765 3.61702128 10.6790123 3.61702128 7.47530864 6.38297872 8.54320988 10" stroke="none"></polygon>
                                          </svg>
                                          <svg height="10px" version="1.1" viewBox="0 0 11 10" width="11px" >
                                             <polygon fill="#e57520" fill-rule="evenodd" points="5.33950617 7.65957447 2.13580247 10 3.2037037 6.38297872 0 3.61702128 3.84444444 3.61702128 5.33950617 0 6.62098765 3.61702128 10.6790123 3.61702128 7.47530864 6.38297872 8.54320988 10" stroke="none"></polygon>
                                          </svg>
                                          <svg height="10px" version="1.1" viewBox="0 0 11 10" width="11px" >
                                             <polygon fill="#e57520" fill-rule="evenodd" points="5.33950617 7.65957447 2.13580247 10 3.2037037 6.38297872 0 3.61702128 3.84444444 3.61702128 5.33950617 0 6.62098765 3.61702128 10.6790123 3.61702128 7.47530864 6.38297872 8.54320988 10" stroke="none"></polygon>
                                          </svg>
                                          <svg height="10px" version="1.1" viewBox="0 0 11 10" width="11px" >
                                             <polygon fill="#e57520" fill-rule="evenodd" points="5.33950617 7.65957447 2.13580247 10 3.2037037 6.38297872 0 3.61702128 3.84444444 3.61702128 5.33950617 0 6.62098765 3.61702128 10.6790123 3.61702128 7.47530864 6.38297872 8.54320988 10" stroke="none"></polygon>
                                          </svg>
                                          <svg height="10px" version="1.1" viewBox="0 0 11 10" width="11px" >
                                             <polygon fill="#b7b7b7" fill-rule="evenodd" points="5.33950617 7.65957447 2.13580247 10 3.2037037 6.38297872 0 3.61702128 3.84444444 3.61702128 5.33950617 0 6.62098765 3.61702128 10.6790123 3.61702128 7.47530864 6.38297872 8.54320988 10" stroke="none"></polygon>
                                          </svg>
                                       </span>
                                                <!-- /Hotel-Rating -->
                                                <div class=" reviewpad  ">
                                                    <div class=" ropen">
                                                    </div>
                                                </div>
                                                <div class=" retext"></div>
                                                <div class=" retext"><span>    88 Google Reviews</span></div>
                                            </div>
                                        </div>
                                        <div class=" ">
                                            <div>
                                                <div class=" hprice">
                                                    &#8377; 2000
                                                </div>
                                                <a class="link" href="ulo-drizzle-drop-inn">
                                                    <button class="bookbtn">SELECT ROOM</button>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <hr class="hline" />
                        </div>
    
                        <div class=" seotag  ">
                         
                    </div>
                    <div class="footer     footstyle  ">
                        <a class="link" href="https://www.ulohotels.com">
                            <div class=" footbox  ">
                                <div class=" foottext">
                                    Ulohotels Private Limited 2018-2019
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <amp-install-serviceworker src="/serviceWorker.js" data-iframe-src="https://www.ulohotels.com/" layout="nodisplay">
    </amp-install-serviceworker>
<amp-analytics config="https://www.googletagmanager.com/amp.json?id=GTM-NXVPRL8&gtm.url=SOURCE_URL" data-credentials="include"></amp-analytics>

</body>

</html>