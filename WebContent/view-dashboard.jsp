<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<div class="content-wrapper dashboard">
   <section class="content ulotel-dashboard">
      <div class="row wel">
         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  ">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 dbnote">
           
               <h1><span id="lblGreetings"></span> <span class="uname">&nbsp;&nbsp;<s:property value="user.userName" /></span></h1>
            </div>
         </div>
      </div>
   </section>
   <!-- icon list begins -->
   <section class="content ">
      <div class="row">
         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 dashicons">
               <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 ">
               
                  <div class="col-lg-6  col-sm-6 col-md-6 col-xs-6 ">
                     <img src="images/dashboard/checkin.png"  />
                  </div>
                  <div class="col-lg-6  col-sm-6 col-md-6 col-xs-6 detmar">  
                     <span class="iconname " ng-repeat="ar in bookingstatus">{{bookingstatus[0].arrival}}</span>
                     <br>
                     <span class="iconname2">Check-In</span>
                  </div>
               </div>
               <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 ">
              
                  <div class="col-lg-6  col-sm-6 col-md-6 col-xs-6 ">
                     <img src="images/dashboard/checkout.png"  />
                  </div>
                  <div class="col-lg-6  col-sm-6 col-md-6 col-xs-6 detmar">  
                     <span class="iconname " ng-repeat="dp in bookingstatus">{{bookingstatus[0].departure}}</span>
                     <br>
                     <span class="iconname2">Check-Out</span>
                  </div>
               </div>
               <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 ">
              
                  <div class="col-lg-6  col-sm-6 col-md-6 col-xs-6 ">
                     <img src="images/dashboard/roomsold.png"  />
                  </div>
                  <div class="col-lg-6  col-sm-6 col-md-6 col-xs-6 detmar">  
                     <span class="iconname ">{{occupancy[0].occupancy}}</span>
                     <br>
                     <span class="iconname2">Room sold</span>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
   <!-- icon list ends -->
   <!-- Booking list-->
   <section class="content dbtodaylist">
      <div class="row">
         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 dbtodaylistbg">
               <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                  <h3>Today's Guest Information</h3>
               </div>
               <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                  <h3>
                     <div class="input-group add-on dsearch">
                        <input type="text" ng-model="search" class="dbsearch form-control" placeholder="Search">
                        <div class="input-group-btn">
                           <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                        </div>
                     </div>
                  </h3>
               </div>
               <div class="table table-responsive">
                  <table class="table  table-bordered">
                     <thead>
                        <tr>
                           <th>Booking Id</th>
                           <th>Guest Name</th>
                           <th>Rooms Booked</th>
                           <th>Amount Paid</th>
                           <th>Hotel Pay</th>
                           <th>Status</th>
                           <th>Edit</th>
                        </tr>
                     </thead>
                     <tbody>
                        <tr ng-repeat="bc in bookdetail | filter:search track by $index ">
                           <td ><span class="dbbkid">{{bc.DT_RowId}}</span></td>
                           <td >{{bc.guestName}}</td>
                           <td> {{bc.rooms}} {{bc.accommodationType}}</td>
                           <td>{{bc.advanceAmount}}</td>
                           <td>{{bc.dueAmount}}</td>
                           <td>
                              <select name="statusId" id="statusId" ng-model="statusId" ng-change="editBookingStatus(bc.DT_RowId,statusId,bc.arrivalDate,bc.departureDate,$index)" value="" ng-required="true">
                                 <option ng-repeat="st in status" value="{{st.statusId}}"  ng-selected ="st.statusId == bc.statusId">{{st.status}}</option>
                              </select>
                           </td>
                           <td><button type="button" class="btn  btn-xs vmore" data-toggle="modal" data-target="#exampleModalLong" ng-click="viewBookedSplitUp(bc.DT_RowId,bc.accommodationId)">View More</button></td>
                           <!-- customer full detail modal-->
                           <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                 <div class="modal-content dbform">
                                    <div class="modal-header">
                                       <h5 class="modal-title" id="exampleModalLongTitle">Guest Information</h5>
                                       <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                       <span aria-hidden="true">&times;</span>
                                       </button>
                                    </div>
                                    <div class="modal-body">
                                       <form>
                                          <div class="col-lg-6 col-sm-6 form-group">
                                             <label for="viewBookingId">Booking Id</label>
                                             <input type="text" class="form-control" id="viewBookingId" name="viewBookingId" placeholder="Booking Id" value="{{bookedDetails[0].bookingId}}" readonly>
                                          </div>
                                          <div class=" col-lg-6 col-sm-6 form-group">
                                             <label for="viewGuestName">Guest Name</label>
                                             <input type="text" class="form-control" id="viewGuestName" name="viewGuestName" placeholder="Guest Name" value="{{bookedDetails[0].guestName}}" readonly>
                                          </div>
                                          <div class="col-lg-6 col-sm-6 form-group">
                                             <label for="viewCheckIn">Check-In</label>
                                             <input type="text" class="form-control" id="viewCheckIn" name="viewCheckIn" placeholder="Check In" value="{{bookedDetails[0].arrivalDate}}" readonly>
                                          </div>
                                          <div class=" col-lg-6  col-sm-6 form-group">
                                             <label for="viewCheckOut">Check-out</label>
                                             <input type="text" class="form-control" id="viewCheckOut" name="viewCheckOut" placeholder="Check Out" value="{{bookedDetails[0].departureDate}}" readonly>
                                          </div>
                                          <div class="col-lg-6 col-sm-6 form-group">
                                             <label for="viewCategory">Category</label>
                                             <input type="text" class="form-control" id="viewCategory" name="viewCategory" placeholder="Category" value="{{bookedDetails[0].accommodationType}}" readonly>
                                          </div>
                                          <div class=" col-lg-6 col-sm-6 form-group">
                                             <label for="viewBookedRooms">Rooms Booked</label>
                                             <input type="text" class="form-control" id="viewBookedRooms" name="viewBookedRooms" placeholder="Booked Rooms" value="{{bookedDetails[0].rooms}}" readonly>
                                          </div>
                                          <div class="col-lg-6 col-sm-6 form-group">
                                             <label for="viewTariff">Tariff</label>
                                             <input type="text" class="form-control" id="viewTariff" name="viewTariff" placeholder="Tariff" value="{{bookedDetails[0].tariffAmount}}" readonly>
                                          </div>
                                          <div class=" col-lg-6  col-sm-6 form-group">
                                             <label for="viewTax">Tax</label>
                                             <input type="text" class="form-control" id="viewTax" name="viewTax" placeholder="Tax" value="{{bookedDetails[0].taxAmount}}" readonly>
                                          </div>
                                          <div class="col-lg-6 col-sm-6 form-group">
                                             <label for="viewTotal">Total</label>
                                             <input type="text" class="form-control" id="viewTotal" name="viewTotal" placeholder="Total" value="{{bookedDetails[0].totalAmount}}" readonly>
                                          </div>
                                          <div class=" col-lg-6  col-sm-6 form-group">
                                             <label for="viewAdvance">Advance</label>
                                             <input type="text" class="form-control" id="viewAdvance" name="viewAdvance" placeholder="Advance" value="{{bookedDetails[0].advanceAmount}}" readonly>
                                          </div>
                                          <div class=" col-lg-6  col-sm-6 form-group">
                                             <label for="viewHotelPay">Hotel Pay</label>
                                             <input type="text" class="form-control" id="viewHotelPay" name="viewHotelPay" placeholder="Hotel Pay" value="{{bookedDetails[0].dueAmount}}" readonly>
                                          </div>
                                          <div class=" col-lg-6  col-sm-6 form-group">
                                             <label for="viewStatusId"> status</label>
                                             <select name="statusId"  class="form-control" id="viewStatusId" ng-model="viewStatusId" value="" ng-required="true" readonly>
                                                <option ng-repeat="st in status" value="{{st.statusId}}"  ng-selected ="st.statusId == bookedDetails[0].statusId">{{st.status}}</option>
                                             </select>
                                          </div>
                                          <div class=" col-lg-6  col-sm-6 form-group">
                                             <label for="viewPaymentRemarks">Payment Remarks</label>
                                             <textarea rows="" cols="" id="viewPaymentRemarks" name="viewPaymentRemarks" class="form-control" readonly>{{bookedDetails[0].paymentRemarks}}	</textarea>
                                          </div>
                                          <div class=" col-lg-6  col-sm-6 form-group">
                                             <label for="viewPaymentRemarks">Special Request</label>
                                             <textarea rows="" cols="" id="vi" name="viewPaymentRemarks" class="form-control" readonly>{{bookedDetails[0].specialRequest}}	</textarea>
                                          </div>
                                       </form>
                                    </div>
                                    <div class="modal-footer">
                                      <!--  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                       <button type="button" class="btn savebtn">Save changes</button> -->
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <!-- customer full detail modal-->
                        </tr>
                     </tbody>
                  </table>
               </div>
               <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 ">
                  <div class="dataTables_info" id="example2_info" role="status" aria-live="polite">Showing {{startPage}} to {{endPage}} of {{bookdetail1.length}} entries</div>
               </div>
               <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                   <div data-pagination="" data-num-pages="numPages()" ng-init="numPages()"
					      data-current-page="currentPage" data-max-size="maxSize"  
					      data-boundary-links="true"></div> 
               </div>
            </div>
         </div>
      </div>
   </section>
   <!-- booking list -->
   <!-- Today availablity list -->
   <section class="content dbtodayavailable">
      <div class="row">
         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 dbtoday  ">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 dbprice ">
                  <h3>Today Price</h3>
                  <table class="table table-responsive table-bordered">
                     <thead>
					    <tr>
					      <th scope="col">Room category</th>
					      <th scope="col">Available</th>
					      <th scope="col">Sold</th>
					      <th scope="col">Price</th>
					    </tr>
					  </thead>
					  <tbody>
					    <tr ng-repeat="av in availablities track by $index">
					      <td>{{av.accommodationType}}</td>
					      <td>{{av.availCount}}</td>
					      <td>{{av.soldCount}}</td>
					      <td>{{av.minimumAmount}}</td>
					    </tr>
		  			</tbody>
                  </table>
               </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 dbrevenue ">
                  <h3>Revenue Projection</h3>
                  <form name="rpSearch">
                     <div class="form-group col-lg-5 col-md-5 col-sm-6 col-xs-12">
                        <div class="input-group date">
                           <input type="text" class="form-control" value="<%= ((session.getAttribute("checkInDate")==null)?"":session.getAttribute("checkInDate")) %>" placeholder="Check In" id="checkIn" name="checkIn"  ng-required="true"> 
                           <label class="input-group-addon btn" for="checkIn">
                           <span class="fa fa-calendar"></span>
                           </label>  
                        </div>
                     </div>
                     <div class="form-group col-lg-5 col-md-5 col-sm-6 col-xs-12">
                        <div class="input-group date">
                           <input type="text" class="form-control"  value="<%= ((session.getAttribute("checkOutDate")==null)?"":session.getAttribute("checkOutDate")) %>" placeholder="Check Out" id="checkOut" name="checkOut" ng-required="true" >
                           <label class="input-group-addon btn" for="checkOut">
                           <span class="fa fa-calendar"></span>
                           </label> 
                        </div>
                     </div>
                     <div class="form-group col-lg-2 col-md-2 col-sm-6 col-xs-12">
                        <button class="btn btn-search" type="button" ng-click="getPropertyRevenue()"><i class="fa fa-search fa-fw"></i></button> 
                     </div>
                  </form>
                  <table class="table table-responsive ">
                     <thead>
                        <tr>
                           <th scope="col">No of Nights</th>
                           <th scope="col">Revenue</th>
                        </tr>
                     </thead>
                     <tbody>
                        <tr>
                           <td>{{revenue[0].totalRooms}}</td>
                           <td>{{revenue[0].totalAmount}}</td>
                        </tr>
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </section>
   <!-- Today availablity list  -->
   <div class="input-group date">
		<input type="hidden" class="form-control"  value="<%= ((session.getAttribute("checkInDate")==null)?"":session.getAttribute("checkInDate")) %>" id="StartDate" name="StartDate" >
		<input type="hidden" class="form-control"  value="<%= ((session.getAttribute("checkOutDate")==null)?"":session.getAttribute("checkOutDate")) %>" id="EndDate" name="EndDate" >
	</div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script type="text/javascript"  src="contents/js/ngprogress.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>
<!--  <link data-require="bootstrap-css@2.3.2" data-semver="2.3.2" rel="stylesheet" href="https://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" /> -->
  <script data-require="angular.js@1.1.5" data-semver="1.1.5" src="https://code.angularjs.org/1.1.5/angular.min.js"></script>
 <script data-require="angular-ui-bootstrap@0.3.0" data-semver="0.3.0" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.3.0.min.js"></script>	
<script>
   var app = angular.module('myApp', ['ngProgress','ui.bootstrap']);
   app.controller('customersCtrl', function($scope, $http,$timeout,ngProgressFactory) {
		$scope.progressbar = ngProgressFactory.createInstance();
	    $scope.progressbar.start();
   
   	$scope.date = new Date();
   	
         	$timeout(function(){
        	    // $scope.progressbar.complete();
              $scope.show = true;
              $("#pre-loader").css("display","none");
          }, 2000);
   
          $("#checkIn").datepicker({
              dateFormat: 'MM d, yy',
            
              minDate:  0,
              onSelect: function (formattedDate) {
                  var date1 = $('#checkIn').datepicker('getDate'); 
                  var date = new Date( Date.parse( date1 ) ); 
                  date.setDate( date.getDate() + 1 );        
                  var newDate = date.toDateString(); 
                  newDate = new Date( Date.parse( newDate ) );   
                  $('#checkOut').datepicker("option","minDate",newDate);
                  $timeout(function(){
                    //scope.checkIn = formattedDate;
                  });
              }
          });
   
          $("#checkOut").datepicker({
              dateFormat: 'MM d, yy',
            
              minDate:  0,
             
              onSelect: function (formattedDate) {
                  var date2 = $('#checkOut').datepicker('getDate'); 
                  $timeout(function(){
                    //scope.checkOut = formattedDate;
                  });
              }
          });
   	
          $("#StartDate").datepicker({
              dateFormat: 'MM d, yy',
            
              minDate:  0,
              onSelect: function (formattedDate) {
                  var date1 = $('#StartDate').datepicker('getDate'); 
                  var date = new Date( Date.parse( date1 ) ); 
                  date.setDate( date.getDate() + 1 );        
                  var newDate = date.toDateString(); 
                  newDate = new Date( Date.parse( newDate ) );   
                  $('#EndDate').datepicker("option","minDate",newDate);
                  $timeout(function(){
                    //scope.checkIn = formattedDate;
                  });
              }
          });
   
          $("#EndDate").datepicker({
              dateFormat: 'MM d, yy',
            
              minDate:  0,
             
              onSelect: function (formattedDate) {
                  var date2 = $('#EndDate').datepicker('getDate'); 
                  $timeout(function(){
                    //scope.checkOut = formattedDate;
                  });
              }
          });
          
   	$scope.unread = function() {
   
   	var notifiurl = "unreadnotifications.action";
   	$http.get(notifiurl).success(function(response) {
   		$scope.latestnoti = response.data;
   	});
   	};
   
             
             $scope.getArrival = function() {
     			
     			var url = 'get-arrival';
     			$http.get(url).success(function(response) {
     			   //console.log(response);
     				$scope.arrival= response.data;
   
     			});
     		};    
     		
     		 $scope.getDeparture = function() {
     				
     				var url = 'get-departure';
     				$http.get(url).success(function(response) {
     				   //console.log(response);
     					$scope.departure= response.data;
   
     				});
     			};
     			
             $scope.getStatus = function() {
     				
     				var url = 'get-status';
     				$http.get(url).success(function(response) {
     				   //console.log(response);
     					$scope.status= response.data;
   
     				});
     			};
     			
   	   $scope.editBookingStatus = function(bookingId,statusId,arrival,departure,indx) {
   
   		   
   		   var gdata = "bookingId=" + bookingId
   			+ "&statusId=" + statusId
   
   		   $http(
   					{
   						method : 'POST',
   						data : gdata,
   						headers : {
   							'Content-Type' : 'application/x-www-form-urlencoded'
   						},
   						url : 'edit-booking-status'
   					}).then(function successCallback(response) {
   						
   						 var gdata = "bookingId=" + bookingId
   							+ "&statusId=" + statusId
   					
   						
   						$http(
   							    {
   								method : 'POST',
   								data : gdata,										
   								dataType: 'json',
   								headers : {
   								
   									'Content-Type' : 'application/x-www-form-urlencoded'
   								},
   								url : 'edit-booking-details-status'										
   							   }).success(function(response) {
   								var statusFlag=true; 
 								 var rdata = "statusId=" + statusId+"&statusFlag="+statusFlag+"&bookingId=" + bookingId;
 	   							
 	   					
 	   						
	   	   						$http(
	   	   							    {
	   	   								method : 'POST',
	   	   								data : rdata,										
	   	   								dataType: 'json',
	   	   								headers : {
	   	   								
	   	   									'Content-Type' : 'application/x-www-form-urlencoded'
	   	   								},
	   	   								url : 'status-booking-details'										
	   	   							   }).success(function(response) {
	   	   									$scope.bookingstatus= response.data;
	   	   						
	   	   								});
   						
   								}); 
   					
   					});
     				
     			};
     			
     			$scope.getTodayOccupancy = function() {
     				
     				var url = 'get-todayoccupancy';
     				$http.get(url).success(function(response) {
     				   //console.log(response);
     					$scope.occupancy= response.data;
   
     				});
     			};   
     			
     	$scope.items = [];		
     			
   		$scope.getAllBookings = function() {
   			
   			var url = "get-allbookings";
   			$http.get(url).success(function(response) {
   			    //console.log(response);
   				$scope.bookdetail= response.data;
   				for(var i=0;i<$scope.bookdetail.length;i++){
   					$scope.items.push($scope.bookdetail[i])
   				}	
   			
   		
   		$scope.filteredTodos = []
	   	  	,$scope.currentPage = 1
	   	  	,$scope.numPerPage = 3 
	   	  	,$scope.maxSize = 5;
	   		//$scope.numPages();
	   	 $scope.numPages = function () {
	   	    return Math.ceil($scope.items.length / $scope.numPerPage);
	   	 };
	   	 
	   		$scope.$watch('currentPage + numPerPage', function() {
	   	   	    var begin = (($scope.currentPage - 1) * $scope.numPerPage);
	   	   	    var end = begin + $scope.numPerPage;
	   	   	    $scope.filteredTodos = $scope.items.slice(begin,end);
	   	   	    $scope.bookdetail = $scope.filteredTodos;
	   			$scope.startPage = begin+1;
				$scope.endPage = end;
			 	if($scope.items.length < end ){   			 
	   			$scope.endPage = $scope.items.length;
	   	 			  }
	   	   	  	});			

		});
	};
   		
   			$scope.getPropertyAccommodation = function() {
   			
   			var url = "get-propertyaccommodation";
   			$http.get(url).success(function(response) {
   			    //console.log(response);
   				$scope.accdetail= response.data;
   				
   	
   			});
   		};  
   		
   		$scope.getBookingStatus = function() {
				
				var statusFlag=false; 
				
				var rdata="statusFlag="+statusFlag;
				
				$http(
					    {
						method : 'POST',
						data : rdata,										
						dataType: 'json',
						headers : {
						
							'Content-Type' : 'application/x-www-form-urlencoded'
						},
						url : 'status-booking-details'										
					   }).success(function(response) {
							$scope.bookingstatus= response.data;
						});

				
			};  
			
         $scope.viewBookedSplitUp = function(bookingId,accommId) {
        	 
        	 var ndata="bookingId="+bookingId+"&accommodationId="+accommId;
				$http(
					    {
						method : 'POST',
						data : ndata,										
						dataType: 'json',
						headers : {
							'Content-Type' : 'application/x-www-form-urlencoded'
// 							'Content-Type' : 'application/json; charset=utf-8'
						},
						url : 'view-booked-details'										
					   }).success(function(response) {
						   $scope.bookedDetails= response.data;
						});
         };
         
         function formatDate(date) {
        	  var monthNames = [
        	    "January", "February", "March",
        	    "April", "May", "June", "July",
        	    "August", "September", "October",
        	    "November", "December"
        	  ];

        	  var day = date.getDate();
        	  var monthIndex = date.getMonth();
        	  var year = date.getFullYear();

        	  return monthNames[monthIndex] +' '+day +', ' + year;
        	}
         
         
         var spinner = $('#loadertwo');
   			$scope.getPropertyRevenue = function() {
   				spinner.show();
   				var startDate=document.getElementById('checkIn').value;
              	var endDate=document.getElementById('checkOut').value;
              	var dateStart=new Date(startDate);
              	var dateEnd=new Date(endDate);
              	 
              	var formatStart=formatDate(dateStart);
              	 
              	var formatEnd=formatDate(dateEnd);
   				var url = "get-propertyrevenue?strStartDate="+formatStart+"&strEndDate="+formatEnd;
   				$http.get(url).success(function(response) {
   					
   				$scope.revenue= response.data;
   			 setTimeout(function(){ spinner.hide(); 
   		  }, 1000);
   					
   						});
   				//return total;
   			};
   			
   			$scope.getAvailability = function() {
           	 var startDate=document.getElementById('StartDate').value;
           	 var endDate=document.getElementById('EndDate').value;
           	 var dateStart=new Date(startDate);
           	 var dateEnd=new Date(endDate);
           	 
           	 var formatStart=formatDate(dateStart);
           	 
           	 var formatEnd=formatDate(dateEnd);

           	 var fdata="strStartDate="+formatStart+"&strEndDate="+formatEnd+"&sourceTypeId="+1;
           	 
           	
   				$http(
   					    {
   						method : 'POST',
   						data : fdata,										
   						dataType: 'json',
   						headers : {
   							'Content-Type' : 'application/x-www-form-urlencoded'
   						},
   						url : 'get-new-offline-room-availability'										
   					   }).success(function(response) {
   						   $scope.availablities = response.data;
   						$scope.progressbar.complete();   
   						});
	   				
			};
			

			$scope.getAvailability();       
             
			$scope.getBookingStatus();
			
			
             
            // $scope.getBookings();
   	
   			$scope.getArrival();
             
             $scope.getDeparture();
             
             $scope.getTodayOccupancy();
             
             $scope.getAllBookings();
             
   			$scope.getPropertyAccommodation();
             
             $scope.getPropertyRevenue();
             
             $scope.getStatus();
             
   	//$scope.unread();
   	//
   	
   	
   });    
   
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.12.0/moment.js"></script>
<script>
   var enumerateDaysBetweenDates = function(startDate, endDate) {
   var now = startDate,
   dates = [];
   
   while (now.isBefore(endDate) || now.isSame(endDate)) {
         dates.push(now.format("YYYY-MM-DD"));
   
         now.add('days',1 );
     }
   return dates;
   };
   
   var fromDate = moment();
   
   var toDate   = moment().add('days',30);
   
   
   results = enumerateDaysBetweenDates(fromDate, toDate); 
   
   var enumerateDaysBetweenDates = function(startDate, endDate) {
   var now = startDate,
   dates = [];
   
   
   while (now.isBefore(endDate) || now.isSame(endDate)) {
        dates.push(now.format("MMM D"));
   
   
        now.add('days',1);
    }
   return dates;
   };
   
   var fromDate = moment();
   
   var toDate   = moment().add('days', 30);
   
   result = enumerateDaysBetweenDates(fromDate, toDate); 
   
   
</script>   
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="css/jquery-ui.theme.css">
      <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="js/widgets.js"></script>
<script src="partner/assets/js/shared/chart.js"></script>
<script>
   $( function() {
       /* $("#bookCheckin" ).datepicker({minDate:0});
       $("#bookCheckout" ).datepicker({minDate:0}); */
       $("#checkIn" ).datepicker({minDate:0});
       $("#checkOut" ).datepicker({minDate:1,maxDate:'+30D'});
     });
</script>