<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="s" uri="/struts-tags"%>

   <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <section class="content-header">
          <h1>
            Property Profile
            <!--<small>Subscription Expiry on {{property[0].subscription[0].expirydate}}</small>-->
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Property</a></li>
            <li class="active">Amenities</li>
          </ol>
        </section>
		
        <!-- Main content -->
        <section class="content">
        
        	
        
        	<div class="row">
            	<div class="col-xs-12" ng-if="amenities.length>0">
						<div class="box">
							<div class="box-header">
								<h3 class="box-title">Add Amenities</h3>
							</div>
							
							
							<!-- /.box-header -->
							<div class="box-body table-responsive no-padding" > 
								<table class="table table-hover" id="example1">
									<tr>
										<th>Edit / Delete</th>
										<!--  <th>Amenity Id</th>-->
										<th>Amenity Name</th>
									</tr>
									<tr ng-repeat="a in amenities">
										<td>
											<a href="#editAmenity" ng-click="getAmenity(a.DT_RowId)"  data-toggle="modal"><i  class="fa fa-pencil"></i></a>
											<a href="#deleteAmenity" ng-click="deleteAmenity(a.DT_RowId)"  data-toggle="modal"><i  class="fa fa-fw fa-trash"></i></a>
										</td>
										<!--<td>{{a.DT_RowId}}</td>-->
										<td>{{a.amenityName}}</td>
									</tr>
									<tr>
									</tr>
								</table>
							</div>
							<!-- /.box-body -->
							
							
							<div class="box-footer clearfix">
								 <ul class="pagination pagination-sm no-margin pull-right">
									<a class="btn" href="#addAmenity" ng-click="enableTask(x.DT_RowId)"  data-toggle="modal" >Add Amenity</a>
								</ul> 
							</div>
						</div>
						<!-- /.box -->
					</div>	
         	</div>
          
          
        </section><!-- /.content -->
      
      	<div class="modal" id="addAmenity">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" id="btnclose" class="close" data-dismiss="modal"
							aria-hidden="true">x</button>
						<h4 class="modal-title">Add Amenity</h4>
					</div>
					 <form name="addAmenity">
					<div class="modal-body" >
					<div id="message"></div>
						<div class="form-group">
							<label>Amenity Name</label>
							<input type="text" class="form-control"  name="amenityName"  id="amenityName"  placeholder="Name" ng-model='amenityName' ng-pattern="/^[a-zA-Z0-9]+$/" ng-required="true">
						    <span ng-show="addAmenity.categoryName.$error.pattern">Not a valid Name!</span>
						     
						</div>
						<div class="form-group">
							<label>Amenity Description</label>
							<input type="text" class="form-control"  name="amenityDescription"  id="amenityDescription">
						   
						</div>
						
					</div>
					<div class="modal-footer">
	
						<a href="#" data-dismiss="modal" class="btn">Close</a>
						<button ng-click="addAmenity.$valid && addSingleAmenity()" ng-disabled="addAmenity.$invalid" class="btn btn-primary">Save</button>
					</div>
					</form>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dalog -->
		</div>
       <div class="modal" id="editAmenity">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" id="editbtnclose" class="close" data-dismiss="modal"
							aria-hidden="true">x</button>
						<h4 class="modal-title">Edit Category</h4>
					</div>
					<div class="modal-body" ng-repeat="am in amenity">
					<div id="editmessage" ></div>
					    <input type="hidden" name ="editAmenityId" id ="editAmenityId" value="{{am.DT_RowId}}">
						<div class="form-group">
							<label>Amenity Name</label>
							<input type="text" class="form-control" name="editAmenityName"  value="{{am.amenityName}}" id="editAmenityName">
						</div>
						<div class="form-group">
							<label>Amenity Description</label>
							<input type="text" class="form-control" name="editAmenityDescription"  value="{{am.amenityDescription}}" id="editAmenityDescription">
						</div>
						
					</div>
					<div class="modal-footer">
	                    
						<a href="#" data-dismiss="modal" class="btn">Close</a>
						<button ng-click="editSingleAmenity()" class="btn btn-primary">Save</button>
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dalog -->
		</div>
		
		<div class="modal" id="deleteAmenity">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" id="deletebtnclose" class="close" data-dismiss="modal"
							aria-hidden="true">x</button>
						<h4 class="modal-title">Delete Message</h4>
					</div>
					<div class="modal-body">
					 <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-check"></i>Your Item Category has been deleted Sucessfully </h4>
                  
                 </div>
					    
						
					</div>
					<div class="modal-footer">
	                    
						<a href="#" data-dismiss="modal" class="btn">Close</a>
						
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dalog -->
		</div>

      </div><!-- /.content-wrapper -->
     


<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>

	<script>

		var app = angular.module('myApp', ['ngProgress']);
		app.controller('customersCtrl', function($scope, $http,$timeout,ngProgressFactory) {

			$("#alert-message").hide();
			//$scope.progressbar = ngProgressFactory.createInstance();
			//$scope.progressbar.setColor("green");
			//$scope.progressbar.start();
	       	$timeout(function(){
	      	    // $scope.progressbar.complete();
	            $scope.show = true;
	            $("#pre-loader").css("display","none");
	        }, 2000);

			 
			
			$scope.unread = function() {
			//var notifiurl = "http://localhost:8085/collaborative-workflow/unreadnotifications.action";
			var notifiurl = "unreadnotifications.action";
			$http.get(notifiurl).success(function(response) {
				$scope.latestnoti = response.data;
			});
			};
			
			
            $scope.addSingleAmenity = function(){
				
			    var fdata = "amenityName=" + $('#amenityName').val()
				+ "&amenityDescription=" + $('#amenityDescription').val()
				+ "&propertyId=1";
				
				
				$http(
						{
							method : 'POST',
							data : fdata,
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							url : 'add-amenity'
						}).then(function successCallback(response) {
							
							$scope.getAmenities();
							var alert = ' <div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Alert!</h4>Success Item Added Succesfully.</div>';
				            $('#message').html(alert);
				            
							$timeout(function(){
					      	$('#btnclose').click();
					        }, 2000);
							
							
				}, function errorCallback(response) {
					
				  
					// called asynchronously if an error occurs
					// or server returns response with an error status.
				});

			};
			
			
			
			
               $scope.editSingleAmenity = function(){
				
				
            	   var fdata = "amenityName=" + $('#editAmenityName').val()
   				+ "&amenityDescription=" + $('#editAmenityDescription').val()
   				+ "&amenityId=" + $('#editAmenityId').val()
   				+ "&propertyId=1";
				
				//alert(fdata);
				$http(
						{
							method : 'POST',
							data : fdata,
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							url : 'edit-amenity'
						}).then(function successCallback(response) {
							
							$scope.getAmenities();
							var alert = ' <div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Alert!</h4>Success Item Updated Succesfully.</div>';
				            $('#editmessage').html(alert);
							$timeout(function(){
					      	$('#editbtnclose').click();
					        }, 2000);
				}, function errorCallback(response) {
					
					// called asynchronously if an error occurs
					// or server returns response with an error status.
				});

			};
			
            
            
			
			
            
			$scope.getAmenity = function(rowid) {
				
			
				
				var url = "get-amenity?amenityId="+rowid;
				$http.get(url).success(function(response) {
				    //console.log(response);
				   
					$scope.amenity = response.data;
		
				});
				
			};
			
            $scope.deleteAmenity = function(rowid) {
				
            	var url = "delete-amenity?amenityId="+rowid;
				$http.get(url).success(function(response) {
				    //console.log(response);
				    $scope.getAmenities();
				    $('#deletemodal').modal('toggle');
				    $timeout(function(){
				      	$('#deletebtnclose').click();
				    }, 2000);
					
		
				});
				
			};
			
			$scope.getAmenities = function() {

				var url = "get-amenities?propertyId=1";
				$http.get(url).success(function(response) {
					//console.log(response);
					$scope.amenities = response.data;
			
				});
			};
			
			
			 $scope.getPropertyList = function() {
				 
		        	
		        	
			        var userId = $('#adminId').val();
		 			var url = "get-user-properties?userId="+userId;
		 			$http.get(url).success(function(response) {
		 			    
		 				$scope.props = response.data;
		 	
		 			});
		 		};
		 		
           $scope.change = function() {
     	   
		        //alert($scope.id);
		        
		        var propertyId = $scope.id;	
     	    var url = "change-user-property?propertyId="+propertyId;
	 			$http.get(url).success(function(response) {
	 				
	 				 window.location = '/ulopms/dashboard'; 
	 				//$scope.change = response.data;
	 	
	 			});
			       
		 		};
		 		
				
		    $scope.getPropertyList();
			
			$scope.getAmenities();
			//$scope.unread();
			
			
			
		});

	
		
	</script>
	
	 <script src="js/jquery-2.1.1.js" type="text/javascript"></script>
	  <!-- iCheck 1.0.1 -->
    <script src="plugins/iCheck/icheck.min.js"></script>
       