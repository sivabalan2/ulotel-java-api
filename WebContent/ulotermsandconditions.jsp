<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
   pageEncoding="ISO-8859-1"%>
<section id="innerPageContent">
   <!-- Tab Header -->

   <!-- Tab Header -->
   <div class="container tabBG">
      <div class="row">
         <div class="col-sm-12">
            <!-- Overview Tab -->
            <div class="tabContent" id="overviewTab" style="display:block;">
               <h1 class="heading1">TERMS AND CONDITIONS</h1>
               <div class="textContent">
                  <h2 class="heading1">INTRODUCTION:</h2>
                  <br>
                  <p>www.ulohotels.com (hereinafter, the “Website”, “Site”) is owned and operated by ULO HOTELS PRIVATE LIMITED., a private limited company incorporated under the companies act, 2017, having its registered at. 56,B2,Oyster Apartment, 4th Avenue,19th Street, Ashok Nagar,Chennai, Tamil Nadu 600083</p>
                  <p>This document is an electronic record published in accordance with the provisions of the Information Technology (Intermediaries guidelines) Rules, 2011 that require publishing the Rules and Regulations, Privacy Policy and Terms of Service for access or  use of the website and being generated by a computer system, it does not require any physical or digital signatures.</p>
                  <p>For the purpose of these Terms of Use, along with any amendments to the same, and wherever the context so requires “You”, “Your,” “Yourself” or “User” shall mean and refer to the person visiting, accessing, browsing through and/or using the Website at any point in time and shall include the “Clients/Customers”. However, where a Client/Customer is mentioned specifically, it shall only mean and refer to that class of User.</p>
                  <p>The term “We”, “Us”, “Our” “COMPANY”, shall mean and refer to the website and/or The ULO HOTELS PRIVATE LIMITED depending on the context.</p>
                  <p>This Agreement shall cover your usage of the Website as a User, which shall include Customers and Service Providers. Further the Service Providers are also bound by the Service Level Agreements, signed with the ULO HOTELS PRIVATE LIMITED, besides this Agreement.</p>
                  <h2>TRADE MARK LICENSE AND ACCESS</h2>
                  <br>
                  <p>The company grants you no sub-license, whether it is limited or not, to access and make personal use of the Site, and not to download (other than page caching) or modify it or any portion of it, except with express written consent of the company. You are not permitted any resale or commercial use of the Site or its contents; collection and use of any product listings, descriptions, or prices; any derivative use of the Site or its contents; any downloading or copying of information for the benefit of another merchant; or use of data mining, robots, or similar data gathering and extraction tools. Any portion of the Site may not be reproduced, duplicated, copied, sold, resold, or otherwise exploited for any commercial purpose without express written consent of the COMPANY. You may not frame or utilize framing techniques to enclose any trademark, logo, or other proprietary information (including images, text, page layout, or form) of the Website or of the COMPANY and/or its affiliates without the express written consent of the COMPANY. You may not use any Meta tags or any other “hidden text” utilizing the COMPANY’s name or trademarks without the express written consent of the COMPANY. You shall not attempt to gain unauthorized access to any portion or feature of the Site, or any other systems or networks connected to the Site or to any server, computer, network, or to any of the services offered on or through the Site, by hacking, ‘password mining’ or any other illegitimate means.</p>
                  <p>You hereby agree and undertake not to host, display, upload, modify, publish, transmit, update or share any information which:</p>
                  <p>A.Belongs to another person and to which you do not have any right;</p>
                  <p>B.Is grossly harmful, harassing, blasphemous, defamatory, obscene, pornographic, pedophilic, libelous, invasive of another’s privacy, hateful, or racially, ethnically objectionable, disparaging, relating or encouraging money laundering or gambling, or otherwise unlawful in any manner whatever;</p>
                  <p>C.Harm minors in any way;</p>
                  <p>D.Infringes any patent, trademark, copyright or other proprietary/intellectual property rights; </p>
                  <p>E.Violates any law for the time being in force;</p>
                  <p>F.Deceives or misleads the addressee about the origin of such messages, communicates any information which is grossly offensive or menacing in nature</p>
                  <p>G.Impersonates another person;</p>
                  <p>H.Contains software viruses or any other computer code, files or programs designed to interrupt, destroy or limit the functionality of any computer resource;</p>
                  <p>I.	Threatens the unity, integrity, defense, security or sovereignty of India, friendly relations with foreign states, or public order or causes incitement to the commission of any cognizable offence or prevents investigation of any offence or is insulting any other nation; or is misleading or known to be false in any way.</p>
                  <h2>HOTEL RESERVATIONS</h2>
                  <br>      
                  <p>The reservations feature of the site is provided solely to assist customers in determining the availability of Hotel rooms related services and products and to make legal reservations and for no other purposes. </p>
                  <p>The company hereby emphasizes you that you should be at least 18 years of age, possess the legal authority to enter into the legal agreement constituted by your acceptance of those conditions and to use the website in accordance with such conditions of the company. </p>
                  <p>You are hereby agreeing to be financially responsible for your use of the website, including all reservations made by you or made on your behalf whether it is authorized by you or not. For any reservations or other services for which fees may be charged, you agree to abide by the terms or conditions of supply, including payment of all money due under such terms or conditions. The site contains details of hotel charges and room rates (including available special offers) for hotels and resorts owned or managed by ULO HOTELS. </p>
                  <p>Hotel reservation terms and conditions of booking are displayed on the site and payment will be in accordance with the procedure displayed in such terms and conditions. </p>
                  <p>No contract will subsist between you and ULO HOTELS in respect of any services or products offered through the site unless or until ULO HOTELS accept your booking by e-mail or automated confirmation through the site confirming that it has accepted your reservation, booking or order and any such contract shall be deemed to incorporate the hotel reservation terms and conditions of booking. Any other terms and conditions relating to particular services or products are displayed on the company site. </p>
                  <p>You undertake that all details provided by you in connection with any services or products which may be offered by ULO HOTELS on the site (including hotel room reservations) will be correct and, where applicable, the credit card which you use is your own and that there are sufficient funds to cover the cost of any services or products which you wish to purchase. THE ULO HOTELS has reserved the right to obtain validation of your credit card details before providing you any services or products.</p>
                  <H2>COMMUNICATIONS</H2>
                  <br>
                  <p>By using this Website, it is deemed that you have consented to receive calls, auto dialed and/or pre-recorded message calls, e-mails, from Us at any time with the use of the telephone number and e-mail address that have been provided by you for the use of this website which are subject to the Privacy Policy. The user agrees to receive promotional communication and newsletters from the COMPANY. </p>
                  <p>This includes contacting you through the information received through other parties. The use of this website is also your consent to receive SMS from  us at any time we deem fit. This consent to be contacted is for purposes that include and are not limited to clarification calls and marketing and promotional calls. The user can opt out of such communication and/or newsletters either by unsubscribing on the Website itself, or by contacting the customer services team and placing a request for unsubscribing by sending an email to reservations@ulohotels.com, also be contacted by Service Providers with whom we have entered into a contract in furtherance of our rights, duties and obligations under this document and all other policies followed by Us. Such contact will be made only in pursuance of such objectives, and no other calls will be made. </p>
                  <p>The sharing of the information provided by you will be governed by the Privacy Policy and we may or may not sell your information to third parties. </p>
                  <H2>SECURITY</H2>
                  <br>
                  <p>Transactions on the Web site are secure and protected. Any information entered by the User while transacting on the Website is encrypted to protect the User against unintentional disclosure to third parties. The User’s credit and debit card information are not received, stored by or retained by the company Web site in any manner. This information is supplied by the User directly to the relevant payment gateway which is authorized to handle the information provided, and is compliant with the regulations and requirements of various banks and institutions and payment franchisees that it is associated with. </p>
                  <H2>TERMINATION</H2>
                  <br>
                  <p>If you breach any of the aforesaid conditions the COMPANY may terminate this user license/agreement at any time and may do so immediately without notice, and accordingly deny you access to the site. </p>
                  <p>Such termination will be without any liability to the COMPANY. The COMPANY’s right to any Comments, and to be indemnified pursuant to the terms hereof, shall survive any termination of this User Agreement. Any such termination of the User Agreement shall not cancel your obligation to pay for product (s)/service (s) already ordered from the Site or affect any liability that your license to use the site will terminate immediately without the necessity of any notice being given to you. </p>
                  <H2>INDEMNIFICATION AND LIMITATION OF LIABILITY</H2>
                  <br>
                  <p>You agree to indemnify, defend and hold harmless this website/company, including but not limited to its affiliate vendors, service providers, agents and employees from and against any and all losses, liabilities, claims, damages, demands, costs and expenses (including legal fees and disbursements in connection therewith and interest chargeable thereon) asserted against or incurred by us that arise out of, result from, or may be payable by virtue of, any breach or non-performance of any representation, warranty, covenant or agreement made or obligation to be performed by you pursuant to these terms of service. Further, you agree to hold us harmless against any claims made by any third party due to, or arising out of, or in connection with, your use of the website, any claim that your material caused damage to a third party, your violation of the terms of service, or your violation of any rights of another, including any intellectual property rights. </p>
                  <p>In no event shall we, our officers, directors, employees, partners, suppliers be liable to you, the limitations and exclusions in this section apply to the maximum extent permitted by applicable law. </p>
                  <H2>JURISDICTION</H2>
                  <br>
                  <p>The above conditions are governed by the laws in force in India and you agree to submit to the exclusive jurisdiction of the Courts at Chennai, TamilNadu, India. </p>
                  <H2>CANCELLATION POLICIES</H2>
                  <br>
                  <p>If The Booking Is Cancelled 72 Hrs Prior To The Check-In Date, Booking Amount Will Be Refunded Within 10 To 12 Working Days</p>
                  <p>If The Booking Is Cancelled Within 72 Hrs From The Check-In Date, No Refund Is Applicable.</p>
                  <p>No Refund Is Applicable For Cancellations Done On Long Weekends, Festival And On-Season Times.</p>
                  <H2>CONTACT US</H2>
                  <br>
                  <p>If you have any questions about this agreement, the practices or your experience with the service, you can e-mail us at support@ulohotels.com
                  <p>To return to the site, click where indicated, by doing so, you acknowledge that you have read, understood and accepted these conditions. </p>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script src="js1/ngprogress.js" type="text/javascript"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script> 
<script>
   var app = angular.module('myApp', ['ngProgress']);
   app.controller('customersCtrl', function ($scope, $http, $timeout, ngProgressFactory) {
    $scope.userData = [];
   
    	$scope.login = function () {
   
   
    		var fdata = "username=" + $('#username').val() +
    			"&password=" + $('#password').val();
    		//alert(fdata);
   
    		$http({
    			method: 'POST',
    			data: fdata,
    			headers: {
    				'Content-Type': 'application/x-www-form-urlencoded'
    			},
    			url: 'ulologin'
   
    		}).then(function successCallback(response) {
    			$scope.userData = response.data;
    			console.log($scope.userData);
    			var username = $scope.userData.data[0].userName;
    			var email = $scope.userData.data[0].emailId;
    			var phone = $scope.userData.data[0].phone;
   
    			$('#loginModal').modal('toggle');
    			$('#logSec').html("<div class='mnuContant'><a href='mailto:support@ulohotels.com' title='support@ulohotels.com'><i class='fa fa-envelope'></i><span class='visibleLG'>support@ulohotels.com</span></a><a href='tel:9543592593' title=''+91 95435 92593'><i class='fa fa-phone'></i><span class='visibleLG'>+91 95435 92593</span></a></div><div class='mnuProfile'><div class='mpAvatar hidden-xs' style='background:url(ulowebsite/images/avatar.png);'></div><div class='mpUsername'><a href='javascript:showpro();' class=' mnuProfileOpen'>Hi admin<i class='fa fa-angle-down'></i></a></div><div class='mpSubMenu' id='showmenu' ><ul><li><a href='ulouserprofile'>Profile</a></li><li><a href='ulouserprofile'>Booking History</a></li><li><a href='ulologout'>Log out</a></li></ul></div></div>");
    		}, function errorCallback(response) {
   
    			var alert = ' <div class="alert alert-danger alert-dismissible" style="color:red">Invalid username or password.</div>';
    			$('#loginerror').html(alert);
    		    $(function() {
    	          // setTimeout() function will be fired after page is loaded
    	          // it will wait for 5 sec. and then will fire
    	          // $("#successMessage").hide() function
    	          setTimeout(function() {
    	              $("#loginerror").hide('blind', {}, 100)
    	          }, 5000);
    	      });
    		});
   
    	};
   
    	$scope.signup = function () {
   
   
    		var fdata = "userName=" + $('#userName').val() +
    			"&emailId=" + $('#emailId').val() +
    			"&roleId=" + $('#roleId').val() +
    			"&accessRightsId=" + $('#accessRightsId').val() +
    			"&password=" + $('#signpassword').val();
    		//alert(fdata);
   
    		$http(
   
    			{
    				method: 'POST',
    				data: fdata,
    				headers: {
    					'Content-Type': 'application/x-www-form-urlencoded'
    				},
    				url: 'ulosignup'
    			}).then(function successCallback(response) {
   
   
    			if (response.data == "") {
   
    				var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:red">Sorry This Email Id already exist.</div>';
    				$('#signuperror').html(alertmsg);
   
   
    			} else {
   
    				$scope.userData = response.data;
    				var username = $scope.userData.data[0].userName;
    				var email = $scope.userData.data[0].emailId;
    				var phone = $scope.userData.data[0].phone;
   
   
    				$('#registerModal').modal('toggle');
   
    				$('#logSec').html("<div class='mnuContant'><a href='mailto:support@ulohotels.com' title='support@ulohotels.com'><i class='fa fa-envelope'></i><span class='visibleLG'>support@ulohotels.com</span></a><a href='tel:9543592593' title=''+91 95435 92593'><i class='fa fa-phone'></i><span class='visibleLG'>+91 95435 92593</span></a></div><div class='mnuProfile'><div class='mpAvatar hidden-xs' style='background:url(ulowebsite/images/avatar.png);'></div><div class='mpUsername'><a href='javascript:showpro();' class=' mnuProfileOpen'>Hi admin<i class='fa fa-angle-down'></i></a></div><div class='mpSubMenu' id='showmenu' ><ul><li><a href='ulouserprofile'>Profile</a></li><li><a href='ulouserprofile'>Booking History</a></li><li><a href='ulologout'>Log out</a></li></ul></div></div>");
    			}
   
    		}, function errorCallback(response) {
   
   
    			var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:red">This Email ID Already Exist</div>';
    			$('#signuperror').html(alertmsg);
   
    			// or server returns response with an error status.
    		});
   
    	};
   
   
    	$scope.passwordRequest = function () {
   
   
    		var fdata = "emailId=" + $('#forgetEmailId').val();
   
   
    		$http({
    			method: 'POST',
    			data: fdata,
    			headers: {
    				'Content-Type': 'application/x-www-form-urlencoded'
    			},
    			url: 'ulosendpassword'
    		}).then(function successCallback(response) {
   
    			var alertmsg = ' <div class="alert alert-success alert-dismissible" style="color:red">Password Sent Sucessfully.</div>';
   
    			$('#forgetmessage').html(alertmsg);
   
   
    		}, function errorCallback(response) {
   
   
    			var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:red">Your Email ID does not Registred Yet</div>';
    			$('#forgetmessage').html(alertmsg);
   
   
    		});
   
    	};
   
    	$('#forgetPanel').hide();
    	$scope.showForget = function () {
   
    		$('#loginPanel').hide();
    		$('#forgetPanel').show();
   
   
    	};
   
    	$scope.showLogin = function () {
   
    		$('#forgetPanel').hide();
    		$('#loginPanel').show();
   
   
    	};
   
   $timeout(function () {
     	$('#btnclose').click();
     }, 1000);
     $timeout(function () {
     	$('#btncloses').click();
     }, 1000);
   
   });
   
   
</script>