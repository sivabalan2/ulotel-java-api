<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html>
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>ULO Hotels</title>
      <!-- Tell the browser to be responsive to screen width -->
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
      <!-- Bootstrap 3.3.5 -->
      <link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css">
      <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
      <!-- Font Awesome -->
      <link rel="icon" href="images/logo/favicon.png" type="image/gif" />
      <link rel="stylesheet" href="css/ulotel.css">
      <link rel="stylesheet" href="css/ulotelmq.css">
   </head>
   <body class="utforgotpassword">
   
      
    <div class="containers">
    <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-2 col-xs-12 utlogheader">
    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 utlogo">
    <img src="images/utlogin/ulogo.png" alt="welcome to ulotel">
    </div>
    <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">

    </div>
    </div>
    </div>
    </div> 
    <div class="container">
    <div class="row uloform">
    
    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 loginnote">
    <img alt="ulotel" src="images/utlogin/ulotel.svg" class="ulotelcentral">
    <h1>Maximizing Revenue & value <br> for single branded budget hotel</h1>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 loginform">
  <div class="circle-tile ">
        <a href="#"><div class="circle-tile-heading "><i class="fa fa-user fa-3x"></i></div></a>
        
        <div class="circle-tile-content">
        <p>Change Your Password</p>
       <s:form id="myForm" action="passwordchange.action" method="post" theme="simple" onsubmit ="return validate()">
 <div class=" form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <input type="password" id="newpassword" name="newpassword" class="form-control" required>
        <label class="form-control-placeholder" for="email">New Password</label>
      </div>
      <div class=" form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <input type="password" id="password" name="password" class="form-control" required>
        <label class="form-control-placeholder" for="email">Confirm Password</label>
      </div>
        <div class="row">
   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
  <button type="submit" class="btn btn-primry btnconfirm">Change Password</button>
  </div>
  <input type="hidden" name="link" id="link" value="<%=request.getParameter("link") %>" />
  </div>
        </div>
      </div>
    </s:form>
    </div>
    </div>
    </div>
    <div class="container">
    <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <hr class="loginline"></hr>
    </div>
    </div>
    </div>
       <div class="container">
    <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 loginicon">
    <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12 ">
    </div>
    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6 ">
    <img src="images/utlogin/ui1.svg" class="img-responsive">
    <p>Real-Time Booking</p>
    </div>
    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6">
     
    <img src="images/utlogin/ui2.svg" class="img-responsive">
     <p>Detailed Revenue Forecast</p>
    </div>
    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6">
        <img src="images/utlogin/ui3.svg" class="img-responsive">
         <p>Instant Revenue reconciliation</p>
    </div>
    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6">
    
    <img src="images/utlogin/ui4.svg" class="img-responsive">
     <p>Comprehensive Dashboard & Reporting</p>
    </div>
    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
    
    <img src="images/utlogin/ui5.svg" class="img-responsive">
     <p>Integration With Online/Offline Partners</p>
    </div>
    <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12 ">
    </div>
    </div>
    </div>
    </div>
    <footer>
    <img src="images/utlogin/utfoot.png"  />
    </footer>
    </body>
     <!-- /.login-box -->
      <!-- jQuery 2.1.4 -->
      <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
      <!-- Bootstrap 3.3.5 -->
      <script src="bootstrap/js/bootstrap.min.js"></script>
            <script>
      function passvalidate(){
    	var newpass = document.getElementById("newpassword").value;
    	var confirmpass = document.getElementById("password").value;
    	var passpattern = /(?=.*\d)(?=.*[@#$%])(?=.*[a-z])(?=.*[A-Z]).{8,}/;
    	
    	if(newpass=="") {
	        alert("New password is required");
	        return false;
	      }
    	
    	 if(!passpattern.test(newpass)) {
    	        alert("Password should have atleast 8 characters length,with 1 uppercase,1 special character and 1 number");
    	      
    	        return false;
    	  }
        
    	 if(newpass != confirmpass) {
 	        alert("password doesnot match!");
 	        return false;
 	      }
        
      }
      </script>

</html>