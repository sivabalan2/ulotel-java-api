<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
   pageEncoding="ISO-8859-1"%>
<section id="innerPageContent">
   <!-- Tabs Header -->
   <div class="headingBar">
      <div class="container">
         <div class="row">
            <div class="col-sm-12">
               <ul class="tabMenu nav nav-tabs">
                  <li class="active" >
                     <a id="web" href="javascript:void(0)">
                     <i class="fa fa-globe"></i><br />
                     <span>Web</span>
                     </a>
                  </li>
               </ul>
            </div>
         </div>
      </div>
   </div>
   <!-- //Tabs Header -->
   <div class="container tabBG">
   <div class="row">
      <div class="col-sm-12">
         <!-- Web Release Tab -->
         <div class="tabContent" id="webTab">
            <h1 class="heading1">Web <span>Release</span></h1>
            <div class="webContent">
               <div class="row">
                  <div class="col-sm-3"><img src="ulowebsite/images/media/techstory.png" class="img-responsive" alt="quality budget hotel" /></div>
                  <div class="col-sm-9">
                     <div class="webDetails">
                        <span>We'll Disrupt The Budget Hotel Market With Our 5B Services
                        </span>
                        <p>Viswanathan started his career in 2011 after completing his education in the Presidency College and during his role at Visiit, his primary responsibility was to convince and onboard property owners in our listing platform that is where he felt a gap in hospitality sector..</p>
                        <a href="https://techstory.in/vishwanathan-ulo-hotels/" target="_blank" class="btn btn-default greyBtn pull-right">Read Full Article</a>
                     </div>
                  </div>
               </div>
            </div>
            <div class="webContent">
               <div class="row">
                  <div class="col-sm-3"><img src="ulowebsite/images/media/startup.png" class="img-responsive" alt="ulohotels media content" /></div>
                  <div class="col-sm-9">
                     <div class="webDetails">
                        <span>"Hospitality startup Ulo hotels aims to provide budget-friendly stays at affordable prices throughout India"</span>
                        <p>What is the problem you are trying to solve? Can you share with us any insights that led you to believe that this is a big enough problem? 
                           Not every traveler look for luxury accommodations. Folks mostly look for rooms which fit their budget. But there is a huge vacant for budget hotels in the hospitality sector.
                        </p>
                        <a href="https://startupsuccessstories.in/chennai-based-startup-set-disrupt-hospitality-sector/" class="btn btn-default greyBtn pull-right">Read Full Article</a>
                     </div>
                  </div>
               </div>
            </div>
            <div class="webContent">
               <div class="row">
                  <div class="col-sm-3"><img src="ulowebsite/images/media/storytoday.png" class="img-responsive" alt="budget hotels" /></div>
                  <div class="col-sm-9">
                     <div class="webDetails">
                        <span>"Chennai Based hospitality Startup Ulo Hotels is charming vacationers with budget stays"</span>
                        <p>The Indian Hospitality Sector has shown tremendous growth in recent years. People are saving money to travel around the world. Instead of well-known tourist destinations, folks prefer less visited destinations. One can find that many new startups are created to focus on the needs of the trippers.</p>
                        <a href="http://www.storytoday.co.in/chennai-based-hospitality-startup-ulo-hotels-charming-vacationers-budget-stays/" class="btn btn-default greyBtn pull-right" target="_blank">Read Full Article</a>
                     </div>
                  </div>
               </div>
            </div>
            <div class="webContent">
               <div class="row">
                  <div class="col-sm-3"><img src="ulowebsite/images/media/yourstory.png" style="background-color:grey" class="img-responsive" alt="quality hotels" /></div>
                  <div class="col-sm-9">
                     <div class="webDetails">
                        <span>"சமிப காலமாக பயணத்தின் மீது மக்களுக்கு அதிகம் ஈடுபாடு ஏற்பட்டுள்ளது."</span>
                        <p>
                         சமிப காலமாக பயணத்தின் மீது மக்களுக்கு அதிகம் ஈடுபாடு ஏற்பட்டுள்ளது. ஆண்டு முழுவதும் உழைக்கும் மக்கள் தங்களுக்கு கிடைக்கும் ஓய்வு நேரத்தை வெளியூரில் சென்று கழிக்கின்றனர்.
                        </p>
                        <a href="https://tamil.yourstory.com/read/cee939d69c/chennai-star-up-" class="btn btn-default greyBtn pull-right" target="_blank">Read Full Article</a>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- //Web Release Tab -->
      </div>
   </div>
</section>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script src="js1/ngprogress.js" type="text/javascript"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script> 
<script>
   var app = angular.module('myApp', ['ngProgress']);
   app.controller('customersCtrl', function ($scope, $http, $timeout, ngProgressFactory) {
    $scope.userData = [];
   
    	$scope.login = function () {
   
   
    		var fdata = "username=" + $('#username').val() +
    			"&password=" + $('#password').val();
    		//alert(fdata);
   
    		$http({
    			method: 'POST',
    			data: fdata,
    			headers: {
    				'Content-Type': 'application/x-www-form-urlencoded'
    			},
    			url: 'ulologin'
   
    		}).then(function successCallback(response) {
    			$scope.userData = response.data;
    			console.log($scope.userData);
    			var username = $scope.userData.data[0].userName;
    			var email = $scope.userData.data[0].emailId;
    			var phone = $scope.userData.data[0].phone;
   
    			$('#loginModal').modal('toggle');
    			$('#logSec').html("<div class='mnuContant'><a href='mailto:support@ulohotels.com' title='support@ulohotels.com'><i class='fa fa-envelope'></i><span class='visibleLG'>support@ulohotels.com</span></a><a href='tel:9543592593' title=''+91 95435 92593'><i class='fa fa-phone'></i><span class='visibleLG'>+91 95435 92593</span></a></div><div class='mnuProfile'><div class='mpAvatar hidden-xs' style='background:url(ulowebsite/images/avatar.png);'></div><div class='mpUsername'><a href='javascript:showpro();' class=' mnuProfileOpen'>Hi admin<i class='fa fa-angle-down'></i></a></div><div class='mpSubMenu' id='showmenu' ><ul><li><a href='ulouserprofile'>Profile</a></li><li><a href='ulouserprofile'>Booking History</a></li><li><a href='ulologout'>Log out</a></li></ul></div></div>");
    		}, function errorCallback(response) {
   
    			var alert = ' <div class="alert alert-danger alert-dismissible" style="color:red">Invalid username or password.</div>';
    			$('#loginerror').html(alert);
    		    $(function() {
    	          // setTimeout() function will be fired after page is loaded
    	          // it will wait for 5 sec. and then will fire
    	          // $("#successMessage").hide() function
    	          setTimeout(function() {
    	              $("#loginerror").hide('blind', {}, 100)
    	          }, 5000);
    	      });
    		});
   
    	};
   
    	$scope.signup = function () {
   
   
    		var fdata = "userName=" + $('#userName').val() +
    			"&phone=" + $('#mobilePhone').val() +
    			"&emailId=" + $('#emailId').val() +
    			"&roleId=" + $('#roleId').val() +
    			"&accessRightsId=" + $('#accessRightsId').val() +
    			"&password=" + $('#signpassword').val();
    		//alert(fdata);
   
    		$http(
   
    			{
    				method: 'POST',
    				data: fdata,
    				headers: {
    					'Content-Type': 'application/x-www-form-urlencoded'
    				},
    				url: 'ulosignup'
    			}).then(function successCallback(response) {
   
   
    			if (response.data == "") {
   
    				var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:red">Sorry This Email Id already exist.</div>';
    				$('#signuperror').html(alertmsg);
   
   
    			} else {
   
    				$scope.userData = response.data;
    				var username = $scope.userData.data[0].userName;
    				var email = $scope.userData.data[0].emailId;
    				var phone = $scope.userData.data[0].phone;
   
   
    				$('#registerModal').modal('toggle');
   
    				$('#logSec').html("<div class='mnuContant'><a href='mailto:support@ulohotels.com' title='support@ulohotels.com'><i class='fa fa-envelope'></i><span class='visibleLG'>support@ulohotels.com</span></a><a href='tel:9543592593' title=''+91 95435 92593'><i class='fa fa-phone'></i><span class='visibleLG'>+91 95435 92593</span></a></div><div class='mnuProfile'><div class='mpAvatar hidden-xs' style='background:url(ulowebsite/images/avatar.png);'></div><div class='mpUsername'><a href='javascript:showpro();' class=' mnuProfileOpen'>Hi admin<i class='fa fa-angle-down'></i></a></div><div class='mpSubMenu' id='showmenu' ><ul><li><a href='ulouserprofile'>Profile</a></li><li><a href='ulouserprofile'>Booking History</a></li><li><a href='ulologout'>Log out</a></li></ul></div></div>");
    			}
   
    		}, function errorCallback(response) {
   
   
    			var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:red">This Email ID Already Exist</div>';
    			$('#signuperror').html(alertmsg);
   
    			// or server returns response with an error status.
    		});
   
    	};
   
   
    	$scope.passwordRequest = function () {
   
   
    		var fdata = "emailId=" + $('#forgetEmailId').val();
   
   
    		$http({
    			method: 'POST',
    			data: fdata,
    			headers: {
    				'Content-Type': 'application/x-www-form-urlencoded'
    			},
    			url: 'ulosendpassword'
    		}).then(function successCallback(response) {
   
    			var alertmsg = ' <div class="alert alert-success alert-dismissible" style="color:red">Password Sent Sucessfully.</div>';
   
    			$('#forgetmessage').html(alertmsg);
   
   
    		}, function errorCallback(response) {
   
   
    			var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:red">Your Email ID does not Registred Yet</div>';
    			$('#forgetmessage').html(alertmsg);
   
   
    		});
   
    	};
   
    	$('#forgetPanel').hide();
    	$scope.showForget = function () {
   
    		$('#loginPanel').hide();
    		$('#forgetPanel').show();
   
   
    	};
   
    	$scope.showLogin = function () {
   
    		$('#forgetPanel').hide();
    		$('#loginPanel').show();
   
   
    	};
   
   $timeout(function () {
     	$('#btnclose').click();
     }, 1000);
     $timeout(function () {
     	$('#btncloses').click();
     }, 1000);
   
   });
   
   
</script>