<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="s" uri="/struts-tags"%>

 <!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/codemirror.min.css">
 <link href="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.6.0/css/froala_editor.pkgd.min.css" rel="stylesheet" type="text/css" />
 <link href="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.6.0/css/froala_style.min.css" rel="stylesheet" type="text/css" />
 -->
   
   <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
           Blog Article Content  
          </h1>
          <ol class="breadcrumb">
            <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Blog Article Content</li>
          </ol>
        </section>
        <!-- Main content -->
<section class="content">
 <div class="row">
<div class="col-md-12 col-sm-12">
 <form>
<div class="form-group col-md-6 col-sm-6"  >
<h4>Select Article: </h4>
 <select name="blogArticleId" id="blogArticleId" ng-change="getSeoBlogArticleContent()" ng-model="blogArticleId" class="form-control"> 
<!-- <option value="select">select</option> -->
 <option ng-repeat="ba in blogArticles" value="{{ba.blogArticleId}}">{{ba.blogArticleTitle}}</option>
 </select>
 </div>
 <div  >  <!-- ng-repeat="slc in seoLocationContent" -->

<div class="form-group col-md-12 col-sm-12">
<h4>Meta Tags: </h4>
<textarea id="title" style="font-size: 14px;font-weight: normal;resize: none;overflow-y: scroll;" name="title" class="form-control" ng-model="title" value="{{seoLocationContent.data[0].title}}"  >
</textarea>
</div>
<div class="form-group col-md-12 col-sm-12">
<h4>Description: </h4>                                  
<textarea id="description" style="font-size: 14px;font-weight: normal;resize: none;overflow-y: scroll;" name="description" ng-model="description"  class=" form-control" value="{{seoLocationContent.data[0].description}}">
</textarea>
</div>
<div class="form-group col-md-12 col-sm-12">
<h4>Script : </h4>                                                                     
<textarea id="content" style="font-size: 14px;font-weight: normal;resize: none;overflow-y: scroll;" name="content" ng-model="content" class=" form-control" value="{{seoLocationContent.data[0].content}}"   ng-required="true">		                    		
</textarea>
</div>
<!-- <div class="form-group col-md-12 col-sm-12">
 <h4>key Words: </h4>                                   
 <textarea  id="keywords" style="font-size: 14px;font-weight: normal;resize: none;overflow-y: scroll;" name="keywords" ng-model="keywords" class=" form-control" value="{{seoLocationContent.data[0].keywords}}"    ng-required="true">
</textarea>
</div>
<div class="form-group col-md-12 col-sm-12">
 <h4>Short Description : </h4>                                   
 <textarea  id="shortDescription" style="font-size: 14px;font-weight: normal;resize: none;overflow-y: scroll;" name="shortDescription" ng-model="shortDescription" class=" form-control" value="{{seoLocationContent.data[0].shortDescription}}"    ng-required="true">
</textarea>
</div>
<div class="form-group col-md-12 col-sm-12">
 <h4>Social Meta Tag : </h4>                                   
 <textarea  id="socialMetaTag" style="font-size: 14px;font-weight: normal;resize: none;overflow-y: scroll;" name="socialMetaTag" ng-model="socialMetaTag" class=" form-control" value="{{seoLocationContent.data[0].socialMetaTag}}"    ng-required="true">
</textarea>
</div>
<div class="form-group col-md-12 col-sm-12">
 <h4>H1 Tags: </h4>                                   
 <textarea  id="h1" style="font-size: 14px;font-weight: normal;resize: none;overflow-y: scroll;" name="h1" ng-model="h1" class=" form-control" value="{{seoLocationContent.data[0].h1}}"    ng-required="true">
</textarea>
</div>
<div class="form-group col-md-12 col-sm-12">
 <h4>H2 Tags: </h4>                                   
 <textarea  id="h2" style="font-size: 14px;font-weight: normal;resize: none;overflow-y: scroll;" name="h2" ng-model="h2"  class=" form-control" value="{{seoLocationContent.data[0].h2}}"    ng-required="true">
</textarea>
</div>
<div class="form-group col-md-12 col-sm-12">
 <h4>H3 Tags: </h4>                                   
 <textarea  id="h3" style="font-size: 14px;font-weight: normal;resize: none;overflow-y: scroll;" name="h3" ng-model="h3" class=" form-control" value="{{seoLocationContent.data[0].h3}}"    ng-required="true">
</textarea>
</div>
<div class="form-group col-md-12 col-sm-12">
 <h4>H4 Tags: </h4>                                   
 <textarea  id="h4" style="font-size: 14px;font-weight: normal;resize: none;overflow-y: scroll;" name="h4" ng-model="h4"  class=" form-control" value="{{seoLocationContent.data[0].h4}}"    ng-required="true">
</textarea>
</div> -->
 <div class="form-group col-md-12 col-sm-12">
<button type="submit" ng-click="editSeoBlogArticleContent()" class="btn btn-primary btngreeen pull-right" >Save</button>
</div>
</div> <!-- NG-repeat ends -->

</form>
</div>
</div>
</section>                           
 </div>                       
          



<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>
  
 <%--  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> 
 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/codemirror.min.js"></script>
 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/mode/xml/xml.min.js"></script>
 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.6.0//js/froala_editor.pkgd.min.js"> </script> --%>
	
 <script>


 
	
		var app = angular.module('myApp', ['ngProgress']);
		app.controller('customersCtrl', function($scope, $http,$timeout,ngProgressFactory) {

			 $(function(){
				  $('#edit').froalaEditor()
				}); 
			
	       	$timeout(function(){
	      	    // $scope.progressbar.complete();
	            $scope.show = true;
	            $("#pre-loader").css("display","none");
	        }, 2000);

			 
			
			$scope.unread = function() {
			//var notifiurl = "http://localhost:8085/collaborative-workflow/unreadnotifications.action";
			var notifiurl = "unreadnotifications.action";
			$http.get(notifiurl).success(function(response) {
				$scope.latestnoti = response.data;
			});
			};

           
			
			
			var spinner = $('#loadertwo');
			 $scope.getSeoBlogArticleContent = function(){
				
				var fdata = "blogArticleId=" + $('#blogArticleId').val();
				spinner.show();
				$http(
						{
							method : 'POST',
							data : fdata,
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							url : 'get-seo-blog-article-content'
						}).then(function successCallback(response) {
							 setTimeout(function(){ spinner.hide(); 
							  }, 1000);
							$scope.seoLocationContent = response.data;
							//alert("Your Location Is changed");
							
							$scope.title = $scope.seoLocationContent.data[0].title;
							$scope.description = $scope.seoLocationContent.data[0].description;
							$scope.keywords = $scope.seoLocationContent.data[0].keywords;
							$scope.content = $scope.seoLocationContent.data[0].content;
							$scope.shortDescription = $scope.seoLocationContent.data[0].shortDescription;
							$scope.socialMetaTag = $scope.seoLocationContent.data[0].socialMetaTag;
							$scope.h1 = $scope.seoLocationContent.data[0].h1;
							$scope.h2 = $scope.seoLocationContent.data[0].h2;
							$scope.h3 = $scope.seoLocationContent.data[0].h3;
							$scope.h4 = $scope.seoLocationContent.data[0].h4;
							//alert($scope.h1);description
							
				
			 	}, function errorCallback(response) {
			 		 setTimeout(function(){ spinner.hide(); 
			 		  }, 1000);
					// called asynchronously if an error occurs
					// or server returns response with an error status.
				});

			};     
			
			$scope.editSeoBlogArticleContent = function(){
				
				var title = $scope.getEscapeHtml($('#title').val());
			    var description = $scope.getEscapeHtml($('#description').val());
			    var content = $('#content').val();
			    var temp = content.replace("+", "plus");
				var fdata = "title=" + title
				+ "&description=" + description
				+ "&content=" + temp
				+ "&keywords=" + $('#keywords').val()
				+ "&shortDescription=" + $('#shortDescription').val()
				+ "&socialMetaTag=" + $('#socialMetaTag').val()
				+ "&h1=" + $('#h1').val()
				+ "&h2=" + $('#h2').val()
				+ "&h3=" + $('#h3').val()
				+ "&h4=" + $('#h4').val()
				+ "&blogArticleId=" + $('#blogArticleId').val();
				
				
				spinner.show();
				
				//alert(fdata);
				$http(
						{
							method : 'POST',
							data : fdata,
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							url : 'edit-seo-blog-article-content'
						}).then(function successCallback(response) {
						//	alert("Location Content Added Successfully");
							//$scope.getUsers();
							var alert = ' <div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Alert!</h4>Success User Updated Succesfully.</div>';
				            $('#editmessage').html(alert);
				            
							$timeout(function(){
							$('#editbtnclose').click();
					        }, 2000);
							 setTimeout(function(){ spinner.hide(); 
							  }, 1000);
							
				}, function errorCallback(response) {
					 setTimeout(function(){ spinner.hide(); 
					  }, 1000);
					// called asynchronously if an error occurs
					// or server returns response with an error status.
				});

			};
			
			
			$scope.getEscapeHtml =  function(unsafe){
				
				 return unsafe
		         .replace(/&/g, "and")
		         //.replace(/</g, "&lt;")
		         //.replace(/>/g, "&gt;")
		         //.replace(/'/g, "&#039;")
		         //.replace(/"/g, "'")
				 .replace(/%/g, "percent");
		        
				
			};
			

		 		$scope.getBlogArticles = function() {
	      			
	      			var url = "get-all-blog-articles";
	      			$http.get(url).success(function(response) {
	      				//console.log(response);
	      				//alert(JSON.stringify(response.data));
	      				$scope.blogArticles = response.data;
	      			
	      			});
	      		};
		 		
				
			
		    $scope.getBlogArticles();
			
		   
		    
			//$scope.unread();
			//
			
			
		});

	
		
		   
		        

		
	</script>
	
	
	<%-- <script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.js"></script> 

<script>
   $(function () {
      // Replace the <textarea id="editor1"> with a CKEditor
      // instance, using default configuration.
      CKEDITOR.replace('editor1');
      //bootstrap WYSIHTML5 - text editor
      $(".textarea").wysihtml5();
      
      CKEDITOR.replace('metaDescription');
      //bootstrap WYSIHTML5 - text editor
      $(".textarea").wysihtml5();
      
     CKEDITOR.replace('schemaContent');
      //bootstrap WYSIHTML5 - text editor
      $(".textarea").wysihtml5();
      
      CKEDITOR.replace('locationContent');
      //bootstrap WYSIHTML5 - text editor
      $(".textarea").wysihtml5();
      
     CKEDITOR.replace('scriptContent');
     // bootstrap WYSIHTML5 - text editor
      $(".textarea").wysihtml5();
    });
	
  
</script> --%>





 
	
	 <script src="js/jquery-2.1.1.js" type="text/javascript"></script>
       