<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
 <%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<div id="wrapper">
       <section id="page-header" class="section background">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <ul class="c1 breadcrumb text-left">
                        <li><a href="index">Home</a></li>
                        <li>Cancellation & Refund Policy</li>
                    </ul>
                    <h3>Cancellation & Refund Policy</h3>
                    
                </div>
            </div> 
        </div>
    </section>

    

    <section class="section clearfix">
        <div class="container">
            <div class="row">
                <div id="fullwidth" class="col-sm-12 pad-bot">

                    <!-- START CONTENT -->
                  
                        <div class="col-md-12 col-sm-12 col-xs-12">
<p>In the event of cancellation of hotel rooms booked by you, due to any avoidable / unavoidable reason/s we must be notified of the same in writing. Cancellation charges will be effective from the date we receive advice in writing, and cancellation charges would be as follows:</p>

<ol class="list-style" type="A">
<li>45 days prior to arrival: 100 %of the Room / service cost</li>
<li>15 days prior to arrival:  60 % of the Room / service cost</li>
<li>07 days prior to arrival:  25 % of the Room / service cost</li>
<li>48 hours prior to arrival OR No Show: No Refund</li>
</ol>
<p><span style="font-weight:600;">Note:</span> Written cancellation be accepted on all working days, except Sunday's, Any cancellation sent on Sunday's will be considered on next working day (Monday).</p>
      <p style="font-weight:600;">Vacation timings payment is non-refundable.</p>
      <p><span style="font-weight:600;">Please Note:</span> We will not be responsible for any costs arising out of unforeseen circumstances like landslides, road blocks, bad weather, natural disorder etc.</p>
      </div><!-- end col -->
                    <!-- END CONTENT -->

                </div><!-- end fullwidth -->
            </div><!-- end row -->
        </div><!-- end container -->
    </section><!-- end section -->
</div>
<script src="ulowebsite/js/jquery.min.js"></script>
<script src="ulowebsite/js/jquery-ui.js"></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-86275668-1', 'auto');
  ga('send', 'pageview');

</script> 
