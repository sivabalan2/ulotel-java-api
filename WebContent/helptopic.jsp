<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>


<div id="page-wrapper" class="full-width-page-wrapper">
	<div id="page-inner">
		<div class="col-md-12" style="min-height: 100%; position: relative;">
		  <ol class="breadcrumb bread-primary">
                                    <li>Home</li>
                                    <li>Help</li>
                            </ol>
			
			<!--help page start-->
			<div class="help-page-container">
				<div class="help-page-title">
					<div class="col-md-6 col-sm-5 col-xs-12">Suggested Topics</div>
					<div class="col-md-6 col-sm-7 col-xs-12" style="padding: 0px;">
						<div class="help-page-right">
							<div class="add-user" id="adduser">
								<div class="help-page-right-action">
									How <b>can we help you? </b><span><img
										src="image/search-icon.svg" /></span>
								</div>
							</div>

						</div>

					</div>
				</div>

	


<%-- <div class="accordion" id="ppanel-{{$index}}" role="tablist" multiselectable="true"  ng-repeat="z in help">
	<div class="panel-title"><a  onclick='clikedA(this.id);' aria-expanded="true" aria-controls="panel-{{$index}}" id="control-panel-{{$index}}">{{z.topicName}}<span class="icon"><i class="ion-ios-arrow-right"></i></span></a></div>
	<div class="panel-content" id="cpanel-{{$index}}" aria-labelledby="control-panel-{{$index}}" aria-hidden="false" role="tabpanel">
	Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
	
	{{z.topicContent}}</div>
</div>		</div> --%>


 
			
			<!--  <div class="accordion" >
                                    <div class="panel-title">LOREUM IPSUM DOLOR SETE</div>                                  
                                    <div class="panel-content">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                                       Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
                                       commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                                       Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                                       sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                       Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
                                       Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                                    </div>
                                </div> -->
                                
                                
                                
                                		 <div class="accordion"  ng-repeat="z in help">
                                    <div class="panel-title">{{z.topicName}}</div>                                  
                                    <div class="panel-content" ng-bind-html-unsafe="html">
                                       {{z.topicContent}}
                                    </div>
                                </div>
			<!-- help page End-->

			<!--footer start-->
			<div class="footer-container">
				<div class="footer-left">
					<span><img src="image/copy-rights-icon.svg" /></span>
					Codemantra.com 2016
				</div>
				<div class="footer-right">Disclaimer</div>
			</div>
			<!--footer end-->
		</div>
	</div>
	<!-- PAGE INNER -->
</div>
<!-- PAGE WRAPPER -->

<script
	src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script src="js/jquery-2.1.1.js" type="text/javascript"></script>

<!--content toggle script-->
<script src="js/content-toggle/modernizr.js" type="text/javascript"></script>



<script>
        var app = angular.module('myApp', ['ngProgress']);
		app.controller('customersCtrl', function($scope, $http,$timeout,ngProgressFactory) {

			//$scope.progressbar = ngProgressFactory.createInstance();
			//$scope.progressbar.setColor("green");
			//$scope.progressbar.start();
	       	$timeout(function(){
	      	     //$scope.progressbar.complete();
	            $scope.show = true;
	            $("#pre-loader").css("display","none");
	        }, 2000);
	        
			$scope.gethelp = function() {
				//var customurl = "http://localhost:8085/collaborative-workflow/task-view.action?id="+ $('#objectId').val();
				var customurl = "helptopicjson.action";

				//alert(customurl);
				$http.get(customurl).success(function(response) {
					$scope.help = response.data;
					//console.log($scope.help);


					  $timeout(HelpBinding, 10); 
				});
			};

			$scope.gethelp();

			

			$scope.unread = function() {
				//var notifiurl = "http://localhost:8085/collaborative-workflow/unreadnotifications.action";
				var notifiurl = "unreadnotifications.action";
				$http.get(notifiurl).success(function(response) {
					$scope.latestnoti = response.data;
				});
				};
				$scope.unread();
		});
 
        // Hiding the panel content. If JS is inactive, content will be displayed
       
        </script>
        <!--user master show hide script-->
        <script>

        function HelpBinding()
        {

console.log('Printing Helpbinding');
            
       	 $( '.panel-content' ).hide();

            // Preparing the DOM

            // -- Update the markup of accordion container 
            $( '.accordion' ).attr({
              role: 'tablist',
              multiselectable: 'true'
             });

            // -- Adding ID, aria-labelled-by, role and aria-labelledby attributes to panel content
            $( '.panel-content' ).attr( 'id', function( IDcount ) { 
              return 'panel-' + IDcount; 
            });
            $( '.panel-content' ).attr( 'aria-labelledby', function( IDcount ) { 
              return 'control-panel-' + IDcount; 
            });
            $( '.panel-content' ).attr( 'aria-hidden' , 'true' );
            // ---- Only for accordion, add role tabpanel
            $( '.accordion .panel-content' ).attr( 'role' , 'tabpanel' );

            // -- Wrapping panel title content with a <a href="">
            $( '.panel-title' ).each(function(i){

              // ---- Need to identify the target, easy it's the immediate brother
              $target = $(this).next( '.panel-content' )[0].id;

              // ---- Creating the link with aria and link it to the panel content
              $link = $( '<a>', {
                'href': '#' + $target,
                'aria-expanded': 'false',
                'aria-controls': $target,
                'id' : 'control-' + $target
              });

              // ---- Output the link
              $(this).wrapInner($link);  

            });

            // Optional : include an icon. Better in JS because without JS it have non-sense.
            $( '.panel-title a' ).append('<span class="icon"><i class="ion-ios-arrow-right"></i></span>');

            // Now we can play with it
            $( '.panel-title a' ).click(function() {

              if ($(this).attr( 'aria-expanded' ) == 'false'){ //If aria expanded is false then it's not opened and we want it opened !

                // -- Only for accordion effect (2 options) : comment or uncomment the one you want

                // ---- Option 1 : close only opened panel in the same accordion
                //      search through the current Accordion container for opened panel and close it, remove class and change aria expanded value
                $(this).parents( '.accordion' ).find( '[aria-expanded=true]' ).attr( 'aria-expanded' , false ).removeClass( 'active' ).parent().next( '.panel-content' ).slideUp(200).attr( 'aria-hidden' , 'true');

                // Option 2 : close all opened panels in all accordion container
                //$('.accordion .panel-title > a').attr('aria-expanded', false).removeClass('active').parent().next('.panel-content').slideUp(200);

                // Finally we open the panel, set class active for styling purpos on a and aria-expanded to "true"
                $(this).attr( 'aria-expanded' , true ).addClass( 'active' ).parent().next( '.panel-content' ).slideDown(200).attr( 'aria-hidden' , 'false');

              } else { // The current panel is opened and we want to close it

                $(this).attr( 'aria-expanded' , false ).removeClass( 'active' ).parent().next( '.panel-content' ).slideUp(200).attr( 'aria-hidden' , 'true');;

              }
              // No Boing Boing
              return false;
            });

            

            }
         $(document).ready(function (){


   
           });


         
        </script>