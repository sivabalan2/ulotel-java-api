<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="s" uri="/struts-tags"%>

   <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Users Details  
          </h1>
          <ol class="breadcrumb">
            <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Property Users</li>
          </ol>
        </section>
		
        <!-- Main content -->
        <section class="content">
        
        	<div class="row">
            	<div class="col-xs-12" ng-if="users.length > 0">
						<div class="box">     
							<div class="box-header">
								<h3 class="box-title">Users</h3>
							</div>
							
							
							<!-- /.box-header -->
							<div class="box-body table-responsive no-padding" > 
								<table class="table table-hover">
									<tr>										
										<th>User Name</th>
										<th>Email</th>
										<th>Role</th>
										<th>Edit</th>
										<th>Delete</th>
									</tr>
									<tr ng-repeat="u in users">										
										<td>{{u.userName}}</td>
										<td>{{u.emailId}}</td>
										<td>{{u.roleName}}</td>
										<td>
											<a href="#editUser" ng-click="getSingleUser(u.propUserId)"  data-toggle="modal"><i  class="fa fa-pencil text-green"></i></a>
										</td>
										<td>
										<a  href="" ng-click="deleteUser(u.propUserId,u.userId)" ><i  class="fa fa-fw fa-trash text-red"></i></a>
										</td>
									</tr>
									<tr>
									</tr>
								</table>
							</div>
							<!-- /.box-body -->
							
							
							<div class="box-footer clearfix">
								<span id="addgetuseralert"></span>
								 <ul class="pagination pagination-sm no-margin pull-right">
									<a class="btn btn-primary" href="#adduser"  data-toggle="modal" >Add User</a>
								</ul> 
							</div>
						</div>
						<!-- /.box -->
					</div>	
         	</div>
          
        </section><!-- /.content -->
        
      	
							
      	<div class="modal" id="adduser">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" id="btnclose" class="close" data-dismiss="modal"
							aria-hidden="true">x</button>
						<h4 class="modal-title">Add User</h4>
					</div>
					 <form name="addUser">
					<div class="modal-body" >
					<div id="message"></div>
						<div class="form-group">
							<label>User Name</label>
							<input type="text" class="form-control"  name="userName"  id="userName"  placeholder="UserName" ng-model='userName' ng-pattern="/^[a-zA-Z0-9]+$/" ng-required="true" autocomplete="off">
						    <span ng-show="addUser.userName.$error.pattern" style="color:red">Not a valid Name!</span>
						     
						</div>
						
						<div class="form-group">
							<label>Email</label>
							<input type="email" class="form-control"  name="emailId"  id="emailId"  placeholder="email" ng-model='emailId'  ng-pattern="/^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/" ng-required="true" autocomplete="off">
						    <span ng-show="addUser.emailId.$error.pattern"  style="color:red">Not a valid Email!</span>
						</div>
						
						<div class="form-group">
							<label>Roles</label>
							<select name="roleId" id="roleId" value="" class="form-control">
	                       	<option ng-repeat="r in roles" value="{{r.DT_RowId}}">{{r.roleName}}</option>
	                        </select>
							
						</div>
						
						
						
					</div>
					<div class="modal-footer">
						<span id="adduseralert"></span>
						<a href="#" data-dismiss="modal" class="btn btn-danger">Close</a>
						<button ng-click="addUser.$valid && addSingleUser()"  class="btn btn-primary">Save</button>
					</div>
					</form>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dalog -->
		</div>
		<div class="modal" id="editUser">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" id="editbtnclose" class="close" data-dismiss="modal"
							aria-hidden="true">x</button>
						<h4 class="modal-title">Edit User</h4>
					</div>
					 <form name="editUser">					
					<div class="modal-body" ng-repeat ="us in user" >
					<div id="editmessage"></div>
					    <input type="hidden" value="{{us.propertyUserId}}" name="editPropertyUserId" id="editPropertyUserId">
					    <input type="hidden" value="{{us.userId}}" name="editUserId" id="editUserId">
						<div class="form-group">
							<label>User Name</label>
							<input type="text" class="form-control"  name="editUserName"  id="editUserName" value="{{us.userName}}" ng-model='us.userName' ng-pattern="/^[a-zA-Z0-9]+$/" ng-required="true" autocomplete="off">
						    <span ng-show="editUser.editUserName.$error.pattern" style="color:red">Not a valid Name!</span>
						     
						</div>
						
						<div class="form-group">
							<label>Email</label>
							<input type="email" class="form-control"  name="editEmailId"  id="editEmailId"  value="{{us.emailId}}" ng-model='us.emailId'  ng-pattern="/^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/" ng-required="true" autocomplete="off">
						    <span ng-show="editUser.editEmailId.$error.pattern"  style="color:red">Not a valid Email!</span>
						</div>
						
						<div class="form-group">
							<label>Roles</label>
							<select name="editRoleId" id="editRoleId" value="" class="form-control">
	                       	<option ng-repeat="r in roles" value="{{r.DT_RowId}}" ng-selected ="r.DT_RowId == us.roleId">{{r.roleName}}</option>
	                        </select>							
						</div>
						
						<div class="modal-footer">
						<span id="edituseralert"></span>	
						<a href="#" data-dismiss="modal" class="btn btn-danger">Close</a>
						<button ng-click="editUser.$valid && editSingleUser(us.userId)" class="btn btn-primary">Save</button>
					</div>
						
					</div>
					</form>
					
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dalog -->
		</div>
    

      </div><!-- /.content-wrapper -->
      


<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>

	<script>


 
	
		var app = angular.module('myApp', ['ngProgress']);
		app.controller('customersCtrl', function($scope, $http,$timeout,ngProgressFactory) {
			var spinner = $('#loadertwo');

			
	       	$timeout(function(){
	      	    // $scope.progressbar.complete();
	            $scope.show = true;
	            $("#pre-loader").css("display","none");
	        }, 2000);

			 
			
			$scope.unread = function() {
			//var notifiurl = "http://localhost:8085/collaborative-workflow/unreadnotifications.action";
			var notifiurl = "unreadnotifications.action";
			$http.get(notifiurl).success(function(response) {
				$scope.latestnoti = response.data;
			});
			};

           
			
			$scope.getUsers = function() {
				
				
				
				var url = "get-property-users";
				$http.get(url).success(function(response) {
					
					console.log(response);
	                
					$scope.users = response.data;
		
				});
			};
			
			
			
			  $scope.getRoles = function() {
					
					var url = "get-property-roles";
					$http.get(url).success(function(response) {
					    //console.log(response);
						$scope.roles = response.data;
			
					});
			   };
				
			$scope.addSingleUser = function(){
				
				//alert($('#taskId').val());
				var fdata = "userName=" + $('#userName').val()
				+ "&emailId=" + $('#emailId').val()
				+ "&roleId=" + $('#roleId').val();
				
				spinner.show();
				//alert(fdata);
				$http(
						{
							method : 'POST',
							data : fdata,
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							url : 'add-property-user'
						}).then(function successCallback(response) {
							var check=response.data.data[0].status;
							setTimeout(function(){ spinner.hide(); 
						   	  }, 1000);
							if(check=="true"){
								$("#addgetuseralert").hide();
								   var alertmsg = ' <div class="alert" style="color:green;text-align:center;">User added successfully</div>';
						            $('#addgetuseralert').html(alertmsg);
						            $("#addgetuseralert").fadeTo(2000, 500).slideUp(500, function(){
						              
						                 });
						            $('#adduser').modal('toggle');
						            $scope.getUsers();
						           
							}else if(check=="false"){
								$("#adduseralert").hide();
								   var alertmsg = ' <div class="alert" style="color:green;text-align:center;">User already exists</div>';
						            $('#adduseralert').html(alertmsg);
						            $("#adduseralert").fadeTo(2000, 500).slideUp(500, function(){
						              
						                 });
						           
						            return;
						            
						            
							}else if(check=="NA"){
								$("#adduseralert").hide();
								   var alertmsg = ' <div class="alert" style="color:green;text-align:center;">User role is incorrect</div>';
						            $('#adduseralert').html(alertmsg);
						            $("#adduseralert").fadeTo(2000, 500).slideUp(500, function(){
						              
						                 });
						           
						            return;
							}
							   
				}, function errorCallback(response) {
					setTimeout(function(){ spinner.hide(); 
				   	  }, 1000);
					$("#adduseralert").hide();
					   var alertmsg = ' <div class="alert" style="color:green;text-align:center;">Process failed please try again</div>';
			            $('#adduseralert').html(alertmsg);
			            $("#adduseralert").fadeTo(2000, 500).slideUp(500, function(){
			              
			                 });
			             
			            return;
					// called asynchronously if an error occurs
					// or server returns response with an error status.
					   
				});

			};
			
			$scope.editSingleUser = function(userid){
				
				
				
				//alert($('#editRoleId').val());
				
				var fdata = "userName=" + $('#editUserName').val()
				+ "&emailId=" + $('#editEmailId').val()
				+ "&roleId=" + $('#editRoleId').val()
				+ "&propertyUserId=" + $('#editPropertyUserId').val()
				+"&userId="+ $('#editUserId').val();
				spinner.show();
				//alert(fdata);
				$http(
						{
							method : 'POST',
							data : fdata,
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							url : 'edit-property-user'
						}).then(function successCallback(response) {
							var check=response.data.data[0].status;
							   setTimeout(function(){ spinner.hide(); 
							   	  }, 1000);
							   if(check=="true"){
								   $("#addgetuseralert").hide();
								   var alertmsg = ' <div class="alert" style="color:green;text-align:center;">User updated successfully</div>';
						            $('#addgetuseralert').html(alertmsg);
						            $("#addgetuseralert").fadeTo(2000, 500).slideUp(500, function(){
						              
						                 });
						            $('#editUser').modal('toggle');
									$scope.getUsers();
							   }else if(check=="false"){
								   $("#edituseralert").hide();
								   var alertmsg = ' <div class="alert" style="color:green;text-align:center;">User role is incorrect</div>';
						            $('#edituseralert').html(alertmsg);
						            $("#edituseralert").fadeTo(2000, 500).slideUp(500, function(){
						              
						                 });
						            
						            return;
							   }else if(check=="NA"){
								   $("#edituseralert").hide();
								   var alertmsg = ' <div class="alert" style="color:green;text-align:center;">User email is incorrect</div>';
						            $('#edituseralert').html(alertmsg);
						            $("#edituseralert").fadeTo(2000, 500).slideUp(500, function(){
						              
						                 });
						            
						            return;
						        
							   }
							
				}, function errorCallback(response) {
					   setTimeout(function(){ spinner.hide(); 
					   	  }, 1000);
					   $("#edituseralert").hide();
					   var alertmsg = ' <div class="alert" style="color:green;text-align:center;">Process failed please try again</div>';
			            $('#edituseralert').html(alertmsg);
			            $("#edituseralert").fadeTo(2000, 500).slideUp(500, function(){
			              
			                 });
			            
			            return;
					// called asynchronously if an error occurs
					// or server returns response with an error status.
				});

			};
			
            $scope.getSingleUser = function(rowid,userid) {
				
				//alert(rowid); 
				var url = "get-user?propertyUserId="+rowid;
					$http.get(url).success(function(response) {
				    console.log(response);				
					$scope.user = response.data;
		
				});
				
			};
			
			$scope.deleteUser = function(rowid,userId) {
				
				//alert(rowid);
				var check = confirm("Are you sure you want to delete this User?");
    			if( check == true){
				var url = "delete-property-user?propertyUserId="+rowid+"&userId="+userId;
					$http.get(url).success(function(response) {
						$scope.getUsers(); 
				});
    			}
				
			};

			 $scope.getPropertyList = function() {
				 
		        	
		        	
			        var userId = $('#adminId').val();
		 			var url = "get-user-properties?userId="+userId;
		 			$http.get(url).success(function(response) {
		 			    
		 				$scope.props = response.data;
		 	
		 			});
		 		};
		 		
            $scope.change = function() {
     	   
		        //alert($scope.id);
		        
		        var propertyId = $scope.id;	
     	    var url = "change-user-property?propertyId="+propertyId;
	 			$http.get(url).success(function(response) {
	 				
	 				 window.location = '/ulopms/dashboard'; 
	 				//$scope.change = response.data;
	 	
	 			});
			       
		 		};
		 		
				
		    $scope.getPropertyList();
			
			
			$scope.getUsers();
			
		    $scope.getRoles();
		    
			//$scope.unread();
			//
			
			
		});

	
		
		   
		        

		
	</script>
	
	 <script src="js/jquery-2.1.1.js" type="text/javascript"></script>
       