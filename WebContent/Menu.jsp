<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sj" uri="/struts-jquery-tags"%>

  
   <!-- Left column -->
            <a href="#"><strong><i class="glyphicon glyphicon-wrench"></i> Tools</strong></a>

            <hr>

            <ul class="nav nav-stacked">
            
            	<s:set name="vmenu" value="aaa"/>
					<s:iterator value="user.role.screens" var="menu" >
						<s:if test="%{#vmenu.contains(#menu.menu.menuName)}">
	
						</s:if>
						<s:else>
							<li class="nav-header"> <a href="#" data-toggle="collapse" data-target="#userMenu"><s:property value="%{#menu.menu.menuName}"/> <i class="glyphicon glyphicon-chevron-down"></i></a>
							 <ul class="nav nav-stacked collapse in" id="userMenu">
							<s:iterator value="user.role.screens" var="screen">
								<s:if test="%{#menu.menu.menuName==#screen.menu.menuName}">
									 <li> <s:a href="%{#screen.screenPath}"><i class="glyphicon"></i> <s:property value="%{#screen.screenName}"/></s:a></li>
								</s:if>
							</s:iterator>
							</ul></li>
						</s:else>
						<s:set name="vmenu" value="%{#vmenu +#menu.menu.menuName}"/>
					</s:iterator>
            
            </ul>

            <hr>

            <a href="#"><strong><i class="glyphicon glyphicon-link"></i> Resources</strong></a>

            <hr>

            <ul class="nav nav-pills nav-stacked">
                <li class="nav-header"></li>
                <li><a href="#"><i class="glyphicon glyphicon-list"></i> Layouts &amp; Templates</a></li>
                <li><a href="#"><i class="glyphicon glyphicon-briefcase"></i> Toolbox</a></li>
                <li><a href="#"><i class="glyphicon glyphicon-link"></i> Widgets</a></li>
                <li><a href="#"><i class="glyphicon glyphicon-list-alt"></i> Reports</a></li>
                <li><a href="#"><i class="glyphicon glyphicon-book"></i> Pages</a></li>
                <li><a href="#"><i class="glyphicon glyphicon-star"></i> Social Media</a></li>
            </ul>


           
  
