<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="s" uri="/struts-tags"%>
<style>
    .pac-container {
        z-index: 10000 !important;
    }
</style>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
   <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
           Google Area
          </h1>
          <ol class="breadcrumb">
            <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Google Area</li>
           
             
          </ol>
          
        </section>
		
        <!-- Main content -->
        <section class="content">
        
        	<div class="row">
            	<div class="col-xs-12" ng-if="googleAreas.length>=0">
						<div class="box">
							<div class="box-header">
								<h3 class="box-title">Google Area</h3>
								 <a class="btn btn-primary pull-right" href="edit-inactive-area">View Inactive Areas
								 </a>
							</div>
							
							
							<!-- /.box-header -->
							<div class="box-body table-responsive no-padding" > 
								<table class="table table-hover" id="example1">
									<tr>
										<th>Order</th>
										<th>Google Location </th>
										<th>Google Area Name</th>
										<th>Edit</th>
										<th>Delete</th>
										<th>User Name</th>
									</tr>
									<tr ng-repeat="ga in googleAreas">
										<td>{{ga.serialNo}}</td>
										<td>{{ga.googleLocationName}}</td>
										<td>{{ga.googleAreaName}}</td>
										<td>
											<a href="#editmodal" ng-click="getGoogleArea(ga.googleAreaId)"  data-toggle="modal"><i  class="fa fa-pencil text-green"></i></a>
										</td>
										<td>
											<a href="#deletemodal" ng-click="deleteGoogleArea(ga.googleAreaId)" data-toggle="modal" ><i  class="fa fa-fw fa-trash text-red" ></i></a>
										</td>
										<td>{{ga.createdBy}}</td>
									</tr>
									<tr>
									</tr>
								</table>
							</div>
							<!-- /.box-body -->
							
							
							<div class="box-footer clearfix">
								 <ul class="pagination pagination-sm no-margin pull-right">
									<a class="btn btn-primary " href="#addmodal" data-toggle="modal" >Add Area</a>
								</ul> 
							</div>
							<div class="box-footer clearfix">
								<ul class="pagination pagination-sm"></ul>
								<div class="simple-pagination pull-right" >   
									<p class="simple-pagination__items">Showing {{areaCount[0].count}} out of {{settings.pageLimit}}</p>
									<p><button ng-click="previousPage()" ng-disabled="settings.currentPage <= 0" class="simple-pagination__button simple-pagination__button--prev"> &#10094;          
									</button><span class="simple-pagination__pages">{{settings.currentPage + 1}} of {{getTotalPages()}}</span>
									<button ng-click="nextPage()" ng-disabled="settings.currentPage === (getTotalPages() - 1)" class="simple-pagination__button simple-pagination__button--next">&#10095;</button></p>
									<a href="" ng-click="limited(10)" ng-class="active">{{10}}</a> |
									<span><a href="" ng-click="all(areaCount[0].count)" ng-class="active">All</a></span> 
								</div>
						   </div> 
						</div>
						<!-- /.box -->
					</div>	
         	</div>
          
        </section><!-- /.content -->
      	<input type="hidden" value ="<%=session.getAttribute("areaId") %>" id="areaId" name="areaId" >
      	<div class="modal" id="addmodal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" id="btnclose" class="close" data-dismiss="modal"
							aria-hidden="true">x</button>
						<h4 class="modal-title">Add Google Area</h4>
					</div>
					 <form method="post" theme="simple" name="addarea">
					 <section>
					<div class="modal-body" >
						<div class="form-group col-md-12">
						  <label>Select Google Location<span style="color:red">*</span></label>
						  <select  name="areaLocationId" id="areaLocationId" ng-model="areaLocationId" ng-required="true" class="form-control" >
						  <option style="display:none" value="0">Select Your Location</option>
		                  <option ng-repeat="gl in googleLocations" value="{{gl.googleLocationId}}"  >{{gl.googleLocationName}}</option>
		                  </select>
						</div>
						<div class="form-group col-md-12">
							<label for="areaName">Google Area Name<span style="color:red">*</span></label>
							<input type="text" class="form-control"  name="googleAreaName"  id="googleAreaName"  placeholder="Area Name">
							<input type="hidden" id="googleAreaLatitude" name="googleAreaLatitude" class="form-control"  placeholder="Enter a location" />
							<input type="hidden" id="googleAreaLongitude" name="googleAreaLongitude" class="form-control"  placeholder="Enter a location" />
							<input type="hidden" id="googleAreaPlaceId" name="googleAreaPlaceId" class="form-control"  placeholder="Enter a location" />
						</div>
						<div class="form-group col-md-12">
							<label for="areaName">Google Area Display Name<span style="color:red">*</span></label>
							<input type="text" class="form-control"  name="googleAreaDisplayName"  id="googleAreaDisplayName"  placeholder="Display Area Name" value=""  ng-pattern="^/^[A-Za-z\d\s]+$/" ng-required="true">
						    <span ng-show="addarea.googleAreaDisplayName.$error.pattern" style="color:red">Not a Area Name!</span>
						</div>
						
						<div class="form-group col-md-12">
							<label for="areaName">Google Area Description<span style="color:red">*</span></label>
							<input type="text" class="form-control"  name="areaDescription"  id="areaDescription"  placeholder="Area Description" value="" ng-pattern="^/^[A-Za-z\d\s]+$/" ng-required="true">
						    <span ng-show="addarea.areaDescription.$error.pattern" style="color:red">Not a Area Name!</span>
						</div>
						
						<div class="form-group col-md-12">
							<label for="areaUrl">Google Area URL<span style="color:red">*</span></label>
							<input type="text" class="form-control"  name="googleAreaUrl"  id="googleAreaUrl"  placeholder="Area Url" value=""  ng-pattern="^/^[A-Za-z\d\s]+$/" ng-required="true">
						    <span ng-show="addarea.googleAreaUrl.$error.pattern" style="color:red">Not a Area Name!</span>
						</div>
						<div class="form-group col-md-6">
							<label>Google Area Image</label>
							<input type="file" class="form-control" file-input="files" name="photo" id="photo">
							<span style="color:red">upload only(214 * 221)</span>
<!-- 							<div id="validimage" name="validimage" style="color: red"><label>Not Image Format!!</label></div> -->
						</div>
						<div class="form-group" >
						   <div class="input-group" >
						  		<label>&nbsp;&nbsp;</label>
						  		<label>&nbsp;&nbsp;</label>
						  		<label>&nbsp;&nbsp;</label>
						  		<label>&nbsp;&nbsp;</label>
						  		<input type="hidden" class="form-control">
						   </div>

						</div>
					</div>
					</section>
					<section>
						<div class="modal-footer">
							<a href="#" data-dismiss="modal" class="btn btn-danger">Close</a>
							<button ng-click="addarea.$valid && addGoogleArea()" ng-disabled="addarea.$invalid" class="btn btn-primary">Save</button>
						</div>
					</section>
					</form>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dalog -->
		</div>
		<div class="modal" id="editmodal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" id="editbtnclose" class="close" data-dismiss="modal"
							aria-hidden="true">x</button>
						<h4 class="modal-title">Edit Area</h4>
					</div>
					 	 <form method="post" theme="simple" name="editarea">
					 	 <section>
						<div class="modal-body">
							<input type="hidden" value="{{googleArea[0].googleAreaId}}" id="editGoogleAreaId" name="editGoogleAreaId">
						    <input type="hidden" value="{{googleArea[0].googleAreaLocationId}}" id="editAreaLocationId" name="editAreaLocationId">
						    <div class="form-group col-md-12">
								<label>Area Name<span style="color:red">*</span></label>
								<input type="text" class="form-control"  name="editGoogleAreaName"  id="editGoogleAreaName"  value="{{googleArea[0].googleAreaName}}" disabled>
							    <span ng-show="editarea.editAreaName.$error.pattern">Not a Area Name!</span>
							</div>
							<div class="form-group col-md-12">
								<label>Display Area Name<span style="color:red">*</span></label>
								<input type="text" class="form-control"  name="editGoogleAreaDisplayName"  id="editGoogleAreaDisplayName"  value="{{googleArea[0].googleAreaDisplayName}}" ng-required="true">
							    <span ng-show="editarea.editDisplayAreaName.$error.pattern">Not a Area Name!</span>
							</div>
							<div class="form-group col-md-12">
								<label>Area Name<span style="color:red">*</span></label>
								<input type="text" class="form-control"  name="editAreaDescription"  id=editAreaDescription  value="{{googleArea[0].googleAreaDescription}}" ng-required="true">
							    <span ng-show="editarea.editAreaDescription.$error.pattern">Not a Area Name!</span>
							</div>
							<div class="form-group col-md-12">
								<label>Area URL<span style="color:red">*</span></label>
								<input type="text" class="form-control"  name="editGoogleAreaUrl"  id="editGoogleAreaUrl"  value="{{googleArea[0].googleAreaUrl}}" ng-required="true">
							    <span ng-show="editarea.editAreaUrl.$error.pattern">Not a Area URL!</span>
							</div>
							 <div class="form-group col-md-12">
								<label for="child_included_rate">Area Image (choose the file or tab the image to upload image.)</label>
								<input type="file" class="form-control"   file-input="files" name="photo" id="photo">
								<span style="color:red">upload only(214 * 221)</span>
<!-- 								<div id="validimage" name="validimage" style="color: red"><label>Not Image Format!!</label></div> -->
							 </div>
							   <div class="form-group col-md-12">
			                     <label>Status <span style="color:red">*</span> </label>
				                 <select name="areaIsActive" id="areaIsActive" class="form-control" ng-model ="areaIsActive"  ng-change="showRedirectInput(areaIsActive)">
					                     <option value="true" ng-selected='true'>Active</option>
					                     <option value="false">InActive</option>
				                 </select>
			                   </div>
			                
			                
			                 <div class="form-group col-md-12" id ="showRedirectUrl">
							<label>Redirect Area URL<span style="color:red">*</span></label>
							<select name="editGoogleRedirectAreaUrl"  ng-model="editGoogleRedirectAreaUrl" id="editGoogleRedirectAreaUrl" placeholder="Select urls" class="form-control"  ng-required="isValidate">
                           	<option  value="" selected >Select Source</option>
							<option ng-repeat="ar in areaUrls" value="{{ar.googleRedirectUrl}}" >{{ar.googleRedirectUrl}}</option>
							</select>
						    </div> 
						    
						    
			                  
							   
							   
							   
							   
							<div class="col-md-12">
					         	<img class="img-thumbnail"  ngf-select="upload($file)"  ngf-accept="'image/*'" 
					         	ngf-pattern="'.jpg,.png,!.gif'" 
					         	src="{{googleArea[0].photoPath}}"  alt="User Avatar" width="100%">
					        </div>
								
						</div>
						</section>
						<section>
							<div class="modal-footer">
								<a href="#" data-dismiss="modal" class="btn btn-danger">Close</a>
								<button ng-click="editarea.$valid && editGoogleArea()" ng-disabled="editarea.$invalid" id ="updateArea" class="btn btn-primary">Update</button>
							</div>  
						</section>
						</form>
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dalog -->
		</div>
		
		
  

      
      

<%-- <script src="js/autocomplete.js"></script> --%>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>
<script>


 
 
 
	
	var app = angular.module('myApp',['ngFileUpload']);
	app.controller('customersCtrl',['$scope',
	             					'Upload',
	            					'$timeout',
	            					'$http', function($scope,Upload,$timeout,$http) {

		
		
		$scope.settings = {
	            currentPage: 0,
	            offset: 0,
	            pageLimit: 10,
	            pageLimits: ['10', '50', '100']
	          };

		 $scope.getTotalPages = function () {
			return Math.ceil($scope.areaCount[0].count / $scope.settings.pageLimit);
		};
			
		 $scope.isCurrentPageLimit = function (value) {
		       return $scope.settings.pageLimit == value;
		 };
		 
		 $scope.previousPage = function () {
			   $scope.settings.currentPage -= 1;
			   var previousOffset = $scope.settings.currentPage * $scope.settings.pageLimit;
		       var url = "get-google-areas?limit="+$scope.settings.pageLimit+"&offset="+previousOffset;
				$http.get(url).success(function(response) {
					//console.log(response);
					$scope.googleAreas = response.data;
				}); 
		    }; 
		 
     		$scope.nextPage = function () {
		       $scope.settings.currentPage += 1;
		       var nextOffset = $scope.settings.currentPage * $scope.settings.pageLimit;
		       var url = "get-google-areas?limit="+$scope.settings.pageLimit+"&offset="+nextOffset;
				$http.get(url).success(function(response) {
					//console.log(response);
					$scope.googleAreas = response.data;
				}); 
		     }; 
		       
      			$scope.all = function (total) {
		    	   $scope.settings.pageLimit = total;
			       var nextOffset = 0;
			       var url = "get-google-areas?limit="+total+"&offset="+nextOffset;
					$http.get(url).success(function(response) {
						//console.log(response);
						$scope.googleAreas = response.data;
					}); 
			    };
			       
      			 $scope.limited = function (limit) {
			    	   $scope.settings.pageLimit = limit;
				       var nextOffset = 0;
				       var url = "get-google-areas?limit="+limit+"&offset="+nextOffset;
						$http.get(url).success(function(response) {
							//console.log(response);
							$scope.googleAreas = response.data;
					}); 
		       };			

		    
		
	       	$timeout(function(){
	      	    // $scope.progressbar.complete();
	            $scope.show = true;
	            $("#pre-loader").css("display","none");
	        }, 2000);

			 
			
			$scope.unread = function() {
			//var notifiurl = "http://localhost:8085/collaborative-workflow/unreadnotifications.action";
			var notifiurl = "unreadnotifications.action";
			$http.get(notifiurl).success(function(response) {
				$scope.latestnoti = response.data;
			});
			};

			
			
			$scope.getGoogleAreas = function() {

				var url = "get-google-areas?limit="+$scope.settings.pageLimit+"&offset="+$scope.settings.offset;
				$http.get(url).success(function(response) {
				    console.log(response);
					$scope.googleAreas = response.data;
		
				});
			};
			
			
			$scope.addGoogleArea = function(){
				 var file = $scope.files;
				 console.log(file); 
				// var file = this.files[0];
				 var fileType = file["type"];
				 var ValidImageTypes = ["image/gif", "image/jpeg", "image/png"];
				 if ($.inArray(fileType, ValidImageTypes) < 0) {
					     // invalid file type code goes here.
					     alert('image invalid')
					}
					else{
						
						var fdata = "googleLocationId=" + $('#areaLocationId').val()
							+ "&googleAreaName=" + $('#googleAreaName').val()
							+ "&googleAreaDisplayName=" + $('#googleAreaDisplayName').val()
							+ "&googleAreaUrl=" + $('#googleAreaUrl').val()
							+ "&googleAreaLatitude=" + $('#googleAreaLatitude').val()
							+ "&googleAreaLongitude=" + $('#googleAreaLongitude').val()
							+ "&googleAreaPlaceId=" + $('#googleAreaPlaceId').val()
						    + "&description=" + $('#areaDescription').val(); 
						$http(
								{
									method : 'POST',
									data : fdata,
									headers : {
										'Content-Type' : 'application/x-www-form-urlencoded'
									},
									url : 'add-google-area'
								}).then(function successCallback(response) {
									$scope.addedareas = response.data;
									
									console.log($scope.addedareas.data[0].locationAreaId);
									var googleAreaId = $scope.addedareas.data[0].googleAreaId;
									 Upload.upload({
							                url: 'googleareapicupdate',
							                enableProgress: true, 
							                data: {myFile: file, 'googleAreaId': googleAreaId} 
							            }).then(function (resp) {
							            	
							            	
							            });
								
										
									$scope.getGoogleAreas();
									var alertmsg = ' <div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Alert!</h4>Success Item Added Succesfully.</div>';
						            $('#message').html(alertmsg);
						            //window.location.reload(); 
									$timeout(function(){
							      	$('#btnclose').click();
							        }, 2000); 
						        	
						        
						}, function errorCallback(response) {
							
						});
				}
			};
			
			$scope.editGoogleArea = function(){
				var file = $scope.files;
				 console.log(file); 
				// var file = this.files[0];
				 var fileType = file["type"];
				 var ValidImageTypes = ["image/gif", "image/jpeg", "image/png"];
				 if ($.inArray(fileType, ValidImageTypes) < 0) {
					     // invalid file type code goes here.
					     alert('image invalid')
					}
					else{
						var fdata = "googleAreaId=" + $('#editGoogleAreaId').val()	
						+ "&googleAreaDisplayName=" + $('#editGoogleAreaDisplayName').val()
						+ "&googleAreaUrl=" + $('#editGoogleAreaUrl').val()
// 						+ "&areaIsActive=" + $('#areaIsActive').val()
 						+ "&googleRedirectUrl=" + $('#editGoogleRedirectAreaUrl').val()
						+ "&description=" + $('#editAreaDescription').val(); 
						$http(
								{
									method : 'POST',
									data : fdata,
									headers : {
										'Content-Type' : 'application/x-www-form-urlencoded'
									},
									url : 'edit-google-area'
								}).then(function successCallback(response) {
									
									Upload.upload({
						                url: 'googleareapicupdate',
						                enableProgress: true, 
						                data: {myFile: file, 'googleAreaId': $('#editGoogleAreaId').val()} 
						            }).then(function (resp) {
						                window.location.reload(); 
						            	
						            });
									
									$scope.getGoogleAreas();
									
									$timeout(function(){
								      	$('#editbtnclose').click();
								        }, 2000);
									
						}, function errorCallback(response) {
							
						});
					}
                        
					
			};
			
            
	    	 $scope.getGoogleLocations = function() {

				var url = "get-area-google-locations";
				$http.get(url).success(function(response) {
				    console.log(response);
					$scope.googleLocations = response.data;
		
				});
			};
			
			

	    	 $scope.getAreaUrls = function() {

				var url = "get-google-url";
				$http.get(url).success(function(response) {
				   
					$scope.areaUrls = response.data;
		
				});
			};
			
            
			$scope.getGoogleArea = function(rowid) {
				var url = "get-google-area?googleAreaId="+rowid;
				$http.get(url).success(function(response) {
				    console.log(response);
					$scope.googleArea = response.data;
		
				});
				
			};
			
			//$scope.showRedirectUrl = false;
			$('#showRedirectUrl').hide();
			$scope.isValidate = false;
			$scope.showRedirectInput = function(val) {
				//alert(val);
				if(val == "true"){
				
				$('#showRedirectUrl').hide();	
				//$('#updateArea').prop('disabled',false);
				}
				else{
				$('#showRedirectUrl').show();
				$scope.isValidate = true;
				}
				
			};
			
			
            			
			$scope.deleteGoogleArea=function(rowid){
				var fdata = "&googleAreaId="+ rowid
				var deleteLocationCheck=confirm("Do you want to delete the area?");
				if(deleteLocationCheck){
					$http(
							{
								method : 'POST',
								data : fdata,
								headers : {
									'Content-Type' : 'application/x-www-form-urlencoded'
								},
								url: 'delete-google-area'
							}).then(function successCallback(response) {
								window.location.reload();
							   }, function errorCallback(response) {
								
								
							});
				}
				else{
					
				}
				
			};
			
			$scope.getAreaCount = function() {

				var url = "get-google-area-count";
				$http.get(url).success(function(response) {
				    //console.log(response);
					$scope.areaCount = response.data;
		            //console.log($scope.accommodationRoomsCount.data[0].size);
				});
			};
			
			
			
			$scope.getGoogleAreas();
			$scope.getGoogleLocations();
			$scope.getAreaCount();
			$scope.getAreaUrls();
			
	}]);


	app.directive('fileInput', ['$parse', function ($parse) {
	    return {
	        restrict: 'A',
	        link: function (scope, element, attributes) {
	            element.bind('change', function () {
	                $parse(attributes.fileInput)
	                .assign(scope,element[0].files[0])
	                scope.$apply()
	            });
	        }
	    };
	}]);		
		   
	
		
	</script>
		<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyDg9dqRsKwRWXGdTKsUNHTHhfQqdSyip9k"></script>
    <script type="text/javascript">
        google.maps.event.addDomListener(window, 'load', function () {
		       var options = {

  componentRestrictions: {country: "IN"}
 };
		
            var places = new google.maps.places.Autocomplete(document.getElementById('googleAreaName'),options);
            google.maps.event.addListener(places, 'place_changed', function () {
                var place = places.getPlace();
                var address = place.formatted_address;
                var latitude = place.geometry.location.lat();
                var longitude = place.geometry.location.lng();
                var placeId = place.place_id;
                /* var mesg = "Address: " + address;
                mesg += "\nLatitude: " + latitude;
                mesg += "\nLongitude: " + longitude; */ 
                //alert(mesg);
                document.getElementById('googleAreaLatitude').value = latitude;
                document.getElementById('googleAreaLongitude').value = longitude;
                document.getElementById('googleAreaPlaceId').value = placeId;
                //alert(document.getElementById('googleAreaLatitude').value);
				console.log(mesg)
            });
        });
    </script> 
    	
	<link rel="stylesheet" href="dist/css/dataTables.bootstrap.css">
	<script src="js1/upload/ng-file-upload-shim.min.js"></script>
	<script src="js1/upload/ng-file-upload.min.js"></script>
	 <script src="js1/jquery.dataTables.min.js" type="text/javascript"></script>
	 <script src="js1/dataTables.bootstrap.min.js" type="text/javascript"></script>
       