<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
  <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
  <head>
   <!-- Google Tag Manager -->
   <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
         new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
         j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
         'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
         })(window,document,'script','dataLayer','GTM-M3RH7N5');
      </script> 
      <!-- End Google Tag Manager -->
    <!-- Required meta tags -->
    <meta charset="utf-8">
    
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
    <link rel="icon" type="image/png" href="contents/images/ulofavicon2.png">
    <meta name="description" content="Ulo Hotels offers you hotel bookings all over South India Book budget hotels in 9+ destinations at affordable rates with amenities like Free Breakfast, AC, LCD TV, Free Wi-Fi, Hygienic Bathrooms & Comfortable Beds from leading budget hotel chain."/>
      <meta name="keywords" content="hotel booking offers, booking hotel, hotels near me, online hotel booking, book hotels"/>
      <meta property="og:site_name" content="ULO Hotels" />
      <meta property="og:description" content=" Ulo Hotels offers you hotel bookings all over South India. Book budget hotels in 9+ destinations at affordable rates with amenities like Free Breakfast, AC, LCD TV, Free Wi-Fi, Hygienic Bathrooms & Comfortable Beds from leading budget hotel chain." />
      <meta property="og:title" content=" Ulo: Hotel Booking, Budget Hotels With Quality 5Bs Services " />
      <meta property="og:publisher" content="https://www.facebook.com/Ulohotels" />
      <meta property="og:url" content=" https://www.ulohotels.com/" />
      <meta content="origin" name="referrer" />
      <meta name="google" content="notranslate">
      <meta http-equiv="Content-Language" content="en">
      <link rel="canonical" href=" https://www.ulohotels.com/">
      <title>Ulo Hotels: India's Budget Hotel Chain </title>
    <!-- Required css -->
    <link rel="stylesheet" href="contents/css/bootstrap.min.css?v=2.0">
    <!-- Template css -->
    <link rel="stylesheet" href="contents/css/style.min.css?v=2.3.8">
    <link rel="stylesheet" href="contents/css/responsive.css?v=2.3.8">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <!-- Date picker -->
    <link rel="stylesheet" href="contents/css/jquery-ui.css?v=2.0">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>   
    <link href="https://fonts.googleapis.com/css?family=Merriweather|Quantico" rel="stylesheet">

  </head>
  <body ng-app="myApp" ng-cloak ng-controller="customersCtrl">

    <!-- Google Tag Manager (noscript) -->
       <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M3RH7N5"
         height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
      <!-- End Google Tag Manager (noscript) -->
    <!-- Middle Header -->
    <style>
  .datepicker{
      display:none;
      z-index:2;
        cursor: pointer; /* Add a pointer on hover */
    position:absolute;
      }
      .mbdatepicker{
      display:none;
           z-index:2;
        cursor: pointer; /* Add a pointer on hover */
    position:absolute;
top:-4000%;
      }
      .dp-highlight .ui-state-default {
      background: #a9e006; 
      color: #FFF;
          text-align:center;
      }
          .dp-highlight {
      background: #a9e006; 
      color: #FFF;
      text-align:center;
      }
      .ui-datepicker.ui-datepicker-multi  {
      width: 50% !important;
      margin: 0px 324px auto;
      display:block;
      }
      .ui-datepicker-multi .ui-datepicker-group {
      float:left;
      }
      #datepicker {
      height: 300px;
      overflow-x: scroll;
      }
      #mbdatepicker {
      height: 300px;
      overflow-x: scroll;
      }
      
      .ui-widget { font-size: 100%; }
    .disabled{
    pointer-events:none;
    }
   
    </style>
    <!-- helping css -->
    <div class="middle-header" id="middle-header">
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-6 col-xs-6 hidden-lg hidden-md">
            <a href="#" class="text-dark " data-toggle="modal" data-target="#menuModal"><img src="contents/images/menu-icon/mburger.png" alt="mobile menu"/></a>
          </div>
          <div class="col-sm-6 col-xs-6 hidden-lg hidden-md">
            <nav class="nav nav-counter">
             <a href="ulouserprofile" ng-show="showMprofile" class="nav-link d-sm-block hidden-md muser"  style="color:#fff;">Hi Welcome</a>
            
              <s:if test="#session.uloUserId != null" >
              <a href="ulouserprofile" class="nav-link d-sm-block hidden-md muser"  style="color:#fff;">Hi Welcome</a>
             </s:if> 
              <s:if test="#session.uloUserId == null" >
              <a href="#" ng-show="showMlogin" class="nav-link d-sm-block hidden-md muser" data-toggle="modal" data-target="#mymobileModal"><img src="contents/images/menu-icon/muser.png" alt="login"/></a>
              </s:if> 
              <a href="tel:180030008686" title="180030008686" class="nav-link  d-sm-block hidden-md mcall"><img src="contents/images/menu-icon/mcall.png" alt="ulo contact number"/></a>
            </nav>
          </div>
          <div class="col-lg-4 col-md-4  col-sm-4 hidden-xs">
            <a href="index" class="logo">
            <img src="contents/images/ulologo.svg" alt="ulohotels" class="logo">
            </a>
          </div>
          <div class="col-lg-8 col-md-8 col-sm-8 hidden-xs">
            <nav class="nav nav-counter">
              <a href="corporate" class="nav-link d-none d-lg-block d-md-block">Corporate Enquiry</a>
              <a href="#" class="nav-link d-none d-lg-block hide">Day Stay</a>
              <a href="#" class="nav-link d-none d-lg-block hide">Share & Earn</a>
               <a href="ulouserprofile" ng-show="showDprofile" class="nav-link d-none d-lg-block d-md-block dlogin"  ><img src="contents/images/menu-icon/duser.png" alt="login"/> Hi ! Welcome Back</a>
     <s:if test="#session.uloUserId == null" > 
              <a href="#" class="nav-link d-none d-lg-block d-md-block dlogin" ng-show="showDlogin" data-toggle="modal" data-target="#myModal"><img src="contents/images/menu-icon/duser.png" alt="login"/> Log In</a>
                   
                       </s:if>  
               <s:if test="#session.uloUserId != null" > 
              
                   <a href="ulouserprofile" class="nav-link d-none d-lg-block d-md-block dlogin"  ><img src="contents/images/menu-icon/duser.png" alt="login"/> Hi ! <%=session.getAttribute("uloUserName")%></a>
              
                       </s:if>  
              <a href="#" class="nav-link d-none d-lg-block d-md-block dcall"><img src="contents/images/menu-icon/dcalls.svg" alt="ulo contact number"/> 1800 3000 8686</a>
            </nav>
          </div>
        </div>
      </div>
    </div>
    <!-- /Header -->
    <!-- banner begins -->
    <div class="banner">
      <img src="contents/images/banner/banner1.jpg" alt="Welcome to ulo hotels" class="mbbanner">
      <!--  home search bar -->
      <div class="container ">
        <div class="desktopsearch">
          <div class="row ">
           <div class="col-lg-12 col-md-12 col-sm-12 hidden-xs bcontest ">
        </div>
            <div class="col-lg-12 col-md-12 col-sm-12 ">
              <h2>India's Premium Budget Hotel Chain</h2>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 Dhsearch">
            <form>
              	 <!-- <div class="demo"> --> 
	 
<div class="col-lg-4 col-md-4 col-sm-4 dlistsearch">
<img src="contents/images/menu-icon/flag2.png" alt="seacrh" class="dhflag"/>
<angucomplete-alt id="ex1" ng-required="true" placeholder="Search by city ,location" selected-object="locationurl" local-data="filterLocation" search-fields="displayLocationName"  ng-click="testbox()" title-field="displayLocationName" minlength="1" class="form-control"  match-class="highlight" field-required="true"/>
</div>
<div class="col-lg-1 col-md-1 col-sm-1  ">
                <div class="whitebgm">
                  <img src="contents/images/menu-icon/date2.png" alt="seacrh" class="dhsep"/>
                </div>
              </div>
              <div class="col-lg-2 col-md-2 col-sm-2">
                <input type="text" class="form-control" placeholder="21 July Sunday " onkeypress="return false;" id="datepicker0" readonly>
                <input type="hidden" class="form-control" placeholder="Check-in"  id="input1">
                <input type="hidden" class="form-control" placeholder="Check-in"  id="alternate">
              </div>
              <div class="col-lg-1 col-md-1 col-sm-1 dwhitebgmnorad ">
                <img src="contents/images/menu-icon/sep2.png" alt="seacrh" class="dhsep"/>
              </div>
              <div class="col-lg-2 col-md-2 col-sm-2 ">
                <input type="text" class="form-control" placeholder="Checkout " onkeypress="return false;" id="datepicker1" readonly>
                <input type="hidden" class="form-control" placeholder="Checkout" id="input2" onkeypress="return false;" readonly="readonly" >
                <input type="hidden" class="form-control" placeholder="Checkout" id="alternate1" onkeypress="return false;" readonly="readonly" >
              </div>
              <div class="col-lg-2 col-md-2 col-sm-2 zeropadding">
              <a ng-href="{{locationUrl}}?checkIn={{StartDate}}&checkOut={{EndDate}}"  name="checkAvail" ng-click="getChangeValues()"  class="btn deskbtn disabled" id="checkAvail" >Search</a>
                <!-- <button type="submit" class="btn deskbtn">Search</button> -->
              </div>
              </form>
            </div>
            <!-- datepicker css -->
            <div class="col-lg-12 col-md-12">
              <div class="desktopdate">
                <div class="datepicker"></div>
              </div>
            </div>
            <!-- datepicker div ends -->
          </div>
        </div>
        <!-- mobile logo  begins-->
        <div class="mobilehomelogo hidden-lg hidden-md">
          <div class="container">
            <div class="row">
              <img src="contents/images/ulologo.svg" alt="ulo hotels mobile logo" height="50px">
              <h5>India's Premium Budget Hotel Chain</h5>
            </div>
          </div>
        </div>
        <!-- mobile logo ends-->
      </div>
      <!--  home search bar -->
    </div>
    <!-- mobile search bar begins -->
    <div class="mobilesearchbackground">
      <div class="container hidden-lg hidden-md hidden-sm">
        <div class="mobilehomesearch">
          <div class="row">
            <div class="col-sm-12 col-xs-12 mbsearchborder">
           <div class="form-group">
              <img src="contents/images/menu-icon/flag.png" alt="icon"  >
              <angucomplete-alt id="ex1" ng-required="true" placeholder="Search by city ,location" selected-object="locationurl" local-data="filterLocation" search-fields="displayLocationName" ng-click="testbox();" title-field="displayLocationName" minlength="1" class="form-control"  match-class="highlight" field-required="true"/>
            
           </div>
           </div>
            <div class="col-sm-1 col-xs-1">
              <img src="contents/images/menu-icon/date.png" alt="checkin" class="mbsdate">
            </div>
            <div class="col-sm-4 col-xs-4">
              <label class="mbcheckinlable">Check In</label>
              <input type="text" class="form-control mbcheckin" id="mdatepicker0" onkeypress="return false;" readonly="readonly"  > 
              <input type="hidden" class="form-control mbcheckin" placeholder="Check-in"  id="malternate"> 
              <input type="hidden" class="form-control mbcheckin" placeholder="Check-in"  id="minput1"> 
            </div>
            <div class="col-sm-1 col-xs-1">
              <img src="contents/images/menu-icon/sep.png" alt="checkout" class="mbsdate" >
            </div>
            <div class="col-sm-4 col-xs-4">
              <label class="mbcheckinlable">Check Out</label>
              <input type="text" class="form-control mbcheckout"  id="mdatepicker1" onkeypress="return false;" readonly="readonly" >
              <input type="hidden" class="form-control" placeholder="Checkout" id="malternate1" onkeypress="return false;" readonly="readonly" >
              <input type="hidden" class="form-control" placeholder="Checkout" id="minput2" onkeypress="return false;" readonly="readonly" >
            </div>
            <div class="col-sm-12 col-xs-12">
               <a ng-href="{{locationUrl}}?checkIn={{mbStartDate}}&checkOut={{mbEndDate}}"  name="checkAvail" ng-click="getMbChangeValues()" class="btn mbhomebtn ripple disabled" id="checkAvailMb" >Search</a>
            </div>
           
          </div>
           <!-- mobile date picker -->
            <div class="col-sm-12 col-12 ">
              <div class="mbdatepicker"></div>
            </div>
            <!--  mobile date picker -->
        </div>
      </div>
      <!-- coupon code for mobile begins -->
      <div class="container">
        <div class="row mobilehomecoupon hidden-lg hidden-md">
          <div class="col-lg-12 col-md-12 col-sm-12 col-12">
            <h4>GRAB YOUR DEALS <span class="hcicon"><i class="fas fa-long-arrow-alt-down"></i></span></h4>
          </div>
          <!-- coupon images -->
          <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-12" ng-repeat="pm in promotionImages" >
              <a href="{{pm.photoPathUrl}}"><img ng-src="get-promotion-image-stream?promotionImageId={{pm.DT_RowId}}" class="img-responsive" ></a>
            </div>
          </div>
          <!-- coupon images -->
        </div>
      </div>
      <!-- coupon code for mobile ends -->
    </div>
    <!-- mobile search bar ends -->	
    <!-- banner location begins -->
    <div class="bannerlocation" id="banloc">
    <div class="container">
    <div class="row">
    <div class="col-lg-12 col-md-12 hidden-xs hidden-sm" >
    <a ng-repeat="l in location" href="{{l.locationUrl}}" ><button class="draw" >{{l.locationName}}</button></a>
    </div>
       <div class="hidden-lg hidden-md col-sm-12 col-xs-12" >
    <a ng-repeat="l in location | limitTo:8" href="{{l.locationUrl}}" ><button class="drawlocation" >{{l.locationName}}</button></a>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <button id="explorebtndiv" class="explorebtndiv hidden-md hidden-lg" onclick="showExplore()">Explore <i class="fas fa-long-arrow-alt-down"></i></button>
    </div>
    </div>
    </div>
    </div>
    <!-- banner location ends -->  
    <!-- Modal Menu -->
    <!-- banner ends -->
    <!--   home coupon begins -->
    <div class="homecoupon " id="banloctwo">
      <div class="container">
        <div class="row hidden-xs">
          <div class="col-lg-12 col-md-12 col-sm-12 hidden-xs">
            <h4>GRAB YOUR DEALS <span class="hcicon"><i class="fas fa-long-arrow-alt-down"></i></span></h4>
          </div>
          <!-- coupon images -->
          <div class="row" >
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" ng-repeat="pm in promotionImages">
              <a href="{{pm.photoPathUrl}}"><img ng-src="{{pm.imagePath}}?w=293&h=141&fit=crop&auto=compress"></a>
            </div>
          </div>
          <!-- coupon images -->
        </div>
      </div>
    </div>
    <!--   home coupon ends -->
    
    <!-- home page 5B's begins -->
    <section id="exploreAll">
    <div class="fivebs">
    <div class="container">
    <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <h4>OUR 5B SERVICE </h4>
    </div>
    <!-- five bs images -->
    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4">
    <img src="contents/images/fivebs/gs100.png" alt="">
    <h6>Budget Friendly & Value</h6>
    </div>
    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-4">
    <img src="contents/images/fivebs/gs101.png" alt="Brakfast Healthy & Regional">
    <h6>Breakfast Healthy & Regional</h6>
    </div>
    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-4">
    <img src="contents/images/fivebs/gs102.png" alt="Bed Clean & Comfort">
    <h6>Bed Clean & Comfort</h6>
    </div>
    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6">
    <img src="contents/images/fivebs/gs104.png" alt="Best Guest Service & Care">
    <h6>Best Guest Service & Care</h6>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
    <img src="contents/images/fivebs/gs103.png" alt="Bathroom Hygienic & Functional">
    <h6>Bathroom Hygienic & Functional</h6>
    </div>
    <!-- five bs images -->
    </div>
    </div>
    </div>
    <!-- home page 5B's ends -->
    <!-- home page ulo exclusive -->
    <div class="homeexclusive">
    <div class="container">
    <div class="row p-3">
    <div class="col-lg-12 col-md-12 col-sm-12 col-12">
    <h4>ulo's exclusive</h4>
    <!-- <div class="starheading">
      <img src="images/crown/star1.png">
      </div> -->
    </div>
    <!-- ulo crwon images -->
    <div class="col-lg-4 col-md-4 col-sm-12 col-12">
    <img src="contents/images/crown/ulobstayhome.png" alt="ulo crown" class="crownimage"> 
    <h4 class="crownname"></h4>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-12 col-12">
    <h5><img src="contents/images/crown/ring.png" class="queencount"> Pocket Friendly</h5>
    <h5><img src="contents/images/crown/ring.png" class="queencount"> Premium quality services</h5>
    <h5><img src="contents/images/crown/ring.png" class="queencount"> Full refund for disqualified stay</h5>
    <h5><img src="contents/images/crown/ring.png" class="queencount"> Top-notch amenities</h5>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-12 col-12">
    <h5><img src="contents/images/crown/ring.png" class="queencount"> Flexible room</h5>
    <h5><img src="contents/images/crown/ring.png" class="queencount"> Easy cancellation</h5>
    <h5><img src="contents/images/crown/ring.png" class="queencount"> Spacious room</h5>
    <button class="btn explorebtn">EXPLORE</button>
    </div>
    <!-- ulo crwon images -->
    </div>
    </div>
    </div>
    <!-- home page ulo exclusive -->
    <!-- home top deatination begins -->
    <div class="hometopcity">
    <div class="container-fluid">
    <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-10 col-10">
    <h4>DESTINATIONS TO CHOOSE FROM </h4>

    </div>
    </div>
    <!-- mobile scroll images -->
    <div class="homecityscroll">
    <!-- top city images -->
    <div class="row hidden-sm hidden-xs">
    <div class="col-lg-1 col-md-1 col-sm-2 col-xs-1">
    <img src="contents/images/crown/dest.png" class="floatbannerleft " alt="ulo hotels">
    </div>
    <div class="col-lg-2 col-md-2 "  ng-repeat="l in location | orderBy:'locationName' | limitTo:5 ">
    <div class="locationcontainer">
    <img src="get-location-picture?locationId={{l.DT_RowId}}" alt="{{l.locationName}}" class="roundcity img-circle" >
    <div class="middle">
    <div class="text"><a href="{{l.locationUrl}}">View Hotels</a></div>
    </div>
    <h6>{{l.locationName}}</h6>
    </div>
    </div>

    <div class="col-lg-1 col-md-1 col-sm-2 col-xs-1">
    <img src="contents/images/crown/dest2.png" class="floatbannerright" alt="ulo hotels logo">
    </div>
    </div>
    <!-- top city images -->
        <!-- mobile scroll images -->
    <div class="row">
    	    <ul class="hidden-lg hidden-md" >
    <li ng-repeat="l in location | limitTo:5 ">
     <img src="get-location-picture?locationId={{l.DT_RowId}}" alt="{{l.locationName}}" class="roundcity img-circle" >
    <h6>{{l.locationName}}</h6>
    </li>
    </ul>
    </div>
    <!-- mobile scroll ends -->
    </div>

    </div>
    </div>
    <!-- home top deatination ends -->
    <!-- Home pagre ulo cities -->
    <div class="desktopulocities">
    <div class="container">
    <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <h4>Quick Links</h4>
    </div>
    <!-- citites name begins -->
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 homeurlbox">
    <!-- <a ng-repeat="l in filterLocation | filter : locationName" > <li  href="{{l.locationUrl}}">{{l.locationName}}</li></a> -->
    <div >
    <a href="hotels-in-omr-chennai" class="homeareaurl">Hotels In OMR | </a>
    <a href="hotels-in-vadapalani-chennai" class="homeareaurl">Hotels In Vadapalani | </a>
    <a href="hotels-in-t-nagar-chennai" class="homeareaurl">Hotels In T-Nagar | </a>
    <a href="hotels-near-chennai-central" class="homeareaurl">Hotels Near Chennai Central | </a>
    <a href="hotels-in-arumbakkam-chennai" class="homeareaurl">Hotels In Arumbakkam | </a>
    <a href="hotels-in-koyambedu-chennai" class="homeareaurl">Hotels In Koyambedu |</a>
    <a href="hotels-in-chrompet-chennai" class="homeareaurl">Hotels In Chrompet | </a>
    <a href="hotels-in-thoraipakkam-chennai" class="homeareaurl">Hotels In Thoraipakkam | </a>
    <a href="resorts-in-ecr-chennai" class="homeareaurl">Resorts In ECR | </a>
    <a href="hotels-in-peelamedu-coimbatore" class="homeareaurl">Hotels In Peelamedu </a>
    </div>
    </div>
    <!-- citties nsame ends -->
    </div>
    </div>
    </div>
    <!-- Home pagre ulo cities -->
    <!-- home page footer BEGINS -->
    <div class="desktopfooter hidden-sm hidden-xs">
    <div class="container">
    <div class="row p-3">
    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
    <h5>About ulo</h5>
    <a href="aboutulo" class="footurl">About Ulo</a>
    <a href="careers-ulo" class="footurl">Careers</a>
    <a href="blog" class="footurl">Blog</a>
    <a href="contactus" class="footurl">Contact</a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
    <h5>Business Info</h5>
    <a href="corporate" class="footurl">Corporate enquiry</a>
    <a href="partner-with-us" class="footurl">Grow with us</a>
      <a href="guest-policy" class="footurl">Cancellation</a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
    <h5>Policies</h5>
    <a href="termsandconditions" class="footurl">Terms and conditions</a>
    <a href="privacy" class="footurl">Privacy Policy</a>
    <a href="guest-policy" class="footurl">Guest Policy</a>
  
    <a href="ulo-faq" class="footurl">FAQ</a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
    <a href="partner-with-us"><div class="footpartner">
    <p>We are working tirelessly to mark our brand presence all over India and we are ambitious to work with franchise partners who share our zeal for premium guest service. If you want to join our hands in this epic journey and get amazing business opportunity.</p>
    <h4>PARTNER WITH US</h4>
    
    </div>
    </a>
    </div>
    <!-- secure payment begins -->
    <div class="col-lg-12 col-md-12 col-sm-12 col-12 d-none d-lg-block d-md-block p-3">
    <p class="securepayment">Secure Payment Partner <img src="contents/images/payment/rp.svg" class="moveimage"> <p>
    </div>
    <!-- secure payment ends-->
    </div>
    </div>
    </div>
    <!-- home page footer ENDS --->
    <!-- ulo hotel copy right -->
    <div class="copyrights">
    <div class="container">
    <div class="row">
    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 mbcpline p-3">
    <p>Copyrights Ulo Hotels Private Limited &copy; 2018 All Rights Reserved</p>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 desksocialbox">
       <a href="https://www.pinterest.com/ulohotels/" class="sociallink"><i class="fab fa-pinterest fa-x"></i></a>
     <a href="https://www.instagram.com/ulohotels/" class="sociallink"><i class="fab fa-instagram fa-x"></i></a>
      <a href="https://plus.google.com/+ULOHOTELS" class="sociallink"><i class="fab fa-google-plus fa-x"></i></a>
        <a href="https://twitter.com/ulohotels" class="sociallink"><i class="fab fa-twitter fa-x"></i></a>
     <a href="https://www.facebook.com/Ulohotels/" class="sociallink"><i class="fab fa-facebook-square fa-x"></i></a>
    </div>
    </div>
    </div>
    </div>
    <!-- mobile copyright footer -->
    <div class=" hidden-lg hidden-md">
    <div class="container ">
    <div class="row mobilesocial">
    <div class="col-lg-3 col-md-3 col-sm-12 col-12">
    <a href="https://www.facebook.com/Ulohotels/" class="sociallink"><i class="fab fa-facebook-square fa-x"></i></a>
    <a href="https://twitter.com/ulohotels" class="sociallink"><i class="fab fa-twitter fa-x"></i></a>
    <a href="https://plus.google.com/+ULOHOTELS" class="sociallink"><i class="fab fa-google-plus fa-x"></i></a>
    <a href="https://www.instagram.com/ulohotels/" class="sociallink"><i class="fab fa-instagram fa-x"></i></a>
    <a href="https://www.pinterest.com/ulohotels/" class="sociallink"><i class="fab fa-pinterest fa-x"></i></a>
    </div>
    </div>
    </div>
    </div>
    <div class="container mobilecopy hidden-lg hidden-md">
    <div class="row ">
    <div class="col-lg-9 col-md-9 col-sm-12 col-12 mbcpline">
    <p>Copyrights Ulo Hotels Private Limited &copy; 2018 All Rights Reserved</p>
    </div>
    </div>
    </div>
    </section>
    <!-- mobile copyright footer -->
    <!-- ulo hotels copy right -->
    <div class="modal fade modal-menu msidemenu" id="menuModal" tabindex="-1" role="dialog" aria-labelledby="menuModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
           <!--  <a class="mcallicon list-group-item list-group-item-action" href="#" data-target="#mymobileModal" data-dismiss="modal" data-toggle="collapse" aria-expanded="false" aria-controls="list-submenu-1">
            <img src="contents/images/menu-icon/two/log.png"> Login / Signup
            </a> -->
            <a class="mcallicon list-group-item list-group-item-action" href="#" data-toggle="collapse" aria-expanded="false" aria-controls="list-submenu-1">
             <img src="contents/images/menu-icon/two/call.png"> Call Us
            </a>
            <a class="list-group-item list-group-item-action" href="corporate" data-toggle="collapse" aria-expanded="false" aria-controls="list-submenu-1">
           <img src="contents/images/menu-icon/two/corporate.png">  Corporate Enquiry
            </a>
            <a class="list-group-item list-group-item-action hide" href="#" data-toggle="collapse" aria-expanded="false" aria-controls="list-submenu-1">
            <img src="contents/images/menu-icon/two/cancel.png"> Cancellation
            </a>
            <a class="list-group-item list-group-item-action hide" href="#" data-toggle="collapse" aria-expanded="false" aria-controls="list-submenu-1">
             <img src="contents/images/menu-icon/two/share.png"> Share & Earn
            </a>
            <a class="list-group-item list-group-item-action" href="partner-with-us" data-toggle="collapse" aria-expanded="false" aria-controls="list-submenu-1">
             <img src="contents/images/menu-icon/two/grow.png"> Grow With Us
            </a>
            <a class="list-group-item list-group-item-action" href="aboutulo" data-toggle="collapse" aria-expanded="false" aria-controls="list-submenu-1">
             <img src="contents/images/menu-icon/two/about.png"> About Us
            </a>
          </div>
        </div>
      </div>
    </div>
    <!-- Modal Menu -->
    <!-- ulohotels signup form  begins -->
    <!-- Modal -->
    <div id="myModal" class="modal fade newsignup" role="dialog" >
      <div class="modal-dialog modal-md ">
  
        <!-- Modal content-->
        <div class="modal-content desktoplogin ">
   
          <div class="modal-body">
            <!-- row begins -->
            <div class="row">
              <div class="col-md-5 leftlogin">
                <div class="leftloginheader">
                  <h4>Sign up / Login</h4>
                  <span class="logintag">To Your Account & Get Ulo Rewards & Earns </span>
                </div>
                <div class="row spllogin">
                  <div class="col-md-4 ">
                    <span class="imagebgs"><img src="contents/images/menu-icon/manage.png" class="img-responsive"></span>
                  </div>
                  <div class="col-md-8  ">
                    <span class="loginimagetag">Manage Your Bookings Anywhere Anytime</span>
                  </div>
                </div>
                <div class="row spllogin">
                  <div class="col-md-4 ">
                    <span class="imagebgs"><img src="contents/images/menu-icon/share.png" class="img-responsive"></span>
                  </div>
                  <div class="col-md-8 ">
                    <span class="loginimagetag">Share & Earn</span>
                  </div>
                </div>
                <div class="row spllogin">
                  <div class="col-md-4 ">
                    <span class="imagebgs"><img src="contents/images/menu-icon/cancel.png" class="img-responsive"></span>
                  </div>
                  <div class="col-md-8 ">
                    <span class="loginimagetag">Easy Cancellation & Refunds</span>
                  </div>
                </div>
              </div>
              <div class="col-md-7 rightlogin">
             
                <form name="dlogin" id="login">
                 <div ng-repeat="rd in rewardDetails" ng-if="rd.rewardDetailId != 'None'"> 
          <input type="hidden" id="rewardDetailId" value="{{rd.rewardDetailId}}" />
          </div>
                 
                 <div class="form-group">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <input type="text" class="form-control signupuser" ng-model="userName" id="userName" placeholder="Name" ng-required="true">
                  </div>
                  <div class="form-group">
                    <input type="email" class="form-control signupuser" ng-model="emailId" id="emailId" placeholder="Email" ng-required="true">
                  </div>
                  <div class="form-group">
                    <input type="text" class="form-control signupuser" ng-model="mobilePhone" id="mobilePhone" placeholder="Mobile" ng-required="true">
                  </div>
                  <div class="form-group">
                  <div id="signuperrors"></div>
                  <div id="signuperror"></div>
                 
                  </div>
                  <div class="form-group">
                      <input type="hidden" value="4" id="roleId" name="roleId">
                      <input type="hidden" value="2" id ="accessRightsId"  name="accessRightsId">
                    <button type="submit"  ng-click="dlogin.$valid && signup()" class="btn btn-primary signupbtn">{{dsignin}}</button>
                  </div>
                </form>
                <!--  social login buttons -->
                <div class="row">
                  <div class="col-md-12">
                    <span class="signupor">Or</span>
                  </div>
                  <div class="col-md-6">
                    <a><button class="signupfb" ng-click = "facebookLogin();">FACEBOOK</button></a>
                  </div>
                  <div class="col-md-6">
                    <a><button class="signupgplus"  ng-click = "googleLogin();"><img src="contents/images/menu-icon/gplus1.png" alt="google login" clss="img-responsive" height="20px" width="20px"> Google</button></a>
                  </div>
                    <div class="col-md-12">
                    <span class="creditterms">* Credits are not applicable for social login</span>
                  </div>
                </div>
                <!-- social login button -->
                <!-- already login begins -->
                <div class="row">
                  <div class="col-md-12">
                    <span class="signupolduser">Already Have a Account? <a href="javascript:void(0)" data-dismiss="modal"  data-toggle="modal" data-target="#loginModal">LOGIN HERE</a></span>
                  </div>
                </div>
                <!-- already login ends -->
              </div>
            </div>
            <!--   row ends -->
          </div>
        </div>
      </div>
    </div>
    <!-- ulohotels signup form ends -->
    <!-- Ulohotels login form begins -->
    <!-- Modal -->
    <div id="loginModal" class="modal fade newsignup" role="dialog" >
      <div class="modal-dialog modal-md ">
        <!-- Modal content-->
        <div class="modal-content desktoplogin ">
          <div class="modal-body">
            <!-- row begins -->
            <div class="row">
              <div class="col-md-5 leftlogin">
                <div class="leftloginheader">
                  <h4>Sign up / Login</h4>
                  <span class="logintag">To Your Account & Get Ulo Rewards & Earns</span>
                </div>
                <div class="row spllogin">
                  <div class="col-md-4 ">
                    <span class="imagebgs"><img src="contents/images/menu-icon/manage.png" class="img-responsive"></span>
                  </div>
                  <div class="col-md-8  ">
                    <span class="loginimagetag">Manage Your Bookings Anywhere Anytime</span>
                  </div>
                </div>
                <div class="row spllogin">
                  <div class="col-md-4 ">
                    <span class="imagebgs"><img src="contents/images/menu-icon/share.png" class="img-responsive"></span>
                  </div>
                  <div class="col-md-8 ">
                    <span class="loginimagetag">Share & Earn</span>
                  </div>
                </div>
                <div class="row spllogin">
                  <div class="col-md-4 ">
                    <span class="imagebgs"><img src="contents/images/menu-icon/cancel.png" class="img-responsive"></span>
                  </div>
                  <div class="col-md-8 ">
                    <span class="loginimagetag">Easy Cancellation & Refunds</span>
                  </div>
                </div>
              </div>
              <div class="col-md-7 rightsignin">
               <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <div class="userinfo ">
                  <form name="userinfoform">
                    <div class="form-group">
                      <input type="text" class="form-control signupuser" id="loginUserName" ng-model="loginUserName" placeholder="MOBILE NO / EMAIL"   ng-required="true">
                    </div>
                    <div class="form-group">
                    <div id="loginerror"></div>
                    </div>
                    <div class="form-group">
                      <button type="submit" class="btn btn-primary signupbtn"  ng-click="userinfoform.$valid && sendUloLoginOtp();">Submit</button>
                    </div>
                    
                    <div id="otpsendmsg"></div>
                    <div class="form-group">
                        <span class="signupolduser">New To Ulo Hotels , Just <a href="javascript:void(0)" data-dismiss="modal"  data-toggle="modal" data-target="#myModal">Signup Here</a></span>
                 
                    </div>
                  </form>
                </div>
                <div class="userotp" >
                  <form name="userotp">                  
                  <input type="hidden" id="uloLoginId" value="{{ulouserId}}"/> 
                  <input type="hidden" id="uloLoginName" value="{{username}}"/>
                    <div class="form-group">
                      <span class="usernote">A One Time Password has been sent via SMS To <a class="usernumber" ></a>{{username}}</span>
                    </div>
                    <div class="form-group">
                      <input type="text" class="form-control signupuser" id="loginOtp" ng-model="loginOtp" placeholder="Enter your OTP" ng-required="true">
                    </div>
                    <div class="form-group">
                      <button type="submit" class="btn btn-primary signupbtn" ng-click="userotp.$valid && verifyUloLoginOtp();" >Submit</button>
                    </div>
                    <div class="form-group">
                      <span class="usernotetwo">Haven't received the code yet ? <a class="userotpagain" ng-click="resendUloLoginOtp()">Resend OTP</a></span>
                    </div>                    
                  </form>
                </div>
              </div>
            </div>
            <!--   row ends -->
          </div>
        </div>
      </div>
    </div>
    <!-- ulohotels login form ends -->
    <!-- mobile login forms begins -->
    <!-- ulohotels mobile signup form  begins -->
    <!-- Modal -->
    <div id="mymobileModal" class="modal fade" role="dialog" >
      <div class="modal-dialog modal-xs  ">
        <!-- Modal content-->
        <div class="modal-content mobilesignup ">
         <div ng-repeat="rd in rewardDetails" ng-if="rd.rewardDetailId != 'None'"> 
          <input type="hidden" id="mbRewardDetailId" value="{{rd.rewardDetailId}}" />
          </div>
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <img src="contents/images/ulologo.svg" alt="ulo hotels mobile logo" height="50px" class="logo">
            <span class="logotext">Signup To Your Account<span>
          </div>
          <div class="modal-body">
            <!-- row begins -->
            <div class="row">
              <div class="col-xs-12">
                <form name="mobilesignupform" id="mobilesignup">
                  <div class="form-group">
                    <input type="text" class="form-control signupuser" ng-model="mbUserName" id="mbUserName"  placeholder="Name" ng-required="true">
                  </div>
                  <div class="form-group">
                    <input type="email" class="form-control signupuser"  ng-model="mbEmailId"   id="mbEmailId" placeholder="Email" ng-required="true">
                  </div>
                  <div class="form-group">
                    <input type="text" class="form-control signupuser" ng-model="mbMobilePhone" id="mbMobilePhone"  placeholder="Mobile" ng-required="true">
                  </div>
                  <div class="form-group">
                  <div id="msignuperrors"></div>
                  <div id="msignuperror"></div>
                 
                  </div>
                  <div class="form-group">
                  <input type="hidden" value="4" id="mbRoleId" name="roleId">
                      <input type="hidden" value="2" id ="mbAccessRightsId"  name="accessRightsId">
                    <button type="submit" class="btn btn-primary signupbtn" ng-click="mobilesignupform.$valid && mbsignup()">{{msignin}}</button>
                  </div>
                </form>
              </div>
              <div class="row">
                <div class="col-xs-12">
                  <span class="signupor">Or</span>
                </div>
                <div class="col-xs-6">
                  <a><button class="signupfb" ng-click = "facebookLoginMobile();">FACEBOOK</button></a>
                </div>
                <div class="col-xs-6">
                  <a><button class="signupgplus" ng-click = "googleLoginMobile();"><img src="contents/images/menu-icon/gplus1.png" alt="google login" clss="img-responsive" height="20px" width="20px"> Google</button></a>
                </div>
                <div class="col-xs-12">
                  <span class="signupolduser">Already Have a Account? <a href="javascript:void(0)" data-dismiss="modal"  data-toggle="modal" data-target="#loginmobileModal">LOGIN HERE</a></span>
                </div>
                 <div class="col-xs-12">
                    <span class="creditterms">* Credits are not applicable for social login</span>
                  </div>
              </div>
            </div>
            <!--   row ends -->
          </div>
        </div>
      </div>
    </div>
    <!-- ulohotels mobile signup form ends -->
    <!-- Ulohotels login form begins -->
    <!-- Modal -->
    <div id="loginmobileModal" class="modal fade newsignup" role="dialog" >
      <div class="modal-dialog modal-xs  ">
        <!-- Modal content-->
        <div class="modal-content mobilesignup ">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <img src="contents/images/ulologo.svg" alt="ulo hotels mobile logo" height="50px" class="logo">
          </div>
          <div class="modal-body">
            <!-- row begins -->
            <div class="row mobileuserinfo">
              <div class="col-xs-12">
                <form name="mobilesignup">
                  <div class="form-group">
                    <input type="text" class="form-control signupuser" id="mbLoginUserName" ng-model="mbLoginUserName" placeholder="Mobile Number / Email" ng-required="true">
                  </div>
                   <div class="form-group">
                    <div id="loginerrormb"></div>
                    </div>
                  <div class="form-group">
                    <button type="submit" class="btn btn-primary signupbtn" ng-click="mobilesignup.$valid  && mbSendUloLoginOtp()">Submit</button>
                  </div>
                    <div class="form-group">
                        <span class="signupolduser">New To Ulo Hotels , Just <a href="javascript:void(0)" data-dismiss="modal"  data-toggle="modal" data-target="#mymobileModal">Signup Here</a></span>
                 
                    </div>
                </form>
              </div>
            </div>
            <div class="row mobileuserotp">
              <div class="col-xs-12">
                <form name="muserotp">
                <input type="hidden" id="mbUloLoginId" value="{{mbulouserId}}"/>
                  <input type="hidden" id="mbUloLoginName" value="{{mbusername}}"/>
                  <div class="form-group">
                    <span class="usernote">A One Time Password has been sent via SMS To <a class="usernumber">{{mbusername}}</a></span>
                  </div>
                  <div class="form-group">
                    <input type="text" class="form-control signupuser" id="mbLoginOtp" ng-model="mbLoginOtp" placeholder="Enter your OTP" ng-required="true">
                  </div>
                  <div class="form-group">
                    <button type="submit" class="btn btn-primary signupbtn" ng-click="muserotp.$valid && mbVerifyUloLoginOtp();" >Submit</button>
                  </div>
                  <div class="form-group">
                    <span class="usernotetwo">Haven't received the code yet ? <a class="userotpagain" ng-click="mbResendUloLoginOtp()">Resend OTP</a></span>
                  </div>
                </form>
              </div>
            </div>
            <!--   row ends -->
          </div>
        </div>
      </div>
    </div>
    <!-- ulohotels login form ends -->
    <!-- mobile login form ends --> 
    <!-- Required js -->
    <script src="contents/js/jquery.min.js"></script>
    <script src="contents/js/bootstrap.min.js"></script>
    <script src="contents/js/jquery-ui.js"></script>
    <script type="text/javascript" src="contents/js/ulo.js?v=2.1.2"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
    <script type="text/javascript" src="https://apis.google.com/js/api.js" ng-init="handleGoogleApiLibrary()" ></script>
    <script type="text/javascript"  type="text/javascript"src="contents/js/ngprogress.js"></script>
    <script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angucomplete-alt/2.4.1/angucomplete-alt.min.js"></script> 
    <script>
      var app = angular.module('myApp', ['ngProgress','angucomplete-alt']);
          app.controller('customersCtrl', function($scope, $http, $timeout, ngProgressFactory) {
        	  $scope.testbox = function(){
        		 $("#checkAvail").removeClass("disabled");
        		 $("#checkAvailMb").removeClass("disabled");
        		 
        		 
        	  };	
        	  /* Raja */
        	  /* login and signup start here */
      		$scope.progressbar = ngProgressFactory.createInstance();
   $scope.handleGoogleApiLibrary = function() {
          gapi.load('client:auth2', {
              callback: function() {
                  gapi.client.init({
                      apiKey: 'AIzaSyAuJwG_DuX3eEXTgsoEY9A8LRvRVfeilFs',
                      clientId: '106759720549-tfe9i1b16ovvpqn8aho6hooiaftt2h4n.apps.googleusercontent.com',
                      scope: 'https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/plus.me'
                  }).then(
                      // On success
                      function(success) {
                          $("#login-button").removeAttr('disabled');
                      },
                      // On error
                      function(error) {
                      }
                  );
              },
              onerror: function() {
                  // Failed to load libraries
              }
          });
      };
      $scope.googleLogin = function() {
    	  
    	  $scope.progressbar.start();
    	  $('#myModal').modal('hide');
	      //$('#mymobileModal').modal('hide');
          gapi.auth2.getAuthInstance().signIn().then(
              function(success) {
                  gapi.client.request({
                      path: 'https://www.googleapis.com/plus/v1/people/me'
                  }).then(
                      function(success) {
                          var user_info = JSON.parse(success.body);
                          var username = user_info.displayName;
                          var email = user_info.emails[0].value;
                          var phone = null;
                  
                          var fdata = "username=" + user_info.displayName +
                              "&emailid=" + user_info.emails[0].value +
                              "&userid=" + user_info.id;
                          $http({
                              method: 'POST',
                              data: fdata,
                              headers: {
                                  'Content-Type': 'application/x-www-form-urlencoded'
                              },
                              url: 'sociallogin'
                          }).then(function successCallback(response) {
                        	  
                        	  $scope.sociallogin = response.data;
                        
                         	   
                        	  $scope.showDprofile = true;
                              $scope.showDlogin = false;
                              $scope.dofferlogin = false;
                              $scope.showreward = false;
                              $scope.username = $scope.sociallogin.data[0].uloUserName;
                              $('#myModal').modal('hide');
                   	          //$('#mymobileModal').modal('toggle');
                              $scope.progressbar.complete();
                          }, function errorCallback(response) {

                          });
                      },
                      // On error
                      function(error) {
                          $("#login-button").removeAttr('disabled');
                          alert('Error : Failed to get user user information');
                      }
                  );
              },
              // On error
              function(error) {
                  $("#login-button").removeAttr('disabled');
                  $('#myModal').modal('toggle');
       	          //$('#mymobileModal').modal('toggle');
                  $scope.progressbar.complete();
              }
          );
      };
      $scope.facebookLogin = function() {
          $scope.authUser();
      };
      $scope.facebookLoginMobile = function() {
          $scope.authUser();
      };
      $scope.authUser = function() {
          FB.login($scope.checkLoginStatus, {
              scope: 'email, user_likes, user_birthday, user_photos'
          });
      };
      $scope.authUserMobile = function() {
          FB.login($scope.checkLoginStatusMobile, {
              scope: 'email, user_likes, user_birthday, user_photos'
          });
      };
      
      
      $scope.checkLoginStatus = function(response) {
    	  $scope.progressbar.start();
    	  $('#myModal').modal('hide');
	      //$('#mymobileModal').modal('hide');
          if (response && response.status == 'connected') {
              console.log('User is authorized');
              FB.api('/me?fields=name,email', function(response) {
                  console.log(response);
                  console.log('Good to see you, ' + response.email + '.');
                  var username = response.name;
                  var email = response.email;
                  var phone = null;
            
                  var fdata = "username=" + response.name +
                      "&emailid=" + response.email +
                      "&userid=" + response.id;
                  $http({
                      method: 'POST',
                      data: fdata,
                      headers: {
                          'Content-Type': 'application/x-www-form-urlencoded'
                      },
                      url: 'sociallogin'
                  }).then(function successCallback(response) {

                	  $scope.facebooklogin = response.data;
                	  $scope.showDprofile = true;
                      $scope.showDlogin = false;
                      $scope.dofferlogin = false;
                      $scope.showreward = false;
                      $scope.username = $scope.facebooklogin.data[0].uloUserName;
                      $scope.progressbar.complete();
                  }, function errorCallback(response) {});
              })
          } else if (response.status === 'not_authorized') {
              // the user is logged in to Facebook, but has not authenticated your app
              console.log('User is not authorized');
              $('#myModal').modal('toggle');
    	      //$('#mymobileModal').modal('hide');
          } else {
              // the user isn't logged in to Facebook.
              console.log('User is not logged into Facebook');
              $('#myModal').modal('toggle');
    	      //$('#mymobileModal').modal('hide');
              $scope.progressbar.complete();

          }
      };
      $scope.checkLoginStatusMobile = function(response) {
    	  $scope.progressbar.start();
    	  //$('#myModal').modal('hide');
	      $('#mymobileModal').modal('hide');
          if (response && response.status == 'connected') {
              console.log('User is authorized');
              FB.api('/me?fields=name,email', function(response) {
                  console.log(response);
                  console.log('Good to see you, ' + response.email + '.');
                  var username = response.name;
                  var email = response.email;
                  var phone = null;
            
                  var fdata = "username=" + response.name +
                      "&emailid=" + response.email +
                      "&userid=" + response.id;
                  $http({
                      method: 'POST',
                      data: fdata,
                      headers: {
                          'Content-Type': 'application/x-www-form-urlencoded'
                      },
                      url: 'sociallogin'
                  }).then(function successCallback(response) {

                	  $scope.facebooklogin = response.data;
                	  $scope.showDprofile = true;
                      $scope.showDlogin = false;
                      $scope.dofferlogin = false;
                      $scope.showreward = false;
                      $scope.username = $scope.facebooklogin.data[0].uloUserName;
                      $scope.progressbar.complete();
                  }, function errorCallback(response) {});
              })
          } else if (response.status === 'not_authorized') {
              // the user is logged in to Facebook, but has not authenticated your app
              console.log('User is not authorized');
              //$('#myModal').modal('toggle');
    	      $('#mymobileModal').modal('hide');
          } else {
              // the user isn't logged in to Facebook.
              console.log('User is not logged into Facebook');
              //$('#myModal').modal('toggle');
    	      $('#mymobileModal').modal('hide');
              $scope.progressbar.complete();

          }
      };
 $scope.googleLoginMobile = function() {
    	  
    	  $scope.progressbar.start();
    	  //$('#myModal').modal('hide');
	      $('#mymobileModal').modal('hide');
          gapi.auth2.getAuthInstance().signIn().then(
              function(success) {
                  gapi.client.request({
                      path: 'https://www.googleapis.com/plus/v1/people/me'
                  }).then(
                      function(success) {
                          var user_info = JSON.parse(success.body);
                          var username = user_info.displayName;
                          var email = user_info.emails[0].value;
                          var phone = null;
                  
                          var fdata = "username=" + user_info.displayName +
                              "&emailid=" + user_info.emails[0].value +
                              "&userid=" + user_info.id;
                          $http({
                              method: 'POST',
                              data: fdata,
                              headers: {
                                  'Content-Type': 'application/x-www-form-urlencoded'
                              },
                              url: 'sociallogin'
                          }).then(function successCallback(response) {
                        	  
                        	  $scope.sociallogin = response.data;
                        
                         	   
                        	  $scope.showDprofile = true;
                              $scope.showDlogin = false;
                              $scope.dofferlogin = false;
                              $scope.showreward = false;
                              $scope.username = $scope.sociallogin.data[0].uloUserName;
                              //$('#myModal').modal('hide');
                   	          //$('#mymobileModal').modal('toggle');
                              $scope.progressbar.complete();
                          }, function errorCallback(response) {

                          });
                      },
                      // On error
                      function(error) {
                          $("#login-button").removeAttr('disabled');
                          alert('Error : Failed to get user user information');
                          $('#mymobileModal').modal('toggle');
                          $scope.progressbar.complete();
                      }
                  );
              },
              // On error
              function(error) {
                  $("#login-button").removeAttr('disabled');
                  //$('#myModal').modal('toggle');
       	          $('#mymobileModal').modal('toggle');
                  $scope.progressbar.complete();
              }
          );
      };
      
        	  $('.userotp').hide();

        	  $('.mobileuserotp').hide();
        	  $scope.sendMobileUserOtp = function(){
        		  
        	  }
        	  
        	  $scope.showDprofile = false;
        		$scope.showDlogin = true;
        		$scope.showMprofile = false;
        		$scope.showMlogin = true;
        		$scope.showreward = false;
        	    $scope.dsignin = "Submit";

            $scope.signup = function() {
          	  

              $scope.dsignin = "Please Wait...";
             
           	  var rewardDetailId = $('#rewardDetailId').val();
           	  if(rewardDetailId != null){
           		  var fdata = "userName=" + $('#userName').val() +
                     "&phone=" + $('#mobilePhone').val() +
                     "&emailId=" + $('#emailId').val() +
                     "&roleId=" + $('#roleId').val() +
                     "&accessRightsId=" + $('#accessRightsId').val() +
                     //"&password=" + $('#signpassword').val() +
                     "&rewardDetailId=" + rewardDetailId;
           	  }
           	  else{ 
                 var fdata = "userName=" + $('#userName').val() +
                     "&phone=" + $('#mobilePhone').val() +
                     "&emailId=" + $('#emailId').val() +
                     "&roleId=" + $('#roleId').val() +
                     "&accessRightsId=" + $('#accessRightsId').val();
                    // "&password=" + $('#signpassword').val();
           	  }
           	 
                 $http({
                     method: 'POST',
                     data: fdata,
                     headers: {
                         'Content-Type': 'application/x-www-form-urlencoded'
                     },
                     url: 'ulosignup'
                 }).then(function successCallback(response) {
                	 $scope.signup = response.data;
                	 
                	
              	    var alertmsg = ' <div class="alert alert-success alert-dismissible" style="color:red;text-align:center;">Login Successfully.</div>';
                      $('#signuperrors').html(alertmsg);
                      $(function() {
                          setTimeout(function() {
                              $("#signuperrors").hide('blind', {}, 100)
                          }, 2000);
                      });
                    //  $('#loginModal').modal('toggle');
                      //location.reload();
                     $('#myModal').modal('toggle');

                     $scope.dsignin = "Submit";
                     $scope.showDprofile = true;
                     $scope.showDlogin = false;
                     $scope.dofferlogin = false;
                     $scope.showreward = true;
                     $scope.UserIds = $scope.signup.data[0].userId;
                     $scope.getUser();
              	  
                
                 }, function errorCallback(response) {
                	
                	  $scope.dsignin = "Submit";
                	/*  $('#signuperror').html(alertmsg);
                        $(function() {
                            setTimeout(function() {
                                $("#signuperror").hide('blind', {}, 100)
                            }, 2000);
                        }) */
                        
                		 $("#signuperror").hide();
                        var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:red;text-align:center;">Email / Mobile Number already exist.</div>';
                        
                      $('#signuperror').html(alertmsg);
       		            $("#signuperror").fadeTo(2000, 500).slideUp(500, function(){
       		                $("#signuperror").slideUp(500);
       		                //$('.nav-tabs > .active').next('li').find('a').trigger('click');	
       		                 });
                 });
             };
             $scope.msignin = "Submit";
             $scope.mbsignup = function() {
            
          	   $scope.msignin = "Please Wait";
            	  var rewardDetailId = $('#mbRewardDetailId').val();
            	  if(rewardDetailId != null){
            		  var fdata = "userName=" + $('#mbUserName').val() +
                      "&phone=" + $('#mbMobilePhone').val() +
                      "&emailId=" + $('#mbEmailId').val() +
                      "&roleId=" + $('#mbRoleId').val() +
                      "&accessRightsId=" + $('#mbAccessRightsId').val() +
                      //"&password=" + $('#signpassword').val() +
                      "&rewardDetailId=" + rewardDetailId;
            	  }
            	  else{ 
                  var fdata = "userName=" + $('#mbUserName').val() +
                      "&phone=" + $('#mbMobilePhone').val() +
                      "&emailId=" + $('#mbEmailId').val() +
                      "&roleId=" + $('#mbRoleId').val() +
                      "&accessRightsId=" + $('#mbAccessRightsId').val();
                     // "&password=" + $('#signpassword').val();
            	  }
                  $http({
                      method: 'POST',
                      data: fdata,
                      headers: {
                          'Content-Type': 'application/x-www-form-urlencoded'
                      },
                      url: 'ulosignup'
                  }).then(function successCallback(response) {
                	  $scope.mbsignup = response.data;
                  	 $('#mymobileModal').modal('toggle');

                       
                       $scope.showMprofile = true;
                       $scope.showMlogin = false;
                       $scope.msignin = "Submit";
                       $scope.showreward = true;
                       $scope.UserIds = $scope.mbsignup.data[0].userId();
                       $scope.getUser();
               
                  }, function errorCallback(response) {
                	  $scope.msignin = "Submit";
                 	 $("#msignuperror").hide();
                     var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:red;text-align:center;">Mobile Number / Email already Already exist.</div>';
                     
                   $('#msignuperror').html(alertmsg);
       		            $("#msignuperror").fadeTo(2000, 500).slideUp(500, function(){
       		                $("#msignuperror").slideUp(500);
                 });
                  });
              };  
        		
        			
             
              $scope.sendUloLoginOtp = function() {
                  var fdata = "username="+$('#loginUserName').val();

                  $http({
                      method: 'POST',
                      data: fdata,
                      headers: {
                          'Content-Type': 'application/x-www-form-urlencoded'
                      },
                      url: 'send-ulologin-otp'
                  }).then(function successCallback(response) {
                       $scope.sendLoginOtp = response.data;
               		$scope.ulouserId = $scope.sendLoginOtp.data[0].uloUserId;
               		$scope.username =  $scope.sendLoginOtp.data[0].userName;
                       var alertmsg = ' <div class="alert alert-success alert-dismissible" style="color:red;text-align:center;">OTP Send Successfully.</div>';
                       $('#otpsendmsg').html(alertmsg);
                       $(function() {
                           setTimeout(function() {
                               $("#otpsendmsg").hide('blind', {}, 100)
                           }, 2000);
                           $('.userotp').show();
                   		  $('.userinfo').hide(); 
                       });
                    },function errorCallback(response) {
                    	 $("#loginerror").hide();
                      var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:red;text-align:center;">This email/mobile number does not exist.</div>';
                      
                    $('#loginerror').html(alertmsg);
         		            $("#loginerror").fadeTo(2000, 500).slideUp(500, function(){
         		                $("#loginerror").slideUp(500);
                  });
                  });
              };
              
              $scope.mbSendUloLoginOtp = function() {
                  var fdata = "username=" + $('#mbLoginUserName').val();

                  $http({
                      method: 'POST',
                      data: fdata,
                      headers: {
                          'Content-Type': 'application/x-www-form-urlencoded'
                      },
                      url: 'send-ulologin-otp'
                  }).then(function successCallback(response) {
                       $scope.mbSendLoginOtp = response.data;
                       $scope.mbulouserId = $scope.mbSendLoginOtp.data[0].uloUserId;
               		$scope.mbusername =  $scope.mbSendLoginOtp.data[0].userName;
                       $('.mobileuserotp').show();
             		  $('.mobileuserinfo').hide();
                
                  }, function errorCallback(response) {
                  	 $("#loginerrormb").hide();
                      var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:red;text-align:center;">This email/mobile number does not exist.</div>';
                      
                    $('#loginerrormb').html(alertmsg);
         		            $("#loginerrormb").fadeTo(2000, 500).slideUp(500, function(){
         		                $("#loginerrormb").slideUp(500);
                  });
               });
              };
              
              $scope.verifyUloLoginOtp = function() {
                  var fdata = "userId=" + $('#uloLoginId').val()+  
                               "&loginOtp=" + $('#loginOtp').val();
                  $http({
                      method: 'POST',
                      data: fdata,
                      headers: {
                          'Content-Type': 'application/x-www-form-urlencoded'
                      },
                      url: 'verify-ulologin-otp'
                  }).then(function successCallback(response) {
                 	$('#loginModal').modal('toggle');
                       $scope.verifyLoginOtp = response.data;
                       $scope.showDprofile = true;
                       $scope.showDlogin = false;
                       $scope.dofferlogin = false;
                       $scope.UserIds = $scope.verifyLoginOtp.data[0].userId;
                       $scope.getUser();
                       $('#loginModal').modal('toggle');
                       /// location.reload();
                      
                  }, function errorCallback(response) {
               	 $("#loginerror").hide();
                  var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:red;text-align:center;">Invalid username or password.</div>';
                  
                $('#loginerror').html(alertmsg);
         	            $("#loginerror").fadeTo(2000, 500).slideUp(500, function(){
         	                $("#loginerror").slideUp(500);
                  });
         	            
               });
              };
              
              $scope.mbVerifyUloLoginOtp = function() {
                  var fdata = "userId=" + $('#mbUloLoginId').val()+  
                               "&loginOtp=" + $('#mbLoginOtp').val();
                  $http({
                      method: 'POST',
                      data: fdata,
                      headers: {
                          'Content-Type': 'application/x-www-form-urlencoded'
                      },
                      url: 'verify-ulologin-otp'
                  }).then(function successCallback(response) {
                       $scope.mbVerifyLoginOtp = response.data;
                       $scope.UserIds = $scope.mbVerifyLoginOtp.data[0].userId;
                         $scope.getUser();
                         $('#loginmobileModal').modal('toggle');
                        //location.reload();
                      
                  }, function errorCallback(response) {
                   $("#loginerror").hide();
                  var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:red;text-align:center;">Invalid username or password.</div>';
                  
                $('#loginerror').html(alertmsg);
         	            $("#loginerror").fadeTo(2000, 500).slideUp(500, function(){
         	                $("#loginerror").slideUp(500);
                  });
                   
               });
              };           
              
              
              $scope.resendUloLoginOtp = function() {
                 var fdata = "userId=" + $('#uloLoginId').val()+  
                              "&username=" + $('#uloLoginName').val();   /* $('#uloLoginName').val() */
               
                 $http({
                     method: 'POST',
                     data: fdata,
                     headers: {
                         'Content-Type': 'application/x-www-form-urlencoded'
                     },
                     url: 'resend-ulologin-otp'
                 }).then(function successCallback(response) {
                      $scope.resendLoginOtp = response.data;
                     
                 });
             };
             
             $scope.mbResendUloLoginOtp = function() {
                 var fdata = "userId=" + $('#mbUloLoginId').val()+  
                              "&username=" + $('#mbUloLoginName').val();   /* $('#uloLoginName').val() */
                 $http({
                     method: 'POST',
                     data: fdata,
                     headers: {
                         'Content-Type': 'application/x-www-form-urlencoded'
                     },
                     url: 'resend-ulologin-otp'
                 }).then(function successCallback(response) {
                      $scope.mbResendLoginOtp = response.data;
                     
                 });
             };
             
             /* login and signup end here */ 
        	  $scope.getLocation = function() {
                  var url = "get-location";
                  $http.get(url).success(function(response) {
                      $scope.location = response.data;
                      console.log(response);
                  });
              };
              
              $scope.getPromotionImages = function() {
                  var url = "get-promotion-images";
                  $http.get(url).success(function(response) {
                      $scope.promotionImages = response.data;
                      //console.log(response);
                  });
              };
              
              $scope.getFilterSearch = function() {
  				var url = 'get-filter-search';
  				$http.get(url).success(function(response) {
  				   //console.log(response);
  					$scope.filterLocation= response.data;
  				});
  			};
  			
  			$("#panel li").click(function(){
		        location.href = $(this).find("a").attr("href");
		});
	   
  
		$(document).ready(function(){
   		    $("#hotellist").click(function(){
   		        $("#panel").slideDown("slow");
   		    });
   		});
		
		 $scope.getUrl = function() {

			 $scope.locationUrl = $scope.locationUrl; 
        	
          };
          
          $scope.StartDate=document.getElementById("alternate").value;
	 	  $scope.EndDate=document.getElementById("alternate1").value;
	 	  $scope.mbStartDate=document.getElementById("malternate").value;
	 	  $scope.mbEndDate=document.getElementById("malternate1").value;
          
          $scope.getChangeValues = function(){
        	 // $scope.locationUrl =  $scope.locationUrl; 
	          $scope.StartDate=document.getElementById("alternate").value;
		 	  $scope.EndDate=document.getElementById("alternate1").value;
          };
          
          $scope.getMbChangeValues = function(){
        	  	         	 // $scope.locationUrl =  $scope.locationUrl; 
        	  	 	          $scope.mbStartDate=document.getElementById("malternate").value;
        	  	 		 	  $scope.mbEndDate=document.getElementById("malternate1").value;
          };
          
          $scope.getRewardDetails = function() {
				
				var url = "get-reward-details";
				$http.get(url).success(function(response) {
				    //console.log(response);
					$scope.rewardDetails = response.data;
		
				});
		   };
		   
		   
           
           
           
           $scope.locationurl = function(selected) {        	     
        	    $scope.locationUrl =  selected.originalObject.locationUrl;
        	    }
          
           
           $scope.localSearch = function(str) {
        	   var matches = [], firstName, lastName, fullName, user;
        	    $scope.filterLocation.forEach(function(user) {
        	    	matches = user.locationName.match(/str/g)
        	     /* if {
        	    	 ((user.first_name.toLowerCase().indexOf(str.toString().toLowerCase()) >= 0) ||
        	         (user.last_name.toLowerCase().indexOf(str.toString().toLowerCase()) >= 0) ||
        	         ((user.first_name + ' ' + user.last_name).toLowerCase().indexOf(str.toString().toLowerCase()) >= 0)) 
        	       matches[matches.length] = $scope.people[i];
        	     } */
        	   });
        	   //return matches; 
        	 };
          
          $scope.getLocation();
          $scope.getRewardDetails();
          $scope.getPromotionImages();
          $scope.getFilterSearch();
          });
    </script>
    <script>	
      function showExplore() {
         document.getElementById('exploreAll').style.display = "block";
         document.getElementById('explorebtndiv').style.display = "none";
        }
       
    </script>
    <!-- Datepicker js -->
    <script>
    $( "#datepicker0" ).focus(function() {
   	  $('.datepicker').show('slow');
   	$("#banloc").addClass('banlocactive');
   	$("#banloctwo").addClass('addhomecoupon');
   	});
   $( "#mdatepicker0" ).focus(function() {
   	  $('.mbdatepicker').show('slow');
   	});
   //checkout open
       $( "#datepicker1" ).focus(function() {
   	  $('.datepicker').show('slow');
   	$("#banloc").addClass('banlocactive');
   	});
   $( "#mdatepicker1" ).focus(function() {
   	  $('.mbdatepicker').show('slow');
   	});	 
   var monthNames = [
   	              "Jan", "Feb", "Mar",
   	              "Apr", "May", "Jun", "Jul",
   	              "Aug", "Sep", "Oct",
   	              "Nov", "Dec"
   	          ];
   			  
   	 var weekDays = [
   	                " Sunday", " Monday", " Tuesday",
   	                " Wednesday", " Thursday", " Friday", " Saturday"
   	                ];
   	 
   	var mbweekDays = [
   	                " Sun", " Mon", " Tue",
   	                " Wed", " Thu", " Fri", " Sat"
   	                ];
   	var monthNos = [01,02,03,04,05,06,07,08,09,10,11,12];
   							
   	 var today = new Date();
   	 var toda_dd = today.getDate();
   	 var toda_mm = today.getMonth();
   	 var toda_yy = today.getFullYear();
   	 var toda_day = today.getDay();
   	 
   	 
   	 
   	 var tomorrow =  new Date();
   	 tomorrow.setDate(tomorrow.getDate() + 1);
   	 var tomo_dd = tomorrow.getDate();
   	 var tomo_mm = tomorrow.getMonth();
   	 var tomo_yy = tomorrow.getFullYear();
   	 var tomo_day = tomorrow.getDay();
   	 
   	 
   	/*var str1  = monthNos[toda_mm] + '/' + toda_dd + '/' + toda_yy;
   	var str2 =  monthNos[tomo_mm] + '/' + tomo_dd + '/' + tomo_yy;
   	$('#input1').val(str1);
   	$('#input2').val(str2);*/
   	 
   	 

   	 $('#input1').val(monthNos[toda_mm] + '/' + toda_dd + '/' + toda_yy);	
   	 $('#input2').val(monthNos[tomo_mm] + '/' + tomo_dd + '/' + tomo_yy);
   	 $('#datepicker0').val(monthNames[toda_mm] + ' ' + toda_dd + '' + weekDays[toda_day]);	
   	 $('#datepicker1').val(monthNames[tomo_mm] + ' ' + tomo_dd + '' + weekDays[tomo_day]);
   	 $('#alternate').val(monthNames[toda_mm] + ' ' + toda_dd + ',' + toda_yy);
   	 $('#alternate1').val(monthNames[tomo_mm] + ' ' + tomo_dd + ',' + tomo_yy); 
   	 
   	 $('#minput1').val(monthNos[toda_mm] + '/' + toda_dd + '/' + toda_yy);	
   	 $('#minput2').val(monthNos[tomo_mm] + '/' + tomo_dd + '/' + tomo_yy);
   	 $('#mdatepicker0').val(monthNames[toda_mm] + ' ' + toda_dd + '' + mbweekDays[toda_day]);	
   	 $('#mdatepicker1').val(monthNames[tomo_mm] + ' ' + tomo_dd + '' + mbweekDays[tomo_day]);
   	 $('#malternate').val(monthNames[toda_mm] + ' ' + toda_dd + ',' + toda_yy);
   	 $('#malternate1').val(monthNames[tomo_mm] + ' ' + tomo_dd + ',' + tomo_yy); 
   	 
   	
   	
   	 $(".datepicker").datepicker({
   	            //dateFormat: 'dd M DD',
   				minDate: 0,
   				numberOfMonths:2,
   				autoclose: false,
   	            beforeShowDay: function(date) {
   				var date1 = $.datepicker.parseDate($.datepicker._defaults.dateFormat, $("#input1").val());
   			    var date2 = $.datepicker.parseDate($.datepicker._defaults.dateFormat, $("#input2").val());
   				return [true, date1 && ((date.getTime() == date1.getTime()) || (date2 && date >= date1 && date <= date2)) ? "dp-highlight" : ""]; 
   				}, 
   				onSelect: function(dateText, inst) {
   				    
   					var date1 = $.datepicker.parseDate($.datepicker._defaults.dateFormat, $("#input1").val());
   					var date2 = $.datepicker.parseDate($.datepicker._defaults.dateFormat, $("#input2").val());
   	                var dt1 = new Date(dateText);
   				    var dd = dt1.getDate();
   	                var mm = dt1.getMonth();
   					var yy = dt1.getFullYear();
   	                var dayName = dt1.getDay();
   					
   					
   					
   					if (!date1 || date2) {
   					    
   						
   						checkIn = monthNames[mm] + ' ' + dd + '' + weekDays[dayName];
   						alternate = monthNames[mm] + ' ' + dd + ',' + yy;
   						stDate = dateText; 
   						
   						$("#input1").val(dateText);
   						$("#datepicker0").val(checkIn);
   						$("#alternate").val(alternate);
   						$("#input2").val("");
   						$(this).datepicker();
   					} else {
   					
   					    checkOut = monthNames[mm] + ' ' + dd + '' + weekDays[dayName];
   						alternate1 = monthNames[mm] + ' ' + dd + ',' + yy;
                        
   						if ((Date.parse(dateText) < Date.parse(stDate))){
   							
   							
   							$("#input2").val(stDate);
   	   						$("#datepicker1").val(checkIn);
   	   						$("#alternate1").val(alternate);
   							
   							$("#input1").val(dateText);
   	   						$("#datepicker0").val(checkOut);
   	   						$("#alternate").val(alternate1);
   	   						
   	   	                    
   	   						
   	   						$(this).datepicker();
   	   						$('.datepicker').hide('slow');
   	   						
   	   						
   							
   						}
   						
   						else if((Date.parse(dateText) == Date.parse(stDate))){
   							
   							var nextDay = new Date(dateText);
   							nextDay.setDate(nextDay.getDate()+ 1);
   							var nextDD = nextDay.getDate();
   		   	                var nextMM = nextDay.getMonth();
   		   					var nextYY = nextDay.getFullYear();
   		   	                var nextName = nextDay.getDay();
   							
   		   	                checkOut = monthNames[nextMM] + ' ' + nextDD + '' + weekDays[nextName];
 						    alternate1 = monthNames[nextMM] + ' ' + nextDD + ',' + nextYY;
 						    var input2 =  nextMM+1 + '/' + nextDD + '/' + nextYY;
 						   
   							$("#input2").val(input2);
   	   						$("#datepicker1").val(checkOut);
   	   						$("#alternate1").val(alternate1);
   	   						
   	   					    $(this).datepicker();
	   						$('.datepicker').hide('slow');
   							
   						}
   						
   						else {
   							
   							$("#input2").val(dateText);
   	   						$("#datepicker1").val(checkOut);
   	   						$("#alternate1").val(alternate1);
   	   						
   	   	                    $(this).datepicker();
   	   						$('.datepicker').hide('slow');
   							
   						}
                     	$("#banloc").removeClass('banlocactive');
   					}
   					
   				}
   			});
   	$(".mbdatepicker").datepicker({
           //dateFormat: 'dd M DD',
   		minDate: 0,
   		numberOfMonths:1,
   		autoclose: false,
           beforeShowDay: function(date) {
   		var date1 = $.datepicker.parseDate($.datepicker._defaults.dateFormat, $("#minput1").val());
   	    var date2 = $.datepicker.parseDate($.datepicker._defaults.dateFormat, $("#minput2").val());
   		return [true, date1 && ((date.getTime() == date1.getTime()) || (date2 && date >= date1 && date <= date2)) ? "dp-highlight" : ""]; 
   		}, 
   		onSelect: function(dateText, inst) {
   		    
   			var date1 = $.datepicker.parseDate($.datepicker._defaults.dateFormat, $("#minput1").val());
   			var date2 = $.datepicker.parseDate($.datepicker._defaults.dateFormat, $("#minput2").val());
               var dt1 = new Date(dateText);
   		    var dd = dt1.getDate();
               var mm = dt1.getMonth();
   			var yy = dt1.getFullYear();
               var dayName = dt1.getDay();
   			
   			
   			
   			if (!date1 || date2) {
   			    
   				
   				checkIn = monthNames[mm] + ' ' + dd + '' + mbweekDays[dayName];
   				malternate = monthNames[mm] + ' ' + dd + ',' + yy;
   				stDate = dateText;
   				
   				
   				$("#minput1").val(dateText);
   				$("#mdatepicker0").val(checkIn);
   				$("#malternate").val(malternate);
   				$("#minput2").val("");
   
                   $(this).datepicker();
   			} 
   			
   			else {
   				
   				if ((Date.parse(dateText) < Date.parse(stDate))){
						
   					   checkOut = monthNames[mm] + ' ' + dd + '' + mbweekDays[dayName];
		   			   malternate1 = monthNames[mm] + ' ' + dd + ',' + yy;
						
						$("#minput2").val(stDate);
  						$("#mdatepicker1").val(checkIn);
  						$("#malternate1").val(malternate);
						
						$("#minput1").val(dateText);
  						$("#mdatepicker0").val(checkOut);
  						$("#malternate").val(malternate1);
  						
  	                    
  						
  						$(this).datepicker();
  						$('.mbdatepicker').hide('slow');
  						
  						
						
					}
					
					else if((Date.parse(dateText) == Date.parse(stDate))){
						//checkOut = monthNames[mm] + ' ' + dd + '' + mbweekDays[dayName];
			   			//malternate1 = monthNames[mm] + ' ' + dd + ',' + yy;
			   			
			   			var nextDay = new Date(dateText);
							nextDay.setDate(nextDay.getDate()+ 1);
							var nextDD = nextDay.getDate();
		   	                var nextMM = nextDay.getMonth();
		   					var nextYY = nextDay.getFullYear();
		   	                var nextName = nextDay.getDay();
							
		   	                checkOut = monthNames[nextMM] + ' ' + nextDD + '' + weekDays[nextName];
						    malternate1 = monthNames[nextMM] + ' ' + nextDD + ',' + nextYY;
						    var input2 =  nextMM+1 + '/' + nextDD + '/' + nextYY;
						   
							$("#minput2").val(input2);
	   						$("#mdatepicker1").val(checkOut);
	   						$("#malternate1").val(malternate1);
	   						
	   					    $(this).datepicker();
   						    $('.mbdatepicker').hide('slow');
			   			 
						
  						
  					    $(this).datepicker();
						$('.mbdatepicker').hide('slow');
						
					}
					
					else {
						
						checkOut = monthNames[mm] + ' ' + dd + '' + mbweekDays[dayName];
			   			malternate1 = monthNames[mm] + ' ' + dd + ',' + yy;
						$("#minput2").val(dateText);
  						$("#mdatepicker1").val(checkOut);
  						$("#malternate1").val(malternate1);
  						
  	                    $(this).datepicker();
  						$('.mbdatepicker').hide('slow');
						
					}
   			  /*   checkOut = monthNames[mm] + ' ' + dd + '' + mbweekDays[dayName];
   			    malternate1 = monthNames[mm] + ' ' + dd + ',' + yy;
   				$("#minput2").val(dateText);
   				$("#mdatepicker1").val(checkOut);
   				$("#malternate1").val(malternate1);
                   $(this).datepicker();
   				$('.mbdatepicker').hide('slow'); */
   				
   			}
   			
   		}
   	});	

       
    </script>
    <script>
// Set the date we're counting down to
var countDownDate = new Date("dec 31, 2018 23:58:00").getTime();

// Update the count down every 1 second
var x = setInterval(function() {

  // Get todays date and time
  var now = new Date().getTime();
    
  // Find the distance between now and the count down date
  var distance = countDownDate - now;
    
  // Time calculations for days, hours, minutes and seconds
  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    
  // Output the result in an element with id="demo"
  document.getElementById("newlaunch").innerHTML = days + "d " + hours + "h "
  + minutes + "m " + seconds + "s ";
    
  // If the count down is over, write some text 
  if (distance < 0) {
    clearInterval(x);
    document.getElementById("demo").innerHTML = "EXPIRED";
  }
}, 1000);
</script>
  </body>
</html>