<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="s" uri="/struts-tags"%>

   <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Taxes 
          </h1>
          <ol class="breadcrumb">
            <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
 
            <li class="active">Taxes</li>
          </ol>
        </section>
		
        <!-- Main content -->
        <section class="content">
        
        	<div class="row">
            	<div class="col-xs-12">
						<div class="box">
							<div class="box-header">
								<h3 class="box-title">Taxes</h3>
							</div>
							
							
							<!-- /.box-header -->
							<div class="box-body table-responsive no-padding" > 
								<table class="table table-hover" id="example1">
									<tr>
										
										
									<th>Tax Code</th>
									<th>Edit</th>
									<th>Delete</th>
									</tr>
									  <tr ng-repeat="t in taxes track by $index">
									
										
										<td>Tax&nbsp{{$index}}</td>
											<td>
											<a href="#editmodal" ng-click="getTax(t.taxId)"  data-toggle="modal"><i  class="fa fa-pencil text-green"></i></a>
										
										</td>
											<td>
											
											<a href="#" ng-click="deleteTax(t.taxId)"  data-toggle="modal"><i  class="fa fa-fw fa-trash text-red"></i></a>
										</td>
									</tr>
									<tr>
									</tr>
								</table>
							</div>
							<!-- /.box-body -->
							
							
							<div class="box-footer clearfix">
								 <ul class="pagination pagination-sm no-margin pull-right">
									<a class="btn btn-primary" href="#addmodal" ng-click=""  data-toggle="modal" >Add Tax</a>
								</ul> 
							</div>
						</div>
						<!-- /.box -->
					</div>	
         	</div>
          
        </section><!-- /.content -->
      	
      	<div class="modal" id="addmodal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" id="btnclose" class="close" data-dismiss="modal"
							aria-hidden="true">x</button>
						<h4 class="modal-title">Add Tax</h4>
					</div>
					 <form name="addForm">
					<div class="modal-body" >
					<div id="message"></div>
					
					   
						 
						 
						<div class="form-group">
							
							<input type="hidden" class="form-control"  name="taxName"  id="taxName"  placeholder="Name" ng-model='taxName' ng-pattern="^/^[A-Za-z\d\s]+$/"  maxlength="50">
						    <span ng-show="addForm.taxName.$error.pattern">Not a valid Name!</span>
						     
						</div>
						
						<div class="form-group">
							
							<input type="hidden" class="form-control"  name="taxType"  id="taxType"  placeholder="Name" value= "GST"   maxlength="20">
						    <span ng-show="addForm.taxType.$error.pattern">Not a valid Type!</span>
						     
						</div>
						
						
						<div class="form-group">
							
							<input type="hidden" class="form-control"  name="taxCode"  id="taxCode"  placeholder="Code" ng-model='taxCode' ng-pattern="/^[0-9]/"  maxlength="20">
						    <span ng-show="addForm.taxCode.$error.pattern" style="color:red">Not a valid Code!</span>
						</div>
						
						<div class="form-group">
							<label>Amount From</label>
							<input type="text" class="form-control"  name="taxAmountFrom"  id="taxAmountFrom"  placeholder="Tax Amount From" ng-model='taxAmountFrom' ng-pattern="/^[0-9]/" ng-required="true">
						    <span ng-show="addForm.taxAmountFrom.$error.pattern" style="color:red">Not a valid Amount!</span>
						</div>
						
						<div class="form-group">
							<label>Amount To</label>
							<input type="text" class="form-control"  name="taxAmountTo"  id="taxAmountTo"  placeholder="Tax Amount To" ng-model='taxAmountTo' ng-pattern="/^[0-9]/" ng-required="true">
						    <span ng-show="addForm.taxAmountTo.$error.pattern" style="color:red">Not a valid Amount!</span>
						</div>
						
						<div class="form-group">
							<label>Percentage</label>
							<input type="text" class="form-control"  name="taxPercentage"  id="taxPercentage"  placeholder="Tax Percentage" ng-model='taxPercentage' ng-pattern="/^[0-9]/" ng-required="true">
						    <span ng-show="addForm.taxPercentage.$error.pattern" style="color:red">Not a valid Amount!</span>
						</div>
						
						
						
					</div>
					<div class="modal-footer">
	
						<a href="#" data-dismiss="modal" class="btn btn-danger">Close</a>
						<button ng-click="addForm.$valid && addTax()" ng-disabled="addForm.$invalid" class="btn btn-primary">Save</button>
					</div>
					</form>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dalog -->
		</div>
		<div class="modal" id="editmodal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" id="editbtnclose" class="close" data-dismiss="modal"
							aria-hidden="true">x</button>
						<h4 class="modal-title">Edit Tax</h4>
					</div>
					<form name="editForm">
					<div class="modal-body" ng-repeat="ta in tax">
					<div id="editmessage"></div>
					<input type="hidden" class="form-control"  name="editTaxId"  id="editTaxId"  value="{{ta.propertyTaxId}}" >
						
						
						
						<div class="form-group">
							
							<input type="hidden" class="form-control"  name="editTaxName"  id="editTaxName" value="{{ta.taxName}}"  ng-pattern="/^[a-zA-Z0-9]+$/" >
						    <span ng-show="editForm.editTaxName.$error.pattern">Not a valid Name!</span>
						     
						</div>
						
						<div class="form-group">
							
							<input type="hidden" class="form-control"  name="editTaxType"  id="editTaxType"  value="{{ta.taxType}}"   ng-pattern="/^[a-zA-Z0-9]+$/" >
						    <span ng-show="editForm.editTaxType.$error.pattern">Not a valid Type!</span>
						     
						</div>
						
						
						<div class="form-group">
							
							<input type="hidden" class="form-control"  name="editTaxCode"  id="editTaxCode" value="{{ta.taxCode}}"   ng-pattern="/^[0-9]/" >
						    <span ng-show="editForm.editTaxCode.$error.pattern">Not a valid Code!</span>
						</div>
						
						<div class="form-group">
							<label>Tax Amount From</label>
							<input type="text" class="form-control"  name="editTaxAmountFrom"  id="editTaxAmountFrom" value="{{ta.taxAmountFrom}}" ng-model='ta.taxAmountFrom'  ng-pattern="/^[0-9]/" ng-required="true">
						    <span ng-show="editForm.editTaxAmountFrom.$error.pattern" style="color:red">Not a valid Amount!</span>
						</div>
						
						<div class="form-group">
							<label>Tax Amount To</label>
							<input type="text" class="form-control"  name="editTaxAmountTo"  id="editTaxAmountTo" value="{{ta.taxAmountTo}}"  ng-model='ta.taxAmountTo'  ng-pattern="/^[0-9]/" ng-required="true">
						    <span ng-show="editForm.editTaxAmountTo.$error.pattern" style="color:red">Not a valid Amount!</span>
						</div>
						
						<div class="form-group">
							<label>Tax Percentage</label>
							<input type="text" class="form-control"  name="editTaxPercentage"  id="editTaxPercentage" value="{{ta.taxPercentage}}" ng-model='ta.taxPercentage'  ng-pattern="/^[0-9]/" ng-required="true">
						    <span ng-show="editForm.editTaxPercentage.$error.pattern" style="color:red">Not a valid Amount!</span>
						</div>
						
					</div>
					<div class="modal-footer">
	
						<a href="#" data-dismiss="modal" class="btn btn-danger">Close</a>
						<button ng-click="editForm.$valid && editTax()" ng-disabled="editForm.$invalid" class="btn btn-primary">Save</button>
					</div>
					</form>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dalog -->
		</div>
		
		<div class="modal" id="deletemodal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" id="deletebtnclose" class="close" data-dismiss="modal"
							aria-hidden="true">x</button>
						<h4 class="modal-title">Delete Message</h4>
					</div>
					<div class="modal-body">
					 <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-check"></i>Your Tax has been deleted Sucessfully </h4>
                  
                 </div>
					    
						
					</div>
					<div class="modal-footer">
	                    
						<a href="#" data-dismiss="modal" class="btn">Close</a>
						
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dalog -->
		</div>
    

      </div><!-- /.content-wrapper -->
      


<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>

	<script>


 
	
		var app = angular.module('myApp', ['ngProgress']);
		app.controller('customersCtrl', function($scope, $http,$timeout,ngProgressFactory) {
         
			var spinner = $('#loadertwo');

			
	       	$timeout(function(){
	      	    // $scope.progressbar.complete();
	            $scope.show = true;
	            $("#pre-loader").css("display","none");
	        }, 2000);

			 
			
			$scope.unread = function() {
			//var notifiurl = "http://localhost:8085/collaborative-workflow/unreadnotifications.action";
			var notifiurl = "unreadnotifications.action";
			$http.get(notifiurl).success(function(response) {
				$scope.latestnoti = response.data;
			});
			};

			
				
			$scope.getTaxes = function() {

				var url = "get-taxes";
				$http.get(url).success(function(response) {
				    //console.log(response);
					$scope.taxes = response.data;
		
				});
			};
			
			
			$scope.addTax = function(){
				//alert($('#taskId').val());
				
				var fdata = "taxName=" + $('#taxName').val()
				+ "&taxType=" + $('#taxType').val()
				+ "&taxCode=" + $('#taxCode').val()
				+ "&taxAmountFrom=" + $('#taxAmountFrom').val()
				+ "&taxPercentage=" + $('#taxPercentage').val()
				+ "&taxAmountTo=" + $('#taxAmountTo').val();
				
				
				spinner.show();
				//alert(fdata);
				$http(
						{
							method : 'POST',
							data : fdata,
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							url : 'add-tax'
						}).then(function successCallback(response) {
							
							$scope.getTaxes();
							var alert = ' <div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Alert!</h4>Success Item Added Succesfully.</div>';
				            $('#message').html(alert);
				            window.location.reload(); 	 	
							$timeout(function(){
					      	$('#btnclose').click();
					        }, 2000);
							   setTimeout(function(){ spinner.hide(); 
							   }, 1000);

							//$('#btnclose').click();
				}, function errorCallback(response) {
					
					   setTimeout(function(){ spinner.hide(); 
					   }, 1000);

					// called asynchronously if an error occurs
					// or server returns response with an error status.
				});

			};
			
			$scope.editTax = function(){
				
				
				
				var fdata = "taxName=" + $('#editTaxName').val()
				+ "&propertyTaxId=" + $('#editTaxId').val()
				+ "&taxType=" + $('#editTaxType').val()
				+ "&taxCode=" + $('#editTaxCode').val()
				+ "&taxAmountFrom=" + $('#editTaxAmountFrom').val()
				+ "&taxAmountTo=" + $('#editTaxAmountTo').val()
				+ "&taxPercentage=" + $('#editTaxPercentage').val();
				
				
				spinner.show();
				//alert(fdata);
				$http(
						{
							method : 'POST',
							data : fdata,
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							url : 'edit-tax'
						}).then(function successCallback(response) {
							
							$scope.getTaxes();
							var alert = ' <div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Alert!</h4>Success Item Updated Succesfully.</div>';
				            $('#editmessage').html(alert);
							$timeout(function(){
					      	$('#editbtnclose').click();
					        }, 2000);
							   setTimeout(function(){ spinner.hide(); 
							   }, 1000);

				}, function errorCallback(response) {
					   setTimeout(function(){ spinner.hide(); 
					   }, 1000);

					// called asynchronously if an error occurs
					// or server returns response with an error status.
				});

			};
			
            
            
			
			
            
			$scope.getTax = function(rowid) {
				
				
				var url = "get-tax?propertyTaxId="+rowid;
				$http.get(url).success(function(response) {
				    //console.log(response);
				   
					$scope.tax = response.data;
					//alert(response.data);
				});
				
			};
			
            $scope.deleteTax = function(rowid) {
            	var check = confirm("Are you sure you want to delete this Inventory?");
    			if( check == true){
				
				var url = "delete-tax?propertyTaxId="+rowid;
				$http.get(url).success(function(response) {
				    //console.log(response);
				    $scope.getTaxes();
				    $('#deletemodal').modal('toggle');
				    $timeout(function(){
				      	$('#deletebtnclose').click();
				    }, 2000);
					
		
				});
    			}
			};
			
			$scope.getAccommodations = function() {

				var url = "get-accommodations";
				$http.get(url).success(function(response) {
				    //console.log(response);
					$scope.accommodations = response.data;
		
				});
			};
			
			
			 $scope.getPropertyList = function() {
				 
		        	
		        	
			        var userId = $('#adminId').val();
		 			var url = "get-user-properties?userId="+userId;
		 			$http.get(url).success(function(response) {
		 			    
		 				$scope.props = response.data;
		 	
		 			});
		 		};
		 		
           $scope.change = function() {
     	   
		        //alert($scope.id);
		        
		        var propertyId = $scope.id;	
     	    var url = "change-user-property?propertyId="+propertyId;
	 			$http.get(url).success(function(response) {
	 				
	 				 window.location = '/ulopms/dashboard'; 
	 				//$scope.change = response.data;
	 	
	 			});
			       
		 		};
		 		
				
		    $scope.getPropertyList();
		    
			$scope.getTaxes();
			
			$scope.getAccommodations();
			
			//$scope.unread();
			//
			
			
		});


		
		   
		        

		
	</script>
		
	<link rel="stylesheet" href="dist/css/dataTables.bootstrap.css">
	 <script src="js1/jquery.dataTables.min.js" type="text/javascript"></script>
	 <script src="js1/dataTables.bootstrap.min.js" type="text/javascript"></script>
       