<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="s" uri="/struts-tags"%>

   <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <section class="content-header">
          <h1>
            Property Profile
            <!--<small>Subscription Expiry on {{property[0].subscription[0].expirydate}}</small>-->
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Property</a></li>
            <li class="active">Amenities</li>
          </ol>
        </section>
		
        <!-- Main content -->
        <section class="content">
        <div class="row">
        <form name="bookingForm">
                <div class="form-group col-md-3">
                <label>Check-in:</label>

                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="date" class="form-control" id="bookCheckin"  name="bookCheckin">
                </div>
                <!-- /.input group -->
              </div>
                        <div class="form-group col-md-3">
                <label>Check-out:</label>

                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                    <input type="date" class="form-control" id="bookCheckout"  name="bookCheckout">
                  
                </div>
                <!-- /.input group -->
              </div>
              <div class="col-md-2">
              <button type="button" ng-click="getAvailability()"class="btn btn-block btn-info btn-sm" style="margin-top:25px">Search</button>
             </div>
             
   
        </form>
        
            </div>
        				<div class="box-body table-responsive no-padding" > 
								<table class="table table-bordered" id="example1">
									<h4>Availability</h4>
									<tr>
										
										<th>Type</th>
										<th>Starting from</th>
										<th>Arrival Date</th>
										<th>Departure date </th>
										<th>Available</th>
										<th>Select Rooms</th>
										<th>Action</th>
										
									</tr>
									<tr ng-repeat="av in availablities track by $index">
									
										<td>{{av.accommodationType}}</td>
										<td>{{av.baseAmount}}</td>
										<td>{{av.arrivalDate}}</td>
									    <td>{{av.departureDate}}</td>
									    <td>{{av.available}}</td>
									    <td><select ng-model="r.value" ng-change="update(r.value)"><option ng-repeat="r in range(1,av.available)" value="{{r}}">{{r}}</option></select></td>
									    Selected Value is:{{rValue}}
									    
									    <td><a class="btn btn-xs btn-primary" href="#addmodal" ng-click="getBooking($index)"  data-toggle="modal" >Add</a></td>
									    
									</tr>
									<tr>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
										<!-- <td>Sub Total <br> 100rs</td>
										<td>Grand Total <br> 100rs</td>
									    <td>Deposit <br> 100rs</td> -->
									</tr>
								</table>
							</div>
    
     
          
          
        </section><!-- /.content -->
      
      <div class="modal" id="addmodal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" id="btnclose" class="close" data-dismiss="modal"
							aria-hidden="true">x</button>
						<h4 class="modal-title">Booking Details</h4>
					</div>
					 <form name="myform">
					<div class="modal-body">
					<div id="message"></div>
						<div class="form-group">
						    
							<input type="hidden" class="form-control"  name="accommodationId"  id="accommodationId" value="{{availablities[indexval].accommodationId}}" >
						 </div>
						<div class="form-group">
						   <label>AccommodationType</label>
							<input type="text" class="form-control"  name="accommodationType"  id="accommodationType" value="{{availablities[indexval].accommodationType}}" >
						</div>
						
						<div class="row">           
                        <div class="form-group col-xs-6 calend ">               
                        <input id="checkin" class="form-control input-group-lg reg_name" type="text"  name="checkin" value="{{availablities[indexval].arrivalDate}}"/>
			            </div>			
			            <div class="form-group col-xs-6 calend">               
                        <input id="checkout" class="form-control input-group-lg reg_name" type="text" name="checkout" value="{{availablities[indexval].departureDate}}"/>
			            </div>			
                        </div>
                        
                        <div class="form-group">
						   <label>maximum Occupancy</label>
							<input type="text" class="form-control"  name="maxOccupancy"  id="maxOccupancy" value="{{availablities[indexval].maxOccupancy}}" >
						</div>
						
						 <div class="row">
                      <div class="form-group col-xs-4">
                      <label for="label-1-0">Rooms</label>
			             <div>					  
                        <select id="numberofrooms" class="form-control input-group-lg reg_name " type="text" name="rooms" class="form-control">
                           <option ng-repeat="n in range(1,availablities[indexval].available)" value="{{n}}">{{n}}</option>
                           <!--   <option value="0">0</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<option value="5">5</option>  -->
                        </select>
                      </div>				
                      </div> 
                    <div class="form-group col-xs-4">
                    <label for="label-1-0">Adult (12yrs +)</label>
                     <select id="adults" class="form-control input-group-lg reg_name" type="text" name="adults" class="form-control">				
                           <option value="0">0</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<option value="5">5</option>  
                        </select>
			         </div>
			       <div class="form-group col-xs-4">
                   <label for="label-1-0">child(2-12yrs)</label>
			          <div>
					  
                        <select id="child" class="form-control input-group-lg reg_name" type="text" name="child" class="form-control">
                           
							<option value="0">0</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<option value="5">5</option>                     
                        </select>
                    </div>
			       </div>
                   </div> 
					
					
					<div class="form-group">
						  
							<input type="hidden" class="form-control"  name="availableRooms"  id="availableRooms" value="{{availablities[indexval].available}}" >
				    </div>
				    
					<div class="form-group">
						   <label>Base Amount</label>
							<input type="text" class="form-control"  name="baseAmount"  id="baseAmount" value="{{availablities[indexval].baseAmount}}" >
				    </div>
				    
				    <div class="form-group">
						   <label>Tax</label>
							<input type="text" class="form-control"  name="tax"  id="tax" value="0" >
				    </div>
				    
				    <div class="form-group">
						   <label>security Deposit</label>
							<input type="text" class="form-control"  name="securityDeposit"  id="securityDeposit" value="0" >
				    </div>
				    
				    <div class="form-group">
						   <label>Base Amount</label>
							<input type="text" class="form-control"  name="baseAmount"  id="baseAmount" value="{{availablities[indexval].baseAmount}}" >
				    </div>
				    
				    <div class="form-group">
						   <label>Temp Amount</label>
							<input type="text" class="form-control"  name="price"  id="price" value="{{availablities[indexval].baseAmount}}" >
				    </div>
				    
				    <div class="form-group">
						    
							<input type="hidden" class="form-control"  name="adultsAllowed"  id="adultsAllowed" value="{{availablities[indexval].adultsIncludedRate}}" >
				    </div>
				    
				    <div class="form-group">
						  
							<input type="hidden" class="form-control"  name="childAllowed"  id="childAllowed" value="{{availablities[indexval].childIncludedRate}}" >
				    </div>
				    
				    <div class="form-group">
						   
							<input type="hidden" class="form-control"  name="extraAdult"  id="extraAdult" value="{{availablities[indexval].extraAdult}}" >
				    </div>
				    
				     <div class="form-group">
						  
							<input type="hidden" class="form-control"  name="extraChild"  id="extraChild" value="{{availablities[indexval].extraChild}}" >
				    </div>
				    
				    
				    
				     <div class="form-group">
						   
							<input type="hidden" class="form-control"  name="days"  id="days" value="" >
				     </div>
							
					</div>
					<div class="modal-footer">
	                    <button  class="btn btn-primary" onclick ="bookfix()">CheckTotal</button>
						<a href="#" data-dismiss="modal" class="btn">Close</a>
						<button  class="btn btn-primary" onclick ="bookNext()">Next</button>
						<button ng-click="confirmBooking()" class="btn btn-primary">Save</button>
					</div>
					</form>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dalog -->
		</div>
		
		 <div class="modal" id="guestmodal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" id="btnclose" class="close" data-dismiss="modal"
							aria-hidden="true">x</button>
						<h4 class="modal-title">Booking Details</h4>
					</div>
					 <form name="guestdetails">
					<div class="modal-body">
					<div id="message"></div>
					
									
						<div class="form-group col-sm-4">
							<label for="firstName">First Name</label>
							<input type="text" class="form-control"  name="firstName"  id="firstName"  placeholder="First Name" ng-model='firstName' ng-pattern="/^[a-zA-Z\s]*$/" ng-required="true" maxlength="50">
						    <span ng-show="addguestinformation.firstName.$error.pattern">Enter The Valid First Name</span>
						</div>
							<div class="form-group col-sm-4">
							<label for="lastName">Last Name</label>
							<input type="text" class="form-control"  name="lastName"  id="lastName"  placeholder="Last Name" ng-model='lastName' ng-pattern="/^[a-zA-Z\s]*$/" ng-required="true" maxlength="50">
						    <span ng-show="addguestinformation.lastName.$error.pattern">Enter The Valid Second Name!</span>
						</div>
								<div class="form-group col-sm-4">
							<label for="guestEmail">E-mail</label>
							<input type="email" class="form-control"  name="guestEmail"  id="guestEmail"  placeholder="Enter Your Email" ng-model='guestEmail' ng-pattern="/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i" ng-required="true" maxlength="50">
						    <span ng-show="addguestinformation.guestEmail.$error.pattern">Enter The Valid Email ID!</span>
						     
						</div>
								<div class="form-group col-sm-4">
							<label for="mobile_phone">Mobile Phone</label>
							<input type="text" class="form-control"  name="mobilePhone"  id="mobilePhone"  placeholder="Enter The Mobile Number" ng-model='mobilePhone' ng-pattern="/^[0-9]*$/" ng-required="true" maxlength="20">
						    <span ng-show="addguestinformation.mobilePhone.$error.pattern">Enter the Valid Mobile Number!</span> 
						</div>
								<div class="form-group col-sm-4">
							<label for="landlinePhone">LandLine Number</label>
							<input type="text" class="form-control"  name="landlinePhone"  id="landlinePhone"  placeholder="Enter The LandLine Number " ng-model='landlinePhone' ng-pattern="/^[0-9]*$/" ng-required="true" maxlength="20">
						    <span ng-show="addguestinformation.landlinePhone.$error.pattern">Enter the Valid Phone Number!</span> 
						</div>
								<div class="form-group col-sm-4">
							<label for="firstAddressLine">Address Line 1</label>
							<input type="text" class="form-control"  name="firstAddressLine"  id="firstAddress"  placeholder="Enter The Address" ng-model='firstAddressLine' ng-pattern="/^[ A-Za-z0-9_@./#&+-]*$/" ng-required="true" maxlength="250">
						    <span ng-show="addguestinformation.firstAddressLine.$error.pattern">Not a Valid Fields !</span>
						     
						</div>
									<div class="form-group col-sm-4">
							<label for="secondAddressLine">Address Line 2</label>
							<input type="text" class="form-control"  name="secondAddressLine"  id="secondAddress"  placeholder="Enter The Address" ng-model='secondAddressLine' ng-pattern="/^[ A-Za-z0-9_@./#&+-]*$/" ng-required="true" maxlength="250">
						    <span ng-show="addguestinformation.secondAddressLine.$error.pattern">Not a Valid Fields !</span>
						     
						</div>
						<div class="form-group col-sm-4">
							<label for="guestCity">City</label>
							<input type="text" class="form-control"  name="guestCity"  id="guestCity"  placeholder="City" ng-model='guestCity' ng-pattern="/^[a-zA-Z\s]*$/" ng-required="true" maxlength="50">
						    <span ng-show="addguestinformation.guestCity.$error.pattern">Enter The City</span>     
						</div>
						    <div class="form-group col-sm-4">
							<label for="guestZipcode">Postal Zipcode</label>
							<input type="text" class="form-control"  name="guestZipcode" id="guestZipCode" placeholder="Zip Code" ng-model='guestZipcode' ng-pattern="/^[0-9]/" ng-required="true" maxlength="10">
						     <span ng-show="addguestinformation.guestZipcode.$error.pattern">Enter The Valid POstal Code</span>
						</div>
							<div class="form-group col-sm-4">
                            <label for="guestcountrycode">Country</label>
                         <select name="countryCode" id="countryCode" value="{{property[0].countryCode}}" class="form-control">
                                        <option value="select">select</option>
                            <option ng-repeat="c in countries" value="{{c.countryCode}}"  ng-selected ="property[0].countryCode == c.countryCode">{{c.countryName}}</option>
                                      </select>
                        </div>
                              <div class="form-group col-sm-4">
                                      <label>State</label>
                                      <select name="stateCode" id="stateCode" value="{{property[0].stateCode}}" class="form-control">
                                        <option value="select">select</option>
                                    <option ng-repeat="s in states" value="{{s.stateCode}}" ng-selected ="property[0].stateCode == s.stateCode">{{s.stateName}}</option>
                                      </select>
                                      
                         </div>
				
						
					</div>
					
					<div class="modal-footer">
	                    <br>
						<!--  <button  class="btn btn-primary" onclick ="bookNext()" style="margin-top:100px;">Next</button>-->
						<button ng-click="confirmBooking()" class="btn btn-primary">Save</button>
					</div>
					</form>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dalog -->
		</div>

      </div><!-- /.content-wrapper -->
     


<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>

	 <script src="js/jquery-2.1.1.js" type="text/javascript"></script>
 <script>
 $('.datepicker').datepicker({
	    dateFormat: 'dd-mm-yy'
	 });
 </script>
	<script>
    
	 
	
	var app = angular.module('myApp', ['ngProgress']);
	app.controller('customersCtrl', function($scope, $http,$timeout,ngProgressFactory) {

		 
		
       	$timeout(function(){
      	    // $scope.progressbar.complete();
            $scope.show = true;
            $("#pre-loader").css("display","none");
        }, 2000);

		 
		
		$scope.unread = function() {
		//var notifiurl = "http://localhost:8085/collaborative-workflow/unreadnotifications.action";
		var notifiurl = "unreadnotifications.action";
		$http.get(notifiurl).success(function(response) {
			$scope.latestnoti = response.data;
		});
		};

		
		
		
		
       $scope.getAvailability = function(){
			
			
			//alert($('#propertyItemCategoryId').val());
			
			var fdata = "&arrivalDate=" + $('#bookCheckin').val()
			+ "&departureDate=" + $('#bookCheckout').val()
             
			
			
			$http(
					{
						method : 'POST',
						data : fdata,
						headers : {
							'Content-Type' : 'application/x-www-form-urlencoded'
						},
						url : 'get-availability'
					}).success(function(response) {
						
						$scope.availablities = response.data;
						
					});

		};
		
		$scope.getBooking = function (id) {
		
			$scope.indexval = id;
	        
		};
		
		
  $scope.confirmBooking = function(){
			
			
			//alert($('#propertyItemCategoryId').val());
			
			var fdata = "arrivalDate=" + $('#checkin').val()
			+ "&departureDate=" + $('#checkout').val()
			+ "&rooms=" + $('#numberofrooms').val()
			+ "&totalTax=" + $('#tax').val()
			+ "&securityDeposit=" + $('#securityDeposit').val()
			
			
			
			$http(
					{
						method : 'POST',
						data : fdata,
						headers : {
							'Content-Type' : 'application/x-www-form-urlencoded'
						},
						url : 'add-booking'
					}).then(function successCallback(response) {
						
						
						var gdata = "firstName=" + $('#firstName').val()
						+ "&lastName=" + $('#lastName').val()
						+ "&emailId=" + $('#guestEmail').val()
						+ "&phone=" + $('#mobilePhone').val()
						+ "&landline=" + $('#landlinePhone').val()
						+ "&address1=" + $('#firstAddress').val()
						+ "&address1=" + $('#secondAddress').val()
						+ "&city=" + $('#guestCity').val()
						+ "&zipCode=" + $('#guestZipCode').val()
						+ "&countryCode=" + $('#countryCode').val()
						+ "&stateCode=" + $('#stateCode').val()
						
						$http(
					{
						method : 'POST',
						data : gdata,
						headers : {
							'Content-Type' : 'application/x-www-form-urlencoded'
						},
						url : 'add-guest'
						
					}).then(function successCallback(response) {
						
						var bdata = "accommodationId=" + $('#accommodationId').val()
						+ "&adultCount=" + $('#adults').val()
						+ "&childCount=" + $('#child').val()
						+ "&amount=" + $('#price').val()
						+ "&refund=0"
						+ "&statusId=2"
						+ "&tax=0"
					
						
						
						$http(
					{
						method : 'POST',
						data : bdata,
						headers : {
							'Content-Type' : 'application/x-www-form-urlencoded'
						},
						url : 'add-booking-details'
						
					}).then(function successCallback(response) {
						
					alert("booking has been done sucessfully");	
						
						
					
					
					}, function errorCallback(response) {
						
						  
						
					});
						
					}, function errorCallback(response) {
						
						  
						
					});
					
					}, function errorCallback(response) {
				
			  
				// called asynchronously if an error occurs
				// or server returns response with an error status.
			});

		};
		
		
		$scope.range = function(min, max, step){
		    step = step || 1;
		    var input = [];
		    for (var i = min; i <= max; i += step) input.push(i);
		    return input;
		 };

		 $scope.getStates = function() {
             
             var url = "get-states";
             $http.get(url).success(function(response) {
                 //console.log(response);
                 //alert(JSON.stringify(response.data));
                 $scope.states = response.data;
             
             });
         };
         
         $scope.getCountries = function() {
             
             var url = "get-countries";
             $http.get(url).success(function(response) {
                 //console.log(response);
                 //alert(JSON.stringify(response.data));
                 $scope.countries = response.data;
             
             });
         };
        
         
        
		
		 $scope.getPropertyList = function() {
			 
	        	var userId = $('#adminId').val();
	 			var url = "get-user-properties?userId="+userId;
	 			$http.get(url).success(function(response) {
	 			    
	 				$scope.props = response.data;
	 	
	 			});
	 		};
	 		
        $scope.change = function() {
 	   
	        //alert($scope.id);
	        
	        var propertyId = $scope.id;	
 	        var url = "change-user-property?propertyId="+propertyId;
 			$http.get(url).success(function(response) {
 				
 				 window.location = '/ulopms/dashboard'; 
 				//$scope.change = response.data;
 	
 			});
		       
	 		};
	 		
	 	$scope.update = function(rValue) {
	 		
	 		$scope.rValue = rValue;
	 		
	 		
	 		   // console.log($scope.item.n, $scope.item.name)
	    }
			
	    $scope.getPropertyList();
	    
	    
		
		
		$scope.getBooking();
		
		$scope.getStates();
        
        $scope.getCountries();
		//$scope.unread();
		//
		
		
	});

	</script>
  <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
  $( function() {
    $("#bookCheckin" ).datepicker({minDate:0});
    $("#bookCheckout" ).datepicker({minDate:0});
    $("#checkin" ).datepicker({minDate:0});
    $("#checkout" ).datepicker({minDate:0});
  });
  </script>
	  <!-- iCheck 1.0.1 -->
    <script src="plugins/iCheck/icheck.min.js"></script>
    
    <script>
    function bookfix(){
    	 
    	  
    	  
    	  
    	 
    	  var extra =    parseInt(document.getElementById('extraAdult').value);
    	 
    	  var rooms =    parseInt(document.getElementById('numberofrooms').value);
    	
    	  var noofrooms = parseInt(document.getElementById('availableRooms').value);
    	  
    	 
    	  var dftminoccu = parseInt(document.getElementById('adultsAllowed').value);
    	 
    	  
    	  
    	  var dftmaxoccu = parseInt(document.getElementById('maxOccupancy').value);
    	  
    	  var adults =   parseInt(document.getElementById('adults').value);
    	  
    	 
    	  var child =    parseInt(document.getElementById('child').value);
    	  
    	  var orgprice = parseInt(document.getElementById('baseAmount').value);
    	 
    	  
    	  
    	  var dftroom =  1;
    	  
    	  
    	  
    	  /* var orgprice = parseInt(document.getElementById('orgprice').value); */
    	  var checkin = document.getElementById('checkin').value;
    	  var checkout = document.getElementById('checkout').value;
    	  
    	  
    	  var date1 = new Date(checkin);
    	  var date2 = new Date(checkout);
    	  
    	  var timeDiff = Math.abs(date2.getTime() - date1.getTime());
    	  
    	 
    	  var diffDays = parseInt(Math.round(timeDiff / (1000 * 3600 * 24)));
    	  
    	 
    	  
    	  if(diffDays==0){
    	  diffDays = 1;
    	  }
    	  
    	 
    	  
    	  $('#days').val(diffDays);
    	  
    	  /* $('#price').val(orgprice*diffDays*rooms); */
    	  
    	  
    	  
    	  
    	   if(rooms > noofrooms){
    		alert("Running out of maximum rooms available");
    		$('#price').val("");
    		
    		} 
    	   else if(rooms <= noofrooms){
    		 
    		
    		
    		var minoccu =  parseInt(dftminoccu*rooms);
    		var maxoccu =  parseInt(dftmaxoccu*rooms);
    		var useroccu = parseInt(adults+child);
    	  
    		
    		
    	    if(useroccu <= minoccu){
    		 
    		 var tot = orgprice*rooms;
    		 
    		 $('#price').val(tot*diffDays); 
    		
    		}

    	    if(useroccu > minoccu){
    		 
    		if(useroccu > maxoccu){
    		
    		alert("Running out of maximum occupancy please select More Rooms");
    		$('#price').val("");
    		
    		}
    		
    		else if(useroccu <= maxoccu){
    		
    		if(adults>minoccu){
    		
    		
    	     var adultleft = adults-minoccu;
    	     
    	     var bdprice1 =  adultleft*extra; 	 
    	     
    		 var bdprice2 = child*(extra/2);	
    		 var totalbedprice = bdprice1+bdprice2;
    		 alert(totalbedprice);
    		}
    		
    		if(adults<=minoccu){
    		var totalbedprice = Math.abs(useroccu-minoccu)*(extra/2); 	
    		 
    		}
    		
    		var totroomprice = (orgprice*rooms)+totalbedprice;
    		var finalprice =  totroomprice*diffDays;
    		$('#price').val(finalprice);
    		
    		
    		
    		}
    		
    		}	
    		 
    		 
    	 }  
    	 
    	 
    	  }
    
    function bookNext(){
   
    	//alert("hai");
    	$("#addmodal").modal('toggle');
    	$("#guestmodal").modal('show');
		
		
    	
    }
    </script>
       