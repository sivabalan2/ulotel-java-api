<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="s" uri="/struts-tags"%>

   <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Promotions
            <!--<small>Subscription Expiry on {{property[0].subscription[0].expirydate}}</small>-->
          </h1>
          <ol class="breadcrumb">
            <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Manage Promotions</li>
          </ol>
        </section>
		
        <!-- Main content -->
        <section class="content">
       
        	
        	<div class="row">
       
            <div class="col-xs-12">
              <div class="nav-tabs-custom">
              	<ul class="nav nav-tabs">
                  <li class="active"><a href="#fa-icons" data-toggle="tab">Last Minute Promotions</a></li>
                  <li ><a href="#tab-rooms" data-toggle="tab">Room Promotions</a></li>
                  <li ><a href="#tab-nights" data-toggle="tab">Night Promotions</a></li>
                  <li ><a href="#tab-flat" data-toggle="tab">Flat Promotions</a></li>
                  <li ><a href="#tab-all-active" data-toggle="tab">List of Promotions</a></li>
                  <li ><a href="#tab-active" data-toggle="tab">Active Promotions</a></li>
                </ul>
             <div class="tab-content">
                  <!-- Font Awesome Icons -->
                  <div class="tab-pane active" id="fa-icons">
                  <form method="post" theme="simple" name="lastMinutePromotionForm" >
                  <section class="content">
                  	<div class=" form-group col-md-4">
						 <label>Start Date :</label>
						 <div class="input-group date">
		                     <label class="input-group-addon btn" for="lastStartDate">
		                     <span class="fa fa-calendar"></span>
		                     </label>   
		                     <input type="text" id="lastStartDate" class="form-control" name="lastStartDate"  value="" onkeypress="return false;" ng-required="true" placeholder="Start Date" autocomplete="off">
	                	</div>
					</div>
					<div class="form-group col-md-4">
						<label >End Date :</label>
					    <div class="input-group date">
	                	  	<label class="input-group-addon btn" for="lastEndDate">
	                   		<span class="fa fa-calendar"></span>
	              			</label>   
	                  		<input type="text" id="lastEndDate" class="form-control" name="lastEndDate"  value="" onkeypress="return false;" ng-required="true" placeholder="End Date" autocomplete="off"> 
	    	            </div>
					</div>
					<div class="form-group col-md-4">
						<label>Promotion Name :</label>
						<input type="text" class="form-control"  name="lastPromotionName"  value=""  ng-model='lastPromotionName' id="lastPromotionName" ng-required="true" placeholder="Promotion Name">
						<span ng-show="lastMinutePromotionForm.lastPromotionName.$error.pattern" style="color:red">Not a valid Name!</span>
					</div>
					<div class="form-group col-md-4">
						<label>Percentage :</label>
						<input type="text" class="form-control"  name="lastPercentAmount"  id="lastPercentAmount" ng-model="lastPercentAmount" ng-pattern="/^[0-9]/" ng-required="true" maxlength="2" placeholder="Percentage">
					    <span ng-show="lastMinutePromotionForm.lastPercentAmount.$error.pattern" style="color:red">Please Enter Numeric Value</span>
					</div>
					<div class="form-group col-md-4">
						<label>Hours :</label>
						<input type="text" class="form-control"  name="lastHours"  id="lastHours" ng-model="lastHours" ng-pattern="/^[0-9]/" ng-required="true" placeholder="Hours" placeholder="Promotion Hours">
					       <span ng-show="lastMinutePromotionForm.lastHours.$error.pattern" style="color:red">Please Enter Hours</span>
					</div>
					<div class="form-group col-md-4" id="lastcheckedaccommodations">
						  <label>Select Accommodation Type :</label>
						  <select  name="lastPropertyAccommodationId" id="lastPropertyAccommodationId" ng-model="lastPropertyAccommodationId" value="" ng-required="true" class="form-control" >
						  <option value="0">Select Your  Accommodation</option>
						  <option value="0">All</option>
		                  <option ng-repeat="at in accommodations" value="{{at.accommodationId}}">{{at.accommodationType}}</option>
		                  </select>
					</div>
					<!-- <div class="form-group col-md-6" id="lastcheckedaccommodations">
							<label>Select Accommodation Type :</label>
						   <div class="input-group" >

						   	<label class="checkbox-inline"><input type="checkbox"  id="lastCheckAccommodationAll" value="0">All</label>
                          <div ng-repeat="at in accommodations">
                         
                      					<input type="checkbox" name="checkedAccommodation[]"  id="checkedAccommodation" value="{{at.accommodationId}}"  class="flat-red" >&nbsp {{at.accommodationType}}
                    				
                    				</div>
						   </div>

					</div> -->
					<div class="form-group col-md-12" id="lastcheckeddays">
							<label>Days of week :</label>
						   <div class="input-group" >

						   <label class="checkbox-inline"><input type="checkbox"  id="lastCheckAll" name="lastCheckAll" value="0">All Days</label>
                              <label class="checkbox-inline" ng-repeat="d in days">
                      				<input type="checkbox" name="checkedDay[]"  id="checkedDay" value="{{d.dayId}}"  class="flat-red" >&nbsp {{d.daysOfWeek}}
                    			</label>
                    				
						   </div>
					</div>
					
	             
	                      <div class="row fontawesome-icon-list">
	                        <div class="col-md-3 col-sm-4"></div>
	                        <div class="col-md-3 col-sm-4"></div>
	                        <div class="col-md-3 col-sm-4"></div>
	                        <div class="col-md-3 col-sm-4">
	                        	<button type="submit" ng-click="lastMinutePromotionForm.$valid && addGetLastMinutePromotions()" id="lastminute" ng-disabled="lastMinutePromotionForm.$invalid" class="btn btn-primary btngreeen pull-right">Activate</button>
	                        </div>
	                     </div>
	               </section>
                  </form>
                  </div>
                  <div class="tab-pane" id="tab-rooms">
                  <form method="post" theme="simple" name="roomPromotionForm">
                  	<div class=" form-group col-md-3">
						 <label>Start Date :</label>
						 <div class="input-group date">
		                     <label class="input-group-addon btn" for="roomStartDate">
		                     <span class="fa fa-calendar"></span>
		                     </label>   
		                     <input type="text" id="roomStartDate" class="form-control" name="roomStartDate"  value="" onkeypress="return false;" ng-required="true" placeholder="Start Date"  autocomplete="off">
	                	</div>
					</div>
					<div class="form-group col-md-3">
						<label >End Date :</label>
					    <div class="input-group date">
	                	  	<label class="input-group-addon btn" for="roomEndDate">
	                   		<span class="fa fa-calendar"></span>
	              			</label>   
	                  		<input type="text" id="roomEndDate" class="form-control" name="roomEndDate"  value="" onkeypress="return false;" ng-required="true" placeholder="End Date"  autocomplete="off"> 
	    	            </div>
					</div>
					<div class="form-group col-md-3">
						<label>Promotion Name :</label>
						<input type="text" class="form-control"  name="roomPromotionName"  value=""  ng-model='roomPromotionName' id="roomPromotionName" ng-required="true" placeholder="Promotion Name">
						<span ng-show="roomPromotionForm.roomPromotionName.$error.pattern" style="color:red">Not a valid Name!</span>
					</div>
					
					<div class="form-group col-md-3">
						  <label>Select Accommodation Type :</label>
						  <select  name="roomPropertyAccommodationId" id="roomPropertyAccommodationId" ng-model="roomPropertyAccommodationId" ng-required="true" ng-change="getRoomAvailability()" class="form-control" >
						  <option style="display:none" value="0">Select Your  Accommodation</option>
						  <option style="display:none" value="0">All</option>
		                  <option ng-repeat="at in accommodations" value="{{at.accommodationId}}"  >{{at.accommodationType}}</option>
		                  </select>
					</div>
					      <div ng-if="availablities.length>0">
					      <h4 class="page-header">Room Promotion Details </h4>
	                      <div class="col-md-2 form-group ">
	                      	<label>Book Rooms :</label>
	                      	 <div ng-repeat="av in availablities track by $index">
			                	<select  ng-model="ad.value" name="bookRooms" id="bookRooms" class="form-control" ng-required="true" ng-change="addRoom(av.accommodationId,av.accommodationType,av.available,noofAdults,noofChilds,noofInfant,priceAmount,taxAmount,ad.value,$index);roomCheck(av.available,ad.value,$index)">
			                	<option ng-repeat="ad in range(1,av.available)" value="{{ad}}">{{ad}}</option>
			                	</select>
			                </div>
	                      	<%-- <select name="bookRooms" ng-model="bookRooms" id="bookRooms" class="form-control" >
							  <option style="display:none" value="0">0</option>
			                  <option ng-repeat="av in availablities" value="{{av.available}}">{{av.available}}</option>
			                </select> 
			               
	                     	<input type="text" class="form-control"  name="bookRooms"  id="bookRooms" ng-model="bookRooms" ng-pattern="/^[0-9]/" ng-required="true" placeholder="Book Rooms">
	                        <span ng-show="roomPromotionForm.bookRooms.$error.pattern" style="color:red">Not a valid Amount!</span>--%>
	                      </div>
	                      
	                      <div class="col-md-2 form-group ">
	                      	<label>Get Rooms :</label>
	                      	<div ng-repeat="av in availablities track by $index">
			                	<select  ng-model="av.rooms" name="getRooms" id="getRooms" ng-required="true" class="form-control">
			                	<option ng-repeat="ad in range(0,av.rooms)" value="{{ad}}">{{ad}}</option>
			                	</select>
			                </div>
<!-- 	                        <input type="text" class="form-control"  name="getRooms"  id="getRooms" ng-model="getRooms" ng-pattern="/^[0-9]/" ng-required="true" placeholder="Get Rooms"> -->
<%-- 	                        <span ng-show="roomPromotionForm.getRooms.$error.pattern" style="color:red">Not a valid Amount!</span> --%>
	                      </div>
	                      <div class="col-md-2 form-group ">	
	                      	<label>Tariff :</label>
	                        <input type="text" class="form-control"  name="roomBaseAmount"  id="roomBaseAmount" ng-model="roomBaseAmount" ng-pattern="/^[0-9]/" ng-required="true" placeholder="Tariff">
	                        <span ng-show="roomPromotionForm.roomBaseAmount.$error.pattern" style="color:red">Not a valid Amount!</span>
	                      </div>
	                      <div class="col-md-2 form-group ">
	                      	<label>Extra Adult Tariff :</label>
	                        <input type="text" class="form-control"  name="roomExtraAdult"  id="roomExtraAdult" ng-model="roomExtraAdult" ng-pattern="/^[0-9]/" ng-required="true" placeholder="Extra Adult">
	                        <span ng-show="roomPromotionForm.roomExtraAdult.$error.pattern" style="color:red">Not a valid Amount!</span>
	                      </div>
	                      <div class="col-md-2 form-group ">
	                      	<label>Extra Child Tariff :</label>
	                        <input type="text" class="form-control"  name="roomExtraChild"  id="roomExtraChild" ng-model="roomExtraChild" ng-pattern="/^[0-9]/" ng-required="true" placeholder="Extra Child">
	                        <span ng-show="roomPromotionForm.roomExtraChild.$error.pattern" style="color:red">Not a valid Amount!</span>
	                      </div>
					<!-- <div class="form-group col-md-6" id="roomcheckedaccommodations">
							<label >Select Accommodation Type :</label>
						 <div class="input-group" >

						   	<label class="checkbox-inline"><input type="checkbox"  id="roomCheckAccommodationAll" value="0">All</label>
                          <div ng-repeat="at in accommodations">
                          <label class="checkbox-inline">
                      					<input type="checkbox" name="checkedAccommodation[]"  id="checkedAccommodation" value="{{at.accommodationId}}"  class="flat-red" >&nbsp {{at.accommodationType}}
                    				</label>
                    				</div>
						  </div>

					</div> -->
					</div>
					<div class="form-group col-md-12" id="roomcheckeddays">
							<label>Days of week :</label>
						  <div class="input-group" >
						   	<label class="checkbox-inline"><input type="checkbox"  id="roomCheckAll" name="roomCheckAll" value="0">All Days</label>
                       
                           <label class="checkbox-inline" ng-repeat="d in days" >
                      					<input type="checkbox" name="checkedDay[]"  id="checkedDay" value="{{d.dayId}}"  class="flat-red" >&nbsp {{d.daysOfWeek}}
                    				</label>
                    				
                    				</div>
						  
					</div>
 
               
	                      <div class="row fontawesome-icon-list">
	                        <div class="col-md-3 col-sm-4"></div>
	                        <div class="col-md-3 col-sm-4"></div>
	                        <div class="col-md-3 col-sm-4"></div>
	                        <div class="col-md-3 col-sm-4">
	                        	<button type="submit" ng-click="roomPromotionForm.$valid && addGetRoomPromotions()" id="roompromo" ng-disabled="roomPromotionForm.$invalid" class="btn btn-primary btngreeen pull-right">Activate</button>
	                        </div>
	                     </div>
	            
                  </form>
                  </div>
                  <!-- NIGHT PROMOITION -->
                  <div class="tab-pane" id="tab-nights">
                  <form method="post" theme="simple" name="nightsPromotionForm">
                  	<div class=" form-group col-md-3">
						 <label>Start Date :</label>
						 <div class="input-group date">
		                     <label class="input-group-addon btn" for="nightStartDate">
		                     <span class="fa fa-calendar"></span>
		                     </label>   
		                     <input type="text" id="nightStartDate" class="form-control" name="nightStartDate"  value="" onkeypress="return false;" ng-required="true" placeholder="Start Date"  autocomplete="off">
	                	</div>
					</div>
					<div class="form-group col-md-3">
						<label >End Date :</label>
					    <div class="input-group date">
	                	  	<label class="input-group-addon btn" for="nightEndDate">
	                   		<span class="fa fa-calendar"></span>
	              			</label>   
	                  		<input type="text" id="nightEndDate" class="form-control" name="nightEndDate"  value="" onkeypress="return false;" ng-required="true" placeholder="End Date" autocomplete="off"> 
	    	            </div>
					</div>
					<div class="form-group col-md-3">
						<label>Promotion Name :</label>
						<input type="text" class="form-control"  name="nightPromotionName"  value=""  ng-model='nightPromotionName' id="nightPromotionName" ng-required="true" placeholder="Promotion Name">
						<span ng-show="nightsPromotionForm.nightPromotionName.$error.pattern" style="color:red">Not a valid Name!</span>
					</div>
					
					<div class="form-group col-md-3">
						  <label>Select Accommodation Type :</label>
						  <select name="nightPropertyAccommodationId" ng-model="nightPropertyAccommodationId" id="nightPropertyAccommodationId" ng-required="true" ng-change="getNightAvailability()" class="form-control" >
						  <option style="display:none" value="0">Select Your  Accommodation</option>
						   <option style="display:none" value="0">All</option>
		                  <option ng-repeat="at in accommodations" value="{{at.accommodationId}}">{{at.accommodationType}}</option>
		                  </select>
					</div>
					
						<div ng-if="availablities.length>0">
                      <h4 class="page-header">Night Promotion Details </h4>
	                      <div class="col-md-2 form-group " >
	                      <label>Book Nights :</label>
	                      <div ng-repeat="av in availablities track by $index">
			                	<select  ng-model="ad.value" name="bookNights" id="bookNights" class="form-control" ng-required="true" ng-change="addRoom(av.accommodationId,av.accommodationType,av.available,av.diffnights,noofAdults,noofChilds,noofInfant,priceAmount,taxAmount,ad.value,$index);nightCheck(av.diffnights,ad.value,$index)">
			                	<option ng-repeat="ad in range(1,av.diffnights)" value="{{ad}}">{{ad}}</option>
			                	</select>
			                </div>
	                      	<%-- <select name="bookNights" ng-model="bookNights" id="bookNights" class="form-control" >
							  <option style="display:none" value="0">0</option>
			                  <option ng-repeat="av in availablities" value="{{av.available}}">{{av.available}}</option>
			                </select> --%>
<!-- 	                        <input type="text" class="form-control"  name="bookNights"  id="bookNights" ng-model="bookNights" ng-pattern="/^[0-9]/" ng-required="true" placeholder="Book Rooms"> -->
<%-- 	                        <span ng-show="nightsPromotionForm.bookNights.$error.pattern" style="color:red">Not a valid Amount!</span> --%>
	                      </div>
	                      <div class="col-md-2 form-group ">
	                      	<label>Get Nights :</label>
	                      	<div ng-repeat="av in availablities track by $index">
			                	<select  ng-model="av.nights" name="getNights" id="getNights" ng-required="true" class="form-control">
			                	<option ng-repeat="ad in range(0,av.nights)" value="{{ad}}">{{ad}}</option>
			                	</select>
			                </div>
			                
<!-- 	                        <input type="text" class="form-control"  name="getNights"  id="getNights" ng-model="getNights" ng-pattern="/^[0-9]/" ng-required="true" placeholder="Get Nights"> -->
<%-- 	                        <span ng-show="nightsPromotionForm.getNights.$error.pattern" style="color:red">Not a valid Amount!</span> --%>
	                      </div>
	                      <div class="col-md-2 form-group ">
	                      	<label>Tariff :</label>
	                        <input type="text" class="form-control"  name="nightsBaseAmount"  id="nightsBaseAmount" ng-model="nightsBaseAmount" ng-pattern="/^[0-9]/" ng-required="true" placeholder="Tariff">
	                        <span ng-show="nightsPromotionForm.nightsBaseAmount.$error.pattern" style="color:red">Not a valid Amount!</span>
	                      </div>
	                      <div class="col-md-2 form-group ">
	                      	<label>Extra Adult Tariff :</label>
	                        <input type="text" class="form-control"  name="nightsExtraAdult"  id="nightsExtraAdult" ng-model="nightsExtraAdult" ng-pattern="/^[0-9]/" ng-required="true" placeholder="Extra Adult">
	                        <span ng-show="nightsPromotionForm.nightsExtraAdult.$error.pattern" style="color:red">Not a valid Amount!</span>
	                      </div>
	                      <div class="col-md-2 form-group ">
	                      	<label>Extra Child Tariff :</label>
	                        <input type="text" class="form-control"  name="nightsExtraChild"  id="nightsExtraChild" ng-model="nightsExtraChild" ng-pattern="/^[0-9]/" ng-required="true" placeholder="Extra Child">
	                        <span ng-show="nightsPromotionForm.nightsExtraChild.$error.pattern" style="color:red">Not a valid Amount!</span>
	                      </div> 
	                    <!-- <div class="form-group col-md-12" id="nightscheckedaccommodations">
							<label>Select Accommodation Type :</label>
						   <div class="input-group" >

						   	<label class="checkbox-inline"><input type="checkbox"  id="nightsCheckAccommodationAll" value="0">All</label>
                          
                          <label class="checkbox-inline" ng-repeat="at in accommodations">
                      					<input type="checkbox" name="checkedAccommodation[]"  id="checkedAccommodation" value="{{at.accommodationId}}"  class="flat-red" >&nbsp {{at.accommodationType}}
                    				</label>
                    				
						   </div>

					</div> -->
					</div>
					<div class="form-group col-md-12" id="nightscheckeddays">
							<label>Days of week :</label>
						   <div class="input-group" >

						   	<label class="checkbox-inline"><input type="checkbox"  id="nightsCheckAll" name="nightsCheckAll" value="0">All Days</label>
                          
                         <label class="checkbox-inline" ng-repeat="d in days">
                      					<input type="checkbox" name="checkedDay[]"  id="checkedDay" value="{{d.dayId}}"  class="flat-red" >&nbsp {{d.daysOfWeek}}
                    				</label>
                    			
						   </div>

						</div>                      
                   
	                      <div class="row fontawesome-icon-list">
	                        <div class="col-md-3 col-sm-4"></div>
	                        <div class="col-md-3 col-sm-4"></div>
	                        <div class="col-md-3 col-sm-4"></div>
	                        <div class="col-md-3 col-sm-4">
	                        	<button type="submit" ng-click="nightsPromotionForm.$valid && addGetNightPromotions()" id="nightpromo" ng-disabled="nightsPromotionForm.$invalid" class="btn btn-primary btngreeen pull-right">Activate</button>
	                        </div>
	                     </div>
	              
                  </form>
                  </div>
                  <div class="tab-pane" id="tab-flat">
                  <form method="post" theme="simple" name="flatPromotionForm">
              
                  	<div class=" form-group col-md-3">
						 <label>Start Date :</label>
						 <div class="input-group date">
		                     <label class="input-group-addon btn" for="flatStartDate">
		                     <span class="fa fa-calendar"></span>
		                     </label>   
		                     <input type="text" id="flatStartDate" class="form-control" name="flatStartDate"  value="" onkeypress="return false;" ng-required="true" placeholder="Start Date" autocomplete="off">
	                	</div>
					</div>
					<div class="form-group col-md-3">
						<label >End Date :</label>
					    <div class="input-group date">
	                	  	<label class="input-group-addon btn" for="flatEndDate">
	                   		<span class="fa fa-calendar"></span>
	              			</label>   
	                  		<input type="text" id="flatEndDate" class="form-control" name="flatEndDate"  value="" onkeypress="return false;" ng-required="true" placeholder="End Date" autocomplete="off"> 
	    	            </div>
					</div>
					<div class="form-group col-md-3">
						<label>Promotion Name :</label>
						<input type="text" class="form-control"  name="flatPromotionName"  value=""  ng-model='flatPromotionName' id="flatPromotionName" ng-required="true" placeholder="Promotion Name">
						<span ng-show="flatPromotionForm.flatPromotionName.$error.pattern" style="color:red">Not a valid Name!</span>
					</div>
					<div class="form-group col-md-3">
						<label>Percentage :</label>
						<input type="text" class="form-control"  name="flatPercentAmount"  id="flatPercentAmount" ng-model="flatPercentAmount" ng-pattern="/^[0-9]/" ng-required="true" placeholder="Percentage">
					  <span ng-show="flatPromotionForm.flatPercentAmount.$error.pattern" style="color:red">Not a valid Number!</span>
					</div>
					<div class="form-group col-md-12" id="flatcheckeddays">
							<label>Days of week :</label>
						   <div class="input-group" >

						   	<label class="checkbox-inline"><input type="checkbox"  id="flatCheckAll" name="flatCheckAll" value="0">All Days</label>
                         
                           <label class="checkbox-inline" ng-repeat="d in days">
                 					<input type="checkbox" name="checkedDay[]"  id="checkedDay" value="{{d.dayId}}"  class="flat-red" >&nbsp {{d.daysOfWeek}}
               				</label>
               			
						   </div>

					</div>
			
				
	                      <div class="row fontawesome-icon-list">
	                        <div class="col-md-3 col-sm-4"></div>
	                        <div class="col-md-3 col-sm-4"></div>
	                        <div class="col-md-3 col-sm-4"></div>
	                        <div class="col-md-3 col-sm-4">
	                        	<button type="submit" ng-click="flatPromotionForm.$valid && addGetFlatPromotions()" id="flatpromo" ng-disabled="flatPromotionForm.$invalid" class="btn btn-primary btngreeen pull-right">Activate</button>
	                        </div>
	                     </div>
	             
                  </form>
                  </div>
                 <div class="tab-pane" id="tab-all-active">
                  <form method="post" theme="simple" name="activeallpromotions">
                  <div class="box-body table-responsive no-padding" > 
						<table class="table table-hover" id="example1">
						<thead>
						<tr>
							<th>Order</th>
						    <th>Start Date</th>
						   <th>End Date</th>
						   <th>Promotion Name</th>
						   <th>Accommodation Type</th>
							<th>View</th> 
							<th>Delete</th>
							</tr>
						</thead>
						<tbody class="sortable">
								<tr ng-repeat="p in promotions track by $index"  >
								<td>{{p.serialNo}}</td>
							    <td>{{p.startDate}}</td>
								<td>{{p.endDate}}</td>
								<td>{{p.promotionName}}</td>										
								<td>{{p.accommodationType}}</td>
								 <td>
									<a href="#viewmodal" ng-click="getActive(p.promotionId)"  data-toggle="modal"><i  class="fa fa-pencil text-green"></i></a>
								
								</td> 
									<td>
									
<!-- 									<a href="#deletemodal" ng-click="getInactive(p.promotionId)"  data-toggle="modal"><i  class="fa fa-fw fa-trash text-red"></i></a> -->
									<a href="#" ng-click="deletedPromotions(p.promotionId)"  data-toggle="modal"><i  class="fa fa-fw fa-trash text-red"></i></a>
								</td> 
							</tr>
						
						</tbody>
					
						</table>
					</div>
                  </form>
                  </div>
                  <div class="tab-pane" id="tab-active">
                  <form method="post" theme="simple" name="activepromotions" id="activepromotions">
                  <div class="box-body no-padding" > 
                  	<div id="activecheckedpromotions">
						<table class="table table-hover" id="example1">
							<thead>
								<tr>
									<th><h3>Active Promotions</h3></th>
								</tr>
							</thead>
							<tbody class="sortable">
								<tr ng-repeat="p in allPromotions">
									<td><input type="checkbox" name="checkedPromotions[]"  id="checkedPromotions" value="{{p.promotionId}}" ng-checked ="p.status == 'true'">&nbsp; {{p.promotionName}}</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="row fontawesome-icon-list">
                        <div class="col-md-3 col-sm-3"></div>
                        <div class="col-md-3 col-sm-3"></div>
                        <div class="col-md-3 col-sm-3"></div>
                        <div class="col-md-3 col-sm-3">
                        	<button type="submit" ng-click="updateActivePromotions()" id="updatePromotions" name="updatePromotions" class="btn btn-primary btngreeen pull-right">Activate</button>
                        </div>
	                </div>
				  </div>
                  </form>
                  </div>
                  <div class="modal" id="viewmodal">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" id="editbtnclose" class="close" data-dismiss="modal"
									aria-hidden="true">x</button>
								<h4 class="modal-title">View Promotion</h4>
							</div>
							<form name="deletePromotions" id="deletePromotions" >
							<div class="modal-body" ng-repeat="p in viewPromotions">
							
							<div class="form-group col-md-12">
								  <h4><b>{{p.promotionType}}</b></h4>
							 </div>
							  <div class="form-group col-md-4">
							      <label>Accommodation Type :</label>
							      <h5>{{p.accommodationType}}</h5>
							 </div>
								
							<div class=" form-group col-md-4">
								 <label>Start Date :</label>
								 <h5>{{p.startDate}}</h5>
							</div>
							
							<div class=" form-group col-md-4">
								 <label>End Date :</label>
								 <h5>{{p.endDate}}</h5>
							</div>
								 
							<div class="form-group col-md-4">
								<label >Percentage :</label>
								<h5>{{p.promotionPercent}}</h5>
							</div>	
							
							<div class="form-group col-md-4">
								<label >Promotion Hours :</label>
								<h5>{{p.promotionHours}}</h5>
							</div>	
							
							<div class="form-group col-md-4">
								<label >Tariff :</label>
								<h5>{{p.promotionBaseAmount}}</h5>
							</div>	 
							
							<div class="form-group col-md-4">
								<label >Extra Adult :</label>
								<h5>{{p.promotionAdultAmount}}</h5>
							</div>
							
							<div class="form-group col-md-4">
								<label >Extra Child :</label>
								<h5>{{p.promotionChildAmount}}</h5>
							</div>
							
							<div class="form-group col-md-4">
								<label >Book Rooms :</label>
								<h5>{{p.promotionBookRooms}}</h5>
							</div>	
							
							<div class="form-group col-md-4">
								<label >Get Rooms :</label>
								<h5>{{p.promotionGetRooms}}</h5>
							</div>		
							
							<div class="form-group col-md-4">
								<label >Book Nights :</label>
								<h5>{{p.promotionBookNights}}</h5>
							</div>	
							
							<div class="form-group col-md-4">
								<label >Get Nights :</label>
								<h5>{{p.promotionGetNights}}</h5>
							</div> 
							
							<div class="form-group col-md-4">
								<label >Days :</label>
								<h5>{{p.promotionDays}}</h5>
							</div> 
							
							<div class="form-group col-md-4">
								<label >&nbsp;</label>
								<h5>&nbsp;</h5>
							</div>	
							
							<div class="form-group col-md-4">
								<label >&nbsp;</label>
								<h5>&nbsp;</h5>
							</div>	
					
							<div class="modal-footer">
								<a href="#" data-dismiss="modal" class="btn btn-primary btngreeen pull-right">Close</a>
							</div>
							</div>
							</form>
						</div>
						<!-- /.modal-content -->
					</div>
					<!-- /.modal-dalog -->
				</div>
                  <div class="modal" id="deletemodal">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" id="editbtnclose" class="close" data-dismiss="modal"
									aria-hidden="true">x</button>
								<h4 class="modal-title">Delete Promotions</h4>
							</div>
							<form name="deletePromotions" id="deletePromotions" >
							<div class="modal-body" ng-repeat="p in updatePromotions">
							<input type="hidden" class="form-control" value="{{p.promotionId}}" name="updatePromotionId"  id="updatePromotionId" ng-required="true">
							<div class="form-group col-md-12">
								  <h4><b>{{p.promotionType}}</b></h4>
							 </div>
							  <div class="form-group col-md-4">
							      <label>Accommodation Type :</label>
								  <input type="text" class="form-control"  name="updateAccommodationId"  id="updateAccommodationId" value="{{p.accommodationType}}" ng-required="true" placeholder="accommodation">
							 </div>
								
							<div class=" form-group col-md-4">
								 <label>Start Date :</label>
								 <div class="input-group date">
				                     <label class="input-group-addon btn">
				                     <span class="fa fa-calendar"></span>
				                     </label>   
				                     <input type="text" id="updateStartDate" class="form-control" name="updateStartDate"  value="{{p.startDate}}" onkeypress="return false;" ng-required="true">
			                	</div>
							</div>
							<div class="form-group col-md-4">
								<label >End Date :</label>
							    <div class="input-group date">
			                	  	<label class="input-group-addon btn">
			                   		<span class="fa fa-calendar"></span>
			              			</label>   
			                  		<input type="text" id="updateEndDate" class="form-control" name="updateEndDate"  value="{{p.endDate}}" onkeypress="return false;" ng-required="true"> 
			    	            </div>
							</div>	 
								 
					
							<div class="modal-footer">
			
								<a href="#" data-dismiss="modal" class="btn">Close</a>
								<button type="submit" ng-click="deletePromotions()" class="btn btn-primary">Delete</button>
							</div>
							</div>
							</form>
						</div>
						<!-- /.modal-content -->
					</div>
					<!-- /.modal-dalog -->
				</div>
                  </div>
           
              </div>
              </div><!-- /.nav-tabs-custom -->
            </div><!-- /.col -->
            </section><!-- /.content -->
            <input type="hidden" name="userId" id="userId" value="<%=session.getAttribute("userId") %>">
          </div><!-- /.row -->

<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="plugins/fullcalendar/fullcalendar.min.js"></script>
  <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <style type="text/css">
    #ui-datepicker-div
    {
        z-index: 9999999 !important;
    }
</style>
	<script>
	 
	
	var app = angular.module('myApp', ['ngProgress']);
	app.controller('customersCtrl', function($scope, $http,$timeout,ngProgressFactory) {
		
		
          
          var sortableEle;
              sortableEle = $('.sortable').sortable({
              start: $scope.dragStart,
              update: $scope.dragEnd
              
          });
              $("#lastCheckAll").click(function () {
 			     $("#lastcheckeddays :checkbox").not(this).prop('checked', this.checked);
 			    
 			 });
              
              $("#roomCheckAll").click(function () {
  			     $("#roomcheckeddays :checkbox").not(this).prop('checked', this.checked);
  			 });
             
              $("#nightsCheckAll").click(function () {
  			     $("#nightscheckeddays :checkbox").not(this).prop('checked', this.checked);
  			 });
              
              $("#flatCheckAll").click(function () {
  			     $("#flatcheckeddays :checkbox").not(this).prop('checked', this.checked);
  			 });

		
       	$timeout(function(){
      	    // $scope.progressbar.complete();
            $scope.show = true;
            $("#pre-loader").css("display","none");
        }, 2000);

		 
		
		$scope.unread = function() {
		//var notifiurl = "http://localhost:8085/collaborative-workflow/unreadnotifications.action";
		var notifiurl = "unreadnotifications.action";
		$http.get(notifiurl).success(function(response) {
			$scope.latestnoti = response.data;
		});
		};

		 
	       $("#lastStartDate").datepicker({
	            dateFormat: 'MM d, yy',
	            minDate:  0,
	            onSelect: function (formattedDate) {
	                var date1 = $('#lastStartDate').datepicker('getDate'); 
	                var date = new Date( Date.parse( date1 ) ); 
	                date.setDate( date.getDate() );        
	                var newDate = date.toDateString(); 
	                newDate = new Date( Date.parse( newDate ) );   
	                $('#lastEndDate').datepicker("option","minDate",newDate);
	                $timeout(function(){
	                	document.getElementById("lastStartDate").style.borderColor = "LightGray";
	                });
	            }
	        });
	      
	      $("#lastEndDate").datepicker({
	            dateFormat: 'MM d, yy',
	            minDate:  0,
	            onSelect: function (formattedDate) {
	                var date2 = $('#lastEndDate').datepicker('getDate'); 
	                $timeout(function(){
	                	document.getElementById("lastEndDate").style.borderColor = "LightGray";
	                });
	            }
	        });
	      $("#roomStartDate").datepicker({
	            dateFormat: 'MM d, yy',
	            minDate:  0,
	            onSelect: function (formattedDate) {
	                var date1 = $('#roomStartDate').datepicker('getDate'); 
	                var date = new Date( Date.parse( date1 ) ); 
	                date.setDate( date.getDate() );        
	                var newDate = date.toDateString(); 
	                newDate = new Date( Date.parse( newDate ) );   
	                $('#roomEndDate').datepicker("option","minDate",newDate);
	                $timeout(function(){
	                	document.getElementById("roomStartDate").style.borderColor = "LightGray";
	                });
	            }
	        });
	      
	      $("#roomEndDate").datepicker({
	            dateFormat: 'MM d, yy',
	            minDate:  0,
	            onSelect: function (formattedDate) {
	                var date2 = $('#roomEndDate').datepicker('getDate'); 
	                $timeout(function(){
	                	document.getElementById("roomEndDate").style.borderColor = "LightGray";
	                });
	            }
	        });
	      
	      $("#nightStartDate").datepicker({
	            dateFormat: 'MM d, yy',
	            minDate:  0,
	            onSelect: function (formattedDate) {
	                var date1 = $('#nightStartDate').datepicker('getDate'); 
	                var date = new Date( Date.parse( date1 ) ); 
	                date.setDate( date.getDate() );        
	                var newDate = date.toDateString(); 
	                newDate = new Date( Date.parse( newDate ) );   
	                $('#nightEndDate').datepicker("option","minDate",newDate);
	                $timeout(function(){
	                	document.getElementById("nightStartDate").style.borderColor = "LightGray";
	                });
	            }
	        });
	      
	      $("#nightEndDate").datepicker({
	            dateFormat: 'MM d, yy',
	            minDate:  0,
	            onSelect: function (formattedDate) {
	                var date2 = $('#nightEndDate').datepicker('getDate'); 
	                $timeout(function(){
	                	document.getElementById("nightEndDate").style.borderColor = "LightGray";
	                });
	            }
	        });
	      
	      $("#flatStartDate").datepicker({
	            dateFormat: 'MM d, yy',
	            minDate:  0,
	            onSelect: function (formattedDate) {
	                var date1 = $('#flatStartDate').datepicker('getDate'); 
	                var date = new Date( Date.parse( date1 ) ); 
	                date.setDate( date.getDate() );        
	                var newDate = date.toDateString(); 
	                newDate = new Date( Date.parse( newDate ) );   
	                $('#flatEndDate').datepicker("option","minDate",newDate);
	                $timeout(function(){
	                	document.getElementById("flatStartDate").style.borderColor = "LightGray";
	                });
	            }
	        });
	      
	      $("#flatEndDate").datepicker({
	            dateFormat: 'MM d, yy',
	            minDate:  0,
	            onSelect: function (formattedDate) {
	                var date2 = $('#flatEndDate').datepicker('getDate'); 
	                $timeout(function(){
	                	document.getElementById("flatEndDate").style.borderColor = "LightGray";
	                });
	            }
	        });
	      
	     
	      
	      $scope.addGetLastMinutePromotions = function(){
	    	  $("#lastminute").prop('disabled', true);
			    var checkbox_days_value = "";
				    $("#lastcheckeddays :checkbox").each(function () {
				        var ischecked = $(this).is(":checked");
				        if (ischecked) {
				        	checkbox_days_value += $(this).val() + ",";
				        }
				    });
				    
			    
		     	
		     	var fdata = "checkedDays=" + checkbox_days_value
		     	+ "&propertyAccommodationId=" + $('#lastPropertyAccommodationId').val()
				+ "&strStartDate=" + $('#lastStartDate').val()
				+ "&strEndDate=" + $('#lastEndDate').val()
				+ "&promotionName=" + $('#lastPromotionName').val()
				+ "&percentage="+ $('#lastPercentAmount').val()
				+ "&promotionHours="+ $('#lastHours').val()
				+ "&promotionType=L"
				var checkedDayslength = $('[name="checkedDay[]"]:checked').length;
		     	var checkedAll=$('[name="lastCheckAll"]:checked').length;
				
				var isChecked=false;
				if(document.getElementById('lastStartDate').value==""){
		 			 document.getElementById('lastStartDate').focus();
		 			 document.getElementById("lastStartDate").style.borderColor = "red";
		 		 }else if(document.getElementById('lastEndDate').value==""){
		 			 document.getElementById('lastEndDate').focus();
		 			document.getElementById("lastEndDate").style.borderColor = "red";
		 		 }else if(document.getElementById('lastStartDate').value!="" && document.getElementById('lastEndDate').value!=""){
		 		
		 			 
					if(checkedDayslength>0 || checkedAll>0 ){
						if(( checkedDayslength == 7 && checkedAll>0)){
							isChecked=true;
						}else if(checkedDayslength>0 && checkedDayslength<=6 && checkedAll==0){
							isChecked=true;
						} else{
							alert('Tick to continue all days')
						}
						
						if(isChecked){
							$http(
									{
										method : 'POST',
										data : fdata,
										headers : {
											'Content-Type' : 'application/x-www-form-urlencoded'
										},
										url: 'add-promotions'
										
									}).then(function successCallback(response) {
										
										var promotionchk=response.data.data[0].check;
										
										if(promotionchk=="true"){
	// 										window.location.reload();
											alert('Already have an Promotions');
										}
										else if(promotionchk=="false"){
										
										var gdata = "checkedDays=" + checkbox_days_value
								     	+ "&strStartDate=" + $('#lastStartDate').val()
										+ "&strEndDate=" + $('#lastEndDate').val()
										+ "&promotionName=" + $('#lastPromotionName').val()
										+ "&percentage="+ $('#lastPercentAmount').val()
										+ "&promotionHours="+ $('#lastHours').val()
										+ "&promotionType=L"
										
										$http(
									    {
										method : 'POST',
										data : gdata,
										//data : '{"data":' +JSON.stringify($scope.roomsSelected) + '}',
										
										headers : {
											'Content-Type' : 'application/x-www-form-urlencoded'
											//'Content-Type' : 'application/x-www-form-urlencoded'
										},
										url: 'add-promotion-details'
									   }).then(function successCallback(response) {
										   window.location.reload();
										
									    alert("promotions added successfully");
									    $("#lastminute").prop('disabled', false); 
									    
									   }, function errorCallback(response) {
										
										  
										
									});
										
								}		
										
							}, function errorCallback(response) {
								
							  
								
							});
						}
					
					
					} else{
						alert('please check atleast one day')
					}
		 		 }

			};
	     
			$scope.addGetRoomPromotions = function(){
				  $("#roompromo").prop('disabled', true);
			    var checkbox_value = "";
				    $("#roomcheckeddays :checkbox").each(function () {
				        var ischecked = $(this).is(":checked");
				        if (ischecked) {
				            checkbox_value += $(this).val() + ",";
				        }
				    });
				
			   
		     	
		     	var fdata = "checkedDays=" + checkbox_value
		     	+ "&propertyAccommodationId=" + $('#roomPropertyAccommodationId').val()
				+ "&strStartDate=" + $('#roomStartDate').val()
				+ "&strEndDate=" + $('#roomEndDate').val()
				+ "&promotionName=" + $('#roomPromotionName').val()
				+ "&bookRooms="+ $('#bookRooms').val()
		     	+ "&getRooms="+ $('#getRooms').val()
		     	+ "&promotionBaseAmount="+ $('#roomBaseAmount').val()
		     	+ "&promotionExtraAdult="+ $('#roomExtraAdult').val()
		     	+ "&promotionExtraChild="+ $('#roomExtraChild').val()
		     	+ "&promotionType=R"
				
				
		     	var checkedDayslength = $('[name="checkedDay[]"]:checked').length;
		     	var checkedAll=$('[name="roomCheckAll"]:checked').length;
				
				var isChecked=false;
				if(document.getElementById('roomStartDate').value==""){
		 			 document.getElementById('roomStartDate').focus();
		 			document.getElementById("roomStartDate").style.borderColor = "red";
		 		 }else if(document.getElementById('roomEndDate').value==""){
		 			 document.getElementById('roomEndDate').focus();
		 			document.getElementById("roomEndDate").style.borderColor = "red";
		 		 }else if(document.getElementById('roomStartDate').value!="" && document.getElementById('roomEndDate').value!=""){
		 			
						if(checkedDayslength>0 || checkedAll>0 ){
							if(( checkedDayslength == 7 && checkedAll>0)){
								isChecked=true;
							}else if(checkedDayslength>0 && checkedDayslength<=6 && checkedAll==0){
								isChecked=true;
							} else{
								alert('Tick to continue all days')
							}
						
						if(isChecked){
						$http(
								{
									method : 'POST',
									data : fdata,
									headers : {
										'Content-Type' : 'application/x-www-form-urlencoded'
									},
									url: 'add-promotions'
								}).then(function successCallback(response) {
									
									var promotionchk=response.data.data[0].check;
									
									if(promotionchk=="true"){
		// 								window.location.reload();
										alert('Already have an Promotions');
									}
									else if(promotionchk=="false"){
									var gdata = "checkedDays=" + checkbox_value
									+ "&strStartDate=" + $('#roomStartDate').val()
									+ "&strEndDate=" + $('#roomEndDate').val()
									+ "&promotionName=" + $('#roomPromotionName').val()
									+ "&bookRooms="+ $('#bookRooms').val()
							     	+ "&getRooms="+ $('#getRooms').val()
							     	+ "&promotionBaseAmount="+ $('#roomBaseAmount').val()
							     	+ "&promotionExtraAdult="+ $('#roomExtraAdult').val()
							     	+ "&promotionExtraChild="+ $('#roomExtraChild').val()
							     	+ "&promotionType=R"
									
									$http(
								    {
									method : 'POST',
									data : gdata,
									//data : '{"data":' +JSON.stringify($scope.roomsSelected) + '}',
									
									headers : {
										'Content-Type' : 'application/x-www-form-urlencoded'
										//'Content-Type' : 'application/x-www-form-urlencoded'
									},
									url: 'add-promotion-details'
								   }).then(function successCallback(response) {
									   window.location.reload();
									
								    alert("promotions added successfully");
								    $("#roompromo").prop('disabled', false);
								    
								   }, function errorCallback(response) {
									
									  
									
								});
									
							}		
									
						}, function errorCallback(response) {
							
						  
							
						});
						}
						}else {
							alert('please check atleast one day')
						}
		 		 }
				
			};
			
			$scope.addGetNightPromotions = function(){
				$("#nightpromo").prop('disabled', true);
			    var checkbox_value = "";
				    $("#nightscheckeddays :checkbox").each(function () {
				        var ischecked = $(this).is(":checked");
				        if (ischecked) {
				            checkbox_value += $(this).val() + ",";
				        }
				    });
				    
			    
		     	
		     	var fdata = "checkedDays=" + checkbox_value
		     	+ "&propertyAccommodationId=" + $('#nightPropertyAccommodationId').val()
				+ "&strStartDate=" + $('#nightStartDate').val()
				+ "&strEndDate=" + $('#nightEndDate').val()
				+ "&promotionName=" + $('#nightPromotionName').val()
				+ "&bookNights="+ $('#bookNights').val()
		     	+ "&getNights="+ $('#getNights').val()
		     	+ "&promotionBaseAmount="+ $('#nightsBaseAmount').val()
		     	+ "&promotionExtraAdult="+ $('#nightsExtraAdult').val()
		     	+ "&promotionExtraChild="+ $('#nightsExtraChild').val()
		     	+ "&promotionType=N"
				
				
		     	var checkedDayslength = $('[name="checkedDay[]"]:checked').length;
		     	var checkedAll=$('[name="nightsCheckAll"]:checked').length;
				
				var isChecked=false;
				
				if(document.getElementById('nightStartDate').value==""){
		 			 document.getElementById('nightStartDate').focus()
		 			document.getElementById("nightStartDate").style.borderColor = "red";
		 		 }else if(document.getElementById('nightEndDate').value==""){
		 			 document.getElementById('nightEndDate').focus();
		 			document.getElementById("nightEndDate").style.borderColor = "red";
		 		 }else if(document.getElementById('nightStartDate').value!="" && document.getElementById('nightEndDate').value!=""){
		 			
						if(checkedDayslength>0 || checkedAll>0 ){
							if(( checkedDayslength == 7 && checkedAll>0)){
								isChecked=true;
							}else if(checkedDayslength>0 && checkedDayslength<=6 && checkedAll==0){
								isChecked=true;
							} else{
								alert('please tick to continue all days')
							}
							
						if(isChecked){
						
						$http(
								{
									method : 'POST',
									data : fdata,
									headers : {
										'Content-Type' : 'application/x-www-form-urlencoded'
									},
									url: 'add-promotions'
								}).then(function successCallback(response) {
									var promotionchk=response.data.data[0].check;
									
									if(promotionchk=="true"){
		// 								window.location.reload();
										alert('Already have an Promotions');
									}
									else if(promotionchk=="false"){
									
									var gdata = "checkedDays=" + checkbox_value
									+ "&strStartDate=" + $('#nightStartDate').val()
									+ "&strEndDate=" + $('#nightEndDate').val()
									+ "&promotionName=" + $('#nightPromotionName').val()
									+ "&bookNights="+ $('#bookNights').val()
							     	+ "&getNights="+ $('#getNights').val()
							     	+ "&promotionBaseAmount="+ $('#nightsBaseAmount').val()
							     	+ "&promotionExtraAdult="+ $('#nightsExtraAdult').val()
							     	+ "&promotionExtraChild="+ $('#nightsExtraChild').val()
							     	+ "&promotionType=N"
									
									$http(
								    {
									method : 'POST',
									data : gdata,
									//data : '{"data":' +JSON.stringify($scope.roomsSelected) + '}',
									
									headers : {
										'Content-Type' : 'application/x-www-form-urlencoded'
										//'Content-Type' : 'application/x-www-form-urlencoded'
									},
									url: 'add-promotion-details'
								   }).then(function successCallback(response) {
									   window.location.reload();
									
								    alert("promotions added successfully");
								    $("#nightpromo").prop('disabled', false);
								    
								   }, function errorCallback(response) {
									
									  
									
								});
									
								}	
									
						}, function errorCallback(response) {
							
						  
							
						});
						
					}
					}else {
						alert('please check atleast one day')
					}
		 		 }

			};
			
			$scope.addGetFlatPromotions = function(){
				$("#flatpromo").prop('disabled', true);
			    var checkbox_value = "";
				    $("#flatcheckeddays :checkbox").each(function () {
				        var ischecked = $(this).is(":checked");
				        if (ischecked) {
				            checkbox_value += $(this).val() + ",";
				        }
				    });
				    
		     	
		     	var fdata = "checkedDays=" + checkbox_value
				+ "&strStartDate=" + $('#flatStartDate').val()
				+ "&strEndDate=" + $('#flatEndDate').val()
				+ "&promotionName=" + $('#flatPromotionName').val()
				+ "&percentage="+ $('#flatPercentAmount').val()
				+ "&promotionType=F"
				
				var checkedDayslength = $('[name="checkedDay[]"]:checked').length;
		     	var checkedAll=$('[name="flatCheckAll"]:checked').length;
				
				var isChecked=false;
				
				if(document.getElementById('flatStartDate').value==""){
		 			 document.getElementById('flatStartDate').focus();
		 			document.getElementById("flatStartDate").style.borderColor = "red";
		 		 }else if(document.getElementById('flatEndDate').value==""){
		 			 document.getElementById('flatEndDate').focus();
		 			document.getElementById("flatEndDate").style.borderColor = "red";
		 		 }else if(document.getElementById('flatStartDate').value!="" && document.getElementById('flatEndDate').value!=""){
		 			
						if(checkedDayslength>0 || checkedAll>0 ){
							if(( checkedDayslength == 7 && checkedAll>0)){
								isChecked=true;
							}else if(checkedDayslength>0 && checkedDayslength<=6 && checkedAll==0){
								isChecked=true;
							} else{
								alert('please tick to continue all days')
							}
							
						if(isChecked){
						
						
						$http(
								{
									method : 'POST',
									data : fdata,
									headers : {
										'Content-Type' : 'application/x-www-form-urlencoded'
									},
									url: 'add-promotions'
								}).then(function successCallback(response) {
									var promotionchk=response.data.data[0].check;
									
									if(promotionchk=="true"){
		// 								window.location.reload();
										alert('Already have an Promotions');
									}
									else if(promotionchk=="false"){
										var gdata = "checkedDays=" + checkbox_value
										+ "&strStartDate=" + $('#flatStartDate').val()
										+ "&strEndDate=" + $('#flatEndDate').val()
										+ "&PromotionName=" + $('#flatPromotionName').val()
										+ "&Percentage="+ $('#flatPercentAmount').val()
										+ "&promotionType=F"
										
										$http(
									    {
										method : 'POST',
										data : gdata,
										//data : '{"data":' +JSON.stringify($scope.roomsSelected) + '}',
										
										headers : {
											'Content-Type' : 'application/x-www-form-urlencoded'
											//'Content-Type' : 'application/x-www-form-urlencoded'
										},
										url: 'add-promotion-details'
									   }).then(function successCallback(response) {
										   window.location.reload();
										
									    alert("promotions added successfully");
									    $("#flatpromo").prop('disabled', false);
									    
									   }, function errorCallback(response) {
										
										  
										
									});
								}
									
						}, function errorCallback(response) {
							
						  
							
						});
						
						}
					}else {
						alert('please check atleast one day')
					}
		 		 }

			};
			$scope.updateActivePromotions=function(rowid){
				/* $('input.checkedpromotions').on('change', function() {
					    $('input.checkedpromotions').not(this).prop('checked', false);  
					}); */
					
				 var checkbox_promotions_ids = "";
				    $("#activecheckedpromotions :checkbox").each(function () {
				        var ischecked = $(this).is(":checked");
				        if (ischecked) {
				        	checkbox_promotions_ids += $(this).val() + ",";
				        }
				    });
				  var promoIdLength=checkbox_promotions_ids.length;
				  if(promoIdLength==0){
					  checkbox_promotions_ids=0;
				  }
				var fdata = "&updatePromotionIds="+ checkbox_promotions_ids;
				
				$http(
						{
							method : 'POST',
							data : fdata,
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							url: 'update-promotions'
						}).then(function successCallback(response) {
							var promotionchk=response.data.data[0].check;
							
							if(promotionchk=="true"){
								alert('Active only one property promotion or one category promotion');
							}else if(promotionchk=="false"){
								window.location.reload();
							}
							
						   }, function errorCallback(response) {
							
							
						});
				
			};
			$scope.deletedPromotions=function(rowid){
				var fdata = "&updatePromotionId="+ rowid
				var deletePromotionsCheck=confirm("Do u want to delete the promotion?");
				if(deletePromotionsCheck){
					$http(
							{
								method : 'POST',
								data : fdata,
								headers : {
									'Content-Type' : 'application/x-www-form-urlencoded'
								},
								url: 'get-promotions-disable'
							}).then(function successCallback(response) {
								window.location.reload();
							   }, function errorCallback(response) {
								
								
							});
				}
				else{
					
				}
				
			};
			
			
			$scope.getRoomAvailability = function(){
				
				var startdate=$('#roomStartDate').val();
				var enddate=$('#roomEndDate').val();
				var accommId=$('#roomPropertyAccommodationId').val();
				
				if(enddate=="" && startdate==""){
					alert('please fill the date fields');
				}
				if($('#roomStartDate').val()!="" && $('#roomEndDate').val()!="" && $('#roomPropertyAccommodationId').val()!=0){
					
					var fdata = "&strStartDate=" + $('#roomStartDate').val()
					+ "&strEndDate=" + $('#roomEndDate').val()
					+ "&accommodationTypeId=" + $('#roomPropertyAccommodationId').val()
		             
					$http(
							{
								method : 'POST',
								data : fdata,
								headers : {
									'Content-Type' : 'application/x-www-form-urlencoded'
								},
								url : 'get-promotion-room-availability'
							}).success(function(response) {
								
								$scope.availablities = response.data;
								var indx = 0;
		                       
		                          for (var i = 0; i < $scope.availablities.length; i++) 
		                             {
			                             var dt = $scope.availablities[i];
			                             $scope.addRow(dt.accommodationId,dt.accommodationType,dt.baseAmount,dt.arrivalDate, dt.departureDate,dt.rooms,dt.noOfAdults,dt.noOfChild,dt.noOfInfant,dt.minOccupancy,dt.extraAdult,dt.extraInfant,dt.extraChild,dt.available,dt.diffnights,i,dt.tax);    
		                             }  
								
							});
				}
				
				
				

			};
			
			$scope.getNightAvailability = function(){
				
				var startdate=$('#nightStartDate').val();
				var enddate=$('#nightEndDate').val();
				var accommId=$('#nightPropertyAccommodationId').val();
				
				if(enddate=="" && startdate==""){
					alert('please fill the date fields');
				}
				if($('#nightStartDate').val()!="" && $('#nightEndDate').val()!="" && $('#nightPropertyAccommodationId').val()!=0){
					
					var fdata = "&strStartDate=" + $('#nightStartDate').val()
					+ "&strEndDate=" + $('#nightEndDate').val()
					+ "&accommodationTypeId=" + $('#nightPropertyAccommodationId').val()
		             
					$http(
							{
								method : 'POST',
								data : fdata,
								headers : {
									'Content-Type' : 'application/x-www-form-urlencoded'
								},
								url : 'get-promotion-room-availability'
							}).success(function(response) {
								
								$scope.availablities = response.data;
								var indx = 0;
		                       
		                          for (var i = 0; i < $scope.availablities.length; i++) 
		                             {
			                             var dt = $scope.availablities[i];
			                             $scope.addRow(dt.accommodationId,dt.accommodationType,dt.baseAmount,dt.arrivalDate, dt.departureDate,dt.rooms,dt.noOfAdults,dt.noOfChild,dt.noOfInfant,dt.minOccupancy,dt.extraAdult,dt.extraInfant,dt.extraChild,dt.available,dt.diffnights,i,dt.tax);    
		                             }  
								
							});
				}
				
				
				

			};
		$scope.getDay = function() {

			var url = "get-day";
			$http.get(url).success(function(response) {
			    //console.log(response);
				$scope.days = response.data;
	
			});
		};	
			
			
		$scope.getAccommodations = function() {

			var url = "get-accommodations";
			$http.get(url).success(function(response) {
			    //console.log(response);
				$scope.accommodations = response.data;
	
			});
		};
		
		$scope.getPromotions = function() {

			var url = "get-promotions";
			$http.get(url).success(function(response) {
			    console.log(response);
				$scope.promotions = response.data;
	
			});
		};
		$scope.getAllPromotions = function() {

			var url = "get-all-promotions";
			$http.get(url).success(function(response) {
			    console.log(response);
				$scope.allPromotions = response.data;
	
			});
		};
		$scope.getInactive = function(rowid){
			var url="get-inactive-promotions?updatePromotionId="+rowid;
			$http.get(url).success(function(response) {
			    console.log(response);
				$scope.updatePromotions = response.data;
	
			});
		};
		
		$scope.getActive = function(rowid){
			var url="get-active-promotions?updatePromotionId="+rowid;
			$http.get(url).success(function(response) {
			    console.log(response);
				$scope.viewPromotions = response.data;
	
			});
		};
			
		$scope.getAccommodations();
		$scope.getDay();
		$scope.getPromotions();
		$scope.getAllPromotions();
		
		$scope.range = function(min, max, step){
		    step = step || 1;
		    var input = [];
		    for (var i = min; i <= max; i += step) input.push(i);
		    return input;
		 };
		 
		 
		
		 $scope.roomsSelected = [];
       
         $scope.addRow = function(id,type,baseAmount,arrival, departure,rooms,adultsAllow,childAllow,infantAllow,minOccu,extraAdult,extraChild,tax,available,indx){
         if(available == 0){
			 console.log("Disable");
         }
         else{
        	 var type = type.replace(/\s/g,'');
	         var id = parseInt(id);
	         var minOccu = parseInt(minOccu);
	         var extraAdult = parseInt(extraAdult);
	         var extraChild = parseInt(extraChild);         
	         var adultsAllow = parseInt(adultsAllow);
	         var childAllow = parseInt(childAllow);        
	         var amount = parseFloat(baseAmount);
	         var rooms = parseInt(rooms);
	         var date1 = new Date(arrival);
	         var date2 = new Date(departure);
	         var timeDiff = Math.abs(date2.getTime() - date1.getTime());
	         var diffDays = parseInt(Math.round(timeDiff / (1000 * 3600 * 24)));
	         var randomNo = Math.random();         
	         var taxes = tax * diffDays;
         	 var total = (diffDays*rooms*amount);
        	 var newData = {'arrival':arrival,'indx':indx,'departure':departure,'accommodationId':id,'accommodationType':type,'available':available, 'rooms': rooms,'diffDays':diffDays, 'baseAmount':amount, 'total':total ,'adultsAllow':adultsAllow,'childAllow':childAllow,'minOccu':minOccu,'extraAdult':extraAdult,'extraChild':extraChild,'adultsCount':0,'childCount':0,'infantCount':0,'refund':0,'statusId':2,'tax':taxes,'randomNo':randomNo};
             $scope.roomsSelected.push(newData);
        	 console.log($scope.roomsSelected);
        	
        	 $scope.availablities[indx].available = $scope.availablities[indx].available;
        	 return false;
          }	
        };
        
        $scope.roomCheck=function(available,bookrooms,index){	
            if(available == 0){
   			 console.log("Disable");
            }
            else{
   	         var rooms = parseInt(available)-parseInt(bookrooms);
           	 var newData = {'available':available, 'rooms': rooms};
             $scope.roomsSelected.push(newData);
           	 console.log($scope.roomsSelected);
           	
           	 $scope.availablities[index].rooms = rooms;
           	 
           	 return false;
             }	
           };
           
        $scope.nightCheck=function(available,booknights,index){	
            if(available == 0){
   			 console.log("Disable");
            }
            else{
   	         var nights = parseInt(available)-parseInt(booknights);
           	 var newData = {'available':available, 'nights': nights};
             $scope.roomsSelected.push(newData);
           	 console.log($scope.roomsSelected);
           	
           	 $scope.availablities[index].nights = nights;
           	 
           	 return false;
             }	
           };  
	});

	
	

	</script>
   