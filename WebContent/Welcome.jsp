<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<link rel="stylesheet" href="css/formelements.css">
<link rel='stylesheet' href='fullcalendar/fullcalendar.css' />
<script src='fullcalendar/lib/jquery.min.js'></script>
<script src='fullcalendar/lib/moment.min.js'></script>
<script src='fullcalendar/fullcalendar.js'></script>
<title>Welcome sample development</title>  
<script type="text/javascript">
$(document).ready(function() {
$('#calendar').fullCalendar({
	header: {
		left: 'prev,next today',
		center: 'title',
		right: 'month,basicWeek,basicDay'
	},
	eventSources: [

        // your event source
        {
            url: '/95/calendar-view1.action',
            type: 'GET',
            error: function() {
                alert('there was an error while fetching events!');
            }
        }       

    ]	
    // any other sources...
});
});
</script>
<style>

	body {
		
		font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
		font-size: 14px;
	}

	#calendar {
		max-width: 900px;
		margin: 0 auto;
	}

</style>
</head>


<body>
<table style="width:100%;">
	<tr>
		<td colspan="2">
			<table style="width:100%;">
			<tr>
					<td align="left"><div class="frmName">Dashboard</div></td>
					<td align="right">
					</td>
				</tr>
				
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top">
			<div id='calendar'></div>
    	</td>
		
	</tr>

</table>

</body>
</html>
