<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="s" uri="/struts-tags"%>
<style>
    .pac-container {
        z-index: 10000 !important;
    }
</style>

   <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Location
          </h1>
         <ol class="breadcrumb">
            <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Location</li>
          </ol>
        </section>
		
        <!-- Main content -->
        <section class="content">
        
        	<div class="row">
            	<div class="col-xs-12" ng-if="googleLocations.length>0">
						<div class="box">
							<div class="box-header">
								<h3 class="box-title">Location</h3>
							</div>
							
							
							<!-- /.box-header -->
							<div class="box-body table-responsive no-padding" > 
								<table class="table table-hover" id="example1">
									<tr>
										<th>Order</th>
										<th>Location </th>
										<th>Edit</th>
										<th>Delete</th>
										<th>User Name</th>
									</tr>
									<tr ng-repeat="gloc in googleLocations">
										<td>{{gloc.serialNo}}</td>
										<td>{{gloc.googleLocationName}}</td>
										<td>
											<a href="#editmodal" ng-click="getGoogleLocation(gloc.googleLocationId)"  data-toggle="modal"><i  class="fa fa-pencil text-green"></i></a>
										</td>
										<td>
											<a href="#deletemodal" ng-click="deleteLocation(gloc.googleLocationId)" data-toggle="modal" ><i  class="fa fa-fw fa-trash text-red" ></i></a>
										</td>
										<td>{{gloc.createdBy}}</td>
									</tr>
									<tr>
									</tr>
								</table>
							</div>
							<!-- /.box-body -->
							
							
							<div class="box-footer clearfix">
								 <ul class="pagination pagination-sm no-margin pull-right">
									<a class="btn btn-primary " href="#addmodal" data-toggle="modal" >Add Location</a>
								</ul> 
							</div>
							<div class="box-footer clearfix">
								<ul class="pagination pagination-sm"></ul>
								<div class="simple-pagination pull-right" >   
									<p class="simple-pagination__items">Showing {{locationCount[0].count}} out of {{settings.pageLimit}}</p>
									<p><button ng-click="previousPage()" ng-disabled="settings.currentPage <= 0" class="simple-pagination__button simple-pagination__button--prev"> &#10094;          
									</button><span class="simple-pagination__pages">{{settings.currentPage + 1}} of {{getTotalPages()}}</span>
									<button ng-click="nextPage()" ng-disabled="settings.currentPage === (getTotalPages() - 1)" class="simple-pagination__button simple-pagination__button--next">&#10095;</button></p>
									<a href="" ng-click="limited(10)" ng-class="active">{{10}}</a> |
									<span><a href="" ng-click="all(locationCount[0].count)" ng-class="active">All</a></span> 
								</div>
						   </div> 
						</div>
						<!-- /.box -->
					</div>	
         	</div>
          
        </section><!-- /.content -->
      	<input type="hidden" value ="<%=session.getAttribute("locationId") %>" id="locationId" name="locationId" >
      	  <!--  <input type="button" value = "Submit" ng-click="GetLocation()"/> -->
      	<div class="modal" id="addmodal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" id="btnclose" class="close" data-dismiss="modal"
							aria-hidden="true">x</button>
						<h4 class="modal-title">Add Google Location</h4>
					</div>
					 <form method="post" theme="simple" name="addloc">
					 <section>
					<div class="modal-body" >
						<div class="form-group col-md-12">
							<label for="googleLocationName">Google Location Name<span style="color:red">*</span></label>
							<input type="text" id="googleLocationName" name="googleLocationName" class="form-control"  placeholder="Enter a location" />
							<input type="hidden" id="googleLocationLatitude" name="googleLocationLatitude" class="form-control"  placeholder="Enter a location" />
							<input type="hidden" id="googleLocationLongitude" name="googleLocationLongitude" class="form-control"  placeholder="Enter a location" />
							<input type="hidden" id="googleLocationPlaceId" name="googleLocationPlaceId" class="form-control"  placeholder="Enter a location" />
						<%-- <input type="text" class="form-control"  name="locationName"  id="locationName"  placeholder="Location Name" value="" ng-model='locationName' ng-pattern="^/^[A-Za-z\d\s]+$/" ng-required="true">
						    <span ng-show="addloc.locationName.$error.pattern" style="color:red">Not a Location Name!</span> --%>
						 </div>
						 <div class="form-group col-md-12">
							<label for="googleLocationDisplayName">Google Location Display Name<span style="color:red">*</span></label>
							<input type="text" class="form-control"  name="googleLocationDisplayName"  id="googleLocationDisplayName"  placeholder="Display Location Name" value="" ng-model='googleLocationDisplayName' ng-pattern="^/^[A-Za-z\d\s]+$/" ng-required="true">
						    <span ng-show="addloc.googleLocationDisplayName.$error.pattern" style="color:red">Not a Location Name!</span>
						</div>
						<div class="form-group col-md-12">
							<label for="googleLocationDescription">Google Location Description<span style="color:red">*</span></label>
							<input type="text" class="form-control"  name="googleLocationDescription"  id="googleLocationDescription"  placeholder="Location Description" value="" ng-model='googleLocationDescription' ng-pattern="^/^[A-Za-z\d\s]+$/" ng-required="true">
						    <span ng-show="addloc.googleLocationDescription.$error.pattern" style="color:red">Not a Location Name!</span>
						</div>
						<div class="form-group col-md-12">
							<label for="googleLocationUrl">Google Location URL<span style="color:red">*</span></label>
							<input type="text" class="form-control"  name="googleLocationUrl"  id="googleLocationUrl"  placeholder="Location URL" value="" ng-model='googleLocationUrl' ng-pattern="^/^[A-Za-z\d\s]+$/" ng-required="true">
						    <span ng-show="addloc.googleLocationUrl.$error.pattern" style="color:red">Not a Location URL!</span>
						</div>
						<div class="form-group col-md-6">
							<label for="child_included_rate">Google Location Image</label>
							<input type="file" class="form-control"   file-input="files" name="photo" id="photo">
							<span style="color:red">upload only(300 * 400)</span>
<!-- 							<div id="validimage" name="validimage" style="color: red"><label>Not Image Format!!</label></div> -->
						</div>
						<div class="form-group" >
						   <div class="input-group" >
						  		<label>&nbsp;&nbsp;</label>
						  		<label>&nbsp;&nbsp;</label>
						  		<label>&nbsp;&nbsp;</label>
						  		<label>&nbsp;&nbsp;</label>
						  		<input type="hidden" class="form-control"  >
						   </div>

						</div>
					</div>
					</section>
					<section>
						<div class="modal-footer">
							<a href="#" data-dismiss="modal" class="btn btn-danger">Close</a>
							<button ng-click="addloc.$valid && addGoogleLocation()" ng-disabled="addloc.$invalid" class="btn btn-primary">Save</button>
						</div>
					</section>
					</form>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dalog -->
		</div>
			
							
		<div class="modal" id="editmodal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" id="editbtnclose" class="close" data-dismiss="modal"
							aria-hidden="true">x</button>
						<h4 class="modal-title">Edit Location</h4>
					</div>
					 	 <form method="post" theme="simple" name="editloc">
					 	 <section>
						<div class="modal-body">
							<input type="hidden" value="{{googleLocation[0].googleLocationId}}" id="editGoogleLocationId" name="editGoogleLocationId">
						    <div class="form-group col-md-12">
								<label>Google Location Name<span style="color:red">*</span></label>
								<input type="text" class="form-control"  name="editGoogleLocationName"  id="editGoogleLocationName" value="{{googleLocation[0].googleLocationName}}" ng-required="true"  disabled>
							</div>
							<div class="form-group col-md-12">
								<label>Google Place Id<span style="color:red">*</span></label>
								<input type="text" class="form-control"  name="editGoogleLocationPlaceId"  id="editGoogleLocationPlaceId" value="{{googleLocation[0].googleLocationPlaceId}}" ng-required="true"  disabled>
							</div>
							<div class="form-group col-md-12">
								<label>Google Location Display Name<span style="color:red">*</span></label>
								<input type="text" class="form-control"  name="editGoogleLocationDisplayName"  id="editGoogleLocationDisplayName"  value="{{googleLocation[0].googleLocationDisplayName}}" ng-required="true">
							</div>
							<div class="form-group col-md-12">
								<label>Google Location URL<span style="color:red">*</span></label>
								<input type="text" class="form-control"  name="editGoogleLocationUrl"  id="editGoogleLocationUrl"  value="{{googleLocation[0].googleLocationUrl}}" ng-required="true" >
							</div>
							<div class="form-group col-md-12">
								<label for="child_included_rate">Google Location Image</label>
								<input type="file" class="form-control"   file-input="files" name="photo" id="photo">
								<span style="color:red">upload only(300 * 400)</span>
<!-- 								<div id="validimage" name="validimage" style="color: red"><label>Not Image Format!!</label></div> -->
							</div>
							
							<div class="col-md-12">
					         	<img class="img-thumbnail" 
					         	ng-src="{{googleLocation[0].photoPath}}" 
					         	 ngf-select="upload($file)"  ngf-accept="'image/*'" ngf-pattern="'.jpg,.png,!.gif'" alt="User Avatar" width="100%"> 
					        </div>
								
						</div>
						</section>
						<section>
							<div class="modal-footer">
								<a href="#" data-dismiss="modal" class="btn btn-danger">Close</a>
								<button ng-click="editloc.$valid && editGoogleLocation()" ng-disabled="editloc.$invalid" class="btn btn-primary">Save</button>
							</div>  
						</section>
						</form>
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dalog -->
		</div>
		
		
  

      
      


<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>

	<script>


 
	
	var app = angular.module('myApp',['ngFileUpload']);
	app.controller('customersCtrl',['$scope',
	             					'Upload',
	            					'$timeout',
	            					'$http', function($scope,Upload,$timeout,$http) {
		
		/* $scope.GetLocation = function () {
			if($scope.SelectedLocation && $scope.SelectedLocation.length > 0){
			if(!this.geocoder) this.geocoder = new google.maps.Geocoder();
			var options = {

					  componentRestrictions: {country: "IN"}
					 };
			this.geocoder.geocode({ 'address':$scope.SelectedLocation }, function(results,status) {
			alert(status)
			if(status == google.maps.GeocoderStatus.OK){
			var loc = results[0].geometry.location;
			alert(loc.lat());
			alert(loc.lng());
			}else{
			alert("sorry")
			}
			});
			//alert("enter")
			}
                //$window.alert($scope.SelectedLocation);
            }; */

		$scope.settings = {
	            currentPage: 0,
	            offset: 0,
	            pageLimit: 10,
	            pageLimits: ['10', '50', '100']
	          };

		 $scope.getTotalPages = function () {
			return Math.ceil($scope.locationCount[0].count / $scope.settings.pageLimit);
		};
			
		 $scope.isCurrentPageLimit = function (value) {
		       return $scope.settings.pageLimit == value;
		 };
		 
		 $scope.previousPage = function () {
			   $scope.settings.currentPage -= 1;
			   var previousOffset = $scope.settings.currentPage * $scope.settings.pageLimit;
		       var url = "get-google-locations?limit="+$scope.settings.pageLimit+"&offset="+previousOffset;
				$http.get(url).success(function(response) {
					//console.log(response);
					$scope.googleLocations = response.data;
				}); 
		    }; 
		 
     		$scope.nextPage = function () {
		       $scope.settings.currentPage += 1;
		       var nextOffset = $scope.settings.currentPage * $scope.settings.pageLimit;
		       var url = "get-google-locations?limit="+$scope.settings.pageLimit+"&offset="+nextOffset;
				$http.get(url).success(function(response) {
					//console.log(response);
					$scope.googleLocations = response.data;
				}); 
		     }; 
		       
      			$scope.all = function (total) {
		    	   $scope.settings.pageLimit = total;
			       var nextOffset = 0;
			       var url = "get-google-locations?limit="+total+"&offset="+nextOffset;
					$http.get(url).success(function(response) {
						//console.log(response);
						$scope.googleLocations = response.data;
					}); 
			    };
			       
      			 $scope.limited = function (limit) {
			    	   $scope.settings.pageLimit = limit;
				       var nextOffset = 0;
				       var url = "get-google-locations?limit="+limit+"&offset="+nextOffset;
						$http.get(url).success(function(response) {
							//console.log(response);
							$scope.googleLocations = response.data;
					}); 
		       };
		
	       	$timeout(function(){
	      	    // $scope.progressbar.complete();
	            $scope.show = true;
	            $("#pre-loader").css("display","none");
	        }, 2000);

			 
			
			$scope.unread = function() {
			//var notifiurl = "http://localhost:8085/collaborative-workflow/unreadnotifications.action";
			var notifiurl = "unreadnotifications.action";
			$http.get(notifiurl).success(function(response) {
				$scope.latestnoti = response.data;
			});
			};

			
			
			$scope.getGoogleLocations = function() {

				var url = "get-google-locations?limit="+$scope.settings.pageLimit+"&offset="+$scope.settings.offset;
				$http.get(url).success(function(response) {
				    console.log(response);
					$scope.googleLocations = response.data;
		
				});
			};
			var spinner = $('#loadertwo');
		
			
			
			$scope.addGoogleLocation = function(){
				
				 var file = $scope.files;
				 console.log(file); 
				// var file = this.files[0];
				 var fileType = file["type"];
				 var ValidImageTypes = ["image/gif", "image/jpeg", "image/png"];
				 if ($.inArray(fileType, ValidImageTypes) < 0) {
					     // invalid file type code goes here.
					     alert('image invalid')
					}
					else{
						//alert('image valid')
						var fdata = "googleLocationName=" + $('#googleLocationName').val()
						+ "&googleLocationUrl=" + $('#googleLocationUrl').val()
						+ "&googleLocationDisplayName=" + $('#googleLocationDisplayName').val()
						+ "&googleLocationLatitude=" + $('#googleLocationLatitude').val()
						+ "&googleLocationLongitude=" + $('#googleLocationLongitude').val()
						+ "&googleLocationPlaceId=" + $('#googleLocationPlaceId').val()
						+ "&description=" + $('#googleLocationDescription').val();
					spinner.show();
						$http(
								{
									method : 'POST',
									data : fdata,
									headers : {
										'Content-Type' : 'application/x-www-form-urlencoded'
									},
									url : 'add-google-location'
								}).then(function successCallback(response) {
									$scope.addedGoogleLocation = response.data;
									console.log($scope.addedGoogleLocation.data[0].googleLocationId);
									   setTimeout(function(){ spinner.hide(); 
										  }, 1000);
									var googleLocationId = $scope.addedGoogleLocation.data[0].googleLocationId;
									 Upload.upload({
							                url: 'googlelocationpicupdate',
							                enableProgress: true, 
							                data: {myFile: file, 'googleLocationId': googleLocationId} 
							            }).then(function (resp) {
							         	   setTimeout(function(){ spinner.hide(); 
							 			  }, 1000);
							            	
							            });
									 
									
						            $scope.getGoogleLocations();
									var alertmsg = ' <div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Alert!</h4>Success Item Added Succesfully.</div>';
						            $('#message').html(alertmsg);
						            //window.location.reload(); 
									$timeout(function(){
							      	$('#btnclose').click();
							        }, 2000); 
									   setTimeout(function(){ spinner.hide(); 
										  }, 1000);
						        
						}, function errorCallback(response) {
							   setTimeout(function(){ spinner.hide(); 
								  }, 1000);
						});
					}
			};
			
			$scope.editGoogleLocation = function(){
				 var file = $scope.files;
				 var fileType = file["type"];
				 var ValidImageTypes = ["image/gif", "image/jpeg", "image/png"];
				 if ($.inArray(fileType, ValidImageTypes) < 0) {
					     // invalid file type code goes here.
					     alert('image invalid')
					}
					else{
 						alert('image valid')
						var fdata = "googleLocationName=" + $('#editGoogleLocationName').val()
						+ "&googleLocationId=" + $('#editGoogleLocationId').val()
						+ "&googleLocationUrl=" + $('#editGoogleLocationUrl').val()
						+ "&googleLocationDisplayName=" + $('#editGoogleLocationDisplayName').val();
						spinner.show();
						$http(
								{
									method : 'POST',
									data : fdata,
									headers : {
										'Content-Type' : 'application/x-www-form-urlencoded'
									},
									url : 'edit-google-location'
								}).then(function successCallback(response) {
									Upload.upload({
						                url: 'googlelocationpicupdate',
						                enableProgress: true, 
						                data: {myFile: file, 'googleLocationId': $('#editGoogleLocationId').val()} 
						            }).then(function (resp) {
						         	   setTimeout(function(){ spinner.hide(); 
						 			  }, 1000);
						            	window.location.reload(); 
						            		
						            });
									
						}, function errorCallback(response) {
							   setTimeout(function(){ spinner.hide(); 
								  }, 1000);
						});
					}
			};
			
            
			
			$scope.getGoogleLocation = function(rowid) {
				var url = "get-google-location?googleLocationId="+rowid;
				$http.get(url).success(function(response) {
				    console.log(response);
					$scope.googleLocation = response.data;
					console.log($scope.googleLocation[0].googleLocationName)
		
				});
				
			};
			
			
			$scope.deleteLocation=function(rowid){
				var fdata = "&googleLocationId="+ rowid
				var deleteLocationCheck=confirm("Do you want to delete the location?");
				if(deleteLocationCheck){
					$http(
							{
								method : 'POST',
								data : fdata,
								headers : {
									'Content-Type' : 'application/x-www-form-urlencoded'
								},
								url: 'delete-google-location'
							}).then(function successCallback(response) {
								window.location.reload();
							   }, function errorCallback(response) {
								
								
							});
				}
				else{
					
				}
				
			};
			
			$scope.getGoogleLocationCount = function() {

				var url = "get-google-location-count";
				$http.get(url).success(function(response) {
				    //console.log(response);
					$scope.locationCount = response.data;
		            //console.log($scope.accommodationRoomsCount.data[0].size);
				});
			};
			$scope.getGoogleLocations();
			$scope.getGoogleLocationCount();
	}]);


	app.directive('fileInput', ['$parse', function ($parse) {
	    return {
	        restrict: 'A',
	        link: function (scope, element, attributes) {
	            element.bind('change', function () {
	                $parse(attributes.fileInput)
	                .assign(scope,element[0].files[0])
	                scope.$apply()
	            });
	        }
	    };
	}]);		
		   
		        

		
	</script>
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyDg9dqRsKwRWXGdTKsUNHTHhfQqdSyip9k"></script>
    <script type="text/javascript">
        google.maps.event.addDomListener(window, 'load', function () {
		       var options = {

  componentRestrictions: {country: "IN"}
 };
		
            var places = new google.maps.places.Autocomplete(document.getElementById('googleLocationName'),options);
            //alert("enter")
            google.maps.event.addListener(places, 'place_changed', function () {
                var place = places.getPlace();
                var address = place.formatted_address;
                var latitude = place.geometry.location.lat();
                var longitude = place.geometry.location.lng();
                var placeId = place.place_id;
                /* var mesg = "Address: " + address;
                mesg += "\nLatitude: " + latitude;
                mesg += "\nLongitude: " + longitude; */ 
                //alert(mesg);
                document.getElementById('googleLocationLatitude').value = latitude;
                document.getElementById('googleLocationLongitude').value = longitude;
                document.getElementById('googleLocationPlaceId').value = placeId;
               // alert(document.getElementById('googleLocationLongitude').value());
				console.log(mesg)
            });
        });
    </script> 
			
	 <link rel="stylesheet" href="dist/css/dataTables.bootstrap.css">
	 <script src="js1/upload/ng-file-upload-shim.min.js"></script>
	 <script src="js1/upload/ng-file-upload.min.js"></script>
	 <script src="js1/jquery.dataTables.min.js" type="text/javascript"></script>
	 <script src="js1/dataTables.bootstrap.min.js" type="text/javascript"></script>
       
