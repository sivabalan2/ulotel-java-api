<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="s" uri="/struts-tags"%>

 <!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/codemirror.min.css">
 <link href="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.6.0/css/froala_editor.pkgd.min.css" rel="stylesheet" type="text/css" />
 <link href="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.6.0/css/froala_style.min.css" rel="stylesheet" type="text/css" />
 -->
   
   <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
          Ota Hotels 
          </h1>
          <ol class="breadcrumb">
            <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Ota Hotels</li>
          </ol>
        </section>
        <!-- Main content -->
                <!-- Main content -->
        <section class="content">
        
        	<div class="row">
            	<div class="col-xs-12">
						<div class="box">
							<div class="box-header">
								<h3 class="box-title">OTA Hotel </h3>
							</div>
							
							
							<!-- /.box-header -->
							<div class="box-body table-responsive no-padding" > 
						 <form>
 <div>  <!-- ng-repeat="slc in seoLocationContent" -->

<div class="form-group col-md-4 col-sm-12">
<h4>Ota Hotel Code</h4>
<input type="text" id="otaHotelCode" name="otaHotelCode" class="form-control"   value="{{otaHotels[0].otaHotelCode}}"  />

</div>
<div class="form-group col-md-4 col-sm-12">
<h4>Ota Bearer Token</h4>                                  
<input type="text" id="otaBearerToken" name="otaBearerToken"   class=" form-control" value="{{otaHotels[0].otaBearerToken}}"/>
</div>
<div class="form-group col-md-4 col-sm-12">
<h4>Ota Channel Token</h4>                                                                     
<input type="text" id="otaChannelToken" name="otaChannelToken"  class=" form-control" value="{{otaHotels[0].otaChannelToken}}"   ng-required="true"/>		                    		
</div>

 <div class="form-group col-md-12 col-sm-12">
<button type="submit" ng-click="saveOtaHotels()" class="btn btn-primary btngreeen pull-right" >Save</button>
</div>
</div> <!-- NG-repeat ends -->
</div>
</form>

							</div>
							<!-- /.box-body -->
							
						
						</div>
						<!-- /.box -->
					</div>	
         	</div>
          
        </section><!-- /.content -->
 </div>                       
          



<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>
  
 <%--  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> 
 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/codemirror.min.js"></script>
 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/mode/xml/xml.min.js"></script>
 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.6.0//js/froala_editor.pkgd.min.js"> </script> --%>
	
 <script>


 
	
		var app = angular.module('myApp', ['ngProgress']);
		app.controller('customersCtrl', function($scope, $http,$timeout,ngProgressFactory) {

			 $(function(){
				  $('#edit').froalaEditor()
				}); 
			
	       	$timeout(function(){
	      	    // $scope.progressbar.complete();
	            $scope.show = true;
	            $("#pre-loader").css("display","none");
	        }, 2000);

			 
			
			$scope.unread = function() {
			//var notifiurl = "http://localhost:8085/collaborative-workflow/unreadnotifications.action";
			var notifiurl = "unreadnotifications.action";
			$http.get(notifiurl).success(function(response) {
				$scope.latestnoti = response.data;
			});
			};

           
			
			
				
			$scope.getOtaHotels = function() {
	   			
				var url = "get-ota-hotels";
	   			$http.get(url).success(function(response) {
	   				//alert(JSON.stringify(response.data));
	   				$scope.otaHotels = response.data;
	   				
	   			
	   			});
	   		};    
	   		
			$scope.getOtaHotelDetails = function() {
	   			
	   			var url = "get-ota-hotel-details";
	   			$http.get(url).success(function(response) {
	   				//alert(JSON.stringify(response.data));
	   				$scope.otaHotelDetails = response.data;
	   			
	   			});
	   		};    
	   		var spinner = $('#loadertwo');
	   	
			$scope.saveOtaHotels = function(){
					
				var fdata = "otaHotelCode=" + $('#otaHotelCode').val()
				+ "&otaBearerToken=" + $('#otaBearerToken').val()
				+ "&otaChannelToken=" +$('#otaChannelToken').val();
				spinner.show();
				//alert(fdata);
				$http(
						{
							method : 'POST',
							data : fdata,
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							url : 'save-ota-hotels'
						}).then(function successCallback(response) {
							   setTimeout(function(){ spinner.hide(); 
							   	  }, 1000);
							alert("ota hotel detail update successfully!!")
							$scope.getOtaHotels();
							
				}, function errorCallback(response) {
					   setTimeout(function(){ spinner.hide(); 
					   	  }, 1000);
					// called asynchronously if an error occurs
					// or server returns response with an error status.
				});

			};
			
			
			
            $scope.getSingleUser = function(rowid) {
				
				//alert(rowid);
				var url = "get-user?propertyUserId="+rowid;
					$http.get(url).success(function(response) {
				    console.log(response);				
					$scope.user = response.data;
		
				});
				
			};

			 $scope.getPropertyList = function() {
				 
		        	
		        	
			        var userId = $('#adminId').val();
		 			var url = "get-user-properties?userId="+userId;
		 			$http.get(url).success(function(response) {
		 			    
		 				$scope.props = response.data;
		 	
		 			});
		 		};
		 		
            $scope.change = function() {
     	   
		        //alert($scope.id);
		        
		        var propertyId = $scope.id;	
     	    var url = "change-user-property?propertyId="+propertyId;
	 			$http.get(url).success(function(response) {
	 				
	 				 window.location = '/ulopms/dashboard'; 
	 				//$scope.change = response.data;
	 	
	 			});
			       
		 		};
		 		
		 	
		 		
				
		    $scope.getPropertyList();
			
		    $scope.getOtaHotels();
			
		   
		    
			//$scope.unread();
			//
			
			
		});

	
		
		   
		        

		
	</script>
	
	
	<%-- <script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.js"></script> 

<script>
   $(function () {
      // Replace the <textarea id="editor1"> with a CKEditor
      // instance, using default configuration.
      CKEDITOR.replace('editor1');
      //bootstrap WYSIHTML5 - text editor
      $(".textarea").wysihtml5();
      
      CKEDITOR.replace('metaDescription');
      //bootstrap WYSIHTML5 - text editor
      $(".textarea").wysihtml5();
      
     CKEDITOR.replace('schemaContent');
      //bootstrap WYSIHTML5 - text editor
      $(".textarea").wysihtml5();
      
      CKEDITOR.replace('locationContent');
      //bootstrap WYSIHTML5 - text editor
      $(".textarea").wysihtml5();
      
     CKEDITOR.replace('scriptContent');
     // bootstrap WYSIHTML5 - text editor
      $(".textarea").wysihtml5();
    });
	
  
</script> --%>





 
	
	 <script src="js/jquery-2.1.1.js" type="text/javascript"></script>
       