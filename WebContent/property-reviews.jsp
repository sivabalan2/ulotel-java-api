<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="s" uri="/struts-tags"%>

   <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Property Review
          </h1>
          <ol class="breadcrumb">
            <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Property Review</li>
          </ol>
        </section>
		
        <!-- Main content -->
        <section class="content">
        
        	<div class="row">
            	<div class="col-xs-12" ng-if="properties.length>=0">
						<div class="box">
							<div class="box-header">
								<h3 class="box-title">Property Review</h3>
							</div>
							
							
							<!-- /.box-header -->
							<div class="box-body table-responsive no-padding" > 
								 <div class="form-group col-md-2 right" > 
									<label>Search</label>
									<input type="text" ng-model="search" class="form-control" placeholder="Search">
								  
								  </div>
								<table class="table table-hover" id="example1">
									<tr>
										<th>Order</th>
										<th>Property Name</th>
										<th>Google Star Count</th>
										<th>Google Review Count</th>
										<th>Google Average Review</th>
										<th>Tripadvisor Star Count</th>
										<th>Tripadvisor Review Count</th>
										<th>Tripadvisor Average Review</th>
										<th>Edit</th>
										<th>Delete</th>
										<th>User Name</th>
									</tr>
									<tr ng-repeat="pr in propertyreview | filter:search">
										<td>{{pr.serialNo}}</td>
										<td>{{pr.reviewPropertyName}}</td>
										<td>{{pr.starCount}}</td>
										<td>{{pr.reviewCount}}</td>
										<td>{{pr.averageReview}}</td>
										<td>{{pr.tripadvisorStarCount}}</td>
										<td>{{pr.tripadvisorReviewCount}}</td>
										<td>{{pr.tripadvisorAverageReview}}</td>
										<td>
											<a href="#editmodal" ng-click="getPropertyReview(pr.propertyReviewId,pr.reviewPropertyId)"  data-toggle="modal"><i  class="fa fa-pencil text-green"></i></a>
										</td>
										<td>
											<a href="#deletemodal" ng-click="deletePropertyReview(pr.propertyReviewId)" data-toggle="modal" ><i  class="fa fa-fw fa-trash text-red" ></i></a>
										</td>
										<td>{{pr.createdBy}}</td>
									</tr>
									<tr>
									</tr>
								</table>
							</div>
							<!-- /.box-body -->
							
							
							<div class="box-footer clearfix">
								 <ul class="pagination pagination-sm no-margin pull-right">
									<a class="btn btn-primary " href="#addmodal" data-toggle="modal" >Add Property Review</a>
								</ul> 
							</div>
							<div class="box-footer clearfix">
								<ul class="pagination pagination-sm"></ul>
								<div class="simple-pagination pull-right" >   
									<p class="simple-pagination__items">Showing {{propertyReviewCount[0].count}} out of {{settings.pageLimit}}</p>
									<p><button ng-click="previousPage()" ng-disabled="settings.currentPage <= 0" class="simple-pagination__button simple-pagination__button--prev"> &#10094;          
									</button><span class="simple-pagination__pages">{{settings.currentPage + 1}} of {{getTotalPages()}}</span>
									<button ng-click="nextPage()" ng-disabled="settings.currentPage === (getTotalPages() - 1)" class="simple-pagination__button simple-pagination__button--next">&#10095;</button></p>
									<a href="" ng-click="limited(10)" ng-class="active">{{10}}</a> |
									<span><a href="" ng-click="all(propertyReviewCount[0].count)" ng-class="active">All</a></span> 
								</div>
						   </div> 
						</div>
						<!-- /.box -->
					</div>	
         	</div>
          
        </section><!-- /.content -->
      	<input type="hidden" value ="<%=session.getAttribute("propertyId") %>" id="propertyId" name="propertyId" >
      	<div class="modal" id="addmodal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" id="btnclose" class="close" data-dismiss="modal"
							aria-hidden="true">x</button>
						<h4 class="modal-title">Add Property Review</h4>
					</div>
					<form  name="propertyreviews">
					<div class="modal-body" >
						<div class="form-group col-md-12">
						  <label>Select Property<span style="color:red">*</span></label>
						  <select  name="reviewPropertyId" id="reviewPropertyId" ng-model="reviewPropertyId" ng-required="true" class="form-control" >
							  <option style="display:none" value="0" >Select Your Property</option>
			                  <option ng-repeat="p in properties" value="{{p.DT_RowId}}" >{{p.propertyName}}</option>
		                  </select>
						</div>
						<div class="form-group col-md-6">
						  <label>Google Star Count<span style="color:red">*</span></label>
						  <select  name="propertyStarCount" id="propertyStarCount" ng-required="true" ng-model="propertyStarCount" class="form-control" >
							  <option disabled selected value>Select Star Count</option>
			                  <option value="1">1</option>
			                  <option value="2">2</option>
			                  <option value="3">3</option>
			                  <option value="4">4</option>
			                  <option value="5">5</option>
		                  </select>
						</div>
						<div class="form-group col-md-6">
						  <label>Tripadvisor Star Count<span style="color:red">*</span></label>
						  <select  name="tripadvisorPropertyStarCount" id="tripadvisorPropertyStarCount" ng-required="true" ng-model="tripadvisorPropertyStarCount" class="form-control" >
							  <option disabled selected value>Select Star Count</option>
			                  <option value="1">1</option>
			                  <option value="2">2</option>
			                  <option value="3">3</option>
			                  <option value="4">4</option>
			                  <option value="5">5</option>
		                  </select>
						</div>
						
						<div class="form-group col-md-6">
							<label>Google Review Count<span style="color:red">*</span></label>
							<input type="text" class="form-control"  name="reviewCount"  id="reviewCount" ng-model="reviewCount" placeholder="Google Review Count"   ng-pattern="/^[0-9]/"  ng-required="true">
						    <span ng-show="propertyreviews.reviewCount.$error.pattern" style="color:red">Not a Review Count!</span>
						</div>
						<div class="form-group col-md-6">
							<label>Tripadvisor Review Count<span style="color:red">*</span></label>
							<input type="text" class="form-control"  name="tripadvisorReviewCount"  id="tripadvisorReviewCount" ng-model="tripadvisorReviewCount" placeholder="Tripadvisor Review Count"   ng-pattern="/^[0-9]/"  ng-required="true">
						    <span ng-show="propertyreviews.tripadvisorReviewCount.$error.pattern" style="color:red">Not a Review Count!</span>
						</div>
						<div class="form-group col-md-6">
							<label>Google Average Review Count<span style="color:red">*</span></label>
							<input type="text" class="form-control"  name="averageReview"  id="averageReview" ng-model="averageReview" placeholder="Average Review"  ng-pattern="/^[0-9]/" ng-required="true">
						    <span ng-show="propertyreviews.averageReview.$error.pattern" style="color:red">Not a Average Review Count!</span>
                    	</div>
                    	<div class="form-group col-md-6">
							<label>Tripadvisor Average Review Count<span style="color:red">*</span></label>
							<input type="text" class="form-control"  name="tripadvisorAverageReview"  id="tripadvisorAverageReview" ng-model="tripadvisorAverageReview" placeholder="Tripadvisor Average Review"  ng-pattern="/^[0-9]/" ng-required="true">
						    <span ng-show="propertyreviews.tripadvisorAverageReview.$error.pattern" style="color:red">Not a Average Review Count!</span>
                    	</div>
                    	<div class="form-group col-md-6">
							<label>Google Review URL<span style="color:red">*</span></label>
							<input type="text" class="form-control"  name="googleReviewUrl"  id="googleReviewUrl" ng-model="googleReviewUrl" placeholder="Google Review URL"  ng-pattern="/^[0-9]/" ng-required="true">
						    <span ng-show="propertyreviews.googleReviewUrl.$error.pattern" style="color:red">Not a Google Review URL!</span>
                    	</div>
                    	<div class="form-group col-md-6">
							<label>Tripadvisor Review URL<span style="color:red">*</span></label>
							<input type="text" class="form-control"  name="tripadvisorReviewUrl"  id="tripadvisorReviewUrl" ng-model="tripadvisorReviewUrl" placeholder="Tripadvisor Review URL"  ng-pattern="/^[0-9]/" ng-required="true">
						    <span ng-show="propertyreviews.tripadvisorReviewUrl.$error.pattern" style="color:red">Not a Average Review URL!</span>
                    	</div>
                    	<div class="form-group col-md-6">
						  <label>Property Rating<span style="color:red">*</span></label>
						  <select  name="propertyRating" id="propertyRating" ng-required="true" ng-model="propertyRating" class="form-control" >
							  <option disabled selected value>Select Property Rating</option>
			                  <option value="Average">Average</option>
			                  <option value="Good">Good</option>
			                  <option value="Excellent">Excellent</option>
		                  </select>
						</div>
						<div class="form-group col-md-6">
							<label>TripAdvisor Image</label>
							<input type="file" class="form-control"   file-input="files" name="photo" id="photo">
							<span style="color:red">upload only(214 * 221)</span>	
						</div>
						<div class="form-group" >
						   <div class="input-group" >
						  		<label>&nbsp;&nbsp;</label>
						  		<label>&nbsp;&nbsp;</label>
						  		<label>&nbsp;&nbsp;</label>
						  		<label>&nbsp;&nbsp;</label>
						  		<input type="hidden" class="form-control">
						   </div>

						</div>
					</div>
					
						<div class="modal-footer">
							<a href="#" data-dismiss="modal" class="btn btn-danger">Close</a>
							<button type="submit" ng-click="addPropertyReviewDetails()"  class="btn btn-primary">Save</button>
						</div>
				
					</form>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dalog -->
		</div>
		<div class="modal" id="editmodal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" id="editbtnclose" class="close" data-dismiss="modal"
							aria-hidden="true">x</button>
						<h4 class="modal-title">Edit Property Review</h4>
					</div>
					 	 <form method="post" theme="simple" name="editpropertyreview">
					 	 <section>
						<div class="modal-body">
							<input type="hidden" value="{{reviews[0].propertyReviewId}}" id="editPropertyReviewId" name="editPropertyReviewId">
						    <input type="hidden" value="{{reviews[0].reviewPropertyId}}" id="editReviewPropertyId" name="editReviewPropertyId">
						    <div class="form-group col-md-12">
							    <label>Property Name<span style="color:red">*</span></label>
								<input type="text" class="form-control" id="editReviewPropertyName" name="editReviewPropertyName" placeholder="Property Name" value="{{reviews[0].reviewPropertyName}}">
							</div>
							
						    <div class="form-group col-md-6">
								 <label>Google Star Count<span style="color:red">*</span></label>
								 <select  name="editPropertyStarCount" id="editPropertyStarCount" ng-modal="editPropertyStarCount" ng-required="true" class="form-control" >
									  <option disabled selected value>Select Star Count</option>
					                  <option value="1" ng-selected ="reviews[0].starCount == '1'">1</option>
					                  <option value="2" ng-selected ="reviews[0].starCount == '2'">2</option>
					                  <option value="3" ng-selected ="reviews[0].starCount == '3'">3</option>
					                  <option value="4" ng-selected ="reviews[0].starCount == '4'">4</option>
					                  <option value="5" ng-selected ="reviews[0].starCount == '5'">5</option>
				                  </select>
							</div>
							<div class="form-group col-md-6">
								 <label>Tripadvisor Star Count<span style="color:red">*</span></label>
								 <select  name="editTripadvisorPropertyStarCount" id="editTripadvisorPropertyStarCount"  ng-modal="editTripadvisorPropertyStarCount" ng-required="true" class="form-control" >
									  <option disabled selected value>Select Star Count</option>
					                  <option value="1" ng-selected ="reviews[0].tripadvisorStarCount == '1'">1</option>
					                  <option value="2" ng-selected ="reviews[0].tripadvisorStarCount == '2'">2</option>
					                  <option value="3" ng-selected ="reviews[0].tripadvisorStarCount == '3'">3</option>
					                  <option value="4" ng-selected ="reviews[0].tripadvisorStarCount == '4'">4</option>
					                  <option value="5" ng-selected ="reviews[0].tripadvisorStarCount == '5'">5</option>
				                  </select>
							</div>
							
							<div class="form-group col-md-6">
							    <label>Google Review Count<span style="color:red">*</span></label>
								<input type="text" class="form-control"  name="editReviewCount"  id="editReviewCount"  placeholder="Google Review Count" value="{{reviews[0].reviewCount}}"   ng-pattern="/^[0-9]/"  ng-required="true" maxlength="20">
						    	<span ng-show="editpropertyreview.editReviewCount.$error.pattern" style="color:red">Not a Review Count!</span>
							</div>
							<div class="form-group col-md-6">
							    <label>Tripadvisor Review Count<span style="color:red">*</span></label>
								<input type="text" class="form-control"  name="editTripadvisorReviewCount"  id="editTripadvisorReviewCount"  placeholder="tripadvisor Review Count" value="{{reviews[0].tripadvisorReviewCount}}"  ng-pattern="/^[0-9]/"  ng-required="true" maxlength="20">
						    	<span ng-show="editpropertyreview.editTripadvisorReviewCount.$error.pattern" style="color:red">Not a Review Count!</span>
							</div>
							<div class="form-group col-md-6">
								<label>Google Average Review Count<span style="color:red">*</span></label>
								<input type="text" class="form-control"  name="editAverageReview"  id="editAverageReview"  placeholder="Google Average Review" value="{{reviews[0].averageReview}}"  ng-pattern="/^[0-9]/" ng-required="true" maxlength="20">
							    <span ng-show="editpropertyreview.editAverageReview.$error.pattern" style="color:red">Not a Average Review!</span>
	                    	</div>
	                    	<div class="form-group col-md-6">
								<label>Tripadvisor Average Review Count<span style="color:red">*</span></label>
								<input type="text" class="form-control"  name="editTripadvisorAverageReview"  id="editTripadvisorAverageReview"  placeholder="Tripadvisor Average Review" value="{{reviews[0].tripadvisorAverageReview}}"  ng-pattern="/^[0-9]/" ng-required="true" maxlength="20">
							    <span ng-show="editpropertyreview.editTripadvisorAverageReview.$error.pattern" style="color:red">Not a Average Review!</span>
	                    	</div>
	                    	<div class="form-group col-md-6">
								<label>Google Review URL<span style="color:red">*</span></label>
								<input type="text" class="form-control"  name="editGoogleReviewUrl"  id="editGoogleReviewUrl" value="{{reviews[0].googleReviewUrl}}" placeholder="Google Review URL"  ng-pattern="/^[0-9]/" ng-required="true">
							    <span ng-show="editpropertyreviews.editGoogleReviewUrl.$error.pattern" style="color:red">Not a Google Review URL!</span>
	                    	</div>
	                    	<div class="form-group col-md-6">
								<label>Tripadvisor Review URL<span style="color:red">*</span></label>
								<input type="text" class="form-control"  name="editTripadvisorReviewUrl"  id="editTripadvisorReviewUrl" value="{{reviews[0].tripadvisorReviewUrl}}" placeholder="Tripadvisor Review URL"  ng-pattern="/^[0-9]/" ng-required="true">
							    <span ng-show="editpropertyreviews.editTripadvisorReviewUrl.$error.pattern" style="color:red">Not a Average Review Count!</span>
	                    	</div>
	                    	<div class="form-group col-md-6">
							  <label>Property Rating<span style="color:red">*</span></label>
							  <select  name="editPropertyRating" id="editPropertyRating" ng-required="true" class="form-control" >
								  <option disabled selected value>Select Property Rating</option>
				                  <option value="Average" ng-selected ="reviews[0].propertyRating == 'Average'">Average</option>
				                  <option value="Good" ng-selected ="reviews[0].propertyRating == 'Good'">Good</option>
				                  <option value="Excellent" ng-selected ="reviews[0].propertyRating == 'Excellent'">Excellent</option>
			                  </select>
							</div>
							 <div class="form-group col-md-6">
								<label>TripAdvisor Image</label>
								<input type="file" class="form-control"   file-input="files" name="photo" id="photo">
								<span style="color:red">upload only(214 * 221)</span>	
							</div>
							
							<div class="col-md-12">
					         	<img class="img-thumbnail"  ngf-select="upload($file)"  ngf-accept="'image/*'" ngf-pattern="'.jpg,.png,!.gif'" src="get-property-review-picture?propertyReviewId={{reviews[0].propertyReviewId}}"  alt="User Avatar" width="100%">
					        </div>
							<div class="form-group" >
						   <div class="input-group" >
						  		<label>&nbsp;&nbsp;</label>
						  		<label>&nbsp;&nbsp;</label>
						  		<label>&nbsp;&nbsp;</label>
						  		<label>&nbsp;&nbsp;</label>
						  		<input type="hidden" class="form-control">
						   </div>

						</div>		
						</div>
						</section>
						<section>
							<div class="modal-footer">
								<a href="#" data-dismiss="modal" class="btn btn-danger">Close</a>
								<button ng-click="editPropertyReview()" class="btn btn-primary">Update</button>
							</div>  
						</section>
						</form>
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dalog -->
		</div>
		
		
  

      
      


<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>

	<script>


 
	
	var app = angular.module('myApp',['ngFileUpload']);
	app.controller('customersCtrl',['$scope',
	             					'Upload',
	            					'$timeout',
	            					'$http', function($scope,Upload,$timeout,$http) {

		
		
		$scope.settings = {
	            currentPage: 0,
	            offset: 0,
	            pageLimit: 10,
	            pageLimits: ['10', '50', '100']
	          };

		 $scope.getTotalPages = function () {
			return Math.ceil($scope.propertyReviewCount[0].count / $scope.settings.pageLimit);
		};
			
		 $scope.isCurrentPageLimit = function (value) {
		       return $scope.settings.pageLimit == value;
		 };
		 
		 $scope.previousPage = function () {
			   $scope.settings.currentPage -= 1;
			   var previousOffset = $scope.settings.currentPage * $scope.settings.pageLimit;
		       var url = "get-property-review?limit="+$scope.settings.pageLimit+"&offset="+previousOffset;
				$http.get(url).success(function(response) {
					//console.log(response);
					$scope.propertyreview = response.data;
				}); 
		    }; 
		 
     		$scope.nextPage = function () {
		       $scope.settings.currentPage += 1;
		       var nextOffset = $scope.settings.currentPage * $scope.settings.pageLimit;
		       var url = "get-property-review?limit="+$scope.settings.pageLimit+"&offset="+nextOffset;
				$http.get(url).success(function(response) {
					//console.log(response);
					$scope.propertyreview = response.data;
				}); 
		     }; 
		       
      			$scope.all = function (total) {
		    	   $scope.settings.pageLimit = total;
			       var nextOffset = 0;
			       var url = "get-property-review?limit="+total+"&offset="+nextOffset;
					$http.get(url).success(function(response) {
						//console.log(response);
						$scope.propertyreview = response.data;
					}); 
			    };
			       
      			 $scope.limited = function (limit) {
			    	   $scope.settings.pageLimit = limit;
				       var nextOffset = 0;
				       var url = "get-property-review?limit="+limit+"&offset="+nextOffset;
						$http.get(url).success(function(response) {
							//console.log(response);
							$scope.propertyreview = response.data;
					}); 
		       };			
		
	       	$timeout(function(){
	      	    // $scope.progressbar.complete();
	            $scope.show = true;
	            $("#pre-loader").css("display","none");
	        }, 2000);

			$scope.unread = function() {
			//var notifiurl = "http://localhost:8085/collaborative-workflow/unreadnotifications.action";
			var notifiurl = "unreadnotifications.action";
			$http.get(notifiurl).success(function(response) {
				$scope.latestnoti = response.data;
			});
			};
			
			$scope.getPropertyReviews = function() {

				var url = "get-property-review?limit="+$scope.settings.pageLimit+"&offset="+$scope.settings.offset;
				$http.get(url).success(function(response) {
				    console.log(response);
					$scope.propertyreview = response.data;
		
				});
			};
			
			$scope.getPropertyReview = function(selectedRowId,selectedPropertyId) {

				var url = "get-property-review-id?propertyReviewId="+selectedRowId+"&reviewPropertyId="+selectedPropertyId;
				$http.get(url).success(function(response) {
				    console.log(response);
					$scope.reviews = response.data;
		
				});
			};
			
			$scope.getProperties=function(){
				var url = "get-all-property";
				$http.get(url).success(function(response) {
					$scope.properties = response.data;
		
				});
			};
			
			$scope.deletePropertyReview=function(rowid){
				var fdata = "&propertyReviewId="+ rowid
				var deleteLocationCheck=confirm("Do you want to delete the property review?");
				if(deleteLocationCheck){
					$http(
							{
								method : 'POST',
								data : fdata,
								headers : {
									'Content-Type' : 'application/x-www-form-urlencoded'
								},
								url: 'delete-property-review'
							}).then(function successCallback(response) {
								window.location.reload();
							   }, function errorCallback(response) {
								
								
							});
				}
				else{
					
				}
				
			};
			
			$scope.getPropertyReviewCount = function() {

				var url = "get-property-review-count";
				$http.get(url).success(function(response) {
				    //console.log(response);
					$scope.propertyReviewCount = response.data;
				});
			};
			var spinner = $('#loadertwo');
			  
			$scope.addPropertyReviewDetails = function(){
				
				 var file = $scope.files;
				 console.log(file); 
				// var file = this.files[0];
				 var fileType = file["type"];
				 var ValidImageTypes = ["image/gif", "image/jpeg", "image/png"];
				 if ($.inArray(fileType, ValidImageTypes) < 0) {
					     // invalid file type code goes here.
					     alert('image invalid')
					}
					else{
						//alert('valid image')
						var fdata = "propertyStarCount=" + $('#propertyStarCount').val()
						+ "&reviewCount=" + $('#reviewCount').val()
							+ "&averageReview=" + $('#averageReview').val()
							+ "&tripadvisorPropertyStarCount=" + $('#tripadvisorPropertyStarCount').val()
							+ "&tripadvisorReviewCount=" + $('#tripadvisorReviewCount').val()
							+"&tripadvisorAverageReview=" + $('#tripadvisorAverageReview').val()
							+ "&googleReviewUrl=" + $('#googleReviewUrl').val()
							+ "&tripadvisorReviewUrl=" + $('#tripadvisorReviewUrl').val()
							+"&propertyRating=" + $('#propertyRating').val()
							+"&reviewPropertyId=" + $('#reviewPropertyId').val();
						spinner.show();
						$http(
								{
									method : 'POST',
									data : fdata,
									headers : {
										'Content-Type' : 'application/x-www-form-urlencoded'
									},
									url : 'add-review-details'
								}).then(function successCallback(response) {
									$scope.addedpropertyreview = response.data;
									
									console.log($scope.addedpropertyreview.data[0].propertyReviewId);
							        
									var propertyReviewId = $scope.addedpropertyreview.data[0].propertyReviewId;
									 Upload.upload({
							                url: 'tripadvisorpicupdate',
							                enableProgress: true, 
							                data: {myFile: file, 'propertyReviewId': propertyReviewId} 
							            }).then(function (resp) {
							            	 setTimeout(function(){ spinner.hide(); 
							   			  }, 1000);
							            	window.location.reload();
							            	
							            });
								
						        
						}, function errorCallback(response) {
							 setTimeout(function(){ spinner.hide(); 
							  }, 1000);
						});
				}
			};
			
			$scope.editPropertyReview = function(){
				var file = $scope.files;
				 console.log(file); 
				 var fileType = file["type"];
				 var ValidImageTypes = ["image/gif", "image/jpeg", "image/png"];
				 if ($.inArray(fileType, ValidImageTypes) < 0) {
					     // invalid file type code goes here.
					     alert('image invalid')
					}
					else{
						
						var fdata = "propertyStarCount=" + $('#editPropertyStarCount').val()
						+ "&reviewCount=" + $('#editReviewCount').val()
							+ "&averageReview=" + $('#editAverageReview').val()
							+ "&tripadvisorPropertyStarCount=" + $('#editTripadvisorPropertyStarCount').val()
							+ "&tripadvisorReviewCount=" + $('#editTripadvisorReviewCount').val()
							+ "&tripadvisorAverageReview=" + $('#editTripadvisorAverageReview').val()
							+ "&googleReviewUrl=" + $('#editGoogleReviewUrl').val()
							+ "&tripadvisorReviewUrl=" + $('#editTripadvisorReviewUrl').val()
							+"&propertyRating=" + $('#editPropertyRating').val()
							+"&reviewPropertyId=" + $('#editReviewPropertyId').val()
							+ "&propertyReviewId=" + $('#editPropertyReviewId').val();
						spinner.show();
						$http(
								{
									method : 'POST',
									data : fdata,
									headers : {
										'Content-Type' : 'application/x-www-form-urlencoded'
									},
									url : 'edit-review-details'
								}).then(function successCallback(response) {
									Upload.upload({
						                url: 'tripadvisorpicupdate',
						                enableProgress: true, 
						                data: {myFile: file, 'propertyReviewId': $('#editPropertyReviewId').val()} 
						            }).then(function (resp) {
						            	 setTimeout(function(){ spinner.hide(); 
						   			  }, 1000);
						            	window.location.reload(); 
						            });
									
									
						}, function errorCallback(response) {
							 setTimeout(function(){ spinner.hide(); 
							  }, 1000);
						});
					}
			};
			
			
			$scope.getPropertyReviews();
			$scope.getPropertyReviewCount();
			$scope.getProperties();
			
	}]);


	app.directive('fileInput', ['$parse', function ($parse) {
	    return {
	        restrict: 'A',
	        link: function (scope, element, attributes) {
	            element.bind('change', function () {
	                $parse(attributes.fileInput)
	                .assign(scope,element[0].files[0])
	                scope.$apply()
	            });
	        }
	    };
	}]);		
		   
		        

		
	</script>
			
	<link rel="stylesheet" href="dist/css/dataTables.bootstrap.css">
	<script src="js1/upload/ng-file-upload-shim.min.js"></script>
	<script src="js1/upload/ng-file-upload.min.js"></script>
	 <script src="js1/jquery.dataTables.min.js" type="text/javascript"></script>
	 <script src="js1/dataTables.bootstrap.min.js" type="text/javascript"></script>
       