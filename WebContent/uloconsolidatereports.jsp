<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="s" uri="/struts-tags"%>

   <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <section class="content-header">
          <h1>
            ULO Consolidate Reports
            <!--<small>Subscription Expiry on {{property[0].subscription[0].expirydate}}</small>-->
          </h1>
          <ol class="breadcrumb">
            <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Report</a></li>
            <li class="active">ULO Consolidate Reports</li>
          </ol>
        </section>
 		
 		<!-- Main content -->
    <section class="content">
    <div class="box box-default color-palette-box">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-tag"></i> Search &amp; Filter</h3>
        </div>
        <div class="box-body">
        <form>
          <div class="row">
          	<div class="form-group col-md-3">
			  <label>Select Location :</label>
			  <select  name="reportLocationId" id="reportLocationId" ng-model="reportLocationId" ng-required="true" ng-change="getReportAreas()" class="form-control" >
			  	<option disabled selected value> &nbsp;-- Locations --&nbsp; </option>
			  	<option style="display:none" value="0">All</option>
                 <option ng-repeat="l in locations" value="{{l.locationId}}">{{l.locationName}}</option>
               </select>
			</div>
            <div class="form-group col-md-3">
			  <label>Select Area :</label>
			  <select name="reportAreaId" ng-model="reportAreaId" id="reportAreaId" ng-required="true" ng-change="getReportAreaProperties()" class="form-control" >
			 	 <option disabled selected value> &nbsp;-- Areas --&nbsp; </option>
			   	<option style="display:none" value="0">All</option>
                 	<option ng-repeat="ar in reportareas" value="{{ar.DT_RowId}}">{{ar.areaName}}</option>
                 </select>
			</div>  
          </div>
          <div id="searchProperties">
         	 <div class="row">
				<div class="form-group col-md-12" id="reportProperties">
					<label>List of Properties :</label>
				   	<div class="input-group" >
				   		<label class="checkbox-inline" id="reportPropertyId"><input type="checkbox"  id="reportPropertyAll" name="reportPropertyAll" value="0" ng-click="reportPropertyList()">ALL</label>
                        <label class="checkbox-inline" ng-repeat="ap in reportAreaProperty">
            				<input type="checkbox" name="checkedProperty[]"  id="checkedProperty" value="{{ap.propertyId}}" ng-model="ap.checked" class="flat-red" ng-click="reportPropertyList(ap.propertyId,ap.propertyName,$index,reportAreaProperty)" >&nbsp {{ap.propertyName}}
          				</label>
				   	</div>
				</div>
			</div>
          </div>
          <div class="row" id="dateRangeId">
          		<div class="col-md-2">
					<div class="input-group" >
                        <label>Date Range :</label>
				   	</div>
				</div>
          		
          		<div class="form-group col-md-2" >
					<label class="flat-red">
          				<input type="radio" name="filterDateWise"  id="filterSingle" value="SingleDate" ng-click="getChooseDate()" >Single Date
        			</label>
				</div>
				<div class="form-group col-md-2" >
					<label>
            			<input type="radio" name="filterDateWise"  id="filterMultiple" value="MultipleDate" class="flat-red" ng-click="getChooseDate()" >Multiple Date
          			</label>
				</div>
				<div class="form-group col-md-2" id="divBookingDate">
					<input type="radio" name="filterDate" id="filterBookingDate" value="BookingDate" placeholder="Booking Date">
					<label>Booking Date</label>
				</div>
				<div class="form-group col-md-2" id="divCheckInDate">
					<input type="radio" name="filterDate" id="filterCheckInDate" value="CheckInDate" placeholder="Check In Date">
					<label>Check In Date</label>
				</div>
          </div>
          <div id="searchOptionsReport">	
          <div class="row">
          <div class="col-sm-2 col-md-3">
           <div class="form-group">
                <label>From Date:</label>

                <div class="input-group date">
                <label class="input-group-addon btn" for="fromDate">
					<span class="fa fa-calendar"></span>
				</label>
                  <input type="text" class="form-control pull-right" id="fromDate" name="fromDate" value="" onkeypress="return false;" autocomplete="off">
                </div>
              </div>
              
              </div>
              <div class="col-sm-2 col-md-3">
           <div class="form-group">
                <label>To Date:</label>

                <div class="input-group date">
                  <label class="input-group-addon btn" for="toDate">
					<span class="fa fa-calendar"></span>
				</label>
                  <input type="text" class="form-control pull-right" id="toDate" name="toDate" value="" onkeypress="return false;" autocomplete="off">
                </div>
                <!-- /.input group -->
              </div>
              
              </div>
          </div>
          <div class="row">
				<div class="col-sm-2 col-md-2">
              	<div class="form-group">
<!--               <label>Search</label> -->
				<label>&nbsp;</label>
              	<button type="submit" ng-click="getRecordList()" class="btn btn-danger pull-right btn-block btn-sm">View</button>
              	</div>
              </div>
				<div class="col-sm-2 col-md-2">
              	<div class="form-group">
<!--               	<label>Download</label> -->
				<label>&nbsp;</label>
				<button id="mydownload" class="btn btn-danger pull-right btn-block btn-sm" ng-csv="bookingrecords" filename="consolidatereport.csv"
				 csv-header="getHeader()" field-separator="," decimal-separator=".">Download</button>
              	</div>
              </div>
              <div class="col-sm-2 col-md-2" id="searchRecordId"> 
				<label>Search :</label>
				<input type="text" ng-model="search" class="form-control" placeholder="Search">
			  
			  </div>
              <div class="col-sm-2 col-md-2">
              	<div id="reportalert"></div>
              </div>
              
			</div>
        </div>
          <!-- /.row -->
          </form>
        </div>
        <!-- /.box-body -->
      </div>
      
      
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Results</h3>

              
            </div>
            <!-- /.box-header -->
            <div  class="box-body table-responsive no-padding" style="height:300px;" ng-if="bookingrecords.length==0">
            	<table class="table table-hover">
               <tr>
                	<td  colspan="8">No records found
                	</td>
                </tr>
                </table>
            </div>
            
            <div class="box-body table-responsive no-padding" style="height:300px;" ng-if="bookingrecords.length>0">
              <table class="table table-hover table-striped">
                <tr role="row">
                  <th ng-repeat="columns in reportColumns" style="overflow:hidden;white-space:nowrap;">{{columns.columnName}}</th>
                </tr>
                <tr ng-repeat="(key,value) in bookingrecords | filter:search">
                  <td ng-repeat="(key,value) in value" >{{value}}</td>
                </tr>
               
                
             </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
      
    </section>
 		
 		
</div>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>
<!-- csv download script start -->
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.15/angular-sanitize.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ng-csv/0.3.6/ng-csv.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.0rc1/angular-route.min.js"></script>
<!-- csv download script end -->
	 <script src="js/jquery-2.1.1.js" type="text/javascript"></script>

	<script>
	var app = angular.module('myApp', ['ngProgress','ngSanitize', 'ngCsv']);
	app.controller('customersCtrl', function($scope, $http,$timeout,ngProgressFactory) {
		
		  $("#fromDate").datepicker({
		      dateFormat: 'MM d, yy',
		    
		      onSelect: function (formattedDate) {
		          var date1 = $('#fromDate').datepicker('getDate'); 
		          var date = new Date( Date.parse( date1 ) ); 
		         // date.setDate( date.getDate() + 1 );        
		          var newDate = date.toDateString(); 
		          newDate = new Date( Date.parse( newDate ) );   
		          $('#toDate').datepicker("option","minDate",newDate);
		          $timeout(function(){
		        	  document.getElementById("fromDate").style.borderColor = "LightGray";
		          });
		      }
		  });

		  $("#toDate").datepicker({
		      dateFormat: 'MM d, yy',
		   
		      onSelect: function (formattedDate) {
		          var date2 = $('#toDate').datepicker('getDate'); 
		          $timeout(function(){
		        	  document.getElementById("toDate").style.borderColor = "LightGray";
		          });
		      }
		  }); 
		
		
		
		  var spinner = $('#loadertwo');
		  $scope.getRecordList = function(){
				var checkbox_value="";
				$("#reportProperties :checkbox").each(function () {
			        var ischecked = $(this).is(":checked");
			        if (ischecked) {
			            checkbox_value += $(this).val() + ",";
			        }
			    });
				
				var filterDateName="";
				
				if(document.getElementById('filterMultiple').checked){
					if(document.getElementById('filterBookingDate').checked){
						filterDateName='BookingDate';
					}
					if(document.getElementById('filterCheckInDate').checked){
						filterDateName='CheckInDate';
					}
				}else if(document.getElementById('filterSingle').checked){
					filterDateName='StayInfo';
				}
				
				
				
				
				if(filterDateName==""){
					filterDateName="NA"
				}
				
				var date1 = $('#fromDate').datepicker('getDate');
				var date2 = $('#toDate').datepicker('getDate');
				
				// The number of milliseconds in one day
			    var ONE_DAY = 1000 * 60 * 60 * 24;

			    // Convert both dates to milliseconds
			    var date1_ms = date1.getTime();
			    var date2_ms = date2.getTime();

			    // Calculate the difference in milliseconds
			    var difference_ms = Math.abs(date1_ms - date2_ms);

			    // Convert back to days and return
			    var diffDays= Math.round(difference_ms/ONE_DAY);
				
			    var searchType="NA";
			    if(diffDays>1 && document.getElementById('filterMultiple').checked){
			    	searchType="Multiple";
			    }else if(diffDays>1 && document.getElementById('filterSingle').checked){
			    	$("#reportalert").hide();
					   var alertmsg = ' <div class="alert" style="color:green;text-align:center;">please select multiple date!!</div>';
			            $('#reportalert').html(alertmsg);
			            $("#reportalert").fadeTo(2000, 500).slideUp(500, function(){
			              
			                 });
	   				 
	   				return;
			    } 
			    
			    if(diffDays==1 && document.getElementById('filterSingle').checked){
			    	searchType="Single";
			    }else{
			    	searchType="Multiple";
			    }
			    
			    
			    /* if(diffDays==1 && !document.getElementById('filterDateWise').checked){
			    	searchType="Multiple";
			    }else if(diffDays==1 && document.getElementById('filterDateWise').checked){
			    	$("#reportalert").hide();
					   var alertmsg = ' <div class="alert" style="color:green;text-align:center;">please unselect multiple date!!</div>';
			            $('#reportalert').html(alertmsg);
			            $("#reportalert").fadeTo(2000, 500).slideUp(500, function(){
			              
			                 });
	   				 
	   				return;
			    } */
			    
				$scope.reportColumnList();
				if(document.getElementById('fromDate').value==""){
		 			 document.getElementById('fromDate').focus();
		 			 document.getElementById("fromDate").style.borderColor = "red";
		 		 }else if(document.getElementById('toDate').value==""){
		 			 document.getElementById('toDate').focus();
		 			document.getElementById("toDate").style.borderColor = "red";
		 		 }else if(document.getElementById('fromDate').value!="" && document.getElementById('toDate').value!="" && filterDateName!="NA" && searchType!="NA"){

		 			var fdata = "strFromDate=" + $('#fromDate').val()+ "&strToDate=" + $('#toDate').val()
		 			+"&checkedProperty="+checkbox_value+"&filterDateName="+filterDateName+"&searchDateType="+searchType;
		 			spinner.show();
					$http(
							{
								method : 'POST',
								data : fdata,
								headers : {
									'Content-Type' : 'application/x-www-form-urlencoded'
								},
								url : 'consolidate-booking-records'
							}).success(function(response) {
								 
								$scope.bookingrecords = response.data;
								if($scope.bookingrecords.length>0){
									document.getElementById("mydownload").disabled=false;	
									$('#searchRecordId').show();
								}else if($scope.bookingrecords.length==0){
									document.getElementById("mydownload").disabled=true;
								}
								 setTimeout(function(){ spinner.hide(); 
								  }, 1000);
							});
		 		 }
				
			};
			
			$scope.getLocations = function() {

				var url = "get-property-locations";
				$http.get(url).success(function(response) {
					$scope.locations = response.data;
		
				});
			};
			
			$scope.getReportAreas = function(){
   				var locationId=$('#reportLocationId').val();
   				$('#searchProperties').hide();
   				$scope.bookingrecords =["data"];
   				$scope.reportAreaProperty=[];
   				$('#searchOptionsReport').hide();
   				$('#dateRangeId').hide();
   				
   				var url ="get-location-areas?areaLocationId="+locationId;
   				$http.get(url).success(function(response) {
   					$scope.reportareas = response.data;
   				});
   			};
			
   			$scope.getReportAreaProperties = function() {
   	 			var areaId=$('#reportAreaId').val();
   	 			$('#searchProperties').show();
   	 			$scope.bookingrecords =["data"];
				$scope.reportAreaProperty=[];
   	 			var url="area-property?areaId="+areaId;
   	 			$http.get(url).success(function(response) {
					$scope.reportAreaProperty = response.data;
					if($scope.reportAreaProperty.length>1){
						$('#reportPropertyId').show();
					}else{
						$('#reportPropertyId').hide();
					}
					document.getElementById("reportPropertyAll").checked=false;
				});
   	 			
   	 		};
   			
   			$scope.reportPropertyList= function(id,name,indx,properties){
   				$scope.propertyId=id;
   				$('#searchOptionsReport').show();
   				$('#dateRangeId').show();
   				var firstDate = new Date();
   				firstDate.setDate(firstDate.getDate());
   				$("#fromDate").datepicker('setDate', firstDate);
   				
   				var lastDate = new Date();
   				lastDate.setDate(lastDate.getDate() +1);//any date you want
   				$("#toDate").datepicker('setDate', lastDate);
   				
   				document.getElementById("filterSingle").checked=true;
   			};

   			$scope.reportColumnList = function(){
   				var name="REVENUE REPORTS";
   				$scope.fileName="revenuereport";
   				
   				var url ="list-report-columns?reportName="+name;
   				$http.get(url).success(function(response) {
   					$scope.reportColumns = response.data;
   					$scope.listcolumns=[];
   					for(var i=0; i<$scope.reportColumns.length;i++){
   						var columns=$scope.reportColumns[i].columnName;
   						$scope.listcolumns.push(columns);
   					}
   					$scope.bookingrecords=["data"];
   					$scope.getHeader();
   					
   				});
   			};
   			
   			$scope.selectPropertyAll =function(len, areaproperties){
   				for(var i=0;i<areaproperties.length;i++){
   				}
   			};
   			$scope.getChooseDate = function(){
   				var dateSingle=document.getElementById("filterSingle").checked;
   				var dateMultiple=document.getElementById("filterMultiple").checked; 
   				document.getElementById("mydownload").disabled=true;
   				if(dateMultiple){
   					$('#divBookingDate').show();
   	   				$('#divCheckInDate').show();
   	   				document.getElementById("filterBookingDate").checked=true;
   	   				document.getElementById("filterCheckInDate").checked=false;
   	   				
   	   				
   				}else if(dateSingle){
   					$('#divBookingDate').hide();
   	   				$('#divCheckInDate').hide();
   	   				document.getElementById("filterBookingDate").checked=false;
   	   				document.getElementById("filterCheckInDate").checked=false;
	   	   			var firstDate = new Date();
	   				firstDate.setDate(firstDate.getDate());
	   				$("#fromDate").datepicker('setDate', firstDate);
	   				
	   				var lastDate = new Date();
	   				lastDate.setDate(lastDate.getDate() +1);//any date you want
	   				$("#toDate").datepicker('setDate', lastDate);
   				}
   				
   				
   			};

   		 $("#reportPropertyAll").click(function () {
		     $("#reportProperties :checkbox").not(this).prop('checked', this.checked);
		    
		 }); 
   		 
   			$scope.getHeader = function(){
   				return $scope.listcolumns;
   			};
			$scope.getLocations();
			$('#searchOptionsReport').hide();
			$('#dateRangeId').hide();
			$('#searchProperties').hide();
			$('#divBookingDate').hide();
			$('#divCheckInDate').hide();
			document.getElementById("mydownload").disabled=true;
			$('#searchRecordId').hide();
	});
		</script>
		
		<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
		  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
		  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		  <script src="plugins/datepicker/bootstrap-datepicker.js"></script>
		  
		
