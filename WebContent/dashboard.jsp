<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<style>
.selected {
	background-color: black;
	color: white;
	font-weight: bold;
}
</style>
<!-- Select2 -->
<link rel="stylesheet" href="plugins/select2/select2.min.css">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Projects
			<!--<small>QA & file archival</small>-->
		</h1>
	</section>

<div class="pad margin no-print" ng-if="projects.length==0">
          <div class="callout callout-info" style="margin-bottom: 0!important;">
            <h4><i class="fa fa-info"></i> Note:</h4>
           No projects found.
          </div>
        </div>


	<!-- Main content -->
	<section class="content">

		
		<!-- Your Page Content Here -->
		<div class="row">
			
			

			<div class="col-xs-12" ng-if="projects.length>0">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title">My Projects</h3>
					</div>
	
					
					<!-- /.box-header -->
					<div class="box-body table-responsive no-padding" > 
						<table id="example2" class="table table-hover">
							<thead>
								<th>Project</th>
								<th>Created Date</th>
								<th>Status</th>
							</thead>
							 <tr ng-repeat="x in projects">
								<td>
									<!-- <div ng-switch on="x.o_taskcompleteddate"> -->
									<a href="#editCollabModal" ng-click="editUser($index,x.DT_RowId)" data-toggle="modal"><i  class="fa fa-pencil"></i></a>
									<!-- </div> -->
								</td>
								<td>{{x.projectName}}</td>
								<td>{{x.createdDate}}</td>
								<td>
									<div ng-switch on="x.isActive">
										<span class='badge bg-red' ng-switch-when="false">In Active
										</span>
										<span ng-switch-default class='badge bg-green'>Active</span>
									</div>
								</td>
							</tr> 
							<tr>
							</tr>
						</table>
					</div>
					<!-- /.box-body -->
										
					<div class="box-footer clearfix">
						 <ul class="pagination pagination-sm no-margin pull-right">
							<button class="btn" data-toggle="modal" ng-click="enableTask()"
								href="#addCollabModal">Create Project</button>
						</ul>
					</div>
				</div>
				<!-- /.box -->
			</div>
			<!-- /.box -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->

</div>
<s:hidden name="taskId" value=""></s:hidden>
<!-- /.content-wrapper -->
<s:if test="%{objectId != null}">
	<s:hidden name="objectId" value="%{objectId}"></s:hidden>
</s:if>
<s:else>
	<input type="hidden" name="objectId" id="objectId"
		value="<%=request.getParameter("id")%>">
</s:else>

<input type="hidden" name="prefix" id="prefix"
	value="<%=request.getParameter("prefix")%>">
	
	<input type="hidden" name="status" id="status"
	value="<%=request.getParameter("status")%>">
	


<!-- REQUIRED JS SCRIPTS -->


<script
	src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
	
<script>
	var app = angular.module('myApp');
	app.controller(
					'customersCtrl',
					function($scope, $http) {

						 //var taskurl = "http://localhost:8085/collaborative-workflow/gettasksbyuserlatest.action";
						 var taskurl = "gettasksbyuserlatest.action";
							$http.get(taskurl)
							.success(function (response) {$scope.latesttasks = response.data;});
							
							//var notifiurl = "http://localhost:8085/collaborative-workflow/unreadnotifications.action";
							var notifiurl = "unreadnotifications.action";
							$http.get(notifiurl)
							.success(function (response) {$scope.latestnoti = response.data;});

						$scope.getProjects= function() {
						
							
							//var customurl = "http://localhost:8085/collaborative-workflow/gettasksbyuser.action?"+fdata;
							var url = "projectjson.action";
							//alert(customurl);
							$http.get(url).success(function(response) {
								$scope.projects = response.data;
							});
						};
						
						//edit user
						$scope.editUser=function(index,id){
							var fdata = "auserId=" + id;
							//alert(fdata);
							$http(
							{
								method : 'POST',
								data : fdata,
								headers : {
									'Content-Type' : 'application/x-www-form-urlencoded'
								},
								url : 'usersinglejson.action'
							}).then(function successCallback(response) {
									//alert(response.data);
									$scope.singleuser = response.data;
							}, function errorCallback(response) {
								// called asynchronously if an error occurs
								// or server returns response with an error status.
							});
							
							
							
						};
						

				
						$scope.getProjects();
						
						$scope.enableTask = function(taskId) {
							//task button
							$('#taskId').val(taskId);
							

						};
						
						
						 $scope.getPropertyList = function() {
							 
					        	
					        	
						        var userId = $('#adminId').val();
					 			var url = "get-user-properties?userId="+userId;
					 			$http.get(url).success(function(response) {
					 			    
					 				$scope.props = response.data;
					 	
					 			});
					 		};
					 		
			              $scope.change = function() {
			        	   
					        //alert($scope.id);
					        
					        var propertyId = $scope.id;	
			        	    var url = "change-user-property?propertyId="+propertyId;
				 			$http.get(url).success(function(response) {
				 				
				 				 window.location = '/ulopms/dashboard'; 
				 				//$scope.change = response.data;
				 	
				 			});
						       
					 		};
					 		
							
					    $scope.getPropertyList();
						
					});
	

</script>


