<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/themes/south-street/jquery-ui.min.css" />
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<style>
.bookinfodiv{
border: 1px solid #3c8dbc;
}
table.table-bordered>tbody>tr>th { //
	border: 1px solid #3c8dbc;
}

table.table-bordered { //
	border: 1px solid #3c8dbc;
	/*     margin-top:20px; */
}

table.table-bordered>thead>tr>th { //
	border: 1px solid #3c8dbc;
}

table.table-bordered>tbody>tr>td { //
	border: 1px solid #3c8dbc;
}

.tablesuccess {
	background-color: #092f53 !important;
	color: #ffffff !important;
	text-align: center;
}

.boldtext {
	font-weight: bold;
}
.hfourbold{
    color: #092f53 !important;
    font-size: 14px;
    margin-top:5px;
    margin-bottom:5px;
}
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Guest Hotel Booking</h1>
		<ol class="breadcrumb">
			<li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Guest Hotel Booking</li>
		</ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#fa-icons" data-toggle="tab">Hotel
								List</a></li>
						<li><a href="#tab-guest-information" data-toggle="tab">Hotel
								Information</a></li>
						<li><a href="#tab-confirmation-summary" data-toggle="tab">Confirmation
								Guest Information</a></li>
						<li ><a href="#tab-payment-details" data-toggle="tab">Payment Details</a></li>
					</ul>
					<div class="tab-content">
						<!-- Font Awesome Icons -->
						<div class="tab-pane active" id="fa-icons">
							<section class="content">
								<div class="row">
									<form name="bookingForm">
										<div class="form-group col-md-3">
											<label>Select Location</label>
											 <select name="googleLocationId" id="googleLocationId" ng-change="getGoogleAreas()"
												ng-model="googleLocationId"
												class="form-control input-number taxPercentage{{$index}}">
												<option value="">select</option>
												<option ng-repeat="gl in googleLocations" value="{{gl.googleLocationId}}">{{gl.googleLocationName}}</option>
											</select>
										</div>
													<div class="form-group col-md-3">
											<label>Select Area</label> <select name="googleAreaId"
												id="googleAreaId" 
												ng-model="googleAreaId"
												class="form-control input-number taxPercentage{{$index}}">
												<option value="">select</option>
												<option ng-repeat="a in areas" value="{{a.googleAreaUrl}}">{{a.googleAreaDisplayName}}</option>
											</select>
										</div>
										<div class="form-group col-md-3">
											<label>Check-in:</label>
											<div class="input-group date">
												<label class="input-group-addon btn" for="bookCheckin">
													<span class="fa fa-calendar"></span>
												</label> <input type="text" id="bookCheckin" type="text"
													class="form-control" name="bookCheckin" value=""
													ng-required="true" onkeydown="return false"
													autocomplete="off">
											</div>
											<!-- /.input group -->
										</div>
										<div class="form-group col-md-3">
											<label>Check-out:</label>
											<div class="input-group date">
												<label class="input-group-addon btn" for="bookCheckout">
													<span class="fa fa-calendar"></span>
												</label> <input type="text" id="bookCheckout" type="text"
													class="form-control" name="bookCheckout" value=""
													ng-required="true" onkeydown="return false"
													autocomplete="off">
											</div>
											<!-- /.input group -->
										</div>
										<div class="col-md-12">
											<button type="submit" ng-click="getGuestOfflinePropertyList()"
												class="btn  btn-primary btn-sm pull-right"
												>Search</button>
										</div>
									</form>
								</div>
								<div class="table-responsive no-padding">
									<table class="table table-bordered" id="example1">
										<h4>Availability</h4>
										<tr class="tablesuccess">
											<th>S.No</th>
											<th>Hotel Name</th>
											<th> Area</th>
											<th>Room starts from</th>
											<th>Action</th>

										</tr>
										<tr ng-repeat="gha in guestHotelAvailability">
											<td>{{gha.serialNo}}</td>
											<td>{{gha.propertyName}}</td>
											<td>{{gha.areaName}} /{{gha.locationName}}</td>
											<td>{{gha.minimumAmount}}</td>
											<td><button type="button" class="btn btn-success btn-style btn-sm" class="btn btn-success btn-style" id="selectButton{{gha.propertyId}}"  ng-click="selectHotel(gha.propertyId,gha.areaId)" >Select Property</button></td>
<!-- 											<td><a type="button"  -->
<!-- 												class="btn btn-success btn-style btn-sm" -->
<!-- 												ng-click="selectHotel(gha.propertyId,gha.areaId);">Select Property</a></td> -->
										</tr>
									</table>
								</div>
							</section>
							<!-- /.content -->
						</div>
						<!-- /.tab-content -->
						<!-- guest information begins -->
						<div class="tab-pane" id="tab-guest-information">
							<form name="addguestinformation">
								<div class="modal-body">
									<div class="row">

										<div class="col-md-12">
											<div class="table-responsive no-padding">
												<h4>Hotel Details</h4>
												<table class="table table-bordered">
													<tr>
														<th>Hotel Name</th>
														<th>Area</th>
														<th>Amenities</th>
														<th>Near By</th>
													
													</tr>
													<tr ng-repeat="phd in pushHotelDetail">
														<td>{{phd.propertyName}}</td>
														<td>{{phd.areaName}}</td>
														<td><ul ng-repeat="am in phd.amenities">
																<li>{{am.amenityName}}</li>
															</ul></td>
														<td><ul ng-repeat="at in phd.attractions">
																<li>{{at.attraction1}}</li>
																<li>{{at.attraction2}}</li>
																<li>{{at.attraction3}}</li>
																<li>{{at.attraction4}}</li>
																<li>{{at.attraction5}}</li>
															</ul></td>
											
													</tr>

												</table>

											</div>
										</div>

										<!-- Room Details Begins -->
										<div class="col-md-12">
											<div class="table-responsive no-padding">
												<h4>Room Details</h4>
												<table>
												<tr ng-repeat="pa in pushHotelDetail">
												<td style="color:red;"> <b>Check-In Date : {{pa.startDate}}</b></td>
												<td> <b>&nbsp;&nbsp;</b></td>
												<td style="color:red;"> <b>Check-Out Date : {{pa.endDate}}</b></td>
												</tr>
												</table>																

												<table class="table table-bordered">
												
													<tr>
														<th>Room Categoty Name</th>
														<th>Room Count</th>
														<th>Max-Occupancy</th>
														<th>Base Amount</th>
														<th>Discount</th>
														<th>Today's Price Allocation</th>
														<th>Action</th>
													</tr>
													<tr ng-repeat="pa in pushHotelDetail[0].accommodations  track by $index">
														<th>{{pa.accommodationName}}</th>
														<td>{{pa.available}}</td>
														<td>Max {{pa.maxOccupancy}} Person</td>
														<td>{{pa.maximumAmount}}</td>
														<td style="color:red;"><span ng-if="pa.offerAmount != 0" >{{pa.promotionFirstName}}&nbsp;&nbsp;{{pa.promotionSecondName}}&nbsp;&nbsp;/&nbsp;&nbsp;{{pa.offerAmount}} Rs</span></td>
														<td>{{pa.minimumAmount}}</td>
														<td><a type="button" class= "btn btn-danger btn-style btn-sm" ng-show="pa.available == 0 ">sold out</a>
														  <button type="button" ng-hide="pa.available == 0 " class="btn btn-success btn-style btn-sm"  class="btn btn-success btn-style" id="btn{{pa.accommodationId}}"  ng-click="selectType(pa.accommodationId,$index)" >SELECT ROOM</button>
                                    					  <input type='checkbox' name='type[]' style="visibility: hidden;"  value='{{pa.accommodationId}}' id="{{pa.accommodationId}}"  ng-click="manageTypes(pa.accommodationId);"/>
                                    					</td>
														<!-- <td><button class="btn btn-primary">Select</button></td> -->
													</tr>

												</table>

											</div>
										</div>
										<!--   Room Details Ends -->
										<!-- Selected Room Details Begins -->

										<div class="col-md-12">
											<div class="table-responsive no-padding">
												<h4>Seleted Room Details</h4>
												<table class="table table-bordered">
													<tr>
														<th>Room Categoty Name</th>
														<th>Room Count</th>
														<th>Adult</th>
														<th>Child</th>
														<th>Base Tariff</th>
														<th>Extra Amount</th>
														<th>Addon</th>
														<th>Tariff</th>
														<th>Tax</th>
														<th>Advance</th>														
														<th>Due Amount</th>

													</tr>
													<tr ng-repeat="rs in roomsSelected track by $index">
														<td>{{rs.accommodationType}}</td>
														<td><select name="roomCount" id="roomCount{{$index}}"
															 ng-init="rm.value = 1" ng-model="rm.value"
															ng-change="updateRoomAvailable($index,rm.value,ad.value,ch.value,rs.available,rs.baseAmount,rs.minOccu,rs.maxOccu,rs.extraAdult,rs.extraChild,rs.arrival,rs.departure,rs.total,rs.tax,rs.taxPercentage);totalAdultCheck($index,rs.maxOccu,rm.value)">
																<option ng-repeat="rm in range(1,rs.available)" value="{{rm}}">{{rm}}</option>
															
														</select></td>
														<td><select name="adults" id="adults{{$index}}"
															ng-change="updateAvailable($index,rm.value,ad.value,ch.value,rs.available,rs.baseAmount,rs.minOccu,rs.maxOccu,rs.extraAdult,rs.extraChild,rs.arrival,rs.departure,rs.total,rs.tax,rs.taxPercentage);minimumOccupancyCheck($index,rs.maxOccu,ad.value)" ng-init="ad.value = 1" ng-model="ad.value"
															>
																 <option ng-repeat="rm in range(1,rs.adultsAllow)" value="{{rm}}">{{rm}}</option>
														</select></td>
														<td><select name="child" id="child{{$index}}"
															ng-change="updateAvailableChild($index,rm.value,ad.value,ch.value,rs.available,rs.baseAmount,rs.minOccu,rs.maxOccu,rs.extraAdult,rs.extraChild,rs.arrival,rs.departure,rs.total,rs.tax,rs.taxPercentage);totalChildCheck($index,rs.maxOccu,ch.value)" ng-init="ch.value = 0" ng-model="ch.value"
															>
																<option ng-repeat="rm in range(0,rs.childAllow)" value="{{rm}}">{{rm}}</option>
														</select></td>
														<td style="color:red;">{{rs.baseAmount*rs.rooms}}</td>
														<td>{{rs.extraAdultChildAmount}}</td>
														<td>
														<select name="addon" id="addon" 
															ng-change="updateAddon(addon,$index)" ng-model="addon"
															class="form-control ">
																<option value="" ng-selected ="rs.addonAmount==0">Choose Addon</option>
																<option ng-repeat="pa in propertyAddon" value="{{pa.propertyAddonId}}">{{pa.addonName}} -- {{pa.addonRate}}</option>
																
														</select>
														</td>
														<td><input type="text" class="form-control" ng-model="rs.totalForTariff" ng-change="updateTariff(rs.totalForTariff,rs.accommodationId)" value="{{rs.totalForTariff}}"></td>
														<td>{{rs.tax}}</td>
														<td><input type="text" class="form-control" id="advanceAmount{{rs.accommodationId}}" ng-change="updateAdvance(rs.accommodationId,rs.advanceAmount)" ng-model ="rs.advanceAmount" ></td>
														<td>{{getTempTotal(rs.accommodationId,$index)}}</td>

													</tr>
													<div ng-if="roomsSelected.length != '0'">
													<tr>
														<td colspan="9"></td>
														<td style="color:blue;">Advance Amount</td>
														<td style="color:blue;">{{getAdvanceTotal()}}</td>
													</tr>
													<tr>
														<td colspan="9"></td>
														<td style="color:blue;">Discounted Amount</td>
														<td style="color:blue;">{{getDiscountTotal()}}</td>
													</tr>
													<tr>
														<td colspan="9"></td>
														<td style="color:red;">Hotel Pay</td>
														<td style="color:red;">{{getAllTempTotal()}}</td>
													</tr>
													<tr>
														<td colspan="9"></td>
														<td style="color:green;">Total Amount</td>
														<td style="color:green;">{{getTotal()}}</td>
													</tr>
													</div>
													<tr>
														<td colspan="10"></td>

														<td><button ng-click="clickNext()" class="btn btn-primary pull-right">Book
																Now</button></td>

														
													</tr>

												</table>


											</div>
										</div>
										<!-- Selected Room Details Ends -->

									</div>
								</div>
								<div class="modal-footer">
									<button class="btn btn-danger previousBtn pull-left "
										type="button">Previous</button>
									<!-- <button class="btn btn-primary nextBtn" disabled>Next</button> -->
								</div>
							</form>
						

						</div>
						<!-- guest information begins -->
						<div class="tab-pane" id="tab-confirmation-summary">
						<!-- Row Begins -->
						<div class="row">
						<div class="col-md-6 ">
						<h4>Booking Information</h4>
						<form name="guestBookingInformation">
						<div class="col-md-12 bookinfodiv">
						 <div class="form-group col-md-12">
							<label>Hotel Name :</label>
							<h4 class="hfourbold">{{pushHotelDetail[0].propertyName}}</h4>
						</div>
						<div class="form-group col-md-6">
							<label>Check-In  :</label>
							<h4 class="hfourbold">{{pushHotelDetail[0].startDate}}</h4>
						</div>
						<div class="form-group col-md-6">
						<label>Check-In  :</label>
							<h4 class="hfourbold">{{pushHotelDetail[0].endDate}}</h4>
						</div>
						<div class="form-group col-md-6">
						<label>Room Category  :</label>
							<h4 class="hfourbold">{{getRoomCategory()}}</h4>
						</div>
						<div class="form-group col-md-6">
						<label>Room Count :</label>
							<h4 class="hfourbold">{{getTotalRooms()}}</h4>
						</div>
						<div class="form-group col-md-6">
						<label>Adult :</label>
							<h4 class="hfourbold">{{getTotalAdult()}}</h4>
						</div>
						<div class="form-group col-md-6">
						<label>Child :</label>
							<h4 class="hfourbold">{{getTotalChild()}}</h4>
						</div>
							<div class="form-group col-md-6">
						<label>Amount :</label>
							<h4 class="hfourbold">{{getTotalBase()}}</h4>
						</div>
							<div class="form-group col-md-6">
						<label>Tax :</label>
							<h4 class="hfourbold">{{getTotalTax()}}</h4>
						</div>
							<div class="form-group col-md-6">
						<label>Addon Cost :</label>
							<h4 class="hfourbold">{{getTotalAddon()}} - {{roomsSelected[0].addonDescription}}</h4>
						</div>
							<div class="form-group col-md-6">
						<label>Total Amount :</label>
							<h4 class="hfourbold" style="color:red;">{{getTotal()}}</h4>
						</div>
						</div>
						</form>
						</div>
						<div class="col-md-6">
						<h4>Guest Information</h4>
						<form name="guestBookingInformation">
						 <div class="form-group col-md-12">
							<label>Name :</label>
							<input type="text" id="guestName" class="form-control">
						</div>
						 <div class="form-group col-md-12">
							<label>Email:</label>
							<input type="text" id="guestEmail" class="form-control">
						</div>
						 <div class="form-group col-md-12">
							<label>Mobile :</label>
							<input type="text" id="guestPhone" class="form-control">
						</div>
						 <div class="form-group col-md-12">
							<label>Employee Id</label>
							<select name="employeeId" id="employeeId"
															ng-model="employeeId"
															class="form-control ">
																<option value="">Select User</option>
																<option value="{{gbu.userId}}" ng-repeat="gbu in guestBookingUsers">{{gbu.userName}}</option>
														</select>
						</div>
						<%--  <div class="form-group col-md-12"> siva addon change
							<label>Add-on</label>
							<select name="addon" id="addon" 
															ng-change="updateAddon()" ng-model="addon"
															class="form-control ">
																<option value="">Choose Addon</option>
																<option ng-repeat="pa in propertyAddon" value="{{pa.propertyAddonId}}">{{pa.addonName}}</option>
																
														</select>
						</div> --%>
						 <div class="form-group col-md-12">
							<label>Special Request</label>
							<input type="text" id="specialRequest" class="form-control">
						</div>
						 <div class="form-group col-md-4" value="{{getAdvanceTotal()}}" ng-if="getAdvanceTotal() != 0">
							<button href="#addmodal" data-toggle="modal" class="btn btn-warning">Advance payment </button>
						</div>
						  <div class="form-group col-md-4" ng-if="getAdvanceTotal() == 0">
							<button ng-click="blockConfirmBooking()"  class="btn btn-info">Pay @ Hotel</button>
						</div>
						<!--  <div class="form-group col-md-4">
							<button class="btn btn-primary">Pay Now</button>
						</div> -->
						</form>
						<div class="modal" id="addmodal">
                        <div class="modal-dialog">
                           <div class="modal-content">
                              <div class="modal-header">
                                 <button type="button" id="btnclose" class="close" data-dismiss="modal"
                                    aria-hidden="true">x</button>
                                 <h4 class="modal-title">Create Payment Link</h4> 
                                 <div class="text-right">Payable Amount : <strong>{{getTotal()}}</strong></div>
                              </div>
                              <form name="addItems">
                                 <div class="modal-bodys" >
                                    <div class="form-group col-md-12">
                                       <label>Amount: </label>
                                       <input type="text"  id="totalAmounts"   value="{{getAdvanceTotal()}}"  name ="totalAmounts" class="form-control" disabled>
                                    </div>
                                    <div class="form-group col-md-12">
                                       <label>Summary:</label>
                                       <textarea  class="form-control"  name="itemDescription"  id="itemDescription" value=""  placeholder="Description" required maxlength="250"></textarea>
                                    </div>
                                    <div class="form-group col-md-6">
                                       <label>Expiry Date:</label>
                                       <input type="text" class="form-control"  name="expiryDate"  id="expiryDate"  value="" placeholder="Select Date" required maxlength="250" autocomplete="off">
                                    </div>
                                    <div class="form-group col-md-6">
                                       <label>Customer Phone:</label>
                                       <input type="text" class="form-control"  id="itemMobile"  name="itemMobile"   placeholder="Mobile Number"  value="{{mobilePhone}}"  ng-pattern="/^[0-9]*$/" ng-required="true" maxlength="20">
                                       <span ng-show="addItems.itemMobile.$error.pattern" style="color:red">Not a valid Number !</span>
                                    </div>
                                    <div class="form-group col-md-12">
                                       <label>Customer Email:</label>
                                       <input type="text" class="form-control"  name="itemEmail"  id="itemEmail"  placeholder="E-mail" value="{{guestEmail}}" ng-pattern="/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i" ng-required="true" maxlength="50">
                                       <span ng-show="addItems.itemEmail.$error.pattern" style="color:red">Not a valid Email id !</span>
                                    </div>
                                 </div>
                                 <div class="modal-footer">
                                    <a href="#" data-dismiss="modal" class="btn btn-danger">Close</a>
                                    <button ng-click="createLink()" ng-disabled="addItems.$invalid" class="btn btn-primary">Send Payment Link</button>
                                 </div>
                              </form>
                           </div>
                           <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dalog -->
                     </div>
						</div>	
					     </div>
					     <!-- Row Ends -->
						 </div>
						 <div class="tab-pane" id="tab-payment-details">
               
                     <form name="getPaymentDetails">
                        <div class="box-body table-responsive no-padding"  style="height:500px;"> 
								<table class="table table-hover">
								<tr>
					<th>
					<label>Search Payee</label>
				<input type="text" ng-model="search" class="form-control" placeholder="Search">
					</th>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
				

								</tr>
									<tr>
										<th>Id</th>
										<th>Invoice Id</th>
										<th>Email ID</th>
										<th>Contact Number</th>
										<th>Description</th>
										<th>Create Date</th>
										<th>Status</th>
<!-- 										<th>Send Voucher</th> -->
									</tr>
									<tr ng-repeat="payment in paymentDetails | filter:search">
										<td>{{payment.id}}</td>
										<td>{{payment.invoice_id}}</td>
										<td>{{payment.email}}</td>
										<td>{{payment.contact}}</td>
										<td>{{payment.description}}</td>
										<td>{{payment.created_at*1000 | date}}</td>
										<td>{{payment.status}}</td>
<!-- 										<td> -->
<!-- 											<button ng-click="sendVoucher(payment.invoice_id,payment.status)" class="btn btn-primary nextBtn">Send</button> -->
<!-- 										</td> -->
									</tr>
				
								</table>
							</div>
                     </form>
                  </div>
					
						</div>
					</div>
					<!-- /.nav-tabs-custom -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->
		</div>
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- <button id="rzp-button1">Pay</button> -->
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script>
	var link = {

		"customer" : {
			"name" : "Test",
			"email" : "test@test.com",
			"contact" : "9999999999"
		},

		"type" : "link",
		"view_less" : 1,
		"amount" : 2000,
		"currency" : "INR",
		"description" : "Welcome to Ulohotels",
		//    	  "key": "rzp_test_bQQGkpEPtwppNI",
		"key" : "rzp_live_vuYF9peaDXMoQF",
		"name" : "Merchant Name",

		"image" : "/your_logo.png",
		"expire_by" : 1493630556

	};
	var options = {
		//    	"key": "rzp_test_bQQGkpEPtwppNI",
		"key" : "rzp_live_vuYF9peaDXMoQF",
		"amount" : "2000", // 2000 paise = INR 20
		"name" : "Merchant Name",
		"description" : "Purchase Description",
		"image" : "/your_logo.png",
		"handler" : function(response) {
			alert(response.razorpay_payment_id);
		},
		"prefill" : {
			"name" : "Harshil Mathur",
			"email" : "harshil@razorpay.com"
		},
		"notes" : {
			"address" : "Hello World"
		},
		"theme" : {
			"color" : "#F37254"
		}
	};
	var rzp1 = new Razorpay(link);

	document.getElementById('rzp-button1').onclick = function(e) {
		rzp1.open();
		e.preventDefault();
	}
</script>
<script
	src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1"
	src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>
<script src="js/jquery-2.1.1.js" type="text/javascript"></script>
<script>
	$('.datepicker').datepicker({
		dateFormat : 'dd-mm-yy'
	});
</script>
<script>
	var app = angular.module('myApp', [ 'ngProgress' ]);
	app
			.controller(
					'customersCtrl',
					function($scope, $http, $timeout, $interval, ngProgressFactory,
							$filter) {

						var spinner = $('#loadertwo');

						$timeout(function() {
							// $scope.progressbar.complete();
							$scope.show = true;
							$("#pre-loader").css("display", "none");
						}, 2000);
						

						$("#bookCheckin")
								.datepicker(
										{
											dateFormat : 'MM d, yy',

											onSelect : function(formattedDate) {
												var date1 = $('#bookCheckin')
														.datepicker('getDate');
												var date = new Date(Date
														.parse(date1));
												date
														.setDate(date.getDate() + 1);
												var newDate = date
														.toDateString();
												newDate = new Date(Date
														.parse(newDate));
												$('#bookCheckout').datepicker(
														"option", "minDate",
														newDate);
												$timeout(function() {
													document
															.getElementById("bookCheckin").style.borderColor = "LightGray";
												});
											}
										});

						$("#bookCheckout")
								.datepicker(
										{
											dateFormat : 'MM d, yy',

											onSelect : function(formattedDate) {
												var date2 = $('#bookCheckout')
														.datepicker('getDate');
												$timeout(function() {
													document
															.getElementById("bookCheckout").style.borderColor = "LightGray";
												});
											}
										});
						
						
						$scope.stdPrice = 0;
						$scope.unread = function() {
							var notifiurl = "unreadnotifications.action";
							$http.get(notifiurl).success(function(response) {
								$scope.latestnoti = response.data;
							});
						};

						$scope.getAvailability = function() {

							$("#example1 :checkbox")
									.each(
											function() {
												var ischecked = $(this).is(
														":checked");
												if (ischecked) {

													checkbox_value = $(this)
															.val()
															+ ",";


													var text = document
															.getElementById("btn"
																	+ $(this)
																			.val()).firstChild;

													if (document
															.getElementById($(
																	this).val()).checked) {

														document
																.getElementById($(
																		this)
																		.val()).checked = false;

														text.data = "SELECT ROOM";
														document
																.getElementById("btn"
																		+ $(
																				this)
																				.val()).classList
																.remove("actives");

													}

												}
											});

							var fdata = "&strStartDate="
									+ $('#bookCheckin').val() + "&strEndDate="
									+ $('#bookCheckout').val();

							if (document.getElementById('bookCheckin').value == "") {
								document.getElementById('bookCheckin').focus();
								document.getElementById("bookCheckin").style.borderColor = "red";
							} else if (document.getElementById('bookCheckout').value == "") {
								document.getElementById('bookCheckout').focus();
								document.getElementById("bookCheckout").style.borderColor = "red";
							} else if (document.getElementById('bookCheckin').value != ""
									&& document.getElementById('bookCheckout').value != "") {
								spinner.show();
								$http(
										{
											method : 'POST',
											data : fdata,
											headers : {
												'Content-Type' : 'application/x-www-form-urlencoded'
											},
											url : 'get-new-offline-room-availability'
										})
										.success(
												function(response) {

													$scope.availablities = response.data;
													console
															.log($scope.availablities);
													spinner.hide();
													if ($scope.roomsSelected != '') {
														$scope.roomsSelected
																.splice(
																		0,
																		$scope.roomsSelected.length);

													}

												});
							}

						};

						$scope.plus = function(indx, className, limit,
								arrivalDate, departureDate, minOccupancy,
								maxOccupancy, extraAdult, extraInfant,
								extraChild, baseAmount, totalBaseAmount,
								diffDays, taxPercentage) {
							var currentVal = parseInt($('.' + className + indx)
									.val());
							// If is not undefined
							if (!isNaN(currentVal)) {

								if (currentVal >= limit) {

									$('.' + className + indx).val(currentVal);

								} else {
									// Increment
									$('.' + className + indx).val(
											currentVal + 1);

								}
							}

							else {
								// Otherwise put a 0 there
								$('.' + className + indx).val(0);
							}

							var rooms = parseInt($('.rooms' + indx).val());
							var adult = parseInt($('.adult' + indx).val());
							var child = parseInt($('.child' + indx).val());
							var infant = parseInt($('.infant' + indx).val());

							var diffDays = parseInt(diffDays);

							var minOccu = parseInt(minOccupancy * rooms);

							var maxOccu = parseInt(maxOccupancy * rooms);

							var extraAdult = parseInt(extraAdult);
							var extraInfant = parseInt(extraInfant);
							var extraChild = parseInt(extraChild);
							var baseAmount = parseFloat(baseAmount);
							var totalOccu = parseInt(adult + child + infant);
							var total = (rooms * baseAmount);
							var roomAmount = parseFloat(total);
							var adltChd = parseInt(adult + child);

							var adltChdIft = parseInt(adult + child + infant);
							$scope.roomsSelected[indx].rooms = rooms;
							$scope.roomsSelected[indx].total = $scope.roomsSelected[indx].baseAmount
									* rooms;
							$scope.roomsSelected[indx].tempTotal = $scope.roomsSelected[indx].baseAmount
									* rooms;
							$scope.roomsSelected[indx].adultsCount = adult;
							$scope.roomsSelected[indx].childCount = child;
							$scope.roomsSelected[indx].infantCount = infant;

							if (totalOccu > maxOccu) {

								alert("Running out of Maximum Occupancy");

								$('.adult' + indx).val(minOccu);
								$('.child' + indx).val(0);
								$('.infant' + indx).val(0);

								$scope.roomsSelected[indx].adultsCount = 1;
								$scope.roomsSelected[indx].childCount = 0;
								$scope.roomsSelected[indx].infantCount = 0;
								$scope.roomsSelected[indx].total = $scope.roomsSelected[indx].baseAmount
										* rooms;
								$scope.roomsSelected[indx].tempTotal = $scope.roomsSelected[indx].baseAmount
										* rooms;

							}

							else {
								if (adult > minOccu) {
									var adultsLeft = (adult - minOccu);
									$scope.roomsSelected[indx].total = (adultsLeft
											* extraAdult * diffDays + roomAmount);
									$scope.roomsSelected[indx].tempTotal = (adultsLeft
											* extraAdult * diffDays + roomAmount);
								}

								else if (child > minOccu) {
									var childLeft = (child - minOccu);
									$scope.roomsSelected[indx].total = (childLeft
											* extraChild * diffDays + roomAmount);
									$scope.roomsSelected[indx].tempTotal = (childLeft
											* extraChild * diffDays + roomAmount);
								}

								else if (infant > minOccu) {
									var infantLeft = (infant - minOccu);
									$scope.roomsSelected[indx].total = (infantLeft
											* extraInfant * diffDays + roomAmount);
									$scope.roomsSelected[indx].tempTotal = (infantLeft
											* extraInfant * diffDays + roomAmount);
								}

								else if ((adult + child) > minOccu) {
									var childLeft = (adltChd - minOccu);
									$scope.roomsSelected[indx].total = (childLeft
											* extraChild * diffDays + roomAmount);
									$scope.roomsSelected[indx].tempTotal = (childLeft
											* extraChild * diffDays + roomAmount);
								}

								else if ((adult + child + infant) > minOccu
										&& infant > 0) {
									var infantLeft = (adltChdIft - minOccu);
									$scope.roomsSelected[indx].total = (infantLeft
											* extraInfant * diffDays + roomAmount);
									$scope.roomsSelected[indx].tempTotal = (infantLeft
											* extraInfant * diffDays + roomAmount);
								}

							}
							var finTotal = $scope.roomsSelected[indx].total;

							$scope.roomsSelected[indx].tax = Math
									.round(taxPercentage / 100 * finTotal);

							$scope.roomsSelected[indx].totalBaseAmount = $scope.roomsSelected[indx].tax
									+ $scope.roomsSelected[indx].total;
						}

						$scope.minus = function(indx, className, arrivalDate,
								departureDate, minOccupancy, maxOccupancy,
								extraAdult, extraInfant, extraChild,
								baseAmount, totalBaseAmount, diffDays,
								taxPercentage) {

							var currentVal = parseInt($('.' + className + indx)
									.val());
							var limit = 0;
							// If is not undefined
							if (!isNaN(currentVal)) {
								// Increment

								if (currentVal <= limit) {

									$('.' + className + indx).val(currentVal);

								} else {
									// Increment
									$('.' + className + indx).val(
											currentVal - 1);

								}

							} else {
								// Otherwise put a 0 there
								$('.' + className + indx).val(0);
							}

							var rooms = parseInt($('.rooms' + indx).val());
							var adult = parseInt($('.adult' + indx).val());
							var child = parseInt($('.child' + indx).val());
							var infant = parseInt($('.infant' + indx).val());

							var diffDays = parseInt(diffDays);
							var minOccu = parseInt(minOccupancy * rooms);
							var maxOccu = parseInt(maxOccupancy * rooms);
							var extraAdult = parseInt(extraAdult);
							var extraInfant = parseInt(extraInfant);
							var extraChild = parseInt(extraChild);
							var baseAmount = parseFloat(baseAmount);
							var totalOccu = parseInt(adult + child + infant);
							var total = (rooms * baseAmount);
							var roomAmount = parseFloat(total);
							var adltChd = parseInt(adult + child);

							var adltChdIft = parseInt(adult + child + infant);
							$scope.roomsSelected[indx].rooms = rooms;
							$scope.roomsSelected[indx].total = $scope.roomsSelected[indx].baseAmount
									* rooms;
							$scope.roomsSelected[indx].tempTotal = $scope.roomsSelected[indx].baseAmount
									* rooms;
							$scope.roomsSelected[indx].adultsCount = adult;
							$scope.roomsSelected[indx].childCount = child;
							$scope.roomsSelected[indx].infantCount = infant;
							var finTotal = $scope.roomsSelected[indx].total;

							if (totalOccu > maxOccu) {

								alert("Running out of Maximum Occupancy");

								$('.adult' + indx).val(1);
								$('.child' + indx).val(0);
								$('.infant' + indx).val(0);

								$scope.roomsSelected[indx].adultsCount = 1;
								$scope.roomsSelected[indx].childCount = 0;
								$scope.roomsSelected[indx].infantCount = 0;
								$scope.roomsSelected[indx].total = $scope.roomsSelected[indx].baseAmount
										* rooms;
								$scope.roomsSelected[indx].tempTotal = $scope.roomsSelected[indx].baseAmount
										* rooms;

							}

							else {

								if (adult > minOccu) {

									var adultsLeft = (adult - minOccu);
									$scope.roomsSelected[indx].total = (adultsLeft
											* extraAdult * diffDays + roomAmount);
									$scope.roomsSelected[indx].tempTotal = (adultsLeft
											* extraAdult * diffDays + roomAmount);
								}

								if (child > minOccu) {

									var childLeft = (child - minOccu);
									$scope.roomsSelected[indx].total = (childLeft
											* extraChild * diffDays + roomAmount);
									$scope.roomsSelected[indx].tempTotal = (childLeft
											* extraChild * diffDays + roomAmount);
								}

								if (infant > minOccu) {

									var infantLeft = (infant - minOccu);
									$scope.roomsSelected[indx].total = (infantLeft
											* extraInfant * diffDays + roomAmount);
									$scope.roomsSelected[indx].tempTotal = (infantLeft
											* extraInfant * diffDays + roomAmount);
								}

								if ((adult + child) > minOccu) {

									var childLeft = (adltChd - minOccu);
									$scope.roomsSelected[indx].total = (childLeft
											* extraChild * diffDays + roomAmount);
								}

								if ((adult + child + infant) > minOccu
										&& infant > 0) {

									var infantLeft = (adltChdIft - minOccu);
									$scope.roomsSelected[indx].total = (infantLeft
											* extraInfant * diffDays + roomAmount);
									$scope.roomsSelected[indx].tempTotal = (infantLeft
											* extraInfant * diffDays + roomAmount);
								}

							}
							$scope.roomsSelected[indx].tax = Math
									.round(taxPercentage / 100
											* $scope.roomsSelected[indx].total);

							$scope.roomsSelected[indx].totalBaseAmount = $scope.roomsSelected[indx].tax
									+ $scope.roomsSelected[indx].total;

						}

						

						$scope.confirmBooking = function() {

							var text = '{"array":'
									+ JSON.stringify($scope.roomsSelected)
									+ '}';
							var data = JSON.parse(text);
							var sourceid = $('#sourceId').val();
							var discountid = $('#discount').val();
							var totalAmounts = $('#totalAmounts').val();
							$http(
									{
										method : 'POST',
										data : data,
										dataType : 'json',
										headers : {
											'Content-Type' : 'application/json; charset=utf-8'

										},
										url : "add-booking?sourceId="
												+ sourceid

									})
									.then(
											function successCallback(response) {

												var gdata = "firstName="+ $('#firstName').val()
														+ "&emailId="+ $('#guestEmail').val()
														+ "&phone="+ $('#mobilePhone').val();

												$http(
														{
															method : 'POST',
															data : gdata,
															headers : {
																'Content-Type' : 'application/x-www-form-urlencoded'
															},
															url : 'add-guest'

														})
														.then(
																function successCallback(
																		response) {

																	
																	console.log($scope.roomsSelected);
																	var text = '{"array":'+ JSON.stringify($scope.roomsSelected)+ '}';
																	var data = JSON.parse(text);
																	var totalAmounts = $('#totalAmounts').val();
																	var specialRequest = $('#specialRequest').val();
																	var sourceid = $('#sourceId').val();
																	var mailSource = true;
																	$http(
																			{
																				method : 'POST',
																				data : data,
																				dataType : 'json',
																				headers : {
																					'Content-Type' : 'application/json; charset=utf-8'
																				},
																				url : "add-booking-details?txtGrandTotal="+ totalAmounts+ '&specialRequest='+ specialRequest + '&mailSource='+ mailSource+ '&sourceId='+ sourceid
																			})
																			.then(
																					function successCallback(response) {

																						$scope.booked = response.data;
																						console.log($scope.booked.data[0].bookingId);

																					},
																					function errorCallback(
																							response) {
																						alert('process failed please try again!! ')
																					});

																},
																function errorCallback(
																		response) {
																	alert('process failed please try again!! ')
																});

											},
											function errorCallback(response) {
												alert('process failed please try again!! ')
											});

						};


						$scope.getStates = function() {

							var url = "get-states";
							$http.get(url).success(function(response) {
								$scope.states = response.data;

							});
						};

						$scope.getCountries = function() {

							var url = "get-countries";
							$http.get(url).success(function(response) {
								//console.log(response);
								$scope.countries = response.data;

							});
						};

						$scope.getResortDiscount = function() {

							var url = "get-resort-discount";
							$http.get(url).success(function(response) {
								//console.log(response);
								$scope.resortdiscount = response.data;

							});

						}

						$scope.editResortPrice = function(indx, className) {
							var currentVal = parseInt($('.' + className + indx)
									.val());
							if (isNaN(currentVal)) {
								$scope.roomsSelected[indx].total = Math
										.round($scope.roomsSelected[indx].baseAmount
												* $scope.roomsSelected[indx].rooms);

							}

							else {
								$scope.roomsSelected[indx].total = Math
										.round($scope.roomsSelected[indx].tempTotal
												- currentVal);

							}

							$scope.roomsSelected[indx].tax = Math
									.round($scope.roomsSelected[indx].total
											* $scope.roomsSelected[indx].taxPercentage
											/ 100);
							$scope.roomsSelected[indx].totalBaseAmount = Math
									.round($scope.roomsSelected[indx].tax
											+ $scope.roomsSelected[indx].total);

						}

						$scope.editResortTax = function(indx, className) {

							var currentVal = parseInt($('.' + className + indx)
									.val());
							if (isNaN(currentVal)) {
								$scope.roomsSelected[indx].tax = $scope.roomsSelected[indx].tax;
								$scope.roomsSelected[indx].totalBaseAmount = Math
										.round($scope.roomsSelected[indx].tax
												+ $scope.roomsSelected[indx].total);
							} else {
								//var totalAmount = Math.round($scope.roomsSelected[indx].baseAmount  * $scope.roomsSelected[indx].rooms);
								$scope.roomsSelected[indx].tax = Math
										.round($scope.roomsSelected[indx].total
												* currentVal / 100);
								$scope.roomsSelected[indx].totalBaseAmount = Math
										.round($scope.roomsSelected[indx].tax
												+ $scope.roomsSelected[indx].total);

							}
						}
						
						$scope.roomsSelected = [];

						console.log($scope.roomsSelected);
						// shiva add addrom tax

						$scope.addRow = function(id, type, baseAmount, arrival,
								departure, rooms, adultsAllow, childAllow,
								minOccu, maxOccu, extraAdult,
								extraChild, available, indx, tax, diffDays,
								taxPercentage, promotionName,propertyId,  minAmount, maxAmount,discountAmount) {
					
							
							if (available == 0) {
								console.log("Disable");
							} else {

								var type = type.replace(/\s/g, '');
								var id = parseInt(id);
								var minOccu = parseInt(minOccu);
								var maxOccu = parseInt(maxOccu);
								var extraAdult = parseInt(extraAdult);
								var extraInfant = parseInt(0);
								var extraChild = parseInt(extraChild);

								var adultsAllow = parseInt(adultsAllow);
								var childAllow = parseInt(childAllow);

								var amount = parseFloat(baseAmount);
								var taxPercentage = parseFloat(taxPercentage);
								var rooms = parseInt(rooms);
								var diffDays = parseInt(diffDays);
								var taxes = parseInt(tax * diffDays);

							    var randomNo = Math.random();
								var baseAmountTax = amount / diffDays;
								var total = (amount);
								var tempTotal = (amount);
								var totalBaseAmount = total + taxes;
								var minimumAmount = parseFloat(minAmount);
								var maximumAmount = parseFloat(maxAmount);
								var promoAmount = parseFloat(discountAmount);
								//shiva update newData diffDays only
								var newData = {
									'arrival' : arrival,
									'indx' : indx,
									'available' : available,
									'departure' : departure,
									'accommodationId' : id,
									'sourceId' : 12,
									'accommodationType' : type,
									'rooms' : rooms,
									'diffDays' : diffDays,
									'baseAmount' : amount,
									'total' : total,
									'adultsAllow' : adultsAllow,
									'childAllow' : childAllow,
									'minOccu' : minOccu,
									'maxOccu' : maxOccu,
									'tempMinOccu':minOccu,
									'tempMaxOccu':maxOccu,
									'extraAdult' : extraAdult,
									'extraInfant' : extraInfant,
									'extraChild' : extraChild,
									'adultsCount' : 1,
									'infantCount' : 0,
									'childCount' : 0,
									'refund' : 0,
									'statusId' : 2,
									'tax' : taxes,
									'roomTax' : taxes,
									'randomNo' : randomNo,
									'totalBaseAmount' : totalBaseAmount,
									'taxPercentage' : taxPercentage,
									'discountId' : 35,
									'propertyId' : propertyId,
									'tempTotal' : tempTotal,
									'totalForAdvance' : total,
									'totalForAddon' : total,
									'totalForTariff' : total,
									'finalTotal':total,
									'otaCommission' : 0,
									'otaTax' : 0,
									'addonDescription' : "NA",
									'addonAmount' : 0,
									'advanceAmount' : 0,									
									'minimumAmount' : minimumAmount,
									'promotionAmount' : promoAmount,
									'tempPromotionAmount' : promoAmount,
									'maximumAmount' : maximumAmount,
									'extraAdultChildAmount' : 0
								};

								$scope.roomsSelected.push(newData);
							}
						};

						$scope.getTotal = function() {

							var total = 0;
							var tax = 0;
							var addon = 0;
							for ( var i = 0; i < $scope.roomsSelected.length; i++) {
								var accommodation = $scope.roomsSelected[i];
								tax += parseInt(accommodation.tax); 
								total += parseInt(accommodation.total);
							}

							var all = (total + tax + addon);
							if(all!=0 || all !=null){
								
							return all;
							}else{
								return 0;
								}
							
						}

						$scope.getTempTotal = function(id,indx) {
							var total = 0;
							var tax = 0;
							for ( var i = 0; i < $scope.roomsSelected.length; i++) {
								var accommodation = $scope.roomsSelected[i];
								if(accommodation.accommodationId == id){
								tax += (accommodation.tax);
								total += (accommodation.tempTotal);
								}
							}
							
							var all = (total + tax);
							if(all!=0 || all !=null){
								
							return all;
							}else{
								return 0;
								}
							
						}
						
						$scope.getAllTempTotal = function() {
							var total = 0;
							var tax = 0;
							for ( var i = 0; i < $scope.roomsSelected.length; i++) {
								var accommodation = $scope.roomsSelected[i];
								tax += (accommodation.tax);
								total += (accommodation.tempTotal);
							}
							
							var all = (total + tax);
							if(all!=0 || all !=null){
								
							return all;
							}else{
								return 0;
								}
							
						}
						
						
						$scope.updateTariff = function(val,id){
							if(val  == ''){
								val=0;
							}
							for ( var i = 0; i < $scope.roomsSelected.length; i++) {
								var accommodation = $scope.roomsSelected[i];
								if(accommodation.accommodationId == id){
									var taxPercent = accommodation.taxPercentage;
									if(accommodation.totalForAddon<val){
										accommodation.promotionAmount = Math.round(parseInt(accommodation.tempPromotionAmount*accommodation.rooms));	
									}else{
										accommodation.promotionAmount = Math.round(( parseInt(accommodation.totalForAddon) - val) + parseInt(accommodation.tempPromotionAmount*accommodation.rooms));
									}
									accommodation.tax = Math.round(parseInt((val * taxPercent)/100));
									accommodation.total = parseInt(val);
									accommodation.tempTotal = parseInt(val);
									accommodation.totalForAdvance = parseInt(val);
									accommodation.advanceAmount = 0;
									
								}
							}
							console.log(JSON.stringify($scope.roomsSelected));
						}
						
						$scope.getTotalBase = function(){
							
							var totalBase = 0;
							for ( var i = 0; i < $scope.roomsSelected.length; i++) {
									var accommodation = $scope.roomsSelected[i];
										 totalBase += Math.round(accommodation.total);
								}
							return totalBase;
							}
						
						$scope.getTotalAddon = function(){
							
							var totalAddon = 0;
							for ( var i = 0; i < $scope.roomsSelected.length; i++) {
									var accommodation = $scope.roomsSelected[i];
									totalAddon += Math.round(accommodation.addonAmount);
								}
							return totalAddon;
							}
						
						$scope.getTotalTax = function(){
							
							var totalTax = 0;
							for ( var i = 0; i < $scope.roomsSelected.length; i++) {
									var accommodation = $scope.roomsSelected[i];
									totalTax += accommodation.tax;
								}
							return totalTax;
							}
						
						$scope.getTotalAdult = function(){
							
							var totalAdult = 0;
							for ( var i = 0; i < $scope.roomsSelected.length; i++) {
									var accommodation = $scope.roomsSelected[i];
									totalAdult += accommodation.adultsCount;
								}
							return totalAdult;
							}
						
						
						
						$scope.getTotalChild = function(){
							
							var totalChild = 0;
							for ( var i = 0; i < $scope.roomsSelected.length; i++) {
									var accommodation = $scope.roomsSelected[i];
									totalChild += accommodation.childCount;
								}
							return totalChild;
							}
							
						$scope.getTotalRooms = function(){
							
							var totalRooms = 0;
							for ( var i = 0; i < $scope.roomsSelected.length; i++) {
									var accommodation = $scope.roomsSelected[i];
									totalRooms += accommodation.rooms;
								}
							return totalRooms;
							}	
						
						$scope.getRoomCategory = function(){
							
							var roomCategory = [];
							for ( var i = 0; i < $scope.roomsSelected.length; i++) {
									var accommodation = $scope.roomsSelected[i];
									roomCategory.push(accommodation.accommodationType);
								}
							return roomCategory;
							}	
						
						$scope.updateAdvance = function(id,advance) {
							var advance = parseInt(advance);
							for ( var i = 0; i < $scope.roomsSelected.length; i++) {
								var accommodation = $scope.roomsSelected[i];
								if(accommodation.accommodationId == id){
								var amount = $('#advanceAmount'+id).val();
									if(amount == ''){																			
										advance = 0;
									}	
									accommodation.advanceAmount = advance;
									accommodation.tempTotal = Math.round(accommodation.totalForAdvance) - advance;
									
								}
							}
							console.log(JSON.stringify($scope.roomsSelected));
						}
						
						
						$scope.getAdvanceTotal = function() {

							var advanceTotal = 0;
							for ( var i = 0; i < $scope.roomsSelected.length; i++) {
								var accommodation = $scope.roomsSelected[i];
								advanceTotal += parseInt(accommodation.advanceAmount); 
							}
							
							if(advanceTotal!=0 || advanceTotal !=null){
								
							return advanceTotal;
							}else{
								return 0;
								}
							
						}
						
						$scope.getDiscountTotal = function() {

							var promotionTotal = 0;
							for ( var i = 0; i < $scope.roomsSelected.length; i++) {
								var accommodation = $scope.roomsSelected[i];
								promotionTotal += parseInt(accommodation.promotionAmount); 
							}
							
							if(promotionTotal!=0 || promotionTotal !=null){
								
							return promotionTotal;
							}else{
								return 0;
								}
							
						}
						
						
						$scope.getPaymentStatus=function(){
						
							var url = "get-payment-status";
							$http.get(url).success(function(response) {
								$scope.paymentDetails= response.data;
							});
						};
						

						$scope.createLink = function() {
							var Amount = $('#totalAmounts').val();
							for(var i=0;$scope.roomsSelected.length>i;i++){
								$scope.roomsSelected[i].advanceAmount = Amount;								
									
							}
							
							var type = "link";
							var view_less = 1;
							
							var amount = Amount * 100;
							var currency = "INR";
							var description = "Payment Link";
							var expire_by = Math.floor(new Date(
									$('#expiryDate').val()).getTime() / 1000);
							var name = $('#guestName').val();
							var email = $('#itemEmail').val();
							var phone = $('#itemMobile').val();

						

							var idata = "type=" + type + "&view_less="
									+ view_less + "&amount=" + amount
									+ "&name=" + name + "&email=" + email
									+ "&phone=" + phone + "&currency="
									+ currency + "&description=" + description
									+ "&expire_by=" + expire_by;
							$http(
									{
										method : 'POST',
										data : idata,

										headers : {
											'Content-Type' : 'application/x-www-form-urlencoded'
										},
										url : 'create-payment-link'

									}).success(function(response) {

								$scope.invoice = response.data;
								
								//alert("link sent successfully");
								$scope.invoiceId = $scope.invoice[0].id;
								
								$scope.blockConfirmBooking();
								$timeout(function() {
									$('#btnclose').click();
								}, 1000);

							});

						};
						
						$scope.blockConfirmBooking = function(){
							spinner.show();
		        			var invoiceId = null;
		        			var blockFlag = true;
		        			if(typeof invoiceId === 'undefined'){
		        				invoiceId=='valid';
		        			} 
		                	 if(invoiceId=="" && invoiceId=='undefined')
		                	 {
		                		 invoiceId=='valid';
		                	 }
		                	 if($scope.invoiceId != null){
		                		 invoiceId =  $scope.invoiceId;
		                	 }
		                	  for(var i=0;$scope.roomsSelected.length>i;i++){
									if($scope.roomsSelected[i].advanceAmount == 0){
										 blockFlag = false;	
									}
										
								} 
		                	 
		                   var userId =  $('#employeeId').val();          
				           var text = '{"array":' + JSON.stringify($scope.roomsSelected) + '}';
				           var data = JSON.parse(text);
				           var sourceid =  12;
				           var discountid =  $('#discount').val(); 
				           var totalAmounts =  $('#totalAmounts').val();
				           $http(
				           {
				           method : 'POST',
				           data : data,
				           dataType: 'json',
				           headers : {
				           'Content-Type' : 'application/json; charset=utf-8'
				           
				           },
				          url : "add-booking?sourceId="+sourceid+"&invoiceId=" + invoiceId+'&blockFlag='+blockFlag+'&userId='+userId
				           
				           
				           }).then(function successCallback(response) {
				           
				           
				           var gdata = "firstName=" + $('#guestName').val()
				           + "&emailId=" + $('#guestEmail').val()
				           + "&phone=" + $('#guestPhone').val();
				           $http(
				           {
				           method : 'POST',
				           data : gdata,
				           headers : {
				           'Content-Type' : 'application/x-www-form-urlencoded'
				           },
				           url : 'add-guest' 
				           }).then(function successCallback(response) {
				           
				           var text = '{"array":' + JSON.stringify($scope.roomsSelected) + '}';
				           var data = JSON.parse(text);
				           var discountid =  $('#discount').val(); 
				           var totalAmounts =  $('#totalAmounts').val();
				           var specialRequest =  $('#specialRequest').val();
				           var sourceid = 12;
				           
				           var mailSource = true;
				           $http(
				               {
				           method : 'POST',
				           data : data,
				           dataType: 'json',
				           headers : {
				           'Content-Type' : 'application/json; charset=utf-8'
				           },
				           url : "add-booking-details?txtGrandTotal="+totalAmounts+'&specialRequest='+specialRequest+'&mailSource='+mailSource+'&sourceId='+sourceid+'&blockFlag='+blockFlag+'&userId='+userId
				              }).then(function successCallback(response) {
				           
				           
				                $scope.booked = response.data;
				                if(blockFlag){
				                	  alert("Payment link has been sent");
				                }
				                else{
				                	alert("Room Booked Successfully");	
				                }
				                
						           window.location.reload();
				                
				  
				              }, function errorCallback(response) {
				            	  alert('process failed please try again!! ')
				           });
				           
				           }, function errorCallback(response) {
				        	   alert('process failed please try again!! ')
				           });
				           
				           }, function errorCallback(response) {
				        	   alert('process failed please try again!! ')
				           });
				          
		             };

						/* $scope.goLink = function() {

 							var invoiceId = $('#linkId').val();
							var fdata = "invoiceId=" + invoiceId;


							$http(
									{
										method : 'POST',
										data : fdata,
										headers : {
											'Content-Type' : 'application/x-www-form-urlencoded'
										},
										url : 'get-invoice-status'
									}).success(function(response) {

								$scope.linku = response.data;
								if ($scope.linku[0].amount_paid == 0) {
									alert("Payment Pending");
								} else {
									alert("Payment Success");
								}

							});

						}; */

						$scope.getTaxes = function() {

							var url = "get-taxes";
							$http.get(url).success(function(response) {
								//console.log(response);
								$scope.taxes = response.data;
							});
						};

						$scope.getSources = function() {

							var url = "get-resort-sources";
							$http.get(url).success(function(response) {
								//console.log(response);
								$scope.sources = response.data;

							});
						};

						 	    
						 	   $scope.getGoogleLocations = function() {
									var url = "get-area-google-locations";
									$http.get(url).success(function(response) {
									    console.log(response);
										$scope.googleLocations = response.data;
							
									});
								};
								
								$scope.getGoogleAreas = function() {
									var areaLocationId=$('#googleLocationId').val();
									var url = "get-google-location-areas?areaLocationId="+areaLocationId;
									$http.get(url).success(function(response) {
										$scope.areas = response.data;
									});
								};  
								
								$scope.getGuestBookingUsers = function() {
									var url = "get-guest-booking-users";
									$http.get(url).success(function(response) {
										$scope.guestBookingUsers = response.data;
									});
								};
								
								$scope.getGuestOfflinePropertyList = function() {
									spinner.show();
									$scope.roomsSelected = [];
									$scope.pushHotelDetail = [];
									var fdata = "startDate=" +$('#bookCheckin').val()
									 			+"&endDate=" +$('#bookCheckout').val()
									 			+"&googleLocationId=" +$('#googleLocationId').val()
									 			+"&locationUrl=" +$('#googleAreaId').val();



									$http(
											{
												method : 'POST',
												data : fdata,
												headers : {
													'Content-Type' : 'application/x-www-form-urlencoded'
												},
												url : 'get-guest-area-properties'
											}).success(function(response) {

										$scope.guestHotelAvailability = response.data;
										setTimeout(function(){ spinner.hide(); 
								  }, 1000);
										

									});

								};
								
								$scope.selectHotel = function(id,areaid) {
									 for(var i=0;$scope.guestHotelAvailability.length > i;i++){
										if($scope.guestHotelAvailability[i].propertyId == id){
										     var propId = $scope.guestHotelAvailability[i].propertyId;
											document.getElementById("selectButton"+propId).innerHTML = "Selected";
							                
										}
										else{
											var propId = $scope.guestHotelAvailability[i].propertyId;
											 document.getElementById("selectButton"+propId).innerHTML = "Select Property";
										}
									} 
									 spinner.show();
									
									$scope.pushHotelDetail = [];
									$scope.roomsSelected = [];
									var fdata = "startDate=" +$('#bookCheckin').val()
						 			+"&endDate=" +$('#bookCheckout').val()
						 			+"&propertyId=" +id
						 			+"&areaId=" +areaid;

						$http(
								{
									method : 'POST',
									data : fdata,
									headers : {
										'Content-Type' : 'application/x-www-form-urlencoded'
									},
									url : 'select-guest-area-properties'
								}).success(function(response) {
									
								
									setTimeout(function(){ spinner.hide(); 
									  }, 1000);
							$scope.pushHotelDetail = response.data;
							$scope.getPropertyAddon(id);
							$('.nav-tabs > .active').next('li').find('a')
							.trigger('click');
							

						});
									
								};
								
								$scope.selectType = function(id,indx){
									
									
					                var text = document.getElementById("btn"+id).firstChild;
					                 
					                if(document.getElementById(id).checked){
					                
					                document.getElementById(id).checked = false;
					                
					                text.data = "SELECT ROOM";
					                document.getElementById("btn"+id).classList.remove("actives");
					                $scope.manageTypes(id);
					                
					                }
					                
					                else{
					                text.data = "SELECTED";  
					                document.getElementById(id).checked = true;   
					                document.getElementById("btn"+id).classList.add("actives");
					                $scope.manageTypes(id,indx);
					                
					                }
					                  
					            }

								 $scope.manageTypes = function(id,indx){
								        if(document.getElementById(id).checked){
								        
								         var dt = $scope.pushHotelDetail[0].accommodations[indx];
								       var dd = $scope.pushHotelDetail[0];
								       $scope.addRow(dt.accommodationId,dt.accommodationName,dt.minimumAmount,dd.arrivalDate, dd.departureDate,dt.rooms,dt.noOfAdults,
								        		 dt.noOfChild,dt.minOccupancy,dt.maxOccupancy,dt.extraAdult,dt.extraChild,dt.available,indx,dt.tax,
								        		 dt.diffDays,dt.taxPercentage,dt.promotionFirstName,dd.propertyId,dt.minimumAmount,dt.maximumAmount,dt.difference);
								         
								        
								         console.log($scope.roomSelected);
								        
								      } 
								        
								     else{
								        
								        $scope.cleaner($scope.roomsSelected,id);
								      }
								   
								        
								            };
								            
								            
								            $scope.cleaner = function(arr,id){
								          	
								              	for (var i = 0; i < arr.length; i++) {
								              	        var cur = arr[i];
								              	        
								              	        if (cur.accommodationId == id) {
								              	            arr.splice(i, 1);
								              	            break;
								              	        }
								              	    }
								              	 
								              };
								              $scope.updateRoomAvailable = function(indx, room, adult, child, available, baseAmount, minOccupancy, maxOccupancy, extraAdult, extraChild, arrivalDate, departureDate,totalAmount,tax,taxPercent) {
								            	    var adult = parseInt(adult);
									                var child = parseInt(child);
									                var date1 = new Date(arrivalDate);
									                var date2 = new Date(departureDate);
									                var amount = parseFloat(baseAmount);
									                var timeDiff = Math.abs(date2.getTime() - date1.getTime());
									                var room = parseInt(room);
									                var tax = parseInt(tax);
									                $('#adults'+indx).val(1);
								                    $('#child'+indx).val(0);
									                var diffDays = parseInt(Math.round(timeDiff / (1000 * 3600 * 24)));
									                var totalOccu = parseInt(adult + child);
									                var total = (room * amount);
									                var roomAmount = parseFloat(total);
									                var adltChd = parseInt(adult + child);
									                var adltChdIft = parseInt(adult + child);
									                $scope.roomsSelected[indx].room = room;
									                $scope.roomsSelected[indx].totalBaseAmount = roomAmount;
									                $scope.roomsSelected[indx].adultsCount = adult;
									                $scope.roomsSelected[indx].childCount = child;
									                $scope.roomsSelected[indx].rooms = room;
									                $scope.roomsSelected[indx].childAllow = 0;
									                $scope.roomsSelected[indx].addonAmount = 0;
									                $scope.roomsSelected[indx].promotionAmount = parseInt($scope.roomsSelected[indx].tempPromotionAmount * room);
									                var tempMinOccu =  $scope.roomsSelected[indx].tempMinOccu;
									                var tempMaxOccu =  $scope.roomsSelected[indx].tempMaxOccu;
									                $scope.roomsSelected[indx].minOccu = parseInt(tempMinOccu * room);
									                $scope.roomsSelected[indx].maxOccu = parseInt(tempMaxOccu * room);
									                var minOccu = parseInt($scope.roomsSelected[indx].minOccu);
									                var maxOccu = parseInt($scope.roomsSelected[indx].maxOccu);
									                
									                
									                var roomTax = $scope.roomsSelected[indx].tax;
									                $scope.roomsSelected[indx].total = Math.round(roomAmount);
									                $scope.roomsSelected[indx].tempTotal = Math.round(roomAmount);
									                $scope.roomsSelected[indx].totalForAdvance = Math.round(roomAmount);
									                $scope.roomsSelected[indx].totalForAddon = Math.round(roomAmount);
									                $scope.roomsSelected[indx].totalForTariff = Math.round(roomAmount);
									                var maxPrice = $scope.roomsSelected[indx].maximumAmount;
									                $scope.roomsSelected[indx].maximumAmount = Math.round(maxPrice * room);
									                $scope.roomsSelected[indx].advanceAmount = 0;
									                //$scope.roomsSelected[indx].discountBaseAmount = roomAmount;
									                $scope.roomsSelected[indx].extraAdultChildAmount = 0;
									                $scope.roomsSelected[indx].tax = Math.round(parseInt((roomAmount * taxPercent)/100));
								                    $scope.roomsSelected[indx].taxPercentage = taxPercent;
									               if (totalOccu > maxOccu) {
									                    alert("Runnig out of Maximum Occupancy");
									                    $('#adults').val(minOccu);
									                    $('#child').val(0);
									                    
									                    $scope.roomsSelected[indx].adultsAllow = minOccu;
									                    $scope.roomsSelected[indx].childAllow = 0;
									                    
									                  
									                } else {
									                    if (adltChd > minOccu) {
									                    	
									                        adultsLeft = (adltChd)-(minOccu+childLeft);
									                        var total = (diffDays * adultsLeft * extraAdult + roomAmount)+(childLeft*extraChild);
									                        $scope.roomsSelected[indx].total = Math.round(total);
									                        $scope.roomsSelected[indx].tempTotal = Math.round(total);
									                        $scope.roomsSelected[indx].totalForAdvance = Math.round(total);
									                        $scope.roomsSelected[indx].totalForAddon = Math.round(total);
									                        $scope.roomsSelected[indx].totalForTariff = Math.round(total);
									                        var tax = (total*taxPercent/100);
									                        $scope.roomsSelected[indx].tax = Math.round(tax); 
									                        $scope.roomsSelected[indx].extraAdultChildAmount = (diffDays * adultsLeft * extraAdult)+(childLeft*extraChild);
									                        
									                    } 
									                    
									                    else if(adltChd <= minOccu) {
									                    	
									                    	  childLeft = 0;	
										                      adultsLeft = 0; 
										                        
										                    }

									                	
									                }
									                console.log(JSON.stringify($scope.roomsSelected));
									            };
									            
									            
									            $scope.updateAvailable = function(indx, room, adult, child, available, baseAmount, minOccupancy, maxOccupancy, extraAdult, extraChild, arrivalDate, departureDate,totalAmount,tax,taxPercent) {
									            	var adult = parseInt(adult);
									                var child = parseInt(child);
									                var date1 = new Date(arrivalDate);
									                var date2 = new Date(departureDate);
									                var amount = parseFloat(baseAmount);
									                var timeDiff = Math.abs(date2.getTime() - date1.getTime());
									                var room = parseInt(room);
									                var tax = parseInt(tax);
									                var minOccu = parseInt(minOccupancy);
									                var maxOccu = parseInt(maxOccupancy);
									                var diffDays = parseInt(Math.round(timeDiff / (1000 * 3600 * 24)));
									                var totalOccu = parseInt(adult + child);
									                var total = (room * amount);
									                var addonPrice = parseInt($scope.roomsSelected[indx].addonAmount);
									                var roomAmount = parseFloat(total+addonPrice);
									                var adltChd = parseInt(adult + child);
									                var adltChdIft = parseInt(adult + child);
									                
									                $scope.roomsSelected[indx].room = room;
									                $scope.roomsSelected[indx].adultsCount = adult;
									                $scope.roomsSelected[indx].childCount = child;
									                $scope.roomsSelected[indx].rooms = room;
									               
									                var roomTax = $scope.roomsSelected[indx].roomTax;
									                
									                $scope.roomsSelected[indx].total = Math.round(roomAmount);
									                $scope.roomsSelected[indx].tempTotal = Math.round(roomAmount);
									                $scope.roomsSelected[indx].totalForAdvance = Math.round(roomAmount);
									                $scope.roomsSelected[indx].totalForAddon = Math.round(roomAmount);
									                $scope.roomsSelected[indx].totalForTariff = Math.round(roomAmount);
									                $scope.roomsSelected[indx].extraAdultChildAmount = 0;
									                $scope.roomsSelected[indx].advanceAmount = 0;
									                $scope.roomsSelected[indx].promotionAmount = parseInt($scope.roomsSelected[indx].tempPromotionAmount * room);
									                $scope.roomsSelected[indx].tax = Math.round(parseInt((roomAmount * taxPercent)/100));
								                    $scope.roomsSelected[indx].taxPercentage = taxPercent;
								                    var childLeft = 0;
									                var adultsLeft = 0;
									                
									                

									                if (totalOccu > maxOccu) {
									                    alert("Runnig out of Maximum Occupancy");
									                    $('#adults').val(minOccu);
									                    $('#child').val(0);
									                    
									                    $scope.roomsSelected[indx].adultsCount = minOccu;
									                    $scope.roomsSelected[indx].childCount = 0;
									                  
									                } else {
									                    if (adltChd > minOccu) {
									                    	if(child > 0){
									                    		childLeft = child;   	
									                    	}
									                        adultsLeft = (adltChd)-(minOccu+childLeft);
									                        var total = (diffDays * adultsLeft * extraAdult + roomAmount)+(childLeft*extraChild);
									                       /*  var calTotal = Math.round(parseInt(total/room));
									                        if(calTotal < 1000){
												                taxPercent = 0;	
												                }else if(calTotal >= 1000 && calTotal < 2500){
												                taxPercent = 12;	
												                }else if(calTotal >= 2500 && calTotal < 7500){
												                taxPercent = 18;	
												                }else if(calTotal >= 7500){
												                 taxPercent = 28;		
												                } */
									                        $scope.roomsSelected[indx].total = Math.round(total);
									                        $scope.roomsSelected[indx].tempTotal = Math.round(total);
									                        $scope.roomsSelected[indx].totalForAdvance = Math.round(total);
									                        $scope.roomsSelected[indx].totalForAddon = Math.round(total);
									                        $scope.roomsSelected[indx].totalForTariff = Math.round(total);
									                        $scope.roomsSelected[indx].extraAdultChildAmount = (diffDays * adultsLeft * extraAdult)+(childLeft*extraChild)
									                        var tax = (total*taxPercent/100);
									                        $scope.roomsSelected[indx].tax = Math.round(tax); 
									                        
									                        
									                    } 

									                	
									                }
									                console.log(JSON.stringify($scope.roomsSelected));
									            };
									            
									            
									            $scope.updateAvailableChild = function(indx, room, adult, child, available, baseAmount, minOccupancy, maxOccupancy, extraAdult, extraChild, arrivalDate, departureDate,totalAmount,tax,taxPercent) {
									            	var adult = parseInt(adult);
									                var child = parseInt(child);
									                var date1 = new Date(arrivalDate);
									                var date2 = new Date(departureDate);
									                var amount = parseFloat(baseAmount);
									                var timeDiff = Math.abs(date2.getTime() - date1.getTime());
									                var room = parseInt(room);
									                var tax = parseInt(tax);
									                var minOccu = parseInt(minOccupancy);
									                var maxOccu = parseInt(maxOccupancy);
									                var diffDays = parseInt(Math.round(timeDiff / (1000 * 3600 * 24)));
									                var totalOccu = parseInt(adult + child);
									                var total = (room * amount);
									                var addonPrice = parseInt($scope.roomsSelected[indx].addonAmount)
									                var roomAmount = parseFloat(total+addonPrice);
									                var adltChd = parseInt(adult + child);
									                var adltChdIft = parseInt(adult + child);
									                $scope.roomsSelected[indx].room = room;
									                $scope.roomsSelected[indx].adultsCount = adult;
									                $scope.roomsSelected[indx].childCount = child;
									                $scope.roomsSelected[indx].rooms = room;
									             	$scope.roomsSelected[indx].maxAdult = maxOccu - child ; 
									                
									                var roomTax = $scope.roomsSelected[indx].roomTax;
									                
									                $scope.roomsSelected[indx].total = Math.round(roomAmount);
									                $scope.roomsSelected[indx].tempTotal = Math.round(roomAmount);
									                $scope.roomsSelected[indx].totalForAdvance = Math.round(roomAmount);
									                $scope.roomsSelected[indx].totalForAddon = Math.round(roomAmount);
									                $scope.roomsSelected[indx].totalForTariff = Math.round(roomAmount);
									                $scope.roomsSelected[indx].extraAdultChildAmount = 0;
									                $scope.roomsSelected[indx].advanceAmount = 0;
									                $scope.roomsSelected[indx].promotionAmount = parseInt($scope.roomsSelected[indx].tempPromotionAmount * room);
									               	$scope.roomsSelected[indx].tax = Math.round(parseInt((roomAmount * taxPercent)/100));
								                    $scope.roomsSelected[indx].taxPercentage = taxPercent;
									                var childLeft = 0;
									                var adultsLeft = 0;
									                
									                

									                if (totalOccu > maxOccu) {
									                    alert("Runnig out of Maximum Occupancy");
									                    $('#adults').val(minOccu);
									                    $('#child').val(0);
									                    
									                    $scope.roomsSelected[indx].adultsCount = minOccu;
									                    $scope.roomsSelected[indx].childCount = 0;
									                  
									                } else {
									                	
									                    if (adltChd > minOccu) {
										                    	if(adult > minOccu){
										                    		adultsLeft = parseInt(adult - minOccu);   	
										                    	}
										                        childLeft = (adltChd)-(minOccu+adultsLeft);
									                        console.log($scope.roomsSelected[indx]);
									                    	var total = (diffDays * childLeft * extraChild + roomAmount)+(adultsLeft*extraAdult);
									                    	$scope.roomsSelected[indx].total = Math.round(total);
									                    	$scope.roomsSelected[indx].tempTotal = Math.round(total);
									                    	$scope.roomsSelected[indx].totalForAdvance = Math.round(total);
									                    	$scope.roomsSelected[indx].totalForAddon = Math.round(total);
									                    	$scope.roomsSelected[indx].totalForTariff = Math.round(total);
									                    	$scope.roomsSelected[indx].extraAdultChildAmount = (diffDays * childLeft * extraChild)+(adultsLeft*extraAdult)
									                        var tax = (total*taxPercent/100);
									                        $scope.roomsSelected[indx].tax = Math.round(tax); 
									                        
									                        
									                    }
									                  
									                } 
									                console.log(JSON.stringify($scope.roomsSelected));
									            };
									            
									            
									            $scope.totalAdultCheck = function(indx, available,room) {
									                if (available == 0) {
									                    console.log("Disable");
									                } else {

									                    var noOfAdult = parseInt(available);
									                    $scope.roomsSelected[indx].adultsAllow = noOfAdult;
									                    return false;
									                }
									            };
									            
									            $scope.totalChildCheck = function(indx, available,child) {
									                if (available == 0) {
									                    console.log("Disable");
									                } else {

									                    var noOfAdult = parseInt(available)- parseInt(child);
									                    $scope.roomsSelected[indx].adultsAllow = noOfAdult;
									                    return false;
									                }
									            };
									            
									            $scope.minimumOccupancyCheck = function(indx, available, noOfAdults) {

										                    var noOfChild = parseInt(available) - parseInt(noOfAdults);
										                    $scope.roomsSelected[indx].childAllow = noOfChild;
										                    return false;
										            };
										            
										            $scope.clickNext = function(){
										            	$('.nav-tabs > .active').next('li').find('a')
														.trigger('click');
										            }
										            
										            $scope.updateAddon = function(id,indx){
										            if(id === ""){
								            				
								                			var rooms = parseInt($scope.roomsSelected[indx].rooms);
								                			var addonPrice = Math.round(parseInt($scope.roomsSelected[indx].addonAmount));
								                			var tempTotal = parseInt($scope.roomsSelected[indx].totalForAddon); 
								                			var total = Math.round(tempTotal - addonPrice);
								                			var taxPercent = $scope.roomsSelected[indx].taxPercentage;
													            $scope.roomsSelected[indx].promotionAmount = parseInt($scope.roomsSelected[indx].tempPromotionAmount * rooms);
										                    	$scope.roomsSelected[indx].total = Math.round(total);
													            $scope.roomsSelected[indx].totalForAdvance = Math.round(total);
													            $scope.roomsSelected[indx].totalForAddon = Math.round(total);
										                    	$scope.roomsSelected[indx].tempTotal = Math.round(total);
										                    	$scope.roomsSelected[indx].totalForTariff = Math.round(total);
										                    	$scope.roomsSelected[indx].addonDescription= 'NA';
									                			$scope.roomsSelected[indx].addonAmount = 0;
									                			$scope.roomsSelected[indx].advanceAmount = 0;
										                        var tax = (total*taxPercent/100);
										                        $scope.roomsSelected[indx].tax = Math.round(tax); 
								            		
								            		}		
										            else if(id != null){

										            			for(var i=0;i<$scope.propertyAddon.length;i++){
										            				if($scope.propertyAddon[i].propertyAddonId == id){	
										            			
										            				$scope.roomsSelected[indx].addonDescription= $scope.propertyAddon[i].addonName;
										                			$scope.roomsSelected[indx].addonAmount = parseInt($scope.propertyAddon[i].addonRate);
										                			var rooms = parseInt($scope.roomsSelected[indx].rooms);
										                			var addonPrice = Math.round(parseInt($scope.roomsSelected[indx].addonAmount) * rooms);
										                			$scope.roomsSelected[indx].addonAmount = parseInt(addonPrice);
										                			var tempTotal = parseInt($scope.roomsSelected[indx].totalForAddon); 
										                			var taxPercent = $scope.roomsSelected[indx].taxPercentage;
										                			var total = Math.round(tempTotal + addonPrice);
															            $scope.roomsSelected[indx].promotionAmount = parseInt($scope.roomsSelected[indx].tempPromotionAmount * rooms);    
															            $scope.roomsSelected[indx].total = Math.round(total);
																        $scope.roomsSelected[indx].totalForAdvance = Math.round(total);
																        $scope.roomsSelected[indx].totalForAddon = Math.round(total);
												                    	$scope.roomsSelected[indx].tempTotal = Math.round(total);
												                    	$scope.roomsSelected[indx].totalForTariff = Math.round(total);
												                    	$scope.roomsSelected[indx].advanceAmount = 0;
												                        var tax = (total*taxPercent/100);
												                        $scope.roomsSelected[indx].tax = Math.round(tax); 
										            			}
										            		}
										            		}
										            console.log(JSON.stringify($scope.roomsSelected));		
										            
										            }
								$scope.getPropertyAddon = function(id){
									var url = "get-guest-property-addon?propertyId="+id;
									$http.get(url).success(function(response) {
										$scope.propertyAddon = response.data;
									});
								};
								
								$scope.range = function(min, max, step) {
									step = step || 1;
									var input = [];
									for ( var i = min; i <= max; i += step)
										input.push(i);
									return input;
								};
				
								
						$scope.getGoogleLocations();
						
						//$scope.getPropertyAddon();
						
						$scope.getGuestBookingUsers();
						
						$scope.getSources();

						$scope.getStates();

						$scope.getCountries();

						$scope.getResortDiscount();

						$scope.getTaxes();
						
						$scope.getPaymentStatus();


					});
</script>
<link rel="stylesheet"
	href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
	$(function() {
	
		$("#checkin").datepicker({
			minDate : 0
		});
		$("#checkout").datepicker({
			minDate : 0
		});
		$("#expiryDate").datepicker({
			minDate : 1
		});
	});
</script>
<!-- iCheck 1.0.1 -->
<script src="plugins/iCheck/icheck.min.js"></script>
<script>
	/* next and previous button */
	$('.nextBtn').click(function() {
		$('.nav-tabs > .active').next('li').find('a').trigger('click');
	});

	$('.previousBtn').click(function() {
		$('.nav-tabs > .active').prev('li').find('a').trigger('click');
	});
</script>
<script language="javascript" type="text/javascript">
	function printDiv(divID) {
		//Get the HTML of div
		var divElements = document.getElementById(divID).innerHTML;
		//Get the HTML of whole page
		var oldPage = document.body.innerHTML;

		//Reset the page's HTML with div's HTML only
		document.body.innerHTML = "<html><head><title></title></head><body>"
				+ divElements + "</body>";

		//Print Page
		window.print();

		//Restore orignal HTML
		document.body.innerHTML = oldPage;

	}
</script>
