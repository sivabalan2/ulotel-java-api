<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
   pageEncoding="ISO-8859-1"%>
<!-- list page begins  -->
<section class="listbackground hidden-xs">
  <!-- LIST SEARCH BAR BEGINS -->	
  <div class="listsearchbox">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 desklistsearch">
          <div class="col-lg-4 col-md-4 col-sm-4 dlistsearch">
            <img src="contents/images/menu-icon/flag2.png" alt="seacrh" class="dhflag"/>
            <angucomplete-alt id="ex1" class="form-control dlistsearchfilter" ng-required="true" placeholder="{{displayNames}}" selected-object="locationurl" local-data="filterLocation" search-fields="displayLocationName" title-field="displayLocationName" minlength="1" class="form-control mlsearch"  match-class="highlight" field-required="true"/>
          </div>
          <div class="col-lg-1 col-md-1 col-sm-1  ">
            <div class="whitebgm">
              <img src="contents/images/menu-icon/date2.png" alt="seacrh" class="dhsep"/>
            </div>
          </div>
          <div class="col-lg-2 col-md-2 col-sm-2">
            <input type="text" class="form-control" placeholder="21 July Sunday" value="<%= ((request.getAttribute("checkIn2")==null)?session.getAttribute("checkIn2"):request.getAttribute("checkIn2")) %>" onkeypress="return false;" id="datepicker0">
            <input type="hidden" class="form-control" placeholder="Check-in" value="<%= ((request.getAttribute("checkIn1")==null)?session.getAttribute("checkIn1"):request.getAttribute("checkIn1")) %>" id="input1">
            <input type="hidden" class="form-control" placeholder="Check-in" value="<%= ((request.getAttribute("checkIn")==null)?session.getAttribute("checkIn"):request.getAttribute("checkIn")) %>" id="alternate">
          </div>
          <div class="col-lg-1 col-md-1 col-sm-1 dwhitebgmnorad ">
            <img src="contents/images/menu-icon/sep2.png" alt="seacrh" class="dhsep"/>
          </div>
          <div class="col-lg-2 col-md-2 col-sm-2 ">
            <input type="text" class="form-control" placeholder="Checkout " value="<%= ((request.getAttribute("checkOut2")==null)?session.getAttribute("checkOut2"):request.getAttribute("checkOut2")) %>" onkeypress="return false;" id="datepicker1">
            <input type="hidden" class="form-control" placeholder="Checkout" value="<%= ((request.getAttribute("checkOut1")==null)?session.getAttribute("checkOut1"):request.getAttribute("checkOut1")) %>" size="10" id="input2" onkeypress="return false;" readonly="readonly" >
            <input type="hidden" class="form-control" placeholder="Checkout" value="<%= ((request.getAttribute("checkOut")==null)?session.getAttribute("checkOut"):request.getAttribute("checkOut")) %>" size="10" id="alternate1" onkeypress="return false;" readonly="readonly" >
          </div>
          <div class="col-lg-2 col-md-2 col-sm-2 desklistbtndiv">
            <a ng-href="{{locationUrl}}?checkIn={{StartDate}}&checkOut={{EndDate}}"> <button type="submit" ng-click="getChangeValues()" class="btn desklistbtn">Search</button></a>
            <!-- <button type="submit" class="btn desklistbtn">Search</button> -->
          </div>
        </div>
        <div class="col-lg-12">
          <div class="datepicker"></div>
        </div>
      </div>
    </div>
  </div>
  <!-- LIST SEARCH BAR BEGINS -->
  <!-- List breadcrumb begins -->
  <div class="listbreadcrumb">
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-8 col-sm-8">
          <a class="breadcrumblist" href="index">Home</a> > <a class="breadcrumbactive" href="#"><%= session.getAttribute("breadCrumAreaName")%></a>
        </div>
        <!-- sort by filter begins -->
        <div class="col-lg-4 col-md-4 col-sm-4">
          <select id="sortBy" name="sortBy"  ng-model="sortBy" ng-change="sortByPrice()">
            <option value="" selected disabled>Sort </option>
            <option value="1" class="selecthighlight">Low to High</option>
            <option value="2">High to Low</option>
          </select>
        </div>
        <!-- sort by filter ends -->
      </div>
    </div>
  </div>
  <!-- List Breadcrumb ends -->
  <!-- property listing begins -->
  <div class="container">
    <div class="row">
      <!-- filter starts -->
      <div class="col-lg-3 col-md-3 col-sm-3 deskfilterbg">
        <div class="col-lg-12 col-md-12 col-sm-3 deskfilter">
          <div class="col-lg-6 col-md-6 col-sm-6">
            <h4>Filter</h4>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-6">
            <button class="btn deskresetbtn" ng-click="reseAllFilter()">Reset All</button>
          </div>
        </div>
        <!-- filter search -->
        <div class="col-lg-12 col-md-12 col-sm-12 deskfilter">
          <div class="col-lg-6 col-md-6 col-sm-6">
            <h4 class="filtername">Location</h4>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-6">
            <input id="checkbox8" class="hidden" type="checkbox" checked>
          </div>
          <div class="col-lg-12 col-md-12 col-sm-12 deskfilterscroll">
            <%-- <div class="input-group c-search">
              <input type="text" class="form-control filtersearchbar" id="contact-list-search">
              <span class="input-group-btn">
              <button class="btn btn-default deskfiltersearchbtn" type="button"><i class="fa fa-search" aria-hidden="true"></i></button>
              </span>
              </div> --%>
            <label class="deskrecentplaces" ng-repeat="la in filteredAreas | limitTo:3"><a href="{{la.areaUrl}}?checkIn={{StartDate}}&checkOut={{EndDate}}">{{la.areaName}}</a></label>
            <!-- <label class="deskrecentplaces" ng-repeat="locAreas in locationAreas">{{locAreas.areaName}}</label>
              <ul ng-repeat="locAreas in locationAreas"><li>{{locAreas.length}}</li></ul> -->
            <div id="demo" class="collapse">
              <label class="deskrecentplaces"  ng-repeat="la in filteredAreas | limitTo : -3 "><a href="{{la.areaUrl}}?checkIn={{StartDate}}&checkOut={{EndDate}}">{{la.areaName}}</a></label>
            </div>
            <button type="button" class="btn deskfiltersearchscrollbtn" data-toggle="collapse" data-target="#demo">View More</button>
          </div>
        </div>
        <!-- price filter -->
        <div class="col-lg-12 col-md-12 col-sm-12 deskfilter">
          <div class="col-lg-6 col-md-6 col-sm-6">
            <h4 class="filtername">Price</h4>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-6">
            <input id="checkbox8" class="hidden" type="checkbox" checked>
          </div>
          <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="col-lg-12 col-md-12 col-sm-12">
              <label class="radio-inline deskpricelable" for="r">
              <input type="radio" name="optradio" value="999" id="r" ng-model="value" ng-click="filterByHotelPrice(value)"  >
              <i class="fa fa-rupee-sign"></i> 0 - <i class="fa fa-rupee-sign"></i> 999
              </label>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12">
              <label class="radio-inline deskpricelable">
              <input type="radio" name="optradio" value="1000-1999" ng-model="value" ng-click="filterByHotelPrice(value)">
              <i class="fa fa-rupee-sign"></i> 1000 - <i class="fa fa-rupee-sign"></i> 1999
              </label>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12">
              <label class="radio-inline deskpricelable">
              <input type="radio" name="optradio" value="2000-2999" ng-model="value" ng-click="filterByHotelPrice(value)" >
              <i class="fa fa-rupee-sign"></i> 2000 - <i class="fa fa-rupee-sign"></i> 2999
              </label>	
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12">
              <label class="radio-inline deskpricelable">
              <input type="radio" name="optradio" value="3000-4999" ng-model="value" ng-click="filterByHotelPrice(value)" >
              <i class="fa fa-rupee-sign"></i> 3000 - <i class="fa fa-rupee-sign"></i> 4999
              </label>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12">
              <label class="radio-inline deskpricelable">
              <input type="radio" name="optradio" value="5000" ng-model="value" ng-click="filterByHotelPrice(value)" >
              <i class="fa fa-rupee-sign"></i> 5000 & Above
              </label>
            </div>
          </div>
        </div>
        <!-- policies filter -->
        <!-- hotel type filter -->
        <div class="col-lg-12 col-md-12 col-sm-12 deskfilter">
          <div class="col-lg-6 col-md-6 col-sm-6">
            <h4 class="filtername">Hotel Type</h4>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-6">
            <input id="checkbox8" class="hidden" type="checkbox" checked> 
          </div>
          <div class="col-lg-12 col-md-12 col-sm-12 activehoteltype">
            <label class="deskhoteltype " id="dpbutton{{$index}}" ng-repeat="pt in propertytypes"><input type="checkbox" class="hidden" ng-click="filterByHotelTypes($index)" value="{{pt.Dt.RowId}}" ng-model="pt.isDeleted" >{{pt.propertyTypeName}}</label>
          </div>
        </div>
        <!-- amenities filter -->
      </div>
      <!-- filter ends -->
      <!-- hotel list begins -->
      <div class="col-lg-9 col-md-9 col-sm-9">
        <!-- lisiting coupon begins -->
        <div class="desklistcoupon">
          <div class="row ">
            <div class="col-lg-12 col-md-12 col-sm-12">
              <div class="listcouponbanner" ng-repeat="cd in discounts| orderBy:'-discountPercentage' | limitTo:1 ">
                To get <span class="deskoffvalue">{{cd.discountPercentage | number}} % Offer </span><input type="text" class="deskcouponcode" id="myInput" value="{{cd.discountName}}" ><span class="deskcouponuse"> Use this code  </span><a href="javascript:void(0);" class="deskcodecopy" data-clipboard-text="SUMMER10" ng-click="myFunction()"> Copy & Apply</a> <span class="tooltiptext" id="myTooltip">  </span>
              </div>
            </div>
          </div>
        </div>
        <!-- lisiting  coupon ends -->
        <!-- standard amenities begins -->
        <div class="desklistamenities">
          <div class="row ">
            <div class="col-lg-12 col-md-12 col-sm-12">
              <h6>OUR STANDARD AMENITIES</h6>
              <div class="col-lg-2 col-md-2 col-sm-2">
                <img src="contents/images/amenities/breakfast.png" class="img-responsive">
                <span class="amenityname">Free Breakfast</span>
              </div>
              <div class="col-lg-2 col-md-2 col-sm-2">
                <img src="contents/images/amenities/cleanbed.png" class="img-responsive">
                <span class="amenityname text-center">Comfortable Beds</span>
              </div>
              <div class="col-lg-2 col-md-2 col-sm-2">
                <img src="contents/images/amenities/freetoilet.png" class="img-responsive">
                <span class="amenityname">Free Toiletries</span>
              </div>
              <div class="col-lg-2 col-md-2 col-sm-2">
                <img src="contents/images/amenities/bathroom.png" class="img-responsive">
                <span class="amenityname">Clean Bathroom</span>
              </div>
              <div class="col-lg-2 col-md-2 col-sm-2">
                <img src="contents/images/amenities/wifi.png" class="img-responsive">
                <span class="amenityname">Free Wi-fi</span>
              </div>
              <div class="col-lg-2 col-md-2 col-sm-2">
                <img src="contents/images/amenities/tv.png" class="img-responsive">
                <span class="amenityname">Television</span>
              </div>
            </div>
          </div>
        </div>
        <!-- standard amenities ends -->
        <div ng-show="shownoList" class="nolistalert">
          <h4>Sorry!</h4>
          <h5>No hotels found ,Please try another hotel</h5>
        </div>
        <!-- Dummy loader begins-->
        <div class="row " ng-show="showloader">
          <div class="col-lg-12 col-md-12 col-xs-12 col-md-12 dloaderdiv">
            <div class="col-lg-3 col-md-3 col-xs-3 col-md-3">
              <img src="contents/images/loader/image.png" class="img-responsive">
            </div>
            <div class="col-lg-9 col-md-9 col-xs-9 col-md-9">
              <div class="col-lg-12 col-md-12 col-xs-12 col-md-12">
                <h2 class="loadname"></h2>
              </div>
              <div class="col-lg-12 col-md-12 col-xs-12 col-md-12">
                <h2 class="loadlocation"></h2>
              </div>
              <div class="col-lg-12 col-md-12 col-xs-12 col-md-12">
                <button class="loadbooknow"></button>
              </div>
            </div>
          </div>
          <div class="col-lg-12 col-md-12 col-xs-12 col-md-12 dloaderdiv">
            <div class="col-lg-3 col-md-3 col-xs-3 col-md-3">
              <img src="contents/images/loader/image.png" class="img-responsive">
            </div>
            <div class="col-lg-9 col-md-9 col-xs-9 col-md-9">
              <div class="col-lg-12 col-md-12 col-xs-12 col-md-12">
                <h2 class="loadname"></h2>
              </div>
              <div class="col-lg-12 col-md-12 col-xs-12 col-md-12">
                <h2 class="loadlocation"></h2>
              </div>
              <div class="col-lg-12 col-md-12 col-xs-12 col-md-12">
                <button class="loadbooknow"></button>
              </div>
            </div>
          </div>
          <div class="col-lg-12 col-md-12 col-xs-12 col-md-12 dloaderdiv">
            <div class="col-lg-3 col-md-3 col-xs-3 col-md-3">
              <img src="contents/images/loader/image.png" class="img-responsive">
            </div>
            <div class="col-lg-9 col-md-9 col-xs-9 col-md-9">
              <div class="col-lg-12 col-md-12 col-xs-12 col-md-12">
                <h2 class="loadname"></h2>
              </div>
              <div class="col-lg-12 col-md-12 col-xs-12 col-md-12">
                <h2 class="loadlocation"></h2>
              </div>
              <div class="col-lg-12 col-md-12 col-xs-12 col-md-12">
                <button class="loadbooknow"></button>
              </div>
            </div>
          </div>
        </div>
        <!-- Dummy loader ends-->
        <div class="row desklistproperty" ng-repeat="p in properties">
          <!-- pop up begins -->
          <!-- Trigger the modal with a button -->
          <!-- Modal -->
          <div id="myModal{{p.DT_RowId}}" class="modal fade mydimage" role="dialog">
            <div class="modal-dialog">
              <!-- Modal content-->
              <div class="modal-content">
                <div class="gallery">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <div class="modal-header ">
                    <div class="col-sm-6">
                      <h4 class="modal-title"> {{p.propertyName}}, <span class="popupcity">{{p.city}}</span></h4>
                    </div>
                    <div class="col-sm-6">
                      <div class="saveprice hide" ng-show="p.available  !== '0'">Save <i class="fa fa-rupee-sign"></i>{{p.baseActualAmount - p.totalAmount}}</div>
                      <span class="desklistpricing"><strike class="cutoffprice hide"><i class="fa fa-rupee-sign"></i> {{p.baseActualAmount | currency:" ":0}}</strike><span class="roomprice"> <i class="fa fa-rupee-sign"></i>{{p.totalAmount | currency:"":0}}</span></span>
                    </div>
                  </div>
                  <div class="modal-body">
                    <slick id="largeSlider{{p.DT_RowId}}"  class="slides fadein fadeout largeSlider" data="p.photos" dots="false"  settings="largePanel">
                      <div ng-repeat="pp in p.photos | limitTo:10">
                        <div class="slide slide-1">
                          <img ng-src="get-property-image-stream?propertyPhotoId={{pp.DT_RowId}}">
                        </div>
                      </div>
                    </slick>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- pop up ends -->
          <!-- photos -->
          <div class="col-lg-4 col-md-4 col-sm-4">
            <img ng-src="get-property-thumb?propertyId={{p.DT_RowId}}" alt="propertyname" class="img-thumbnail desklisthotelimage">
            <a data-toggle="modal" data-target="#myModal{{p.DT_RowId}}" class="deskroomimagepops"><img src="contents/images/listpopicon.png"  alt="seacrh" class="dhsep"/></a>
            <span class="deskroomleft">{{p.available}} Rooms Left</span> 
            <span class="deskroomleft">{{p.available}} Rooms Left</span> 
            <span class="listhoteltype">{{p.propertyTypeName}}</span>
          </div>
          <!-- photos -->
          <!-- hotel details -->
          <div class="col-lg-8 col-md-8 col-sm-8">
            <div class="row">
              <div class="col-lg-8 col-md-8 col-sm-8">
                <a href="{{p.propertyUrl}}?checkIn={{StartDate}}&checkOut={{EndDate}}" class="nopointer">
                  <h4 class="listhotelname">{{p.propertyName}}</h4>
                </a>
                <h5 class="listhotellocation"><i class="fa fa-map-marker fa-x"></i> {{p.city}}   </h5>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="listoffertop" ng-if="p.promotionFirstName!='None'">
                  <div class="ribbonpropgress">
                    <progressbar max="max" value="p.promoFirstPercent"><span>{{p.promotionFirstName}}</span></progressbar>
                  </div>
                </div>
                <!-- <div class="listoffertoplast" ng-if="p.promotionSecondName!='None'" >
                  <div class="desklistsecondoffer" >{{p.promotionSecondName}} </div>
                </div> -->
              </div>
              <div class="col-lg-8 col-md-8 col-sm-8" ng-repeat="review in p.reviews" >
                <table>
                  <tr title="{{review.reviewCount}} Google Reviews"  data-toggle="tooltip" data-placement="bottom">
                    <th><img src="contents/images/googlelistlogo.png" alt="trip adivisior" class="deskgooglerating"></th>
                    <th>
                      <div star-rating rating-value="review.starCount" max="5" ></div>
                    </th>
                    <th> <span class="googlecount" >{{review.reviewCount}}</span></th>
                  </tr>
                </table>
              </div>
              <%-- <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="desklistoffernote" ng-if="p.promoLastMinuteTimer!='None'" ><i class="fa fa-clock-o"></i> <span id="timer{{p.DT_RowId}}"></span></div>
              </div> --%>
              <div class="col-lg-8 col-md-8 col-sm-8 desklisttrip" ng-repeat="review in p.reviews">
                <table>
                  <tr title="{{review.tripadvisorReviewCount}} Google Reviews"  data-toggle="tooltip" data-placement="bottom">
                    <th><img src="contents/images/triplistlogo.png" alt="trip adivisior" class="desktriprating"></th>
                    <th>
                      <div star-rating2 rating-value="review.tripadvisorStarCount" max="5" ></div>
                    </th>
                    <th> <span class="tripcount" ng-repeat="review in p.reviews">{{review.tripadvisorReviewCount}}</span>
                    </th>
                  </tr>
                </table>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-4">
                <p ng-show ="p.available === '0'" class="soldoutnote">We are unable to serve on these dates,Please select another date or hotel</p>
                <div class="saveprice" ng-show="p.available  !== '0'" ng-hide="p.baseActualAmount === p.totalAmount">Save <i class="fa fa-rupee-sign"></i>{{p.baseActualAmount - p.totalAmount}}</div>
              </div>
              <div class="col-lg-8 col-md-8 col-sm-8">
                <div class="ptop">
                  <span class="splrequest" ng-if="p.coupleStatus == 'true'">Couple Friendly</span>
                  <span class="splrequest" ng-if="p.breakfastStatus == 'true'">Free Breakfast</span>
                  <span class="splrequest" ng-if="p.payAtHotelStatus == 'true'">Pay @ Hotel</span>
                </div>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-4">
                <div class="listpricebottom">
                  <span class="desklistpricing" ng-show="p.available  !== '0'" ><strike class="cutoffprice" ng-hide="p.baseActualAmount === p.totalAmount"><i class="fa fa-rupee-sign"></i> {{p.baseActualAmount | currency:" ":0}}</strike><span class="roomprice"> <i class="fa fa-rupee-sign"></i>{{p.totalAmount | currency:"":0}}</span></span>
                  <a href="{{p.propertyUrl}}?checkIn={{StartDate}}&checkOut={{EndDate}}" class="desklistbookbtn" ng-show="p.available  !== '0'">Book Room</a>
                  <button class="btn soldoutbtn" ng-show ="p.available === '0'"> SOLD OUT </button>
                </div>
                <!-- price diving if available -->
              </div>
            </div>
          </div>
          <!-- rate details -->
          <div class="col-lg-12 col-md-12 col-sm-12">
            <!-- price diving if available -->
          </div>
          <div class="row" ng-if="($index+1)==2">
            <div class="col-lg-12">
              <div class="listloginbanner">
                <img src="contents/images/detail/gift.png" alt="ulo login">  
                Login To Get Your Surprise Deal
                <a class="deskloginoffer" data-toggle="modal" data-target="#myModal">Login</a>
              </div>
            </div>
          </div>
        </div>
        <!-- second listing page begins  -->
        <!-- login banner begins -->
        <!-- login banner ends --
          <!-- sold out begins -->
      </div>
      <!-- hotel list ends -->
    </div>
    <div class="row" ng-show="showcontent" >
      <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 seotitle seopara">
        <span class="more"><%= request.getAttribute("description")==null?"":request.getAttribute("description")%></span>
      </div>
    </div>
  </div>
  <!-- property listing ends -->
</section>
<!-- list page ends -->
<!-- Mobile landing page begins -->
<section class="mobilelistsearchbar hidden-md hidden-lg hidden-sm">
  <div class="container">
    <div class="row">
      <div class="col-xs-2">
        <a href="javascript:void(0)" onclick="goBack()" class="mlistcall"><img src="contents/images/menu-icon/backarrow.png" alt="ulo contact number"/></a>
      </div>
      <a href="javascript:void(0)" data-toggle="modal" data-target="#mobilesearchmodal" class="pop">
        <div class="col-xs-8 mobilesearchlist">
          <div class="col-xs-12">
            <angucomplete-alt id="ex1" ng-required="true" placeholder="{{displayNames}}" selected-object="locationurl" local-data="filterLocation" search-fields="displayLocationName" title-field="displayLocationName" minlength="1" class="form-control mlsearch"  match-class="highlight" field-required="true"/>
          </div>
        <div class="col-xs-12 ">
        <input type="text" id="mdatepicker0"  class="form-control mldate mldateleft" placeholder="21 July Monday" value="<%= ((request.getAttribute("mbcheckIn2")==null)?session.getAttribute("mbcheckIn2"):request.getAttribute("mbcheckIn2")) %>" onkeypress="return false;">
        <input type="hidden" id="minput1" value="<%= ((request.getAttribute("mbcheckIn1")==null)?session.getAttribute("mbcheckIn1"):request.getAttribute("mbcheckIn1")) %>" size="10">
        <input type="hidden" id="malternate" value="<%= ((request.getAttribute("mbcheckIn")==null)?session.getAttribute("mbcheckIn"):request.getAttribute("mbcheckIn")) %>" size="10">
        <input type="text" id="mdatepicker1"  class="form-control mldate mldateright" placeholder="21 July Monday " value="<%= ((request.getAttribute("mbcheckOut2")==null)?session.getAttribute("mbcheckOut2"):request.getAttribute("mbcheckOut2")) %>" onkeypress="return false;">
        <input type="hidden" id="minput2" value="<%= ((request.getAttribute("mbcheckOut1")==null)?session.getAttribute("mbcheckOut1"):request.getAttribute("mbcheckOut1")) %>" size="10">
        <input type="hidden" id="malternate1" value="<%= ((request.getAttribute("mbcheckOut")==null)?session.getAttribute("mbcheckOut"):request.getAttribute("mbcheckOut")) %>" size="10">
</div>
          <div class="col-xs-12">
            <div class="mbdatepicker"></div>
          </div>
        </div>
      </a>
      <div class="col-xs-2">
        <a href="tel:9543592593" title="+91 95435 92593" class="mlistcall"><img src="contents/images/menu-icon/mcall.png" alt="ulo contact number"/></a>
      </div>
    </div>
  </div>
  <!-- search popup begins -->
  <div id="mobilesearchmodal" class="modal fade " role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content mobilesearchmodal">
        <div class="modal-header ">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <div class="row">
            <div class="col-xs-12">
              <angucomplete-alt id="ex1" ng-required="true" placeholder="Search by city ,location" selected-object="locationurl" local-data="filterLocation" search-fields="locationName" title-field="locationName" minlength="1" class="form-control mlsearch"  match-class="highlight" field-required="true"/>
            </div>
            <div class="col-xs-6 ">
              <input type="text" id="msbdatepicker0"  class="form-control mldate" placeholder="21 July Monday" value="<%= ((request.getAttribute("mbcheckIn2")==null)?session.getAttribute("mbcheckIn2"):request.getAttribute("mbcheckIn2")) %>" onkeypress="return false;">
              <input type="hidden" id="msbinput1" value="<%= ((request.getAttribute("mbcheckIn1")==null)?session.getAttribute("mbcheckIn1"):request.getAttribute("mbcheckIn1")) %>" size="10">
              <input type="hidden" id="msbalternate" value="<%= ((request.getAttribute("mbcheckIn")==null)?session.getAttribute("mbcheckIn"):request.getAttribute("mbcheckIn")) %>" size="10">
            </div>
            <div class="col-xs-6 ">
              <input type="text" id="msbdatepicker1"  class="form-control mldate" placeholder="21 July Monday " value="<%= ((request.getAttribute("mbcheckOut2")==null)?session.getAttribute("mbcheckOut2"):request.getAttribute("mbcheckOut2")) %>" onkeypress="return false;">
              <input type="hidden" id="msbinput2" value="<%= ((request.getAttribute("mbcheckOut1")==null)?session.getAttribute("mbcheckOut1"):request.getAttribute("mbcheckOut1")) %>"  size="10">
              <input type="hidden" id="msbalternate1" value="<%= ((request.getAttribute("mbcheckOut")==null)?session.getAttribute("mbcheckOut"):request.getAttribute("mbcheckOut")) %>" size="10">
            </div>
            <div class="col-xs-12">
              <div class="msbdatepicker"></div>
            </div>
            <div class="col-xs-12">
              <a ng-href="{{locationUrl}}?checkIn={{mbStartDate}}&checkOut={{mbEndDate}}" ng-click="getMbChangeValues()" class="btn btnlistfloat">Search Hotels</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- search popup ends -->
</section>
<!-- Mobile breadcrumb begins -->
<section class="listmobilebreadcrumb hidden-md hidden-lg hidden-sm">
  <div class="container">
    <div class="row">
      <div class="col-xs-8 col-sm-8">
        <a class="mobilebreadcrumblist" href="index">Home</a> > <a class="mobilebreadcrumbactive" href="#"><%= session.getAttribute("breadCrumAreaName")%></a>
      </div>
      <div class="col-sm-4 col-xs-4">
        <span class="mobilelistpernight">Price Per Night</span>
      </div>
    </div>
  </div>
</section>
<!-- Mobile breadcrumbs ends -->
<!-- Mobile Property Listing Begins -->
<section class="mobilehotellist hidden-md hidden-lg hidden-sm">
  <div class="container">
    <div class="row" >
      <div ng-show="shownoList" class="nolistalert">
        <h4>Sorry!</h4>
        <h5>No hotels found ,Please try another hotel</h5>
      </div>
      <!-- Dummy loader begins-->
      <div class="row " ng-show="showloader">
        <div class="col-lg-12 col-md-12 col-xs-12 col-md-12 dloaderdiv">
          <div class="col-lg-3 col-md-3 col-xs-3 col-md-3">
            <img src="contents/images/loader/image.png" class="img-responsive">
          </div>
          <div class="col-lg-9 col-md-9 col-xs-9 col-md-9">
            <div class="col-lg-12 col-md-12 col-xs-12 col-md-12">
              <h2 class="loadname"></h2>
            </div>
            <div class="col-lg-12 col-md-12 col-xs-12 col-md-12">
              <h2 class="loadlocation"></h2>
            </div>
            <div class="col-lg-12 col-md-12 col-xs-12 col-md-12">
              <button class="loadbooknow"></button>
            </div>
          </div>
        </div>
        <div class="col-lg-12 col-md-12 col-xs-12 col-md-12 dloaderdiv">
          <div class="col-lg-3 col-md-3 col-xs-3 col-md-3">
            <img src="contents/images/loader/image.png" class="img-responsive">
          </div>
          <div class="col-lg-9 col-md-9 col-xs-9 col-md-9">
            <div class="col-lg-12 col-md-12 col-xs-12 col-md-12">
              <h2 class="loadname"></h2>
            </div>
            <div class="col-lg-12 col-md-12 col-xs-12 col-md-12">
              <h2 class="loadlocation"></h2>
            </div>
            <div class="col-lg-12 col-md-12 col-xs-12 col-md-12">
              <button class="loadbooknow"></button>
            </div>
          </div>
        </div>
        <div class="col-lg-12 col-md-12 col-xs-12 col-md-12 dloaderdiv">
          <div class="col-lg-3 col-md-3 col-xs-3 col-md-3">
            <img src="contents/images/loader/image.png" class="img-responsive">
          </div>
          <div class="col-lg-9 col-md-9 col-xs-9 col-md-9">
            <div class="col-lg-12 col-md-12 col-xs-12 col-md-12">
              <h2 class="loadname"></h2>
            </div>
            <div class="col-lg-12 col-md-12 col-xs-12 col-md-12">
              <h2 class="loadlocation"></h2>
            </div>
            <div class="col-lg-12 col-md-12 col-xs-12 col-md-12">
              <button class="loadbooknow"></button>
            </div>
          </div>
        </div>
      </div>
      <!-- Dummy loader ends-->
      <div class="col-sm-12 col-xs-12 mobilehotelrepeats" ng-repeat="p in properties ">
        <!-- pop up begins -->
        <!-- Trigger the modal with a button -->
        <!-- Modal -->
        <div id="myModaltwo{{p.DT_RowId}}" class="modal fade mydimagetwo" role="dialog">
          <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header ">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"> {{p.propertyName}}</h4>
              </div>
              <div class="modal-body">
                <slick id="largeSlider{{p.DT_RowId}}"  class="slides fadein fadeout largeSlider" data="p.photos" dots="false"  settings="largePanel">
                  <div ng-repeat="pp in p.photos | limitTo:10">
                    <div class="slide slide-1">
                      <img ng-src="get-property-image-stream?propertyPhotoId={{pp.DT_RowId}}">
                    </div>
                  </div>
                </slick>
              </div>
            </div>
          </div>
        </div>
        <!-- pop up ends -->
        <div class="col-sm-4 col-xs-4 zeropadding" >
          <img ng-src="get-property-thumb?propertyId={{p.DT_RowId}}" alt="propertyname" class="mobilehotellistimage">
          <a data-toggle="modal" data-target="#myModaltwo{{p.DT_RowId}}" class="deskroomimagepops"><img src="contents/images/listpopicon.png"  alt="seacrh" class="dhsep"/></a>
          <span class="deskroomleft">{{p.available}} Rooms Left</span>
          <span class="listhoteltype">{{p.propertyTypeName}}</span>
          <!-- Popup begins --> 
          <!-- popup ends -->
        </div>
        <div class="col-sm-8 col-xs-8 zeropadding">
          <div class="col-sm-8 col-xs-8 fivepadding">
            <a href="{{p.propertyUrl}}?checkIn={{StartDate}}&checkOut={{EndDate}}" class="nopointer"><span class="mlhotelname">{{p.propertyName}}</span></a>
            <span class="mlhotelplace"><i class="fa fa-map-marker"></i> {{p.city}}</span>
          </div>
          <div class="col-sm-4 col-xs-4 zeropadding">
            <div class="ribbonpropgress" ng-if="p.promotionFirstName!='None'">
              <progressbar max="max" value="p.promoFirstPercent"><span>{{p.promotionFirstName}}</span></progressbar>
            </div>
            <%-- <div ng-if="p.promotionSecondName!='None'" class="listoffertoplast">
              <span class="mobilesecondpromolabel">{{p.promotionSecondName}}</span> 
            </div> --%>
<%--             <div ng-if="p.promoLastMinuteTimer!='None'" class="mobiletimerlabel"><i class="fa fa-clock-o"></i> <span id="timermbl{{p.DT_RowId}}" class="mobiletimer"></span></div> --%>
          </div>
          <div class="col-sm-12 col-xs-12 zeropadding" ng-repeat="review in p.reviews">
            <button class="mbreviewbtn"><span class="excellantcolor">{{review.starCount}}</span><span class="mobilelistreviewexcellant"> {{review.propertyRating}}</span></button>
          </div>
          <div class="col-sm-12 col-xs-12 zeropadding">
            <span class="mobilesploffer" ng-if="p.coupleStatus == 'true'">Couple Friendly</span>
            <span class="mobilesploffer" ng-if="p.payAtHotelStatus == 'true'">Pay @ Hotel</span> 
            <span class="mobilesploffer" ng-if="p.breakfastStatus == 'true'">Free Breakfast</span>
          </div>
          <div class="col-sm-12 col-xs-12 zeropadding">
            <span class="mobilelistsave" ng-show="p.available  !== '0'" ng-hide="p.baseActualAmount === p.totalAmount">Save {{p.baseActualAmount - p.totalAmount}}</span>
          </div>
          <div class="col-sm-12 col-xs-12 zeropadding ">
            <div class="listprice"  ng-show="p.available  !== '0'">
              <span class="mobileliststrike"  ng-hide="p.baseActualAmount === p.totalAmount"> <i class="fa fa-rupee-sign"></i>{{p.baseActualAmount | currency:" ":0}}</span>
              <span class="mobilelistprice"> <i class="fa fa-rupee-sign" ></i>{{p.totalAmount | currency:"":0}}</span>
            </div>
          </div>
          <div class="col-sm-12 col-xs-12 zeropadding">
            <a href="{{p.propertyUrl}}?checkIn={{mbStartDate}}&checkOut={{mbEndDate}}" class="mobilelistbooknow ripple"  ng-show="p.available  !== '0'">Book Now</a>
            <button class="btn soldoutbtn" ng-show ="p.available === '0'"> SOLD OUT </button>
          </div>
        </div>
      </div>
    </div>
    <div class="row" ng-show="showcontent" >
      <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 seotitle seopara">
        <span class="more"><%= request.getAttribute("description")==null?"":request.getAttribute("description")%></span>
      </div>
    </div>
  </div>
  <!-- singe offer begins -->
</section>
<!-- mobile floating filter begins -->
<div class="container">
  <div class="row listfloatingbox hidden-lg hiden-md hidden-sm">
    <div class="col-sm-6 col-xs-6">
      <%-- <select class="btn" id="mbSortBy" name="mbSortBy" ng-model="mbSortBy" ng-change="mbSortByPrice()">
        <option value="" selected disabled>Sort by </option>
        <option value="1" class="selecthighlight">Low to High</option>
        <option value="2">High to Low</option>
        </select> --%>
      <a  data-toggle="modal" data-target="#Filtersortpopup" data-toggle="modal" class="btn deskroomimagepops">Sort By <i class="fas fa-sort-down fa-1x"></i></a>
    </div>
    <div class="col-sm-6 col-xs-6">
      <a  data-toggle="modal" data-target=".Filterpopup" class="btn deskroomimagepops">Filter</a>
    </div>
  </div>
</div>
<!-- Modal -->
<div id="Filtersortpopup" class="modal fade" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Sort By</h4>
      </div>
      <div class="modal-body">
        <button class="btn btmbselect" value="1" ng-model="option1" ng-click="mbSortByPrice(1)" id="option1"><input type="radio" name="options" value="1" ng-model="option1" ng-click="mbSortByPrice(option1)" id="option1"> Low to High</button>
        <button class="btn btmbselect" value="2" ng-model="option1" ng-click="mbSortByPrice(2)" id="option1"><input type="radio" name="options" value="2" ng-model="option2" ng-click="mbSortByPrice(option2)" id="option2"> High to Low</button>
        <%--       <select class="btn" id="mbSortBy" name="mbSortBy" ng-model="mbSortBy" ng-change="mbSortByPrice()">
          <option value="" selected disabled>Sort by </option>
          <option value="1" class="selecthighlight"></option>
          <option value="2"></option>
          </select>  --%>
        <div class="col-lg-12 col-md-12 col-sm-12 ">
          <button type="submit" class="btn mbfiltersubmit" data-dismiss="modal">Apply Fiter</button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- pop up begins -->
<div class="container">
  <div class="modal fade  Filterpopup" id="filterpopup" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <!-- filter starts -->
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Filter</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <!-- filter search -->
            <div class="col-lg-12 col-md-12 col-sm-12 deskfilter">
              <div class="col-lg-6 col-md-6 col-sm-6">
                <h4 class="filtername">Location</h4>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-6">
                <input id="checkbox8" class="hidden" type="checkbox" checked> 
              </div>
              <div class="col-lg-12 col-md-12 col-sm-12 deskfilterscroll">
                <label class="deskrecentplaces" ng-repeat="la in filteredAreas | limitTo:3"><a href="{{la.areaUrl}}?checkIn=Oct 20,2018&checkOut=Oct 21,2018">{{la.areaName}}</a></label>
                <div id="demo" class="collapse">
                  <label class="deskrecentplaces"  ng-repeat="la in filteredAreas | limitTo : -3 "><a href="{{la.areaUrl}}?checkIn=Oct 20,2018&checkOut=Oct 21,2018">{{la.areaName}}</a></label>
                </div>
                <button type="button" class="btn deskfiltersearchscrollbtn" data-toggle="collapse" data-target="#demo">View More</button>
              </div>
            </div>
            <!-- price filter -->
            <div class="col-lg-12 col-md-12 col-sm-12 deskfilter">
              <div class="col-lg-6 col-md-6 col-sm-6">
                <h4 class="filtername">Price</h4>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-6">
                <input id="checkbox8" class="hidden" type="checkbox" checked> 
              </div>
              <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <label class="radio-inline deskpricelable">
                  <input type="radio" name="optradio" value="999" ng-model="values" ng-click="filterByHotelPrice(values)" checked ><i class="fa fa-rupee-sign"></i> 0- <i class="fa fa-rupee-sign"></i> 999
                  </label>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <label class="radio-inline deskpricelable">
                  <input type="radio" name="optradio" value="1000-1999" ng-model="values" ng-click="filterByHotelPrice(values)"><i class="fa fa-rupee-sign"></i> 1000 - <i class="fa fa-rupee-sign"></i> 1999
                  </label>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <label class="radio-inline deskpricelable">
                  <input type="radio" name="optradio" value="2000-2999" ng-model="values" ng-click="filterByHotelPrice(values)"><i class="fa fa-rupee-sign"></i> 2000 - <i class="fa fa-rupee-sign"></i> 2999
                  </label>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12">	
                  <label class="radio-inline deskpricelable">
                  <input type="radio" name="optradio" value="3000-4999" ng-model="values" ng-click="filterByHotelPrice(values)"><i class="fa fa-rupee-sign"></i> 3000 - <i class="fa fa-rupee-sign"></i> 4999
                  </label>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <label class="radio-inline deskpricelable">
                  <input type="radio" name="optradio" value="5000" ng-model="values" ng-click="filterByHotelPrice(values)"><i class="fa fa-rupee-sign"></i> 5000 & Above
                  </label>
                </div>
              </div>
              <!-- rate filter ends -->
            </div>
            <!-- policies filter -->
            <!-- hotel type filter -->
            <div class="col-lg-12 col-md-12 col-sm-12 deskfilter">
              <div class="col-lg-6 col-md-6 col-sm-6">
                <h4 class="filtername">Hotel Type</h4>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-6">
                <input id="checkbox8" class="hidden" type="checkbox" checked> 
              </div>
              <div class="col-lg-12 col-md-12 col-sm-12 activehoteltype">
                <label class="deskhoteltype"  id="dpbutton2{{$index}}" ng-repeat="pt in propertytypes"><input type="checkbox" class="hidden" ng-click="filterByHotelType(indx)" value="{{pt.Dt.RowId}}" ng-model="pt.isDeleted">{{pt.propertyTypeName}}</label>
              </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 ">
              <button type="submit" class="btn mbfiltersubmit" data-dismiss="modal">Apply Fiter</button>
            </div>
          </div>
          <!-- filter ends -->
        </div>
      </div>
    </div>
  </div>
</div>
<!-- popup ends -->
<!-- mobile floating filter begins -->
<!-- Mobile Property Listing Begins --> 
<!-- Mobile landing page ends -->	
<s:hidden type="hidden" value ="%{locationId}" id="locationId" name="locationId" />
<s:hidden type="hidden" value ="%{areaId}" id="areaId" name="areaId" />
<%--  <input type="hidden" value ="<%= request.getAttribute("areaId")%>" id="areaId" name="areaId" /> --%>
<%-- 	<input type="hidden" value ="<%= request.getAttribute("uloAreaId")%>" id="areaId" name="areaId" /> --%>
<input type="hidden" value ="<%= request.getAttribute("areaLocationId")%>" id="areaLocationId" name="areaLocationId" />
<!-- list page ends -->
 <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
  <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.css" />
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.8/slick.min.js"></script>
  <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.css"/> 
 <script  type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
 <script type="text/javascript"  type="text/javascript" src="contents/js/ngprogress.js"></script>
 <script defer type="text/javascript" src="https://apis.google.com/js/api.js" ng-init="handleGoogleApiLibrary()" ></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/angucomplete-alt/2.4.1/angucomplete-alt.min.js"></script> 
<script  src="https://rawgit.com/devmark/angular-slick-carousel/master/dist/angular-slick.js"></script>
  <link type="text/css" rel="stylesheet" href="contents/css/jquery-ui.css">
<script  type="text/javascript" src="https://code.jquery.com/jquery.min.js"></script>
<script type="text/javascript">
var app = angular.module('myApp', ['ngProgress','ui.bootstrap','angucomplete-alt','slickCarousel']);
app.controller('customersCtrl', function($scope, $http, $timeout, ngProgressFactory, $location, $filter,$interval) {
    $scope.progressbar = ngProgressFactory.createInstance();
    $scope.progressbar.start();
    
	  $scope.shownoList = false;
	  $scope.showloader = true;
	  $scope.showcontent = false;
	  /* login and signup start here */
		
	$scope.handleGoogleApiLibrary = function() {
          gapi.load('client:auth2', {
              callback: function() {
                  gapi.client.init({
                      apiKey: 'AIzaSyAuJwG_DuX3eEXTgsoEY9A8LRvRVfeilFs',
                      clientId: '106759720549-tfe9i1b16ovvpqn8aho6hooiaftt2h4n.apps.googleusercontent.com',
                      scope: 'https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/plus.me'
                  }).then(
                      // On success
                      function(success) {
                          $("#login-button").removeAttr('disabled');
                      },
                      // On error
                      function(error) {
                      }
                  );
              },
              onerror: function() {
                  // Failed to load libraries
              }
          });
      };
      $scope.googleLogin = function() {
    	  
    	  $scope.progressbar.start();
    	  $('#myModal').modal('hide');
	      //$('#mymobileModal').modal('hide');
          gapi.auth2.getAuthInstance().signIn().then(
              function(success) {
                  gapi.client.request({
                      path: 'https://www.googleapis.com/plus/v1/people/me'
                  }).then(
                      function(success) {
                          var user_info = JSON.parse(success.body);
                          var username = user_info.displayName;
                          var email = user_info.emails[0].value;
                          var phone = null;
                  
                          var fdata = "username=" + user_info.displayName +
                              "&emailid=" + user_info.emails[0].value +
                              "&userid=" + user_info.id;
                          $http({
                              method: 'POST',
                              data: fdata,
                              headers: {
                                  'Content-Type': 'application/x-www-form-urlencoded'
                              },
                              url: 'sociallogin'
                          }).then(function successCallback(response) {
                        	  
                        	  $scope.sociallogin = response.data;
                        
                         	   
                        	  $scope.showDprofile = true;
                              $scope.showDlogin = false;
                              $scope.dofferlogin = false;
                              $scope.showreward = false;
                              $scope.username = $scope.sociallogin.data[0].uloUserName;
                              $('#myModal').modal('hide');
                   	          //$('#mymobileModal').modal('toggle');
                              $scope.progressbar.complete();
                          }, function errorCallback(response) {

                          });
                      },
                      // On error
                      function(error) {
                          $("#login-button").removeAttr('disabled');
                          alert('Error : Failed to get user user information');
                      }
                  );
              },
              // On error
              function(error) {
                  $("#login-button").removeAttr('disabled');
                  $('#myModal').modal('toggle');
       	          //$('#mymobileModal').modal('toggle');
                  $scope.progressbar.complete();
              }
          );
      };
      $scope.facebookLogin = function() {
          $scope.authUser();
      };
      $scope.facebookLoginMobile = function() {
          $scope.authUser();
      };
      $scope.authUser = function() {
          FB.login($scope.checkLoginStatus, {
              scope: 'email, user_likes, user_birthday, user_photos'
          });
      };
      $scope.authUserMobile = function() {
          FB.login($scope.checkLoginStatusMobile, {
              scope: 'email, user_likes, user_birthday, user_photos'
          });
      };
      
      
      $scope.checkLoginStatus = function(response) {
    	  $scope.progressbar.start();
    	  $('#myModal').modal('hide');
	      //$('#mymobileModal').modal('hide');
          if (response && response.status == 'connected') {
              console.log('User is authorized');
              FB.api('/me?fields=name,email', function(response) {
                  console.log(response);
                  console.log('Good to see you, ' + response.email + '.');
                  var username = response.name;
                  var email = response.email;
                  var phone = null;
            
                  var fdata = "username=" + response.name +
                      "&emailid=" + response.email +
                      "&userid=" + response.id;
                  $http({
                      method: 'POST',
                      data: fdata,
                      headers: {
                          'Content-Type': 'application/x-www-form-urlencoded'
                      },
                      url: 'sociallogin'
                  }).then(function successCallback(response) {

                	  $scope.facebooklogin = response.data;
                	  $scope.showDprofile = true;
                      $scope.showDlogin = false;
                      $scope.dofferlogin = false;
                      $scope.showreward = false;
                      $scope.username = $scope.facebooklogin.data[0].uloUserName;
                      $scope.progressbar.complete();
                  }, function errorCallback(response) {});
              })
          } else if (response.status === 'not_authorized') {
              // the user is logged in to Facebook, but has not authenticated your app
              console.log('User is not authorized');
              $('#myModal').modal('toggle');
    	      //$('#mymobileModal').modal('hide');
          } else {
              // the user isn't logged in to Facebook.
              console.log('User is not logged into Facebook');
              $('#myModal').modal('toggle');
    	      //$('#mymobileModal').modal('hide');
              $scope.progressbar.complete();

          }
      };
      $scope.checkLoginStatusMobile = function(response) {
    	  $scope.progressbar.start();
    	  //$('#myModal').modal('hide');
	      $('#mymobileModal').modal('hide');
          if (response && response.status == 'connected') {
              console.log('User is authorized');
              FB.api('/me?fields=name,email', function(response) {
                  console.log(response);
                  console.log('Good to see you, ' + response.email + '.');
                  var username = response.name;
                  var email = response.email;
                  var phone = null;
            
                  var fdata = "username=" + response.name +
                      "&emailid=" + response.email +
                      "&userid=" + response.id;
                  $http({
                      method: 'POST',
                      data: fdata,
                      headers: {
                          'Content-Type': 'application/x-www-form-urlencoded'
                      },
                      url: 'sociallogin'
                  }).then(function successCallback(response) {

                	  $scope.facebooklogin = response.data;
                	  $scope.showDprofile = true;
                      $scope.showDlogin = false;
                      $scope.dofferlogin = false;
                      $scope.showreward = false;
                      $scope.username = $scope.facebooklogin.data[0].uloUserName;
                      $scope.progressbar.complete();
                  }, function errorCallback(response) {});
              })
          } else if (response.status === 'not_authorized') {
              // the user is logged in to Facebook, but has not authenticated your app
              console.log('User is not authorized');
              //$('#myModal').modal('toggle');
    	      $('#mymobileModal').modal('hide');
          } else {
              // the user isn't logged in to Facebook.
              console.log('User is not logged into Facebook');
              //$('#myModal').modal('toggle');
    	      $('#mymobileModal').modal('hide');
              $scope.progressbar.complete();

          }
      };
 $scope.googleLoginMobile = function() {
    	  
    	  $scope.progressbar.start();
    	  //$('#myModal').modal('hide');
	      $('#mymobileModal').modal('hide');
          gapi.auth2.getAuthInstance().signIn().then(
              function(success) {
                  gapi.client.request({
                      path: 'https://www.googleapis.com/plus/v1/people/me'
                  }).then(
                      function(success) {
                          var user_info = JSON.parse(success.body);
                          var username = user_info.displayName;
                          var email = user_info.emails[0].value;
                          var phone = null;
                  
                          var fdata = "username=" + user_info.displayName +
                              "&emailid=" + user_info.emails[0].value +
                              "&userid=" + user_info.id;
                          $http({
                              method: 'POST',
                              data: fdata,
                              headers: {
                                  'Content-Type': 'application/x-www-form-urlencoded'
                              },
                              url: 'sociallogin'
                          }).then(function successCallback(response) {
                        	  
                        	  $scope.sociallogin = response.data;
                        
                         	   
                        	  $scope.showDprofile = true;
                              $scope.showDlogin = false;
                              $scope.dofferlogin = false;
                              $scope.showreward = false;
                              $scope.username = $scope.sociallogin.data[0].uloUserName;
                              //$('#myModal').modal('hide');
                   	          //$('#mymobileModal').modal('toggle');
                              $scope.progressbar.complete();
                          }, function errorCallback(response) {

                          });
                      },
                      // On error
                      function(error) {
                          $("#login-button").removeAttr('disabled');
                          alert('Error : Failed to get user user information');
                          $('#mymobileModal').modal('toggle');
                          $scope.progressbar.complete();
                      }
                  );
              },
              // On error
              function(error) {
                  $("#login-button").removeAttr('disabled');
                  //$('#myModal').modal('toggle');
       	          $('#mymobileModal').modal('toggle');
                  $scope.progressbar.complete();
              }
          );
      };
	  $('.userotp').hide();

	  $('.mobileuserotp').hide();
	  $scope.sendMobileUserOtp = function(){
		  
	  }
	  
	  $scope.showDprofile = false;
		$scope.showDlogin = true;
		$scope.showMprofile = false;
		$scope.showMlogin = true;
		$scope.showreward = false;
	    $scope.dsignin = "Submit";

    $scope.signup = function() {
  	  

      $scope.dsignin = "Please Wait...";
     
   	  var rewardDetailId = $('#rewardDetailId').val();
   	  if(rewardDetailId != null){
   		  var fdata = "userName=" + $('#userName').val() +
             "&phone=" + $('#mobilePhone').val() +
             "&emailId=" + $('#emailId').val() +
             "&roleId=" + $('#roleId').val() +
             "&accessRightsId=" + $('#accessRightsId').val() +
             //"&password=" + $('#signpassword').val() +
             "&rewardDetailId=" + rewardDetailId;
   	  }
   	  else{ 
         var fdata = "userName=" + $('#userName').val() +
             "&phone=" + $('#mobilePhone').val() +
             "&emailId=" + $('#emailId').val() +
             "&roleId=" + $('#roleId').val() +
             "&accessRightsId=" + $('#accessRightsId').val();
            // "&password=" + $('#signpassword').val();
   	  }
   	 
         $http({
             method: 'POST',
             data: fdata,
             headers: {
                 'Content-Type': 'application/x-www-form-urlencoded'
             },
             url: 'ulosignup'
         }).then(function successCallback(response) {
        	 $scope.signup = response.data;
        	 
        	
      	    var alertmsg = ' <div class="alert alert-success alert-dismissible" style="color:red;text-align:center;">Login Successfull.</div>';
              $('#signuperrors').html(alertmsg);
              $(function() {
                  setTimeout(function() {
                      $("#signuperrors").hide('blind', {}, 100)
                  }, 2000);
              });
            //  $('#loginModal').modal('toggle');
              //location.reload();
             $('#myModal').modal('toggle');

             $scope.dsignin = "Submit";
             $scope.showDprofile = true;
             $scope.showDlogin = false;
             $scope.dofferlogin = false;
             $scope.showreward = true;
             $scope.UserIds = $scope.signup.data[0].userId;
             $scope.getUser();
      	  
        
         }, function errorCallback(response) {
        	  $scope.dsignin = "Submit";
        		 $("#signuperror").hide();
                 var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:red;text-align:center;">Mobile Number / Email already Already exist.</div>';
                 
               $('#signuperror').html(alertmsg);
		            $("#signuperror").fadeTo(2000, 500).slideUp(500, function(){
		                $("#signuperror").slideUp(500);
		            });
		            });
     };
     $scope.msignin = "Submit";
     $scope.mbsignup = function() {
  	   $scope.msignin = "Please Wait";
    	  var rewardDetailId = $('#mbRewardDetailId').val();
    	  if(rewardDetailId != null){
    		  var fdata = "userName=" + $('#mbUserName').val() +
              "&phone=" + $('#mbMobilePhone').val() +
              "&emailId=" + $('#mbEmailId').val() +
              "&roleId=" + $('#mbRoleId').val() +
              "&accessRightsId=" + $('#mbAccessRightsId').val() +
              //"&password=" + $('#signpassword').val() +
              "&rewardDetailId=" + rewardDetailId;
    	  }
    	  else{ 
          var fdata = "userName=" + $('#mbUserName').val() +
              "&phone=" + $('#mbMobilePhone').val() +
              "&emailId=" + $('#mbEmailId').val() +
              "&roleId=" + $('#mbRoleId').val() +
              "&accessRightsId=" + $('#mbAccessRightsId').val();
             // "&password=" + $('#signpassword').val();
    	  }
          $http({
              method: 'POST',
              data: fdata,
              headers: {
                  'Content-Type': 'application/x-www-form-urlencoded'
              },
              url: 'ulosignup'
          }).then(function successCallback(response) {
        	  $scope.mbsignup = response.data;
          	 $('#mymobileModal').modal('toggle');

               
               $scope.showMprofile = true;
               $scope.showMlogin = false;
               $scope.msignin = "Submit";
               $scope.showreward = true;
               $scope.UserIds = $scope.mbsignup.data[0].userId();
               $scope.getUser();
       
          }, function errorCallback(response) {
         	 //a0lert("erroor")
             $scope.msignin = "Submit";
             
        	 $("#msignuperror").hide();
             var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:red;text-align:center;">Mobile Number / Email already Already exist.</div>';
             
           $('#msignuperror').html(alertmsg);
		            $("#msignuperror").fadeTo(2000, 500).slideUp(500, function(){
		                $("#msignuperror").slideUp(500);
              });
          });
      };  
		
			
     
      $scope.sendUloLoginOtp = function() {
          var fdata = "username="+$('#loginUserName').val();

          $http({
              method: 'POST',
              data: fdata,
              headers: {
                  'Content-Type': 'application/x-www-form-urlencoded'
              },
              url: 'send-ulologin-otp'
          }).then(function successCallback(response) {
               $scope.sendLoginOtp = response.data;
       		$scope.ulouserId = $scope.sendLoginOtp.data[0].uloUserId;
       		$scope.username =  $scope.sendLoginOtp.data[0].userName;
               var alertmsg = ' <div class="alert alert-success alert-dismissible" style="color:red;text-align:center;">OTP Send Successfull.</div>';
               $('#otpsendmsg').html(alertmsg);
               $(function() {
                   setTimeout(function() {
                       $("#otpsendmsg").hide('blind', {}, 100)
                   }, 2000);
                   $('.userotp').show();
           		  $('.userinfo').hide(); 
               });
            },function errorCallback(response) {
            	 $("#loginerror").hide();
              var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:red;text-align:center;">This email/mobile number does not exist.</div>';
              
            $('#loginerror').html(alertmsg);
 		            $("#loginerror").fadeTo(2000, 500).slideUp(500, function(){
 		                $("#loginerror").slideUp(500);
          });
          });
      };
      
      $scope.mbSendUloLoginOtp = function() {
          var fdata = "username=" + $('#mbLoginUserName').val();

          $http({
              method: 'POST',
              data: fdata,
              headers: {
                  'Content-Type': 'application/x-www-form-urlencoded'
              },
              url: 'send-ulologin-otp'
          }).then(function successCallback(response) {
               $scope.mbSendLoginOtp = response.data;
               $scope.mbulouserId = $scope.mbSendLoginOtp.data[0].uloUserId;
       		$scope.mbusername =  $scope.mbSendLoginOtp.data[0].userName;
               $('.mobileuserotp').show();
     		  $('.mobileuserinfo').hide();
        
          }, function errorCallback(response) {
          	 $("#loginerrormb").hide();
              var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:red;text-align:center;">This email/mobile number does not exist.</div>';
              
            $('#loginerrormb').html(alertmsg);
 		            $("#loginerrormb").fadeTo(2000, 500).slideUp(500, function(){
 		                $("#loginerrormb").slideUp(500);
          });
       });
      };
      
      $scope.verifyUloLoginOtp = function() {
          var fdata = "userId=" + $('#uloLoginId').val()+  
                       "&loginOtp=" + $('#loginOtp').val();
          $http({
              method: 'POST',
              data: fdata,
              headers: {
                  'Content-Type': 'application/x-www-form-urlencoded'
              },
              url: 'verify-ulologin-otp'
          }).then(function successCallback(response) {
               $scope.verifyLoginOtp = response.data;
               $scope.showDprofile = true;
               $scope.showDlogin = false;
               $scope.dofferlogin = false;
               $scope.UserIds = $scope.verifyLoginOtp.data[0].userId;
               $scope.getUser();
               $('#loginModal').modal('toggle');
               /// location.reload();
              
          }, function errorCallback(response) {
       	 $("#loginerror").hide();
          var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:red;text-align:center;">Invalid username or password.</div>';
          
        $('#loginerror').html(alertmsg);
 	            $("#loginerror").fadeTo(2000, 500).slideUp(500, function(){
 	                $("#loginerror").slideUp(500);
          });
 	            
       });
      };
      
      $scope.mbVerifyUloLoginOtp = function() {
          var fdata = "userId=" + $('#mbUloLoginId').val()+  
                       "&loginOtp=" + $('#mbLoginOtp').val();
          $http({
              method: 'POST',
              data: fdata,
              headers: {
                  'Content-Type': 'application/x-www-form-urlencoded'
              },
              url: 'verify-ulologin-otp'
          }).then(function successCallback(response) {
               $scope.mbVerifyLoginOtp = response.data;
               $scope.UserIds = $scope.mbVerifyLoginOtp.data[0].userId;
                 $scope.getUser();
                 $('#loginmobileModal').modal('toggle');
                //location.reload();
              
          }, function errorCallback(response) {
           $("#loginerror").hide();
          var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:red;text-align:center;">Invalid username or password.</div>';
          
        $('#loginerror').html(alertmsg);
 	            $("#loginerror").fadeTo(2000, 500).slideUp(500, function(){
 	                $("#loginerror").slideUp(500);
          });
           
       });
      };     
     $scope.resendUloLoginOtp = function() {
         var fdata = "userId=" + $('#uloLoginId').val()+  
                      "&username=" + $('#uloLoginName').val();   /* $('#uloLoginName').val() */
       
         $http({
             method: 'POST',
             data: fdata,
             headers: {
                 'Content-Type': 'application/x-www-form-urlencoded'
             },
             url: 'resend-ulologin-otp'
         }).then(function successCallback(response) {
              $scope.resendLoginOtp = response.data;
             
         });
     };
     
     $scope.mbResendUloLoginOtp = function() {
         var fdata = "userId=" + $('#mbUloLoginId').val()+  
                      "&username=" + $('#mbUloLoginName').val();   /* $('#uloLoginName').val() */
         $http({
             method: 'POST',
             data: fdata,
             headers: {
                 'Content-Type': 'application/x-www-form-urlencoded'
             },
             url: 'resend-ulologin-otp'
         }).then(function successCallback(response) {
              $scope.mbResendLoginOtp = response.data;
             
         });
     };
     
     /* login and signup end here */  
   
	$scope.userData = [];
    
    
    $scope.viewData = [{
        text: "My View1",
        property: 0,
        photo: 0,
        description: 'desc 1'
      }, {
        text: "My View1",
        property: 0,
        photo: 1,
        description: 'desc 2'
      }, {
        text: "My View1",
        property: 0,
        photo: 2,
        description: 'desc 3'
      }, {
        text: "My View1",
        property: 0,
        photo: 3,
        description: 'desc 4'
      }, {
        text: "My View1",
        property: 0,
        photo: 4,
        description: 'desc 5'
      }, {
        text: "My View1",
        property: 0,
        photo: 9
      }];
    
    
    
    $('#forgetPanel').hide();
    $scope.showForget = function() {
        $('#loginPanel').hide();
        $('#forgetPanel').show();
    };
    $scope.showLogin = function() {
        $('#forgetPanel').hide();
        $('#loginPanel').show();
    };
    $scope.roomsSelected = [];
   
    
    //$scope.properties = [];
    $scope.firstProperties = [];
    
    
    $scope.getProperties = function() {
    	var locationId=$('#locationId').val();
    	if(locationId!=null && locationId!=""){
    		var fdata = "&startDate=" + $('#alternate').val() +
            "&endDate=" + $('#alternate1').val() +
            "&locationId=" + $('#locationId').val()
            $http({
                method: 'POST',
                data: fdata,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                url: "get-first-location-properties?limit="+3
            }).success(function(response) {
                $scope.properties = response.data;
                $scope.properties = $filter('orderBy')($scope.properties, '-available');
                $http({
                    method: 'POST',
                    data: fdata,
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    url: "get-location-properties"
                }).success(function(response) {
                	
                	$scope.properties = response.data;
                	$scope.showcontent = true;
                	$scope.properties = $filter('orderBy')($scope.properties, '-available');
                	
                    
                	 for(var i=0;i<$scope.properties.length;i++){
                		 $scope.displayNames =$scope.properties[i].locationName;
//                 		 $scope.getTimer($scope.properties[i].promoLastMinuteTimer,$scope.properties[i].DT_RowId); 
                	 }
                	 
                    
                	 
                });
               
            });
    	}
    	
    };
    function pad(n) {
      return (n < 10 ? "0" + n : n);
    }
    $scope.getTimer= function(hmsTimer,i){
    	if(hmsTimer!='None'){
    		var countDownDate = new Date(hmsTimer);
        	// Update the count down every 1 second
        		var x = setInterval(function() {

        	    // Get todays date and time
        	    var now = new Date().getTime();
        	    
        	    // Find the distance between now and the count down date
        	    var distance = countDownDate - now;
        	    
        	    // Time calculations for days, hours, minutes and seconds
        	    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        	    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        	    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        	    var seconds = Math.floor((distance % (1000 * 60)) / 1000);
        	    
        	    // Output the result in an element with id="demo"
        	    document.getElementById("timer"+i).innerHTML = pad(hours) + " : "
        	    + pad(minutes) + " : " + pad(seconds) ;
        	    document.getElementById("timermbl"+i).innerHTML = pad(hours) + " : "
        	    + pad(minutes) + " : " + pad(seconds) ;
        	    
        	    // If the count down is over, write some text 
        	    if (distance < 0) {
        	        clearInterval(x);
        	        document.getElementById("timer"+i).innerHTML = "EXPIRED";
        	        document.getElementById("timermbl"+i).innerHTML = "EXPIRED";
        	        
        	    }
        	}, 1000);	
    	}else{
//     		document.getElementById("timer"+i).innerHTML = "";
    	}
    	
    };
    
    
    $scope.getProperties = function() {
    	var locationId=$('#locationId').val();
    	if(locationId!=null && locationId!=""){
    		var fdata = "&startDate=" + $('#alternate').val() +
            "&endDate=" + $('#alternate1').val() +
            "&locationId=" + $('#locationId').val()
            $http({
                method: 'POST',
                data: fdata,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                url: "get-location-properties?limit="+4
                //var url = "get-accommodation-rooms?limit="+total+"&offset="+nextOffset;
            }).success(function(response) {
                $scope.firstProperties = response.data;
                $scope.showcontent = true;
                for(var i=0;i < $scope.firstProperties.length;i++){
                	$scope.properties.push($scope.firstProperties[i]); 
                }
                //$scope.properties.push(JSON.stringify($scope.firstproperties));
                //$scope.properties = $filter('orderBy')($scope.properties, '-available');
              
                
            });
    	}
    	
    };
    
    $scope.getAllProperties = function() {
    	var locationId=$('#locationId').val();
    	if(locationId!=null && locationId!=""){
    		var fdata = "&startDate=" + $('#alternate').val() +
            "&endDate=" + $('#alternate1').val() +
            "&locationId=" + $('#locationId').val()
            $http({
                method: 'POST',
                data: fdata,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                url: "get-location-all-properties"
                //var url = "get-accommodation-rooms?limit="+total+"&offset="+nextOffset;
            }).success(function(response) {
                $scope.allProperties = response.data;
              
                //$scope.properties.push(JSON.stringify($scope.firstproperties));
                //$scope.properties = $filter('orderBy')($scope.properties, '-available');
              
               
            });
    	}
    	
    };
    $scope.maxVal = 100;
	  $scope.max=100;
	    
    $scope.getScrollProperties = function() {
    	//$scope.getScrollId = [];
    	
            var firstlength = $scope.firstProperties.length;
            if(firstlength < $scope.allProperties.length){
            	var length = 4;
            	var pluslength = firstlength + (length);
                for(var i= firstlength;i<pluslength;i++){
                	$scope.properties.push($scope.allProperties[i])
                	$scope.firstProperties.push($scope.allProperties[i])
                }
            }
    };
    
  
    $scope.filterType = [];
    $scope.filterPrice = [];
    $scope.filterPolicy = [];
    $scope.resetFilter = [];
    
    $scope.getAreaProperties = function() {

    	var areaId=parseInt($('#areaId').val());
    	
    	//var areaLocationId=$('#areaLocationId').val();
    	if(!isNaN(areaId) && areaId != null && areaId!=""){
    		 var fdata = "startDate="+ $('#alternate').val()
    		 + "&endDate=" + $('#alternate1').val()
    		 + "&areaId=" + areaId;
    		 + "&sourceTypeId="+1;
            $http(
                    {
                       method : 'POST',
                       data : fdata,
                       headers : {
                           'Content-Type' : 'application/x-www-form-urlencoded'
 	                       },
                      url : "get-new-area-properties"
                    }).success(function(response) {
                    
                    	$scope.showloader = false;	
		              $scope.properties = response.data; 
		              $scope.properties = $filter('orderBy')($scope.properties, '-available');
		              
		              $scope.resetFilter = $scope.properties;
		              $scope.filterType = $scope.properties;
		              $scope.filterPrice = $scope.properties;
		              $scope.filterPolicy = $scope.properties;
		              $scope.progressbar.complete();
		              $scope.showcontent = true;
		              /* $http({
		                  method: 'POST',
		                  data: fdata,
		                  headers: {
		                      'Content-Type': 'application/x-www-form-urlencoded'
		                  },
		                  url: "get-area-properties"
		              }).success(function(response) {
		              	$scope.properties = response.data;
		              	 console.log($scope.properties);
		                   $scope.properties = $filter('orderBy')($scope.properties, '-available');
		                   $scope.showcontent = true;
		                   $scope.showloader = false;
		                 	 for(var i=0;i<$scope.properties.length;i++){
		                 		 $scope.getTimer($scope.properties[i].promoLastMinuteTimer,$scope.properties[i].DT_RowId); 
		                 	 }
		                     $scope.showloader = false; 
		                   $scope.resetFilter = $scope.properties;
		                   $scope.filterType = $scope.properties;
		                   $scope.filterPrice = $scope.properties;
		                   $scope.filterPolicy = $scope.properties;
		              
		                   $scope.progressbar.complete();  
		                   
		              }); */
             	
           
           });
    	}
   	 	
   };
  
   $scope.getProperties = function() {
   	var locationId=$('#locationId').val();
   	if(locationId!=null && locationId!=""){
   		var fdata = "&startDate=" + $('#alternate').val() +
           "&endDate=" + $('#alternate1').val() +
           "&locationId=" + $('#locationId').val()
           + "&sourceTypeId="+1;
           $http({
               method: 'POST',
               data: fdata,
               headers: {
                   'Content-Type': 'application/x-www-form-urlencoded'
               },
               url: "get-new-location-properties"
           }).success(function(response) {
        	     
               $scope.showloader = false;
               $scope.showcontent = true;
               $scope.properties = response.data;
               $scope.properties = $filter('orderBy')($scope.properties, '-available');
               $scope.resetFilter = $scope.properties;
               $scope.filterType = $scope.properties;
               $scope.filterPrice = $scope.properties;
               $scope.filterPolicy = $scope.properties;
               $scope.progressbar.complete();
               /* $http({
                   method: 'POST',
                   data: fdata,
                   headers: {
                       'Content-Type': 'application/x-www-form-urlencoded'
                   },
                   url: "get-location-properties"
               }).success(function(response) {
               	$scope.properties = response.data;
               	$scope.properties = $filter('orderBy')($scope.properties, '-available');
               	
               	$scope.showcontent = true;
              	 for(var i=0;i<$scope.properties.length;i++){
              		 $scope.getTimer($scope.properties[i].promoLastMinuteTimer,$scope.properties[i].DT_RowId); 
              	 }
              	 
               	$scope.resetFilter = $scope.properties;
                $scope.filterType = $scope.properties;
                $scope.filterPrice = $scope.properties;
                $scope.filterPolicy = $scope.properties;
              
                $scope.progressbar.complete();  
               }); */
            
           });
   	}
   	
   };
   
   $scope.getPropertyPhotos = function(rowId) {
	   
	   var PropertyId = 7;
      var url = "get-ulo-property-photos?propertyId="+PropertyId;
      $http.get(url).success(function(response) {
          //console.log(response); 
           $scope.photos = [];
           $scope.imagePrice = [];
           
           $scope.photos = response.data;
           console.log($scope.photos);
   for(var i = 0; i < $scope.properties.length; i++){
	  var proplength = $scope.properties[i];
	  
	  if(proplength.DT_RowId == PropertyId){
		var newdata = {'price':proplength.totalAmount};
		  $scope.imagePrice.push(newdata);

	  }
  } 
 
      });
  };

    $scope.sortByPrice = function() {
       var val = $('#sortBy').val()
    	//var val = $scope.sortBy;
        if (val == 1) {
            $scope.properties.sort(function(a, b) {
                return parseInt(a.totalAmount) - parseInt(b.totalAmount);
            });
        }
        if (val == 2) {
   
        	$scope.properties.sort(function(a, b) {
                return parseInt(b.totalAmount) - parseInt(a.totalAmount);
            });
        }
    };
    
    $scope.mbSortclose = function(){
    		    	$('#Filtersortpopup').modal('toggle');
    		    	$('#Filtersortpopup').modal('hide');
    		    };
    
    $scope.mbSortByPrice = function(val) {
   
    	if (val == 1) {
             $scope.properties.sort(function(a, b) {
           
                 return parseInt(a.totalAmount) - parseInt(b.totalAmount);
                 $('#Filtersortpopup').modal('hide');
             });
         }
         if (val == 2) {
    
         	$scope.properties.sort(function(a, b) {
                 return parseInt(b.totalAmount) - parseInt(a.totalAmount);
                 $('#Filtersortpopup').modal('hide');
             });
         }
         $('#Filtersortpopup').modal('toggle');
     };
   
    $scope.getLocation = function() {
    var url = "get-location";
        $http.get(url).success(function(response) {
            $scope.location = response.data;
            console.log($scope.location);
        });
    };
    
    $scope.getLocationAreas = function() {
    	var areaLocationId=$('#areaLocationId').val();
    	
        var fdata = "areaLocationId=" + $('#areaLocationId').val();
        $http({
            method: 'POST',
            data: fdata,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            url: 'get-ulo-location-areas'
        }).then(function successCallback(response) {
        	
            $scope.filteredAreas = response.data.data;
            
            
        }, function errorCallback(response) {});
        };
   
    $scope.range = function(min, max, step) {
        step = step || 1;
        var input = [];
        for (var i = min; i <= max; i += step) input.push(i);
        return input;
    };
   

   
  
    $scope.getCurrentDiscounts = function() {
        var propertyId = $('#propertyId').val();
        var ulosignup = $('#uloSignup').val();
        var url = "get-current-discounts";
        $http.get(url).success(function(response) {
            $scope.discounts = response.data;
            console.log($scope.discounts);
        });
    };
    $scope.getSeoLocationContent = function() {
    	var areaLocationId=$('#areaLocationId').val();
    	var locationId=$('#locationId').val();
        var fdata; 
	   	 if(locationId!=null && locationId!=""){
	   		 fdata = "locationId=" + $('#locationId').val();
	   	 }else{
	   		 fdata = "locationId=" + $('#areaLocationId').val();
	   	 }
        $http({
            method: 'POST',
            data: fdata,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            url: 'get-seo-location-content'
        }).then(function successCallback(response) {
            $scope.seoLocationContent = response.data;
            var cons = $scope.seoLocationContent.data[0].title;
            console.log($scope.seoLocationContent);
        }, function errorCallback(response) {});
    };
    
   
    
    
    
    
  
 
    $scope.qpush = function(indx){
    	// Measure adding a product to a shopping cart by using an 'add' actionFieldObject
    	// and a list of productFieldObjects.
    	dataLayer.push({
    	  'event': 'addToCart',
    	  'ecommerce': {
    	    'currencyCode': 'USD',
    	    'add': {                                // 'add' actionFieldObject measures.
    	      'products': [{                        //  adding a product to a shopping cart.
    	        'name': $scope.properties[indx].propertyName,
    	        'id': $scope.properties[indx].DT_RowId,
    	        'price': $scope.properties[indx].totalAmount,
    	        'category': $scope.properties[indx].accommodationName,
    	        'quantity': $scope.properties[indx].room
    	       }]
    	    }
    	  }
    	});
    	 
     };
   
	  $scope.getUrl = function() {
		  for(var i = 0; i < $scope.location.length; i++){
			    if($scope.locationId == $scope.location[i].DT_RowId){ 
			    	$scope.locationUrl =  $scope.location[i].locationUrl; 
			    	$scope.StartDate=document.getElementById("alternate").value;
   		 	      	$scope.EndDate=document.getElementById("alternate1").value;
			    }
		    }
	  };
  	
	  $scope.getChangeValues = function(){
    	  //$scope.locationUrl =  $scope.locationUrl;
          $scope.StartDate=document.getElementById("alternate").value;
	 	  $scope.EndDate=document.getElementById("alternate1").value;
	 	
	 	  
	  };
	  
	  $scope.getMbChangeValues = function(){
    	  //$scope.locationUrl =  $scope.locationUrl;
          $scope.mbStartDate=document.getElementById("msbalternate").value;
	 	  $scope.mbEndDate=document.getElementById("msbalternate1").value;
	 	
	 	  
	  };
	  
	  $scope.getPropertyTypes = function() {
		  
	        var url = "get-ulo-property-types";	            
	        $http.get(url).success(function(response) {
	        	$scope.propertytypes = response.data;

	        	//console.log($scope.amenities);
	        });
	    };
	    
	    
	    $scope.mbfiltersubmit = function(){
	    	
	    	 $('#modal').modal('toggle');
	    };
	    
	    $scope.propertyPolicy = 
	                              [{"policyName":"Couple Friendly","status":"false"},
	                              {"policyName":"Pay At Hotel","status":"false"},
	                              {"policyName":"Free BreakFast","status":"false"},
	                              {"policyName":"Early CheckIn","status":"false"}]
	    
	    $scope.filterByHotelPolicy = function(){  
	    	
	    	$scope.properties = [];
	    	
	    	 for(var j = 0; j< $scope.propertyPolicy.length; j++){
	    		 if($scope.propertyPolicy[j].status == true){
	    	for (var i = 0; i < $scope.filterPolicy.length; i++) {	    		
	    			 
	    				 if($scope.filterPolicy[i].payAtHotelStatus == 'true'){
	    					 $scope.properties.push($scope.filterPolicy[i]); 
	    					 //$('#filterpopup').modal('toggle');
	    					
	    				 }
	    				 else if($scope.properties == 0){
	    					 	$scope.shownoList = true;
	    					 	$('#filterpopup').modal('toggle');
	    					 	$scope.properties = $scope.resetFilter;
 			    		 }
	    				/*  else if($scope.propertyPolicy[j].status == $scope.filterPolicy[i].coupleStatus){
	    					 $scope.properties.push($scope.filterPolicy[i]);  
	    				 }
						else if($scope.propertyPolicy[j].status == $scope.filterPolicy[i].breakfastStatus){
							$scope.properties.push($scope.filterPolicy[i]); 
	    				 } */
	    				 //$scope.properties.push($scope.propertyPolicy[j]);	 

	    			 }
	    		 
	    	 }
	    	 }
	    	

	    };
	    
	    /* filter begins */
	    $scope.filterByHotelPrice = function(res){            
	    		    	$scope.properties = [];
	    		    	for(var k = 1; k <= $scope.filterPrice.length; k++){
	    		    		
	    		    		if([k] == $scope.filterPrice.length){
	    		    			 if($scope.properties == 0){
	    		    				 $scope.shownoList = true;	 
	    			    		$scope.properties = $scope.resetFilter;
	    		    			 }
	    		    		 }
	    		    		
	    		    		else if( $scope.filterPrice[k].totalAmount >= res.split('-')[0]  && $scope.filterPrice[k].totalAmount <= res.split('-')[1]){
	    		    			$scope.properties.push($scope.filterPrice[k]);
	    		    			$scope.shownoList = false;
	    		    			 
	    		    			
	    		    		}
	    		    		 else if(res.split('-')[0] == 5000 && $scope.filterPrice[k].totalAmount > 5000){
	    		    			 $scope.properties.push($scope.filterPrice[k]);
	    		    			 $scope.shownoList = false;
	    		    				
	    		    		 }
	    		    		 else if(res.split('-')[0] == 999  && $scope.filterPrice[k].totalAmount < 1000){
	    		    			 $scope.properties.push($scope.filterPrice[k]);
	    		    			 $scope.shownoList = false;
	    		    		 }
	    		    		 
	    		    	}
	    		    	$scope.filterType = $scope.properties;
	    		    };

$scope.filterByHotelTypes = function(indx){
	    	$('#dpbutton'+indx).addClass("active");
	    	
	    /* 	
	    	$('#dpbutton').addClass("active");
	    	$('#dpbutton2').addClass("active")
	    		$('#filterpopup').modal('toggle'); //or  $('#IDModal').modal('hide');
	     //or  $('#IDModal').modal('hide');
	    		    return false;	 */ 
	    	$scope.properties = [];
	    	for (var i = 0; i < $scope.propertytypes.length; i++) {
	    		
               if ($scope.propertytypes[i].isDeleted == true ) {
             	 
             		/*  if(checkbox_value == null){
             			 var checkbox_value = "";
             		 }
                	   checkbox_value += $scope.propertytypes[i].DT_RowId + ",";
                	  */
             	
                 
                	  
             	  
             	   for(var j = 0; j < $scope.filterType.length; j++){

             		  if($scope.filterType[j].propertyTypeId == $scope.propertytypes[i].DT_RowId){
             			  var filter = $scope.filterType[j];
                
                     	  if($scope.properties.length == $scope.filterType.length){
             			  
             			    	$scope.properties = [];
             			    	$scope.properties.push(filter);
             			    	if($scope.properties == 0){
	    		    				 $scope.shownoList = true;	 
	    			    		$scope.properties = $scope.resetFilter;
	    		    			 }
                     	  }
                     	  else{
                 			    
             			    	$scope.properties.push(filter);
             			    	$scope.shownoList = false;
             			    	if($scope.properties == 0){
   	    		    				 $scope.shownoList = true;	 
   	    			    		$scope.properties = $scope.resetFilter;
   	    		    			 }
                     	  /*  if($scope.properties != null){
       			    	 */
       			    	//}
             			    
             			       // $('#filterpopup').modal('toggle');
             			    } 
             		  
             			     
             			  
             			 /*  if(filter != null){
             				 
             				  $scope.properties.push(filter);
             			  }
             			  else{
             			  //$scope.filterProp = {"DT_RowId":"19","city":"Sholinganallur, Chennai","propertyTypeId":"1","propertyName":"ULO HOLIDAY STAY","propertyUrl":"ulo-hotel-hoilday-stay","payAtHotelStatus":"true","coupleStatus":"false","breakfastStatus":"false","hotelStatus":"false","arrivalDate":"2018-09-24","departureDate":"2018-09-25","diffDays":"1"};
                 		  $scope.properties.push(filter);
             			  } */
                 	
             		  } 
             		  else{
             			  if($scope.properties == 0){
	    		    				 $scope.shownoList = true;	 
	    			    		$scope.properties = $scope.resetFilter;
	    		    			 }
             		  }
             		 
          			
               } 
               }	 
	    }
	    	
	    		if($scope.properties == 0){
				 $scope.shownoList = true;	 
	    		$scope.properties = $scope.resetFilter;
			 }
	    		$scope.filterPrice = $scope.properties;
	    	
	    };
	    $scope.filterByHotelType = function(indx){
	    	$('#dpbutton2'+indx).addClass("active");
	    /* 	
	    	$('#dpbutton').addClass("active");
	    	$('#dpbutton2').addClass("active")
	    		$('#filterpopup').modal('toggle'); //or  $('#IDModal').modal('hide');
	     //or  $('#IDModal').modal('hide');
	    		    return false;	 */ 
	    	$scope.properties = [];
	    	for (var i = 0; i < $scope.propertytypes.length; i++) {
	    		
               if ($scope.propertytypes[i].isDeleted == true ) {
             	 
             		/*  if(checkbox_value == null){
             			 var checkbox_value = "";
             		 }
                	   checkbox_value += $scope.propertytypes[i].DT_RowId + ",";
                	   */
             	
                 
                	  
             	  
             	   for(var j = 0; j < $scope.filterType.length; j++){

             		  if($scope.filterType[j].propertyTypeId == $scope.propertytypes[i].DT_RowId){
             			  var filter = $scope.filterType[j];
                
                     	  if($scope.properties.length == $scope.filterType.length){
             			  
             			    	$scope.properties = [];
             			    	$scope.properties.push(filter);
             			    	if($scope.properties == 0){
	    		    				 $scope.shownoList = true;	 
	    			    		$scope.properties = $scope.resetFilter;
	    		    			 }
                     	  }
                     	  else{
                 			    
             			    	$scope.properties.push(filter);
             			    	$scope.shownoList = false;
             			    	if($scope.properties == 0){
   	    		    				 $scope.shownoList = true;	 
   	    			    		$scope.properties = $scope.resetFilter;
   	    		    			 }
                     	  /*  if($scope.properties != null){
       			    	 */
       			    	//}
             			    
             			       // $('#filterpopup').modal('toggle');
             			    } 
             		  
             			     
             			  
             			 /*  if(filter != null){
             				 
             				  $scope.properties.push(filter);
             			  }
             			  else{
             			  //$scope.filterProp = {"DT_RowId":"19","city":"Sholinganallur, Chennai","propertyTypeId":"1","propertyName":"ULO HOLIDAY STAY","propertyUrl":"ulo-hotel-hoilday-stay","payAtHotelStatus":"true","coupleStatus":"false","breakfastStatus":"false","hotelStatus":"false","arrivalDate":"2018-09-24","departureDate":"2018-09-25","diffDays":"1"};
                 		  $scope.properties.push(filter);
             			  } */
                 	
             		  } 
             		  else{
             			  if($scope.properties == 0){
	    		    				 $scope.shownoList = true;	 
	    			    		$scope.properties = $scope.resetFilter;
	    		    			 }
             		  }
             		 
          			
               } 
               }	 
	    }
	    	
	    		if($scope.properties == 0){
				 $scope.shownoList = true;	 
	    		$scope.properties = $scope.resetFilter;
			 }
	    		$scope.filterPrice = $scope.properties;
	    	
	    };

	    
    $scope.reseAllFilter = function(){            
			
	    	$scope.properties = [];
	    	$scope.properties = $scope.resetFilter;
	    	//document.getElementById('radioButton').checked = false;
	    };
	  
        $scope.getFilterSearch = function() {
				var url = 'get-filter-search';
				$http.get(url).success(function(response) {
				   //console.log(response);
					$scope.filterLocation= response.data;
					$scope.displayNameSearch($scope.filterLocation.length);
				});
			};
			
			$scope.displayNameSearch= function(areaLen){
 		    	
 		    	var areaId=parseInt($('#areaId').val());
 		    	var locationId = parseInt($('#locationId').val());
 		    	var finalId = "",finalType="";
 		    	if(!isNaN(areaId) && areaId != null && areaId!=""){
 		    		finalId=areaId;
 		    		finalType="Area";
 		    	}else if(!isNaN(locationId) && locationId != null && locationId!=""){
 		    		finalId=locationId;
 		    		finalType="Location";
 		    	}
 		    	
 		    	/* if(areaId != ''){
 		    		 finalId = areaId;
 		    	}else{
 		    		 finalId = locationId;
 		    	} */
 		    	
 		    	for(var i=0; i<=areaLen;i++){
 		    		if(finalId==$scope.filterLocation[i].DT_RowId){
 		    			if(finalType==$scope.filterLocation[i].type){
 		    				$scope.displayNames =$scope.filterLocation[i].displayLocationName;	
 		    			}
 		    					
 		    		};
 		    		
 		    	}
 		    	
 		    	
 		    };
			
		          $scope.StartDate=document.getElementById("alternate").value;
			 	  $scope.EndDate=document.getElementById("alternate1").value; 
			 	  $scope.mbStartDate=document.getElementById("msbalternate").value;
			 	  $scope.mbEndDate=document.getElementById("msbalternate1").value; 

			 	  $scope.locationurl = function(selected) {   
        	    $scope.locationUrl =  selected.originalObject.locationUrl;
        	    
        	    }
			 	 ;
				    
	             
	             $scope.getRewardDetails = function() {
	 				
	 				var url = "get-reward-details";
	 				$http.get(url).success(function(response) {
	 				    //console.log(response);
	 					$scope.rewardDetails = response.data;
	 		
	 				});
	 		   };
	 		   
	 		 
			
  $scope.curVal= 0;
	  
	  $scope.maxVal = 100;
	  $scope.max=100;
	  
	  $scope.myFunction = function(){
	    var copyText = document.getElementById("myInput");
	    copyText.select();
	    document.execCommand("copy");
	    var tooltip = document.getElementById("myTooltip");
	    tooltip.innerHTML = "Copied: " + copyText.value;
	    $("#myTooltip").fadeTo(2000, 500).slideUp(500, function(){
            $("#myTooltip").slideUp(500);
             });
	  };
	  $scope.getCurrentDiscounts = function() {
	        var propertyId = $('#propertyId').val();
	        var ulosignup = $('#uloSignup').val();
	        var url = "get-current-discounts";
	        $http.get(url).success(function(response) {
	            $scope.discounts = response.data;
	            console.log($scope.discounts);
	        });
	    };
	  	 $(".datepicker").datepicker({
	            //dateFormat: 'dd M DD',
				minDate: 0,
				numberOfMonths:2,
				autoclose: false,
	            beforeShowDay: function(date) {
				var date1 = $.datepicker.parseDate($.datepicker._defaults.dateFormat, $("#input1").val());
			    var date2 = $.datepicker.parseDate($.datepicker._defaults.dateFormat, $("#input2").val());
				return [true, date1 && ((date.getTime() == date1.getTime()) || (date2 && date >= date1 && date <= date2)) ? "dp-highlight" : ""]; 
				}, 
				onSelect: function(dateText, inst) {
				    
					var date1 = $.datepicker.parseDate($.datepicker._defaults.dateFormat, $("#input1").val());
					var date2 = $.datepicker.parseDate($.datepicker._defaults.dateFormat, $("#input2").val());
	                var dt1 = new Date(dateText);
				    var dd = dt1.getDate();
	                var mm = dt1.getMonth();
					var yy = dt1.getFullYear();
	                var dayName = dt1.getDay();
					
					
					
					if (!date1 || date2) {
					    
						
						checkIn = monthNames[mm] + ' ' + dd + '' + weekDays[dayName];
						alternate = monthNames[mm] + ' ' + dd + ',' + yy;
						stDate = dateText;	
						
						$("#input1").val(dateText);
						$("#datepicker0").val(checkIn);
						$("#alternate").val(alternate);
						$("#input2").val("");
				
	                    $(this).datepicker();
	            
					} else {
					
						  checkOut = monthNames[mm] + ' ' + dd + '' + weekDays[dayName];
	   						alternate1 = monthNames[mm] + ' ' + dd + ',' + yy;
	                        
	   						if ((Date.parse(dateText) < Date.parse(stDate))){
	   							
	   							
	   							$("#input2").val(stDate);
	   	   						$("#datepicker1").val(checkIn);
	   	   						$("#alternate1").val(alternate);
	   							
	   							$("#input1").val(dateText);
	   	   						$("#datepicker0").val(checkOut);
	   	   						$("#alternate").val(alternate1);
	   	   						
	   	   	                    
	   	   						
	   	   						$(this).datepicker();
	   	   						$('.datepicker').hide('slow');
	   	   						
	   	   						
	   							
	   						}
	   						
	   						else if((Date.parse(dateText) == Date.parse(stDate))){
	   							
	   							var nextDay = new Date(dateText);
	   							nextDay.setDate(nextDay.getDate()+ 1);
	   							var nextDD = nextDay.getDate();
	   		   	                var nextMM = nextDay.getMonth();
	   		   					var nextYY = nextDay.getFullYear();
	   		   	                var nextName = nextDay.getDay();
	   							
	   		   	                checkOut = monthNames[nextMM] + ' ' + nextDD + '' + weekDays[nextName];
	 						    alternate1 = monthNames[nextMM] + ' ' + nextDD + ',' + nextYY;
	 						    var input2 =  nextMM+1 + '/' + nextDD + '/' + nextYY;
	 						   
	   							$("#input2").val(input2);
	   	   						$("#datepicker1").val(checkOut);
	   	   						$("#alternate1").val(alternate1);
	   	   						
	   	   					    $(this).datepicker();
		   						$('.datepicker').hide('slow');
	   							
	   						}
	   						
	   						else {
	   							
	   							$("#input2").val(dateText);
	   	   						$("#datepicker1").val(checkOut);
	   	   						$("#alternate1").val(alternate1);
	   	   						
	   	   	                    $(this).datepicker();
	   	   						$('.datepicker').hide('slow');
	   							
	   						}
						
					}
					
				}
			});
	   	$(".mbdatepicker").datepicker({
	           //dateFormat: 'dd M DD',
	   		minDate: 0,
	   		numberOfMonths:1,
	   		autoclose: false,
	           beforeShowDay: function(date) {
	   		var date1 = $.datepicker.parseDate($.datepicker._defaults.dateFormat, $("#minput1").val());
	   	    var date2 = $.datepicker.parseDate($.datepicker._defaults.dateFormat, $("#minput2").val());
	   		return [true, date1 && ((date.getTime() == date1.getTime()) || (date2 && date >= date1 && date <= date2)) ? "dp-highlight" : ""]; 
	   		}, 
	   		onSelect: function(dateText, inst) {
	   		    
	   			var date1 = $.datepicker.parseDate($.datepicker._defaults.dateFormat, $("#minput1").val());
	   			var date2 = $.datepicker.parseDate($.datepicker._defaults.dateFormat, $("#minput2").val());
	               var dt1 = new Date(dateText);
	   		    var dd = dt1.getDate();
	               var mm = dt1.getMonth();
	   			var yy = dt1.getFullYear();
	               var dayName = dt1.getDay();
	   			
	   			
	   			
	   			if (!date1 || date2) {
	   			    
	   				
	   				checkIn = monthNames[mm] + ' ' + dd + '' + mbweekDays[dayName];
	   				malternate = monthNames[mm] + ' ' + dd + ',' + yy;
	   				
	   				
	   				
	   				$("#minput1").val(dateText);
	   				$("#mdatepicker0").val(checkIn);
	   				$("#malternate").val(malternate);
	   				$("#minput2").val("");
	   	
	                   $(this).datepicker();
	   			} else {
	   			
	   				if ((Date.parse(dateText) < Date.parse(stDate))){
						
	   					   checkOut = monthNames[mm] + ' ' + dd + '' + mbweekDays[dayName];
			   			   malternate1 = monthNames[mm] + ' ' + dd + ',' + yy;
							
							$("#minput2").val(stDate);
	  						$("#mdatepicker1").val(checkIn);
	  						$("#malternate1").val(malternate);
							
							$("#minput1").val(dateText);
	  						$("#mdatepicker0").val(checkOut);
	  						$("#malternate").val(malternate1);
	  						
	  	                    
	  						
	  						$(this).datepicker();
	  						$('.mbdatepicker').hide('slow');
	  						
	  						
							
						}
						
						else if((Date.parse(dateText) == Date.parse(stDate))){
							//checkOut = monthNames[mm] + ' ' + dd + '' + mbweekDays[dayName];
				   			//malternate1 = monthNames[mm] + ' ' + dd + ',' + yy;
				   			
				   			var nextDay = new Date(dateText);
								nextDay.setDate(nextDay.getDate()+ 1);
								var nextDD = nextDay.getDate();
			   	                var nextMM = nextDay.getMonth();
			   					var nextYY = nextDay.getFullYear();
			   	                var nextName = nextDay.getDay();
								
			   	                checkOut = monthNames[nextMM] + ' ' + nextDD + '' + weekDays[nextName];
							    malternate1 = monthNames[nextMM] + ' ' + nextDD + ',' + nextYY;
							    var input2 =  nextMM+1 + '/' + nextDD + '/' + nextYY;
							   
								$("#minput2").val(input2);
		   						$("#mdatepicker1").val(checkOut);
		   						$("#malternate1").val(malternate1);
		   						
		   					    $(this).datepicker();
	   						    $('.mbdatepicker').hide('slow');
				   			 
							
	  						
	  					    $(this).datepicker();
							$('.mbdatepicker').hide('slow');
							
						}
						
						else {
							
							checkOut = monthNames[mm] + ' ' + dd + '' + mbweekDays[dayName];
				   			malternate1 = monthNames[mm] + ' ' + dd + ',' + yy;
							$("#minput2").val(dateText);
	  						$("#mdatepicker1").val(checkOut);
	  						$("#malternate1").val(malternate1);
	  						
	  	                    $(this).datepicker();
	  						$('.mbdatepicker').hide('slow');
							
						}
	   				
	   			}
	   			
	   		}
	   	});	
	   	
	   	$(".msbdatepicker").datepicker({
	        //dateFormat: 'dd M DD',
			minDate: 0,
			numberOfMonths:1,
			autoclose: false,
	        beforeShowDay: function(date) {
			var date1 = $.datepicker.parseDate($.datepicker._defaults.dateFormat, $("#msbinput1").val());
		    var date2 = $.datepicker.parseDate($.datepicker._defaults.dateFormat, $("#msbinput2").val());
			return [true, date1 && ((date.getTime() == date1.getTime()) || (date2 && date >= date1 && date <= date2)) ? "dp-highlight" : ""]; 
			}, 
			onSelect: function(dateText, inst) {
			    
				var date1 = $.datepicker.parseDate($.datepicker._defaults.dateFormat, $("#msbinput1").val());
				var date2 = $.datepicker.parseDate($.datepicker._defaults.dateFormat, $("#msbinput2").val());
	            var dt1 = new Date(dateText);
			    var dd = dt1.getDate();
	            var mm = dt1.getMonth();
				var yy = dt1.getFullYear();
	            var dayName = dt1.getDay();
				
				
				
				if (!date1 || date2) {
				    
					
					checkIn = monthNames[mm] + ' ' + dd + '' + mbweekDays[dayName];
					malternate = monthNames[mm] + ' ' + dd + ',' + yy;
					stDate = dateText;	
					
					
					$("#msbinput1").val(dateText);
					$("#msbdatepicker0").val(checkIn);
					$("#msbalternate").val(malternate);
					$("#msbinput2").val("");
				
	                $(this).datepicker();
				} else {
					if ((Date.parse(dateText) < Date.parse(stDate))){
						
	   					   checkOut = monthNames[mm] + ' ' + dd + '' + mbweekDays[dayName];
			   			   malternate1 = monthNames[mm] + ' ' + dd + ',' + yy;
							
							$("#msbinput2").val(stDate);
	  						$("#msbdatepicker1").val(checkIn);
	  						$("#msbalternate1").val(malternate);
							
							$("#msbinput1").val(dateText);
	  						$("#msbdatepicker0").val(checkOut);
	  						$("#msbalternate").val(malternate1);
	  						
	  	                    
	  						
	  						$(this).datepicker();
	  						$('.msbdatepicker').hide('slow');
	  						
	  						
							
						}
						
						else if((Date.parse(dateText) == Date.parse(stDate))){
							//checkOut = monthNames[mm] + ' ' + dd + '' + mbweekDays[dayName];
				   			//malternate1 = monthNames[mm] + ' ' + dd + ',' + yy;
				   			
				   			var nextDay = new Date(dateText);
								nextDay.setDate(nextDay.getDate()+ 1);
								var nextDD = nextDay.getDate();
			   	                var nextMM = nextDay.getMonth();
			   					var nextYY = nextDay.getFullYear();
			   	                var nextName = nextDay.getDay();
								
			   	                checkOut = monthNames[nextMM] + ' ' + nextDD + '' + weekDays[nextName];
							    malternate1 = monthNames[nextMM] + ' ' + nextDD + ',' + nextYY;
							    var input2 =  nextMM+1 + '/' + nextDD + '/' + nextYY;
							   
								$("#msbinput2").val(input2);
		   						$("#msbdatepicker1").val(checkOut);
		   						$("#msbalternate1").val(malternate1);
		   						
		   					    $(this).datepicker();
	   						    $('.msbdatepicker').hide('slow');
				   			 
							
	  						
	  					    $(this).datepicker();
							$('.msbdatepicker').hide('slow');
							
						}
						
						else {
							
							checkOut = monthNames[mm] + ' ' + dd + '' + mbweekDays[dayName];
				   			malternate1 = monthNames[mm] + ' ' + dd + ',' + yy;
							$("#msbinput2").val(dateText);
	  						$("#msbdatepicker1").val(checkOut);
	  						$("#msbalternate1").val(malternate1);
	  						
	  	                    $(this).datepicker();
	  						$('.msbdatepicker').hide('slow');
							
						}
	   			  /*   checkOut = monthNames[mm] + ' ' + dd + '' + mbweekDays[dayName];
	   			    malternate1 = monthNames[mm] + ' ' + dd + ',' + yy;
	   				$("#minput2").val(dateText);
	   				$("#mdatepicker1").val(checkOut);
	   				$("#malternate1").val(malternate1);
	                   $(this).datepicker();
	   				$('.mbdatepicker').hide('slow'); */;
					
				}
				
				
			}
		});
	    $scope.getCurrentDiscounts();
	  $scope.getFilterSearch();
	  $scope.getRewardDetails();
	    //$scope.getPropertyPhotos(); 
	  $scope.getAreaProperties(); 
	  $scope.getPropertyTypes();
	  $scope.getLocation();
	  $scope.getLocationAreas();
	$scope.getProperties();  
	//$scope.getAllProperties();
   // $scope.getSeoLocationContent();    
    //$scope.getCurrentDiscounts();
      
   // $scope.getPropertyAmenities1();
   // $scope.propertyDataLayer(); 
});
app.directive('starRating', function() { return { restrict: 'A', template: '<ul class="rating"> ' + '<li ng-repeat="star in stars" ng-class="star">' + '<i class="fa fa-star" aria-hidden="true"></i>' + '</li>' + '</ul>', scope: { ratingValue: '=', max: '=' }, link: function(scope, elem, attrs) { scope.stars = []; for (var i = 0; i < scope.max; i++) { scope.stars.push({ filled: i < scope.ratingValue }); } } } }); app.directive('starRating2', function() { return { restrict: 'A', template: '<ul class="rating"> ' + '<li ng-repeat="star in stars" ng-class="star">' + '<i class="fa fa-dot-circle-o"></i>' + '</li>' + '</ul>', scope: { ratingValue: '=', max: '=' }, link: function(scope, elem, attrs) { scope.stars = []; for (var i = 0; i < scope.max; i++) { scope.stars.push({ filled: i < scope.ratingValue }); } } } });
</script>
<script>
$( "#datepicker0" ).focus(function() { $('.datepicker').show('slow'); }); $( "#mdatepicker0" ).focus(function() { $('.mbdatepicker').show('slow'); }); $( "#msbdatepicker0" ).focus(function() {; $('.msbdatepicker').show('slow'); }); var monthNames = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ]; var weekDays = [ " Sunday", " Monday", " Tuesday", " Wednesday", " Thursday", " Friday", " Saturday" ]; var mbweekDays = [ " Sun", " Mon", " Tue", " Wed", " Thu", " Fri", " Sat" ]; var monthNos = [01,02,03,04,05,06,07,08,09,10,11,12]; var today = new Date(); var toda_dd = today.getDate(); var toda_mm = today.getMonth(); var toda_yy = today.getFullYear(); var toda_day = today.getDay(); var tomorrow = new Date(); tomorrow.setDate(tomorrow.getDate() + 1); var tomo_dd = tomorrow.getDate(); var tomo_mm = tomorrow.getMonth(); var tomo_yy = tomorrow.getFullYear(); var tomo_day = tomorrow.getDay(); if ($('#datepicker0').val() == ""){ $('#input1').val(monthNos[toda_mm] + '/' + toda_dd + '/' + toda_yy); $('#input2').val(monthNos[tomo_mm] + '/' + tomo_dd + '/' + tomo_yy); $('#datepicker0').val(monthNames[toda_mm] + ' ' + toda_dd + '' + toda_day); $('#datepicker1').val(monthNames[tomo_mm] + ' ' + tomo_dd + '' + weekDays[tomo_day]); $('#alternate').val(monthNames[toda_mm] + ' ' + toda_dd + ',' + toda_yy); $('#alternate1').val(monthNames[tomo_mm] + ' ' + tomo_dd + ',' + tomo_yy); $('#minput1').val(monthNos[toda_mm] + '/' + toda_dd + '/' + toda_yy); $('#minput2').val(monthNos[tomo_mm] + '/' + tomo_dd + '/' + tomo_yy); $('#mdatepicker0').val(monthNames[toda_mm] + ' ' + toda_dd + '' + mbweekDays[toda_day]); $('#mdatepicker1').val(monthNames[tomo_mm] + ' ' + tomo_dd + '' + mbweekDays[tomo_day]); $('#malternate').val(monthNames[toda_mm] + ' ' + toda_dd + ',' + toda_yy); $('#malternate1').val(monthNames[tomo_mm] + ' ' + tomo_dd + ',' + tomo_yy); $('#msbinput1').val(monthNos[toda_mm] + '/' + toda_dd + '/' + toda_yy); $('#msbinput2').val(monthNos[tomo_mm] + '/' + tomo_dd + '/' + tomo_yy); $('#msbdatepicker0').val(monthNames[toda_mm] + ' ' + toda_dd + '' + mbweekDays[toda_day]); $('#msbdatepicker1').val(monthNames[tomo_mm] + ' ' + tomo_dd + '' + mbweekDays[tomo_day]); $('#msbalternate').val(monthNames[toda_mm] + ' ' + toda_dd + ',' + toda_yy); $('#msbalternate1').val(monthNames[tomo_mm] + ' ' + tomo_dd + ',' + tomo_yy); }
$(document).ready(function() { var showChar = 310; var ellipsestext = ""; var moretext = "Read more"; var lesstext = "Read less"; $('.more').each(function() { var content = $(this).html(); if(content.length > showChar) { var c = content.substr(0, showChar); var h = content.substr(showChar, content.length - showChar); var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>'; $(this).html(html); } }); $(".morelink").click(function(){ if($(this).hasClass("less")) { $(this).removeClass("less"); $(this).html(moretext); } else { $(this).addClass("less"); $(this).html(lesstext); } $(this).parent().prev().toggle(); $(this).prev().toggle(); return false; }); }); var uri = window.location.toString(); if (uri.indexOf("?") > 0) { var clean_uri = uri.substring(0, uri.indexOf("?")); window.history.replaceState({}, document.title, clean_uri); }	
</script>
<style>
.mydimage .modal-body{position:relative;padding:0}.mydimage .modal-body img{height:400px;width:600px}.mydimagetwo h4{font-size:12px}.mydimagetwo .modal-dialog{position:absolute;top:50%!important;transform:translate(0,-50%)!important;-ms-transform:translate(0,-50%)!important;-webkit-transform:translate(0,-50%)!important;margin:auto 8%;width:85%;height:80%}.mydimagetwo .modal-body{position:relative;padding:0}.datepicker,.mbdatepicker,.msbdatepicker{display:none;z-index:2;cursor:pointer;position:absolute}.mydimagetwo .modal-body img{height:200px}.mbdatepicker{top:-500%;left:0}.dp-highlight,.dp-highlight .ui-state-default{background:#a9e006;color:#FFF;text-align:center}.ui-datepicker.ui-datepicker-multi{width:50%!important;margin:0 400px auto;display:block}.ui-datepicker-multi .ui-datepicker-group{float:left}#datepicker,#mbdatepicker{height:300px;overflow-x:scroll}.ui-widget{font-size:100%}@media (min-width:320px) and (max-width:767px){.middle-header{display:none}}.gallery #largeSlider .slides{background-color:#151515}.gallery .paddit{padding:20px}.gallery .slide{color:#fff;font-size:20px;text-align:center;margin-right:5px;margin-top:5px}.gallery p{padding:0;margin:5px!important}.gallery #navigationSlider img{max-height:60px}.gallery #largeSlider img{display:inline}.gallery .centerSlider{background-color:red;text-align:center}.gallery .slide p{color:#fff}.gallery .slick-next .slick-arrow{background:url(ulowebsite/images/chevron-right.png)!important}.slick-next{width:10px}.slick-prev{left:-36px}.slick-prev:before{color:red!important;content:url(ulowebsite/images/chevron-left.png)!important}.slick-next:before{color:red!important;content:url(ulowebsite/images/chevron-right.png)!important}.gallery .fadein,.gallery .fadeout{-webkit-transition:all cubic-bezier(.25,.46,.45,.94) 1.5s;-moz-transition:all cubic-bezier(.25,.46,.45,.94) 1.5s;-o-transition:all cubic-bezier(.25,.46,.45,.94) 1.5s;transition:all cubic-bezier(.25,.46,.45,.94) 1.5s}.gallery .fadein.ng-hide-remove,.gallery .fadeout.ng-hide-add.ng-hide-add-active{opacity:0;display:block!important}.gallery .fadein.ng-hide-remove.ng-hide-remove-active,.gallery .fadeout.ng-hide-add{opacity:1;display:block!important}.modal-content .gallery{background:#fff!important}.gallery .modal-content,.gallery .modal-footer,.gallery .modal-header{color:#EEE}.gallery .modal-body{background:#151515;padding:0!important}.gallery .modal-footer{margin-top:0;padding-left:30px!important;padding-right:30px!important}.gallery .modal-footer,.gallery .modal-header{border-color:#333!important}.tooltip{position:relative;display:inline-block}.tooltip .tooltiptext{visibility:hidden;width:140px;background-color:#555;color:#fff;text-align:center;border-radius:6px;padding:5px;position:absolute;z-index:1;bottom:150%;left:50%;margin-left:-75px;opacity:0;transition:opacity .3s}.tooltip .tooltiptext::after{content:"";position:absolute;top:100%;left:50%;margin-left:-5px;border-width:5px;border-style:solid;border-color:#555 transparent transparent}
</style>