<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
 <%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <section id="innerPageContent">
    	<!-- Tab Header -->
       <div class="headingBarps">
             <div class="container ">
                <div class="row">
                     <div class="col-sm-12">

                   <img src="contents/images/paysuccess/tp1.png" class="img-responsive thankimg text-center" style="display:block;margin:0 auto;">
     
                     </div>
                </div>
            </div>
        </div>
        <!-- Tab Header -->                        <input type="hidden" value ="<%=session.getAttribute("bookingId") %>" id="bookingId" name="bookingId" >
            <div ng-repeat="bk in booked" >
                         
                         <input type="hidden" id="propertyid"  value ="<%=session.getAttribute("uloPropertyId") %>" id="propertyId" name="propertyId" >
                        <input type="hidden" id="propertyBookingid"  value="{{bk.bookingid}}"> 
                         <input type="hidden" id="propertyNames" value="{{bk.propertyName}}">
                         <input type="hidden" id="propertyName" value="{{bk.propertyName}}">
                         <input type="hidden" id="PropertyAmount" value="{{bk.totalamount}}">
                         <input type="hidden" id="PropertyRoom" value="{{bk.rooms}}">
                          <input type="hidden" id="Propertytax" value="{{bk.tax}}">
                          <input type="hidden" id="Propertytaxs" value="{{bk.tax}}">
                          <input type="hidden" id="PropertySku" value="ABC1234">
                   <div ng-repeat="tg in bk.types">
                   <input type="hidden" id="RoomName" value="{{tg.accommodationType}}">
                   <input type="hidden" id="Propertytotal" value="{{tg.total}}"> 
                
                   </div> 
                   <div ng-init="myFunction()"></div>
                         </div>  
        <div class="container tabBGps " id="printer">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  >
                        <input type="hidden" value ="<%=session.getAttribute("bookingId") %>" id="bookingId" name="bookingId" >
                	<!-- Overview Tab -->
               <div class="col-lg-8 col-md-9 col-sm-12 col-xs-12 psgd" >

               <h4>Guest Details</h4>
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 btop">
               <div class="col-lg-4 col-sm-4 ">
               <p class="dtext">Primary Guest</p>
               <p class="ltext">{{booked[0].guestname}} </p>
               </div>
               <div class="col-lg-5 col-sm-4 col-xs-6">
               <p class="dtext">Email Id</p>
               <p class="ltext">{{booked[0].emailId}}</p>
               </div>
               <div class="col-lg-3 col-sm-4 col-xs-6">
               <p class="dtext">Mobile</p>
               <p class="ltext">{{booked[0].mobile}}</p>
               </div>
               </div>
               <h4 >Booking Details</h4>
                             <div class="col-lg-6 col-sm-6 col-xs-6"> 
                <p class="dtext">Booking Id</p>
               <p class="ltext"><%=session.getAttribute("bookingId") %></p>
                <p class="dtext">Check In</p>
               <p class="ltext">{{booked[0].checkIn}}</p>
                <p class="dtext">Rooms</p>
               </div>
               <div class="col-lg-6 col-sm-6 col-xs-6"> 
                <p class="dtext">Booking Date</p>
               <p class="ltext">{{booked[0].timeHours}}</p>
                 <p class="dtext">Check Out</p>
               <p class="ltext">{{booked[0].checkOut}}</p>
                  <p class="dtext">Guest</p>
          
               </div>
               <div ng-repeat="bk in booked" >
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" ng-repeat="tg in bk.types">
             <div class="col-lg-6 col-sm-6 col-xs-6"> 
                         
               <p class="ltext">{{tg.accommodationType}}<span class="spanltext"> ( {{tg.rooms}} Rooms * {{booked[0].days}}  N ) </span></p>
              </div>
              <div class="col-lg-6 col-sm-6 col-xs-6"> 
               <p class="ltext"> {{tg.adultCount - tg.childCount}} <span class="spanltext">( {{tg.adultCount}} Adult , {{tg.childCount}} Child ) </span></p>
              </div>
              </div>
             </div>
              
                  <div class="col-lg-8 col-sm-12 borderamount" ng-show="paymentstatus == paid">
              <div class="col-lg-6 col-sm-6 col-xs-6"><h3>Amount Paid </h3></div>
              <div class="col-lg-6 col-sm-6 col-xs-6 "><h3><i class="fa fa-inr"></i>{{booked[0].totalamount | currency:"":0}} </h3></div>
              </div>
                   <div class="col-lg-8 col-sm-12 borderamount" ng-hide="paymentstatus == paid">
              <div class="col-lg-6 col-sm-6"><h3>Pay @ Hotel </h3></div>
              <div class="col-lg-6 col-sm-6"><h3><i class="fa fa-inr"></i>{{booked[0].totalamount | currency:"":0}} </h3></div>
              </div>
              <div class="col-lg-4 col-sm-4" ></div>
              
               </div>
             <div class="col-sm-4 pspd"><h4>Property Details</h4>
             <img src="get-property-thumb?propertyId=<%=session.getAttribute("uloPropertyId") %>" class="img-responsive" />
               <div class="caption">
                           <h5 >{{booked[0].propertyName}}</h5>
                           <span class="hoteladdress">{{booked[0].address1}}{{booked[0].address2}}<a href="https://www.google.com/maps/?q={{booked[0].latitude}},{{booked[0].longitude}}" target="_blank" class="pmap">View Map</a></span>
                        </div>
             </div> 	
                   </div>
             
            </div>
            <div class="row">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 note">
                   <h4>Note <i class="fa fa-edit pnoteicon"></i></h4>
                   <p class="pnote">The confirmation voucher will be directly sent to your mail.</p>
                   <p class="pnote">It is mandatory to show the confirmation voucher during check-in time.</p>
                   <p class="pnote">Please do not hesitate to contact us if you any further questions.</p>
                   <p class="pnote">We assure you that you will enjoy your stay with us.</p>
                   </div>
            </div>
              <div class="row">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                  <p class="text-center queries">Have queries? Mail to <a href="support@ulohotels.com" class="contact">support@ulohotels.com</a> or Call @ <a href="tel:+919543592593" class="contact">9543 592 593</a></p>
                  </div>
                  </div>
        </div>
        
    </section>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>

<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>
<script type="text/javascript"  src="contents/js/ngprogress.js"></script>
<script>

        var app = angular.module('myApp',['ngProgress']);

        app.controller('customersCtrl',function($scope, $http,$timeout,ngProgressFactory,$location) {
        
        	$scope.userData = [];
 
	      	$scope.login = function () {


	      		var fdata = "username=" + $('#username').val() +
	      			"&password=" + $('#password').val();
	      		//alert(fdata);

	      		$http({
	      			method: 'POST',
	      			data: fdata,
	      			headers: {
	      				'Content-Type': 'application/x-www-form-urlencoded'
	      			},
	      			url: 'ulologin'

	      		}).then(function successCallback(response) {
	      			$scope.userData = response.data;
	      			console.log($scope.userData);
	      			var username = $scope.userData.data[0].userName;
	      			var email = $scope.userData.data[0].emailId;
	      			var phone = $scope.userData.data[0].phone;

	      			$('#loginModal').modal('toggle');
	      			$('#logSec').html("<div class='mnuContant'><a href='mailto:support@ulohotels.com' title='support@ulohotels.com'><i class='fa fa-envelope'></i><span class='visibleLG'>support@ulohotels.com</span></a><a href='tel:9543592593' title=''+91 95435 92593'><i class='fa fa-phone'></i><span class='visibleLG'>+91 95435 92593</span></a></div><div class='mnuProfile'><div class='mpAvatar hidden-xs' style='background:url(ulowebsite/images/avatar.png);'></div><div class='mpUsername'><a href='javascript:showpro();' class=' mnuProfileOpen'>Hi admin<i class='fa fa-angle-down'></i></a></div><div class='mpSubMenu' id='showmenu' ><ul><li><a href='ulouserprofile'>Profile</a></li><li><a href='ulouserprofile'>Booking History</a></li><li><a href='ulologout'>Log out</a></li></ul></div></div>");
	      		}, function errorCallback(response) {

	      			var alert = ' <div class="alert alert-danger alert-dismissible" style="color:red">Invalid username or password.</div>';
	      			$('#loginerror').html(alert);
	      		    $(function() {
	      	          // setTimeout() function will be fired after page is loaded
	      	          // it will wait for 5 sec. and then will fire
	      	          // $("#successMessage").hide() function
	      	          setTimeout(function() {
	      	              $("#loginerror").hide('blind', {}, 100)
	      	          }, 5000);
	      	      });
	      		});

	      	};

	      	$scope.signup = function () {


	      		var fdata = "userName=" + $('#userName').val() +
	      			"&phone=" + $('#mobilePhone').val() +
	      			"&emailId=" + $('#emailId').val() +
	      			"&roleId=" + $('#roleId').val() +
	      			"&accessRightsId=" + $('#accessRightsId').val() +
	      			"&password=" + $('#signpassword').val();
	      		//alert(fdata);

	      		$http(

	      			{
	      				method: 'POST',
	      				data: fdata,
	      				headers: {
	      					'Content-Type': 'application/x-www-form-urlencoded'
	      				},
	      				url: 'ulosignup'
	      			}).then(function successCallback(response) {


	      			if (response.data == "") {

	      				var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:red">Sorry This Email Id already exist.</div>';
	      				$('#signuperror').html(alertmsg);


	      			} else {

	      				$scope.userData = response.data;
	      				var username = $scope.userData.data[0].userName;
	      				var email = $scope.userData.data[0].emailId;
	      				var phone = $scope.userData.data[0].phone;


	      				$('#registerModal').modal('toggle');

	      				$('#logSec').html("<div class='mnuContant'><a href='mailto:support@ulohotels.com' title='support@ulohotels.com'><i class='fa fa-envelope'></i><span class='visibleLG'>support@ulohotels.com</span></a><a href='tel:9543592593' title=''+91 95435 92593'><i class='fa fa-phone'></i><span class='visibleLG'>+91 95435 92593</span></a></div><div class='mnuProfile'><div class='mpAvatar hidden-xs' style='background:url(ulowebsite/images/avatar.png);'></div><div class='mpUsername'><a href='javascript:showpro();' class=' mnuProfileOpen'>Hi admin<i class='fa fa-angle-down'></i></a></div><div class='mpSubMenu' id='showmenu' ><ul><li><a href='ulouserprofile'>Profile</a></li><li><a href='ulouserprofile'>Booking History</a></li><li><a href='ulologout'>Log out</a></li></ul></div></div>");
	      			}

	      		}, function errorCallback(response) {


	      			var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:red">This Email ID Already Exist</div>';
	      			$('#signuperror').html(alertmsg);

	      			// or server returns response with an error status.
	      		});

	      	};


	      	$scope.passwordRequest = function () {


	      		var fdata = "emailId=" + $('#forgetEmailId').val();


	      		$http({
	      			method: 'POST',
	      			data: fdata,
	      			headers: {
	      				'Content-Type': 'application/x-www-form-urlencoded'
	      			},
	      			url: 'ulosendpassword'
	      		}).then(function successCallback(response) {

	      			var alertmsg = ' <div class="alert alert-success alert-dismissible" style="color:red">Password Sent Sucessfully.</div>';

	      			$('#forgetmessage').html(alertmsg);


	      		}, function errorCallback(response) {


	      			var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:red">Your Email ID does not Registred Yet</div>';
	      			$('#forgetmessage').html(alertmsg);


	      		});

	      	};

	      	$('#forgetPanel').hide();
	      	$scope.showForget = function () {

	      		$('#loginPanel').hide();
	      		$('#forgetPanel').show();


	      	};

	      	$scope.showLogin = function () {

	      		$('#forgetPanel').hide();
	      		$('#loginPanel').show();


	      	};

        $timeout(function () {
          	$('#btnclose').click();
          }, 1000);
          $timeout(function () {
          	$('#btncloses').click();
          }, 1000);
           var bookingId = $('#bookingId').val();
        $scope.getBookedDetails = function(){
        var fdata = "bookingId=" + bookingId; 
        $http(
   {

                            method : 'POST',

                            data : fdata,

                            headers : {

                                'Content-Type' : 'application/x-www-form-urlencoded'

                            },

                            url : 'get-booked-details'

                        }).success(function(response) {
                           $scope.booked = response.data;
                           //alert($scope.booked[0].bookingid);
           
                        });
 };
       $scope.myFunction = function(){
    	   setTimeout(function wait(){
    	        dataLayer.push({
            		'event': 'purchase',	
            	  'ecommerce': {
            	    'currencyCode': 'INR', 
            	    'purchase': {
            	      'actionField': {
            	  		'id': document.getElementById("propertyid").value, // Transaction ID. Required.
            			'affiliation': document.getElementById("propertyName").value, // Affiliation or store name.
            			'revenue': document.getElementById("Propertytotal").value
            	      },
            	      'products': [{                            // List of productFieldObjects.// Name or ID is required.
            	    	  'name': document.getElementById("propertyName").value,  
            	    	  'id': document.getElementById("propertyBookingid").value,
            		        'price': document.getElementById("Propertytotal").value, // Unit price.
            		    	'quantity': document.getElementById("PropertyRoom").value,
            		       'category': document.getElementById("RoomName").value                         // Optional fields may be omitted or set to empty string.
            	       }]
            	    }
            	  }
            	});
    	    }, 5000);

    		//alert(document.getElementById("propertyNames").value);
    		//console.log("hai0" + $scope.booked.types[0].accommodationType);
    
   
    	};

  $scope.getBookedDetails();
     });
</script>
<script>
// Send transaction data with a pageview if available
// when the page loads. Otherwise, use an event when the transaction
// data becomes available.

</script>
<script src="ulowebsite/js/jquery.min.js"></script>

    