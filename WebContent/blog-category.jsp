<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="s" uri="/struts-tags"%>

   <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Blog Category
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
   
            <li class="active">Blog Category</li>
          </ol>
        </section>
		
        <!-- Main content -->
        <section class="content">
        
        	<div class="row">
            	<div class="col-xs-12" > <!-- ng-if="properties.length>=0" -->
						<div class="box">
							<div class="box-header">
								<h3 class="box-title">Blog Category</h3>
							</div>
							
							
							<!-- /.box-header -->
							<div class="box-body table-responsive no-padding" > 
								<table class="table table-hover" id="example1">
									<tr>
										<th>Order</th>
										<th>Category Name</th>
										<th>Edit</th>
										<th>Delete</th>
									
									</tr>
									<tr ng-repeat="b in blogcategories">
										<td>{{b.serialNo}}</td>
										<td>{{b.blogCategoryName}}</td>
										<td>
											<a href="#editmodal" ng-click="getBlogCategory(b.blogCategoryId)"  data-toggle="modal"><i  class="fa fa-pencil text-green"></i></a>
										</td>
										<td>
											<a href="#deletemodal" ng-click="deleteBlogCategory(b.blogCategoryId)" data-toggle="modal" ><i  class="fa fa-fw fa-trash text-red" ></i></a>
										</td>
									</tr>
									<tr>
									</tr>
								</table>
							</div>
							<!-- /.box-body -->
							
							
							<div class="box-footer clearfix">
								 <ul class="pagination pagination-sm no-margin pull-right">
									<a class="btn btn-primary " href="#addmodal" data-toggle="modal" >Add Category</a>
								</ul> 
							</div>
							<div class="box-footer clearfix">
								<ul class="pagination pagination-sm"></ul>
								<div class="simple-pagination pull-right" >   
									<p class="simple-pagination__items">Showing {{blogCategoryCount[0].count}} out of {{settings.pageLimit}}</p>
									<p><button ng-click="previousPage()" ng-disabled="settings.currentPage <= 0" class="simple-pagination__button simple-pagination__button--prev"> &#10094;          
									</button><span class="simple-pagination__pages">{{settings.currentPage + 1}} of {{getTotalPages()}}</span>
									<button ng-click="nextPage()" ng-disabled="settings.currentPage === (getTotalPages() - 1)" class="simple-pagination__button simple-pagination__button--next">&#10095;</button></p>
									<a href="" ng-click="limited(10)" ng-class="active">{{10}}</a> |
									<span><a href="" ng-click="all(blogCategoryCount[0].count)" ng-class="active">All</a></span> 
								</div>
						   </div> 
						</div>
						<!-- /.box -->
					</div>	
         	</div>
          
        </section><!-- /.content -->
      	<div class="modal" id="addmodal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" id="btnclose" class="close" data-dismiss="modal"
							aria-hidden="true">x</button>
						<h4 class="modal-title">Add Category</h4>
					</div>
					 <form method="post" theme="simple" name="addblogcategory">
					 <section>
					<div class="modal-body" >
						<div class="form-group col-md-12">
						  <label>Category Name<span style="color:red">*</span></label>
						  <input type="text" class="form-control"  name="categoryName"  id="categoryName"  placeholder="Category Name" value="" ng-model='categoryName' ng-pattern="^/^[A-Za-z\d\s]+$/" ng-required="true" maxlength="250">						  
		                 <span ng-show="addblogcategory.categoryName.$error.pattern" style="color:red">Not a Category Name!</span>
		                  </select>
						</div>
						<div class="form-group col-md-12">
							<label for="categoryContent">Category Content<span style="color:red">*</span></label>
							<input type="text" class="form-control"  name="categoryContent"  id="categoryContent"  placeholder="Category Name" value="" ng-model='categoryContent' ng-pattern="^/^[A-Za-z\d\s]+$/" ng-required="true" maxlength="10485760">
						    <span ng-show="addblogcategory.categoryContent.$error.pattern" style="color:red">Not a Category Content!</span>
						</div>
						<div class="form-group col-md-12">
							<label for="categoryUrl">Category URL<span style="color:red">*</span></label>
							<input type="text" class="form-control"  name="categoryUrl"  id="categoryUrl"  placeholder="Category URL" value="" ng-model='categoryUrl' ng-pattern="^/^[A-Za-z\d\s]+$/" ng-required="true">
						    <span ng-show="addblogcategory.categoryUrl.$error.pattern" style="color:red">Not a Category URL!</span>
						</div>
						<div class="form-group col-md-6">
							<label>Category Image</label>
							<input type="file" class="form-control"   file-input="files" name="photo" id="photo">
							<span style="color:red">upload only(214 * 221)</span>
<!-- 							<div id="validimage" name="validimage" style="color: red"><label>Not Image Format!!</label></div> -->
						</div>
						<div class="form-group" >
						   <div class="input-group" >
						  		<label>&nbsp;&nbsp;</label>
						  		<label>&nbsp;&nbsp;</label>
						  		<label>&nbsp;&nbsp;</label>
						  		<label>&nbsp;&nbsp;</label>
						  		<input type="hidden" class="form-control"  >
						   </div>

						</div>
					</div>
					</section>
					<section>
						<div class="modal-footer">
							<a href="#" data-dismiss="modal" class="btn btn-danger">Close</a>
							<button ng-click="addblogcategory.$valid && addBlogCategory()" ng-disabled="addblogcategory.$invalid" class="btn btn-primary">Save</button>
						</div>
					</section>
					</form>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dalog -->
		</div>
		<div class="modal" id="editmodal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" id="editbtnclose" class="close" data-dismiss="modal"
							aria-hidden="true">x</button>
						<h4 class="modal-title">Edit BlogCategory</h4>
					</div>
					 	 <form method="post" theme="simple" name="editblogcategory">
					 	 <section>
						<div class="modal-body" ng-repeat="blogcate in blogcategory">
							<input type="hidden" value="{{blogcate.blogCategoryId}}" name="editBlogCategoryId" id="editBlogCategoryId">
						    <div class="form-group col-md-12">
								<label>Category Name<span style="color:red">*</span></label>
								<input type="text" class="form-control"  name="editBlogCategoryName"  id="editBlogCategoryName"  value="{{blogcate.blogCategoryName}}" ng-required="true" >
							    <span ng-show="editblogcategory.editBlogCategoryName.$error.pattern">Not a Category Name!</span>
							</div>
							 <div class="form-group col-md-12">
								<label>Category Content<span style="color:red">*</span></label>
								<input type="text" class="form-control"  name="editBlogCategoryContent"  id="editBlogCategoryContent"  value="{{blogcate.blogCategoryContent}}" ng-required="true" >
							    <span ng-show="editblogcategory.editBlogCategoryContent.$error.pattern">Not a Category Content!</span>
							</div>
							 <div class="form-group col-md-12">
								<label>Category URL<span style="color:red">*</span></label>
								<input type="text" class="form-control"  name="editBlogCategoryUrl"  id="editBlogCategoryUrl"  value="{{blogcate.blogCategoryUrl}}" ng-required="true" >
							    <span ng-show="editblogcategory.editBlogCategoryUrl.$error.pattern">Not a Category URL!</span>
							</div>
							
							 <div class="form-group col-md-12">
								<label for="child_included_rate">Category Image</label>
								<!-- <input type="file" class="form-control"   file-input="file" name="photo" id="photo"> -->
								<span style="color:red">upload only(214 * 221)</span>
<!-- 								<div id="validimage" name="validimage" style="color: red"><label>Not Image Format!!</label></div> -->
							</div>
							
							<div class="col-md-12">
					         	<img class="img-thumbnail"  ngf-select="upload($file)"  ngf-accept="'image/*'" ngf-pattern="'.jpg,.png,!.gif'" src="get-blog-category-image?blogCategoryId={{blogcate.blogCategoryId}}"  alt="User Avatar" width="100%">
					        </div> 
								
						</div>
						</section>
						<section>
							<div class="modal-footer">
								<a href="#" data-dismiss="modal" class="btn btn-danger">Close</a>
								<button ng-click="editblogcategory.$valid && editBlogCategory()" ng-disabled="editblogcategory.$invalid" class="btn btn-primary">Update</button>
							</div>  
						</section>
						</form>
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dalog -->
		</div>
		
		
  

      
      


<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>

	<script>


 
	
	var app = angular.module('myApp',['ngFileUpload']);
	app.controller('customersCtrl',['$scope',
	             					'Upload',
	            					'$timeout',
	            					'$http', function($scope,Upload,$timeout,$http) {

		
		
		$scope.settings = {
	            currentPage: 0,
	            offset: 0,
	            pageLimit: 10,
	            pageLimits: ['10', '50', '100']
	          };

		 $scope.getTotalPages = function () {
			return Math.ceil($scope.blogCategoryCount[0].count / $scope.settings.pageLimit);
		};
			
		 $scope.isCurrentPageLimit = function (value) {
		       return $scope.settings.pageLimit == value;
		 };
		 
		 $scope.previousPage = function () {
			   $scope.settings.currentPage -= 1;
			   var previousOffset = $scope.settings.currentPage * $scope.settings.pageLimit;
		       var url = "get-blog-categories?limit="+$scope.settings.pageLimit+"&offset="+previousOffset;
				$http.get(url).success(function(response) {
					//console.log(response);
					$scope.properties = response.data;
				}); 
		    }; 
		 
     		$scope.nextPage = function () {
		       $scope.settings.currentPage += 1;
		       var nextOffset = $scope.settings.currentPage * $scope.settings.pageLimit;
		       var url = "get-blog-categories?limit="+$scope.settings.pageLimit+"&offset="+nextOffset;
				$http.get(url).success(function(response) {
					//console.log(response);
					$scope.properties = response.data;
				}); 
		     }; 
		       
      			$scope.all = function (total) {
		    	   $scope.settings.pageLimit = total;
			       var nextOffset = 0;
			       var url = "get-blog-categories?limit="+total+"&offset="+nextOffset;
					$http.get(url).success(function(response) {
						//console.log(response);
						$scope.properties = response.data;
					}); 
			    };
			       
      			 $scope.limited = function (limit) {
			    	   $scope.settings.pageLimit = limit;
				       var nextOffset = 0;
				       var url = "get-blog-categories?limit="+limit+"&offset="+nextOffset;
						$http.get(url).success(function(response) {
							//console.log(response);
							$scope.properties = response.data;
					}); 
		       };			

		    
		
	       	$timeout(function(){
	      	    // $scope.progressbar.complete();
	            $scope.show = true;
	            $("#pre-loader").css("display","none");
	        }, 2000);

			 
			
			$scope.unread = function() {
			//var notifiurl = "http://localhost:8085/collaborative-workflow/unreadnotifications.action";
			var notifiurl = "unreadnotifications.action";
			$http.get(notifiurl).success(function(response) {
				$scope.latestnoti = response.data;
			});
			};
			
			
			$scope.getBlogCategories = function() {
				var url = "get-blog-categories?limit="+$scope.settings.pageLimit+"&offset="+$scope.settings.offset;
				$http.get(url).success(function(response) {
				    console.log(response);
					$scope.blogcategories = response.data;
		
				});
				
			};
			
			$scope.getBlogCategory = function(rowid) {
				
				
				var url = "get-blog-category?blogCategoryId="+rowid;
				$http.get(url).success(function(response) {
				    console.log(response);
					$scope.blogcategory = response.data;
		
				});
				
			};
			
			$scope.getEscapeHtml =  function(unsafe){
				
				 return unsafe
		         .replace(/&/g, "and")
		         // .replace(/and/g, "&")
		         //.replace(/</g, "&lt;")
		         //.replace(/>/g, "&gt;")
		         //.replace(/'/g, "&#039;")
		         //.replace(/"/g, "'")
		         //.replace(/percent/g, "%");
				  .replace(/%/g, "percent");
		        
				
			};
			var spinner = $('#loadertwo');
			
			$scope.addBlogCategory = function(){
				
				 var file = $scope.files;
				 console.log(file); 
				// var file = this.files[0];
				 var fileType = file["type"];
				 var ValidImageTypes = ["image/gif", "image/jpeg", "image/png"];
				 if ($.inArray(fileType, ValidImageTypes) < 0) {
					     // invalid file type code goes here.
					     alert('image invalid')
					}
					else{
						var content = $scope.getEscapeHtml($('#categoryContent').val());
						//alert('valid image')
						var fdata = "blogCategoryName=" + $('#categoryName').val()
							+ "&blogCategoryContent=" + content
							+"&blogCategoryUrl="+ $('#categoryUrl').val();
						spinner.show();
						$http(
								{
									method : 'POST',
									data : fdata,
									headers : {
										'Content-Type' : 'application/x-www-form-urlencoded'
									},
									url : 'add-blog-category'
								}).then(function successCallback(response) {
									$scope.addedcategory = response.data;
									   setTimeout(function(){ spinner.hide(); 
										  }, 1000);
							        
									var blogCategoryId = $scope.addedcategory.data[0].blogCategoryId;
									 Upload.upload({
							                url: 'add-blog-category-image',
							                enableProgress: true, 
							                data: {myFile: file, 'blogCategoryId': blogCategoryId} 
							            }).then(function (resp) {
							            	//alert("added successfully")
							            	   setTimeout(function(){ spinner.hide(); 
			                                }, 1000);
							            	window.location.reload(); 
							            });	
						        
						}, function errorCallback(response) {
							   setTimeout(function(){ spinner.hide(); 
								  }, 1000);
							
						});
				}
			};
			
			$scope.editBlogCategory = function(){
				
				/* var file = $scope.file;
				alert(file);
				 console.log(file); 
				 var fileType = file["type"];
				 var ValidImageTypes = ["image/gif", "image/jpeg", "image/png"];
				 if ($.inArray(fileType, ValidImageTypes) < 0) {
					     // invalid file type code goes here.
					     alert('image invalid')
					} */
					//else{
						
						var editContent = $scope.getEscapeHtml($('#editBlogCategoryContent').val() );
						
						var fdata = "blogCategoryId=" + $('#editBlogCategoryId').val()
						+ "&blogCategoryName=" + $('#editBlogCategoryName').val()
						+ "&blogCategoryContent=" + editContent
						+"&blogCategoryUrl="+ $('#editBlogCategoryUrl').val();
						spinner.show();
						$http(
								{
									method : 'POST',
									data : fdata,
									headers : {
										'Content-Type' : 'application/x-www-form-urlencoded'
									},
									url : 'edit-blog-catergory'
								}).then(function successCallback(response) {
									   setTimeout(function(){ spinner.hide(); 
										  }, 1000);
									window.location.reload(); 
									/* Upload.upload({
						                url: 'edit-blog-catergory-image',
						                enableProgress: true, 
						                data: {myFile: file, 'blogCategoryId': $('#editBlogCategoryId').val()} 
						            }).then(function (resp) {
						            	window.location.reload(); 
						            	
						            }); */
									
									
						}, function errorCallback(response) {
							   setTimeout(function(){ spinner.hide(); 
								  }, 1000);
						});
					//}
			};
			
			$scope.upload = function (file) {
	 			
		        	//var accommodationId = document.getElementById("editPropertyAccommodationId").value;
		        	
		            Upload.upload({
		                url: 'edit-blog-catergory-image',
		                enableProgress: true, 
		                data: {myFile: file, 'blogCategoryId': $('#editBlogCategoryId').val()} 
		            }).then(function (resp) {
		            	//window.location.reload();
		            	//$scope.getUserPhotos();
		               // console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
		                //$scope.getPropertyPhotos();
		            });
		           
		            
		     };
		     
		     $scope.deleteBlogCategory=function(rowid){
					var fdata = "&blogCategoryId="+ rowid
					var deleteLocationCheck=confirm("Do you want to delete the property?");
					if(deleteLocationCheck){
						$http(
								{
									method : 'POST',
									data : fdata,
									headers : {
										'Content-Type' : 'application/x-www-form-urlencoded'
									},
									url: 'delete-blog-category'
								}).then(function successCallback(response) {
									window.location.reload();
								   }, function errorCallback(response) {
									
									
								});
					}
					else{
						
					}
					
				};			
            
	    		
			
			
			$scope.getBlogCategoryCount = function() {

				var url = "get-blog-category-count";
				$http.get(url).success(function(response) {
				    //console.log(response);
					$scope.blogCategoryCount = response.data;
		            //console.log($scope.accommodationRoomsCount.data[0].size);
				});
			};
			
			$scope.getBlogCategoryCount();
			$scope.getBlogCategories();
			
	}]);


	app.directive('fileInput', ['$parse', function ($parse) {
	    return {
	        restrict: 'A',
	        link: function (scope, element, attributes) {
	            element.bind('change', function () {
	                $parse(attributes.fileInput)
	                .assign(scope,element[0].files[0])
	                scope.$apply()
	            });
	        }
	    };
	}]);		
		   
		        

		
	</script>
			
	<link rel="stylesheet" href="dist/css/dataTables.bootstrap.css">
	<script src="js1/upload/ng-file-upload-shim.min.js"></script>
	<script src="js1/upload/ng-file-upload.min.js"></script>
	 <script src="js1/jquery.dataTables.min.js" type="text/javascript"></script>
	 <script src="js1/dataTables.bootstrap.min.js" type="text/javascript"></script>
						
