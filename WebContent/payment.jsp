<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="s" uri="/struts-tags"%>

    <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Invoice
        <small>#007612</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Subscription</a></li>
        <li class="active">Invoice</li>
      </ol>
    </section>


    <!-- Main content -->
    <section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-globe"></i> IIKSHANA
            <small class="pull-right">Date: {{date | date:'yyyy-MM-dd'}}</small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          From
          <address>
            <strong>IIKSHANA</strong><br>
            795 Folsom Ave, Suite 600<br>
            San Francisco, CA 94107<br>
            Phone: (804) 123-5432<br>
            Email: info@IIKSHANA.com
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          To
          <address>
            <strong><s:property value="user.userName" /> </strong><br>
            Email: <s:property value="user.emailid" />
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          <b>Invoice #007612</b><br>
          <br>
          <b>Order ID:</b> 4F3S8J<br>
          <b>Payment Due:</b> {{date | date:'yyyy-MM-dd'}}<br>
          <b>Account:</b> 968-34567
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped">
            <thead>
            <tr>
              <th>Name</th>
              <th>Package</th>
              <th>Months</th>
              <th>Rate</th>
              <th>Subtotal</th>
            </tr>
            </thead>
            <tbody>
            <tr>
              <td>{{subscription[0].familymember}}</td>
              <td>{{subscription[0].packagename}}</td>
              <td>{{subscription[0].months}}</td>
              <td>{{subscription[0].ratepermonth}}</td>
              <td>{{subscription[0].totalamount}}</td>
            </tr>
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-6">
          <p class="lead">Payment Methods:</p>
          <img src="dist/img/credit/visa.png" alt="Visa">
          <img src="dist/img/credit/mastercard.png" alt="Mastercard">
          <img src="dist/img/credit/american-express.png" alt="American Express">
          <img src="dist/img/credit/paypal2.png" alt="Paypal">

          <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
            Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya handango imeem plugg
            dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra.
          </p>
        </div>
        <!-- /.col -->
        <div class="col-xs-6">
          <p class="lead">Amount Due {{date | date:'yyyy-MM-dd'}}</p>

          <div class="table-responsive">
            <table class="table">
              <tr>
                <th style="width:50%">Subtotal:</th>
                <td>{{subscription[0].totalamount}}</td>
              </tr>
              <tr>
                <th>Tax (9.3%)</th>
                <td>{{ (subscription[0].totalamount * 9.3)/100}}</td>
              </tr>
              
              <tr>
                <th>Total:</th>
                <td>{{subscription[0].totalamount + (subscription[0].totalamount * 9.3/100)}}</td>
              </tr>
            </table>
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-xs-12">
          <a href="invoice-print.html" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a>
          <button type="button" class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Submit Payment
          </button>
          <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
            <i class="fa fa-download"></i> Generate PDF
          </button>
        </div>
      </div>
    </section>
    <!-- /.content -->
    <div class="clearfix"></div>
  </div>
  <!-- /.content-wrapper -->
       
 <s:hidden name="subscriptionid" value="%{subscriptionid}"></s:hidden>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>
        
        <script>
        var app = angular.module('myApp',['ngProgress']);
            	app
    			.controller(
    					'customersCtrl',
    					function($scope, $http,	$timeout,ngProgressFactory) {

    						//$scope.progressbar = ngProgressFactory.createInstance();
    						//$scope.progressbar.setColor("green");
    						//$scope.progressbar.start();
    				       	$timeout(function(){
    				      	     //$scope.progressbar.complete();
    				            $scope.show = true;
    				            $("#pre-loader").css("display","none");
    				        }, 2000);
    				        
    						 var taskurl = "gettasksbyuserlatest.action";
    						 
    							$http.get(taskurl)
    							.success(function (response) {$scope.latesttasks = response.data;});
    							
    							var notifiurl = "unreadnotifications.action";
    							$http.get(notifiurl)
    							.success(function (response) {$scope.latestnoti = response.data;});


    							 $scope.date = new Date();
    							 
    							 
    							 $scope.getSubscription=function(){
    								 var notifiurl = "subscription.action?subscriptionid="+$("#subscriptionid").val();
    	    							$http.get(notifiurl)
    	    							.success(function (response) {$scope.subscription = response.data;});
    							 };

    							
    							

    					
    								 $scope.getSubscription();


    								
    					});
        </script>
        <script>
       
        function cancelname()
        {
            document.getElementById("closename").click();

            }
        function cancelpassword()
        {
            document.getElementById("passwordclose").click();

            }
        
		function validate()
		{
			var ret= false;
			//alert($("#userName").val());
			if( $("#userName").val().trim()=="" )
				{
					$("#userName").css("border-color","red");
					$("#userName").attr("placeholder", "User name is required");
					ret= false;
					//alert(ret);
				}
			else
				{
					$("#userName").css("border-color","");
					ret= true;
					//alert(ret);
				}
			
				return ret;
		}
		function fileBrowse()
		{
			document.getElementById("myFile").click();
		}
		
		function fileSubmit()
		{
			document.frmProfileUpdate.submit();
		}
        
		
        </script>
       