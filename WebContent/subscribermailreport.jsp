<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="s" uri="/struts-tags"%>

   <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <section class="content-header">
          <h1>
            Subscriber Mail Report
            <!--<small>Subscription Expiry on {{property[0].subscription[0].expirydate}}</small>-->
          </h1>
          <ol class="breadcrumb">
            <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Report</a></li>
            <li class="active">Subscriber Mail Report</li>
          </ol>
        </section>
 		
 		<!-- Main content -->
    <section class="content">
    <div class="box box-default color-palette-box">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-tag"></i> Search &amp; Filter</h3>
        </div>
        <div class="box-body">
        <form>
          <div class="row">
          <div class="col-sm-2 col-md-3">
           <div class="form-group">
                <label>From Date:</label>

                <div class="input-group date">
                <label class="input-group-addon btn" for="fromDate">
					<span class="fa fa-calendar"></span>
				</label>
                  <input type="text" class="form-control pull-right" id="fromDate" name="fromDate" value="" onkeypress="return false;" autocomplete="off">
                </div>
              </div>
              
              </div>
              <div class="col-sm-2 col-md-3">
           <div class="form-group">
                <label>To Date:</label>

                <div class="input-group date">
                  <label class="input-group-addon btn" for="toDate">
					<span class="fa fa-calendar"></span>
				</label>
                  <input type="text" class="form-control pull-right" id="toDate" name="toDate" value="" onkeypress="return false;" autocomplete="off">
                </div>
                <!-- /.input group -->
              </div>
              
              </div>
              
              <div class="col-sm-2 col-md-2">
	              <div class="form-group">
	<!--               <label>Search</label> -->
					<label>&nbsp;</label>
	              	<button type="submit" ng-click="getSubscriberMailList()" class="btn btn-danger pull-right btn-block btn-sm">Search</button>
	              	</div>
              </div>
             
          </div>
          
        
          <!-- /.row -->
          </form>
        </div>
        <!-- /.box-body -->
      </div>
      
      
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Results</h3>

              
            </div>
            <div  class="box-body table-responsive no-padding" style="height:300px;"  ng-if="subscribermaillist.length==0">
            	<table class="table table-hover">
               <tr>
                	<td  colspan="8">No Records found
                	</td>
                </tr>
                </table>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding" style="height:300px;" ng-if="subscribermaillist.length>0">
              <table class="table table-hover">
                <tr>
                  <th style="overflow:hidden;white-space:nowrap;">Serial No</th>
                  <th style="overflow:hidden;white-space:nowrap;">Subscriber Mail</th>
                  <th style="overflow:hidden;white-space:nowrap;">Subscription Date</th>
                </tr>
                <tr ng-repeat="mail in subscribermaillist">
                  <td>{{mail.serialNo}}</td>
                  <td>{{mail.subscriberMailId}}</td>
                  <td>{{mail.subscriberDate}}</td>
                  
                </tr>
                
             </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
      
    </section>
 		
 		
</div>
    
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>
<!-- csv download script start -->
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.15/angular-sanitize.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ng-csv/0.3.6/ng-csv.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.0rc1/angular-route.min.js"></script>
<!-- csv download script end -->
	 <script src="js/jquery-2.1.1.js" type="text/javascript"></script>

	<script>
	var app = angular.module('myApp', ['ngProgress','ngSanitize', 'ngCsv']);
	app.controller('customersCtrl', function($scope, $http,$timeout,ngProgressFactory) {
		
		  $("#fromDate").datepicker({
		      dateFormat: 'MM d, yy',
		    
		      onSelect: function (formattedDate) {
		          var date1 = $('#fromDate').datepicker('getDate'); 
		          var date = new Date( Date.parse( date1 ) ); 
		         // date.setDate( date.getDate() + 1 );        
		          var newDate = date.toDateString(); 
		          newDate = new Date( Date.parse( newDate ) );   
		          $('#toDate').datepicker("option","minDate",newDate);
		          $timeout(function(){
		        	  document.getElementById("fromDate").style.borderColor = "LightGray";
		          });
		      }
		  });

		  $("#toDate").datepicker({
		      dateFormat: 'MM d, yy',
		   
		      onSelect: function (formattedDate) {
		          var date2 = $('#toDate').datepicker('getDate'); 
		          $timeout(function(){
		        	  document.getElementById("toDate").style.borderColor = "LightGray";
		          });
		      }
		  }); 
		
		
		 $scope.getSubscriberMailList = function(){

				
				var fdata = "strFromDate=" + $('#fromDate').val()+ "&strToDate=" + $('#toDate').val();
				
				
				if(document.getElementById('fromDate').value==""){
		 			 document.getElementById('fromDate').focus();
		 			 document.getElementById("fromDate").style.borderColor = "red";
		 		 }else if(document.getElementById('toDate').value==""){
		 			 document.getElementById('toDate').focus();
		 			document.getElementById("toDate").style.borderColor = "red";
		 		 }else if(document.getElementById('fromDate').value!="" && document.getElementById('toDate').value!=""){
	    			    		
					$http(
							{
								method : 'POST',
								data : fdata,
								headers : {
									'Content-Type' : 'application/x-www-form-urlencoded'
								},
								url : 'report-subscriber-mail'
							}).success(function(response) {
								
								$scope.subscribermaillist = response.data;
								
							});
		 		 }
			
				
			};
			
						
			
			
		
	});
		</script>
		
		<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
		  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
		  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		  <script src="plugins/datepicker/bootstrap-datepicker.js"></script>
		  
		  <script>
 
//Date picker

  

  </script>
