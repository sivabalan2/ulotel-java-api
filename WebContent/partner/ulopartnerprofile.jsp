<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
          <div class="content-wrapper">
       
            <div class="row">
              <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 grid-margin stretch-card">
                <div class="card card-statistics">
                  <div class="card-body pb-0">
                   <!-- Form For User Info Begins -->
                <form>
  <div class="form-row">
  <div class="form-group col-md-12"><h5 class="text-center">Your Profile Information</h5></div>
    <div class="form-group col-md-4">
     <input type="hidden"  class="form-control" name="userId" id="userId" value="{{user[0].userId}}">
      <label for="userName">Name</label>
      <input type="text" class="form-control" id="userName" value="{{user[0].userName}}" placeholder="Name">
    </div>
   <div class="form-group col-md-4">
      <label for="userName">Email</label>
      <input type="email" class="form-control" id="userEmail" value="{{user[0].emailId}}" placeholder="Email" disabled>
    </div>
      <div class="form-group col-md-4">
      <label for="userName">Mobile</label>
      <input type="text" class="form-control" id="phone" value="{{user[0].phone}}" placeholder="Mobile">
    </div>
  </div>
  <div class="form-group col-md-4">
    <label for="inputAddress">Hotel Address</label>
    <input type="text" class="form-control" value="{{user[0].address1}}" id="address1" placeholder="HotelAddress"></input>
  </div> 
  <div class="form-group col-md-4">
    <label for="inputAddress">Hotel Address</label>
    <input type="text" class="form-control" value="{{user[0].address2}}" id="address2" placeholder="HotelAddress"></input>
  </div> 

  <div class="form-row">
    <div class="form-group col-md-12">
        <button class="btn btn-warning" data-toggle="collapse" href="#collapseOne">
        Change Password
      </button>
    <button type="submit" ng-click="editFamilyUser()" class="btn btn-dark">Save Changes</button>
    </div>
  </div>

  
</form>
<div id="accordion">

  <div class="card">
    <div id="collapseOne" class="collapse" data-parent="#accordion">
      <div class="card-body">
        <form>
    <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">New Password</label>
    <div class="col-sm-10">
      <input type="password" class="form-control" id="newPassword" placeholder="Password">
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Confirm Password</label>
    <div class="col-sm-10">
      <input type="password" class="form-control" id="confirmPassword" placeholder="Password">
    </div>
  </div>
   <div class="form-group row col-sm-12">
   <div id="changepasswordalert"></div>
     <button type="button" ng-click="changepassword()" class="btn btn-dark float-left">Save Password </button>
  </div>
</form>
      </div>
    </div>
  </div>
</div>
                   <!-- Form For User Info Ends <button type="submit" class="btn btn-warning">Change Password</button>-->
                </div>
            
                </div>
              </div>

             <!-- Reports section -->
    
             <!-- Reports section ends -->

      
       
         
            </div>
          </div>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ngprogress/1.1.3/ngProgress.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>
<script>
 var app = angular.module('myApp', ['ngProgress','ui.bootstrap']);
 app.controller('customersCtrl', function($scope, $http,$timeout,ngProgressFactory) {
	
 
          $scope.getUserProfile = function() {
              var url = "get-user-profile";
              $http.get(url).success(function(response) {
                  $scope.user = response.data;
             
              });
          };
          
          $scope.editFamilyUser=function(){
				var fdata ="userId="+$('#userId').val()
				+"&userName="+$('#userName').val()
				+"&phone="+$('#phone').val()
				+"&address1="+$('#address1').val()
				+"&address2="+$('#address2').val()
				+"&emailId="+$('#userEmail').val();
				
				$http(
				{
					method : 'POST',
					data : fdata,
					headers : {
						'Content-Type' : 'application/x-www-form-urlencoded'
					},
					url : 'edit-family-user'
				}).then(function successCallback(response) {
					$("#userprofilealert").hide();
					   var alertmsg = ' <div class="alert" style="color:green;text-align:center;">User profile update successfully</div>';
			            $('#userprofilealert').html(alertmsg);
			            $("#userprofilealert").fadeTo(2000, 500).slideUp(500, function(){
			              
			                 });
				}, function errorCallback(response) {
					$("#userprofilealert").hide();
					   var alertmsg = ' <div class="alert" style="color:green;text-align:center;">Process failed please try again</div>';
			            $('#userprofilealert').html(alertmsg);
			            $("#userprofilealert").fadeTo(2000, 500).slideUp(500, function(){
			              
			                 });
					
					// called asynchronously if an error occurs
					// or server returns response with an error status.
				});
			};
			
			$scope.changepassword=function(){
				var newpass = document.getElementById("newPassword").value;
		    	var confirmpass = document.getElementById("confirmPassword").value;
		    	var passpattern = /(?=.*\d)(?=.*[@#$%])(?=.*[a-z])(?=.*[A-Z]).{8,}/;
		    	
		    	if(newpass=="") {
		    		$("#changepasswordalert").hide();
					   var alertmsg = ' <div class="alert" style="color:green;text-align:center;">Please enter the new password</div>';
			            $('#changepasswordalert').html(alertmsg);
			            $("#changepasswordalert").fadeTo(2000, 500).slideUp(500, function(){
			              
			                 });
			        return false;
			      }
		    	
		    	if(confirmpass==""){
		    		$("#changepasswordalert").hide();
					   var alertmsg = ' <div class="alert" style="color:green;text-align:center;">Please enter the confirm password</div>';
			            $('#changepasswordalert').html(alertmsg);
			            $("#changepasswordalert").fadeTo(2000, 500).slideUp(500, function(){
			              
			                 });
		    		return false;
		    	}
		    	
		    	 if(!passpattern.test(newpass)) {

		    		 $("#changepasswordalert").hide();
					   var alertmsg = ' <div class="alert" style="color:green;text-align:center;">Password does not match</div>';
			            $('#changepasswordalert').html(alertmsg);
			            $("#changepasswordalert").fadeTo(2000, 500).slideUp(500, function(){
			              
			                 });
		    		 return false;
		    	  }
		        
		    	 if(newpass != confirmpass) {
		 	        alert("password doesnot match!");
		 	        return false;
		 	      }
		    	 
				//alert ("userId="+$('#userid').val());
				var fdata = "userId="+$('#userId').val()
				+"&newPassword="+$('#newPassword').val()					
				+"&confirmPassword="+$('#confirmPassword').val();
				
				$http(
				{
					method : 'POST',
					data : fdata,
					headers : {
						'Content-Type' : 'application/x-www-form-urlencoded'
					},
					url : 'changeuserpassword'
			
					
				});  
				$("#userprofilealert").hide();
				   var alertmsg = ' <div class="alert" style="color:green;text-align:center;">Password updated successfully</div>';
		            $('#userprofilealert').html(alertmsg);
		            $("#userprofilealert").fadeTo(2000, 500).slideUp(500, function(){
		              
		                 });
		            $('#addmodal').modal('toggle');
			};
			
			var selectPartnerPropertyId=0,partnerPropertyId=0;
	  		$scope.getUserProperty = function() {
	  			var url = "get-partner-user-property";
	  			$http.get(url).success(function(response) {
	  				$scope.userProperty = response.data;
	  				//partnerPropertyId=$scope.userProperty[0].selectedPropertyId;
	  				//$scope.getAccommodations(partnerPropertyId);
	  			});
	  			
	  	  };
	  	  
	  	$scope.selectProperty= function(rowid){
			  selectPartnerPropertyId=rowid;
			  var url="get-selected-property-id?selectedPartnerPropertyId="+selectPartnerPropertyId;
			  $http.get(url).success(function(response) {
					$scope.selectedPropertyId= response.data;
					location.reload();
				});	
		  };
	  	  
	  	  $scope.getUserProperty();
          $scope.getUserProfile(); 
				
          
 });
 </script>
 <!-- DATEPICKER CDN BEGINS -->
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/themes/pepper-grinder/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> 
 <!-- DATEPICKER CDN ENDS -->
              