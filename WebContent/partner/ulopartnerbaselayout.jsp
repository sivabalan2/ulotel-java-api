<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>ULOTEL-PARTNER</title>
    <!-- plugins:css -->
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/3.3.92/css/materialdesignicons.css" />
   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
   <link rel="stylesheet" href="assets/vendors/iconfonts/puse-icons-feather/feather.css">
     <link rel="stylesheet" href="assets/vendors/css/vendor.bundle.base.css"> 
     <link rel="stylesheet" href="assets/vendors/css/vendor.bundle.addons.css"> 
     <script src="assets/vendors/js/vendor.bundle.base.js"></script>
         <style>
    #loadertwo {
  display: none;
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  width: 100%;
  background: rgba(0,0,0,0.75) url(http://www.marcorpsa.com/ee/images/loading2.gif) no-repeat center center;
  z-index: 10000;
}
    </style>
    <!-- endinject -->
    <!-- plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="assets/css/shared/style.css">
    <!-- endinject -->
    <!-- Layout styles -->
    <link rel="stylesheet" href="assets/css/demo_2/style.css">
    <!-- End Layout styles -->
    <link rel="shortcut icon" href="assets/images/favicon.png" />
  </head>
  <body ng-app="myApp" ng-cloak ng-controller="customersCtrl">
   <div id="loadertwo"></div>
    <div class="container-scroller">
      <!-- partial:partials/_horizontal-navbar.html -->
      <nav class="navbar horizontal-layout col-lg-12 col-12 p-0">
        <div class="container d-flex flex-row nav-top">
          <div class="text-center navbar-brand-wrapper d-flex align-items-top">
            <a class="navbar-brand brand-logo" href="partner-dashboard">
              <img src="../images/logo/ulotel.png" alt="logo" /> </a>
            <a class="navbar-brand brand-logo-mini" href="partner-dashboard">
              <img src="../images/logo/ulotel.png" alt="logo" /> </a>
          </div>
          <div class="navbar-menu-wrapper d-flex align-items-center">
       
            <ul class="navbar-nav ml-auto">
               <form action="" >
              <div class="input-group search-box">
                <div class="input-group-prepend">
                <input type="hidden" value ="<%=session.getAttribute("partnerPropertyId") %>"  />
                </div>
                
                <select  name="selectPartnerPropertyId" id="selectPartnerPropertyId" ng-model="selectPartnerPropertyId" value="" ng-change="selectProperty(selectPartnerPropertyId)" class="form-control" ng-required="true">
                   		<option ng-repeat="up in userProperty" value="{{up.propertyId}}" ng-selected="up.propertyId==<%=session.getAttribute("partnerPropertyId") %>">{{up.propertyName}}</option>
                    </select>
                
              </div>
            </form>
             <%--  <li class="nav-item dropdown ml-4">
                <a class="nav-link count-indicator dropdown-toggle" id="notificationDropdown" href="#" data-toggle="dropdown">
                  <i class="mdi mdi-bell-outline"></i>
                  <span class="count bg-success">4</span>
                </a>
                <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list pb-0" aria-labelledby="notificationDropdown">
                  <a class="dropdown-item py-3 border-bottom">
                    <p class="mb-0 font-weight-medium float-left">You have 4 new bookings </p>
                    
                  </a>
                  <a class="dropdown-item preview-item py-3">
                    <div class="preview-thumbnail">
                      <i class="mdi mdi-airballoon m-auto text-primary"></i>
                    </div>
                    <div class="preview-item-content">
                      <h6 class="preview-subject font-weight-normal text-dark mb-1">1 New booking arrived for Deluxe Room</h6>
                    </div>
                  </a>
                </div>
              </li> --%>
              <li class="nav-item dropdown d-none d-xl-inline-block">
                <a class="nav-link dropdown-toggle" id="UserDropdown" href="#" data-toggle="dropdown" aria-expanded="false"><img class="img-xs rounded-circle ml-3" src="assets/images/faces/face8.png" alt="Profile image"> </a>
                <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="UserDropdown">
                  <a class="dropdown-item p-0">
                    <div class="d-flex border-bottom">
                      <div class="py-3 px-4 d-flex align-items-center justify-content-center">
                        <i class="mdi mdi-bookmark-plus-outline mr-0 text-gray"></i>
                      </div>
                      <div class="py-3 px-4 d-flex align-items-center justify-content-center border-left border-right">
                        <i class="mdi mdi-account-outline mr-0 text-gray"></i>
                      </div>
                      <div class="py-3 px-4 d-flex align-items-center justify-content-center">
                        <i class="mdi mdi-alarm-check mr-0 text-gray"></i>
                      </div>
                    </div>
                  </a>
                  <a class="dropdown-item mt-2" href="partner-profile"> Profile </a>
                  <a class="dropdown-item" href="partner-logout"> Sign Out </a>
                </div>
              </li>
            </ul>
            <button class="navbar-toggler align-self-center" type="button" data-toggle="minimize">
              <span class="mdi mdi-menu"></span>
            </button>
          </div>
        </div>
        <div class="nav-bottom">
          <div class="container">
            <ul class="nav page-navigation">
              <li class="nav-item">
                <a href="partner-dashboard" class="nav-link">
                  <i class="link-icon mdi mdi-airplay"></i>
                  <span class="menu-title">Dashboard</span>
                </a>
                  
              </li>
              <li class="nav-item">
                <a href="partner-inventory" class="nav-link">
                  <i class="link-icon mdi mdi-apple-safari"></i>
                  <span class="menu-title">Inventory Management</span>
                </a>
              </li>
              <li class="nav-item">
                <a href="#" class="nav-link">
                  <i class=" link-icon mdi mdi-chart-line"></i>
                  <span class="menu-title ">Reports Management</span>
                  <i class="menu-arrow "></i>
                </a>
                <div class="submenu ">
                  <ul class="submenu-item ">
                  <li class="nav-item ">
                     <a class="nav-link " href="partner-booking-report"> Booking Report</a>
                    </li>
                   <li class="nav-item ">
                    <a class="nav-link " href="partner-revenue-report"> Revenue Report</a>
                    </li>
                  </ul>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
        <div class="main-panel container">
         <!-- Body Layout Add Here -->
          <tiles:insertAttribute name="body" />
          <!-- Body Layout End Here  -->
          <!-- partial:partials/_footer.html -->
          <footer class="footer">
            <div class="container clearfix">
              <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright &copy; 2019 All rights reserved.</span>
              <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center"> <a href="http://www.ulohotels.com/" target="_blank">www.Ulohotels.com</a>.
              </span>
            </div>
          </footer>
          <!-- partial -->
        </div>
        <!-- main-panel ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <!-- plugins:js -->
        
       <script src="assets/vendors/js/vendor.bundle.addons.js"></script>  
    <!-- endinject -->
    <!-- Plugin js for this page-->
    <!-- End plugin js for this page-->
    <!-- inject:js -->
    <script src="assets/js/shared/off-canvas.js"></script>
    <script src="assets/js/shared/hoverable-collapse.js"></script>
    <script src="assets/js/shared/misc.js"></script>
    <script src="assets/js/shared/settings.js"></script>
    <script src="assets/js/shared/todolist.js"></script>
    <!-- endinject -->
    <!-- Custom js for this page-->
    <script src="assets/js/shared/widgets.js"></script>
    <script src="assets/js/demo_2/dashboard.js"></script>
    <script src="assets/js/demo_2/script.js"></script>
<%--     <script src="assets/js/shared/chart.js"></script> --%>
        <script src="assets/js/shared/alerts.js"></script>
    <script src="assets/js/shared/avgrund.js"></script>

    <!-- End custom js for this page-->
  </body>
</html>