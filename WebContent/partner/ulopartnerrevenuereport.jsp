<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<div class="content-wrapper">
<!-- Wrapper Section Begins -->
          <div class="row">
           <div class="col-12">
            <div class="card">
             <div class="card-body">
             <form>
  <div class="form-row">
     <div class="form-group col-md-2">
      <label for="inputState">Select Room Type</label>
      <select id="accommodationId" class="form-control">
         <option selected="selected" value="0">--All--</option>
         <option ng-repeat="acc in accommodations" value="{{acc.accommodationId}}">{{acc.accommodationType}}</option>
      </select>
    </div>
    <div class="form-group col-md-3">
      <label >Start Date</label>
      <div class="input-group" >
      <div class="input-group-prepend" >
          <label class="input-group-text" for="fromDate" id="inputGroupPrepend" ><span  class="mdi mdi-calendar"></span></label>
        </div>
      <input type="text" class="form-control" id="fromDate" ng-required="true" autocomplete="off">
      </div>
    </div>
 
    <div class="form-group col-md-3">
      <label for="checkOut">End Date</label>
      <div class="input-group">
      <div class="input-group-prepend">
          <label class="input-group-text" for="toDate" id="inputGroupPrepend"><span class="mdi mdi-calendar"></span></label>
        </div>
      <input type="text" class="form-control" id="toDate" ng-required="true" autocomplete="off">
      </div>
    </div>
   <div class="form-group col-md-2">
    <label >Export CSV</label>
     <button class="btn btn-info btn-block" ng-csv="revenuelist" filename="revenuereport.csv" csv-header="['Accommodations', 'From Date', 'To Date','No. of Nights','Total Revenue']" field-separator="," decimal-separator="."><span class="mdi mdi-file-excel"></span> Export CSV</button>
       </div>
           <div class="form-group col-md-2">
            <label >Search</label>
     <button type="submit" ng-click="getPropertyRevenueList()"  class="btn btn-dark btn-block"><span class="mdi mdi-map-search"></span> Search</button>
  </div>
       
  </div>
 </form>
    <!-- Reports shown begins here -->
       <div  class="box-body table-responsive no-padding" style="height:300px;"  ng-if="revenuelist.length==0">
            	<table class="table table-hover">
               <tr>
                	<td  colspan="8">No Records found
                	</td>
                </tr>
                </table>
            </div>
    <div class="table-responsive">
    <table class="table table-striped">
    <thead>
        <tr>
        <th>Accommodation Type</th>
      <th>Start Date</th>
      <th>End Date</th>
      <th>Total Nights</th>
      <th>Total Revenue</th>
        </tr>
    </thead>
    <tbody>
        <tr ng-repeat="revenue in revenuelist">
            <td class="filterable-cell">{{revenue.accommodationType}}</td>
            <td class="filterable-cell">{{revenue.startDate}}</td>
            <td class="filterable-cell">{{revenue.endDate}}</td>
            <td class="filterable-cell">{{revenue.nights}}</td>
            <td class="filterable-cell">{{revenue.amount}}</td>
        </tr>
    </tbody>
</table>
    </div>
    <!-- Reports shown ends here -->  
             </div>
            </div>
           </div>
          </div>
<!-- Wrapper Section Ends -->            
</div>
 <%-- <%=session.getAttribute("propertyId") %> --%>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ngprogress/1.1.3/ngProgress.js"></script>
<!-- csv download script start -->
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.15/angular-sanitize.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ng-csv/0.3.6/ng-csv.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.0rc1/angular-route.min.js"></script>
<!-- csv download script end -->
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>
<script>
 var app = angular.module('myApp', ['ngProgress','ui.bootstrap', 'ngSanitize', 'ngCsv']);
 app.controller('customersCtrl', function($scope, $http,$timeout,ngProgressFactory) {
 //Datepicker begins 
  var spinner = $('#loadertwo');
	 var showDisplayReport = $('#showDisplayReport');
    $("#fromDate").datepicker({
              dateFormat: 'MM d, yy',
            
              //minDate:  0,
              onSelect: function (formattedDate) {
                  var date1 = $('#fromDate').datepicker('getDate'); 
                  var date = new Date( Date.parse( date1 ) ); 
                  date.setDate( date.getDate() + 1 );        
                  var newDate = date.toDateString(); 
                  newDate = new Date( Date.parse( newDate ) );   
                  $('#toDate').datepicker("option","minDate",newDate);
                  $timeout(function(){
                    //scope.checkIn = formattedDate;
                  });
              }
          });
   
          $("#toDate").datepicker({
              dateFormat: 'MM d, yy',
            
              //minDate:  0,
             
              onSelect: function (formattedDate) {
                  var date2 = $('#toDate').datepicker('getDate'); 
                  $timeout(function(){
                    //scope.checkOut = formattedDate;
                  });
              }
          });

 //Datepicker Ends
     showDisplayReport.hide();
          $scope.getPropertyRevenueList = function(){
				
				var fdata = "strFromDate=" + $('#fromDate').val()+ "&strToDate=" + $('#toDate').val()+
				"&accommodationId="+$('#accommodationId').val();
				if(document.getElementById('fromDate').value==""){
		 			 document.getElementById('fromDate').focus();
		 			 document.getElementById("fromDate").style.borderColor = "red";
		 		 }else if(document.getElementById('toDate').value==""){
		 			 document.getElementById('toDate').focus();
		 			document.getElementById("toDate").style.borderColor = "red";
		 		 }else if(document.getElementById('fromDate').value!="" && document.getElementById('toDate').value!=""){
	    			    spinner.show()	
	    			    showDisplayReport.hide();
					$http(
							{
								method : 'POST',
								data : fdata,
								headers : {
									'Content-Type' : 'application/x-www-form-urlencoded'
								},
								url : 'report-partner-revenue'
							}).success(function(response) {
								spinner.hide();
								showDisplayReport.show()
								$scope.revenuelist = response.data;
								
							});
		 		 }
			};
			
 
			var selectPartnerPropertyId=0,partnerPropertyId=0;
	  		$scope.getUserProperty = function() {
	  			var url = "get-partner-user-property";
	  			$http.get(url).success(function(response) {
	  				$scope.userProperty = response.data;
	  				//partnerPropertyId=$scope.userProperty[0].selectedPropertyId;
	  				//$scope.getAccommodations(partnerPropertyId);
	  			});
	  			
	  	  };
	  	  
	  	$scope.selectProperty= function(rowid){
			  selectPartnerPropertyId=rowid;
			  var url="get-selected-property-id?selectedPartnerPropertyId="+selectPartnerPropertyId;
			  $http.get(url).success(function(response) {
					$scope.selectedPropertyId= response.data;
						location.reload(); 
				});	
			  //$scope.getAccommodations(selectPartnerPropertyId);
		  }
	  	  
	  	  
	  	  
	  	$scope.getAccommodations = function() {
			var url = "get-partner-accommodations"
			$http.get(url).success(function(response) {
			    //console.log(response);
				$scope.accommodations = response.data;
	
			});
		};
		
		$scope.getUserProperty();
		$scope.getAccommodations();	
 });
 </script>
 <!-- DATEPICKER CDN BEGINS -->
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/themes/pepper-grinder/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> 
 <!-- DATEPICKER CDN ENDS -->