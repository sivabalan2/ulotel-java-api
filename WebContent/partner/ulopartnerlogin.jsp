<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html>
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>ULO Hotels partner</title>
      <!-- Tell the browser to be responsive to screen width -->
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
      <!-- Bootstrap 3.3.5 -->
      <link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css">
      <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
      <!-- Font Awesome -->
      <link rel="icon" href="images/logo/favicon.png" type="image/gif" />
      <link rel="stylesheet" href="css/ulotel.css">
      <link rel="stylesheet" href="css/ulotelmq.css">
   </head>
   <body class="utlogin">
   
      
    <div class="containers">
    <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-2 col-xs-12 utlogheader">
    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 utlogo">
    <img src="images/utlogin/ulogo.png" alt="welcome to ulotel">
    </div>
    <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">

    </div>
    </div>
    </div>
    </div> 
    <div class="container">
    <div class="row uloform">
    
    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 loginnote">
    <img alt="ulotel" src="images/utlogin/ulotel.svg" class="ulotelcentral">
    <h1>Maximizing Revenue & value <br> for single branded budget hotel</h1>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 loginform">
  <div class="circle-tile ">
        <a href="#"><div class="circle-tile-heading "><i class="fa fa-user fa-3x"></i></div></a>
        
        <div class="circle-tile-content">
        <p>Welcome Back !</p>
       <s:form id="myForm" action="partner/partnerlogin.action" method="post" theme="simple" onsubmit ="return validate()">
 		<div class=" form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
	        <input type="text" id="username" name="username" class="form-control" required>
	        <label class="form-control-placeholder" for="username">Email</label>
	     </div>
 		<div class=" form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
	        <input type="password" id="password" name="password" class="form-control" required>
	        <label class="form-control-placeholder" for="password">Password</label>
	      </div>
      
		  <div class="row">
		   <div class="form-group col-lg-6 col-md-6 col-sm-7 col-xs-7">
		    <label class="checkbox inline ulcb" for="rememberme-0">
                  <input type="checkbox" name="rememberme" id="rememberme-0" value="Remember me">
                  Remember me
                </label>
				  </div>
				   <div class="col-lg-6 col-md-6 col-sm-5 col-xs-5">
				  <button type="submit" class="btn btn-primry btnconfirm">Sign in</button>
				  </div>
				  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 flink">
				  <a href="forgetpassword.jsp" class="">Reset Password</a>
				  </div>
				  </div>
				  </s:form>
				  <s:if test="hasActionErrors()">
					<label style="color:red">
						<s:actionerror/>
					</label>
					</s:if>
		        </div>
		      </div>
	    </div>
	    </div>
    </div>
    <div class="container">
    <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <hr class="loginline"></hr>
    </div>
    </div>
    </div>
       <div class="container">
    <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 loginicon">
    <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12 ">
    </div>
    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6 ">
    <img src="images/utlogin/ui1.svg" class="img-responsive">
    <p>Real-Time Booking</p>
    </div>
    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6">
     
    <img src="images/utlogin/ui2.svg" class="img-responsive">
     <p>Detailed Revenue Forecast</p>
    </div>
    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6">
        <img src="images/utlogin/ui3.svg" class="img-responsive">
         <p>Instant Revenue reconciliation</p>
    </div>
    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-6">
    
    <img src="images/utlogin/ui4.svg" class="img-responsive">
     <p>Comprehensive Dashboard & Reporting</p>
    </div>
    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
    
    <img src="images/utlogin/ui5.svg" class="img-responsive">
     <p>Integration With Online/Offline Partners</p>
    </div>
    <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12 ">
    </div>
    </div>
    </div>
    </div>
    <footer>
    <img src="images/utlogin/utfoot.png"  />
    </footer>
    </body>
     <!-- /.login-box -->
      <!-- jQuery 2.1.4 -->
      <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
      <!-- Bootstrap 3.3.5 -->
      <script src="bootstrap/js/bootstrap.min.js"></script>
      <script>
         function validate(){
         	
         	
         	var username = document.getElementById("username").value;
         	var password = document.getElementById("password").value;
         	var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
         	
         	if(username == "")
             {
                alert( "Please enter username!" );
                document.getElementById("username").focus(); 
                return false;
             }
         	
         	if(!username.match(mailformat))  
         	{  
         		alert("You have entered an invalid email address!");
         		return false; 
         		document.getElementById("username").focus();  
         	    
         	}  
         	
            if(password == "")
             {
                alert( "Please enter passowrd!" );
                document.getElementById("password").focus(); 
                return false;
             }
         	
         	
           /* if($('#captchashow').is(":visible"))
         	{
         	    var captcha = document.getElementById("g-recaptcha-response").value;
         		if(captcha === null || captcha.length === 0 ) {
         		alert("please check the captcha");
         			return false;
         			} 
         	
         	}
         	
         	else{
         		
         		return true;  
         	} */
         	
         	}
         
         
         
         
         
          /* function Captcha(){
           
           
          canvas = document.getElementById("myCanvas");
          ctx=canvas.getContext("2d");
          ctx.font="30px Comic Sans MS";
          ctx.fillStyle = "";
         
         ctx.textAlign = "center";
         ctx.clearRect(0, 0, canvas.width, canvas.height);
         
                              var alpha = new Array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z');
                              var i;
                              for (i=0;i<4;i++){
                                var a = alpha[Math.floor(Math.random() * alpha.length)];
                                var b = alpha[Math.floor(Math.random() * alpha.length)];
                                var c = alpha[Math.floor(Math.random() * alpha.length)];
                                var d = alpha[Math.floor(Math.random() * alpha.length)];
                                var e = alpha[Math.floor(Math.random() * alpha.length)];
                                var f = alpha[Math.floor(Math.random() * alpha.length)];
                                var g = alpha[Math.floor(Math.random() * alpha.length)];
                               }
                             
         code = a + ' ' + b + ' ' + ' ' + c + ' ' + d + ' ' + e;
             
         ctx.fillText(code, canvas.width/2, canvas.height/2);
                     }
                           function ValidCaptcha(){
         
                               var string1 =code.replace(/ /g,'');
                               var string2 = (document.getElementById('txtInput').value).replace(/ /g,'');
         
                               if (string1 == string2){
                                 
                                 return true;
                               }
                               else{        
                                 return false;
                               }
                           }
                           function removeSpaces(string){
                             return string.split(' ').join('');
                           }
         
         
         
                           $(document).ready(function () {
                         	  Captcha();
                          }); */
         
      </script>  
</html>