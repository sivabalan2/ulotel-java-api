<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
          <div class="content-wrapper">
            <div class="row">
              <div class="col-12 grid-margin d-none d-lg-block">
                <div class="intro-banner">
                  <div class="banner-image">
                    <img src="assets/images/dashboard/banner_img.png" alt="banner image">
                  </div>
                  <div class="content-area">
                    <h3 class="mb-0">Welcome back</h3>
                    <p class="mb-0">Need anything more to know more? Feel free to contact us at any point.</p>
                  </div>
                  <a href="mailto:revenue@ulohotels.com" target="_top" class="btn btn-light">revenue@ulohotels.com</a>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-xl-3 col-lg-3 col-md-3 col-sm-4 grid-margin stretch-card">
                <div class="card card-statistics">
                  <div class="card-body pb-0">
                    <p class="text-muted text-center">Today'Check In</p>
                    <div class=" align-items-center">
                        <h3 class="text-success text-center font-weight-semibold ml-2"><i class="mdi mdi-account-check text-info"></i>  {{partnerTodayDetails[0].arrivals}}</h3>
                    </div>
        
                  </div>
            
                </div>
              </div>
              <div class="col-xl-3 col-lg-3 col-md-3 col-sm-4 grid-margin stretch-card">
                <div class="card card-statistics">
                  <div class="card-body pb-0">
                    <p class="text-muted text-center">Today'Check Out</p>
                    <div class="align-items-center">
                      <h3 class="text-danger text-center font-weight-semibold ml-2"><i class="mdi mdi-account-remove text-info"></i>  {{partnerTodayDetails[0].departures}}</h3>
                    </div>
                 
                  </div>
                 
                </div>
              </div>
              <div class="col-xl-3 col-lg-3 col-md-3 col-sm-4 grid-margin stretch-card">
                <div class="card card-statistics">
                  <div class="card-body pb-0">
                    <p class="text-muted text-center">Today'Room Occupied</p>
                    <div class=" align-items-center">
                     
                      <h3 class="text-success text-center font-weight-semibold ml-2"><i class="mdi mdi-scale-bathroom text-info"></i> {{partnerTodayDetails[0].occupancy}}</h3>
                    </div>
                    
                  </div>
                 
                </div>
              </div>
                  <div class="col-xl-3 col-lg-3 col-md-3 col-sm-4 grid-margin stretch-card">
                <div class="card card-statistics">
                  <div class="card-body pb-0">
                    <p class="text-muted text-center">Today' Revenue</p>
                    <div class=" align-items-center">
                        <h3 class="text-danger text-center font-weight-semibold "><i class="mdi mdi-currency-inr text-info"></i>{{partnerTodayDetails[0].revenue | number:0}}</h3>
                    </div>
        
                  </div>
            
                </div>
              </div>
             <!-- Reports section -->
                  <div class="col-md-12 grid-margin ">
                  
                <div class="card">
                <div class="card-header header-sm">
                    <div class="d-flex align-items-center">
                      <h5 class="card-title">Today's Occupancy Guest Information</h5>
                
                    </div>
                  </div>
                  <div class="card-body" >
                   <div class="table-responsive">
                    <table  class="table table-striped w-100">
                      <thead>
                        <tr>
                          <th>Booking Id</th>
                          <th>Guest Name</th>
                          <th>Rooms Categories</th>
                          <th>Rooms Booked</th>
                          <th>Amount Paid</th>
                          <th>Hotel Pay</th>
                          <th>Status</th>
                       
                        </tr>
                      </thead>
                      <tbody>
                        <tr role="row" class="odd" ng-repeat="bc in bookdetail | filter:search track by $index ">
                          <td>{{bc.DT_RowId}}</td>
                          <td>{{bc.guestName}}</td>
                          <td>{{bc.accommodationType}}</td>
                          <td>{{bc.rooms}}</td>
                          <td>{{bc.advanceAmount}}</td>
                          <td>{{bc.dueAmount}}</td>
                          <td >{{bc.statusName}}</td>
                          </div>
                        </tr>                      
                      </tbody>
                    </table>
                   </div>
                  </div>
                </div>
              </div>
             <!-- Reports section ends -->

         
              <div class="col-sm-6 col-md-6 col-lg-6  grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                     <h4 class="card-title font-weight-medium mb-3">Revenue Till Date</h4>
                    </div>
                      <div class="col-lg-5 col-md-12 col-sm-12 p-1">
                          <div class="input-group" >
      <div class="input-group-prepend" >
          <label class="input-group-text" for="checkIn" id="inputGroupPrepend" ><span  class="mdi mdi-calendar"></span></label>
        </div>
      <input type="text" class="form-control" id="checkIn" ng-required="true" autocomplete="off">
      </div>
                      </div>
                      <div class="col-lg-5 col-md-12 col-sm-12 p-1">
                        <div class="input-group" >
      <div class="input-group-prepend" >
          <label class="input-group-text" for="checkOut" id="inputGroupPrepend" ><span  class="mdi mdi-calendar"></span></label>
        </div>
      <input type="text" class="form-control" id="checkOut" ng-required="true" autocomplete="off">
      </div>
                      </div>
                      <div class="col-lg-2 col-md-12 col-sm-12 p-1">
                      <span class="input-group-addon input-group-append border-left pb-2 text-center">
                            <span class="btn btn-sm btn-dark" ng-click="getSearchPropertyRevenue()">Search</span>
                          </span>
                      </div>
                    <div class="col-md-12">
                    <div class="table-responsive">
                      <table class="table table-bordered mt-1 text-center">
                        <thead>
                          <tr class="bg-light rounded">
                        
                            <th>Room Nights</th>
                            <th>Total Revenue</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                       
                            <td>{{revenue[0].totalRooms}}</td>
                            <td><i class="mdi mdi-currency-inr text-info"></i>{{revenue[0].totalAmount | number:0}}</td>
                          </tr>
                  
                       
                        </tbody>
                      </table>
                    </div>
                     </div>
                    </div>
                  </div>
                </div>
              </div>
               <div class="col-sm-6 col-md-6 col-lg-6 grid-margin stretch-card">
                <div class="card">
                <div class="card-head  text-center">
                    <h4 class="card" id="chartDate"></h4>
                  </div>
                  <div class="card-body">
                    <h4 class="card-title">Rooms Sold</h4>
                    <canvas id="pieChart" style="height:230px"></canvas>
                  </div>
                  </div>
                  </div>
       
         
            </div>
          </div>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ngprogress/1.1.3/ngProgress.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>
<script>
 var app = angular.module('myApp', ['ngProgress','ui.bootstrap']);
 app.controller('customersCtrl', function($scope, $http,$timeout,ngProgressFactory) {
	 var spinner = $('#loadertwo');
	 
	 //Datepicker begins 
    $("#checkIn").datepicker({
              dateFormat: 'MM d, yy',
              maxDate:0,
              onSelect: function (formattedDate) {
                  var date1 = $('#checkIn').datepicker('getDate'); 
                  var date = new Date( Date.parse( date1 ) ); 
                  date.setDate( date.getDate() + 1 );        
                  var newDate = date.toDateString(); 
                  newDate = new Date( Date.parse( newDate ) );   
                  $('#checkOut').datepicker("option","minDate",newDate);
                  $timeout(function(){
                    //scope.checkIn = formattedDate;
                  });
              }
          });
   
          $("#checkOut").datepicker({
              dateFormat: 'MM d, yy',
            
            //  minDate:  0,
             maxDate:0,
              onSelect: function (formattedDate) {
                  var date2 = $('#checkOut').datepicker('getDate'); 
                  $timeout(function(){
                    //scope.checkOut = formattedDate;
                  });
              }
          });

 //Datepicker Ends
 
          var selectPartnerPropertyId=0,partnerPropertyId=0;
  		$scope.getUserProperty = function() {
  			var url = "get-partner-user-property";
  			$http.get(url).success(function(response) {
  				$scope.userProperty = response.data;
  				//partnerPropertyId=$scope.userProperty[0].selectedPropertyId;
  				//alert(partnerPropertyId)
  				//$scope.getArrival(partnerPropertyId);
  				  //$scope.getAllBookings(partnerPropertyId);
  				  //$scope.getPropertyRevenue(partnerPropertyId);
  			});
  	  };
  	  
  	  $scope.selectProperty= function(rowid){
  		 // alert(rowid)
  		  selectPartnerPropertyId=rowid;
  		  var url="get-selected-property-id?selectedPartnerPropertyId="+selectPartnerPropertyId;
  		  $http.get(url).success(function(response) {
    				$scope.selectedPropertyId= response.data;

    			});	
  		 // $scope.getArrival(rowid);
  		 // $scope.getAllBookings(rowid);
  		 // $scope.getPropertyRevenue(rowid);
  		
  			pieCharts();
  		location.reload(); 
  		
  	  }
  	  
  	 
  	  
            $scope.getArrival = function(rowid) {
            	//alert("row"+rowid)
     			var url = 'get-partner-checkin-checkout'
     			$http.get(url).success(function(response) {
     				$scope.partnerTodayDetails= response.data;
   
     			});
     		};
     		
     		$scope.getAllBookings = function(rowid) {
     			var url = "get-partner-allbookings"
     			$http.get(url).success(function(response) {
     				$scope.bookdetail= response.data;
  		});
  	};
  	
  	$scope.getSearchPropertyRevenue = function(){
		var startDate=document.getElementById('checkIn').value;
      	var endDate=document.getElementById('checkOut').value;
			var url = "get-partner-propertyrevenue?strStartDate="+startDate+"&strEndDate="+endDate;
			$http.get(url).success(function(response) {
				
			$scope.revenue= response.data;
				
					});
	}
  	
  	$scope.getPropertyRevenue = function(rowid) {
  		var startDate=document.getElementById('checkIn').value;
        	var endDate=document.getElementById('checkOut').value;
  			var url = "get-partner-propertyrevenue?strStartDate="+startDate+"&strEndDate="+endDate;
  			$http.get(url).success(function(response) {
  				
  			$scope.revenue= response.data;
  				
  					});

  	}; 
  	 $scope.getUserProperty();
  	$scope.getArrival();
  	$scope.getAllBookings();
  	$scope.getPropertyRevenue();
 });
 </script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.12.0/moment.js"></script>
 <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
<script>
var startOfMonth = moment().startOf('month').format('LL');
var end = moment().format('LL');
document.getElementById('checkIn').value = startOfMonth;
document.getElementById('checkOut').value = end;

var fromDate = moment().subtract('days', 29);
fromDate = fromDate.format("LL");
var toDate = moment().format("LL");
document.getElementById('chartDate').innerHTML  = fromDate +"     to    "+toDate;
$("#pieChart").ready(function(){
	pieCharts();
});

</script>   
 <!-- DATEPICKER CDN BEGINS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/themes/pepper-grinder/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> 
<script src="assets/js/shared/chart.js"></script>
 <!-- DATEPICKER CDN ENDS -->
              