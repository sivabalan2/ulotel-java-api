<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<div class="content-wrapper">
<!-- Wrapper Section Begins -->
          <div class="row">
           <div class="col-12">
            <div class="card">
             <div class="card-body">
             <form>
  <div class="form-row">
     <div class="form-group col-md-4">
      <label for="inputState">Select Room Type</label>
      <select id="accommodationId" name="accommodationId" class="form-control">
       <option selected="selected" value="0">All</option>
       <option ng-repeat="acc in accommodations" value="{{acc.accommodationId}}">{{acc.accommodationType}}</option>
      </select>
    </div>
    <div class="form-group col-md-3">
      <label >Start Date</label>
      <div class="input-group" >
      <div class="input-group-prepend" >
          <label class="input-group-text" for="fromDate" id="inputGroupPrepend" ><span  class="mdi mdi-calendar"></span></label>
        </div>
      <input type="text" class="form-control" id="fromDate" name="fromDate"  autocomplete="off">
      </div>
    </div>
 
    <div class="form-group col-md-3">
      <label for="checkOut">End Date</label>
      <div class="input-group">
      <div class="input-group-prepend">
          <label class="input-group-text" for="toDate" id="inputGroupPrepend"><span class="mdi mdi-calendar"></span></label>
        </div>
      <input type="text" class="form-control" id="toDate" name="toDate"  autocomplete="off">
      </div>
    </div>
       <div class="form-group col-md-2">
      <label for="checkOut">Booking Id</label>
      <div class="input-group">
      <input type="text" class="form-control" id="filterBookingId" name="filterBookingId" >
      </div>
    </div>
 
          <div class="form-group col-md-2">
              <div class="form-radio">
                   <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="optionsRadios" id="filterBookingDate" value="" checked > Booking Date<i class="input-helper"></i></label>
              </div>
          </div>
                <div class="form-group col-md-2">
              <div class="form-radio">
                   <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="optionsRadios" id="filterCheckInDate" value="" > Check In Date <i class="input-helper"></i></label>
              </div>
          </div>
                <div class="form-group col-md-2">
              <div class="form-radio">
                   <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="optionsRadios" id="filterCheckOutDate" value="" > Check Out Date <i class="input-helper"></i></label>
              </div>
            </div>
                  <div class="form-group col-md-2">
              <div class="form-radio">
                   <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="optionsRadios" id="filterStayInfo" value="" > Stay Info <i class="input-helper"></i></label>
              </div>
          </div>
           <div class="form-group col-md-2">
     <button type="submit" class="btn btn-info mt-1"ng-csv="bookinglist" filename="bookingreport.csv" 
					csv-header="['Booking Id','Guest Name','Booking Date','Check-In','Check-Out','Accommodations','Rooms','Nights','Room Nights','No of Adult',
					'No of Child','Revenue Amount','Tax Amount','Total Revenue','Ulo Commission','Ulo Tax','Payable to Ulo','Hotel Revenue','Tax Amount','Hotel Pay','Payable to Hotel','Status']" field-separator="," decimal-separator="."><span class="mdi mdi-file-excel"></span> Export CSV</button>
       </div>
           <div class="form-group col-md-2">
     <button type="submit" ng-click="getPropertyBookingList()" class="btn btn-dark mt-1"><span class="mdi mdi-map-search"></span> Search</button>
  </div>
       
  </div>
 </form>
    <!-- Reports shown begins here -->
    <div class="table-responsive" id="showDisplayReport">
    <table class="table table-striped">
    <thead>
        <tr>
    <th> Booking ID</th>
	<th>Guest	</th>
<!-- 	<th>Mobile	</th> -->
<!-- 	<th>Email	</th> -->
	<th>Booking Date</th>
	<th>Check-In	</th>
	<th>Check-Out	</th>
	<th>Accommodations </th>
	<th>Rooms</th>
	<th>Nights	</th>
	<th>Room Nights</th>
	<th>Adult	</th>
	<th>Child	</th>
	<th>Revenue Amount</th>
	<th>Tax Amount</th>
	<th>Total Revenue</th>
	<th>Ulo Commission</th>
	<th>Ulo Tax	</th>
	<th>Payable to Ulo</th>
	<th>Hotel Revenue</th>
	<th>Tax Amount</th>
	<th>Hotel Pay</th>
	<th>Payable to Hotel</th>
	<th>Status</th>

        </tr>
    </thead>
    <tbody>
    	<tr ng-if="bookinglist.length==0" >
    		<label>No record found</label>
    	</tr>
        <tr ng-if="bookinglist.length>0" ng-repeat="book in bookinglist">
            <td class="filterable-cell">{{book.bookingId}}</td>
            <td class="filterable-cell">{{book.guestName}}</td>
<!--             <td class="filterable-cell">{{book.mobileNumber}}</td> -->
<!--              <td class="filterable-cell">{{book.emailId}}</td> -->
            <td class="filterable-cell">{{book.bookingDate}}</td>
             <td class="filterable-cell">{{book.checkIn}}</td>
              <td class="filterable-cell">{{book.checkOut}}</td>
               <td class="filterable-cell">{{book.accommodationType}}</td>
                <td class="filterable-cell">{{book.rooms}}</td>
                 <td class="filterable-cell">{{book.diffDays}}</td>
                  <td class="filterable-cell">{{book.roomNights}}</td>
                   <td class="filterable-cell">{{book.adultCount}}</td>
                    <td class="filterable-cell">{{book.childCount}}</td>
                     <td class="filterable-cell">{{book.revenueAmount}}</td>
                      <td class="filterable-cell">{{book.taxAmount}}</td>
                       <td class="filterable-cell">{{book.revenueTaxAmount}}</td>
                        <td class="filterable-cell">{{book.uloCommission}}</td>
                         <td class="filterable-cell">{{book.uloTaxAmount}}</td>
                          <td class="filterable-cell">{{book.uloRevenue}}</td>
                           <td class="filterable-cell">{{book.hotelPayRevenue}}</td>
                            <td class="filterable-cell">{{book.taxTotalAmount}}</td>
                             <td class="filterable-cell">{{book.dueAmount}}</td>
                              <td class="filterable-cell">{{book.hotelRevenue}}</td>
                               <td class="filterable-cell">{{book.status}}</td>
                              
        </tr>
    </tbody>
</table>
    </div>
    <!-- Reports shown ends here -->  
             </div>
            </div>
           </div>
          </div>
<!-- Wrapper Section Ends -->            
</div>
<%-- <%=session.getAttribute("propertyId") %> --%>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ngprogress/1.1.3/ngProgress.js"></script>
<!-- csv download script start -->
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.15/angular-sanitize.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ng-csv/0.3.6/ng-csv.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.0rc1/angular-route.min.js"></script>
<!-- csv download script end -->
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>
<script>
 var app = angular.module('myApp', ['ngProgress','ui.bootstrap','ngSanitize', 'ngCsv']);
 app.controller('customersCtrl', function($scope, $http,$timeout,ngProgressFactory) {
	 var spinner = $('#loadertwo');
	 var showDisplayReport = $('#showDisplayReport');
 //Datepicker begins 
    $("#fromDate").datepicker({
              dateFormat: 'MM d, yy',
            
             // minDate:  0,
              onSelect: function (formattedDate) {
                  var date1 = $('#fromDate').datepicker('getDate'); 
                  var date = new Date( Date.parse( date1 ) ); 
                  date.setDate( date.getDate() + 1 );        
                  var newDate = date.toDateString(); 
                  newDate = new Date( Date.parse( newDate ) );   
                  $('#toDate').datepicker("option","minDate",newDate);
                  $timeout(function(){
                    //scope.checkIn = formattedDate;
                  });
              }
          });
   
          $("#toDate").datepicker({
              dateFormat: 'MM d, yy',
            
              //minDate:  0,
             
              onSelect: function (formattedDate) {
                  var date2 = $('#toDate').datepicker('getDate'); 
                  $timeout(function(){
                    //scope.checkOut = formattedDate;
                  });
              }
          });

 //Datepicker Ends
 //showDisplayReport.hide();
          $scope.getPropertyBookingList = function(){
        	  
				var filterDateName="",filterBookingId="", displayReport="";
				if(document.getElementById('filterBookingDate').checked){
					filterDateName='BookingDate';
				}
				if(document.getElementById('filterCheckInDate').checked){
					filterDateName='CheckInDate';
				}
				if(document.getElementById('filterCheckOutDate').checked){
					filterDateName='CheckOutDate';
				}
				if(document.getElementById('filterStayInfo').checked){
					filterDateName='StayInfo';
				}
				filterBookingId=document.getElementById('filterBookingId').value;
				if(filterBookingId==""){
					filterBookingId="0";
					displayReport="DateFilter";
				}else{
					displayReport="BookingIdFilter";
				}
				if(displayReport=="DateFilter"){
					
					var fdata = "strFromDate=" + $('#fromDate').val()+ "&strToDate=" + $('#toDate').val()
					+ "&accommodationId="+$('#accommodationId').val()+"&filterDateName="+filterDateName
					+"&filterBookingId="+filterBookingId;
					
					if(document.getElementById('fromDate').value==""){
			 			 document.getElementById('fromDate').focus();
			 			 document.getElementById("fromDate").style.borderColor = "red";
			 		 }else if(document.getElementById('toDate').value==""){
			 			 document.getElementById('toDate').focus();
			 			document.getElementById("toDate").style.borderColor = "red";
			 		 }else if(document.getElementById('fromDate').value!="" && document.getElementById('toDate').value!=""){
                      spinner.show();
                      
			 			 $http(
								{
									method : 'POST',
									data : fdata,
									headers : {
										'Content-Type' : 'application/x-www-form-urlencoded'
									},
									url : 'report-partner-booking'
								}).success(function(response) {
									
									$scope.bookinglist = response.data;
									//alert($scope.bookinglist.length);
									spinner.hide();
									//showDisplayReport.show();
								});
			 		 }
				}else if(displayReport=="BookingIdFilter"){
					var fdata = '&filterBookingId='+filterBookingId;	
					spinner.show();
					$http(
							{
								method : 'POST',
								data : fdata,
								headers : {
									'Content-Type' : 'application/x-www-form-urlencoded'
								},
								url : 'report-partner-booking'
							}).success(function(response) {
								spinner.hide();
								$scope.bookinglist = response.data;
								
							});
					}
			};
			
			var selectPartnerPropertyId=0,partnerPropertyId=0;
	  		$scope.getUserProperty = function() {
	  			var url = "get-partner-user-property";
	  			$http.get(url).success(function(response) {
	  				$scope.userProperty = response.data;
	  				//partnerPropertyId=$scope.userProperty[0].selectedPropertyId;
	  				//$scope.getAccommodations(partnerPropertyId);
	  			});
	  			
	  	  };
	  	  
	  	$scope.selectProperty= function(rowid){
			  selectPartnerPropertyId=rowid;
			  var url="get-selected-property-id?selectedPartnerPropertyId="+selectPartnerPropertyId;
			  $http.get(url).success(function(response) {
					$scope.selectedPropertyId= response.data;
					location.reload();
				});	
			  //$scope.getAccommodations(selectPartnerPropertyId);
		  };
	  	  
	  	 
	  	  
	  	$scope.getAccommodations = function(rowid) {
			var url = "get-partner-accommodations"
			$http.get(url).success(function(response) {
			    //console.log(response);
				$scope.accommodations = response.data;
	
			});
		};
		
		 $scope.getUserProperty();
		$scope.getAccommodations();
			
 });
 </script>
 <!-- DATEPICKER CDN BEGINS -->
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/themes/pepper-grinder/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> 
 <!-- DATEPICKER CDN ENDS -->
  <style>



</style>