<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<div class="content-wrapper">
<!-- Wrapper Section Begins -->
          <div class="row">
           <div class="col-12">
            <div class="card">
             <div class="card-body">
             <form>
  <div class="form-row">
     <div class="form-group col-md-4">
      <label for="inputState">Select Room Type</label>
      <select id="propertyAccommodationId" class="form-control">
         <option value="">Choose Accommodation</option>
         <option ng-repeat="at in accommodations" value="{{at.accommodationId}}">{{at.accommodationType}}</option>
      </select>
    </div>
    <div class="form-group col-md-3">
      <label >Start Date</label>
      <div class="input-group" >
      <div class="input-group-prepend" >
          <label class="input-group-text" for="startDate" id="inputGroupPrepend" ><span  class="mdi mdi-calendar"></span></label>
        </div>
      <input type="text" class="form-control" id="startDate" ng-required="true" autocomplete="off">
      </div>
    </div>
 
    <div class="form-group col-md-3">
      <label for="checkOut">End Date</label>
      <div class="input-group">
      <div class="input-group-prepend">
          <label class="input-group-text" for="endDate" id="inputGroupPrepend"><span class="mdi mdi-calendar"></span></label>
        </div>
      <input type="text" class="form-control" id="endDate" ng-required="true" autocomplete="off">
      </div>
    </div>
     <div class="form-group col-md-2">
     <label for="inputCity">Search</label>
     <button type="submit" ng-click="getBulkBlockInventory()" class="btn btn-dark btn-block"><span class="mdi mdi-map-search"></span> Search</button>
  </div>
  </div>
 </form>
<!-- Room Types list starts here  -->
<div class="row" id="showAvailableCount">
<div class="col-md-12 col-xs-12 "><h4>Available Room Count</h4></div>
<div class="col-md-6 col-xs-12">
<label><button class="btn btn-success float-left mx-1 btn-xs"></button>Available </label>
<label><button class="btn btn-danger float-left mx-1 btn-xs"></button>Sold</label>
</div>
<div class="col-md-6 col-xs-12  ">
<button class="btn btn-primary mx-1 float-sm-left float-md-right btn-xs font-weight-bold" ng-click="dateNext()">Next 7 Days <i class="fas fa-chevron-right"></i></button>
<button class="btn btn-primary mx-1 float-sm-left float-md-right btn-xs font-weight-bold" ng-click="datePrevious()"><i class="fas fa-chevron-left"></i> Previous 7 Days</button>
</div>
</div>
<div class="table-responsive mb-1" ng-repeat="rc in blockinventory |limitTo :1">
<table class="table table-bordered">
<tbody>
    <tr>
      <th scope="row">Room Types</th>
      <td ng-repeat="dd in rc.roomAvailable | limitTo:filterLimit track by $index" class="text-center" id="tlastchild{{$index}}"><small> {{dd.date}}</small></td>
      
    </tr>
    <tr>
      <th scope="row">{{ rc.accommodationType }}</th>
      <td ng-repeat="dd in rc.roomAvailable | limitTo:filterLimit track by $index">
      <input type="text" id="roomId{{$index}}"  ng-change="singleBlockInventory(dd.available,dd.date,rc.accommodationId,dd.totalRooms,dd.totalAvailable,dd.soldRooms,$index)" ng-model="dd.available" class="form-control form-control-sm text-center m-1" ><br>
      <button class="btn btn-success m-1 btn-block">{{dd.totalRoomCount}}</button><button class="btn btn-danger m-1 btn-block ">{{dd.sold}}</button>
      </td>
    </tr>
  </tbody>
</table>
</div>
<!-- Blocking reason begins here -->
<div class="row" id="showAvailableCountForm">
<div class="col-12">
             <form>
  <div class="form-row">
    
    <div class="form-group col-md-12">
      <small class="px-1 text-center">Please provide reason for blocking:</small>
       <select id="singleInventoryRemarks" class="form-control" ng-required="true">
         <option value="">Choose reason for blocking:</option>
         <option  value="walk in">Walk-in</option>
         <option  value="pre booking">Pre Booking</option>
         <option value="maintanance">Maintanance</option>
      </select>
    </div>
     <div class="form-group col-md-12"> 
  <div id="roompromoalertShow"></div>
     </div>
    <div class="form-group col-md-12"> 
  
     <button type="submit" ng-click="updateRooms()" class="btn btn-dark float-right mx-1"><span class="mdi mdi-content-save"></span>Save Changes</button>
  </div>
  </div>
 </form>
</div>
</div>
<!-- Blocking reason ends here -->

<!-- Room Types list ends here  --> 
 
             </div>
            </div>
           </div>
    
          </div>
              <!--  Bulk Block Begins  -->
    <div class="row mt-1">
    <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                 <div class="accordion" id="accordion-4" role="tablist">
                      <div class="card accordion-inverse-primary">
                        <div class="card-header" role="tab" id="headingOne-4">
                          <h5 class="mb-0">
                            <a data-toggle="collapse" href="#collapseOne-4" id="ariaExpand" aria-expanded="false" aria-controls="collapseOne-4" class="collapsed"> Bulk Room Update</a>
                          </h5>
                        </div>
                        <div id="collapseOne-4" class="collapse" role="tabpanel" aria-labelledby="headingOne-4" data-parent="#accordion-4" style="">
                          <div class="card-body">.
                                   <form>
  <div class="form-row">
     <div class="form-group col-md-3">
      <label for="inputState">Select Room Type</label>
      <select id="bulkpropertyAccommodationId" class="form-control">
         <option value="">Choose Accommodation</option>
         <option ng-repeat="at in accommodations" value="{{at.accommodationId}}">{{at.accommodationType}}</option>
      </select>
    </div>
    <div class="form-group col-md-3">
      <label >Start Date</label>
      <div class="input-group" >
      <div class="input-group-prepend" >
          <label class="input-group-text" for="startBulkDate" id="inputGroupPrepend" ><span  class="mdi mdi-calendar"></span></label>
        </div>
      <input type="text" class="form-control" id="startBulkDate" ng-required="true" autocomplete="off">
      </div>
    </div>
 
    <div class="form-group col-md-3">
      <label for="checkOut">End Date</label>
      <div class="input-group">
      <div class="input-group-prepend">
          <label class="input-group-text" for="endBulkDate" id="inputGroupPrepend"><span class="mdi mdi-calendar"></span></label>
        </div>
      <input type="text" class="form-control" id="endBulkDate" ng-required="true" autocomplete="off">
      </div>
    </div>
      <div class="form-group col-md-3">
      <label for="checkOut">Room Count</label>
      <div class="input-group">
    
      <input type="text" class="form-control" id="bulkRoomCount" ng-required="true" autocomplete="off">
      </div>
    </div>
    
    <div class="form-group col-md-12">
      <small class="px-1 text-center">Please provide reason for blocking:</small>
       <select id="bulkInventoryRemarks" class="form-control" ng-required="true">
         <option value="">Choose reason for blocking:</option>
         <option  value="walk in">Walk-in</option>
         <option  value="pre booking">Pre Booking</option>
         <option value="maintanance">Maintanance</option>
      </select>
    </div>
     <div class="form-group col-md-12"> 
  <div id="roompromoalert"></div>
     </div>
    <div class="form-group col-md-12"> 
  
     <button type="submit" ng-click="bulkBlockInventory()" class="btn btn-dark float-right mx-1"><span class="mdi mdi-content-save"></span>Submit</button>
  </div>
  </div>
 </form>
                          </div>
                        </div>
                      </div>
               
                      
                    </div>
                  </div>
                </div>
              </div>
    
    </div>
    <!-- Bulk Block ends --> 
<!-- Wrapper Section Ends -->            
</div>
  <%-- <%=session.getAttribute("propertyId") %> --%>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ngprogress/1.1.3/ngProgress.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>
<script>
 var app = angular.module('myApp', ['ngProgress','ui.bootstrap']);
 app.controller('customersCtrl', function($scope, $http,$timeout,ngProgressFactory) {
 //Datepicker begins 
  var spinner = $('#loadertwo');
    $("#startDate").datepicker({
              dateFormat: 'MM d, yy',
            
              minDate:  0,
              onSelect: function (formattedDate) {
                  var date1 = $('#startDate').datepicker('getDate'); 
                  var date = new Date( Date.parse( date1 ) ); 
                  date.setDate( date.getDate() );        
                  var newDate = date.toDateString(); 
                  newDate = new Date( Date.parse( newDate ) );   
                  $('#endDate').datepicker("option","minDate",newDate);
                  $timeout(function(){
                    //scope.checkIn = formattedDate;
                  });
              }
          });
   
          $("#endDate").datepicker({
              dateFormat: 'MM d, yy',
            
              minDate:  0,
             
              onSelect: function (formattedDate) {
                  var date2 = $('#endDate').datepicker('getDate'); 
                  $timeout(function(){
                    //scope.checkOut = formattedDate;
                  });
              }
          });
          
          $("#startBulkDate").datepicker({
              dateFormat: 'MM d, yy',
            
              minDate:  0,
              onSelect: function (formattedDate) {
                  var date1 = $('#startBulkDate').datepicker('getDate'); 
                  var date = new Date( Date.parse( date1 ) ); 
                  date.setDate( date.getDate() +1);        
                  var newDate = date.toDateString(); 
                  newDate = new Date( Date.parse( newDate ) );   
                  $('#endBulkDate').datepicker("option","minDate",newDate);
                  $timeout(function(){
                    //scope.checkIn = formattedDate;
                  });
              }
          });
   
          $("#endBulkDate").datepicker({
              dateFormat: 'MM d, yy',
            
              minDate:  0,
             
              onSelect: function (formattedDate) {
                  var date2 = $('#endBulkDate').datepicker('getDate'); 
                  $timeout(function(){
                    //scope.checkOut = formattedDate;
                  });
              }
          });

 //Datepicker Ends
 
          var selectPartnerPropertyId=0,partnerPropertyId=0;
    		
          $scope.getUserProperty = function() {
  			var url = "get-partner-user-property";
  			$http.get(url).success(function(response) {
  				$scope.userProperty = response.data;
  				//partnerPropertyId=$scope.userProperty[0].selectedPropertyId;
  				
  				//$scope.getAccommodations(partnerPropertyId);
  			});
  			
  			
  			
  	  };
  	  
  	$scope.selectProperty= function(rowid){
		  selectPartnerPropertyId=rowid;
		  var url="get-selected-property-id?selectedPartnerPropertyId="+selectPartnerPropertyId;
		  $http.get(url).success(function(response) {
				$scope.selectedPropertyId= response.data;
				location.reload();
			});	
		  //$scope.getAccommodations(selectPartnerPropertyId);
	  }
  	  
  	  $scope.getUserProperty();
  	  
  	$scope.getAccommodations = function() {
		
		var url = "get-partner-accommodations"
		$http.get(url).success(function(response) {
			$scope.accommodations = response.data;

		});
	};
	
      var showAvailableCount = $('#showAvailableCount');
      var showAvailableCountForm = $('#showAvailableCountForm');
      showAvailableCount.hide();
      showAvailableCountForm.hide();
 		var lastDate = 0;
        var startDate = 0;
 		$scope.filterLimit = 7;
 		 $scope.getBulkBlockInventory = function() {
          	 	var fdata = "&startBlockDate=" + $('#startDate').val()
   			+ "&endBlockDate=" + $('#endDate').val()
   			+ "&propertyAccommodationId=" + $('#propertyAccommodationId').val()
   			spinner.show();
          	  $http(
            		   {   
            			   	   
            			   method : 'POST',
            			   data : fdata,            			  
            			   headers : {
            			   'Content-Type' : 'application/x-www-form-urlencoded'
            			   },
            			  url : 'get-partner-bulk-block-inventory'   /*   get-ota-booked-details */
            			   }).success(function(response) {
            			   
            			   $scope.blockinventory = response.data;
            			   spinner.hide();
            			   showAvailableCount.show();
            			   showAvailableCountForm.show();
              			   lastDate = $scope.blockinventory[0].roomAvailable[$scope.blockinventory[0].roomAvailable.length-1].date;
            			   startDate = $scope.blockinventory[0].roomAvailable[0].date;
            		   
            		   });
          		
          	};	
          	
          	$scope.datePrevious = function(){
          		
        		 var result = new Date(startDate);
        		result.setDate(result.getDate() - 7);
        		var months = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
        		var month = months[result.getMonth()];
        		var year = result.getFullYear();
        		var dates = month  + " " + result.getDate() + "," + year;

        		var fdata = "&startBlockDate=" + dates
     			+ "&endBlockDate=" + startDate
     			+ "&propertyAccommodationId=" + $('#propertyAccommodationId').val()
            	 	 spinner.show();
            			   showAvailableCount.show();
            			   showAvailableCountForm.show();
            	  $http(
              		   {
              			   method : 'POST',
              			   data : fdata,
              			   headers : {
              			   'Content-Type' : 'application/x-www-form-urlencoded'
              			   },
              			 url : 'get-partner-bulk-block-inventory'
              			   }).success(function(response) {
              			   
              			   $scope.blockinventory = response.data;
              			   
               			   
                			   lastDate = $scope.blockinventory[0].roomAvailable[$scope.blockinventory[0].roomAvailable.length-1].date;
              			   startDate = $scope.blockinventory[0].roomAvailable[0].date;
              			 spinner.hide();
          			   showAvailableCount.show();
          			   showAvailableCountForm.show();
              		   
              		   });
         	};
       	$scope.dateNext = function(){
         		 var result = new Date(lastDate);
         		result.setDate(result.getDate() + 7);
         		var months = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
         		var month = months[result.getMonth()];
         		var year = result.getFullYear();
         		var dates = month  + " " + result.getDate() + "," + year;

         		var fdata = "&startBlockDate=" + lastDate
      			+ "&endBlockDate=" + dates
      			+ "&propertyAccommodationId=" + $('#propertyAccommodationId').val()
             	 	 spinner.show();
            			   showAvailableCount.show();
            			   showAvailableCountForm.show();
             	  $http(
               		   {
               			   method : 'POST',
               			   data : fdata,
               			   headers : {
               			   'Content-Type' : 'application/x-www-form-urlencoded'
               			   },
               			 url : 'get-partner-bulk-block-inventory'
               			   }).success(function(response) {
               			   
               			   $scope.blockinventory = response.data;
               			   
                			   
                 			   lastDate = $scope.blockinventory[0].roomAvailable[$scope.blockinventory[0].roomAvailable.length-1].date;
               			   startDate = $scope.blockinventory[0].roomAvailable[0].date;
               			 spinner.hide();
          			   showAvailableCount.show();
          			   showAvailableCountForm.show();
               		   
               		   });
         	};
         	$scope.roomsInfo = [];
        	$scope.singleBlockInventory = function(available,date,id,totalRooms,totalAvailable,soldRooms,indx){
        		var availRoom=parseInt(available);
        		var totalAvail=parseInt(totalRooms);
        		var soldRoom=parseInt(soldRooms);
        		var availableRooms=totalAvail-soldRoom;
        		$scope.validate = true;
        		if(availRoom<=parseInt(availableRooms)){
        			var newData = {'startBlockDate':date,'inventoryCount':available,'propertyAccommodationId':id};
                	  	
        			angular.forEach($scope.roomsInfo, function(value, key) {
        				if(value.startBlockDate == date){
        					value.inventoryCount = available;
        					$scope.validate = false;
                  		 }
                  	});
        			if($scope.validate == true){
        				$scope.roomsInfo.push(newData);
        				
        			}
        			
		  		//console.log("console.log"+JSON.stringify($scope.roomsInfo))       	 
                
            
               	 /* $("#roompromoalert").hide();
				  var alertmsg = ' <div class="alert" style="color:green;text-align:center;">Inventory changed for '+date+'.</div>';
		            $('#roompromoalert').html(alertmsg);
		            $("#roompromoalert").fadeTo(2000, 500).slideUp(500, function(){
		                $("#roompromoalert").slideUp(500);
		            }); */
        		}else if(availRoom>parseInt(availableRooms)){
        			$scope.blockinventory[0].roomAvailable[indx].blockedRooms=totalAvailable;
		            document.getElementById('roomId'+indx).value=totalAvailable;
        			$("#roompromoalertShow").hide();
					  var alertmsg = ' <div class="alert" style="color:green;text-align:center;">Maximum inventory updated.</div>';
			            $('#roompromoalertShow').html(alertmsg);
			            $("#roompromoalertShow").fadeTo(2000, 500).slideUp(500, function(){
			                $("#roompromoalertShow").slideUp(500);
			            });
			            
        			return;
        		}
          	 
          	};
          	
          	 $scope.roomsInfo = [];
          	$scope.updateRooms = function(){
          		$("#loading-example-btn").prop('disabled', true);
          		spinner.show();
          		var text = '{"array":' + JSON.stringify($scope.roomsInfo) + '}';
          		
          		var fdata = JSON.parse(text);
          		var inventoryRemarks=$('#singleInventoryRemarks').val();
          		if(inventoryRemarks.trim().length==0)
        			{
          			
          			spinner.hide();
 				   //alert remarks
 	              	var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:green;text-align:center;">Please Select the reason for blocking.</div>';
        	   			            $('#roompromoalertShow').html(alertmsg);
        	   			            $("#roompromoalertShow").fadeTo(2000, 500).slideUp(500, function(){
        	   			                $("#roompromoalertShow").slideUp(500);
        	   			            });
     				return;
        			}
          		
          		
          	   $http(
             		   { 
             			   		   
             			   method : 'POST',
             			   data : fdata,
             			   dataType: 'json',
             			   headers : {
             				   'Content-Type': 'application/json; charset=utf-8'
             			   },
                 			url : "partner-single-block-room-inventory?inventoryRemarks="+inventoryRemarks
                 			   }).success(function(response) {
                 				  spinner.hide();
                 			    $scope.singleblockroom = response.data;
                 			    $("#loading-example-btn").prop('disabled', false);
                 			   
                 			    $("#roompromoalertShow").hide();
          					    var alertmsg = ' <div class="alert" style="color:green;text-align:center;">Room inventory updated successfully.</div>';
          			            $('#roompromoalertShow').html(alertmsg);
          			            $("#roompromoalertShow").fadeTo(2000, 500).slideUp(500, function(){
          			            $("#roompromoalertShow").slideUp(500);
          			            });
          			          $scope.roomsInfo = [];
                 			 }).error(function(response) {
                 				spinner.hide();
          						
          						var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:green;text-align:center;">Process failed, Please update the romm count again.</div>';
        	   			            $('#roompromoalertShow').html(alertmsg);
        	   			            $("#roompromoalertShow").fadeTo(2000, 500).slideUp(500, function(){
        	   			                $("#roompromoalertShow").slideUp(500);
        	   			            });
          					});
                 		   
                     	};
                     	
                    	$scope.bulkBlockInventory = function(){
                      		
                      	   var fdata = "startBlockDate=" + $('#startBulkDate').val()
                         	+ "&endBlockDate=" + $('#endBulkDate').val()
                      	    + "&inventoryCount=" + $('#bulkRoomCount').val()
                         	+ "&propertyAccommodationId=" + $('#bulkpropertyAccommodationId').val()
                         	+"&inventoryRemarks="+$('#bulkInventoryRemarks').val();
                      	   
                      	  	var inventoryRemarks=$('#bulkInventoryRemarks').val();
                      	   if(inventoryRemarks.trim().length==0)
                  			{
                      		  
                    			$("#roominventorypromoalert").hide();
             				  var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:green;text-align:center;">please check the inventory on '+date+'.</div>';
             		            $('#roominventorypromoalert').html(alertmsg);
             		            $("#roominventorypromoalert").fadeTo(2000, 500).slideUp(500, function(){
             		                $("#roominventorypromoalert").slideUp(500);
             		            });
             	            
               				return;
                  			}
                      		   $http(
                      		   {
                      			   method : 'POST',
                      			   data : fdata,
                      			   headers : {
                      			   'Content-Type' : 'application/x-www-form-urlencoded'
                      			   },
                      			  url : 'partner-block-room-inventory'
                      			   }).success(function(response) {
                      				
                      			   $scope.blockrooms = response.data;
                      			
                      			   $("#roompromoalert").hide();
              					  var alertmsg = ' <div class="alert" style="color:green;text-align:center;">room inventory updated.</div>';
              			            $('#roompromoalert').html(alertmsg);
              			            $("#roompromoalert").fadeTo(2000, 500).slideUp(500, function(){
              			                $("#roompromoalert").slideUp(500);
              			            }); 
                      			   
              			          // $('#bulkupdate').modal('toggle'); 
                      			 $scope.roomsInfo = [];
                      			  }).error(function(response) {
                      				  
                  					 $("#roompromoalert").hide();
                    					  var alertmsg = ' <div class="alert" style="color:green;text-align:center;">process failed, please try again!! .</div>';
                    			            $('#roompromoalert').html(alertmsg);
                    			            $("#roompromoalert").fadeTo(2000, 500).slideUp(500, function(){
                    			                $("#roompromoalert").slideUp(500);
                    			            });
                    			        // $('#bulkupdate').modal('toggle'); 
                  					});
                      	
                         };
 
                         $scope.getAccommodations();
        
 });
 </script>
 <!-- DATEPICKER CDN BEGINS -->
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/themes/pepper-grinder/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> 
 <!-- DATEPICKER CDN ENDS -->
              