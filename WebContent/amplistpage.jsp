<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
 <%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <!doctype html>
<html amp>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width,minimum-scale=1">
<!-- seo header -->

<title>ULO: Book Hotels In Bangalore | Pay @ Hotel</title>
<meta name="description" content="Book hotels in bangalore, and get Complimentary Breakfast, AC Rooms, LCD TV,Hygienic Bathrooms and Comfortable Beds. Book 
   quality pocket-friendly hotels in bangalore from the leading budget hotel chain.">
<meta name="keywords" content="hotels in bangalore"/>
<meta property="og:site_name" content="ULO Hotels" />
<meta property="og:description" content="Book hotels in bangalore, and get Complimentary Breakfast, AC Rooms, LCD TV, Hygienic Bathrooms and 
   Comfortable Beds. Book quality pocket-friendly hotels in bangalore coorg from the leading budget hotel chain." />
<meta property="og:title" content="ULO: Book hotels in bangalore | Pay @ Hotel" />
<meta property="og:url" content="https://www.ulohotels.com/hotels-in-bangalore" />
<link rel="canonical" href="https://www.ulohotels.com/hotels-in-bangalore">
<meta content="origin" name="referrer" />
<meta name="google" content="notranslate">
<meta http-equiv="Content-Language" content="en">
 <style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
  <script async src="https://cdn.ampproject.org/v0.js"></script>
 <script async custom-element="amp-install-serviceworker" src="https://cdn.ampproject.org/v0/amp-install-serviceworker-0.1.js"></script>
 <script async custom-element="amp-list" src="https://cdn.ampproject.org/v0/amp-list-0.1.js"></script>
 <script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>
 <script async custom-template="amp-mustache" src="https://cdn.ampproject.org/v0/amp-mustache-0.1.js"></script>
 <style amp-custom >
 .header{
 border:1px solid #ccc;
 background:
 }
 .header .logo{
   display:block;
    margin:auto;
    padding:5px;
 }
 .header .logotext{
 display:block;
 color:#333;
 font-size:9px;
 text-align:center;
 padding:5px;
 }
 .header .headercontact{
  display:block;
  text-align:center;
  color:#000;
  font-weight:bold;
  font-size:16;
  text-decoration:none;
  }
  .offermanagement{
  background: #699;
     padding:5px;
     font-size:15px
  }
    .offermanagement .offerdetail{
    color:#fff;
    text-align:center;
   display:block;
   padding:5px;
   
    }
      .offermanagement .offercode{
      color:red;
      background:#FFF;
      padding:5px;
      }
.hotelmanagement{
width:100%
}
.hotelmanagement .hotelimage{
padding:5px;
}
.hotelmanagement .hotelname{
padding:5px;
float:left;
text-transform:uppercase;
color: #693;
}
.hotelmanagement .hotelprice{
padding:5px;
float:left;
}
 </style>
</head>
<body>
<header>
<div class="header">
<amp-img src="ulowebsite/images/logo/ulologo.png" alt="Welcome" height="40" width="141" class="logo"></amp-img>
<span class="logotext">A NEXT GENERATION QUALITY BUDGET HOTEL CHAIN</span>
<a href="tel:9543592593" title="+91 95435 92593" class="headercontact"><span >Call @ 91 9543 592 593</span></a>
</div>
</header>
 <div class="offermanagement">
<amp-list src="http://localhost:8080/ulopms/get-amp-current-discounts" height="30">
  <template type="amp-mustache">
  <span class="offerdetail "><span class="offercode">{{discountName}}</span> - Use this code To Get {{discountPercentage}} % OFF</span>
  </template>
</amp-list>
</div> 
<div class="hotelmanagement">
<div class="hotelimage">
<amp-img alt="hotels in bangalore" fallback width="60" height="60" layout="responsive" src="https://www.ulohotels.com/get-property-thumb?propertyId=23"></amp-img>
</div>     
<div class="hotelname">
<h3 class="hname">ulo square plaza</h3>
</div>   
<div class="hotelprice">
<h3 class="hprice">d</h3>
</div>   
</div>
</body>

</html>
    