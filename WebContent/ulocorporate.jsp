  <%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
   pageEncoding="ISO-8859-1"%>
 <section class="dcorporate">
 <div class="container">
 <div class="row">
 
 <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12 corporatecontent">
 <h2>Welcome TO Ulo Corporate</h2>
 <img src="contents/images/partner/b5.png" class="img-responsive">
 </div>
  <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12 corporateform">
  <form name="corporate">
  <h3>Get in Touch</h3>
  <div class="form-group">
  
  </div>
    <div class="form-group">
  <input type="text" class="form-control" id="employeeName" ng-model="employeeName" placeholder="Name"  ng-required="true">
  </div>
    <div class="form-group">
  <input type="text" class="form-control" id="corporateNumber" ng-model="corporateNumber" placeholder="Mobile"  ng-required="true">
  </div>
    <div class="form-group">
  <input type="email" class="form-control" id="corporateEmail" ng-model="corporateEmail" placeholder="Email"  ng-required="true">
  </div>
    <div class="form-group">
  <input type="text" class="form-control" id="corporateName" ng-model="corporateName" placeholder="Company Name" ng-required="true">
  </div>
  <div class="form-group">
  <div id="success"></div>
  </div>
  <div class="form-group">
 <button class="btn btncorporate" type = "submit" ng-click="corporate.$valid && sendCorporateEnquiry()">{{submit}}</button>
  </div>
  </form>
  </div>
 
 </div>
 </div>
 </section>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script src="js1/ngprogress.js" type="text/javascript"></script>
 <script type="text/javascript" src="https://apis.google.com/js/api.js" ng-init="handleGoogleApiLibrary()" ></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script> 
<script>
   var app = angular.module('myApp', ['ngProgress']);
   app.controller('customersCtrl', function ($scope, $http, $timeout, ngProgressFactory) {
    $scope.userData = [];
    
    $scope.handleGoogleApiLibrary = function() {
        gapi.load('client:auth2', {
            callback: function() {
                gapi.client.init({
                    apiKey: 'AIzaSyAuJwG_DuX3eEXTgsoEY9A8LRvRVfeilFs',
                    clientId: '106759720549-tfe9i1b16ovvpqn8aho6hooiaftt2h4n.apps.googleusercontent.com',
                    scope: 'https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/plus.me'
                }).then(
                    // On success
                    function(success) {
                        $("#login-button").removeAttr('disabled');
                    },
                    // On error
                    function(error) {
                        // alert('Error : Failed to Load Library');
                    }
                );
            },
            onerror: function() {
                // Failed to load libraries
            }
        });
    };
    $scope.googleLogin = function() {
        gapi.auth2.getAuthInstance().signIn().then(
            function(success) {
                gapi.client.request({
                    path: 'https://www.googleapis.com/plus/v1/people/me'
                }).then(
                    function(success) {
                        var user_info = JSON.parse(success.body);
                        var username = user_info.displayName;
                        var email = user_info.emails[0].value;
                        var phone = null;
                        $('#loginModal').modal('toggle');
                        $('#logSec').html("<div class='mnuContant'><a href='mailto:support@ulohotels.com' title='support@ulohotels.com'><i class='fa fa-envelope'></i><span class='visibleLG'>support@ulohotels.com</span></a><a href='tel:9543592593' title=''+91 95435 92593'><i class='fa fa-phone'></i><span class='visibleLG'>+91 95435 92593</span></a></div><div class='mnuProfile'><div class='mpAvatar hidden-xs' style='background:url(ulowebsite/images/avatar.png);'></div><div class='mpUsername'><a href='javascript:showpro();' class=' mnuProfileOpen'>Hi " + username + " <i class='fa fa-angle-down'></i></a></div><div class='mpSubMenu' id='showmenu' ><ul><li><a href='ulouserprofile'>Profile</a></li><li><a href='ulouserprofile'>Booking History</a></li><li><a href='ulologout'>Log out</a></li></ul></div></div>");
                        $('#logSecs').html("<div class='mnuContant'><a href='mailto:support@ulohotels.com' title='support@ulohotels.com'><i class='fa fa-envelope'></i><span class='visibleLG'>support@ulohotels.com</span></a><a href='tel:9543592593' title=''+91 95435 92593'><i class='fa fa-phone'></i><span class='visibleLG'>+91 95435 92593</span></a></div><div class='mnuProfile'><div class='mpAvatar hidden-xs' style='background:url(ulowebsite/images/avatar.png);'></div><div class='mpUsername'><a href='javascript:showpro();' class=' mnuProfileOpen'>Hi " + username + " <i class='fa fa-angle-down'></i></a></div><div class='mpSubMenu' id='showmenu' ><ul><li><a href='ulouserprofile'>Profile</a></li><li><a href='ulouserprofile'>Booking History</a></li><li><a href='ulologout'>Log out</a></li></ul></div></div>");
                        location.reload();
                        var fdata = "username=" + user_info.displayName +
                            "&emailid=" + user_info.emails[0].value +
                            "&userid=" + user_info.id;
                        $http({
                            method: 'POST',
                            data: fdata,
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded'
                            },
                            url: 'sociallogin'
                        }).then(function successCallback(response) {}, function errorCallback(response) {

                        });
                    },
                    // On error
                    function(error) {
                        $("#login-button").removeAttr('disabled');
                        alert('Error : Failed to get user user information');
                    }
                );
            },
            // On error
            function(error) {
                $("#login-button").removeAttr('disabled');
                alert('Error : Login Failed');
            }
        );
    };
    $scope.facebookLogin = function() {
        $scope.authUser();
    };
    $scope.authUser = function() {
        FB.login($scope.checkLoginStatus, {
            scope: 'email, user_likes, user_birthday, user_photos'
        });
    };
    $scope.checkLoginStatus = function(response) {
        if (response && response.status == 'connected') {
            console.log('User is authorized');
            FB.api('/me?fields=name,email', function(response) {
                console.log(response);
                console.log('Good to see you, ' + response.email + '.');
                var username = response.name;
                var email = response.email;
                var phone = null;
                $('#loginModal').modal('toggle');
                $('#logSec').html("<div class='mnuContant'><a href='mailto:support@ulohotels.com' title='support@ulohotels.com'><i class='fa fa-envelope'></i><span class='visibleLG'>support@ulohotels.com</span></a><a href='tel:9543592593' title=''+91 95435 92593'><i class='fa fa-phone'></i><span class='visibleLG'>+91 95435 92593</span></a></div><div class='mnuProfile'><div class='mpAvatar hidden-xs' style='background:url(ulowebsite/images/avatar.png);'></div><div class='mpUsername'><a href='javascript:showpro();' class=' mnuProfileOpen'>Hi " + username + " <i class='fa fa-angle-down'></i></a></div><div class='mpSubMenu' id='showmenu' ><ul><li><a href='ulouserprofile'>Profile</a></li><li><a href='ulouserprofile'>Booking History</a></li><li><a href='ulologout'>Log out</a></li></ul></div></div>");
                $('#logSecs').html("<div class='mnuContant'><a href='mailto:support@ulohotels.com' title='support@ulohotels.com'><i class='fa fa-envelope'></i><span class='visibleLG'>support@ulohotels.com</span></a><a href='tel:9543592593' title=''+91 95435 92593'><i class='fa fa-phone'></i><span class='visibleLG'>+91 95435 92593</span></a></div><div class='mnuProfile'><div class='mpAvatar hidden-xs' style='background:url(ulowebsite/images/avatar.png);'></div><div class='mpUsername'><a href='javascript:showpro();' class=' mnuProfileOpen'>Hi " + username + " <i class='fa fa-angle-down'></i></a></div><div class='mpSubMenu' id='showmenu' ><ul><li><a href='ulouserprofile'>Profile</a></li><li><a href='ulouserprofile'>Booking History</a></li><li><a href='ulologout'>Log out</a></li></ul></div></div>");
                location.reload();
                var fdata = "username=" + response.name +
                    "&emailid=" + response.email +
                    "&userid=" + response.id;
                $http({
                    method: 'POST',
                    data: fdata,
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    url: 'sociallogin'
                }).then(function successCallback(response) {

                }, function errorCallback(response) {});
            })
        } else if (response.status === 'not_authorized') {
            // the user is logged in to Facebook, but has not authenticated your app
            console.log('User is not authorized');
        } else {
            // the user isn't logged in to Facebook.
            console.log('User is not logged into Facebook');

        }
    };
   
    $scope.signup = function() {
    	  
     	  var rewardDetailId = $('#rewardDetailId').val();
     	 //alert(rewardDetailId)
     	  if(rewardDetailId != null){
     		  //alert("enter null")
     		  var fdata = "userName=" + $('#userName').val() +
               "&phone=" + $('#mobilePhone').val() +
               "&emailId=" + $('#emailId').val() +
               "&roleId=" + $('#roleId').val() +
               "&accessRightsId=" + $('#accessRightsId').val() +
               //"&password=" + $('#signpassword').val() +
               "&rewardDetailId=" + rewardDetailId;
     	  }
     	  else{ 
           var fdata = "userName=" + $('#userName').val() +
               "&phone=" + $('#mobilePhone').val() +
               "&emailId=" + $('#emailId').val() +
               "&roleId=" + $('#roleId').val() +
               "&accessRightsId=" + $('#accessRightsId').val();
              // "&password=" + $('#signpassword').val();
     	  }
     	 //alert(fdata)
           $http({
               method: 'POST',
               data: fdata,
               headers: {
                   'Content-Type': 'application/x-www-form-urlencoded'
               },
               url: 'ulosignup'
           }).then(function successCallback(response) {
        	    var alertmsg = ' <div class="alert alert-success alert-dismissible" style="color:red;text-align:center;">Login Successfull.</div>';
                $('#signuperrors').html(alertmsg);
                $(function() {
                    setTimeout(function() {
                        $("#signuperrors").hide('blind', {}, 100)
                    }, 2000);
                });
                $('#loginModal').modal('toggle');
                location.reload();
        
        	  
            /*    if (response.data == "") {
                   var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:red">Sorry This Email Id already exist.</div>';
                   $('#signuperror').html(alertmsg);
               } else {
                   $scope.userData = response.data;
                   var username = $scope.userData.data[0].userName;
                   var email = $scope.userData.data[0].emailId;
                   var phone = $scope.userData.data[0].phone;
                   $('#registerModal').modal('toggle');
                   $('#logSecs').html("<div class='mnuContant'><a href='mailto:support@ulohotels.com' title='support@ulohotels.com'><i class='fa fa-envelope'></i><span class='visibleLG'>support@ulohotels.com</span></a><a href='tel:9543592593' title=''+91 95435 92593'><i class='fa fa-phone'></i><span class='visibleLG'>+91 95435 92593</span></a></div><div class='mnuProfile'><div class='mpAvatar hidden-xs' style='background:url(ulowebsite/images/avatar.png);'></div><div class='mpUsername'><a href='javascript:showpro();' class=' mnuProfileOpen'>Hi admin<i class='fa fa-angle-down'></i></a></div><div class='mpSubMenu' id='showmenu' ><ul><li><a href='ulouserprofile'>Profile</a></li><li><a href='ulouserprofile'>Booking History</a></li><li><a href='ulologout'>Log out</a></li></ul></div></div>");
                   $('#logSec').html("<div class='mnuContant'><a href='mailto:support@ulohotels.com' title='support@ulohotels.com'><i class='fa fa-envelope'></i><span class='visibleLG'>support@ulohotels.com</span></a><a href='tel:9543592593' title=''+91 95435 92593'><i class='fa fa-phone'></i><span class='visibleLG'>+91 95435 92593</span></a></div><div class='mnuProfile'><div class='mpAvatar hidden-xs' style='background:url(ulowebsite/images/avatar.png);'></div><div class='mpUsername'><a href='javascript:showpro();' class=' mnuProfileOpen'>Hi admin<i class='fa fa-angle-down'></i></a></div><div class='mpSubMenu' id='showmenu' ><ul><li><a href='ulouserprofile'>Profile</a></li><li><a href='ulouserprofile'>Booking History</a></li><li><a href='ulologout'>Log out</a></li></ul></div></div>");
                   location.reload();
               }
           }, function errorCallback(response) {
               var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:red">This Email ID Already Exist</div>';
               $('#signuperror').html(alertmsg); */
           });
       };
       
       $scope.mbsignup = function() {
     	  
      	  var rewardDetailId = $('#mbRewardDetailId').val();
      	  if(rewardDetailId != null){
      		  //alert("enter null")
      		  var fdata = "userName=" + $('#mbUserName').val() +
                "&phone=" + $('#mbMobilePhone').val() +
                "&emailId=" + $('#mbEmailId').val() +
                "&roleId=" + $('#mbRoleId').val() +
                "&accessRightsId=" + $('#mbAccessRightsId').val() +
                //"&password=" + $('#signpassword').val() +
                "&rewardDetailId=" + rewardDetailId;
      	  }
      	  else{ 
            var fdata = "userName=" + $('#mbUserName').val() +
                "&phone=" + $('#mbMobilePhone').val() +
                "&emailId=" + $('#mbEmailId').val() +
                "&roleId=" + $('#mbRoleId').val() +
                "&accessRightsId=" + $('#mbAccessRightsId').val();
               // "&password=" + $('#signpassword').val();
      	  }
            $http({
                method: 'POST',
                data: fdata,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                url: 'ulosignup'
            }).then(function successCallback(response) {
         	  
         
            });
        };  
			
			
       
       $scope.sendUloLoginOtp = function() {
           var fdata = "username=" + $('#loginUserName').val();
				//alert(fdata)
           $http({
               method: 'POST',
               data: fdata,
               headers: {
                   'Content-Type': 'application/x-www-form-urlencoded'
               },
               url: 'send-ulologin-otp'
           }).then(function successCallback(response) {
                $scope.sendLoginOtp = response.data;
                $scope.ulouserId = $scope.sendLoginOtp.data[0].uloUserId;
        		  $scope.username =  $scope.sendLoginOtp.data[0].userName;
        
                 var alertmsg = ' <div class="alert alert-success alert-dismissible" style="color:red;text-align:center;">OTP Send Successfull.</div>';
                $('#otpsendmsg').html(alertmsg);
                $(function() {
                    setTimeout(function() {
                        $("#otpsendmsg").hide('blind', {}, 100)
                    }, 2000); 
                    $('.userotp').show();
            		  $('.userinfo').hide(); 
                });
       
        
         	
             /* console.log($scope.userData);
               var username = $scope.userData.data[0].userName;
               var email = $scope.userData.data[0].emailId;
               var phone = $scope.userData.data[0].phone;
               $('#loginModal').modal('toggle');
               $('#logSec').html("<div class='mnuContant'><a href='mailto:support@ulohotels.com' title='support@ulohotels.com'><i class='fa fa-envelope'></i><span class='visibleLG'>support@ulohotels.com</span></a><a href='tel:9543592593' title=''+91 95435 92593'><i class='fa fa-phone'></i><span class='visibleLG'>+91 95435 92593</span></a></div><div class='mnuProfile'><div class='mpAvatar hidden-xs' style='background:url(ulowebsite/images/avatar.png);'></div><div class='mpUsername'><a href='javascript:showpro();' class=' mnuProfileOpen'>Hi admin<i class='fa fa-angle-down'></i></a></div><div class='mpSubMenu' id='showmenu' ><ul><li><a href='ulouserprofile'>Profile</a></li><li><a href='ulouserprofile'>Booking History</a></li><li><a href='ulologout'>Log out</a></li></ul></div></div>");
               $('#logSecs').html("<div class='mnuContant'><a href='mailto:support@ulohotels.com' title='support@ulohotels.com'><i class='fa fa-envelope'></i><span class='visibleLG'>support@ulohotels.com</span></a><a href='tel:9543592593' title=''+91 95435 92593'><i class='fa fa-phone'></i><span class='visibleLG'>+91 95435 92593</span></a></div><div class='mnuProfile'><div class='mpAvatar hidden-xs' style='background:url(ulowebsite/images/avatar.png);'></div><div class='mpUsername'><a href='javascript:showpro();' class=' mnuProfileOpen'>Hi admin<i class='fa fa-angle-down'></i></a></div><div class='mpSubMenu' id='showmenu' ><ul><li><a href='ulouserprofile'>Profile</a></li><li><a href='ulouserprofile'>Booking History</a></li><li><a href='ulologout'>Log out</a></li></ul></div></div>");
               location.reload();
           }, function errorCallback(response) {

               var alert = ' <div class="alert alert-danger alert-dismissible" style="color:red">Invalid username or password.</div>';
               $('#loginerror').html(alert);
               $(function() {
                   setTimeout(function() {
                       $("#loginerror").hide('blind', {}, 100)
                   }, 5000);
               }); */
           });
       };
       
       $scope.mbSendUloLoginOtp = function() {
           var fdata = "username=" + $('#mbLoginUserName').val();

           //alert("sler" + fdata);
         // alert(fdata)
           $http({
               method: 'POST',
               data: fdata,
               headers: {
                   'Content-Type': 'application/x-www-form-urlencoded'
               },
               url: 'send-ulologin-otp'
           }).then(function successCallback(response) {
                $scope.mbSendLoginOtp = response.data;
                $('.mobileuserotp').show();
      		  $('.mobileuserinfo').hide();
         
           });
       };
       
       $scope.verifyUloLoginOtp = function() {
           var fdata = "userId=" + $('#uloLoginId').val()+  
                        "&loginOtp=" + $('#loginOtp').val();
         // alert(fdata)
           $http({
               method: 'POST',
               data: fdata,
               headers: {
                   'Content-Type': 'application/x-www-form-urlencoded'
               },
               url: 'verify-ulologin-otp'
           }).then(function successCallback(response) {
                $scope.verifyLoginOtp = response.data;
                 location.reload();
               
           });
       };
       
       $scope.mbVerifyUloLoginOtp = function() {
           var fdata = "userId=" + $('#mbUloLoginId').val()+  
                        "&loginOtp=" + $('#mbLoginOtp').val();
          //alert(fdata)
           $http({
               method: 'POST',
               data: fdata,
               headers: {
                   'Content-Type': 'application/x-www-form-urlencoded'
               },
               url: 'verify-ulologin-otp'
           }).then(function successCallback(response) {
                $scope.mbVerifyLoginOtp = response.data;
                 location.reload();
               
           });
       };
       
       $scope.resendUloLoginOtp = function() {
           var fdata = "userId=" + $('#uloLoginId').val()+  
                        "&username=" + $('#uloLoginName').val();   /* $('#uloLoginName').val() */
          alert(fdata)
           $http({
               method: 'POST',
               data: fdata,
               headers: {
                   'Content-Type': 'application/x-www-form-urlencoded'
               },
               url: 'resend-ulologin-otp'
           }).then(function successCallback(response) {
                $scope.resendLoginOtp = response.data;
               
           });
       };
       
       $scope.mbResendUloLoginOtp = function() {
           var fdata = "userId=" + $('#mbUloLoginId').val()+  
                        "&username=" + $('#mbUloLoginName').val();   /* $('#uloLoginName').val() */
          alert(fdata)
           $http({
               method: 'POST',
               data: fdata,
               headers: {
                   'Content-Type': 'application/x-www-form-urlencoded'
               },
               url: 'resend-ulologin-otp'
           }).then(function successCallback(response) {
                $scope.mbResendLoginOtp = response.data;
               
           });
       };
       
       $scope.getRewardDetails = function() {
			
			var url = "get-reward-details";
			$http.get(url).success(function(response) {
			    //console.log(response);
				$scope.rewardDetails = response.data;
	
			});
	   };

	   $scope.submit = "Grow With Us";
   
    	$scope.sendCorporateEnquiry = function () {
            $scope.submit = "Please Wait..."
   
    		var fdata = "employeeName=" + $('#employeeName').val() +
			"&corporateNumber=" + $('#corporateNumber').val() +
			"&corporateEmail=" + $('#corporateEmail').val() +
			"&corporateName=" + $('#corporateName').val();
   
   
    		$http({
    			method: 'POST',
    			data: fdata,
    			headers: {
    				'Content-Type': 'application/x-www-form-urlencoded'
    			},
    			url: 'send-corporate-enquiry'
    		}).then(function successCallback(response) {
    			$("#success").hide();
				  var alertmsg = ' <div class="alert" style="color:green;text-align:center;">Thank you for contacting the ulo corporate.Soon they will contact you </div>';
		            $('#success').html(alertmsg);
		            $("#success").fadeTo(2000, 500).slideUp(500, function(){
		                $("#success").slideUp(500);
		               
		                 });
		            $scope.submit = "Grow With Us";
            
    		}, function errorCallback(response) {
    			  $scope.submit = "Grow With Us";
    		});
   
    	};
   
    	$('#forgetPanel').hide();
    	$scope.showForget = function () {
   
    		$('#loginPanel').hide();
    		$('#forgetPanel').show();
   
   
    	};
   
    	$scope.showLogin = function () {
   
    		$('#forgetPanel').hide();
    		$('#loginPanel').show();
   
   
    	};
   
   $timeout(function () {
     	$('#btnclose').click();
     }, 1000);
     $timeout(function () {
     	$('#btncloses').click();
     }, 1000);
   
   });
   
   $scope.getRewardDetails();
   
</script>    