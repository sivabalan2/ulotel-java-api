<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="s" uri="/struts-tags"%>

  
   <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
             Promotion Images
          </h1>
          <ol class="breadcrumb">
            <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
   
            <li class="active">Promotion Images</li>
          </ol>
        </section>
		
        <!-- Main content -->
        <section class="content">
        
        	<div class="row">
            	<div class="col-xs-12" >
						<div class="box">
							<div class="box-header">
								<h3 class="box-title"> Add Promotion Images</h3>
								<p style="color:red">1. The Image Size Must Be Width of 600px and height of 100px</p>
								<p style="color:red">2. The Image Size Must Be In .png Format</p>
								<p style="color:red">3. The User Can Add only Two images For Promotion</p>
							</div>
							
							
							<!-- /.box-header -->
							<div class="box-body table-responsive no-padding" > 
					      <div class="col-md-6" ng-repeat="pm in promotionImages" >
                              <div class="box box-solid panel panel-default ">
                                 <div class="box-header with-border">
                                    <h3 class="box-title">	
                                     				                 
                                       <input type="radio" name="promotionPhotoId" value=" " class="flat-red">
                                    </h3>
                                     <a href="#" ng-click="deletePromotionImage(pm.DT_RowId)"  data-toggle="modal"><i  class="fa fa-fw fa-trash text-red"></i></a> 
                                 </div>
                                 <!-- /.box-header -->
                                 <div class="box-body">
                                     <img class="attachment-img img-responsive" id="promotionImage" src="get-promotion-image-stream?promotionImageId={{pm.DT_RowId}}"  alt="attachment image"/> 
                                 </div>
                                 <div class="box-body">
                                     <input type="text" class="form-control" value="{{pm.photoPathUrl}}" id="promotionImageUrl" />
                                     <button type="button" class="btn btn-primary btngreen pull-right" ng-click="savePhotoPathUrl(pm.DT_RowId)" />save</button>
                                 </div>
                                 <!-- /.box-body -->
                              </div>
                              <!-- /.box -->
                           </div>
							</div>
							<!-- /.box-body -->
							
							
							<div class="box-footer clearfix">
								 <ul class="pagination pagination-sm no-margin pull-right">
								 <button  ngf-select="upload($file)" ngf-pattern="'.jpg,.png,!.gif'"  accept=".png,.jpg,.jpeg" class="btn btn-primary btngreen pull-right">Add Images</button>
								</ul> 
							</div>
						</div>
						<!-- /.box -->
					</div>	
         	</div>
          
        </section><!-- /.content -->

			<!-- /.modal-dalog -->
		</div>
		
		
  

      
          


<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>

	<script>

		var app = angular.module('myApp',['ngFileUpload']);
		app.controller('customersCtrl',['$scope',
		             					'Upload',
		            					'$timeout',
		            					'$http', function($scope,Upload,$timeout,$http) {

			
			
			$("#alert-message").hide();
			
			var spinner = $('#loadertwo');
			
			//$scope.progressbar = ngProgressFactory.createInstance();
			//$scope.progressbar.setColor("green");
		    //$scope.progressbar.start();
			
	       	$timeout(function(){
	      	    // $scope.progressbar.complete();
	            $scope.show = true;
	            $("#pre-loader").css("display","none");
	        }, 2000);
	       	
	       	$scope.unread = function() {
				var notifiurl = "unreadnotifications.action";
				$http.get(notifiurl).success(function(response) {
					$scope.latestnoti = response.data;
				});
				};

				
				
				
				$scope.getPromotionImages = function() {
					//var userid = document.getElementById("userId").value;
					var url = "get-promotion-images";
					$http.get(url).success(function(response) {
						//console.log(response);
						//alert(JSON.stringify(response.data));
						$scope.promotionImages = response.data;
					
					});
				};
				
				
				$scope.deletePromotionImage = function(rowid) {
		   			
					var check = confirm("Are you sure you want to delete this Image?");
					if( check == true){
		               
		   			var url = "delete-promotion-image?promotionImageId="+rowid;
		   			$http.get(url).success(function(response) {
		   			    //console.log(response);
		   			    $scope.getPromotionImages();
		   			    $('#deletemodal').modal('toggle');
		   			    $timeout(function(){
		   			      	$('#deletebtnclose').click();
		   			    }, 2000);
		   				
		   	
		   			});
					}
		   			
		   		};
						
				
				
				
		   		
	
		       	// upload on file select or drop
		        $scope.upload = function (file) { 
		            spinner.show();
		            Upload.upload({
		                url: 'add-promotion-image',
		                enableProgress: true, 
		                data: {myFile: file}
		           
		            }).then(function (resp) {
		            	
		            	
		            	   setTimeout(function(){ spinner.hide(); 
		            	   }, 1000);

		            	$scope.getPromotionImages();
		            	
		               
		            	 //alert('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
		                //$scope.getPropertyPhotos();
		            }, 
						function errorCallback(resp) {
		            	   setTimeout(function(){ spinner.hide(); 
		            	   }, 1000);

						alert("The Images Does Not In Exact Size or Format");
						// called asynchronously if an error occurs
						// or server returns response with an error status.
					 });	
		        		/* }
		        	 
		        	 else{
		        		alert("upload corrwect size");
		        	}  */
		        	
		        	 
		        	
		            
		        };
		        
				$scope.savePhotoPathUrl = function(rowid) {	   			
		      		
	      			
	      			var fdata="promotionImageId="+ rowid
	      			 + "&promotionImageUrl=" +$('#promotionImageUrl').val();
	      			/* +"&propertyCancellationPolicy="+ $('#propertyCancellationPolicy').val()
	      				+ "&propertyChildPolicy=" +$('#propertyChildPolicy').val(); */
	      			spinner.show();
	      			$http(
	      					{
	      						method : 'POST',
	      						data : fdata,
	      						headers : {
	      							'Content-Type' : 'application/x-www-form-urlencoded'
	      						},
	      						url : 'save-promotion-image-url'
	      					}).then(function successCallback(response) {
	      						
	      						 
	      						$scope.getPromotionImages();
	      					   setTimeout(function(){ spinner.hide(); 
	      					  }, 1000);
	      						//$("#alert-message").show();
	      				
	      			}, function errorCallback(response) {
	      			   setTimeout(function(){ spinner.hide(); 
	      			  }, 1000);
	      			}); 
	      		}
		       
			//$scope.unread();
		        //$scope.editFamilyUser(); 

		        $scope.getPromotionImages(); 
		}]);
		
	
	</script>
	
	
	<script src="js1/upload/ng-file-upload-shim.min.js"></script>
	<script src="js1/upload/ng-file-upload.min.js"></script>
	<script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.js"></script>
	
  
    

	 
       