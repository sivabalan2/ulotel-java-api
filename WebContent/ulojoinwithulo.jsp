
  <%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
   pageEncoding="ISO-8859-1"%>
  <section class="partnerbanner">
  <div class="container">
  <div class="row">
  <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
  <h1>PARTNER WITH US</h1>
  <h2>FOR EXCEPTIONAL GROWTH & GREAT GUEST EXPERIENCE</h2>
  <img src="contents/images/partner/banner.png" class="img-responsive">
  </div>
  </div>
  </div>
</section>
  <section class="whypartner">
  <div class="container">
  <div class="row">
  <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
  <h3>WHY PARTNER WITH ULO</h3>
</div>
 <div class="col-lg-1 col-md-1 hidden-sm hidden-xs">

</div>
 <div class="col-lg-2 col-md-2 col-xs-4 col-sm-4">
  <img src="contents/images/partner/why1.png">
  <h4>Assured Revenue</h4>
</div>
 <div class="col-lg-2 col-md-2 col-xs-4 col-sm-4">
  <img src="contents/images/partner/why4.png">
  <h4>Quality Maintainance</h4>
  <h4></h4>
</div>
 <div class="col-lg-2 col-md-2 col-xs-4 col-sm-4">
  <img src="contents/images/partner/why5.png">
  <h4>Constant revenue management</h4>
</div>
 <div class="col-lg-2 col-md-2 col-xs-6 col-sm-6">
  <img src="contents/images/partner/why3.png">
  	<h4>Professional Highly trained staffs</h4>
</div>
 <div class="col-lg-2 col-md-2 col-xs-6 col-sm-6">
  <img src="contents/images/partner/why2.png">
  <h4>Guarennted occupancy</h4>
</div>
 <div class="col-lg-1 col-md-1 hidden-sm hidden-xs">

</div>

  </div>
  </div>
</section>
<section class="partnerplan">
<div class="container">
<div class="row">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
<h3>CONTRACT PLANS</h3>
</div>
<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 ">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 whiteboard">
<img src="contents/images/partner/mg.png" class="img-responsive">
<h4>Minimum Guarantee</h4>
</div>
<div class="col-lg-12 whiteboardsecond">
	<h5>Monthly and Annual Plans</h5>
<h5>Guaranteed Revenue to Hoteliers</h5>
<h5>Guaranteed Occupancy Rate</h5>
<h5>55% of Assured Revenue (approx)</h5>
<h5>Dedicated Technical Support</h5>
<h5>Review Management</h5>
<h5>Staff Deployment</h5>
<h5>Free Toiletries, Runners & Throw Pillows</h5>
<h5>Property Branding</h5>
</div>
</div>
<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" >
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 whiteboard">
<img src="contents/images/partner/rs.png" class="img-responsive">
<h4>Revenue Share</h4>
</div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 whiteboardsecond">
<h5>Monthly Target Basis</h5>
	<h5>Minimum 40% Occupancy Rate</h5>
	<h5>Guaranteed Revenue to Hoteliers</h5>
	<h5>Dedicated Technical Support</h5>
	<h5>Review Management</h5>
	<h5>Staff Deployment</h5>
	<h5>Free Toiletries, Runners & Throw Pillows</h5>
	<h5>Property Branding</h5>
</div>
</div>
<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 ">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 whiteboard">
<img src="contents/images/partner/mv.png" class="img-responsive">
<h4>Ulo Crown</h4>
</div>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 whiteboardsecond">
<h5>Complete Property Management</h5>
<h5>Assured Revenue Share to Hoteliers (min 30%)</h5>
<h5>Complete Staff Deployment & Training</h5>
<h5>Guaranteed Occupancy Rate</h5>
<h5>Dedicated Technical Support</h5>
<h5>Review Management</h5>
<h5>Free Toiletries, Runners & Throw Pillows</h5>
<h5>Property Branding</h5>
</div>
</div>
</div>
</div>
</section>
<section class="partnergrowth">
<div class="container">
<div class="row">
<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
<h3>BUSSINESS GROWTH STRATEGY</h3>
</div>
<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
<img src="contents/images/partner/b5.png" class="img-responsive pull-right">
</div>
</div>
<div class="row">
<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
<img src="contents/images/partner/b1.png" class="img-responsive">
<h4>1st -3 Months- <b>15% - 20 %</b> </h4>
</div>
<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
<img src="contents/images/partner/b2.png" class="img-responsive">
<h4>1st -6 Months- <b>20% - 35 %</b> </h4>
</div>
<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
<img src="contents/images/partner/b3.png" class="img-responsive">
<h4>1 Year- <b>35% - 50%</b> </h4>
</div>
<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
<img src="contents/images/partner/b3.png" class="img-responsive">
<h4>Year On Year Growth <b> 20% Increase</b> </h4>
</div>

</div>
</div>
</section>
<section class="partnerreview">
<div class="container">
<div class="row">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
<h3>PARTNER TESTIMONIALS</h3>
</div>

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
<img src="contents/images/partner/user.png" class="img-responsive">
</div>
<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
<h5>Karthick</h5>
Ulo Spacio...!! Stayed in Ulo Spacio hotel. Rooms are very neat,clean and noiseless atmosphere. Also this hotel is very near to most of IT companies in OMR. Ulo Spacio staffs are very friendly
</div>
</div>
<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
<img src="contents/images/partner/user.png" class="img-responsive">
</div>
<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
<h5>Karthick</h5>
Ulo Spacio...!! Stayed in Ulo Spacio hotel. Rooms are very neat,clean and noiseless atmosphere. Also this hotel is very near to most of IT companies in OMR. Ulo Spacio staffs are very friendly
</div>
</div>
<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
<img src="contents/images/partner/user.png" class="img-responsive">
</div>
<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
<h5>Karthick</h5>
Ulo Spacio...!! Stayed in Ulo Spacio hotel. Rooms are very neat,clean and noiseless atmosphere. Also this hotel is very near to most of IT companies in OMR. Ulo Spacio staffs are very friendly
</div>
</div>

</div>

</div>
</div>
</section>
<section class="partnerform">
<div class="container">
<div class="row">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 partnerjoin">
<h3>Join us</h3>
<p>
"We are en route, expanding our base across 
the country and are looking for partners who share the same 
passion of offering great customer experience in budget stays.
If you want to be a part of us and looking for great growth in 
your business, kindly write to us using the form below"
</p>
</div>

<div class="col-lg-6 col-md-6 hidden-sm hidden-xs partnertouch">
<h5>Get in Touch</h5>
<form name="partnerForm" id="partnerForm">
   <div class="col-lg-6 form-group">
                    <input type="text" class="form-control" id="name" placeholder="Name" required/>
                </div>
                <div class="col-lg-6 form-group">
                    <input type="text" class="form-control" id="phone" placeholder="Mobile" required/>
                </div>
                    <div class="col-lg-12 form-group">
                    <input type="email" class="form-control" id="email" placeholder="Email" required/>
                </div>
                    <div class="col-lg-12 form-group">
                    <input type="text" class="form-control" id="hotelName" placeholder="Hotel name" required/>
                </div>
                    <div class="col-lg-12 form-group">
                    <input type="text" class="form-control" id="location" placeholder="Location" required/>
                </div>
                     <div class="col-lg-12 form-group">
                    <textarea id="message" class="form-control"></textarea>
                </div>
                  <div class="form-group">
  <div id="success"></div>
  </div>
                      <div class="col-lg-12 form-group">
                    <button class="btn growbtn" type="button" ng-click="sendPatnerEmail()">{{dsubmit}}</button>
                </div>
                
                
</form>
</div>
<!-- mobile form -->
<div class="hidden-lg hidden-md col-sm-12 col-xs-12 ">
<!-- Trigger the modal with a button -->
<button type="button" class="btn growbtn" style="display:block;margin:0 auto;"data-toggle="modal" data-target="#myModal">Contact Us</button>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Get in Touch</h4>
      </div>
      <div class="modal-body">
        
<form name="partnerForm">
   <div class="col-lg-6 form-group">
                    <input type="text" class="form-control" id="name" placeholder="Name" required/>
                </div>
                <div class="col-lg-6 form-group">
                    <input type="text" class="form-control" id="mphone" placeholder="Mobile" required/>
                </div>
                    <div class="col-lg-12 form-group">
                    <input type="email" class="form-control" id="memail" placeholder="Email" required/>
                </div>
                    <div class="col-lg-12 form-group">
                    <input type="text" class="form-control" id="mhotelName" placeholder="Hotel name" required/>
                </div>
                    <div class="col-lg-12 form-group">
                    <input type="text" class="form-control" id="mlocation" placeholder="Location" required/>
                </div>
                     <div class="col-lg-12 form-group">
                    <textarea id="mmessage" class="form-control"></textarea>
                </div>
                                  <div class="form-group">
  <div id="successmb"></div>
  </div>
                      <div class="col-lg-12 form-group">
                    <button class="btn growbtn" type="button" ng-click="sendMbPatnerEmail()">{{msubmit}}</button>
                </div>
                
                
</form>
      </div>
    </div>

  </div>
</div>
</div>

</div>
</div>
</div>
</section>
     <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script src="js1/ngprogress.js" type="text/javascript"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script> 
<script>
   var app = angular.module('myApp', ['ngProgress']);
   app.controller('customersCtrl', function ($scope, $http, $timeout, ngProgressFactory) {
    $scope.userData = [];
   
    $('.userotp').hide();
	  $scope.dofferlogin = true;
	  $('.mobileuserotp').hide();
	  $scope.showDprofile = false;
		$scope.showDlogin = true;
		$scope.showMprofile = false;
		$scope.showMlogin = true;
	    $scope.dsignin = "Submit";

  $scope.signup = function() {
	  
	   $(function() {
           setTimeout(function() {
        	   $scope.dsignin = "Please Wait...";
           }, 1000);
       });
 	  var rewardDetailId = $('#rewardDetailId').val();
 // alert(rewardDetailId)
 	  if(rewardDetailId != null){
 		  //alert("enter null")
 		  var fdata = "userName=" + $('#userName').val() +
           "&phone=" + $('#mobilePhone').val() +
           "&emailId=" + $('#emailId').val() +
           "&roleId=" + $('#roleId').val() +
           "&accessRightsId=" + $('#accessRightsId').val() +
           //"&password=" + $('#signpassword').val() +
           "&rewardDetailId=" + rewardDetailId;
 	  }
 	  else{ 
       var fdata = "userName=" + $('#userName').val() +
           "&phone=" + $('#mobilePhone').val() +
           "&emailId=" + $('#emailId').val() +
           "&roleId=" + $('#roleId').val() +
           "&accessRightsId=" + $('#accessRightsId').val();
          // "&password=" + $('#signpassword').val();
 	  }
 	 //alert(fdata)
 	 
       $http({
           method: 'POST',
           data: fdata,
           headers: {
               'Content-Type': 'application/x-www-form-urlencoded'
           },
           url: 'ulosignup'
       }).then(function successCallback(response) {
    	    var alertmsg = ' <div class="alert alert-success alert-dismissible" style="color:red;text-align:center;">Login Successfull.</div>';
            $('#signuperrors').html(alertmsg);
            $(function() {
                setTimeout(function() {
                    $("#signuperrors").hide('blind', {}, 100)
                }, 2000);
            });
          //  $('#loginModal').modal('toggle');
            //location.reload();
           $('#myModal').modal('toggle');

           $scope.dsignin = "Submit";
           $scope.showDprofile = true;
           $scope.showDlogin = false;
     	  $scope.dofferlogin = false;
  
       });
   };
   $scope.msignin = "Submit";
   $scope.mbsignup = function() {
	   $scope.msignin = "Please Wait";
  	  var rewardDetailId = $('#mbRewardDetailId').val();
  	  if(rewardDetailId != null){
  		  //alert("enter null")
  		  var fdata = "userName=" + $('#mbUserName').val() +
            "&phone=" + $('#mbMobilePhone').val() +
            "&emailId=" + $('#mbEmailId').val() +
            "&roleId=" + $('#mbRoleId').val() +
            "&accessRightsId=" + $('#mbAccessRightsId').val() +
            //"&password=" + $('#signpassword').val() +
            "&rewardDetailId=" + rewardDetailId;
  	  }
  	  else{ 
        var fdata = "userName=" + $('#mbUserName').val() +
            "&phone=" + $('#mbMobilePhone').val() +
            "&emailId=" + $('#mbEmailId').val() +
            "&roleId=" + $('#mbRoleId').val() +
            "&accessRightsId=" + $('#mbAccessRightsId').val();
           // "&password=" + $('#signpassword').val();
  	  }
        $http({
            method: 'POST',
            data: fdata,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            url: 'ulosignup'
        }).then(function successCallback(response) {
        	
        	 $('#mymobileModal').modal('toggle');

             
             $scope.showMprofile = true;
             $scope.showMlogin = false;
             $scope.msignin = "Submit";
     
        });
    };  
		
  $scope.handleGoogleApiLibrary = function() {
      gapi.load('client:auth2', {
          callback: function() {
              gapi.client.init({
                  apiKey: 'AIzaSyAuJwG_DuX3eEXTgsoEY9A8LRvRVfeilFs',
                  clientId: '106759720549-tfe9i1b16ovvpqn8aho6hooiaftt2h4n.apps.googleusercontent.com',
                  scope: 'https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/plus.me'
              }).then(
                  // On success
                  function(success) {
                      $("#login-button").removeAttr('disabled');
                  },
                  // On error
                  function(error) {
                      // alert('Error : Failed to Load Library');
                  }
              );
          },
          onerror: function() {
              // Failed to load libraries
          }
      });
  };
  $scope.googleLogin = function() {
      gapi.auth2.getAuthInstance().signIn().then(
          function(success) {
              gapi.client.request({
                  path: 'https://www.googleapis.com/plus/v1/people/me'
              }).then(
                  function(success) {
                      var user_info = JSON.parse(success.body);
                      var username = user_info.displayName;
                      var email = user_info.emails[0].value;
                      var phone = null;
                      $('#loginModal').modal('toggle');
                      $('#logSec').html("<div class='mnuContant'><a href='mailto:support@ulohotels.com' title='support@ulohotels.com'><i class='fa fa-envelope'></i><span class='visibleLG'>support@ulohotels.com</span></a><a href='tel:9543592593' title=''+91 95435 92593'><i class='fa fa-phone'></i><span class='visibleLG'>+91 95435 92593</span></a></div><div class='mnuProfile'><div class='mpAvatar hidden-xs' style='background:url(ulowebsite/images/avatar.png);'></div><div class='mpUsername'><a href='javascript:showpro();' class=' mnuProfileOpen'>Hi " + username + " <i class='fa fa-angle-down'></i></a></div><div class='mpSubMenu' id='showmenu' ><ul><li><a href='ulouserprofile'>Profile</a></li><li><a href='ulouserprofile'>Booking History</a></li><li><a href='ulologout'>Log out</a></li></ul></div></div>");
                      $('#logSecs').html("<div class='mnuContant'><a href='mailto:support@ulohotels.com' title='support@ulohotels.com'><i class='fa fa-envelope'></i><span class='visibleLG'>support@ulohotels.com</span></a><a href='tel:9543592593' title=''+91 95435 92593'><i class='fa fa-phone'></i><span class='visibleLG'>+91 95435 92593</span></a></div><div class='mnuProfile'><div class='mpAvatar hidden-xs' style='background:url(ulowebsite/images/avatar.png);'></div><div class='mpUsername'><a href='javascript:showpro();' class=' mnuProfileOpen'>Hi " + username + " <i class='fa fa-angle-down'></i></a></div><div class='mpSubMenu' id='showmenu' ><ul><li><a href='ulouserprofile'>Profile</a></li><li><a href='ulouserprofile'>Booking History</a></li><li><a href='ulologout'>Log out</a></li></ul></div></div>");
                      location.reload();
                      var fdata = "username=" + user_info.displayName +
                          "&emailid=" + user_info.emails[0].value +
                          "&userid=" + user_info.id;
                      $http({
                          method: 'POST',
                          data: fdata,
                          headers: {
                              'Content-Type': 'application/x-www-form-urlencoded'
                          },
                          url: 'sociallogin'
                      }).then(function successCallback(response) {}, function errorCallback(response) {

                      });
                  },
                  // On error
                  function(error) {
                      $("#login-button").removeAttr('disabled');
                      alert('Error : Failed to get user user information');
                  }
              );
          },
          // On error
          function(error) {
              $("#login-button").removeAttr('disabled');
              alert('Error : Login Failed');
          }
      );
  };
  $scope.facebookLogin = function() {
      $scope.authUser();
  };
  $scope.authUser = function() {
      FB.login($scope.checkLoginStatus, {
          scope: 'email, user_likes, user_birthday, user_photos'
      });
  };
  $scope.checkLoginStatus = function(response) {
      if (response && response.status == 'connected') {
          console.log('User is authorized');
          FB.api('/me?fields=name,email', function(response) {
              console.log(response);
              console.log('Good to see you, ' + response.email + '.');
              var username = response.name;
              var email = response.email;
              var phone = null;
              $('#loginModal').modal('toggle');
              $('#logSec').html("<div class='mnuContant'><a href='mailto:support@ulohotels.com' title='support@ulohotels.com'><i class='fa fa-envelope'></i><span class='visibleLG'>support@ulohotels.com</span></a><a href='tel:9543592593' title=''+91 95435 92593'><i class='fa fa-phone'></i><span class='visibleLG'>+91 95435 92593</span></a></div><div class='mnuProfile'><div class='mpAvatar hidden-xs' style='background:url(ulowebsite/images/avatar.png);'></div><div class='mpUsername'><a href='javascript:showpro();' class=' mnuProfileOpen'>Hi " + username + " <i class='fa fa-angle-down'></i></a></div><div class='mpSubMenu' id='showmenu' ><ul><li><a href='ulouserprofile'>Profile</a></li><li><a href='ulouserprofile'>Booking History</a></li><li><a href='ulologout'>Log out</a></li></ul></div></div>");
              $('#logSecs').html("<div class='mnuContant'><a href='mailto:support@ulohotels.com' title='support@ulohotels.com'><i class='fa fa-envelope'></i><span class='visibleLG'>support@ulohotels.com</span></a><a href='tel:9543592593' title=''+91 95435 92593'><i class='fa fa-phone'></i><span class='visibleLG'>+91 95435 92593</span></a></div><div class='mnuProfile'><div class='mpAvatar hidden-xs' style='background:url(ulowebsite/images/avatar.png);'></div><div class='mpUsername'><a href='javascript:showpro();' class=' mnuProfileOpen'>Hi " + username + " <i class='fa fa-angle-down'></i></a></div><div class='mpSubMenu' id='showmenu' ><ul><li><a href='ulouserprofile'>Profile</a></li><li><a href='ulouserprofile'>Booking History</a></li><li><a href='ulologout'>Log out</a></li></ul></div></div>");
              location.reload();
              var fdata = "username=" + response.name +
                  "&emailid=" + response.email +
                  "&userid=" + response.id;
              $http({
                  method: 'POST',
                  data: fdata,
                  headers: {
                      'Content-Type': 'application/x-www-form-urlencoded'
                  },
                  url: 'sociallogin'
              }).then(function successCallback(response) {

              }, function errorCallback(response) {});
          })
      } else if (response.status === 'not_authorized') {
          // the user is logged in to Facebook, but has not authenticated your app
          console.log('User is not authorized');
      } else {
          // the user isn't logged in to Facebook.
          console.log('User is not logged into Facebook');

      }
  };
  
  $scope.sendUloLoginOtp = function() {
      var fdata = "username=" + $('#loginUserName').val();
			//alert(fdata)
      $http({
          method: 'POST',
          data: fdata,
          headers: {
              'Content-Type': 'application/x-www-form-urlencoded'
          },
          url: 'send-ulologin-otp'
      }).then(function successCallback(response) {
           $scope.sendLoginOtp = response.data;
           $scope.ulouserId = $scope.sendLoginOtp.data[0].uloUserId;
   		  $scope.username =  $scope.sendLoginOtp.data[0].userName;
   
            var alertmsg = ' <div class="alert alert-success alert-dismissible" style="color:red;text-align:center;">OTP Send Successfull.</div>';
           $('#otpsendmsg').html(alertmsg);
           $(function() {
               setTimeout(function() {
                   $("#otpsendmsg").hide('blind', {}, 100)
               }, 2000); 
               $('.userotp').show();
       		  $('.userinfo').hide(); 
           });
  
   
    	
        /* console.log($scope.userData);
          var username = $scope.userData.data[0].userName;
          var email = $scope.userData.data[0].emailId;
          var phone = $scope.userData.data[0].phone;
          $('#loginModal').modal('toggle');
          $('#logSec').html("<div class='mnuContant'><a href='mailto:support@ulohotels.com' title='support@ulohotels.com'><i class='fa fa-envelope'></i><span class='visibleLG'>support@ulohotels.com</span></a><a href='tel:9543592593' title=''+91 95435 92593'><i class='fa fa-phone'></i><span class='visibleLG'>+91 95435 92593</span></a></div><div class='mnuProfile'><div class='mpAvatar hidden-xs' style='background:url(ulowebsite/images/avatar.png);'></div><div class='mpUsername'><a href='javascript:showpro();' class=' mnuProfileOpen'>Hi admin<i class='fa fa-angle-down'></i></a></div><div class='mpSubMenu' id='showmenu' ><ul><li><a href='ulouserprofile'>Profile</a></li><li><a href='ulouserprofile'>Booking History</a></li><li><a href='ulologout'>Log out</a></li></ul></div></div>");
          $('#logSecs').html("<div class='mnuContant'><a href='mailto:support@ulohotels.com' title='support@ulohotels.com'><i class='fa fa-envelope'></i><span class='visibleLG'>support@ulohotels.com</span></a><a href='tel:9543592593' title=''+91 95435 92593'><i class='fa fa-phone'></i><span class='visibleLG'>+91 95435 92593</span></a></div><div class='mnuProfile'><div class='mpAvatar hidden-xs' style='background:url(ulowebsite/images/avatar.png);'></div><div class='mpUsername'><a href='javascript:showpro();' class=' mnuProfileOpen'>Hi admin<i class='fa fa-angle-down'></i></a></div><div class='mpSubMenu' id='showmenu' ><ul><li><a href='ulouserprofile'>Profile</a></li><li><a href='ulouserprofile'>Booking History</a></li><li><a href='ulologout'>Log out</a></li></ul></div></div>");
          location.reload();
      }, function errorCallback(response) {

          var alert = ' <div class="alert alert-danger alert-dismissible" style="color:red">Invalid username or password.</div>';
          $('#loginerror').html(alert);
          $(function() {
              setTimeout(function() {
                  $("#loginerror").hide('blind', {}, 100)
              }, 5000);
          }); */
      });
  };
  
  $scope.mbSendUloLoginOtp = function() {
      var fdata = "username=" + $('#mbLoginUserName').val();

      //alert("sler" + fdata);
    // alert(fdata)
      $http({
          method: 'POST',
          data: fdata,
          headers: {
              'Content-Type': 'application/x-www-form-urlencoded'
          },
          url: 'send-ulologin-otp'
      }).then(function successCallback(response) {
           $scope.mbSendLoginOtp = response.data;
           $('.mobileuserotp').show();
 		  $('.mobileuserinfo').hide();
    
      });
  };
  
  $scope.verifyUloLoginOtp = function() {
      var fdata = "userId=" + $('#uloLoginId').val()+  
                   "&loginOtp=" + $('#loginOtp').val();
    // alert(fdata)
      $http({
          method: 'POST',
          data: fdata,
          headers: {
              'Content-Type': 'application/x-www-form-urlencoded'
          },
          url: 'verify-ulologin-otp'
      }).then(function successCallback(response) {
           $scope.verifyLoginOtp = response.data;
            //location.reload();
          
      });
  };
  
  $scope.mbVerifyUloLoginOtp = function() {
      var fdata = "userId=" + $('#mbUloLoginId').val()+  
                   "&loginOtp=" + $('#mbLoginOtp').val();
     //alert(fdata)
      $http({
          method: 'POST',
          data: fdata,
          headers: {
              'Content-Type': 'application/x-www-form-urlencoded'
          },
          url: 'verify-ulologin-otp'
      }).then(function successCallback(response) {
           $scope.mbVerifyLoginOtp = response.data;
            //location.reload();
          
      });
  };
  
  $scope.resendUloLoginOtp = function() {
      var fdata = "userId=" + $('#uloLoginId').val()+  
                   "&username=" + $('#uloLoginName').val();   /* $('#uloLoginName').val() */
     alert(fdata)
      $http({
          method: 'POST',
          data: fdata,
          headers: {
              'Content-Type': 'application/x-www-form-urlencoded'
          },
          url: 'resend-ulologin-otp'
      }).then(function successCallback(response) {
           $scope.resendLoginOtp = response.data;
          
      });
  };
  
  $scope.mbResendUloLoginOtp = function() {
      var fdata = "userId=" + $('#mbUloLoginId').val()+  
                   "&username=" + $('#mbUloLoginName').val();   /* $('#uloLoginName').val() */
    
      $http({
          method: 'POST',
          data: fdata,
          headers: {
              'Content-Type': 'application/x-www-form-urlencoded'
          },
          url: 'resend-ulologin-otp'
      }).then(function successCallback(response) {
           $scope.mbResendLoginOtp = response.data;
          
      });
  };
  $scope.msubmit = "Discoveer Growth";
  $scope.dsubmit = "Discoveer Growth";
  $scope.sendPatnerEmail = function() {
	  $scope.dsubmit = "Please Wait...";
      var fdata = "name=" + $('#name').val()+  
                   "&email=" + $('#email').val()+
                   "&phone=" + $('#phone').val()+
                   "&hotelName=" + $('#hotelName').val()+
                   "&location=" + $('#location').val()+
                   "&message=" + $('#message').val();
    // alert(fdata)
      $http({
          method: 'POST',
          data: fdata,
          headers: {
              'Content-Type': 'application/x-www-form-urlencoded'
          },
          url: 'partnerwithus'
      }).then(function successCallback(response) {
    	  $scope.dsubmit = "Discoveer Growth";
           $scope.patnerEmail = response.data;
       	$("#success").hide();
		  var alertmsg = ' <div class="alert" style="color:green;text-align:center;">Thank you for contacting the Ulo Hotels.Soon they will contact you </div>';
          $('#success').html(alertmsg);
          $("#success").fadeTo(2000, 500).slideUp(500, function(){
              $("#success").slideUp(500);
             
               });
           $('#partnerForm')[0].reset();
            //location.reload();
          
      });
  };
  
  $scope.sendMbPatnerEmail = function() {
	  $scope.msubmit = "Please Wait";
      var fdata = "name=" + $('#mname').val()+  
                   "&email=" + $('#memail').val()+
                   "&phone=" + $('#mphone').val()+
                   "&hotelName=" + $('#mhotelName').val()+
                   "&location=" + $('#mlocation').val()+
                   "&message=" + $('#mmessage').val();
     //alert(fdata)
      $http({
          method: 'POST',
          data: fdata,
          headers: {
              'Content-Type': 'application/x-www-form-urlencoded'
          },
          url: 'partnerwithus'
      }).then(function successCallback(response) {
           $scope.mbpatnerEmail = response.data;
       	$("#successmb").hide();
		  var alertmsg = ' <div class="alert" style="color:green;text-align:center;">Thank you for contacting the Ulo Hotels.Soon they will contact you </div>';
        $('#successmb').html(alertmsg);
        $("#successmb").fadeTo(2000, 500).slideUp(500, function(){
            $("#successmb").slideUp(500);
           
             });
         $('#partnerForm')[0].reset();
           $scope.msubmit = "Discoveer Growth";
           
            //location.reload();
          
      });
  };
   
    	$('#forgetPanel').hide();
    	$scope.showForget = function () {
   
    		$('#loginPanel').hide();
    		$('#forgetPanel').show();
   
   
    	};
   
    	$scope.showLogin = function () {
   
    		$('#forgetPanel').hide();
    		$('#loginPanel').show();
   
   
    	};
   
   $timeout(function () {
     	$('#btnclose').click();
     }, 1000);
     $timeout(function () {
     	$('#btncloses').click();
     }, 1000);
   
   });
   
   
</script>    