
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
   pageEncoding="ISO-8859-1"%>
<section id="innerPageContent">
   <!-- Tab Header -->
   <!-- Tab Header -->
   <div class="container tabBG">
      <div class="row">
         <div class="col-sm-12">
            <!-- Overview Tab -->
            <div class="tabContent" id="overviewTab" style="display:block;">
               <h1 class="heading1">FREQUENTLY ASKED QUESTIONS</h1>
               <div class="textContent">
                  <h2>ABOUT ULO</h2>
                  <br>
                  <H6>WHAT ARE ULO HOTELS?</H6>
                  <P>We are India's First Quality Budget Hotel Chain, serving you professionally in excellent properties while ensuring your stay is smooth and doesn't put a hole in your pocket.</p>
                  <H6>WHY SHOULD YOU BOOK WITH US? </H6>
                  <P>We give you excellent quality without burning your pockets, offer attractive discounts on every booking, 24*7 support, instant confirmation, easy cancellations and quick refunds. </p>
                  <H6>HOW ARE WE DIFFERENT? </H6>
                  <P>We are in direct contact with the owners of all our hotels and have control over the property quality. We organize audits through the managers of the properties and ensure that you get the best of everything. </p>
                  <h2>BOOKINGS</h2>
                  <br>
                  <H6>WHAT ARE THE DIFFERENT WAYS OF MAKING A RESERVATION WITH YOU? </H6>
                  <P>You can make a booking through any of the following: </p>
                  <P>You can make a booking through any of the following: </p>
                  <p>a. Through our website www.ulohotels.com</p>
                  <p>b. Calling our customer care helpdesk at 9543-592-593</p>
                  <p>c .Write-in  to us reservations@ulohotels.com</p>
                  <H6>WHAT ARE THE VARIOUS PAYMENT MODES AVAILABLE WHILE BOOKING VIA YOUR WEBSITE? </H6>
                  <P>You can make payment via: </p>
                  <P> a. Credit card, debit card, net-banking or e-wallets. </p>
                  <p>b. You can pay a partial advance online and pay the rest in person during check-out. </p>
                  <p>c. You could do an online transfer. Write to us at support@ulohotels.com for our account details. </p>
                  <H6>I AM UNABLE TO MAKE A RESERVATION THROUGH THE WEBSITE. WHAT SHOULD I DO? </H6>
                  <P>In cases where you are unable to complete the reservation through our website, please call us  at 9543-592-593 and we will be glad to assist you. </p>
                  <H6>WHAT IS THE PROCEDURE AFTER I MAKE A BOOKING THROUGH YOUR WEBSITE? </H6>
                  <P>After you are done booking, you will receive a confirmation mail with all relevant details: check in & checkout, directions & contact information of the hotel. In case you did not receive the mail, you can reach out to us on support@ulohotels.com or call us  at 123456789 to confirm your booking. </p>
                  <H6>ARE BOOKINGS MADE VIA TRAVEL WEBSITES LIKE YATRA, MAKEMYTRIP ETC CONSIDERED BY YOU? </H6>
                  <P>Yes, bookings can be made via other online portals and travel web sites. However, your booking and changes/cancellation is subject to their policies, terms and conditions. Ulo Hotels is not responsible for the above or payments, discounts and other deals promised by other portals. </p>
                  <H6>IS THERE A SEPARATE POLICY FOR BULK BOOKINGS? </H6>
                  <P>You may call us  at 9543-592-593 or leave your details and requests regarding bulk bookings and corporate booking with us at support@ulohotels.com. </p>
                  <h2>MODIFICATION</h2>
                  <br>
                  <H6>HOW CAN I CANCEL/AMEND MY RESERVATION? </H6>
                  <P>For all cancellations and amendments, please call us at 9543-592-593 if the booking is made via our website or through our customer care helpdesk. If your booking is made via other online travel websites like yatra.com etc, all modifications have to be made at the website where the booking was  made. </p>
                  <H6>HOW DO I CANCEL MY RESERVATION? </H6>
                  <P>Cancellation policies may vary depending upon the dates of your reservation. Please refer to the cancellation policy. You can cancel your booking online or call us at 123456789 for assistance. </p>
                  <H6>WHAT IS THE CANCELLATION PROCEDURE? </H6>
                  <P>For any cancellation or changes made up to 72 hours before the check-in date through Ulo hotels website, the entire booking amount will be refunded (within 7 working days) except the cancellation fee of Rs. 300/-. If any cancellation is made within 72 hours of check-in, we will be unable to refund the amount paid. </p>
                  <H6>WHAT IS THE CANCELLATION FEE? </H6>
                  <P>A service charge of Rs. 300 is charged to the guest on cancellation made up to 72 hours before the check-in date. The balance booking amount will be refunded within 7 working days. </p>
                  <H6>IS IT POSSIBLE TO MODIFY MY RESERVATION? </H6>
                  <P>Yes, you can modify your bookings subjected to the availability of the rooms. A service fee of Rs. 300 is charged  to the guest for the amendment. </p>
                  <H6>WHAT ARE YOUR ON-GOING OFFERS AND DISCOUNTS? </H6>
                  <P>We offer a flat 10% discount on all our bookings, year round. </p>
                  <h2>CHECK IN & CHECK OUT TIME</h2>
                  <br>
                  <H6>WHAT IS THE STANDARD CHECK IN & CHECK OUT TIME</H6>
                  <P>Our check in time is 10 A.M and check out time is 9 A.M. For additional details, please contact the respective hotel directly or call our helpdesk at 9543-592-593. </p>
                  <H6>WHAT DOCUMENTS ARE REQUIRED AT THE TIME OF CHECK-IN AT THE HOTEL? </H6>
                  <P>Booking confirmation E-mail/ SMS (with Booking ID) and a valid Photo ID (with address) is required for all the guests. Photo ID proofs accepted are Driver's License, Voters Card, Passport, Ration Card. Note: PAN Cards will not be accepted as a valid Photo ID card. </p>
                  <H6>WHAT IF I AM ARRIVING EARLIER THAN 10 AM AND WANT TO LEAVE POST 9 AM? </H6>
                  <P>Unfortunately, early check-ins or late check-outs are subjected to availability. However, we will certainly try to offer you an early check-in (free of charge) as long as we do not have other guests already staying in the booked room, or a late check out (free of charge), provided we do not have other guests arriving immediately</p>
                  .
                  <H6>ARE COMPLIMENTARY BREAKFASTS INCLUDED? </H6>
                  <P>Yes, all Ulo properties offer complimentary breakfast. </p>
                  <h2>OTHER AMENITIES</h2>
                  <br>
                  <H6>WHAT ARE THE OTHER AMENITIES PROVIDED IN ULO HOTELS? </H6>
                  <P>As our cherished guest, the following amenities are covered under our quality guarantee program: </p>
                  <P>Clean, fresh linen</p>
                  <P>  Sterile, hygienic bath towels</p>
                  <P>  Sanitary, fully equipped bathrooms</p>
                  <P>  Free Wifi*</p>
                  <P>  Complimentary bottled water</p>
                  <P>  Complimentary breakfast</p>
                  <P>  Branded toiletries</p>
                  *Not applicable for hill stations or areas where connectivity isn't available</p>
                  <H6>WHAT ARE THE FACILITIES AVAILABLE FOR FOOD? </H6>
                  <P>Breakfast is complimentary at all our hotels. For other meals and snacks, most of our hotels have room service available, wherein food is either prepared in house or can be facilitated from nearby restaurants on request. </p>
                  <H6>IS THERE ANY FACILITY FOR LAUNDRY? </H6>
                  <P>Laundry facilities are available in all our hotels, but it is charged at an extra price which varies from hotel to hotel. </p>
                  <H6>DOES YOUR HOTEL MAKE ARRANGEMENTS FOR AIRPORT TRANSPORTATION? </H6>
                  <P>We can arrange the same at an extra cost in some cases, but we encourage you to try arranging transport yourself. </p>
                  <H6>WILL I GET HELP IN TRANSFERRING LUGGAGE AND OTHER BAGS TO MY ROOM? </H6>
                  <P>Our hotel staff will be happy to help you with all your luggage transfers within the hotel premises.</p>
                  <H6>IS EXTRA BED PROVIDED IN ALL ROOMS? IS IT INCLUDED IN ROOM TARIFF? </H6>
                  <P>Extra mattress can be arranged for in all our hotels at extra charge. This is subject to the maximum permissible occupancy for the given room category. </p>
                  <H6>CAN I ASK FOR A SMOKING ROOM? </H6>
                  <P>Most of our hotels have non-smoking rooms except for a few. For exact details, please drop in a mail or call us at 9543-592-593. </p>
                  <H6>WHAT IF I AM DISSATISFIED WITH THE HOTEL? </H6>
                  <P>We always do our best to ensure your stay with us is comfortable and safe. In the rare event, something isn't to your satisfaction, please contact us at 9543-592-593 or write to us at support@ulohotels.com to resolve the issue. </p>
                  <h2>Partners</h2>
                  <br>
                  <H6>WHY SHOULD YOU WORK WITH US? </H6>
                  <P>We believe excellent quality should be affordable. If you are associated with a hotel/property and believe in the same, we would love to partner with you. We will improve quality, increase your turnover and increase occupancy rate. </p>
                  <H6>HOW DOES ULO WORK? </H6>
                  <P>Ulo is designed to help budget hotel chains improve their quality level through various hands-on processes. We will pay a visit to your property and hire a General Manager to oversee the administration and quality control. We will review the day-to-day operations, improve the quality and ensure your occupancy rate is high and maintained through the year. </p>
                  <H6>HOW DOES PARTNERING WITH ULO GUARANTEE REVENUE? </H6>
                  <P>We will set personalized targets for your property every month, which we will then achieve to meet the revenue standards. </p>
                  <H6>WILL ULO OVERSEE THE COMPLETE ADMINISTRATION OF THE HOTEL? </H6>
                  <P>Yes, you don't have to worry about the administration. We will appoint a General Manager for your hotel who will oversee the day-to-day operations of your property and report to us. </p>
                  <H6>HOW WILL ULO HELP US IN OTHER OPERATIONS? </H6>
                  <P>We will generate reports on an everyday basis in the Property Management System [PMS]. These reports will be analyzed by our team to discover what is unique to your property, areas to improve, your performance and so on. </p>
                  <H6>CAN I ACCESS ALL THE DAY-TODAY REPORTS? </H6>
                  <P>Yes, you can access all the reports through our PMS platform. </p>
                  <H6>HOW DOES ULO HANDLE PAYMENTS? </H6>
                  <P>We are directly linked to the OTA for our bookings. All payments from OTA will be released on the 15th of every calendar month to the hotel owners after deducting the agreed upon Ulo revenue. </p>
                  <H6>HOW WILL YOU BE PARTNERING WITH HOTELS? </H6>
                  <P>We work on the Franchise model, where, we take control of the quality aspect of the property while you retain ownership of the property. We will help give your property a makeover, so to speak and increase occupancy rate. For more details please mail us at contracts@ulohotels.com. </p>
                  <H6>WILL YOU PERFORM AN AUDIT OF THE PROPERTY PRIOR TO PARTNERSHIP? </H6>
                  <P>By law, we are required to verify the property before making the contract. Once we are satisfied with the standards & all other criteria are met, we will draft the contract. </p>
                  <H6>WHAT ARE THE DOCUMENTS REQUIRED FOR PARTNERING WITH ULO? </H6>
                  <p>We require the following for partnering with you: </p>
                  <P>a. Updated Property License</p>
                  <P>b. Updated Property Tax</p>
                  <P>c. If you are sub-lease owner, we require a no objection letter from the property owner.</p>
                  <H6>IS THERE A MINIMUM CRITERIA REQUIRED FOR PARTNERING WITH ULO? </H6>
                  <P>Yes, we require that your property has a minimum of at least 5 rooms. </p>
                  <H6>IS THERE A MINIMUM CONTRACT PERIOD FOR PARTNERING WITH ULO? </H6>
                  <P>Yes, our contract has a minimum 3 years lock-in policy. If you are a lease owner, we require proof of your lease and an NOC (No Objection Certificate) from the owner of the property. </p>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script src="js1/ngprogress.js" type="text/javascript"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script> 
<script>
   var app = angular.module('myApp', ['ngProgress']);
   app.controller('customersCtrl', function ($scope, $http, $timeout, ngProgressFactory) {
    $scope.userData = [];
   
    	$scope.login = function () {
   
   
    		var fdata = "username=" + $('#username').val() +
    			"&password=" + $('#password').val();
    		//alert(fdata);
   
    		$http({
    			method: 'POST',
    			data: fdata,
    			headers: {
    				'Content-Type': 'application/x-www-form-urlencoded'
    			},
    			url: 'ulologin'
   
    		}).then(function successCallback(response) {
    			$scope.userData = response.data;
    			console.log($scope.userData);
    			var username = $scope.userData.data[0].userName;
    			var email = $scope.userData.data[0].emailId;
    			var phone = $scope.userData.data[0].phone;
   
    			$('#loginModal').modal('toggle');
    			$('#logSec').html("<div class='mnuContant'><a href='mailto:support@ulohotels.com' title='support@ulohotels.com'><i class='fa fa-envelope'></i><span class='visibleLG'>support@ulohotels.com</span></a><a href='tel:9543592593' title=''+91 95435 92593'><i class='fa fa-phone'></i><span class='visibleLG'>+91 95435 92593</span></a></div><div class='mnuProfile'><div class='mpAvatar hidden-xs' style='background:url(ulowebsite/images/avatar.png);'></div><div class='mpUsername'><a href='javascript:showpro();' class=' mnuProfileOpen'>Hi admin<i class='fa fa-angle-down'></i></a></div><div class='mpSubMenu' id='showmenu' ><ul><li><a href='ulouserprofile'>Profile</a></li><li><a href='ulouserprofile'>Booking History</a></li><li><a href='ulologout'>Log out</a></li></ul></div></div>");
    		}, function errorCallback(response) {
   
    			var alert = ' <div class="alert alert-danger alert-dismissible" style="color:red">Invalid username or password.</div>';
    			$('#loginerror').html(alert);
    		    $(function() {
    	          // setTimeout() function will be fired after page is loaded
    	          // it will wait for 5 sec. and then will fire
    	          // $("#successMessage").hide() function
    	          setTimeout(function() {
    	              $("#loginerror").hide('blind', {}, 100)
    	          }, 5000);
    	      });
    		});
   
    	};
   
    	$scope.signup = function () {
   
   
    		var fdata = "userName=" + $('#userName').val() +
    			"&phone=" + $('#mobilePhone').val() +
    			"&emailId=" + $('#emailId').val() +
    			"&roleId=" + $('#roleId').val() +
    			"&accessRightsId=" + $('#accessRightsId').val() +
    			"&password=" + $('#signpassword').val();
    		//alert(fdata);
   
    		$http(
   
    			{
    				method: 'POST',
    				data: fdata,
    				headers: {
    					'Content-Type': 'application/x-www-form-urlencoded'
    				},
    				url: 'ulosignup'
    			}).then(function successCallback(response) {
   
   
    			if (response.data == "") {
   
    				var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:red">Sorry This Email Id already exist.</div>';
    				$('#signuperror').html(alertmsg);
   
   
    			} else {
   
    				$scope.userData = response.data;
    				var username = $scope.userData.data[0].userName;
    				var email = $scope.userData.data[0].emailId;
    				var phone = $scope.userData.data[0].phone;
   
   
    				$('#registerModal').modal('toggle');
   
    				$('#logSec').html("<div class='mnuContant'><a href='mailto:support@ulohotels.com' title='support@ulohotels.com'><i class='fa fa-envelope'></i><span class='visibleLG'>support@ulohotels.com</span></a><a href='tel:9543592593' title=''+91 95435 92593'><i class='fa fa-phone'></i><span class='visibleLG'>+91 95435 92593</span></a></div><div class='mnuProfile'><div class='mpAvatar hidden-xs' style='background:url(ulowebsite/images/avatar.png);'></div><div class='mpUsername'><a href='javascript:showpro();' class=' mnuProfileOpen'>Hi admin<i class='fa fa-angle-down'></i></a></div><div class='mpSubMenu' id='showmenu' ><ul><li><a href='ulouserprofile'>Profile</a></li><li><a href='ulouserprofile'>Booking History</a></li><li><a href='ulologout'>Log out</a></li></ul></div></div>");
    			}
   
    		}, function errorCallback(response) {
   
   
    			var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:red">This Email ID Already Exist</div>';
    			$('#signuperror').html(alertmsg);
   
    			// or server returns response with an error status.
    		});
   
    	};
   
   
    	$scope.passwordRequest = function () {
   
   
    		var fdata = "emailId=" + $('#forgetEmailId').val();
   
   
    		$http({
    			method: 'POST',
    			data: fdata,
    			headers: {
    				'Content-Type': 'application/x-www-form-urlencoded'
    			},
    			url: 'ulosendpassword'
    		}).then(function successCallback(response) {
   
    			var alertmsg = ' <div class="alert alert-success alert-dismissible" style="color:red">Password Sent Sucessfully.</div>';
   
    			$('#forgetmessage').html(alertmsg);
   
   
    		}, function errorCallback(response) {
   
   
    			var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:red">Your Email ID does not Registred Yet</div>';
    			$('#forgetmessage').html(alertmsg);
   
   
    		});
   
    	};
   
    	$('#forgetPanel').hide();
    	$scope.showForget = function () {
   
    		$('#loginPanel').hide();
    		$('#forgetPanel').show();
   
   
    	};
   
    	$scope.showLogin = function () {
   
    		$('#forgetPanel').hide();
    		$('#loginPanel').show();
   
   
    	};
   
   $timeout(function () {
     	$('#btnclose').click();
     }, 1000);
     $timeout(function () {
     	$('#btncloses').click();
     }, 1000);
   
   });
   
   
</script>