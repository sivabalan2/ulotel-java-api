<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
 <%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <section class="mobileuserprofile hidden-lg hidden-md hidden-sm" >
     <div class="container">
     <div class="row">
    <!-- mobile user banner begins -->
    <div id="mobileallpage">
        <div class="col-xs-12 mprofilebg" >
       <h4>Ulo Welcomes You !</h4>
    <img src="contents/images/payment/u1.svg" alt="userporofilr" class="profileimage"> 
    <h5>Make A Booking Now</h5>
     </div>
   <!--  mobile user banner ends -->
   <!-- mobile user menu begins -->
   <div class="col-xs-12 mobilemenulist"  id="mobileallpage">
   <a ng-click="mobileonepage();"><h4>My Profile</h4></a>
   <a ng-click="mobilesecondpage();"><h4>Booking History</h4></a>
   <a ng-click="mobilethirdpage();"><h4>Rewards & Earns</h4></a>
    <a href="ulologout"><h4 class="mlogout">Logout</h4></a>
   </div>
   </div>
   <!-- mobile user menu ends -->
  <!--  mobile user data begins -->
     <div class="col-xs-12 mobileuserdata" style="display:none" id="mobileonepage">

     	<form>   
     	          
			    <div class="col-xs-12 usermdata" ng-repeat="u in user">
					 		 <div class="form-group">
		 <button class="btn mpcancelbtn">CANCEL</button>
		<button class="btn mpupdatebtn" ng-click="updateMbUloUserProfile()">UPDATE</button>
 </div>   
					     <div class="form-group">
					   
                    <input type="text" class="form-control dpform" id="mbuserName" placeholder="Name" value="{{u.o_username}}" ng-required="true">
                  </div>
                  	     <div class="form-group">
					    
                    <input type="text" class="form-control dpform" id="mbuserMobile" value="{{u.o_phone}}" placeholder="Mobile 1" ng-required="true">
                    <button class="datamore" ng-click="addmMobile()"><i class="fa fa-plus-circle"></i> Add</button>
                    <input type="text" class="form-control dpform" id="mbuserMobile1" value="{{u.o_phone1}}" placeholder="Mobile 2" style="display:none;" ng-required="true" id="mobilenumber2">
                  </div>
                  	     <div class="form-group">
				
                    <input type="text" class="form-control dpform" id="mbuserEmail" value="{{u.o_email}}" placeholder="email" ng-required="true">
                    <button class="datamore" ng-click="addmEmail()"><i class="fa fa-plus-circle"></i> Add</button>
                    <input type="text" class="form-control dpform" id="mbuserEmail1" value="{{u.o_email1}}" placeholder="email" ng-required="true" id="email2" style="display:none;">
                    
                  </div>
         
					           	     <div class="form-group">
					     
                    <input type="text" class="form-control dpform" id="mbuserAddress1" value="{{u.o_address1}}" placeholder="Address 1" ng-required="true">
                      <input type="text" class="form-control dpform" id="mbuserAddress1" value="{{u.o_address2}}" placeholder="Address 2" ng-required="true">
                       <input type="text" class="form-control dpform" id="mbuserAddress1" value="{{u.o_address3}}" placeholder="Address 3"  ng-required="true">
                  </div>
			 
					  
					                      <div class="form-group">
  <label for="traveldiaries">What kind of traveller you are ?</label>
  <select class="form-control dpform" id="mbtravelerType">
    <option value="Bussiness Class" ng-selected ="u.travelerType == 'Bussiness Class'">Bussiness Class</option>
    <option value="Vacation" ng-selected ="u.travelerType == 'Vacation'">Vacation</option>
    <option value="Adventure" ng-selected ="u.travelerType == 'Adventure'">Adventure</option>
    <option value="Backpackers" ng-selected ="u.travelerType == 'Backopackers'">Backpackers</option>
    <option value="Leisure" ng-selected ="u.travelerType == 'Leisure'">Leisure</option>
    <option value="Frequent" ng-selected ="u.travelerType == 'Frequent'">Frequent</option>
  </select>
</div>
 
					    </div>
					   </form>
   </div>
 <!--   mobile user data ends -->
<!--  mobile booking history begins -->
<section class="mobilehistory"  style="display:none" id="mobilesecondpage">
<div class="col-xs-12 mobilebookhistory">
  <h3>Upcoming Booking</h3>
  
					   <div class="row dubook" ng-repeat="upb in upcomingBookings track by $index">
					   <div class="col-xs-4">
					   <img src="get-property-thumb?propertyId={{upb.propertyId}}" alt="ulohotels" class="img-thumbnail"> 
					   </div>
					    <div class="col-xs-8">
					   <h4>{{upb.propertyName}}</h4>
					   <h6>{{upb.address1}},{{upb.address2}},{{upb.city}}</h6>
                    </div>
				
					   <div class="col-xs-12">
					   	   <div class="col-xs-5">
					   <span class="bci">{{upb.arrivalDate}}</span>  <!-- 30 July -->
					   <span class="bconame">CHECK IN</span>
					   </div>
					    <div class="col-xs-2">
					    <img src="contents/images/menu-icon/sep2.png" alt="seacrh" class="dhsep"/>
					   </div>
					    <div class="col-xs-5">
					    <span class="bci">{{upb.departureDate}}</span>
					    <span class="bconame">CHECK OUT</span>
					   </div>
					   </div>
					   	    <div class="col-xs-12">
					    	   <div class="col-xs-5">
					    <span class="mrdetail">{{upb.accommodationType}} x  {{upb.rooms}}</span>
					   </div>
					    <div class="col-xs-4">
					    <span class="mrdetail">{{upb.adultCount + upb.childCount}} GUEST</span>
					   </div>
					    <div class="col-xs-3">
					    <span class="mrdetail">{{upb.diffDays}} Night</span>
					   </div>
					    </div>
					     <div class="col-xs-12">
					   <button class="btn btncancel" ng-click="dcancelbtn();" data-toggle="modal" data-target="#myModalcancelation">Cancellation</button>
					   <!-- cancel reason begins -->
					       <div id="myModalcancelation" class="modal fade mcancelpop" role="dialog" >
      <div class="modal-dialog modal-xs">
        <!-- Modal content-->
        <div class="modal-content ">
          <div class="modal-body">
           <div class="row">
           <div class="col-xs-12">
          <h4 class="text-center">Cancellation</h4>
          				
						
					  <form>
					  <div class="form-group">
    <label class="radio-inline">
      <input type="radio" name="optradio" checked>Wrongly Booked
    </label>
    <label class="radio-inline">
      <input type="radio" name="optradio">High Price
    </label>
    <label class="radio-inline">
      <input type="radio" name="optradio">Rescheduled
    </label>
       <label class="radio-inline">
      <input type="radio" name="optradio">Change of Mind
    </label>
    <label class="radio-inline">
      <input type="radio" name="optradio">Others
    </label>
    </div>
    <div class="form-group"><textarea rows="4" class="form-control"></textarea></div>
    	<%-- <div class="form-group">
					<span class="cpreason">Your Total Booking Amount</span>
					<span class="cpamount"><i class="fa fa-rupee"></i> 5650</span>
					</div>
						<div class="form-group">
					<span class="cpreason">Eligible for Refund</span>
					<span class="cpstatus">Refundable</span>
					</div>
								<div class="form-group">
					<span class="cpreason">Cancellation Charge</span>
					<span class="cpamount"><i class="fa fa-rupee"></i> 500</span>
					</div>
					<div class="form-group">
					<span class="cpreason">Refund Amount</span>
					<span class="cpamount" ><i class="fa fa-rupee"></i> 400</span>
					</div> --%>
  </form>
		
           </div>
			<div class="col-xs-12"><button class="drequestbtn">REQUEST FOR CANCELLATION</button></div>
					<div class="col-xs-12"><button class="dortext">OR</button></div>
					<div class="col-xs-12"><a class="drequestbtn" href="tel:9543592593" title="+91 95435 92593"><button>CALL US</button></a></div>
           </div>
          </div>
        </div>
      </div>
    </div>
					   <!-- cancel reason ends -->
					   
					   </div>
					   </div>
					   </div>
					   <div class="col-xs-12 mobilepastbooking">
					   						<div class="row dpbook">
						 <a class="pbooking" data-toggle="collapse" data-target="#mdemo" ng-click="getUserBookings()"><span>Past Booking<span class="pull-right"><i class="fa fa-angle-down"></i></span></span></a>
  <div id="mdemo" class="collapse">
                      <div class="col-xs-12 blurmode" ng-repeat="ub in userBookings track by $index">
                      <div class="col-xs-4">
					   <img src="get-property-thumb?propertyId={{ub.propertyId}}" alt="ulohotels" class="img-thumbnail"> 
					   </div>
					    <div class="col-xs-8">
					   <h4>{{ub.propertyName}}</h4>
					   <h6>{{ub.address1}},{{ub.address2}},{{ub.city}}</h6>
					   	           </div>
					   <div class="col-xs-12">
					   	   <div class="col-xs-5">
					   <span class="bci">{{ub.arrivalDate}}</span>  <!-- 30 July -->
					   <span class="bconame">CHECK IN</span>
					   </div>
					    <div class="col-xs-2">
					    <img src="contents/images/menu-icon/sep2.png" alt="seacrh" class="dhsep"/>
					   </div>
					    <div class="col-xs-5">
					    <span class="bci">{{ub.departureDate}}</span>
					    <span class="bconame">CHECK OUT</span>
					   </div>
					   </div>
					   <div class="col-xs-12">
					   	   <div class="col-xs-5">
					    <span class="mrdetail">{{ub.accommodationType}} x  {{ub.rooms}}</span>
					   </div>
					    <div class="col-xs-3">
					    <span class="mrdetail">{{ub.adultCount + ub.childCount}} GUEST</span>
					   </div>
					    <div class="col-xs-4">
					    <span class="mrdetail">{{ub.diffDays}} Night</span>
					   </div>
					   </div>
		
					     <!--  review section -->
					  <div class="col-xs-12">
					  <h4 class="dreview">Share Our Experience With Us <a href="{{ub.propertyReviewLink}}" target="_blank" class="dreviewbtn">Write Review <i class="fa fa-edit"></i></a></h4>
					  </div>
					   </div>
					
  </div>
					  	</div>
					   </div>
</div>
</section>

<!--  mobile booking history ends -->
<!-- mobile coupon code begins -->
<div class="col-xs-12"  style="display:none" id="mobilethirdpage">
	<div class="col-xs-12 mrewards" ng-repeat="u in user">
							<h3 class="text-center">Rewards & Earns</h3>
							<img src="contents/images/payment/u2.svg" alt="rewards & Earns" class="drimage">
							<h3 class="text-center">{{u.rewardPoints}} Credits</h3>
							<span class="redeemnote">Book And Stay To Get More Ulo Credits </span>
							<button class="dredeembtn">REDEEM BOOKING</button>
						</div>
</div>
<!-- mobile coupon code ends -->
     </div>
     </div>
     </section>
    
   <section class="deskuserprofile hidden-xs">
   <div class="container">
    <div class="row deskshadow">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 dprofilebg" >
    <h4>Ulo Welcomes You !</h4>
    <img src="contents/images/payment/u1.svg" alt="userporofilr" class="profileimage"> 
    <h5>Make A Booking Now</h5>
    </div>
		<div class="col-xs-3">
		 
			<!-- tabs -->
			<div class="tabbable tabs-left">
				<ul class="nav nav-tabs">
					<li class="active"><a href="#home" data-toggle="tab">My Profile</a></li>
					<li><a href="#about" data-toggle="tab">Booking History</a></li>
					<li><a href="#services" data-toggle="tab">Rewards & Earns</a></li>
					<li><a href="#contact" data-toggle="tab" class="hidden">Help</a></li>
					<li><a href="ulologout"  class="ulo">Logout</a></li>
				</ul>
				</div>
				</div>
				<div class="col-xs-9">
				 <input type="hidden" id="uloUserId" value="<%= session.getAttribute("uloUserId") %>" />
				 <input type="hidden" id="uloEmailId" value="<%= session.getAttribute("uloEmailId") %>" /> 
				<div class="tab-content">
					<div class="tab-pane active duserdata" id="home" ng-repeat="u in user">  
					<form>             
					    <div class="col-xs-6">					   
					     <div class="form-group">
					     <label for="email">Name</label>
                    <input type="text" class="form-control dpform" id="userName" value="{{u.o_username}}" placeholder="Name" ng-required="true">
                  </div>
                  	     <div class="form-group">
					     <label for="Mobile">Mobile</label>
                    <input type="text" class="form-control dpform" id="userMobile" value="{{u.o_phone}}" placeholder="Mobile" ng-required="true">
                    <button class="datamore" ng-click="addMobile()"><i class="fa fa-plus-circle"></i> Add</button>
                    <input type="text" class="form-control dpform" placeholder="Mobile 1" value="{{u.o_phone1}}" style="display:none;" ng-required="true" id="userMobile1">
                  </div>
                  	     <div class="form-group">
					     <label for="email">Email</label>
                    <input type="text" class="form-control dpform" id="userEmail" value="{{u.o_email}}" placeholder="email" ng-required="true">
                    <button class="datamore" ng-click="addEmail()"><i class="fa fa-plus-circle"></i> Add</button>
                    <input type="text" class="form-control dpform" placeholder="email" value="{{u.o_email1}}" ng-required="true" id="userEmail1" style="display:none;">
                    
                  </div>
             </div>
					    <div class="col-xs-6">
					           	     <div class="form-group">
					     <label for="address">Address</label>
                    <input type="text" class="form-control dpform" id="userAddress1" value="{{u.o_address1}}" placeholder="Address 1" ng-required="true">
                    <input type="text" class="form-control dpform" id="userAddress2" value="{{u.o_address2}}" placeholder="Address 2" ng-required="true">
                    <input type="text" class="form-control dpform" id="userAddress3" value="{{u.o_address3}}" placeholder="Address 3" ng-required="true">
                  </div>
			 
					  
					                      <div class="form-group">
  <label for="traveldiaries">What kind of traveller you are ?</label>
  <select class="form-control dpform" id="travelerType">
    <option value="Bussiness Class" ng-selected ="u.travelerType == 'Bussiness Class'">Bussiness Class</option>
    <option value="Vacation" ng-selected ="u.travelerType == 'Vacation'">Vacation</option>
    <option value="Adventure" ng-selected ="u.travelerType == 'Adventure'">Adventure</options>
    <option value="Backpackers" ng-selected ="u.travelerType == 'Backopackers'">Backpackers</option>
    <option value="Leisure" ng-selected ="u.travelerType == 'Leisure'">Leisure</option>
    <option value="Frequent" ng-selected ="u.travelerType == 'Frequent'">Frequent</option>
  </select>
</div>
 <div class="col-xs-6">
<button class="btn cancelbtn">CANCEL</button>
 </div>
		
		 <div class="col-xs-6">
		<button type="submit" ng-click="updateUloUserProfile()" class="btn updatebtn">UPDATE</button>
 </div> 
					    </div>
					   </form>
					</div> 
					<div class="tab-pane dbookhistory" id="about"> 
					<!-- cancellation begins -->
					<div class="col-xs-12 deskcancellation" style="display:none;" id="deskcancellation">
					<div class="col-xs-12"><a href=""  target="_blank" class="dvpolicies">View Cancellation Policy</a></div>
					<h4 class="text-center">Cancellation</h4>
					<div class="Col-xs-12">
					  <form>
					  <div class="form-group">
    <label class="radio-inline">
      <input type="radio" name="optradio" checked>Wrongly Booked
    </label>
    <label class="radio-inline">
      <input type="radio" name="optradio">High Price
    </label>
    <label class="radio-inline">
      <input type="radio" name="optradio">Rescheduled
    </label>
       <label class="radio-inline">
      <input type="radio" name="optradio">Change of Mind
    </label>
    <label class="radio-inline">
      <input type="radio" name="optradio">Others
    </label>
    </div>
    <div class="form-group"><textarea rows="4" class="form-control"></textarea></div>
    	<%-- <div class="form-group">
					<span class="cpreason">Your Total Booking Amount</span>
					<span class="cpamount"><i class="fa fa-rupee"></i> 5650</span>
					</div>
						<div class="form-group">
					<span class="cpreason">Eligible for Refund</span>
					<span class="cpstatus">Refundable</span>
					</div>
								<div class="form-group">
					<span class="cpreason">Cancellation Charge</span>
					<span class="cpamount"><i class="fa fa-rupee"></i> 500</span>
					</div>
					<div class="form-group">
					<span class="cpreason">Refund Amount</span>
					<span class="cpamount" ><i class="fa fa-rupee"></i> 400</span>
					</div> --%>
  </form>
					</div>
					<div class="col-xs-2"><button class="drequestback" ng-click="dcancelback();">BACK</button></div>
					<div class="col-xs-4"><button class="drequestbtn" ng-click="requestCancellation()">REQUEST FOR CANCELLATION</button></div>
					<div class="col-xs-2"><button class="dortext">OR</button></div>
					<div class="col-xs-4"><a class="drequestbtn" href="tel:9543592593" title="+91 95435 92593"><button>CALL US</button></a></div>
					</div>
					<!-- cancellation ends -->
						<div class="col-xs-12 dubook" id="dupbooking" >
					   <h3>Upcoming Booking</h3>
					   <div ng-repeat="upb in upcomingBookings track by $index">
					   <div class="col-xs-3">
					   <img src="get-property-thumb?propertyId={{upb.propertyId}}" alt="ulohotels" class="img-thumbnail"> 
					   </div>
					    <div class="col-xs-7">
					   <h4>{{upb.propertyName}}</h4>
					   <h6>{{upb.address1}},{{upb.address2}},{{upb.city}}</h6>
					   <div class="col-xs-5">
					   <span class="bci">{{upb.arrivalDate}}</span>
					   <span class="bconame">CHECK IN</span>
					   </div>
					    <div class="col-xs-2">
					    <img src="contents/images/menu-icon/sep2.png" alt="seacrh" class="dhsep"/>
					   </div>
					    <div class="col-xs-5">
					    <span class="bci">{{upb.departureDate}}</span>
					    <span class="bconame">CHECK OUT</span>
					   </div>
					   <div class="col-xs-4">
					    <span class="mrdetail">{{upb.accommodationType}} x  {{upb.rooms}}</span>
					   </div>
					    <div class="col-xs-4">
					    <span class="mrdetail">{{upb.adultCount+upb.childCount}} GUEST</span>
					   </div>
					    <div class="col-xs-4">
					    <span class="mrdetail">{{upb.diffDays}} Night</span>
					   </div>
					   </div>
					     <div class="col-xs-2">
					   <button class="btn btncancel" ng-click="dcancelbtn(upb.bookingId);">Cancellation</button>
					   </div>
					   </div>
					   </div>
						<div class="col-xs-12 dpbook">
						 <a class="pbooking" data-toggle="collapse" ng-click="getUserBookings()" data-target="#demo"><span>Past Booking<span class="pull-right"><i class="fa fa-angle-down"></i></span></span></a>
              <div id="demo" class="collapse" ng-repeat="ub in userBookings track by $index">
                      <div class="col-xs-12 blurmode">
                      <div class="col-xs-3">
					   <img src="get-property-thumb?propertyId={{ub.propertyId}}" alt="ulohotels" class="img-thumbnail"> 
					   </div>
					    <div class="col-xs-9">
					   <h4>{{ub.propertyName}}</h4>
					   <h6>{{ub.address1}},{{ub.address2}},{{ub.city}}</h6>
					   <div class="col-xs-5">
					   <span class="bci">{{ub.arrivalDate}}</span>
					   <span class="bconame">CHECK IN</span>
					   </div>
					    <div class="col-xs-2">
					    <img src="contents/images/menu-icon/sep2.png" alt="seacrh" class="dhsep"/>
					   </div>
					    <div class="col-xs-5">
					    <span class="bci">{{ub.departureDate}}</span>
					    <span class="bconame">CHECK OUT</span>
					   </div>
					   <div class="col-xs-4">
					    <span class="mrdetail">{{ub.accommodationType}} x  {{ub.rooms}}</span>
					   </div>
					    <div class="col-xs-4">
					    <span class="mrdetail">{{ub.adultCount + ub.childCount}} GUEST</span>
					   </div>
					    <div class="col-xs-4">
					    <span class="mrdetail">{{ub.diffDays}} Night</span>
					   </div>
					   </div>
					   </div>
					  <!--  review section -->
					  <div class="col-xs-12">
					  <h4 class="dreview">Share Our Experience With Us <a href="{{ub.propertyReviewLink}}" target="_blank" class="dreviewbtn">Write Review <i class="fa fa-edit"></i></a></h4>
					  </div>
  </div>
					  	</div>
					</div>
				
					<div class="tab-pane drewards" id="services"> 
					<input type="hidden" id="uloUserId" value="<%= session.getAttribute("uloUserId") %>" /> 
						<div class="col-xs-10" ng-repeat="u in user">
							<h3 class="text-center">Rewards & Earns</h3>
							<img src="contents/images/payment/u2.svg" alt="rewards & Earns" class="drimage">
							<h3 class="text-center">{{u.rewardPoints}} Credits</h3>
							<span class="redeemnote">Book And Stay To Get More Ulo Credits </span>
							<button class="dredeembtn hide">REDEEM BOOKING</button>
						</div>
						<div class="col-xs-2"></div>
					</div>
				
					<div class="tab-pane hidden" id="contact"> 
						 <div class="">
							<h1>Contact Tab</h1>
							<p>deserve editor attention because they are the most important for an encyclopedia to have, as determined by the community of participating editors. They may also be of interest to readers as an alternative to lists of overview articles.</p>                 
						 </div>
					</div>
				</div>
			</div>
			<!-- /tabs -->
		</div>
	</div>
</div>

   
   </section>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
    <script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>
    <script src="ulowebsite/js/jquery.min.js"></script>
    <script src="ulowebsite/js/jquery-ui.js"></script>
    <script src="js1/upload/ng-file-upload-shim.min.js"></script>
	<script src="js1/upload/ng-file-upload.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.js"></script>
<script>


 
	
		var app = angular.module('myApp',['ngFileUpload']);
		app.controller('customersCtrl',['$scope',
		             					'Upload',
		            					'$timeout',
		            					'$http', function($scope,Upload,$timeout,$http) {
//alert("fd");
$scope.addEmail = function(){
	    var x = document.getElementById("userEmail1");
    if (x.style.display === "none") {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }
};
$scope.addMobile = function(){
    var x = document.getElementById("userMobile1");
    if (x.style.display === "none") {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }
};
$scope.addmEmail = function(){
    var x = document.getElementById("email2");
if (x.style.display === "none") {
    x.style.display = "block";
} else {
    x.style.display = "none";
}
};
$scope.addmMobile = function(){
var x = document.getElementById("mobilenumber2");
if (x.style.display === "none") {
    x.style.display = "block";
} else {
    x.style.display = "none";
}
};
$scope.dcancelbtn = function(bookingId){
	document.getElementById("dupbooking").style.display="none"; 
	document.getElementById("deskcancellation").style.display="block"; 
	$scope.BookingId = bookingId;
};
$scope.dcancelback = function(){
	document.getElementById("dupbooking").style.display="block"; 
	document.getElementById("deskcancellation").style.display="none"; 
};
$scope.mobileonepage = function(){
	
	//("f");
	document.getElementById("mobileallpage").style.display="none"; 
	document.getElementById("mobileonepage").style.display="block"; 
};
$scope.mobilesecondpage = function(){
	document.getElementById("mobileallpage").style.display="none"; 
	document.getElementById("mobilesecondpage").style.display="block"; 
};
$scope.mobilethirdpage = function(){
	 document.getElementById("mobileallpage").style.display="none"; 
	document.getElementById("mobilethirdpage").style.display="block"; 
};


   /*  $scope.getUserRewards = function() {
		//alert("filter")
		var url = "get-user-rewards?userId="+$('#uloUserId').val();
		$http.get(url).success(function(response) {
		   //console.log(response);
			$scope.userRewards = response.data;
				//alert(JSON.stringify($scope.filterSearch))
		});
	}; */	
	
	
	$scope.getUser = function() {
		
		//alert($('#userId').val());
		
	    var userid = $('#uloUserId').val();
	   // alert(userid)
		var url = "get-customer?auserId="+userid;
		$http.get(url).success(function(response) {
			//console.log(response);
			//alert(JSON.stringify(response.data));
			$scope.user = response.data;
		
		});
	};
	
	 $scope.getUserBookings = function() {
			
			
			
		    var emailId = $('#uloEmailId').val();
		  
		    //alert(emailId);
		    //var emailId = "gopinath@ulohotels.com";
			var url = "get-user-bookings?emailId="+emailId;
			$http.get(url).success(function(response) {
				//alert(response.data);
				
				//console.log(response);
				//alert(JSON.stringify(response.data));
				$scope.userBookings = response.data;
			
			});
		};
		
		$scope.getUpcomingBookings = function() {
			
			
		    var emailId =  $('#uloEmailId').val();
		    //alert("emailid"+emailId);
		   // alert($('#gopi').val());
		    //var emailId = "gopinath@ulohotels.com";
			var url = "get-upcoming-bookings?emailId="+emailId;
			$http.get(url).success(function(response) {
				
				//console.log(response);
				//alert(JSON.stringify(response.data));
				$scope.upcomingBookings = response.data;
			
			});
		};
	
	 $scope.ulologout = function(){
			//alert("logout")
			var url = 'ulologout';
			$http.get(url).success(function(response) {
			   //console.log(response);
				$scope.ulologout = response.data;
				//location.reload();
				 //window.location = '/ulopms';
					//alert(JSON.stringify($scope.filterSearch))
			});
			
		};
		
	$scope.requestCancellation=function(){
			
			
			var fdata = "userId="+$('#uloUserId').val() 	 	
			+"&userName="+$('#userName').val()
			+"&phone="+$('#userMobile').val()
			+"&phone1="+$('#userMobile1').val()
			+"&address1="+$('#userAddress1').val()
			+"&address2="+$('#userAddress2').val()
			+"&address3="+$('#userAddress3').val()
			+"&travelerType="+$('#travelerType').val()
			//+"&roleId="+$('#roleId').val()
			+"&emailId="+$('#userEmail').val()
			+"&emailId1="+$('#userEmail1').val();
			
			
			$http(
			{
				method : 'POST',
				data : fdata,
				headers : {
					'Content-Type' : 'application/x-www-form-urlencoded'
				},
				url : 'cancellation-request'
			}).then(function successCallback(response) {
					//alert(response.data);
					$scope.cancelrequest = response.datasss;
                   alert("Your Request updated sucessfully");
					console.log($scope.singleuser);
			}, function errorCallback(response) {
				// called asynchronously if an error occurs
				// or server returns response with an error status.
			});
		};
	
		$scope.updateUloUserProfile=function(){
			
			
			var fdata = "userId="+$('#uloUserId').val() 	 	
			+"&userName="+$('#userName').val()
			+"&phone="+$('#userMobile').val()
			+"&phone1="+$('#userMobile1').val()
			+"&address1="+$('#userAddress1').val()
			+"&address2="+$('#userAddress2').val()
			+"&address3="+$('#userAddress3').val()
			+"&travelerType="+$('#travelerType').val()
			//+"&roleId="+$('#roleId').val()
			+"&emailId="+$('#userEmail').val()
			+"&emailId1="+$('#userEmail1').val();
			
			
			$http(
			{
				method : 'POST',
				data : fdata,
				headers : {
					'Content-Type' : 'application/x-www-form-urlencoded'
				},
				url : 'edit-family-user'
			}).then(function successCallback(response) {
					//alert(response.data);
					$scope.singleuser = response.data.data;
                   alert("profile updated sucessfully");
					console.log($scope.singleuser);
			}, function errorCallback(response) {
				// called asynchronously if an error occurs
				// or server returns response with an error status.
			});
		};
		
$scope.updateMbUloUserProfile=function(){
			
			
			var fdata = "userId="+$('#uloUserId').val() 	 	
			+"&userName="+$('#mbuserName').val()
			+"&phone="+$('#mbuserMobile').val()
			+"&phone1="+$('#mbuserMobile1').val()
			+"&address1="+$('#mbuserAddress1').val()
			+"&address2="+$('#mbuserAddress2').val()
			+"&address3="+$('#mbuserAddress3').val()
			+"&travelerType="+$('#mbtravelerType').val()
			//+"&roleId="+$('#roleId').val()
			+"&emailId="+$('#mbuserEmail').val()
			+"&emailId1="+$('#mbuserEmail1').val();
			
			
			$http(
			{
				method : 'POST',
				data : fdata,
				headers : {
					'Content-Type' : 'application/x-www-form-urlencoded'
				},
				url : 'edit-family-user'
			}).then(function successCallback(response) {
					//alert(response.data);
					$scope.singleuser = response.data.data;
                   alert("profile updated sucessfully");
					console.log($scope.singleuser);
			}, function errorCallback(response) {
				// called asynchronously if an error occurs
				// or server returns response with an error status.
			});
		};
	
	
	//$scope.getUserRewards();
	$scope.getUpcomingBookings();
	$scope.getUser();
	
		}]);

		
	</script>
       
