<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="s" uri="/struts-tags"%>

   <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Rooms category
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
   
            <li class="active">Accommodation</li>
          </ol>
        </section>
		
        <!-- Main content -->
        <section class="content">
        
        	<div class="row">
            	<div class="col-xs-12" ng-if="accommodations.length>=0">
						<div class="box">
							<div class="box-header">
								<h3 class="box-title">Rooms category</h3>
							</div>
							
							
							<!-- /.box-header -->
							<div class="box-body table-responsive no-padding" > 
								<table class="table table-hover" id="example1">
									<tr>
					
										<!-- <th>Abbreviation </th> -->
										<th>Accommodation Type</th>
										<th>Tariff</th>
										<th>Room Count</th>
										<th>Edit</th>
										<th>Delete</th>
									
									</tr>
									<tr ng-repeat="a in accommodations">
										<!-- <td>{{a.abbreviation}}</td> -->
										<td>{{a.accommodationType}}</td>
										<td>{{a.baseAmount}}</td>
										<td>{{a.noOfUnits}}</td>
										<td>
											<a href="#editmodal" ng-click="getAccommodation(a.accommodationId)"  data-toggle="modal"><i  class="fa fa-pencil text-green"></i></a>
											
										</td>
										<td>
											
											<a href="#deletemodal" ng-click="deleteAccommodation(a.accommodationId)" ng-really-message="Are you sure ?" ><i  class="fa fa-fw fa-trash text-red" ></i></a>
										</td>
									</tr>
									<tr>
									</tr>
								</table>
							</div>
							<!-- /.box-body -->
							
							
							<div class="box-footer clearfix">
								 <ul class="pagination pagination-sm no-margin pull-right">
									<a class="btn btn-primary " href="#addmodal" ng-click="enableTask(x.DT_RowId)"  data-toggle="modal" >Add Accommodation</a>
								</ul> 
							</div>
						</div>
						<!-- /.box -->
					</div>	
         	</div>
          
        </section><!-- /.content -->
      	<input type="hidden" value ="<%=session.getAttribute("propertyAccommodationId") %>" id="propertyAccommodationId" name="propertyAccommodationId" >
      	<div class="modal" id="addmodal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" id="btnclose" class="close" data-dismiss="modal"
							aria-hidden="true">x</button>
						<h4 class="modal-title">Add Rooms</h4>
					</div>
					 <form name="addaccom">
					<div class="modal-body" >
					<div id="message"></div>
					
					
					<div class="form-group col-md-6">
							<label for="accommodation_type">Room Type</label>
							<input type="text" class="form-control"  name="accommodationType"  id="accommodationType"  placeholder="Accommodation Type" ng-model='accommodationType' ng-pattern="^/^[A-Za-z\d\s]+$/" ng-required="true" maxlength="20">
						    <span ng-show="addaccom.accommodationType.$error.pattern" style="color:red">Not a Room Type!</span>
					    </div>
					    <div class="form-group col-md-6">
							<label for="accommodation_type">Room code</label>
							<input type="text" class="form-control"  name="abbreviation" id="abbreviation" placeholder="Abbreviation" ng-model='abbreviation' ng-pattern="/^[a-zA-Z0-9]+$/" ng-required="true" maxlength="5">
						     <span ng-show="addaccom.abbreviation.$error.pattern" style="color:red">Not a Valid Room code!</span>
						</div>
								<div class="form-group col-md-6">
							<label for="no_of_units">No of Rooms</label>
							<input type="text" class="form-control"  name="noOfUnits"  id="noOfUnits"  placeholder="No of Units" ng-model='noOfUnits' ng-pattern="/^[0-9]/" ng-required="true">
						    <span ng-show="addaccom.noOfUnits.$error.pattern" style="color:red">Not a Valid Number!</span>
						     
						</div>
						
						<div class="form-group col-md-6">
							<label for="min_occupancy">Minimum Occupancy</label>
							<input type="text" class="form-control"  name="minOccupancy"  id="minOccupancy"  placeholder="Minimum Occupancy" ng-model='minOccupancy' ng-pattern="/^[0-9]/" ng-required="true">
						    <span ng-show="addaccom.minOccupancy.$error.pattern" style="color:red">Not a Valid Occupancy !</span>
						     
						</div>
						
						<div class="form-group col-md-6">
							<label for="max_occupancy">Maximum Occupancy</label>
							<input type="text" class="form-control"  name="maxOccupancy"  id="maxOccupancy"  placeholder="Maximum Occupancy" ng-model='maxOccupancy' ng-pattern="/^[0-9]/" ng-required="true">
						    <span ng-show="addaccom.maxOccupancy.$error.pattern" style="color:red">Not a Valid Occupancy !</span>
						     
						</div>
								<div class="form-group col-md-6">
							<label for="no_of_adults">No of Adults</label>
							<input type="text" class="form-control"  name="noOfAdults"  id="noOfAdults"  placeholder="No Of Adults" ng-model='noOfAdults' ng-pattern="/^[0-9]/" ng-required="true">
						    <span ng-show="addaccom.noOfAdults.$error.pattern" style="color:red">Enter the Numeric Value!</span>
						     
						</div>
						<div class="form-group col-md-6">
							<label for="no_of_child">No of Child </label>
							<input type="text" class="form-control"  name="noOfChild"  id="noOfChild"  placeholder="No Of Child" ng-model='noOfChild' ng-pattern="/^[0-9]/" ng-required="true">
						    <span ng-show="addaccom.noOfChild.$error.pattern" style="color:red">Enter the numerica valiue!</span>
						      
						</div>
						<div class="form-group col-md-6">
							<label for="child_included_rate">Tariff</label>
							<input type="text" class="form-control"  name="baseAmount"  id="baseAmount"  placeholder="Tariff" ng-model='baseAmount' ng-pattern="/^[0-9]/" ng-required="true">
						    <span ng-show="addaccom.baseAmount.$error.pattern" style="color:red">Enter the numerica valiue!</span>
						      
						</div>
						<div class="form-group col-md-6">
							<label for="child_included_rate">Extra Child</label>
							<input type="text" class="form-control"  name="extraChild"  id="extraChild"  placeholder="Extra Child Rate" ng-model='extraChild' ng-pattern="/^[0-9]/" ng-required="true">
						    <span ng-show="addaccom.extraChild.$error.pattern" style="color:red">Enter the numerica valiue!</span>
						      
						</div>
						<div class="form-group col-md-6">
							<label for="child_included_rate">Extra Adult</label>
							<input type="text" class="form-control"  name="extraAdult"  id="extraAdult"  placeholder="Extra Adult Rate" ng-model='extraAdult' ng-pattern="/^[0-9]/" ng-required="true">
						    <span ng-show="addaccom.extraAdult.$error.pattern" style="color:red">Enter the numerica valiue!</span>
						      
						</div>
						<div class="form-group col-md-6">
							<label for="child_included_rate">Extra Infant</label>
							<input type="text" class="form-control"  name="extraInfant"  id="extraInfant"  placeholder="Extra Infant Rate" ng-model='extraInfant' ng-pattern="/^[0-9]/" ng-required="true">
						    <span ng-show="addaccom.extraInfant.$error.pattern" style="color:red">Enter the numerica valiue!</span>
						      
						</div>
							<div class="form-group col-md-6">
							<label for="child_included_rate">Room Image</label>
						<input type="file" class="form-control"   file-input="files" name="photo" id="photo">
						</div>
					   
						
						 <!--  <div class="col-md-12">
				         <img class="img-thumbnail"  ngf-select="upload($file)" ngf-accept="'image/*'" ngf-pattern="'.jpg,.png,!.gif'" src="accommodation-picture?propertyAccommodationId={{accom.propertyAccommodationId}}"  alt="User Avatar" width="100%">
				         </div> -->
				         
						<!-- <div class="form-group col-md-12">
							<label for="child_included_rate">Room Description</label>
							<textarea  class="form-control"  name="description" id="description" value="" placeholder="Description" maxlength="1000">{{cat.description}}</textarea>
						</div> -->
				        
				       
						
					</div>
					<div class="modal-footer">
	
						<a href="#" data-dismiss="modal" class="btn btn-danger">Close</a>
						<button ng-click="addaccom.$valid && addAccommodation()" ng-disabled="addaccom.$invalid" class="btn btn-primary">Save</button>
					</div>
					</form>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dalog -->
		</div>
		<div class="modal" id="editmodal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" id="editbtnclose" class="close" data-dismiss="modal"
							aria-hidden="true">x</button>
						<h4 class="modal-title">Edit Accommodation</h4>
					</div>
					 <form name="editaccom">
					<div class="modal-body" ng-repeat ="accom in accommodation">
					<div id="editmessage"></div>
					    <input type="hidden" value="{{accom.propertyAccommodationId}}" id="editPropertyAccommodationId">
					    		<div class="form-group col-md-6">
							<label for="accommodation_type">Room Type</label>
							<input type="text" class="form-control"  name="editAccommodationType"  id="editAccommodationType"  value="{{accom.accommodationType}}"  ng-required="true" maxlength="20">
						    <span ng-show="editaccom.accommodationType.$error.pattern">Not a Room Type!</span>
						     
						</div>
					    <div class="form-group col-md-6">
							<label for="accommodation_type">Room Code</label>
							<input type="text" class="form-control"  name="editAbbreviation" id="editAbbreviation" value="{{accom.abbreviation}}"  ng-required="true" maxlength="5">
						     <span ng-show="editaccom.abbreviation.$error.pattern" >Not a Valid Code!</span>
						</div>
						
				
								<div class="form-group col-md-6">
							<label for="no_of_units">No of Rooms</label>
							<input type="text" class="form-control"  name="editNoOfUnits"  id="editNoOfUnits"  value="{{accom.noOfUnits}}" ng-model='accom.noOfUnits' ng-pattern="/^[0-9]/"  ng-required="true" readonly>
						    <span ng-show="editaccom.editNoOfUnits.$error.pattern" style="color:red">Not a Valid Number!</span>
						     
						</div>
						<div class="form-group col-md-6">
							<label for="min_occupancy">Minimum Occupancy</label>
							<input type="text" class="form-control"  name="editMinOccupancy"  id="editMinOccupancy"  value="{{accom.minimumOccupancy}}" ng-model='accom.minimumOccupancy' ng-pattern="/^[0-9]/" ng-required="true">
						    <span ng-show="editaccom.editMinOccupancy.$error.pattern" style="color:red">Not a Valid Occupancy !</span>
						     
						</div>
						
					    <div class="form-group col-md-6">
							<label for="max_occupancy">Maximum Occupancy</label>
							<input type="text" class="form-control"  name="editMaxOccupancy"  id="editMaxOccupancy"  value="{{accom.maximumOccupancy}}" ng-model='accom.maximumOccupancy' ng-pattern="/^[0-9]/" ng-required="true">
						    <span ng-show="editaccom.editMaxOccupancy.$error.pattern" style="color:red">Not a Valid Occupancy !</span>
						     
						</div>
								<div class="form-group col-md-6">
							<label for="adults_included_rate">No of Adults</label>
							<input type="text" class="form-control"  name="editNoOfAdults"  id="editNoOfAdults"   value="{{accom.noOfAdults}}" ng-model='accom.noOfAdults' ng-pattern="/^[0-9]/"  ng-required="true">
						    <span ng-show="editaccom.editNoOfAdults.$error.pattern" style="color:red">Enter the Numeric Value!</span>
						     
						</div>
						<div class="form-group col-md-6">
							<label for="child_included_rate">No of Child</label>
							<input type="text" class="form-control"  name="editNoOfChild"  id="editNoOfChild"   value="{{accom.noOfChild}}" ng-model='accom.noOfChild' ng-pattern="/^[0-9]/" ng-required="true">
						    <span ng-show="editaccom.editNoOfChild.$error.pattern" style="color:red">Enter the numerica valiue!</span>
						      
						</div>
						<div class="form-group col-md-6">
							<label for="child_included_rate">Base Amount</label>
							<input type="text" class="form-control"  name="editBaseAmount"  id="editBaseAmount"  value="{{accom.baseAmount}}" ng-model='accom.baseAmount' ng-pattern="/^[0-9]/" ng-required="true" >
						    <span ng-show="editaccom.editBaseAmount.$error.pattern" style="color:red">Enter the numerica valiue!</span>
						      
						</div>
						<div class="form-group col-md-6">
							<label for="child_included_rate">Extra Child</label>
							<input type="text" class="form-control"  name="editExtraChild"  id="editExtraChild"  value="{{accom.extraChild}}" ng-model='accom.extraChild' ng-pattern="/^[0-9]/"  ng-required="true">
						    <span ng-show="editaccom.editExtraChild.$error.pattern" style="color:red">Enter the numerica valiue!</span>
						      
						</div>
						<div class="form-group col-md-6">
							<label for="child_included_rate">Extra Adult</label>
							<input type="text" class="form-control"  name="editExtraAdult"  id="editExtraAdult" value="{{accom.extraAdult}}" ng-model='accom.extraAdult' ng-pattern="/^[0-9]/" ng-required="true">
						    <span ng-show="editaccom.editExtraAdult.$error.pattern" style="color:red">Enter the numerica valiue!</span>
						      
						</div>
						<div class="form-group col-md-6">
							<label for="child_included_rate">Extra Infant</label>
							<input type="text" class="form-control"  name="editExtraInfant"  id="editExtraInfant" value="{{accom.extraInfant}}" ng-model='accom.extraInfant' ng-pattern="/^[0-9]/" ng-required="true">
						    <span ng-show="editaccom.editExtraInfant.$error.pattern" style="color:red">Enter the numerica valiue!</span>
						      
						</div>
						  	<div class="form-group col-md-6">
							<label for="child_included_rate">Room Image</label>
						<input type="file" class="form-control"   file-input="files" name="photo" id="photo">
						</div>
						<div class="form-group col-md-12">
							<label for="child_included_rate"> Room Description</label>
							<textarea type="text" class="form-control"  name="editDescription" id="editDescription"  value="" placeholder="Description">{{accom.description}}</textarea>
						</div>
				         <div class="col-md-12">
				         <img class="img-thumbnail"  ngf-select="upload($file)"  ngf-accept="'image/*'" ngf-pattern="'.jpg,.png,!.gif'" src="accommodation-picture?propertyAccommodationId={{accom.propertyAccommodationId}}"  alt="User Avatar" width="100%">
				         </div>
				        
				       
					   
				         
						<!-- ngf-select="upload($file)" <img src="accommodation-picture?propertyAccommodationId={{accom.propertyAccommodationId}}" alt="" class="img-responsive">   -->
					</div>
					<div class="modal-footer">
	
						<a href="#" data-dismiss="modal" class="btn btn-danger">Close</a>
						<button ng-click="editaccom.$valid && editAccommodation()" ng-disabled="editaccom.$invalid" class="btn btn-primary">Save</button>
					</div>
					</form>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dalog -->
		</div>
		
		<div class="modal" id="deletemodal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" id="deletebtnclose" class="close" data-dismiss="modal"
							aria-hidden="true">x</button>
						<h4 class="modal-title">Delete Message</h4>
					</div>
					<div class="modal-body">
					 <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-check"></i>Your Accommodation has been deleted Sucessfully </h4>
                  
                 </div>
					    
						
					</div>
					<div class="modal-footer">
	                    
						<a href="#" data-dismiss="modal" class="btn">Close</a>
						
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dalog -->
		</div>
    		<div id="confirm" class="modal hide fade">
			  <div class="modal-body">
			    Are you sure?
			  </div>
					 <div class="modal-footer">
			    <button type="button" data-dismiss="modal" class="btn btn-primary" id="delete">Delete</button>
			    <button type="button" data-dismiss="modal" class="btn">Cancel</button>
			  </div>
			  </div>
  

      </div><!-- /.content-wrapper -->
      


<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>

	<script>


 
	
	var app = angular.module('myApp',['ngFileUpload']);
	app.controller('customersCtrl',['$scope',
	             					'Upload',
	            					'$timeout',
	            					'$http', function($scope,Upload,$timeout,$http) {

		
		
		$("#alert-message").hide();
			

			
	       	$timeout(function(){
	      	    // $scope.progressbar.complete();
	            $scope.show = true;
	            $("#pre-loader").css("display","none");
	        }, 2000);

			 
			
			$scope.unread = function() {
			//var notifiurl = "http://localhost:8085/collaborative-workflow/unreadnotifications.action";
			var notifiurl = "unreadnotifications.action";
			$http.get(notifiurl).success(function(response) {
				$scope.latestnoti = response.data;
			});
			};

			
			
			$scope.getAccommodations = function() {

				var url = "get-accommodations";
				$http.get(url).success(function(response) {
				    //console.log(response);
					$scope.accommodations = response.data;
		
				});
			};
			
			
			$scope.addAccommodation = function(){
				
				//alert($('#accommodationType').val());
				//var file = $('#photo');
				 // var file = document.getElementById('photo').files[0];
				 
				 var file = $scope.files;
				 alert(file.toString());
				 console.log(file); 
				
				var fdata = "abbreviation=" + $('#abbreviation').val()
				+ "&accommodationType=" + $('#accommodationType').val()
				+ "&noOfUnits=" + $('#noOfUnits').val()
				+ "&minOccupancy=" + $('#minOccupancy').val()
				+ "&maxOccupancy=" + $('#maxOccupancy').val()
				+ "&noOfAdults=" + $('#noOfAdults').val()
				+ "&noOfChild=" + $('#noOfChild').val()
				+ "&baseAmount=" + $('#baseAmount').val()
				+ "&extraChild=" + $('#extraChild').val()
				+ "&extraAdult=" + $('#extraAdult').val()
				+ "&extraInfant=" + $('#extraInfant').val()
				+ "&description=" + $('#description').val();
				
				
				
				
			    
				
				$http(
						{
							method : 'POST',
							data : fdata,
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							url : 'add-accommodation'
						}).then(function successCallback(response) {
							
							$scope.addedAccommodation = response.data;
                            
							console.log($scope.addedAccommodation.data[0].accommodationId);
					        //var accommodationId = 200;
					        
							var accommodationId = $scope.addedAccommodation.data[0].accommodationId;
							
							 Upload.upload({
					                url: 'accommodationpicupdate',
					                enableProgress: true, 
					                data: {myFile: file, 'propertyAccommodationId': accommodationId} 
					            }).then(function (resp) {
					            	
					            	
					            });
							 
							 
							
							
				            $scope.getAccommodations();
							var alertmsg = ' <div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Alert!</h4>Success Item Added Succesfully.</div>';
				            $('#message').html(alertmsg);
				            window.location.reload(); 
							$timeout(function(){
					      	$('#btnclose').click();
					        }, 2000); 
				        	
				        	
							
						   	
							//$('#btnclose').click();
				}, function errorCallback(response) {
					
				  
					// called asynchronously if an error occurs
					// or server returns response with an error status.
				});

			};
			
			$scope.editAccommodation = function(){
				
				
				var fdata = "abbreviation=" + $('#editAbbreviation').val()
				+ "&accommodationType=" + $('#editAccommodationType').val()
				+ "&noOfUnits=" + $('#editNoOfUnits').val()
				+ "&minOccupancy=" + $('#editMinOccupancy').val()
				+ "&maxOccupancy=" + $('#editMaxOccupancy').val()
				+ "&noOfAdults=" + $('#editNoOfAdults').val()
				+ "&noOfChild=" + $('#editNoOfChild').val()
				+ "&baseAmount=" + $('#editBaseAmount').val()
				+ "&extraChild=" + $('#editExtraChild').val()
				+ "&extraAdult=" + $('#editExtraAdult').val()
				+ "&extraInfant=" + $('#editExtraInfant').val()
				+ "&description=" + $('#editDescription').val()
				+ "&PropertyAccommodationId=" + $('#editPropertyAccommodationId').val();
				
				
				
				//var file = $('#photo');
				//var accommodationId = $('#editPropertyAccommodationId').val();
				
				//alert(fdata);
				$http(
						{
							method : 'POST',
							data : fdata,
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							url : 'edit-accommodation'
						}).then(function successCallback(response) {
							
							 
							var alertmsg = '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Alert!</h4>Success Item Added Succesfully.</div>';
				            $('#editmessage').html(alertmsg);
				            window.location.reload(); 
							$timeout(function(){
					      	$('#editbtnclose').click();
					        }, 2000);
							
							
				}, function errorCallback(response) {
					
					
					// called asynchronously if an error occurs
					// or server returns response with an error status.
				});

			};
			
            
			
			
	   $scope.upload = function (file) {
	 			
		   
		       console.log(file);
	 			
	        	var accommodationId = document.getElementById("editPropertyAccommodationId").value;
	        	
	            Upload.upload({
	                url: 'accommodationpicupdate',
	                enableProgress: true, 
	                data: {myFile: file, 'propertyAccommodationId': accommodationId} 
	            }).then(function (resp) {
	            	
	            	//$scope.getUserPhotos();
	               // console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
	                //$scope.getPropertyPhotos();
	            });
	           
	            
	     };
			
			
            
			$scope.getAccommodation = function(rowid) {
				
				
				var url = "get-accommodation?propertyAccommodationId="+rowid;
				$http.get(url).success(function(response) {
				    console.log(response);
					$scope.accommodation = response.data;
		
				});
				
			};
			
            $scope.deleteAccommodation = function(rowid) {
            	
            	var check = confirm("Are you sure you want to delete this Category?");
			if( check == true){
               
				var url = "delete-accommodation?propertyAccommodationId="+rowid;
				$http.get(url).success(function(response) {
				    //console.log(response);
				    $scope.getAccommodations();
				    $('#deletemodal').modal('toggle');
				    $timeout(function(){
				      	$('#deletebtnclose').click();
				    }, 2000);
					
		
				});
			}
				
			};
			
			 $scope.getPropertyList = function() {
				 
		        	
		        	
			        var userId = $('#adminId').val();
		 			var url = "get-user-properties?userId="+userId;
		 			$http.get(url).success(function(response) {
		 			    
		 				$scope.props = response.data;
		 	
		 			});
		 		};
		 		
           $scope.change = function() {
     	   
		        //alert($scope.id);
		        
		        var propertyId = $scope.id;	
     	    var url = "change-user-property?propertyId="+propertyId;
	 			$http.get(url).success(function(response) {
	 				
	 				 window.location = '/ulopms/dashboard'; 
	 				//$scope.change = response.data;
	 	
	 			});
			       
		 		};
		 		
		 		// upload on file select or drop		       
		   
		 		
				
		    $scope.getPropertyList();
		    
			$scope.getAccommodations();
			
			
			//$scope.unread();
			//
			
			
	}]);


	app.directive('fileInput', ['$parse', function ($parse) {
	    return {
	        restrict: 'A',
	        link: function (scope, element, attributes) {
	            element.bind('change', function () {
	                $parse(attributes.fileInput)
	                .assign(scope,element[0].files[0])
	                scope.$apply()
	            });
	        }
	    };
	}]);		
		   
		        

		
	</script>
			<script>
		  $(function () {
		    $("#example1").DataTable();
		    $('#example2').DataTable({
		      "paging": true,
		      "lengthChange": false,
		      "searching": false,
		      "ordering": true,
		      "info": true,
		      "autoWidth": false
		    });
		  });
		</script>
	<link rel="stylesheet" href="dist/css/dataTables.bootstrap.css">
	<script src="js1/upload/ng-file-upload-shim.min.js"></script>
	<script src="js1/upload/ng-file-upload.min.js"></script>
	 <script src="js1/jquery.dataTables.min.js" type="text/javascript"></script>
	 <script src="js1/dataTables.bootstrap.min.js" type="text/javascript"></script>
       