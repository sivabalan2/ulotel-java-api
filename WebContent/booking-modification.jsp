<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!-- <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/themes/south-street/jquery-ui.min.css" />
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<style>
.tablerow{
    overflow-x:scroll;
    width:1000px;
}

table.table-bordered > tbody > tr > th {
//border:1px solid #3c8dbc ;
}
table.table-bordered  {
  // border:1px solid #3c8dbc ;
/*     margin-top:20px; */
 }
table.table-bordered > thead > tr > th{
   //border:1px solid #3c8dbc ;
}
table.table-bordered > tbody > tr > td{
   //border:1px solid #3c8dbc ;
}
.tablesuccess{
    background-color: #092f53 !important;
    color:#ffffff !important;
    text-align:center;
}
</style>
   <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
           
            <small>Booking Modification</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        
            <li class="active">Booking Modification</li>
          </ol>
        </section>
		
        <!-- Main content -->
        <section class="content">
        
        	 <div class="row">
            <div class="col-xs-12">
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#editvoucher" data-toggle="tab">Booking Modification</a></li>
               </ul>
                <div class="tab-content">
                  <!-- Font Awesome Icons -->
             <div class="tab-pane active" id="editvoucher">
        		          <div class="row">
        <form name="bookingForm">
                 <div class="form-group col-md-3">
                <label>Booking Id:</label>

                    
        
                  <input type="text" id="filterBookingId" class="form-control" name="filterBookingId"  value="" ng-required="true" autocomplete="off">
 
                  
           
                <!-- /.input group -->
              </div>
   
              <div class="col-md-2">
              <button type="submit" ng-click="getBookedDetails()" class="btn btn-block btn-primary btn-sm" style="margin-top:25px">Search</button>
             </div>
             
   
        </form>
        
            </div>
                	<div class="table-responsive no-padding"  > 
								<table class="table table-bordered" id="example1">
									<h4>Availability</h4>
									<tr class="tablesuccess">
										
										<th>Room Category</th>										
										<th>Room Count</th>
										<th>Adult</th>
										<th>Child</th>
									    <th>Price</th>
									    <th>Tax</th>
										<th>Advance</th>
										
										<th>Due</th>
										
										
										
									</tr>
										<tr ng-repeat="bl in bookinglist track by $index">
									    <td ng-model="accommodationType">{{bl.accommodationType}}</td>
										<td>{{bl.rooms}}</td>
<!--  										<td>{{bl.adultsCount}}</td> -->
<!-- 									    <td>{{bl.childCount}}</td> -->
									    <td><input type="text" class="form-control"  name="adultCount" ng-keyup="editRoom(bl.total,bl.advanceAmount,bl.tax,bl.adultsCount,bl.childCount,$index)" id="adultCount" ng-model='bl.adultsCount' ng-pattern="/^[ A-Za-z0-9_@./#&+-]*$/" ng-required="true" maxlength="250" autocomplete="off" ></td>									    
									    <td><input type="text" class="form-control"  name="childCount" ng-keyup="editRoom(bl.total,bl.advanceAmount,bl.tax,bl.adultsCount,bl.childCount,$index)" id="childCount" ng-model='bl.childCount' ng-pattern="/^[ A-Za-z0-9_@./#&+-]*$/" ng-required="true" maxlength="250" autocomplete="off" ></td>
                                     <!--  <td><input type="text" class="form-control"  name="noofInfant" ng-keyup="addRoom(av.accommodationId,av.accommodationType,av.available,noofAdults,noofChilds,noofInfant,priceAmount,taxAmount,ad.value,$index)" id="noofInfant"  placeholder="Infant" ng-model='noofInfant' ng-pattern="/^[ A-Za-z0-9_@./#&+-]*$/" ng-required="true" maxlength="250" autocomplete="off"></td> --> 
									    <td><input type="text" class="form-control"  name="priceAmount" ng-keyup="editRoom(bl.total,bl.advanceAmount,bl.tax,bl.adultsCount,bl.childCount,$index)" id="priceAmount" ng-model='bl.total' placeholder="Price"  value="{{bl.total | number}}" ng-pattern="/^[ A-Za-z0-9_@./#&+-]*$/" ng-required="true" maxlength="250" autocomplete="off" ></td>									    
									    <td><input type="text" class="form-control"  name="taxAmount" ng-keyup="editRoom(bl.total,bl.advanceAmount,bl.tax,bl.adultsCount,bl.childCount,$index)" id="taxAmount" ng-model='bl.tax' placeholder="Tax" value="{{bl.tax | number}}" ng-pattern="/^[ A-Za-z0-9_@./#&+-]*$/" ng-required="true" maxlength="250" autocomplete="off" ></td>
									    <td><input type="text" class="form-control"  name="advance" ng-keyup="editRoom(bl.total,bl.advanceAmount,bl.tax,bl.adultsCount,bl.childCount,$index)" id="advance" ng-model='bl.advanceAmount' placeholder="Advance"  value="{{bl.advanceAmount | number}}" ng-pattern="/^[ A-Za-z0-9_@./#&+-]*$/" ng-required="true" maxlength="250" autocomplete="off"></td>
									   
									    <td><input type="text" class="form-control"  name="due" ng-keyup="editRoom(bl.total,bl.advanceAmount,bl.tax,bl.adultsCount,bl.childCount,$index)" id="due{{$index}}" ng-model='bl.due' placeholder="due" value="{{bl.due | number}}" ng-pattern="/^[ A-Za-z0-9_@./#&+-]*$/" ng-required="true" maxlength="250" autocomplete="off" disabled></td>
									   
									 
									    
									</tr>
									<tr>
								
										<!-- <td>Sub Total <br> 100rs</td>
										<td>Grand Total <br> 100rs</td>
									    <td>Deposit <br> 100rs</td> -->
									</tr>
								</table>
								<div>
									 <div class="form-group col-sm-3">
										<label for="firstName">Name<span style="color:red">*</span></label>
										<input type="text" class="form-control"  name="guestName"  id="guestName"  placeholder="Name" ng-model="bookinglist[0].guestName" ng-pattern="/^[a-zA-Z\s]*$/" ng-required="true" maxlength="50">
									</div> 
									<div class="form-group col-sm-3">
										<label for="guestEmail">E-mail<span style="color:red">*</span></label>
										<input type="email" class="form-control"  name="guestMailId"  id="guestMailId"  placeholder="Email" ng-model='bookinglist[0].emailId' ng-pattern="/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i" ng-required="true" maxlength="50">
									</div>
									<div class="form-group col-sm-3">
										<label for="landlinePhone">Mobile<span style="color:red">*</span></label>
										<input type="text" class="form-control"  name="guestNumber"  id="guestNumber"  placeholder="Mobile Number " ng-model='bookinglist[0].mobileNumber' ng-pattern="/^[0-9]*$/" ng-required="true" maxlength="10">
									</div>
									<div class="form-group col-sm-3">
										<label for="guestOtaBookingId">OTA Booking ID<span style="color:red">*</span></label>
										<input type="text" class="form-control"  name="guestOtaBookingId"  id="guestOtaBookingId"  placeholder="OTA Booking Id " ng-model="bookinglist[0].otaBookingId" ng-pattern="" ng-required="true" maxlength="15">
									</div>
									<div class="form-group col-sm-3">
										<label for="specialRequest">Special Request<span style="color:red">*</span></label>
										<textarea class="form-control"  name="specialRequest"  id="specialRequest"  placeholder="special" ng-model="bookinglist[0].specialRequest" ng-pattern="" ng-required="true"></textarea>
									</div>
									<%-- <div class="form-group col-sm-3">
									<label>Add-on</label>
										<select name="addon" id="addon" ng-change="updateAddon()" ng-model="addon" class="form-control ">
											<option value="">Choose Addon</option>
											<option ng-repeat="pa in propertyAddon" ng-selected="pa.addonName == bookinglist[0].addonName" value="{{pa.propertyAddonId}}">{{pa.addonName}} -- {{pa.addonRate |  currency:"RS. "}}</option>
																
										</select>
									</div> --%>
									<div>
										<input type="text"   class="form-control hidden"  name="guestId"  id="guestId"  placeholder="guestId" ng-model="bookinglist[0].guestId">
									</div>
								</div>
								<div class="form-group col-sm-12">
								<div id="updatevoucher"></div><div id="resendvoucher"></div>
								</div>
								  
								<div class="modal-footer">
								  
									 <button ng-click="updateBooking()"  id="upvoucher" class="btn btn-primary">{{upvoucher}}</button> <!-- ng-disabled="addguestinformation.$invalid" -->
								     <button ng-click="resendVoucher()"  id="revoucher" class="btn btn-danger">{{revoucher}}</button> <!-- ng-disabled="addguestinformation.$invalid" -->
								</div>
							</div>
        		 </div>		
				
    
        				
				
    
                        
          
          
        </section><!-- /.content -->
                
                      
                        

                </div><!-- /.tab-content -->
                <!-- guest information begins -->
              	  

              	
              </div><!-- /.nav-tabs-custom -->
            </div><!-- /.col -->
          </div><!-- /.row -->
          </div>
        
        
          
          
        </section><!-- /.content -->
      
    

      </div><!-- /.content-wrapper -->
      


<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>

	 <script src="js/jquery-2.1.1.js" type="text/javascript"></script>
 <script>
/*  $('.datepicker').datepicker({
	    dateFormat: 'dd-mm-yy'
	 }); */
 </script>
	<script>
    
	 
	
	var app = angular.module('myApp', ['ngProgress']);
	app.controller('customersCtrl', function($scope, $http,$timeout,ngProgressFactory) {
		
		var spinner = $('#loadertwo');
		$("#bookCheckin").datepicker({
            dateFormat: 'MM d, yy',
          
            //minDate:  0,
            onSelect: function (formattedDate) {
                var date1 = $('#bookCheckin').datepicker('getDate'); 
                var date = new Date( Date.parse( date1 ) ); 
                date.setDate( date.getDate() + 1 );        
                var newDate = date.toDateString(); 
                newDate = new Date( Date.parse( newDate ) );   
                $('#bookCheckout').datepicker("option","minDate",newDate);
                $timeout(function(){
                	document.getElementById("bookCheckin").style.borderColor = "LightGray";
                });
            }
        });

        $("#bookCheckout").datepicker({
            dateFormat: 'MM d, yy', 
          
            //minDate:  0,
            onSelect: function (formattedDate) {
                var date2 = $('#bookCheckout').datepicker('getDate'); 
                $timeout(function(){
                	document.getElementById("bookCheckout").style.borderColor = "LightGray";
                });
            }
        }); 
		 $('.select2').select2()
       	$timeout(function(){
      	    // $scope.progressbar.complete();
            $scope.show = true;
            $("#pre-loader").css("display","none");
        }, 2000);

		 
		
		$scope.unread = function() {
		//var notifiurl = "http://localhost:8085/collaborative-workflow/unreadnotifications.action";
		var notifiurl = "unreadnotifications.action";
		$http.get(notifiurl).success(function(response) {
			$scope.latestnoti = response.data;
		});
		};

		var blnAddValue=false;
		
	
       
		$scope.blocknote = "Block";
		$scope.showprocessone = false;
       
                
       
       
       $scope.revoucher = "Resend Voucher";
      $scope.resendVoucher = function(){
    	   $scope.revoucher = "Please Wait..";
    	   $("#revoucher").prop('disabled', true);
		   var bookingId = parseInt(document.getElementById('filterBookingId').value);
		   var mailFlag = true;
		   var mailSubjectFlag=true;
		   var text = '{"array":' + JSON.stringify($scope.bookinglist) + '}';
		   
		   var gdata = "firstName=" + $('#guestName').val() 
			+ "&emailId=" + $('#guestMailId').val()
			+ "&phone=" + $('#guestNumber').val()
			+ "&guestId="+$('#guestId').val();
		   var otaBookingId=$('#guestOtaBookingId').val();
		   var specialRequest = $('#specialRequest').val();
		   console.log(text);
			var data = JSON.parse(text);
			spinner.show();
			$http(
   					{
   						method : 'POST',
   						data : data,
   						dataType: 'json',
   						headers : {
   							 'Content-Type' : 'application/json; charset=utf-8'

   						},
   						url : "edit-booking?bookingId="+bookingId+"&otaBookingId="+otaBookingId
   					}).then(function successCallback(response) {
   						spinner.show();
   					
				$http(
					    {
					    	method : 'POST',
							data : gdata,
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							url : 'edit-guest?bookingId='+ bookingId
						
					   }).then(function successCallback(response) {
						  
							
							spinner.show();
					$http(
						{
							method : 'POST',
							data : data,
							dataType: 'json',
							headers : {
								 'Content-Type' : 'application/json; charset=utf-8'
								
							},
							url : 'edit-booking-details?bookingId='+bookingId+'&mailFlag='+mailFlag+'&specialRequest='+specialRequest+'&mailSubjectFlag='+mailSubjectFlag
							//url : 'jsonTest'
							
						}).then(function successCallback(response){
							  //alert("The Voucher Resend Successfully");
							  $("#revoucher").prop('disabled', false);
							  $scope.revoucher = "Resend Voucher";
							  $("#updatevoucher").hide();
			                  var alertmsg = ' <div class="alert alert-success alert-dismissible" style="color:red;text-align:center;">The Voucher Resend Successfully</div>';
			                $('#updatevoucher').html(alertmsg);
			         	            $("#updatevoucher").fadeTo(2000, 500).slideUp(500, function(){
			         	                $("#updatevoucher").slideUp(500);
			                  });
			         	           setTimeout(function(){ spinner.hide(); 
			         	          }, 1000);
							 // window.location.reload(); 
							})  
					   
					   })
   				})
   			 $("#upvoucher").prop('disabled', false);
		 };
		 
		 
		 $scope.upvoucher = "Update Voucher";
          $scope.updateBooking = function(){
        	  $("#upvoucher").prop('disabled', true);
        	  $scope.upvoucher = "Please Wait...";
		   var bookingId = parseInt(document.getElementById('filterBookingId').value);
		   var mailFlag = false;
		   var text = '{"array":' + JSON.stringify($scope.bookinglist) + '}';
			var specialRequest = $('#specialRequest').val();
		   var gdata = "firstName=" + $('#guestName').val() 
			+ "&emailId=" + $('#guestMailId').val()
			+ "&phone=" + $('#guestNumber').val()
			+ "&guestId="+$('#guestId').val();
			
		  
		   var otaBookingId=$('#guestOtaBookingId').val();
		   
		   
		   console.log(text);
			var data = JSON.parse(text);
			spinner.show();
			$http(
   					{
   						method : 'POST',
   						data : data,
   						dataType: 'json',
   						headers : {
   							 'Content-Type' : 'application/json; charset=utf-8'

   						},
   						url : "edit-booking?bookingId="+bookingId+"&otaBookingId="+otaBookingId
   					}).then(function successCallback(response) {
   						
   					spinner.show();
				$http(
					    {
					    	method : 'POST',
							data : gdata,
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							url : 'edit-guest?bookingId='+bookingId
						
					   }).then(function successCallback(response) {
						 spinner.show();
							$http(
						{
							method : 'POST',
							data : data,
							dataType: 'json',
							headers : {
								 'Content-Type' : 'application/json; charset=utf-8'
							},
							url : 'edit-booking-details?bookingId='+bookingId+'&mailFlag='+mailFlag+'&specialRequest='+specialRequest
									
						}).then(function successCallback(response){
							 // alert("The Given details are updated successfully");
							 $("#upvoucher").prop('disabled', false);
							 $scope.upvoucher = "Update Voucher";
							  $("#updatevoucher").hide();
				                  var alertmsg = ' <div class="alert alert-success alert-dismissible" style="color:red;text-align:center;">The Given details are updated successfully</div>';
				                $('#updatevoucher').html(alertmsg);
				         	            $("#updatevoucher").fadeTo(2000, 500).slideUp(500, function(){
				         	                $("#updatevoucher").slideUp(500);
				                  });
				         	           setTimeout(function(){ spinner.hide(); 
				         	          }, 1000);
							   //window.location.reload(); 
							}) 
					   })
   					})
   				 $("#upvoucher").prop('disabled', false);
		 };
		 
		 
		$scope.range = function(min, max, step){
		    step = step || 1;
		    var input = [];
		    for (var i = min; i <= max; i += step) input.push(i);
		    return input;
		 };
		 
		
		 
		 

          $scope.editRoom = function(price,advance,tax,adult,child,indx){
	         var tariff=0;
	         
	         if(isNaN(parseFloat(price))){
	        	 tariff=0;	 
	         }else{
	        	 tariff=parseFloat(price);
	         }
	         
	         /* if(isNaN(parseFloat(tax))){
	        	 taxAmount=0;	 
	         }else{
	        	 taxAmount=parseFloat(tax);
	         } */
	         
	         /* if(tariff<1000){
	        	 taxAmount=0;
	         }else if(tariff>=1000 && tariff<=2500){
	        	 taxAmount=tariff*12/100;
	         }else if(tariff>=2501 && tariff<=7500){
	        	 taxAmount=tariff*18/100;
	         }else if(tariff>7500){
	        	 taxAmount=tariff*28/100;
	         } */
	         
// 	         taxAmount=Math.round(taxAmount);
	         var tot = parseFloat(tariff)+parseFloat(tax);
	         
	         var due = parseFloat(tot-advance).toFixed(2);
	         $('#due'+ indx).val(due);
	         $scope.bookinglist[indx].adultsCount = adult; 
	         $scope.bookinglist[indx].childCount = child;
	         $scope.bookinglist[indx].advanceAmount = advance;
	         $scope.bookinglist[indx].total = price; 
	         $scope.bookinglist[indx].tax = tax; 
	         $scope.bookinglist[indx].due = due;
	         
		 };

		
        
	 		
        $scope.change = function() {
 	   
	        //alert($scope.id);
	        
	        var propertyId = $scope.id;	
 	        var url = "change-user-property?propertyId="+propertyId;
 			$http.get(url).success(function(response) {
 				
 				 window.location = '/ulopms/dashboard'; 
 				//$scope.change = response.data;
 	
 			});
		       
	 		}; 
      
        
		$scope.getPropertyAddon = function() {

			var url = "get-property-addon";
			$http.get(url).success(function(response) {
				$scope.propertyAddon = response.data;
	
			});
		};
		
		/* $scope.updateAddon = function(){
        	var ad = $('#addon').val();
        	for(var i=0;i<$scope.propertyAddon.length;i++){
        		if($scope.propertyAddon[i].propertyAddonId == ad){
        			$scope.bookinglist[0].addonDescription= $scope.propertyAddon[i].addonName;
        			$scope.bookinglist[0].addonAmount = parseInt($scope.propertyAddon[i].addonRate);
        		}else if(ad == ''){
        			$scope.bookinglist[0].addonName = 'NA';
        			$scope.bookinglist[0].addonCost = 0;
        		}
        		
        	}
        	console.log(JSON.stringify($scope.bookinglist));
        } */

        $scope.getBookedDetails = function(){
				var filterBookingId="";
				
				filterBookingId=document.getElementById('filterBookingId').value;
				if(filterBookingId==""){
					filterBookingId="0";
				}
				
				var fdata = 'filterBookingId='+filterBookingId;
				
	    	   spinner.show();		    		
					$http(
							{
								method : 'POST',
								data : fdata,
								headers : {
									'Content-Type' : 'application/x-www-form-urlencoded'
								},
								url : 'get-bookingid-details'
							}).success(function(response) {
								
								$scope.bookinglist = response.data;
								setTimeout(function(){ spinner.hide(); 
								  }, 1000);
								
							});
		 		 
			};
	    
		
		$scope.getPropertyAddon();
        
	});

	</script>
  <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

	  <!-- iCheck 1.0.1 -->
    <script src="plugins/iCheck/icheck.min.js"></script>

