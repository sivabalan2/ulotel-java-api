<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!-- <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/themes/south-street/jquery-ui.min.css" />
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<style>
.tablerow{
    overflow-x:scroll;
    width:1000px;
}

table.table-bordered > tbody > tr > th {
//border:1px solid #3c8dbc ;
}
table.table-bordered  {
  // border:1px solid #3c8dbc ;
/*     margin-top:20px; */
 }
table.table-bordered > thead > tr > th{
   //border:1px solid #3c8dbc ;
}
table.table-bordered > tbody > tr > td{
   //border:1px solid #3c8dbc ;
}
.tablesuccess{
    background-color: #092f53 !important;
    color:#ffffff !important;
    text-align:center;
}
</style>
   <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
           
            <small>Booking Entry</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        
            <li class="active">Booking Entry</li>
          </ol>
        </section>
		
        <!-- Main content -->
        <section class="content">
        
        	 <div class="row">
            <div class="col-xs-12">
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#fa-icons" data-toggle="tab">Availability</a></li>
              </ul>
                <div class="tab-content">
                  <!-- Font Awesome Icons -->
                  <div class="tab-pane active" id="fa-icons">
        <section class="content">
        <div class="row">
          <div class="row">
        <form name="bookingForm" id="bookingForm">
                 <div class="form-group col-md-3">
							<label> Select Hotel</label>
							<select name="propertyId" data-allow-clear="true"  data-placeholder="select Hotel" ng-model="propertyId" id="propertyId" ng-change="getPropertyAccommodation(propertyId)" class="form-control">
                            <option  value="">Select Property</option>
                            <option  ng-repeat="p in properties" value="{{p.propertyId}}">{{p.propertyName}}</option>
	                         </select>
						  </div>
               	<div class="form-group col-md-3">
							<label> Select Accommodation</label>
							<select name="propertyAccommodationId" ng-model="propertyAccommodationId" id="propertyAccommodationId" data-allow-clear="true" multiple="multiple" class="form-control select2"> <!-- data-allow-clear="true" multiple="multiple" class="__select2" -->
	                       	<option value="">Select Your Accommodation</option>
	                       	<option ng-repeat="at in accommodation" value="{{at.accommodationId}}">{{at.accommodationType}}</option>
	                        </select>
						  </div>
                 <div class="form-group col-md-3">
                <label>Check-in:</label>

                     <div class="input-group date">
                   <label class="input-group-addon btn" for="bookCheckin">
                   <span class="fa fa-calendar"></span>
              </label>   
                  <input type="text" id="bookCheckin" class="form-control" name="bookCheckin"  value="" ng-required="true" autocomplete="off">
 
                  
                </div>
                <!-- /.input group -->
              </div>
                        <div class="form-group col-md-3">
                <label>Check-out:</label>

                <div class="input-group date">
                   <label class="input-group-addon btn" for="bookCheckout">
                   <span class="fa fa-calendar"></span>
              </label>   
                  <input type="text" id="bookCheckout" class="form-control" name="bookCheckout"  value="" ng-required="true" autocomplete="off">
 
                  
                </div>
                <!-- /.input group -->
              </div>
              
              <div class="col-md-12">
              <button type="submit" ng-click="getRoomAvailability()" class="btn btn-primary btn-sm pull-right" >Search</button>
             </div>
               <div class="col-md-12">
              <div id="selectError"></div>
             </div>
   
        </form>
         
            </div>
          
            	<form name="addRowInformation" id="addRowInformation">
            	<div class="table-responsive no-padding"  > 
								<table class="table table-bordered table-responsive tablerow" id="example1">
									<h4>Availability</h4>
									<tr class="tablesuccess">
										<th>Room Category</th>										
										<th>Available</th>
										<th style="white-space: nowrap; overflow: hidden;">Select Room</th>
										<th style="white-space: nowrap; overflow: hidden;">&nbsp;&nbsp;&nbsp;&nbsp;Adult&nbsp;&nbsp;&nbsp;&nbsp;</th>
										<th style="white-space: nowrap; overflow: hidden;">&nbsp;&nbsp;&nbsp;&nbsp;Child&nbsp;&nbsp;&nbsp;&nbsp;</th>
									    <th style="white-space: nowrap; overflow: hidden;">Tariff Amount</th>
									    <th style="white-space: nowrap; overflow: hidden;">Tax Amount</th>
									    <th style="white-space: nowrap; overflow: hidden;">OTA Commission</th>
								        <th style="white-space: nowrap; overflow: hidden;">&nbsp;&nbsp;OTA %&nbsp;&nbsp;</th>
								        <th style="white-space: nowrap; overflow: hidden;">&nbsp;&nbsp;OTA Tax&nbsp;&nbsp;</th>
								        <th style="white-space: nowrap; overflow: hidden;">&nbsp;&nbsp;TCS %&nbsp;&nbsp;</th>
								        <th style="white-space: nowrap; overflow: hidden;">&nbsp;&nbsp;OTA TCS&nbsp;&nbsp;</th>
								        <th style="white-space: nowrap; overflow: hidden;">Revenue Amount</th>
										<th style="white-space: nowrap; overflow: hidden;">Advance Amount</th>
								        <th style="white-space: nowrap; overflow: hidden;">Due Amount</th>
										
										<th>Action</th>
									</tr>
									<tr ng-repeat="av in availablities track by $index">
										<td ng-model="accommodationType">{{av.accommodationType}}</td>
										<td>{{av.available}}</td>
										<td><select  ng-model="ad.value" id="rooms" class="form-control" ng-change="addRoom(av.accommodationId,av.accommodationType,av.available,noofAdults,noofChilds,noofInfant,priceAmount,advance,taxAmount,otaCommission,otaPercent,otaTax,tcsPercent,otaTcs,revenue,ad.value,$index)"><option ng-repeat="ad in range(1,av.available)" value="{{ad}}">{{ad}}</option></select></td>
 										<td><input type="number" class="form-control"  name="noofAdults" ng-keyup="addRoom(av.accommodationId,av.accommodationType,av.available,noofAdults,noofChilds,noofInfant,priceAmount,advance,taxAmount,otaCommission,otaPercent,otaTax,tcsPercent,otaTcs,revenue,ad.value,$index)"  id="noofAdults"  placeholder="Adult" ng-model='noofAdults' ng-pattern="/^[ A-Za-z0-9_@./#&+-]*$/" ng-required="true" maxlength="250" autocomplete="off"></td>
									    <td><input type="number" class="form-control"  name="noofChilds" ng-keyup="addRoom(av.accommodationId,av.accommodationType,av.available,noofAdults,noofChilds,noofInfant,priceAmount,advance,taxAmount,otaCommission,otaPercent,otaTax,tcsPercent,otaTcs,revenue,ad.value,$index)"  id="noofChilds"  placeholder="Child" ng-model='noofChilds' ng-pattern="/^[ A-Za-z0-9_@./#&+-]*$/" ng-required="true" maxlength="250" autocomplete="off"></td>
                                     <!--  <td><input type="text" class="form-control"  name="noofInfant" ng-keyup="addRoom(av.accommodationId,av.accommodationType,av.available,noofAdults,noofChilds,noofInfant,priceAmount,taxAmount,ad.value,$index)" id="noofInfant"  placeholder="Infant" ng-model='noofInfant' ng-pattern="/^[ A-Za-z0-9_@./#&+-]*$/" ng-required="true" maxlength="250" autocomplete="off"></td> --> 
									    <td><input type="text" class="form-control"  name="priceAmount" ng-keyup="addRoom(av.accommodationId,av.accommodationType,av.available,noofAdults,noofChilds,noofInfant,priceAmount,advance,taxAmount,otaCommission,otaPercent,otaTax,tcsPercent,otaTcs,revenue,ad.value,$index)" id="priceAmount"  placeholder="Price" ng-model='priceAmount' ng-pattern="/^[ A-Za-z0-9_@./#&+-]*$/" ng-required="true" maxlength="250" autocomplete="off"></td>									    
									    <td><input type="text" class="form-control"  name="taxAmount" ng-keyup="addRoom(av.accommodationId,av.accommodationType,av.available,noofAdults,noofChilds,noofInfant,priceAmount,advance,taxAmount,otaCommission,otaPercent,otaTax,tcsPercent,otaTcs,revenue,ad.value,$index)" id="taxAmount"  placeholder="Tax" ng-model='taxAmount' ng-pattern="/^[ A-Za-z0-9_@./#&+-]*$/" ng-required="true" maxlength="250" autocomplete="off"></td>
									    <td><input type="text" class="form-control"  name="otaCommission" ng-keyup="addRoom(av.accommodationId,av.accommodationType,av.available,noofAdults,noofChilds,noofInfant,priceAmount,advance,taxAmount,otaCommission,otaPercent,otaTax,tcsPercent,otaTcs,revenue,ad.value,$index)" id="otaCommission"  placeholder="OTA Commission" ng-model='otaCommission' ng-pattern="/^[ A-Za-z0-9_@./#&+-]*$/" ng-required="true" maxlength="250" autocomplete="off"></td>
									    <td><input type="text" class="form-control"  name="otaPercent" ng-keyup="addRoom(av.accommodationId,av.accommodationType,av.available,noofAdults,noofChilds,noofInfant,priceAmount,advance,taxAmount,otaCommission,otaPercent,otaTax,tcsPercent,otaTcs,revenue,ad.value,$index)" id="otaPercent"  placeholder="OTA Percent" ng-model='otaPercent' ng-pattern="/^[ A-Za-z0-9_@./#&+-]*$/" ng-required="true" maxlength="250" autocomplete="off"></td>
									    <td><input type="text" class="form-control"  name="otaTax" ng-keyup="addRoom(av.accommodationId,av.accommodationType,av.available,noofAdults,noofChilds,noofInfant,priceAmount,advance,taxAmount,otaCommission,otaPercent,otaTax,tcsPercent,otaTcs,revenue,ad.value,$index)" id="otaTax{{$index}}"  placeholder="OTA Tax" ng-model='otaTax' ng-pattern="/^[ A-Za-z0-9_@./#&+-]*$/" ng-required="true" maxlength="250" autocomplete="off"></td>
									    <td><input type="text" class="form-control"  name="tcsPercent" ng-keyup="addRoom(av.accommodationId,av.accommodationType,av.available,noofAdults,noofChilds,noofInfant,priceAmount,advance,taxAmount,otaCommission,otaPercent,otaTax,tcsPercent,otaTcs,revenue,ad.value,$index)" id="tcsPercent{{$index}}"  placeholder="TCS Percent" ng-model='tcsPercent' ng-pattern="/^[ A-Za-z0-9_@./#&+-]*$/" ng-required="true" maxlength="250" autocomplete="off"></td>
									    <td><input type="text" class="form-control"  name="otaTcs" ng-keyup="addRoom(av.accommodationId,av.accommodationType,av.available,noofAdults,noofChilds,noofInfant,priceAmount,advance,taxAmount,otaCommission,otaPercent,otaTax,tcsPercent,otaTcs,revenue,ad.value,$index)" id="otaTcs{{$index}}"  placeholder="TCS Tax" ng-model='otaTcs' ng-pattern="/^[ A-Za-z0-9_@./#&+-]*$/" ng-required="true" maxlength="250" autocomplete="off"></td>
									    <td><input type="text" class="form-control"  name="revenue" ng-keyup="addRoom(av.accommodationId,av.accommodationType,av.available,noofAdults,noofChilds,noofInfant,priceAmount,advance,taxAmount,otaCommission,otaPercent,otaTax,tcsPercent,otaTcs,revenue,ad.value,$index)" id="revenue{{$index}}"  placeholder="Revenue" ng-model='revenue' ng-pattern="/^[ A-Za-z0-9_@./#&+-]*$/" ng-required="true" maxlength="250" autocomplete="off"></td>
									    <td><input type="text" class="form-control"  name="advance" ng-keyup="addRoom(av.accommodationId,av.accommodationType,av.available,noofAdults,noofChilds,noofInfant,priceAmount,advance,taxAmount,otaCommission,otaPercent,otaTax,tcsPercent,otaTcs,revenue,ad.value,$index)" id="advance"  placeholder="Advance"  ng-model='advance' ng-pattern="/^[ A-Za-z0-9_@./#&+-]*$/" ng-required="true" maxlength="250" autocomplete="off"></td>
									    <td><input type="text" class="form-control"  name="due" ng-keyup="addRoom(av.accommodationId,av.accommodationType,av.available,noofAdults,noofChilds,noofInfant,priceAmount,advance,taxAmount,otaCommission,otaPercent,otaTax,tcsPercent,otaTcs,revenue,ad.value,$index)" id="due{{$index}}"  placeholder="due" ng-model='due' ng-pattern="/^[ A-Za-z0-9_@./#&+-]*$/" ng-required="true" maxlength="250" autocomplete="off"></td>
				                        <td><button class="btn btn-xs btn-primary" ng-click ="removeRoom($index)">Remove</button></td>
									   
									    <!--<td><select ng-model="r.value" ng-change="update(r.value,$index)"><option ng-repeat="r in range(1,1)" value="{{r}}">{{r}}</option></select></td>
									    <!--  Selected Value is:{{rValue}}-->
									   
									    <!-- <td><a class="btn btn-xs btn-primary" href="#addmodal" ng-disabled="av.available == 0"  ng-click="addRow(av.accommodationId,av.accommodationType,av.baseAmount,av.arrivalDate,av.departureDate,av.rooms,av.noOfAdults,av.noOfChild,av.minOccupancy,av.extraAdult,av.extraChild,av.tax,av.available,$index)"  data-toggle="modal" min="0" >Add</a></td> -->
									    
									</tr>
									<tr>
								
										<!-- <td>Sub Total <br> 100rs</td>
										<td>Grand Total <br> 100rs</td>
									    <td>Deposit <br> 100rs</td> -->
									</tr>
								</table>
							</div>
						<!-- </form>
		
              	     <form name="addguestinformation" id="addguestinformation"> -->
              	     <h4>Guest Information</h4>
					<div class="modal-body" >
					<div class="row">
					<div id="message"></div>
					
					<div class="form-group col-sm-3">
							<label>Channel Manager<span style="color:red">*</span></label>
							<select   name="sourceId"  ng-model="sourceId" id="sourceId" placeholder="Select Source" class="form-control" ng-change="getSourceId(sourceId);getStayType(sourceId)" ng-required="true" autocomplete="off">
                           	<option  value="" selected >Select Source</option>
							<option ng-repeat="sr in sources | orderBy:'sourceName' " value="{{sr.sourceId}}"  ng-selected ="sr.sourceId == r.sourceId">{{sr.sourceName}}</option>
							</select>
						</div>	
					  
					   <div class="form-group col-sm-3">
							<label for="firstName">Name<span style="color:red">*</span></label>
							<input type="text" class="form-control"  name="firstName"  id="firstName"  placeholder="Name" ng-model='firstName' ng-pattern="/^[a-zA-Z\s]*$/" ng-required="true" maxlength="50" autocomplete="off">
						    <span ng-show="addguestinformation.firstName.$error.pattern" style="color:red">Enter The Valid First Name</span>
						</div> 
						
						<div class="form-group col-sm-3">
							<label for="guestEmail">E-mail<span style="color:red">*</span></label>
							<input type="email" class="form-control"  name="guestEmail"  id="guestEmail"  placeholder="Enter Your Email" ng-model='guestEmail' ng-pattern="/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i" ng-required="true" maxlength="50" autocomplete="off">
						    <span ng-show="addguestinformation.guestEmail.$error.pattern" style="color:red">Enter The Valid Email ID!</span>
						     
						</div>
								
						<div class="form-group col-sm-3">
							<label for="landlinePhone">Mobile<span style="color:red">*</span></label>
							<input type="text" class="form-control"  name="mobileNumber"  id="mobileNumber"  placeholder="Enter The Mobile Number " ng-model='mobileNumber' ng-pattern="/^[0-9]*$/" ng-required="true" maxlength="10" autocomplete="off">
						    <span ng-show="addguestinformation.mobileNumber.$error.pattern" style="color:red">Enter the Valid Phone Number!</span> 
						</div>
						
						<div class="form-group col-sm-3" id="otaId">
							<label for="otaBookingId">OTA Booking ID<span style="color:red">*</span></label>
							<input type="text" class="form-control"  name="otaBookingId"  id="otaBookingId"  placeholder="Enter The OTA Booking Id " ng-model="otaBookingId" ng-pattern="" ng-required="true" autocomplete="off">
						    <span ng-show="addguestinformation.otaBookingId.$error.pattern" style="color:red">Enter the Valid OTA Booking Id!</span> 
						</div>
						<div class="form-group col-sm-2" id="otaStayId">
							<label>Stay Type<span style="color:red">*</span></label>
							<select name="stayTypeId" ng-model="stayTypeId" id="stayTypeId" placeholder="Select Stay" class="form-control"  ng-required="true" ng-change="changeShot(stayTypeId)">
                           	<option disabled selected  value="0">Select Stay</option>
	                           	<option value="1">Day-Wise</option>
    	                       	<option value="2">Hourly-Wise</option>
							</select> 
							
						</div>
						<div>  
							<div class="form-group col-sm-3" id="slotTypeId">
							<label>Slot Type<span style="color:red">*</span></label>
							<select name="slotType" ng-model="slotType" id="slotType" placeholder="Select Slot" class="form-control"  ng-required="true">
                           	<option disabled selected  value="">Select Slot</option>
                           	<option value="1">00 - 01 Hour</option>
                           	<option value="2">00 - 02 Hours</option>
                            <option value="3">00 - 03 Hours</option>
                            <option value="4">00 - 04 Hours</option>
                           	<option value="5">00 - 05 Hours</option>
                            <option value="6">00 - 06 Hours</option>
                            <option value="7">00 - 07 Hours</option>
                           	<option value="8">00 - 08 Hours</option>
                            <option value="9">00 - 09 Hours</option>
                            <option value="10">00 - 10 Hours</option>
                           	<option value="11">00 - 11 Hours</option>
                            <option value="12">00 - 12 Hours</option>
                            <option value="24">00 - 24 Hours</option>
							</select>
						</div>
						 <div class="form-group col-sm-3" id="slotTypeHoursId">
							<label>Check In<span style="color:red">*</span></label>
							<select name="checkInTime"  ng-model="checkInTime" id="checkInTime" placeholder="Select Check-In Time" class="form-control" ng-change="changeTime()" ng-required="true">
                           	<option disabled selected value="">Select Check-In Time</option>
                           	<option value="0">12-AM</option>
                           	<option value="1">01-AM</option>
                           	<option value="2">02-AM</option>
                           	<option value="3">03-AM</option>
                           	<option value="4">04-AM</option>
                           	<option value="5">05-AM</option>
                           	<option value="6">06-AM</option>
                           	<option value="7">07-AM</option>
                           	<option value="8">08-AM</option>
                           	<option value="9">09-AM</option>
                           	<option value="10">10-AM</option>
                           	<option value="11">11-AM</option>
                           	<option value="12">12-PM</option>
                           	<option value="13">01-PM</option>
                           	<option value="14">02-PM</option>
                           	<option value="15">03-PM</option>
                           	<option value="16">04-PM</option>
                           	<option value="17">05-PM</option>
                           	<option value="18">06-PM</option>
                           	<option value="19">07-PM</option>
                           	<option value="20">08-PM</option>
                           	<option value="21">09-PM</option>
                           	<option value="22">10-PM</option>
                           	<option value="23">11-PM</option>
							</select>
							<input type="hidden" name="finalCheckInTime"  ng-model="finalCheckInTime" id="finalCheckInTime" placeholder="Select Check-Out Time" class="form-control"  ng-required="true">
						</div>
						<div class="form-group col-sm-4" id="slotTypeTimeId">
							<label>Check Out<span style="color:red">*</span></label>
							<input type="text" name="checkOutTime"  ng-model="checkOutTime" id="checkOutTime" placeholder="Select Check-Out Time" class="form-control"  ng-disabled="true">
							<input type="hidden" name="finalCheckOutTime"  ng-model="finalCheckOutTime" id="finalCheckOutTime" placeholder="Select Check-Out Time" class="form-control"  ng-required="true">
                           	
						</div>
					</div>
                         <div class="form-group col-sm-12">
							<label for="landlinePhone">Special Request <span style="color:red"></span></label>
							<textarea class="form-control"  name="specialRequest"  id="specialRequest"  ng-model='splRequest'></textarea>
						     
						</div>
								
						</div>
                             
						</div>
					<div class="modal-footer">
			 		
                    <div id="bookingSuccess"></div>
                    <div id="bookingFailed"></div>
					 <a href="#viewmodal" ng-click="viewBookingSplitUp()" class="btn btn-primary"  data-toggle="modal">View Split Up</a>  
				     <button ng-click="confirmBooking()" ng-disabled="addguestinformation.$invalid" id="loading-example-btn" class="btn btn-primary ">{{blocknote}}</button> <!-- ng-disabled="addguestinformation.$invalid" -->
					<!--  <a href="#viewConfirmation" class="btn btn-primary"  data-toggle="modal">Confirmation</a>  
				     -->
					</div>
					</form>
					</div>
					
					<div class="modal" id="viewmodal">
					<div class="modal-dialog">
						<div class="modal-content">
							<div>
								<button type="button" id="editbtnclose" class="close" data-dismiss="modal"
									aria-hidden="true">x</button>
								<h3>&nbsp;&nbsp;View Split</h3>
							</div>
							<div class="modal-body">
							
								  <div class="form-group col-md-4">
								      <label>Tariff Amount :</label>
								      <h5>{{viewBookingSplit.data[0].tariffAmount}}</h5>
								 </div>
									
								<div class=" form-group col-md-4">
									 <label>Tax Amount :</label>
									 <h5>{{viewBookingSplit.data[0].taxAmount}}</h5>
								</div>
								
								<div class=" form-group col-md-4">
									 <label>Advance Amount :</label>
									 <h5>{{viewBookingSplit.data[0].advanceAmount}}</h5>
								</div>
								
								<div class=" form-group col-md-4">
									 <label>Due Amount :</label>
									 <h5>{{viewBookingSplit.data[0].dueAmount}}</h5>
								</div>
								<div class=" form-group col-md-4">
									 <label>OTA Commission :</label>
									 <h5>{{viewBookingSplit.data[0].otaCommission}}</h5>
								</div>
									 
								<div class="form-group col-md-4">
									<label >OTA Tax :</label>
									<h5>{{viewBookingSplit.data[0].otaTax}}</h5>
								</div>	
								
								<div class="form-group col-md-4">
									<label>OTA Revenue Amount :</label>
									<h5>{{viewBookingSplit.data[0].otaRevenueAmount}}</h5>
								</div>	
								<div class="form-group col-md-4">
									<label>Revenue Amount :</label>
									<h5>{{viewBookingSplit.data[0].revenueAmount}}</h5>
								</div>
								<div class="form-group col-md-4">
									<label >Tax :</label>
									<h5>{{viewBookingSplit.data[0].taxAmount}}</h5>
								</div>	 
								
								<div class="form-group col-md-4">
									<label >Total Revenue :</label>
									<h5>{{viewBookingSplit.data[0].revenueTaxAmount}}</h5>
								</div>
								
								<div class="form-group col-md-4">
									<label >Ulo Commission :</label>
									<h5>{{viewBookingSplit.data[0].uloCommission}}</h5>
								</div>
								
								<div class="form-group col-md-4">
									<label >Ulo Tax :</label>
									<h5>{{viewBookingSplit.data[0].uloTaxAmount}}</h5>
								</div>	
								
								<div class="form-group col-md-4">
									<label >Payable to Ulo :</label>
									<h5>{{viewBookingSplit.data[0].uloPayable}}</h5>
								</div>		
								
								<div class="form-group col-md-4">
									<label >Payable to Hotel :</label>
									<h5>{{viewBookingSplit.data[0].hotelPayable}}</h5>
								</div>	
								
								<div class="form-group col-md-4">
									<label >&nbsp;</label>
 									<h5>&nbsp;</h5>
								</div>	
								
<!-- 								<div class="form-group col-md-4"> -->
<!-- 									<label >&nbsp;</label> -->
<!-- 									<h5>&nbsp;</h5> -->
<!-- 								</div>	 -->
						
								<div class="modal-footer">
									<a href="#" data-dismiss="modal" class="btn btn-primary btngreeen pull-right">Close</a>
								</div>
							</div>
						</div>
						<!-- /.modal-content -->
					</div>
					<!-- /.modal-dalog -->
				</div>
				<!-- Confirmation popup -->
									<div class="modal" id="viewConfirmation">
					<div class="modal-dialog">
						<div class="modal-content">
						
							<div class="modal-body">
							<div class="col-md-12">
							<h3 class="text-center">Your Booking Confirmed</h3> 
							<h4 class="text-center">Your Booking Confirmed,Voucher has been successfully send to Email</h4>
							</div>
							<div class="modal-footer">
									<a href="#" data-dismiss="modal" class="btn btn-primary btngreeen pull-right">Close</a>
								</div>
						</div>
						<!-- /.modal-content -->
					</div>
					<!-- /.modal-dalog -->
				</div>
				<!-- Confirmation Popup ends -->
        
            </div>
 </section><!-- /.content -->
                
                      
                        

                </div><!-- /.tab-content -->
                <!-- guest information begins -->
              	  

              	
              </div><!-- /.nav-tabs-custom -->
            </div><!-- /.col -->
          </div><!-- /.row -->
          </div>
        
        
          
          
        </section><!-- /.content -->
      
    

      </div><!-- /.content-wrapper -->
      


<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>

	 <script src="js/jquery-2.1.1.js" type="text/javascript"></script>
 <script>
/*  $('.datepicker').datepicker({
	    dateFormat: 'dd-mm-yy'
	 }); */
 </script>
	<script>
    
	 
	
	var app = angular.module('myApp', ['ngProgress']);
	app.controller('customersCtrl', function($scope, $http,$timeout,ngProgressFactory) {
		
		var spinner = $('#loadertwo');
		$("#bookCheckin").datepicker({
            dateFormat: 'MM d, yy',
          
            //minDate:  0,
            onSelect: function (formattedDate) {
                var date1 = $('#bookCheckin').datepicker('getDate'); 
                var date = new Date( Date.parse( date1 ) ); 
                date.setDate( date.getDate() + 1 );        
                var newDate = date.toDateString(); 
                newDate = new Date( Date.parse( newDate ) );   
                $('#bookCheckout').datepicker("option","minDate",newDate);
                $timeout(function(){
                	document.getElementById("bookCheckin").style.borderColor = "LightGray";
                });
            }
        });

        $("#bookCheckout").datepicker({
            dateFormat: 'MM d, yy', 
          
            //minDate:  0,
            onSelect: function (formattedDate) {
                var date2 = $('#bookCheckout').datepicker('getDate'); 
                $timeout(function(){
                	document.getElementById("bookCheckout").style.borderColor = "LightGray";
                });
            }
        }); 
		 $('.select2').select2()
       	$timeout(function(){
      	    // $scope.progressbar.complete();
            $scope.show = true;
            $("#pre-loader").css("display","none");
        }, 2000);

		 
		
		$scope.unread = function() {
		//var notifiurl = "http://localhost:8085/collaborative-workflow/unreadnotifications.action";
		var notifiurl = "unreadnotifications.action";
		$http.get(notifiurl).success(function(response) {
			$scope.latestnoti = response.data;
		});
		};

		var blnAddValue=false;
		
		$scope.getTaxes = function() {
			
			var url = "get-taxes";
			$http.get(url).success(function(response) {
			    //console.log(response);
				$scope.taxes = response.data;
	
			});
		};
       $scope.getRoomAvailability = function(){
    	   var propAccomId = $('#propertyAccommodationId').val();
    	   var propertyId=$('#propertyId').val();
			var fdata = "&strArrivalDate=" + $('#bookCheckin').val()  //checkOutTime
			+ "&strDepartureDate=" + $('#bookCheckout').val()  //checkOutTime
			+ "&accommodationTypeId=" + $('#propertyAccommodationId').val()
			+ "&propertyId=" + propertyId;
			
			$scope.otaPercent=0;
			if(document.getElementById('bookCheckin').value==""){
				 document.getElementById('bookCheckin').focus();
				 document.getElementById("bookCheckin").style.borderColor = "red";
			 }else if(document.getElementById('bookCheckout').value==""){
				 document.getElementById('bookCheckout').focus();
				document.getElementById("bookCheckout").style.borderColor = "red";
			 }else if(document.getElementById('bookCheckin').value!="" && document.getElementById('bookCheckout').value!=""){
				 
				if(propAccomId == null){
				
					$("#selectError").hide();
	                  var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:red;text-align:center;">Please Select Any Accommodation</div>';
	                $('#selectError').html(alertmsg);
	         	            $("#selectError").fadeTo(2000, 500).slideUp(500, function(){
	         	                $("#selectError").slideUp(500);
	                  });
				}
				else{
					spinner.show();
				$http(
						{
							method : 'POST',
							data : fdata,
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							url : 'get-new-room-availability'
						}).success(function(response) {
							
							$scope.availablities = response.data;
							   setTimeout(function(){ spinner.hide(); 
							   }, 1000);
							if( $scope.roomsSelected != ''){
             	        		//alert("enter");
             	        		//alert("length"+$scope.roomsSelected.length)
             	        		
             	        		$scope.roomsSelected = [];
             	        		$scope.roomsSelected.splice(0,$scope.roomsSelected.length);
             	        		 
             	        	}
							
							  var indx = 0;
	                          //$scope.roomsSelected = [];
	                          //addRow(av.accommodationId,av.accommodationType,av.baseAmount,av.arrivalDate,av.departureDate,av.rooms,av.noOfAdults,av.noOfChild,av.minOccupancy,av.extraAdult,av.extraChild,av.available,$index,av.tax)      
	                         $('#addRowInformation').closest('form').find("input[type=text], textarea,input[type=number]").val("");

	                          for (var i = 0; i < $scope.availablities.length; i++) 
	                             {
	                             var dt = $scope.availablities[i];
	                           // alert(dt);
	                             $scope.addRow(dt.accommodationId,dt.accommodationType,dt.baseAmount,dt.arrivalDate, dt.departureDate,
	                            		 dt.rooms,dt.noOfAdults,dt.noOfChild,dt.noOfInfant,dt.minOccupancy,dt.extraAdult,dt.extraChild,
	                            		 dt.tax,dt.available,i,dt.propertyId,dt.minimumAmount,dt.maximumAmount);    
	                             //alert($scope.addRow);
	                            
	                             }  
	                          //console.log($scope.booked[0].firstName);
						
						});
				}
			 }
		};
		$scope.blocknote = "Block";
		$scope.showprocessone = false;
       $scope.confirmBooking = function(){
    	   if(blnAddValue){
    		   $("#loading-example-btn").prop('disabled', true); // disable button
    		   $scope.blocknote = " Please Wait...";
    		   
    		     $scope.showprocessone = true;
           	     spinner.show();
    		     var text = '{"array":' + JSON.stringify($scope.roomsSelected) + '}';
       			 var data = JSON.parse(text);
       			 var sourceId =  $('#sourceId').val();
       			 var checkInTime = $scope.finalCheckInTime;
       			 if(typeof checkInTime==="undefined"){
       				checkInTime="NA";
       			 }
       			 var checkOutTime =  $scope.finalCheckOutTime;
       			if(typeof checkOutTime==="undefined"){
       				checkOutTime="NA";
       			 }
       			 
       			 var priceAmount =  $('#priceAmount').val()
       			 var taxAmount =  $('#taxAmount').val()
       			 var totalAmount= parseFloat(priceAmount)+parseFloat(taxAmount);
       			 var otaBookingId=$('#otaBookingId').val();
       			$http(
       					{
       						method : 'POST',
       						data : data,
       						dataType: 'json',
       						headers : {
       							 'Content-Type' : 'application/json; charset=utf-8'

       						},
       						url : "add-booking?sourceId="+sourceId+"&otaBookingId="+otaBookingId
       					}).then(function successCallback(response) {
       					    $scope.blocknote = "Please Wait...";
       	    		        $scope.showprocessone = true;
       	    		        spinner.show();
       						var firstName=$('#firstName').val();
       						var guestEmail=$('#guestEmail').val();
       						var phoneNumber=$('#mobileNumber').val();
       						
       						var specialRequest = $('#specialRequest').val();
       						var gdata = "firstName=" + firstName.trim() 
       						+ "&emailId=" + guestEmail.trim()
       						+ "&phone=" + phoneNumber.trim()
       						
       						$http(
       					{
       						method : 'POST',
       						data : gdata,
       						headers : {
       							'Content-Type' : 'application/x-www-form-urlencoded'
       						},
       						url : 'add-guest'
       						
       					}).then(function successCallback(response){
       						
       						var text = '{"array":' + JSON.stringify($scope.roomsSelected) + '}';
                               var sourceId =  $('#sourceId').val()  
       						var data = JSON.parse(text);
                               spinner.show();
       						$http(
       							    {
       								method : 'POST',
       								data : data,
       								dataType: 'json',
       								headers : {
       									 'Content-Type' : 'application/json; charset=utf-8'
       								},
       								  url : 'add-booking-details?txtGrandTotal='+totalAmount+'&specialRequest='+specialRequest+'&sourceId='+sourceId+'&checkInTime='+checkInTime+'&checkOutTime='+checkOutTime
       							   }).then(function successCallback(response) {
       								  // $scope.booked = response.data;
       								   alert("Room Booked successfully");
       								  $("#bookingSuccess").hide();
       				                  var alertmsg = ' <div class="alert alert-success alert-dismissible" style="color:red;text-align:center;">Your Room Blocked Successfully.</div>';
       				                  
       				                $('#bookingSuccess').html(alertmsg);
       				         	            $("#bookingSuccess").fadeTo(2000, 500).slideUp(500, function(){
       				         	                $("#bookingSuccess").slideUp(500);
       				                  });
       				         	        spinner.hide();
       								   window.location.reload(); 
       								   $scope.blocknote = "Block";
       								$scope.showprocessone = false;
       								   $("#loading-example-btn").prop('disabled', false); 
       							   }, function errorCallback(response) {
       								spinner.hide();
       										  $("#bookingFailed").hide();
       				                  var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:red;text-align:center;">Process Failed, Please Try Again!!</div>';
       				                  
       				                $('#bookingFailed').html(alertmsg);
       				         	            $("#bookingFailed").fadeTo(2000, 500).slideUp(500, function(){
       				         	                $("#bookingFailed").slideUp(500);
       				                  });
       									document.getElementById('bookingForm').reset();
       									document.getElementById('addguestinformation').reset();
       									document.getElementById('addRowInformation').reset();
       								})
       						
       					}, function errorCallback(response) {
       						$scope.blocknote = "Block";
       						spinner.hide();
       					  $("#bookingFailed").hide();
			                  var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:red;text-align:center;">Process Failed, Please Try Again!!</div>';
			                  
			                $('#bookingFailed').html(alertmsg);
			         	            $("#bookingFailed").fadeTo(2000, 500).slideUp(500, function(){
			         	                $("#bookingFailed").slideUp(500);
			                  });
       						document.getElementById('bookingForm').reset();
							document.getElementById('addguestinformation').reset();
							document.getElementById('addRowInformation').reset();
       					})
       					}, function errorCallback(response) {
       						$scope.blocknote = "Block";
       						spinner.hide();
       					  $("#bookingFailed").hide();
			                  var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:red;text-align:center;">Process Failed, Please Try Again!!</div>';
			                  
			                $('#bookingFailed').html(alertmsg);
			         	            $("#bookingFailed").fadeTo(2000, 500).slideUp(500, function(){
			         	                $("#bookingFailed").slideUp(500);
			                  });
       						document.getElementById('bookingForm').reset();
							document.getElementById('addguestinformation').reset();
							document.getElementById('addRowInformation').reset();
       					})
    	   }else{
    		   //alert('please fill all the fields !!!')
    		   spinner.hide();
    			  $("#bookingFailed").hide();
                  var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:red;text-align:center;">Please fill all the fields !!!</div>';
                  
                $('#bookingFailed').html(alertmsg);
         	            $("#bookingFailed").fadeTo(2000, 500).slideUp(500, function(){
         	                $("#bookingFailed").slideUp(500);
                  });
    	   }
    	   
    
					
       };
                
       
       
       $scope.revoucher = "Resend Voucher";
      $scope.resendVoucher = function(){
    	   $scope.revoucher = "Please Wait..";
    	   $("#revoucher").prop('disabled', true);
		   var bookingId = parseInt(document.getElementById('filterBookingId').value);
		   var mailFlag = true;
		   var text = '{"array":' + JSON.stringify($scope.bookinglist) + '}';
		   var gdata = "firstName=" + $('#guestName').val() 
			+ "&emailId=" + $('#guestMailId').val()
			+ "&phone=" + $('#guestNumber').val()
			+ "&guestId="+$('#guestId').val();
		   var otaBookingId=$('#guestOtaBookingId').val();
		   
		   console.log(text);
			var data = JSON.parse(text);
			spinner.show();
			$http(
   					{
   						method : 'POST',
   						data : data,
   						dataType: 'json',
   						headers : {
   							 'Content-Type' : 'application/json; charset=utf-8'

   						},
   						url : "edit-booking?bookingId="+bookingId+"&otaBookingId="+otaBookingId
   					}).then(function successCallback(response) {
   						spinner.show();
   					
				$http(
					    {
					    	method : 'POST',
							data : gdata,
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							url : 'edit-guest?bookingId='+ bookingId
						
					   }).then(function successCallback(response) {
						  
							
							spinner.show();
					$http(
						{
							method : 'POST',
							data : data,
							dataType: 'json',
							headers : {
								 'Content-Type' : 'application/json; charset=utf-8'
								
							},
							url : 'edit-booking-details?bookingId='+bookingId+'&mailFlag='+mailFlag
							//url : 'jsonTest'
							
						}).then(function successCallback(response){
							  //alert("The Voucher Resend Successfully");
							  $("#revoucher").prop('disabled', false);
							  $scope.revoucher = "Resend Voucher";
							  $("#updatevoucher").hide();
			                  var alertmsg = ' <div class="alert alert-success alert-dismissible" style="color:red;text-align:center;">The Voucher Resend Successfully</div>';
			                $('#updatevoucher').html(alertmsg);
			         	            $("#updatevoucher").fadeTo(2000, 500).slideUp(500, function(){
			         	                $("#updatevoucher").slideUp(500);
			                  });
			         	           setTimeout(function(){ spinner.hide(); 
			         	          }, 1000);
							 // window.location.reload(); 
							})  
					   
					   })
   				})
		 };
		 
		 
		 $scope.upvoucher = "Update Voucher";
          $scope.updateBooking = function(){
        	  $("#upvoucher").prop('disabled', true);
        	  $scope.upvoucher = "Please Wait...";
		   var bookingId = parseInt(document.getElementById('filterBookingId').value);
		   var mailFlag = false;
		   var text = '{"array":' + JSON.stringify($scope.bookinglist) + '}';

		   var gdata = "firstName=" + $('#guestName').val() 
			+ "&emailId=" + $('#guestMailId').val()
			+ "&phone=" + $('#guestNumber').val()
			+ "&guestId="+$('#guestId').val();
			
		  
		   var otaBookingId=$('#guestOtaBookingId').val();
		   
		   
		   console.log(text);
			var data = JSON.parse(text);
			spinner.show();
			$http(
   					{
   						method : 'POST',
   						data : data,
   						dataType: 'json',
   						headers : {
   							 'Content-Type' : 'application/json; charset=utf-8'

   						},
   						url : "edit-booking?bookingId="+bookingId+"&otaBookingId="+otaBookingId
   					}).then(function successCallback(response) {
   						
   					spinner.show();
				$http(
					    {
					    	method : 'POST',
							data : gdata,
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							url : 'edit-guest?bookingId='+bookingId
						
					   }).then(function successCallback(response) {
						 spinner.show();
							$http(
						{
							method : 'POST',
							data : data,
							dataType: 'json',
							headers : {
								 'Content-Type' : 'application/json; charset=utf-8'
							},
							url : 'edit-booking-details?bookingId='+bookingId+'&mailFlag='+mailFlag
									
						}).then(function successCallback(response){
							 // alert("The Given details are updated successfully");
							 $("#upvoucher").prop('disabled', false);
							 $scope.upvoucher = "Update Voucher";
							  $("#updatevoucher").hide();
				                  var alertmsg = ' <div class="alert alert-success alert-dismissible" style="color:red;text-align:center;">The Given details are updated successfully</div>';
				                $('#updatevoucher').html(alertmsg);
				         	            $("#updatevoucher").fadeTo(2000, 500).slideUp(500, function(){
				         	                $("#updatevoucher").slideUp(500);
				                  });
				         	           setTimeout(function(){ spinner.hide(); 
				         	          }, 1000);
							   //window.location.reload(); 
							}) 
					   })
   					})
		 };
		 $scope.viewBookingSplitUp=function(){
				var text = '{"array":' + JSON.stringify($scope.roomsSelected) + '}';
				var data = JSON.parse(text);
				$http(
					    {
						method : 'POST',
						data : data,
						dataType: 'json',
						headers : {
							 'Content-Type' : 'application/json; charset=utf-8'
						},
						url : 'get-booking-split'
					   }).then(function successCallback(response) {
						   $scope.viewBookingSplit = response.data;
					   })
				 
			};
		 
		$scope.range = function(min, max, step){
		    step = step || 1;
		    var input = [];
		    for (var i = min; i <= max; i += step) input.push(i);
		    return input;
		 };
		$scope.getStayType= function(rowid){
			var arrStay=[19,20,22];
			var inputStay=false;
			inputStay=$scope.checkSource(rowid,arrStay);
			if(inputStay==true){
				$("#otaStayId").show();
				$scope.stayTypeId =0;
			}else{
				$("#otaStayId").hide();
				$scope.stayTypeId =0;
			}
		}; 
		$scope.getSourceId= function(rowid){
			$scope.changeSourceId =rowid;
			var arr=[2,3,4,5,6,11,15,18,19,20,21,22];
			
			var input=false;
			input=$scope.checkSource(rowid,arr);
			
			if(input==true){
				$("#otaId").show();
				$scope.otaBookingId ="";
			}else{
				$("#otaId").hide();
				$scope.otaBookingId ="NA";
			}
			
			
		};	
		
		$scope.changeShot = function(rowid){
			
			if(rowid==1){
				$("#slotTypeTimeId").hide();
				$("#slotTypeId").hide();
				$("#slotTypeHoursId").hide();
				$scope.checkOutTime = "NA";
                $scope.finalCheckInTime = "NA";
                $scope.finalCheckOutTime = "NA";
                
			}else if(rowid==2){
				$("#slotTypeTimeId").show();
				$("#slotTypeId").show();
				$("#slotTypeHoursId").show();
			}
		};
		
		 $scope.checkSource=function(rowid, arr){
			 
				for (var i = 0; i < arr.length; i++) {
			        if (arr[i] == parseInt(rowid)) {
			        	return true;
			        }
			    }
		 }
		 $scope.addRoom = function(id,type,available,adult,child,infant,price,advance,tax,
				 otaCommission,otaPercent,otaTax,tcsPercent,otaTcs,revenue,rooms,indx){
			
			var type = type.replace(/\s/g,'');
	         var id = parseInt(id);
	         var available = parseInt(available);
	         var adult = parseInt(adult);
	         var child = parseInt(child);	         
	         var price = parseFloat(price);
	         var tax = parseFloat(tax);
	         var advance=parseFloat(advance);
	         var otaCommission = parseFloat(otaCommission);
	         var otaPercent = parseFloat(otaPercent);
	         var tcsPercent = parseFloat(tcsPercent);
	         /* if (!isNaN(otaPercent)) {
				 $('#otaPercent'+ indx).val(otaPercent);
			 }else{
				 $('#otaPercent'+ indx).val(0);
			 } */
	         var otaComTaxAmount=parseFloat(otaCommission*otaPercent/100);
			 if (!isNaN(otaComTaxAmount)) {
				 $('#otaTax'+ indx).val(parseFloat(otaComTaxAmount).toFixed(2));
			 }else{
				 $('#otaTax'+ indx).val(0);
			 }
			 var otaTcsCommAmount=0;
			 if(!isNaN(tcsPercent) && tcsPercent>0){
				 $('#otaTcs'+ indx).val(parseFloat(price*tcsPercent/100).toFixed(2));
				 otaTcsCommAmount=parseFloat(price*tcsPercent/100);
				 
			 }else{
				 otaTcsCommAmount=0;
				 $('#otaTcs'+ indx).val(otaTcsCommAmount);
			 }
	         //$('#otaTax'+ indx).val(otaComTaxAmount);
// 	         var revenueAmount=parseFloat((price)-(otaCommission+otaComTaxAmount));
			 var revenueAmount=0;
	         if(parseFloat(otaTcsCommAmount)>0){
	        	 revenueAmount=parseFloat((price+tax)-parseFloat(otaCommission+otaComTaxAmount)-parseFloat(otaTcsCommAmount));	 
	         }else{
	        	 revenueAmount=parseFloat((price)-parseFloat(otaCommission+otaComTaxAmount));
	         }
	         
	         if(!isNaN(revenueAmount)){
	        	 $('#revenue'+ indx).val(parseFloat(revenueAmount).toFixed(2));
	         }else{
	        	 $('#revenue'+ indx).val(0);
	         }
	         
	         //var sourceId = parseInt(source);
	         var rooms = parseInt(rooms);
	         var fixedPrice=parseFloat(price);
	         var fixedTax=parseFloat(tax);
	         var fixedAdvance=parseFloat(advance);
	         var tot = (fixedPrice+fixedTax).toFixed(2);
	         var advance=(fixedAdvance).toFixed(2);
	         
	         if(!isNaN(tot-advance)){
	        	 $('#due'+ indx).val(parseFloat(tot-advance));
	         }else{
	        	 $('#due'+ indx).val(0);
	         }
	         

             //$('#due'+ indx).val(price-advance);
	        //alert(sourceId);
	         $scope.roomsSelected[indx].rooms = rooms; 
	         $scope.roomsSelected[indx].adultsCount = adult; 
	         $scope.roomsSelected[indx].childCount = child;
	         $scope.roomsSelected[indx].infantCount = infant;
             $scope.roomsSelected[indx].advanceAmount = advance;
             //alert($scope.roomsSelected[indx].advanceAmount);
	         $scope.roomsSelected[indx].total = price; 
	         $scope.roomsSelected[indx].tax = tax; 
	         $scope.roomsSelected[indx].otaCommission = otaCommission; 
	         //$scope.roomsSelected[indx].otaTax = otaComTaxAmount;
	         $scope.roomsSelected[indx].otaTax = parseFloat(otaComTaxAmount+otaTcsCommAmount); 
	         //$scope.roomsSelected[indx].sourceId = sourceId; 
			 
	         if(!isNaN($scope.roomsSelected[indx].rooms) && !isNaN($scope.roomsSelected[indx].adultsCount) && !isNaN($scope.roomsSelected[indx].childCount) && !isNaN($scope.roomsSelected[indx].adultsCount) && !isNaN($scope.roomsSelected[indx].advanceAmount) && !isNaN($scope.roomsSelected[indx].total) && !isNaN($scope.roomsSelected[indx].tax) && !isNaN($scope.roomsSelected[indx].otaCommission) && !isNaN($scope.roomsSelected[indx].otaTax) ){
	        	 if($scope.availablities.length==(indx+1)){
	        		 blnAddValue=true;
	        	 }
	        	 
	         }
	         
		 }

          $scope.editRoom = function(price,advance,tax,indx){
	         /* var advance = parseFloat(advance);
	         var price = parseFloat(price);
	         var tax = parseFloat(tax); */
	         //var sourceId = parseInt(source);
	       
	         var tot = parseFloat(price)+parseFloat(tax);
	         var due = parseFloat(tot-advance).toFixed(2);
	         $('#due'+ indx).val(due);

	         $scope.bookinglist[indx].advanceAmount = advance;
	         $scope.bookinglist[indx].total = price; 
	         $scope.bookinglist[indx].tax = tax; 
				 
		 };

		
        
         $scope.roomsSelected = [];
         // shiva add addrom tax
         $scope.addRow = function(id,type,baseAmount,arrival, departure,rooms,adultsAllow,childAllow,infantAllow,
        		 minOccu,extraAdult,extraChild,tax,available,indx,propertyId,minAmount,maxAmount){	
          //var available = 0;
         // alert(propertyId)
         if(available == 0){
     	  //$("#submitBtn").attr("disabled","disabled");
     	    //btn.disabled = true;
			 console.log("Disable");
         }
         else{
        	 
        
         var type = type.replace(/\s/g,'');
         var id = parseInt(id);
         var minOccu = parseInt(minOccu);
         var extraAdult = parseInt(extraAdult);
         var extraChild = parseInt(extraChild);         
         var adultsAllow = parseInt(adultsAllow);
         var childAllow = parseInt(childAllow);        
         var amount = parseFloat(baseAmount);
         var rooms = parseInt(rooms);
         var date1 = new Date(arrival);
         var date2 = new Date(departure);
         var timeDiff = Math.abs(date2.getTime() - date1.getTime());
         var diffDays = parseInt(Math.round(timeDiff / (1000 * 3600 * 24)));
         var randomNo = Math.random();         
         var taxes = tax * diffDays;
         var advance = 0;
         var otaCommission=0;
         var otaTax=0;
         var minimumAmount=parseFloat(minAmount);
         var maximumAmount=parseFloat(maxAmount);
         var total = (diffDays*rooms*amount);
	        var newData = {'arrival':arrival,'indx':indx,'departure':departure,'accommodationId':id,'accommodationType':type,'available':available, 
	        		'rooms': rooms,'diffDays':diffDays, 'advanceAmount':advance,'baseAmount':amount, 'total':total ,'adultsAllow':adultsAllow,'childAllow':childAllow,
	        		'minOccu':minOccu,'extraAdult':extraAdult,'extraChild':extraChild,'adultsCount':0,'childCount':0,'infantCount':0,'refund':0,'statusId':2,
	        		'tax':taxes,'randomNo':randomNo,'discountId':35,'otaCommission':otaCommission,'otaTax':otaTax,'propertyId':propertyId,
	        		'minimumAmount':minimumAmount,'maximumAmount':maximumAmount};
	       
               $scope.roomsSelected.push(newData);
        	   console.log($scope.roomsSelected);
        	   $scope.availablities[indx].available = $scope.availablities[indx].available;
        	
        	   return false;
        	   
       
         }	
         };
        	
          $scope.getTotal = function(){
        	    var total = 0;
        	    for(var i = 0; i < $scope.roomsSelected.length; i++){
        	        var accommodation = $scope.roomsSelected[i];
        	        total += (accommodation.total);
        	    }
        	    return total;
        	} 	
          
          $scope.getRoomsTotal = function(){
      	    var totalRooms = 0;
      	    for(var i = 0; i < $scope.roomsSelected.length; i++){
      	        var roomsCount = $scope.roomsSelected[i];
      	        totalRooms += parseInt(roomsCount.rooms);
      	    }
      	    return totalRooms;
      	  } 	
		
          $scope.removeRoom = function(index){
        	 
        	   
        	 //$scope.availablities[indx].available = parseInt($scope.availablities[indx].available) + 1; 
        	
              $scope.availablities.splice(index,1); 
              $scope.roomsSelected.splice(index,1); 
              //alert($scope.availablities[indx]);
        	 //$scope.availablities[id].available = 1 ; 
        	 //alert($scope.roomsSelected[indx]);
        	 
        	
        	} 	
          
		 $scope.getPropertyList = function() {
			 
	        	var userId = $('#adminId').val();
	 			var url = "get-active-properties?userId="+userId;
	 			$http.get(url).success(function(response) {
	 			    
	 				$scope.properties = response.data;
	 	
	 			});
	 		};
	 		
        $scope.change = function() {
 	   
	        //alert($scope.id);
	        
	        var propertyId = $scope.id;	
 	        var url = "change-user-property?propertyId="+propertyId;
 			$http.get(url).success(function(response) {
 				
 				 window.location = '/ulopms/dashboard'; 
 				//$scope.change = response.data;
 	
 			});
		       
	 		}; 
      
        

		
	 		$scope.getAllSources = function() {
	 		   
	 		   var url = "get-sources";
	 		   $http.get(url).success(function(response) {
	 		       //console.log(response);
	 		   $scope.sources = response.data;
	 		   
	 		    });
	 	   };
		
		$scope.getPropertyAccommodation = function(propId) {

			var url = "get-property-accommodations?propertyId="+propId;
			$http.get(url).success(function(response) {
			    //console.log(response);
				$scope.accommodation = response.data;
	
			});
		};
		
		$scope.changeTime = function() {
			//$("#bookCheckin").datepicker({
	           // dateFormat: 'MM d, yy',
	           var time = $('#checkInTime').val();
	           var type = $('#slotType').val();
	           var plus = parseInt(parseInt(time)+parseInt(type));
	                var date1 = $('#bookCheckin').datepicker('getDate'); 
	                var checkIndate = new Date(date1);
	                var checkoutdate = new Date(date1);
	                var setcheckintime = checkIndate.setHours(time);
	                var checkintime = new Date(setcheckintime);
	                //alert(checkintime)
	                
	                var setcheckouttime = checkoutdate.setHours(plus);
	                var checkouttime = new Date(setcheckouttime);
	                var checkoutDate = checkouttime.getDate();
	                var checkoutMonth = checkouttime.getMonth()+1;
	                chackoutDate = checkoutDate < 10 ? '0'+checkoutDate : checkoutDate;
	                checkoutMonth = checkoutMonth < 10 ? '0'+checkoutMonth : checkoutMonth;
	                //alert(checkouttime)
	                 var hours;
	                
	                if(type==24){
	                	hours = checkouttime.getHours()-1;	
	                }else{
	                	hours = checkouttime.getHours();
	                }
	                 
	                 var minutes = checkouttime.getMinutes();
	                 var ampm = hours >= 12 ? 'PM' : 'AM';
	                 hours = hours % 12;
	                 hours = hours < 10 ? '0'+hours : hours;
	                 hours = hours ? hours : 12; // the hour '0' should be '12'
	                 minutes = minutes < 10 ? '0'+minutes : minutes;
	                 var strTime = hours + ':' + minutes + ' ' + ampm;
	                 //$scope.checkInTime = checkintime.getFullYear()+"-"+(checkintime.getMonth()+1)+"-"+checkintime.getDate()+' '+strTime;
	                 $scope.checkOutTime = chackoutDate+"/"+checkoutMonth+"/"+checkouttime.getFullYear()+' '+strTime;
	                 $scope.finalCheckInTime = checkintime.getFullYear()+"-"+(checkintime.getMonth()+1)+"-"+checkintime.getDate()+' '+checkintime.getHours()+":"+checkintime.getMinutes()+":"+checkintime.getSeconds();
	                 if(type==24){
	                	 $scope.finalCheckOutTime = checkouttime.getFullYear()+"-"+(checkouttime.getMonth()+1)+"-"+checkouttime.getDate()+' '+checkouttime.getHours()-1+":"+checkouttime.getMinutes()+":"+checkouttime.getSeconds();
	                 }else{
	                	 $scope.finalCheckOutTime = checkouttime.getFullYear()+"-"+(checkouttime.getMonth()+1)+"-"+checkouttime.getDate()+' '+checkouttime.getHours()+":"+checkouttime.getMinutes()+":"+checkouttime.getSeconds();
	                 }
	                 //var date2= new Date(d0a1);
                     //var newDate = date.toDateString(); 
	               /*  newDate = new Date( Date.parse( newDate ) );   
	                $('#bookCheckout').datepicker("option","minDate",newDate);
	                $timeout(function(){
	                	document.getElementById("bookCheckin").style.borderColor = "LightGray";
	                }); */
	           //}
	        //});
		};

        $scope.getBookedDetails = function(){
			 //alert('filterBookingId..'+document.getElementById('filterBookingId').value)
				var filterDateName="",filterBookingId="";
				
				filterBookingId=document.getElementById('filterBookingId').value;
				if(filterBookingId==""){
					filterBookingId="0";
				}
				
				var fdata = 'filterBookingId='+filterBookingId;
				//alert('fdata..'+fdata);
				
	    	   spinner.show();		    		
					$http(
							{
								method : 'POST',
								data : fdata,
								headers : {
									'Content-Type' : 'application/x-www-form-urlencoded'
								},
								url : 'get-bookingid-details'
							}).success(function(response) {
								
								$scope.bookinglist = response.data;
								setTimeout(function(){ spinner.hide(); 
								  }, 1000);
							//	alert(JSON.stringify(response.data));
								
							});
		 		 
			};
        $scope.getPropertyList();
	    
		$scope.getTaxes();
		
        $scope.getAllSources();
        $("#otaId").hide();
        $("#otaStayId").hide();
        $("#slotTypeTimeId").hide();
		$("#slotTypeId").hide();
		$("#slotTypeHoursId").hide();
	});

	</script>
  <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

	  <!-- iCheck 1.0.1 -->
    <script src="plugins/iCheck/icheck.min.js"></script>
	     <script>
  /* next and previous button */
 $('.nextBtn').click(function(){
  $('.nav-tabs > .active').next('li').find('a').trigger('click');
});

  $('.previousBtn').click(function(){
  $('.nav-tabs > .active').prev('li').find('a').trigger('click');
});
  </script>
   <style>
 .select2-container--default .select2-selection--multiple .select2-selection__choice {
    background-color: green;
    border: 1px solid #aaa;
    border-radius: 4px;
    cursor: default;
    float: left;
    margin-right: 5px;
    margin-top: 5px;
    padding: 0 5px;
}
 </style>
