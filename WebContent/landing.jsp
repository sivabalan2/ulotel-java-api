<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
   pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<head>
   <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <!-- Tell the browser to be responsive to screen width -->
   <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
   <!-- Bootstrap 3.3.5 -->
   <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
   <!-- Font Awesome -->
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
   <!-- Ionicons -->
   <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
   <!-- Theme style -->
   <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
   <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1">
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
   <link rel="stylesheet" href="css/bootstrap.min.css">
   <link rel="stylesheet" href="css/ulotel.css">
   <link rel="stylesheet" href="css/ulotelmq.css">
   <link rel="stylesheet" href="css/AdminLTE.min.css">
   <script src="js/selectize.js"></script>
   <script src="js/jquery.min.js"></script>
   <title>Ulotel is an complete property management</title>
   <style> 
      #panel {
      padding: 50px;
      display: none;
      }
   </style>
</head>
<body ng-app="myApp" ng-controller="customersCtrl"  class="ulotellanding">
   <section class="container">
      <div class="row">
         <div class="col-lg-12 col-md-12 col-sm-12 cl-xs-12">
            <form>
               <div class="demo">
                  <div class="form-group" >
			        <input type="text" id="hotellist" class="form-control" ng-model="hotellist" required>
			        <label class="form-control-placeholder" for="hotellist">Select the property</label>
			      </div>
                     <div id="panel">
                        <ul>
                          <a ng-repeat="ap in userProperty | filter : hotellist" href="change-user-property?propertyId={{ap.DT_RowId}}"> <li  href="change-user-property?propertyId={{ap.DT_RowId}}">{{ap.propertyName}}</li></a>
                        </ul>
                     </div>
                
               </div>
               
               <div class="text-center">
	            	<a href="login">Sign in with different user</a>
	         	</div>
            </form>
         </div>
      </div>
   </section>
</body>
<script></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>
<script   type="text/javascript"src="js1/ngprogress.js"></script>
<script>
   var app = angular.module('myApp', ['ngProgress']);
   app.controller('customersCtrl', function($scope, $http,$timeout,ngProgressFactory) {
	   $("#panel li").click(function(){
	        location.href = $(this).find("a").attr("href");
	});
   
   	$(function() {
   		var $wrapper = $('#wrapper');
   
   		// theme switcher
   		var theme_match = String(window.location).match(/[?&]theme=([a-z0-9]+)/);
   		var theme = (theme_match && theme_match[1]) || 'default';
   		var themes = ['default','legacy','bootstrap2','bootstrap3'];
   		$('head').append('<link rel="stylesheet" href="css/selectize.' + theme + '.css">');
   
   		var $themes = $('<div>').addClass('theme-selector').insertAfter('h1');
   		for (var i = 0; i < themes.length; i++) {
   			$themes.append('<a href="?theme=' + themes[i] + '"' + (themes[i] === theme ? ' class="active"' : '') + '>' + themes[i] + '</a>');
   		}
   
   		// display scripts on the page
   		$('script', $wrapper).each(function() {
   			var code = this.text;
   			if (code && code.length) {
   				var lines = code.split('\n');
   				var indent = null;
   
   				for (var i = 0; i < lines.length; i++) {
   					if (/^[	 ]*$/.test(lines[i])) continue;
   					if (!indent) {
   						var lineindent = lines[i].match(/^([ 	]+)/);
   						if (!lineindent) break;
   						indent = lineindent[1];
   					}
   					lines[i] = lines[i].replace(new RegExp('^' + indent), '');
   				}
   
   				var code = $.trim(lines.join('\n')).replace(/	/g, '    ');
   				var $pre = $('<pre>').addClass('js').text(code);
   				$pre.insertAfter(this);
   			}
   		});
   	});
   	  $scope.getUserProperty = function() {
   			
   			var url = "get-user-property";
   			$http.get(url).success(function(response) {
   			    //console.log(response);
   				$scope.userProperty = response.data;
   	
   			});
   	   };
   		
   	   $(document).ready(function(){
   		    $("#hotellist").click(function(){
   		        $("#panel").slideDown("slow");
   		    });
   		});
   	
       $scope.getUserProperty();
   
   	//$scope.unread();
   	//
   	
   	
   });
</script>
</html>