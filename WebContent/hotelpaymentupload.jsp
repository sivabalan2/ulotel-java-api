<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!-- <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/themes/south-street/jquery-ui.min.css" />
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <small>Payment Upload</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Payment Upload</li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="nav-tabs-custom">
          <ul class="nav nav-tabs">
            <li class="active"><a href="#fa-icons" data-toggle="tab">Payment Upload</a></li>
          </ul>
          <div class="tab-content">
            <!-- Font Awesome Icons -->
            <div class="tab-pane active" id="fa-icons">
            <!-- upload Payment begins-->
              <section class="content">
                <div class="row">
                 <form name="uploadPayment">
                 <div class="form-group col-md-3" >
                  <label>From</label>
                   <div class="input-group date">
                   <label class="input-group-addon btn" for="validFrom">
                   <span class="fa fa-calendar"></span>
                   </label>   
                   <input type="text" id="validFrom" class="form-control" name="validFrom"  value="" ng-required="true" autocomplete="off">
                  </div>
                 </div>
                  <div class="form-group col-md-3">
                  <label>To</label>
                   <div class="input-group date">
                   <label class="input-group-addon btn" for="validTo">
                   <span class="fa fa-calendar"></span>
                   </label>   
                   <input type="text" id="validTo" class="form-control" name="validTo"  value="" ng-required="true" autocomplete="off">
                  </div>
                 </div>
                  <div class="col-sm-12 form-group">
                 	<label>Booking Id's</label>
				    <textarea class="form-control" id="paymentBookingId" ng-required="true"></textarea>
                 </div>
                      <div class="col-sm-12 form-group">
                 	<label>Amount</label>
				    <input class="form-control"  id="paymentAmount" ng-required="true"/>
                 </div>
                    <div class="col-sm-12 form-group">
                 	<label>Upload Receipt</label>
                 	<input type="file" class="form-control"  ngf-select  ng-model="picFile" name="photo" id="photo"> <!-- ngf-accept="'image/*'"  ngf-pattern="'.pdf'" -->
				   <!--  <input type="file"  ng-required="true" id="paymentRecepit"/> -->
                 </div>
                  <div class="form-group col-sm-12">
                  <div id="paymentImageSuccess"></div>
                  <button type="submit" class="btn btn-primary pull-right" ng-click="saveRecepit(picFile);">Send Receipt</button>   <!-- ng-disabled="uploadPayment.$invalid" -->
                  </div>
                 </form>
                </div>
             </section>
              <!-- create coupon ends-->
            </div>
            <!-- /.tab-content -->
             </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
  </section>

  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>
<script src="js/jquery-2.1.1.js" type="text/javascript"></script>
<script src="js1/upload/ng-file-upload-shim.min.js"></script>
<script src="js1/upload/ng-file-upload.min.js"></script>
<script>
    
/* var app = angular.module('myApp',['ngFileUpload']);
app.controller('customersCtrl',['$scope',
             					'Upload',
            					'$timeout',
            					'$http', function($scope,Upload,$timeout,$http) { */
	
	/*  var app = angular.module('myApp', ['ngProgress']);
	app.controller('customersCtrl', function($scope, $http,$timeout,ngProgressFactory) {  */
		
		
		var app = angular.module('myApp', ['ngFileUpload']);
		app.controller('customersCtrl',['$scope',
		             					'Upload',
		            					'$timeout',
		            					'$http', function($scope,Upload,$http,$timeout) {
			
			
		
		var spinner = $('#loadertwo');
		$("#validFrom").datepicker({
			dateFormat: 'MM d, yy',

			//minDate:  0,
			onSelect: function (formattedDate) {
				var date1 = $('#validFrom').datepicker('getDate');
				var date = new Date(Date.parse(date1));
				date.setDate(date.getDate() + 1);
				var newDate = date.toDateString();
				newDate = new Date(Date.parse(newDate));
				$('#validTo').datepicker("option", "minDate", newDate);
				$timeout(function () {
					document.getElementById("validFrom").style.borderColor = "LightGray";
				});
			}
		});

		$("#validTo").datepicker({
			dateFormat: 'MM d, yy',

			//minDate:  0,
			onSelect: function (formattedDate) {
				var date2 = $('#validTo').datepicker('getDate');
				$timeout(function () {
					document.getElementById("validTo").style.borderColor = "LightGray";
				});
			}
		});
		 
	$scope.saveRecepit = function(file){
		
		  var fromDate = $("#validFrom").val();
		  var toDate = $("#validTo").val();
		  var bookingId = $("#paymentBookingId").val();
		  var amount = $("#paymentAmount").val();
       	
           Upload.upload({
               url: 'upload-property-payment-receipt',
               enableProgress: true, 
               data: {myFile: file,'startDate':fromDate,'endDate':toDate,'paymentBookingId':bookingId,'paymentAmount':amount}
           }).then(function successCallback (response) {
           	
        	   		$("#paymentImageSuccess").hide();
					  var alertmsg = ' <div class="alert" style="color:green;text-align:center;">Receipt Updated SuccessFully</div>';
			            $('#paymentImageSuccess').html(alertmsg);
			            $("#paymentImageSuccess").fadeTo(2000, 500).slideUp(500, function(){
		                $("#paymentImageSuccess").slideUp(500);
		                 }); 
           
           }, 
			function errorCallback(response) {
        	   
			});
		
	}
	
     
		
	}]);

	</script>
  <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="plugins/iCheck/icheck.min.js"></script>
<script>
  /* next and previous button */
 $('.nextBtn').click(function(){
  $('.nav-tabs > .active').next('li').find('a').trigger('click');
});

  $('.previousBtn').click(function(){
  $('.nav-tabs > .active').prev('li').find('a').trigger('click');
});
  
  
  </script>