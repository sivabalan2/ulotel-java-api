<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="s" uri="/struts-tags"%>

   <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
           <section class="content-header">
          <h1>
          Ota Hotel Details
          </h1>
          <ol class="breadcrumb">
            <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Ota Hotel Details</li>
          </ol>
        </section>
      
        <!-- Main content -->
        <section class="content">
        
        	<div class="row">
            	<div class="col-xs-12">
						<div class="box">
							<div class="box-header">
								<h3 class="box-title">OTA Hotel Details</h3>
								
							</div>
							
							
							<!-- /.box-header -->
							<div class="box-body table-responsive no-padding" > 
								<table class="table table-hover">
									<tr>										
										<th>Room Name</th>
										<th>Room Code</th>
										<th>Rate Name</th>
										<th>Rate Code</th>
										<th>Meal Plan</th>
										<th>Pay Mode</th>
										<th>Active</th>
									</tr>
									
									<tr ng-repeat="ohd in otaHotelDetails">										
										<td>{{ohd.roomTypeName}}</td>
										<td>{{ohd.roomTypeCode}}</td>
										<td>{{ohd.ratePlanName}}</td>
										<td>{{ohd.ratePlanCode}}</td>
										<td>{{ohd.meanPlan}}</td>
										<td>{{ohd.payMode}}</td>
										<td>{{ohd.isActive}}</td>
									</tr>
									
								</table>
								<div class="text-center" ng-if="otaHotelDetails == null">Please check OTA Hotel code, Bearer and Channel token</div>
							</div>
							<!-- /.box-body -->
							
							
							<div class="box-footer clearfix">
								 <ul class="pagination pagination-sm no-margin pull-right">
									<div ng-repeat="oh in otaHotels"><input type="hidden" id="otaHotelCode" value="{{oh.otaHotelCode}}" /></div>
									<button type="submit" ng-click="getOtaHotelDetails()" class="btn btn-primary" >Get Details</button>
								</ul> 
							</div>
						</div>
						<!-- /.box -->
					</div>	
         	</div>
          
        </section><!-- /.content -->
      	 <!-- Another MAin -->
      	   <section class="content">
        
        	<div class="row">
            	<div class="col-xs-12">
						<div class="box">
							<div class="box-header">
								<h3 class="box-title">Ulotel OTA Hotel Details</h3>
								
							</div>
							
							
							<!-- /.box-header -->
							<div class="box-body table-responsive no-padding" > 

													<table class="table table-hover">
									<tr>										
										<th>Room Type Id</th>
										<th>Room Type Name</th>
										<th>RatePlan Id</th>
										<th>RAtePlan Name</th>
										<th>Status</th>
										<th>Delete</th>
									</tr>
									<tr ng-repeat="ofhd in otaFullHotelDetails">										
										<td>{{ofhd.otaRoomTypeId}}</td>
										<td>{{ofhd.accommodationName}}</td>
										<td>{{ofhd.otaRatePlanId}}</td>
										<td>{{ofhd.ratePlanName}}</td>
										<td>{{ofhd.isActive}}</td>
										<td>
											<a href="" ng-click="deleteOtaHotelDetails(ofhd.DT_RowId)"  data-toggle="modal"><i  class="fa fa-fw fa-trash text-red"></i></a>
										</td>
									</tr>
									<tr>
									</tr>
								</table>
							</div>
							<!-- /.box-body -->
							
							
							<div class="box-footer clearfix">
								 <ul class="pagination pagination-sm no-margin pull-right">
								<a class="btn btn-primary  btngreen" href="#addmodal" ng-click="enableTask(x.DT_RowId)"  data-toggle="modal" >Add Details</a>

								</ul> 
							</div>
						</div>
						<!-- /.box -->
					</div>	
         	</div>
          
        </section><!-- /.content -->
      	<div class="modal" id="addmodal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" id="btnclose" class="close" data-dismiss="modal"
							aria-hidden="true">x</button>
						<h4 class="modal-title">Add Hotel Details</h4>
					</div> 
					 <form name="addrooms">
					<div class="modal-body" >
					  	 <div class="form-group col-md-6">
							<label>Select Accomadtion Type</label>
							<select name="propertyAccommodationId" id="propertyAccommodationId" value="" class="form-control">
	                       	<option ng-repeat="at in accommodations" value="{{at.accommodationId}}">{{at.accommodationType}}</option>
	                        </select>
						  </div>
					    <div class="form-group col-md-6">
							<label for="roomTypeCode">Room Type Code</label>
							<input type="text" class="form-control"  name="roomTypeCode" id="roomTypeCode" placeholder="Room Type Code" ng-model='roomalias' ng-pattern="/^[A-Za-z\d\s]+$/" ng-required="true" maxlength="100">
						     <span ng-show="addrooms.roomTypeCode.$error.pattern">Not a Valid Room Alias!</span>
						</div>
						
						<div class="form-group col-md-6">
							<label for="ratePlanCode">Rate Plan Code</label>
							<input type="text" class="form-control"  name="ratePlanCode"  id="ratePlanCode"  placeholder="Rate Plan Code" ng-model='SortKey' ng-pattern="/^[a-zA-Z0-9]+$/" ng-required="true" maxlength="100">
						    <span ng-show="addrooms.ratePlanCode.$error.pattern">Not a Valid Sort Key!</span>
						</div>
						<div class="form-group col-md-6">
							<label>Select Rate Paln Type</label>
							<select name="ratePlanId" id="ratePlanId" value="" class="form-control">
	                       	<option ng-repeat="rp in ratePlans" value="{{rp.ratePlanId}}">{{rp.ratePlanType}}</option>
	                        </select>
						  </div>
		
					</div>
					<div class="modal-footer">
	
						<a href="#" data-dismiss="modal" class="btn btn-danger">Close</a>
						<button ng-click="addrooms.$valid && saveOtaHotelDetails()" ng-disabled="addrooms.$invalid" class="btn btn-primary btngreen">Save</button>
					</div>
					</form>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dalog -->
		</div>    

      </div><!-- /.content-wrapper -->
      




<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>
  
 <%--  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> 
 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/codemirror.min.js"></script>
 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.25.0/mode/xml/xml.min.js"></script>
 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.6.0//js/froala_editor.pkgd.min.js"> </script> --%>
	
 <script>


 
	
		var app = angular.module('myApp', ['ngProgress']);
		app.controller('customersCtrl', function($scope, $http,$timeout,ngProgressFactory) {
			var spinner = $('#loadertwo');
			  
			 $(function(){
				  $('#edit').froalaEditor()
				}); 
			
	       	$timeout(function(){
	      	    // $scope.progressbar.complete();
	            $scope.show = true;
	            $("#pre-loader").css("display","none");
	        }, 2000);

			 
			
			$scope.unread = function() {
			//var notifiurl = "http://localhost:8085/collaborative-workflow/unreadnotifications.action";
			var notifiurl = "unreadnotifications.action";
			$http.get(notifiurl).success(function(response) {
				$scope.latestnoti = response.data;
			});
			};

           
			
			
				
			$scope.getOtaHotels = function() {
	   			
	   			var url = "get-ota-hotels";
	   			$http.get(url).success(function(response) {
	   				//alert(JSON.stringify(response.data));
	   				$scope.otaHotels = response.data;
	   			
	   			});
	   		};    
	   		
			  $scope.getOtaHotelDetails = function() {
				  
	   			var url = "get-ota-hotel-details";
	   			spinner.show();
	   			$http.get(url).success(function(response) {
// 	   				alert('test...'+JSON.stringify(response.data));
	   				$scope.otaHotelDetails = response.data;
	   				
	   				setTimeout(function(){ spinner.hide(); 
					  }, 1000); 
	   			});
	   		}; 
	   		
 	   	/* 	$scope.getOtaHotelDetails = function(){

	   			var fdata = "otaHotelCode=" + $('#otaHotelCode').val();
			
			$http(
					{
						method : 'POST',
						data : fdata,
						headers : {
							'Content-Type' : 'application/x-www-form-urlencoded'
						},
						url : 'get-ota-hotel-details'
					}).then(function successCallback(response) {
						
						$scope.otaHotelDetails = response.data;
						alert($scope.otaHotelDetails.data[0].roomTypeName)
			}, function errorCallback(response) {
					$scope.resultmsg = "sorry no records found";
			});

		}; 

 */	   		
		$scope.getOtaFullHotelDetails = function() {
	   				
	   			var url = "get-ota-full-hotel-details";
	   			$http.get(url).success(function(response) {
	   				//alert(JSON.stringify(response.data));
	   				$scope.otaFullHotelDetails = response.data;
	   			
	   			});
	   		};  
	   		
			$scope.getRatePlans = function() {
	   			
	   			var url = "get-rate-plans";
	   			$http.get(url).success(function(response) {
	   				//alert(JSON.stringify(response.data));
	   				$scope.ratePlans = response.data;
	   			
	   			});
	   		};   
	   		
	   		$scope.getAccommodations = function() {

				var url = "get-accommodations";
				$http.get(url).success(function(response) {
				    //console.log(response);
					$scope.accommodations = response.data;
		
				});
			};
			
			$scope.saveOtaHotelDetails = function(){
					//alert("enter")
				var fdata = "otaHotelCode=" + $('#otaHotelCode').val()
				+ "&otaRoomTypeId=" + $('#roomTypeCode').val()
				+ "&otaRatePlanId=" +$('#ratePlanCode').val()
				+ "&ratePlanId=" +$('#ratePlanId').val()
				+ "&propertyAccommodationId=" +$('#propertyAccommodationId').val();
			spinner.show();
				$http(
						{
							method : 'POST',
							data : fdata,
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							url : 'save-ota-hotel-details'
						}).then(function successCallback(response) {
							alert("ota hotel detail update successfully!!")
							 setTimeout(function(){ spinner.hide(); 
			  }, 1000);
							$scope.getOtaFullHotelDetails();
							
				}, function errorCallback(response) {

					// called asynchronously if an error occurs
					// or server returns response with an error status.
				});

			};

			$scope.deleteOtaHotelDetails = function(rowId){
				//alert("enter")
			var fdata = "otaHotelDetailsId=" + rowId;
			spinner.show();
			//alert(fdata);
			$http(
					{
						method : 'POST',
						data : fdata,
						headers : {
							'Content-Type' : 'application/x-www-form-urlencoded'
						},
						url : 'delete-ota-hotel-details'
					}).then(function successCallback(response) {
						alert("your ota hotel detail removed successfully!!")
						 setTimeout(function(){ spinner.hide(); 
			  }, 1000);
						$scope.getOtaFullHotelDetails();
						
			}, function errorCallback(response) {

				// called asynchronously if an error occurs
				// or server returns response with an error status.
			});

		};
			
			
            $scope.getSingleUser = function(rowid) {
				
				//alert(rowid);
				var url = "get-user?propertyUserId="+rowid;
					$http.get(url).success(function(response) {
				    console.log(response);				
					$scope.user = response.data;
		
				});
				
			};

			 $scope.getPropertyList = function() {
				 
		        	
		        	
			        var userId = $('#adminId').val();
		 			var url = "get-user-properties?userId="+userId;
		 			$http.get(url).success(function(response) {
		 			    
		 				$scope.props = response.data;
		 	
		 			});
		 		};
		 		
            $scope.change = function() {
     	   
		        //alert($scope.id);
		        
		        var propertyId = $scope.id;	
     	    var url = "change-user-property?propertyId="+propertyId;
	 			$http.get(url).success(function(response) {
	 				
	 				 window.location = '/ulopms/dashboard'; 
	 				//$scope.change = response.data;
	 	
	 			});
			       
		 		};
		 		
		 	
		 		
				
		    $scope.getPropertyList();
			
		    $scope.getOtaHotels();
			
		    $scope.getRatePlans();
		    
		    $scope.getAccommodations();
		    
		    $scope.getOtaFullHotelDetails();
		    
		    $scope.otaHotelDetails=["data"];
		    
			//$scope.unread();
			//
			
			
		});

	
		
		   
		        

		
	</script>
	
	
	<%-- <script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.js"></script> 

<script>
   $(function () {
      // Replace the <textarea id="editor1"> with a CKEditor
      // instance, using default configuration.
      CKEDITOR.replace('editor1');
      //bootstrap WYSIHTML5 - text editor
      $(".textarea").wysihtml5();
      
      CKEDITOR.replace('metaDescription');
      //bootstrap WYSIHTML5 - text editor
      $(".textarea").wysihtml5();
      
     CKEDITOR.replace('schemaContent');
      //bootstrap WYSIHTML5 - text editor
      $(".textarea").wysihtml5();
      
      CKEDITOR.replace('locationContent');
      //bootstrap WYSIHTML5 - text editor
      $(".textarea").wysihtml5();
      
     CKEDITOR.replace('scriptContent');
     // bootstrap WYSIHTML5 - text editor
      $(".textarea").wysihtml5();
    });
	
  
</script> --%>





 
	
	 <script src="js/jquery-2.1.1.js" type="text/javascript"></script>
       