<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="s" uri="/struts-tags"%>
   <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
        Rate Manager
          </h1>
          <ol class="breadcrumb">
            <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Rate Manager</li>
          </ol>
        </section>
		
        <!-- Main content -->
        <section class="content">
        
        	<div class="row">
            	<div class="col-xs-12">
						<div class="box">
							<div class="box-header">
								<h3 class="box-title">Rate Manager</h3>
							</div>
							
							
							<!-- /.box-header -->
							<div class="box-body table-responsive no-padding" > 
								<table class="table table-hover" id="example1">
								<thead>
									<tr>
									 <th>Order</th>
								   <th>Rate Name</th>
								   <th>Accommodation Type</th>
								   <th>Price</th>
								   <th>Start</th>
								    <th>End</th>
								    
									<th>Edit</th>
									<th>Delete</th>
									</tr>
								</thead>
								<tbody class="sortable">
										<tr ng-repeat="r in rates track by $index"  >
										<td>{{$index}}</td>
									    <td>{{r.rateName}}</td>
										<td>{{r.accommodationType}}</td>
										<td>{{r.baseAmount}}</td>										
										<td>{{r.startDate}}</td>
										<td>{{r.endDate}}</td>
										
											<td>
											<a href="#editmodal" ng-click="getRate(r.rateId)"  data-toggle="modal"><i  class="fa fa-pencil text-green"></i></a>
										
										</td>
											<td>
											
											<a href="#" ng-click="deleteRates(r.rateId)"  data-toggle="modal"><i  class="fa fa-fw fa-trash text-red"></i></a>
										</td>
									</tr>
								
								</tbody>
							
								</table>
							</div>
							<!-- /.box-body -->
							
							
							<div class="box-footer clearfix">
								 <ul class="pagination pagination-sm no-margin pull-right">
									<a class="btn btn-primary" href="#addmodal" ng-click=""  data-toggle="modal" >Add Rates</a>
								</ul> 
							</div>
						</div>
						<!-- /.box -->
					</div>	
         	</div>
          
        </section><!-- /.content -->
      	
      	<div class="modal" id="addmodal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" id="btnclose" class="close" data-dismiss="modal"
							aria-hidden="true">x</button>
						<h4 class="modal-title">Add Rates</h4>
					</div>
					 <form name="addForm" id="addRate">
					<div class="modal-body" >
					<div id="message"></div>
                          <div class="form-group">
					      <label>Select Accommodation Type :</label>
							<select  name="propertyAccommodationId" id="propertyAccommodationId" value="" class="form-control " ng-required="true">
							<option style="display:none" value="">Select Your  Accommodation</option>
	                       	<option ng-repeat="at in accommodations" value="{{at.accommodationId}}">{{at.accommodationType}}</option>
	                        </select>
						 </div>
						 
						<div class="form-group">
							<label>Rate Name</label>
							<input type="text" class="form-control"  name="rateName"  value=""  ng-model='rateName' ng-pattern= "/^[a-zA-Z0-9]*$/" id="rateName" ng-required="true">
							<span ng-show="addForm.rateName.$error.pattern" style="color:red">Not a valid Name!</span>
						 </div>
						
						<div class="form-group">
							<label>Source</label>
							<select  class="form-control" name="sourceId"  id="sourceId" value="" class="form-control " ng-required="true">
							<option ng-repeat="sr in sources" value="{{sr.sourceId}}" ng-selected ="sr.sourceId == sr.sourceId">{{sr.sourceName}}</option>
							</select>
						</div>
						 
						<div class=" form-group">
							<label>Start Date :</label>
					 <div class="input-group date">
                   <label class="input-group-addon btn" for="bookCheckin">
                   <span class="fa fa-calendar"></span>
                  </label>   
                  <input type="text" id="startDate" type="text" class="form-control" name="startDate"  value="" onkeypress="return false;" ng-required="true">
 
                  
                </div>
						     
						</div>
						
						<div class="form-group">
							<label >End Date :</label>
				      <div class="input-group date">
                   <label class="input-group-addon btn" for="bookCheckout">
                   <span class="fa fa-calendar"></span>
              </label>   
                  <input type="text" id="endDate" type="text" class="form-control" name="endDate"  value="" onkeypress="return false;" ng-required="true"> 

                </div>
						     
						</div>
			
						<div class="form-group">
							<label>Days of week :</label>
						   <div class="input-group" >

						   	<label class="checkbox-inline"><input type="checkbox"  id="checkAll" value="">All Days</label>
                          <div ng-repeat="d in days">
                           <label>
                      					<input type="checkbox" name="checkedDay[]"  id="checkedDay" value="{{d.dayId}}"  class="flat-red" >&nbsp {{d.daysOfWeek}}
                    				</label>
                    				</div>
						   </div>

						</div>
					    <div class="form-group">
							<label>Amount</label>
							<input type="text" class="form-control"  name="amount"  id="amount" ng-model="amount" ng-pattern="/^[0-9]/" ng-required="true">
						    <span ng-show="addForm.amount.$error.pattern" style="color:red">Not a valid Amount!</span>
						</div>
			
					<div class="modal-footer">
	
						<a href="#" data-dismiss="modal" class="btn">Close</a>
						<button ng-click="addForm.$valid && updatePrice()" ng-disabled="addForm.$invalid" class="btn btn-primary">Save</button>
					</div>
					</form>
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dalog -->
		</div>
		<div class="modal" id="editmodal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" id="editbtnclose" class="close" data-dismiss="modal"
							aria-hidden="true">x</button>
						<h4 class="modal-title">Edit Tax</h4>
					</div>
					<form name="editForm" id="editRate" >
					<div class="modal-body" ng-repeat="r in rate">
					<div id="editmessage"></div>
					<input type="hidden" class="form-control" value="{{r.rateId}}" name="editrateId"  id="editrateId" ng-required="true">
					  <div class="form-group">
					   
					      <label>Select Accommodation Type :</label>
							<select  name="editpropertyAccommodationId" id="editpropertyAccommodationId" value="" class="form-control " ng-required="true">
							<option style="display:none" value="">Select Your  Accommodation</option>
	                       	<option ng-repeat="at in accommodations" value="{{at.accommodationId}}" ng-selected ="at.accommodationId == r.accommodationId">{{at.accommodationType}}</option>
	                        </select>
						 </div>
						 
						 <div class="form-group">
							<label>RateName</label>
							<input type="text" class="form-control" value="{{r.rateName}}" name="editrateName"  id="editrateName" ng-model='r.rateName' ng-pattern= "/^[a-zA-Z0-9]*$/" ng-required="true">
						    <span ng-show="editForm.editrateName.$error.pattern" style="color:red">Not a valid Name!</span>
						</div>
						
						<div class="form-group">
							<label>Source</label>
							<select  class="form-control" name="editsourceId"  id="editsourceId" value="" class="form-control " ng-required="true">
							<option ng-repeat="sr in sources" value="{{sr.sourceId}}" ng-selected ="sr.sourceId == r.sourceId">{{sr.sourceName}}</option>
							</select>
						</div>
						 
						<div class=" form-group">
							<label>Start Date :</label>
					 <div class="input-group date">
                   <label class="input-group-addon btn" for="bookCheckin">
                   <span class="fa fa-calendar"></span>
                  </label>   
                  <input type="text" id="editstartDate" type="text" class="form-control" name="editstartDate"  value="{{r.startDate}}" onkeypress="return false;" ng-required="true">
 
                  
                </div>
						     
						</div>
						
						<div class="form-group">
							<label >End Date :</label>
				      <div class="input-group date">
                   <label class="input-group-addon btn" for="bookCheckout">
                   <span class="fa fa-calendar"></span>
              </label>   
                  <input type="text" id="editendDate" type="text" class="form-control" name="editendDate"  value="{{r.endDate}}" onkeypress="return false;" ng-required="true"> 

                </div>  <!---->
						     
						</div>
						
						<div class="form-group">
							<label>Days of week :</label>
						   <div class="input-group"   >

						   	<label class="checkbox-inline"><input type="checkbox"  id="checkAll" value="">All Days</label>
                          <div ng-repeat="ds in r.days" > <!--  ng-repeat="d in days" -->
                           <label>
                      					<input type="checkbox" name="checkedDay[]"  id="checkedDay" value="{{ds.dayPart}}" ng-checked ="ds.status == 'true'"  class="flat-red" >&nbsp {{ds.daysOfWeek}}
                    				</label>
                    				</div>
						   </div>

						</div>
						
						<div>
					     <div class="form-group">
							<label>Amount</label>
							<input type="text"  class="form-control"  name="editamount"  id="editamount" value="{{r.baseAmount}}" ng-model="r.baseAmount" ng-pattern="/^[0-9]/" ng-required="true">
						    <span ng-show="editForm.editamount.$error.pattern" style="color:red">Not a valid Amount!</span>
						</div>
						</div>
			
					   <div class="modal-footer">
	
						<a href="#" data-dismiss="modal" class="btn">Close</a>
						<button type="submit" ng-click="editRate()" class="btn btn-primary">Update</button>
					</div>
					</div>
					</form>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dalog -->
		</div>
		
		<div class="modal" id="deletemodal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" id="deletebtnclose" class="close" data-dismiss="modal"
							aria-hidden="true">x</button>
						<h4 class="modal-title">Delete Message</h4>
					</div>
					<div class="modal-body">
					 <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-check"></i>Your Rates has been deleted Sucessfully </h4>
                  
                 </div>
					    
						
					</div>
					<div class="modal-footer">
	                    
						<a href="#" data-dismiss="modal" class="btn">Close</a>
						
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dalog -->
		</div>
    

      </div><!-- /.content-wrapper -->
 
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>
  <style type="text/css">
    #ui-datepicker-div
    {
        z-index: 9999999 !important;
    }
</style>>
	<script>
	 
	
	var app = angular.module('myApp', ['ngProgress']);
	app.controller('customersCtrl', function($scope, $http,$timeout,ngProgressFactory) {
     
		$scope.dragStart = function (e, ui) {
            ui.item.data('start', ui.item.index());
          }
          
            $scope.dragEnd = function (e, ui) {
            var start = ui.item.data('start'),
            end = ui.item.index();
          
            $scope.rates.splice(end, 0, $scope.rates.splice(start, 1)[0]);
            $scope.$apply();
            console.log($scope.rates);
           // var myJSON = JSON.stringify($scope.rates);
           // alert(myJSON);
            var text = '{"orders":' + JSON.stringify($scope.rates) + '}';
            
			  var data = JSON.parse(text);
				
	             $http(
						{
							method : 'POST',
							data : data,
							//data : '{"data":' +JSON.stringify($scope.roomsSelected) + '}',
							dataType: 'json',
							headers : {
								 'Content-Type' : 'application/json; charset=utf-8'

							},
							
							//url : 'bookingdetails'
							url : 'edit-rate-orders'
							
						}).success(function(response) {
							
							//window.location = '/bookingdetails'; 
							
							$scope.orders = response.data;
							
							//console.log($scope.booked[0].firstName);
							
							
						});
            
          }
          
          var sortableEle;
              sortableEle = $('.sortable').sortable({
              start: $scope.dragStart,
              update: $scope.dragEnd
              
          });
		 $("#checkAll").click(function () {
		     $('input:checkbox').not(this).prop('checked', this.checked);
		 });

		
       	$timeout(function(){
      	    // $scope.progressbar.complete();
            $scope.show = true;
            $("#pre-loader").css("display","none");
        }, 2000);

		 
		
		$scope.unread = function() {
		//var notifiurl = "http://localhost:8085/collaborative-workflow/unreadnotifications.action";
		var notifiurl = "unreadnotifications.action";
		$http.get(notifiurl).success(function(response) {
			$scope.latestnoti = response.data;
		});
		};

		 
	       $("#startDate").datepicker({
	            dateFormat: 'mm/dd/yy',
	            minDate:  0,
	            onSelect: function (formattedDate) {
	                var date1 = $('#startDate').datepicker('getDate'); 
	                var date = new Date( Date.parse( date1 ) ); 
	                date.setDate( date.getDate() + 1 );        
	                var newDate = date.toDateString(); 
	                newDate = new Date( Date.parse( newDate ) );   
	                $('#endDate').datepicker("option","minDate",newDate);
	                $timeout(function(){
	                  //scope.checkIn = formattedDate;
	                });
	            }
	        });
	      
	      $("#endDate").datepicker({
	            dateFormat: 'mm/dd/yy',
	            minDate:  0,
	            onSelect: function (formattedDate) {
	                var date2 = $('#endDate').datepicker('getDate'); 
	                $timeout(function(){
	                  //scope.checkOut = formattedDate;
	                });
	            }
	        });
	      $("#startDates").datepicker({
	            dateFormat: 'mm/dd/yy',
	            minDate:  0,
	            onSelect: function (formattedDate) {
	                var date1 = $('#startDates').datepicker('getDate'); 
	                var date = new Date( Date.parse( date1 ) ); 
	                date.setDate( date.getDate() + 1 );        
	                var newDate = date.toDateString(); 
	                newDate = new Date( Date.parse( newDate ) );   
	                $('#endDates').datepicker("option","minDate",newDate);
	                $timeout(function(){
	                  //scope.checkIn = formattedDate;
	                });
	            }
	        });
	      
	      $("#endDates").datepicker({
	            dateFormat: 'mm/dd/yy',
	            minDate:  0,
	            onSelect: function (formattedDate) {
	                var date2 = $('#endDates').datepicker('getDate'); 
	                $timeout(function(){
	                  //scope.checkOut = formattedDate;
	                });
	            }
	        });
		
	      $scope.updatePrice = function(){
				//alert($('#taskId').val());
				
			    var checkbox_value = "";
				    $("#addRate :checkbox").each(function () {
				        var ischecked = $(this).is(":checked");
				        if (ischecked) {
				            checkbox_value += $(this).val() + ",";
				        }
				    });
				    
		     	//alert(checkbox_value);
		     	//alert($('#propertyAccommodationId').val());
		     	//alert($('#startDate').val());
		     	//alert($('#endDate').val());
		     	//alert($('#amount').val());
		     	
		     	var fdata = "checkedDays=" + checkbox_value
		     	+ "&propertyAccommodationId=" + $('#propertyAccommodationId').val()
		     	+ "&sourceId=" + $('#sourceId').val()
		     	+ "&rateName=" + $('#rateName').val()
				+ "&startDate=" + $('#startDate').val()
				+ "&endDate=" + $('#endDate').val()
				
				
				
				
				//alert(fdata);
				$http(
						{
							method : 'POST',
							data : fdata,
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							url : 'add-rate'
						}).then(function successCallback(response) {
							
							
							var gdata = "checkedDays=" + checkbox_value
					     	    + "&baseAmount=" + $('#amount').val()
					     	    + "&startDate=" + $('#startDate').val()
				                + "&endDate=" + $('#endDate').val()
							
							//alert(gdata);
							$http(
						    {
							method : 'POST',
							data : gdata,
							//data : '{"data":' +JSON.stringify($scope.roomsSelected) + '}',
							
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
								//'Content-Type' : 'application/x-www-form-urlencoded'
							},
							url : 'add-rate-details'
							//url : 'jsonTest'
						   }).then(function successCallback(response) {
							   window.location.reload();
							
						    alert("rate added successfully");
						  
						    
						   }, function errorCallback(response) {
							
							  
							
						});
							
							
							
				}, function errorCallback(response) {
					
				  
					
				});

			};
			
			  $scope.editRate = function(){
					//alert($('#taskId').val());
					
				    var checkbox_value = "";
					    $("#editRate :checkbox").each(function () {
					        var ischecked = $(this).is(":checked");
					        if (ischecked) {
					            checkbox_value += $(this).val() + ",";
					        }
					    });
					    
			     	//alert(checkbox_value);
			     	//alert($('#propertyAccommodationId').val());
			     	//alert($('#startDate').val());
			     	//alert($('#endDate').val());
			     	//alert($('#amount').val());
			     	
			     	var fdata = "checkedDays=" + checkbox_value
			     	+ "&propertyAccommodationId=" + $('#editpropertyAccommodationId').val()
			     	+ "&sourceId=" + $('#editsourceId').val()
			     	+ "&rateName=" + $('#editrateName').val()
			     	+ "&propertyRateId=" + $('#editrateId').val()
					+ "&startDate=" + $('#editstartDate').val()
					+ "&endDate=" + $('#editendDate').val()
					
					
					
					
					//alert(fdata);
					$http(
							{
								method : 'POST',
								data : fdata,
								headers : {
									'Content-Type' : 'application/x-www-form-urlencoded'
								},
								url : 'edit-rate'
							}).then(function successCallback(response) {
								
								
								var gdata = "checkedDays=" + checkbox_value
						     	    + "&baseAmount=" + $('#editamount').val()
						     	   + "&propertyRateId=" + $('#editrateId').val()
						     	    + "&startDate=" + $('#editstartDate').val()
					                + "&endDate=" + $('#editendDate').val()
								
								//alert(gdata);
								$http(
							    {
								method : 'POST',
								data : gdata,
								//data : '{"data":' +JSON.stringify($scope.roomsSelected) + '}',
								
								headers : {
									'Content-Type' : 'application/x-www-form-urlencoded'
									//'Content-Type' : 'application/x-www-form-urlencoded'
								},
								url : 'edit-rate-details'
								//url : 'jsonTest'
							   }).then(function successCallback(response) {
								   window.location.reload();
								
							    alert("rate updated successfully");
							  
							    
							   }, function errorCallback(response) {
								
								  
								
							});
								
								
								
					}, function errorCallback(response) {
						
					  
						
					});

				};
			
			
	    $scope.getDays = function(){
				
		       var checkbox_value = "";
		       $(":checkbox").each(function () {
		        var ischecked = $(this).is(":checked");
		        if (ischecked) {
		            checkbox_value += $(this).val() + ",";
		        }
		       });
		      
		       //alert(checkbox_value);
		       
			    var fdata = "startDate=" + $('#startDate').val()
				+ "&endDate=" + $('#endDate').val()
				+ "&checkedDays=" + checkbox_value
				
				
				//alert(fdata);
			    
				$http(
						{
							method : 'POST',
							data : fdata,
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							url : 'get-days'
						}).then(function successCallback(response) {
							$timeout(function(){
						      	$('#btnclose').click();
						        }, 2000); 
							window.location.reload();
							
						//alert(response.data);
				}, function errorCallback(response) {
					
					// called asynchronously if an error occurs
					// or server returns response with an error status.
				});

			};
			
		$scope.getAccommodations = function() {

			var url = "get-accommodations";
			$http.get(url).success(function(response) {
			    //console.log(response);
				$scope.accommodations = response.data;
	
			});
		};
		
		$scope.getRates = function() {

			var url = "get-rates";
			$http.get(url).success(function(response) {
			    //console.log(response);
				$scope.rates = response.data;
	
			});
		};
		
		 $scope.getRate = function(rowid) {
			// alert(rowid);
				var url = "get-rate?propertyRateId="+rowid;
				$http.get(url).success(function(response) {
				   //alert(response);
					$scope.rate = response.data;
		
				});
			};
			
			 $scope.deleteRates = function(rowid) {
					
					var url = "delete-rates?propertyRateId="+rowid;
					$http.get(url).success(function(response) {
					    //console.log(response);
					    //$scope.getTaxes();
					    // $('#deletemodal').modal('toggle');
					    $timeout(function(){
					      	$('#deletebtnclose').click();
					    }, 2000); 
					    window.location.reload();
			
					});
					
				};
		
		$scope.getSources = function() {

			var url = "get-sources";
			$http.get(url).success(function(response) {
			    //console.log(response);
				$scope.sources = response.data;
	
			});
		};
		
		$scope.getDay = function() {

			var url = "get-day";
			$http.get(url).success(function(response) {
			    //console.log(response);
				$scope.days = response.data;
	
			});
		};
		
		
		
		
	  $scope.getPropertyList = function() {
			 
	        	
	        	
		        var userId = $('#adminId').val();
	 			var url = "get-user-properties?userId="+userId;
	 			$http.get(url).success(function(response) {
	 			    
	 				$scope.props = response.data;
	 	
	 			});
	 		};
	 		
       $scope.change = function() {
 	   
	        //alert($scope.id);
	        
	        var propertyId = $scope.id;	
 	        var url = "change-user-property?propertyId="+propertyId;
 			$http.get(url).success(function(response) {
 				
 				 window.location = '/ulopms/dashboard'; 
 				//$scope.change = response.data;
 	
 			});
		       
	 		};
	 		
			
	    $scope.getPropertyList();
	    $scope.getSources();
		$scope.getAccommodations();
		$scope.getRates();
		$scope.getDay();
		//$scope.unread();
		//
		
		
	});
	</script>
  <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
 
<%--     <script>
  $( function() {
    $("#startDate" ).datepicker({minDate:0});
    $("#endDate" ).datepicker({minDate:0});
    $("#checkin" ).datepicker({minDate:0});
    $("#checkout" ).datepicker({minDate:0});
  });
  </script> --%>
       