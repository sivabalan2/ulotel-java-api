<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<!--
   This is a starter template page. Use this page to start your new project from
   scratch. This page gets rid of all links and provides the needed markup only.
   -->
<html>
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>ULO Hotels</title>
      <!-- Tell the browser to be responsive to screen width -->
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
      <!-- Bootstrap 3.3.5 -->
      <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
      <!-- Font Awesome -->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
      <!-- Ionicons -->
      <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
      <!-- Theme style -->
       <link rel="stylesheet" href="css/bootstrap.min.css">
      <link rel="stylesheet" href="css/ulotel.css?v=1.2">
      <link rel="stylesheet" href="css/ulotelmq.css">
      <link rel="stylesheet" href="css/AdminLTE.min.css?v=1.2">
       <link rel="stylesheet" href="css/select2.css">
      
      <!-- ngprogress-lite - progressbar -->
      <!-- ngprogress-lite.css -->
   </head>
    <body  ng-app="myApp" ng-cloak ng-controller="customersCtrl" class="ulobody" >
    <div id="preloader"><div class="spinner-sm spinner-sm-1" id="status"> <img src="https://www.yogatreesf.com/app/plugins/yogaworks-mbo/dist/images/loader.gif" width="50" height="50" alt="Loading..." class="loader"></div></div>
        <div id="loadertwo"></div>
      <div class="wrapper">
      
         <header class="main-header ulo-header">
            <!-- Logo -->
            <a href="dashboard" class="logo ">
            <img src="images/logo/ulotel.png" alt="logo">
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
               <!-- Sidebar toggle button-->
               <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
               <span class="sr-only">Toggle navigation</span>
               </a>
               <div class="navbar-custom-menu">
                  <ul class="nav navbar-nav">
                     <li class="dropdown notifications-menu unhov">
                        <a class="navbartime">
                        <i class="fa fa-clock-o" aria-hidden="true"></i>
                        <span class="navtime">
                          <span id="todaytime"></span>
                        </span>
                        </a>
                     </li>
                     <li class="dropdown notifications-menu unhov sideborder">
                        <a class="navbartime">
                        <i class="fa fa-calendar" aria-hidden="true"></i>
                        <span class="navtime">
                         <span id="todaydate"></span>
                        </span>
                        </a>
                     </li>
                     <li class="dropdown notifications-menu sideborder">
                        <a href="landing" class="navbarlist">
                           <span class="hidden-xs">
                              <s:property value="#session.propertyName"/>
                           </span>
                           <i class="fa fa-chevron-down" aria-hidden="true" ></i>
                        </a>
                     </li>
                     <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                           <img src="<s:url action='profile-picture.action'/>" class="user-image" alt="User Image">
                           <i class="fa fa-chevron-down" aria-hidden="true"></i>
                           <span class="hidden-xs">
                              <s:property value="user.userName"/>
                           </span>
                        </a>
                        <ul class="dropdown-menu">
                           <!-- User image -->
                           <li class="user-header">
                              <img src="<s:url action='profile-picture.action'/>" class="img-circle" alt="User Image">
                              <p>
                                 <s:property value="user.userName"/>
                                 - 
                                 <s:property value="#session.role"/>
                                 <br>
                                 <s:property value="#session.propertyName"/>
                                 <span class="hidden">
                                    <s:property value="#session.propertyId"/>
                                 </span>
                              </p>
                           </li>
                           <li class="user-footer">
                              <div class="pull-left">
                                 <a href="userprofile" class="btn btn-primary btn-flat">Profile</a>
                              </div>
                              <div class="pull-right">
                                 <a href="logout" class="btn btn-danger btn-flat">Sign out</a>
                              </div>
                           </li>
                        </ul>
                     </li>
                  </ul>
               </div>
            </nav>
         </header>
         <!-- Left side column. contains the logo and sidebar -->
         <aside class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">
               <!-- sidebar menu: : style can be found in sidebar.less -->
               <ul class="sidebar-menu">
                  <li class="header">NAVIGATION</li>
                  <s:if test="%{#session.userRights.indexOf('DASHBOARD')!=-1}">
                     <li class="active treeview">
                        <a href="dashboard">
                           <input type="hidden" id="adminId" value="<s:property value="user.userId" />" name="">
                           <!--  <input type ="text" value="<s:property value="#session.userRights"/>" > -->
                         <img src="images/baselayout/yellow/dashboard.png" alt="Dashboard">
                           <span>DASHBOARD</span></i>
                        </a>
                     </li>
                  </s:if>
                  <s:if test="%{#session.userRights.indexOf('PROPERTY DETAILS')!=-1}">
                     <li class="treeview">
                        <a href="#">
                        <img src="images/baselayout/yellow/propertydetails.png" alt="Dashboard"><span>HOTEL PROFILE</span>
                        <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                           <li><a href="property-profile"> <i class="fa fa-arrow-circle-right"></i> 
                              HOTEL PROFILE</a>
                           </li>
<!--                            <li><a href="property-amenities"><i class="fa fa-arrow-circle-right"></i>  PROPERTY AMENITIES</a></li> -->
                           <%--  <li><a href="add-amenities"><i class="fa fa-circle-o"></i> Add Amenities</a></li>  --%>
<!--                            <li><a href="accommodation"><i class="fa fa-arrow-circle-right"></i>  ROOM CATEGORY</a></li> -->
<!--                            <li><a href="accommodation-rooms"><i class="fa fa-arrow-circle-right"></i> ROOM INVENTORY</a></li> -->
                        </ul>
                     </li>
                  </s:if>
                  <s:if test="%{#session.userRights.indexOf('TAXES')!=-1}">
                     <li class="treeview">
                        <a href="#">
                        <img src="images/baselayout/yellow/taxes.png" alt="Dashboard"> <span>TAXES</span>
                        <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                           <li><a href="taxes"><i class="fa fa-arrow-circle-right"></i> ADD TAX</a></li>
                           <!--   <li><a href="item-category"><i class="fa fa-sticky-note" style="color:#88ba41;"></i>MEAL PLAN</a></li>
                              <li><a href="item"><i class="fa fa-cart-arrow-down" style="color:#88ba41;"></i>FOOD ITEMS</a></li> -->
                        </ul>
                     </li>
                  </s:if>
                  <%-- <s:if test="%{#session.userRights.indexOf('RATE MANAGEMENT')!=-1}">
                     <li class="treeview">
                        <a href="#">
                        <img src="images/baselayout/yellow/rates.png" alt="Dashboard"> <span>RATE MANAGEMENT</span>
                        <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                           <!-- <li><a href="booking"><i class="fa fa-circle-o"></i>Booking</a></li> -->
                           <li><a href="ratemanager"><i class="fa fa-arrow-circle-right"></i> <span>PRICE MANAGEMENT</span></a></li>
                           <li><a href="activeratemanager"><i class="fa fa-arrow-circle-right"></i> <span>ACTIVE RATES</span></a></li>
                        </ul>
                     </li>
                  </s:if> --%>
                  <s:if test="%{#session.userRights.indexOf('OFFER AND DEALS')!=-1}">
                     <li class="treeview">
                        <a href="#">
                        <img src="images/baselayout/yellow/offers.png" alt="Dashboard"> <span>COUPONS & REWARDS</span>
                        <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
<%--                            <li><a href="promotion-manager"><i class="fa fa-arrow-circle-right"></i> <span>MANAGE PROMOTION</span></a></li> --%>
                           <li><a href="promotion-images"><i class="fa fa-arrow-circle-right"></i>  <span>PROMOTION IMAGES</span></a></li>
                           <li><a href="discountmanager"><i class="fa fa-arrow-circle-right"></i> <span> MANAGE COUPON</span></a></li>
                           <li><a href="rewards-manager"><i class="fa fa-arrow-circle-right" ></i> <span> MANAGE REWARDS</span></a></li>

                        </ul>
                     </li>
                  </s:if>
                  <%-- <s:if test="%{#session.userRights.indexOf('ROOM MANAGEMENT')!=-1}">
                     <li class="treeview">
                        <a href="#">
                        <img src="images/baselayout/yellow/rooms.png" alt="Dashboard"><span>ROOM MANAGEMENT</span>
                        <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                           <!-- <li><a href="booking"><i class="fa fa-circle-o"></i>Booking</a></li> -->
                           <li><a href="room-management"><i class="fa fa-arrow-circle-right"></i>  <span>BOOKING ENTRY</span></a></li>
                    //       <li><a href="roomviewcalendar"><i class="fa fa-arrow-circle-right"></i>  <span>ROOM AVAILABILTY</span></a></li>
                           <li><a href="offline-management"><i class="fa fa-arrow-circle-right"></i> <span>OFFLINE ROOM MANAGEMENT</span></a></li>
                        </ul>
                     </li>
                  </s:if> --%>
                   <%-- <s:if test="%{#session.userRights.indexOf('INVENTORY BLOCK')!=-1}">
					<li class="treeview">
					<a href="#">
                     	<img src="images/baselayout/yellow/rooms.png" alt="Dashboard"><span>BLOCK MANAGEMENT</span>
                        <i class="fa fa-angle-left pull-right"></i>
                     </a>
                     <ul class="treeview-menu">
                     	<li><a href="bulk-block"><i class="fa fa-arrow-circle-right"></i> <span>BLOCK ROOMS</span></a></li>
                     </ul>
                     </li>
                  </s:if> --%>
                  <s:if test="%{#session.userRights.indexOf('REVENUE MANAGEMENT')!=-1}">
					<li class="treeview">
					<a href="#">
                     	<img src="images/baselayout/yellow/rooms.png" alt="Dashboard"><span>REVENUE MANAGEMENT</span>
                        <i class="fa fa-angle-left pull-right"></i>
                     </a>
                     <ul class="treeview-menu">
                     	<li><a href="revenue-management"><i class="fa fa-arrow-circle-right"></i> <span>MANAGE REVENUE</span></a></li>
                     	<li><a href="guest-status-report"><i class="fa fa-arrow-circle-right"></i>REPORT</a></li>
                     </ul>
                     </li>
                  </s:if>
                  <s:if test="%{#session.userRights.indexOf('ROOM MANAGEMENT')!=-1}">
                      <li class="treeview">
                        <a href="#">
                        <img src="images/baselayout/yellow/rooms.png"><span>GUEST SUPPORT</span>
                        <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                           <li><a href="guest-hotel-booking"><i class="fa fa-arrow-circle-right"></i>OFFLINE BOOKING</a></li>
                            <li><a href="booking-entry"><i class="fa fa-arrow-circle-right"></i>OTA BOOKING ENTRY</a></li>
                             <li><a href="booking-modification"><i class="fa fa-arrow-circle-right"></i>BOOKING MODIFICATION</a></li>
                             <li><a href="booking-cancel-detail"><i class="fa fa-arrow-circle-right"></i>NO SHOW & CANCELLATIONS</a></li>      
                             <li><a href="guest-status-report"><i class="fa fa-arrow-circle-right"></i>REPORT</a></li>      
                      </ul>                     
                  	</li>
                  </s:if>
                  <li class="treeview">
                     <s:if test="%{#session.userRights.indexOf('LOGS')!=-1}">
                        <a href="#">
                        <img src="images/baselayout/yellow/rooms.png"><span>LOGS</span>
                        <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                           <s:if test="%{#session.userRights.indexOf('REVENUE LOGS')!=-1}">
                              <li><a href="revenue-logs"><i class="fa fa-arrow-circle-right"></i>REVENUE LOGS</a></li>
                           </s:if>
                        </ul>
                     </s:if>
                  </li>
                  
                  <li class="treeview">
                     <s:if test="%{#session.userRights.indexOf('REPORTS')!=-1}">
                        <a href="#">
                        <img src="images/baselayout/yellow/reports.png" alt="Dashboard"><span>REPORTS</span>
                        <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                           <s:if test="%{#session.userRights.indexOf('HOTEL BOOKING REPORTS')!=-1}">
                              <li><a href="booking-reports"><i class="fa fa-arrow-circle-right"></i>  BOOKING REPORTS</a></li>
                           </s:if>
                           <s:if test="%{#session.userRights.indexOf('REVENUE REPORTS')!=-1}">
                              <li><a href="revenue-reports"><i class="fa fa-arrow-circle-right"></i>  REVENUE REPORTS</a></li>
                           </s:if>
                           <s:if test="%{#session.userRights.indexOf('USER REPORTS')!=-1}">
                              <li><a href="user-guest-reports"><i class="fa fa-arrow-circle-right"></i> SIGN-UP REPORTS</a></li>
                           </s:if>
                           <s:if test="%{#session.userRights.indexOf('CANCELLATION REPORTS')!=-1}">
                              <li><a href="booking-cancellation-reports"><i class="fa fa-arrow-circle-right"></i>CANCELLATION REPORTS</a></li>
                           </s:if>
                           <s:if test="%{#session.userRights.indexOf('RESORT BOOKING REPORTS')!=-1}">
                              <li><a href="resort-booking-reports"><i class="fa fa-arrow-circle-right"></i> HOTEL DIRECT REPORTS</a></li>
                           </s:if>
                           <s:if test="%{#session.userRights.indexOf('ULO BOOKING REPORTS')!=-1}">
                              <li><a href="ulo-booking-reports"><i class="fa fa-arrow-circle-right"></i> ULO BOOKING REPORTS</a></li>
                           </s:if>
                           <s:if test="%{#session.userRights.indexOf('ULO REPORTS')!=-1}">
                              <li><a href="ulo-reports"><i class="fa fa-arrow-circle-right"></i> ULO REPORTS</a></li>
                           </s:if>
                           <s:if test="%{#session.userRights.indexOf('ULO CONSOLIDATE REPORTS')!=-1}">
                              <li><a href="ulo-consolidate-reports"><i class="fa fa-arrow-circle-right"></i> ULO CONSOLIDATE REPORTS</a></li>
                           </s:if>
                           <s:if test="%{#session.userRights.indexOf('SUBSCRIBER MAIL REPORTS')!=-1}">
                              <li><a href="ulo-subscriber-mail-reports"><i class="fa fa-arrow-circle-right"></i> SUBSCRIBER USER REPORTS</a></li>
                           </s:if>
                        </ul>
                     </s:if>
                  </li>
                  <s:if test="%{#session.userRights.indexOf('MASTERS')!=-1}">
                     <li class="treeview">
                        <a href="#">
                        <img src="images/baselayout/yellow/master.png" alt="Dashboard"><span>MASTERS</span>
                        <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                           <s:if test="%{#session.userRights.indexOf('PROPERTY')!=-1}">
                              <li><a href="create-property"><i class="fa fa-arrow-circle-right"></i> PROPERTY</a></li>
                           </s:if>
                           <s:if test="%{#session.userRights.indexOf('LOCATION')!=-1}">
                              <li><a href="create-location"><i class="fa fa-arrow-circle-right"></i> LOCATION</a></li>
                           </s:if>
                           <s:if test="%{#session.userRights.indexOf('AREA')!=-1}">
                              <li><a href="create-area"><i class="fa fa-arrow-circle-right"></i> AREA</a></li>
                           </s:if>
                           <s:if test="%{#session.userRights.indexOf('RATEPLAN')!=-1}">
	                     		<li><a href="create-rate-plan"><i class="fa fa-arrow-circle-right"></i>RATE PLAN</a></li>
	                  		</s:if>
                        </ul>
                     </li>
                  </s:if>
                  <s:if test="%{#session.userRights.indexOf('USER MANAGEMENT')!=-1}">
                     <li class="treeview">
                        <a href="#">
                        <img src="images/baselayout/yellow/settings.png" alt="Dashboard"> <span>SETTING</span>
                        <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                           <li><a href="property-users"><i class="fa fa-arrow-circle-right"></i>  ADD USER</a></li>
                           <li><a href="manage-access"><i class="fa fa-arrow-circle-right"></i>  MANAGE ACCESS</a></li>
                        </ul>
                     </li>
                  </s:if>
                   <s:if test="%{#session.userRights.indexOf('OTA MANAGEMENT')!=-1}">
                     <li class="treeview">
                        <a href="#">
                        <img src="images/baselayout/yellow/settings.png" alt="Dashboard"> <span>OTA</span>
                        <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                           <li><a href="ota-hotels"><i class="fa fa-arrow-circle-right"></i>OTA HOTEL</a></li>
                           <li><a href="ota-hotel-details"><i class="fa fa-arrow-circle-right"></i>OTA HOTEL DETAILS</a></li>
                        </ul>
                     </li>
                  </s:if>
                  <%-- <s:if test="%{#session.userRights.indexOf('BOOKING CANCELLATION')!=-1}">
                     <li class="treeview">
                        <a href="#">
                        <img src="images/baselayout/yellow/cancel.png" alt="Dashboard"><span>CANCELLATION</span>
                        <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                           <li><a href="booking-cancellation"><i class="fa fa-arrow-circle-right"></i> UNBLOCK ROOMS</a></li>
                           <li><a href="booking-cancel-detail"><i class="fa fa-arrow-circle-right"></i> ROOM CANCELLATION</a></li>
                        </ul>
                     </li>
                  </s:if> --%>
                  <s:if test="%{#session.userRights.indexOf('SEO CONTENT')!=-1}">
                     <li class="treeview">
                        <a href="#">
                        <img src="images/baselayout/yellow/seo.png" alt="Dashboard"> <span>SEO</span>
                        <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                         <li><a href="location-content"><i class="fa fa-arrow-circle-right"></i> LOCATION CONTENT</a></li>
                           <li><a href="property-content"><i class="fa fa-arrow-circle-right"></i> PROPERTY CONTENT</a></li>
                           <li><a href="area-content"><i class="fa fa-arrow-circle-right"></i> AREA CONTENT</a></li>
                           <li><a href="blog-article-content"><i class="fa fa-arrow-circle-right"></i> BLOG ARTICLE CONTENT</a></li>
                           <li><a href="property-reviews"><i class="fa fa-arrow-circle-right"></i> PROPERTY REVIEWS</a></li>
                           <li><a href="guest-reviews"><i class="fa fa-arrow-circle-right"></i>GUEST REVIEWS</a></li>
                            <li><a href="user-cookies"><i class="fa fa-arrow-circle-right"></i>USER COOKIES</a></li>
                        </ul>
                     </li>
                  </s:if>
                  <s:if test="%{#session.userRights.indexOf('SEO CONTENT')!=-1}">
                     <li class="treeview">
                        <a href="#">
                        <img src="images/baselayout/yellow/blog.png" alt="Dashboard"><span>BLOG</span>
                        <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                           <li><a href="blog-category"><i class="fa fa-arrow-circle-right"></i> ADD CATEGORY</a></li>
                           <li><a href="blog-location"><i class="fa fa-arrow-circle-right"></i> </i>ADD LOCATION</a></li>
                           <li><a href="blog-article"><i class="fa fa-arrow-circle-right"></i> ADD BLOG ARTICLE</a></li>
                           <li><a href="blog-user-comment"><i class="fa fa-arrow-circle-right"></i> BLOG USER COMMENT</a></li>
                        </ul>
                     </li>
                  </s:if>
                  <%-- <s:if test="%{#session.userRights.indexOf('RESORT DIRECT')!=-1}">
                     <li class="header">Direct-Walkin</li>
                     <li><a href="reservation"><img src="images/baselayout/yellow/resort.png" alt="Dashboard"> <span>HOTEL DIRECT</span></a></li>
                        //<li><a href="room-management"><i class="fa fa-credit-card" style="color:#88ba41;"></i> <span>ROOM MANAGEMENT</span></a></li>	
                        //<li><a href="roomviewcalendar"><i class="fa fa-credit-card" style="color:#88ba41;"></i> <span>ROOM VIEW</span></a></li>
                        //<li><a href="bookingcalendar"><i class="fa fa-calendar text-chartreuse " style="color:#88ba41;"></i> <span>BOOKING CALENDAR</span></a></li>
                  </s:if> --%>
                  <s:if test="%{#session.userRights.indexOf('FINANCE MANAGEMENT')!=-1}">
                     <li class="header">Finance-Accounts</li>
                     <li class="treeview">
                        <a href="#">
<%--                         <i class="fa fa-inr" aria-hidden="true"></i><span>FINANCE MANAGEMENT</span> --%>
                         <img src="images/baselayout/yellow/rates.png" alt="Dashboard"><span>FINANCE MANAGEMENT</span>
                        <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                           <li><a href="dsfr-voucher"><i class="fa fa-arrow-circle-right"></i>B-JOIN FLAT DSFR</a></li>
<!--                            <li><a href="dsfr-revenue-tax-voucher"><i class="fa fa-arrow-circle-right"></i> TAXABLE DSFR VOUCHER</a></li> -->
                           	<li><a href="commission-voucher"><i class="fa fa-arrow-circle-right"></i> COMMISSION INVOICE</a></li>
                           	<li><a href="hotelpaymentupload"><i class="fa fa-arrow-circle-right"></i>PAYMENT UPLOAD</a></li>
                        	<li><a href="hotelpaymenthistory"><i class="fa fa-arrow-circle-right"></i>PAYMENT HISTORY</a></li>
                         	<li><a href="hotelbookingreports"><i class="fa fa-arrow-circle-right"></i>BOOKING REPORT</a></li>
                        </ul>
                     </li>
                  </s:if>
               </ul>
            </section>
            <!-- /.sidebar -->
         </aside>
         <tiles:insertAttribute name="body" />
         <footer class="main-footer">
            <div class="pull-right hidden-xs">
               <b>Version</b> 2.0
            </div>
            <strong>
               Copyright &copy; 2018 
               <a href='<s:property value="https://www.ulohotels.com"/>'>
                  <s:property value="%{companyName}"/>
               </a>
               .
            </strong>
            All rights reserved.
         </footer>
         <!-- Control Sidebar -->  <!-- /.control-sidebar -->
         <!-- Add the sidebar's background. This div must be placed
            immediately after the control sidebar -->
         <div class="control-sidebar-bg"></div>
      </div>
      <!-- ./wrapper -->
      <!-- jQuery 2.1.4 -->
      <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script> 
      <script src="plugins/select2/select2.js"></script> 
      <script src="plugins/select2/select2.full.js"></script> 
      <!-- jQuery UI 1.11.4 -->
      <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
      <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
      <!-- AdminLTE App -->
      <script src="dist/js/app.min.js"></script>
      <!-- AdminLTE for demo purposes -->
      <script src="dist/js/demo.js"></script>
      <script>
         $.widget.bridge('uibutton', $.ui.button);
      </script>
      <!-- Bootstrap 3.3.5 -->
      <script src="bootstrap/js/bootstrap.min.js"></script>
      <!-- Slimscroll -->
      <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
      <!-- FastClick -->
      <script src="plugins/fastclick/fastclick.min.js"></script>

      <!-- progress bar -->
      <script src="js1/ngprogress.js" type="text/javascript"></script>
      <!--<script src="js1/ngprogress-lite.mins.js" type="text/javascript"></script>-->
      <!-- DataTables -->
      <script src="plugins/datatables/jquery.dataTables.min.js"></script>
      <script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
      <script src="dist/js/pages/dashboard.js"></script>
    <script>
(function () {

	  var clockElement = document.getElementById( "todaytime" );

	  function updateClock ( clock ) {
	    clock.innerHTML = new Date().toLocaleTimeString();
	  }

	  setInterval(function () {
	      updateClock( clockElement );
	  }, 1000);

	}());
</script>
<script>
var d = new Date();
document.getElementById("todaydate").innerHTML = d.toDateString('yyyy-MM-dd');
</script>
<script>
    var myDate = new Date();
    var hrs = myDate.getHours();

    var greet;

    if (hrs < 12)
        greet = 'Hi Good Morning';
    else if (hrs >= 12 && hrs <= 17)
        greet = 'Hi Good Afternoon';
    else if (hrs >= 17 && hrs <= 24)
        greet = 'Hi Good Evening';

    document.getElementById('lblGreetings').innerHTML =
        '<b>' + greet + '</b>';
</script> 
<Script>

$(window).on('load', function() { // makes sure the whole site is loaded 
  $('#status').fadeOut(); // will first fade out the loading animation 
  $('#preloader').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website. 
  $('body').delay(350).css({'overflow':'visible'});
})
</Script>
</body>

</html>
