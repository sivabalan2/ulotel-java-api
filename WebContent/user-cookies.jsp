<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="s" uri="/struts-tags"%>

   <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            User-Cookies  
          </h1>
          <ol class="breadcrumb">
            <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">User Cookies</li>
          </ol>
        </section>
		
        <!-- Main content -->
        <section class="content">
        		<div class="box box-default color-palette-box">
       				<div class="box-header with-border">
						<h3 class="box-title">User Search</h3>
					</div>
					<div class="box-body row">
						<form name="searchValue">
							<div class="col-sm-3 col-md-3">
				           		<div class="form-group">
				                  <label>Start Date :</label>
				                  <div class="input-group date">
				                     <label class="input-group-addon btn" for="startDate">
				                     <span class="fa fa-calendar"></span>
				                     </label>   
				                     <input type="text"  id="startDate" class="form-control" name="startDate"  value="" ng-required="true" autocomplete="off">
				                  </div>
				               </div>
				            </div>
				            <div class="col-sm-3 col-md-3">
				               <div class="form-group">
				                  <label>End Date :</label>
				                  <div class="input-group date">
				                     <label class="input-group-addon btn" for="endDate">
				                     <span class="fa fa-calendar"></span>
				                     </label>   
				                     <input type="text" id="endDate" class="form-control" name="endDate" value="" ng-required="true" autocomplete="off"> 
				                  </div>
				               </div>
				            </div>
							
							<div class="form-group col-md-1">
								<input type="radio" name="filterDate" id="filterBookingDate" value="BookingDate" placeholder="Booking Date" checked>
								<label>Search Date</label>
							</div>
							<div class="form-group col-md-1">
								<input type="radio" name="filterDate" id="filterCheckInDate" value="CheckInDate" placeholder="Check In Date">
								<label>Check In Date</label>
							</div>
							<div class="form-group col-md-1">
								<input type="radio"  name="filterDate" id="filterCheckOutDate" value="CheckOutDate" placeholder="Check Out Date">
								<label>Check Out Date</label>
							</div>
								          	
				             <div class="col-sm-2 col-md-2" >
				             	<label>&nbsp;</label>
				                <button type="submit" ng-click="getSearchValue()"  class="btn btn-primary btngreeen pull-right btnpd"  >Search</button> 
				             </div>
			             </form>
					</div>
					<div class="box-body row">
						<div class="col-sm-2 col-md-2">
							<button class="btn btn-danger pull-right btn-block btn-sm" ng-csv="searchValue" filename="usersearch.csv"
							 csv-header="['Order','Search Value','Search Date','Search Check-In','Search Check-Out','User IP Address']" field-separator="," decimal-separator=".">Export to CSV</button>
	           			</div>
					</div>
        	</div>
        	<div class="row">
            	<div class="col-xs-12">
						<div class="box">     
							<div class="box-header">
				            <h3 class="box-title">Results</h3>
				            </div>
							<!-- /.box-header -->
							<div  class="box-body table-responsive no-padding" style="height:300px;"  ng-if="searchValue.length==0">
				            	<table class="table table-hover">
				               <tr>
				                	<td  colspan="8">No Records found
				                	</td>
				                </tr>
				                </table>
				            </div>
							<div class="box-body table-responsive no-padding"  style="height:300px;" ng-if="searchValue.length > 0"> 
								<table class="table table-hover">
									<tr>										
										<th>Order</th>
										<th>Search Area</th>
										<th>Search Date</th>
										<th>Search Check-In</th>
										<th>Search Check-Out</th>
										<th>User IP Address</th>
									</tr>
									<tr ng-repeat="sv in searchValue">										
										<td>{{sv.serialNo}}</td>
										<td>{{sv.searchArea}}</td>
										<td>{{sv.searchDate}}</td>
										<td>{{sv.searchStart}}</td>
										<td>{{sv.searchEnd}}</td>
										<td>{{sv.userIpAddress}}</td>
									</tr>
									<tr>
									</tr>
								</table>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>	
         	</div>
          
        </section><!-- /.content -->
        
      	
							
      
		
    

      </div><!-- /.content-wrapper -->
      
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="plugins/fullcalendar/fullcalendar.min.js"></script>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>
<!-- csv download script start -->
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.15/angular-sanitize.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ng-csv/0.3.6/ng-csv.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.0rc1/angular-route.min.js"></script>
<!-- csv download script end -->

	<script>


 
	
		var app = angular.module('myApp', ['ngProgress','ngSanitize', 'ngCsv']);
		app.controller('customersCtrl', function($scope, $http,$timeout,ngProgressFactory) {


			
	       	$timeout(function(){
	      	    // $scope.progressbar.complete();
	            $scope.show = true;
	            $("#pre-loader").css("display","none");
	        }, 2000);

			 
			
			$scope.unread = function() {
			//var notifiurl = "http://localhost:8085/collaborative-workflow/unreadnotifications.action";
			var notifiurl = "unreadnotifications.action";
			$http.get(notifiurl).success(function(response) {
				$scope.latestnoti = response.data;
			});
			};
			
			$scope.getUserCookies = function() {
				var url = "get-user-cookies";
				$http.get(url).success(function(response) {
					$scope.usercookies = response.data;
				});
			};
			
			 $scope.getPropertyList = function() {
			        var userId = $('#adminId').val();
		 			var url = "get-user-properties?userId="+userId;
		 			$http.get(url).success(function(response) {
		 				$scope.props = response.data;
		 			});
		 		};
		 		
            $scope.change = function() {     	   
		        var propertyId = $scope.id;	
	     	    var url = "change-user-property?propertyId="+propertyId;
	 			$http.get(url).success(function(response) {
	 				 window.location = '/ulopms/dashboard'; 
	 				//$scope.change = response.data;
	 			});
		 	};
		 		
		 		$("#startDate").datepicker({
		            dateFormat: 'MM d, yy',
		            onSelect: function (formattedDate) {
		                var date1 = $('#startDate').datepicker('getDate'); 
		                var date = new Date( Date.parse( date1 ) ); 
		                date.setDate( date.getDate()+1 );        
		                var newDate = date.toDateString(); 
		                newDate = new Date( Date.parse( newDate ) );   
		                $('#endDate').datepicker("option","minDate",newDate);
		                $timeout(function(){
		                	document.getElementById("startDate").style.borderColor = "LightGray";
		                });
		            }
		        });
		      
		 		
		      $("#endDate").datepicker({
		            dateFormat: 'MM d, yy',
		            onSelect: function (formattedDate) {
		                var date2 = $('#endDate').datepicker('getDate'); 
		                $timeout(function(){
		                	document.getElementById("endDate").style.borderColor = "LightGray";
		                });
		            }
		        });

		      
		 	 	$scope.getSearchValue = function() {
		 	 		var url;
		 	 		if($('#startDate').val()!="" && $('#endDate').val()!=""){
		 	 			var filterDateName="";
						if(document.getElementById('filterBookingDate').checked){
							filterDateName='BookingDate';
						}
						if(document.getElementById('filterCheckInDate').checked){
							filterDateName='CheckInDate';
						}
						if(document.getElementById('filterCheckOutDate').checked){
							filterDateName='CheckOutDate';
						}
						
		 	 			url = "get-search-value?strStartDate="+$('#startDate').val()+"&strEndDate="+$('#endDate').val()+"&filterSearch="+filterDateName;	
		 	 		}else{
		 	 			url = "get-search-value?filterSearch=currentDateName";
		 	 		}
		 	 		
					$http.get(url).success(function(response) {
						$scope.searchValue = response.data;
					});
				};
				
		    $scope.getPropertyList();
			
			$scope.getSearchValue();
// 			$scope.getUserCookies();
			
			
			
		});

	
		
		   
		        

		
	</script>
	
	 <script src="js/jquery-2.1.1.js" type="text/javascript"></script>
       