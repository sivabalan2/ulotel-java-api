<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
   pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://ckeditor.com" prefix="ckeditor" %>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- text json -->
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Hotel Details
         <!--<small>Subscription Expiry on {{property[0].subscription[0].expirydate}}</small>-->
      </h1>
 
   </section>
   <!-- Main content -->
      <section class="content">
      <div class="row" id="hotelProfile">
      <!-- Nav Tabs Begins -->
        <div class="col-xs-12">
            <div class="nav-tabs-custom">
               <ul class="nav nav-tabs" ng-repeat="uht in ulotelHotelTags">
                  <li  class="{{uht.hotelProfile}}"><a href="#tab-profile" data-toggle="tab">Hotel Profile</a></li>
                   <li class="{{uht.roomCategories}}"><a href="#tab-inventory" ng-disabled="{{uht.roomCategories == 'disable'}}" data-toggle="tab">Room Categories </a></li>  <!-- ng-disabled="true"  -->
                  <li class="{{uht.ratePlan}}"><a href="#tab-rate-plan" ng-disabled="{{uht.ratePlan == 'disable'}}" data-toggle="tab">Rate Plan </a></li>
                  <li class="{{uht.hotelAmenities}}"><a href="#tab-amenities" ng-disabled="{{uht.hotelAmenities == 'disable'}}" data-toggle="tab">Hotel Amenities</a></li>
                  <li class="{{uht.hotelPolicy}}"><a href="#tab-policy" ng-disabled="{{uht.hotelPolicy == 'disable'}}" data-toggle="tab">Hotel Policy</a></li>
                  <li class="{{uht.addOn}}"><a href="#tab-addon" ng-disabled="{{uht.addOn == 'disable'}}" data-toggle="tab">Add on</a></li>
                  <li class="{{uht.hotelLandmark}}"><a href="#tab-landmark" ng-disabled="{{uht.hotelLandmark == 'disable'}}" data-toggle="tab">Hotel Landmark</a></li>
				  <li class="{{uht.hotelImages}}"><a href="#tab-images" ng-disabled="{{uht.hotelImages == 'disable'}}" data-toggle="tab">Hotel Images</a></li>
                  <li class="{{uht.hotelStatus}}"><a href="#tab-status" ng-disabled="{{uht.hotelStatus == 'disable'}}" data-toggle="tab">Hotel Status</a></li>
               </ul>
               <div ng-if="ulotelHotelTags == null" style="color:green;text-align:center;">Please choose your property Location Category
               </div>
               <div class="tab-content">
               <!-- Hotel profile begins -->
                 <div class="tab-pane {{ulotelHotelTags[0].hotelProfile}}" id="tab-profile">
        <form name="propertyprofile">
                  <h4 class="page-headers">Hotel Info :</h4>
                  <div class="row">
                     <div class="col-md-3 col-sm-3">
                                 <div class="form-group">
                                   <label>Hotel Type  <span style="color:red">*</span></label>
                                    <select name="propertyTypeId" id="propertyTypeId"  ng-model="propertyTypeselect" class="form-control" >
                                       <option value="">select</option>
                                       <option ng-repeat="pt in propertytypes" value="{{pt.DT_RowId}}" ng-selected ="property[0].propertyTypeId == pt.DT_RowId">{{pt.propertyTypeName}}</option>
                                    </select> </div>
                              </div>
                     <div class="col-md-3 col-sm-3">
                                 <div class="form-group">
                                    <label for="address">Hotel Name <span style="color:red">*</span> </label>
                                    <input type="text" class="form-control" name="propertyName"  id="propertyName" value="{{property[0].propertyName}}" placeholder="Hotel Name" ng-model='property[0].propertyName' ng-pattern="/^[a-zA-Z0-9]/"  ng-required="true">
                                    <span ng-show="propertyprofile.propertyName.$error.pattern" style="color:red">Not a Valid Property Name!</span>
                                 </div>
                              </div>
                                <div class="col-md-3 col-sm-3">
                                 <div class="form-group">
                                   <label>Contact Type  <span style="color:red">*</span></label>
                                    <select name="contactType" id="contactType"  ng-model="contactType" class="form-control" >
                                       <option value="0">select</option>
                                      <option value="1">Mobile</option>
                                      <option value="2">Landline</option>
                                  </select> 
                                  </div>
                              </div>
                           
                                <div class="col-md-3 col-sm-3">
                                 <div class="form-group">
                                    <label>Hotel Contact <span style="color:red">*</span> </label>	                      			
                                    <input type="text" class="form-control" name="propertyContact" maxlength="10" id=propertyContact value="{{property[0].propertyContact}}" placeholder="Contact" ng-model='property[0].propertyContact' ng-pattern="/^[0-9]{10}$/" ng-required="true">
                                    <span ng-show="propertyprofile.propertyContact.$error.pattern" style="color:red">Not a Valid Property Contact!</span>
                                 </div>
                              </div>  
                               
                                <div class="col-md-12 col-sm-12">
                                 <div class="form-group">
                                    <label>Hotel Email (Email are seperated by ',')<span style="color:red">*</span> </label>	                      			
                                    <textarea rows="2" type="text" name="propertyEmail" id="propertyEmail" value="{{property[0].propertyEmail}}" ng-model="property[0].propertyEmail" class="form-control" ng-required="true"></textarea>
                                </div>
                              </div>  
                  </div>
                   <h4 class="page-headers">Contract Relationship Manager :</h4>
                  <div class="row">
                   <div class="col-md-4 col-sm-4">
                                 <div class="form-group">
                                    <label for="address"> Name <span style="color:red">*</span> </label>
                                    <input type="text" class="form-control" name="contractManagerName"  id="contractManagerName" value="{{property[0].contractManagerName}}" placeholder="Name" ng-model='property[0].contractManagerName' ng-pattern="/^[a-zA-Z0-9]/"  ng-required="true">
                                    <span ng-show="propertyprofile.contractManagerName.$error.pattern" style="color:red">Not a Valid Property Name!</span>
                                 </div>
                              </div>
                              <div class="col-md-4 col-sm-4">
                                 <div class="form-group">
                                    <label for="address"> Email <span style="color:red">*</span> </label>
                                    <input type="text" class="form-control" name="contractManagerEmail"  id="contractManagerEmail" value="{{property[0].contractManagerEmail}}" placeholder="Email" ng-model='property[0].contractManagerEmail' ng-pattern="/^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/"  ng-required="true">
                                    <span ng-show="propertyprofile.contractManagerEmail.$error.pattern" style="color:red">Not a Valid Property Name!</span>
                                 </div>
                              </div>
                              <div class="col-md-4 col-sm-4">
                                 <div class="form-group">
                                    <label for="address"> Mobile <span style="color:red">*</span> </label>
                                    <input type="text" class="form-control" name="contractManagerContact" maxlength="10" id="contractManagerContact"  value="{{property[0].contractManagerContact}}" placeholder="Mobile" ng-model='property[0].contractManagerContact' ng-pattern="/^[0-9]{10}$/"  ng-required="true">
                                    <span ng-show="propertyprofile.contractManagerContact.$error.pattern" style="color:red">Not a Valid Number!</span>
                                 </div>
                              </div>
                   
                    </div>
                       <h4 class="page-headers">Revenue Relationship Manager :</h4>
                                  <div class="row">
                   <div class="col-md-4 col-sm-4">
                                 <div class="form-group">
                                    <label for="address"> Name <span style="color:red">*</span> </label>
                                    <input type="text" class="form-control" name="revenueManagerName"  id="revenueManagerName" value="{{property[0].revenueManagerName}}" placeholder="Name" ng-model='property[0].revenueManagerName' ng-pattern="/^[a-zA-Z0-9]/"  ng-required="true">
                                    <span ng-show="propertyprofile.revenueManagerName.$error.pattern" style="color:red">Not a Valid Property Name!</span>
                                 </div>
                              </div>
                              <div class="col-md-4 col-sm-4">
                                 <div class="form-group">
                                    <label for="address"> Email <span style="color:red">*</span> </label>
                                    <input type="text" class="form-control" name="revenueManagerEmail"  id="revenueManagerEmail" value="{{property[0].revenueManagerEmail}}" placeholder="Email" ng-model='property[0].revenueManagerEmail' ng-pattern="/^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/" ng-required="true">
                                    <span ng-show="propertyprofile.revenueManagerEmail.$error.pattern" style="color:red">Not a Valid Property Name!</span>
                                 </div>
                              </div>
                              <div class="col-md-4 col-sm-4">
                                 <div class="form-group">
                                    <label for="address"> Mobile <span style="color:red">*</span> </label>
                                    <input type="text" class="form-control" name="revenueManagerContact" maxlength="10"  id="revenueManagerContact" value="{{property[0].revenueManagerContact}}" placeholder="Mobile" ng-model='property[0].revenueManagerContact' ng-pattern="/^[0-9]{10}$/"  ng-required="true">
                                    <span ng-show="propertyprofile.revenueManagerContact.$error.pattern" style="color:red">Not a Valid Number!</span>
                                 </div>
                              </div>
                   
                    </div>
                       <h4 class="page-headers">Guest Relationship Contact :</h4>
                  <div class="row">
                       <div class="col-md-12 col-sm-12">
                                 <div class="form-group">
                                    <label>Guest Relationship Email (Email are seperated by ',')<span style="color:red">*</span>  </label>	                      			
                                    <textarea rows="2" type="text" id="reservationManagerEmail" name="reservationManagerEmail" value="{{property[0].reservationManagerEmail}}" ng-model="property[0].reservationManagerEmail" class="form-control" ng-required="true"></textarea>
                                </div>
                              </div> 
                   
                    </div>
                       <h4 class="page-headers">Escalation Contact 1:</h4>
                      <div class="row">
                         <div class="col-md-3 col-sm-3">
                                 <div class="form-group">
                                    <label for="address"> Designation <span style="color:red">*</span> </label>
                                    <input type="text" class="form-control" name="escalationName"  id="escalationName" value="{{property[0].resortManagerDesignation}}" placeholder=" Name" ng-model='property[0].resortManagerDesignation' ng-pattern="/^[a-zA-Z0-9]/" ng-required="true" >
                                    <span ng-show="propertyprofile.escalationName.$error.pattern" style="color:red">Not a Valid Property Name!</span>
                                 </div>
                              </div>
                  <div class="col-md-3 col-sm-3">
                                 <div class="form-group">
                                    <label for="address"> Name <span style="color:red">*</span> </label>
                                    <input type="text" class="form-control" name="resortManagerName"  id="resortManagerName" value="{{property[0].resortManagerName}}" placeholder=" Name" ng-model='property[0].resortManagerName' ng-pattern="/^[a-zA-Z0-9]/"  ng-required="true">
                                    <span ng-show="propertyprofile.resortManagerName.$error.pattern" style="color:red">Not a Valid Property Name!</span>
                                 </div>
                              </div>
                              <div class="col-md-3 col-sm-3">
                                 <div class="form-group">
                                    <label for="address"> Email <span style="color:red">*</span> </label>
                                    <input type="text" class="form-control" name="resortManagerEmail"  id="resortManagerEmail" value="{{property[0].resortManagerEmail}}" placeholder="Email" ng-model='property[0].resortManagerEmail' ng-pattern="/^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/"  ng-required="true">
                                    <span ng-show="propertyprofile.resortManagerEmail.$error.pattern" style="color:red">Not a Valid Property Name!</span>
                                 </div>
                              </div>
                              <div class="col-md-3 col-sm-3">
                                 <div class="form-group">
                                    <label for="address"> Mobile <span style="color:red">*</span> </label>
                                    <input type="text" class="form-control" name="resortManagerContact" maxlength="10" id="resortManagerContact" value="{{property[0].resortManagerContact}}" placeholder="Mobile" ng-model='property[0].resortManagerContact' ng-pattern="/^[0-9]{10}$/"  ng-required="true">
                                    <span ng-show="propertyprofile.resortManagerContact.$error.pattern" style="color:red">Not a Valid Number!</span>
                                 </div>
                              </div>
                              <div class="col-md-12 col-sm-12">

                              <a class="btn-sm btn-warning pull-right" style="margin:1px; cursor:pointer" ng-click="hotelAddEscalationTwo();">Add Contact 2</a>
                              </div>
                           </div>
                             <div class="row" ng-show="property[0].resortManagerDesignation1 != '' || addEscalationTwo">
                             <div class="col-md-12 col-sm-12">
                             <h4>Escalation Contact 2:</h4>
                             </div>
                         <div class="col-md-3 col-sm-3">
                                 <div class="form-group">
                                    <label for="address"> Designation <span style="color:red">*</span> </label>
                                    <input type="text" class="form-control" name="escalationName1"  id="escalationName1" value="{{property[0].resortManagerDesignation1}}" placeholder="Designation"  ng-pattern="/^[a-zA-Z0-9]/" >
                                    <span ng-show="propertyprofile.escalationName1.$error.pattern" style="color:red">Not a Valid Property Name!</span>
                                 </div>
                              </div>
                  <div class="col-md-3 col-sm-3">
                                 <div class="form-group">
                                    <label for="address"> Name <span style="color:red">*</span> </label>
                                    <input type="text" class="form-control" name="resortManagerName1"  id="resortManagerName1" value="{{property[0].resortManagerName1}}" placeholder=" Name" ng-pattern="/^[a-zA-Z0-9]/"  ng-required="true">
                                    <span ng-show="propertyprofile.resortManagerName1.$error.pattern" style="color:red">Not a Valid Property Name!</span>
                                 </div>
                              </div>
                              <div class="col-md-3 col-sm-3">
                                 <div class="form-group">
                                    <label for="address"> Email <span style="color:red">*</span> </label>
                                    <input type="text" class="form-control" name="resortManagerEmail1"  id="resortManagerEmail1" value="{{property[0].resortManagerEmail1}}" placeholder="Email"  ng-pattern="/^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/"  ng-required="true">
                                    <span ng-show="propertyprofile.resortManagerEmail1.$error.pattern" style="color:red">Not a Valid Property Name!</span>
                                 </div>
                              </div>
                              <div class="col-md-3 col-sm-3">
                                 <div class="form-group">
                                    <label for="address"> Mobile <span style="color:red">*</span> </label>
                                    <input type="text" class="form-control" name="resortManagerContact1" maxlength="10" id="resortManagerContact1" value="{{property[0].resortManagerContact1}}" placeholder="Mobile"  ng-pattern="/^[0-9]{10}$/"  ng-required="true">
                                    <span ng-show="propertyprofile.resortManagerContact1.$error.pattern" style="color:red">Not a Valid Number!</span>
                                 </div>
                              </div>
                               <div class="col-md-12 col-sm-12">
                              <a class="btn-sm btn-danger pull-right" style="margin:1px; cursor:pointer" ng-click="removeEscalationThree();">Remove </a>
                              <a class="btn-sm btn-warning pull-right" style="margin:1px; cursor:pointer" ng-click="hotelAddEscalationThree();">Add Contact 3</a>
                              </div>
                           </div>
                             <div class="row" ng-show="property[0].resortManagerDesignation2 != '' || addEscalationThree">
                                <div class="col-md-12 col-sm-12">
                             <h4>Escalation Contact 3:</h4>
                             </div>
                         <div class="col-md-3 col-sm-3">
                                 <div class="form-group">
                                    <label for="address"> Designation <span style="color:red">*</span> </label>
                                    <input type="text" class="form-control" name="escalationName2"  id="escalationName2" value="{{property[0].resortManagerDesignation2}}" placeholder="Designation"  ng-pattern="/^[a-zA-Z0-9]/" >
                                    <span ng-show="propertyprofile.escalationName2.$error.pattern" style="color:red">Not a Valid Property Name!</span>
                                 </div>
                              </div>
                  <div class="col-md-3 col-sm-3">
                                 <div class="form-group">
                                    <label for="address"> Name <span style="color:red">*</span> </label>
                                    <input type="text" class="form-control" name="resortManagerName2"  id="resortManagerName2" value="{{property[0].resortManagerName2}}" placeholder=" Name"  ng-pattern="/^[a-zA-Z0-9]/"  ng-required="true">
                                    <span ng-show="propertyprofile.resortManagerName2.$error.pattern" style="color:red">Not a Valid Property Name!</span>
                                 </div>
                              </div>
                              <div class="col-md-3 col-sm-3">
                                 <div class="form-group">
                                    <label for="address"> Email <span style="color:red">*</span> </label>
                                    <input type="text" class="form-control" name="resortManagerEmail2"  id="resortManagerEmail2" value="{{property[0].resortManagerEmail2}}" placeholder="Email"  ng-pattern="/^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/"  ng-required="true">
                                    <span ng-show="propertyprofile.resortManagerEmail2.$error.pattern" style="color:red">Not a Valid Property Name!</span>
                                 </div>
                              </div>
                              <div class="col-md-3 col-sm-3">
                                 <div class="form-group">
                                    <label for="address"> Mobile <span style="color:red">*</span> </label>
                                    <input type="text" class="form-control" name="resortManagerContact2" maxlength="10" id="resortManagerContact2" value="{{property[0].resortManagerContact2}}" placeholder="Mobile"  ng-pattern="/^[0-9]{10}$/"  ng-required="true">
                                    <span ng-show="propertyprofile.resortManagerContact2.$error.pattern" style="color:red">Not a Valid Number!</span>
                                 </div>
                              </div>
                               <div class="col-md-12 col-sm-12">
                              <a class="btn-sm btn-danger pull-right" style="margin:1px; cursor:pointer" ng-click="removeEscalationFour();">Remove </a>
                              <!-- <a class="btn-sm btn-warning pull-right" style="margin:1px; cursor:pointer" ng-click="hotelAddEscalationFour();">Add Contact 4</a> -->
                              </div>
                           </div>
                             <%-- <div class="row" ng-show="property[0].resortManagerDesignation3 != '' || addEscalationFour">
                                <div class="col-md-12 col-sm-12">
                             <h4>Escalation Contact 4:</h4>
                             </div>
                         <div class="col-md-3 col-sm-3">
                                 <div class="form-group">
                                    <label for="address"> Designation <span style="color:red">*</span> </label>
                                    <input type="text" class="form-control" name="escalationName3"  id="escalationName3" value="{{property[0].resortManagerDesignation3}}" placeholder="Designation" ng-pattern="/^[a-zA-Z0-9]/" >
                                    <span ng-show="propertyprofile.escalationName3.$error.pattern" style="color:red">Not a Valid Property Name!</span>
                                 </div>
                              </div>
                  <div class="col-md-3 col-sm-3">
                                 <div class="form-group">
                                    <label for="address"> Name <span style="color:red">*</span> </label>
                                    <input type="text" class="form-control" name="resortManagerName3"  id="resortManagerName3" value="{{property[0].resortManagerName3}}" placeholder=" Name" ng-pattern="/^[a-zA-Z0-9]/"  ng-required="true">
                                    <span ng-show="propertyprofile.resortManagerName3.$error.pattern" style="color:red">Not a Valid Property Name!</span>
                                 </div>
                              </div>
                              <div class="col-md-3 col-sm-3">
                                 <div class="form-group">
                                    <label for="address"> Email <span style="color:red">*</span> </label>
                                    <input type="text" class="form-control" name="resortManagerEmail3"  id="resortManagerEmail3" value="{{property[0].resortManagerEmail3}}" placeholder="Email" ng-pattern="/^[a-zA-Z0-9]/"  ng-required="true">
                                    <span ng-show="propertyprofile.resortManagerEmail3.$error.pattern" style="color:red">Not a Valid Property Name!</span>
                                 </div>
                              </div>
                              <div class="col-md-3 col-sm-3">
                                 <div class="form-group">
                                    <label for="address"> Mobile <span style="color:red">*</span> </label>
                                    <input type="text" class="form-control" name="resortManagerContact3"  id="resortManagerContact3" value="{{property[0].resortManagerContact3}}" placeholder="Mobile" ng-pattern="/^[a-zA-Z0-9]/"  ng-required="true">
                                    <span ng-show="propertyprofile.resortManagerContact3.$error.pattern" style="color:red">Not a Valid Property Name!</span>
                                 </div>
                              </div>
                               <div class="col-md-12 col-sm-12">
                              <a class="btn-sm btn-danger pull-right" style="margin:1px;" ng-click="removeEscalationFive();">Remove </a>
                             
                              </div>
                           </div> --%>
                 <h4 class="page-headers">Other Hotel Info:</h4>
                <div class="row">
                              <div class="col-md-3 col-sm-3">
                                 <div class="form-group">
                                    <label for="countryCode">Country <span style="color:red">*</span></label>
                                    <select name="countryCode" id="countryCode" ng-model="property[0].countryCode" value="{{property[0].countryCode}}" class="form-control" ng-required="true">
                                       <option value="select">select</option>
                                       <option ng-repeat="c in countries" value="{{c.countryCode}}"  ng-selected ="property[0].countryCode == c.countryCode">{{c.countryName}}</option>
                                    </select>
                                 </div>
                              </div>
                              <div class="col-md-3 col-sm-4">
                                 <div class="form-group">
                                    <label>Address 1 <span style="color:red">*</span></label>	                      			
                                    <input type="text" class="form-control" name="address1"  id="address1" value="{{property[0].address1}}" placeholder="Address 1" ng-model='property[0].address1' ng-pattern="/^[a-zA-Z0-9]+(?:\,)*/" ng-required="true">
                                    <span ng-show="propertyprofile.address1.$error.pattern" style="color:red">Not a Valid Alphabet!</span>
                                 </div>
                              </div>
                              <div class="col-md-3 col-sm-4">
                                 <div class="form-group">
                                    <label>Address 2 <span style="color:red">*</span></label>                   			
                                    <input type="text" class="form-control" name="address2"  id="address2" value="{{property[0].address2}}" placeholder="Address 2" ng-model='property[0].address2' ng-pattern="/^[a-zA-Z0-9]+(?:\,)*/" ng-required="true">
                                    <span ng-show="propertyprofile.address2.$error.pattern" style="color:red">Not a Valid Alphabet!</span>
                                 </div>
                              </div>
                              <div class="col-md-3 col-sm-4">
                                 <div class="form-group">
                                    <label>City <span style="color:red">*</span></label>	                      			
                                    <input type="text" class="form-control" name="city"  id="city" value="{{property[0].city}}" placeholder="City" ng-model='property[0].city' ng-pattern="/^[a-zA-Z]/" ng-required="true">
                                    <span ng-show="propertyprofile.city.$error.pattern" style="color:red">Not a Valid Alphabet!</span>
                                 </div>
                              </div>
                              <div class="col-md-3 col-sm-4">
                                 <div class="form-group">
                                    <label>State <span style="color:red">*</span></label>
                                    <select name="stateCode" id="stateCode" ng-model="property[0].stateCode" value="{{property[0].stateCode}}" class="form-control" ng-required="true">
                                       <option value="select">select</option>
                                       <option ng-repeat="s in states" value="{{s.stateCode}}" ng-selected ="property[0].stateCode == s.stateCode">{{s.stateName}}</option>
                                    </select>
                                 </div>
                              </div>
                              <div class="col-md-3 col-sm-4">
                                 <div class="form-group">
                                    <label>Zip Code <span style="color:red">*</span> </label>	                      			
                                    <input type="text" class="form-control" name="zipCode"  id="zipCode" value="{{property[0].zipCode}}" placeholder="Zip Code" ng-model='property[0].zipCode' ng-pattern="/^[0-9]/" ng-required="true">
                                    <span ng-show="propertyprofile.zipCode.$error.pattern" style="color:red">Not a Valid Numeric!</span>
                                 </div>
                              </div>
                              <div class="col-md-3 col-sm-4">
                                 <div class="form-group">
                                    <label>Latitude <span style="color:red">*</span> </label>	                      			
                                    <input type="text" class="form-control" name="latitude"  id="latitude" value="{{property[0].latitude}}" placeholder="Latitude" ng-model='property[0].latitude' ng-pattern="/^[0-9]/" ng-required="true" >
                                    <span ng-show="propertyprofile.latitude.$error.pattern" style="color:red">Not a Valid Numeric!</span>
                                 </div>
                              </div>
                              <div class="col-md-3 col-sm-4">
                                 <div class="form-group">
                                    <label>Longitude <span style="color:red">*</span></label>	                      			
                                    <input type="text" class="form-control" name="longitude"  id="longitude" value="{{property[0].longitude}}" placeholder="Longitude" ng-model='property[0].longitude' ng-pattern="/^[0-9]/" ng-required="true">
                                    <span ng-show="propertyprofile.longitude.$error.pattern" style="color:red">Not a Valid Numeric!</span>
                                 </div>
                              </div>
                                      <div class="col-md-12 col-sm-12">
                                 <div class="form-group">
                                    <label>Route Map <span style="color:red">*</span><i class="fa fa-info-circle" data-toggle="tooltip" tooltip="This content is used for Route Map SMS" tooltip-placement="right" aria-hidden="true"></i></label>	
                                    <textarea row="2" type="text "name="routeMap" id="routeMap" value="{{property[0].routeMap}}" ng-model="property[0].routeMap" class="form-control" ng-required="true"></textarea>                      			
                              </div>
                              </div>
                                  <div class="col-md-3 col-sm-3">
                                 <div class="form-group">
                                    <label>Hotel GST No <span style="color:red">*</span></label>	                      			
                                    <input type="text" class="form-control" name="propertyGstNo"  id="propertyGstNo" value="{{property[0].propertyGstNo}}" placeholder="hotelGST" ng-model='property[0].propertyGstNo' ng-pattern="/^[a-zA-Z0-9]+(?:\,)*/"  ng-required="true">
                                    <span ng-show="propertyprofile.propertyGstNo.$error.pattern" style="color:red">Not a Valid Numeric!</span>
                                 </div>
                              </div>
                                     <div class="col-md-3 col-sm-3">
                                 <div class="form-group">
                                    <label>Google Review Link <span style="color:red">*</span></label>	                      			
                                    <input type="text" class="form-control" name="propertyReviewLink"  id="propertyReviewLink" value="{{property[0].propertyReviewLink}}" placeholder="Google Review Link" ng-model='property[0].propertyReviewLink' ng-pattern="/^[a-zA-Z0-9]+(?:\,)*/"  ng-required="true">
                                    <span ng-show="propertyprofile.propertyReviewLink.$error.pattern" style="color:red">Not a Valid Numeric!</span>
                                 </div>
                              </div>
                             <%--  <div class="col-md-3 col-sm-3">
				                     <div class="form-group">
					                     <label>Property Commission<span style="color:red" >*</span> <i class="fa fa-info-circle" data-toggle="tooltip" tooltip="If this property is taken by B-Join Flat Model put in commission %" tooltip-placement="right" aria-hidden="true"></i></label>
					                    <input type="text" class="form-control" name="propertyCommission"  id="propertyCommission" value="{{property[0].propertyCommission | number}}" placeholder="property commission " ng-model='property[0].propertyCommission' ng-pattern="/^[0-9]/"  ng-required="true">
                                    <span ng-show="propertyprofile.propertyCommission.$error.pattern" style="color:red">Not a Valid Numeric!</span>
                                 </div>
			                     </div> --%>
                                     <div class="col-md-3 col-sm-3">
                                 <div class="form-group">
                                    <label>Tripadvisor Review Link <span style="color:red">*</span></label>	                      			
                                    <input type="text" class="form-control" name="tripAdvisorLink"  id="tripAdvisorLink" value="{{property[0].tripadvisorReviewLink}}" placeholder="Tripadvisior Review Link " ng-model='property[0].tripadvisorReviewLink' ng-pattern="/^[a-zA-Z0-9]+(?:\,)*/"  ng-required="true">
                                    <span ng-show="propertyprofile.tripAdvisorLink.$error.pattern" style="color:red">Not a Valid Numeric!</span>
                                 </div>
                              </div>
              <div class="col-md-3 col-sm-3">
				                     <div class="form-group">
					                     <label>Pay At Hotel Status<span style="color:red">*</span> </label>
					                     <select name="payAtHotelStatus" id="payAtHotelStatus"   class="form-control" ng-required="true">
						                     <option value="true"  ng-selected ="property[0].payAtHotelStatus == 'true'">Active</option>
						                     <option value="false" ng-selected ="property[0].payAtHotelStatus == 'false'">InActive</option>
					                     </select>
				                     </div>
			                     </div> 
                           </div>
                           
           
               <h4 class="page-headers">Hotel Tag: <span id="propertyTagAlert" ></span></h4>
               
               <div class="row"> 
                <div class="col-md-3" ng-repeat="pt in propertyTags">
               <div class="checkbox">
                      <label> 
                        <input type="checkbox" name="tagCheckbox" ng-disabled="!pt.status && checkedb()" class="selectTwo" ng-model="pt.status" value="{{pt.tagId}}"> {{pt.tagName}}
                      </label>
                    </div>
               </div>
               <div class="col-md-12">
                <div id ="propertyalert"></div>
               </div>
                  
                <div class="col-md-12">
			                        <button class="btn btn-primary pull-right" ng-click="editProperty()" ng-disabled="propertyprofile.$invalid">Save</button>
			                        </div>
               </div>
               
                 </form>
                 </div>
               <!-- Hotel profile ends -->
               <!-- Hotel room categories Begins -->
                 <div class="tab-pane {{ulotelHotelTags[0].roomCategories}}" id="tab-inventory">
             <h4 class="page-headers">Add Room Categories</h4>
               <form name="propertyInventory" id="propertyInventory">
               <div class="row">
                   								<div class="form-group col-md-3">
													<label >Contract Type</label>
											         <select name="propertyContractType" id="propertyContractType"  ng-model="propertyContractType"  class="form-control" >
				                                       <option value="">Select</option>
				                                      <option ng-repeat="ct in contractType" value="{{ct.contractTypeId}}">{{ct.contractTypeName}}</option>
				                                  </select> 
											 </div>
											  	<div class="form-group col-md-3">
													<label>Category  Name</label>
													<input type="text" class="form-control"  name="accommodationType"  id="accommodationType"  placeholder="Category  Name" ng-model='accommodationType' ng-pattern="^/^[A-Za-z\d\s]+$/" ng-required="true" maxlength="50">
												    <span ng-show="propertyInventory.accommodationType.$error.pattern" style="color:red">Not a Room Type!</span>
											    </div>
												<div class="form-group col-md-3">
													<label for="no_of_units">No of Rooms </label>
													<input type="text" class="form-control"  name="noOfUnits"  id="noOfUnits"  placeholder="No of Units" ng-model='noOfUnits' ng-pattern="/^[0-9]/" ng-required="true">
												    <span ng-show="propertyInventory.noOfUnits.$error.pattern" style="color:red">Not a Valid Number!</span>
												</div>
												<div class="form-group col-md-3">
													<label for="min_occupancy">Minimum Occupancy</label>
													<input type="text" class="form-control"  name="minOccupancy"  id="minOccupancy"  placeholder="Minimum Occupancy" ng-model='minOccupancy' ng-pattern="/^[0-9]/" ng-required="true">
												    <span ng-show="propertyInventory.minOccupancy.$error.pattern" style="color:red">Not a Valid Occupancy !</span>
												</div>
												<div class="form-group col-md-3">
													<label for="max_occupancy">Maximum Occupancy</label>
													<input type="text" class="form-control"  name="maxOccupancy"  id="maxOccupancy"  placeholder="Maximum Occupancy" ng-model='maxOccupancy' ng-pattern="/^[0-9]/" ng-required="true">
												    <span ng-show="propertyInventory.maxOccupancy.$error.pattern" style="color:red">Not a Valid Occupancy !</span>
												</div>
												<div class="form-group col-md-3">
													<label for="no_of_adults">No of Adults (Max)</label>
													<input type="text" class="form-control"  name="noOfAdults"  id="noOfAdults"  placeholder="No Of Adults" ng-model='noOfAdults' ng-pattern="/^[0-9]/" ng-required="true">
												    <span ng-show="propertyInventory.noOfAdults.$error.pattern" style="color:red">Enter the Numeric Value!</span>
												</div>
												<div class="form-group col-md-3">
													<label for="no_of_child">No of Child (Max)</label>
													<input type="text" class="form-control"  name="noOfChild"  id="noOfChild"  placeholder="No Of Child" ng-model='noOfChild' ng-pattern="/^[0-9]/" ng-required="true">
												    <span ng-show="propertyInventory.noOfChild.$error.pattern" style="color:red">Enter the numeric value!</span>
												</div>
										<div class="form-group col-md-12">
										<div id="addAccommodationsuccess"></div>
												<button ng-click="addAccommodation()" ng-disabled="propertyInventory.$invalid" class="btn btn-primary pull-right">{{addroombtn}}</button>
											
								 	</div>
												
               </div>
               </form>
              <h4 class="page-headers">Room Categories</h4>
              <div class="pdlist">
               <div class="panel-group" id="accordionLays">
               <div class="panel panel-default" ng-repeat="acc in accommodations track by $index">
						            <div class="panel-heading" >
						                <h4 class="panel-title">
						                    <a data-toggle="collapse" data-parent="#accordionLays" href="#collapse{{acc.accommodationId}}" ng-click="getAccommodation(acc.accommodationId);getOpen(acc.accommodationId);"><span class="glyphicon glyphicon-chevron-down"></span>{{acc.accommodationType}} - {{acc.propertyContractType}}</a>
						                </h4>
						            </div>
						            <div  id="collapse{{acc.accommodationId}}" class="panel-collapse collapse">
							                   <form name="editaccom{{acc.accommodationId}}" id="editaccom{{acc.accommodationId}}">
							                <div class="panel-body" id="editaccom1">
							            
							                <div class="form-group col-md-3">
													<label >Contract Type</label>
                                  			<select name="editContractType" id="editContractType{{$index}}"  class="form-control" >
                           			            <option value="" ng-selected="acc.propertyContractTypeId == 0">Select Value</option>
                           			            <option ng-repeat="ct in contractType" value="{{ct.contractTypeId}}" ng-selected="ct.contractTypeId == acc.propertyContractTypeId">{{ct.contractTypeName}}</option>
                              		      </select>
											 </div>
											  	<div class="form-group col-md-3">
													<label >Category  Name</label>
													<input type="text" class="form-control"  name="editAccommodationType"  ng-model="acc.accommodationType" id="editAccommodationType{{$index}}" value="{{acc.accommodationType}}" placeholder="Category  Name"  ng-pattern="^/^[A-Za-z\d\s]+$/" ng-required="true" maxlength="50">
												    <span ng-show="editaccom.editAccommodationType.$error.pattern" style="color:red">Not a Room Type!</span>
											    </div>
												<div class="form-group col-md-3">
													<label for="no_of_units">No of Rooms </label>
													<input type="text" class="form-control"  name="editNoOfUnits"  ng-model="acc.noOfUnits" id="editNoOfUnits{{$index}}" value="{{acc.noOfUnits}}" placeholder="No of Units"  ng-pattern="/^[0-9]/" ng-required="true">
												    <span ng-show="editaccom.editNoOfUnits.$error.pattern" style="color:red">Not a Valid Number!</span>
												</div>
												<div class="form-group col-md-3">
													<label for="min_occupancy">Minimum Occupancy</label>
													<input type="text" class="form-control"  name="editMinOccupancy"  ng-model="acc.minOccupancy" id="editMinOccupancy{{$index}}" value="{{acc.minOccupancy}}" placeholder="Minimum Occupancy"  ng-pattern="/^[0-9]/" ng-required="true">
												    <span ng-show="editaccom.editMinOccupancy.$error.pattern" style="color:red">Not a Valid Occupancy !</span>
												</div>
												<div class="form-group col-md-3">
													<label for="max_occupancy">Maximum Occupancy</label>
													<input type="text" class="form-control"  name="editMaxOccupancy" ng-model="acc.maxOccupancy"  id="editMaxOccupancy{{$index}}" value="{{acc.maxOccupancy}}"  placeholder="Maximum Occupancy"  ng-pattern="/^[0-9]/" ng-required="true">
												    <span ng-show="editaccom.editMaxOccupancy.$error.pattern" style="color:red">Not a Valid Occupancy !</span>
												</div>
												<div class="form-group col-md-3">
													<label for="no_of_adults">No of Adults (Max)</label>
													<input type="text" class="form-control"  name="editNoOfAdults" ng-model="acc.noOfAdults"  id="editNoOfAdults{{$index}}" value="{{acc.noOfAdults}}" placeholder="No Of Adults"  ng-pattern="/^[0-9]/" ng-required="true">
												    <span ng-show="editaccom.editNoOfAdults.$error.pattern" style="color:red">Enter the Numeric Value!</span>
												</div>
												<div class="form-group col-md-3">
													<label for="no_of_child">No of Child (Max)</label>
													<input type="text" class="form-control"  name="editNoOfChild" ng-model="acc.noOfChild"  id="editNoOfChild{{$index}}" value="{{acc.noOfChild}}" placeholder="No Of Child" ng-pattern="/^[0-9]/" ng-required="true">
												    <span ng-show="editaccom.editNoOfChild.$error.pattern" style="color:red">Enter the numeric value!</span>
												</div>
							             
											
												</div>
												<div class="col-md-12">
												<div id="accommodationsuccess{{acc.accommodationId}}"></div>
												</div>
												<div class="modal-footer">
												
													<button ng-click="deleteAccommodation(acc.accommodationId)" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i> Remove</button>
													<button type="submit" ng-click="editAccommodation(acc.accommodationId,$index)" ng-disabled="editaccom{{acc.accommodationId}}.$invalid" class="btn btn-primary"><i class="fa fa-edit" aria-hidden="true"></i> {{updateroombtn}}</button>
												</div>
												   </form>
												   
							                </div>
						        </div>
               </div>
						        <section>
                           <h4 class="page-header"></h4>
                           <div class="row fontawesome-icon-list">
                           <div id="roomLength"></div>
                              <div class="col-md-3 col-sm-4"></div>
                              <div class="col-md-3 col-sm-4"></div>
                              <div class="col-md-3 col-sm-4"></div>
                              <div class="col-md-3 col-sm-4">
                                 <button type="submit" ng-disabled="accommodationLegth" class="btn btn-primary btnnext pull-right" ng-click="getEnableAccommodation();">Continue</button>
                              </div>
                           </div>
                        </section>
              </div>
              
               </div>
               <!-- Hotel room categories Ends  -->
               <!-- Hotel Rate Plan Begins -->
                <div class="tab-pane {{ulotelHotelTags[0].ratePlan}}" id="tab-rate-plan">
                         <h4 class="page-headers">Room Categories</h4>
              <div class="pdlist panel-group" id="accordion">
             <div class="panel panel-default" ng-if="property[0].propertyLocationTypeId == 1" ng-repeat="acc in accommodations">
						            <div class="panel-heading">
						                <h4 class="panel-title">
						                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseNew{{acc.accommodationId}}" ><span class="glyphicon glyphicon-chevron-down"></span>{{acc.accommodationType}}-{{acc.propertyContractType}} - Metro</a>
						                </h4>
						            </div>
						            <div id="collapseNew{{acc.accommodationId}}" class="panel-collapse collapse">
						            
							              <div class="panel-body">
							              <form name="bjointNetrateMetroForm">
							              <div class="bjointNetrateMetroSetup" ng-if="acc.propertyContractTypeId == 7 ">
												
												<h4>B-Join Net Rate</h4>
										       <div class="form-group col-md-3">
													<label >Net Rate</label>
													<input type="text" class="form-control"  name="bJoinMetroNet"  id="bJoinMetroNet{{acc.accommodationId}}" value="" placeholder="Net Rate" ng-model='acc.netRate' ng-required="true" maxlength="50" ng-pattern="/^[0-9]/">
												      <span ng-show="bjointNetrateMetroForm.bJoinMetroNet.$error.pattern" style="color:red">Not a Valid Number!</span>
												</div>
												<div class="form-group col-md-3">
                                                    <label >Select Tax</label>
                                              		  <select name="bJoinMetroTaxType" id="bJoinMetroTaxType{{acc.accommodationId}}"  ng-change="changeBJoinMetroTaxType(acc.accommodationId)" ng-model="bJoinMetroTaxType" class="form-control">
                                    						 <option value="" ng-selected="acc.taxType == ''">Select</option>
                                     						  <option value="1" ng-selected="acc.taxType == 1">Inclusive Tax</option>
                                     						 <option value="2" ng-selected="acc.taxType == 2">Exclusive Tax</option>
                                                      </select>
                                                </div>
												<div class="form-group col-md-3">
													<label >Tariff</label>
													<input type="text" class="form-control"  name="bJoinMetroTariff"  id="bJoinMetroTariff{{acc.accommodationId}}" value="{{acc.tariff | number}}"  placeholder="Tariff Rate" ng-model='acc.tariff' ng-required="true" maxlength="50" disabled>
												</div>
												  <div class="form-group col-md-3">
													<label >Tax</label>
													<input type="text" class="form-control"  name="bJoinMetroTax"  id="bJoinMetroTax{{acc.accommodationId}}" value="{{acc.tax | number}}" placeholder="Tax Amount" ng-model='acc.tax' ng-required="true" maxlength="50" disabled>
												</div>
												  <div class="form-group col-md-3">
													<label >Selling Rate<span style="color:red">(without tax)</span></label>
													<input type="text" class="form-control"  name="bJoinMetroSelling"  id="bJoinMetroSelling{{acc.accommodationId}}" value="{{acc.sellingRate | number}}" placeholder="Selling Rate" ng-model='acc.sellingRate'  ng-required="true" maxlength="50" disabled>
												</div>
												  <div class="form-group col-md-3">
													<label >Extra Adult</label>
													<input type="text" class="form-control"  name="bJoinMetroExtraAdult"  id="bJoinMetroExtraAdult{{acc.accommodationId}}" ng-keyup="getSellingAdultRate(acc.extraAdult,acc.accommodationId)" placeholder="Extra Adult" ng-model='acc.extraAdult' ng-required="true" maxlength="50" ng-pattern="/^[0-9]/">
												     <span ng-show="bjointNetrateMetroForm.bJoinMetroExtraAdult.$error.pattern" style="color:red">Not a Valid Number!</span>
												</div>
												  <div class="form-group col-md-3">
													<label >Extra Child</label>
													<input type="text" class="form-control"  name="bJoinMetroExtraChild"  id="bJoinMetroExtraChild{{acc.accommodationId}}" ng-keyup="getSellingChildRate(acc.extraChild,acc.accommodationId)" placeholder="Extra Child" ng-model='acc.extraChild'  ng-required="true" maxlength="50" ng-pattern="/^[0-9]/">
											         <span ng-show="bjointNetrateMetroForm.bJoinMetroExtraChild.$error.pattern" style="color:red">Not a Valid Number!</span>
												</div>
												<div class="form-group col-md-3">
													<label >Selling Adult Rate</label>
													<input type="text" class="form-control"  name="sellingAdult"  id="sellingAdult{{acc.accommodationId}}" ng-model="acc.sellingAdult" readonly>
												</div>
												  <div class="form-group col-md-3">
													<label >Extra Child Rate</label>
													<input type="text" class="form-control"  name="sellingChild"  id="sellingChild{{acc.accommodationId}}" ng-model="acc.sellingChild" readonly>
												</div>
												 <div class="col-md-12 modal-footer">
												<div id="accommodationsuccessRate{{acc.accommodationId}}"></div>
													<!-- <button ng-click="deleteAccommodation(acc.accommodationId)" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i> Remove</button> -->
													<button ng-click="editAccommodationRate(acc.accommodationId,acc.propertyContractTypeId,property[0].propertyLocationTypeId)" ng-disabled="bjointNetrateMetroForm.$invalid" class="btn btn-primary"><i class="fa fa-edit" aria-hidden="true"></i> {{updateroombtn}}</button>
												</div>
												</div>
												</form>
												<form name="bjointFlatrateMetroForm">
												 <div class="bjointFlatrateMertroSetup" ng-if="acc.propertyContractTypeId == 9">   
										      <h4>B-Join Flat Rate </h4>
										       <div class="form-group col-md-3">
													<label >Base Rate </label>
													<input type="text" class="form-control"  name="bJoinFlatMetroBase"  id="bJoinFlatMetroBase{{acc.accommodationId}}" value="{{acc.baseAmount | number}}" placeholder="Base Rate" ng-model='acc.baseAmount' ng-required="true" maxlength="50" ng-pattern="/^[0-9]/">
												   <span ng-show="bjointFlatrateMetroForm.bJoinFlatMetroBase.$error.pattern" style="color:red">Not a Valid Number!</span>
												</div>
												<div class="form-group col-md-3">
                                                    <label >Select Tax</label>
                                              		      <select name="bJoinFlatMetroTaxType" id="bJoinFlatMetroTaxType{{acc.accommodationId}}" ng-change="changeBJoinFlatMetroTaxType(acc.accommodationId)" ng-model="bJoinFlatMetroTaxType" class="form-control" >
                                    						 <option value="" ng-selected="acc.taxType == ''">Select</option>
                                     						 <option value="1" ng-selected="acc.taxType == 1">Inclusive Tax</option>
                                     						 <option value="2" ng-selected="acc.taxType == 2">Exclusive Tax</option>
                                                      	  </select>
                                          		</div>
												   <div class="form-group col-md-3">
													<label >Tariff</label>
													<input type="text" class="form-control"  name="bJoinFlatMetroTariff"  id="bJoinFlatMetroTariff{{acc.accommodationId}}" value="{{acc.tariff | number}}" placeholder="Tariff Amount" ng-model='acc.tariff' ng-required="true" maxlength="50" disabled>
												</div>
												  <div class="form-group col-md-3">
													<label >Tax</label>
													<input type="text" class="form-control"  name="bJoinFlatMetroTax"  id="bJoinFlatMetroTax{{acc.accommodationId}}" value="{{acc.tax | number}}" placeholder="Tax Amount" ng-model='acc.tax'  ng-required="true" maxlength="50" disabled>
												</div>
												  <div class="form-group col-md-3">
													<label >Selling Rate<span style="color:red">(without tax)</span></label>
													<input type="text" class="form-control"  name="bJoinFlatMetroSelling"  id="bJoinFlatMetroSelling{{acc.accommodationId}}" value="{{acc.sellingRate | number}}" placeholder="Selling Rate" ng-model='acc.sellingRate' ng-required="true" maxlength="50" disabled>
												</div>
												  <div class="form-group col-md-3">
													<label >Extra Adult</label>
													<input type="text" class="form-control"  name="bJoinFlatMetroExtraAdult"  id="bJoinFlatMetroExtraAdult{{acc.accommodationId}}" placeholder="Extra Adult" ng-model='acc.extraAdult' ng-keyup="getSellingAdultRate(acc.extraAdult,acc.accommodationId)" ng-required="true" maxlength="50" ng-pattern="/^[0-9]/">
												   <span ng-show="bjointFlatrateMetroForm.bJoinFlatMetroExtraAdult.$error.pattern" style="color:red">Not a Valid Number!</span>
												</div>
												  <div class="form-group col-md-3">
													<label >Extra Child</label>
													<input type="text" class="form-control"  name="bJoinFlatMetroExtraChild"  id="bJoinFlatMetroExtraChild{{acc.accommodationId}}" placeholder="Extra Child" ng-model='acc.extraChild' ng-required="true" ng-keyup="getSellingChildRate(acc.extraChild,acc.accommodationId)" maxlength="50" ng-pattern="/^[0-9]/">
												    <span ng-show="bjointFlatrateMetroForm.bJoinFlatMetroExtraChild.$error.pattern" style="color:red">Not a Valid Number!</span>
												</div>
												<div class="form-group col-md-3">
													<label>Selling Adult Rate</label>
													<input type="text" class="form-control"  name="sellingAdult"  id="sellingAdult{{acc.accommodationId}}" ng-model="acc.sellingAdult" readonly>
												</div>
												  <div class="form-group col-md-3">
													<label >Selling Child Rate</label>
													<input type="text" class="form-control"  name="sellingChild"  id="sellingChild{{acc.accommodationId}}" ng-model="acc.sellingChild" readonly>
												</div>
												<div class="form-group col-md-3">
													<label >Property Commission</label>
													<input type="text" class="form-control"  name="bJoinFlatMetroCommission"  id="bJoinFlatMetroCommission{{acc.accommodationId}}" value="{{acc.propertyCommission | number}}" placeholder="Property Commission" ng-model='acc.propertyCommission' ng-required="true" maxlength="50" ng-pattern="/^[0-9]/">
												    <span ng-show="bjointFlatrateMetroForm.bJoinFlatMetroCommission.$error.pattern" style="color:red">Not a Valid Number!</span>
												</div>
												 <div class="col-md-12 modal-footer">
												<div id="accommodationsuccessRate{{acc.accommodationId}}"></div>
													<!-- <button ng-click="deleteAccommodation(acc.accommodationId)" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i> Remove</button> -->
													<button ng-click="editAccommodationRate(acc.accommodationId,acc.propertyContractTypeId,property[0].propertyLocationTypeId)" ng-disabled="bjointFlatrateMetroForm.$invalid" class="btn btn-primary"><i class="fa fa-edit" aria-hidden="true"></i> {{updateroombtn}}</button>
												</div>
												</div>
							                    </form>
							                    <form name="bsureMetroForm" id="bsureMetroForm">
							                    <div class="bsureMetroSetup" ng-if="acc.propertyContractTypeId == 6 ">
							                    <h4>B-Sure</h4>
										       <div class="form-group col-md-3">
													<label >Purchase Rate</label>
													<input type="text" class="form-control"  name="bSureMetroPurchase"  id="bSureMetroPurchase{{acc.accommodationId}}" value="{{acc.purchaseRate | number}}" ng-model="acc.purchaseRate" placeholder="Purchase Rate"  ng-required="true" maxlength="50" ng-pattern="/^[0-9]/">  <!-- ng-pattern="^/^[A-Za-z\d\s]+$/" -->
												     <span ng-show="bsureMetroForm.bSureMetroPurchase.$error.pattern" style="color:red">Not a Valid Number!</span>
												</div>
												 <div class="form-group col-md-3">
                                                    <label >Select Tax</label>
                                              		<select name="bSureMetroTaxType" id="bSureMetroTaxType{{acc.accommodationId}}"   ng-change="changeBSureMetroTaxType(acc.accommodationId)" ng-model="bSureMetroTaxType" class="form-control" >
                                     					<option value="" ng-selected="acc.taxType == ''">Select</option>
                                      				    <option value="1" ng-selected="acc.taxType == 1">Inclusive Tax</option>
                                				        <option value="2" ng-selected="acc.taxType == 2">Exclusive Tax</option>
                                                    </select>
                                             </div>
												   <div class="form-group col-md-3">
													<label >Tariff</label>
													<input type="text" class="form-control"  name="bSureMetroTariff"  id="bSureMetroTariff{{acc.accommodationId}}" value="{{acc.tariff | number}}" placeholder="Tariff Rate" ng-model='acc.tariff' ng-required="true" maxlength="50" disabled> 
												</div>
												  <div class="form-group col-md-3">
													<label >Tax</label>
													<input type="text" class="form-control"  name="bSureMetroTax"  id="bSureMetroTax{{acc.accommodationId}}" value="{{acc.tax | number}}" placeholder="Tax Amount" ng-model='acc.tax' ng-required="true" maxlength="50" disabled>
												</div>
												  <div class="form-group col-md-3">
													<label >Selling Tariff Rate<span style="color:red">(without tax)</span></label>
													<input type="text" class="form-control"  name="bSureMetroSelling"  id="bSureMetroSelling{{acc.accommodationId}}" value="{{acc.sellingRate | number}}" placeholder="Selling Rate" ng-model='acc.sellingRate'  ng-required="true" maxlength="50" disabled>
												</div>
												  <div class="form-group col-md-3">
													<label >Extra Adult</label>
													<input type="text" class="form-control"  name="bSureMetroExtraAdult"  id="bSureMetroExtraAdult{{acc.accommodationId}}" placeholder="Extra Adult Amount" ng-model='acc.extraAdult' ng-keyup="getSellingAdultRate(acc.extraAdult,acc.accommodationId)" ng-required="true" maxlength="50" ng-pattern="/^[0-9]/">
												     <span ng-show="bsureMetroForm.bSureMetroExtraAdult.$error.pattern" style="color:red">Not a Valid Number!</span>
												</div>
												  <div class="form-group col-md-3">
													<label >Extra Child</label>
													<input type="text" class="form-control"  name="bSureMetroExtraChild"  id="bSureMetroExtraChild{{acc.accommodationId}}" placeholder="Extra Child Amount" ng-model='acc.extraChild' ng-keyup="getSellingChildRate(acc.extraChild,acc.accommodationId)" ng-required="true" maxlength="50" ng-pattern="/^[0-9]/">
												      <span ng-show="bsureMetroForm.bSureMetroExtraChild.$error.pattern" style="color:red">Not a Valid Number!</span>
							                    </div>
							                    <div class="form-group col-md-3">
													<label >Selling Adult Rate</label>
													<input type="text" class="form-control"  name="sellingAdult"  id="sellingAdult{{acc.accommodationId}}" ng-model="acc.sellingAdult" readonly>
												</div>
												  <div class="form-group col-md-3">
													<label >Selling Child Rate</label>
													<input type="text" class="form-control"  name="sellingChild"  id="sellingChild{{acc.accommodationId}}" ng-model="acc.sellingChild" readonly>
							                    </div>
							                    <div class="col-md-12 modal-footer">
												<div id="accommodationsuccessRate{{acc.accommodationId}}"></div>
													<!-- <button ng-click="deleteAccommodation(acc.accommodationId)" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i> Remove</button> -->
													<button ng-click="editAccommodationRate(acc.accommodationId,acc.propertyContractTypeId,property[0].propertyLocationTypeId)" ng-disabled="bsureMetroForm.$invalid" class="btn btn-primary"><i class="fa fa-edit" aria-hidden="true"></i> {{updateroombtn}}</button>
												</div>
							                </div>
							                </form>
							                </div>    
						        </div>
						        </div>
						        </div>

						       <div class="editaccomLeisure">

						        <div class="pdlist panel-group" id="accordionLeisure">
						         <div class="panel panel-default" ng-if="property[0].propertyLocationTypeId == 2" ng-repeat="acc in accommodations | orderBy:'propertyContractTypeId'  ">
						            <div class="panel-heading">
						                <h4 class="panel-title">
						                    <a data-toggle="collapse" data-parent="#accordionLeisure" href="#collapseNewLeisure{{acc.accommodationId}}" ng-click="getAccommodation(acc.accommodationId);getOpen(acc.accommodationId);"><span class="glyphicon glyphicon-chevron-down"></span> {{acc.accommodationType}} - {{acc.propertyContractType}} -Leisure</a>
						                </h4>
						            </div>
						            <div id="collapseNewLeisure{{acc.accommodationId}}" class="panel-collapse collapse">
							                <div class="panel-body" id="editaccom">
							                 <form name="bjoinleisuresetup{{acc.accommodationId}}">

							                <div class="bjointsetup"  ng-if="acc.propertyContractTypeId == 7 ">
							                    <div class="form-group col-md-12">
							                <h4>B-Join Net Rate</h4>
							                   </div> 
                                        <div class="weekdaydiv">
                                        		<div class="form-group col-md-3">
													<label >Weekday NetRate</label>
													<input type="text" class="form-control"  name="bJoinLeisureWeekdayNet"  id="bJoinLeisureWeekdayNet{{acc.accommodationId}}" value="{{acc.weekdayNetRate | number}}" ng-model="acc.weekdayNetRate" placeholder="Weekday NetRate" ng-required="true" maxlength="50" ng-pattern="/^[0-9]/" />
												     	     <span ng-show="bjoinleisuresetup.bJoinLeisureWeekdayNet.$error.pattern" style="color:red">Not a Valid Number!</span>
												</div>
												 <div class="form-group col-md-3">
													<label>WeekEnd NetRate</label>
													<input type="text" class="form-control"  name="bJoinLeisureWeekendNet"  id="bJoinLeisureWeekendNet{{acc.accommodationId}}" value="{{acc.weekendNetRate | number}}" ng-model="acc.weekendNetRate" placeholder="WeekEnd NetRate" ng-required="true" maxlength="50" ng-pattern="/^[0-9]/" />
												    	     <span ng-show="bjoinleisuresetup.bJoinLeisureWeekendNet.$error.pattern" style="color:red">Not a Valid Number!</span>
												</div>
												<div class="form-group col-md-3">
                                                    <label >Select Tax</label>
                                              		  <select name="bJoinLeisureTaxType" id="bJoinLeisureTaxType{{acc.accommodationId}}" ng-change="changeBJoinLeisureTaxType(acc.accommodationId)" ng-model="bJoinLeisureTaxType" class="form-control" >
                                     						<option value="" ng-selected="acc.taxType == ''">Select</option>
                                      						<option value="1" ng-selected="acc.taxType == 1">Inclusive Tax</option>
                                      						<option value="2" ng-selected="acc.taxType == 2">Exclusive Tax</option>
                                                       </select>
                                             </div>
												   <div class="form-group col-md-3">
													<label >Weekday Tariff</label>
													<input type="text" class="form-control"  name="bJoinLeisureWeekdayTariff"  id="bJoinLeisureWeekdayTariff{{acc.accommodationId}}" value="{{acc.weekdayTariff | number}}" ng-model="acc.weekdayTariff" placeholder="Weekday Tariff"  maxlength="50" disabled>
												</div>
												  <div class="form-group col-md-3">
													<label >Weekday Tax</label>
													<input type="text" class="form-control"  name="bJoinLeisureWeekdayTax"  id="bJoinLeisureWeekdayTax{{acc.accommodationId}}" value="{{acc.weekdayTax | number}}" ng-model="acc.weekdayTax" placeholder="Weekday Tax"  maxlength="50" disabled>
												</div>
												
												  <div class="form-group col-md-3">
													<label >Weekday Selling Rate<span style="color:red">(without tax)</span></label>
													<input type="text" class="form-control"  name="bJoinLeisureWeekdaySelling"  id="bJoinLeisureWeekdaySelling{{acc.accommodationId}}" value="{{acc.weekdaySelling | number}}" ng-model="acc.weekdaySelling" placeholder="Weekday Selling Rate"  maxlength="50" disabled>
												</div>
                                        
                                        </div>
                                        <div class="weekenddiv">
												   <div class="form-group col-md-3">
													<label >WeekEnd Tariff</label>
													<input type="text" class="form-control"  name="bJoinLeisureWeekendTariff"  id="bJoinLeisureWeekendTariff{{acc.accommodationId}}" value="{{acc.weekendTariff | number}}" ng-model="acc.weekendTariff" placeholder="WeekEnd Tariff"  maxlength="50" disabled>
												   </div>
												   <div class="form-group col-md-3">
													<label >WeekEnd Tax</label>
													<input type="text" class="form-control"  name="bJoinLeisureWeekendTax"  id="bJoinLeisureWeekendTax{{acc.accommodationId}}" value="{{acc.weekdendTax | number}}" ng-model="acc.weekdendTax" placeholder="WeekEnd Tax" maxlength="50" disabled>
												   </div>
												   <div class="form-group col-md-3"> 
													<label >WeekEnd Selling Rate<span style="color:red">(without tax)</span></label>
													<input type="text" class="form-control"  name="bJoinLeisureWeekendSelling"  id="bJoinLeisureWeekendSelling{{acc.accommodationId}}" value="{{acc.weekendSelling | number}}" ng-model="acc.weekendSelling" placeholder="WeekEnd Selling Rate"  maxlength="50" disabled>
												   </div>
												</div>
												<div class="form-group col-md-3">
													<label >Extra Adult</label>
													<input type="text" class="form-control"  name="bJoinLeisureExtraAdult"  id="bJoinLeisureExtraAdult{{acc.accommodationId}}" ng-keyup="getSellingAdultRate(acc.extraAdult,acc.accommodationId)" ng-model="acc.extraAdult" placeholder="Extra Adult" ng-required="true" maxlength="50" ng-pattern="/^[0-9]/">
												    <span ng-show="bjoinleisuresetup.bJoinLeisureExtraAdult.$error.pattern" style="color:red">Not a Valid Number!</span>
												</div>
												<div class="form-group col-md-3">
												  <label >Extra Child</label>
												  <input type="text" class="form-control"  name="bJoinLeisureExtraChild"  id="bJoinLeisureExtraChild{{acc.accommodationId}}" ng-model="acc.extraChild" ng-keyup="getSellingChildRate(acc.extraChild,acc.accommodationId)" placeholder="Extra Child" ng-required="true" maxlength="50" ng-pattern="/^[0-9]/">
												  <span ng-show="bjoinleisuresetup.bJoinLeisureExtraChild.$error.pattern" style="color:red">Not a Valid Number!</span>
												</div>
												<div class="form-group col-md-3">
													<label >Selling Adult Rate</label>
													<input type="text" class="form-control"  name="sellingAdult"  id="sellingAdult{{acc.accommodationId}}" ng-model="acc.sellingAdult" readonly>
												</div>
												<div class="form-group col-md-3">
												  <label >Selling Child Rate</label>
												  <input type="text" class="form-control"  name="sellingChild"  id="sellingChild{{acc.accommodationId}}" ng-model="acc.sellingChild" readonly>
												</div>
												   <div class="col-md-12 modal-footer">
												<div id="accommodationsuccessRate{{acc.accommodationId}}"></div>
													<!-- <button ng-click="deleteAccommodation(acc.accommodationId)" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i> Remove</button> -->
													<button ng-click="editAccommodationRate(acc.accommodationId,acc.propertyContractTypeId,property[0].propertyLocationTypeId)" ng-disabled="bjoinleisuresetup.$invalid" class="btn btn-primary"> {{updateroombtn}}</button>
												</div>   
							                </div>
							                	
							                </form>
							                <form name="bsureleisuresetup{{acc.accommodationId}}" >
							                  <div class="bsureSetup" ng-if="acc.propertyContractTypeId == 6 ">
							                   <div class="col-md-12">
							                <h4>B-sure</h4>
							                </div>
                                        <div class="weekdaydiv">
                                       		    <div class="form-group col-md-3">
													<label >Weekday Purchase Rate</label>
													<input type="text" class="form-control"  name="bSureLeisureWeekdayPurchase"  id="bSureLeisureWeekdayPurchase{{acc.accommodationId}}" value="{{acc.weekdayPurchase | number}}" ng-model="acc.weekdayPurchase" placeholder="Weekday Purchase Rate" ng-required="true" maxlength="50" ng-pattern="/^[0-9]/">
												    <span ng-show="bsureleisuresetup.bSureLeisureWeekdayPurchase.$error.pattern" style="color:red">Not a Valid Number!</span>
												</div>
												<div class="form-group col-md-3">
													<label >WeekEnd Purchase Rate</label>
													<input type="text" class="form-control"  name="bSureLeisureWeekendPurchase"  id="bSureLeisureWeekendPurchase{{acc.accommodationId}}" value="{{acc.weekendPurchase | number}}" ng-model="acc.weekendPurchase" placeholder="WeekEnd Purchase Rate" ng-required="true" maxlength="50" ng-pattern="/^[0-9]/">
												    <span ng-show="bsureleisuresetup.bSureLeisureWeekendPurchase.$error.pattern" style="color:red">Not a Valid Number!</span>
												</div>
												<div class="form-group col-md-3">
                                                    <label >Select Tax</label>
                                              		   <select name="bSureLeisureTaxType" id="bSureLeisureTaxType{{acc.accommodationId}}" ng-change="changeBSureLeisureTaxType(acc.accommodationId)" ng-model="bSureLeisureTaxType" class="form-control" >
                                     						<option value="" ng-selected="acc.taxType == ''">Select</option>
                                      						<option value="1" ng-selected="acc.taxType == 1">Inclusive Tax</option>
                                      						<option value="2" ng-selected="acc.taxType == 2">Exclusive Tax</option>
                                                       </select>
                                             	</div>
												   <div class="form-group col-md-3">
													<label >Weekday Tariff</label>
													<input type="text" class="form-control"  name="bSureLeisureWeekdayTariff"  id="bSureLeisureWeekdayTariff{{acc.accommodationId}}" value="{{acc.weekdayTariff | number}}" ng-model="acc.weekdayTariff" placeholder="Weekday Tariff" ng-required="true" maxlength="50" disabled>
												</div>
												  <div class="form-group col-md-3">
													<label >Weekday Tax</label>
													<input type="text" class="form-control"  name="bSureLeisureWeekdayTax"  id="bSureLeisureWeekdayTax{{acc.accommodationId}}" value="{{acc.weekdayTax | number}}" ng-model="acc.weekdayTax" placeholder="Weekday Tax" ng-required="true" maxlength="50" disabled>
												</div>
												
												  <div class="form-group col-md-3">
													<label >Weekday Selling Rate<span style="color:red">(without tax)</span></label>
													<input type="text" class="form-control"  name="bSureLeisureWeekdaySelling"  id="bSureLeisureWeekdaySelling{{acc.accommodationId}}" value="{{acc.weekdaySelling | number}}" ng-model="acc.weekdaySelling" placeholder="Selling Rate" ng-required="true" maxlength="50" disabled>
												</div>
                                        </div>
                                        <div class="weekenddiv">
												   <div class="form-group col-md-3">
													<label >WeekEnd Tariff</label>
													<input type="text" class="form-control"  name="bSureLeisureWeekendTariff"  id="bSureLeisureWeekendTariff{{acc.accommodationId}}" value="{{acc.weekendTariff | number}}" ng-model="acc.weekendTariff" placeholder="WeekEnd Tariff" ng-required="true" maxlength="50" disabled>
												</div>
												  <div class="form-group col-md-3">
													<label >WeekEnd Tax</label>
													<input type="text" class="form-control"  name="bSureLeisureWeekendTax"  id="bSureLeisureWeekendTax{{acc.accommodationId}}" value="{{acc.weekdendTax | number}}" ng-model="acc.weekdendTax" placeholder="WeekEnd Tax" ng-required="true" maxlength="50" disabled>
												</div>
												  <div class="form-group col-md-3"> 
													<label >WeekEnd Selling Rate<span style="color:red">(without tax)</span></label>
													<input type="text" class="form-control"  name="bSureLeisureWeekendSelling"  id="bSureLeisureWeekendSelling{{acc.accommodationId}}" value="{{acc.weekendSelling | number}}" ng-model="acc.weekendSelling" placeholder="WeekEnd Selling Rate" ng-required="true" maxlength="50" disabled>
												</div>
												</div>
												<div class="form-group col-md-3">
													<label >Extra Adult</label>
													<input type="text" class="form-control"  name="bSureLeisureExtraAdult"  id="bSureLeisureExtraAdult{{acc.accommodationId}}" ng-keyup="getSellingAdultRate(acc.extraAdult,acc.accommodationId)" placeholder="Extra Adult" ng-model="acc.extraAdult" ng-required="true" maxlength="50" ng-pattern="/^[0-9]/">
												     <span ng-show="bsureleisuresetup.bSureLeisureExtraAdult.$error.pattern" style="color:red">Not a Valid Number!</span>
												</div>
												  <div class="form-group col-md-3">
													<label >Extra Child</label>
													<input type="text" class="form-control"  name="bSureLeisureExtraChild"  id="bSureLeisureExtraChild{{acc.accommodationId}}" ng-keyup="getSellingChildRate(acc.extraChild,acc.accommodationId)" placeholder="Extra Child" ng-model="acc.extraChild" ng-required="true" maxlength="50" ng-pattern="/^[0-9]/">
												     <span ng-show="bsureleisuresetup.bSureLeisureExtraChild.$error.pattern" style="color:red">Not a Valid Number!</span>
												</div>
												<div class="form-group col-md-3">
													<label >Selling Adult Rate</label>
													<input type="text" class="form-control"  name="sellingAdult"  id="sellingAdult{{acc.accommodationId}}" ng-model="acc.sellingAdult" readonly>
												</div>
												<div class="form-group col-md-3">
													<label >Selling Child Rate</label>
													<input type="text" class="form-control"  name="sellingChild"  id="sellingChild{{acc.accommodationId}}" ng-model="acc.sellingChild" readonly>
												</div>
											<div class="col-md-12 modal-footer">
												<div id="accommodationsuccessRate{{acc.accommodationId}}"></div>
													<!-- <button ng-click="deleteAccommodation(acc.accommodationId)" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i> Remove</button> -->
													<button ng-click="editAccommodationRate(acc.accommodationId,acc.propertyContractTypeId,property[0].propertyLocationTypeId)" ng-disabled="bsureleisuresetup.$invalid" class="btn btn-primary"><i class="fa fa-edit" aria-hidden="true"></i> {{updateroombtn}}</button>
												</div>
							                </div>
							                </form>
							                <form name="bjointFlatSetup{{acc.accommodationId}}">
							               <div class="bjointFlatSetup"  ng-if="acc.propertyContractTypeId == 9">
							               <div class="col-md-12">
							               <h4>B-Join Flat Rate</h4>
							               </div>
                                        <div class="weekdaydiv">
                                        		<div class="form-group col-md-3">
													<label >Weekday Base Rate</label>
													<input type="text" class="form-control"  name="bJoinFlatLeisureWeekdayBase"  id="bJoinFlatLeisureWeekdayBase{{acc.accommodationId}}" value="{{acc.weekdayBaseAmount | number}}" ng-model="acc.weekdayBaseAmount" placeholder="Weekday Base Rate" ng-required="true" maxlength="50" ng-pattern="/^[0-9]/">
												  	     <span ng-show="bjointFlatSetup.bSureLeisureExtraChild.$error.pattern" style="color:red">Not a Valid Number!</span>
												</div>
												 <div class="form-group col-md-3">
													<label >WeekEnd Base Rate</label>
													<input type="text" class="form-control"  name="bJoinFlatLeisureWeekendBase"  id="bJoinFlatLeisureWeekendBase{{acc.accommodationId}}" value="{{acc.weekendBaseAmount | number}}" ng-model="acc.weekendBaseAmount" placeholder="WeekEnd Base Rate" ng-required="true" maxlength="50" ng-pattern="/^[0-9]/">
												  	     <span ng-show="bjointFlatSetup.bJoinFlatLeisureWeekendBase.$error.pattern" style="color:red">Not a Valid Number!</span>
												</div>
												<div class="form-group col-md-3">
                                                    <label >Select Tax</label>
                                                    <select name="bJoinFlatLeisureTaxType" id="bJoinFlatLeisureTaxType{{acc.accommodationId}}" ng-change="changeBJoinFlatLeisureTaxType(acc.accommodationId)" ng-model="bJoinFlatLeisureTaxType" class="form-control" >
                                     						<option value="" ng-selected="acc.taxType == ''">Select</option>
                                      						<option value="1" ng-selected="acc.taxType == 1">Inclusive Tax</option>
                                      						<option value="2" ng-selected="acc.taxType == 2">Exclusive Tax</option>
                                                       </select>
                                             	</div>
												   <div class="form-group col-md-3">
													<label >Weekday Tariff</label>
													<input type="text" class="form-control"  name="bJoinFlatLeisureWeekdayTariff"  id="bJoinFlatLeisureWeekdayTariff{{acc.accommodationId}}" value="{{acc.weekdayTariff | number}}" ng-model="acc.weekdayTariff" placeholder="Weekday Tariff" ng-required="true" maxlength="50" disabled>
												</div>
												  <div class="form-group col-md-3">
													<label >Weekday Tax</label>
													<input type="text" class="form-control"  name="bJoinFlatLeisureWeekdayTax"  id="bJoinFlatLeisureWeekdayTax{{acc.accommodationId}}" value="{{acc.weekdayTax | number}}" ng-model="acc.weekdayTax" placeholder="Weekday Tax" ng-required="true" maxlength="50" disabled>
												</div>
												  <div class="form-group col-md-3">
													<label >Selling Rate<span style="color:red">(without tax)</span></label>
													<input type="text" class="form-control"  name="bJoinFlatLeisureWeekdaySelling"  id="bJoinFlatLeisureWeekdaySelling{{acc.accommodationId}}" value="{{acc.weekdaySelling | number}}" ng-model="acc.weekdaySelling" placeholder="Selling Rate" ng-required="true" maxlength="50" disabled>
												</div>
                                        </div>
                                        <div class="weekenddiv">
												   <div class="form-group col-md-3">
													<label >WeekEnd Tariff</label>
													<input type="text" class="form-control"  name="bJoinFlatLeisureWeekendTariff"  id="bJoinFlatLeisureWeekendTariff{{acc.accommodationId}}" value="{{acc.weekdaySelling | number}}" ng-model="acc.weekendTariff" placeholder="WeekEnd Tariff" ng-required="true" maxlength="50" disabled>
												</div>
												  <div class="form-group col-md-3">
													<label >WeekEnd Tax</label>
													<input type="text" class="form-control"  name="bJoinFlatLeisureWeekendTax"  id="bJoinFlatLeisureWeekendTax{{acc.accommodationId}}" value="{{acc.weekdendTax | number}}" ng-model="acc.weekdendTax" placeholder="WeekEnd Tax" ng-required="true" maxlength="50" disabled>
												</div>
												
												  <div class="form-group col-md-3"> 
													<label >WeekEnd Selling Rate<span style="color:red">(without tax)</span></label>
													<input type="text" class="form-control" name="bJoinFlatLeisureWeekendSelling"  id="bJoinFlatLeisureWeekendSelling{{acc.accommodationId}}" value="{{acc.weekendSelling | number}}" ng-model="acc.weekendSelling" placeholder="WeekEnd Selling Rate" ng-required="true" maxlength="50" disabled>
												</div></div>
												  <div class="form-group col-md-3">
													<label >Extra Adult</label>
													<input type="text" class="form-control"  name="bJoinFlatLeisureExtraAdult"  id="bJoinFlatLeisureExtraAdult{{acc.accommodationId}}" ng-keyup="getSellingAdultRate(acc.extraAdult,acc.accommodationId)" ng-model="acc.extraAdult" placeholder="Extra Adult" ng-required="true" maxlength="50" ng-pattern="/^[0-9]/">
													<span ng-show="bjointFlatSetup.bJoinFlatLeisureExtraAdult.$error.pattern" style="color:red">Not a Valid Number!</span>
												</div>
												  <div class="form-group col-md-3">
													<label >Extra Child</label>
													<input type="text" class="form-control"  name="bJoinFlatLeisureExtraChild"  id="bJoinFlatLeisureExtraChild{{acc.accommodationId}}" ng-keyup="getSellingChildRate(acc.extraChild,acc.accommodationId)" ng-model="acc.extraChild" placeholder="Extra Child" ng-required="true" maxlength="50" ng-pattern="/^[0-9]/">
												    <span ng-show="bjointFlatSetup.bJoinFlatLeisureExtraChild.$error.pattern" style="color:red">Not a Valid Number!</span>
												</div>
												<div class="form-group col-md-3">
													<label >Selling Adult Rate</label>
													<input type="text" class="form-control"  name="sellingAdult"  id="sellingAdult{{acc.accommodationId}}" ng-model="acc.sellingAdult" readonly>
												</div>
												  <div class="form-group col-md-3">
													<label >Selling Child Rate</label>
													<input type="text" class="form-control"  name="sellingChild"  id="sellingChild{{acc.accommodationId}}" ng-model="acc.sellingChild" readonly>
												</div>
												 <div class="form-group col-md-3">
													<label >Property Commission</label>
													<input type="text" class="form-control"  name="bJoinFlatLeisureCommission"  id="bJoinFlatLeisureCommission{{acc.accommodationId}}" value="{{acc.propertyCommission | number}}" ng-model="acc.propertyCommission" placeholder="Property Commission" ng-required="true" maxlength="50" ng-pattern="/^[0-9]/">
												    <span ng-show="bjointFlatSetup.bJoinFlatLeisureCommission.$error.pattern" style="color:red">Not a Valid Number!</span>
												</div>
										       <div class="col-md-12 modal-footer">
												<div id="accommodationsuccessRate{{acc.accommodationId}}"></div>
													<!-- <button ng-click="deleteAccommodation(acc.accommodationId)" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i> Remove</button> -->
													<button ng-click="editAccommodationRate(acc.accommodationId,acc.propertyContractTypeId,property[0].propertyLocationTypeId)" ng-disabled="bjointFlatSetup.$invalid" class="btn btn-primary"><i class="fa fa-edit" aria-hidden="true"></i> {{updateroombtn}}</button>
												</div>
							               </div>
												</form>
												</div>
							                </div>
						        </div>
						        <section>
                           <h4 class="page-header"></h4>
                           <div class="row fontawesome-icon-list">
                           <div id="roomLength"></div>
                              <div class="col-md-3 col-sm-4"></div>
                              <div class="col-md-3 col-sm-4"></div>
                              <div class="col-md-3 col-sm-4"></div>
                              <div class="col-md-3 col-sm-4">
                                 <button type="submit"  class="btn btn-primary btnnext pull-right"  ng-click="getEnableRate(); ">Continue</button>
                              </div>
                           </div>
                        </section>
              </div>
               </div>
               </div>
               <!-- Hotel Rate Plan Ends -->
               <!-- Hotel Amenities begins -->
               <div class="tab-pane {{ulotelHotelTags[0].hotelAmenities}}" id="tab-amenities" >
                <div class="row">
                <div class="col-md-12">
						 <h4>Hotel Amenities</h4>
						 <form name="form1" id="form1">
						 <div class="col-md-3" ng-repeat="pa in propertyAmenities">
						<div class="checkbox" >
                			<label> 
                				<input type="checkbox" ng-disabled="!pa.status && checkedAmenity()" ng-model="pa.status" value="{{pa.DT_RowId}}" ><span style="font-size:18px;">{{pa.amenityName}}</span>
                			</label>
                		</div>
						</div>
								 
						 <div class="col-md-12 col-sm-12">
						 <div id="propertyAmenities"></div>
			                        	<button type="button" ng-selected="checkedAmenity5()" ng-disabled="!count == true" class="btn btn-primary  pull-right" ng-click="savePropertyAmenities()"><i class="fa fa-edit" aria-hidden="true"></i> {{updatehamenitybtn}}</button>
			             </div>
						 </form>
			    </div>
                </div>
                <div class="row">
                <div class="col-md-12">
                	 <h4>Room Amenities</h4>
                	 
                	 
                </div>
                <div class="col-md-12">
                	<div class="panel-group" id="accordionAmmenity">
                      <div class="panel panel-default"  ng-repeat="acc in accommodations">
        
			            <div class="panel-heading">
			              <h4 class="panel-title" >
			                    <a data-toggle="collapse" data-parent="#accordionAmmenity" href="#collapsefour{{acc.accommodationId}}" ng-click="getAccommodationAmenities(acc.accommodationId);getOpen(acc.accommodationId);"><span class="glyphicon glyphicon-chevron-down"></span>{{acc.accommodationType}}</a> 
			                </h4>
			            </div>
			            <div id="collapsefour{{acc.accommodationId}}" class="panel-collapse collapse">
			                 <div class="panel-body"  >
			              		 <form id ="form{{acc.accommodationId}}" name="form{{acc.accommodationId}}" method="post" action="" enctype="multipart/form-data" theme="simple">
										  <section id="new">
										<div class="col-md-3 col-sm-4" ng-repeat="accam in accamenities  | orderBy:'amenityName'">
				                        	 <div class="form-group" >
				                        	 	<label>
			                      					<input type="checkbox" ng-disabled="!accam.status && checkedAccomAmenity()" ng-model="accam.status" value="{{accam.DT_RowId}}"  >&nbsp <span style="font-size:18px;">{{accam.amenityName}}</span>
			                    				</label>
				                        	 </div>
				                        </div>
				                        </section>
				                        <section>
									 <div class="col-md-12 col-sm-12">
									         <div id ="amenitysuccess{{acc.accommodationId}}"></div>
						                      <button type="button"  class="btn btn-primary  pull-right" ng-selected="checkedAccomAmenity5()"  ng-disabled="!countaccom == true" ng-click="saveAccommodationAmenities(acc.accommodationId)"><i class="fa fa-edit" aria-hidden="true"></i> {{updateAamenitybtn}}</button>
						              </div>
						              </section>
								 </form>
						     </div>
			            </div>
        			</div>
                </div>
                 <%-- <section>
                           <h4 class="page-header"></h4>
                           <div class="row fontawesome-icon-list">
                           <div id="roomLength"></div>
                              <div class="col-md-3 col-sm-4"></div>
                              <div class="col-md-3 col-sm-4"></div>
                              <div class="col-md-3 col-sm-4"></div>
                              <div class="col-md-3 col-sm-4">
                                 <button type="submit" ng-disabled="!count == true || !countaccom == true" class="btn btn-primary btnnext pull-right" ng-click="getEnableAmenities();">Update</button>
                              </div>
                           </div>
                        </section> --%>
                </div>
               
                </div>
                <div class="row">
                <!-- <div class="col-md-12">
                	 <h4>Tag Amenities</h4>
                	 
                	 
                </div> -->
                <div class="col-md-12">
                <%-- <div class="panel-group" id="accordionTagAmenity">
                      <div class="panel panel-default"  ng-repeat="pt in propertyTags">
        
            <div class="panel-heading">
              <h4 class="panel-title" >
                    <a data-toggle="collapse" data-parent="#accordionTagAmenity" href="#collapsefour{{pt.tagId}}" ng-click="getTagAmenities(pt.tagId);getOpen(pt.tagId);"><span class="glyphicon glyphicon-chevron-down"></span>{{pt.tagName}}</a> 
                </h4>
            </div>
            <div id="collapsefour{{pt.tagId}}" class="panel-collapse collapse">
                 <div class="panel-body"  >
              		 <form id ="formTag{{pt.tagId}}" name="formTag{{pt.tagId}}" method="post" action="" enctype="multipart/form-data" theme="simple">
							  <section id="new">
							<div class="col-md-3 col-sm-4" ng-repeat="tagam in tagamenities  | orderBy:'amenityName'">
	                        	 <div class="form-group" >
	                        	 	<label>
                      					<input type="checkbox" ng-disabled="!tagam.status && checkedTagAmenity()" ng-model="tagam.status" value="{{tagam.DT_RowId}}"  >&nbsp {{tagam.amenityName}}
                    				</label>
	                        	 </div>
	                        </div>
	                        </section>
	                        <section>
						 <div class="col-md-12 col-sm-12">
						         <div id="tagAmenitiesSuccess{{pt.tagId}}"></div>
			                      <button type="button"  class="btn btn-primary  pull-right" ng-selected="checkedTagAmenityCount()"  ng-disabled="!counttag == true" ng-click="saveTagAmenities(pt.tagId)"><i class="fa fa-edit" aria-hidden="true"></i> {{updatetagamenitiesbtn}}</button>
			              </div>
			              </section>
						 </form>
			            </div>
            	</div>
        	</div>
                </div> --%>
                 <section>
                           <h4 class="page-header"></h4>
                           <div class="row fontawesome-icon-list">
                           <div id="roomLength"></div>
                              <div class="col-md-3 col-sm-4"></div>
                              <div class="col-md-3 col-sm-4"></div>
                              <div class="col-md-3 col-sm-4"></div>
                              <div class="col-md-3 col-sm-4">
                                 <button type="submit"  class="btn btn-primary btnnext pull-right" ng-click="getEnableAmenities();">Continue</button>  <!-- ng-disabled="accomAmenitySize == true || propAmenitySize == true" -->
                              </div>
                           </div>
                        </section>
                </div>
               
                </div>
               </div>
               <!-- Hotel Amenities ends -->
               <!-- Hotel Policy Begins -->
                <div class="tab-pane {{ulotelHotelTags[0].hotelPolicy}}" id="tab-policy" >
                <div class="row">	
						 
						  <div class="col-md-12">
					
                     <form method="post" theme="simple" name="propertyPolicy">
                        <section id="new">
                             <div class=" fontawesome-icon-list">
                              <div class="col-md-12 col-sm-12">
                                 <div class="form-group">
                                    <h4>Gigi Policy : </h4>
                                    <textarea id="propertyGigiPolicy" ng-model="property[0].propertyGigiPolicy" name="propertyGigiPolicy" class="form-control" value="{{property[0].propertyGigiPolicy}}"  placeholder="Addon Policy" ng-required="true">
		                    		{{property[0].propertyGigiPolicy}}
		                    		</textarea>
                                 </div>
		                    		
                              </div>
                           </div>
                           <div class="fontawesome-icon-list">
                              <div class="col-md-12 col-sm-12">
                                 <div class="form-group">
                                    <h4>Hotel Policy : </h4>
                                    <textarea id="propertyHotelPolicy" name="propertyHotelPolicy" ng-model="property[0].propertyHotelPolicy" class="form-control" value="{{property[0].propertyHotelPolicy}}" placeholder="Hotel Policy"   ng-required="true">
		                    		{{property[0].propertyHotelPolicy}}
		                    		</textarea>
                                 </div>
                              </div>
                           </div>
                           <div class=" fontawesome-icon-list">
                              <div class="col-md-12 col-sm-12">
                                 <div class="form-group">
                                    <h4>Cancellation Policy : </h4>
                                    <textarea  id="propertyCancellationPolicy" ng-model="property[0].propertyCancellationPolicy" name="propertyCancellationPolicy" class="form-control" value="{{property[0].propertyCancellationPolicy}}"  placeholder="Cancellation Policy" ng-required="true">
		                    		{{property[0].propertyCancellationPolicy}}
		                    		</textarea>
                                 </div>
                              </div>
                           </div>
                         <div class="fontawesome-icon-list">
                              <div class="col-md-12 col-sm-12">
                                 <div class="form-group">
                                    <h4>Refund Policy : </h4>
                                    <textarea id="propertyRefundPolicy" ng-model="property[0].propertyRefundPolicy" name="propertyRefundPolicy" class="form-control" value="{{property[0].propertyRefundPolicy}}"  placeholder="Refund Policy" ng-required="true">
		                    		{{property[0].propertyRefundPolicy}}
		                    		</textarea>
                                 </div>
                              </div>
                           </div>

                        </section>
                        <section>
                           <h4 class="page-header"></h4>
                           <div class="row fontawesome-icon-list">
                           <div class="col-md-12 col-sm-12">
                             <div id="policysuccess"></div>
                             <div id="policyfailed"></div>
                             </div>
                              <div class="col-md-12 col-sm-12">
                                 <button type="submit" ng-click="editPropertyPolicy()" class="btn btn-primary btngreeen pull-right btnNext"  onclick ="goToByScroll('page-header');">Save & Continue</button>
                              </div>
                           </div>
                        </section>
                     </form>
                  </div> 
			      </div>
               </div>
               <!-- Hotel Policy Ends -->
               <!-- Hotel Landmark Begins  -->
               <div class="tab-pane {{ulotelHotelTags[0].hotelLandmark}}" id="tab-landmark">
                <div class="row fontawesome-icon-list">
                           <div class="col-md-12 col-sm-12">
                           <form name="hotelLandmarks">
                             <h4>5 Major Landmarks</h4>
                       	    <div class="form-group col-md-8">
							  <label >Landmark 1</label>
							  <input type="text" ng-pattern="/^[a-zA-Z0-9]/" class="form-control"  name="landmark1"  id="landmark1" ng-model="propLandmark[0].propertyLandmark1" value="{{propLandmark[0].propertyLandmark1}}" placeholder="Landmark" ng-required="true" maxlength="50">
						      <span ng-show="hotelLandmarks.landmark1.$error.pattern" style="color:red">Not a Valid Number!</span>
						    </div>
						    <div class="form-group col-md-4">
							  <label >Kms</label>
							  <input type="text" ng-pattern="/^[0-9]/" class="form-control" ng-model="propLandmark[0].kilometers1"  name="kilom1"  id="kilom1" ng-model="propLandmark[0].kilometers1" value="{{propLandmark[0].kilometers1}}"  placeholder="Kilo Meter" ng-required="true" maxlength="50">
						      <span ng-show="hotelLandmarks.kilom1.$error.pattern" style="color:red">Not a Valid Number!</span>
						    </div>
						    <div class="form-group col-md-8">
							  <label >Landmark 2</label>
							  <input type="text" ng-pattern="/^[a-zA-Z0-9]/" class="form-control"  name="landmark2"  id="landmark2" ng-model="propLandmark[0].propertyLandmark2" value="{{propLandmark[0].propertyLandmark2}}" placeholder="Landmark" ng-required="true" maxlength="50">
						      <span ng-show="hotelLandmarks.landmark2.$error.pattern" style="color:red">Not a Valid Number!</span>
						    </div>
						    <div class="form-group col-md-4">
							  <label >Kms</label>
							  <input type="text" ng-pattern="/^[0-9]/" ng-model="propLandmark[0].kilometers2" class="form-control"  name="kilom2"  id="kilom2" ng-model="propLandmark[0].kilometers2" value="{{propLandmark[0].kilometers2}}" placeholder="Kilo Meters" ng-required="true" maxlength="50">
						      <span ng-show="hotelLandmarks.kilom2.$error.pattern" style="color:red">Not a Valid Number!</span>
						    </div>
						    <div class="form-group col-md-8">
							  <label >Landmark 3</label>
							  <input type="text" ng-pattern="/^[a-zA-Z0-9]/" class="form-control"  name="landmark3"  id="landmark3" ng-model="propLandmark[0].propertyLandmark3" value="{{propLandmark[0].propertyLandmark3}}" placeholder="Landmark" ng-required="true" maxlength="50">
						      <span ng-show="hotelLandmarks.landmark3.$error.pattern" style="color:red">Not a Valid Number!</span>
						    </div>
						    <div class="form-group col-md-4">
							  <label >Kms</label>
							  <input type="text" ng-pattern="/^[0-9]/" class="form-control"  name="kilom3"  id="kilom3"  ng-model="propLandmark[0].kilometers3" value="{{propLandmark[0].kilometers3}}" placeholder="Kilo Meters" ng-required="true" maxlength="50">
						      <span ng-show="hotelLandmarks.kilom3.$error.pattern" style="color:red">Not a Valid Number!</span>
						    </div>
						    <div class="form-group col-md-8">
							  <label >Landmark 4</label>
							  <input type="text" ng-pattern="/^[a-zA-Z0-9]/" class="form-control"  name="landmark4"  id="landmark4" ng-model="propLandmark[0].propertyLandmark4" value="{{propLandmark[0].propertyLandmark4}}" placeholder="Landmark" ng-required="true" maxlength="50">
						      <span ng-show="hotelLandmarks.landmark4.$error.pattern" style="color:red">Not a Valid Number!</span>
						    </div>
						    <div class="form-group col-md-4">
							   <label >Kms</label>
							   <input type="text" ng-pattern="/^[0-9]/" class="form-control" ng-model="propLandmark[0].kilometers4" name="kilom4"  id="kilom4" value="{{propLandmark[0].kilometers4}}" placeholder="Kilo Meters" ng-required="true" maxlength="50">
						       <span ng-show="hotelLandmarks.kilom4.$error.pattern" style="color:red">Not a Valid Number!</span>
						    </div>
						       <div class="form-group col-md-8">
													<label >Landmark 5</label>
													<input type="text" ng-pattern="/^[a-zA-Z0-9]/" class="form-control"  name="landmark5"  id="landmark5" ng-model="propLandmark[0].propertyLandmark5" value="{{propLandmark[0].propertyLandmark5}}" placeholder="Landmark" ng-required="true" maxlength="50">
						     <span ng-show="hotelLandmarks.landmark5.$error.pattern" style="color:red">Not a Valid Number!</span>
						    </div>
						     <div class="form-group col-md-4">
													<label>Kms</label>
													<input type="text" ng-pattern="/^[0-9]/" class="form-control"  name="kilom5"  id="kilom5" value="{{propLandmark[0].kilometers5}}" ng-model="propLandmark[0].kilometers5" placeholder="Kilo Meters" ng-required="true" maxlength="50">
						      <span ng-show="hotelLandmarks.kilom5.$error.pattern" style="color:red">Not a Valid Number!</span>
						    </div>
						    <h4>5 Major Attraction</h4>
                            <div class="form-group col-md-8">
													<label >Attraction 1</label>
													<input type="text" ng-pattern="/^[a-zA-Z0-9]/" class="form-control"  name="attraction1"  id="attraction1" ng-model="propLandmark[0].propertyAttraction1" value="{{propLandmark[0].propertyAttraction1}}" placeholder="Attraction" ng-required="true" maxlength="50">
						      <span ng-show="hotelLandmarks.attraction1.$error.pattern" style="color:red">Not a Valid Number!</span>
						    </div>
						     <div class="form-group col-md-4">
													<label >Kms</label>
													<input type="text" ng-pattern="/^[0-9]/"  ng-model="propLandmark[0].distance1" class="form-control"  name="distance1"  id="distance1" ng-model="propLandmark[0].distance1" value="{{propLandmark[0].distance1}}" placeholder="Kilo Meters" ng-required="true" maxlength="50">
						     <span ng-show="hotelLandmarks.distance1.$error.pattern" style="color:red">Not a Valid Number!</span>
						    </div>
						     <div class="form-group col-md-8">
													<label >Attraction 2</label>
													<input type="text" ng-pattern="/^[a-zA-Z0-9]/" class="form-control"  name="attraction2"  id="attraction2" ng-model="propLandmark[0].propertyAttraction2" value="{{propLandmark[0].propertyAttraction2}}" placeholder="Attraction" ng-required="true" maxlength="50">
						      <span ng-show="hotelLandmarks.attraction2.$error.pattern" style="color:red">Not a Valid Number!</span>
						    </div>
						     <div class="form-group col-md-4">
													<label >Kms</label>
													<input type="text" ng-pattern="/^[0-9]/" class="form-control"  name="distance2"  id="distance2" value="{{propLandmark[0].distance2}}" ng-model="propLandmark[0].distance2" placeholder="Kilo Meters" ng-required="true" maxlength="50">
						     <span ng-show="hotelLandmarks.distance2.$error.pattern" style="color:red">Not a Valid Number!</span>
						    </div>
						     <div class="form-group col-md-8">
													<label >Attraction 3</label>
													<input type="text" ng-pattern="/^[a-zA-Z0-9]/" class="form-control"  name="attraction3"  id="attraction3" ng-model="propLandmark[0].propertyAttraction3" value="{{propLandmark[0].propertyAttraction3}}" placeholder="Attraction" ng-required="true" maxlength="50">
						      <span ng-show="hotelLandmarks.attraction3.$error.pattern" style="color:red">Not a Valid Number!</span>
						    </div>
						     <div class="form-group col-md-4">
													<label >Kms</label>
													<input type="text"  class="form-control"  name="distance3"  id="distance3" value="{{propLandmark[0].distance3}}" ng-model="propLandmark[0].distance3" placeholder="Kilo Meters" ng-required="true" maxlength="50" ng-pattern="/^[0-9]/">
						      <span ng-show="hotelLandmarks.distance3.$error.pattern" style="color:red">Not a Valid Number!</span>
						    </div>
						     <div class="form-group col-md-8">
													<label >Attraction 4</label>
													<input type="text" ng-pattern="/^[a-zA-Z0-9]/" class="form-control"  name="attraction4"  id="attraction4" value="{{propLandmark[0].propertyAttraction4}}" ng-model="propLandmark[0].propertyAttraction4" placeholder="Attraction" ng-required="true" maxlength="50">
						      <span ng-show="hotelLandmarks.attraction4.$error.pattern" style="color:red">Not a Valid Number!</span>
						    </div>
						     <div class="form-group col-md-4">
													<label>Kms</label>
													<input type="text" class="form-control"  name="distance4"  id="distance4" value="{{propLandmark[0].distance4}}" ng-model="propLandmark[0].distance4" placeholder="Kilo Meters" ng-required="true" maxlength="50" ng-pattern="/^[0-9]/">
						      <span ng-show="hotelLandmarks.distance4.$error.pattern" style="color:red">Not a Valid Number!</span>
						    </div>
						     <div class="form-group col-md-8">
													<label>Attraction 5</label>
													<input type="text" ng-pattern="/^[a-zA-Z0-9]/" class="form-control"  name="attraction5"  id="attraction5" ng-model="propLandmark[0].propertyAttraction5" value="{{propLandmark[0].propertyAttraction5}}" placeholder="Attraction" ng-required="true" maxlength="50">  <!-- /^[A-z\d_@.#$=!%^)(\]:\*;\?\/\,}{'\|<>\[&\+-]*$/ -->
						      <span ng-show="hotelLandmarks.attraction5.$error.pattern" style="color:red">Please Fill This</span>
						    </div>
						     <div class="form-group col-md-4">
													<label>Kms</label>
													<input type="text" class="form-control"  name=distance5  id="distance5" value="{{propLandmark[0].distance5}}" ng-model="propLandmark[0].distance5" placeholder="Kilo Meters" ng-required="true" maxlength="50" ng-pattern="/^[0-9]/">
						      <span ng-show="hotelLandmarks.distance5.$error.pattern" style="color:red">Not a Valid Number!</span>
						    </div>
						   <div class="col-md-12 col-sm-12">
						   <div id="landmarksuccess"></div>
                                 <button type="submit" ng-click="editPropertyLandmark()" ng-disabled ="hotelLandmarks.$invalid" class="btn btn-primary btngreeen pull-right btnNext"  onclick ="goToByScroll('page-header');">Save & Continue</button>
                              </div>
                           </form>
                           </div>
                  </div>         
               </div>
               <!-- Hotel Addon Ends -->
                 <!-- Hotel Addon Begins  -->
              <div class="tab-pane {{ulotelHotelTags[0].addOn}}" id="tab-addon">
                     
                       <div class="bs-example hoteladdonsection">
						    <div class="panel-group" id="accordionAddon">
						    <form method="post" theme="simple" name="addaddon" id="addaddon">
						    <div class="panel panel-default">
							            <div class="panel-heading">
							                <h4 class="panel-title">
							                    <a data-toggle="collapse"  data-parent="#accordionaddonnew" href="#collapseTwonew"><span class="glyphicon glyphicon-chevron-down"></span> Add Addon</a>
							                </h4>
							            </div>
							            <div id="collapseTwonew" class="panel-collapse collapse">
							                <div class="panel-body" id="addaddon" >
							                   	<div class="form-group col-md-3">
													<label>Addon Name <span style="color:red">*</span> </label>
													<input type="text" class="form-control"  name="addonName"  id="addonName" ng-model='addonName' ng-pattern="^/^[A-Za-z\d\s]+$/" ng-required="true">
												    <span ng-show="addaddon.addonName.$error.pattern" style="color:red">Not a Valid Name!</span>
											    </div>
											    <div class="form-group col-md-3">
													<label>Addon Rate <span style="color:red">*</span> </label>
													<input type="text" class="form-control"  name="addonRate" id="addonRate" ng-model='addonRate' ng-pattern="/^[0-9]/" ng-required="true" >
												     <span ng-show="addaddon.addonRate.$error.pattern" style="color:red">Not a Valid Price!</span>
												</div>
												<div class="form-group col-md-3">
								                     <label>Addon Status <span style="color:red">*</span> </label>
								                     <select name="addonIsActive" id="addonIsActive" class="form-control" ng-model="addonIsActive">
									                     <option value="true"  ng-selected ="">Active</option>
									                     <option value="false" ng-selected ="">InActive</option>
								                     </select>
								                 </div>
							                </div>
							                <div class="modal-footer">
<!-- 												<a href="#" data-dismiss="modal" class="btn btn-danger">Close</a> -->
                                                 <div id="addonsuccess"></div>
												<button ng-click="addAddon()" ng-disabled="addaddon.$invalid" class="btn btn-primary">{{newaddonbtn}}</button>
											</div>	
							            </div>
							        </div>
							          </form>
							        <div class="panel-group" id="accordion">
						        <div class="panel panel-default" ng-repeat="addon in propertyaddon track by $index">
						            <div class="panel-heading">
						                <h4 class="panel-title">
						                    <a data-toggle="collapse" data-parent="#accordionAddon" href="#collapse{{addon.propertyAddonId}}" ng-click="getAddon(addon.propertyAddonId);getOpen(addon.propertyAddonId);"><span class="glyphicon glyphicon-chevron-down"></span> {{addon.addonName}}</a>
						                </h4>
						            </div>
						            <form method="post" theme="simple" name="editaddon" id="addaddon">
						            <div id="collapse{{addon.propertyAddonId}}" class="panel-collapse collapse">
						                <div class="panel-body" id="editaddon" >
											    <input type="hidden" value="{{addon.propertyAddonDetailId}}" id="editPropertyAddonDetailId{{$index}}" name="editPropertyAddonDetailId">
											    <input type="hidden" value="{{addon.addonPropertyId}}" id="editAddonPropertyId{{$index}}" name="editAddonPropertyId">
											    <input type="hidden" value="{{addon.propertyAddonId}}" id="editPropertyAddonId{{$index}}" name="editPropertyAddonId">
											    <div class="form-group col-md-3">
													<label>Addon Name</label>
													<input type="text" class="form-control"  name="editAddonName"  id="editAddonName{{$index}}"  value="" ng-model='addon.addonName'  ng-required="true" >
												    <span ng-show="editaddon.editAddonName.$error.pattern">Not a Valid Name</span>
												</div>
											    <div class="form-group col-md-3">
													<label>Addon Rate</label>
													<input type="text" class="form-control"  name="editAddonRate" id="editAddonRate{{$index}}" value="" ng-model='addon.addonRate | number' ng-pattern="/^[0-9]/"   ng-required="true">
												    <span ng-show="editaddon.editAddonRate.$error.pattern" >Not a Valid Price</span>
												</div>
												<div class="form-group col-md-3">
													<label>Addon Status <span style="color:red">*</span> </label>
								                     <select name="editAddonIsActive" id="editAddonIsActive{{$index}}" class="form-control" ng-model="editAddonIsActive" >
									                     <option value="true"  ng-selected ="addon.addonIsActive=='true'">Active</option>
									                     <option value="false" ng-selected ="addon.addonIsActive=='false'">InActive</option>
								                     </select>
												</div>
											</div>
											<div class="modal-footer">
											<div id="addonsuccess2{{addon.propertyAddonId}}"></div>
												<button ng-click="deleteAddon(addon.propertyAddonId)" class="btn btn-danger">Remove</button>
												<button ng-click="editAddon(addon.propertyAddonId,$index)" ng-disabled="editaddon.$invalid" class="btn btn-primary">{{updateaddonbtn}}</button>
											</div>
						                </div>
						                </form>
						        </div>
						        
						    </div>
						    </div>
						</div>
                    
                       <section>
                           <h4 class="page-header"></h4>
                           <div class="row fontawesome-icon-list">
                           <div id="roomLength"></div>
                              <div class="col-md-3 col-sm-4"></div>
                              <div class="col-md-3 col-sm-4"></div>
                              <div class="col-md-3 col-sm-4"></div>
                              <div class="col-md-3 col-sm-4">
                                 <button type="submit"  class="btn btn-primary btnnext pull-right" ng-click="getEnableAddon();">Continue</button>
                              </div>
                           </div>
                        </section>
                  </div>
               <!-- Hotel Addon Ends -->
                    <!-- Hotel Photo Begins  -->
               <div class="tab-pane {{ulotelHotelTags[0].hotelImages}}" id="tab-images">
               <div class="row">
               <div class="col-md-12">
                <div class="col-md-12">
              <h4>Add Hotel Image</h4>
              </div>
               <form name="propertyimage">
               				<div class="form-group col-md-5">
								<label>Hotel Image<span style="color:red">* (Upload Only 400 * 600 Image)</span></label>
								<input type="file" class="form-control"  ngf-select ngf-accept="'image/*'" ngf-pattern="'.jpg,.png,!.gif'" ng-model="picFile" name="photo" id="photo">
							</div>
						  	<div class="form-group col-md-5">
								<label >Image Tag <span style="color:red">*</span></label>
								    <select name="photoCategoryType" id="photoCategoryType"  ng-model="photoCategoryType" class="form-control">
                                       <option value="0">Choose...</option>
                                        <option ng-repeat="pc in photocategory" value="{{pc.photoCategoryId}}">{{pc.photoCategoryName}}</option>
                                  </select> 
							 </div>
							 <div class="form-group col-md-3">
								<label>ALT Tag Name</label>
								<input type="text" class="form-control"  name="propertyAltName"  id="propertyAltName"  value="" ng-model='propertyAltName' ng-pattern="/^[a-zA-Z0-9]/" ng-required="true" >
							    <span ng-show="propertyimage.propertyAltName.$error.pattern">Not a Valid ALT Tag Name</span>
							</div>
							 <div class="form-group col-md-2">
							 <label >Image Upload </label><br>
							 <button ng-click="uploadPropertyImage(picFile,photoCategoryType)"  class="btn btn-primary ">Upload</button>
							</div>
               </form>
               </div>
           <div id="propertyImageSuccess"></div>
						
               <!-- Room image Begins -->
                    <div class="col-md-12">
                <div class="col-md-12">
              <h4>Add Room Image</h4>
              </div>
               <form name="roomimage">
               	<div class="form-group col-md-4">
						<label>Room Image <span style="color:red">* (Upload Only 400 * 600 Image)</span></label>
						<input type="file" class="form-control"  ngf-select ngf-accept="'image/*'" ngf-pattern="'.jpg,.png,!.gif'" ng-model="accomFile" name="accomPhoto" id="accomPhoto">
				</div>
				<div class="form-group col-md-3">
					<label >Room Type<span style="color:red">*</span></label>
					<select name="imageAccommodationTypeId" id="imageAccommodationTypeId"  ng-model="imageAccommodationTypeId" class="form-control" >
                    <option ng-repeat="ai in accommodations" value="{{ai.accommodationId}}">{{ai.accommodationType}}</option>
                    </select> 
				</div>
				<div class="form-group col-md-3">
					<label >Room Tags<span style="color:red">*</span></label>
					<select name="imageAccommodationTagId" id="imageAccommodationTagId"  ng-model="imageAccommodationTagId" class="form-control" >
                    <option value="0" >Choose...</option>
                    <option ng-repeat="rc in roomcategory" value="{{rc.photoCategoryId}}">{{rc.photoCategoryName}}</option>
                    </select> 
				</div>
				<div class="form-group col-md-3">
					<label>ALT Tag Name</label>
					<input type="text" class="form-control"  name="roomAltName"  id="roomAltName"  value="" ng-model='roomAltName' ng-pattern="/^[a-zA-Z0-9]/" ng-required="true" >
				    <span ng-show="roomimage.roomAltName.$error.pattern">Not a Valid ALT Tag Name</span>
				</div>
				<div class="form-group col-md-2">
					 <label >Image Upload </label><br>
					 <button type="submit" ng-click="uploadAccommodationImage(accomFile,imageAccommodationTypeId,imageAccommodationTagId)" class="btn btn-primary ">Upload</button>
				</div>
               </form>
               </div>
               <div id="roomImageSuccess">	</div>
               <!-- Room Image ends -->
                   <!-- Image preview Section -->
               <div class="col-md-12">
               <h4>Hotel Image Preview</h4>
               <div class="col-md-3" ng-repeat="ap in photos[0].bedroom">
	               <img ng-src="{{ap.photoPath}}?w=100&h=100&fit=crop&auto=compress"></a>
	<!--                <img class="attachment-img " src="get-property-image-stream?propertyPhotoId={{ap.DT_RowId}}"  width="100" height="100" alt="attachment image"/> -->
	              <!--  <img src="https://images.pexels.com/photos/1663262/pexels-photo-1663262.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940" class="img-responsive"> -->
	               <h4>{{ap.type}}({{ap.categoryName}})</h4>
	               <input type="text" class="form-control"  name="altName"  id="altName{{ap.DT_RowId}}"  value="{{ap.altName}}" ng-model='ap.altName' ng-pattern="/^[a-zA-Z0-9]/" ng-required="true" >
	               <button type="submit" class="btn btn-danger btn-sm" ng-click="deletePhoto(ap.DT_RowId)">Remove</button>
	               <button type="submit" class="btn btn-danger btn-sm" ng-click="updatePhoto(ap.DT_RowId)">Update</button>
	               <h5>&nbsp;</h5>
               </div>
               <div class="col-md-3" ng-repeat="ap in photos[0].restroom">
               <img ng-src="{{ap.photoPath}}?w=100&h=100&fit=crop&auto=compress"></a>
<!--                <img class="attachment-img " src="get-property-image-stream?propertyPhotoId={{ap.DT_RowId}}"  width="100" height="100" alt="attachment image"/> -->
              <!--  <img src="https://images.pexels.com/photos/1663262/pexels-photo-1663262.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940" class="img-responsive"> -->
               <h4>{{ap.type}}({{ap.categoryName}})</h4>
               <input type="text" class="form-control"  name="altName"  id="altName{{ap.DT_RowId}}"  value="{{ap.altName}}" ng-model='ap.altName' ng-pattern="/^[a-zA-Z0-9]/" ng-required="true" >
               
               <button type="submit" class="btn btn-danger btn-sm" ng-click="deletePhoto(ap.DT_RowId)">Remove</button>
               <button type="submit" class="btn btn-danger btn-sm" ng-click="updatePhoto(ap.DT_RowId)">Update</button>
               <h5>&nbsp;</h5>
               </div>
               <div class="col-md-3" ng-repeat="ep in photos[0].exterior">
               <img ng-src="{{ep.photoPath}}?w=100&h=100&fit=crop&auto=compress"></a>
<!--                <img class="attachment-img " src="get-property-image-stream?propertyPhotoId={{ep.DT_RowId}}"  width="100" height="100" alt="attachment image"/> -->
              <!--  <img src="https://images.pexels.com/photos/1663262/pexels-photo-1663262.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940" class="img-responsive"> -->
               <h4>{{ep.type}}</h4>
               <input type="text" class="form-control"  name="altName"  id="altName{{ep.DT_RowId}}"  value="{{ep.altName}}" ng-model='ep.altName' ng-pattern="/^[a-zA-Z0-9]/" ng-required="true" >
               
               <button type="submit" class="btn btn-danger btn-sm" ng-click="deletePhoto(ep.DT_RowId)">Remove</button>
               <button type="submit" class="btn btn-danger btn-sm" ng-click="updatePhoto(ep.DT_RowId)">Update</button>
               <h5>&nbsp;</h5>
               </div>
               <div class="col-md-3" ng-repeat="rp in photos[0].rooms">
               <img ng-src="{{rp.photoPath}}?w=100&h=100&fit=crop&auto=compress"></a>
<!--                <img class="attachment-img " src="get-property-image-stream?propertyPhotoId={{rp.DT_RowId}}"  width="100" height="100" alt="attachment image"/> -->
              <!--  <img src="https://images.pexels.com/photos/1663262/pexels-photo-1663262.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940" class="img-responsive"> -->
               <h4>{{rp.type}}</h4>
               <input type="text" class="form-control"  name="altName"  id="altName{{rp.DT_RowId}}"  value="{{rp.altName}}" ng-model='rp.altName' ng-pattern="/^[a-zA-Z0-9]/" ng-required="true" >
               <button type="submit" class="btn btn-danger btn-sm" ng-click="deletePhoto(rp.DT_RowId)">Remove</button>
               <button type="submit" class="btn btn-danger btn-sm" ng-click="deletePhoto(rp.DT_RowId)">Update</button>
               <h5>&nbsp;</h5>
               </div>
               <div class="col-md-3" ng-repeat="fp in photos[0].restaurant">
               <img ng-src="{{fp.photoPath}}?w=100&h=100&fit=crop&auto=compress"></a>
<!--               <img class="attachment-img " src="get-property-image-stream?propertyPhotoId={{fp.DT_RowId}}"  width="100" height="100" alt="attachment image"/> -->
              <!--  <img src="https://images.pexels.com/photos/1663262/pexels-photo-1663262.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940" class="img-responsive"> -->
               <h4>{{fp.type}}</h4>
               <input type="text" class="form-control"  name="altName"  id="altName{{fp.DT_RowId}}"  value="{{fp.altName}}" ng-model='fp.altName' ng-pattern="/^[a-zA-Z0-9]/" ng-required="true" >
               <button type="submit" class="btn btn-danger btn-sm" ng-click="deletePhoto(fp.DT_RowId)">Remove</button>
               <button type="submit" class="btn btn-danger btn-sm" ng-click="updatePhoto(fp.DT_RowId)">Update</button>
               <h5>&nbsp;</h5>
               </div>
               <div class="col-md-3" ng-repeat="lp in photos[0].others">
               <img ng-src="{{lp.photoPath}}?w=100&h=100&fit=crop&auto=compress"></a>
<!--               <img class="attachment-img " src="get-property-image-stream?propertyPhotoId={{lp.DT_RowId}}"  width="100" height="100" alt="attachment image"/> -->
              <!--  <img src="https://images.pexels.com/photos/1663262/pexels-photo-1663262.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940" class="img-responsive"> -->
               <h4>{{lp.type}}</h4>
               <input type="text" class="form-control"  name="altName"  id="altName{{lp.DT_RowId}}"  value="{{lp.altName}}" ng-model='lp.altName' ng-pattern="/^[a-zA-Z0-9]/" ng-required="true" >
               <button type="submit" class="btn btn-danger btn-sm" ng-click="deletePhoto(lp.DT_RowId)">Remove</button>
               <button type="submit" class="btn btn-danger btn-sm" ng-click="updatePhoto(lp.DT_RowId)">Update</button>
               <h5>&nbsp;</h5>
               </div>
               <!-- <div class="col-md-3" ng-repeat="rop in photos[0].roofTop">
               <img ng-src="{{rop.photoPath}}?w=100&h=100&fit=crop&auto=compress"></a>
              <img class="attachment-img " src="get-property-image-stream?propertyPhotoId={{rop.DT_RowId}}"  width="100" height="100" alt="attachment image"/>
               <img src="https://images.pexels.com/photos/1663262/pexels-photo-1663262.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940" class="img-responsive">
               <h4>{{rop.type}}</h4>
               <button type="submit" class="btn btn-danger btn-sm" ng-click="deletePhoto(rop.DT_RowId)">Remove</button>
               </div> -->
               </div>
               <!-- Image preview ends -->
                <section>
                           <div class="fontawesome-icon-list">
                           <div id="roomLength"></div>
                              <div class="col-md-3 col-sm-4"></div>
                              <div class="col-md-3 col-sm-4"></div>
                              <div class="col-md-3 col-sm-4"></div>
                              <div class="col-md-3 col-sm-4">
                                 <button type="submit" ng-disabled="!photos[0].total == 5 " class="btn btn-primary btnnext pull-right" ng-click="getEnableImage();">Continue</button>
                              </div>
                           </div>
                        </section>
               </div>
               </div>
               <!-- Hotel Addon Ends -->
                       <!-- Hotel Status Begins  -->
               <div class="tab-pane {{ulotelHotelTags[0].hotelStatus}}" id="tab-status">
               <div class="row">
               <div class="col-md-12">
              <div class="col-md-12">
              <h4>Hotel Status</h4>
              </div>
               <div class="form-group col-md-4">
								                     <label>Make Live <span style="color:red">*</span> </label>
								                     <select name="hotelIsActive" id="hotelIsActive" class="form-control" ng-model="hotelIsActive" ng-required="true">
									                     <option value="" ng-selected ="property[0].hotelIsActive == 'null'">Choose Status</option>
									                     <option value="true"  ng-selected ="property[0].hotelIsActive == 'true'">Yes</option>
									                     <option value="false" ng-selected ="property[0].hotelIsActive == 'false'">No</option>
								                     </select>
								                 </div>
								                  <div class="col-md-12">
								                 <p style="color:red;">* After Filling all the details in Ulotel Hotel Profile,Please make the make live opition 'YES' to activate the hotel listing in website</p>
								                 </div>
								                
								                     <div class="form-group col-md-4">
								                     <label>Stop Sell <span style="color:red">*</span> </label>
								                     <select name="stopSellIsActive" id="stopSellIsActive" class="form-control" ng-model="stopSellIsActive" ng-required="true">
									                     <option value="" ng-selected ="property[0].stopSellIsActive == 'null'">Choose Status</option>
									                     <option value="true"  ng-selected ="property[0].stopSellIsActive == 'true'">Yes</option>
									                     <option value="false" ng-selected ="property[0].stopSellIsActive == 'false'">No</option>
								                     </select>
								                 </div>
								                 <div class="col-md-12">
								                 <p style="color:red;">* Stop Sell' opition used to make a hotel sold out in ulotel and website listing.Booking cannot be processed while hotel in stop sell</p>
								                 </div>
								                     <div class="form-group col-md-4">
								                     <label>Delist From Website <span style="color:red">*</span> </label>
								                     <select name="delistIsActive" id="delistIsActive" class="form-control" ng-model="delistIsActive" ng-required="true">
									                     <option value="" ng-selected ="property[0].delistIsActive == 'null'">Choose Status</option>
									                     <option value="true"  ng-selected ="property[0].delistIsActive == 'true'">Yes</option>
									                     <option value="false" ng-selected ="property[0].delistIsActive == 'false'">No</option>
								                     </select>
								                 </div>
								                  <div class="col-md-12">
								                 <p style="color:red;">*  This opition used to remove the hotel from our website listing.</p>
								                 </div>
								                  <div class="col-md-12">
<!-- 												<a href="#" data-dismiss="modal" class="btn btn-danger">Close</a> -->
                                                 <div id="addonsuccess"></div>
												<button ng-click="savePropertyStatus()" ng-disabled="" class="btn btn-primary">Update</button>
											</div>
               </div>
               <div id="propertyStatusSuccess"></div>
               </div>
               </div>
               <!-- Hotel Addon Ends -->
               </div>
               </div>
               </div>
      <!-- Nav Tabs Ends -->
      </div>
      </section>
   <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>
<script>
   var app = angular.module('myApp',['ngFileUpload','ui.bootstrap']);
   app.controller('customersCtrl',['$scope',
                					'Upload',
               					'$timeout',
               					'$http', function($scope,Upload,$timeout,$http) {
	   
	   $scope.addEscalationTwo = false; 
	    $scope.addEscalationThree = false; 
	    $scope.addEscalationFour = false; 
	    $scope.hotelAddEscalationTwo = function(){
	    	$scope.addEscalationTwo = true;     	
	    };
	   /*  $scope.removeEscalationFour = function(){
	    	$scope.addEscalationFour = false;     	
	    }; */
	    $scope.hotelAddEscalationThree = function(){
	    	$scope.addEscalationThree = true;     	
	    };
	    $scope.removeEscalationTwo = function(){
	    	$scope.addEscalationTwo = false;     	
	    };
	    $scope.removeEscalationThree = function(){
	    	
	    	$scope.property[0].resortManagerDesignation1 =  '';
	    	$('#resortManagerDesignation1').val('');
	    	$('#resortManagerName1').val('');
	    	$('#resortManagerEmail1').val('');
	    	$('#resortManagerContact1').val('');
	    	$scope.addEscalationTwo = false;     	
	    };
	    $scope.removeEscalationFour = function(){
	    	
	    	$scope.property[0].resortManagerDesignation2 =  '';
	    	$('#resortManagerDesignation2').val('');
	    	$('#resortManagerName2').val('');
	    	$('#resortManagerEmail2').val('');
	    	$('#resortManagerContact2').val('');
	    	$scope.addEscalationThree = false;     	
	    };
	    $scope.removeEscalationFive = function(){
	    	$scope.property[0].resortManagerDesignation3 =  '';
	    	$('#resortManagerDesignation3').val('');
	    	$('#resortManagerName3').val('');
	    	$('#resortManagerEmail3').val('');
	    	$('#resortManagerContact3').val('');
	    	/* $scope.property[0].resortManagerName3 =  '';
	    	$scope.property[0].resortManagerEmail3 =  '';
	    	$scope.property[0].resortManagerContact3 =  ''; */
	    	$scope.addEscalationFour = false;     	
	    };
	    $scope.hotelAddEscalationFour = function(){
	    	$scope.addEscalationFour = true;     	
	    };   
   
   	$("#alert-message").hide();
    $scope.getOpen = function(id){
    	$(".collapse" + id).on('show.bs.collapse', function(){
        	$(this).parent().find(".glyphicon").removeClass("glyphicon-plus").addClass("glyphicon-minus");
        }).on('hide.bs.collapse', function(){
        	$(this).parent().find(".glyphicon").removeClass("glyphicon-minus").addClass("glyphicon-plus");
        });
    };
    $scope.getMove = function(roomLength,type){
    	var alertmsg="";
    	if(roomLength > 0){
    		addonAccommodationStatus=true;
    		 addonStatus=true;
    		$('.nav-tabs > .active').next('li').find('a').trigger('click');	
    	}else{
    		if(type=="addon"){
    			alertmsg = ' <div class="alert" style="color:red;text-align:center;">Please add addon details</div>';
    			 addonStatus=true;
    		}else{
    			addonAccommodationStatus=false;
    			alertmsg = ' <div class="alert" style="color:red;text-align:center;">Please add accommodation details</div>';
    		}
            
            $('#roomLength').html(alertmsg);
            $('#roomLength').delay(2000).fadeOut('slow');
    	}
    	
    };
    $scope.getMplan = function(){
    	$('.nav-tabs > .active').next('li').find('a').trigger('click');	
    	
    };
    $scope.getNextoff = function(){
    	$('.nav-tabs > .active').next('li').find('a').trigger('click');	
    	
    };
   	//$scope.progressbar = ngProgressFactory.createInstance();
   	//$scope.progressbar.setColor("green");
       //$scope.progressbar.start();
   	
         	$timeout(function(){
        	    // $scope.progressbar.complete();
              $scope.show = true;
              $("#pre-loader").css("display","none");
          }, 2000);
         	
         	$scope.unread = function() {
   		var notifiurl = "unreadnotifications.action";
   		$http.get(notifiurl).success(function(response) {
   			$scope.latestnoti = response.data;
   		});
   		};
   
   		$("#taxValidFrom").datepicker({
            dateFormat: 'MM d, yy',
            minDate:  0,
            onSelect: function (formattedDate) {
                var date1 = $('#taxValidFrom').datepicker('getDate'); 
                var date = new Date( Date.parse( date1 ) ); 
                date.setDate( date.getDate() );        
                var newDate = date.toDateString(); 
                newDate = new Date( Date.parse( newDate ) );   
                $('#taxValidTo').datepicker("option","minDate",newDate);
                $timeout(function(){
                  //scope.checkIn = formattedDate;
                });
            }
        });
   		
   		$("#taxValidTo").datepicker({
            dateFormat: 'MM d, yy',
            minDate:  0,
            onSelect: function (formattedDate) {
                var date2 = $('#taxValidTo').datepicker('getDate'); 
                $timeout(function(){
                  //scope.checkOut = formattedDate;
                });
            }
        });
   		
   		$("#fromDate").datepicker({
            dateFormat: 'MM d, yy',
          
            //minDate:  0,
            onSelect: function (formattedDate) {
                var date1 = $('#fromDate').datepicker('getDate'); 
                var date = new Date( Date.parse( date1 ) ); 
                date.setDate( date.getDate() + 1 );        
                var newDate = date.toDateString(); 
                newDate = new Date( Date.parse( newDate ) );   
                $('#toDate').datepicker("option","minDate",newDate);
                $timeout(function(){
                	document.getElementById("fromDate").style.borderColor = "LightGray";
                });
            }
        });

        $("#toDate").datepicker({
            dateFormat: 'MM d, yy', 
          
            //minDate:  0,
            onSelect: function (formattedDate) {
                var date2 = $('#toDate').datepicker('getDate'); 
                $timeout(function(){
                	document.getElementById("toDate").style.borderColor = "LightGray";
                });
            }
        }); 
       
   		$scope.addHotelDetails = function(){
   			var fdata="&propertyContact="+$('#propertyContact').val()
   					+"&propertyEmail="+$('#propertyEmail').val()
   					+"&propertyName="+$('#propertyName').val()
   					+"&propertyRole="+$('#propertyRole').val();
   			
   			$http(
   					{
   						method : 'POST',
   						data : fdata,
   						headers : {
   							'Content-Type' : 'application/x-www-form-urlencoded'
   						},
   						url : 'add-hotel-details'
   					}).then(function successCallback(response) {
   						$scope.getPropertyRoleDetails();
   					
   			}, function errorCallback(response) {

   			}); 
   		};
   		
		$scope.addRowDetails= function(input){
			
			if(input==true){
				$("#propertyRoleName").show();
				$("#propertyRoleId").show();
				$("#propertyRoleContact").show();
				$("#propertyRoleEmail").show();
			}else{
				$("#propertyRoleName").hide();
				$("#propertyRoleId").hide();
				$("#propertyRoleContact").hide();
				$("#propertyRoleEmail").hide();
			}
		};	
		
   		$scope.getPropertyDetails = function(){
   			var url = "get-Property-details";
				$http.get(url).success(function(response) {
					$scope.propertyDetails = response.data;
				
				});
   		};
   		
   		
   		$scope.getCategoryPhotos = function(photoCategoryId) {
   			document.getElementById("photoCategoryId").value = photoCategoryId;
   			 var url = "get-category-photos?photoCategoryId="+photoCategoryId							
   			$http.get(url).success(function(response) {
   				//console.log(response);
   				$scope.catphotos = response.data;
   				//window.location.reload();
   		
   			}); 
   		};
   		
   		$scope.getAllCategoryPhotos = function()  {
   			 var url = "get-all-category-photos?propertyId="+$('#propertyId').val();					
   			$http.get(url).success(function(response) {
   				$scope.allcatphotos = response.data;
   			    
   			}); 
   		};
   		
   		
   			$scope.getCategoryPhotos1 = function() {
   			
   			//$('#adminId').val()	
   			var  photoCategoryId = document.getElementById("photoCategoryId").value;
   			 var url = "get-category-photos?photoCategoryId="+photoCategoryId	
   			$http.get(url).success(function(response) {
   				//console.log(response);
   				$scope.catphotos = response.data;
   				//window.location.reload();
   		
   			}); 
   		};
   		
   		 $scope.getPropertyList = function() {
   	        	
   		        var userId = $('#adminId').val();
   	 			var url = "get-user-properties?userId="+userId;
   	 			$http.get(url).success(function(response) {
   	 			    
   	 				$scope.props = response.data;
   	 	
   	 			});
   	 		};
   	 		
                $scope.change = function() {
          	   
   	        var propertyId = $scope.id;	
          	    var url = "change-user-property?propertyId="+propertyId;
    			$http.get(url).success(function(response) {
    				
    				 window.location = '/ulopms/dashboard'; 
    				//$scope.change = response.data;
    	
    			});
   		       
   	 		};
	 		
			$scope.nextAmenity = function(){
				 $("#amenitysuccess").hide();
				 amenityStatus=true;
				  var alertmsg = ' <div class="alert" style="color:green;text-align:center;">Your data successfully saved</div>';
		            $('#amenitysuccess').html(alertmsg);
		            $("#amenitysuccess").fadeTo(2000, 500).slideUp(500, function(){
		                $("#amenitysuccess").slideUp(500);
		                $('.nav-tabs > .active').next('li').find('a').trigger('click');	
		                 });
			}
			
			
			$scope.nextImagesTab = function(imageSize0,imageSize1,imageSize2,imageSize3){
				
				if(imageSize0>=5 && imageSize1>=5 && imageSize2>=5 && imageSize3>=5){
					imageStatus=true;
					 $("#imagesuccess").hide();
					  var alertmsg = ' <div class="alert" style="color:green;text-align:center;">Your data successfully saved.</div>';
			            $('#imagesuccess').html(alertmsg);
			            $("#imagesuccess").fadeTo(2000, 500).slideUp(500, function(){
			                $("#imagesuccess").slideUp(500);
			                $('.nav-tabs > .active').next('li').find('a').trigger('click');	
			                 });

				} else if(imageSize0 == undefined || imageSize1 == undefined || imageSize2 == undefined || imageSize3 == undefined){
					imageStatus=false;
					 $("#imageLength").hide();
					  var alertmsg = ' <div class="alert" style="color:red;text-align:center;">Please add minimum 5 images in each category</div>';
			            $('#imageLength').html(alertmsg);
			            $("#imageLength").fadeTo(2000, 500).slideUp(500, function(){
			                $("#imageLength").slideUp(500);
			                 });  
					
				} else {
					imageStatus=false;
					 $("#imageLength").hide();
					  var alertmsg = ' <div class="alert" style="color:red;text-align:center;">Please add minimum 5 images in each category</div>';
			            $('#imageLength').html(alertmsg);
			            $("#imageLength").fadeTo(2000, 500).slideUp(500, function(){
			                $("#imageLength").slideUp(500);
			            });  
			            //$('#imageLength').delay(2000).fadeOut('slow');
			            //$("imageLength").show().delay(200).addClass("in").fadeOut(1000);
				};
			};
			
			
			$scope.getPropertyRoles = function() {
				
				var url = "get-hotelier-roles";
				$http.get(url).success(function(response) {
					$scope.hotelierRoles = response.data;
					
			});
		};
		
		$scope.getPropertyContractTypes = function() {
			
			var url = "get-contract-types";
			$http.get(url).success(function(response) {
				$scope.propertyContractTypes = response.data;
				
		});
	};
   		

	$scope.getContractTypesCheck = function(){
		var url = "get-contract-types-check";
		$http.get(url).success(function(response) {
			$scope.enableContractType = response.data;
			
			$scope.changeNetRateType($scope.enableContractType[0].netRateContractTypeId);
		});
	};
	        
			 $scope.editPlan = function(id,type,name,tariff,status,accommId,indx,symbolType){
				 
		         $scope.rateplanlist[indx].ratePlanId=id;
		         $scope.rateplanlist[indx].ratePlanType = type; 
		         $scope.rateplanlist[indx].ratePlanTariff = tariff; 
		         $scope.rateplanlist[indx].planIsActive = status; 
		         $scope.rateplanlist[indx].ratePlanSymbolType= symbolType;
		         $scope.rateplanlist[indx].ratePlanName = name; 
		         $scope.rateplanlist[indx].accommodationId=accommId;
			};
			 
			     $scope.updaterateplanbtn = "Update";
				$scope.editRatePlan = function(id){
								
					var text =  '{"array":' + JSON.stringify($scope.rateplanlist) + '}';
					var fdata = JSON.parse(text);		
					 $scope.updaterateplanbtn = "Please Wait...";
					$http(
							{
								method : 'POST',
								data : fdata,
								headers : {
									'Content-Type': 'application/json; charset=utf-8'
								},
								url : 'edit-rate-plan'
							}).then(function successCallback(response) {
								$scope.getRatePlanList(id);
								 $scope.updaterateplanbtn = "Update";
								 $("#rateplansuccess2"+id).hide();
								  var alertmsg = ' <div class="alert" style="color:green;text-align:center;">Your data updated successfully saved.</div>';
						            $('#rateplansuccess2'+id).html(alertmsg);
						            $("#rateplansuccess2"+id).fadeTo(2000, 500).slideUp(500, function(){
						                $("#rateplansuccess2"+id).slideUp(500);
						            });
					}, function errorCallback(response) {
						$("#rateplansuccess2"+id).hide();
						 $scope.updaterateplanbtn = "Update";
						  var alertmsg = ' <div class="alert" style="color:green;text-align:center;">process failed, please try again.</div>';
				            $('#rateplansuccess2'+id).html(alertmsg);
				            $("#rateplansuccess2"+id).fadeTo(2000, 500).slideUp(500, function(){
				                $("#rateplansuccess2"+id).slideUp(500);
				            });
					});
	
				};
			
			$scope.addRatePlan = function(){
				
				var fdata = "&ratePlanName=" + $('#planName').val()
				+ "&ratePlanType=" + $('#planType').val() 
				+ "&strStartDate=" + $('#fromDate').val() 
				+ "&strEndDate=" + $('#toDate').val();
				
				$http(
						{
							method : 'POST',
							data : fdata,
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							url : 'add-rate-plan'
						}).then(function successCallback(response) {
							 //window.location.reload(); 
				}, function errorCallback(response) {
					
				});

			};
			
			$scope.getRatePlanList = function(rowId) {
				var url = "get-rate-plan-list?accommodationId="+rowId;
				$http.get(url).success(function(response) {
				    console.log(response);
				   
					$scope.rateplanlist = response.data;
		
				});
				
			};
			
			$scope.getPropertyRoleDetails = function() {
				var url = "get-all-property-details";
				$http.get(url).success(function(response) {
					$scope.propertyroledetails = response.data;
		
				});
			};
		$scope.getAccommodationAmenities1 = function() {
				
				//$('#adminId').val()			
				var accommodationId = $('#accommodationId').val();
	
				//document.getElementById("accommodationId").value = accommodationId;
				var url = "get-accommodation-amenities1?accommodationId="+accommodationId;
				$http.get(url).success(function(response) {
					//console.log(response);
					$scope.accamenities1 = response.data;
			
				});
			}; 
		
		$scope.getTagAmenities = function(tagId) {
				
				var url = "get-tag-amenities?tagId="+tagId;
				$http.get(url).success(function(response) {
					$scope.tagamenities = response.data;
									
				});
			}; 
			
			$scope.changeEditNetRateType = function(rowid){
				if(rowid==0){
					$('#editNetRateRevenueId').hide();
					
				}else{
					if(rowid==1){ //Minimum Guarantee
						$('#editNetRateRevenueId').show();
					}else if(rowid==2){ //Crown
						$('#editNetRateRevenueId').show();
					}else if(rowid==3){//Net Rate Revenue
						$('#editNetRateRevenueId').show();
					}else if(rowid==4){// Online Management
						$('#editNetRateRevenueId').hide();
					}else if(rowid==5){// Top line Revenue share
						$('#editNetRateRevenueId').hide();
					}
				}
			};
			
			$scope.changeNetRateType = function(rowid){
				if(rowid==0){
					$('#netRateRevenueId').hide();
					
				}else{
					if(rowid==1){ //Minimum Guarantee
						$('#netRateRevenueId').show();
					}else if(rowid==2){ //Crown
						$('#netRateRevenueId').show();
					}else if(rowid==3){//Net Rate Revenue
						$('#netRateRevenueId').show();
					}else if(rowid==4){// Online Management
						$('#netRateRevenueId').hide();
					}else if(rowid==5){// Top line Revenue share
						$('#netRateRevenueId').hide();
					}
				}
			};
			
			$scope.changeType = function(rowid){
				
				if(rowid==0){
					$("#uloCommissionId").hide();
					$("#walkinCommissionId").hide();
					$("#onlineCommissionId").hide();
					$('#uloNetRateId').hide();
					
				}else{
					if(rowid==1){ //Minimum Guarantee
						$("#uloCommissionId").hide();
						$("#walkinCommissionId").hide();
						$("#onlineCommissionId").hide();
						$('#uloNetRateId').hide();
						
					}else if(rowid==2){ //Crown
						$("#uloCommissionId").hide();
						$("#walkinCommissionId").hide();
						$("#onlineCommissionId").hide();
						$('#uloNetRateId').hide();
						
					}else if(rowid==3){//Net Rate Revenue
						$("#uloCommissionId").hide();
						$("#walkinCommissionId").hide();
						$("#onlineCommissionId").show();
						$('#uloNetRateId').show();
						
						
					}else if(rowid==4){// Online Management
						$("#walkinCommissionId").hide();
						$("#onlineCommissionId").hide();
						$('#uloNetRateId').hide();
						$("#uloCommissionId").show();
						
						
					}else if(rowid==5){// Top line Revenue share
						$("#walkinCommissionId").show();
						$("#onlineCommissionId").show();
						$('#uloNetRateId').hide();
						$("#uloCommissionId").hide();
						
					}
					
				}
			};
			
				
				$scope.getUpdateProperty = function(){
					var url = "update-property";
					$http.get(url).success(function(response) {
						$scope.getPropertyAddon();
			
					});
				};
				
/* Hotel Profile Function Starts Here */
  
 	    $scope.rateplanlist = [];
        //$scope.propertyactive=false;
        var policyStatus=false;
        var amenityStatus=false;
        var imageStatus=false;
        var addonStatus=false;
        var addonAccommodationStatus=false;
        var propertyStatus=false;
        var propertyContractTypeId=1;
        
   		$scope.getProperty = function() {
   			
   			var url = "get-property";
   			$http.get(url).success(function(response) {
   				//console.log(response);
   				$scope.property = response.data;
   				propertyContractTypeId=$scope.property[0].contractTypeId;
   				var propertyContact = $scope.property[0].propertyContact;
   				if(propertyContact.length ==10){
   					$('#contactType').val(1);	
   				}else if(propertyContact.length > 10){
   					$('#contactType').val(2);	
   				}
   				
//   				$scope.changeType(propertyContractTypeId);
   			});
   		};
   		
   		
   		$scope.getPropertyTags = function(){
			var url = "get-property-tags";
			$http.get(url).success(function(response) {
				$scope.propertyTags = response.data;
				 var newdata = {'checked':true};
			 	for(i=0;i<$scope.propertyTags.length;i++){
					//if($scope.propertyTags[i].status == false){
						$scope.propertyTags[i].push(newdata);
					//}
						
				} 
				/* $scope.propertyTags.map((e) = {
				    e.status == "true" ? e.status = true : e.status = false
				}); */
			});
		};
   		
		$scope.profilebtn =  "Save & Continue";
		var maxChk = 2;
		$scope.checkedb = function() {
		      return $scope.propertyTags.filter(
		      	function(pt) {
		      		
		      		return pt.status; }).length >= maxChk;
		      		
		    };
	
   		$scope.editProperty = function() {
   		var i=0;
   		var checkbox_value = "";
   		$("input:checkbox[name=tagCheckbox]:checked").each(function(){
   			i++;
   			checkbox_value += $(this).val() + ",";
   		});
   		if(i==2){
   		/* 
		     $("#tagCheckbox").each(function () {
		        var ischecked = $(this).is(":checked");
		        if (ischecked) {
		            checkbox_value += $(this).val() + ",";
		        }
		    }); */
   	
   			var fdata="&address1="+ $('#address1').val()
   					+ "&address2=" + $('#address2').val()
   					+"&city="+ $('#city').val()
   					+"&latitude="+$('#latitude').val()
   					+"&longitude="+$('#longitude').val()
   					+"&propertyTypeId=" + $('#propertyTypeId').val()
   					+"&propertyContact="+$('#propertyContact').val()
   					+"&propertyEmail="+$('#propertyEmail').val()
   					+"&propertyName="+$('#propertyName').val()
   					+"&resortManagerName="+$('#resortManagerName').val()
   					+"&resortManagerEmail="+$('#resortManagerEmail').val()
   					+"&resortManagerContact="+$('#resortManagerContact').val()
   					+"&resortManagerDesignation="+$('#escalationName').val()
   					+"&resortManagerName1="+$('#resortManagerName1').val()
   					+"&resortManagerEmail1="+$('#resortManagerEmail1').val()
   					+"&resortManagerContact1="+$('#resortManagerContact1').val()
   					+"&resortManagerDesignation1="+$('#escalationName1').val()
   					+"&resortManagerName2="+$('#resortManagerName2').val()
   					+"&resortManagerEmail2="+$('#resortManagerEmail2').val()
   					+"&resortManagerContact2="+$('#resortManagerContact2').val()
   					+"&resortManagerDesignation2="+$('#escalationName2').val()
   					+"&reservationManagerEmail="+$('#reservationManagerEmail').val()
   					+"&contractManagerName="+$('#contractManagerName').val()
   					+"&contractManagerEmail="+$('#contractManagerEmail').val()
   					+"&contractManagerContact="+$('#contractManagerContact').val()
   					+"&revenueManagerName="+$('#revenueManagerName').val()
   					+"&revenueManagerEmail="+$('#revenueManagerEmail').val()
   					+"&revenueManagerContact="+$('#revenueManagerContact').val()
   					+"&zipCode="+$('#zipCode').val() 
   					+"&countryCode="+$('#countryCode').val()
   					+"&payAtHotelStatus="+$('#payAtHotelStatus').val()
   					+"&stateCode="+$('#stateCode').val()
					+"&propertyGstNo="+$('#propertyGstNo').val()
					+"&propertyCommission="+$('#propertyCommission').val()
   					+"&routeMap="+$('#routeMap').val()
   					+"&propertyReviewLink="+$('#propertyReviewLink').val()
   					+"&tripadvisorReviewLink="+$('#tripAdvisorLink').val()
   					+"&checkedPropertyTag="+checkbox_value;
 	
   			var propertyType = $('#propertyTypeId').val();
   			var routeMap= $('#routeMap').val();
   			$scope.profilebtn =  "Please Wait...";
			if (propertyType == ''){
				
				document.getElementById('propertyTypeId').focus();
			}
			else if (routeMap == ''){
				document.getElementById('routeMap').focus();
			}
			
               else{
   			
   		$http(
   					{
   						method : 'POST',
   						data : fdata,
   						headers : {
   							'Content-Type' : 'application/x-www-form-urlencoded'
   						},
   						url : 'edit-property'
   					}).then(function successCallback(response) {
   						 
   						 $scope.getProperty();
   						                                                                            
   						 $("#propertyalert").hide();
   						 propertyStatus=true;
   						 
					  var alertmsg = ' <div class="alert" style="color:green;text-align:center;">Your data successfully saved.</div>';
					  $scope.profilebtn =  "Save & Continue";  
					  $('#propertyalert').html(alertmsg);
			            $("#propertyalert").fadeTo(2000, 500).slideUp(500, function(){
			                
			               // $('.nav-tabs > .active').next('li').find('a').trigger('click');	
			                 });
   						
			           // $scope.getUlotelHotelTags();
						window.location.reload();
   			}, function errorCallback(response) {
   				$scope.propertyStatus=false;
   				$scope.profilebtn =  "Save & Continue";
   			}); 
               }
   		}else{
   	  $("#propertyTagAlert").hide();
			 
	  var alertmsg = ' <span class="alert" style="color:red;text-align:center;font-size:10pt">You Must Choose 2 Hotel Tags</span>';
	  $('#propertyTagAlert').html(alertmsg);
        $("#propertyTagAlert").fadeTo(2000, 500).slideUp(500, function(){
            
           // $('.nav-tabs > .active').next('li').find('a').trigger('click');	
             });
   		}
   
   		};
   		
		$scope.getPropertyTypes = function() {
   			
   			var url = "get-property-types";
   			$http.get(url).success(function(response) {
   				//console.log(response);
   				$scope.propertytypes = response.data;
   			
   			});
   		};
   		
   		$scope.getStates = function() {
   			
   			var url = "get-states";
   			$http.get(url).success(function(response) {
   				//console.log(response);
   				$scope.states = response.data;
   			
   			});
   		};
   		
   		$scope.getCountries = function() {
   			
   			var url = "get-countries";
   			$http.get(url).success(function(response) {
   				//console.log(response);
   				$scope.countries = response.data;
   			
   			});
   		};

/* Hotel Profile Function End Here  */
  
 /* Room Categories Function Start Here */
 		$scope.accommodationLegth = false;
 			$scope.getAccommodations = function() {
				
					var url = "get-accommodations";
					$http.get(url).success(function(response) {
					$scope.accommodations = response.data;
					var accomlen = $scope.accommodations.length;
					for(var i=0;i<accomlen;i++){
						if($scope.accommodations[i].propertyContractTypeId == ""){
							$scope.accommodationLegth = true;
						}
					}
				});
			};
			
			$scope.getAccommodation = function(rowid) {
				var url = "get-accommodation?propertyAccommodationId="+rowid;
				$http.get(url).success(function(response) {
				    console.log(response);
					$scope.accommodation = response.data;
					//$scope.changeEditNetRateType($scope.accommodation[0].netRateContractTypeId);
				});
				
			};
			
			$scope.getEnableAccommodation = function() {
				var url = "get-enable-accommodation";
				$http.get(url).success(function(response) {
				    console.log(response);
					$scope.accommodation = response.data;
					//$scope.getUlotelHotelTags();
					window.location.reload();
					//$scope.changeEditNetRateType($scope.accommodation[0].netRateContractTypeId);
				});
				
			};
			
			$scope.addroombtn = "Add";  
	        
			$scope.addAccommodation = function(){
				 
				var fdata = "propertyContractTypeId=" + $('#propertyContractType').val()
				+ "&accommodationType=" + $('#accommodationType').val()
				+ "&noOfUnits=" + $('#noOfUnits').val()
				+ "&minOccupancy=" + $('#minOccupancy').val()
				+ "&maxOccupancy=" + $('#maxOccupancy').val()
				+ "&noOfAdults=" + $('#noOfAdults').val()
				+ "&noOfChild=" + $('#noOfChild').val();
				
				$scope.addroombtn = "Please Wait";
				$http(
						{
							method : 'POST',
							data : fdata,
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							url : 'add-accommodation'
						}).then(function successCallback(response) {
							
							$scope.addedAccommodation = response.data;
                            
							var accommodationId = $scope.addedAccommodation.data[0].accommodationId;
							
				            $scope.getAccommodations();
				            document.getElementById("propertyInventory").reset();
				           
				            $scope.addroombtn = "Add";
				            $("#addAccommodationsuccess").hide();
							  var alertmsg = ' <div class="alert" style="color:green;text-align:center;">Your data successfully saved.</div>';
					            $('#addAccommodationsuccess').html(alertmsg);
					            $("#addAccommodationsuccess").fadeTo(2000, 500).slideUp(500, function(){
					                $("#addAccommodationsuccess").slideUp(500);
					     					
					                 });
				}, function errorCallback(response) {
					$scope.addroombtn = "Add";
				});

			};
			
			$scope.updateroombtn = "Update";
			$scope.editAccommodation = function(id,indx){
				var contractType = $('#editContractType'+indx).val();
				if(contractType != ""){ 
				var fdata = "accommodationType=" + $('#editAccommodationType'+indx).val()
				+ "&propertyContractTypeId=" + $('#editContractType'+indx).val()
				+ "&noOfUnits=" + $('#editNoOfUnits'+indx).val()
				+ "&minOccupancy=" + $('#editMinOccupancy'+indx).val()
				+ "&maxOccupancy=" + $('#editMaxOccupancy'+indx).val()
				+ "&noOfAdults=" + $('#editNoOfAdults'+indx).val()
				+ "&noOfChild=" + $('#editNoOfChild'+indx).val()
				+ "&propertyAccommodationId=" + id;
				$scope.updateroombtn = "Please Wait";	
				$http(
						{
							method : 'POST',
							data : fdata,
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							url : 'edit-accommodation'
						}).then(function successCallback(response) {
							
							 
							 //$scope.getAccommodation(id);
							 $scope.getAccommodations();
							 $scope.updateroombtn = "Update";
							  var alertmsg = ' <div class="alert" style="color:green;text-align:center;">Your data successfully saved.</div>';
					            $('#accommodationsuccess'+id).html(alertmsg);
					            $("#accommodationsuccess"+id).fadeTo(2000, 500).slideUp(500, function(){
					                $("#accommodationsuccess"+id).slideUp(500);
					     					
					                 });
				}, function errorCallback(response) {
					$scope.updateroombtn = "Update";
					$("#accommodationsuccess"+id).hide();
					var alertmsg = ' <div class="alert" style="color:green;text-align:center;">process failed, please try again.</div>';
		            $('#accommodationsuccess'+id).html(alertmsg);
		            $("#accommodationsuccess"+id).fadeTo(2000, 500).slideUp(500, function(){
		                $("#accommodationsuccess"+id).slideUp(500);
		     					
		                 });
				});
			    }else{
				   $("#accommodationsuccess"+id).hide();
					var alertmsg = ' <div class="alert" style="color:green;text-align:center;">Please Choose contract Type,try again Later.</div>';
		            $('#accommodationsuccess'+id).html(alertmsg);
		            $("#accommodationsuccess"+id).fadeTo(2000, 500).slideUp(500, function(){
		                $("#accommodationsuccess"+id).slideUp(500);
		     					
		                 });	
				} 

			};
			
			$scope.deleteAccommodation = function(rowid) {
            	
            	var check = confirm("Are you sure you want to delete this Category?");
			if( check == true){
               
				var url = "delete-accommodation?propertyAccommodationId="+rowid;
				$http.get(url).success(function(response) {
				    $scope.getAccommodations();
		
				});
			}
				
			};
			
			$scope.getContractTypes = function(){
				var url = "get-new-contract-types";
				$http.get(url).success(function(response) {
					$scope.contractType = response.data;
		
				});
			};
 
 
 /* Room Categories Function End Here */
 
 /* Rate plan Function start here */
 
 			
 				$scope.getEnableRate = function() {
				//document.getElementById("accommodationId").value = rowId;
				var url = "get-enable-rate";
				$http.get(url).success(function(response) {
					$scope.accamenities = response.data;
					//$scope.getUlotelHotelTags();
					window.location.reload();
				});
			}; 
		     
		     $scope.changeBSureMetroTaxType = function(id){
		  	   var purchaseRate = $('#bSureMetroPurchase'+id).val();
		  	   var taxType = $('#bSureMetroTaxType'+id).val();
		  	   var taxPercent = 0;
		  	   if(purchaseRate <= 999){
		  		  taxPercent = 0; 
		  	   }else if(purchaseRate >= 1000 && purchaseRate <=2500){
		  		   taxPercent = 12; 
		  	   }else if(purchaseRate >=2501){
		  		   taxPercent = 18;
		  	   }
		  	  if(taxType == 1){
		  		 for(var i=1;i<purchaseRate;i++){
		  			  var perc = Math.round(i * taxPercent/100);
		  			  var j = Math.round(perc + i); 
		  			  var fee = 0;
		  			   if(j == purchaseRate){
		  				  $('#bSureMetroTariff'+id).val(i);
		  				  $('#bSureMetroTax'+id).val(perc);  
		  				
		  				  if(i <= 1000){
		  					  fee = parseInt(150); 
		  				 }else if(i >= 1001 && i <= 1500){
		  					  fee = parseInt(225); 
		  				  }else if(i >= 1501){
		  					  fee = parseInt(300); 
		  				  }
		  				var fees = parseInt(parseInt(i)+fee);
		  				  $('#bSureMetroSelling'+id).val(fees);
		  				  break;
		  				  
		  			  } 
		  			
		  		  }
		  		  
		  	  } else if(taxType == 2){
	            var fee = 0;
		  		var tax = Math.round(purchaseRate * taxPercent / 100);
		  		 $('#bSureMetroTariff'+id).val(purchaseRate);
		  		 $('#bSureMetroTax'+id).val(tax);
		  		 if(purchaseRate <= 1000){
 					  fee = parseInt(150); 
 				 }else if(purchaseRate >= 1001 && purchaseRate <= 1500){
 					  fee = parseInt(225); 
 				  }else if(purchaseRate >= 1501){
 					  fee = parseInt(300);
 					  
 				  }
 					 var fees = parseInt(parseInt(purchaseRate)+fee);
 					 $('#bSureMetroSelling'+id).val(fees);
 				  
		  	  }
		     };
		   
		   $scope.changeBJoinMetroTaxType = function(id){
			   var purchaseRate = $('#bJoinMetroNet'+id).val();
		  	   var taxType = $('#bJoinMetroTaxType'+id).val();
		  	   var taxPercent = 0;
		  	   if(purchaseRate <= 999){
		  		  taxPercent = 0; 
		  	   }else if(purchaseRate >= 1000 && purchaseRate <=2500){
		  		   taxPercent = 12; 
		  	   }else if(purchaseRate >=2501){
		  		   taxPercent = 18;
		  	   }
		  	  if(taxType == 1){
		  		 for(var i=1;i<purchaseRate;i++){
		  			  var perc = Math.round(i * taxPercent/100);
		  			  var j = Math.round(perc + i); 
		  			   if(j == purchaseRate){
		  				  $('#bJoinMetroTariff'+id).val(i);
		  				  $('#bJoinMetroTax'+id).val(perc);
		  				 if(i <= 1000){
		  					  fee = parseInt(150); 
		  				 }else if(i >= 1001 && i <= 1500){
		  					  fee = parseInt(225); 
		  				  }else if(i >= 1501){
		  					  fee = parseInt(300); 
		  				  }
		  				var fees = parseInt(i+fee);
		  				  $('#bJoinMetroSelling'+id).val(fees);
		  				  break
		  			  } 
		  		  }
		  		  
		  	  } else if(taxType == 2){
		  		  var fee = 0;
		  		var tax = Math.round(purchaseRate * taxPercent / 100);
		  		 $('#bJoinMetroTariff'+id).val(purchaseRate);
		  		 $('#bJoinMetroTax'+id).val(tax);
		  		 if(purchaseRate <= 1000){
 					  fee = parseInt(150); 
 				 }else if(purchaseRate >= 1001 && purchaseRate <= 1500){
 					  fee = parseInt(225); 
 				  }else if(purchaseRate >= 1501){
 					  fee = parseInt(300); 
 				  }
		  		var fees = parseInt(parseInt(purchaseRate)+fee);
 				  $('#bJoinMetroSelling'+id).val(fees);
		  	  }
			   
		   };
		   
	 $scope.changeBJoinFlatMetroTaxType = function(id){
			   var purchaseRate = $('#bJoinFlatMetroBase'+id).val();
		  	   var taxType = $('#bJoinFlatMetroTaxType'+id).val();
		  	   var taxPercent = 0;
		  	   if(purchaseRate <= 999){
		  		  taxPercent = 0; 
		  	   }else if(purchaseRate >= 1000 && purchaseRate <=2500){
		  		   taxPercent = 12; 
		  	   }else if(purchaseRate >=2501){
		  		   taxPercent = 18;
		  	   }
		  	  if(taxType == 1){
		  		  
		  		 for(var i=1;i<purchaseRate;i++){
		  			  var perc = Math.round(i * taxPercent/100);
		  			  var j = Math.round(perc + i); 
		  			   if(j == purchaseRate){
		  				  $('#bJoinFlatMetroTariff'+id).val(i);
		  				  $('#bJoinFlatMetroTax'+id).val(perc);
		  				 if(i <= 1000){
		  					  fee = parseInt(150); 
		  				 }else if(i >= 1001 && i <= 1500){
		  					  fee = parseInt(225); 
		  				  }else if(i >= 1501){
		  					  fee = parseInt(300); 
		  				  }
		  				var fees = parseInt(i+fee);
		  				  $('#bJoinFlatMetroSelling'+id).val(fees);
		  				  break
		  			  } 
		  		  }
		  		  
		  	  } else if(taxType == 2){
	 			var fee = 0;
		  		var tax = Math.round(purchaseRate * taxPercent / 100);
		  		 $('#bJoinFlatMetroTariff'+id).val(purchaseRate);
		  		 $('#bJoinFlatMetroTax'+id).val(tax);
		  		 if(purchaseRate <= 1000){
					  fee = parseInt(150); 
				 }else if(purchaseRate >= 1001 && purchaseRate <= 1500){
					  fee = parseInt(225); 
				  }else if(purchaseRate >= 1501){
					  fee = parseInt(300); 
				  }
				var fees = parseInt(parseInt(purchaseRate)+fee);
				  $('#bJoinFlatMetroSelling'+id).val(fees);
		  	  }
			   
		   };
		   
		   $scope.changeBSureLeisureTaxType = function(id){
		
			   var weekdayPurchase = $('#bSureLeisureWeekdayPurchase'+id).val();
			   var weekendPurchase = $('#bSureLeisureWeekendPurchase'+id).val();
		  	   var taxType = $('#bSureLeisureTaxType'+id).val();
		  	   var weekdayTaxPercent = 0;
		  	   var weekendTaxPercent = 0;
		  	   var weekdayUloFee = 0;
		  	   var weekendUloFee = 0;
		  	   
		  	   if(weekdayPurchase <= 999){
		  		 weekdayTaxPercent = 0; 
		  	   }else if(weekdayPurchase >= 1000 && weekdayPurchase <=2500){
		  		 weekdayTaxPercent = 12; 
		  	   }else if(weekdayPurchase >=2501){
		  		 weekdayTaxPercent = 18;
		  	   }
		  	 if(weekendPurchase <= 999){
		  		 weekendTaxPercent = 0; 
		  	   }else if(weekendPurchase >= 1000 && weekendPurchase <=2500){
		  		 weekendTaxPercent = 12; 
		  	   }else if(weekendPurchase >=2501){
		  		 weekendTaxPercent = 18;
		  	   }
		  	  if(taxType == 1){
		  		  
		  		 for(var i=1;i<weekdayPurchase;i++){
		  			  var perc = Math.round(i * weekdayTaxPercent/100);
		  			  var total = Math.round(perc + i); 
		  			   if(total == weekdayPurchase){
		  				  $('#bSureLeisureWeekdayTariff'+id).val(i);
		  				  $('#bSureLeisureWeekdayTax'+id).val(perc);
		  				 if(i <= 1000){
		  					weekdayUloFee = parseInt(150); 
		  				 }else if(i >= 1001 && i <= 1500){
		  					weekdayUloFee = parseInt(225); 
		  				  }else if(i >= 1501){
		  					weekdayUloFee = parseInt(300); 
		  				  }
		  				var fees = parseInt(i+weekdayUloFee);
		  				  $('#bSureLeisureWeekdaySelling'+id).val(fees);
		  				  break
		  			  } 
		  		  }
		  		 
		  		for(var j=1;j<weekendPurchase;j++){
		  			  var perc = Math.round(j * weekendTaxPercent/100);
		  			  var total = Math.round(perc + j); 
		  			   if(total == weekendPurchase){
		  				  $('#bSureLeisureWeekendTariff'+id).val(j);
		  				  $('#bSureLeisureWeekendTax'+id).val(perc);
		  				 if(j <= 1000){
		  					weekendUloFee = parseInt(150); 
		  				 }else if(j >= 1001 && j <= 1500){
		  					weekendUloFee = parseInt(225); 
		  				  }else if(j >= 1501){
		  					weekendUloFee = parseInt(300); 
		  				  }
		  				var fees = parseInt(j+weekendUloFee);
		  				  $('#bSureLeisureWeekendSelling'+id).val(fees);
		  				  break
		  			  } 
		  		  }
		  		  
		  	  } else if(taxType == 2){
	 
		  		var weekdaytax = Math.round(weekdayPurchase * weekdayTaxPercent / 100);
		  		var weekendtax = Math.round(weekendPurchase * weekendTaxPercent / 100);
		  		 $('#bSureLeisureWeekdayTariff'+id).val(weekdayPurchase);
		  		 $('#bSureLeisureWeekdayTax'+id).val(weekdaytax);
		  		 $('#bSureLeisureWeekendTariff'+id).val(weekendPurchase);
		  		 $('#bSureLeisureWeekendTax'+id).val(weekendtax);
		  		 
		  		 if(weekdayPurchase <= 1000){
		  			weekdayUloFee = parseInt(150); 
				 }else if(weekdayPurchase >= 1001 && weekdayPurchase <= 1500){
					 weekdayUloFee = parseInt(225); 
				  }else if(weekdayPurchase >= 1501){
					  weekdayUloFee = parseInt(300); 
				  }
		  		var fees = parseInt(parseInt(weekdayPurchase)+weekdayUloFee);
				  $('#bSureLeisureWeekdaySelling'+id).val(fees);
				  
		  		if(weekendPurchase <= 1000){
		  			weekendUloFee = parseInt(150); 
				 }else if(weekendPurchase >= 1001 && weekendPurchase <= 1500){
					 weekendUloFee = parseInt(225); 
				  }else if(weekendPurchase >= 1501){
					  weekendUloFee = parseInt(300); 
				  }
				var fees = parseInt(parseInt(weekendPurchase)+weekendUloFee);
				  $('#bSureLeisureWeekendSelling'+id).val(fees);
		  	  }
		  	  
		     };
		     
		     
		     $scope.changeBJoinLeisureTaxType = function(id){
		 		
				   var weekdayNet = $('#bJoinLeisureWeekdayNet'+id).val();
				   var weekendNet = $('#bJoinLeisureWeekendNet'+id).val();
			  	   var taxType = $('#bJoinLeisureTaxType'+id).val();
			  	   var weekdayTaxPercent = 0;
			  	   var weekendTaxPercent = 0;
			  	   var weekdayUloFee = 0;
			  	   var weekendUloFee = 0;
			  	   
			  	   if(weekdayNet <= 999){
			  		 weekdayTaxPercent = 0; 
			  	   }else if(weekdayNet >= 1000 && weekdayNet <=2500){
			  		 weekdayTaxPercent = 12; 
			  	   }else if(weekdayNet >=2501){
			  		 weekdayTaxPercent = 18;
			  	   }
			  	 if(weekendNet <= 999){
			  		 weekendTaxPercent = 0; 
			  	   }else if(weekendNet >= 1000 && weekendNet <=2500){
			  		 weekendTaxPercent = 12; 
			  	   }else if(weekendNet >=2501){
			  		 weekendTaxPercent = 18;
			  	   }
			  	  if(taxType == 1){
			  		  
			  		 for(var i=1;i<weekdayNet;i++){
			  			  var perc = Math.round(i * weekdayTaxPercent/100);
			  			  var total = Math.round(perc + i); 
			  			   if(total == weekdayNet){
			  				  $('#bJoinLeisureWeekdayTariff'+id).val(i);
			  				  $('#bJoinLeisureWeekdayTax'+id).val(perc);
			  				 if(i <= 1000){
			  					weekdayUloFee = parseInt(150); 
			  				 }else if(i >= 1001 && i <= 1500){
			  					weekdayUloFee = parseInt(225); 
			  				  }else if(i >= 1501){
			  					weekdayUloFee = parseInt(300); 
			  				  }
			  				var fees = parseInt(i+weekdayUloFee);
			  				  $('#bJoinLeisureWeekdaySelling'+id).val(fees);
			  				  break
			  			  } 
			  		  }
			  		 
			  		for(var j=1;j<weekendNet;j++){
			  			  var perc = Math.round(j * weekendTaxPercent/100);
			  			  var total = Math.round(perc + j); 
			  			   if(total == weekendNet){

			  				  $('#bJoinLeisureWeekendTariff'+id).val(j);
			  				  $('#bJoinLeisureWeekendTax'+id).val(perc);
			  				 if(j <= 1000){
			  					weekendUloFee = parseInt(150); 
			  				 }else if(j >= 1001 && j <= 1500){
			  					weekendUloFee = parseInt(225); 
			  				  }else if(j >= 1501){
			  					weekendUloFee = parseInt(300); 
			  				  }
			  				var fees = parseInt(j+weekendUloFee);
			  				  $('#bJoinLeisureWeekendSelling'+id).val(fees);
			  				  break
			  			  } 
			  		  }
			  		  
			  	  } else if(taxType == 2){
		 
			  		var weekdaytax = Math.round(weekdayNet * weekdayTaxPercent / 100);
			  		var weekendtax = Math.round(weekendNet * weekendTaxPercent / 100);
			  		 $('#bJoinLeisureWeekdayTariff'+id).val(weekdayNet);
			  		 $('#bJoinLeisureWeekdayTax'+id).val(weekdaytax);
			  		 $('#bJoinLeisureWeekendTariff'+id).val(weekendNet);
			  		 $('#bJoinLeisureWeekendTax'+id).val(weekendtax);
			  		 
			  		 if(weekdayNet <= 1000){
			  			weekdayUloFee = parseInt(150); 
					 }else if(weekdayNet >= 1001 && weekdayNet <= 1500){
						 weekdayUloFee = parseInt(225); 
					  }else if(weekdayNet >= 1501){
						  weekdayUloFee = parseInt(300); 
					  }
			  		var fees = parseInt(parseInt(weekdayNet)+weekdayUloFee);
					  $('#bJoinLeisureWeekdaySelling'+id).val(fees);
					  
			  		if(weekendNet <= 1000){
			  			weekendUloFee = parseInt(150); 
					 }else if(weekendNet >= 1001 && weekendNet <= 1500){
						 weekendUloFee = parseInt(225); 
					  }else if(weekendNet >= 1501){
						  weekendUloFee = parseInt(300); 
					  }
					var fees = parseInt(parseInt(weekendNet)+weekendUloFee);
					  $('#bJoinLeisureWeekendSelling'+id).val(fees);
			  	  }
			  	  
			     };
			     
			     
			     $scope.changeBJoinFlatLeisureTaxType = function(id){
			 		
					   var weekdayBase = $('#bJoinFlatLeisureWeekdayBase'+id).val();
					   var weekendBase = $('#bJoinFlatLeisureWeekendBase'+id).val();
				  	   var taxType = $('#bJoinFlatLeisureTaxType'+id).val();
				  	 // var  taxType = 0;
				  	   var weekdayTaxPercent = 0;
				  	   var weekendTaxPercent = 0;
				  	   var weekdayUloFee = 0;
				  	   var weekendUloFee = 0;
				  	   
				  	   if(weekdayBase <= 999){
				  		 weekdayTaxPercent = 0; 
				  	   }else if(weekdayBase >= 1000 && weekdayBase <=2500){
				  		 weekdayTaxPercent = 12; 
				  	   }else if(weekdayBase >=2501){
				  		 weekdayTaxPercent = 18;
				  	   }
				  	 if(weekendBase <= 999){
				  		 weekendTaxPercent = 0; 
				  	   }else if(weekendBase >= 1000 && weekendBase <=2500){
				  		 weekendTaxPercent = 12; 
				  	   }else if(weekendBase >=2501){
				  		 weekendTaxPercent = 18;
				  	   }
				  	  if(taxType == 1){
				  		  
				  		 for(var i=1;i<weekdayBase;i++){
				  			  var perc = Math.round(i * weekdayTaxPercent/100);
				  			  var total = Math.round(perc + i); 
				  			   if(total == weekdayBase){
				  				  $('#bJoinFlatLeisureWeekdayTariff'+id).val(i);
				  				  $('#bJoinFlatLeisureWeekdayTax'+id).val(perc);
				  				 if(i <= 1000){
				  					weekdayUloFee = parseInt(150); 
				  				 }else if(i >= 1001 && i <= 1500){
				  					weekdayUloFee = parseInt(225); 
				  				  }else if(i >= 1501){
				  					weekdayUloFee = parseInt(300); 
				  				  }
				  				var fees = parseInt(i+weekdayUloFee);
				  				  $('#bJoinFlatLeisureWeekdaySelling'+id).val(fees);
				  				  break
				  			  } 
				  		  }
				  		 
				  		for(var j=1;j<weekendBase;j++){
				  			  var perc = Math.round(j * weekendTaxPercent/100);
				  			  var total = Math.round(perc + j); 
				  			   if(total == weekendBase){
				  				  $('#bJoinFlatLeisureWeekendTariff'+id).val(j);
				  				  $('#bJoinFlatLeisureWeekendTax'+id).val(perc);
				  				 if(j <= 1000){
				  					weekendUloFee = parseInt(150); 
				  				 }else if(j >= 1001 && j <= 1500){
				  					weekendUloFee = parseInt(225); 
				  				  }else if(j >= 1501){
				  					weekendUloFee = parseInt(300); 
				  				  }
				  				var fees = parseInt(j+weekendUloFee);
				  				  $('#bJoinFlatLeisureWeekendSelling'+id).val(fees);
				  				  break
				  			  } 
				  		  }
				  		  
				  	  } else if(taxType == 2){
			 
				  		var weekdaytax = Math.round(weekdayBase * weekdayTaxPercent / 100);
				  		var weekendtax = Math.round(weekendBase * weekendTaxPercent / 100);
				  		 $('#bJoinFlatLeisureWeekdayTariff'+id).val(weekdayBase);
				  		 $('#bJoinFlatLeisureWeekdayTax'+id).val(weekdaytax);
				  		 $('#bJoinFlatLeisureWeekendTariff'+id).val(weekendBase);
				  		 $('#bJoinFlatLeisureWeekendTax'+id).val(weekendtax);
				  		 
				  		 if(weekdayBase <= 1000){
				  			weekdayUloFee = parseInt(150); 
						 }else if(weekdayBase >= 1001 && weekdayBase <= 1500){
							 weekdayUloFee = parseInt(225); 
						  }else if(weekdayBase >= 1501){
							  weekdayUloFee = parseInt(300); 
						  }
				  		var fees = parseInt(parseInt(weekdayBase)+weekdayUloFee);
						  $('#bJoinFlatLeisureWeekdaySelling'+id).val(fees);
						  
				  		if(weekendBase <= 1000){
				  			weekendUloFee = parseInt(150); 
						 }else if(weekendBase >= 1001 && weekendBase <= 1500){
							 weekendUloFee = parseInt(225); 
						  }else if(weekendBase >= 1501){
							  weekendUloFee = parseInt(300); 
						  }
						var fees = parseInt(parseInt(weekendBase)+weekendUloFee);
						  $('#bJoinFlatLeisureWeekendSelling'+id).val(fees);
				  	  }
				  	  
				     };
		   
		   $scope.editAccommodationRate = function(id,contractId,locationTypeId){
			var fdata;
			  if(locationTypeId == 1){ 
			   if(contractId == 7){
			   var bjoinSell = $('#bJoinMetroSelling'+id).val();
			   var bjoinNet = $('#bJoinMetroNet'+id).val();
			   var taxType = $('#bJoinMetroTaxType'+id).val();
			   var bjoinTariff = $('#bJoinMetroTariff'+id).val()
			   var bjoinTax = $('#bJoinMetroTax'+id).val();
			   var bjoinSell = $('#bJoinMetroSelling'+id).val();
			   var extraAdult = $('#bJoinMetroExtraAdult'+id).val();
			   var extraChild = $('#bJoinMetroExtraChild'+id).val();
				   fdata = "netRate="+ bjoinNet
					          + "&taxType="+ taxType
					          + "&tariff="+ bjoinTariff
					          + "&tax="+ bjoinTax
					          + "&propertyAccommodationId="+ id
					          + "&propertyContractTypeId="+ contractId
					          + "&locationTypeId="+locationTypeId
					          + "&sellingRate="+ bjoinSell
					          + "&extraAdult="+ extraAdult
					          + "&extraChild="+ extraChild;
				   
			   }else if(contractId == 6){
				   
				   var bsureSell = $('#bSureMetroSelling'+id).val();
				   var bsurePurchase = $('#bSureMetroPurchase'+id).val();
				   var taxType = $('#bSureMetroTaxType'+id).val();
				   var bsureTariff = $('#bSureMetroTariff'+id).val()
				   var bsureTax = $('#bSureMetroTax'+id).val();
				   var bsureSell = $('#bSureMetroSelling'+id).val();
				   var extraAdult = $('#bSureMetroExtraAdult'+id).val();
				   var extraChild = $('#bSureMetroExtraChild'+id).val();
				   
				   fdata = "purchaseRate="+ bsurePurchase
			          + "&taxType="+taxType
			          + "&propertyAccommodationId="+id
			          + "&propertyContractTypeId="+contractId
			          + "&locationTypeId="+locationTypeId
			          + "&tariff="+bsureTariff
			          + "&tax="+bsureTax
			          + "&sellingRate="+bsureSell
			          + "&extraAdult="+extraAdult
			          + "&extraChild="+extraChild;
				   
			   }/* else if(contractId == 8){
				   var fdata = "purchaseRate="+ $('#bSureMetroPurchase').val()
			          + "&taxType="+$('#bSureMetroTaxType').val()
			          + "&tariff="+$('#bSureMetroTariff').val()
			          + "&tax="+$('#bSureMetroTax').val()
			          + "&propertyAccommodationId="+id
			          + "&propertyContractTypeId="+contractId
			          + "&sellingRate="+$('#bSureMetroSelling').val()
			          + "&extraAdult="+$('#bSureMetroExtraAdult').val()
			          + "&extraChild="+$('#bSureMetroExtraChild').val();
			   } */else if(contractId == 9){
				   
				   var  bjoinFlatPurchase = $('#bJoinFlatMetroBase'+id).val();
				   var taxType = $('#bJoinFlatMetroTaxType'+id).val();
				   var bjoinFlatTariff = $('#bJoinFlatMetroTariff'+id).val()
				   var bjoinFlatTax = $('#bJoinFlatMetroTax'+id).val();
				   var bjoinFlatSell = $('#bJoinFlatMetroSelling'+id).val();
				   var extraAdult = $('#bJoinFlatMetroExtraAdult'+id).val();
				   var extraChild = $('#bJoinFlatMetroExtraChild'+id).val();
				   var commission = $('#bJoinFlatMetroCommission'+id).val();
				   fdata = "baseAmount="+ bjoinFlatPurchase
			          + "&taxType="+taxType
			          + "&propertyAccommodationId="+id
			          + "&propertyContractTypeId="+contractId
			          + "&locationTypeId="+locationTypeId
			          + "&tariff="+bjoinFlatTariff
			          + "&tax="+bjoinFlatTax
			          + "&sellingRate="+bjoinFlatSell
			          + "&extraAdult="+extraAdult
			          + "&extraChild="+extraChild
			          + "&propertyCommission="+commission;
			   }
			  }
			  else if(locationTypeId == 2){
				   if(contractId == 7){
					 
					   var bjoinDayNet = $('#bJoinLeisureWeekdayNet'+id).val();
					   var bjoinEndNet = $('#bJoinLeisureWeekendNet'+id).val();
					   var bjoinTaxType = $('#bJoinLeisureTaxType'+id).val();
					   var bjoinDayTariff = $('#bJoinLeisureWeekdayTariff'+id).val();
					   var bjoinEndTariff = $('#bJoinLeisureWeekendTariff'+id).val();
					   var bjoinDayTax = $('#bJoinLeisureWeekdayTax'+id).val();
					   var bjoinEndTax = $('#bJoinLeisureWeekendTax'+id).val();
					   var bjoinDaySell = $('#bJoinLeisureWeekdaySelling'+id).val();
					   var bjoinEndSell = $('#bJoinLeisureWeekendSelling'+id).val();
					   var extraAdult = $('#bJoinLeisureExtraAdult'+id).val();
					   var extraChild = $('#bJoinLeisureExtraChild'+id).val();
				   
					   fdata = "weekdayNetRate="+ bjoinDayNet
					   			  + "&weekendNetRate="+ bjoinEndNet
						          + "&taxType="+ bjoinTaxType
						          + "&weekdayTariff="+ bjoinDayTariff
						          + "&weekendTariff="+ bjoinEndTariff
						          + "&weekdayTax="+ bjoinDayTax
						          + "&weekendTax="+ bjoinEndTax
						          + "&propertyAccommodationId="+id
						          + "&propertyContractTypeId="+contractId
						          + "&locationTypeId="+locationTypeId
						          + "&weekendSellingRate="+ bjoinEndSell
						          + "&weekdaySellingRate="+ bjoinDaySell
						          + "&extraAdult="+ extraAdult
						          + "&extraChild="+ extraChild;
					   
				   }else if(contractId == 6){
					   
					   var bsureDayPurchase = $('#bSureLeisureWeekdayPurchase'+id).val();
					   var bsureEndPurchase = $('#bSureLeisureWeekendPurchase'+id).val();
					   var bsureTaxType = $('#bSureLeisureTaxType'+id).val();
					   var bsureDayTariff = $('#bSureLeisureWeekdayTariff'+id).val();
					   var bsureEndTariff = $('#bSureLeisureWeekendTariff'+id).val();
					   var bsureDayTax = $('#bSureLeisureWeekdayTax'+id).val();
					   var bsureEndTax = $('#bSureLeisureWeekendTax'+id).val();
					   var bsureDaySell = $('#bSureLeisureWeekdaySelling'+id).val();
					   var bsureEndSell = $('#bSureLeisureWeekendSelling'+id).val();
					   var extraAdult = $('#bSureLeisureExtraAdult'+id).val();
					   var extraChild = $('#bSureLeisureExtraChild'+id).val();
					   
					   fdata = "weekdayPurchaseRate="+ bsureDayPurchase
					      + "&weekendPurchaseRate="+ bsureEndPurchase
				          + "&taxType="+ bsureTaxType
				          + "&propertyAccommodationId="+id
				          + "&propertyContractTypeId="+contractId
				          + "&locationTypeId="+locationTypeId
				          + "&weekdayTariff="+ bsureDayTariff
				          + "&weekendTariff="+ bsureEndTariff
				          + "&weekdayTax="+ bsureDayTax
				          + "&weekendTax="+ bsureEndTax
				          + "&weekdaySellingRate="+ bsureDaySell
				          + "&weekendSellingRate="+ bsureEndSell
				          + "&extraAdult="+ extraAdult
				          + "&extraChild="+ extraChild;
					   
				   }/* else if(contractId == 8){
					   var fdata = "purchaseRate="+ $('#bSureMetroPurchase').val()
				          + "&taxType="+$('#bSureMetroTaxType').val()
				          + "&tariff="+$('#bSureMetroTariff').val()
				          + "&tax="+$('#bSureMetroTax').val()
				          + "&propertyAccommodationId="+id
				          + "&propertyContractTypeId="+contractId
				          + "&sellingRate="+$('#bSureMetroSelling').val()
				          + "&extraAdult="+$('#bSureMetroExtraAdult').val()
				          + "&extraChild="+$('#bSureMetroExtraChild').val();
				   } */else if(contractId == 9){
					   
					   var bjoinflatDayBase = $('#bJoinFlatLeisureWeekdayBase'+id).val();
					   var bjoinflatEndBase = $('#bJoinFlatLeisureWeekendBase'+id).val();
					   var bjoinflatTaxType = $('#bJoinFlatLeisureTaxType'+id).val();
					   var bjoinflatDayTariff = $('#bJoinFlatLeisureWeekdayTariff'+id).val();
					   var bjoinflatEndTariff = $('#bJoinFlatLeisureWeekendTariff'+id).val();
					   var bjoinflatDayTax = $('#bJoinFlatLeisureWeekdayTax'+id).val();
					   var bjoinflatEndTax = $('#bJoinFlatLeisureWeekendTax'+id).val();
					   var bjoinflatDaySell = $('#bJoinFlatLeisureWeekdaySelling'+id).val();
					   var bjoinflatEndSell = $('#bJoinFlatLeisureWeekendSelling'+id).val();
					   var extraAdult = $('#bJoinFlatLeisureExtraAdult'+id).val();
					   var extraChild = $('#bJoinFlatLeisureExtraChild'+id).val();
					   var commission = $('#bJoinFlatLeisureCommission'+id).val();
				   
					   fdata = "weekdayBaseAmount="+ bjoinflatDayBase
					   	  + "&weekendBaseAmount="+ bjoinflatEndBase
				          + "&taxType="+ bjoinflatTaxType
				          + "&propertyAccommodationId="+id
				          + "&propertyContractTypeId="+contractId
				          + "&locationTypeId="+locationTypeId
				          + "&weekdayTariff="+ bjoinflatDayTariff
				          + "&weekendTariff="+ bjoinflatEndTariff
				          + "&weekdayTax="+ bjoinflatDayTax
				          + "&weekendTax="+ bjoinflatEndTax
				          + "&weekdaySellingRate="+ bjoinflatDaySell
				          + "&weekendSellingRate="+ bjoinflatEndSell
				          + "&extraAdult="+ extraAdult
				          + "&extraChild="+ extraChild
				          + "&propertyCommission="+ commission;
				   } 
			  }
			//  alert(fdata)
					$http(
							{
								method : 'POST',
								data : fdata,
								headers : {
									'Content-Type' : 'application/x-www-form-urlencoded'
								},
							 url : 'add-property-accommodation-rate'
							}).then(function successCallback(response) {
								//window.location.reload();
								$scope.updatehamenitybtn = "Update";
								
								//$('.nav-tabs > .active').next('li').find('a').trigger('click');	
								 $("#accommodationsuccessRate"+id).hide();
								  var alertmsg = ' <div class="alert" style="color:green;text-align:center;">Your Price Updated sucessfully</div>';
						            $('#accommodationsuccessRate'+id).html(alertmsg);
						            $("#accommodationsuccessRate"+id).fadeTo(2000, 500).slideUp(500, function(){
						                $("#accommodationsuccessRate"+id).slideUp(500);
						                 });  
						            $scope.getAccommodations();
					}, function errorCallback(response) {
					});
			   
		   };
		   
	/* Rate Plan Function End Here */
 
	/* Amenity Function Start Here */
	
		$scope.getPropertyAmenities = function() {
				var url = "get-property-amenities";
				$http.get(url).success(function(response) {
					$scope.propertyAmenities = response.data;
				});
			};
			
			$scope.propAmenitySize = false;
			$scope.accomAmenitySize = false;
// 			$scope.tagAmenitySize = false;
			$scope.getAllAmenitySize = function() {
				var url = "get-all-amenity-size";
				$http.get(url).success(function(response) {
					$scope.amenitySize = response.data;
					var totalSize = $scope.amenitySize.length; 
					$scope.propAmenitySize = false;
					$scope.accomAmenitySize = false;
// 					$scope.tagAmenitySize = false;
					for(var i=0;i<totalSize;i++){
							if($scope.amenitySize[i].propertySize != 6){
								$scope.propAmenitySize = true;	
								break;
							}
					        var accomSize = $scope.amenitySize[i].accommodation.length;	
					        for(var j=0;j<accomSize;j++){
								if($scope.amenitySize[i].accommodation[j].accommodationSize != 6)
									$scope.accomAmenitySize = true;
									break;
							}
					        /* var tagSize = $scope.amenitySize[i].propertyTags.length;  
					        for(var k=0;k<tagSize;k++){
								if($scope.amenitySize[i].propertyTags[k].tagSize != 6){
									$scope.tagAmenitySize = true;
									break;
							} 
					}*/
					}
				
				});
			};
			
			var maxamenity = 6;
			$scope.checkedAmenity = function() {
			      return $scope.propertyAmenities.filter(
			    			
			      	function(pa) {
			      		
			      		return pa.status; }).length >= maxamenity;
			      	
			    };
			    
			    $scope.checkedAmenity5 = function() {
			    	$scope.count = false;
			    	$scope.count1 = 0;
			    	$scope.propertyAmenities.filter(function(pa){
			    		 if(pa.status == true){
			    		        $scope.count1 += 1;
			    		      }
			    	})
			    	if($scope.count1 ==6){
			    		$scope.count = true;
			    	}
			    	
			    };
			    
			    $scope.checkedTagAmenityCount = function(){
			    	$scope.counttag = false;
			    	$scope.counttag1 = 0;
			    	$scope.tagamenities.filter(function(tag){
			    		 if(tag.status == true){
			    		        $scope.counttag1 += 1;
			    		      }
			    	})
			    	if($scope.counttag1 ==6){
			    		$scope.counttag = true;
			    	}	  
			    }
			    
			    $scope.checkedAccomAmenity5 = function() {
			    	$scope.countaccom = false;
			    	$scope.countaccom1 = 0;
			    	$scope.accamenities.filter(function(aca){
			    		 if(aca.status == true){
			    		        $scope.countaccom1 += 1;
			    		      }
			    	})
			    	if($scope.countaccom1 ==6){
			    		$scope.countaccom = true;
			    	}
			    	
			    }; 	
			
			$scope.updatehamenitybtn = "Update";
			$scope.savePropertyAmenities = function(){
				
				 var checkbox_value = "";
				    $("#form1 :checkbox").each(function () {
				        var ischecked = $(this).is(":checked");
				        if (ischecked) {
				            checkbox_value += $(this).val() + ",";
				        }
				    });
				    
				    if(checkbox_value == ''){
				    	checkbox_value="NA";
				    }
				    $scope.updatehamenitybtn = "Please Wait...";
				    
				 var fdata = "checkedAmenity=" + checkbox_value;
				$http(
						{
							method : 'POST',
							data : fdata,
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							url : 'save-property-amenities'
						}).then(function successCallback(response) {

							$scope.updatehamenitybtn = "Update";
							$scope.getPropertyAmenities();
							$scope.getAllAmenitySize();
							$("#propertyAmenities").hide();
							  var alertmsg = ' <div class="alert" style="color:green;text-align:center;">Your Amenities Updated sucessfully</div>';
					            $('#propertyAmenities').html(alertmsg);
					            $("#propertyAmenities").fadeTo(2000, 500).slideUp(500, function(){
					                $("#propertyAmenities").slideUp(500);
					                 });  
							
				}, function errorCallback(response) {
				});
				

			};
			$scope.updatetagamenitiesbtn="Update";
			$scope.saveTagAmenities = function(id){
				
				 var checkbox_value = "";
				 $('#formTag'+id+' :checkbox').each(function () {
	                   var ischecked = $(this).is(":checked");
					        if (ischecked) {
					            checkbox_value += $(this).val() + ",";
					        }
					    });
				    
				    if(checkbox_value == ''){
				    	checkbox_value="NA";
				    }
				    $scope.updatetagamenitiesbtn = "Please Wait...";
				    
				    
				 var fdata = "checkedAmenity=" + checkbox_value+"&tagId="+id;
				$http(
						{
							method : 'POST',
							data : fdata,
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							url : 'save-tag-amenities'
						}).then(function successCallback(response) {
							$scope.updatetagamenitiesbtn = "Update";
							//$scope.getTagAmenities(id);
							$scope.getAllAmenitySize();
							$("#tagAmenitiesSuccess"+id).hide();
							  var alertmsg = ' <div class="alert" style="color:green;text-align:center;">Your Amenities Updated sucessfully</div>';
					            $('#tagAmenitiesSuccess'+id).html(alertmsg);
					            $("#tagAmenitiesSuccess"+id).fadeTo(2000, 500).slideUp(500, function(){
					                $("#tagAmenitiesSuccess"+id).slideUp(500);
					                 });		
							
				}, function errorCallback(response) {
				});
				

			};
			
			
			$scope.getAccommodationAmenities = function(rowId) {
				//document.getElementById("accommodationId").value = rowId;
				var url = "get-accommodation-amenities?accommodationId="+rowId;
				$http.get(url).success(function(response) {
					$scope.accamenities = response.data;
			
				});
			}; 
			
			$scope.getEnableAmenities = function() {
				//document.getElementById("accommodationId").value = rowId;
				var url = "get-enable-amenities";
				$http.get(url).success(function(response) {
					$scope.accamenities = response.data;
					//$scope.getUlotelHotelTags();
					window.location.reload();
				});
			}; 
			
			var maxAccamamenity = 6;
			var maxTagamamenity=6;
			$scope.checkedAccomAmenity = function() {
			      return $scope.accamenities.filter(
			      	function(accam) {
			      		return accam.status; }).length >= maxAccamamenity;
			      		
			    };
			    
			    $scope.checkedTagAmenity = function() {
				      return $scope.tagamenities.filter(
				      	function(tagam) {
				      		return tagam.status; }).length >= maxTagamamenity;
				      		
				    };
			
			    $scope.updateAamenitybtn = "Update";
			$scope.saveAccommodationAmenities = function(id){
				
				 var checkbox_value = "";
				    $('#form'+id+' :checkbox').each(function () {
                   var ischecked = $(this).is(":checked");
				        if (ischecked) {
				            checkbox_value += $(this).val() + ",";
				        }
				    });
				   /*  
				    if(checkbox_value == ''){
				    	checkbox_value="NA";
				    }
				 */
				    var fdata = "checkedAmenity1=" + checkbox_value
					+ "&accommodationId=" + id;
				    $scope.updateAamenitybtn = "Please Wait..";
				    $http(
							{
								method : 'POST',
								data : fdata,
								headers : {
									'Content-Type' : 'application/x-www-form-urlencoded'
								},
								url : 'save-accommodation-amenities'
							}).then(function successCallback(response) {
								 $scope.updateAamenitybtn = "Update";
								 $scope.getAllAmenitySize();
								  var alertmsg = ' <div class="alert" style="color:green;text-align:center;">Amenities add sucessfully</div>';
						            $('#amenitysuccess'+id).html(alertmsg);
						            $("#amenitysuccess"+id).fadeTo(2000, 500).slideUp(500, function(){
						                $("#amenitysuccess"+id).slideUp(500);
						                 }); 
								//window.location.reload(); 
								//$scope.getAccommodationAmenities1();
								
					}, function errorCallback(response) {
						

					});


			};
	
/* Amenity Function End Here */

/* Hotel Policy Function Start Here */

			$scope.getEscapeHtml =  function(unsafe){
				
				 return unsafe
		         .replace(/&/g, "and")
		         .replace(/&nbsp;/g, " ")
		         .replace(/&lt;/g, "<")
		         .replace(/&gt;/g, ">")
		         .replace(/&quot;/g, "\"")
		         //.replace(/>/g, "&gt;")
		         //.replace(/'/g, "&#039;")
		         //.replace(/"/g, "'")
				 .replace(/%/g, "percent");
		        
				
			};
	   		
	   		$scope.editPropertyPolicy = function() {
	   			
	   		 var hotelPolicy= CKEDITOR.instances.propertyHotelPolicy.getData();
	   		 var ChangeHotelPolicy = $scope.getEscapeHtml(hotelPolicy);
	   		 
	   		var cancellationPolicy= CKEDITOR.instances.propertyCancellationPolicy.getData();
	  		 var ChangeCancellationPolicy = $scope.getEscapeHtml(cancellationPolicy);
	  		 
	  		 var refundPolicy= CKEDITOR.instances.propertyRefundPolicy.getData();
	  		 var ChangeRefundPolicy = $scope.getEscapeHtml(refundPolicy); 
	  		 
	  		var gigiPolicy= CKEDITOR.instances.propertyGigiPolicy.getData();
			 var ChangeGigiPolicy = $scope.getEscapeHtml(gigiPolicy); 
			 
	   			/* var propertyHotelPolicyj= CKEDITOR.instances.propertyHotelPolicy.getData();
	   			var propertyRefundPolicy= CKEDITOR.instances.propertyStandardPolicy.getData();
	   			var propertyCancellationPolicy= CKEDITOR.instances.propertyCancellationPolicy.getData(); */
	   			
	   			var fdata="&propertyHotelPolicy="+ ChangeHotelPolicy+ "&propertyRefundPolicy=" +ChangeCancellationPolicy
	   			+"&propertyCancellationPolicy="+ ChangeRefundPolicy+"&propertyGigiPolicy="+ChangeGigiPolicy;
	   			
	   		    if(ChangeHotelPolicy == "" || ChangeCancellationPolicy == '' || ChangeRefundPolicy == '' || ChangeGigiPolicy == '' ){
	   			 $("#policyfailed").hide();
					  var alertmsg = ' <div class="alert" style="color:red;text-align:center;">Please fill the all your given policies.</div>';
			            $('#policyfailed').html(alertmsg);
			            $("#policyfailed").fadeTo(2000, 500).slideUp(500, function(){
			                $("#policyfailed").slideUp(500);
			                //$('.nav-tabs > .active').next('li').find('a').trigger('click');	
			                 });
	   		    	
	   		    }else{
	   		 	$http(
	   					{
	   						method : 'POST',
	   						data : fdata,
	   						headers : {
	   							'Content-Type' : 'application/x-www-form-urlencoded'
	   						},
	   						url : 'edit-property-policy'
	   					}).then(function successCallback(response) {
	   						
	   						 
	   						 $scope.getProperty();
	   						//$("#alert-message").show();
	   						 $("#policysuccess").hide();
	   						 policyStatus=true;
	   							
	   						 
	   					  var alertmsg = ' <div class="alert" style="color:green;text-align:center;">Your data successfully saved.</div>';
	   			            $('#policysuccess').html(alertmsg);
	   			            $("#policysuccess").fadeTo(2000, 500).slideUp(500, function(){
	   			                $("#policysuccess").slideUp(500);
	   			                
	   			                
	   			             $('.nav-tabs > .active').next('li').find('a').trigger('click');
	   			                 });
	   			         	//$scope.getUlotelHotelTags();
	   						window.location.reload();
	   			}, function errorCallback(response) {
	   			
	   			});
	   		    }
	   			/* + "&propertyRefundPolicy=" +$('#propertyRefundPolicy').val()
	   			+"&propertyCancellationPolicy="+ $('#propertyCancellationPolicy').val()
	   				+ "&propertyChildPolicy=" +$('#propertyChildPolicy').val(); */
	   			
	   		 
	   		};
 
/* Hotel Policy Function End Here */

/*Addon function start here  */
				
				$scope.getPropertyAddon = function(){
					var url = "get-property-addon";
					$http.get(url).success(function(response) {
						$scope.propertyaddon = response.data;
					});
				};
				
				$scope.getEnableAddon = function(){
					var url = "get-enable-addon";
					$http.get(url).success(function(response) {
						$scope.propertyaddon = response.data;
						//$scope.getUlotelHotelTags();
   						window.location.reload();
					});
				};
				
				
				$scope.newaddonbtn = "Add";
				$scope.addAddon = function(){
					var fdata = "&addonName=" + $('#addonName').val()
					+ "&addonRate=" + $('#addonRate').val()
					+ "&addonIsActive=" + $('#addonIsActive').val();
					
					$scope.newaddonbtn = "Please Wait...";	
					$http(
							{
								method : 'POST',
								data : fdata,
								headers : {
									'Content-Type' : 'application/x-www-form-urlencoded'
								},
								url : 'add-addon'
							}).then(function successCallback(response) {
								
								var gdata = "&addonName=" + $('#addonName').val()
								+ "&addonRate=" + $('#addonRate').val()
								+ "&addonIsActive=" + $('#addonIsActive').val();
								$scope.newaddonbtn = "Pleae Wait...";
								$http(
										{
											method : 'POST',
											data : gdata,
											headers : {
												'Content-Type' : 'application/x-www-form-urlencoded'
											},
											url : 'add-addon-detail'
										}).then(function successCallback(response) {
											$scope.getPropertyAddon();
											$scope.newaddonbtn = "Add";
											 $("#addonsuccess").hide();
											  var alertmsg = ' <div class="alert" style="color:green;text-align:center;">Your addon successfully saved.</div>';
									            $('#addonsuccess').html(alertmsg);
									            $("#addonsuccess").fadeTo(2000, 500).slideUp(500, function(){
									                $("#addonsuccess").slideUp(500);
									               // $('.nav-tabs > .active').next('li').find('a').trigger('click');	
									                 });
								}, function errorCallback(response) {
								});
								
								
					}, function errorCallback(response) {
					});
				};
				
				
				$scope.updateaddonbtn = "Update";
				
				$scope.editAddon =function(id,indx){
					
					
					var fdata = "&addonName=" + $('#editAddonName'+indx).val()
					+ "&addonRate=" + $('#editAddonRate'+indx).val()
					+ "&addonIsActive=" + $('#editAddonIsActive'+indx).val()
					+ "&addonPropertyId=" + $('#editAddonPropertyId'+indx).val() 
					+ "&propertyAddonDetailId="+ $('#editPropertyAddonDetailId'+indx).val()
					+ "&propertyAddonId="+ $('#editPropertyAddonId'+indx).val(); 
					
					$scope.updateaddonbtn = "Please Wait...";	
					$http(
							{
								method : 'POST',
								data : fdata,
								headers : {
									'Content-Type' : 'application/x-www-form-urlencoded'
								},
								url : 'edit-addon'
							}).then(function successCallback(response) {
								
								var gdata = "&addonName=" + $('#editAddonName'+indx).val()
								+ "&addonRate=" + $('#editAddonRate'+indx).val()
								+ "&addonIsActive=" + $('#editAddonIsActive'+indx).val()
								+ "&addonPropertyId=" + $('#editAddonPropertyId'+indx).val() 
								+ "&propertyAddonDetailId="+ $('#editPropertyAddonDetailId'+indx).val()
								+ "&propertyAddonId="+ $('#editPropertyAddonId'+indx).val(); 
								$scope.updateaddonbtn = "Update";
								$http(
										{
											method : 'POST',
											data : gdata,
											headers : {
												'Content-Type' : 'application/x-www-form-urlencoded'
											},
											url : 'edit-addon-detail'
										}).then(function successCallback(response) {
									
											$scope.updateaddonbtn = "Update";
											 $("#addonsuccess2"+id).hide();
											  var alertmsg = ' <div class="alert" style="color:green;text-align:center;">Your data updated successfully saved.</div>';
									            $('#addonsuccess2'+id).html(alertmsg);
									            $("#addonsuccess2"+id).fadeTo(2000, 500).slideUp(500, function(){
									                $("#addonsuccess2"+id).slideUp(500);

													$scope.getPropertyAddon();
									                 });
								}, function errorCallback(response) {
								});
								
								
					}, function errorCallback(response) {
					});
				};
				
				$scope.deleteAddon = function(rowid){
					var check = confirm("Are you sure you want to delete this addon?");
					if( check == true){
						var url = "delete-addon?propertyAddonId="+rowid;
						$http.get(url).success(function(response) {
							$scope.getPropertyAddon();
				
						});
					}
				};
				
/* Addon function start here */
 
 /* Landmark function start here */
 
 $scope.getPropertyLandmark = function() {

	var url = "get-property-landmark"
	 			$http.get(url).success(function(response) {
	 				$scope.propLandmark = response.data;
	 	
	 			});
	 		};
	 		
	 		
	 		var spinner = $('#loadertwo');
	   	 	$scope.landmarkbtn = "Save";
			$scope.editPropertyLandmark = function() {
	   			var fdata="&propertyLandmark1="+ $('#landmark1').val()
	   					+ "&propertyLandmark2=" + $('#landmark2').val()
	   					+"&propertyLandmark3="+ $('#landmark3').val()
	   					+"&propertyLandmark4="+$('#landmark4').val()
	   					+"&propertyLandmark5="+$('#landmark5').val()
	   					+"&propertyAttraction1="+$('#attraction1').val()
	   					+"&propertyAttraction2="+$('#attraction2').val()
	   					+"&propertyAttraction3="+$('#attraction3').val()
	   					+"&propertyAttraction4="+$('#attraction4').val()
	   					+"&propertyAttraction5="+$('#attraction5').val()   					
	   					+"&kilometers1=" +$('#kilom1').val()
	   					+"&kilometers2="+$('#kilom2').val()
	   					+"&kilometers3="+$('#kilom3').val()
	   					+"&kilometers4="+$('#kilom4').val()
	   					+"&kilometers5="+$('#kilom5').val()
	   					+"&distance1="+$('#distance1').val()
	   					+"&distance2="+$('#distance2').val()
	   					+"&distance3="+$('#distance3').val()
	   					+"&distance4="+$('#distance4').val()
	   					+"&distance5="+$('#distance5').val()
	   			$scope.landmarkbtn = "Please Wait";
	   			//spinner.show(); 
		   		$http(
		   					{
		   						method : 'POST',
		   						data : fdata,
		   						headers : {
		   							'Content-Type' : 'application/x-www-form-urlencoded'
		   						},
		   						url : 'edit-property-landmark'
		   					}).then(function successCallback(response) {
		   						$scope.landmarkbtn = "Save & Continue";
		   						$("#landmarksuccess").hide();
			   					  var alertmsg = ' <div class="alert" style="color:green;text-align:center;">Property detail updated</div>';
			   			            $('#landmarksuccess').html(alertmsg);
			   			            $("#landmarksuccess").fadeTo(2000, 500).slideUp(500, function(){
		   			                $("#landmarksuccess").slideUp(500);
		   			                 });
		   					/*  setTimeout(function(){ 
	  						      spinner.hide(); 
	  					           }, 2000); */
		   				/* 	$('.nav-tabs > .active').next('li').find('a').trigger('click');	
		   						var propertyEnable=false;
		   						
		   						 if(policyStatus && amenityStatus && imageStatus && addonStatus && addonAccommodationStatus && propertyStatus){
		   							 $scope.getUpdateProperty();
		   							propertyEnable=true; 
		   						 }
		   						 if(propertyEnable){
		   							 $scope.getProperty();
		   							$scope.landmarkbtn = "Save & Continue";
		   							
		   						 
				   				
		   						     setTimeout(function(){ 
		   						      spinner.hide(); 
		   					           }, 2000);
			   					
		   						 } */
		   						 
		   						//$scope.getUlotelHotelTags();
		   						window.location.reload();
		   					
		   			            
		   			}, function errorCallback(response) {
		   			 spinner.hide(); 
		   			}); 
	              
	   
	   		};
	   		
	   		
 /* Landmark function end here  */
 
 /* Image function start here  */
 
 			 $scope.getPhotoCategory = function() {
   				
   				var url = "get-photo-category";
   				$http.get(url).success(function(response) {
   					//console.log(response);
   					$scope.photocategory = response.data;
   				
   				});
   			};
   			
   		 $scope.getAccommodationTags = function() {
				
				var url = "get-room-photo-category";
				$http.get(url).success(function(response) {
					//console.log(response);
					$scope.roomcategory = response.data;
				
				});
			};
   		
			$scope.getEnableImage = function() {
   	   			
   	   			var url = "get-enable-photo";
   	   			$http.get(url).success(function(response) {
   	   				//console.log(response);
   	   				$scope.photos = response.data;
   	   				//$scope.getUlotelHotelTags();
					window.location.reload();
   	   			});
   	   		};
   	   		
   	   		
   			$scope.getPropertyPhotos = function() {
   	   			
   	   			var url = "get-property-photos";
   	   			$http.get(url).success(function(response) {
   	   				//console.log(response);
   	   				$scope.photos = response.data;
   	   			
   	   			});
   	   		};
   	   		
   	   		$scope.deletePhoto = function(item) {
   	   	     var url = "delete-property-photo?propertyPhotoId="+item;
   	   			$http.get(url).success(function(response) {

   	   				$scope.getPropertyPhotos();
   	   			  var index = $scope.catphotos.indexOf(item);
   	   	   		  $scope.catphotos.splice(index, 1);  
   	   			   
   	   			});
   	   			
   	   		};
   	   		
   	   	$scope.updatePhoto = function(item) {
   	   		
   	   		var altName=$('#altName'+item).val();
  	   	     var url = "update-property-photo?propertyPhotoId="+item+"&imageAltName="+altName;
  	   			$http.get(url).success(function(response) {

  	   				$scope.getPropertyPhotos();
  	   			  var index = $scope.catphotos.indexOf(item);
  	   	   		  $scope.catphotos.splice(index, 1);  
  	   			   
  	   			});
  	   			
  	   		};
   	     $scope.uploadPropertyImage = function(file,categoryId) {
       	  

   	    	if(file !=null && categoryId != null){
           
            	var altName=$('#propertyAltName').val();
                Upload.upload({
                    url: 'add-property-photo',
                    enableProgress: true, 
                    data: {myFile: file,'photoCategoryId':categoryId,'imageAltName':altName}
                }).then(function (resp) {
                	
						$("#propertyImageSuccess").hide();
   					  var alertmsg = ' <div class="alert" style="color:green;text-align:center;">Property Image updated</div>';
   			            $('#propertyImageSuccess').html(alertmsg);
   			            $("#propertyImageSuccess").fadeTo(2000, 500).slideUp(500, function(){
			                $("#propertyImageSuccess").slideUp(500);
			                 });
   			         $scope.getPropertyPhotos();
                });
   	    	}else{
   	    		$("#propertyImageSuccess").hide();
					  var alertmsg = ' <div class="alert" style="color:red;text-align:center;">Please Choose Image or Image Tag</div>';
			            $('#propertyImageSuccess').html(alertmsg);
			            $("#propertyImageSuccess").fadeTo(2000, 500).slideUp(500, function(){
		                $("#propertyImageSuccess").slideUp(500);
		                 });
   	    	}
               
                
            };
            
            $scope.uploadAccommodationImage = function (file,propAccomId,accomTagId) {
          	  	if(file !=null && propAccomId != null && accomTagId != null){
          	  		
          	  	var altName=$('#roomAltName').val();
          	  	
                   Upload.upload({
                       url: 'add-property-photo',
                       enableProgress: true, 
                       data: {myFile: file,'propertyAccommodationId':propAccomId,'photoCategoryId':accomTagId,'imageAltName':altName}  /* 'photoCategoryId':id, */
                   }).then(function successCallback(response) {
                    
                	 $("#roomImageSuccess").hide();
  					  var alertmsg = ' <div class="alert" style="color:green;text-align:center;">Room Image updated</div>';
  			            $('#roomImageSuccess').html(alertmsg);
  			            $("#roomImageSuccess").fadeTo(2000, 500).slideUp(500, function(){
			                $("#roomImageSuccess").slideUp(500);
			                 });
                	
                   	$scope.getPropertyPhotos();
                   	
                   }, function errorCallback(response) {
                      // console.log('Error status: ' + resp.status);
                   }, function (evt) {
                   	
                       //var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                       
                       //console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
                      
                   });
          	  	}else{
          	  	$("#roomImageSuccess").hide();
				  var alertmsg = ' <div class="alert" style="color:red;text-align:center;">Choose a right image or Category</div>';
		            $('#roomImageSuccess').html(alertmsg);
		            $("#roomImageSuccess").fadeTo(2000, 500).slideUp(500, function(){
		                $("#roomImageSuccess").slideUp(500);
		                 });
          	  	}
                   
               };
 
 /* Image function end here */
 
 /* Hotel Status Function Start here */
 
               $scope.savePropertyStatus = function(){
  				
  				 var fdata = "hotelIsActive=" + $('#hotelIsActive').val()
  				             +"&stopSellIsActive=" + $('#stopSellIsActive').val()
  				             +"&delistIsActive=" + $('#delistIsActive').val();

  				 $http(
  						{
  							method : 'POST',
  							data : fdata,
  							headers : {
  								'Content-Type' : 'application/x-www-form-urlencoded'
  							},
  							url : 'save-property-status'
  						}).then(function successCallback(response) {

  							$scope.updatehamenitybtn = "Update";
  							 $("#propertyStatusSuccess").hide();
  		  					  var alertmsg = ' <div class="alert" style="color:green;text-align:center;">Property Status updated</div>';
  		  			            $('#propertyStatusSuccess').html(alertmsg);
  		  			            $("#propertyStatusSuccess").fadeTo(2000, 500).slideUp(500, function(){
  					                $("#propertyStatusSuccess").slideUp(500);
  					                 });
  							$scope.getPropertyAmenities();
  							
  				}, function errorCallback(response) {
  				});
  				

  			};
 
 /* Hotel Status Function End here */
 
 		$scope.getUlotelHotelTags = function() {
   	   			
   	   			var url = "get-ulotel-hotel-tags";
   	   			$http.get(url).success(function(response) {
   	   				//console.log(response);
   	   				$scope.ulotelHotelTags = response.data;
   	   			
   	   			});
   	   		};
				
   	   		$scope.getSellingAdultRate= function(adult,accommid){
   	   			
   	   			var commission=15;
   	   			var sellrate=0;
   	   			sellrate=parseFloat(adult)+parseFloat(adult*commission/100);
   	   			document.getElementById('sellingAdult'+accommid).value=Math.round(sellrate);
   	   		};
   	   		
   	   		$scope.getSellingChildRate= function(child,accommid){
	   			var commission=15;
   	   			var sellrate=0;
   	   			sellrate=parseFloat(child)+parseFloat(child*commission/100);
   	   			document.getElementById('sellingChild'+accommid).value=Math.round(sellrate);
	   		};
// 			$scope.changeType(1);
 
 			
			
			$scope.getPropertyAmenities();
			
			$scope.getAccommodations();
			
	 		$scope.getPropertyLandmark();
	 		
   	    	$scope.getPropertyList();
   		
   	    	$scope.getPhotoCategory();  
   	    	
   	    	$scope.getAccommodationTags();
   	    	
            $scope.getProperty();
   		
   			$scope.getStates();
   		
   			$scope.getCountries();
   		
   			$scope.getPropertyTypes();
   		
   			$scope.getPropertyPhotos();
   			
   			$scope.getUlotelHotelTags();
   			
   			$scope.getPropertyTags();
   			
   			$scope.getAllAmenitySize();
   			
   	//		$scope.getAllCategoryPhotos();
   			
   			$scope.getPropertyAddon();
   			
//    			$scope.getAccommodationAmenities1();

   			$scope.getPropertyRoleDetails();
   			
   			$scope.getPropertyRoles();
   			
   			$scope.getContractTypes();

   	
           $scope.upload1 = function (file) {
	 			
    		   
		       console.log(file);
	 			
	        	var accommodationId = document.getElementById("editPropertyAccommodationId").value;
	        	
	            Upload.upload({
	                url: 'accommodationpicupdate',
	                enableProgress: true, 
	                data: {myFile: file, 'propertyAccommodationId': accommodationId} 
	            }).then(function (resp) {
// 	            	window.location.reload();
	            });
	           
	            
	     };
              
   	//$scope.unread();
   	
	   
   	
	     
   
   }]);
   
   app.directive('fileInput', ['$parse', function ($parse) {
	    return {
	        restrict: 'A',
	        link: function (scope, element, attributes) {
	            element.bind('change', function () {
	                $parse(attributes.fileInput)
	                .assign(scope,element[0].files[0])
	                scope.$apply()
	            });
	        }
	    };
	}]);
   
   
</script>
<script src="js1/upload/ng-file-upload-shim.min.js"></script>
<script src="js1/upload/ng-file-upload.min.js"></script>
<script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.js"></script>

<script>

   $(function () {
      // Replace the <textarea id="editor1"> with a CKEditor
      // instance, using default configuration.
     
      
     CKEDITOR.replace('propertyHotelPolicy');  
     CKEDITOR.replace('propertyGigiPolicy'); 
      //bootstrap WYSIHTML5 - text editor
     // $(".textarea").wysihtml5();
     // config.enterMode = CKEDITOR.ENTER_BR;
     // config.autoParagraph = false;
      //config.fillEmptyBlocks = false;
      
      
      CKEDITOR.replace('propertyRefundPolicy');
      //bootstrap WYSIHTML5 - text editor
     // $(".textarea").wysihtml5();
     // config.enterMode = CKEDITOR.ENTER_BR;
     // config.autoParagraph = false;
     // config.fillEmptyBlocks = false;
      
     CKEDITOR.replace('propertyCancellationPolicy');
     CKEDITOR.replace('propertyAddonPolicy');
     // bootstrap WYSIHTML5 - text editor
    /*   $(".textarea").wysihtml5();
      config.enterMode = CKEDITOR.ENTER_BR;
      config.autoParagraph = false;
      config.fillEmptyBlocks = false; */
     
      /*  CKEDITOR.replace('editor1');
      //bootstrap WYSIHTML5 - text editor
      $(".textarea").wysihtml5();
      
      CKEDITOR.replace('propertyDescription');
      //bootstrap WYSIHTML5 - text editor
      $(".textarea").wysihtml5(); */
    });
   
   
  
</script>
<script async defer
   src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDg9dqRsKwRWXGdTKsUNHTHhfQqdSyip9k&callback=initMap"></script>
<!--<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>-->
<script type="text/javascript">
   var markers = [
       {
           "title": 'ulohotels',
           "lat": '13.05406559587872',
           "lng": '80.19280683201782',
           "description": 'ULO Hotels is a trusted and fast growing hotel network with over 10 partners. We use the most advanced technology to offer best value promotions and hotel rates to business and leisure travelers.'
       }
   ];
   window.onload = function () {
       var mapOptions = {
           center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
           zoom: 0,
           mapTypeId: google.maps.MapTypeId.ROADMAP
       };
       var infoWindow = new google.maps.InfoWindow();
       var latlngbounds = new google.maps.LatLngBounds();
       var geocoder = geocoder = new google.maps.Geocoder();
       var map = new google.maps.Map(document.getElementById("map"), mapOptions);
       for (var i = 0; i < markers.length; i++) {
           var data = markers[i]
           var myLatlng = new google.maps.LatLng(data.lat, data.lng);
           var marker = new google.maps.Marker({
               position: myLatlng,
               map: map,
               title: data.title,
               draggable: true,
               animation: google.maps.Animation.DROP
           });
           (function (marker, data) {
               google.maps.event.addListener(marker, "click", function (e) {
                   infoWindow.setContent(data.description);
                   infoWindow.open(map, marker);
               });
               google.maps.event.addListener(marker, "dragend", function (e) {
                   var lat, lng, address;
                   geocoder.geocode({ 'latLng': marker.getPosition() }, function (results, status) {
                       if (status == google.maps.GeocoderStatus.OK) {
                           lat = marker.getPosition().lat();
                           lng = marker.getPosition().lng();
                           address = results[0].formatted_address;
                           $('#latitude').val(lat);
                           $('#longitude').val(lng);
                           
                       }
                   });
               });
           })(marker, data);
           latlngbounds.extend(marker.position);
       }
       var bounds = new google.maps.LatLngBounds();
       map.setCenter(latlngbounds.getCenter());
       map.fitBounds(latlngbounds);
   }
</script>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
   $( function() {
//      $("#taxValidFrom").datepicker({minDate:0});
//      $("#taxValidTo").datepicker({minDate:0});
     $("#checkin" ).datepicker({minDate:0});
     $("#checkout" ).datepicker({minDate:0});
     $("#expiryDate" ).datepicker({minDate:0});
   });
</script> 
<script>
   function goToByScroll(id){
      
       // Reove "link" from the ID
     id = id.replace("link", "");
       // Scroll
     $('html,body').animate({
         scrollTop: $("#"+id).offset().top},
         'slow');
   }
   
</script>
<style type="text/css">
 
    .panel-title .glyphicon{
        font-size: 14px;
    }
</style>
<script>   
$(document).ready(function(){
    // Add minus icon for collapse element which is open by default
    $(".collapse.in").each(function(){
    	$(this).siblings(".panel-heading").find(".glyphicon").addClass("glyphicon-minus").removeClass("glyphicon-plus");
    });
    
    // Toggle plus minus icon on show hide of collapse element
    $(".collapse").on('show.bs.collapse', function(){
    	$(this).parent().find(".glyphicon").removeClass("glyphicon-plus").addClass("glyphicon-minus");
    }).on('hide.bs.collapse', function(){
    	$(this).parent().find(".glyphicon").removeClass("glyphicon-minus").addClass("glyphicon-plus");
    });
});
    	  $('.btnPrevious').click(function(){
    	  $('.nav-tabs > .active').prev('li').find('a').trigger('click');
    	});
</script>
