<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
   pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="s" uri="/struts-tags"%>
<style>
.btnpd{margin:5px;}
/* .avabg{background-color:#fff;padding:5px;} */
.invcal{background-color:#069;color:#fff;padding:5px;border:none;}
.rmname{color:#fff;}
.box-titles{display: inline-block; font-size: 18px; margin: 0; }
.box-title {color:#fff;}
.totalrooms{background: #88ba41 !important; border-color: #5cb85c !important;text-align:center;width:50%;padding:5px;float:left;}
.soldtext{background-color: #d9534f; border-color: #d43f3a;text-align:center;width:50%;padding:5px;float:right;}
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- text json -->
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Revenue Logs
      </h1>
 	  <ol class="breadcrumb">
         <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
         <li class="active">Revenue Logs</li>
       </ol>		
   </section>
   <!-- Main content -->
   <section class="content">
      <div class="row">
         <div class="col-xs-12">
            <div class="nav-tabs-custom">
                  <!-- Font Awesome Icons -->
                  
                  <div class="tab-pane">
                  	 <form method="post" theme="simple" name="offers">
                        <section id="new">
	                   		<ul class="nav nav-tabs">
			                  <li class="active"><a href="#fa-icons-logs-inventory" data-toggle="tab">Inventory Logs</a></li>
			                  <li ><a href="#tab-logs-price" data-toggle="tab">Price Logs</a></li>
			                  <li ><a href="#tab-logs-promotion" data-toggle="tab">Promotion Logs</a></li>
			                </ul>
                        </section>
                     </form>
                     <div class="tab-content">
	                     	<div class="tab-pane active" id="fa-icons-logs-inventory">
		                  	<section class="content">
							    <div class="box box-default color-palette-box">
							    	<div class="box-header ">
							    		<div class="form-group col-md-4"><h3 class="box-titles">Inventory Logs</h3></div>
							    		<div class="form-group col-md-4"></div>
							          	<div class="form-group col-md-4">
											  <label>Select Property :</label>
											  <select  name="inventoryPropertyId" id="inventoryPropertyId" ng-model="inventoryPropertyId" ng-required="true" ng-change="getAllLogInventoryProperty()" class="form-control" >
											  <option disabled selected value> &nbsp;-- Property --&nbsp; </option>
		<!-- 									  <option style="display:none" value="0">All</option> -->
							                  <option ng-repeat="pp in activepromotionproperty" value="{{pp.propertyId}}">{{pp.propertyName}}</option>
							                  </select>
										</div>
							        </div>
							        <form method="post" theme="simple" name="inventoryLogsForm">
							        	<section class="content">
			                  			<div class="box-body table-responsive no-padding" > 
											<table class="table table-hover" id="example1">
												<thead>
												<tr>
													<th>Order</th>
<!-- 													<th>Inventory Id</th> -->
												    <th>Blocked Date</th>
												   	<th>Accommodation Type</th>
												   	<th>Room Inventory</th>
												   	<th>Created Date</th>
												   	<th>Created By</th>
												</tr>
												</thead>
												<tbody class="sortable">
													<tr ng-repeat="li in allInventoryLogs track by $index"  >
														<td>{{li.serialNo}}</td>
<!-- 														<td>{{li.inventoryId}}</td> -->
														<td>{{li.blockedDate}}</td>
														<td>{{li.accommodationType}}</td>
														<td>{{li.inventoryCount}}</td>
														<td>{{li.createdDate}}</td>
														<td>{{li.createdBy}}</td>										
														 
													</tr>
												</tbody>
											</table>
										</div>
						                </section>
			                  		</form>
							    </div>
							    </section>
		                  </div>
		                  <div class="tab-pane" id="tab-logs-price">
		                  	<section class="content">
							    <div class="box box-default color-palette-box">
							    	<div class="box-header ">
							    		<div class="form-group col-md-4"><h3 class="box-titles">Pricing Logs</h3></div>
							    		<div class="form-group col-md-4"></div>
							          	<div class="form-group col-md-4">
											  <label>Select Property :</label>
											  <select  name="ratePropertyId" id="ratePropertyId" ng-model="ratePropertyId" ng-required="true" ng-change="getAllLogRatesProperty()" class="form-control" >
											  <option disabled selected value> &nbsp;-- Property --&nbsp; </option>
		<!-- 									  <option style="display:none" value="0">All</option> -->
							                  <option ng-repeat="pp in activepromotionproperty" value="{{pp.propertyId}}">{{pp.propertyName}}</option>
							                  </select>
										</div>
							        </div>
							        <form method="post" theme="simple" name="priceLogsForm">
							        	<section class="content">
			                  			<div class="box-body table-responsive no-padding"> 
											<table class="table table-hover" id="example1">
												<thead>
												<tr>
													<th>Order</th>
<!-- 													<th>Rate Id</th> -->
												    <th>Start Date</th>
												   	<th>End Date</th>
												   	<th>Accommodation Type</th>
												   	<th>Tariff</th>
												   	<th>Source Type</th>
												   	<th>Created Date</th>
												   	<th>Created By</th>
												</tr>
												</thead>
												<tbody class="sortable">
													<tr ng-repeat="rl in allRateLogs track by $index"  >
														<td>{{rl.serialNo}}</td>
<!-- 														<td>{{rl.propertyRateId}}</td> -->
													    <td>{{rl.startDate}}</td>
														<td>{{rl.endDate}}</td>
														<td>{{rl.accommodationType}}</td>
														<td>{{rl.baseAmount}}</td>
														<td>{{rl.sourceType}}</td>										
														<td>{{rl.createdDate}}</td>
														<td>{{rl.createdBy}}</td>
													</tr>
												</tbody>
											</table>
										</div>
						                </section>
			                  		</form>
							    </div>
							    </section>
		                  </div>
		                  <div class="tab-pane" id="tab-logs-promotion">
		                  	<section class="content">
							    <div class="box box-default color-palette-box">
							    	<div class="box-header ">
							    		<div class="form-group col-md-4"><h3 class="box-titles">Promotion Logs</h3></div>
							    		<div class="form-group col-md-4"></div>
							          	<div class="form-group col-md-4">
											  <label>Select Property :</label>
											  <select  name="promotionPropertyId" id="promotionPropertyId" ng-model="promotionPropertyId" ng-required="true" ng-change="getAllLogPromotionProperty()" class="form-control" >
											  <option disabled selected value> &nbsp;-- Property --&nbsp; </option>
		<!-- 									  <option style="display:none" value="0">All</option> -->
							                  <option ng-repeat="pp in activepromotionproperty" value="{{pp.propertyId}}">{{pp.propertyName}}</option>
							                  </select>
										</div>
							        </div>
							        <form method="post" theme="simple" name="promotionLogsForm">
							        	<section class="content">
			                  			<div class="box-body table-responsive no-padding" > 
											<table class="table table-hover" id="example1">
												<thead>
												<tr>
													<th>Order</th>
<!-- 													<th>Promotion Id</th> -->
												    <th>Start Date</th>
												   	<th>End Date</th>
												   	<th>Discount</th>
												   	<th>Created Date</th>
												   	<th>Created By</th>
												</tr>
												</thead>
												<tbody class="sortable">
													<tr ng-repeat="pl in allPromotionLogs track by $index"  >
														<td>{{pl.serialno}}</td>
<!-- 														<td>{{pl.promotionId}}</td> -->
													    <td>{{pl.startDate}}</td>
														<td>{{pl.endDate}}</td>
														<td>{{pl.promotionName}}</td>
														<td>{{pl.createdDate}}</td>
														<td>{{pl.createdBy}}</td>										
													</tr>
												</tbody>
											</table>
										</div>
						                </section>
			                  		</form>
							    </div>
							    </section>
		                  </div>
                     </div>
                  </div>
                  <!-- s -->
                  
                  <!-- t -->
               </div>
              
            </div>
            <!-- /.nav-tabs-custom -->
         </div>
         </section>
         <!-- /.col -->
      </div>
<!-- Page specific script -->
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>
<script>
   var app = angular.module('myApp', ['ngProgress']);
   	app.controller('customersCtrl', function($scope, $http,$timeout,ngProgressFactory) {
     	$timeout(function(){
         	    // $scope.progressbar.complete();
               $scope.show = true;
               $("#pre-loader").css("display","none");
           }, 2000);
            		
   		$scope.unread = function() {
   		var notifiurl = "unreadnotifications.action";
   		$http.get(notifiurl).success(function(response) {
   			$scope.latestnoti = response.data;
   		});
   		};
   
   				
				
			$scope.getAllLogPromotions = function() {

				var url = "get-all-log-promotions";
				$http.get(url).success(function(response) {
					$scope.allPromotionLogs = response.data;
				});
			};
			
			$scope.getAllLogPromotionProperty = function(){
				var promoPropertyId=$('#promotionPropertyId').val();
				var url = "get-log-property-promotions?propertyId="+promoPropertyId;
				$http.get(url).success(function(response) {
					$scope.allPromotionLogs = response.data;
				});
			};
			
			$scope.getAllLogRates = function() {

				var url = "get-all-log-rates";
				$http.get(url).success(function(response) {
					$scope.allRateLogs = response.data;
				});
			};
			
			$scope.getAllLogRatesProperty = function(){
				var ratePropertyId=$('#ratePropertyId').val();
				var url = "get-log-property-rates?propertyId="+ratePropertyId;
				$http.get(url).success(function(response) {
					$scope.allRateLogs = response.data;
				});
			};
			
			$scope.getAllLogInventory = function() {

				var url = "get-all-log-inventory";
				$http.get(url).success(function(response) {
					$scope.allInventoryLogs = response.data;
				});
			};
			
			$scope.getAllLogInventoryProperty = function(){
				var inventoryPropertyId=$('#inventoryPropertyId').val();
				var url = "get-log-property-inventory?propertyId="+inventoryPropertyId;
				$http.get(url).success(function(response) {
					$scope.allInventoryLogs = response.data;
				});
			};
			
			
			
			
			$scope.getActiveProperty = function() {

				var url = "get-active-promotion-property";
				$http.get(url).success(function(response) {
					$scope.activepromotionproperty = response.data;
		
				});
			};
			
   	    	$scope.getActiveProperty();
   	    	$scope.getAllLogPromotions();
   	    	$scope.getAllLogRates();
   	    	$scope.getAllLogInventory();
   	});
   	
   	app.directive('ngModelOnblur', function() {
   	    return {
   	        restrict: 'A',
   	        require: 'ngModel',
   	        priority: 1, // needed for angular 1.2.x
   	        link: function(scope, elm, attr, ngModelCtrl) {
   	            if (attr.type === 'radio' || attr.type === 'checkbox') return;

   	            elm.unbind('input').unbind('keydown').unbind('change');
   	            elm.bind('blur', function() {
   	                scope.$apply(function() {
   	                    ngModelCtrl.$setViewValue(elm.val());
   	                });         
   	            });
   	        }
   	    };
   	});
</script>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

  <style type="text/css">
    #ui-datepicker-div
    {
        z-index: 9999999 !important;
    }
</style>