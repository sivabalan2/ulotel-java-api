<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<html lang="en">
<head>
 <!-- Google Tag Manager -->
   <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
         new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
         j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
         'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
         })(window,document,'script','dataLayer','GTM-M3RH7N5');
      </script> 
      <!-- End Google Tag Manager -->
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <!-- Required meta tags -->
    <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
 <%
    
    if (request.getAttribute("metaTag") != null) {
  %>
      <%= request.getAttribute("metaTag")  %>
  <%
    } else {  request.setAttribute("metaTag","");
  %> 
      <%= request.getAttribute("metaTag") %>
  <%
    }
  %>
  
  
      <%--  <%= request.getAttribute("metaTag") %> --%>
<link rel="icon" type="image/png" href="contents/images/ulofavicon2.png">
<!-- Required css -->
    <link rel="stylesheet" type="text/css" href="contents/css/bootstrap.min.css?v=2.0">
    <!-- Template css -->
    <link rel="stylesheet" type="text/css" href="contents/css/style.min.css?v=2.3.8">
	<link rel="stylesheet" type="text/css" href="contents/css/responsive.css?v=2.3.8">
	<link rel="stylesheet" type="text/css" href="contents/css/font-awesome.min.css?v=2.0">
	<link href="https://fonts.googleapis.com/css?family=Merriweather|Quantico" rel="stylesheet" type="text/css">
   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" type="text/css" crossorigin="anonymous">
   <!-- Date picker -->
             <style>
    [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-    ng-cloak {
        display: none !important;
    }
</style>
<script>
 <%
    
    if (request.getAttribute("content") != null) {
  %>
      <%= request.getAttribute("content")  %>
  <%
    } else {  request.setAttribute("content","");
  %> 
      <%= request.getAttribute("content") %>
  <%
    }
  %>
 </script>   
       
      <%--  <%= request.getAttribute("content") %> --%>

 </head>
 <body  ng-app="myApp" ng-cloak ng-controller="customersCtrl">
  <!-- Google Tag Manager (noscript) -->
       <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M3RH7N5"
         height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
      <!-- End Google Tag Manager (noscript) -->
<!-- Middle Header -->

<!-- helping css -->
    <div class="middle-header" id="middle-header">
      <div class="container-fluid">
     <div class="row">
       <div class="col-sm-8 col-xs-8 hidden-lg hidden-md">
            <a href="#" class="text-dark " data-toggle="modal" data-target="#menuModal"><img src="contents/images/menu-icon/mburger.png" alt="mobile menu"/></a>
          </div>
		  <div class="col-sm-4 col-xs-4 hidden-lg hidden-md">
            <nav class="nav nav-counter">
             <a href="#" class="nav-link d-sm-block d-md-none muser"><img src="contents/images/menu-icon/muser.png" alt="login"/></a>
             <a href="tel:180030008686" title="180030008686" class="nav-link  d-sm-block d-md-none mcall"><img src="contents/images/menu-icon/mcall.png" alt="ulo contact number"/></a>
            </nav> 
          </div>
          <div class="col-lg-4 col-md-4  col-sm-4 hidden-xs">
            <a href="index" class="logo">
              <img src="contents/images/ulologo.svg" alt="ulohotels" class="logo">
            </a>
          </div>
        
          <div class="col-lg-8 col-md-8 col-sm-8 hidden-xs">
           <nav class="nav nav-counter">
              <a href="corporate" class="nav-link d-none d-lg-block d-md-block">Corporate Enquiry</a>
              <a href="#" class="nav-link d-none d-lg-block d-md-block hide">Day Stay</a>
              <a href="#" class="nav-link d-none d-lg-block d-md-block hide">Share & Earn</a>
              <a href="ulouserprofile" ng-show="showDprofile" class="nav-link d-none d-lg-block d-md-block dlogin"  ><img src="contents/images/menu-icon/duser.png" alt="login"/>Hi ! Welcome Back</a>
     <s:if test="#session.uloUserId == null" > 
              <a href="#" ng-show="showDlogin" class="nav-link d-none d-lg-block d-md-block dlogin"  data-toggle="modal" data-target="#myModal"><img src="contents/images/menu-icon/duser.png" alt="login"/> Log In</a>
                   
                       </s:if>  
                       
          
                        <s:if test="#session.uloUserId != null" > 
              
                   <a href="ulouserprofile"  class="nav-link d-none d-lg-block d-md-block dlogin"  ><img src="contents/images/menu-icon/duser.png" alt="login"/> Hi ! <%=session.getAttribute("uloUserName")%></a>
              
                       </s:if> 
              <a href="#" class="nav-link d-none d-lg-block d-md-block dcall"><img src="contents/images/menu-icon/dcalls.svg" alt="ulo contact number"/> 1800 3000 8686</a>
            </nav>
          </div>

        </div>
      </div>
    </div>
    <!-- /Header -->
     <tiles:insertAttribute name="body" />
	
<!-- page footer BEGINS -->
<div class="desktopfooter hidden-sm hidden-xs">
    <div class="container">
    <div class="row p-3">
    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
    <h5>About ulo</h5>
    <a href="aboutulo" class="footurl">About Ulo</a>
    <a href="careers-ulo" class="footurl">Careers</a>
    <a href="blog" class="footurl">Blog</a>
    <a href="contactus" class="footurl">Contact</a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
    <h5>Business Info</h5>
    <a href="corporate" class="footurl">Corporate enquiry</a>
    <a href="partner-with-us" class="footurl">Grow with us</a>
      <a href="guest-policy" class="footurl">Cancellation</a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
    <h5>Policies</h5>
    <a href="termsandconditions" class="footurl">Terms and conditions</a>
    <a href="privacy" class="footurl">Privacy Policy</a>
    <a href="guest-policy" class="footurl">Guest Policy</a>
  
    <a href="ulo-faq" class="footurl">FAQ</a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
    <a href="partner-with-us"><div class="footpartner">
    <p>We are working tirelessly to mark our brand presence all over India and we are ambitious to work with franchise partners who share our zeal for premium guest service. If you want to join our hands in this epic journey and get amazing business opportunity.</p>
    <h4>PARTNER WITH US</h4>
    
    </div>
    </a>
    </div>
    <!-- secure payment begins -->
    <div class="col-lg-12 col-md-12 col-sm-12 col-12 d-none d-lg-block d-md-block p-3">
    <p class="securepayment">Secure Payment Partner <img src="contents/images/payment/rp.svg" class="moveimage"> <p>
    </div>
    <!-- secure payment ends-->
    </div>
    </div>
    </div>
<!-- home page footer ENDS --->
<!-- ulo hotel copy right -->
  <div class="copyrights">
    <div class="container">
    <div class="row">
    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 mbcpline p-3">
    <p>Copyrights Ulo Hotels Private Limited &copy; 2018 All Rights Reserved</p>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 desksocialbox">
       <a href="https://www.pinterest.com/ulohotels/" class="sociallink"><i class="fab fa-pinterest fa-x"></i></a>
     <a href="https://www.instagram.com/ulohotels/" class="sociallink"><i class="fab fa-instagram fa-x"></i></a>
      <a href="https://plus.google.com/+ULOHOTELS" class="sociallink"><i class="fab fa-google-plus fa-x"></i></a>
        <a href="https://twitter.com/ulohotels" class="sociallink"><i class="fab fa-twitter fa-x"></i></a>
     <a href="https://www.facebook.com/Ulohotels/" class="sociallink"><i class="fab fa-facebook-square fa-x"></i></a>
    </div>
    </div>
    </div>
    </div>
<!-- mobile copyright footer -->
<div class="mobilesocial hidden-lg hidden-md hidden-sm">
<div class="container ">
<div class="row p-3">
<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
       <a href="https://www.pinterest.com/ulohotels/" class="sociallink"><i class="fab fa-pinterest fa-x"></i></a>
     <a href="https://www.instagram.com/ulohotels/" class="sociallink"><i class="fab fa-instagram fa-x"></i></a>
      <a href="https://plus.google.com/+ULOHOTELS" class="sociallink"><i class="fab fa-google-plus fa-x"></i></a>
        <a href="https://twitter.com/ulohotels" class="sociallink"><i class="fab fa-twitter fa-x"></i></a>
     <a href="https://www.facebook.com/Ulohotels/" class="sociallink"><i class="fab fa-facebook-square fa-x"></i></a>
</div>
</div>
</div>
</div>
<div class="container mobilecopy hidden-lg hidden-md hidden-sm">
<div class="row ">
<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 mbcpline">
<p>Copyrights Ulo Hotels Private Limited &copy; 2018 All Rights Reserved</p>
</div>
</div>
</div>
<!-- mobile copyright footer -->
<!-- ulo hotels copy right -->
 <!-- Modal Menu -->
    <!-- ulohotels signup form  begins -->
    <!-- Modal -->
    <div id="myModal" class="modal fade newsignup" role="dialog" >
      <div class="modal-dialog modal-md ">
  
        <!-- Modal content-->
        <div class="modal-content desktoplogin ">
          <div class="modal-body">
            <!-- row begins -->
            <div class="row">
              <div class="col-md-5 leftlogin">
                <div class="leftloginheader">
                  <h4>Sign up / Login</h4>
                  <span class="logintag">To Your Account & Get Ulo Rewards & Earns</span>
                </div>
                <div class="row spllogin">
                  <div class="col-md-4 ">
                    <span class="imagebgs"><img src="contents/images/menu-icon/manage.png" class="img-responsive"></span>
                  </div>
                  <div class="col-md-8  ">
                    <span class="loginimagetag">Manage Your Bookings Anywhere Anytime</span>
                  </div>
                </div>
                <div class="row spllogin">
                  <div class="col-md-4 ">
                    <span class="imagebgs"><img src="contents/images/menu-icon/share.png" class="img-responsive"></span>
                  </div>
                  <div class="col-md-8 ">
                    <span class="loginimagetag">Share & Earn</span>
                  </div>
                </div>
                <div class="row spllogin">
                  <div class="col-md-4 ">
                    <span class="imagebgs"><img src="contents/images/menu-icon/cancel.png" class="img-responsive"></span>
                  </div>
                  <div class="col-md-8 ">
                    <span class="loginimagetag">Easy Cancellation & Refunds</span>
                  </div>
                </div>
              </div>
              <div class="col-md-7 rightlogin">
                <form name="dlogin" id="login">
                 <div ng-repeat="rd in rewardDetails" ng-if="rd.rewardDetailId != 'None'"> 
          <input type="hidden" id="rewardDetailId" value="{{rd.rewardDetailId}}" />
          </div>
                 
                 <div class="form-group">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                   
                    <input type="text" class="form-control signupuser" ng-model="userName" id="userName" placeholder="Name" ng-required="true">
                  </div>
                  <div class="form-group">
                    <input type="email" class="form-control signupuser" ng-model="emailId" id="emailId" placeholder="Email" ng-required="true">
                  </div>
                  <div class="form-group">
                    <input type="text" class="form-control signupuser" ng-model="mobilePhone" id="mobilePhone" placeholder="Mobile" ng-required="true">
                  </div>
                  <div class="form-group">
                  <div id="signuperrors"></div>
                  <div id="signuperror"></div>
                 
                  </div>
                  <div class="form-group">
                      <input type="hidden" value="4" id="roleId" name="roleId">
                      <input type="hidden" value="2" id ="accessRightsId"  name="accessRightsId">
                    <button type="submit"  ng-click="dlogin.$valid && signup()" class="btn btn-primary signupbtn">{{dsignin}}</button>
                  </div>
                </form>
                <!--  social login buttons -->
                <div class="row">
                  <div class="col-md-12">
                    <span class="signupor">Or</span>
                  </div>
                  <div class="col-md-6">
                    <a><button class="signupfb" ng-click = "facebookLogin();">FACEBOOK</button></a>
                  </div>
                  <div class="col-md-6">
                    <a><button class="signupgplus" ng-click = "googleLogin();"><img src="contents/images/menu-icon/gplus1.png" alt="google login" clss="img-responsive" height="20px" width="20px"> Google</button></a>
                  </div>
                   <div class="col-md-12">
                    <span class="creditterms">* Credits are not applicable for social login</span>
                  </div>
                </div>
                <!-- social login button -->
                <!-- already login begins -->
                <div class="row">
                  <div class="col-md-12">
                    <span class="signupolduser">Already Have a Account? <a href="javascript:void(0)" data-dismiss="modal"  data-toggle="modal" data-target="#loginModal">LOGIN HERE</a></span>
                  </div>
                </div>
                <!-- already login ends -->
              </div>
            </div>
            <!--   row ends -->
          </div>
        </div>
      </div>
    </div>
    <!-- ulohotels signup form ends -->
    <!-- Ulohotels login form begins -->
    <!-- Modal -->
    <div id="loginModal" class="modal fade newsignup" role="dialog" >
      <div class="modal-dialog modal-md ">
        <!-- Modal content-->
        <div class="modal-content desktoplogin ">
          <div class="modal-body">
            <!-- row begins -->
            <div class="row">
              <div class="col-md-5 leftlogin">
                <div class="leftloginheader">
                  <h4>Sign up / Login</h4>
                  <span class="logintag">To Your Account & Get Ulo Rewards & Earns</span>
                </div>
                <div class="row spllogin">
                  <div class="col-md-4 ">
                    <span class="imagebgs"><img src="contents/images/menu-icon/manage.png" class="img-responsive"></span>
                  </div>
                  <div class="col-md-8  ">
                    <span class="loginimagetag">Manage Your Bookings Anywhere Anytime</span>
                  </div>
                </div>
                <div class="row spllogin">
                  <div class="col-md-4 ">
                    <span class="imagebgs"><img src="contents/images/menu-icon/share.png" class="img-responsive"></span>
                  
                  </div>
                  <div class="col-md-8 ">
                    <span class="loginimagetag">Share & Earn</span>
                  </div>
                </div>
                <div class="row spllogin">
                  <div class="col-md-4 ">
                    <span class="imagebgs"><img src="contents/images/menu-icon/cancel.png" class="img-responsive"></span>
                  </div>
                  <div class="col-md-8 ">
                    <span class="loginimagetag">Easy Cancellation & Refunds</span>
                  </div>
                </div>
              </div>
              <div class="col-md-7 rightsignin">
               <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                   
                      
                <div class="userinfo ">
                <form name="userinfoform">
                    <div class="form-group">
                    <input type="text" class="form-control signupuser" id="loginUserName" ng-model="loginUserName" placeholder="MOBILE NO / EMAIL"   ng-required="true">
                    </div>
                    <div class="form-group">
                    <div id="loginerror"></div>
                    </div>
                    <div class="form-group">
                      <button type="submit" class="btn btn-primary signupbtn"  ng-click="userinfoform.$valid && sendUloLoginOtp();">Submit</button>
                    </div>
                    <div id="otpsendmsg"></div>
                     <div class="form-group">
                        <span class="signupolduser">New To Ulo Hotels , Just <a href="javascript:void(0)" data-dismiss="modal"  data-toggle="modal" data-target="#myModal">Signup Here</a></span>
                 
                    </div>
                  </form>
                </div>
                 <div class="userotp" >
                    <form name="userotp">                  
                  <input type="hidden" id="uloLoginId" value="{{ulouserId}}"/> 
                  <input type="hidden" id="uloLoginName" value="{{username}}"/>
                    <div class="form-group">
                      <span class="usernote">A One Time Password has been sent via SMS To <a class="usernumber" ></a>{{username}}</span>
                    </div>
                    <div class="form-group">
                      <input type="text" class="form-control signupuser" id="loginOtp" ng-model="loginOtp" placeholder="Enter your OTP" ng-required="true">
                    </div>
                    <div class="form-group">
                      <button type="submit" class="btn btn-primary signupbtn" ng-click="userotp.$valid && verifyUloLoginOtp();" >Submit</button>
                    </div>
                    <div class="form-group">
                      <span class="usernotetwo">Haven't received the code yet ? <a class="userotpagain" ng-click="resendUloLoginOtp()">Resend OTP</a></span>
                    </div>                    
                  </form>
                  </div>
                </div>
              </div>
            </div>
            <!--   row ends -->
          </div>
        </div>
      </div>
    </div>
    <!-- ulohotels login form ends -->
    <!-- mobile login forms begins -->
    <!-- ulohotels mobile signup form  begins -->
    <!-- Modal -->
    <div id="mymobileModal" class="modal fade" role="dialog" >
      <div class="modal-dialog modal-xs">
        <!-- Modal content-->
        <div class="modal-content mobilesignup ">
         <div ng-repeat="rd in rewardDetails" ng-if="rd.rewardDetailId != 'None'"> 
          <input type="hidden" id="mbRewardDetailId" value="{{rd.rewardDetailId}}" />
          </div>
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <img src="contents/images/ulologo.svg" alt="ulo hotels mobile logo" height="50px" class="logo">
            <span class="logotext">Signup To Your Account<span>
          </div>
          <div class="modal-body">
            <!-- row begins -->
            <div class="row">
              <div class="col-xs-12">
                   <form name="mobilesignupform">
                  <div class="form-group">
                    <input type="text" class="form-control signupuser" id="mbUserName" ng-model="mbUserName" placeholder="Name" ng-required="true">
                  </div>
                  <div class="form-group">
                    <input type="email" class="form-control signupuser" id="mbEmailId" ng-model="mbEmailId"  placeholder="Email" ng-required="true">
                  </div>
                  <div class="form-group">
                    <input type="text" class="form-control signupuser" id="mbMobilePhone" ng-model="mbMobilePhone"placeholder="Mobile" ng-required="true">
                  </div>
                    <div class="form-group">
                  <div id="msignuperrors"></div>
                  <div id="msignuperror"></div>
                 
                  </div>
                  <div class="form-group">
                  <input type="hidden" value="4" id="mbRoleId" name="roleId">
                      <input type="hidden" value="2" id ="mbAccessRightsId"  name="accessRightsId">
                    <button type="submit" class="btn btn-primary signupbtn" ng-click="mobilesignupform.$valid && mbsignup()">{{msignin}}</button>
                  </div>
                </form>
              </div>
              <div class="row">
                <div class="col-xs-12">
                  <span class="signupor">Or</span>
                </div>
                <div class="col-xs-6">
                  <a><button class="signupfb" ng-click = "facebookLoginMobile();">FACEBOOK</button></a>
                </div>
                <div class="col-xs-6">
                  <a><button class="signupgplus" ng-click = "googleLoginMobile();"><img src="contents/images/menu-icon/gplus1.png" alt="google login" clss="img-responsive" height="20px" width="20px"> Google</button></a>
                </div>
                <div class="col-xs-12">
                  <span class="signupolduser">Already Have a Account? <a href="javascript:void(0)" data-dismiss="modal"  data-toggle="modal" data-target="#loginmobileModal">LOGIN HERE</a></span>
                </div>
                 <div class="col-xs-12">
                    <span class="creditterms">* Credits are not applicable for social login</span>
                  </div>
              </div>
            </div>
            <!--   row ends -->
          </div>
        </div>
      </div>
    </div>
    <!-- ulohotels mobile signup form ends -->
    <!-- Ulohotels login form begins -->
    <!-- Modal -->
    <div id="loginmobileModal" class="modal fade newsignup" role="dialog" >
      <div class="modal-dialog modal-xs  ">
        <!-- Modal content-->
        <div class="modal-content mobilesignup ">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <img src="contents/images/ulologo.svg" alt="ulo hotels mobile logo" height="50px" class="logo">
          </div>
          <div class="modal-body">
            <!-- row begins -->
            <div class="row mobileuserinfo">
              <div class="col-xs-12">
               <form name="mobilesignup">
                  <div class="form-group">
                    <input type="text" class="form-control signupuser" id="mbLoginUserName" ng-model="mbLoginUserName" placeholder="Mobile Number / Email" ng-required="true">
                  </div>
                    <div class="form-group">
                    <div id="loginerrormb"></div>
                    </div>
                  <div class="form-group">
                    <button type="submit" class="btn btn-primary signupbtn" ng-click="mobilesignup.$valid  && mbSendUloLoginOtp()">Submit</button>
                  </div>
                   <div class="form-group">
                        <span class="signupolduser">New To Ulo Hotels , Just <a href="javascript:void(0)" data-dismiss="modal"  data-toggle="modal" data-target="#mymobileModal">Signup Here</a></span>
                 
                    </div>
                </form>
              </div>
            </div>
            <div class="row mobileuserotp">
              <div class="col-xs-12" ng-repeat="mslo in mbSendLoginOtp">
                <form name="muserotp">
                <input type="hidden" id="mbUloLoginId" value="{{mbulouserId}}"/>
                  <input type="hidden" id="mbUloLoginName" value="{{mbusername}}"/>
                  <div class="form-group">
                    <span class="usernote">A One Time Password has been sent via SMS To <a class="usernumber">{{mbusername}}</a></span>
                  </div>
                  <div class="form-group">
                    <input type="text" class="form-control signupuser" id="mbLoginOtp" ng-model="mbLoginOtp" placeholder="Enter your OTP" ng-required="true">
                  </div>
                  <div class="form-group">
                    <button type="submit" class="btn btn-primary signupbtn" ng-click="muserotp.$valid && mbVerifyUloLoginOtp();" >Submit</button>
                  </div>
                  <div class="form-group">
                    <span class="usernotetwo">Haven't received the code yet ? <a class="userotpagain" ng-click="mbResendUloLoginOtp()">Resend OTP</a></span>
                  </div>
                </form>
              </div>
            </div>
            <!--   row ends -->
          </div>
        </div>
      </div>
    </div>
    <!-- ulohotels login form ends -->
    <!-- mobile login form ends --> 
	<!-- Modal Menu -->
    <!-- Required js -->
    
    <script type="text/javascript" src="contents/js/jquery.min.js?v=2.0"></script>

    <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
  <script type="text/javascript" src="contents/js/jquery-ui.js?v=2.0"></script>
  <script type="text/javascript" src="contents/js/ulo.js?v=2.1.2"></script>
  <script type="text/javascript" src="contents/js/readmore.js?v=2.0"></script>
    <script type="text/javascript" src="contents/js/bootstrap.min.js?v=2.0"></script>
<!-- coupon code copy script -->

</html>