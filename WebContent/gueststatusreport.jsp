<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="s" uri="/struts-tags"%>

   <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <section class="content-header">
          <h1>
            Guest Booking Status
            <!--<small>Subscription Expiry on {{property[0].subscription[0].expirydate}}</small>-->
          </h1>
          <ol class="breadcrumb">
            <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Guest Booking Status</a></li>
            <li class="active">Guest Booking Status</li>
          </ol>
        </section>
 		
 		<!-- Main content -->
    <section class="content">
    <div class="box box-default color-palette-box">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-tag"></i> Search &amp; Filter</h3>
        </div>
        <div class="box-body">
        <form>
          <div class="row">
          <div class="col-sm-2 col-md-3">
           <div class="form-group">
                <label>From Date:</label>

                <div class="input-group date">
                <label class="input-group-addon btn" for="fromDate">
					<span class="fa fa-calendar"></span>
				</label>
                  <input type="text" class="form-control pull-right" id="fromDate" name="fromDate" value="" onkeypress="return false;" autocomplete="off">
                </div>
              </div>
              
              </div>
              <div class="col-sm-2 col-md-3">
           <div class="form-group">
                <label>To Date:</label>

                <div class="input-group date">
                  <label class="input-group-addon btn" for="toDate">
					<span class="fa fa-calendar"></span>
				</label>
                  <input type="text" class="form-control pull-right" id="toDate" name="toDate" value="" onkeypress="return false;" autocomplete="off">
                </div>
                <!-- /.input group -->
              </div>
              
              </div>
              <div class="col-sm-2 col-md-4">
              <div class="form-group">
                <label>Select Hotel</label>
                <select class="form-control" style="width: 100%;" id="selectedPropertyId" name="selectedPropertyId"> <!-- class=__select2 -->
                  <option  value="">Choose Your Property</option>
                  <option ng-repeat="ap in allProperties" value="{{ap.propertyId}}">{{ap.propertyName}}</option>
               </select>
              </div>
              </div>
            
              <div class="col-sm-2 col-md-2">
	              <div class="form-group">
	<!--               <label>Search</label> -->
					<label>&nbsp;</label>
	              	<button type="submit" ng-click="getPropertyBookingList()" class="btn btn-danger pull-right btn-block btn-sm">Search</button>
	              	</div>
              </div>
             
          </div>
          <div class="row">
	        <div class="form-group col-md-2">
				<label>Booking Id</label>
				<input type="text"  name="filterBookingId" class="form-control" id="filterBookingId" ng-pattern="/^[0-9]/" maxlength="10" placeholder="Booking Id">
			  </div>
			
                 <div class="col-sm-2 col-md-2">
	              <div class="form-group">
	<!--               <label>Search</label> -->
					<label>&nbsp;</label>
	              	<button type="submit" ng-click="getPropertyBookingList()" class="btn btn-danger pull-right btn-block btn-sm">Search</button>
	              	</div>
              </div>
              <div class="col-sm-2 col-md-2">
	              <div class="form-group">
	<!--               <label>Search</label> -->
					<label>&nbsp;</label>
	              <button class="btn btn-danger pull-right btn-block btn-sm" ng-csv="bookinglist" filename="bookingreport.csv" 
					csv-header="['Booking Id','OTA Booking Id','Hotel Name','Guest Name','Mobile Number','Email Id',
					'Booking Date','Check-In','Check-Out','Accommodations','Rooms','Nights','No of Adult',
					'No of Child','Total Amount','Profit & Loss','Remarks','Status','Source']" field-separator="," decimal-separator=".">Export to CSV</button>	
	              	</div>
              </div>
              <div class="col-sm-2 col-md-2">
	              <div class="form-group">
	              <label> 
                        <input type="checkbox" name="noShowCancel" class="selectTwo" value="1">No Show/Cancellation
                      </label>
                   </div>
              </div>
              <div class="col-sm-2 col-md-2">
                  <div class="form-group">
	              <label> 
                        <input type="checkbox" name="noShowCancel" class="selectTwo" value="2"> Confirm/Arrived
                      </label>
                   </div>
              </div> 
              
         </div>
          <!-- /.row -->
          </form>
        </div>
        <!-- /.box-body -->
      </div>
      
      
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Results</h3>

              
            </div>
            <div  class="box-body table-responsive no-padding" style="height:300px;"  ng-if="bookinglist.length==0">
            	<table class="table table-hover">
               <tr>
                	<td  colspan="8">No Records found
                	</td>
                </tr>
                </table>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding" style="height:300px;" ng-if="bookinglist.length>0">
              <table class="table table-hover">
                <tr>
                  <th style="overflow:hidden;white-space:nowrap;">Booking ID</th>
                  <th>Hotel Name</th>
                  <th>Guest</th>
                  <th>Mobile</th>
                  <th>Email</th>
                  <th>Booking Date</th>
                  <th style="overflow:hidden;white-space:nowrap;">Check-In <br> Check-Out</th>
                  
                  <th>Accommodations</th>
                  <th>Rooms</th>
                  <th>Nights</th>
                  <th>Adult</th>
                  <th>Child</th>
                  <th>Total Amount</th>
                  <th>Profit & Loss</th>
                  <th>Status</th>
                </tr>
                <tr ng-repeat="bl in bookinglist"> 
                  <td>{{bl.bookingId}}</td>
                  <td>{{bl.propertyName}}</td>
                  <td>{{bl.guestName}}</td>
                  <td>{{bl.mobileNumber}}</td>
                  <td style="overflow:hidden;white-space:nowrap;">{{bl.emailId}}</td>
                  <td style="overflow:hidden;white-space:nowrap;">{{bl.bookingDate}}</td>
                  <td style="overflow:hidden;white-space:nowrap;">{{bl.checkIn}}<br>{{bl.checkOut}}</td>
               
                  <td>{{bl.accommodationType}}</td>
                  <td>{{bl.rooms}}</td>
                  <td>{{bl.diffDays}}</td>
                  <td>{{bl.adultsCount}}</td>
                  <td>{{bl.childCount}}</td>
                  <td>{{bl.totalAmount}}</td>
                  <td>{{bl.profitLossAmount}}</td>
                  <td>{{bl.paymentRemarks}}</td>
                </tr>
                
             </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
      
    </section>
 		
 		
</div>
    <input type="hidden" value ="<%=session.getAttribute("propertyId") %>" id="propertyId" name="propertyId" >
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>
<!-- csv download script start -->
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.15/angular-sanitize.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ng-csv/0.3.6/ng-csv.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.0rc1/angular-route.min.js"></script>
<!-- csv download script end -->
	 <script src="js/jquery-2.1.1.js" type="text/javascript"></script>

	<script>
	var app = angular.module('myApp', ['ngProgress','ngSanitize', 'ngCsv']);
	app.controller('customersCtrl', function($scope, $http,$timeout,ngProgressFactory) {
		
		  $("#fromDate").datepicker({
		      dateFormat: 'MM d, yy',
		    
		      onSelect: function (formattedDate) {
		          var date1 = $('#fromDate').datepicker('getDate'); 
		          var date = new Date( Date.parse( date1 ) ); 
		         // date.setDate( date.getDate() + 1 );        
		          var newDate = date.toDateString(); 
		          newDate = new Date( Date.parse( newDate ) );   
		          $('#toDate').datepicker("option","minDate",newDate);
		          $timeout(function(){
		        	  document.getElementById("fromDate").style.borderColor = "LightGray";
		          });
		      }
		  });

		  $("#toDate").datepicker({
		      dateFormat: 'MM d, yy',
		   
		      onSelect: function (formattedDate) {
		          var date2 = $('#toDate').datepicker('getDate'); 
		          $timeout(function(){
		        	  document.getElementById("toDate").style.borderColor = "LightGray";
		          });
		      }
		  }); 
		
		
		 $scope.getPropertyBookingList = function(){
			 var id = $('#noShowCancel').val(); 
			 var favorite = "";
	            $.each($("input[name='noShowCancel']:checked"), function(){            
	                //favorite.push($(this).val());
	                //favorite.join(", ");
	                favorite += $(this).val();
	            });
			 var fdata="",filterBookingId="";
			 filterBookingId=document.getElementById('filterBookingId').value;
				if(filterBookingId==""){
					filterBookingId=0;	
					if(document.getElementById('fromDate').value==""){
			 			 document.getElementById('fromDate').focus();
			 			 document.getElementById("fromDate").style.borderColor = "red";
			 			 return;
			 		 }else if(document.getElementById('toDate').value==""){
			 			 document.getElementById('toDate').focus();
			 			document.getElementById("toDate").style.borderColor = "red";
			 			return;
			 		 }else if(document.getElementById('selectedPropertyId').value==""){
			 			document.getElementById('selectedPropertyId').focus();
			 			document.getElementById("selectedPropertyId").style.borderColor = "red";
			 			 return;
			 		 }else if(document.getElementById('fromDate').value!="" && document.getElementById('toDate').value!=""){
			 			fdata = "strFromDate="+$('#fromDate').val()
						+ "&strToDate="+$('#toDate').val()
						+"&propertyId="+$('#selectedPropertyId').val()+"&filterBookingId="+filterBookingId;	 
			 		 }
				}else{
					fdata = 'filterBookingId='+filterBookingId;
					document.getElementById('fromDate').value=="";
					document.getElementById('toDate').value=="";
					document.getElementById('selectedPropertyId').value=="";
				}
						$http(
								{
									method : 'POST',
									data : fdata,
									headers : {
										'Content-Type' : 'application/x-www-form-urlencoded'
									},
									url : 'property-booking-report?statusId='+favorite
								}).success(function(response) {
									
									$scope.bookinglist = response.data;
									
								});
				
			};
			
			$scope.getExportBookingList = function(){
				
				var fdata = "strFromDate=" + $('#fromDate').val()+ "&strToDate=" + $('#toDate').val()
				+ "&accommodationId="+$('#accommodationId').val()+"&propertyId="+$('#propertyId').val();
				//var fdata = "&arrivalDate=" + ""+ "&departureDate=";
	    		
				$http(
						{
							method : 'POST',
							data : fdata,
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							url : 'export-rpt-booking'
						}).success(function(response) {
							
							$scope.bookinglist = response.data;
// 							alert("Report Saved Successfully in Download's Folder");
							
						});

			};
			
			
			var propertyId = $('#propertyId').val(); 	
			$scope.getAccommodations = function() {

				var url = "get-front-accommodations?propertyId="+propertyId;
				$http.get(url).success(function(response) {
				    //console.log(response);
					$scope.accommodations = response.data;
		
				});
			};    
			
			 
					
					$scope.getAllProperties = function() {

						var url = "get-active-promotion-property";
						$http.get(url).success(function(response) {
						    //console.log(response);
							$scope.allProperties = response.data;
				
						});
					};
			
			
			
			$scope.getAccommodations();
			
			$scope.getAllProperties();
			
			
			
		
	});
		</script>
		
		<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
		  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
		  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		  <script src="plugins/datepicker/bootstrap-datepicker.js"></script>
		  
		  <script>
 
//Date picker

  

  </script>
