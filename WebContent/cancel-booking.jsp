<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!-- <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/themes/south-street/jquery-ui.min.css" />
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<style>
.tablerow{
    overflow-x:scroll;
    width:1000px;
}

table.table-bordered > tbody > tr > th {
//border:1px solid #3c8dbc ;
}
table.table-bordered  {
  // border:1px solid #3c8dbc ;
/*     margin-top:20px; */
 }
table.table-bordered > thead > tr > th{
   //border:1px solid #3c8dbc ;
}
table.table-bordered > tbody > tr > td{
   //border:1px solid #3c8dbc ;
}
.tablesuccess{
    background-color: #092f53 !important;
    color:#ffffff !important;
    text-align:center;
}
</style>
   <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
           Booking Cancellation
          </h1>
          <ol class="breadcrumb">
            <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        
            <li class="active">Booking Cancellation</li>
          </ol>
        </section>
		
        <!-- Main content -->
        <section class="content">
             <div class="box box-default color-palette-box">
              <div class="box-header with-border">
		          <h3 class="box-title"><i class="fa fa-tag"></i>Search Booking Details</h3>
		        </div>
     		    <div class="box-body">
		        <form name="bookingForm">
			        <div class="row">
			               <div class="form-group col-md-2">
			                <label>Booking Id:</label>
			                  <input type="text" id="filterBookingId" class="form-control" name="filterBookingId"  value="" ng-required="true" autocomplete="off">
			              </div>
			              <div class="col-md-2">
			              <button type="submit" ng-click="cancelBooking()" class="btn btn-block btn-primary btn-sm" style="margin-top:25px">Search</button>
			             </div>
			         </div>
		        </form>
	            
               	<div class="table-responsive no-padding"  > 
					<table class="table table-bordered" id="example1">
						<tr class="tablesuccess">
							<th>Choose</th>
							<th>Room Category</th>										
							<th>Room Count</th>
							<th>Check In</th>
							<th>Check Out</th>
							<th>Adult</th>
							<th>Child</th>
						    <th>Tariff</th>
						    <th>Tax</th>
							<th>Advance</th>
							<th>Due</th>
<!-- 							<th>Reduced Amount</th> -->
<!-- 							<th>Refund Amount</th> -->
<!-- 							<th>Remarks</th> -->
						</tr>
						<tr ng-repeat="bl in bookinglist track by $index">
							<h4>Booking Details</h4>
							<td id="selectedAllRooms"><input type="checkbox" ng-model="selectedRooms" name="selectedRooms"  id="{{bl.accommodationId}}" value="{{bl.accommodationId}}" ng-click="selectCategory(bl.accommodationId)"></td>
						    <td>{{bl.accommodationType}}</td>
							<td>{{bl.rooms}}</td>
							<td>{{bl.checkIn}}</td>
							<td>{{bl.checkOut}}</td>
							<td>{{bl.adultCount}}</td>
						    <td>{{bl.childCount}}</td>
						    <td>{{bl.baseAmount}}</td>
						    <td>{{bl.taxAmount}}</td>
						    <td>{{bl.advanceAmount}}</td>
						    <td>{{bl.dueAmount}}</td>
<!-- 						    <td>{{bl.reducedAmount}}</td> -->
<!-- 						    <td>{{bl.refundAmount}}</td> -->
<!-- 						    <td>{{bl.refundTerms}}</td> -->
						</tr>
					</table>
							
					<input type="hidden" id="hidBookingId" name="hidBookingId" value="{{bookinglist[0].bookingId}}">		
					<div id="cancelError"></div>
					<div class="modal-footer">
						 <button ng-click="getCancellationDetails()"  id="loading-example-btn" class="btn btn-primary">Send Voucher</button> <!-- ng-disabled="addguestinformation.$invalid" -->
					</div>
					
				</div>
				</div>
				</div>
        </section>
     </div>
              	  

          
      


<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>

<script src="js/jquery-2.1.1.js" type="text/javascript"></script>

	<script>
	
	var app = angular.module('myApp', ['ngProgress']);
	app.controller('customersCtrl', function($scope, $http,$timeout,ngProgressFactory) {

       
		 $('.select2').select2()
       	$timeout(function(){
      	    // $scope.progressbar.complete();
            $scope.show = true;
            $("#pre-loader").css("display","none");
        }, 2000);

		 var filterAccommId=""; 
		
		$scope.unread = function() {
		//var notifiurl = "http://localhost:8085/collaborative-workflow/unreadnotifications.action";
		var notifiurl = "unreadnotifications.action";
		$http.get(notifiurl).success(function(response) {
			$scope.latestnoti = response.data;
		});
		};
		
		$scope.selectCategory= function(accommId){
			
			filterAccommId = $(':checkbox[name=selectedRooms]:checked').map(function() {
		        return this.id;
		    }).get();
			
		};
		
		var spinner = $('#loadertwo');
		
		$scope.cancelBooking = function(){
			 
				var filterBookingId="";
				
				filterBookingId=document.getElementById('filterBookingId').value;
				if(filterBookingId==""){
					filterBookingId="0";
				}
				
				var fdata = 'filterBookingId='+filterBookingId;
	    		spinner.show();	    		
				$http(
					{
						method : 'POST',
						data : fdata,
						headers : {
							'Content-Type' : 'application/x-www-form-urlencoded'

						},
						url : 'get-booking-id-details'
					}).success(function(response) {
						
						$scope.bookinglist = response.data;
						   setTimeout(function(){ spinner.hide(); 
							  }, 1000);
						   if($scope.bookinglist.length == 0){
							   $("#cancelError").hide();
				                  var alertmsg = ' <div class="alert" style="color:red;text-align:center;">please choose to property cancel the booking </div>';
				                  
				                $('#cancelError').html(alertmsg);
				         	            $("#cancelError").fadeTo(2000, 500).slideUp(500, function(){
				         	                $("#cancelError").slideUp(500);
				                  });
						   }
					},function errorCallback(response){
						alert("error")
						 $("#cancelError").hide();
			                  var alertmsg = ' <div class="alert" style="color:red;text-align:center;">process failed, please try again!!</div>';
			                  
			                $('#cancelError').html(alertmsg);
			         	            $("#cancelError").fadeTo(2000, 500).slideUp(500, function(){
			         	                $("#cancelError").slideUp(500);
			                  });
					});
	 		 
			};
			
		$scope.getCancellationDetails = function(){
			
			var filterBookingId="";
			filterBookingId=document.getElementById("hidBookingId").value;
			var text = '{"listCancelBookingDetails":' + JSON.stringify($scope.bookinglist) + '}'; 
			console.log(text);
			var mailFlag=true;
		    var data1 = JSON.parse(text);
			var gdata = angular.toJson(data1);
		
			
			var selectedlength=$(':checkbox[name=selectedRooms]:checked').length;
			if(selectedlength!=0){
				var checkedBookings=confirm("Do you want to Cancel the Bookings ?");
				if(checkedBookings){
					
					var fdata = 'filterAccommId='+filterAccommId+'&filterBookingId='+filterBookingId;
					spinner.show();
					$http(
						{
							method : 'POST',
							data : fdata,
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							url : 'edit-cancel-booking-details'
						}).then(function successCallback(response) {
								$http(
							    {
									method : 'POST',
									data : gdata,
									
									headers : {
// 										'Content-Type' : 'application/x-www-form-urlencoded'
				 							'Content-Type' : 'application/json; charset=utf-8'
									},
								url : 'get-cancel-booking-details?mailFlag='+mailFlag+'&filterAccommId='+filterAccommId+'&filterBookingId='+filterBookingId
							   }).then(function successCallback(response) {
								   setTimeout(function(){ spinner.hide(); 
									  }, 1000);
								window.location.reload();
							    alert("Booking Cancelled successfully");
							   }, function errorCallback(response) {
								   setTimeout(function(){ spinner.hide(); 
									  }, 1000);
								   alert('Process Failed, Please Try Again!! ');
							   });
								
							
				}, function errorCallback(response) {
					alert('Process Failed, Please Try Again!! ');
				}); 
				}else{
					
				}
			}else{
				alert('select the category');
			}
			
			
		};	
			
	});

	</script>
  <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

	  <!-- iCheck 1.0.1 -->
    <script src="plugins/iCheck/icheck.min.js"></script>
	    
   <style>
 .select2-container--default .select2-selection--multiple .select2-selection__choice {
    background-color: green;
    border: 1px solid #aaa;
    border-radius: 4px;
    cursor: default;
    float: left;
    margin-right: 5px;
    margin-top: 5px;
    padding: 0 5px;
}
 </style>
