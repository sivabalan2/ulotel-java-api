<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="s" uri="/struts-tags"%>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
   <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
           Inactive Area
          </h1>
          <ol class="breadcrumb">
            <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Inactive Area</li>
           
             
          </ol>
          
        </section>
		
        <!-- Main content -->
        <section class="content">
        
        	<div class="row">
            	<div class="col-xs-12" ng-if="areas.length>=0">
						<div class="box">
							<div class="box-header">
								<h3 class="box-title">Area</h3>
								 <a class="btn btn-primary pull-right" href="create-area">Add Area</a>
							</div>
							
							
							<!-- /.box-header -->
							<div class="box-body table-responsive no-padding" > 
								<table class="table table-hover" id="example1">
									<tr>
										<th>Order</th>
										<th>Location </th>
										<th>Area Name</th>
										<th>Edit</th>
										<th>Delete</th>
										<th>User Name</th>
									</tr>
									<tr ng-repeat="a in areas">
										<td>{{a.serialNo}}</td>
										<td>{{a.areaLocationName}}</td>
										<td>{{a.areaName}}</td>
										<td>
											<a href="#editmodal" ng-click="getInactiveArea(a.DT_RowId)"  data-toggle="modal"><i  class="fa fa-pencil text-green"></i></a>
										</td>
										<td>
											<a href="#deletemodal" ng-click="deleteArea(a.DT_RowId)" data-toggle="modal" ><i  class="fa fa-fw fa-trash text-red" ></i></a>
										</td>
										<td>{{a.createdBy}}</td>
									</tr>
									<tr>
									</tr>
								</table>
							</div>
							<!-- /.box-body -->
							
							
							
							<div class="box-footer clearfix">
								<ul class="pagination pagination-sm"></ul>
								<div class="simple-pagination pull-right" >   
									<p class="simple-pagination__items">Showing {{areaCount[0].count}} out of {{settings.pageLimit}}</p>
									<p><button ng-click="previousPage()" ng-disabled="settings.currentPage <= 0" class="simple-pagination__button simple-pagination__button--prev"> &#10094;          
									</button><span class="simple-pagination__pages">{{settings.currentPage + 1}} of {{getTotalPages()}}</span>
									<button ng-click="nextPage()" ng-disabled="settings.currentPage === (getTotalPages() - 1)" class="simple-pagination__button simple-pagination__button--next">&#10095;</button></p>
									<a href="" ng-click="limited(10)" ng-class="active">{{10}}</a> |
									<span><a href="" ng-click="all(areaCount[0].count)" ng-class="active">All</a></span> 
								</div>
						   </div> 
						</div>
						<!-- /.box -->
					</div>	
         	</div>
          
        </section><!-- /.content -->
      	<input type="hidden" value ="<%=session.getAttribute("areaId") %>" id="areaId" name="areaId" >
      	
		<div class="modal" id="editmodal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" id="editbtnclose" class="close" data-dismiss="modal"
							aria-hidden="true">x</button>
						<h4 class="modal-title">Edit Area</h4>
					</div>
					 	 <form method="post" theme="simple" name="editarea">
					 	 <section>
						<div class="modal-body">
							<input type="hidden" value="{{area[0].areaId}}" id="editAreaId" name="editAreaId">
						    <input type="hidden" value="{{area[0].areaLocationId}}" id="editAreaLocationId" name="editAreaLocationId">
						    <div class="form-group col-md-12">
								<label>Area Name<span style="color:red">*</span></label>
								<input type="text" class="form-control"  name="editAreaName"  id="editAreaName"  value="{{area[0].areaName}}" ng-required="true">
							    <span ng-show="editarea.editAreaName.$error.pattern">Not a Area Name!</span>
							</div>
							<div class="form-group col-md-12">
								<label>Area Name<span style="color:red">*</span></label>
								<input type="text" class="form-control"  name="editAreaDescription"  id=editAreaDescription  value="{{area[0].areaDescription}}" ng-required="true">
							    <span ng-show="editarea.editAreaDescription.$error.pattern">Not a Area Name!</span>
							</div>
							<div class="form-group col-md-12">
								<label>Area URL<span style="color:red">*</span></label>
								<input type="text" class="form-control"  name="editAreaUrl"  id="editAreaUrl"  value="{{area[0].areaUrl}}" ng-required="true">
							    <span ng-show="editarea.editAreaUrl.$error.pattern">Not a Area URL!</span>
							</div>
							
							  <div class="form-group col-md-12">
			                     <label>Status <span style="color:red">*</span> </label>
				                 <select name="areaIsActive" id="areaIsActive" class="form-control" ng-model ="areaIsActive"  ng-change="showRedirectInput(areaIsActive)">
					                     <option value="true" >Active</option>
					                     <option value="false" ng-selected='true'>InActive</option>
				                 </select>
			                   </div>
			                
			                
			                
			                <div class="form-group col-md-12">
							<label>Redirect Area URL<span style="color:red">*</span></label>
							<input type="text" class="form-control"  name="editRedirectAreaUrl"  id="editRedirectAreaUrl"  value="{{area[0].redirectUrl}}" ng-required="true">
						    </div>
						    
						    
			                  
							   
							   
							   
							   
							
								
						</div>
						</section>
						<section>
							<div class="modal-footer">
								<a href="#" data-dismiss="modal" class="btn btn-danger">Close</a>
								<button ng-click="editarea.$valid && editArea()" ng-disabled="editarea.$invalid" id ="updateArea" class="btn btn-primary">Update</button>
							</div>  
						</section>
						</form>
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dalog -->
		</div>
		
		
  

      
      

<script src="js/autocomplete.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>
<script>


 
 
 
	
	var app = angular.module('myApp',['ngFileUpload']);
	app.controller('customersCtrl',['$scope',
	             					'Upload',
	            					'$timeout',
	            					'$http', function($scope,Upload,$timeout,$http) {

		
		
		$scope.settings = {
	            currentPage: 0,
	            offset: 0,
	            pageLimit: 10,
	            pageLimits: ['10', '50', '100']
	          };

		 $scope.getTotalPages = function () {
			return Math.ceil($scope.areaCount[0].count / $scope.settings.pageLimit);
		};
			
		 $scope.isCurrentPageLimit = function (value) {
		       return $scope.settings.pageLimit == value;
		 };
		 
		 $scope.previousPage = function () {
			   $scope.settings.currentPage -= 1;
			   var previousOffset = $scope.settings.currentPage * $scope.settings.pageLimit;
		       var url = "get-inactive-areas?limit="+$scope.settings.pageLimit+"&offset="+previousOffset;
				$http.get(url).success(function(response) {
					//console.log(response);
					$scope.areas = response.data;
				}); 
		    }; 
		 
     		$scope.nextPage = function () {
		       $scope.settings.currentPage += 1;
		       var nextOffset = $scope.settings.currentPage * $scope.settings.pageLimit;
		       var url = "get-inactive-areas?limit="+$scope.settings.pageLimit+"&offset="+nextOffset;
				$http.get(url).success(function(response) {
					//console.log(response);
					$scope.areas = response.data;
				}); 
		     }; 
		       
      			$scope.all = function (total) {
		    	   $scope.settings.pageLimit = total;
			       var nextOffset = 0;
			       var url = "get-inactive-areas?limit="+total+"&offset="+nextOffset;
					$http.get(url).success(function(response) {
						//console.log(response);
						$scope.areas = response.data;
					}); 
			    };
			       
      			 $scope.limited = function (limit) {
			    	   $scope.settings.pageLimit = limit;
				       var nextOffset = 0;
				       var url = "get-inactive-areas?limit="+limit+"&offset="+nextOffset;
						$http.get(url).success(function(response) {
							//console.log(response);
							$scope.areas = response.data;
					}); 
		       };			

		    
		
	       	$timeout(function(){
	      	    // $scope.progressbar.complete();
	            $scope.show = true;
	            $("#pre-loader").css("display","none");
	        }, 2000);

			 
			
			$scope.unread = function() {
			//var notifiurl = "http://localhost:8085/collaborative-workflow/unreadnotifications.action";
			var notifiurl = "unreadnotifications.action";
			$http.get(notifiurl).success(function(response) {
				$scope.latestnoti = response.data;
			});
			};

			
			
			$scope.getAreas = function() {

				var url = "get-inactive-areas?limit="+$scope.settings.pageLimit+"&offset="+$scope.settings.offset;
				$http.get(url).success(function(response) {
				   
					$scope.areas = response.data;
		
				});
			};
			
			
			$scope.addArea = function(){
				
				 var file = $scope.files;
				 console.log(file); 
				// var file = this.files[0];
				 var fileType = file["type"];
				 var ValidImageTypes = ["image/gif", "image/jpeg", "image/png"];
				 if ($.inArray(fileType, ValidImageTypes) < 0) {
					     // invalid file type code goes here.
					     alert('image invalid')
					}
					else{
						
						var fdata = "areaLocationId=" + $('#areaLocationId').val()
							+ "&areaName=" + $('#areaName').val()
							+ "&areaUrl=" + $('#areaUrl').val()
						+ "&areaDescription=" + $('#areaDescription').val(); 
						
						$http(
								{
									method : 'POST',
									data : fdata,
									headers : {
										'Content-Type' : 'application/x-www-form-urlencoded'
									},
									url : 'add-new-area'
								}).then(function successCallback(response) {
									$scope.addedareas = response.data;
									
									console.log($scope.addedareas.data[0].locationAreaId);
							        
									var locationAreaId = $scope.addedareas.data[0].locationAreaId;
									 Upload.upload({
							                url: 'areapicupdate',
							                enableProgress: true, 
							                data: {myFile: file, 'locationAreaId': locationAreaId} 
							            }).then(function (resp) {
							            	
							            	
							            });
								
										
									$scope.getAreas();
									var alertmsg = ' <div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Alert!</h4>Success Item Added Succesfully.</div>';
						            $('#message').html(alertmsg);
						            //window.location.reload(); 
									$timeout(function(){
							      	$('#btnclose').click();
							        }, 2000); 
						        	
						        
						}, function errorCallback(response) {
							
						});
				}
			};
			
			$scope.editArea = function(){
				
                        var fdata = "areaLocationId=" + $('#editAreaLocationId').val()	
						+ "&areaName=" + $('#editAreaName').val()
						+ "&locationAreaId=" + $('#editAreaId').val()
						+ "&areaUrl=" + $('#editAreaUrl').val()
						+ "&areaIsActive=" + $('#areaIsActive').val()
						+ "&areaRedirectUrl=" + $('#editRedirectAreaUrl').val()
						+ "&areaDescription=" + $('#editAreaDescription').val(); 
						
                       
                        
						$http(
								{
									method : 'POST',
									data : fdata,
									headers : {
										'Content-Type' : 'application/x-www-form-urlencoded'
									},
									url : 'edit-new-area'
								}).then(function successCallback(response) {
									
									alert("area updated sucessfully");
									
									window.location.reload();
									 
									/* Upload.upload({
						                url: 'areapicupdate',
						                enableProgress: true, 
						                data: {myFile: file, 'locationAreaId': $('#editAreaId').val()} 
						            }).then(function (resp) {
						                window.location.reload(); 
						            	
						            }); */
									
									
						}, function errorCallback(response) {
							
						});
					
			};
			
            
	    	 $scope.getLocations = function() {

				var url = "get-area-locations";
				$http.get(url).success(function(response) {
				    console.log(response);
					$scope.locations = response.data;
		
				});
			};
			
			

	    	 $scope.getAreaUrls = function()
	    	 {

				var url = "get-all-areas";
				$http.get(url).success(function(response) {
				   
					$scope.areaUrls = response.data;
		
				});
			};
			
            
			$scope.getArea = function(rowid) {
				var url = "get-area-table?areaId="+rowid;
				$http.get(url).success(function(response) {
				    console.log(response);
					$scope.area = response.data;
		
				});
				
			};
			
			
			$scope.getInactiveArea = function(rowid) {
				var url = "get-inactive-area-table?areaId="+rowid;
				$http.get(url).success(function(response) {
				   
					$scope.area = response.data;
		
				});
				
			};
			
			//$scope.showRedirectUrl = false;
			$('#showRedirectUrl').hide();
			$scope.isValidate = false;
			$scope.showRedirectInput = function(val) {
				//alert(val);
				if(val == "true"){
				
				$('#showRedirectUrl').hide();	
				//$('#updateArea').prop('disabled',false);
				}
				else{
				$('#showRedirectUrl').show();
				$scope.isValidate = true;
				}
				
			};
			
			
            			
			$scope.deleteArea=function(rowid){
				var fdata = "&areaId="+ rowid
				var deleteLocationCheck=confirm("Do you want to delete the area?");
				if(deleteLocationCheck){
					$http(
							{
								method : 'POST',
								data : fdata,
								headers : {
									'Content-Type' : 'application/x-www-form-urlencoded'
								},
								url: 'delete-area'
							}).then(function successCallback(response) {
								window.location.reload();
							   }, function errorCallback(response) {
								
								
							});
				}
				else{
					
				}
				
			};
			
			$scope.getAreaCount = function() {

				var url = "get-inactive-area-count";
				$http.get(url).success(function(response) {
				    //console.log(response);
					$scope.areaCount = response.data;
		            //console.log($scope.accommodationRoomsCount.data[0].size);
				});
			};
			
			
			
			$scope.getAreas();
			$scope.getLocations();
			$scope.getAreaCount();
			$scope.getAreaUrls();
			
	}]);


	app.directive('fileInput', ['$parse', function ($parse) {
	    return {
	        restrict: 'A',
	        link: function (scope, element, attributes) {
	            element.bind('change', function () {
	                $parse(attributes.fileInput)
	                .assign(scope,element[0].files[0])
	                scope.$apply()
	            });
	        }
	    };
	}]);		
		   
	
		
	</script>
    	
	<link rel="stylesheet" href="dist/css/dataTables.bootstrap.css">
	<script src="js1/upload/ng-file-upload-shim.min.js"></script>
	<script src="js1/upload/ng-file-upload.min.js"></script>
	 <script src="js1/jquery.dataTables.min.js" type="text/javascript"></script>
	 <script src="js1/dataTables.bootstrap.min.js" type="text/javascript"></script>
       