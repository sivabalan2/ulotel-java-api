<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="s" uri="/struts-tags"%>

   <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Accommodation Rooms
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Property</a></li>
            <li class="active">Accomdation Rooms</li>
          </ol>
        </section>
		
        <!-- Main content -->
        <section class="content">
        
        	<div class="row">
            	<div class="col-xs-12" ng-if="accommodationRooms.length>=0">
						<div class="box">
							<div class="box-header">
								<h3 class="box-title">Room Inventory</h3>
							</div>
							
							
							<!-- /.box-header -->
							<div class="box-body table-responsive no-padding" > 
								<table class="table table-hover" id="example1">
									<tr>
					
										<th>Room Alias </th>
										<th>Sort key</th>
										<th>Edit</th>
										<th> Delete</th>
									
									</tr>
									<tr ng-repeat="ar in accommodationRooms">
										<td>{{ar.roomAlias}}</td>
										<td>{{ar.sortKey}}</td>
										<td>
											<a href="#editmodal" ng-click="getAccommodationRoom(ar.propertyAccommodationRoomId)"  data-toggle="modal"><i  class="fa fa-pencil text-green"></i></a>
										</td>
										<td>
										<a href="#deletemodal" ng-click="deleteAccommodationRoom(ar.propertyAccommodationRoomId)" ><i  class="fa fa-fw fa-trash text-red"></i></a>
										</td>
									</tr>
									<tr>
									</tr>
								</table>
							</div>
							<!-- /.box-body -->
							
							
							  <div class="box-footer clearfix">
								 <ul class="pagination pagination-sm no-margin pull-right">
									<a class="btn btn-primary  btngreen" href="#addmodal" ng-click="enableTask(x.DT_RowId)"  data-toggle="modal" >Add Rooms</a>
								</ul> 
							</div>
							
							<div class="box-footer clearfix">
							<!--  <p> {{ accommodationRoomsCount[0].count }} </p>-->
								 <ul class="pagination pagination-sm no-margin pull-right">
								  <div class="simple-pagination">   
								   <p class="simple-pagination__items">Showing {{settings.pageLimit}} out of {{accommodationRoomsCount[0].count}}</p>
								   <p><button ng-click="previousPage()" ng-disabled="settings.currentPage <= 0" class="simple-pagination__button simple-pagination__button--prev"> &#10094;          
								   </button><span class="simple-pagination__pages">{{settings.currentPage + 1}} of {{getTotalPages()}}</span>
								   <button ng-click="nextPage()" ng-disabled="settings.currentPage === (getTotalPages() - 1)" class="simple-pagination__button simple-pagination__button--next">&#10095;</button></p>
								   <a href="" ng-click="limited(10)" 
								    ng-class="active">{{10}}</a> |
								   <span><a href="" ng-click="all(accommodationRoomsCount[0].count)" ng-class="active">All</a></span> 
						    </div>   
							
						</div>
						<!-- /.box -->
					</div>	
         	</div>
          
        </section><!-- /.content -->
      	
      	<div class="modal" id="addmodal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" id="btnclose" class="close" data-dismiss="modal"
							aria-hidden="true">x</button>
						<h4 class="modal-title">Add Rooms category</h4>
					</div> 
					 <form name="addrooms">
					<div class="modal-body" >
					<div id="message"></div>
					  	 <div class="form-group col-md-6">
							<label>Select Accomadtion Type</label>
							<select name="propertyAccommodationId" id="propertyAccommodationId" value="" class="form-control">
	                       	<option ng-repeat="at in accommodations" value="{{at.accommodationId}}">{{at.accommodationType}}</option>
	                        </select>
						  </div>
					    <div class="form-group col-md-6">
							<label for="room_alias">Room Alias</label>
							<input type="text" class="form-control"  name="roomalias" id="roomalias" placeholder="Room Alias" ng-model='roomalias' ng-pattern="/^[A-Za-z\d\s]+$/" ng-required="true" maxlength="20">
						     <span ng-show="addrooms.roomalias.$error.pattern">Not a Valid Room Alias!</span>
						</div>
						
						<div class="form-group col-md-6">
							<label for="sort_key">Sort key</label>
							<input type="text" class="form-control"  name="sortKey"  id="sortKey"  placeholder="sort key" ng-model='SortKey' ng-pattern="/^[a-zA-Z0-9]+$/" ng-required="true" maxlength="10">
						    <span ng-show="addrooms.sortKey.$error.pattern">Not a Valid Sort Key!</span>
						     
						</div>
								<div class="form-group col-md-6">
							<label for="floor">Floor</label>
							<input type="text" class="form-control"  name="floor"  id="floor"  placeholder="No of Floor" ng-model='floor' ng-pattern="/^[0-9]/" ng-required="true" maxlength="10">
						    <span ng-show="addrooms.floor.$error.pattern" style="color:red" >Not a Valid Number!</span>
						     
						</div>					
		                 <div class="form-group col-md-6">
							<label for="area_square_feet">Area Square Feet</label>
							<input type="text" class="form-control"  name="areaSqft"  id="areaSqft"  placeholder="Area Square Feet Covered" ng-model='areaSqft' ng-pattern="/^[0-9]/" ng-required="true" maxlength="10">
						    <span ng-show="addrooms.areaSqft.$error.pattern"  style="color:red">Enter the numeric value!</span>
						      
						</div>
						<div class="form-group col-md-6">
							<label for="phone_extension">Phone Extension</label>
							<input type="text" class="form-control"  name="phoneExtension"  id="phoneExtension"  placeholder="Enter The Phone" ng-model='phoneExtension' ng-pattern="/^[0-9]/" ng-required="true" maxlength="10">
						    <span ng-show="addrooms.phoneExtension.$error.pattern" style="color:red">Enter the numeric value!</span>
						      
						</div>
							<div class="form-group col-md-6">
							<label for="data_extension">data Extension</label>
							<input type="text" class="form-control"  name="dataExtension"  id="dataExtension"  placeholder="Enter The data" ng-model='dataExtension' ng-pattern="/^[A-Za-z\d\s]+$/" ng-required="true" maxlength="10">
						    <span ng-show="addrooms.dataExtension.$error.pattern">Enter the numeric value!</span> 
						</div>
								<div class="form-group col-md-6">
							<label for="remarks">Remarks</label>
							<input type="text" class="form-control"  name="remarks"  id="remark"  placeholder="Remarks" ng-model='remarks'  ng-required="true" maxlength="500">
						    <span ng-show="addrooms.remarks.$error.pattern">Enter the Remarks!</span>
						     
						</div>
				
						
					</div>
					<div class="modal-footer">
	
						<a href="#" data-dismiss="modal" class="btn btn-danger">Close</a>
						<button ng-click="addrooms.$valid && addAccommodationRoom()" ng-disabled="addrooms.$invalid" class="btn btn-primary btngreen">Save</button>
					</div>
					</form>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dalog -->
		</div>
		<div class="modal" id="editmodal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" id="editbtnclose" class="close" data-dismiss="modal"
							aria-hidden="true">x</button>
						<h4 class="modal-title">Edit Inventory</h4>
					</div>
					 <form name="editrooms">
					<div class="modal-body" ng-repeat ="room in accommodationRoom">
					<div id="editmessage"></div>
					    <input type="hidden" value="{{room.propertyAccommodationRoomId}}" id="editPropertyAccommodationRoomId">
			            
			             <div class="form-group col-md-6">
							<label>Select Accommodation Type</label>
							<select name="editPropertyAccommodationId" id="editPropertyAccommodationId" value="" class="form-control">
	                       	<option ng-repeat="at in accommodations" value="{{at.accommodationId}}" ng-selected ="at.accommodationId == room.propertyAccommodationId">{{at.accommodationType}}</option>
	                        </select>
						  </div>
						  
			            <div class="form-group col-md-6">
							<label for="room_alias">Room Alias</label>
							<input type="text" class="form-control"  name="editRoomAlias" id="editRoomAlias" value="{{room.roomAlias}}" ng-required="true">
						     <span ng-show="editrooms.roomalias.$error.pattern">Not a Valid Room Alias!</span>
						</div>
						
						<div class="form-group col-md-6">
							<label for="sort_key">Sort key</label>
							<input type="text" class="form-control"  name="editSortKey"  id="editSortKey" value="{{room.sortKey}}"  ng-required="true">
						    <span ng-show="editrooms.editSortKey.$error.pattern">Not a Valid Sort Key!</span>
						     
						</div>
								<div class="form-group col-md-6">
							<label for="floor">Floor</label>
							<input type="text" class="form-control"  name="editFloor"  id="editFloor" value="{{room.floor}}" ng-model='room.floor' ng-pattern="/^[0-9]/" ng-required="true">  
						    <span ng-show="editrooms.editFloor.$error.pattern" style="color:red" >Not a Valid Number!</span>
						     
						</div>
											
								<div class="form-group col-md-6">
							<label for="area_square_feet">Area Square Feet</label>
							<input type="text" class="form-control"  name="editAreaSqft"  id="editAreaSqft" value="{{room.areaSqft}}" ng-model='room.areaSqft' ng-pattern="/^[0-9]/" ng-required="true">
						    <span ng-show="editrooms.editAreaSqft.$error.pattern" style="color:red">Not a Valid Fields !</span>
						     
						</div>
						<div class="form-group col-md-6">
							<label for="phone_extension">Phone Extension</label>
							<input type="text" class="form-control"  name="editPhoneExtension"  id="editPhoneExtension" value="{{room.phoneExtension}}" ng-model="room.phoneExtension" ng-pattern="/^[0-9]/"  ng-required="true">
						    <span ng-show="editrooms.editPhoneExtension.$error.pattern" style="color:red">Enter the numeric value!</span>
						      
						</div>
							<div class="form-group col-md-6">
							<label for="data_extension">data Extension</label>
							<input type="text" class="form-control"  name="dataExtension"  id="editDataExtension"  value="{{room.dataExtension}}" ng-required="true">
						    <span ng-show="editrooms.editDataExtension.$error.pattern">Enter the numeric value!</span> 
						</div>
								<div class="form-group col-md-6">
							<label for="remarks">Remarks</label>
							<input type="text" class="form-control"  name="editRemark"  id="editRemark" value="{{room.remark}}" ng-required="true">
						    <span ng-show="editrooms.editRemark.$error.pattern">Enter the Remarks!</span>
						     
						</div>
				
						
					</div>
					<div class="modal-footer">
	
						<a href="#" data-dismiss="modal" class="btn btn-danger">Close</a>
						<button ng-click="editrooms.$valid && editAccommodationRoom()" ng-disabled="editrooms.$invalid" class="btn btn-primary btngreen">Save</button>
					</div>
					</form>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dalog -->
		</div>
		
		<div class="modal" id="deletemodal">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" id="deletebtnclose" class="close" data-dismiss="modal"
							aria-hidden="true">x</button>
						<h4 class="modal-title">Delete Message</h4>
					</div>
					<div class="modal-body">
					 <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-check"></i>Your Accommodation has been deleted Sucessfully </h4>
                  
                 </div>
					    
						
					</div>
					<div class="modal-footer">
	                    
						<a href="#" data-dismiss="modal" class="btn">Close</a>
						
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dalog -->
		</div>
    

      </div><!-- /.content-wrapper -->
      


<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>

	<script>


 
	
		var app = angular.module('myApp', ['ngProgress']);
		app.controller('customersCtrl', function($scope, $http,$timeout,ngProgressFactory) {
         
			
			$scope.settings = {
		            currentPage: 0,
		            offset: 0,
		            pageLimit: 10,
		            pageLimits: ['10', '50', '100']
		          };
			
			 $scope.getTotalPages = function () {
					
					return Math.ceil($scope.accommodationRoomsCount[0].count / $scope.settings.pageLimit);
					
					};

			   $scope.isCurrentPageLimit = function (value) {
					       return $scope.settings.pageLimit == value;
					 };
			
			$scope.previousPage = function () {
				  
				  // alert('current' + $scope.settings.currentPage);
				   $scope.settings.currentPage -= 1;
				   var previousOffset = $scope.settings.currentPage * $scope.settings.pageLimit;
			      // $scope.settings.currentPage += 1;
			      // var nextOffset = $scope.settings.currentPage * $scope.settings.pageLimit;
			      // alert(previousOffset);
			       var url = "get-accommodation-rooms?limit="+$scope.settings.pageLimit+"&offset="+previousOffset;
					$http.get(url).success(function(response) {
						//console.log(response);
						$scope.accommodationRooms = response.data;
					
			
					}); 
				
			       }; 
			 
	      $scope.nextPage = function () {
				   
				   //alert('current' + $scope.settings.currentPage);
			       $scope.settings.currentPage += 1;
			       var nextOffset = $scope.settings.currentPage * $scope.settings.pageLimit;
			       //alert(nextOffset);
			       var url = "get-accommodation-rooms?limit="+$scope.settings.pageLimit+"&offset="+nextOffset;
					$http.get(url).success(function(response) {
						//console.log(response);
						$scope.accommodationRooms = response.data;
					
			
					}); 
					
					
			       
			       }; 
			       
	       $scope.all = function (total) {
					   
			    	  //alert(total);
			    	   $scope.settings.pageLimit = total;
					  //alert('current' + $scope.settings.currentPage);
				      // $scope.settings.currentPage += 1;
				       var nextOffset = 0;
				       //alert(nextOffset);
				       var url = "get-accommodation-rooms?limit="+total+"&offset="+nextOffset;
						$http.get(url).success(function(response) {
							//console.log(response);
							$scope.accommodationRooms = response.data;
						
				
						}); 
						
						
				       
				       };
				       
	        $scope.limited = function (limit) {
						   
				    	  
				    	   $scope.settings.pageLimit = limit;
						   //alert('current' + $scope.settings.currentPage);
					      // $scope.settings.currentPage += 1;
					       var nextOffset = 0;
					       //alert(nextOffset);
					       var url = "get-accommodation-rooms?limit="+limit+"&offset="+nextOffset;
							$http.get(url).success(function(response) {
								//console.log(response);
								$scope.accommodationRooms = response.data;
							
					
							}); 
							
							
					       
					       };
			
	       	$timeout(function(){
	      	    // $scope.progressbar.complete();
	            $scope.show = true;
	            $("#pre-loader").css("display","none");
	        }, 2000);

			 
			
			$scope.unread = function() {
			//var notifiurl = "http://localhost:8085/collaborative-workflow/unreadnotifications.action";
			var notifiurl = "unreadnotifications.action";
			$http.get(notifiurl).success(function(response) {
				$scope.latestnoti = response.data;
			});
			};

			
			
			$scope.getAccommodations = function() {

				var url = "get-accommodations";
				$http.get(url).success(function(response) {
				    //console.log(response);
					$scope.accommodations = response.data;
		
				});
			};
			
			$scope.getAccommodationRooms = function() {
                //var offset = 0;
				var url = "get-accommodation-rooms?limit="+$scope.settings.pageLimit+"&offset="+$scope.settings.offset;
				$http.get(url).success(function(response) {
				    //console.log(response);
					$scope.accommodationRooms = response.data;
		
				});
			};
			
			
			$scope.getAccommodationRoomsCount = function() {

				var url = "get-accommodation-rooms-count";
				$http.get(url).success(function(response) {
				    //console.log(response);
					$scope.accommodationRoomsCount = response.data;
		            //console.log($scope.accommodationRoomsCount.data[0].size);
				});
			};
			
			$scope.addAccommodationRoom = function(){
				
				//alert($('#accommodationType').val());
				
				var fdata = "propertyAccommodationId=" + $('#propertyAccommodationId').val()
				+ "&roomAlias=" + $('#roomalias').val()
				+ "&sortKey=" + $('#sortKey').val()
				+ "&floor=" + $('#floor').val()
				+ "&areaSqft=" + $('#areaSqft').val()
				+ "&phoneExtension=" + $('#phoneExtension').val()
				+ "&remark=" + $('#remark').val()
				+ "&dataExtension=" + $('#dataExtension').val();
				
				spinner.show();
				//alert(fdata);
				$http(
						{
							method : 'POST',
							data : fdata,
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							url : 'add-accommodation-room'
						}).then(function successCallback(response) {
							
							$scope.getAccommodationRooms();
							var alert = ' <div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Alert!</h4>Success Room Added Succesfully.</div>';
				            $('#message').html(alert);
				            window.location.reload(); 
							$timeout(function(){
					      	$('#btnclose').click();
					        }, 2000);
							  setTimeout(function(){ spinner.hide(); 
							  }, 1000);
							//$('#btnclose').click();
				}, function errorCallback(response) {
					  setTimeout(function(){ spinner.hide(); 
					  }, 1000);
				  
					// called asynchronously if an error occurs
					// or server returns response with an error status.
				});

			};
			var spinner = $('#loadertwo');
			$scope.editAccommodationRoom = function(){
				
				
				var fdata = "propertyAccommodationId=" + $('#editPropertyAccommodationId').val()
				+ "&roomAlias=" + $('#editRoomAlias').val()
				+ "&sortKey=" + $('#editSortKey').val()
				+ "&floor=" + $('#editFloor').val()
				+ "&areaSqft=" + $('#editAreaSqft').val()
				+ "&phoneExtension=" + $('#editPhoneExtension').val()
				+ "&remark=" + $('#editRemark').val()
				+ "&dataExtension=" + $('#editDataExtension').val()
				+ "&propertyAccommodationRoomId=" + $('#editPropertyAccommodationRoomId').val();
				spinner.show();
				//alert(fdata);
				$http(
						{
							method : 'POST',
							data : fdata,
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded'
							},
							url : 'edit-accommodation-room'
						}).then(function successCallback(response) {
							
							$scope.getAccommodationRooms();
							var alert = ' <div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><h4><i class="icon fa fa-check"></i> Alert!</h4>Success Room Updated Succesfully.</div>';
				            $('#editmessage').html(alert);
							$timeout(function(){
					      	$('#editbtnclose').click();
					        }, 2000);
							   setTimeout(function(){ spinner.hide(); 
							   }, 1000);
				}, function errorCallback(response) {
					   setTimeout(function(){ spinner.hide(); 
					   }, 1000);
					// called asynchronously if an error occurs
					// or server returns response with an error status.
				});

			};
			
            
            
			
			
            
			$scope.getAccommodationRoom = function(rowid) {
				
				
				var url = "get-accommodation-room?propertyAccommodationRoomId="+rowid;
				$http.get(url).success(function(response) {
				    console.log(response);
					$scope.accommodationRoom = response.data;
		
				});
				
			};
			
			/*  $confirm({text: 'Are you sure you want to delete?', title: 'Delete it', ok: 'Yes', cancel: 'No'})
                .then(function(rowid) { });*/
			
            $scope.deleteAccommodationRoom = function(rowid) {
                	var check = confirm("Are you sure you want to delete this Inventory?");
        			if( check == true){
                
                	 // var msgbox = $dialog.messageBox('Delete Item', 'Are you sure?', [{label:'Yes, I\'m sure', result: 'yes'},{label:'Nope', result: 'no'}]);
                	// alert(msgbox);
                	 // msgbox.open().then(function(result){
                      //    if(result === 'yes') {
            	  var url = "delete-accommodation-room?propertyAccommodationRoomId="+rowid;
				$http.get(url).success(function(response) {
				    //console.log(response);
				    $scope.getAccommodationRooms();
				    $('#deletemodal').modal('toggle');
				    $timeout(function(){
				      	$('#deletebtnclose').click();
				    }, 2000);				
		
				}); 
                      }       
			};
			
			 $scope.getPropertyList = function() {
				 
		        	
		        	
			        var userId = $('#adminId').val();
		 			var url = "get-user-properties?userId="+userId;
		 			$http.get(url).success(function(response) {
		 			    
		 				$scope.props = response.data;
		 	
		 			});
		 		};
		 		
           $scope.change = function() {
     	   
		        //alert($scope.id);
		        
		        var propertyId = $scope.id;	
     	        var url = "change-user-property?propertyId="+propertyId;
	 			$http.get(url).success(function(response) {
	 				
	 				 window.location = '/ulopms/dashboard'; 
	 				//$scope.change = response.data;
	 	
	 			});
			       
		 		};
		 		
				
		    $scope.getPropertyList();
		    
			$scope.getAccommodations();
			$scope.getAccommodationRooms();
			$scope.getAccommodationRoomsCount();
			//$scope.unread();
			//
			
			
		});		
	</script>

       