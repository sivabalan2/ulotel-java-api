<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="s" uri="/struts-tags"%>

   <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <section class="content-header">
          <h1>
            Commission Invoice Details
            <!--<small>Subscription Expiry on {{property[0].subscription[0].expirydate}}</small>-->
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Account</a></li>
            <li class="active">Commission Invoice</li>
          </ol>
        </section>
 		
 		<!-- Main content -->
    <section class="content">
    <div class="box box-default color-palette-box">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fa fa-tag"></i> Search &amp; Filter</h3>
        </div>
        <div class="box-body">
        <form>
          <div class="row">
	          <div class="col-sm-2 col-md-3">
	           	  <div class="form-group">
	                <label>From Date:</label>
	
	                <div class="input-group date">
	                <label class="input-group-addon btn" for="fromDate">
						<span class="fa fa-calendar"></span>
					</label>
	                  <input type="text" class="form-control pull-right" id="fromDate" name="fromDate" value="" onkeypress="return false;" ng-required="true" autocomplete="off">
	                </div>
	              </div>
              
              </div>
              <div class="col-sm-2 col-md-3">
		           <div class="form-group">
		                <label>To Date:</label>
		
		                <div class="input-group date">
		                  <label class="input-group-addon btn" for="toDate">
							<span class="fa fa-calendar"></span>
						  </label>
		                  <input type="text" class="form-control pull-right" id="toDate" name="toDate" value="" onkeypress="return false;" ng-required="true" autocomplete="off">
		                </div>
		                <!-- /.input group -->
		             </div>
              </div>
              
              <div class="col-sm-2 col-md-4">
              		<div class="form-group">
              			<label>Tax Split-up:</label>
              		</div>
              		<div class="form-group">
              			<div class="col-md-4">
							<input type="radio" name="filterTax" id="filterTax" value="Taxable" placeholder="Taxable" checked>
							<label>Tax</label>
						</div>
						<div class="col-md-4">
							<input type="radio" name="filterTax" id="filterNonTax" value="NonTaxable" placeholder="Non Taxable">
							<label>Non Tax</label>
						</div>
              		</div>
			  </div>
              
              
          </div>
<!--           <div ng-if="propertyRevenue.length>0"> -->
           <div class="row">
           		<div class="col-sm-2 col-md-3">
		           <div class="form-group">
		                <label>Invoice Date:</label>
		
		                <div class="input-group date">
		                  <label class="input-group-addon btn" for="invoiceDate">
							<span class="fa fa-calendar"></span>
						  </label>
		                  <input type="text" class="form-control pull-right" id="invoiceDate" name="invoiceDate" value="" onkeypress="return false;" ng-required="true" autocomplete="off">
		                </div>
		                <!-- /.input group -->
		             </div>
              </div>
               <div class="col-sm-2 col-md-4">
              		<div class="form-group">
              			<label>Customize Date:</label>
              		</div>
              		<div class="form-group">
              			<div class="col-md-4">
							<input type="radio" name="filterInvoiceDate" id="invoiceEnableDate" value="EnableDate" placeholder="Enable Date" checked>
							<label>Enable</label>
						</div>
						<div class="col-md-4">
							<input type="radio" name="filterInvoiceDate" id="invoiceDisableDate" value="DisableDate" placeholder="Disable Date">
							<label>Disable</label>
						</div>
              		</div>
			  </div>
			  
			  <div class="col-sm-2 col-md-2">
	              <div class="form-group">
	<!--               <label>Search</label> -->
					<label>&nbsp;</label>
	              	<button type="submit" ng-click="getPropertyCommission()" class="btn btn-danger pull-right btn-block btn-sm">Search</button>
	             </div>
              </div>
              
           </div>
<!--            </div> -->
          <!-- /.row -->
          </form>
        </div>
        <!-- /.box-body -->
      </div>
      
      
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Results</h3>

              
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th>Period</th>
                  <th>Ulo Service Commission</th>
                  <th>CGST Tax(9%)</th>
                  <th>SGST Tax(9%)</th>
                  <th>Total Commission Amount</th>
                </tr>
                <tr ng-repeat="revenue in propertyRevenue">
                  <td>{{revenue.durationPeriod}}</td>
                  <td>{{revenue.uloTotalCommission}}</td>
                  <td>{{revenue.centralGstTax}}</td>
                  <td>{{revenue.stateGstTax}}</td>
                  <td>{{revenue.totalUloCommission}}</td>
                </tr>
               
                <tr ng-if="propertyRevenue.length==0">
                	<td  colspan="8">No Records found
                	</td>
                </tr>
                
             </table>
                <div>
              <label>&nbsp;</label>
              <label>&nbsp;</label>
              </div>	
             
              <div ng-if="propertyRevenue.length>0">
              	<button type="submit" ng-click="getPropertyCommissionVoucher()" id="loading-example-btn"  style="margin:5px;" class="btn btn-primary pull-right">Generate Voucher</button>
               	
               	<button type="submit" ng-click="getPropertyCommissionVoucher()" id="loading-example-btn"  style="margin:5px;" class="btn btn-primary pull-right">Send Voucher</button>
               	<label>&nbsp;&nbsp;&nbsp;</label>
              </div>
            </div>
              
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
      
    </section>
 		
 		
</div>
    <input type="hidden" value ="<%=session.getAttribute("propertyId") %>" id="propertyId" name="propertyId" >
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>
<!-- csv download script start -->
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.15/angular-sanitize.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ng-csv/0.3.6/ng-csv.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.0rc1/angular-route.min.js"></script>
<!-- csv download script end -->
	 <script src="js/jquery-2.1.1.js" type="text/javascript"></script>

	<script>
	var app = angular.module('myApp', ['ngProgress','ngSanitize', 'ngCsv']);
	app.controller('customersCtrl', function($scope, $http,$timeout,ngProgressFactory) {
		
		  $("#fromDate").datepicker({
		      dateFormat: 'MM d, yy',
		    
		      onSelect: function (formattedDate) {
		          var date1 = $('#fromDate').datepicker('getDate'); 
		          var date = new Date( Date.parse( date1 ) ); 
		          date.setDate( date.getDate() + 1 );        
		          var newDate = date.toDateString(); 
		          newDate = new Date( Date.parse( newDate ) );   
		          $('#toDate').datepicker("option","minDate",newDate);
		          $timeout(function(){
		        	  document.getElementById("fromDate").style.borderColor = "LightGray";
		          });
		      }
		  });

		  $("#toDate").datepicker({
		      dateFormat: 'MM d, yy',
		   
		      onSelect: function (formattedDate) {
		          var date2 = $('#toDate').datepicker('getDate'); 
		          $timeout(function(){
		        	  document.getElementById("toDate").style.borderColor = "LightGray";
		          });
		      }
		  }); 
		  
		  $("#invoiceDate").datepicker({
		      dateFormat: 'MM d, yy',
		   
		      onSelect: function (formattedDate) {
		          var date2 = $('#invoiceDate').datepicker('getDate'); 
		          $timeout(function(){
		        	  document.getElementById("invoiceDate").style.borderColor = "LightGray";
		          });
		      }
		  }); 
		  
		  
		$scope.getPropertyCommissionVoucher=function(){
			var invoiceDateFlag=false;
			if(document.getElementById('invoiceEnableDate').checked){
				invoiceDateFlag=true;
			}
			if(document.getElementById('invoiceDisableDate').checked){
				invoiceDateFlag=false;
			}
			
			var sendVoucher=confirm("Do you want to send voucher?");
			if(sendVoucher){
				$("#loading-example-btn").prop('disabled', true); // disable button
				var text = '{"listRevenueDetails":' + JSON.stringify($scope.propertyRevenue) + '}';
				console.log(text);
				var mailFlag=true;
			    var data1 = JSON.parse(text);
				var data = angular.toJson(data1);
				
				console.log(data);
				
				$http(
					{
						method : 'POST',
						data : data,
						dataType: 'json',
						headers : {
							 'Content-Type' : 'application/json; charset=utf-8'
							
						},
						url : 'property-commission-voucher?mailFlag='+mailFlag+'&invoiceDateFlag='+invoiceDateFlag
						
					}).then(function successCallback(response){
						  alert("Email Send Successfully");
						  window.location.reload(); 
						  $("#loading-example-btn").prop('disabled', false); 
					})  
			}
			
		};
		 $scope.getPropertyCommission = function(){
			 	var filterTaxName="";
				if(document.getElementById('filterTax').checked){
					filterTaxName='Taxable';
				}
				if(document.getElementById('filterNonTax').checked){
					filterTaxName='NonTaxable';
				}
				
				
				var filterInvoiceDate=$('#invoiceDate').val();
				
				var fdata = "strFromDate=" + $('#fromDate').val()+ "&strToDate=" + $('#toDate').val();
				if(document.getElementById('fromDate').value==""){
		 			 document.getElementById('fromDate').focus();
		 			 document.getElementById("fromDate").style.borderColor = "red";
		 		 }else if(document.getElementById('toDate').value==""){
		 			 document.getElementById('toDate').focus();
		 			document.getElementById("toDate").style.borderColor = "red";
		 		 }else if(document.getElementById('fromDate').value!="" && document.getElementById('toDate').value!="" && document.getElementById('invoiceDate').value!=""){
	    			    		
					$http(
							{
								method : 'POST',
								data : fdata,
								headers : {
									'Content-Type' : 'application/x-www-form-urlencoded'
								},
								url : 'get-property-revenue?filterTaxName='+filterTaxName+'&filterInvoiceDate='+filterInvoiceDate
							}).success(function(response) {
								
								$scope.propertyRevenue = response.data;
								
							});
		 		 }
			};
		
	});
		</script>
		
		<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
		  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
		  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		  <script src="plugins/datepicker/bootstrap-datepicker.js"></script>
		  
		  <script>
 
//Date picker

  

  </script>
