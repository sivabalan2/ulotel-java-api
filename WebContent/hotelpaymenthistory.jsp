<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!-- <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/themes/south-street/jquery-ui.min.css" />
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <small>Payment History</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Payment History</li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="nav-tabs-custom">
          <ul class="nav nav-tabs">
            <li class="active"><a href="#fa-iconstwo" data-toggle="tab">Payment History</a></li>
          </ul>
          <div class="tab-content">
            <!-- Font Awesome Icons -->
            
            <!-- /.tab-content -->
            <div class="tab-pane active " id="fa-iconstwo">
              <section class="content">
                <div class="row">
                  <h4>Payment History</h4>
                  	<div class="table-responsive no-padding"  > 
					 <table class="table table-bordered" id="example1">
					 <tr>
					 <th>S.No</th>
					 <th>Payment date</th>
					 <th>Amount</th>
					 <th>Booking Id</th>
					 <th>Download Receipt</th>
					 </tr>
					 <tr ng-repeat="ph in paymentHistory track by $index">
					 <td>{{ph.serialNo}}</td>
					 <td>{{ph.paymentDate}}</td>
					 <td>{{ph.paidAmount}}</td>
					 <td>{{ph.paymentBookingId}}</td>
					 <td><a ng-click="downloadURI(ph.uploadPath,ph.uploadReceipt,$index)">{{ph.uploadReceipt}}</a></td>
				     </tr>
					 </table>
					 <div class="box-footer clearfix">
								<ul class="pagination pagination-sm"></ul>
								<div class="simple-pagination pull-right" >   
									<p class="simple-pagination__items">Showing {{paymentHistory.length}} out of {{settings.pageLimit}}</p>
									<p><button ng-click="previousPage()" ng-disabled="settings.currentPage <= 0" class="simple-pagination__button simple-pagination__button--prev"> &#10094;          
									</button><span class="simple-pagination__pages">{{settings.currentPage + 1}} of {{getTotalPages()}}</span>
									<button ng-click="nextPage()" ng-disabled="settings.currentPage === (getTotalPages() - 1)" class="simple-pagination__button simple-pagination__button--next">&#10095;</button></p>
									<a href="" ng-click="limited(10)" ng-class="active">{{10}}</a> |
									<span><a href="" ng-click="all(totalPaymentHistory.length)" ng-class="active">All</a></span> 
								</div>
						   </div>
					</div>
                </div>
              <!-- edit coupon form -->
  <!-- Modal -->
 <!-- Edit coupon Form Ends -->
              </section>
              <!-- /.content -->
            </div>
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
  </section>

  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>
<script src="js/jquery-2.1.1.js" type="text/javascript"></script>
<script>
    
	 
	
	var app = angular.module('myApp', ['ngProgress']);
	app.controller('customersCtrl', function($scope, $http,$timeout,ngProgressFactory) {
		
		
		$scope.settings = {
	            currentPage: 0,
	            offset: 0,
	            pageLimit: 5,
	            pageLimits: ['10', '50', '100']
	          };

		 $scope.getTotalPages = function () {
			return Math.ceil($scope.totalPaymentHistory.length / $scope.settings.pageLimit);
		};
			
		 $scope.isCurrentPageLimit = function (value) {
		       return $scope.settings.pageLimit == value;
		 };
		 
		 $scope.previousPage = function () {
			   $scope.settings.currentPage -= 1;
			   var previousOffset = $scope.settings.currentPage * $scope.settings.pageLimit;
		       var url = "get-payment-history?limit="+$scope.settings.pageLimit+"&offset="+previousOffset;
				$http.get(url).success(function(response) {
					//console.log(response);
					$scope.paymentHistory = response.data;
				}); 
		    }; 
		 
     		$scope.nextPage = function () {
		       $scope.settings.currentPage += 1;
		       var nextOffset = $scope.settings.currentPage * $scope.settings.pageLimit;
		       var url = "get-payment-history?limit="+$scope.settings.pageLimit+"&offset="+nextOffset;
				$http.get(url).success(function(response) {
					//console.log(response);
					$scope.paymentHistory = response.data;
				}); 
		     }; 
		       
      			$scope.all = function (total) {
		    	   $scope.settings.pageLimit = total;
			       var nextOffset = 0;
			       var url = "get-payment-history?limit="+total+"&offset="+nextOffset;
					$http.get(url).success(function(response) {
						//console.log(response);
						$scope.paymentHistory = response.data;
					}); 
			    };
			       
      			 $scope.limited = function (limit) {
			    	   $scope.settings.pageLimit = limit;
				       var nextOffset = 0;
				       var url = "get-payment-history?limit="+limit+"&offset="+nextOffset;
						$http.get(url).success(function(response) {
							//console.log(response);
							$scope.paymentHistory = response.data;
					}); 
		       };
		       
		 
		$scope.getPaymentHistory = function() {

			var url = "get-payment-history?limit="+$scope.settings.pageLimit+"&offset="+$scope.settings.offset;
			$http.get(url).success(function(response) {
				$scope.paymentHistory = response.data;
			});
		};
		
		$scope.getTotalPaymentHistory = function() {

			var url = "get-payment-history";
			$http.get(url).success(function(response) {
				$scope.totalPaymentHistory = response.data;
			});
		};
		
		$scope.downloadURI = function(dataurl,filename,ind){
			
			var a = document.createElement("a");
			  a.href = dataurl;
			  a.setAttribute("download", filename);
			  a.click();
			  return false;
		}
     
		$scope.getPaymentHistory();
		
		$scope.getTotalPaymentHistory();
	});

	</script>
  <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="plugins/iCheck/icheck.min.js"></script>
<script>
  /* next and previous button */
 $('.nextBtn').click(function(){
  $('.nav-tabs > .active').next('li').find('a').trigger('click');
});

  $('.previousBtn').click(function(){
  $('.nav-tabs > .active').prev('li').find('a').trigger('click');
});
  
  function downloadURI1(dataurl, filename,indx){
	  alert("ener");
	  var a = document.createElement("a");
	  a.href = dataurl;
	  a.setAttribute("download", filename);
	  //a.click();
	  return false;
  
	  
  }
  </script>