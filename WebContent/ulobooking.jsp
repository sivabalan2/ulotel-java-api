<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
   pageEncoding="ISO-8859-1"%>

<div class="container">
   <div class="row">
      <div class="col-md-12 hidden-sm hidden-xs">
         <div id="discountOffers" ng-repeat="cd in discounts| orderBy:'-discountPercentage' | limitTo:1 ">
            <img src="ulowebsite/images/offer_percentage_sm.png" alt="Ulo hotels offer" class="hidden-xs" />    
            <span class="medium"> {{cd.discountPercentage | number}} % OFF</span> <br class="hidden-sm hidden-md hidden-lg" /> <span class="code">{{cd.discountName}} - Use this code</span> <br class="hidden-sm hidden-md hidden-lg" /> To Get  {{cd.discountPercentage | number}}  % OFF        
            <span class="t_c_apply">T & C apply</span>
         </div>
      </div>
         <div class="hidden-lg hidden-md col-sm-12 col-xs-12 mbcode ">
         <div id="discountOffersx" class="mbcodes" ng-repeat="cd in discounts| orderBy:'-discountPercentage' | limitTo:1 ">
                    <div class="col-sm-5 col-xs-5"><span class="medium"> {{cd.discountPercentage | number}} % OFF</span></div>
                    <div class="col-sm-2 col-xs-2"><span class="code">{{cd.discountName}}</span> </div>
                    <div class="col-sm-5 col-xs-5"><span class="medium">Use this code</span></div>
         </div>
      </div>
   </div>
</div>
<section id="payment" class="paymentfinal">

<%--   <input type="hidden" value ="<%= ((session.getAttribute("userName")==null)?"":session.getAttribute("userName")) %>" id="sc_name" name="sc_name" > --%>
<%--     <input type="hidden" value ="<%= ((session.getAttribute("emailId")==null)?"":session.getAttribute("emailId")) %>" id="sc_email" name="sc_email" > --%>
	<input type="hidden" value ="<%= ((session.getAttribute("uloUserName")==null)?"":session.getAttribute("uloUserName")) %>" id="sc_name" name="sc_name" >
    <input type="hidden" value ="<%= ((session.getAttribute("uloEmailId")==null)?"":session.getAttribute("uloEmailId")) %>" id="sc_email" name="sc_email" >
   <input type="hidden" value ="<%=session.getAttribute("uloPropertyName") %>" id="propertyName" name="propertyName" >
   <input type="hidden" value ="<%=request.getAttribute("urlPropertyId")==null?session.getAttribute("uloPropertyId"):request.getAttribute("urlPropertyId") %>" id="propertyId" name="propertyId" >
   <input type="hidden" value ="<%=session.getAttribute("uloSignup") %>" id="uloSignup" name="uloSignup" >
   <textarea name="details" style="display:none" id ="details"><%= session.getAttribute("details") %></textarea>
   <form name="paymentForm" id="paymentForm" >
      <div class="container">
         <div class="row">
            <div class="col-sm-12 col-md-12">
               <div class="col-lg-8 col-md-8  col-sm-12  col-xs-12 paymentborder">
                  <div class="col-lg-6 col-md-6  col-sm-12  col-xs-12">
                     <div class="payhoteldetail">
                     <a ><img src="get-property-thumb?propertyId=<%=session.getAttribute("uloPropertyId") %>" alt="ulohotels deluxe room" height="221px" width="100%" class="img-responsive img-thumbnail"> </a>                     
                        <div class="caption">
                           <h4 class="roomTitle"><%=session.getAttribute("uloPropertyName") %>&nbsp;<span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star unchecked"></span> <span class="fa fa-star unchecked"></span></h4>
                           <span class="hoteladdress"><%=session.getAttribute("city") %></span>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-6 col-md-6  col-sm-12  col-xs-12 dateinfo">
                     <div class="col-lg-6 col-md-6  col-sm-6  col-xs-6">
                        <label> CHECK IN</label>
                        <h3>&nbsp;  {{bookingDetails[0].arrival | date:'dd-MMM-yyyy' }}</h3>
                     </div>
                     <div class="col-lg-6 col-md-6  col-sm-6  col-xs-6">
                        <label> CHECK OUT</label>
                        <h3>&nbsp;  {{bookingDetails[0].departure | date:'dd-MMM-yyyy'}}</h3>
                     </div>
                     <br>
                     .    
                     <hr>
                     </hr>
                     <div class="col-lg-6 col-md-6  col-sm-6  col-xs-6">
                        <label>ROOMS</label>
                        <h3 class="roominfo">{{getRoomCount()}}<span class="nightinfo">( {{bookingDetails[0].diffDays}} Nights )</span></h3>
                     </div>
                     <div class="col-lg-6 col-md-6  col-sm-6  col-xs-6">
                        <label>GUESTS</label>
                        <h3 class="roominfo">{{getPersonCount()}}<span class="nightinfo">( {{getAdultCount()}} Adults {{getChildCount()}} Child )</span></h3>
                     </div>
                     <br>
                     .    
                     <hr>
                     </hr>
                     <div class="col-lg-12 col-md-12  col-sm-12  col-xs-12 ">
                        <label>INCLUSION</label>
                        <span class="extrainfo"><ul ><li><i class="fa fa-check"></i>Accommodation</li> <li><i class="fa fa-check"></i>Breakfast</li> <li><i class="fa fa-check"></i>Room Service</li></ul></span>
                     </div>
                  </div>
                  <hr>
                  </hr>
                  <div class="col-lg-12 col-md-12  col-sm-12  col-xs-12 guestinfo">
                     <h3>GUEST DETAILS</h3>
                     <div class="guestForm">
                        <input type="hidden" class="form-control" name="userId" id="userId" value="<s:property value="#session.uloUserId"/>" placeholder="usrid">
                        <input type="text" name="firstName" id="firstName" value="{{user[0].o_username}}" placeholder="Name:" ng-model="username" ng-pattern="/^[a-zA-Z\s]*$/" ng-required="true" maxlength="50" />
                        <span ng-show="paymentForm.firstName.$error.pattern" style="color:red">Enter your name </span>
                        <input type="text" name="guestEmail" id="guestEmail" ng-model="useremail" value="{{user[0].o_email}}" placeholder="Email: " ng-required="true" ng-pattern="/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i" />
                        <span ng-show="paymentForm.guestEmail.$error.pattern" style="color:red">Enter the email id!</span>
                        <input type="text" name="mobilePhone" id="mobilePhone"   value="{{user[0].o_phone }}" placeholder="Mobile: " ng-model="userphone" ng-pattern="/^[0-9]*$/"  ng-required="true" maxlength="10" ng-minlength="10" />
                        <span ng-show="paymentForm.mobilePhone.$error.pattern" style="color:red">Enter the valid mobile number!</span> 
                        <textarea class="form-control" name="specialRequest" id="specialRequest" placeholder="Special Request: (Maximum 250 characters)" maxlength="250"></textarea>
                        <span class="splneed">NOTE : Special Request Cannot Be Guaranteed But The Property Will Try To Meet Your Needs  </span>
                     </div>
                  </div>
               </div>
               <div class="col-lg-4 col-md-4  col-sm-12  col-xs-12 payinfo">
                  <h3>PAYMENT DETAILS</h3>
                  <div class="paydetail" >
                     <div class="col-lg-12 col-md-12  col-sm-12  col-xs-12">
                        <div class="col-lg-12 col-md-12  col-sm-12  col-xs-12" ng-repeat ="bd in bookingDetails  track by $index">
                           <div style="display:none">{{bd.tax}} {{bd.total}}</div>
                           <div class="col-lg-6 col-md-6  col-sm-6  col-xs-6">
                              <label>{{bd.accommodationType}}  </label>
                           </div>
                           <div class="col-lg-6 col-md-6  col-sm-6  col-xs-6">
                              <label class="rateinfo"><i class="fa fa-inr"></i><span class="strike" ng-hide="bd.baseOriginAmount == 0"> {{bd.baseOriginAmount * bd.rooms}}</span>{{bd.total | currency:"":0 }}</label>
                           </div>
                          <div class="col-lg-12 col-md-12  col-sm-12  col-xs-12">
                           <div class="col-lg-6 col-md-6  col-sm-6  col-xs-6">
                              <label>Tax </label>
                           </div>
                           <div class="col-lg-6 col-md-6  col-sm-6  col-xs-6">
                              <label class="rateinfo"><i class="fa fa-inr"></i> {{bd.tax | currency:"":0 }}</label>
                           </div>
                           </div>
                        </div>
                    
                        <div class="row subtotal">
                           <div class="col-lg-6 col-md-6  col-sm-6  col-xs-6">
                              <label>Grand Total </label>
                           </div>
                           <div class="col-lg-6 col-md-6  col-sm-6  col-xs-6">
                              <label class="rateinfo"><i class="fa fa-inr"></i> {{  getsolidTotal() | currency:"":0  }}</label>
                           </div>
                        </div>
                        <div class="row subcoupon">
                           <div class="col-sm-12 col-xs-12 "><a href="javascript:void(0)" data-toggle="modal" data-target="#couponsModal">View Available Coupons</a></div>
                        </div>
                        <div class="row">
                           <div class="col-lg-8 col-md-8  col-sm-8  col-xs-8">
                              <label>Coupon Applied <span class="couponname" ng-show="CouponName">{{CouponName}}</span><span class="couponpercent" ng-show="CouponPercent">{{CouponPercent | number}} % Discount From Booking Amount</span> </label>
                           </div>
                           <div class="col-lg-4 col-md-4  col-sm-4  col-xs-4">
                              <label class="rateinfo"><i class="fa fa-inr"></i> {{ priceDropped | currency:"":0  }} </label>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-lg-6 col-md-6  col-sm-6  col-xs-6">
                              <label>Total Amount </label>
                           </div>
                           <div class="col-lg-6 col-md-6  col-sm-6  col-xs-6">
                              <label class="rateinfo"><i class="fa fa-inr"></i> {{ getFinalTotal() | currency:"":0  }}</label>
                           </div>
                        </div>
                        <div class="row" ng-hide="getStrikeAmount() <= getsolidTotal() ">
                           <div class="col-lg-6 col-md-6  col-sm-6  col-xs-6 totalsave" >
                              <label>Total  Savings</label>
                           </div>
                           <div class="col-lg-6 col-md-6  col-sm-6  col-xs-6 totalsave">
                              <label class="rateinfo"><i class="fa fa-inr"></i> {{ (getStrikeAmount() - getsolidTotal()) + priceDropped | currency:"":0  }}</label>
                           </div>
                        </div>

                       <div class="row rsizes">
                           <div class="col-lg-6 col-md-6  col-sm-6  col-xs-6 totalpay">
                              <label>Payable Amount:<br><span class="smallTxt">Inc All Taxes</span></label>
                              
                           </div>
                           <div class="col-lg-6 col-md-6  col-sm-6  col-xs-6 totalpay" >
                              <label class="rateinfodetail"><i class="fa fa-inr"></i>  {{ getTotal() | currency:"":0 }}</label>
                           </div>
                        </div>
                                     <div class ="row">
                         <div class="col-lg-12 col-md-12  col-sm-12  col-xs-12">
                        <button  type="submit" ng-click="paymentForm.$valid && booking()" class="nowPay">PAY NOW</button>
                         </div>
                        </div>
                        <div class ="row">
                         <div class="col-lg-12 col-md-12  col-sm-12  col-xs-12 " ng-repeat="pas in payathotelstatus">
                            <div  ng-if="pas.payAtHotelStatus == 'true'"> <!-- || pas.payAtHotelStatus == '' -->
                              <button  type="submit"  ng-click="paymentForm.$valid && sendPayAtHotelOtp()" class="laterPay">PAY AT HOTEL</button>
                              </div>
                         </div>
                        </div>
                
                 <!--              <div class="row rsize">
                              <div class="col-lg-2 col-md-2 hidden-sm hidden-xs"></div>
                           <div class="col-lg-5 col-md-5  col-sm-6  col-xs-6 pdetailsr" ng-repeat="pas in payathotelstatus">
                              <div  ng-if="pas.payAtHotelStatus == 'true'"> || pas.payAtHotelStatus == ''
                              <button  type="submit"  ng-click="paymentForm.$valid && sendPayAtHotelOtp()" class="laterPay">PAY AT HOTEL</button>
                              </div>
                           </div>
                           <div class="col-lg-5 col-md-5  col-sm-6  col-xs-6 pdetailsl">
                              <button  type="submit" ng-click="paymentForm.$valid && booking()" class="nowPay">PAY NOW</button>
                           </div>
                        </div> -->
                        
                        <div class="row"><span class="uloterms">by proceeding you agree to the <a href="guest-policy" target="_blank">guest policy</a> and <a href="termsandconditions" target="_blank" >terms and conditions </a>of ulohotels.com</span></div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </form>
</section>
<!-- Coupons Popup Modal Box -->
<div class="modal fade" id="couponsModal" aria-hidden="true" data-keyboard="false" data-backdrop="static">
   <div class="modal-dialog">
      <div class="modal-content">
         <button type="button" class="close" id="btnclose" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         <form method="post" id="enquire-form" class="form-horizontal">
            <input type="hidden" name="action" value="batchSubmit" />
            <div class="modal-header">
               <h3 class="modal-title">Apply Coupon Code </h3>
               <div id=codeerror></div>
            </div>
            <div class="form-group">
               <div class="col-sm-10 couponCodeInput">
                  <input type="hidden" class="form-control" id="code_signup" placeholder=""  name="code_signup" value="{{discounts[0].isSignup}}" required />
                  <input type="hidden" class="form-control" id="code_percentage" placeholder=""  name="code_percentage" value="{{discounts[0].discountPercentage}}" required />
                  <input type="hidden" class="form-control" id="code_id" placeholder=""  name="code_id" value="{{discounts[0].discountId}}" required />
                  <input type="text" class="form-control" id="code_input" placeholder=""  name="code_input" value="{{discounts[0].discountName}}" required/>
                  <button type="button" name="apply_code" id="apply_code" ng-click="applyDiscount()"><span ng-show="searchButtonText == 'APPLYING'"><i class="fa fa-spinner fa-spin"></i></span>
                  {{ searchButtonText }}
               </div>
            </div>
            <div class="separator bgcolor"></div>
             
            <div class="form-group couponCode" ng-repeat="cd in discounts track by $index">
               <div class="col-sm-2 col-xs-4">
                  <label>
                     <input type="radio"  name="radio" ng-checked="$index == 0" ng-click="selectDiscount($index)">
                     <!--  <input type="radio" ng-if ="" checked="checked" name="radio"> -->
                     <span class="selectCode"></span>
                  </label>
               </div>
               <div class="col-sm-8 col-xs-8" >
                  <i class="fa fa-inr"></i> {{cd.discountPercentage}} % Flat Discount<br>
                  Use Code:  <strong>{{cd.discountName}}</strong>
               </div>
            </div>
            <div class="separator bgcolor"></div>
         </form>
      </div>
   </div>
</div>
<div class="modal fade" id="smsModal" aria-hidden="true" data-keyboard="false" data-backdrop="static">
   <div class="modal-dialog">
      <div class="modal-content">
         <button type="button" class="close" id="btnclose" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
         <form method="post" id="enquire-form" class="form-horizontal" ng-repeat="pao in payathotelotp" name="smsForm">
            <input type="hidden" name="action" value="batchSubmit" />
            <div class="modal-header">
               <h3 class="modal-title">Verify your Mobile Number</h3>
               <p>A One Time Password (OTP) has been sent via SMS to <span>{{pao.phone}}</span></p>
               <div id="smserror"></div>
            </div>
            
            <div class="form-group" ng-repeat="pao in payathotelotp">
            <input type="hidden" name="smsGuestId" id="smsGuestId" value="{{pao.guestId}}" />
               <div class="col-sm-10 couponCodeInput">
                            <input type="text" class="form-control" id="smsOtp" placeholder="Enter Your OTP" ng-model="smsOtp"  ng-pattern="/^[0-9]/" n name="smsOtp" value="" ng-required="true">
                              
                            <button type="btn" id="loading-example-btn" ng-disabled="smsForm.$invalid" name="apply_code" ng-click="verifySms()" id="apply_code"><span ng-show="searchButtonSmsText == 'APPLYING'"><i class="fa fa-spinner fa-spin"></i></span>
                  {{ searchButtonSmsText }}</button>
                           <span ng-show="smsForm.smsOtp.$error.pattern" style="color:red;font-size:12px;">Please Enter Valid OTP</span>
                        </div>
            </div>
             <div class="resendOtp"><p>Haven't received the code yet? <button class="textlink" ng-click="reSendPayAtHotelOtp()">Resend OTP</button></p></div>
           
         </form>
      </div>
   </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
<script data-require="ui-bootstrap@*" data-semver="0.12.1" src="https://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.1.min.js"></script>
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script>
var app = angular.module('myApp', ['ngProgress']);
app.controller('customersCtrl', function($scope, $http, $timeout, ngProgressFactory, $location) {
    $scope.handleGoogleApiLibrary = function() {
        gapi.load('client:auth2', {
            callback: function() {
                gapi.client.init({
                    apiKey: 'AIzaSyAuJwG_DuX3eEXTgsoEY9A8LRvRVfeilFs',
                    clientId: '106759720549-tfe9i1b16ovvpqn8aho6hooiaftt2h4n.apps.googleusercontent.com',
                    scope: 'https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/plus.me'
                }).then(
                    // On success
                    function(success) {
                        $("#login-button").removeAttr('disabled');
                    },
                    // On error
                    function(error) {
                        // alert('Error : Failed to Load Library');
                    }
                );
            },
            onerror: function() {
                // Failed to load libraries
            }
        });
    };
    $scope.googleLogin = function() {
        gapi.auth2.getAuthInstance().signIn().then(
            function(success) {
                gapi.client.request({
                    path: 'https://www.googleapis.com/plus/v1/people/me'
                }).then(
                    function(success) {
                        var user_info = JSON.parse(success.body);
                        var username = user_info.displayName;
                        var email = user_info.emails[0].value;
                        var phone = null;
                        $('#loginModal').modal('toggle');
                        $('#logSec').html("<div class='mnuContant'><a href='mailto:support@ulohotels.com' title='support@ulohotels.com'><i class='fa fa-envelope'></i><span class='visibleLG'>support@ulohotels.com</span></a><a href='tel:9543592593' title=''+91 95435 92593'><i class='fa fa-phone'></i><span class='visibleLG'>+91 95435 92593</span></a></div><div class='mnuProfile'><div class='mpAvatar hidden-xs' style='background:url(ulowebsite/images/avatar.png);'></div><div class='mpUsername'><a href='javascript:showpro();' class=' mnuProfileOpen'>Hi " + username + " <i class='fa fa-angle-down'></i></a></div><div class='mpSubMenu' id='showmenu' ><ul><li><a href='ulouserprofile'>Profile</a></li><li><a href='ulouserprofile'>Booking History</a></li><li><a href='ulologout'>Log out</a></li></ul></div></div>");
                        $('#logSecs').html("<div class='mnuContant'><a href='mailto:support@ulohotels.com' title='support@ulohotels.com'><i class='fa fa-envelope'></i><span class='visibleLG'>support@ulohotels.com</span></a><a href='tel:9543592593' title=''+91 95435 92593'><i class='fa fa-phone'></i><span class='visibleLG'>+91 95435 92593</span></a></div><div class='mnuProfile'><div class='mpAvatar hidden-xs' style='background:url(ulowebsite/images/avatar.png);'></div><div class='mpUsername'><a href='javascript:showpro();' class=' mnuProfileOpen'>Hi " + username + " <i class='fa fa-angle-down'></i></a></div><div class='mpSubMenu' id='showmenu' ><ul><li><a href='ulouserprofile'>Profile</a></li><li><a href='ulouserprofile'>Booking History</a></li><li><a href='ulologout'>Log out</a></li></ul></div></div>");
                        location.reload();
                        var fdata = "username=" + user_info.displayName +
                            "&emailid=" + user_info.emails[0].value +
                            "&userid=" + user_info.id;
                        $http({
                            method: 'POST',
                            data: fdata,
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded'
                            },
                            url: 'sociallogin'
                        }).then(function successCallback(response) {}, function errorCallback(response) {

                        });
                    },
                    // On error
                    function(error) {
                        $("#login-button").removeAttr('disabled');
                        alert('Error : Failed to get user information');
                    }
                );
            },
            // On error
            function(error) {
                $("#login-button").removeAttr('disabled');
                alert('Error : Login Failed');
            }
        );
    };
    $scope.facebookLogin = function() {
        $scope.authUser();
    };
    $scope.authUser = function() {
        FB.login($scope.checkLoginStatus, {
            scope: 'email, user_likes, user_birthday, user_photos'
        });
    };
    $scope.checkLoginStatus = function(response) {
        if (response && response.status == 'connected') {
            console.log('User is authorized');
            FB.api('/me?fields=name,email', function(response) {
                console.log(response);
                console.log('Good to see you, ' + response.email + '.');
                var username = response.name;
                var email = response.email;
                var phone = null;
                $('#loginModal').modal('toggle');
                $('#logSec').html("<div class='mnuContant'><a href='mailto:support@ulohotels.com' title='support@ulohotels.com'><i class='fa fa-envelope'></i><span class='visibleLG'>support@ulohotels.com</span></a><a href='tel:9543592593' title=''+91 95435 92593'><i class='fa fa-phone'></i><span class='visibleLG'>+91 95435 92593</span></a></div><div class='mnuProfile'><div class='mpAvatar hidden-xs' style='background:url(ulowebsite/images/avatar.png);'></div><div class='mpUsername'><a href='javascript:showpro();' class=' mnuProfileOpen'>Hi " + username + " <i class='fa fa-angle-down'></i></a></div><div class='mpSubMenu' id='showmenu' ><ul><li><a href='ulouserprofile'>Profile</a></li><li><a href='ulouserprofile'>Booking History</a></li><li><a href='ulologout'>Log out</a></li></ul></div></div>");
                $('#logSecs').html("<div class='mnuContant'><a href='mailto:support@ulohotels.com' title='support@ulohotels.com'><i class='fa fa-envelope'></i><span class='visibleLG'>support@ulohotels.com</span></a><a href='tel:9543592593' title=''+91 95435 92593'><i class='fa fa-phone'></i><span class='visibleLG'>+91 95435 92593</span></a></div><div class='mnuProfile'><div class='mpAvatar hidden-xs' style='background:url(ulowebsite/images/avatar.png);'></div><div class='mpUsername'><a href='javascript:showpro();' class=' mnuProfileOpen'>Hi " + username + " <i class='fa fa-angle-down'></i></a></div><div class='mpSubMenu' id='showmenu' ><ul><li><a href='ulouserprofile'>Profile</a></li><li><a href='ulouserprofile'>Booking History</a></li><li><a href='ulologout'>Log out</a></li></ul></div></div>");
                location.reload();
                var fdata = "username=" + response.name +
                    "&emailid=" + response.email +
                    "&userid=" + response.id;
                $http({
                    method: 'POST',
                    data: fdata,
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    url: 'sociallogin'
                }).then(function successCallback(response) {

                }, function errorCallback(response) {});
            })
        } else if (response.status === 'not_authorized') {
            // the user is logged in to Facebook, but has not authenticated your app
            console.log('User is not authorized');
        } else {
            // the user isn't logged in to Facebook.
            console.log('User is not logged into Facebook');

        }
    };
    $scope.searchButtonText = "APPLY";
    $scope.searchButtonSmsText = "APPLY";
    /* 	ulologin */
    $scope.userData = [];
    $scope.tempPrice = 0;
    $scope.priceDropped = 0;
    $scope.userData = [];
    $scope.login = function() {
        var fdata = "username=" + $('#username').val() +
            "&password=" + $('#password').val();
        $http({
            method: 'POST',
            data: fdata,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            url: 'ulologin'
        }).then(function successCallback(response) {
            $scope.userData = response.data;
            console.log($scope.userData);
            var username = $scope.userData.data[0].userName;
            var email = $scope.userData.data[0].emailId;
            var phone = $scope.userData.data[0].phone;
            $('#loginModal').modal('toggle');
            $('#logSec').html("<div class='mnuContant'><a href='mailto:support@ulohotels.com' title='support@ulohotels.com'><i class='fa fa-envelope'></i><span class='visibleLG'>support@ulohotels.com</span></a><a href='tel:9543592593' title=''+91 95435 92593'><i class='fa fa-phone'></i><span class='visibleLG'>+91 95435 92593</span></a></div><div class='mnuProfile'><div class='mpAvatar hidden-xs' style='background:url(ulowebsite/images/avatar.png);'></div><div class='mpUsername'><a href='javascript:showpro();' class=' mnuProfileOpen'>Hi admin<i class='fa fa-angle-down'></i></a></div><div class='mpSubMenu' id='showmenu' ><ul><li><a href='ulouserprofile'>Profile</a></li><li><a href='ulouserprofile'>Booking History</a></li><li><a href='ulologout'>Log out</a></li></ul></div></div>");
            $('#logSecs').html("<div class='mnuContant'><a href='mailto:support@ulohotels.com' title='support@ulohotels.com'><i class='fa fa-envelope'></i><span class='visibleLG'>support@ulohotels.com</span></a><a href='tel:9543592593' title=''+91 95435 92593'><i class='fa fa-phone'></i><span class='visibleLG'>+91 95435 92593</span></a></div><div class='mnuProfile'><div class='mpAvatar hidden-xs' style='background:url(ulowebsite/images/avatar.png);'></div><div class='mpUsername'><a href='javascript:showpro();' class=' mnuProfileOpen'>Hi admin<i class='fa fa-angle-down'></i></a></div><div class='mpSubMenu' id='showmenu' ><ul><li><a href='ulouserprofile'>Profile</a></li><li><a href='ulouserprofile'>Booking History</a></li><li><a href='ulologout'>Log out</a></li></ul></div></div>");
            location.reload();
        }, function errorCallback(response) {

            var alert = ' <div class="alert alert-danger alert-dismissible" style="color:red">Invalid username or password.</div>';
            $('#loginerror').html(alert);
            $(function() {
                setTimeout(function() {
                    $("#loginerror").hide('blind', {}, 100)
                }, 5000);
            });
        });
    };
    $scope.signup = function() {
        var fdata = "userName=" + $('#userName').val() +
        "&phone=" + $('#mobilePhone').val() +    
        "&emailId=" + $('#emailId').val() +
            "&roleId=" + $('#roleId').val() +
            "&accessRightsId=" + $('#accessRightsId').val() +
            "&password=" + $('#signpassword').val();
        $http({
            method: 'POST',
            data: fdata,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            url: 'ulosignup'
        }).then(function successCallback(response) {
            if (response.data == "") {
                var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:red">Sorry This Email Id already exist.</div>';
                $('#signuperror').html(alertmsg);
            } else {
                $scope.userData = response.data;
                var username = $scope.userData.data[0].userName;
                var email = $scope.userData.data[0].emailId;
                var phone = $scope.userData.data[0].phone;
                $('#registerModal').modal('toggle');
                $('#logSecs').html("<div class='mnuContant'><a href='mailto:support@ulohotels.com' title='support@ulohotels.com'><i class='fa fa-envelope'></i><span class='visibleLG'>support@ulohotels.com</span></a><a href='tel:9543592593' title=''+91 95435 92593'><i class='fa fa-phone'></i><span class='visibleLG'>+91 95435 92593</span></a></div><div class='mnuProfile'><div class='mpAvatar hidden-xs' style='background:url(ulowebsite/images/avatar.png);'></div><div class='mpUsername'><a href='javascript:showpro();' class=' mnuProfileOpen'>Hi admin<i class='fa fa-angle-down'></i></a></div><div class='mpSubMenu' id='showmenu' ><ul><li><a href='ulouserprofile'>Profile</a></li><li><a href='ulouserprofile'>Booking History</a></li><li><a href='ulologout'>Log out</a></li></ul></div></div>");
                $('#logSec').html("<div class='mnuContant'><a href='mailto:support@ulohotels.com' title='support@ulohotels.com'><i class='fa fa-envelope'></i><span class='visibleLG'>support@ulohotels.com</span></a><a href='tel:9543592593' title=''+91 95435 92593'><i class='fa fa-phone'></i><span class='visibleLG'>+91 95435 92593</span></a></div><div class='mnuProfile'><div class='mpAvatar hidden-xs' style='background:url(ulowebsite/images/avatar.png);'></div><div class='mpUsername'><a href='javascript:showpro();' class=' mnuProfileOpen'>Hi admin<i class='fa fa-angle-down'></i></a></div><div class='mpSubMenu' id='showmenu' ><ul><li><a href='ulouserprofile'>Profile</a></li><li><a href='ulouserprofile'>Booking History</a></li><li><a href='ulologout'>Log out</a></li></ul></div></div>");
                location.reload();
            }
        }, function errorCallback(response) {
            var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:red">This Email ID Already Exist</div>';
            $('#signuperror').html(alertmsg);
        });
    };
    $scope.passwordRequest = function() {
        var fdata = "emailId=" + $('#forgetEmailId').val();
        $http({
            method: 'POST',
            data: fdata,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            url: 'ulosendpassword'
        }).then(function successCallback(response) {
            var alertmsg = ' <div class="alert alert-success alert-dismissible" style="color:red">Password Sent Sucessfully.</div>';
            $('#forgetmessage').html(alertmsg);
        }, function errorCallback(response) {
            var alertmsg = ' <div class="alert alert-danger alert-dismissible" style="color:red">Your Email ID does not Registred Yet</div>';
            $('#forgetmessage').html(alertmsg);
        });
    };
    $('#forgetPanel').hide();
    $scope.showForget = function() {
        $('#loginPanel').hide();
        $('#forgetPanel').show();
    };
    $scope.showLogin = function() {
        $('#forgetPanel').hide();
        $('#loginPanel').show();
    };
    var propertyId = $('#propertyId').val();
    $scope.getType = function() {
        var text = $('#details').val();
        var data = JSON.parse(text);
        $http({
            method: 'POST',
            data: data,
            dataType: 'json',
            headers: {
                'Content-Type': 'application/json; charset=utf-8'
            },
            //url : 'bookingdetails'
            url: 'set-selected-details'
        }).success(function(response) {
            $scope.bookingDetails = response.data;
        });
    };
    $scope.confirmBooking = function() {
        $(this).attr('disabled', 'disabled');
        $('#loader').show();
       //$scope.bookingDetails[0].advanceAmount = $scope.getTotal();
        for (var i = 0; i < $scope.bookingDetails.length; i++) {
            var booked = $scope.bookingDetails[i];
           	var btotal = parseInt(booked.total);
           	var btax = parseInt(booked.tax);
            var gtotal = btotal+btax;
            var ototal = $scope.getTotal() ;
            var atotal = ototal - gtotal;
            //alert(atotal);
            if(ototal >= atotal){
             var aa = ototal-atotal;
             booked.advanceAmount = aa;
            //alert(booked.advanceAmount);
            }
        }
         var text = '{"array":' + JSON.stringify($scope.bookingDetails) + '}';
       // alert(text);
        var data = JSON.parse(text);
        var sourceid = 1;
        $http({
            method: 'POST',
            data: data,
            //data : '{"data":' +JSON.stringify($scope.roomsSelected) + '}',
            dataType: 'json',
            headers: {
                'Content-Type': 'application/json; charset=utf-8'
            },
            url: "add-booking?sourceId=" + sourceid
        }).then(function successCallback(response) {
            var gdata = "firstName=" + $('#firstName').val() +
                "&emailId=" + $('#guestEmail').val() +
                "&phone=" + $('#mobilePhone').val()
            $http({
                method: 'POST',
                data: gdata,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                url: 'add-guest'

            }).then(function successCallback(response) {
                var specialRequest = $('#specialRequest').val();
                var text = '{"array":' + JSON.stringify($scope.bookingDetails) + '}';
                var data = JSON.parse(text);
                $http({
                    method: 'POST',
                    data: data,
                    dataType: 'json',
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8'
                    },
                    url: 'add-booking-details?specialRequest=' + specialRequest
                    //url : 'jsonTest'
                }).then(function successCallback(response) {
                    $(this).removeAttr('disabled');
                    $('#loader').hide();
                    $scope.booked = response.data;
                    console.log($scope.booked.data[0].bookingId);
                    window.location = '/paymentsuccess';

                }, function errorCallback(response) {});
				
            }, function errorCallback(response) {});

        }, function errorCallback(response) {});

    };
    $scope.booking = function() {
        if ($scope.tempPrice == 0) {
            var payable = $scope.getTotal();
        } else {
            var payable = $scope.getTotal()
        }
     var options = {
//           "key": "rzp_test_bQQGkpEPtwppNI",
            "key": "rzp_live_vuYF9peaDXMoQF",
            "amount": parseInt(payable) * 100, // 2000 paise = INR 20
            "name": "Ulohotels.com",
            "description": "Welcome to Ulohotels",
            //"image": "/your_logo.png",
            "handler": function(response) {;
                $scope.confirmBooking();
            },
            "prefill": {
                "name": $('#firstName').val(),
                "email": $('#guestEmail').val(),
                "contact": $('#mobilePhone').val()
            },
            "notes": {
                "address": $('#guest_ph').val()
            },
            "theme": {
                "color": "#89b717"
            }
        };
        var rzp1 = new Razorpay(options);
        rzp1.open();
        e.preventDefault();
    };
    $scope.getTotal = function() {
        var total = 0;
        var tax = 0;
        for (var i = 0; i < $scope.bookingDetails.length; i++) {
            var bd = $scope.bookingDetails[i];
            tax += parseFloat(bd.tax);
            total += parseFloat(bd.total);
        }
        var all = (total + tax);
        if (isNaN(total)) total = 0;
        return all;
    }
    $scope.getCategoryTotal = function() {
        var total = 0;
        var tax = 0;
        for (var i = 0; i < $scope.bookingDetails.length; i++) {
            var bd = $scope.bookingDetails[i];
            total += parseFloat(bd.baseAmount);
        }
        var all = (total);
        return all;
    }
    $scope.getFinalTotal = function() {
        var total = 0;
        var tax = 0;
        for (var i = 0; i < $scope.bookingDetails.length; i++) {
            var bd = $scope.bookingDetails[i];
            tax += parseFloat(bd.tax);
            total += parseFloat(bd.total);
        }
        var all = (total + tax);
        return all;
    }
    $scope.getGrandTotal = function() {
        var total = 0;
        var tax = 0;
        for (var i = 0; i < $scope.bookingDetails.length; i++) {
            var bd = $scope.bookingDetails[i];
            total += parseFloat(bd.grandTotal);
        }
        var all = (total);
        return all;
    }
    $scope.getsolidTotal = function() {
        var total = 0;
        var tax = 0;
        for (var i = 0; i < $scope.bookingDetails.length; i++) {
            var bd = $scope.bookingDetails[i];
            tax += parseFloat(bd.tax);
            total += parseFloat(bd.total);
        }
        var all = (total + tax);
        return all;
    }
    $scope.getStrikeAmount = function() {
        var total = 0;
        for (var i = 0; i < $scope.bookingDetails.length; i++) {
            var bd = $scope.bookingDetails[i];
            total += parseFloat(bd.baseOriginAmount * bd.rooms);
        }
        var all = (total);
        return all;
    }
    $scope.getRoomCount = function() {
        var total = 0;
        for (var i = 0; i < $scope.bookingDetails.length; i++) {
            var bd = $scope.bookingDetails[i];
            total += parseFloat(bd.rooms);
        }
        var all = (total);
        return all;
    }
    $scope.getPersonCount = function() {
        var total = 0;
        var tax = 0;
        for (var i = 0; i < $scope.bookingDetails.length; i++) {
            var bd = $scope.bookingDetails[i];
            total += parseFloat(bd.adultsCount);
            tax += parseFloat(bd.childCount);
        }
        var all = (total + tax);
        return all;
    }
    $scope.getAdultCount = function() {
        var total = 0;
        for (var i = 0; i < $scope.bookingDetails.length; i++) {
            var bd = $scope.bookingDetails[i];
            total += parseFloat(bd.adultsCount);
        }
        var all = (total);
        return all;
    }
    $scope.getChildCount = function() {
        var total = 0;
        for (var i = 0; i < $scope.bookingDetails.length; i++) {
            var bd = $scope.bookingDetails[i];
            total += parseFloat(bd.childCount);
        }
        var all = (total);
        return all;
    }
    $scope.getTax = function() {
        var tax = 0;
        for (var i = 0; i < $scope.bookingDetails.length; i++) {
            var bd = $scope.bookingDetails[i];
            tax += parseFloat(bd.tax);
        }
        var all = (tax);
        return all;
    }
    $scope.getCurrentDiscounts = function() {
        var propertyId = $('#propertyId').val();
        var ulosignup = $('#uloSignup').val();
        var url = "get-current-discounts";
        $http.get(url).success(function(response) {
            $scope.discounts = response.data;
        });
    };
    
    $scope.getAllCurrentDiscounts = function() {
       
        var url = "get-all-current-discounts";
        $http.get(url).success(function(response) {
            $scope.allDiscounts = response.data;
        });
    };
    $scope.selectDiscount = function(indx) {
        percent = $scope.discounts[indx].discountPercentage;
        code = $scope.discounts[indx].discountName;
        id = $scope.discounts[indx].discountId;
        signup = $scope.discounts[indx].isSignup;
        $('#code_percentage').val(percent);
        $('#code_input').val(code);
        $('#code_id').val(id);
        $('#code_signup').val(signup);
    };
    
    
    
    $scope.applyDiscount = function() {
        $scope.test = "true";
        $scope.searchButtonText = "APPLYING";
        var grandTotal = $scope.getCategoryTotal();
         
        console.log($scope.allDiscounts);
        var CouponName = $('#code_input').val();
        var j = false;
        var len = $scope.allDiscounts.length;
        //var url = "check-coupon?discountName=" +  $('#code_input');
        for (var i = 0; i < $scope.allDiscounts.length; i++) {
            
        	
            if ($scope.allDiscounts[i].discountName === CouponName) {
            	 
                var discountPercentage =  $scope.allDiscounts[i].discountPercentage;
                var discountId =  $scope.allDiscounts[i].discountId;
                var ulosignup =  $scope.allDiscounts[i].ulosignup;
                var isSignup =  $scope.allDiscounts[i].isSignup;
                $scope.CouponName = $('#code_input').val();
                $scope.CouponPercent = discountPercentage;
                j =  true;
             
              
            } 
          
           /*  alert(j);
            
            if(j){
            	break;	
            } */
            
            else{
            	
            	
            	var discountPercentage =  $scope.discounts[0].discountPercentage;
                var discountId =  $scope.discounts[0].discountId;
                var ulosignup =  $scope.discounts[0].ulosignup;
                var isSignup =  $scope.discounts[0].isSignup;
                $scope.CouponName = $scope.discounts[0].discountName;
                $scope.CouponPercent = discountPercentage;
            	//alert(i);
            	
            }
            
           
            $timeout(function() {
            	$scope.searchButtonText = "APPLY";
            }, 2000);
      
      } 
       
      if(j==false){
          var alert = ' <div class="alert alert-danger alert-dismissible" style="color:red;font-size:12px;">Your coupon is invalid.</div>';
          $('#codeerror').html(alert);
          document.getElementById("code_input").value = $scope.discounts[0].discountName;
          $(function() {
              setTimeout(function() {
                  $("#codeerror").hide('blind', {}, 100)
                  $scope.searchButtonText = "APPLY";
              }, 5000);

        	  
          });
         
      }
       /*  var discountPercentage = $('#code_percentage').val();
        var discountId = $('#code_id').val();
        var ulosignup = $('#uloSignup').val();
        var isSignup = $('#code_signup').val();  */
        
        if (isSignup == "true") {
            if (ulosignup == "true") {
                $scope.priceDropped = Math.round(((grandTotal) * discountPercentage) / 100);
                $scope.tempPrice = Math.round((grandTotal) - (((grandTotal) * discountPercentage) / 100));
                $scope.bookingDetails[0].discountId = discountId;

                for (var i = 0; i < $scope.bookingDetails.length; i++) {
                    var booked = $scope.bookingDetails[i];
                    booked.total = Math.round((booked.discountBaseAmount) - (((booked.discountBaseAmount) * discountPercentage) / 100));
                    booked.tax = Math.round(booked.total * booked.taxPercentage / 100);
                }
                console.log($scope.bookingDetails);
                
                $timeout(function() {
                    $('#btnclose').click();
                }, 2000);
                
            } else {
                alert("please signup to apply this code");
            }
        } else {
            $scope.priceDropped = Math.round(((grandTotal) * discountPercentage) / 100);
            $scope.tempPrice = Math.round((grandTotal) - (((grandTotal) * discountPercentage) / 100));
            for (var i = 0; i < $scope.bookingDetails.length; i++) {
                var booked = $scope.bookingDetails[i];
                booked.total = Math.round((booked.discountBaseAmount) - (((booked.discountBaseAmount) * discountPercentage) / 100));
                booked.tax = Math.round(booked.total * booked.taxPercentage / 100);
                booked.discountId = discountId;
            }
            $timeout(function() {
                $('#btnclose').click();
            }, 2000);
            console.log($scope.bookingDetails);
        }

    };
    $scope.getUser = function() {
        $scope.username = $('#sc_name').val();
        $scope.useremail = $('#sc_email').val();
        var url = "get-customer?auserId=" + $('#userId').val();
        $http.get(url).success(function(response) {
            $scope.user = response.data;
            $scope.username = $scope.user[0].o_username;
            $scope.useremail = $scope.user[0].o_email;
            $scope.userphone = $scope.user[0].o_phone;

        });
    };
    var fdata = "propertyId=" + $('#propertyId').val()
    $scope.getProperties = function() {
        $http({
            method: 'POST',
            data: fdata,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            url: "get-location-properties"
        }).success(function(response) {
            $scope.properties = response.data;
            console.log($scope.properties);
            //alert($scope.properties[i].accommodations);
        });
    };
    
    $scope.sendPayAtHotelOtp = function() {
    	
   	 var gdata = "firstName=" + $('#firstName').val() +
        "&emailId=" + $('#guestEmail').val() +
        "&phone=" + $('#mobilePhone').val();
   	 $http({
            method: 'POST',
            data: gdata,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            url: "send-payathotel-otp"
        }).success(function(response) {
            $scope.payathotelotp = response.data;
            console.log($scope.payathotelotp);
            $('#smsModal').modal('show');
        });   	
     
   };    
  
   
   $scope.reSendPayAtHotelOtp = function() {
  	 var gdata = "guestId=" + $('#smsGuestId').val();
  	 $http({
           method: 'POST',
           data: gdata,
           headers: {
               'Content-Type': 'application/x-www-form-urlencoded'
           },
           url: "resend-payathotel-otp"
       }).success(function(response) {
           $scope.repayathotelotp = response.data;
           console.log($scope.repayathotelotp);
       });   	
    
  };
  
  $scope.getPayAtHotelStatus = function() {

	   var gdata = "propertyId=" + $('#propertyId').val();
	   
 	 $http({
          method: 'POST',
          data: gdata,
          headers: {
              'Content-Type': 'application/x-www-form-urlencoded'
          },
          url: "get-payathotel-status"
      }).success(function(response) {
          $scope.payathotelstatus = response.data;
          console.log($scope.payathotelstatus);
      });   	
   
 };
  
  $scope.verifySms = function() {
	  $scope.searchButtonSmsText = "APPLYING";
	 $("#loading-example-btn").prop('disabled', true)
	   var gdata = "guestId=" + $('#smsGuestId').val() +
	    "&payAtHotelOtp=" + $('#smsOtp').val() ;
	   
		 $http({
	            method: 'POST',
	            data: gdata,
	            headers: {
	                'Content-Type': 'application/x-www-form-urlencoded'
	            },
	            url: "verify-sms-otp"
	            
	        }).then(function successCallback(response) {
	   
      var text = '{"array":' + JSON.stringify($scope.bookingDetails) + '}';
      var data = JSON.parse(text);
      var sourceid = 1;
      $http({
          method: 'POST',
          data: data,
          //data : '{"data":' +JSON.stringify($scope.roomsSelected) + '}',
          dataType: 'json',
          headers: {
              'Content-Type': 'application/json; charset=utf-8'
          },
          url: "add-booking?sourceId=" + sourceid
      }).then(function successCallback(response) {
   	   
              var specialRequest = $('#specialRequest').val();
              var text = '{"array":' + JSON.stringify($scope.bookingDetails) + '}';

              var data = JSON.parse(text);
              $http({
                  method: 'POST',
                  data: data,
                  dataType: 'json',
                  headers: {
                      'Content-Type': 'application/json; charset=utf-8'
                  },
                  url: 'add-booking-details?specialRequest=' + specialRequest
                  //url : 'jsonTest'
              }).then(function successCallback(response) {
                  $(this).removeAttr('disabled');
                  $('#loader').hide();
                  $scope.booked = response.data;
                  console.log($scope.booked.data[0].bookingId);
                  window.location = '/paymentsuccess';

              }, function errorCallback(response) {});
				
          }, function errorCallback(response) {});

      }, function errorCallback(response) {
  	    $("#loading-example-btn").prop('disabled', true)
	    $scope.searchButtonSmsText = "APPLY";
       var alert = ' <div class="alert alert-danger alert-dismissible" style="color:red;font-size:12px;">Please Enter Valid OTP</div>';
      $('#smserror').html(alert);
      $(function() {
          setTimeout(function() {
              $("#smserror").hide('blind', {}, 100)
          }, 5000);
    	  
    	  
    	  
      });
	
          }); 
	
	    
	   };
	   
    $scope.getCurrentDiscounts();
    $scope.getAllCurrentDiscounts();
    $scope.getUser();
    $scope.getType();
    $scope.getPayAtHotelStatus();
});
</script>
<!-- //Coupons Popup Modal Box -->

