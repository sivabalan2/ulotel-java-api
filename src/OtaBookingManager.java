



import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;


import com.ulopms.util.HibernateUtil;


public class OtaBookingManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(OtaBookingManager.class);

	public OtaBooking add(OtaBooking otaBooking) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(otaBooking);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return otaBooking;
	}

	public OtaBooking edit(OtaBooking otaBooking) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(otaBooking);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return otaBooking;
	}

	public OtaBooking delete(Long id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		OtaBooking otaBooking = (OtaBooking) session.load(OtaBooking.class, id);
		if (null != otaBooking) {
			session.delete(otaBooking);
		}
		session.getTransaction().commit();
		return otaBooking;
	}

	public OtaBooking find(int id) {
		OtaBooking otaBooking = new OtaBooking();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			otaBooking = (OtaBooking) session.load(OtaBooking.class, id);
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return otaBooking;
	}
	
	
    
}
