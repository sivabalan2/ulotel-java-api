


import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.lang.Long;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.sun.xml.internal.txw2.annotation.XmlAttribute;
import com.sun.xml.internal.txw2.annotation.XmlElement;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

public  class CancelledListing implements Serializable {
private static final long serialVersionUID = 1L; 
	
	
   @JacksonXmlProperty(isAttribute = true, localName = "HotelCode")
   private String HotelCode;
	//private String HotelCode;
   
   @JacksonXmlProperty(localName = "CancelledItem")
   private CancelledItem  CancelledItem;

   

public CancelledListing(){	
		
		
		
	}

public CancelledListing(String customerName,String roomTypeName, Integer bookingId){
	
}

public String getHotelCode() {
	return HotelCode;
}

public void setHotelCode(String HotelCode) {
	HotelCode = HotelCode;
}

public CancelledItem getCancelledItem() {
	return CancelledItem;
}

public void setCancelledItem(CancelledItem CancelledItem) {
	CancelledItem = CancelledItem;
}




}


