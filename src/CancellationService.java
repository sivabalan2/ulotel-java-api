

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import com.ulopms.controller.BookingDetailManager;
import com.ulopms.controller.PmsBookingManager;
import com.ulopms.controller.PmsStatusManager;
import com.ulopms.model.BookingDetail;
import com.ulopms.model.PmsBooking;
import com.ulopms.model.PmsStatus;

public class CancellationService {
	
	
	
	private List<BookingDetail> bookingDetailList;
	
	//@XStreamImplicit(itemFieldName = "CancelledItem")
	//public List<CancelledItem> array ;
	
public Integer add(CancelledListing cancelledListing){
	
	/*else if (cancelledItem != null) {
	}*/
		
    	try{
    		
    		
    		
    		
    		SimpleDateFormat format1=new SimpleDateFormat("yyyy-MM-dd");
    		DateFormat format2=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    		OtaCancellationManager otaCancellationController=new OtaCancellationManager();
    		PmsBookingManager bookingController = new PmsBookingManager();
    		
    		
        	java.util.Date dateCheckIn = format1.parse(cancelledListing.getCancelledItem().getCancellationDate());
        	OtaCancellation otaCancel =new OtaCancellation();
        	PmsStatusManager statusController = new PmsStatusManager();
        	BookingDetailManager detailController = new BookingDetailManager();
        	otaCancel.setOtaBookingId(cancelledListing.getCancelledItem().getBookingId());
        	otaCancel.setCancellationStatus(cancelledListing.getCancelledItem().getCancellationStatus());
        	otaCancel.setOtaCancellationId(cancelledListing.getCancelledItem().getCancellationId());
        	otaCancel.setCancellationDate(dateCheckIn);
        	otaCancellationController.add(otaCancel);
        	PmsBooking otaBooking = bookingController.findOtaBookingId(cancelledListing.getCancelledItem().getBookingId());
    		PmsBooking booking = bookingController.find(otaBooking.getBookingId());
    		
    		long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			booking.setIsActive(false);
			booking.setIsDeleted(true);
			booking.setModifiedDate(new java.sql.Timestamp(date.getTime()));
			PmsStatus status = statusController.find(4);
			booking.setPmsStatus(status);
			bookingController.edit(booking);
			
			
			this.bookingDetailList = detailController.findId(otaBooking.getBookingId());
			for (BookingDetail bookingDetails : bookingDetailList){	
				int accommId=bookingDetails.getPropertyAccommodation().getAccommodationId();
				if(accommId==(int)bookingDetails.getPropertyAccommodation().getAccommodationId()){
					bookingDetails.setIsActive(false);
					bookingDetails.setIsDeleted(true);
					
					bookingDetails.setModifiedDate(new java.sql.Timestamp(date.getTime()));
					bookingDetails.setPmsStatus(status);
					detailController.edit(bookingDetails);
		   
				}
				
			}
    	}catch(Exception e){
    		e.printStackTrace();
    	}finally{
    		
    	}
    	return 200;
    }

}
