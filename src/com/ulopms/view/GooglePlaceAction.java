package com.ulopms.view;

import java.io.File;
import java.io.IOException;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.catalina.startup.UserConfig;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;
import org.jsoup.select.Evaluator.IsEmpty;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.SdkClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.opensymphony.xwork2.ActionSupport;
import com.ulopms.controller.AreaManager;
import com.ulopms.controller.GooglePlaceManager;
import com.ulopms.controller.LocationManager;
import com.ulopms.controller.PmsPropertyManager;
import com.ulopms.controller.PropertyAreasManager;
import com.ulopms.controller.UserLoginManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.Area;
import com.ulopms.model.GoogleArea;
import com.ulopms.model.GoogleLocation;
import com.ulopms.model.GooglePropertyAreas;
import com.ulopms.model.Location;
import com.ulopms.model.LocationType;
import com.ulopms.model.PmsProperty;
import com.ulopms.model.PropertyAreas;
import com.ulopms.model.UlotelHotelDetailTags;
import com.ulopms.model.User;
import com.ulopms.util.Image;


public class GooglePlaceAction extends ActionSupport implements SessionAware,ServletRequestAware,
UserAware {

	private static final long serialVersionUID = 9149826260758390091L;

	
	
	private Integer googleLocationId;
	private Integer googleAreaId;
	private Integer propertyId;
	private Integer locationTypeId;
	private String propertyName;
	private String propertyUrl;
	private String checkedAreas;
	private Integer areaLocationId;
	private String googleLocationName;
	private String googleLocationUrl;
	private String googleLocationDisplayName;
	private String googleLocationPlaceId;
	private String googleRedirectUrl;
	private double googleLocationLatitude;
	private double googleLocationLongitude;
	private String description;
	private Integer limit;
	private Integer offset;
	private Boolean isActive;
	private String myFileFileName;
	private File myFile;
	private String googleAreaName;
	private String googleAreaDisplayName;
	private String googleAreaUrl;
	private String googleAreaPlaceId;
	private double googleAreaLatitude;
	private double googleAreaLongitude;
	private List<GoogleLocation> googleLocationList;
	private List<GoogleArea> googleAreaList;
	private String urlType;
	
	
	private HttpSession session;

	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}
	
	private SessionMap<String,Object> sessionMap;  
	  
	//getters and setters  
	
	public void setSession(Map<String, Object> map) {  
	    sessionMap=(SessionMap)map;  
	}  
	
	
	private static final Logger logger = Logger.getLogger(GooglePlaceAction.class);

	private HttpServletRequest request;

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	private User user;

	@Override
	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	
	
	public GooglePlaceAction() {

	}


	public String execute() {
		
		return SUCCESS;
	}
	
	
	String clientRegion = "ap-south-1";
	String bucketName = "ulohotelsuploads/uloimg";    
    String keyName = "ulohotelsuploads";
	
	
	public String uploadAwsImageLocation(){
			    
	        try {
	        	
	        	GooglePlaceManager googleLocationController=new GooglePlaceManager();
	        	AWSCredentials credentials = new BasicAWSCredentials("AKIA3BSHXMURUP3WEKWH", "BjOPBijiDBysvDDbOLJOTzEK+k7yl/mxxWF28k/Y");
		        String bucketName1 = "ulohotelsuploads/uloimg/location/"+getGoogleLocationId()+"";
	            AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
	                    .withRegion(clientRegion)
	                    .withCredentials(new AWSStaticCredentialsProvider(credentials))
	                    .build();
	        
	            // Upload a text string as a new object.
	            s3Client.putObject(bucketName1, this.getMyFileFileName(), this.myFile);
	            
	            GoogleLocation googleLocation = googleLocationController.find(getGoogleLocationId()); 
				googleLocation.setPhotoPath(this.getMyFileFileName());
				googleLocationController.edit(googleLocation);
	        }
	        catch(AmazonServiceException e) {
	            e.printStackTrace();
	        }
	        catch(SdkClientException e) {
	            e.printStackTrace();
	        }
			
			return SUCCESS; 
		}
	
	public String uploadAwsImageArea(){
	    
        try {
        	
        	GooglePlaceManager googleAreaController=new GooglePlaceManager();
        	AWSCredentials credentials = new BasicAWSCredentials("AKIA3BSHXMURUP3WEKWH", "BjOPBijiDBysvDDbOLJOTzEK+k7yl/mxxWF28k/Y");
	        String bucketName1 = "ulohotelsuploads/uloimg/area/"+getGoogleAreaId()+"";
            AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
                    .withRegion(clientRegion)
                    .withCredentials(new AWSStaticCredentialsProvider(credentials))
                    .build();
        
            // Upload a text string as a new object.
            s3Client.putObject(bucketName1, this.getMyFileFileName(), this.myFile);
            
            GoogleArea googleArea = googleAreaController.findArea(getGoogleAreaId()); 
        	googleArea.setPhotoPath(this.getMyFileFileName());
        	googleAreaController.edit(googleArea);
        }
        catch(AmazonServiceException e) {
            e.printStackTrace();
        }
        catch(SdkClientException e) {
            e.printStackTrace();
        }
		
		return SUCCESS; 
	}
		
	public String addGoogleLocation(){
		try
		{
			long time = System.currentTimeMillis();
			java.util.Date date = new java.util.Date(time);
			Timestamp tsDate=new Timestamp(date.getTime());  
			
			Integer userId=(Integer) sessionMap.get("userId");
			
			GooglePlaceManager locationController=new GooglePlaceManager();
			GoogleLocation googleLocation =new GoogleLocation();
			googleLocation.setGoogleLocationName(getGoogleLocationName());
			googleLocation.setGoogleLocationUrl(getGoogleLocationUrl());
			googleLocation.setGoogleLocationDisplayName(getGoogleLocationDisplayName());
			googleLocation.setDescription(getDescription());
			googleLocation.setGoogleLocationLatitude(getGoogleLocationLatitude());
			googleLocation.setGoogleLocationLongitude(getGoogleLocationLongitude());
			googleLocation.setGoogleLocationPlaceId(getGoogleLocationPlaceId());
			googleLocation.setIsActive(true);
			googleLocation.setIsDeleted(false);
			//googleLocation.setCreatedBy(userId);
			googleLocation.setCreatedOn(tsDate);
			GoogleLocation location = locationController.add(googleLocation);
			
			this.googleLocationId = location.getGoogleLocationId();
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("application/json");
			
			  if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"googleLocationId\":\"" + this.googleLocationId  + "\"";
				
				jsonOutput += "}";


			

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
		}catch(Exception e){
			logger.error(e);
			e.printStackTrace();
		}
		return null;
	}
	
	
	public String getGoogleLocations() throws IOException {
		try {
			String jsonOutput = "";		
			HttpServletResponse response = ServletActionContext.getResponse();
			UserLoginManager userController=new UserLoginManager();
			
			GooglePlaceManager googlePlaceController = new GooglePlaceManager();
			response.setContentType("application/json");
			int serialno=1;
			this.googleLocationList = googlePlaceController.list(limit,offset);
			if(googleLocationList.size()>0 && !googleLocationList.isEmpty()){
				for (GoogleLocation googleLocation : googleLocationList) {
					
					if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += ",{";
					else
						jsonOutput += "{";
					
					jsonOutput += "\"googleLocationId\":\"" + googleLocation.getGoogleLocationId()+ "\"";
					jsonOutput += ",\"googleLocationName\":\"" + googleLocation.getGoogleLocationName()+ "\"";
					jsonOutput += ",\"googleLocationDisplayName\":\"" + googleLocation.getGoogleLocationDisplayName()+ "\"";
					jsonOutput += ",\"googleLocationUrl\":\"" + googleLocation.getGoogleLocationUrl()+ "\"";
					jsonOutput += ",\"serialNo\":\"" + serialno + "\"";
					if(googleLocation.getCreatedBy()!=null){
						User user=userController.find(googleLocation.getCreatedBy());
						String strFirstName=user.getUserName().trim();
						String firstNameLetterUpperCase=strFirstName.substring(0, 1).toUpperCase()+strFirstName.substring(1);
						jsonOutput += ",\"createdBy\":\"" + firstNameLetterUpperCase+ "\"";
					}else{
						jsonOutput += ",\"createdBy\":\"" + " "+ "\"";
					}
					
					
					jsonOutput += "}";
					
					serialno++;
				}
			}else{
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				
				jsonOutput += "\"locationId\":\"" + ""+ "\"";
				jsonOutput += ",\"locationName\":\"" + ""+ "\"";
				jsonOutput += ",\"serialNo\":\"" + "" + "\"";
				jsonOutput += ",\"createdBy\":\"" + ""+ "\"";
				jsonOutput += "}";
			}
			

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	public String getAreaGoogleLocations() throws IOException {
		try {
			String jsonOutput = "";		
			HttpServletResponse response = ServletActionContext.getResponse();
			UserLoginManager userController=new UserLoginManager();
			
			GooglePlaceManager googlePlaceController = new GooglePlaceManager();
			response.setContentType("application/json");
			int serialno=1;
			this.googleLocationList = googlePlaceController.list();
			if(googleLocationList.size()>0 && !googleLocationList.isEmpty()){
				for (GoogleLocation googleLocation : googleLocationList) {
					
					if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += ",{";
					else
						jsonOutput += "{";
					
					jsonOutput += "\"googleLocationId\":\"" + googleLocation.getGoogleLocationId()+ "\"";
					jsonOutput += ",\"googleLocationName\":\"" + googleLocation.getGoogleLocationName()+ "\"";
					jsonOutput += ",\"googleLocationDisplayName\":\"" + googleLocation.getGoogleLocationDisplayName()+ "\"";
					jsonOutput += ",\"googleLocationUrl\":\"" + googleLocation.getGoogleLocationUrl()+ "\"";
					jsonOutput += ",\"serialNo\":\"" + serialno + "\"";
					if(googleLocation.getCreatedBy()!=null){
						User user=userController.find(googleLocation.getCreatedBy());
						String strFirstName=user.getUserName().trim();
						String firstNameLetterUpperCase=strFirstName.substring(0, 1).toUpperCase()+strFirstName.substring(1);
						jsonOutput += ",\"createdBy\":\"" + firstNameLetterUpperCase+ "\"";
					}else{
						jsonOutput += ",\"createdBy\":\"" + " "+ "\"";
					}
					
					
					jsonOutput += "}";
					
					serialno++;
				}
			}else{
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				
				jsonOutput += "\"locationId\":\"" + ""+ "\"";
				jsonOutput += ",\"locationName\":\"" + ""+ "\"";
				jsonOutput += ",\"serialNo\":\"" + "" + "\"";
				jsonOutput += ",\"createdBy\":\"" + ""+ "\"";
				jsonOutput += "}";
			}
			

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	public String getReportGoogleLocation(){

		try {
			String jsonOutput = "";		
			HttpServletResponse response = ServletActionContext.getResponse();
			UserLoginManager userController=new UserLoginManager();
			
			GooglePlaceManager googlePlaceController = new GooglePlaceManager();
			response.setContentType("application/json");
			int serialno=1;
			this.googleLocationList = googlePlaceController.list();
			if(googleLocationList.size()>0 && !googleLocationList.isEmpty()){
				for (GoogleLocation googleLocation : googleLocationList) {
					
					if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += ",{";
					else
						jsonOutput += "{";
					
					jsonOutput += "\"googleLocationId\":\"" + googleLocation.getGoogleLocationId()+ "\"";
					jsonOutput += ",\"googleLocationName\":\"" + googleLocation.getGoogleLocationName()+ "\"";
					jsonOutput += ",\"googleLocationDisplayName\":\"" + googleLocation.getGoogleLocationDisplayName()+ "\"";
					jsonOutput += ",\"googleLocationUrl\":\"" + googleLocation.getGoogleLocationUrl()+ "\"";
					jsonOutput += ",\"serialNo\":\"" + serialno + "\"";
					if(googleLocation.getCreatedBy()!=null){
						User user=userController.find(googleLocation.getCreatedBy());
						String strFirstName=user.getUserName().trim();
						String firstNameLetterUpperCase=strFirstName.substring(0, 1).toUpperCase()+strFirstName.substring(1);
						jsonOutput += ",\"createdBy\":\"" + firstNameLetterUpperCase+ "\"";
					}else{
						jsonOutput += ",\"createdBy\":\"" + " "+ "\"";
					}
					
					
					jsonOutput += "}";
					
					serialno++;
				}
			}
			

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	
	}
	
	public String getGoogleLocationCount() throws IOException {
 		try {
 			String jsonOutput = "";
 			HttpServletResponse response = ServletActionContext.getResponse();
 			response.setContentType("application/json");
 			GooglePlaceManager googleLocationController=new GooglePlaceManager();
 			List<GoogleLocation> listLocationCount =googleLocationController.list(); 
 			if(listLocationCount.size()>0 && !listLocationCount.isEmpty()){
 				if (!jsonOutput.equalsIgnoreCase(""))
 					jsonOutput += ",{";
 				else
 					jsonOutput += "{";
 				
 			    jsonOutput += "\"count\":\"" + listLocationCount.size() + "\"";
 			    
 				jsonOutput += "}";
 			}
 			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
 		} catch (Exception e) {
 			logger.error(e);
 			e.printStackTrace();
 		} finally {

 		}

 		return null;

 	}
	
	public String getGoogleLocation() throws IOException {
		
		try {

			String jsonOutput = "",photoPath="";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			GooglePlaceManager googlePlaceController = new GooglePlaceManager();
			
			response.setContentType("application/json");
			this.googleLocationList = googlePlaceController.list(getGoogleLocationId());
			for (GoogleLocation googleLocation : googleLocationList) {

				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				
				jsonOutput += "\"googleLocationId\":\"" + googleLocation.getGoogleLocationId()+ "\"";
				jsonOutput += ",\"googleLocationName\":\"" + googleLocation.getGoogleLocationName()+ "\"";
				jsonOutput += ",\"googleLocationDisplayName\":\"" + googleLocation.getGoogleLocationDisplayName()+ "\"";
				jsonOutput += ",\"locationDescription\":\"" + googleLocation.getDescription()+ "\"";
				jsonOutput += ",\"photoPath\":\"" + googleLocation.getPhotoPath()+ "\"";
				jsonOutput += ",\"googleLocationUrl\":\"" + googleLocation.getGoogleLocationUrl()+ "\"";
				jsonOutput += ",\"googleLocationPlaceId\":\"" + googleLocation.getGoogleLocationPlaceId()+ "\"";
				
				photoPath = getText("storage.aws.property.photo") + "/uloimg/location/"
						+ googleLocation.getGoogleLocationId()+"/"+googleLocation.getPhotoPath();
						
				jsonOutput += ",\"photoPath\":\"" + (photoPath)+ "\"";
						
				jsonOutput += "}";

			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	public String editGoogleLocation() throws IOException {
		try {
			long time = System.currentTimeMillis();
			java.util.Date date = new java.util.Date(time);
			Timestamp tsDate=new Timestamp(date.getTime());  
			
			Integer userId=(Integer) sessionMap.get("userId");
			
			GooglePlaceManager googleLocationController=new GooglePlaceManager();
			
			GoogleLocation googleLocation = googleLocationController.find(getGoogleLocationId()); 
			googleLocation.setGoogleLocationName(getGoogleLocationName());
			googleLocation.setGoogleLocationDisplayName(getGoogleLocationDisplayName());
			googleLocation.setGoogleLocationUrl(getGoogleLocationUrl());
			//googleLocation.setLocationDescription(getLocationDescription());
			//googleLocation.setDescription(getDescription());
			googleLocation.setCreatedBy(userId);
			googleLocation.setIsActive(true);
			googleLocation.setIsDeleted(false);
			googleLocation.setModifiedBy(userId);
			googleLocation.setModifiedOn(tsDate);
			googleLocationController.edit(googleLocation);
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return SUCCESS;
	}
	
	public String editGoogleArea() throws IOException {
		try {
			long time = System.currentTimeMillis();
			java.util.Date date = new java.util.Date(time);
			Timestamp tsDate=new Timestamp(date.getTime());  
			
			Integer userId=(Integer) sessionMap.get("userId");
			
			GooglePlaceManager googleLocationController=new GooglePlaceManager();
			
			GoogleArea googleArea = googleLocationController.findArea(getGoogleAreaId()); 
			//googleArea.setGoogleAreaName(getGoogleLocationName());
			googleArea.setGoogleAreaDisplayName(getGoogleAreaDisplayName());
			googleArea.setGoogleAreaUrl(getGoogleAreaUrl());
			if(getGoogleRedirectUrl().isEmpty() == false){
				googleArea.setGoogleRedirectUrl(getGoogleRedirectUrl());
				googleArea.setIsActive(false);

			}
			
			googleArea.setDescription(getDescription());
			googleArea.setCreatedBy(userId);
			googleArea.setModifiedBy(userId);
			googleArea.setModifiedOn(tsDate);
			googleLocationController.edit(googleArea);
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return SUCCESS;
	}
	
	public String addGoogleLocationPicture() throws IOException {
		try {
			GooglePlaceManager googleLocationController=new GooglePlaceManager();
			//Location locations=new Location();
			File fileZip1 = new File(getText("storage.googlelocation.photo") + "/"
					+ getGoogleLocationId() + "/", this.getMyFileFileName());
			FileUtils.copyFile(this.myFile, fileZip1);// copying image in the
			GoogleLocation googleLocation = googleLocationController.find(getGoogleLocationId()); 
			googleLocation.setPhotoPath(this.getMyFileFileName());
			googleLocationController.edit(googleLocation);
			/*BufferedImage image = ImageIO.read(this.myFile);
			int height = image.getHeight();
			int width = image.getWidth();
			if(height == 400 && width == 300){
				if(this.getMyFileFileName()!=null && !this.getMyFileFileName().equalsIgnoreCase(""))
				{
					File fileZip1 = new File(getText("storage.location.photo") + "/"
							+ getLocationId() + "/", this.getMyFileFileName());
					FileUtils.copyFile(this.myFile, fileZip1);// copying image in the
					Location locations = locationController.find(getLocationId()); 
					locations.setPhotoPath(this.getMyFileFileName());
					locationController.edit(locations);
				}
			}
			*/
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return SUCCESS;
	}
	
	 public String getGoogleLocationPicture() {
			
			//this.propertyId = (Integer) sessionMap.get("propertyId");
			HttpServletResponse response = ServletActionContext.getResponse();

			GooglePlaceManager googleLocationController = new GooglePlaceManager();
			GoogleLocation googleLocation = googleLocationController.find(getGoogleLocationId());
			String imagePath = "";
			// response.setContentType("");
			try {
				
				if (getGoogleLocationId() >0 ) {
					imagePath = getText("storage.googlelocation.photo") + "/"
							+ googleLocation.getGoogleLocationId()+ "/"
							+ googleLocation.getPhotoPath();
					Image im = new Image();
					response.getOutputStream().write(im.getCustomImageInBytes(imagePath));
					response.getOutputStream().flush();
				} else {
					imagePath = getText("storage.path") + "/emptyprofilepic.png";
					Image im = new Image();
					response.getOutputStream().write(
							im.getCustomImageInBytes(imagePath));
					response.getOutputStream().flush();
				}
			} catch (Exception e1) {
				logger.error(e1);
				e1.printStackTrace();
			} finally {

			}
			return null;

		}
	 
	 
	 public String deleteGoogleLocation(){
			try{
				long time = System.currentTimeMillis();
				java.util.Date date = new java.util.Date(time);
				Timestamp tsDate=new Timestamp(date.getTime());  
				
				Integer userId=(Integer) sessionMap.get("userId");
				
				List<GoogleLocation> listLocation=null;
				GooglePlaceManager googleController=new GooglePlaceManager();
				listLocation=googleController.list(getGoogleLocationId());
				if(listLocation.size()>0 && !listLocation.isEmpty()){
					for(GoogleLocation loc:listLocation){
						loc.setIsActive(false);
						loc.setIsDeleted(true);
						loc.setModifiedBy(userId);
						loc.setModifiedOn(tsDate);
						googleController.edit(loc);
					}
				}
				
			}catch(Exception e){
				logger.error(e);
				e.printStackTrace();
			}finally{
				
			}
			return null;
		}
	 
	 public String deleteGoogleArea(){
			try{
				long time = System.currentTimeMillis();
				java.util.Date date = new java.util.Date(time);
				Timestamp tsDate=new Timestamp(date.getTime());  
				
				Integer userId=(Integer) sessionMap.get("userId");
				
				GooglePlaceManager googleController=new GooglePlaceManager();
				GoogleArea area =googleController.findArea(getGoogleAreaId());
					area.setIsActive(false);
					area.setIsDeleted(true);
					area.setModifiedBy(userId);
					area.setModifiedOn(tsDate);
					googleController.edit(area);
				
			}catch(Exception e){
				logger.error(e);
				e.printStackTrace();
			}finally{
				
			}
			return null;
		}
	 
	 
	 public String googleSearch(){
		 String strReturn = null;
			try{
			
				String strStartDate=request.getParameter("checkIn");
				String strEndDate=request.getParameter("checkOut");
				String lon=request.getParameter("lon");
				String lat=request.getParameter("lat");
				String placeId=request.getParameter("placeid");
				
				if(strStartDate!=null && strStartDate!="" && strEndDate!=null && strEndDate!=""){
					 DateFormat format = new SimpleDateFormat("MMM d,yyyy", Locale.ENGLISH);
				     java.util.Date dateStart = format.parse(strStartDate);
				     java.util.Date dateEnd = format.parse(strEndDate);

				     Calendar calCheckStart=Calendar.getInstance();
				     calCheckStart.setTime(dateStart);
				     java.util.Date checkInDate = calCheckStart.getTime();
				    
				     Calendar calCheckEnd=Calendar.getInstance();
				     calCheckEnd.setTime(dateEnd);
				     java.util.Date checkOutDate = calCheckEnd.getTime();
					
				     String checkIn = new SimpleDateFormat("MMM d,yyyy").format(checkInDate);
	                 String checkOut = new SimpleDateFormat("MMM d,yyyy").format(checkOutDate);
	                 String checkIn1 = new SimpleDateFormat("MM/dd/yyyy").format(checkInDate); //dd MMM
	                 String checkOut1 = new SimpleDateFormat("MM/dd/yyyy").format(checkOutDate);  //dd MMM
	                 String checkIn2 = new SimpleDateFormat("MMM dd EEEEE").format(checkInDate);
	                 String checkOut2 = new SimpleDateFormat("MMM dd EEEEE").format(checkOutDate);
	                 String mbcheckIn = new SimpleDateFormat("MMM d,yyyy").format(checkInDate);
	                 String mbcheckOut = new SimpleDateFormat("MMM d,yyyy").format(checkOutDate);
	                 String mbcheckIn1 = new SimpleDateFormat("MM/dd/yyyy").format(checkInDate); //dd MMM
	                 String mbcheckOut1 = new SimpleDateFormat("MM/dd/yyyy").format(checkOutDate);  //dd MMM
	                 String mbcheckIn2 = new SimpleDateFormat("MMM dd E").format(checkInDate);
	                 String mbcheckOut2 = new SimpleDateFormat("MMM dd E").format(checkOutDate);
	                 request.setAttribute("checkIn",checkIn);
	                 request.setAttribute("checkOut",checkOut);
	                 request.setAttribute("checkIn1",checkIn1);
	                 request.setAttribute("checkOut1",checkOut1);
	                 request.setAttribute("checkIn2",checkIn2);
	                 request.setAttribute("checkOut2",checkOut2);
	                 request.setAttribute("mbcheckIn",mbcheckIn);
	                 request.setAttribute("mbcheckOut",mbcheckOut);
	                 request.setAttribute("mbcheckIn1",mbcheckIn1);
	                 request.setAttribute("mbcheckOut1",mbcheckOut1);
	                 request.setAttribute("mbcheckIn2",mbcheckIn2);
	                 request.setAttribute("mbcheckOut2",mbcheckOut2);

	                 
	                 
				 }else{
					 java.util.Date date=new java.util.Date();
					 Calendar today=Calendar.getInstance();
					 today.setTime(date);
					 today.setTimeZone(TimeZone.getTimeZone("IST"));
					   
						
					 Calendar calCheckStart=Calendar.getInstance();
					 calCheckStart.setTime(date);
				     calCheckStart.setTimeZone(TimeZone.getTimeZone("IST"));
				     java.util.Date checkInDate = calCheckStart.getTime();
				    
				     Calendar calCheckEnd=Calendar.getInstance();
				     calCheckEnd.setTime(date);
				     calCheckEnd.add(Calendar.DATE, 1); 
				     calCheckEnd.setTimeZone(TimeZone.getTimeZone("IST"));
				     java.util.Date checkOutDate = calCheckEnd.getTime();
					
				     String checkIn = new SimpleDateFormat("MMM d,yyyy").format(checkInDate);
	                 String checkOut = new SimpleDateFormat("MMM d,yyyy").format(checkOutDate);
	                 String checkIn1 = new SimpleDateFormat("MM/dd/yyyy").format(checkInDate); //dd MMM
	                 String checkOut1 = new SimpleDateFormat("MM/dd/yyyy").format(checkOutDate);  //dd MMM
	                 String checkIn2 = new SimpleDateFormat("MMM dd EEEEE").format(checkInDate);
	                 String checkOut2 = new SimpleDateFormat("MMM dd EEEEE").format(checkOutDate);
	                 String mbcheckIn = new SimpleDateFormat("MMM d,yyyy").format(checkInDate);
	                 String mbcheckOut = new SimpleDateFormat("MMM d,yyyy").format(checkOutDate);
	                 String mbcheckIn1 = new SimpleDateFormat("MM/dd/yyyy").format(checkInDate); //dd MMM
	                 String mbcheckOut1 = new SimpleDateFormat("MM/dd/yyyy").format(checkOutDate);  //dd MMM
	                 String mbcheckIn2 = new SimpleDateFormat("MMM dd E").format(checkInDate);
	                 String mbcheckOut2 = new SimpleDateFormat("MMM dd E").format(checkOutDate);
	                 request.setAttribute("checkIn",checkIn);
	                 request.setAttribute("checkOut",checkOut);
	                 request.setAttribute("checkIn1",checkIn1);
	                 request.setAttribute("checkOut1",checkOut1);
	                 request.setAttribute("checkIn2",checkIn2);
	                 request.setAttribute("checkOut2",checkOut2);
	                 request.setAttribute("mbcheckIn",mbcheckIn);
	                 request.setAttribute("mbcheckOut",mbcheckOut);
	                 request.setAttribute("mbcheckIn1",mbcheckIn1);
	                 request.setAttribute("mbcheckOut1",mbcheckOut1);
	                 request.setAttribute("mbcheckIn2",mbcheckIn2);
	                 request.setAttribute("mbcheckOut2",mbcheckOut2);
				 }	
				
				GooglePlaceManager googleController = new GooglePlaceManager();
				GoogleLocation google = googleController.findLocation(placeId);
				 if(google != null){
					 request.setAttribute("googleLocationId" ,google.getGoogleLocationId());
					 this.googleLocationId=google.getGoogleLocationId(); 
//					 request.setAttribute("areaLocationId" ,google.getGoogleLocationId());
					 
					
					 }else{
					 GoogleArea area  = googleController.findArea(placeId);
					 request.setAttribute("googleAreaId" ,area.getGoogleAreaId());
				 }
				 urlType="/properties.tiles";
			    // strReturn=SUCCESS;
				
			}catch(Exception e){
				logger.error(e);
				e.printStackTrace();
			}finally{
				
			}
			return SUCCESS;
		}
	
	 public String addGoogleArea() {

			GooglePlaceManager placeController=new GooglePlaceManager();
			Integer userId=(Integer) sessionMap.get("userId");
			try {
				
				GoogleArea area=new GoogleArea();
				long time = System.currentTimeMillis();
				java.util.Date date = new java.util.Date(time);
				Timestamp tsDate=new Timestamp(date.getTime());  
				
				area.setIsActive(true);
				area.setIsDeleted(false);
				area.setGoogleAreaName(getGoogleAreaName());
				area.setGoogleAreaDisplayName(getGoogleAreaDisplayName());
				area.setDescription(getDescription());
				area.setGoogleAreaLatitude(getGoogleAreaLatitude());
				area.setGoogleAreaLongitude(getGoogleAreaLongitude());
				area.setCreatedBy(userId);
				area.setCreatedOn(tsDate);
				area.setGoogleAreaUrl(getGoogleAreaUrl());
				area.setGoogleAreaPlaceId(getGoogleAreaPlaceId());
				GoogleLocation location=placeController.find(getGoogleLocationId());
				area.setGoogleLocation(location);
				GoogleArea areas = placeController.add(area);
				
				this.googleAreaId = areas.getGoogleAreaId();
				
				String jsonOutput = "";
				HttpServletResponse response = ServletActionContext.getResponse();
				response.setContentType("application/json");
				
				  if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += ",{";
					else
						jsonOutput += "{";
					jsonOutput += "\"googleAreaId\":\"" + this.googleAreaId  + "\"";
					
					jsonOutput += "}";

				response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
				
				
			} catch (Exception e) {
				logger.error(e);
				e.printStackTrace();
			} finally {

			}
			return null;
		}
	 
	 public String addGoogleAreaPicture() throws IOException {
			try {
				GooglePlaceManager googleAreaController=new GooglePlaceManager();
				//Location locations=new Location();
				File fileZip1 = new File(getText("storage.googlearea.photo") + "/"
						+ getGoogleAreaId() + "/", this.getMyFileFileName());
				FileUtils.copyFile(this.myFile, fileZip1);// copying image in the
				GoogleArea googleArea = googleAreaController.findArea(getGoogleAreaId()); 
				googleArea.setPhotoPath(this.getMyFileFileName());
				googleAreaController.edit(googleArea);
				
				
			} catch (Exception e) {
				logger.error(e);
				e.printStackTrace();
			} finally {

			}
			return SUCCESS;
		}
		
		 public String getGoogleAreaPicture() {
				
				//this.propertyId = (Integer) sessionMap.get("propertyId");
				HttpServletResponse response = ServletActionContext.getResponse();

				GooglePlaceManager googleLocationController = new GooglePlaceManager();
				GoogleArea googleLocation = googleLocationController.findArea(getGoogleAreaId());
				String imagePath = "";
				// response.setContentType("");
				try {
					
					if (getGoogleAreaId() >0 ) {
						imagePath = getText("storage.googlelocation.photo") + "/"
								+ googleLocation.getGoogleAreaId()+ "/"
								+ googleLocation.getPhotoPath();
						Image im = new Image();
						response.getOutputStream().write(im.getCustomImageInBytes(imagePath));
						response.getOutputStream().flush();
					} else {
						imagePath = getText("storage.path") + "/emptyprofilepic.png";
						Image im = new Image();
						response.getOutputStream().write(
								im.getCustomImageInBytes(imagePath));
						response.getOutputStream().flush();
					}
				} catch (Exception e1) {
					logger.error(e1);
					e1.printStackTrace();
				} finally {

				}
				return null;

			}


	public String getGoogleAreas() throws IOException {
				try {
					String jsonOutput = "";		
					HttpServletResponse response = ServletActionContext.getResponse();
					UserLoginManager userController=new UserLoginManager();
					
					GooglePlaceManager googlePlaceController = new GooglePlaceManager();
					response.setContentType("application/json");
					int serialno=1;
					this.googleAreaList = googlePlaceController.listAreas(limit,offset);
					if(googleAreaList.size()>0 && !googleAreaList.isEmpty()){
						for (GoogleArea googleArea : googleAreaList) {
							
							if (!jsonOutput.equalsIgnoreCase(""))
								jsonOutput += ",{";
							else
								jsonOutput += "{";
							
							jsonOutput += "\"googleAreaId\":\"" + googleArea.getGoogleAreaId()+ "\"";
							jsonOutput += ",\"googleAreaName\":\"" + googleArea.getGoogleAreaName()+ "\"";
							jsonOutput += ",\"googleAreaDisplayName\":\"" + googleArea.getGoogleAreaDisplayName()+ "\"";
							jsonOutput += ",\"googleAreaUrl\":\"" + googleArea.getGoogleAreaUrl()+ "\"";
							jsonOutput += ",\"googleLocationName\":\"" + googleArea.getGoogleLocation().getGoogleLocationName()+ "\"";
							jsonOutput += ",\"serialNo\":\"" + serialno + "\"";
							if(googleArea.getCreatedBy()!=null){
								User user=userController.find(googleArea.getCreatedBy());
								String strFirstName=user.getUserName().trim();
								String firstNameLetterUpperCase=strFirstName.substring(0, 1).toUpperCase()+strFirstName.substring(1);
								jsonOutput += ",\"createdBy\":\"" + firstNameLetterUpperCase+ "\"";
							}else{
								jsonOutput += ",\"createdBy\":\"" + " "+ "\"";
							}
							
							
							jsonOutput += "}";
							
							serialno++;
						}
					}else{
						if (!jsonOutput.equalsIgnoreCase(""))
							jsonOutput += ",{";
						else
							jsonOutput += "{";
						
						jsonOutput += "\"locationId\":\"" + ""+ "\"";
						jsonOutput += ",\"locationName\":\"" + ""+ "\"";
						jsonOutput += ",\"serialNo\":\"" + "" + "\"";
						jsonOutput += ",\"createdBy\":\"" + ""+ "\"";
						jsonOutput += "}";
					}
					

					response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
					

				} catch (Exception e) {
					logger.error(e);
					e.printStackTrace();
				} finally {

				}

				return null;

	}
	
	public String getGoogleAllAreas() throws IOException {
		try {
			String jsonOutput = "";		
			HttpServletResponse response = ServletActionContext.getResponse();
			UserLoginManager userController=new UserLoginManager();
			
			GooglePlaceManager googlePlaceController = new GooglePlaceManager();
			response.setContentType("application/json");
			int serialno=1;
			this.googleAreaList = googlePlaceController.listArea();
			if(googleAreaList.size()>0 && !googleAreaList.isEmpty()){
				for (GoogleArea googleArea : googleAreaList) {
					
					if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += ",{";
					else
						jsonOutput += "{";
					
					jsonOutput += "\"googleAreaId\":\"" + googleArea.getGoogleAreaId()+ "\"";
					jsonOutput += ",\"googleAreaName\":\"" + googleArea.getGoogleAreaName()+ "\"";
					jsonOutput += ",\"googleAreaDisplayName\":\"" + googleArea.getGoogleAreaDisplayName()+ "\"";
					jsonOutput += ",\"googleAreaUrl\":\"" + googleArea.getGoogleAreaUrl()+ "\"";
					jsonOutput += ",\"googleLocationName\":\"" + googleArea.getGoogleLocation().getGoogleLocationName()+ "\"";
					jsonOutput += ",\"serialNo\":\"" + serialno + "\"";
					if(googleArea.getCreatedBy()!=null){
						User user=userController.find(googleArea.getCreatedBy());
						String strFirstName=user.getUserName().trim();
						String firstNameLetterUpperCase=strFirstName.substring(0, 1).toUpperCase()+strFirstName.substring(1);
						jsonOutput += ",\"createdBy\":\"" + firstNameLetterUpperCase+ "\"";
					}else{
						jsonOutput += ",\"createdBy\":\"" + " "+ "\"";
					}
					
					
					jsonOutput += "}";
					
					serialno++;
				}
			}else{
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				
				jsonOutput += "\"locationId\":\"" + ""+ "\"";
				jsonOutput += ",\"locationName\":\"" + ""+ "\"";
				jsonOutput += ",\"serialNo\":\"" + "" + "\"";
				jsonOutput += ",\"createdBy\":\"" + ""+ "\"";
				jsonOutput += "}";
			}
			

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

}

	public String getGoogleAreaProperties(){
		try{

			String jsonOutput = "";		
			HttpServletResponse response = ServletActionContext.getResponse();
			
			GooglePlaceManager placeController = new GooglePlaceManager();
			response.setContentType("application/json");
			
			List<GooglePropertyAreas> listPropertyAreas=placeController.listPropertyArea(getGoogleAreaId());
			if(listPropertyAreas.size()>0 && !listPropertyAreas.isEmpty()){
				for(GooglePropertyAreas googlePropertyareas:listPropertyAreas){
					if(googlePropertyareas.getPmsProperty()!=null){
						if (!jsonOutput.equalsIgnoreCase(""))
							jsonOutput += ",{";
						else
							jsonOutput += "{";
						
						jsonOutput += "\"propertyId\":\"" + googlePropertyareas.getPmsProperty().getPropertyId()+ "\"";
						jsonOutput += ",\"propertyName\":\"" + googlePropertyareas.getPmsProperty().getPropertyName()+ "\"";

						jsonOutput += "}";	
					}
				}
			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	public String getReportGoogleAreaProperties(){

		try{

			String jsonOutput = "";		
			HttpServletResponse response = ServletActionContext.getResponse();
			
			GooglePlaceManager placeController = new GooglePlaceManager();
			response.setContentType("application/json");
			
			List<GooglePropertyAreas> listPropertyAreas=placeController.listAreaProperties(getGoogleAreaId());
			if(listPropertyAreas.size()>0 && !listPropertyAreas.isEmpty()){
				for(GooglePropertyAreas googlePropertyareas:listPropertyAreas){
					if(googlePropertyareas.getPmsProperty()!=null){
						if (!jsonOutput.equalsIgnoreCase(""))
							jsonOutput += ",{";
						else
							jsonOutput += "{";
						
						jsonOutput += "\"propertyId\":\"" + googlePropertyareas.getPmsProperty().getPropertyId()+ "\"";
						jsonOutput += ",\"propertyName\":\"" + googlePropertyareas.getPmsProperty().getPropertyName()+ "\"";

						jsonOutput += "}";	
					}
				}
			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	
	}
	
	public String getGoogleAreaCount() throws IOException {
 		try {
 			String jsonOutput = "";
 			HttpServletResponse response = ServletActionContext.getResponse();
 			response.setContentType("application/json");
 			GooglePlaceManager placeController = new GooglePlaceManager();
 			List<GoogleArea> listAreaCount = placeController.listArea();
 			if(listAreaCount.size()>0 && !listAreaCount.isEmpty()){
 				if (!jsonOutput.equalsIgnoreCase(""))
 					jsonOutput += ",{";
 				else
 					jsonOutput += "{";
 				
 			    jsonOutput += "\"count\":\"" + listAreaCount.size() + "\"";
 			    
 				jsonOutput += "}";
 			}
 			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
 		} catch (Exception e) {
 			logger.error(e);
 			e.printStackTrace();
 		} finally {

 		}

 		return null;

 	}
	
	public String getGoogleArea() throws IOException {
		try {
			String jsonOutput = "",photoPath="";
			HttpServletResponse response = ServletActionContext.getResponse();
			GooglePlaceManager areaController = new GooglePlaceManager();
			response.setContentType("application/json");
			GoogleArea area = areaController.findArea(getGoogleAreaId());

			if(area!=null){
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				
				jsonOutput += "\"googleAreaId\":\"" + area.getGoogleAreaId()+ "\"";
				jsonOutput += ",\"googleAreaName\":\"" + area.getGoogleAreaName()+ "\"";
				jsonOutput += ",\"googleAreaDisplayName\":\"" + area.getGoogleAreaDisplayName()+ "\"";
				jsonOutput += ",\"googleAreaDescription\":\"" + area.getDescription()+ "\"";
				jsonOutput += ",\"googleAreaUrl\":\"" + area.getGoogleAreaUrl()+ "\"";
				jsonOutput += ",\"googleAreaLocationId\":\"" + area.getGoogleLocation().getGoogleLocationId()+ "\"";
				photoPath = getText("storage.aws.property.photo") + "/uloimg/area/"
						+ area.getGoogleAreaId()+"/"+area.getPhotoPath();
						
				jsonOutput += ",\"photoPath\":\"" + (photoPath)+ "\"";
						
				jsonOutput += "}";
	
			}
			
			
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	public String getGoogleUrl() throws IOException {
		//this.areaId = (Integer) sessionMap.get("areaId");
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			AreaManager areaController=new AreaManager();
			GooglePlaceManager googleController = new GooglePlaceManager();
			UserLoginManager userController=new UserLoginManager();
			
			response.setContentType("application/json");
			int serialno=1;
			this.googleLocationList = googleController.list();
			for (GoogleLocation googleLocation : googleLocationList) {
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				
				jsonOutput += "\"DT_RowId\":\"" + googleLocation.getGoogleLocationId()+ "\"";
				jsonOutput += ",\"googleLocationName\":\"" + googleLocation.getGoogleLocationName()+ "\"";
				jsonOutput += ",\"googleRedirectUrl\":\"" + googleLocation.getGoogleLocationUrl()+ "\"";
				jsonOutput += ",\"type\":\"" + "Location"+ "\"";
				if(googleLocation.getCreatedBy()!=null){
					User user=userController.find(googleLocation.getCreatedBy());
					String strFirstName=user.getUserName().trim();
					String firstNameLetterUpperCase=strFirstName.substring(0, 1).toUpperCase()+strFirstName.substring(1);
					jsonOutput += ",\"createdBy\":\"" + firstNameLetterUpperCase+ "\"";
				}else{
					jsonOutput += ",\"createdBy\":\"" + " "+ "\"";
				}
				jsonOutput += "}";
				
			}
			
			this.googleAreaList = googleController.listArea();
			for (GoogleArea googleArea : googleAreaList) {

				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"DT_RowId\":\"" + googleArea.getGoogleAreaId()+ "\"";
				jsonOutput += ",\"serialNo\":\"" + serialno+ "\"";
				jsonOutput += ",\"googleAreaName\":\"" + (googleArea.getGoogleAreaName() == null ? "": googleArea.getGoogleAreaName().toUpperCase().trim())+ "\"";
				jsonOutput += ",\"googleRedirectUrl\":\"" + googleArea.getGoogleAreaUrl().trim() + "\"";
				jsonOutput += ",\"locationId\":\"" + (googleArea.getGoogleLocation().getGoogleLocationId() == null ? "": googleArea.getGoogleLocation().getGoogleLocationId())+ "\"";
				jsonOutput += ",\"areaLocationName\":\"" + (googleArea.getGoogleLocation().getGoogleLocationName() == null ? "": googleArea.getGoogleLocation().getGoogleLocationName().toUpperCase().trim())+ "\"";
				if(googleArea.getCreatedBy()!=null){
					User user=userController.find(googleArea.getCreatedBy());
					String strFirstName=user.getUserName().trim();
					String firstNameLetterUpperCase=strFirstName.substring(0, 1).toUpperCase()+strFirstName.substring(1);
					jsonOutput += ",\"createdBy\":\"" + firstNameLetterUpperCase+ "\"";
				}else{
					jsonOutput += ",\"createdBy\":\"" + " "+ "\"";
				}
				
				jsonOutput += "}";

				serialno++;
			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	public String getGoogleInactiveAreas() throws IOException {
		//this.areaId = (Integer) sessionMap.get("areaId");
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			GooglePlaceManager areaController=new GooglePlaceManager();
			UserLoginManager userController=new UserLoginManager();
			
			response.setContentType("application/json");
			int serialno=1;
			this.googleAreaList = areaController.listInactiveArea(limit,offset);
			for (GoogleArea area : googleAreaList) {

				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"DT_RowId\":\"" + area.getGoogleAreaId()+ "\"";
				jsonOutput += ",\"serialNo\":\"" + serialno+ "\"";
				jsonOutput += ",\"areaName\":\"" + (area.getGoogleAreaName() == null ? "": area.getGoogleAreaName().trim())+ "\"";
				if(area.getGoogleLocation()!=null){
					jsonOutput += ",\"locationId\":\"" + (area.getGoogleLocation().getGoogleLocationId() == null ? "": area.getGoogleLocation().getGoogleLocationId())+ "\"";
					jsonOutput += ",\"areaLocationName\":\"" + (area.getGoogleLocation().getGoogleLocationName() == null ? "": area.getGoogleLocation().getGoogleLocationName().trim())+ "\"";
				}else{
					jsonOutput += ",\"locationId\":\"" + ""+ "\"";
					jsonOutput += ",\"areaLocationName\":\"" + ""+ "\"";
				}
				
				if(area.getCreatedBy()!=null){
					User user=userController.find(area.getCreatedBy());
					String strFirstName=user.getUserName().trim();
					String firstNameLetterUpperCase=strFirstName.substring(0, 1).toUpperCase()+strFirstName.substring(1);
					jsonOutput += ",\"createdBy\":\"" + firstNameLetterUpperCase+ "\"";
				}else{
					jsonOutput += ",\"createdBy\":\"" +" "+ "\"";
				}
				
				jsonOutput += "}";

				serialno++;
			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	
	public String getGoogleInactiveAreaCount() throws IOException {
 		try {
 			String jsonOutput = "";
 			HttpServletResponse response = ServletActionContext.getResponse();
 			response.setContentType("application/json");
 			GooglePlaceManager areaController = new GooglePlaceManager();
 			List<GoogleArea> listAreaCount =areaController.listInactivaArea();
 			if(listAreaCount.size()>0 && !listAreaCount.isEmpty()){
 				if (!jsonOutput.equalsIgnoreCase(""))
 					jsonOutput += ",{";
 				else
 					jsonOutput += "{";
 				
 			    jsonOutput += "\"count\":\"" + listAreaCount.size() + "\"";
 			    
 				jsonOutput += "}";
 			}
 			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
 		} catch (Exception e) {
 			logger.error(e);
 			e.printStackTrace();
 		} finally {

 		}

 		return null;

 	}

	public String getGoogleInactiveArea() throws IOException {
		try {
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			GooglePlaceManager areaController = new GooglePlaceManager();
			response.setContentType("application/json");
			GoogleArea area = areaController.findArea(getGoogleAreaId());

			if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				
				jsonOutput += "\"googleAreaId\":\"" + area.getGoogleAreaId()+ "\"";
				jsonOutput += ",\"googleAreaName\":\"" + area.getGoogleAreaName()+ "\"";
				jsonOutput += ",\"googleAreaDisplayName\":\"" + area.getGoogleAreaDisplayName()+ "\"";
				jsonOutput += ",\"googleAreaDescription\":\"" + area.getDescription()+ "\"";
				jsonOutput += ",\"googleAreaUrl\":\"" + area.getGoogleAreaUrl()+ "\"";
				jsonOutput += ",\"googleAreaRedirectUrl\":\"" + area.getGoogleRedirectUrl()+ "\"";
				jsonOutput += ",\"googleAreaLocationId\":\"" + area.getGoogleLocation().getGoogleLocationId()+ "\"";
				jsonOutput += ",\"googleAreaphotoPath\":\"" + area.getPhotoPath()+ "\"";
				
				jsonOutput += "}";

			
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	public String editGoogleInactiveArea() throws IOException {
		try {
			Integer userId=(Integer) sessionMap.get("userId");
			
			long time = System.currentTimeMillis();
			java.util.Date date = new java.util.Date(time);
			Timestamp tsDate=new Timestamp(date.getTime());
			
			GooglePlaceManager googleController=new GooglePlaceManager();
			GoogleArea area = googleController.findArea(getGoogleAreaId());
			area.setIsActive(getIsActive());
			area.setIsDeleted(false);
			area.setGoogleAreaName(getGoogleAreaName());
			area.setGoogleAreaDisplayName(getGoogleAreaDisplayName());
			area.setDescription(getDescription());
			area.setGoogleAreaUrl(getGoogleAreaUrl());
			area.setGoogleRedirectUrl(getGoogleRedirectUrl());
			area.setCreatedBy(userId);
			area.setCreatedOn(tsDate);
			googleController.edit(area);
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return SUCCESS;
	}
	public String getReportLocationAreas(){

		
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			GooglePlaceManager areaController=new GooglePlaceManager();
			
			response.setContentType("application/json");

			this.googleAreaList = areaController.listGoogleLocationArea(getAreaLocationId());
			for (GoogleArea area : googleAreaList) {

				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"googleAreaId\":\"" + area.getGoogleAreaId()+ "\"";
				jsonOutput += ",\"googleAreaDisplayName\":\"" + area.getGoogleAreaDisplayName().toUpperCase().trim() + "\"";
				jsonOutput += ",\"googleAreaName\":\"" + area.getGoogleAreaName().toUpperCase().trim() + "\"";
				jsonOutput += ",\"areaDescription\":\"" + (area.getDescription()==null?"":area.getDescription().trim().toUpperCase()) + "\"";
				jsonOutput += ",\"googleAreaUrl\":\"" + area.getGoogleAreaUrl().trim() + "\"";
				jsonOutput += "}";


			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	
	}
	public String getGoogleLocationAreas() throws IOException {
		
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			GooglePlaceManager areaController=new GooglePlaceManager();
			
			response.setContentType("application/json");

			this.googleAreaList = areaController.listGoogleLocationArea(getAreaLocationId());
			for (GoogleArea area : googleAreaList) {

				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"DT_RowId\":\"" + area.getGoogleAreaId()+ "\"";
				jsonOutput += ",\"googleAreaDisplayName\":\"" + area.getGoogleAreaDisplayName().toUpperCase().trim() + "\"";
				jsonOutput += ",\"googleAreaName\":\"" + area.getGoogleAreaName().toUpperCase().trim() + "\"";
				jsonOutput += ",\"areaDescription\":\"" + (area.getDescription()==null?"":area.getDescription().trim().toUpperCase()) + "\"";
				jsonOutput += ",\"googleAreaUrl\":\"" + area.getGoogleAreaUrl().trim() + "\"";
				jsonOutput += "}";


			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}

public String addNewGoogleProperty() {
	try {
		this.propertyId = (Integer) sessionMap.get("propertyId");
		PmsPropertyManager propertyController = new PmsPropertyManager();
		GooglePlaceManager placeController=new GooglePlaceManager();
		LocationManager locationController = new LocationManager();
		PropertyAreasManager propertyAreaController=new PropertyAreasManager();
		String[] stringArray = getCheckedAreas().split("\\s*,\\s*");
	    int[] intArray = new int[stringArray.length];
	    if(getCheckedAreas()!=null && !getCheckedAreas().trim().isEmpty()){
	    	for (int i = 0; i < stringArray.length; i++) {
			     String numberAsString = stringArray[i];
			     intArray[i] = Integer.parseInt(numberAsString);
		    }
		}
	    
	    GooglePropertyAreas propertyArea=new GooglePropertyAreas();
	    
		Integer userId=(Integer) sessionMap.get("userId");
		PmsProperty property = new PmsProperty();
		long time = System.currentTimeMillis();
		java.util.Date date = new java.util.Date(time);
		Timestamp tsDate=new Timestamp(date.getTime());  
		
		property.setIsActive(true);
		property.setIsDeleted(false);
		property.setTaxValidFrom(tsDate);
		property.setTaxValidTo(tsDate);
		property.setTaxIsActive(true);
		//property.setHotelIsActive(true);
		property.setPropertyIsControl(false);
		property.setPropertyName(getPropertyName());
		property.setPropertyUrl(getPropertyUrl());
		property.setCreatedBy(userId);
		property.setCreatedDate(tsDate);
		GoogleLocation location=placeController.find(getGoogleLocationId());
		property.setGoogleLocation(location);
		LocationType locationType = locationController.findType(getLocationTypeId());
		property.setLocationType(locationType);
		PmsProperty pmsProperty = propertyController.add(property);
		
		this.propertyId = pmsProperty.getPropertyId();
		
		UlotelHotelDetailTags tags = new UlotelHotelDetailTags();
		
		tags.setHotelProfile("active");
		tags.setRoomCategories("disable");
		tags.setRatePlan("disable");
		tags.setHotelAmenities("disable");
		tags.setHotelPolicy("disable");
		tags.setAddOn("disable");
		tags.setHotelLandmark("disable");
		tags.setHotelStatus("disable");
		tags.setHotelImages("disable");
		tags.setPmsProperty(pmsProperty);
		propertyController.edit(tags);
		
		String jsonOutput = "";
		HttpServletResponse response = ServletActionContext.getResponse();
		response.setContentType("application/json");
		
		  if (!jsonOutput.equalsIgnoreCase(""))
				jsonOutput += ",{";
			else
				jsonOutput += "{";
			jsonOutput += "\"locationPropertyId\":\"" + this.propertyId  + "\"";
			
			jsonOutput += "}";


			
			if(getCheckedAreas()!=null && !getCheckedAreas().trim().isEmpty()){
				
				List<GooglePropertyAreas> listPropertyAreas=placeController.listArea(getPropertyId());
				if(listPropertyAreas.size()>0 && !listPropertyAreas.isEmpty()){
					for(GooglePropertyAreas googlePropertyareas:listPropertyAreas){
						GooglePropertyAreas propertyAreas=placeController.findProperty(googlePropertyareas.getGooglePropertyAreaId());
						propertyAreas.setIsActive(false);
						propertyAreas.setIsDeleted(true);
						propertyArea.setPropertyIsControl(true);
						propertyAreas.setCreatedBy(userId);
						propertyAreas.setCreatedDate(tsDate);
						placeController.edit(propertyAreas);
					}
				}
				
				for (int googleAreaId : intArray) {
					 propertyArea.setIsActive(true);
					 propertyArea.setIsDeleted(false);
					 propertyArea.setCreatedBy(userId);
					 propertyArea.setCreatedDate(tsDate);
					 GoogleArea area=placeController.findArea(googleAreaId);
					 propertyArea.setGoogleArea(area);
					 propertyArea.setPropertyIsControl(true);
					 PmsProperty pmspropery=propertyController.find(getPropertyId());
					 propertyArea.setPmsProperty(pmspropery);
					 placeController.add(propertyArea);
				 }	
			}

		response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
		
		
	} catch (Exception e) {
		logger.error(e);
		e.printStackTrace();
	} finally {

	}
	return null;
}

   
  public String getAllGoogleProperties() throws IOException {
	this.propertyId = (Integer) sessionMap.get("propertyId");
	try {

		String jsonOutput = "";
		HttpServletResponse response = ServletActionContext.getResponse();
	
		
		PmsPropertyManager propertyController = new PmsPropertyManager();
		UserLoginManager userController=new UserLoginManager();
		response.setContentType("application/json");
		int serialno=1;
		List <PmsProperty> propertyList = propertyController.list(limit,offset);
		for (PmsProperty property : propertyList) {

			
			if (!jsonOutput.equalsIgnoreCase(""))
				jsonOutput += ",{";
			else
				jsonOutput += "{";
			jsonOutput += "\"DT_RowId\":\"" + property.getPropertyId()+ "\"";
			jsonOutput += ",\"serialNo\":\"" + serialno+ "\"";
			jsonOutput += ",\"propertyName\":\"" + (property.getPropertyName() == null ? "": property.getPropertyName().trim())+ "\"";
			jsonOutput += ",\"propertyUrl\":\"" + (property.getPropertyUrl() == null ? "": property.getPropertyUrl().trim())+ "\"";
			jsonOutput += ",\"googleLocationId\":\"" + (property.getGoogleLocation() == null ? "": property.getGoogleLocation().getGoogleLocationId())+ "\"";
			jsonOutput += ",\"googleLocationName\":\"" + (property.getGoogleLocation() == null ? "": property.getGoogleLocation().getGoogleLocationName())+ "\"";
			if(property.getHotelIsActive() == null || property.getHotelIsActive() == false){
		    jsonOutput += ",\"isActive\":\"" + "red" + "\"";	
			}else if(property.getHotelIsActive() == true){
		    jsonOutput += ",\"isActive\":\"" + "green" + "\"";	
			}
			if(property.getCreatedBy()!=null){
				User user=userController.find(property.getCreatedBy());
				String strFirstName=user.getUserName().trim();
				String firstNameLetterUpperCase=strFirstName.substring(0, 1).toUpperCase()+strFirstName.substring(1);
				jsonOutput += ",\"createdBy\":\"" + firstNameLetterUpperCase+ "\"";
			}else{
				jsonOutput += ",\"createdBy\":\"" +" "+ "\"";
			}
			
			
			jsonOutput += "}";

			serialno++;
		}

		response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
		

	} catch (Exception e) {
		logger.error(e);
		e.printStackTrace();
	} finally {

	}

	return null;

}
  public String getUloGoogleLocations() throws IOException {
		try {
			String jsonOutput = "";		
			HttpServletResponse response = ServletActionContext.getResponse();
			UserLoginManager userController=new UserLoginManager();
			
			GooglePlaceManager googlePlaceController = new GooglePlaceManager();
			response.setContentType("application/json");
			int serialno=1;
			this.googleLocationList = googlePlaceController.list();
			if(googleLocationList.size()>0 && !googleLocationList.isEmpty()){
				for (GoogleLocation googleLocation : googleLocationList) {
					
					if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += ",{";
					else
						jsonOutput += "{";
					
					jsonOutput += "\"googleLocationId\":\"" + googleLocation.getGoogleLocationId()+ "\"";
					jsonOutput += ",\"googleLocationName\":\"" + googleLocation.getGoogleLocationName()+ "\"";
					jsonOutput += ",\"googleLocationDisplayName\":\"" + googleLocation.getGoogleLocationDisplayName().toUpperCase().trim()+ "\"";
					jsonOutput += ",\"googleLocationUrl\":\"" + googleLocation.getGoogleLocationUrl()+ "\"";
					jsonOutput += ",\"serialNo\":\"" + serialno + "\"";
					if(googleLocation.getCreatedBy()!=null){
						User user=userController.find(googleLocation.getCreatedBy());
						String strFirstName=user.getUserName().trim();
						String firstNameLetterUpperCase=strFirstName.substring(0, 1).toUpperCase()+strFirstName.substring(1);
						jsonOutput += ",\"createdBy\":\"" + firstNameLetterUpperCase+ "\"";
					}else{
						jsonOutput += ",\"createdBy\":\"" + " "+ "\"";
					}
					
					
					jsonOutput += "}";
					
					serialno++;
				}
			}
			

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}

	public String getUloGoogleAreas() throws IOException {
		try {
			String jsonOutput = "";		
			HttpServletResponse response = ServletActionContext.getResponse();
			UserLoginManager userController=new UserLoginManager();
			
			GooglePlaceManager googlePlaceController = new GooglePlaceManager();
			response.setContentType("application/json");
			int serialno=1;
			this.googleAreaList = googlePlaceController.listAreas();
			if(googleAreaList.size()>0 && !googleAreaList.isEmpty()){
				for (GoogleArea googleArea : googleAreaList) {
					
					if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += ",{";
					else
						jsonOutput += "{";
					
					jsonOutput += "\"googleAreaId\":\"" + googleArea.getGoogleAreaId()+ "\"";
					jsonOutput += ",\"googleAreaName\":\"" + googleArea.getGoogleAreaName()+ "\"";
					jsonOutput += ",\"googleAreaDisplayName\":\"" + googleArea.getGoogleAreaDisplayName().toUpperCase().trim()+ "\"";
					jsonOutput += ",\"googleAreaUrl\":\"" + googleArea.getGoogleAreaUrl()+ "\"";
					jsonOutput += ",\"googleLocationName\":\"" + googleArea.getGoogleLocation().getGoogleLocationName().toUpperCase().trim()+ "\"";
					jsonOutput += ",\"serialNo\":\"" + serialno + "\"";
					if(googleArea.getCreatedBy()!=null){
						User user=userController.find(googleArea.getCreatedBy());
						String strFirstName=user.getUserName().trim();
						String firstNameLetterUpperCase=strFirstName.substring(0, 1).toUpperCase()+strFirstName.substring(1);
						jsonOutput += ",\"createdBy\":\"" + firstNameLetterUpperCase+ "\"";
					}else{
						jsonOutput += ",\"createdBy\":\"" + " "+ "\"";
					}
					
					
					jsonOutput += "}";
					
					serialno++;
				}
			}
			

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	public String getPropertyDetail() throws IOException {
		try {
			String jsonOutput = "",jsonAreas="";
			
			HttpServletResponse response = ServletActionContext.getResponse();
			PmsPropertyManager pmsPropertyController = new PmsPropertyManager();
			AreaManager areaController=new AreaManager();
			GooglePlaceManager placeController = new GooglePlaceManager();
			PropertyAreasManager propertyAreasController=new PropertyAreasManager();
			PropertyAreas propertyAreas=new PropertyAreas();
			String photoPath="";
			response.setContentType("application/json");
			List<GoogleArea> listAreas=null;
			List<GooglePropertyAreas> listPropertyAreas=null;
			List <PmsProperty> propertyList = pmsPropertyController.listGooglePlaceProperty(getPropertyId());
			for (PmsProperty property : propertyList) {
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				
				jsonOutput += "\"propertyId\":\"" + property.getPropertyId()+ "\"";
				jsonOutput += ",\"propertyName\":\"" + property.getPropertyName()+ "\"";
				jsonOutput += ",\"googleLocationId\":\"" +(property.getGoogleLocation() == null ? "":property.getGoogleLocation().getGoogleLocationId())+ "\"";
				jsonOutput += ",\"propertyUrl\":\"" + (property.getPropertyUrl() == null ? "": property.getPropertyUrl().trim())+ "\"";
				jsonOutput += ",\"locationTypeId\":\"" + (property.getLocationType() == null ? "":property.getLocationType().getLocationTypeId())+ "\"";
				jsonOutput += ",\"locationTypeName\":\"" + (property.getLocationType() == null ? "":property.getLocationType().getLocationTypeName())+ "\"";
				
				photoPath = getText("storage.aws.property.photo") + "/uloimg/property/"
				+ property.getPropertyId()+"/"+property.getPropertyThumbPath();
				
				jsonOutput += ",\"photoPath\":\"" + (photoPath)+ "\"";
				if(property.getGoogleLocation() != null){
				listAreas=placeController.listGoogleLocationArea(property.getGoogleLocation().getGoogleLocationId());
				if(listAreas.size()>0 && !listAreas.isEmpty()){
					for(GoogleArea areas:listAreas){
						if (!jsonAreas.equalsIgnoreCase(""))
							
							jsonAreas += ",{";
						else
							jsonAreas += "{";	
						
						GoogleArea area=placeController.findArea(areas.getGoogleAreaId());
						jsonAreas += "\"googleAreaId\":\"" + area.getGoogleAreaId()+ "\"";
						jsonAreas += ",\"googleAreaName\":\"" + area.getGoogleAreaName().toUpperCase().trim()+ "\"";
						jsonAreas += ",\"googleAreaDisplayName\":\"" + area.getGoogleAreaDisplayName().toUpperCase().trim()+ "\"";
						jsonAreas += ",\"description\":\"" +(area.getDescription()==null?" ":area.getDescription().toUpperCase().trim())+ "\"";
						listPropertyAreas=placeController.listPropertyArea(property.getPropertyId(),areas.getGoogleAreaId());
						if(listPropertyAreas.size()>0 && !listPropertyAreas.isEmpty()){
							for(GooglePropertyAreas propertyareas:listPropertyAreas){
								int areaId=propertyareas.getGoogleArea().getGoogleAreaId();
								if(areaId==(int)area.getGoogleAreaId()){
									jsonAreas += ",\"status\":\"" + true + "\"";
								}
							}
						}else{
							jsonAreas += ",\"status\":\"" + false + "\"";
						}
						
						jsonAreas += "}";
			        
					}
			        
				}
				}
				  
                jsonOutput += ",\"areas\":[" + jsonAreas+ "]";
				
				
				jsonOutput += "}";

			}
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	public String editGoogleProperty() throws IOException {
		try {
			Integer userId=(Integer) sessionMap.get("userId");
			LocationManager locationController = new LocationManager();
			String[] stringArray = getCheckedAreas().split("\\s*,\\s*");
			
		    int[] intArray = new int[stringArray.length];
			if(getCheckedAreas()!=null && !getCheckedAreas().trim().isEmpty()){
			    
			    for (int i = 0; i < stringArray.length; i++) {
			     String numberAsString = stringArray[i];
			     intArray[i] = Integer.parseInt(numberAsString);
			    }
			}
			
		    long time = System.currentTimeMillis();
			java.util.Date date = new java.util.Date(time);
			Timestamp tsDate=new Timestamp(date.getTime());  
			
			GooglePropertyAreas propertyArea=new GooglePropertyAreas();
			
			
			PmsPropertyManager propertyController = new PmsPropertyManager();
			GooglePlaceManager placeController=new GooglePlaceManager();
			PmsProperty property = propertyController.find(getPropertyId());
			property.setIsActive(true);
			property.setIsDeleted(false);
			property.setPropertyUrl(getPropertyUrl());
			property.setPropertyIsControl(true);
			property.setModifiedBy(userId);
			property.setModifiedDate(tsDate);
			GoogleLocation location=placeController.find(getGoogleLocationId());
			property.setGoogleLocation(location);
			LocationType locationType = locationController.findType(getLocationTypeId());
			property.setLocationType(locationType);
			propertyController.edit(property);
			
			UlotelHotelDetailTags tag = propertyController.findHotelTag(getPropertyId()) ;
			if(tag == null){
			UlotelHotelDetailTags tags = new UlotelHotelDetailTags();	
			tags.setHotelProfile("active");
			tags.setRoomCategories("disable");
			tags.setRatePlan("disable");
			tags.setHotelAmenities("disable");
			tags.setHotelPolicy("disable");
			tags.setAddOn("disable");
			tags.setHotelLandmark("disable");
			tags.setHotelStatus("disable");
			tags.setHotelImages("disable");
			tags.setPmsProperty(property);
			propertyController.edit(tags);
			}
			
			
			if( getCheckedAreas()!=null && !getCheckedAreas().trim().isEmpty()){
				
				
				List<GooglePropertyAreas> listPropertyAreas=placeController.listArea(getPropertyId());
				if(listPropertyAreas.size()>0 && !listPropertyAreas.isEmpty()){
					for(GooglePropertyAreas propertyareas:listPropertyAreas){
						GooglePropertyAreas propertyAreas=placeController.findProperty(propertyareas.getGooglePropertyAreaId());
						propertyAreas.setIsActive(false);
						propertyAreas.setIsDeleted(true);
						propertyArea.setPropertyIsControl(true);
						propertyAreas.setModifiedBy(userId);
						propertyAreas.setModifiedDate(tsDate);
						placeController.edit(propertyAreas);
					}
				}
				
				
				for (int areaId : intArray) {
					 propertyArea.setIsActive(true);
					 propertyArea.setIsDeleted(false);
					 propertyArea.setModifiedBy(userId);
					 propertyArea.setModifiedDate(tsDate);
					 propertyArea.setPropertyIsControl(true);
					 GoogleArea area=placeController.findArea(areaId);
					 propertyArea.setGoogleArea(area);
					 
					 PmsProperty pmspropery=propertyController.find(getPropertyId());
					 propertyArea.setPmsProperty(pmspropery);
					 placeController.add(propertyArea);
				 }
			}
			
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return SUCCESS;
	}

	public String getGoogleLocationArea() throws IOException {
		String output=null;
		try {
			Algorithm algorithm = Algorithm.HMAC256("secret");
			int count=0;
			long nowMillis = System.currentTimeMillis();
		    Date now = new Date(nowMillis+24*60*60*1000);
		    
			String jsonOutput = "",type="";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			GooglePlaceManager googlePlaceController = new GooglePlaceManager();
			
			response.setContentType("application/json");
			this.googleLocationList = googlePlaceController.list();
			if(this.googleLocationList.size()>0 && !this.googleLocationList.isEmpty()){
				for (GoogleLocation googleLocation : googleLocationList) {

					type="location";
					if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += ",{";
					else
						jsonOutput += "{";
					
					jsonOutput += "\"locationId\":\"" + googleLocation.getGoogleLocationId()+ "\"";
					jsonOutput += ",\"locationName\":\"" + googleLocation.getGoogleLocationName()+ "\"";
//					jsonOutput += ",\"locationDisplayName\":\"" + googleLocation.getGoogleLocationDisplayName()+ "\"";
					jsonOutput += ",\"locationUrl\":\"" + googleLocation.getGoogleLocationUrl()+ "\"";
					jsonOutput += ",\"locationType\":\"" + type+ "\"";
//					jsonOutput += ",\"placeId\":\"" + googleLocation.getGoogleLocationPlaceId()+ "\"";
					jsonOutput += "}";
					count++;
				}	
			}
			

			this.googleAreaList=googlePlaceController.listAreas();
			if(this.googleAreaList.size()>0 && !this.googleAreaList.isEmpty()){
				for(GoogleArea area:this.googleAreaList){
					type="area";
					if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += ",{";
					else
						jsonOutput += "{";
					
					jsonOutput += "\"locationId\":\"" + area.getGoogleAreaId()+ "\"";
					jsonOutput += ",\"locationName\":\"" + area.getGoogleAreaName()+ "\"";
//					jsonOutput += ",\"locationDisplayName\":\"" + area.getGoogleAreaDisplayName()+ "\"";
					jsonOutput += ",\"locationUrl\":\"" + area.getGoogleAreaUrl()+ "\"";
					jsonOutput += ",\"locationType\":\"" + type+ "\"";
//					jsonOutput += ",\"locationPlaceId\":\"" + area.getGoogleAreaPlaceId()+ "\"";
					jsonOutput += "}";
					count++;
				}
			}
			
			String data="\"data\":[" + jsonOutput + "]";
 			String strData=data.replaceAll("\"", "\"");
 			/*String token = JWT.create()
 					.withClaim("data", strData)
 					.withExpiresAt(now)
 					.withIssuer("auth0")
 					.sign(algorithm);
 			output = "\"token\":\"" + token+ "\"";
 			String strOutput=output.replaceAll("\"", "\"");
 			*/
 			
 			if(count == 0){
 				output="{\"error\":\"1\",\"message\":\"No Records Found\"}";
			}else{
				output="{\"error\":\"0\"," + strData + "}";
			}
 			
 			
		} catch (Exception e) {
			output="{\"error\":\"2\",\"message\":\"Some technical error on google location and area api\"}";
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return output;

	}
	
	public String getLocationAreas(){

		try {
			String jsonOutput = "",type="";
			HttpServletResponse response = ServletActionContext.getResponse();
			GooglePlaceManager googlePlaceController = new GooglePlaceManager();
			
			response.setContentType("application/json");
			this.googleLocationList = googlePlaceController.list();
			if(this.googleLocationList.size()>0 && !this.googleLocationList.isEmpty()){
				for (GoogleLocation googleLocation : googleLocationList) {

					type="location";
					if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += ",{";
					else
						jsonOutput += "{";
					
					jsonOutput += "\"locationId\":\"" + googleLocation.getGoogleLocationId()+ "\"";
					jsonOutput += ",\"locationName\":\"" + googleLocation.getGoogleLocationName()+ "\"";
					jsonOutput += ",\"locationDisplayName\":\"" + googleLocation.getGoogleLocationDisplayName()+ "\"";
					jsonOutput += ",\"locationUrl\":\"" + googleLocation.getGoogleLocationUrl()+ "\"";
					jsonOutput += ",\"locationType\":\"" + type+ "\"";
					jsonOutput += "}";
				}	
			}
			

			this.googleAreaList=googlePlaceController.listAreas();
			if(this.googleAreaList.size()>0 && !this.googleAreaList.isEmpty()){
				for(GoogleArea area:this.googleAreaList){
					type="area";
					if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += ",{";
					else
						jsonOutput += "{";
					
					jsonOutput += "\"locationId\":\"" + area.getGoogleAreaId()+ "\"";
					jsonOutput += ",\"locationName\":\"" + area.getGoogleAreaName()+ "\"";
					jsonOutput += ",\"locationDisplayName\":\"" + area.getGoogleAreaDisplayName()+ "\"";
					jsonOutput += ",\"locationUrl\":\"" + area.getGoogleAreaUrl()+ "\"";
					jsonOutput += ",\"locationType\":\"" + type+ "\"";
					jsonOutput += "}";
				}
			}
			
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;
	}
	
	public Integer getGoogleLocationId() {
		return googleLocationId;
	}

	public void setGoogleLocationId(Integer googleLocationId) {
		this.googleLocationId = googleLocationId;
	}

	public Integer getGoogleAreaId() {
		return googleAreaId;
	}

	public void setGoogleAreaId(Integer googleAreaId) {
		this.googleAreaId = googleAreaId;
	}

	public Integer getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(Integer propertyId) {
		this.propertyId = propertyId;
	}

	public Integer getLocationTypeId() {
		return locationTypeId;
	}

	public void setLocationTypeId(Integer locationTypeId) {
		this.locationTypeId = locationTypeId;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public String getPropertyUrl() {
		return propertyUrl;
	}

	public void setPropertyUrl(String propertyUrl) {
		this.propertyUrl = propertyUrl;
	}

	public String getCheckedAreas() {
		return checkedAreas;
	}

	public void setCheckedAreas(String checkedAreas) {
		this.checkedAreas = checkedAreas;
	}

	public Integer getAreaLocationId() {
		return areaLocationId;
	}

	public void setAreaLocationId(Integer areaLocationId) {
		this.areaLocationId = areaLocationId;
	}

	public String getGoogleLocationName() {
		return googleLocationName;
	}

	public void setGoogleLocationName(String googleLocationName) {
		this.googleLocationName = googleLocationName;
	}

	public String getGoogleAreaDisplayName() {
		return googleAreaDisplayName;
	}

	public void setGoogleAreaDisplayName(String googleAreaDisplayName) {
		this.googleAreaDisplayName = googleAreaDisplayName;
	}

	public String getGoogleLocationUrl() {
		return googleLocationUrl;
	}

	public void setGoogleLocationUrl(String googleLocationUrl) {
		this.googleLocationUrl = googleLocationUrl;
	}

	public String getGoogleLocationDisplayName() {
		return googleLocationDisplayName;
	}

	public void setGoogleLocationDisplayName(String googleLocationDisplayName) {
		this.googleLocationDisplayName = googleLocationDisplayName;
	}

	public String getGoogleLocationPlaceId() {
		return googleLocationPlaceId;
	}

	public void setGoogleLocationPlaceId(String googleLocationPlaceId) {
		this.googleLocationPlaceId = googleLocationPlaceId;
	}

	public String getGoogleRedirectUrl() {
		return googleRedirectUrl;
	}

	public void setGoogleRedirectUrl(String googleRedirectUrl) {
		this.googleRedirectUrl = googleRedirectUrl;
	}

	public double getGoogleLocationLatitude() {
		return googleLocationLatitude;
	}

	public void setGoogleLocationLatitude(double googleLocationLatitude) {
		this.googleLocationLatitude = googleLocationLatitude;
	}

	public double getGoogleLocationLongitude() {
		return googleLocationLongitude;
	}

	public void setGoogleLocationLongitude(double googleLocationLongitude) {
		this.googleLocationLongitude = googleLocationLongitude;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	public Integer getOffset() {
		return offset;
	}

	public void setOffset(Integer offset) {
		this.offset = offset;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getMyFileFileName() {
		return myFileFileName;
	}

	public void setMyFileFileName(String myFileFileName) {
		this.myFileFileName = myFileFileName;
	}

	public File getMyFile() {
		return myFile;
	}

	public void setMyFile(File myFile) {
		this.myFile = myFile;
	}

	public String getGoogleAreaName() {
		return googleAreaName;
	}

	public void setGoogleAreaName(String googleAreaName) {
		this.googleAreaName = googleAreaName;
	}

	public String getGoogleAreaUrl() {
		return googleAreaUrl;
	}

	public void setGoogleAreaUrl(String googleAreaUrl) {
		this.googleAreaUrl = googleAreaUrl;
	}

	public String getGoogleAreaPlaceId() {
		return googleAreaPlaceId;
	}

	public void setGoogleAreaPlaceId(String googleAreaPlaceId) {
		this.googleAreaPlaceId = googleAreaPlaceId;
	}

	public double getGoogleAreaLatitude() {
		return googleAreaLatitude;
	}

	public void setGoogleAreaLatitude(double googleAreaLatitude) {
		this.googleAreaLatitude = googleAreaLatitude;
	}

	public double getGoogleAreaLongitude() {
		return googleAreaLongitude;
	}

	public void setGoogleAreaLongitude(double googleAreaLongitude) {
		this.googleAreaLongitude = googleAreaLongitude;
	}

	public String getUrlType() {
		return urlType;
	}

	public void setUrlType(String urlType) {
		this.urlType = urlType;
	}

	public List<GoogleLocation> getGoogleLocationList() {
		return googleLocationList;
	}

	public void setGoogleLocationList(List<GoogleLocation> googleLocationList) {
		this.googleLocationList = googleLocationList;
	}

	public List<GoogleArea> getGoogleAreaList() {
		return googleAreaList;
	}

	public void setGoogleAreaList(List<GoogleArea> googleAreaList) {
		this.googleAreaList = googleAreaList;
	}
	
	
	

}
