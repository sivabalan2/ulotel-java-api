package com.ulopms.view;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TimeZone;

import javax.imageio.ImageIO;
import javax.persistence.Column;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;




//import com.medwecare.controller.AwsObjectManager;
//import com.medwecare.controller.CommentManager;
//import com.medwecare.controller.CommentUserManager;
////import com.medwecare.controller.NotificationManager;
//import com.medwecare.controller.ProjectManager;

//import com.medwecare.model.AwsObject;
//import com.medwecare.model.Notification;
//import com.medwecare.model.ObjectComment;
//import com.medwecare.model.ObjectCommentUser;
//import com.medwecare.model.Project;

























import org.joda.time.DateTime;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.opensymphony.xwork2.ActionSupport;

import freemarker.template.Configuration;
import freemarker.template.Template;

import javax.servlet.http.HttpServletRequest;

import com.opensymphony.xwork2.ModelDriven;
import com.ulopms.api.Hotels;
import com.ulopms.controller.AccommodationAmenityManager;
import com.ulopms.controller.AreaManager;
import com.ulopms.controller.GooglePlaceManager;
import com.ulopms.controller.LocationManager;
import com.ulopms.controller.PmsAmenityManager;
import com.ulopms.controller.PmsBookingManager;
import com.ulopms.controller.PmsGuestManager;
import com.ulopms.controller.PmsPropertyManager;
import com.ulopms.controller.PromotionDetailManager;
import com.ulopms.controller.PromotionManager;
import com.ulopms.controller.PropertyAccommodationInventoryManager;
import com.ulopms.controller.PropertyAccommodationManager;
import com.ulopms.controller.PropertyAmenityManager;
import com.ulopms.controller.PropertyAreasManager;
import com.ulopms.controller.PropertyDiscountManager;
import com.ulopms.controller.PropertyLandmarkManager;
import com.ulopms.controller.PropertyPhotoManager;
import com.ulopms.controller.PropertyRateDetailManager;
import com.ulopms.controller.PropertyRateManager;
import com.ulopms.controller.PropertyRatePlanDetailManager;
import com.ulopms.controller.PropertyReviewsManager;
import com.ulopms.controller.PropertyTaxeManager;
import com.ulopms.controller.PropertyTypeManager;
import com.ulopms.controller.SeoContentManager;
import com.ulopms.controller.StateManager;
import com.ulopms.controller.UserCookiesManager;
import com.ulopms.controller.UserLoginManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.AccommodationRoom;
import com.ulopms.model.Area;
import com.ulopms.model.DashBoard;
import com.ulopms.model.GoogleArea;
import com.ulopms.model.GooglePropertyAreas;
import com.ulopms.model.Location;
import com.ulopms.model.LocationType;
import com.ulopms.model.PmsAmenity;
import com.ulopms.model.PmsAvailableRooms;
import com.ulopms.model.PmsGuest;
import com.ulopms.model.PmsPromotionDetails;
import com.ulopms.model.PmsPromotions;
import com.ulopms.model.PmsProperty;
import com.ulopms.model.PmsSmartPrice;
import com.ulopms.model.PmsTags;
import com.ulopms.model.PropertyAccommodation;
import com.ulopms.model.PropertyAccommodationInventory;
import com.ulopms.model.PropertyAmenity;
import com.ulopms.model.PropertyAreas;
import com.ulopms.model.PropertyDiscount;
import com.ulopms.model.PropertyLandmark;
import com.ulopms.model.PropertyPhoto;
import com.ulopms.model.PropertyRate;
import com.ulopms.model.PropertyRateDetail;
import com.ulopms.model.PropertyRatePlanDetail;
import com.ulopms.model.PropertyReviews;
import com.ulopms.model.PropertyTags;
import com.ulopms.model.PropertyTaxe;
import com.ulopms.model.PropertyType;
import com.ulopms.model.SeoContent;
import com.ulopms.model.State;
import com.ulopms.model.User;
import com.ulopms.model.UserCookies;
import com.ulopms.util.DateUtil;
import com.ulopms.util.Email;
import com.ulopms.util.Image;

public class LocationAction extends ActionSupport implements
ServletRequestAware, SessionAware, UserAware{

	private static final long serialVersionUID = 914982665758390091L;

	private Integer areaId;
	private Integer locationId;
	private String locationUrl;
	private String description;
	private String locationName;
	private Integer propertyId;
	private String photoPath;
	private String altName;
	private String propertyThumbPath;
	private Timestamp arrivalDate;
	private Timestamp departureDate;
	private List<Location> locationList;
	private List<LocationType> locationTypeList;
	private Integer sourceTypeId;
	private String metaTag;
	private String metaDescription;
	private String webContent;
	private Integer sourceId;
	private long roomCnt;
	private Integer propertyAccommodationId;
	private String strStartDate;
	private String strEndDate;
	private String StartDate;
	private String EndDate;
	private String editLocationId;
	private String editLocationName;
	private String editLocationDescription;
	private String locationDescription;
	private String displayHoursForTimer;
	
	private File myFile;
	private String myFileContentType;
	private String myFileFileName;
	private Integer limit;
	private Integer offset;
	private List<PmsProperty> propertyList;
	private List<UserCookies> cookiesList;
	private String filterSearch;

	private static final Logger logger = Logger.getLogger(LocationAction.class);

	
	private User user;

	private HttpSession session;

	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}
	
	private SessionMap<String,Object> sessionMap;  
	  
	//getters and setters  
	  
	@Override  
	public void setSession(Map<String, Object> map) {  
	    sessionMap=(SessionMap)map;  
	}  
	
	// @Override
	// public User getModel() {
	// return user;
	// }

	@Override
	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	
	private HttpServletRequest request;

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	public LocationAction() {
		
		

	}

	public String execute() {
		//this.patientId = getPatientId();
		return SUCCESS;
	}

	public String getSearchForm() throws IOException {
		
		LocationManager locationController = new LocationManager();
        SeoContentManager seoContentController = new SeoContentManager();
	    
        /*SeoContent seoContent = seoContentController.findSeoContent(this.locationId);	
		
        request.setAttribute("metaTag" ,seoContent.getTitle());
		request.setAttribute("description" ,seoContent.getDescription());
		request.setAttribute("content" ,seoContent.getContent());*/
		
        List<SeoContent> listSeoLocation=seoContentController.list(this.locationId);
		 if(listSeoLocation.size()>0 && !listSeoLocation.isEmpty()){
			 SeoContent seoContent = seoContentController.findSeoContent(this.locationId);	
			 request.setAttribute("metaTag" ,seoContent.getTitle());
			 request.setAttribute("description" ,seoContent.getDescription());
			 request.setAttribute("content" ,seoContent.getContent());	
		 }else{
			 request.setAttribute("metaTag" ,"<title>Ulohotels</title>");
			 request.setAttribute("description" ,"<p></p>");
			 request.setAttribute("content" ,"<p></p>");	
		 }
		 
		
		
			try {
                 

				 /* if(this.locationId== 1){
					//request.setAttribute("metaTag", " coorg entered");
					//this.metaTag = "Have a comfy stay at the best hotels in Coorg within your budget";
					this.setMetaTag("Have a comfy stay at the best hotels in Coorg within your budget");
					this.setMetaDescription("Explore the ambience of Coorg by staying at the best resorts in Coorg. Ulo Riviera The Spring Arc &amp; Ulo Alipinia Estate resort are budget friendly hotels that gives you the feel of homestay.");
					this.setWebContent("Coorg, also called as Kodagu is a hill station in Karnataka. It is known as the Scotland of India. The note worthy places in Coorg are Madikeri fort, Omkareshwara Temple, Raja�s tomb, Abbi Falls, Tala Cauvery, Gothic-style church and museum. The best time to visit Coorg is between October and March. It is a evergreen place with pleasant climate throughout the year. The Storm festival takes place every February. It is recommended to visit Coorg during summer and winter. It is well known for honey, handmade chocolates, coffee and spice plantations. The scenic charms allure many thrill seekers and nature lovers to this wonderful place. The beauty of this place mesmerizes many tourists to stay in Coorg for a long period of time. Coorg is best suited for family vacations. It is the perfect place for people who love trekking in this breathtaking valleys and mountains. It is a wonderful and safe adventure destination. You must have your travel documents all the time to avoid any inconvenience. You can visit Kushal Nagar Market and Friday Market to buy best quality coffee, spices, nuts and chocolates. You can spot the budget hotels in Coorg with tariff if you search it using the right website. The hotel booking in Coorg, Karnataka is made easier by online booking feature. The bugging question which comes to everyone is where to stay in Coorg?. Coorg has hotels or resorts to suit every traveller�s budget. Search for Coorg places to stay before planning your trip. It can be a boutique resort, budget friendly hotels or home stays and camps.If you plan a trip, the first step is to search for the good resorts in Coorg with all basic amenities. You can locate your ideal hotels and resorts in Coorg by browsing the internet. There are many hotels in Coorg for your stay. Check criteria like cost, travel, food, location, room service, etc., before you book your room. You can search the hotels in Coorg with price on any travel website. There are plenty of lodges in Coorg which are available at moderate prices. Coorg hotels and resorts rates depends upon the location and services provided. You can get the best resorts in Coorg if you are willing to spend some money. Before booking a hotel or resort, check whether it is near to the tourist spots. Nowadays, most of the resorts provide internet connections as it became a major part of our life.You can stay at any 3 star hotels in Coorg based on your budget. You can get many cottages in Coorg at reasonable price. There are many best hotels in Coorg which provides a escape for people from the stressful world. You can check the Coorg hotels price list any time by checking the websites. You have a handful of options when it comes to booking a best place to stay in Coorg. If you are booking online, then you can view all the resorts in Coorg with price. You can book it according to your budget by scrutinizing all available.When it comes to Coorg homestay, you have ample of choices. You should look harder to find a cheap homestay in Coorg. The homestay in Coorg for family is enjoyable if they have special amenities for kids as well as elders. Find the perfect homestay in Coorg with price, location and other features in any best website. You can get valuable deals for cheap hotels in Coorg if you book utilizing any apps. View the resorts in Coorg with tariff before paying for any rooms.Check the hotel reviews prior booking the room on trusted websites. Compares rates of different hotels to find the convenient one for you. There are many best resorts in Coorg for honeymoon couples.Book any resort on booking websites like www.ulohotels.com which provides you with multiple options. You can find the resorts in Coorg with price in this website.The budget hotels in Coorg are ideal for solo travellers and group of friends who wants to spend less on rooms. The best resorts in Coorg, Madikeri are Ulo Riviera The Spring arc and Ulo Alpinia Estate resort.They are located on the hills surrounded by the spice and copy plantation making our stay at Coorg blissful. It provides services like television, complimentary breakfast, personal assistance, bonfire, and extra bed. You have many options for booking rooms like Deluxe rooms, Family Deluxe room and Economy suite.If you search for top resorts in Coorg, Ulo Hotels Group will be one among them. The location and the services provided here makes them the best honeymoon resorts in Coorg. These two are the best budget resorts in Coorg for family. They comes under the top 10 resorts in Coorg. It is comfortable and easy to book the rooms online.The price of the room mentioned in the website is inclusive of all the taxes. They have both budget and luxury rooms. You can book any budget resorts in Coorg with a single touch by utilizing the online booking feature in your mobile phones. You need not to pay any booking fees and they also provide 24/7 Customer service. The resort check-in time is 10 A.M and check-out time is 9 A.M. It is recommended to call and confirm your booking to avoid any issues. Visit www.ulohotels.com to clear any doubts related to booking. There are also many deals available if you book the rooms online.The rooms are equipped with air conditioner, television, laundry,etc,. It have 24/7 hot and cold water supply. The bonfire is also arranged if requested by the tourists. The charges will be based only on the number of rooms you are booking. The cancellation and prepayment policies differs according to the room type. Verify the Fare policy related to your room. You can pay online or directly at the hotel if the room you have chosen supports this option.The price is reasonable when compared to other resorts in Coorg.You can also book rooms with terrace or balcony. You can arrange for a local tour from the travel agency available at the travel desk.They provide car for rentals and you can also hire a guide. You can keep your belongings in your personal lockers. It is the only resort in Coorg which provides Rain Dance. They give complimentary gala dinner on New year and Christmas. Free private parking is available for all customers. The pets are allowed inside the resort. You should display your identification proofs upon check-in. Every room is equipped with a personal bathroom fitted with bath. It serves yummy Breakfast and Dinner daily. If you desire, you can even have your breakfast in your room.It is situated near the major tourist spots and helps the tourists to travel easily. You can find many hotels in Madikeri near bus stand and the best one among them is Ulo Hotels. It is located in 11 kms from the Madikeri Bus Stand on Mangalore Highway. The aroma from the spice and coffee plantation helps to refresh yourselves. It will be definitely be a memorable experience to stay in Coorg.");
					
					}
					else if(this.locationId== 3){
						
						this.setMetaTag("Book Kodaikanal Resorts and Hotels @ Great Rates");
						this.setMetaDescription("Make your trip wonderful by staying at the best luxury and budget resorts in Kodaikanal. ULO SMS Residency, ULO Kodai Green &amp; ULO Muthu Residency are ideal resorts for perfect vacation.");
						this.setWebContent("Kodaikanal is popularly known as the Princess of Hills. It is famous for its green,fresh and scenic environment. It is located @ 2,000 metres above sea level. It is covered with forest, lakes, waterfalls, granite and grassy hills. The points of interest are, Kodaikanal Lake, Kodaikanal Solar Observatory, Silver Cascade Falls, Pine Tree Forest, Dolphin Nose, etc,.It has chill climate for the whole year with seasonal rainfall. Choose a hotel which is present on the top of the hill to get a better view of this place. The famous foods in Kodaikanal are, chocolates, brownies, hot chai or tea and cheeses. Visiting Kodaikanal using travel package is best option if you want to see all the major tourist places.The best time to visit this place is from March to June or December to February. You can go for trekking, horse riding, boating etc,. Only few ATM�s are available in Kodaikanal, so try to carry sufficient amount of money with you. It is the most visited honeymoon destinations in South India. This is a dream destination for family vacation, adventure seekers and honeymoon couples.There are handful of choices for accommodation in Kodaikanal. Look for the list of Kodaikanal hotels and resorts prior booking your room. You can check the best resorts in Kodaikanal with rates from any valid booking websites. Find your ideal Kodaikanal resorts package by logging into the website www.ulohotels.com. Check Kodaikanal resorts list before planning your holiday.If your budget is high, you can reserve any luxury hotels in Kodaikanal. The Kodaikanal resorts price list may slightly differ in some websites.The best hotels in Kodaikanal are the ones which satisfies all your requirements. You can find many best resorts in Kodaikanal at affordable price. The best place to stay in Kodaikanal is the one which is easily accessible.If you travel with a large group, you can get many best cottages in Kodaikanal. ULO SMS Residency and ULO Muthu Residency are the two best hotels in Kodaikanal for family. Log into www.ulohotels.com to locate all hotels in Kodaikanal with rates. You can even book top hotels in Kodaikanal with great deals offered by booking sites. The luxury resorts in Kodaikanal comes with additional features like swimming pools, indoor games, refrigerator, etc,.You can find the hotels in Kodaikanal with phone numbers in ULO Hotels Group official page. Our Kodaikanal hotels rates are moderate when compared to other hotels in the locality. You will have multiple options when it comes to places to stay in Kodaikanal. If you want to limit your travel expenses, view the budget hotels in Kodaikanal with rates in any authorized websites.The cottages in Kodaikanal are generally situated near hills or forest areas. Book independent cottages in Kodaikanal to have a private vacation with your close ones. Make sure that you reserve only family cottages in Kodaikanal if you stay with your family. You can find budget cottages in Kodaikanal with rates in our website. The lodges in Kodaikanal are best suited if you travel alone.ULO Kodai Green is one of the few hotels in Kodaikanal near lake. It is the best lake view resort in Kodaikanal surrounded by the stunning beauty of nature. It is a boutique style hotel which you can rent for few days. It is located at the centre of the city which makes it easily approachable. Many famous tourist spots are located around 4km from the hotel.It has Standard and family rooms with the amenities like TV, AC, Laundry, and 24 hours room service. You can also avail discount of 15%. Even if you cancel the room 24 hours prior the trip, you can receive full refund. The hotel booking in Kodaikanal can be done either by online or directly at the hotels. One can get best offers in cheap hotels in Kodaikanal if you book online with the promo code.If there is a budget cut, rest in any cheap cottages in Kodaikanal. There are plenty of good hotels in Kodaikanal to stay alone or with your family. If you travel as a couple, you can find many good resorts in Kodaikanal. On the basis of your budget, you can choose any 3 star or 5 star hotels in Kodaikanal. The list of hotels in Kodaikanal with tariff can be downloaded from the internet. The cost is calculated with respect to the number of rooms in all Kodaikanal ULO hotels.If you desire to cut short your expenses, try to stay in the dormitory in Kodaikanal. The 3 bedroom cottages in kodaikanal are a wise option if you stay with a large group or family. You can self-cook or arrange a maid for cooking if you stay in a cottage.If you long for your mother�s care, try homestay in Kodaikanal. The homestay in Kodaikanal is cheaper than booking cottages.ULO Muthu Residency provides best accommodation to all its customers. Most of the popular tourist places are located within 7 kms from this hotel. The Bus Stand is also 4 km from the hotel. Each room is equipped with Air conditioners, TV, tables, chairs and personal bathroom. There is also 24*7 hot and cold waeter supply. They also have facilities like, Wi-Fi, laundry, private parking, room service, rental transport and even doctors. ULO SMS Residency is one of the great Kodaikanal hotels near bus stand. They offer services like, laundry, room service, personal parking, free internet, restaurant and also arranging travels. They provide 24/7 room service and hot or cold water supply. You can get extra bed or mattress on request. Make use of meditation room to do yoga or any other exercises. As per the customer�s request, they arrange campfire also. If required, same your valuables in personal lockers. You can book the room online and pay for it directly at the hotel. If you want to make your vacation comfortable, rest in the best villas in Kodaikanal. Many corporates or banks have their guest houses in Kodaikanalfor their employees. Reserve any mount view Kodaikanal hotels on www.ulohotels.com @ starting price of Rs.999. If you want to have a best vacation ever, don�t worry about the money spent.");
						
					}
					else if(this.locationId== 4){
						
						this.setMetaTag("Best Budget Resorts in Yelagiri with Great Ambience @ LowestPrices");
						this.setMetaDescription("Reserve the best low budget luxury hotels and resorts in Yelagiri. Get great deals, offers and discounts @ ULO Hill Breeze, one of the fine resorts on Yelagiri.");
						this.setWebContent("Yelagiri is one of the famous hill stations in Tamil Nadu. It is located at the height of 1,110.6 metres above sea level. It is fully covered with valleys, rose- garden and orchads. The main tourist places to visit are, Jalagamparai Waterfalls, Murugan Temple, and Yelagiri Forest Hill. The best months to visit Yelagiri are November to February. You can buy local honey and jackfruit from the local markets. It is an amazing place for people who love exploring the nature. It is an excellent place for trekking and meditation. Stay in the best hotels in Yelagiri to enjoy the greenery and mesmerizing beauty of Yelagiri. To book your hotel rooms, search as Yelagiri hotels booking in any authenticated booking sites. You will get the list of hotels in Yelagiri with tariff. Select the hotel or resort which is suitable for you. If you want to stay in a homely surrounding, opt for homestay in Yelagiri Hills. If you plan a vacation, you won�t just pack your suitcases and rush to the trip. The crucial step in your trip is to find a good accommodation in Yelagiri. If your family has small kids, they mostly prefer cottages in Yelagiri Hills with swimming pools. Each cottage has a separate swimming pool. You can hire separate cook and maid while you stay at the cottages. If you want to spend your vacation with your family free from other disturbances, book 3 star cottages in Yellagiri. If you are a bachelor or bunch of friends travelling together, book cheap hotels in Yelagiri to save money. If you can enjoy the complete beauty of Yelagiri from your balcony, then that can be called as the best resort in Yelagiri. Ulo Hill Breeze is one of the best hotels in Yelagiri which provide great service for reasonable price. You can say any resorts as the best resorts in Yelagiri if it matches all your expectations.If you search the internet, you will find Ulo Hotel Groups as one of the best places to stay in Yelagiri. If you reserve the cottages online, you can get discounts in Yelagiri cottages prices. One can say that Ulo hill Breeze is one of the best rated Yelagiri Hills resorts in any booking websites. Nowadays, most of the Yelagiri resorts comes with common swimming pool. If you book it online, they also give you upto 50% discounts on total price. Pick any resorts in Yelagiri Hills which is easily accessible via any transport facility. You can reserve even cheap resorts in Yelagiri with swimming pool @ low rates in www.ulohotels.com. See the Yelagiri resorts list in websites and then choose the perfect resort for you. Book any luxury Yelagiri hotels and resorts online @ special price. If you want hotels which is value for money, reserve 3 star budget hotels in Yelagiri like Ulo Hill Breeze. You can find budget hotels in Yelagiri with tariff at their official website. Once the rooms are booked, you will receive either confirmation message or mail within few minutes. They also have 24/7 Customer Support to answer all your queries. They also provide quick refunds if you cancel your booking. The rooms are spacious and are cleaned daily. You can park your cars in private car parking. They have separate desk for local travel agency using which you can book any travel packages. All the rooms have separate bathroom and TV. You can order food directly to your rooms from the restaurant. They have special cottages for honeymoon couples as well.It is also located near the main tourist spots in Yelagiri Hills. If you want to book best budget hotels, then Ulo Hotel Groups is a wonderful choice.");
						
					}
					
					else if(this.locationId== 5){
						
						this.setMetaTag("Experience the hospitality of the amazing Kolli Hills Resorts and Hotels");
						this.setMetaDescription("Great savings on Kolli Hills Resorts and Hotels booking online. ULO Rejoice Villa Resorts in Kolli hills offers great rooms in great prices with best facilities.");
						this.setWebContent("Kolli Hills, also called as Kollimalai is located in Namakkal district in Tamil Nadu. It is located at 1000 to 1300 metres above sea level. It is famous for jackfruit and wild honey which is harvested from the mountains. It is frequently visited by hikers, nature lovers and trekking associations. Kolli Hills is known for its rich and diverse flora and fauna. There are mainly 9 tourist places in Kollimalai which is visited by all tourists. Stay in the best Kolli Hills resorts to enjoy your trip to the fullest. Most of the Kollimalai hotels are situated amidst of hills with breathtaking view. You can get your ideal resorts in Kolli Hills by checking in www.ulohotels.com. The Kolli Hills resorts booking is made easier by booking sites or apps. If you are a family or a group, many Kolli Hills hotels provide you with special packages.  Kolli Hills trip is mostly preferred by couples and group. The best time to visit it is February to December. You can download Kolli Hills Travel Guide to know about places to visit in Kollimalai. Staying in kolli Hills rejuvenate your mind and body. Kolli hills provides the best accommodation  options for all the travellers making your trip pleasant. You can choose among luxurious resorts, budget hotels, lodges and cottages. If you are a honeymoon couple, booking a villa will be the best option. The first step in planning a vacation to Kollimalai is to explore the entire list of hotels in Kollimalai Hills.  To find the best suited room for you, type as Kolli Hills accommodation in www.ulohotels.com. You should assess all the criteria like travel expenses, location, cost, amenities etc, if you plan for Kolli Hills stay. When you look for places to stay in Kollimalai, choose a place which provides easy transportation. If you opt for Kolli Hills homestay, rent homes from people in Kollimalai. You can directly find the perfect homestay for you by searching in any booking websites. You have multiple options when it comes to places to stay in Kolli Hills which you can decide according to your budget. When you select Kollimalai cottages for your stay, you can book either 2 or 3 bedroom cottages based on head count. If you book your resorts online, you will get rooms at discount prices. There is no reservation cost if you book online. You can compare rates of different resorts and then select thee one which satisfies your needs and budget. You can make use of promo codes or coupons to get a discount on your rooms. Check for accommodation details in ULO Hotels official website. You can also get best offers and deals on online booking in Kollimalai. ULO Rejoice Villa resorts is one of the best Kollimalai resorts located near the Arapalliswarar Temple. It is best suited for family, couples and friends. sIt is situated at 6 km from the Kolli Hills. The room categories are, Standard room, Suite room, Family room and Economy suite. The check-in time is 10:00 A.M and check-out time is 12:00 P.M. If you want to change or cancel your booking, do it one week before your arrival date. Most of the tourist places are within 4 km from the resort. They have an on-site restaurant with free Wi-Fi. Each room is fitted with a private bathroom and TV. You can get any help from the 24*7 front desk. No reservation is required for public parking. One of the perks is free maid service. They also have Banquet or Meeting facilities. You can get 2 additional beds in a room if requested. You can enjoy the sunrise and sunset from your balcony. They also arrange campfire with music on customer�s request. ULO Rejoice Villa Resorts is a safe place for your family and friends. You can get best homemade foods when you stay there. The nearest airport, Tiruchirapalli Airport is only 90 minutes from the resort. Make the best of your time by staying @ one of the best resorts in Kollimalai.");
						
					}
				*/
				  	DateFormat format = new SimpleDateFormat("MMM d,yyyy", Locale.ENGLISH);
				    java.util.Date dateStart = format.parse(getStartDate());
				    java.util.Date dateEnd = format.parse(getEndDate());
				   
				    
				    Calendar calCheckStart=Calendar.getInstance();
				    calCheckStart.setTime(dateStart);
				    java.util.Date checkInDate = calCheckStart.getTime();
				   
				    
				    
				    Calendar calCheckEnd=Calendar.getInstance();
				    calCheckEnd.setTime(dateEnd);
				    java.util.Date checkOutDate = calCheckEnd.getTime();
				   


					sessionMap.put("locationId",getLocationId());
					
					Location location = locationController.find(getLocationId());
					sessionMap.put("locationName",location.getLocationName());
					request.setAttribute("locationName" ,location.getLocationName().toUpperCase().trim());
					
					String checkIn = new SimpleDateFormat("MMM d,yyyy").format(checkInDate);
	                String checkOut = new SimpleDateFormat("MMM d,yyyy").format(checkOutDate);
	                String checkIn1 = new SimpleDateFormat("dd MMM").format(checkInDate);
	                String checkOut1 = new SimpleDateFormat("dd MMM").format(checkOutDate);
	                sessionMap.put("checkIn",checkIn);
	                sessionMap.put("checkOut",checkOut);
	                sessionMap.put("checkIn1",checkIn1);
	                sessionMap.put("checkOut1",checkOut1);
	                
	                

				} catch (Exception e) {
				logger.error(e);
				e.printStackTrace();
				} finally {

				}

			return SUCCESS;


		}
	public String editLocation() throws IOException {
		try {
			long time = System.currentTimeMillis();
			java.util.Date date = new java.util.Date(time);
			Timestamp tsDate=new Timestamp(date.getTime());  
			
			Integer userId=(Integer) sessionMap.get("userId");
			
			LocationManager locationController=new LocationManager();
			
			Location locations = locationController.find(getLocationId()); 
			locations.setLocationName(getLocationName());
			locations.setDescription(getDescription());
			locations.setLocationUrl(getLocationUrl());
			locations.setLocationDescription(getLocationDescription());
			locations.setIsActive(true);
			locations.setIsDeleted(false);
			locations.setModifiedBy(userId);
			locations.setModifiedOn(tsDate);
			locationController.edit(locations);
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return SUCCESS;
	}
	
	public String addLocationPicture() throws IOException {
		try {
			LocationManager locationController=new LocationManager();
			//Location locations=new Location();
			File fileZip1 = new File(getText("storage.location.photo") + "/"
					+ getLocationId() + "/", this.getMyFileFileName());
			FileUtils.copyFile(this.myFile, fileZip1);// copying image in the
			Location locations = locationController.find(getLocationId()); 
			locations.setPhotoPath(this.getMyFileFileName());
			locationController.edit(locations);
			/*BufferedImage image = ImageIO.read(this.myFile);
			int height = image.getHeight();
			int width = image.getWidth();
			if(height == 400 && width == 300){
				if(this.getMyFileFileName()!=null && !this.getMyFileFileName().equalsIgnoreCase(""))
				{
					File fileZip1 = new File(getText("storage.location.photo") + "/"
							+ getLocationId() + "/", this.getMyFileFileName());
					FileUtils.copyFile(this.myFile, fileZip1);// copying image in the
					Location locations = locationController.find(getLocationId()); 
					locations.setPhotoPath(this.getMyFileFileName());
					locationController.edit(locations);
				}
			}
			*/
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return SUCCESS;
	}
	
	public String addLocation(){ 
		try
		{
			long time = System.currentTimeMillis();
			java.util.Date date = new java.util.Date(time);
			Timestamp tsDate=new Timestamp(date.getTime());  
			
			Integer userId=(Integer) sessionMap.get("userId");
			
			LocationManager locationController=new LocationManager();
			Location locations=new Location();
			locations.setLocationName(getLocationName());
			locations.setDescription(getDescription());
			locations.setLocationDescription(getLocationDescription());
			locations.setLocationUrl(getLocationUrl());
			locations.setIsActive(true);
			locations.setIsDeleted(false);
			locations.setCreatedBy(userId);
			locations.setCreatedOn(tsDate);
			//locationController.add(locations);
			Location location = locationController.add(locations);
			
			this.locationId = location.getLocationId();
			
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("application/json");
			
			  if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"locationId\":\"" + this.locationId  + "\"";
				
				jsonOutput += "}";


			

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
		}catch(Exception e){
			logger.error(e);
			e.printStackTrace();
		}
		return null;
	}
	
	public String deleteLocation(){
		try{
			long time = System.currentTimeMillis();
			java.util.Date date = new java.util.Date(time);
			Timestamp tsDate=new Timestamp(date.getTime());  
			
			Integer userId=(Integer) sessionMap.get("userId");
			
			List<Location> listLocation=null;
			LocationManager locationController=new LocationManager();
			Location locations=new Location();
			listLocation=locationController.list(getLocationId());
			if(listLocation.size()>0 && !listLocation.isEmpty()){
				for(Location loc:listLocation){
					loc.setIsActive(false);
					loc.setIsDeleted(true);
					loc.setModifiedBy(userId);
					loc.setModifiedOn(tsDate);
					locationController.edit(loc);
				}
			}
			
		}catch(Exception e){
			logger.error(e);
			e.printStackTrace();
		}finally{
			
		}
		return null;
	}

	public String getLocations() throws IOException {
		try {
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			UserLoginManager userController=new UserLoginManager();
			
			LocationManager locationController = new LocationManager();
			response.setContentType("application/json");
			int serialno=1;
			this.locationList = locationController.list(limit,offset);
			if(locationList.size()>0 && !locationList.isEmpty()){
				for (Location location : locationList) {
					
					if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += ",{";
					else
						jsonOutput += "{";
					
					jsonOutput += "\"locationId\":\"" + location.getLocationId()+ "\"";
					jsonOutput += ",\"locationName\":\"" + location.getLocationName()+ "\"";
					jsonOutput += ",\"serialNo\":\"" + serialno + "\"";
					if(location.getCreatedBy()!=null){
						User user=userController.find(location.getCreatedBy());
						String strFirstName=user.getUserName().trim();
						String firstNameLetterUpperCase=strFirstName.substring(0, 1).toUpperCase()+strFirstName.substring(1);
						jsonOutput += ",\"createdBy\":\"" + firstNameLetterUpperCase+ "\"";
					}else{
						jsonOutput += ",\"createdBy\":\"" + " "+ "\"";
					}
					
					
					jsonOutput += "}";
					
					serialno++;
				}
			}else{
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				
				jsonOutput += "\"locationId\":\"" + ""+ "\"";
				jsonOutput += ",\"locationName\":\"" + ""+ "\"";
				jsonOutput += ",\"serialNo\":\"" + "" + "\"";
				jsonOutput += ",\"createdBy\":\"" + ""+ "\"";
				jsonOutput += "}";
			}
			

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	public String getEditLocation() throws IOException {
		
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			LocationManager locationController = new LocationManager();
			
			response.setContentType("application/json");

			this.locationList = locationController.list(getLocationId());
			for (Location location : locationList) {

				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				
				jsonOutput += "\"locationId\":\"" + location.getLocationId()+ "\"";
				jsonOutput += ",\"locationName\":\"" + location.getLocationName()+ "\"";
				jsonOutput += ",\"description\":\"" + location.getDescription()+ "\"";
				jsonOutput += ",\"locationDescription\":\"" + location.getLocationDescription()+ "\"";
				jsonOutput += ",\"photoPath\":\"" + location.getPhotoPath()+ "\"";
				jsonOutput += ",\"locationUrl\":\"" + location.getLocationUrl()+ "\"";
				jsonOutput += "}";

			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	public String getLocation() throws IOException {
		try {
			/*UserCookiesManager cookieController = new UserCookiesManager();
			HttpServletRequest request = ServletActionContext.getRequest();
			Enumeration headerNames = request.getHeaderNames();
	        while (headerNames.hasMoreElements()) {
	            String key = (String) headerNames.nextElement();
	            String value = request.getHeader(key);
	            UserCookies cookie = new UserCookies();
	            cookie.setUserKey(key);
	            cookie.setUserKeyValue(value);
	            cookie.setIsSearchEnable(false);
	            cookie.setIsCookiesEnable(true);
	            cookieController.add(cookie);
	        }*/
			

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			LocationManager locationController = new LocationManager();
			response.setContentType("application/json");

			this.locationList = locationController.list();
			for (Location location : locationList) {

				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				
				jsonOutput += "\"DT_RowId\":\"" + location.getLocationId()+ "\"";
				jsonOutput += ",\"locationName\":\"" + location.getLocationName()+ "\"";
				jsonOutput += ",\"description\":\"" + location.getDescription()+ "\"";
				jsonOutput += ",\"photoPath\":\"" + location.getPhotoPath()+ "\"";
				jsonOutput += ",\"locationUrl\":\"" +(location.getLocationUrl() == null ? "": location.getLocationUrl().trim())+ "\"";
				jsonOutput += "}";

			}

			
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	
	public String getLocationTypes() throws IOException {
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			LocationManager locationController = new LocationManager();
			response.setContentType("application/json");

			this.locationTypeList = locationController.listTypes();
			for (LocationType locationType : locationTypeList) {

				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				
				jsonOutput += "\"DT_RowId\":\"" + locationType.getLocationTypeId()+ "\"";
				jsonOutput += ",\"locationTypeName\":\"" + locationType.getLocationTypeName()+ "\"";
				jsonOutput += "}";

			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	
	public String getSelectedLocation() throws IOException {
		try {
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			LocationManager locationController = new LocationManager();
			response.setContentType("application/json");

			this.locationList = locationController.listSelectLocation();
			for (Location location : locationList) {

				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				
				jsonOutput += "\"DT_RowId\":\"" + location.getLocationId()+ "\"";
				jsonOutput += ",\"locationName\":\"" + location.getLocationName()+ "\"";
				jsonOutput += ",\"description\":\"" + location.getDescription()+ "\"";
				jsonOutput += ",\"photoPath\":\"" + location.getPhotoPath()+ "\"";
				jsonOutput += ",\"locationUrl\":\"" +(location.getLocationUrl() == null ? "": location.getLocationUrl().trim())+ "\"";
				jsonOutput += "}";

			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
     public String getUserCookies() throws IOException {
		
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			UserCookiesManager cookieController = new UserCookiesManager();
			
			response.setContentType("application/json");

			this.cookiesList = cookieController.list();
			for (UserCookies cookie : cookiesList) {

				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				
				jsonOutput += "\"cookiesId\":\"" + cookie.getCookieId()+ "\"";
				jsonOutput += ",\"userKey\":\"" + cookie.getUserKey()+ "\"";
				jsonOutput += ",\"userKeyValue\":\"" + cookie.getUserKeyValue()+ "\"";
				jsonOutput += "}";

			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}


     public String getUserSearch() throws IOException {
  		try {
  			
  			this.filterSearch=getFilterSearch();
  			String jsonOutput = "";
  			SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd");
  			List<UserCookies> userSearch=null;
  			HttpServletResponse response = ServletActionContext.getResponse();
  			UserCookiesManager cookieController = new UserCookiesManager();
  			response.setContentType("application/json");
  			int serialNo=1;
  			if(this.filterSearch.equalsIgnoreCase("currentDateName")){
  				 java.util.Date curdate=new java.util.Date();
 	             Calendar calStart=Calendar.getInstance();
 	             calStart.setTime(curdate);
 	             calStart.setTimeZone(TimeZone.getTimeZone("IST"));
 	             calStart.add(Calendar.DAY_OF_MONTH, -1);
                  java.util.Date startDate=calStart.getTime();
                  String strStartDate=format.format(startDate);
                  java.util.Date dteStart=format.parse(strStartDate);
                  
                  Calendar calEnd=Calendar.getInstance();
                  calEnd.setTime(curdate);
                  calEnd.setTimeZone(TimeZone.getTimeZone("IST"));
                  java.util.Date endDate=calEnd.getTime();
                  String strEndDate=format.format(endDate);
                  java.util.Date dteEnd=format.parse(strEndDate);
                  
  				userSearch = cookieController.listSearch(dteStart,dteEnd,this.filterSearch);	
  			}else if(this.filterSearch!=""){
  				
  				DateFormat format1 = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
  			    java.util.Date dtestartdate= format1.parse(getStrStartDate());
  			    java.util.Date dteenddate = format1.parse(getStrEndDate());
  			   
  			    
 	            Calendar calStart=Calendar.getInstance();
 	            calStart.setTime(dtestartdate);
 	            calStart.setTimeZone(TimeZone.getTimeZone("IST"));
 	            java.util.Date startDate=calStart.getTime();
 	            String strStartDate=format.format(startDate);
                 java.util.Date dteStart=format.parse(strStartDate);
                 
                 Calendar calEnd=Calendar.getInstance();
                 calEnd.setTime(dteenddate);
                 calEnd.setTimeZone(TimeZone.getTimeZone("IST"));
                 java.util.Date endDate=calEnd.getTime();
                 String strEndDate=format.format(endDate);
                 java.util.Date dteEnd=format.parse(strEndDate);
                 
  				userSearch = cookieController.listSearch(dteStart,dteEnd,this.filterSearch);
  			}
  			
  			if(userSearch.size()>0 && !userSearch.isEmpty()){
  				for (UserCookies search : userSearch) {
  					if (!jsonOutput.equalsIgnoreCase(""))
  						jsonOutput += ",{";
  					else
  						jsonOutput += "{";
  					
  					jsonOutput += "\"serialNo\":\"" + serialNo + "\"";
  					if(search.getUserSearchValue()!=null){
  						jsonOutput += ",\"searchArea\":\"" + search.getUserSearchValue().toUpperCase()+ "\"";	
  					}else{
  						jsonOutput += ",\"searchArea\":\"" + search.getUserSearchValue()+ "\"";
  					}
  					if(search.getUserSearchDate()!=null){
  						Calendar calDate=Calendar.getInstance();
  						calDate.setTime(search.getUserSearchDate());
  						String strSearchDate=new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss").format(calDate.getTime());
  						jsonOutput += ",\"searchDate\":\"" + strSearchDate+ "\"";	
  					}else{
  						jsonOutput += ",\"searchDate\":\"" + ""+ "\"";
  					}
  					
  					if(search.getUserStartDate()!=null){
  						Calendar calStartDate=Calendar.getInstance();
  						calStartDate.setTime(search.getUserStartDate());
  						String strStartDate=new SimpleDateFormat("dd-MMM-yyyy").format(calStartDate.getTime());
  						jsonOutput += ",\"searchStart\":\"" + strStartDate+ "\"";
  					}else{
  						jsonOutput += ",\"searchStart\":\"" + "" + "\"";
  					}
  					
  					if(search.getUserEndDate()!=null){
  						Calendar calEndDate=Calendar.getInstance();
  						calEndDate.setTime(search.getUserEndDate());
  						String strEndDate=new SimpleDateFormat("dd-MMM-yyyy").format(calEndDate.getTime());
  						jsonOutput += ",\"searchEnd\":\"" + strEndDate +"\"";
  					}else{
  						jsonOutput += ",\"searchEnd\":\"" + "" +"\"";	
  					}
  					
  					if(search.getUserIpAddress()!=null){
  						jsonOutput += ",\"userIpAddress\":\"" + search.getUserIpAddress() +"\"";
  					}else{
  						jsonOutput += ",\"userIpAddress\":\"" + " " +"\"";
  					}
  					
  					jsonOutput += "}";
  					serialNo++;
  				}	
  			}
  			

  			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");

  		} catch (Exception e) {
  			logger.error(e);
  			e.printStackTrace();
  		} finally {

  	}

  		return null;

  	}
     
     public String getLocationPicture() {
		
		//this.propertyId = (Integer) sessionMap.get("propertyId");
		HttpServletResponse response = ServletActionContext.getResponse();

		LocationManager locationController = new LocationManager();
		Location location = locationController.find(getLocationId());
		String imagePath = "";
		// response.setContentType("");
		try {
			
			if (getLocationId() >0 ) {
				imagePath = getText("storage.location.photo") + "/"
						+ location.getLocationId()+ "/"
						+ location.getPhotoPath();
				Image im = new Image();
				response.getOutputStream().write(im.getCustomImageInBytes(imagePath));
				response.getOutputStream().flush();
			} else {
				imagePath = getText("storage.path") + "/emptyprofilepic.png";
				Image im = new Image();
				response.getOutputStream().write(
						im.getCustomImageInBytes(imagePath));
				response.getOutputStream().flush();
			}
		} catch (Exception e1) {
			logger.error(e1);
			e1.printStackTrace();
		} finally {

		}
		return null;

	}

   public String getFirstLocationProperties() throws IOException{

		try {
		
	 /* SeoContentManager seoContentController = new SeoContentManager();
	  SeoContent seoContent = seoContentController.findSeoContent(getLocationId());	
	        
	  request.setAttribute("metaTag" ,seoContent.getTitle());
	  request.setAttribute("description" ,seoContent.getDescription());
	  request.setAttribute("content" ,seoContent.getContent());*/

			
		Map mapDate=new HashMap();
		
		DateFormat format = new SimpleDateFormat("MMM d,yyyy", Locale.ENGLISH);
	    java.util.Date dateStart = format.parse((String)request.getAttribute("StartDate"));
	    java.util.Date dateEnd = format.parse((String)request.getAttribute("EndDate"));
	   
	    if(getSourceTypeId()==null){
	    	this.sourceTypeId=1;
	    }
	    
	    Calendar calCheckStart=Calendar.getInstance();
	    calCheckStart.setTime(dateStart);
	    java.util.Date checkInDate = calCheckStart.getTime();
	    this.arrivalDate=new Timestamp(checkInDate.getTime());
	   
	    
	    Calendar calCheckEnd=Calendar.getInstance();
	    calCheckEnd.setTime(dateEnd);
	    java.util.Date checkOutDate = calCheckEnd.getTime();
	    this.departureDate=new Timestamp(checkOutDate.getTime());
	    
	    String checkIn = new SimpleDateFormat("MMM d,yyyy").format(checkInDate);
       String checkOut = new SimpleDateFormat("MMM d,yyyy").format(checkOutDate);
       String checkIn1 = new SimpleDateFormat("dd MMM").format(checkInDate);
       String checkOut1 = new SimpleDateFormat("dd MMM").format(checkOutDate);
       
       sessionMap.put("checkIn",checkIn);
       sessionMap.put("checkOut",checkOut);
       sessionMap.put("checkIn1",checkIn1);
       sessionMap.put("checkOut1",checkOut1);
       
		/*this.arrivalDate = getArrivalDate();
	    sessionMap.put("arrivalDate",arrivalDate); 
		
		this.departureDate = getDepartureDate();
		sessionMap.put("departureDate",departureDate); */
		mapDate.put("arrivalDate", arrivalDate);
		mapDate.put("departureDate", departureDate);
		
		PmsPropertyManager propertyController = new PmsPropertyManager();		
		PropertyAccommodationManager accommodationManager = new PropertyAccommodationManager();
		PropertyAmenityManager amenityController = new PropertyAmenityManager();
		List<PropertyAmenity> propertyAmenityList;
		PropertyTypeManager typeController =  new PropertyTypeManager();
		PropertyRateManager rateController = new PropertyRateManager();
		PropertyRateDetailManager detailController = new PropertyRateDetailManager();
		PmsAmenityManager ameController = new PmsAmenityManager();
		PromotionManager promotionController=new PromotionManager();
		PromotionDetailManager promotionDetailController=new PromotionDetailManager();
		PmsBookingManager bookingController=new PmsBookingManager();
		
		
		HashMap<Double,Integer > mapAmountAccommId=new HashMap<Double, Integer>();
		HashMap<Double,Integer > mapSoldOutAmountAccommId=new HashMap<Double, Integer>();
		
		ArrayList<Integer> arlSortPropertyId=new ArrayList<Integer>();
		HashMap<Integer, Integer> mapAvailablePropertyId=new HashMap<Integer, Integer>();
		String jsonOutput = "";
		HttpServletResponse response = ServletActionContext.getResponse();
		
		response.setContentType("application/json");
		
		this.arrivalDate = getArrivalDate();
	    sessionMap.put("arrivalDate",arrivalDate); 
			
		this.departureDate = getDepartureDate();
		sessionMap.put("departureDate",departureDate); 
		 
		Calendar cal = Calendar.getInstance();
		cal.setTime(this.departureDate);
	    cal.add(Calendar.DATE, -1);
	    
	    java.util.Date dteCheckOut = cal.getTime();
	    this.departureDate=new Timestamp(dteCheckOut.getTime());
	    sessionMap.put("endDate",departureDate); 
	    
		String startDate = new SimpleDateFormat("yyyy-MM-dd").format(this.arrivalDate);
		String endDate = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
		DateTime start = DateTime.parse(startDate);
	    DateTime end = DateTime.parse(endDate);
	    DecimalFormat df = new DecimalFormat("###.##");
	    DateFormat f = new SimpleDateFormat("EEEE");
	    
   	List<PmsSmartPrice> listSmartPriceDetail=null; 
   	List<AccommodationRoom> listAccommodationRoom=null;
   	
		PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
		PropertyReviewsManager reviewController=new PropertyReviewsManager();
	    int availRoomCount=0,soldRoomCount=0;
       this.propertyList = propertyController.listFirstPropertyByLocation(getLocationId(),limit);
		for (PmsProperty property : propertyList) {

 			
 			if (!jsonOutput.equalsIgnoreCase(""))
 				jsonOutput += ",{";
 			else
 				jsonOutput += "{";
 			jsonOutput += "\"DT_RowId\":\"" + property.getPropertyId()+ "\"";
 			jsonOutput += ",\"city\":\"" + property.getCity() + "\"";
 			jsonOutput += ",\"propertyTypeId\":\"" + property.getPropertyType().getPropertyTypeId() + "\"";
 			jsonOutput += ",\"propertyTypeName\":\"" + property.getPropertyType().getPropertyTypeName() + "\"";
 			jsonOutput += ",\"propertyName\":\"" + (property.getPropertyName() == null ? "": property.getPropertyName().toUpperCase().trim())+ "\"";
 			jsonOutput += ",\"propertyUrl\":\"" + (property.getPropertyUrl() == null ? "": property.getPropertyUrl().toLowerCase().trim())+ "\"";
 			if(property.getLocation()!=null){
 				jsonOutput += ",\"locationName\":\"" + (property.getLocation().getLocationName() == null ? "": property.getLocation().getLocationName().toUpperCase().trim())+ "\"";	
 			}else{
 				jsonOutput += ",\"locationName\":\"" + "" + "\"";
 			}
// 			jsonOutput += ",\"propertyStandardPolicy\":\"" + (property.getPropertyStandardPolicy() == null ? "None": property.getPropertyStandardPolicy().trim())+ "\"";
// 			jsonOutput += ",\"propertyHotelPolicy\":\"" + (property.getPropertyHotelPolicy() == null ? "None": property.getPropertyHotelPolicy().trim())+ "\"";
// 			jsonOutput += ",\"propertyCancellationPolicy\":\"" + (property.getPropertyCancellationPolicy() == null ? "None": property.getPropertyCancellationPolicy().trim())+ "\"";
 			jsonOutput += ",\"payAtHotelStatus\":\"" + (property.getPayAtHotelStatus() == null ? false: property.getPayAtHotelStatus())+ "\"";
			jsonOutput += ",\"coupleStatus\":\"" + ( property.getCoupleIsActive() == null? false :  property.getCoupleIsActive() )+ "\"";
			jsonOutput += ",\"breakfastStatus\":\"" + ( property.getBreakfastIsActive() == null? false:  property.getBreakfastIsActive() )+ "\"";
			jsonOutput += ",\"hotelStatus\":\"" + ( property.getHotelIsActive() == null? false:  property.getHotelIsActive() )+ "\"";
			
 			SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
 			Timestamp FromDate=(Timestamp)mapDate.get("arrivalDate");
 			Timestamp ToDate=(Timestamp)mapDate.get("departureDate");
 			jsonOutput += ",\"arrivalDate\":\"" + format2.format(FromDate) + "\"";
 			jsonOutput += ",\"departureDate\":\"" + format2.format(ToDate)+ "\"";
 			
 			String strFromDate=format2.format(FromDate);
 			String strToDate=format2.format(ToDate);
 	    	DateTime startdate = DateTime.parse(strFromDate);
 	        DateTime enddate = DateTime.parse(strToDate);
 	        java.util.Date fromdate = startdate.toDate();
 	 		java.util.Date todate = enddate.toDate();
 	 		 
 			int diffInDays = (int) ((todate.getTime() - fromdate.getTime()) / (1000 * 60 * 60 * 24));
 			
 			List arllist=new ArrayList();
 			List arllistAvl=new ArrayList();
 			List arllistSold=new ArrayList();
 			jsonOutput += ",\"diffDays\":\"" + diffInDays+ "\"";
 			
 			String jsonPhotos="";
  			PropertyPhotoManager photoController = new PropertyPhotoManager();
			
		    response.setContentType("application/json");
		    List<PropertyPhoto> listPhotos =photoController.list(property.getPropertyId());
            
            if(listPhotos.size()>0)
  			{
            	for (PropertyPhoto photo : listPhotos) {

    				if (!jsonPhotos.equalsIgnoreCase(""))
    					jsonPhotos += ",{";
    				else
    					jsonPhotos += "{";
    				jsonPhotos += "\"DT_RowId\":\"" + photo.getPropertyPhotoId() + "\"";
    				jsonPhotos += ",\"isPrimary\":\"" + photo.getIsPrimary()+ "\"";
    				jsonPhotos += ",\"photoPath\":\"" + photo.getPhotoPath()+ "\"";
    				
    				
    				jsonPhotos += "}";


    			}
    			
                   jsonOutput += ",\"photos\":[" + jsonPhotos+ "]";
  			
  			}
 			
 			String jsonAmenities ="";
 			propertyAmenityList =  amenityController.list(property.getPropertyId());
 			if(propertyAmenityList.size()>0)
 			{
 				for (PropertyAmenity amenity : propertyAmenityList) {
 					if (!jsonAmenities.equalsIgnoreCase(""))
 						
 						jsonAmenities += ",{";
 					else
 						jsonAmenities += "{";
 					PropertyAmenity amentity1 = amenityController.find(amenity.getPropertyAmenityId());
 				    String path = "ulowebsite/images/icons/";
 					PmsAmenity pmsAmenity1 =ameController.find(amentity1.getPmsAmenity().getAmenityId());
 					jsonAmenities += "\"DT_RowId\":\"" + amenity.getPropertyAmenityId()+ "\"";					
 					jsonAmenities += ",\"amenityName\":\"" + pmsAmenity1.getAmenityName()+ "\"";
 					jsonAmenities += ",\"amenityId\":\"" + pmsAmenity1.getAmenityId()+ "\"";
                     jsonAmenities += ",\"icon\":\"" + path + pmsAmenity1.getAmenityIcon()+ "\"";
              
 					
 					jsonAmenities += "}";
 				}
 				
 				jsonOutput += ",\"amenities\":[" + jsonAmenities+ "]";
 			}
 			
 			String jsonReviews="";
			List<PropertyReviews> listReviews=reviewController.listReview(property.getPropertyId());
			if(listReviews.size()>0 && !listReviews.isEmpty()){
				for(PropertyReviews reviews:listReviews){
					
					if (!jsonReviews.equalsIgnoreCase(""))
						
						jsonReviews += ",{";
					else
						jsonReviews += "{";

					/*jsonReviews += "\"starCount\":\"" + reviews.getStarCount().intValue()+ "\"";	
					jsonReviews += ",\"reviewCount\":\"" + reviews.getReviewCount().intValue()+ "\"";
					Double averageReviews=reviews.getAverageReview();
					jsonReviews += ",\"averageReview\":\"" + df.format(averageReviews)+ "\"";*/
					
					jsonReviews += "\"starCount\":\"" + (reviews.getStarCount()==null?0:reviews.getStarCount().intValue())+ "\"";	
					jsonReviews += ",\"reviewCount\":\"" + (reviews.getReviewCount()==null?0:reviews.getReviewCount().intValue())+ "\"";	
					if(reviews.getAverageReview()!=null){
						Double averageReviews=reviews.getAverageReview();
						jsonReviews += ",\"averageReview\":\"" + df.format(averageReviews)+ "\"";
					}else{
						jsonReviews += ",\"averageReview\":\"" + df.format(0)+ "\"";
					}
					
					jsonReviews += ",\"tripadvisorStarCount\":\"" + (reviews.getTripadvisorStarCount()==null?0:reviews.getTripadvisorStarCount().intValue())+ "\"";	
					jsonReviews += ",\"tripadvisorReviewCount\":\"" + (reviews.getTripadvisorReviewCount()==null?0:reviews.getTripadvisorReviewCount().intValue())+ "\"";
					if(reviews.getTripadvisorAverageReview()!=null){
						Double taAverageReviews=reviews.getTripadvisorAverageReview();
						jsonReviews += ",\"tripadvisorAverageReview\":\"" + df.format(taAverageReviews)+ "\"";
					}else{
						jsonReviews += ",\"tripadvisorAverageReview\":\"" + df.format(0)+ "\"";
					}
					jsonReviews += ",\"googleReviewUrl\":\"" + (reviews.getGoogleReviewUrl()==null?"":reviews.getGoogleReviewUrl())+ "\"";	
					jsonReviews += ",\"tripadvisorReviewUrl\":\"" + (reviews.getTripadvisorReviewUrl()==null?"":reviews.getTripadvisorReviewUrl())+ "\"";	
					jsonReviews += ",\"propertyRating\":\"" + (reviews.getPropertyRating()==null?"None":reviews.getPropertyRating())+ "\"";
					jsonReviews += "}";
				}
				
			}else{
				if (!jsonReviews.equalsIgnoreCase(""))
					
					jsonReviews += ",{";
				else
					jsonReviews += "{";
				
				jsonReviews += "\"starCount\":\"" + 0+ "\"";	
				jsonReviews += ",\"reviewCount\":\"" + 0+ "\"";	
				jsonReviews += ",\"averageReview\":\"" + 0+ "\"";
				
				jsonReviews += ",\"tripadvisorStarCount\":\"" + 0 + "\"";	
				jsonReviews += ",\"tripadvisorReviewCount\":\"" + 0 + "\"";	
				jsonReviews += ",\"tripadvisorAverageReview\":\"" + 0+ "\"";
				
				jsonReviews += ",\"googleReviewUrl\":\"" + "None"+ "\"";	
				jsonReviews += ",\"tripadvisorReviewUrl\":\"" + "None"+ "\"";	
				jsonReviews += ",\"propertyRating\":\"" + "None"+ "\"";
				
				jsonReviews += "}";
			}
			
			jsonOutput += ",\"reviews\":[" + jsonReviews+ "]";
			
 			
 			String jsonAccommodation ="";
 			List<PropertyAccommodation> accommodationsList =  accommodationManager.list(property.getPropertyId());
 			int soldOutCount=0,availCount=0,promotionAccommId=0;
 			if(accommodationsList.size()>0 && !accommodationsList.isEmpty())
 			{
 				for (PropertyAccommodation accommodation : accommodationsList) {
 					int roomCount=0;
 					listAccommodationRoom=accommodationController.listPropertyTotalRooms(property.getPropertyId(), accommodation.getAccommodationId());
 					if(listAccommodationRoom.size()>0 && !listAccommodationRoom.isEmpty()){
 						for(AccommodationRoom roomsList:listAccommodationRoom){
 							 roomCount=(int) roomsList.getRoomCount();
 							 long minimum = getAvailableCount(property.getPropertyId(),getArrivalDate(),getDepartureDate(),roomCount,accommodation.getAccommodationId());
 							 if((int)minimum>0){
 								 availCount++;
 							 }else if((int)minimum==0){
 								 soldOutCount++;
 							 }
 							 break;
 						 }
 					 }
 					
 					
 					if (!jsonAccommodation.equalsIgnoreCase(""))
 						
 						jsonAccommodation += ",{";
 					else
 						jsonAccommodation += "{";
 						jsonAccommodation += "\"accommodationId\":\"" + accommodation.getAccommodationId()+ "\"";
 						jsonAccommodation += ",\"accommodationName\":\"" + accommodation.getAccommodationType().toUpperCase()+ "\"";
 					
 					jsonAccommodation += "}";
 				}
 				
 				jsonOutput += ",\"accommodations\":[" + jsonAccommodation+ "]";
 			}

 			
 			List<PmsPromotionDetails> listPromotionDetailCheck=null;
 			Boolean blnAvailable=false,blnSoldout=false,firstPromotionFlag=false,secondPromotionFlag=false,promotionFlag=false,blnPromoFlag=false;
 			int arlListTotalAccomm=0,soldOutTotalAccomm=0,accommCount=0;
 			String promotionType=null;
 			ArrayList<String> arlListPromoType=new ArrayList<String>();
 			List<PropertyAccommodation> accommodationList =  accommodationManager.list(property.getPropertyId());
 			if(accommodationList.size()>0)
 			{
 				for (PropertyAccommodation accommodation : accommodationList) {
 					Integer promoAccommId=0,enablePromoId=0;
 					ArrayList<Integer> arlEnablePromo=new ArrayList<Integer>();
 					Boolean blnPromotionIsNotFlag=false;
 					Boolean blnLastMinuteIsActive=false,blnLastMinuteFirstActive=false;
 					int dateCount=0,promoCount=0;
 					String strCheckPromotionType=null,promotionFirstDiscountType=null,promotionSecondDiscountType=null,promotionHoursRange=null;
 					double amount=0.0,extraAdultAmount=0.0,extraChildAmount=0.0,totalSmartPriceAmount=0.0;
 					int roomCount=0,availableCount=0,totalCount=0;
 				    double dblPercentCount=0.0,dblSmartPricePercent=0.0,
 				    		promotionFirstPercent=0.0,promotionFirstInr=0.0,
 				    		promotionSecondPercent=0.0,promotionSecondInr=0.0,totalFlatLastAmount=0.0,flatLastAmount=0.0;
 				    Timestamp lastMinStartDate=null,lastMinEndDate=null;
 				    Boolean isPositive=false,isNegative=false,blnLastMinutePromotions=false;
 				   
 	    			
 	    			listAccommodationRoom=accommodationController.listPropertyTotalRooms(property.getPropertyId(), accommodation.getAccommodationId());
 					 
 					if(listAccommodationRoom.size()>0 && !listAccommodationRoom.isEmpty()){
 						
 						for(AccommodationRoom roomsList:listAccommodationRoom){
 							roomCount=(int) roomsList.getRoomCount();
 							if(roomCount>0){
 								long minimum = getAvailableCount(property.getPropertyId(),getArrivalDate(),getDepartureDate(),roomCount,accommodation.getAccommodationId());
 								if((int)minimum>0){
 									blnAvailable=true;
 									blnSoldout=false;
 									arlListTotalAccomm++;
 								}else if((int)minimum==0){
 									blnSoldout=true;
 									blnAvailable=false;
 									soldOutTotalAccomm++;
 								}
 							}
 							 break;
 						 }
 					 }
 					 Timestamp tsPromoBookedDate=null,tsPromoStartDate=null,tsPromoEndDate=null;
 					 Timestamp tsFirstStartRange=null,tsSecondStartRange=null,tsFirstEndRange=null,tsSecondEndRange=null;
 					 Timestamp tsLastStartDate=null,tsLastEndDate=null;
 					 boolean blnFirstActivePromo=false,blnFirstNotActivePromo=false,baseFirstPromoFlag=false,blnEarlyBirdPromo=false,blnLastMinutePromo=false;
 					 boolean blnSecondActivePromo=false,blnSecondNotActivePromo=false,baseSecondPromoFlag=false;
 					 SimpleDateFormat format1=new SimpleDateFormat("yyyy-MM-dd");
 					 java.util.Date currentdate=new java.util.Date();
 					 String strCurrentDate = new SimpleDateFormat("yyyy-MM-dd").format(currentdate);
 					 java.util.Date dtecurdate = format1.parse(strCurrentDate);
				   	 Calendar calStart=Calendar.getInstance();
				   	 calStart.setTime(dtecurdate);
				   	 calStart.setTimeZone(TimeZone.getTimeZone("IST"));
				   	 String StartDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calStart.getTime());
				   	 java.util.Date curdate = format1.parse(StartDate);
 				     this.displayHoursForTimer=null;
 			   	     setDisplayHoursForTimer(this.displayHoursForTimer);
					 List<PmsPromotions> listDatePromotions= promotionController.listBookedDatePromotions(property.getPropertyId(),curdate);
					 if(listDatePromotions.size()>0 && !listDatePromotions.isEmpty()){
						for(PmsPromotions promotions:listDatePromotions){
							tsPromoBookedDate=promotions.getPromotionBookedDate();
							tsPromoStartDate=promotions.getStartDate();
							tsPromoEndDate=promotions.getEndDate();
							blnEarlyBirdPromo=checkEarlyBirdPromos(tsPromoBookedDate,tsPromoStartDate,tsPromoEndDate,getArrivalDate(),getDepartureDate());
						}
					 }else{
						 blnEarlyBirdPromo=false;
					 }
					 List<PmsPromotions> listLastPromotions= promotionController.listLastBookedPromotions(property.getPropertyId(),curdate);
					 if(listLastPromotions.size()>0 && !listLastPromotions.isEmpty()){
						for(PmsPromotions promotions:listLastPromotions){
							tsFirstStartRange=promotions.getFirstRangeStartDate();
							tsSecondStartRange=promotions.getSecondRangeStartDate();
							tsFirstEndRange=promotions.getFirstRangeEndDate();
							tsSecondEndRange=promotions.getSecondRangeEndDate();
							tsLastStartDate=promotions.getStartDate();
							tsLastEndDate=promotions.getEndDate();
							promotionHoursRange=promotions.getPromotionHoursRange();
							blnLastMinuteFirstActive=checkLastMinutePromos(promotionHoursRange,getArrivalDate(),getDepartureDate(),tsSecondStartRange,tsFirstEndRange,tsFirstEndRange,tsSecondStartRange);
//							blnLastMinuteIsActive=getCheckHoursRange(promotionHoursRange,getArrivalDate(),getDepartureDate());
							if(blnLastMinuteFirstActive){
								blnLastMinutePromo=true;
								enablePromoId=promotions.getPromotionId();
								arlEnablePromo.add(enablePromoId);
							}
						}
					 }else{
						 blnLastMinutePromo=false;
						 this.displayHoursForTimer=null;
				    	 setDisplayHoursForTimer(this.displayHoursForTimer);
					 }
 					 ArrayList<Integer> arlFirstActivePromo=new ArrayList<Integer>(); 
 					 ArrayList<Integer> arlSecondActivePromo=new ArrayList<Integer>(); 
 					 List<DateTime> between = DateUtil.getDateRange(start, end);
 					 for (DateTime d : between)
 				     {


 						 if(blnEarlyBirdPromo){
 							 List<PmsPromotions> listEarlyPromotions= promotionController.listBookedDatePromotions(property.getPropertyId(),curdate);
 							 if(listEarlyPromotions.size()>0 && !listEarlyPromotions.isEmpty()){
 								 for(PmsPromotions promos:listEarlyPromotions){
 									 if(promos.getPromotionType().equalsIgnoreCase("E")){
 	 									 if(arlFirstActivePromo.isEmpty()){
 	 										arlFirstActivePromo.add(promos.getPromotionId());
 	 									 }else if(!arlFirstActivePromo.contains(promos.getPromotionId())){
 	 										arlFirstActivePromo.add(promos.getPromotionId());
 	 									 }
 	 								 }else{
 	 									blnFirstNotActivePromo=true;
 	 								 }
 								 }
 							 }
 						 }else if(!blnEarlyBirdPromo){
 							 List<PmsPromotions> listAllPromotions= promotionController.listFlatDatePromotions(property.getPropertyId(),d.toDate());
 							 if(!listAllPromotions.isEmpty() && listAllPromotions.size()>0){
 								 for(PmsPromotions promos:listAllPromotions){
 									 if(promos.getPromotionType().equalsIgnoreCase("F")){
 	 									 if(arlFirstActivePromo.isEmpty()){
 	 										arlFirstActivePromo.add(promos.getPromotionId());
 	 									 }else if(!arlFirstActivePromo.contains(promos.getPromotionId())){
 	 										arlFirstActivePromo.add(promos.getPromotionId());
 	 									 }
 	 								 }else{
 	 									blnFirstNotActivePromo=true;
 	 								 }
 								 }
 							 } 
 						 }else{
 							 blnFirstNotActivePromo=true;
 						 }
 						 
 						if(blnLastMinutePromo){
 							List<PmsPromotions> listLastPromos= promotionController.listLastBookedPromotions(property.getPropertyId(),curdate);
 							 if(listLastPromos.size()>0 && !listLastPromos.isEmpty()){
 								for(PmsPromotions promos:listLastPromos){
 									if(promos.getPromotionType().equalsIgnoreCase("L")){
 										promoAccommId=promos.getPropertyAccommodation().getAccommodationId();
 										if(arlEnablePromo.contains(promos.getPromotionId())){
											if(arlSecondActivePromo.isEmpty()){
 		  										arlSecondActivePromo.add(promos.getPromotionId());
 		  									 }else if(!arlSecondActivePromo.contains(promos.getPromotionId())){
 		  										arlSecondActivePromo.add(promos.getPromotionId());
 		  									 }
										}
 									 }
 								}
 							 }else{
 								blnSecondNotActivePromo=true;
 							 }
						}else{
							blnSecondNotActivePromo=true;
						}
 				     }
 					 int intPromoSize=arlFirstActivePromo.size();
 					 if((arlFirstActivePromo.isEmpty() || blnFirstNotActivePromo)){
 						 blnFirstActivePromo=false;
 					 }else{
 						 blnFirstActivePromo=true;
 					 }
 					 
 					 if(arlSecondActivePromo.isEmpty() || blnSecondNotActivePromo){
 						blnSecondActivePromo=false;
 					 }else{
 						blnSecondActivePromo=true;
 					 }
 					 
 					
 				     for (DateTime d : between)
 				     {
 				    	 Integer promotionId=0;
 				    	 if(blnFirstActivePromo){
 				    		if(blnEarlyBirdPromo){
 				    			List<PmsPromotions> listEarlyPromotions= promotionController.listBookedDatePromotions(property.getPropertyId(),curdate);
 	  							 if(listEarlyPromotions.size()>0 && !listEarlyPromotions.isEmpty()){
 	  				    			for(PmsPromotions promotions:listEarlyPromotions){
 	  				    				if(promotions.getPromotionType().equalsIgnoreCase("E")){
 	 					    				promotionId=promotions.getPromotionId();
 	 					    				promotionFirstDiscountType=promotions.getPromotionDiscountType();
 	 					    				listPromotionDetailCheck=promotionDetailController.listPromotionDetails(promotions.getPromotionId(),f.format(d.toDate()).toLowerCase());
 	 					    				 for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
 	 					    					if(promoCount==0){
 	 					    						promotionFirstPercent=promoDetails.getPromotionPercentage();
 	 	 					    					promotionFirstInr=promoDetails.getPromotionInr();
 	 	 					    					promotionType=promotions.getPromotionType();
 	 	 					    					if(promotionType.equalsIgnoreCase("E")){
 	 	 					    						strCheckPromotionType="E";
 	 	 					    					}
 	 	 					    					
 	 		  						    			arlListPromoType.add(promotionType);
 	 					    					}
 	 					    					promoCount++;
 	 					    				 }
 	  				    				}
 	  				    			}
 	  				    			promotionFlag=true; 
 			    					blnPromoFlag=true;
 	  				    		}else{
 	  				    			promotionFlag=false;
 	  	  		    				promotionType=null;
 	  	  		    				blnPromoFlag=false;
 	  	  		    				promotionId=null;
 	  	  		    				baseFirstPromoFlag=true;
 	  				    		}
 				    		}else if(!blnEarlyBirdPromo){
 				    			List<PmsPromotions> listPromotions= promotionController.listFlatDatePromotions(property.getPropertyId(),d.toDate());
 	  				    		if(listPromotions.size()>0 && !listPromotions.isEmpty()){
 	  				    			for(PmsPromotions promotions:listPromotions){
 	  				    				if(promotions.getPromotionType().equalsIgnoreCase("F")){
 	 					    				promotionId=promotions.getPromotionId();
 	 					    				promotionFirstDiscountType=promotions.getPromotionDiscountType();
 	 					    				listPromotionDetailCheck=promotionDetailController.listPromotionDetails(promotions.getPromotionId(),f.format(d.toDate()).toLowerCase());
 	 					    				 for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
 	 					    					if(promoCount==0){
 	 					    						promotionFirstPercent=promoDetails.getPromotionPercentage();
 	 	 					    					promotionFirstInr=promoDetails.getPromotionInr();
 	 	 					    					promotionType=promotions.getPromotionType();
 	 	 					    					if(promotionType.equalsIgnoreCase("F")){
 	 	 					    						strCheckPromotionType="F";
 	 	 					    					}
 	 	 					    					
 	 		  						    			arlListPromoType.add(promotionType);
 	 					    					}
 	 					    					promoCount++;
 	 					    				 }
 	  				    				}
 	  				    			}
 	  				    			promotionFlag=true; 
 			    					blnPromoFlag=true;
 	  				    		}
 				    		}
 				    	 }
 				    	 if(blnSecondActivePromo){
 				    		List<PmsPromotions> listPmsPromotions= promotionController.listLastDatePromotions(property.getPropertyId(),accommodation.getAccommodationId(),curdate);
				    			 if(listPmsPromotions.size()>0 &&!listPmsPromotions.isEmpty()){
				    				 for(PmsPromotions pmsPromotions:listPmsPromotions){
				    					 if(pmsPromotions.getPromotionType().equalsIgnoreCase("L")){
				    						 promotionId=pmsPromotions.getPromotionId();
							    			 promoAccommId=pmsPromotions.getPropertyAccommodation().getAccommodationId();
							    			 Timestamp tsStartDate=null,tsEndDate=null;
							    			 tsStartDate=pmsPromotions.getStartDate();
							    			 tsEndDate=pmsPromotions.getEndDate();
							    			 
							    			 promotionHoursRange=pmsPromotions.getPromotionHoursRange();
							    			 if(arlEnablePromo.contains(pmsPromotions.getPromotionId())){
							    				 promotionAccommId=promoAccommId.intValue();
							    				 long minimumCount = getAvailableCount(property.getPropertyId(),getArrivalDate(),getDepartureDate(),roomCount,accommodation.getAccommodationId());
				    			    			 if((int)minimumCount>0){
				    			    				 promotionFlag=true; 
			    			    					 blnPromoFlag=true;
			    			    					 strCheckPromotionType="L";
			    			    					 promotionType=pmsPromotions.getPromotionType();
   	 		  						    		 arlListPromoType.add(promotionType);
			    			    					 promotionSecondDiscountType=pmsPromotions.getPromotionDiscountType();
   	 					    					 listPromotionDetailCheck=promotionDetailController.listPromotionDetails(pmsPromotions.getPromotionId(),f.format(curdate).toLowerCase());
   	 					    					 if(listPromotionDetailCheck.size()>0 && !listPromotionDetailCheck.isEmpty()){
   	 					    						for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
		    	 					    					promotionSecondPercent=promoDetails.getPromotionPercentage();
	    	 	 					    					promotionSecondInr=promoDetails.getPromotionInr();
		    	 					    				 }
   	 					    					 }
				    			    			 }else{
				    			    				 promotionFlag=false;
				    			    				 promotionType=null;
				    			    				 blnPromoFlag=false;
				    			    				 baseSecondPromoFlag=true;
				    			    			 }
							    			 }
				    					 }
				    					 
				    				 }
				    			 }
 				    	 }
 				    	 if(promotionFlag){
 				    		int rateCount=0,rateIdCount=0;
   						List<PropertyRate> propertyRateList =  rateController.listAllDates(property.getPropertyId(),accommodation.getAccommodationId(),getSourceTypeId(),d.toDate());
   				    	if(propertyRateList.size()>0)
   				    	{
   				    		 for (PropertyRate pr : propertyRateList)
   			    			 {
				    				 int propertyRateId = pr.getPropertyRateId();
					    			 List<PropertyRateDetail> rateDetailList =   detailController.list(propertyRateId,f.format(d.toDate()).toLowerCase());
	    				    		 if(rateDetailList.size()>0){
	    				    			 if(rateCount<1){
	    				    				 for (PropertyRateDetail rate : rateDetailList){
	    				    					 amount+=  rate.getBaseAmount();
	        				    			 }
	    				    				 rateCount++;
	    				    			 }
	    				    		 }else{
	    				    			 if(propertyRateList.size()==rateIdCount){
	    				    				 amount+= accommodation.getBaseAmount();
	    				    			 }
	        				    	 }
   			    			 }	    		 
   				    	 }
   				    	 else{
   				    		 amount+= accommodation.getBaseAmount();
   				    	 }
   				    	 double discountAmount=0;
 				    		 if(blnFirstActivePromo){
  	    				    	String strTypePercent=String.valueOf(promotionFirstPercent);
  	    				    	String strTypeInr=String.valueOf(promotionFirstInr);
					    		  if(!strTypePercent.equalsIgnoreCase("null") && promotionFirstPercent!=0){
					    			  flatLastAmount = amount-(amount*promotionFirstPercent/100);
					    			  discountAmount+=amount*promotionFirstPercent/100;
					    		  }
					    		  else if(!strTypeInr.equalsIgnoreCase("null") && promotionFirstInr!=0){
					    			  flatLastAmount = amount-promotionFirstInr;
					    			  discountAmount+=promotionFirstInr;
					    		  }
					    		  else{
					    			  flatLastAmount = amount;
					    		  } 
 				    		 }else{
				    			  flatLastAmount = amount;
				    		  } 
 				    		 if(blnSecondActivePromo){
 				    			String strTypePercent=String.valueOf(promotionSecondPercent);
  	    				    	String strTypeInr=String.valueOf(promotionSecondInr);
					    		  if(!strTypePercent.equalsIgnoreCase("null") && promotionSecondPercent!=0){
					    			  flatLastAmount = flatLastAmount-(flatLastAmount*promotionSecondPercent/100);
					    			  discountAmount+=amount*promotionFirstPercent/100;
					    		  }
					    		  else if(!strTypeInr.equalsIgnoreCase("null") && promotionSecondInr!=0){
					    			  flatLastAmount = flatLastAmount-promotionSecondInr;
					    			  discountAmount +=promotionSecondInr;
					    		  }
					    		  else{
					    			  flatLastAmount = flatLastAmount;
					    		  }
 				    		 }else{
				    			  flatLastAmount = flatLastAmount;
				    		  }
 				    		totalFlatLastAmount=amount-discountAmount;
 				    		 
 				    	 }else{
 				    		 if(baseFirstPromoFlag){
 				    			amount += accommodation.getBaseAmount();
 				    		}else{
 				    			int rateCount=0,rateIdCount=0;
 	    						List<PropertyRate> propertyRateList =  rateController.listAllDates(property.getPropertyId(),accommodation.getAccommodationId(),getSourceTypeId(),d.toDate());
 	    				    	if(propertyRateList.size()>0)
 	    				    	{
 	    				    		 for (PropertyRate pr : propertyRateList)
 	    			    			 {
 					    				 int propertyRateId = pr.getPropertyRateId();
						    			 List<PropertyRateDetail> rateDetailList =   detailController.list(propertyRateId,f.format(d.toDate()).toLowerCase());
	        				    		 if(rateDetailList.size()>0){
	        				    			 if(rateCount<1){
	        				    				 for (PropertyRateDetail rate : rateDetailList){
	        				    					 amount +=  rate.getBaseAmount();
		        				    			 }
	        				    				 rateCount++;
	        				    			 }
	        				    		 }else{
	        				    			 if(propertyRateList.size()==rateIdCount){
	        				    				 amount += accommodation.getBaseAmount();
	        				    			 }
	            				    	 }
 	    			    			 }	    		 
 	    				    	 }
 	    				    	 else{
 	    				    		 amount += accommodation.getBaseAmount();
 	    				    	 }
 				    		}
 				    	 }
 				    	 dateCount++;
 				    	 if(dateCount==diffInDays){
 				    		 if(blnAvailable){
 				    			if(promotionType!=null && strCheckPromotionType!=null) {
 				    				if(strCheckPromotionType.equalsIgnoreCase("L") || strCheckPromotionType.equalsIgnoreCase("F") || strCheckPromotionType.equalsIgnoreCase("E")){
			    						mapAmountAccommId.put(totalFlatLastAmount,accommodation.getAccommodationId());
 						      			arllist.add(totalFlatLastAmount);
 					    			}
 					    			
 				    			}else{
 				    				mapAmountAccommId.put(amount,accommodation.getAccommodationId());
 					      			arllist.add(amount);
 				    			}
 		      				 }else if(blnSoldout){
 		      					if(promotionType!=null && strCheckPromotionType!=null) {
	  		      					if(strCheckPromotionType.equalsIgnoreCase("L") || strCheckPromotionType.equalsIgnoreCase("F") || strCheckPromotionType.equalsIgnoreCase("E")){
	  		      						mapSoldOutAmountAccommId.put(totalFlatLastAmount,accommodation.getAccommodationId());
						      			arllist.add(totalFlatLastAmount);
					    			}
 		      					}else{
 				    				mapSoldOutAmountAccommId.put(amount,accommodation.getAccommodationId());
 					      			arllist.add(amount);
 				    			}
 					    	 }
 				    	 }
 				     }
 				     if(blnAvailable){
 				    	 if(promotionType!=null && strCheckPromotionType!=null) {
 				    		if(strCheckPromotionType.equalsIgnoreCase("L") || strCheckPromotionType.equalsIgnoreCase("F") || strCheckPromotionType.equalsIgnoreCase("E")){
 				    			arllistAvl.add(totalFlatLastAmount);
			    			} 
 				    	 }else{
 		    				arllistAvl.add(amount);
 		    			}
 				     }else if(!blnAvailable){
 				    	 if(promotionType!=null && strCheckPromotionType!=null) {
 				    		if(strCheckPromotionType.equalsIgnoreCase("L") || strCheckPromotionType.equalsIgnoreCase("F") || strCheckPromotionType.equalsIgnoreCase("E")){
 					    		 arllistSold.add(totalFlatLastAmount);
 			    			}
 				    	 }else{
 		    				arllistSold.add(amount);
 		    			}
 				     }
 				     accommCount++;
 				}
 			}
 			//end of accommodation
 			double totalAmount=0;
 			boolean blnJSONContent=false;
 			Collections.sort(arllist);
 			int arlListSize=0,accommId=0,intTotalAccomm=0;
 		     intTotalAccomm=accommodationList.size();
 		     if(intTotalAccomm==accommCount){
 		    	 if(intTotalAccomm==soldOutCount){
 			    	 if(blnPromoFlag){
 				    	 if(arlListPromoType.contains("F") || arlListPromoType.contains("E")){
 				    		 arlListSize=arllist.size();
 						     if(soldOutCount==arlListSize){
 						    	 Collections.sort(arllist);
 						    	 for(int i=0;i<arlListSize;i++){
 							    	 totalAmount=(Double) arllist.get(i);
 							    	 if((Double) arllist.get(0)==(Double) arllist.get(i)){
 							    		 if(blnAvailable){
 							    			 accommId=mapAmountAccommId.get(totalAmount);
 								    		 blnJSONContent=true;
 							    		 }else{
 							    			 accommId=mapSoldOutAmountAccommId.get(totalAmount);
 								    		 blnJSONContent=true;
 							    		 }
 							    		 
 							    		 break;
 							    	 }else{
 							    		 
 							    	 }
 							     }
 						     }
 				    	 }else if(arlListPromoType.contains("L")){
 				    		 /*if(promotionAccommId>0){
 					    		 accommId=promotionAccommId;
 					    		 blnJSONContent=true;
 					    	 }*/
 				    		arlListSize=arllist.size();
	 					     Collections.sort(arllist);
	 				    	 for(int i=0;i<arlListSize;i++){
	 				    		 if(arllistAvl.contains((Double) arllist.get(i))){
	 				    			 if((Double) arllist.get(0)==(Double) arllist.get(i)){
	 					    			 totalAmount=(Double) arllist.get(i);
	 					    			 accommId=mapAmountAccommId.get(totalAmount);
	 						    		 blnJSONContent=true;
	 				    			 }else{
	 				    				 totalAmount=(Double) arllist.get(i);
	 					    			 accommId=mapAmountAccommId.get(totalAmount);
	 						    		 blnJSONContent=true;
	 				    			 }
	 				    			 break;
	 				    		 }
	 					     }
 				    	 }
 				     }else{
 					     arlListSize=arllist.size();
 					     if(soldOutCount==arlListSize){
 					    	 Collections.sort(arllist);
 					    	 for(int i=0;i<arlListSize;i++){
 						    	 totalAmount=(Double) arllist.get(i);
 						    	 if((Double) arllist.get(0)==(Double) arllist.get(i)){
 						    		 if(blnAvailable){
 						    			 accommId=mapAmountAccommId.get(totalAmount);
 							    		 blnJSONContent=true;
 						    		 }else{
 						    			 accommId=mapSoldOutAmountAccommId.get(totalAmount);
 							    		 blnJSONContent=true;
 						    		 }
 						    		 break;
 						    	 }else{
 						    		 
 						    	 }
 						     }
 					     }
 					     
 				     }
 			     }else{
 			    	 if(arlListPromoType.size()>0){
 			    		 if(arlListPromoType.contains("F") || arlListPromoType.contains("E")){
// 				    	 if(promotionType.equalsIgnoreCase("F") || promotionType.equalsIgnoreCase("L")){
 				    		 arlListSize=arllist.size();
 				    		 Collections.sort(arllist);
 					    	 for(int i=0;i<arlListSize;i++){
 					    		 if(arllistAvl.contains((Double) arllist.get(i))){
 					    			 if((Double) arllist.get(0)==(Double) arllist.get(i)){
 						    			 totalAmount=(Double) arllist.get(i);
 						    			 accommId=mapAmountAccommId.get(totalAmount);
 							    		 blnJSONContent=true;
 					    			 }else{
 					    				 totalAmount=(Double) arllist.get(i);
 						    			 accommId=mapAmountAccommId.get(totalAmount);
 							    		 blnJSONContent=true;
 					    			 }
 					    			 break;
 					    		 }
 						     }
 				    	 }else if( arlListPromoType.contains("L")){
 				    		 /*if(promotionAccommId>0){
 					    		 accommId=promotionAccommId;
 					    		 blnJSONContent=true;
 					    	 } */
 				    		arlListSize=arllist.size();
	 					     Collections.sort(arllist);
	 				    	 for(int i=0;i<arlListSize;i++){
	 				    		 if(arllistAvl.contains((Double) arllist.get(i))){
	 				    			 if((Double) arllist.get(0)==(Double) arllist.get(i)){
	 					    			 totalAmount=(Double) arllist.get(i);
	 					    			 accommId=mapAmountAccommId.get(totalAmount);
	 						    		 blnJSONContent=true;
	 				    			 }else{
	 				    				 totalAmount=(Double) arllist.get(i);
	 					    			 accommId=mapAmountAccommId.get(totalAmount);
	 						    		 blnJSONContent=true;
	 				    			 }
	 				    			 break;
	 				    		 }
	 					     }
 				    	 }
 				     }else{
 					     arlListSize=arllist.size();
 					     Collections.sort(arllist);
 				    	 for(int i=0;i<arlListSize;i++){
 				    		 if(arllistAvl.contains((Double) arllist.get(i))){
 				    			 if((Double) arllist.get(0)==(Double) arllist.get(i)){
 					    			 totalAmount=(Double) arllist.get(i);
 					    			 accommId=mapAmountAccommId.get(totalAmount);
 						    		 blnJSONContent=true;
 				    			 }else{
 				    				 totalAmount=(Double) arllist.get(i);
 					    			 accommId=mapAmountAccommId.get(totalAmount);
 						    		 blnJSONContent=true;
 				    			 }
 				    			 break;
 				    		 }
 					     }
 				     }
 			     }
 		     }
 		     arlListPromoType.clear();  
 			if(blnJSONContent){
 				int promoCount=0,enablePromoId=0;
 				ArrayList<Integer> arlEnablePromo=new ArrayList<Integer>();
 			    listAccommodationRoom.clear(); 
 			    ArrayList<String> arlPromoType=new ArrayList<String>();
 		    	listAccommodationRoom=accommodationController.listPropertyTotalRooms(property.getPropertyId(), accommId);
 		    	List<DateTime> betweenAmount = DateUtil.getDateRange(start, end);
 		    	double actualPromoBaseAmount=0.0,actualBaseAmount=0.0,actualTotalAmount=0.0,actualTotalAdultAmount=0.0,actualTotalChildAmount=0.0,
 		    			actualAdultAmount=0.0,actualChildAmount=0.0,promotionFirstPercent=0.0,promotionFirstInr=0.0,
 	  				    		promotionSecondPercent=0.0,promotionSecondInr=0.0;
 			    int countDate=0,promotionId=0,promotionHours=0,promoAccommId=0; 
 			    boolean blnStatus=false,isPositive=false,isNegative=false,
 			    		promoFlag=false,blnBookedGetRooms=false,
 			    		blnLastMinutePromotions=false,blnLastMinuteIsActive=false,blnLastMinuteFirstActive=false;
 			    String promotionFirstName=null,promotionSecondName=null,promotionHoursRange=null,
 			    		promotionSecondDiscountType=null,promotionFirstDiscountType=null,strCheckPromotionType=null;
 			    Integer promoFirstPercent=null,promoFirstInr=null,promoSecondInr=null,promoSecondPercent=null;
 			    double promoBaseAmount=0.0,promoAdultAmount=0.0,promoChildAmount=0.0,smartPricePercent=0.0;
 			    double baseAmount=0.0,extraAdultAmount=0.0,extraChildAmount=0.0,totalBaseAmount=0.0,totalSmartPriceAmount=0.0,totalFlatAmount=0.0,totalLastAmount=0.0;
 			    int roomCount=0,availableCount=0,totalCount=0,discountId=0;
 			    double dblPercentCount=0.0,dblSmartPricePercent=0.0, flatAmount=0.0,lastAmount=0.0;
 			    double dblPercentFrom=0.0,dblPercentTo=0.0,lastMinuteHours=0.0;
 			    Timestamp lastMinStartDate=null,lastMinEndDate=null;
 				if(listAccommodationRoom.size()>0 && !listAccommodationRoom.isEmpty()){
 					for(AccommodationRoom roomsList:listAccommodationRoom){
 						 roomCount=(int) roomsList.getRoomCount();
 						 break;
 					 }
 				 }
 				 Timestamp tsPromoBookedDate=null,tsPromoStartDate=null,tsPromoEndDate=null;
 				 Timestamp tsFirstStartRange=null,tsSecondStartRange=null,tsFirstEndRange=null,tsSecondEndRange=null;
				 Timestamp tsLastStartDate=null,tsLastEndDate=null;
 				 boolean blnLastMinutePromo=false,blnEarlyBirdPromo=false,blnFirstActivePromo=false,blnFirstNotActivePromo=false,basePromoFlag=false;
				 boolean blnSecondActivePromo=false,blnSecondNotActivePromo=false;
				 SimpleDateFormat format1=new SimpleDateFormat("yyyy-MM-dd");
				 java.util.Date currentdate=new java.util.Date();
				 String strCurrentDate = new SimpleDateFormat("yyyy-MM-dd").format(currentdate);
				 java.util.Date dtecurdate = format1.parse(strCurrentDate);
			   	 Calendar calStart=Calendar.getInstance();
			   	 calStart.setTime(dtecurdate);
			   	 calStart.setTimeZone(TimeZone.getTimeZone("IST"));
			   	 String StartDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calStart.getTime());
			   	 java.util.Date curdate = format1.parse(StartDate);
			   	 this.displayHoursForTimer=null;
			   	 setDisplayHoursForTimer(this.displayHoursForTimer);
				 List<PmsPromotions> listDatePromotions= promotionController.listBookedDatePromotions(property.getPropertyId(),curdate);
				 if(listDatePromotions.size()>0 && !listDatePromotions.isEmpty()){
					for(PmsPromotions promotions:listDatePromotions){
						tsPromoBookedDate=promotions.getPromotionBookedDate();
						tsPromoStartDate=promotions.getStartDate();
						tsPromoEndDate=promotions.getEndDate();
						blnEarlyBirdPromo=checkEarlyBirdPromos(tsPromoBookedDate,tsPromoStartDate,tsPromoEndDate,getArrivalDate(),getDepartureDate());
					}
				 }else{
					 blnEarlyBirdPromo=false;
				 }
				 
				 List<PmsPromotions> listLastPromotions= promotionController.listLastBookedPromotions(property.getPropertyId(),curdate);
				 if(listLastPromotions.size()>0 && !listLastPromotions.isEmpty()){
					for(PmsPromotions promotions:listLastPromotions){
						tsFirstStartRange=promotions.getFirstRangeStartDate();
						tsSecondStartRange=promotions.getSecondRangeStartDate();
						tsFirstEndRange=promotions.getFirstRangeEndDate();
						tsSecondEndRange=promotions.getSecondRangeEndDate();
						tsLastStartDate=promotions.getStartDate();
						tsLastEndDate=promotions.getEndDate();
						promotionHoursRange=promotions.getPromotionHoursRange();
						blnLastMinuteFirstActive=checkLastMinutePromos(promotionHoursRange,getArrivalDate(),getDepartureDate(),tsSecondStartRange,tsFirstEndRange,tsFirstEndRange,tsSecondStartRange);
//						blnLastMinuteIsActive=getCheckHoursRange(promotionHoursRange,getArrivalDate(),getDepartureDate());
						if(blnLastMinuteFirstActive){
							blnLastMinutePromo=true;
							enablePromoId=promotions.getPromotionId();
							arlEnablePromo.add(enablePromoId);
						}
					}
				 }else{
					 blnLastMinutePromo=false;
					 this.displayHoursForTimer=null;
			    	 setDisplayHoursForTimer(this.displayHoursForTimer);
				 }
				 ArrayList<Integer> arlFirstActivePromo=new ArrayList<Integer>(); 
				 ArrayList<Integer> arlSecondActivePromo=new ArrayList<Integer>(); 
				 List<DateTime> between = DateUtil.getDateRange(start, end);
				 for (DateTime d : between)
			     {
					 if(blnEarlyBirdPromo){
						 List<PmsPromotions> listEarlyPromotions= promotionController.listBookedDatePromotions(property.getPropertyId(),curdate);
						 if(listEarlyPromotions.size()>0 && !listEarlyPromotions.isEmpty()){
							 for(PmsPromotions promos:listEarlyPromotions){
								 if(promos.getPromotionType().equalsIgnoreCase("E")){
									 if(arlFirstActivePromo.isEmpty()){
										arlFirstActivePromo.add(promos.getPromotionId());
									 }else if(!arlFirstActivePromo.contains(promos.getPromotionId())){
										arlFirstActivePromo.add(promos.getPromotionId());
									 }
								 }else{
									blnFirstNotActivePromo=true;
								 }
							 }
						 }
					 }else if(!blnEarlyBirdPromo){
						 List<PmsPromotions> listAllPromotions= promotionController.listFlatDatePromotions(property.getPropertyId(),d.toDate());
						 if(!listAllPromotions.isEmpty() && listAllPromotions.size()>0){
							 for(PmsPromotions promos:listAllPromotions){
								 if(promos.getPromotionType().equalsIgnoreCase("F")){
									 if(arlFirstActivePromo.isEmpty()){
										arlFirstActivePromo.add(promos.getPromotionId());
									 }else if(!arlFirstActivePromo.contains(promos.getPromotionId())){
										arlFirstActivePromo.add(promos.getPromotionId());
									 }
								 }else{
									blnFirstNotActivePromo=true;
								 }
							 }
						 } 
					 }else{
						 blnFirstNotActivePromo=true;
					 }
					 
					 if(blnLastMinutePromo){
							List<PmsPromotions> listLastPromos= promotionController.listLastBookedPromotions(property.getPropertyId(),curdate);
							 if(listLastPromos.size()>0 && !listLastPromos.isEmpty()){
								for(PmsPromotions promos:listLastPromos){
									if(promos.getPromotionType().equalsIgnoreCase("L")){
										promoAccommId=promos.getPropertyAccommodation().getAccommodationId();
										if(arlEnablePromo.contains(promos.getPromotionId())){
											if(arlSecondActivePromo.isEmpty()){
		  										arlSecondActivePromo.add(promos.getPromotionId());
		  									 }else if(!arlSecondActivePromo.contains(promos.getPromotionId())){
		  										arlSecondActivePromo.add(promos.getPromotionId());
		  									 }
										}
									 }
								}
							 }else{
								blnSecondNotActivePromo=true;
							 }
						}else{
							blnSecondNotActivePromo=true;
						}
					 
			     }
				 int intPromoSize=arlFirstActivePromo.size();
				 if((arlFirstActivePromo.isEmpty() || blnFirstNotActivePromo)){
					 blnFirstActivePromo=false;
				 }else{
					 blnFirstActivePromo=true;
				 }
				 
				 if(arlSecondActivePromo.isEmpty() || blnSecondNotActivePromo){
					blnSecondActivePromo=false;
				 }else{
					 blnSecondActivePromo=true;
				 }
 				
 				List<PropertyAccommodation> listAccommodation =  accommodationManager.list(property.getPropertyId(),accommId);
 				
 				for (PropertyAccommodation accommodations : listAccommodation) {
 					for (DateTime d : betweenAmount)
 				    {
 						boolean blnFlatPromotions=false;
 						if(blnFirstActivePromo){
 							if(blnEarlyBirdPromo){
 								List<PmsPromotions> listEarlyPromotions= promotionController.listBookedDatePromotions(property.getPropertyId(),curdate);
 								 if(listEarlyPromotions.size()>0 && !listEarlyPromotions.isEmpty()){
 	  					    		for(PmsPromotions promotions:listEarlyPromotions){
 	  					    			 if(promotions.getPromotionType().equalsIgnoreCase("E")){
 	  					    				 promotionId=promotions.getPromotionId();
 	  					    				 promotionType=promotions.getPromotionType();
 	  					    				 promotionFirstDiscountType=promotions.getPromotionDiscountType();
 	  					    				 listPromotionDetailCheck=promotionDetailController.listPromotionDetails(promotions.getPromotionId(),f.format(d.toDate()).toLowerCase());
 	  					    				 for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
 	  				    						if(promoCount==0){
 	  				    							promotionFirstPercent=promoDetails.getPromotionPercentage();
 	 	 					    					promotionFirstInr=promoDetails.getPromotionInr();
 	 	 					    					promotionType=promotions.getPromotionType();
 	  	  				    						 promotionFlag=true;
 	  	  						    				 blnPromoFlag=true;
 	  	  						    				 arlPromoType.add(promotionType);
 	  				    						}
 	  				    						promoCount++;
 	  				    					 }
 	  					    			 }
 	  					    		}
 	  					    	}
 	  						
 							}else if(!blnEarlyBirdPromo){

 	  							List<PmsPromotions> listFlatPromotions= promotionController.listFlatDatePromotions(property.getPropertyId(),d.toDate());
 	  					    	if(listFlatPromotions.size()>0 && !listFlatPromotions.isEmpty()){
 	  					    		for(PmsPromotions promotions:listFlatPromotions){
 	  					    			 if(promotions.getPromotionType().equalsIgnoreCase("F")){
 	  					    				 promotionId=promotions.getPromotionId();
 	  					    				 promotionType=promotions.getPromotionType();
 	  					    				 promotionFirstDiscountType=promotions.getPromotionDiscountType();
 	  					    				 listPromotionDetailCheck=promotionDetailController.listPromotionDetails(promotions.getPromotionId(),f.format(d.toDate()).toLowerCase());
 	  					    				 for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
 	  				    						if(promoCount==0){
 	  				    							promotionFirstPercent=promoDetails.getPromotionPercentage();
 	 	 					    					promotionFirstInr=promoDetails.getPromotionInr();
 	 	 					    					promotionType=promotions.getPromotionType();
 	  	  				    						 promotionFlag=true;
 	  	  						    				 blnPromoFlag=true;
 	  	  						    				 arlPromoType.add(promotionType);
 	  				    						}
 	  				    						promoCount++;
 	  				    					 }
 	  					    			 }
 	  					    		}
 	  					    	}
 	  					    	
 	  						
 							}
 						}
 						if(blnSecondActivePromo){

				    		List<PmsPromotions> listPmsPromotions= promotionController.listLastDatePromotions(property.getPropertyId(),accommId,curdate);
				    			 if(listPmsPromotions.size()>0 &&!listPmsPromotions.isEmpty()){
				    				 for(PmsPromotions pmsPromotions:listPmsPromotions){
				    					 promotionId=pmsPromotions.getPromotionId();
						    			 promoAccommId=pmsPromotions.getPropertyAccommodation().getAccommodationId();
						    			 promotionHoursRange=pmsPromotions.getPromotionHoursRange();
						    			 Timestamp tsStartDate=null,tsEndDate=null;
						    			 tsStartDate=pmsPromotions.getStartDate();
						    			 tsEndDate=pmsPromotions.getEndDate();
						    			 
						    			 if(arlEnablePromo.contains(pmsPromotions.getPromotionId())){
						    				 promotionAccommId=promoAccommId;
						    				 long minimumCount = getAvailableCount(property.getPropertyId(),getArrivalDate(),getDepartureDate(),roomCount,accommId);
			    			    			 if((int)minimumCount>0){
		    			    					 promotionFlag=true; 
		    			    					 blnPromoFlag=true;
		    			    					 strCheckPromotionType="L";
		    			    					 promotionType=pmsPromotions.getPromotionType();
		    			    					 arlPromoType.add(promotionType);
		    			    					 promotionSecondDiscountType=pmsPromotions.getPromotionDiscountType();
	 					    					 listPromotionDetailCheck=promotionDetailController.listPromotionDetails(pmsPromotions.getPromotionId(),f.format(curdate).toLowerCase());
	 					    					 if(listPromotionDetailCheck.size()>0 && !listPromotionDetailCheck.isEmpty()){
	 					    						for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
	    	 					    					promotionSecondPercent=promoDetails.getPromotionPercentage();
   	 	 					    					promotionSecondInr=promoDetails.getPromotionInr();
	    	 					    				 }
	 					    					 }
			    			    			 }else{
			    			    				 promotionFlag=false;
			    			    				 promotionType=null;
			    			    				 blnPromoFlag=false;
			    			    				 basePromoFlag=true;
			    			    			 }
						    			 }
				    				 }
				    			 }
 						}
 						if(promotionFlag){
 							int rateCount=0,rateIdCount=0;
   						List<PropertyRate> propertyRateList =  rateController.listAllDates(property.getPropertyId(),accommodations.getAccommodationId(),getSourceTypeId(),d.toDate());
   				    	if(propertyRateList.size()>0)
   				    	{
   				    		 for (PropertyRate pr : propertyRateList)
   			    			 {
				    				 int propertyRateId = pr.getPropertyRateId();
					    			 List<PropertyRateDetail> rateDetailList =   detailController.list(propertyRateId,f.format(d.toDate()).toLowerCase());
	    				    		 if(rateDetailList.size()>0){
	    				    			 if(rateCount<1){
	    				    				 for (PropertyRateDetail rate : rateDetailList){
	    				    					 baseAmount+=  rate.getBaseAmount();
	    				    					 extraAdultAmount+=rate.getExtraAdult();
	    	  				    				 extraChildAmount+=rate.getExtraChild();
	        				    			 }
	    				    				 rateCount++;
	    				    			 }
	    				    		 }else{
	    				    			 if(propertyRateList.size()==rateIdCount){
	    				    				 baseAmount += accommodations.getBaseAmount();
	    				    				 extraAdultAmount+=accommodations.getExtraAdult();
	      				    				 extraChildAmount+=accommodations.getExtraChild();
	    				    			 }
	        				    	 }
   			    			 }	    		 
   				    	 }
   				    	 else{
   				    		baseAmount += accommodations.getBaseAmount();
   				    		extraAdultAmount+=accommodations.getExtraAdult();
			    				extraChildAmount+=accommodations.getExtraChild();
   				    	 }
   				    	 double discountAmount=0;
				    		 if(blnFirstActivePromo){
 	    				    	  String strTypePercent=String.valueOf(promotionFirstPercent);
 	    				    	  String strTypeInr=String.valueOf(promotionFirstInr);
					    		  if(!strTypePercent.equalsIgnoreCase("null") && promotionFirstPercent!=0){
					    			  actualPromoBaseAmount = baseAmount-(baseAmount*promotionFirstPercent/100);
					    			  discountAmount+=baseAmount*promotionFirstPercent/100;
					    		  }
					    		  else if(!strTypeInr.equalsIgnoreCase("null") && promotionFirstInr!=0){
					    			  actualPromoBaseAmount = baseAmount-promotionFirstInr;
					    			  discountAmount+=promotionFirstInr;
					    		  }
					    		  else{
					    			  actualPromoBaseAmount = baseAmount;
					    		  } 
				    		 }else{
				    			  actualPromoBaseAmount = baseAmount;
				    		  } 
				    		 if(blnSecondActivePromo){
				    			String strTypePercent=String.valueOf(promotionSecondPercent);
 	    				    	String strTypeInr=String.valueOf(promotionSecondInr);
					    		  if(!strTypePercent.equalsIgnoreCase("null") && promotionSecondPercent!=0){
					    			  actualPromoBaseAmount = actualPromoBaseAmount-(actualPromoBaseAmount*promotionSecondPercent/100);
					    			  discountAmount+=baseAmount*promotionSecondPercent/100;
					    		  }
					    		  else if(!strTypeInr.equalsIgnoreCase("null") && promotionSecondInr!=0){
					    			  actualPromoBaseAmount= actualPromoBaseAmount-promotionSecondInr;
					    			  discountAmount+=promotionSecondInr;
					    		  }
					    		  else{
					    			  actualPromoBaseAmount = actualPromoBaseAmount;
					    		  }
				    		 }else{
				    			  actualPromoBaseAmount = actualPromoBaseAmount;
				    		  }
				    		actualBaseAmount=baseAmount;
				    		totalFlatAmount=baseAmount-discountAmount;
				    		 
				    	 }else{
				    		 if(basePromoFlag){
				    			baseAmount += accommodations.getBaseAmount();
				    			extraAdultAmount+=accommodations.getExtraAdult();
				    			extraChildAmount+=accommodations.getExtraChild();
				    		}else{
				    			int rateCount=0,rateIdCount=0;
	    						List<PropertyRate> propertyRateList =  rateController.listAllDates(property.getPropertyId(),accommodations.getAccommodationId(),getSourceTypeId(),d.toDate());
	    				    	if(propertyRateList.size()>0)
	    				    	{
	    				    		 for (PropertyRate pr : propertyRateList)
	    			    			 {
					    				 int propertyRateId = pr.getPropertyRateId();
						    			 List<PropertyRateDetail> rateDetailList =   detailController.list(propertyRateId,f.format(d.toDate()).toLowerCase());
	        				    		 if(rateDetailList.size()>0){
	        				    			 if(rateCount<1){
	        				    				 for (PropertyRateDetail rate : rateDetailList){
	        				    					 baseAmount +=  rate.getBaseAmount();
	        				    					 extraAdultAmount+=rate.getExtraAdult();
	        	  				    				 extraChildAmount+=rate.getExtraChild();
		        				    			 }
	        				    				 rateCount++;
	        				    			 }
	        				    		 }else{
	        				    			 if(propertyRateList.size()==rateIdCount){
	        				    				 baseAmount += accommodations.getBaseAmount();
	        				    				 extraAdultAmount+=accommodations.getExtraAdult();
	          				    				 extraChildAmount+=accommodations.getExtraChild();
	        				    			 }
	            				    	 }
	    			    			 }	    		 
	    				    	 }
	    				    	 else{
	    				    		baseAmount += accommodations.getBaseAmount();
	    				    		extraAdultAmount+=accommodations.getExtraAdult();
				    				extraChildAmount+=accommodations.getExtraChild();
	    				    	 }
				    		}
				    	 }
 						 actualBaseAmount=baseAmount;
 				    	 countDate++;
 				    }
 					Iterator<String> iterPromoType=arlPromoType.iterator();
 					while(iterPromoType.hasNext()){
 						String strPromoType=iterPromoType.next();
 						if(strPromoType.equalsIgnoreCase("F") || strPromoType.equalsIgnoreCase("E")){
 							String strTypePercent=String.valueOf(promotionFirstPercent);
   				    	String strTypeInr=String.valueOf(promotionFirstInr);
				    		if(!strTypePercent.equalsIgnoreCase("null") && promotionFirstPercent!=0){
				    			promotionFirstName=String.valueOf(Math.round(promotionFirstPercent))+" % OFF";
	  							promoFirstPercent=(int) Math.round(promotionFirstPercent);
				    		}
				    		else if(!strTypeInr.equalsIgnoreCase("null") && promotionFirstInr!=0){
				    			promotionFirstName="Flat "+String.valueOf(Math.round(promotionFirstInr))+" OFF";
	  							promoFirstInr=(int) Math.round(promotionFirstInr);
				    		}
 							
 						}
 						if(strPromoType.equalsIgnoreCase("L")){
 							  String strTypePercent=String.valueOf(promotionSecondPercent);
 							  String strTypeInr=String.valueOf(promotionSecondInr);
				    		  if(!strTypePercent.equalsIgnoreCase("null") && promotionSecondPercent!=0){
				    			  promotionSecondName=String.valueOf(Math.round(promotionSecondPercent))+" % OFF";
		  						  promoSecondPercent=(int) Math.round(promotionSecondPercent);
				    		  }
				    		  else if(!strTypeInr.equalsIgnoreCase("null") && promotionSecondInr!=0){
				    			  promotionSecondName="Flat "+String.valueOf(Math.round(promotionSecondInr))+" OFF";
		  						  promoSecondInr=(int) Math.round(promotionSecondInr);
				    		  }
 							
 						}
 					}
 					arlPromoType.clear();
 				
 					if(blnPromoFlag){
 						if(promotionType.equalsIgnoreCase("L") || promotionType.equalsIgnoreCase("E") || promotionType.equalsIgnoreCase("F")){
 							totalAmount=totalFlatAmount;
 							
 							if(countDate>1){
 								actualAdultAmount=extraAdultAmount/countDate;
 								actualChildAmount=extraChildAmount/countDate;
 							}else{
 								actualAdultAmount=extraAdultAmount;
 								actualChildAmount=extraChildAmount;
 							}
 						}
 					}else{
 						totalAmount=baseAmount;
 						
 						if(countDate>1){
 							actualAdultAmount=extraAdultAmount/countDate;
 							actualChildAmount=extraChildAmount/countDate;
 						}else{
 							actualAdultAmount=extraAdultAmount;
 							actualChildAmount=extraChildAmount;
 						}
 					}
 					
 					
 					totalAmount=Math.round(totalAmount);
 					actualTotalAdultAmount=Math.round(actualAdultAmount);
 					actualTotalChildAmount=Math.round(actualChildAmount);
 					
 					
 					long minimum = getAvailableCount(property.getPropertyId(),getArrivalDate(),getDepartureDate(),roomCount,accommId);
 			    	 
 			    	 if(minimum==0){
 			    		 jsonOutput += ",\"available\":\"" + minimum+ "\"";
 			    		 jsonOutput += ",\"baseActualAmount\":\"" + + (actualBaseAmount==0 ? accommodations.getBaseAmount() : actualBaseAmount )+  "\"";
 			    		 jsonOutput += ",\"baseAmount\":\"" + + (totalAmount==0 ? accommodations.getBaseAmount() : totalAmount )+  "\"";
 			    		 jsonOutput += ",\"totalAmount\":\"" + + (totalAmount==0 ? accommodations.getBaseAmount() : totalAmount )+  "\"";
 			    		 jsonOutput += ",\"accommodationName\":\"" + accommodations.getAccommodationType().toUpperCase()+ "\"";
 						 jsonOutput += ",\"accommodationId\":\"" + accommodations.getAccommodationId()+ "\"";
 						 jsonOutput += ",\"promotionFirstName\":\"" + (promotionFirstName==null? "None":promotionFirstName)+ "\"";
 						 jsonOutput += ",\"promotionSecondName\":\"" + (promotionSecondName==null? "None":promotionSecondName)+ "\"";
 						 jsonOutput += ",\"promoFirstPercent\":\"" + (promoFirstPercent==null? 0:promoFirstPercent)+ "\"";
 						 jsonOutput += ",\"promoFirstInr\":\"" + (promoFirstInr==null? 0:promoFirstInr)+ "\"";
 						 jsonOutput += ",\"promoSecondInr\":\"" + (promoSecondInr==null? 0:promoSecondInr)+ "\"";
 						 jsonOutput += ",\"promoSecondPercent\":\"" + (promoSecondPercent==null? 0:promoSecondPercent)+ "\"";	
 						jsonOutput += ",\"promoLastMinuteTimer\":\"" + (getDisplayHoursForTimer()==null ?"None" :getDisplayHoursForTimer()  )+ "\"";
 				     }else{
 				    	jsonOutput += ",\"accommodationName\":\"" + accommodations.getAccommodationType().toUpperCase()+ "\"";
 						jsonOutput += ",\"accommodationId\":\"" + accommodations.getAccommodationId()+ "\"";
 			    		jsonOutput += ",\"available\":\"" + minimum+ "\"";
 			    		jsonOutput += ",\"maxAdult\":\"" + accommodations.getMaxOccupancy()+ "\"";
 			    		jsonOutput += ",\"maxChild\":\"" + 0 + "\"";
 			    		jsonOutput += ",\"minOccupancy\":\"" + accommodations.getMinOccupancy() + "\"";
 						jsonOutput += ",\"maxOccupancy\":\"" + accommodations.getMaxOccupancy() + "\"";
 						jsonOutput += ",\"baseActualAmount\":\"" +  (actualBaseAmount==0 ? accommodations.getBaseAmount() : actualBaseAmount )+  "\"";
 						jsonOutput += ",\"promoFirstPercent\":\"" + (promoFirstPercent==null? 0:promoFirstPercent)+ "\"";
 						jsonOutput += ",\"promoSecondPercent\":\"" + (promoSecondPercent==null? 0:promoSecondPercent)+ "\"";
 						jsonOutput += ",\"baseAmount\":\"" + + (totalAmount==0 ? accommodations.getBaseAmount() : totalAmount )+  "\"";
 			    		jsonOutput += ",\"totalAmount\":\"" + + (totalAmount==0 ? accommodations.getBaseAmount() : totalAmount )+  "\"";
 			    		jsonOutput += ",\"extraAdultAmount\":\"" + + (actualTotalAdultAmount==0 ? accommodations.getExtraAdult() : actualTotalAdultAmount )+  "\"";
 			    		jsonOutput += ",\"extraChildAmount\":\"" + + (actualTotalChildAmount==0 ? accommodations.getExtraChild() : actualTotalChildAmount )+  "\"";
 			    		jsonOutput += ",\"promotionFirstName\":\"" + (promotionFirstName==null? "None":promotionFirstName)+ "\"";
						jsonOutput += ",\"promotionSecondName\":\"" + (promotionSecondName==null? "None":promotionSecondName)+ "\"";
						jsonOutput += ",\"promoFirstInr\":\"" + (promoFirstInr==null? 0:promoFirstInr)+ "\"";
						jsonOutput += ",\"promoSecondInr\":\"" + (promoSecondInr==null? 0:promoSecondInr)+ "\"";
 			    		jsonOutput += ",\"promotionId\":\"" + (promotionId==0? 0:promotionId) + "\"";
 			    		jsonOutput += ",\"promoLastMinuteTimer\":\"" + (getDisplayHoursForTimer()==null ?"None" :getDisplayHoursForTimer()  )+ "\"";
 						discountId=35;
 						PropertyDiscountManager discountController=new PropertyDiscountManager();
 						PropertyDiscount propertyDiscount=discountController.find(discountId);
 			    		jsonOutput += ",\"discountPercent\":\"" + (propertyDiscount.getDiscountPercentage()==0.0? "0":propertyDiscount.getDiscountPercentage())+ "\"";
 			    		jsonOutput += ",\"discountId\":\"" + (discountId==0? "0" :discountId) + "\"";
 			    		
 			    		
 				     }
 				}
 		     
 			}
 			jsonOutput += "}";
 			
 			arllist.clear();
 		
		}
	 
       
		response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
//		sessionMap.clear();
		

	} catch (Exception e) {
		logger.error(e);
		e.printStackTrace();
	} finally {

	}

	return null;


   }
   
   public String getAreaProperties() throws IOException {


 		try {
 		
 		 /* SeoContentManager seoContentController = new SeoContentManager();
 	  	  SeoContent seoContent = seoContentController.findId(getAreaId());	
 	  	        
 	  	  request.setAttribute("metaTag" ,seoContent.getTitle() == null ? "": seoContent.getTitle());
 	  	  request.setAttribute("description" ,seoContent.getDescription()  == null ? "": seoContent.getDescription());
 	  	  request.setAttribute("content" ,seoContent.getContent() == null ? "": seoContent.getContent());*/
 	  	
 		Map mapDate=new HashMap();
 		PropertyAreasManager areaController=new PropertyAreasManager();
 		DateFormat format = new SimpleDateFormat("MMM d,yyyy", Locale.ENGLISH);
 	    java.util.Date dateStart = format.parse((String)request.getAttribute("StartDate"));
 	    java.util.Date dateEnd = format.parse((String)request.getAttribute("EndDate"));
 	   
 	   if(getSourceTypeId()==null){
	    	this.sourceTypeId=1;
	    }
 	   
 	    Calendar calCheckStart=Calendar.getInstance();
 	    calCheckStart.setTime(dateStart);
 	    java.util.Date checkInDate = calCheckStart.getTime();
 	    this.arrivalDate=new Timestamp(checkInDate.getTime());
 	   
 	    
 	    Calendar calCheckEnd=Calendar.getInstance();
 	    calCheckEnd.setTime(dateEnd);
 	    java.util.Date checkOutDate = calCheckEnd.getTime();
 	    this.departureDate=new Timestamp(checkOutDate.getTime());
 	    
	  	String checkIn = new SimpleDateFormat("MMM d,yyyy").format(checkInDate);
	    String checkOut = new SimpleDateFormat("MMM d,yyyy").format(checkOutDate);
	    String checkIn1 = new SimpleDateFormat("dd MMM").format(checkInDate);
	    String checkOut1 = new SimpleDateFormat("dd MMM").format(checkOutDate);
	      
	    sessionMap.put("checkIn",checkIn);
	    sessionMap.put("checkOut",checkOut);
	    sessionMap.put("checkIn1",checkIn1);
	    sessionMap.put("checkOut1",checkOut1);
     
 	     
 		/*this.arrivalDate = getArrivalDate();
 	    sessionMap.put("arrivalDate",arrivalDate); 
 		
 		this.departureDate = getDepartureDate();
 		sessionMap.put("departureDate",departureDate); */
 		mapDate.put("arrivalDate", arrivalDate);
 		mapDate.put("departureDate", departureDate);
 		
 		PmsPropertyManager propertyController = new PmsPropertyManager();		
 		PropertyAccommodationManager accommodationManager = new PropertyAccommodationManager();
 		PropertyAmenityManager amenityController = new PropertyAmenityManager();
 		List<PropertyAmenity> propertyAmenityList;
 		PropertyTypeManager typeController =  new PropertyTypeManager();
 		PropertyRateManager rateController = new PropertyRateManager();
 		PropertyRateDetailManager detailController = new PropertyRateDetailManager();
 		PmsAmenityManager ameController = new PmsAmenityManager();
 		PromotionManager promotionController=new PromotionManager();
 		PromotionDetailManager promotionDetailController=new PromotionDetailManager();
 		PmsBookingManager bookingController=new PmsBookingManager();
 		
 		
 		HashMap<Double,Integer > mapAmountAccommId=new HashMap<Double, Integer>();
 		HashMap<Double,Integer > mapSoldOutAmountAccommId=new HashMap<Double, Integer>();
 		
 		ArrayList<Integer> arlSortPropertyId=new ArrayList<Integer>();
 		HashMap<Integer, Integer> mapAvailablePropertyId=new HashMap<Integer, Integer>();
 		String jsonOutput = "";
 		HttpServletResponse response = ServletActionContext.getResponse();
 		
 		response.setContentType("application/json");
 		
 		this.arrivalDate = getArrivalDate();
 	    sessionMap.put("arrivalDate",arrivalDate); 
 			
 		this.departureDate = getDepartureDate();
 		sessionMap.put("departureDate",departureDate); 
 		 
 		Calendar cal = Calendar.getInstance();
 		cal.setTime(this.departureDate);
 	    cal.add(Calendar.DATE, -1);
 	    
 	    java.util.Date dteCheckOut = cal.getTime();
 	    this.departureDate=new Timestamp(dteCheckOut.getTime());
 	    sessionMap.put("endDate",departureDate); 
 	    
 		String startDate = new SimpleDateFormat("yyyy-MM-dd").format(this.arrivalDate);
 		String endDate = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
 		DateTime start = DateTime.parse(startDate);
 	    DateTime end = DateTime.parse(endDate);
 	    
 	    DateFormat f = new SimpleDateFormat("EEEE");
 	    DecimalFormat df = new DecimalFormat("###.##");
     	List<PmsSmartPrice> listSmartPriceDetail=null; 
     	List<AccommodationRoom> listAccommodationRoom=null;
     	
     	List<PropertyAreas> listPropertyAreas=null;
 		PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
 		List<PmsProperty> listPmsProperty=new ArrayList<PmsProperty>();
 	    int availRoomCount=0,soldRoomCount=0;
 	    listPropertyAreas = areaController.listPropertyByArea(getAreaId());
 	    if(listPropertyAreas.size()>0 && !listPropertyAreas.isEmpty()){
 	    	for(PropertyAreas propertyareas: listPropertyAreas){
 	    		int areaPropertyId=propertyareas.getPmsProperty().getPropertyId();
 	    		List<PmsProperty> listProperty=propertyController.list(areaPropertyId);
 	    		if(listProperty.size()>0 && !listProperty.isEmpty()){
 	    			for(PmsProperty property:listProperty){
 	    				listPmsProperty.add(property);
 	    			}
 	    		}
 	    		
 	    	}
 	    }
 	    
 		for (PmsProperty property : listPmsProperty) {
 			
 			if (!jsonOutput.equalsIgnoreCase(""))
 				jsonOutput += ",{";
 			else
 				jsonOutput += "{";
 			jsonOutput += "\"DT_RowId\":\"" + property.getPropertyId()+ "\"";
 			jsonOutput += ",\"city\":\"" + property.getCity() + "\"";
 			jsonOutput += ",\"propertyTypeId\":\"" + property.getPropertyType().getPropertyTypeId() + "\"";
 			jsonOutput += ",\"propertyTypeName\":\"" + property.getPropertyType().getPropertyTypeName() + "\"";
 			jsonOutput += ",\"propertyName\":\"" + (property.getPropertyName() == null ? "": property.getPropertyName().toUpperCase().trim())+ "\"";
 			jsonOutput += ",\"propertyUrl\":\"" + (property.getPropertyUrl() == null ? "": property.getPropertyUrl().toLowerCase().trim())+ "\"";
// 			jsonOutput += ",\"propertyStandardPolicy\":\"" + (property.getPropertyStandardPolicy() == null ? "None": property.getPropertyStandardPolicy().trim())+ "\"";
// 			jsonOutput += ",\"propertyHotelPolicy\":\"" + (property.getPropertyHotelPolicy() == null ? "None": property.getPropertyHotelPolicy().trim())+ "\"";
// 			jsonOutput += ",\"propertyCancellationPolicy\":\"" + (property.getPropertyCancellationPolicy() == null ? "None": property.getPropertyCancellationPolicy().trim())+ "\"";
 			jsonOutput += ",\"payAtHotelStatus\":\"" + (property.getPayAtHotelStatus() == null ? false: property.getPayAtHotelStatus())+ "\"";
			jsonOutput += ",\"coupleStatus\":\"" + ( property.getCoupleIsActive() == null? false :  property.getCoupleIsActive() )+ "\"";
			jsonOutput += ",\"breakfastStatus\":\"" + ( property.getBreakfastIsActive() == null? false:  property.getBreakfastIsActive() )+ "\"";
			jsonOutput += ",\"hotelStatus\":\"" + ( property.getHotelIsActive() == null? false:  property.getHotelIsActive() )+ "\"";
			
 			SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
 			Timestamp FromDate=(Timestamp)mapDate.get("arrivalDate");
 			Timestamp ToDate=(Timestamp)mapDate.get("departureDate");
 			jsonOutput += ",\"arrivalDate\":\"" + format2.format(FromDate) + "\"";
 			jsonOutput += ",\"departureDate\":\"" + format2.format(ToDate)+ "\"";
 			
 			String strFromDate=format2.format(FromDate);
 			String strToDate=format2.format(ToDate);
 	    	DateTime startdate = DateTime.parse(strFromDate);
 	        DateTime enddate = DateTime.parse(strToDate);
 	        java.util.Date fromdate = startdate.toDate();
 	 		java.util.Date todate = enddate.toDate();
 	 		 
 			int diffInDays = (int) ((todate.getTime() - fromdate.getTime()) / (1000 * 60 * 60 * 24));
 			
 			List arllist=new ArrayList();
 			List arllistAvl=new ArrayList();
 			List arllistSold=new ArrayList();
 			jsonOutput += ",\"diffDays\":\"" + diffInDays+ "\"";
 			
 			String jsonAmenities ="";
 			propertyAmenityList =  amenityController.list(property.getPropertyId());
 			if(propertyAmenityList.size()>0)
 			{
 				for (PropertyAmenity amenity : propertyAmenityList) {
 					if (!jsonAmenities.equalsIgnoreCase(""))
 						
 						jsonAmenities += ",{";
 					else
 						jsonAmenities += "{";
 					PropertyAmenity amentity1 = amenityController.find(amenity.getPropertyAmenityId());
 				    String path = "ulowebsite/images/icons/";
 					PmsAmenity pmsAmenity1 =ameController.find(amentity1.getPmsAmenity().getAmenityId());
 					jsonAmenities += "\"DT_RowId\":\"" + amenity.getPropertyAmenityId()+ "\"";					
 					jsonAmenities += ",\"amenityName\":\"" + pmsAmenity1.getAmenityName()+ "\"";
 					jsonAmenities += ",\"amenityId\":\"" + pmsAmenity1.getAmenityId()+ "\"";
                     jsonAmenities += ",\"icon\":\"" + path + pmsAmenity1.getAmenityIcon()+ "\"";
              
 					
 					jsonAmenities += "}";
 				}
 				
 				jsonOutput += ",\"amenities\":[" + jsonAmenities+ "]";
 			}
 			
 			String jsonReviews="";
			PropertyReviewsManager reviewController=new PropertyReviewsManager();
			List<PropertyReviews> listReviews=reviewController.listReview(property.getPropertyId());
			if(listReviews.size()>0 && !listReviews.isEmpty()){
				for(PropertyReviews reviews:listReviews){
					
					if (!jsonReviews.equalsIgnoreCase(""))
						
						jsonReviews += ",{";
					else
						jsonReviews += "{";

					/*jsonReviews += "\"starCount\":\"" + reviews.getStarCount().intValue()+ "\"";	
					jsonReviews += ",\"reviewCount\":\"" + reviews.getReviewCount().intValue()+ "\"";
					Double averageReviews=reviews.getAverageReview();
					jsonReviews += ",\"averageReview\":\"" + df.format(averageReviews)+ "\"";*/
					
					jsonReviews += "\"starCount\":\"" + (reviews.getStarCount()==null?0:reviews.getStarCount().intValue())+ "\"";	
					jsonReviews += ",\"reviewCount\":\"" + (reviews.getReviewCount()==null?0:reviews.getReviewCount().intValue())+ "\"";	
					if(reviews.getAverageReview()!=null){
						Double averageReviews=reviews.getAverageReview();
						jsonReviews += ",\"averageReview\":\"" + df.format(averageReviews)+ "\"";
					}else{
						jsonReviews += ",\"averageReview\":\"" + df.format(0)+ "\"";
					}
					
					jsonReviews += ",\"tripadvisorStarCount\":\"" + (reviews.getTripadvisorStarCount()==null?0:reviews.getTripadvisorStarCount().intValue())+ "\"";	
					jsonReviews += ",\"tripadvisorReviewCount\":\"" + (reviews.getTripadvisorReviewCount()==null?0:reviews.getTripadvisorReviewCount().intValue())+ "\"";
					if(reviews.getTripadvisorAverageReview()!=null){
						Double taAverageReviews=reviews.getTripadvisorAverageReview();
						jsonReviews += ",\"tripadvisorAverageReview\":\"" + df.format(taAverageReviews)+ "\"";
					}else{
						jsonReviews += ",\"tripadvisorAverageReview\":\"" + df.format(0)+ "\"";
					}
					jsonReviews += ",\"googleReviewUrl\":\"" + (reviews.getGoogleReviewUrl()==null?"":reviews.getGoogleReviewUrl())+ "\"";	
					jsonReviews += ",\"tripadvisorReviewUrl\":\"" + (reviews.getTripadvisorReviewUrl()==null?"":reviews.getTripadvisorReviewUrl())+ "\"";	
					jsonReviews += ",\"propertyRating\":\"" + (reviews.getPropertyRating()==null?"None":reviews.getPropertyRating())+ "\"";
					jsonReviews += "}";
				}
				
			}else{
				if (!jsonReviews.equalsIgnoreCase(""))
					
					jsonReviews += ",{";
				else
					jsonReviews += "{";
				
				jsonReviews += "\"starCount\":\"" + 0+ "\"";	
				jsonReviews += ",\"reviewCount\":\"" + 0+ "\"";	
				jsonReviews += ",\"averageReview\":\"" + 0+ "\"";
				
				jsonReviews += ",\"tripadvisorStarCount\":\"" + 0 + "\"";	
				jsonReviews += ",\"tripadvisorReviewCount\":\"" + 0 + "\"";	
				jsonReviews += ",\"tripadvisorAverageReview\":\"" + 0+ "\"";
				
				jsonReviews += ",\"googleReviewUrl\":\"" + "None"+ "\"";	
				jsonReviews += ",\"tripadvisorReviewUrl\":\"" + "None"+ "\"";	
				jsonReviews += ",\"propertyRating\":\"" + "None"+ "\"";
				
				jsonReviews += "}";
			}
			
			jsonOutput += ",\"reviews\":[" + jsonReviews+ "]";
			
			String jsonPhotos="";
  			PropertyPhotoManager photoController = new PropertyPhotoManager();
			
		    response.setContentType("application/json");
		    List<PropertyPhoto> listPhotos =photoController.list(property.getPropertyId());
            
            if(listPhotos.size()>0)
  			{
            	for (PropertyPhoto photo : listPhotos) {

    				if (!jsonPhotos.equalsIgnoreCase(""))
    					jsonPhotos += ",{";
    				else
    					jsonPhotos += "{";
    				jsonPhotos += "\"DT_RowId\":\"" + photo.getPropertyPhotoId() + "\"";
    				jsonPhotos += ",\"isPrimary\":\"" + photo.getIsPrimary()+ "\"";
    				jsonPhotos += ",\"photoPath\":\"" + photo.getPhotoPath()+ "\"";
    				
    				
    				jsonPhotos += "}";


    			}
    			
                   jsonOutput += ",\"photos\":[" + jsonPhotos+ "]";
  			
  			}
			
 			
 			String jsonAccommodation ="";
 			List<PropertyAccommodation> accommodationsList =  accommodationManager.list(property.getPropertyId());
 			int soldOutCount=0,availCount=0,promotionAccommId=0;
 			if(accommodationsList.size()>0 && !accommodationsList.isEmpty())
 			{
 				for (PropertyAccommodation accommodation : accommodationsList) {
 					int roomCount=0;
 					listAccommodationRoom=accommodationController.listPropertyTotalRooms(property.getPropertyId(), accommodation.getAccommodationId());
 					if(listAccommodationRoom.size()>0 && !listAccommodationRoom.isEmpty()){
 						for(AccommodationRoom roomsList:listAccommodationRoom){
 							 roomCount=(int) roomsList.getRoomCount();
 							 long minimum = getAvailableCount(property.getPropertyId(),getArrivalDate(),getDepartureDate(),roomCount,accommodation.getAccommodationId());
 							 if((int)minimum>0){
 								 availCount++;
 							 }else if((int)minimum==0){
 								 soldOutCount++;
 							 }
 							 break;
 						 }
 					 }
 					
 					
 					if (!jsonAccommodation.equalsIgnoreCase(""))
 						
 						jsonAccommodation += ",{";
 					else
 						jsonAccommodation += "{";
 						jsonAccommodation += "\"accommodationId\":\"" + accommodation.getAccommodationId()+ "\"";
 						jsonAccommodation += ",\"accommodationName\":\"" + accommodation.getAccommodationType().toUpperCase()+ "\"";
 					
 					jsonAccommodation += "}";
 				}
 				
 				jsonOutput += ",\"accommodations\":[" + jsonAccommodation+ "]";
 			}

 			
 			List<PmsPromotionDetails> listPromotionDetailCheck=null;
 			Boolean blnAvailable=false,blnSoldout=false,firstPromotionFlag=false,secondPromotionFlag=false,promotionFlag=false,blnPromoFlag=false;
 			int arlListTotalAccomm=0,soldOutTotalAccomm=0,accommCount=0;
 			String promotionType=null;
 			ArrayList<String> arlListPromoType=new ArrayList<String>();
 			List<PropertyAccommodation> accommodationList =  accommodationManager.list(property.getPropertyId());
 			if(accommodationList.size()>0)
 			{
 				for (PropertyAccommodation accommodation : accommodationList) {
 					Integer promoAccommId=0,enablePromoId=0;
 					ArrayList<Integer> arlEnablePromo=new ArrayList<Integer>();
 					Boolean blnPromotionIsNotFlag=false;
 					Boolean blnLastMinuteIsActive=false,blnLastMinuteFirstActive=false;
 					int dateCount=0,promoCount=0;
 					String strCheckPromotionType=null,promotionFirstDiscountType=null,promotionSecondDiscountType=null,promotionHoursRange=null;
 					double amount=0.0,extraAdultAmount=0.0,extraChildAmount=0.0,totalSmartPriceAmount=0.0;
 					int roomCount=0,availableCount=0,totalCount=0;
 				    double dblPercentCount=0.0,dblSmartPricePercent=0.0,
 				    		promotionFirstPercent=0.0,promotionFirstInr=0.0,
 				    		promotionSecondPercent=0.0,promotionSecondInr=0.0,totalFlatLastAmount=0.0,flatLastAmount=0.0;
 				    Timestamp lastMinStartDate=null,lastMinEndDate=null;
 				    Boolean isPositive=false,isNegative=false,blnLastMinutePromotions=false;
 				   
 	    			
 	    			listAccommodationRoom=accommodationController.listPropertyTotalRooms(property.getPropertyId(), accommodation.getAccommodationId());
 					 
 					if(listAccommodationRoom.size()>0 && !listAccommodationRoom.isEmpty()){
 						
 						for(AccommodationRoom roomsList:listAccommodationRoom){
 							roomCount=(int) roomsList.getRoomCount();
 							if(roomCount>0){
 								long minimum = getAvailableCount(property.getPropertyId(),getArrivalDate(),getDepartureDate(),roomCount,accommodation.getAccommodationId());
 								if((int)minimum>0){
 									blnAvailable=true;
 									blnSoldout=false;
 									arlListTotalAccomm++;
 								}else if((int)minimum==0){
 									blnSoldout=true;
 									blnAvailable=false;
 									soldOutTotalAccomm++;
 								}
 							}
 							 break;
 						 }
 					 }
 					 Timestamp tsFirstStartRange=null,tsSecondStartRange=null,tsFirstEndRange=null,tsSecondEndRange=null;
					 Timestamp tsLastStartDate=null,tsLastEndDate=null;
 					 Timestamp tsPromoBookedDate=null,tsPromoStartDate=null,tsPromoEndDate=null;
 					 boolean blnLastMinutePromo=false,blnFirstActivePromo=false,blnFirstNotActivePromo=false,baseFirstPromoFlag=false,blnEarlyBirdPromo=false;
 					 boolean blnSecondActivePromo=false,blnSecondNotActivePromo=false,baseSecondPromoFlag=false;
 					 SimpleDateFormat format1=new SimpleDateFormat("yyyy-MM-dd");
 					 java.util.Date currentdate=new java.util.Date();
 					 String strCurrentDate = new SimpleDateFormat("yyyy-MM-dd").format(currentdate);
 				   	 java.util.Date curdate = format1.parse(strCurrentDate);
 				   	 this.displayHoursForTimer=null;
 				   	 setDisplayHoursForTimer(this.displayHoursForTimer);
 				   	 
 					 List<PmsPromotions> listDatePromotions= promotionController.listBookedDatePromotions(property.getPropertyId(),curdate);
					 if(listDatePromotions.size()>0 && !listDatePromotions.isEmpty()){
						for(PmsPromotions promotions:listDatePromotions){
							tsPromoBookedDate=promotions.getPromotionBookedDate();
							tsPromoStartDate=promotions.getStartDate();
							tsPromoEndDate=promotions.getEndDate();
							blnEarlyBirdPromo=checkEarlyBirdPromos(tsPromoBookedDate,tsPromoStartDate,tsPromoEndDate,getArrivalDate(),getDepartureDate());
						}
					 }else{
						 blnEarlyBirdPromo=false;
					 }
					 
					List<PmsPromotions> listLastPromotions= promotionController.listLastBookedPromotions(property.getPropertyId(),curdate);
					 if(listLastPromotions.size()>0 && !listLastPromotions.isEmpty()){
						for(PmsPromotions promotions:listLastPromotions){
							tsFirstStartRange=promotions.getFirstRangeStartDate();
							tsSecondStartRange=promotions.getSecondRangeStartDate();
							tsFirstEndRange=promotions.getFirstRangeEndDate();
							tsSecondEndRange=promotions.getSecondRangeEndDate();
							tsLastStartDate=promotions.getStartDate();
							tsLastEndDate=promotions.getEndDate();
							promotionHoursRange=promotions.getPromotionHoursRange();
							blnLastMinuteFirstActive=checkLastMinutePromos(promotionHoursRange,getArrivalDate(),getDepartureDate(),tsSecondStartRange,tsFirstEndRange,tsFirstEndRange,tsSecondStartRange);
//							blnLastMinuteIsActive=getCheckHoursRange(promotionHoursRange,getArrivalDate(),getDepartureDate());
							if(blnLastMinuteFirstActive){
								blnLastMinutePromo=true;
								enablePromoId=promotions.getPromotionId();
								arlEnablePromo.add(enablePromoId);
							}
						}
					 }else{
						 blnLastMinutePromo=false;
						 this.displayHoursForTimer=null;
				    	 setDisplayHoursForTimer(this.displayHoursForTimer);
					 }
 					 ArrayList<Integer> arlFirstActivePromo=new ArrayList<Integer>(); 
 					 ArrayList<Integer> arlSecondActivePromo=new ArrayList<Integer>(); 
 					 List<DateTime> between = DateUtil.getDateRange(start, end);
 					 for (DateTime d : between)
 				     {

 						 if(blnEarlyBirdPromo){
 							 List<PmsPromotions> listEarlyPromotions= promotionController.listBookedDatePromotions(property.getPropertyId(),curdate);
 							 if(listEarlyPromotions.size()>0 && !listEarlyPromotions.isEmpty()){
 								 for(PmsPromotions promos:listEarlyPromotions){
 									 if(promos.getPromotionType().equalsIgnoreCase("E")){
 	 									 if(arlFirstActivePromo.isEmpty()){
 	 										arlFirstActivePromo.add(promos.getPromotionId());
 	 									 }else if(!arlFirstActivePromo.contains(promos.getPromotionId())){
 	 										arlFirstActivePromo.add(promos.getPromotionId());
 	 									 }
 	 								 }else{
 	 									blnFirstNotActivePromo=true;
 	 								 }
 								 }
 							 }
 						 }else if(!blnEarlyBirdPromo){
 							 List<PmsPromotions> listAllPromotions= promotionController.listFlatDatePromotions(property.getPropertyId(),d.toDate());
 							 if(!listAllPromotions.isEmpty() && listAllPromotions.size()>0){
 								 for(PmsPromotions promos:listAllPromotions){
 									 if(promos.getPromotionType().equalsIgnoreCase("F")){
 	 									 if(arlFirstActivePromo.isEmpty()){
 	 										arlFirstActivePromo.add(promos.getPromotionId());
 	 									 }else if(!arlFirstActivePromo.contains(promos.getPromotionId())){
 	 										arlFirstActivePromo.add(promos.getPromotionId());
 	 									 }
 	 								 }else{
 	 									blnFirstNotActivePromo=true;
 	 								 }
 								 }
 							 } 
 						 }else{
 							 blnFirstNotActivePromo=true;
 						 }
 						 
 						if(blnLastMinutePromo){
 							List<PmsPromotions> listLastPromos= promotionController.listLastBookedPromotions(property.getPropertyId(),curdate);
 							 if(listLastPromos.size()>0 && !listLastPromos.isEmpty()){
 								for(PmsPromotions promos:listLastPromos){
 									if(promos.getPromotionType().equalsIgnoreCase("L")){
 										promoAccommId=promos.getPropertyAccommodation().getAccommodationId();
 										if(arlEnablePromo.contains(promos.getPromotionId())){
											if(arlSecondActivePromo.isEmpty()){
 		  										arlSecondActivePromo.add(promos.getPromotionId());
 		  									 }else if(!arlSecondActivePromo.contains(promos.getPromotionId())){
 		  										arlSecondActivePromo.add(promos.getPromotionId());
 		  									 }	
										}
 									 }
 								}
 							 }else{
 								blnSecondNotActivePromo=true;
 							 }
						}else{
							blnSecondNotActivePromo=true;
						}
 				     }
 					 int intPromoSize=arlFirstActivePromo.size();
 					 if((arlFirstActivePromo.isEmpty() || blnFirstNotActivePromo)){
 						 blnFirstActivePromo=false;
 					 }else{
 						 blnFirstActivePromo=true;
 					 }
 					 
 					 if(arlSecondActivePromo.isEmpty() || blnSecondNotActivePromo){
 						blnSecondActivePromo=false;
 					 }else{
 						blnSecondActivePromo=true;
 					 }
 					 
 					
 				     for (DateTime d : between)
 				     {
 				    	 Integer promotionId=0;
 				    	 if(blnFirstActivePromo){
 				    		 if(blnEarlyBirdPromo){
 				    			List<PmsPromotions> listEarlyPromotions= promotionController.listBookedDatePromotions(property.getPropertyId(),curdate);
 	  							 if(listEarlyPromotions.size()>0 && !listEarlyPromotions.isEmpty()){
 	  								 for(PmsPromotions promos:listEarlyPromotions){
 	  									if(promos.getPromotionType().equalsIgnoreCase("E")){
 	 					    				promotionId=promos.getPromotionId();
 	 					    				promotionFirstDiscountType=promos.getPromotionDiscountType();
 	 					    				listPromotionDetailCheck=promotionDetailController.listPromotionDetails(promos.getPromotionId(),f.format(d.toDate()).toLowerCase());
 	 					    				 for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
 	 					    					if(promoCount==0){
 	 					    						promotionFirstPercent=promoDetails.getPromotionPercentage();
 	 	 					    					promotionFirstInr=promoDetails.getPromotionInr();
 	 	 					    					promotionType=promos.getPromotionType();
 	 	 					    					if(promotionType.equalsIgnoreCase("F")){
 	 	 					    						strCheckPromotionType="F";
 	 	 					    					}else if(promotionType.equalsIgnoreCase("E")){
 	 	 					    						strCheckPromotionType="E";
 	 	 					    					}
 	 	 					    					
 	 		  						    			arlListPromoType.add(promotionType);
 	 					    					}
 	 					    					promoCount++;
 	 					    				 }
 	  				    				}
	  	  								promotionFlag=true; 
	  			    					blnPromoFlag=true;
 	  								 }
 	  							 }else{
 	  				    			promotionFlag=false;
 	  	  		    				promotionType=null;
 	  	  		    				blnPromoFlag=false;
 	  	  		    				promotionId=null;
 	  	  		    				baseFirstPromoFlag=true;
 	  				    		}
 				    		 }else if(!blnEarlyBirdPromo){

 	  				    		List<PmsPromotions> listPromotions= promotionController.listFlatDatePromotions(property.getPropertyId(),d.toDate());
 	  				    		if(listPromotions.size()>0 && !listPromotions.isEmpty()){
 	  				    			for(PmsPromotions promotions:listPromotions){
 	  				    				if(promotions.getPromotionType().equalsIgnoreCase("F")){
 	 					    				promotionId=promotions.getPromotionId();
 	 					    				promotionFirstDiscountType=promotions.getPromotionDiscountType();
 	 					    				listPromotionDetailCheck=promotionDetailController.listPromotionDetails(promotions.getPromotionId(),f.format(d.toDate()).toLowerCase());
 	 					    				 for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
 	 					    					if(promoCount==0){
 	 					    						promotionFirstPercent=promoDetails.getPromotionPercentage();
 	 	 					    					promotionFirstInr=promoDetails.getPromotionInr();
 	 	 					    					promotionType=promotions.getPromotionType();
 	 	 					    					if(promotionType.equalsIgnoreCase("F")){
 	 	 					    						strCheckPromotionType="F";
 	 	 					    					}else if(promotionType.equalsIgnoreCase("E")){
 	 	 					    						strCheckPromotionType="E";
 	 	 					    					}
 	 	 					    					
 	 		  						    			arlListPromoType.add(promotionType);
 	 					    					}
 	 					    					promoCount++;
 	 					    				 }
 	  				    				}
 	  				    			}
 	  				    			promotionFlag=true; 
 			    					blnPromoFlag=true;
 	  				    		}
 				    		 }
 				    	 }
 				    	 if(blnSecondActivePromo){
 				    		List<PmsPromotions> listPmsPromotions= promotionController.listLastDatePromotions(property.getPropertyId(),accommodation.getAccommodationId(),curdate);
				    			 if(listPmsPromotions.size()>0 &&!listPmsPromotions.isEmpty()){
				    				 for(PmsPromotions pmsPromotions:listPmsPromotions){
				    					 if(pmsPromotions.getPromotionType().equalsIgnoreCase("L")){
				    						 promotionId=pmsPromotions.getPromotionId();
							    			 promoAccommId=pmsPromotions.getPropertyAccommodation().getAccommodationId();
							    			 Timestamp tsStartDate=null,tsEndDate=null;
							    			 tsStartDate=pmsPromotions.getStartDate();
							    			 tsEndDate=pmsPromotions.getEndDate();
							    			 
							    			 promotionHoursRange=pmsPromotions.getPromotionHoursRange();
							    			 if(arlEnablePromo.contains(pmsPromotions.getPromotionId())){
							    				 promotionAccommId=promoAccommId.intValue();
							    				 long minimumCount = getAvailableCount(property.getPropertyId(),getArrivalDate(),getDepartureDate(),roomCount,accommodation.getAccommodationId());
				    			    			 if((int)minimumCount>0){
			    			    					 promotionFlag=true; 
			    			    					 blnPromoFlag=true;
			    			    					 strCheckPromotionType="L";
			    			    					 promotionType=pmsPromotions.getPromotionType();
   	 		  						    		 arlListPromoType.add(promotionType);
			    			    					 promotionSecondDiscountType=pmsPromotions.getPromotionDiscountType();
   	 					    					 listPromotionDetailCheck=promotionDetailController.listPromotionDetails(pmsPromotions.getPromotionId(),f.format(curdate).toLowerCase());
   	 					    					 if(listPromotionDetailCheck.size()>0 && !listPromotionDetailCheck.isEmpty()){
   	 					    						for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
		    	 					    					promotionSecondPercent=promoDetails.getPromotionPercentage();
	    	 	 					    					promotionSecondInr=promoDetails.getPromotionInr();
		    	 					    				 }
   	 					    					 }
				    			    			 }else{
				    			    				 promotionFlag=false;
				    			    				 promotionType=null;
				    			    				 blnPromoFlag=false;
				    			    				 baseSecondPromoFlag=true;
				    			    			 }
							    			 }
				    					 }
				    					 
				    				 }
				    			 }
 				    	 }
 				    	 if(promotionFlag){
 				    		int rateCount=0,rateIdCount=0;
   						List<PropertyRate> propertyRateList =  rateController.listAllDates(property.getPropertyId(),accommodation.getAccommodationId(),getSourceTypeId(),d.toDate());
   				    	if(propertyRateList.size()>0)
   				    	{
   				    		 for (PropertyRate pr : propertyRateList)
   			    			 {
				    				 int propertyRateId = pr.getPropertyRateId();
					    			 List<PropertyRateDetail> rateDetailList =   detailController.list(propertyRateId,f.format(d.toDate()).toLowerCase());
	    				    		 if(rateDetailList.size()>0){
	    				    			 if(rateCount<1){
	    				    				 for (PropertyRateDetail rate : rateDetailList){
	    				    					 amount+=  rate.getBaseAmount();
	        				    			 }
	    				    				 rateCount++;
	    				    			 }
	    				    		 }else{
	    				    			 if(propertyRateList.size()==rateIdCount){
	    				    				 amount+= accommodation.getBaseAmount();
	    				    			 }
	        				    	 }
   			    			 }	    		 
   				    	 }
   				    	 else{
   				    		 amount+= accommodation.getBaseAmount();
   				    	 }
   				    	double discountAmount=0;
 				    		 if(blnFirstActivePromo){
  	    				    	String strTypePercent=String.valueOf(promotionFirstPercent);
  	    				    	String strTypeInr=String.valueOf(promotionFirstInr);
					    		  if(!strTypePercent.equalsIgnoreCase("null") && promotionFirstPercent!=0){
					    			  flatLastAmount = amount-(amount*promotionFirstPercent/100);
					    			  discountAmount+=amount*promotionFirstPercent/100;
					    		  }
					    		  else if(!strTypeInr.equalsIgnoreCase("null") && promotionFirstInr!=0){
					    			  flatLastAmount = amount-promotionFirstInr;
					    			  discountAmount+=promotionFirstInr;
					    		  }
					    		  else{
					    			  flatLastAmount = amount;
					    		  } 
 				    		 }else{
				    			  flatLastAmount = amount;
				    		  }
 				    		 if(blnSecondActivePromo){
 				    			String strTypePercent=String.valueOf(promotionSecondPercent);
  	    				    	String strTypeInr=String.valueOf(promotionSecondInr);
					    		  if(!strTypePercent.equalsIgnoreCase("null") && promotionSecondPercent!=0){
					    			  flatLastAmount = flatLastAmount-(flatLastAmount*promotionSecondPercent/100);
					    			  discountAmount+=amount*promotionSecondPercent/100;
					    		  }
					    		  else if(!strTypeInr.equalsIgnoreCase("null") && promotionSecondInr!=0){
					    			  flatLastAmount = flatLastAmount-promotionSecondInr;
					    			  discountAmount+=promotionSecondInr;
					    		  }
					    		  else{
					    			  flatLastAmount = flatLastAmount;
					    		  }
 				    		 }else{
				    			  flatLastAmount = flatLastAmount;
				    		  }
 				    		totalFlatLastAmount=amount-discountAmount;
 				    		 
 				    	 }else{
 				    		 if(baseFirstPromoFlag){
 				    			amount += accommodation.getBaseAmount();
 				    		}else{
 				    			int rateCount=0,rateIdCount=0;
 	    						List<PropertyRate> propertyRateList =  rateController.listAllDates(property.getPropertyId(),accommodation.getAccommodationId(),getSourceTypeId(),d.toDate());
 	    				    	if(propertyRateList.size()>0)
 	    				    	{
 	    				    		 for (PropertyRate pr : propertyRateList)
 	    			    			 {
 					    				 int propertyRateId = pr.getPropertyRateId();
						    			 List<PropertyRateDetail> rateDetailList =   detailController.list(propertyRateId,f.format(d.toDate()).toLowerCase());
	        				    		 if(rateDetailList.size()>0){
	        				    			 if(rateCount<1){
	        				    				 for (PropertyRateDetail rate : rateDetailList){
	        				    					 amount +=  rate.getBaseAmount();
		        				    			 }
	        				    				 rateCount++;
	        				    			 }
	        				    		 }else{
	        				    			 if(propertyRateList.size()==rateIdCount){
	        				    				 amount += accommodation.getBaseAmount();
	        				    			 }
	            				    	 }
 	    			    			 }	    		 
 	    				    	 }
 	    				    	 else{
 	    				    		 amount += accommodation.getBaseAmount();
 	    				    	 }
 				    		}
 				    	 }
 				    	 dateCount++;
 				    	 if(dateCount==diffInDays){
 				    		 if(blnAvailable){
 				    			if(promotionType!=null && strCheckPromotionType!=null) {
 				    				if(strCheckPromotionType.equalsIgnoreCase("L") || strCheckPromotionType.equalsIgnoreCase("F") || strCheckPromotionType.equalsIgnoreCase("E")){
			    						mapAmountAccommId.put(totalFlatLastAmount,accommodation.getAccommodationId());
 						      			arllist.add(totalFlatLastAmount);
 					    			}
 					    			
 				    			}else{
 				    				mapAmountAccommId.put(amount,accommodation.getAccommodationId());
 					      			arllist.add(amount);
 				    			}
 		      				 }else if(blnSoldout){
 		      					if(promotionType!=null && strCheckPromotionType!=null) {
	  		      					if(strCheckPromotionType.equalsIgnoreCase("L") || strCheckPromotionType.equalsIgnoreCase("F") || strCheckPromotionType.equalsIgnoreCase("E")){
	  		      						mapSoldOutAmountAccommId.put(totalFlatLastAmount,accommodation.getAccommodationId());
						      			arllist.add(totalFlatLastAmount);
					    			}
 		      					}else{
 				    				mapSoldOutAmountAccommId.put(amount,accommodation.getAccommodationId());
 					      			arllist.add(amount);
 				    			}
 					    	 }
 				    	 }
 				     }
 				     if(blnAvailable){
 				    	 if(promotionType!=null && strCheckPromotionType!=null) {
 				    		if(strCheckPromotionType.equalsIgnoreCase("L") || strCheckPromotionType.equalsIgnoreCase("F") || strCheckPromotionType.equalsIgnoreCase("E")){
 				    			arllistAvl.add(totalFlatLastAmount);
			    			} 
 				    	 }else{
 		    				arllistAvl.add(amount);
 		    			}
 				     }else if(!blnAvailable){
 				    	 if(promotionType!=null && strCheckPromotionType!=null) {
 				    		if(strCheckPromotionType.equalsIgnoreCase("L") || strCheckPromotionType.equalsIgnoreCase("F") || strCheckPromotionType.equalsIgnoreCase("E")){
 					    		 arllistSold.add(totalFlatLastAmount);
 			    			}
 				    	 }else{
 		    				arllistSold.add(amount);
 		    			}
 				     }
 				     accommCount++;
 				}
 			}
 			//end of accommodation
 			double totalAmount=0;
 			boolean blnJSONContent=false;
 			Collections.sort(arllist);
 			int arlListSize=0,accommId=0,intTotalAccomm=0;
 		     intTotalAccomm=accommodationList.size();
 		     if(intTotalAccomm==accommCount){
 		    	 if(intTotalAccomm==soldOutCount){
 			    	 if(blnPromoFlag){
 				    	 if(arlListPromoType.contains("F") || arlListPromoType.contains("E")){
 				    		 arlListSize=arllist.size();
 						     if(soldOutCount==arlListSize){
 						    	 Collections.sort(arllist);
 						    	 for(int i=0;i<arlListSize;i++){
 							    	 totalAmount=(Double) arllist.get(i);
 							    	 if((Double) arllist.get(0)==(Double) arllist.get(i)){
 							    		 if(blnAvailable){
 							    			 accommId=mapAmountAccommId.get(totalAmount);
 								    		 blnJSONContent=true;
 							    		 }else{
 							    			 accommId=mapSoldOutAmountAccommId.get(totalAmount);
 								    		 blnJSONContent=true;
 							    		 }
 							    		 
 							    		 break;
 							    	 }else{
 							    		 
 							    	 }
 							     }
 						     }
 				    	 }else if(arlListPromoType.contains("L")){
 				    		 /*if(promotionAccommId>0){
 					    		 accommId=promotionAccommId;
 					    		 blnJSONContent=true;
 					    	 } */
 				    		arlListSize=arllist.size();
	 					     Collections.sort(arllist);
	 				    	 for(int i=0;i<arlListSize;i++){
	 				    		 if(arllistAvl.contains((Double) arllist.get(i))){
	 				    			 if((Double) arllist.get(0)==(Double) arllist.get(i)){
	 					    			 totalAmount=(Double) arllist.get(i);
	 					    			 accommId=mapAmountAccommId.get(totalAmount);
	 						    		 blnJSONContent=true;
	 				    			 }else{
	 				    				 totalAmount=(Double) arllist.get(i);
	 					    			 accommId=mapAmountAccommId.get(totalAmount);
	 						    		 blnJSONContent=true;
	 				    			 }
	 				    			 break;
	 				    		 }
	 					     }
 				    	 }
 				     }else{
 					     arlListSize=arllist.size();
 					     if(soldOutCount==arlListSize){
 					    	 Collections.sort(arllist);
 					    	 for(int i=0;i<arlListSize;i++){
 						    	 totalAmount=(Double) arllist.get(i);
 						    	 if((Double) arllist.get(0)==(Double) arllist.get(i)){
 						    		 if(blnAvailable){
 						    			 accommId=mapAmountAccommId.get(totalAmount);
 							    		 blnJSONContent=true;
 						    		 }else{
 						    			 accommId=mapSoldOutAmountAccommId.get(totalAmount);
 							    		 blnJSONContent=true;
 						    		 }
 						    		 break;
 						    	 }else{
 						    		 
 						    	 }
 						     }
 					     }
 					     
 				     }
 			     }else{
 			    	 if(arlListPromoType.size()>0){
 			    		 if(arlListPromoType.contains("F") || arlListPromoType.contains("E")){
// 				    	 if(promotionType.equalsIgnoreCase("F") || promotionType.equalsIgnoreCase("L")){
 				    		 arlListSize=arllist.size();
 				    		 Collections.sort(arllist);
 					    	 for(int i=0;i<arlListSize;i++){
 					    		 if(arllistAvl.contains((Double) arllist.get(i))){
 					    			 if((Double) arllist.get(0)==(Double) arllist.get(i)){
 						    			 totalAmount=(Double) arllist.get(i);
 						    			 accommId=mapAmountAccommId.get(totalAmount);
 							    		 blnJSONContent=true;
 					    			 }else{
 					    				 totalAmount=(Double) arllist.get(i);
 						    			 accommId=mapAmountAccommId.get(totalAmount);
 							    		 blnJSONContent=true;
 					    			 }
 					    			 break;
 					    		 }
 						     }
 				    	 }else if( arlListPromoType.contains("L")){
 				    		 /*if(promotionAccommId>0){
 					    		 accommId=promotionAccommId;
 					    		 blnJSONContent=true;
 					    	 } */
 				    		arlListSize=arllist.size();
	 					     Collections.sort(arllist);
	 				    	 for(int i=0;i<arlListSize;i++){
	 				    		 if(arllistAvl.contains((Double) arllist.get(i))){
	 				    			 if((Double) arllist.get(0)==(Double) arllist.get(i)){
	 					    			 totalAmount=(Double) arllist.get(i);
	 					    			 accommId=mapAmountAccommId.get(totalAmount);
	 						    		 blnJSONContent=true;
	 				    			 }else{
	 				    				 totalAmount=(Double) arllist.get(i);
	 					    			 accommId=mapAmountAccommId.get(totalAmount);
	 						    		 blnJSONContent=true;
	 				    			 }
	 				    			 break;
	 				    		 }
	 					     }
 				    	 }
 				     }else{
 					     arlListSize=arllist.size();
 					     Collections.sort(arllist);
 				    	 for(int i=0;i<arlListSize;i++){
 				    		 if(arllistAvl.contains((Double) arllist.get(i))){
 				    			 if((Double) arllist.get(0)==(Double) arllist.get(i)){
 					    			 totalAmount=(Double) arllist.get(i);
 					    			 accommId=mapAmountAccommId.get(totalAmount);
 						    		 blnJSONContent=true;
 				    			 }else{
 				    				 totalAmount=(Double) arllist.get(i);
 					    			 accommId=mapAmountAccommId.get(totalAmount);
 						    		 blnJSONContent=true;
 				    			 }
 				    			 break;
 				    		 }
 					     }
 				     }
 			     }
 		     }
 		     arlListPromoType.clear();  
 			if(blnJSONContent){
 				int promoCount=0,enablePromoId=0;
 			    listAccommodationRoom.clear();
 			    ArrayList<Integer> arlEnablePromo=new ArrayList<Integer>();
 			    ArrayList<String> arlPromoType=new ArrayList<String>();
 		    	listAccommodationRoom=accommodationController.listPropertyTotalRooms(property.getPropertyId(), accommId);
 		    	List<DateTime> betweenAmount = DateUtil.getDateRange(start, end);
 		    	double actualPromoBaseAmount=0.0,actualBaseAmount=0.0,actualTotalAmount=0.0,actualTotalAdultAmount=0.0,actualTotalChildAmount=0.0,
 		    			actualAdultAmount=0.0,actualChildAmount=0.0,promotionFirstPercent=0.0,promotionFirstInr=0.0,
 	  				    		promotionSecondPercent=0.0,promotionSecondInr=0.0;
 			    int countDate=0,promotionId=0,promotionHours=0,promoAccommId=0; 
 			    boolean blnStatus=false,isPositive=false,isNegative=false,
 			    		promoFlag=false,blnBookedGetRooms=false,
 			    		blnLastMinutePromotions=false,blnLastMinuteIsActive=false,blnLastMinuteFirstActive=false;
 			    String promotionFirstName=null,promotionSecondName=null,promotionHoursRange=null,
 			    		promotionSecondDiscountType=null,promotionFirstDiscountType=null,strCheckPromotionType=null;
 			    Integer promoFirstPercent=null,promoFirstInr=null,promoSecondInr=null,promoSecondPercent=null;
 			    double promoBaseAmount=0.0,promoAdultAmount=0.0,promoChildAmount=0.0,smartPricePercent=0.0;
 			    double baseAmount=0.0,extraAdultAmount=0.0,extraChildAmount=0.0,totalBaseAmount=0.0,totalSmartPriceAmount=0.0,totalFlatAmount=0.0,totalLastAmount=0.0;
 			    int roomCount=0,availableCount=0,totalCount=0,discountId=0;
 			    double dblPercentCount=0.0,dblSmartPricePercent=0.0, flatAmount=0.0,lastAmount=0.0;
 			    double dblPercentFrom=0.0,dblPercentTo=0.0,lastMinuteHours=0.0;
 			    Timestamp lastMinStartDate=null,lastMinEndDate=null;
 				if(listAccommodationRoom.size()>0 && !listAccommodationRoom.isEmpty()){
 					for(AccommodationRoom roomsList:listAccommodationRoom){
 						 roomCount=(int) roomsList.getRoomCount();
 						 break;
 					 }
 				 }
 				 Timestamp tsFirstStartRange=null,tsSecondStartRange=null,tsFirstEndRange=null,tsSecondEndRange=null;
				 Timestamp tsLastStartDate=null,tsLastEndDate=null;
 				 Timestamp tsPromoBookedDate=null,tsPromoStartDate=null,tsPromoEndDate=null;
 				 boolean blnLastMinutePromo=false,blnEarlyBirdPromo=false,blnFirstActivePromo=false,blnFirstNotActivePromo=false,basePromoFlag=false;
				 boolean blnSecondActivePromo=false,blnSecondNotActivePromo=false;
				 SimpleDateFormat format1=new SimpleDateFormat("yyyy-MM-dd");
				 java.util.Date currentdate=new java.util.Date();
				 String strCurrentDate = new SimpleDateFormat("yyyy-MM-dd").format(currentdate);
			   	 java.util.Date curdate = format1.parse(strCurrentDate);
			   	 this.displayHoursForTimer=null;
			   	 setDisplayHoursForTimer(this.displayHoursForTimer);
				 List<PmsPromotions> listDatePromotions= promotionController.listBookedDatePromotions(property.getPropertyId(),curdate);
				 if(listDatePromotions.size()>0 && !listDatePromotions.isEmpty()){
					for(PmsPromotions promotions:listDatePromotions){
						tsPromoBookedDate=promotions.getPromotionBookedDate();
						tsPromoStartDate=promotions.getStartDate();
						tsPromoEndDate=promotions.getEndDate();
						blnEarlyBirdPromo=checkEarlyBirdPromos(tsPromoBookedDate,tsPromoStartDate,tsPromoEndDate,getArrivalDate(),getDepartureDate());
					}
				 }else{
					 blnEarlyBirdPromo=false;
				 }
				 
				 List<PmsPromotions> listLastPromotions= promotionController.listLastBookedPromotions(property.getPropertyId(),curdate);
				 if(listLastPromotions.size()>0 && !listLastPromotions.isEmpty()){
					for(PmsPromotions promotions:listLastPromotions){
						tsFirstStartRange=promotions.getFirstRangeStartDate();
						tsSecondStartRange=promotions.getSecondRangeStartDate();
						tsFirstEndRange=promotions.getFirstRangeEndDate();
						tsSecondEndRange=promotions.getSecondRangeEndDate();
						tsLastStartDate=promotions.getStartDate();
						tsLastEndDate=promotions.getEndDate();
						promotionHoursRange=promotions.getPromotionHoursRange();
						blnLastMinuteFirstActive=checkLastMinutePromos(promotionHoursRange,getArrivalDate(),getDepartureDate(),tsSecondStartRange,tsFirstEndRange,tsFirstEndRange,tsSecondStartRange);
//						blnLastMinuteIsActive=getCheckHoursRange(promotionHoursRange,getArrivalDate(),getDepartureDate());
						if(blnLastMinuteFirstActive){
							blnLastMinutePromo=true;
							enablePromoId=promotions.getPromotionId();
							arlEnablePromo.add(enablePromoId);
						}
					}
				 }else{
					 blnLastMinutePromo=false;
					 this.displayHoursForTimer=null;
			    	 setDisplayHoursForTimer(this.displayHoursForTimer);
				 }
				 ArrayList<Integer> arlFirstActivePromo=new ArrayList<Integer>(); 
				 ArrayList<Integer> arlSecondActivePromo=new ArrayList<Integer>(); 
				 List<DateTime> between = DateUtil.getDateRange(start, end);
				 for (DateTime d : between)
			     {
					 if(blnEarlyBirdPromo){
						 List<PmsPromotions> listEarlyPromotions= promotionController.listBookedDatePromotions(property.getPropertyId(),curdate);
						 if(listEarlyPromotions.size()>0 && !listEarlyPromotions.isEmpty()){
							 for(PmsPromotions promos:listEarlyPromotions){
								 if(promos.getPromotionType().equalsIgnoreCase("E")){
									 if(arlFirstActivePromo.isEmpty()){
										arlFirstActivePromo.add(promos.getPromotionId());
									 }else if(!arlFirstActivePromo.contains(promos.getPromotionId())){
										arlFirstActivePromo.add(promos.getPromotionId());
									 }
								 }else{
									blnFirstNotActivePromo=true;
								 }
							 }
						 }
					 }else if(!blnEarlyBirdPromo){
						 List<PmsPromotions> listAllPromotions= promotionController.listFlatDatePromotions(property.getPropertyId(),d.toDate());
						 if(!listAllPromotions.isEmpty() && listAllPromotions.size()>0){
							 for(PmsPromotions promos:listAllPromotions){
								 if(promos.getPromotionType().equalsIgnoreCase("F")){
									 if(arlFirstActivePromo.isEmpty()){
										arlFirstActivePromo.add(promos.getPromotionId());
									 }else if(!arlFirstActivePromo.contains(promos.getPromotionId())){
										arlFirstActivePromo.add(promos.getPromotionId());
									 }
								 }else{
									blnFirstNotActivePromo=true;
								 }
							 }
						 } 
					 }else{
						 blnFirstNotActivePromo=true;
					 }
					 
					 if(blnLastMinutePromo){
							List<PmsPromotions> listLastPromos= promotionController.listLastBookedPromotions(property.getPropertyId(),curdate);
							 if(listLastPromos.size()>0 && !listLastPromos.isEmpty()){
								for(PmsPromotions promos:listLastPromos){
									if(promos.getPromotionType().equalsIgnoreCase("L")){
										promoAccommId=promos.getPropertyAccommodation().getAccommodationId();
										if(arlEnablePromo.contains(promos.getPromotionId())){
											if(arlSecondActivePromo.isEmpty()){
		  										arlSecondActivePromo.add(promos.getPromotionId());
		  									 }else if(!arlSecondActivePromo.contains(promos.getPromotionId())){
		  										arlSecondActivePromo.add(promos.getPromotionId());
		  									 }
										}
									 }
								}
							 }else{
								blnSecondNotActivePromo=true;
							 }
						}else{
							blnSecondNotActivePromo=true;
						}
			     }
				 int intPromoSize=arlFirstActivePromo.size();
				 if((arlFirstActivePromo.isEmpty() || blnFirstNotActivePromo)){
					 blnFirstActivePromo=false;
				 }else{
					 blnFirstActivePromo=true;
				 }
				 
				 if(arlSecondActivePromo.isEmpty() || blnSecondNotActivePromo){
					blnSecondActivePromo=false;
				 }else{
					 blnSecondActivePromo=true;
				 }
 				
 				List<PropertyAccommodation> listAccommodation =  accommodationManager.list(property.getPropertyId(),accommId);
 				
 				for (PropertyAccommodation accommodations : listAccommodation) {
 					for (DateTime d : betweenAmount)
 				    {
 						boolean blnFlatPromotions=false;
 						if(blnFirstActivePromo){
 							if(blnEarlyBirdPromo){
 								List<PmsPromotions> listEarlyPromotions= promotionController.listBookedDatePromotions(property.getPropertyId(),curdate);
 								 if(listEarlyPromotions.size()>0 && !listEarlyPromotions.isEmpty()){
 	  					    		for(PmsPromotions promotions:listEarlyPromotions){
 	  					    			 if(promotions.getPromotionType().equalsIgnoreCase("E")){
 	  					    				 promotionId=promotions.getPromotionId();
 	  					    				 promotionType=promotions.getPromotionType();
 	  					    				 promotionFirstDiscountType=promotions.getPromotionDiscountType();
 	  					    				 listPromotionDetailCheck=promotionDetailController.listPromotionDetails(promotions.getPromotionId(),f.format(d.toDate()).toLowerCase());
 	  					    				 for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
 	  				    						if(promoCount==0){
 	  				    							promotionFirstPercent=promoDetails.getPromotionPercentage();
 	 	 					    					promotionFirstInr=promoDetails.getPromotionInr();
 	 	 					    					promotionType=promotions.getPromotionType();
 	  	  				    						 promotionFlag=true;
 	  	  						    				 blnPromoFlag=true;
 	  	  						    				 arlPromoType.add(promotionType);
 	  				    						}
 	  				    						promoCount++;
 	  				    					 }
 	  					    			 }
 	  					    		}
 	  					    	}
 							}else if(!blnEarlyBirdPromo){
 								List<PmsPromotions> listFlatPromotions= promotionController.listFlatDatePromotions(property.getPropertyId(),d.toDate());
 	  					    	if(listFlatPromotions.size()>0 && !listFlatPromotions.isEmpty()){
 	  					    		for(PmsPromotions promotions:listFlatPromotions){
 	  					    			 if(promotions.getPromotionType().equalsIgnoreCase("F")){
 	  					    				 promotionId=promotions.getPromotionId();
 	  					    				 promotionType=promotions.getPromotionType();
 	  					    				 promotionFirstDiscountType=promotions.getPromotionDiscountType();
 	  					    				 listPromotionDetailCheck=promotionDetailController.listPromotionDetails(promotions.getPromotionId(),f.format(d.toDate()).toLowerCase());
 	  					    				 for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
 	  				    						if(promoCount==0){
 	  				    							promotionFirstPercent=promoDetails.getPromotionPercentage();
 	 	 					    					promotionFirstInr=promoDetails.getPromotionInr();
 	 	 					    					promotionType=promotions.getPromotionType();
 	  	  				    						 promotionFlag=true;
 	  	  						    				 blnPromoFlag=true;
 	  	  						    				 arlPromoType.add(promotionType);
 	  				    						}
 	  				    						promoCount++;
 	  				    					 }
 	  					    			 }
 	  					    		}
 	  					    	}
 							}
 					    	
 						}
 						if(blnSecondActivePromo){

				    		List<PmsPromotions> listPmsPromotions= promotionController.listLastDatePromotions(property.getPropertyId(),accommId,curdate);
				    			 if(listPmsPromotions.size()>0 &&!listPmsPromotions.isEmpty()){
				    				 for(PmsPromotions pmsPromotions:listPmsPromotions){
				    					 promotionId=pmsPromotions.getPromotionId();
						    			 promoAccommId=pmsPromotions.getPropertyAccommodation().getAccommodationId();
						    			 promotionHoursRange=pmsPromotions.getPromotionHoursRange();
						    			 Timestamp tsStartDate=null,tsEndDate=null;
						    			 tsStartDate=pmsPromotions.getStartDate();
						    			 tsEndDate=pmsPromotions.getEndDate();
						    			 
						    			 if(arlEnablePromo.contains(pmsPromotions.getPromotionId())){
						    				 promotionAccommId=promoAccommId;
						    				 long minimumCount = getAvailableCount(property.getPropertyId(),getArrivalDate(),getDepartureDate(),roomCount,accommId);
			    			    			 if((int)minimumCount>0){
		    			    					 promotionFlag=true; 
		    			    					 blnPromoFlag=true;
		    			    					 strCheckPromotionType="L";
		    			    					 promotionType=pmsPromotions.getPromotionType();
		    			    					 arlPromoType.add(promotionType);
		    			    					 promotionSecondDiscountType=pmsPromotions.getPromotionDiscountType();
	 					    					 listPromotionDetailCheck=promotionDetailController.listPromotionDetails(pmsPromotions.getPromotionId(),f.format(curdate).toLowerCase());
	 					    					 if(listPromotionDetailCheck.size()>0 && !listPromotionDetailCheck.isEmpty()){
	 					    						for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
	    	 					    					promotionSecondPercent=promoDetails.getPromotionPercentage();
   	 	 					    					promotionSecondInr=promoDetails.getPromotionInr();
	    	 					    				 }
	 					    					 }
			    			    			 }else{
			    			    				 promotionFlag=false;
			    			    				 promotionType=null;
			    			    				 blnPromoFlag=false;
			    			    				 basePromoFlag=true;
			    			    			 }
						    			 }
				    				 }
				    			 }
 						}
 						if(promotionFlag){
			    			int rateCount=0,rateIdCount=0;
   						List<PropertyRate> propertyRateList =  rateController.listAllDates(property.getPropertyId(),accommodations.getAccommodationId(),getSourceTypeId(),d.toDate());
   				    	if(propertyRateList.size()>0)
   				    	{
   				    		 for (PropertyRate pr : propertyRateList)
   			    			 {
				    				 int propertyRateId = pr.getPropertyRateId();
				    				 List<PropertyRateDetail> rateDetailList =   detailController.list(propertyRateId,f.format(d.toDate()).toLowerCase());
				    				 if(rateDetailList.size()>0){
       				    			 if(rateCount<1){
       				    				 for (PropertyRateDetail rate : rateDetailList){
       				    					 baseAmount+=  rate.getBaseAmount();
       				    					 extraAdultAmount+=rate.getExtraAdult();
       	  				    				 extraChildAmount+=rate.getExtraChild();
	        				    			 }
       				    				 rateCount++;
       				    			 }
       				    		 }else{
       				    			 if(propertyRateList.size()==rateIdCount){
       				    				 baseAmount += accommodations.getBaseAmount();
       				    				 extraAdultAmount+=accommodations.getExtraAdult();
         				    				 extraChildAmount+=accommodations.getExtraChild();
       				    			 }
           				    	 }
   			    			 }	    		 
   				    	 }
   				    	 else{
   				    		baseAmount += accommodations.getBaseAmount();
   				    		extraAdultAmount+=accommodations.getExtraAdult();
			    				extraChildAmount+=accommodations.getExtraChild();
   				    	 }
				    		 double discountAmount=0;
				    		 if(blnFirstActivePromo){
				    			  String strTypePercent=String.valueOf(promotionFirstPercent);
	    				    	  String strTypeInr=String.valueOf(promotionFirstInr);
					    		  if(!strTypePercent.equalsIgnoreCase("null") && promotionFirstPercent!=0){
					    			  actualPromoBaseAmount = baseAmount-(baseAmount*promotionFirstPercent/100);
					    			  discountAmount+=baseAmount*promotionFirstPercent/100;
					    		  }
					    		  else if(!strTypeInr.equalsIgnoreCase("null") && promotionFirstInr!=0){
					    			  actualPromoBaseAmount = baseAmount-promotionFirstInr;
					    			  discountAmount+=promotionFirstInr;
					    		  }
					    		  else{
					    			  actualPromoBaseAmount = baseAmount;
					    		  } 
				    		 }else{
				    			actualPromoBaseAmount = baseAmount;
				    		 }
				    		 if(blnSecondActivePromo){
				    			String strTypePercent=String.valueOf(promotionSecondPercent);
 	    				    	String strTypeInr=String.valueOf(promotionSecondInr);
					    		  if(!strTypePercent.equalsIgnoreCase("null") && promotionSecondPercent!=0){
					    			  actualPromoBaseAmount = actualPromoBaseAmount-(actualPromoBaseAmount*promotionSecondPercent/100);
					    			  discountAmount+=baseAmount*promotionSecondPercent/100;
					    		  }
					    		  else if(!strTypeInr.equalsIgnoreCase("null") && promotionSecondInr!=0){
					    			  actualPromoBaseAmount= actualPromoBaseAmount-promotionSecondInr;
					    			  discountAmount+=promotionSecondInr;
					    		  }
					    		  else{
					    			  actualPromoBaseAmount = actualPromoBaseAmount;
					    		  }
				    			 
				    		 }else{
 				    			actualPromoBaseAmount = baseAmount;
 				    		 }
				    		 
				    		actualBaseAmount=baseAmount;
				    		totalFlatAmount=baseAmount-discountAmount;
				    		 
				    	 }else{
				    		 if(basePromoFlag){
				    			baseAmount += accommodations.getBaseAmount();
				    			extraAdultAmount+=accommodations.getExtraAdult();
				    			extraChildAmount+=accommodations.getExtraChild();
				    		}else{
				    			int rateCount=0,rateIdCount=0;
	    						List<PropertyRate> propertyRateList =  rateController.listAllDates(property.getPropertyId(),accommodations.getAccommodationId(),getSourceTypeId(),d.toDate());
	    				    	if(propertyRateList.size()>0)
	    				    	{
	    				    		 for (PropertyRate pr : propertyRateList)
	    			    			 {
					    				 int propertyRateId = pr.getPropertyRateId();
						    			 List<PropertyRateDetail> rateDetailList =   detailController.list(propertyRateId,f.format(d.toDate()).toLowerCase());
	        				    		 if(rateDetailList.size()>0){
	        				    			 if(rateCount<1){
	        				    				 for (PropertyRateDetail rate : rateDetailList){
	        				    					 baseAmount +=  rate.getBaseAmount();
	        				    					 extraAdultAmount+=rate.getExtraAdult();
	        	  				    				 extraChildAmount+=rate.getExtraChild();
		        				    			 }
	        				    				 rateCount++;
	        				    			 }
	        				    		 }else{
	        				    			 if(propertyRateList.size()==rateIdCount){
	        				    				 baseAmount += accommodations.getBaseAmount();
	        				    				 extraAdultAmount+=accommodations.getExtraAdult();
	          				    				 extraChildAmount+=accommodations.getExtraChild();
	        				    			 }
	            				    	 }
	    			    			 }	    		 
	    				    	 }
	    				    	 else{
	    				    		baseAmount += accommodations.getBaseAmount();
	    				    		extraAdultAmount+=accommodations.getExtraAdult();
				    				extraChildAmount+=accommodations.getExtraChild();
	    				    	 }
				    		}
				    	 }
 						 actualBaseAmount=baseAmount;
 				    	 countDate++;
 				    }
 					Iterator<String> iterPromoType=arlPromoType.iterator();
 					while(iterPromoType.hasNext()){
 						String strPromoType=iterPromoType.next();
 						if(strPromoType.equalsIgnoreCase("F") || strPromoType.equalsIgnoreCase("E")){
 							String strTypePercent=String.valueOf(promotionFirstPercent);
   				    	String strTypeInr=String.valueOf(promotionFirstInr);
				    		if(!strTypePercent.equalsIgnoreCase("null") && promotionFirstPercent!=0){
				    			promotionFirstName=String.valueOf(Math.round(promotionFirstPercent))+" % OFF";
	  							promoFirstPercent=(int) Math.round(promotionFirstPercent);
				    		}
				    		else if(!strTypeInr.equalsIgnoreCase("null") && promotionFirstInr!=0){
				    			promotionFirstName="Flat "+String.valueOf(Math.round(promotionFirstInr))+" OFF";
	  							promoFirstInr=(int) Math.round(promotionFirstInr);
				    		}
 							
 						}
 						if(strPromoType.equalsIgnoreCase("L")){
 							  String strTypePercent=String.valueOf(promotionSecondPercent);
 							  String strTypeInr=String.valueOf(promotionSecondInr);
				    		  if(!strTypePercent.equalsIgnoreCase("null") && promotionSecondPercent!=0){
				    			  promotionSecondName=String.valueOf(Math.round(promotionSecondPercent))+" % OFF";
		  						  promoSecondPercent=(int) Math.round(promotionSecondPercent);
				    		  }
				    		  else if(!strTypeInr.equalsIgnoreCase("null") && promotionSecondInr!=0){
				    			  promotionSecondName="Flat "+String.valueOf(Math.round(promotionSecondInr))+" OFF";
		  						  promoSecondInr=(int) Math.round(promotionSecondInr);
				    		  }
 							
 						}
 					}
 					arlPromoType.clear();
 				
 					if(blnPromoFlag){
 						if(promotionType.equalsIgnoreCase("L") || promotionType.equalsIgnoreCase("E") || promotionType.equalsIgnoreCase("F")){
 							totalAmount=totalFlatAmount;
 							
 							if(countDate>1){
 								actualAdultAmount=extraAdultAmount/countDate;
 								actualChildAmount=extraChildAmount/countDate;
 							}else{
 								actualAdultAmount=extraAdultAmount;
 								actualChildAmount=extraChildAmount;
 							}
 						}
 					}else{
 						totalAmount=baseAmount;
 						
 						if(countDate>1){
 							actualAdultAmount=extraAdultAmount/countDate;
 							actualChildAmount=extraChildAmount/countDate;
 						}else{
 							actualAdultAmount=extraAdultAmount;
 							actualChildAmount=extraChildAmount;
 						}
 					}
 					
 					
 					totalAmount=Math.round(totalAmount);
 					actualTotalAdultAmount=Math.round(actualAdultAmount);
 					actualTotalChildAmount=Math.round(actualChildAmount);
 					
 					
 					long minimum = getAvailableCount(property.getPropertyId(),getArrivalDate(),getDepartureDate(),roomCount,accommId);
 			    	 
 			    	 if(minimum==0){
 			    		 jsonOutput += ",\"available\":\"" + minimum+ "\"";
 			    		 jsonOutput += ",\"baseActualAmount\":\"" + + (actualBaseAmount==0 ? accommodations.getBaseAmount() : actualBaseAmount )+  "\"";
 			    		 jsonOutput += ",\"baseAmount\":\"" + + (totalAmount==0 ? accommodations.getBaseAmount() : totalAmount )+  "\"";
 			    		 jsonOutput += ",\"totalAmount\":\"" + + (totalAmount==0 ? accommodations.getBaseAmount() : totalAmount )+  "\"";
 			    		 jsonOutput += ",\"accommodationName\":\"" + accommodations.getAccommodationType().toUpperCase()+ "\"";
 						 jsonOutput += ",\"accommodationId\":\"" + accommodations.getAccommodationId()+ "\"";
 						 jsonOutput += ",\"promotionFirstName\":\"" + (promotionFirstName==null? "None":promotionFirstName)+ "\"";
 						 jsonOutput += ",\"promotionSecondName\":\"" + (promotionSecondName==null? "None":promotionSecondName)+ "\"";
 						 jsonOutput += ",\"promoFirstPercent\":\"" + (promoFirstPercent==null? 0:promoFirstPercent)+ "\"";
 						 jsonOutput += ",\"promoSecondPercent\":\"" + (promoSecondPercent==null? 0:promoSecondPercent)+ "\"";
 						 jsonOutput += ",\"promoFirstInr\":\"" + (promoFirstInr==null? 0:promoFirstInr)+ "\"";
						 jsonOutput += ",\"promoSecondInr\":\"" + (promoSecondInr==null? 0:promoSecondInr)+ "\"";
 						 jsonOutput += ",\"promoLastMinuteTimer\":\"" + (getDisplayHoursForTimer()==null ?"None" :getDisplayHoursForTimer()  )+ "\"";
 				     }else{
 				    	jsonOutput += ",\"accommodationName\":\"" + accommodations.getAccommodationType().toUpperCase()+ "\"";
 						jsonOutput += ",\"accommodationId\":\"" + accommodations.getAccommodationId()+ "\"";
 			    		jsonOutput += ",\"available\":\"" + minimum+ "\"";
 			    		jsonOutput += ",\"maxAdult\":\"" + accommodations.getMaxOccupancy()+ "\"";
 			    		jsonOutput += ",\"maxChild\":\"" + 0 + "\"";
 			    		jsonOutput += ",\"minOccupancy\":\"" + accommodations.getMinOccupancy() + "\"";
 						jsonOutput += ",\"maxOccupancy\":\"" + accommodations.getMaxOccupancy() + "\"";
 						jsonOutput += ",\"baseActualAmount\":\"" +  (actualBaseAmount==0 ? accommodations.getBaseAmount() : actualBaseAmount )+  "\"";
 						jsonOutput += ",\"promoFirstPercent\":\"" + (promoFirstPercent==null? 0:promoFirstPercent)+ "\"";
 						jsonOutput += ",\"promoFirstInr\":\"" + (promoFirstInr==null? 0:promoFirstInr)+ "\"";
						jsonOutput += ",\"promoSecondInr\":\"" + (promoSecondInr==null? 0:promoSecondInr)+ "\"";
 						jsonOutput += ",\"promoSecondPercent\":\"" + (promoSecondPercent==null? 0:promoSecondPercent)+ "\"";
 						jsonOutput += ",\"baseAmount\":\"" + + (totalAmount==0 ? accommodations.getBaseAmount() : totalAmount )+  "\"";
 			    		jsonOutput += ",\"totalAmount\":\"" + + (totalAmount==0 ? accommodations.getBaseAmount() : totalAmount )+  "\"";
 			    		jsonOutput += ",\"extraAdultAmount\":\"" + + (actualTotalAdultAmount==0 ? accommodations.getExtraAdult() : actualTotalAdultAmount )+  "\"";
 			    		jsonOutput += ",\"extraChildAmount\":\"" + + (actualTotalChildAmount==0 ? accommodations.getExtraChild() : actualTotalChildAmount )+  "\"";
 			    		jsonOutput += ",\"promotionFirstName\":\"" + (promotionFirstName==null? "None":promotionFirstName)+ "\"";
						jsonOutput += ",\"promotionSecondName\":\"" + (promotionSecondName==null? "None":promotionSecondName)+ "\"";
 			    		jsonOutput += ",\"promotionId\":\"" + (promotionId==0? 0:promotionId) + "\"";
 			    		jsonOutput += ",\"promoLastMinuteTimer\":\"" + (getDisplayHoursForTimer()==null ?"None" :getDisplayHoursForTimer()  )+ "\"";
 						discountId=35;
 						PropertyDiscountManager discountController=new PropertyDiscountManager();
 						PropertyDiscount propertyDiscount=discountController.find(discountId);
 			    		jsonOutput += ",\"discountPercent\":\"" + (propertyDiscount.getDiscountPercentage()==0.0? "0":propertyDiscount.getDiscountPercentage())+ "\"";
 			    		jsonOutput += ",\"discountId\":\"" + (discountId==0? "0" :discountId) + "\"";
 			    		
 			    		
 				     }
 				}
 		     
 			}
 			jsonOutput += "}";
 			
 			arllist.clear();
 		}
 	 
         
 		response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
// 		sessionMap.clear();
 		

 	} catch (Exception e) {
 		logger.error(e);
 		e.printStackTrace();
 	} finally {

 	}

 	
   	return null;
   
   }
   
   public String getNewAreaProperties(){
		try {
		
		 /* SeoContentManager seoContentController = new SeoContentManager();
	  	  SeoContent seoContent = seoContentController.findId(getAreaId());	
	  	        
	  	  request.setAttribute("metaTag" ,seoContent.getTitle() == null ? "": seoContent.getTitle());
	  	  request.setAttribute("description" ,seoContent.getDescription()  == null ? "": seoContent.getDescription());
	  	  request.setAttribute("content" ,seoContent.getContent() == null ? "": seoContent.getContent());*/
	  	
		Map mapDate=new HashMap();
		PropertyAreasManager areaController=new PropertyAreasManager();
		DateFormat format = new SimpleDateFormat("MMM d,yyyy", Locale.ENGLISH);
	    java.util.Date dateStart = format.parse((String)request.getAttribute("StartDate"));
	    java.util.Date dateEnd = format.parse((String)request.getAttribute("EndDate"));
	   
	   if(getSourceTypeId()==null){
	    	this.sourceTypeId=1;
	    }
	   
	    Calendar calCheckStart=Calendar.getInstance();
	    calCheckStart.setTime(dateStart);
	    java.util.Date checkInDate = calCheckStart.getTime();
	    this.arrivalDate=new Timestamp(checkInDate.getTime());
	   
	    
	    Calendar calCheckEnd=Calendar.getInstance();
	    calCheckEnd.setTime(dateEnd);
	    java.util.Date checkOutDate = calCheckEnd.getTime();
	    this.departureDate=new Timestamp(checkOutDate.getTime());
	    
	  	String checkIn = new SimpleDateFormat("MMM d,yyyy").format(checkInDate);
	    String checkOut = new SimpleDateFormat("MMM d,yyyy").format(checkOutDate);
	    String checkIn1 = new SimpleDateFormat("dd MMM").format(checkInDate);
	    String checkOut1 = new SimpleDateFormat("dd MMM").format(checkOutDate);
	      
	    sessionMap.put("checkIn",checkIn);
	    sessionMap.put("checkOut",checkOut);
	    sessionMap.put("checkIn1",checkIn1);
	    sessionMap.put("checkOut1",checkOut1);
    
	     
		/*this.arrivalDate = getArrivalDate();
	    sessionMap.put("arrivalDate",arrivalDate); 
		
		this.departureDate = getDepartureDate();
		sessionMap.put("departureDate",departureDate); */
		mapDate.put("arrivalDate", arrivalDate);
		mapDate.put("departureDate", departureDate);
		
		PmsPropertyManager propertyController = new PmsPropertyManager();		
		PropertyAccommodationManager accommodationManager = new PropertyAccommodationManager();
		PropertyAmenityManager amenityController = new PropertyAmenityManager();
		List<PropertyAmenity> propertyAmenityList;
		PropertyTypeManager typeController =  new PropertyTypeManager();
		PropertyRateManager rateController = new PropertyRateManager();
		PropertyRateDetailManager detailController = new PropertyRateDetailManager();
		PmsAmenityManager ameController = new PmsAmenityManager();
		PromotionManager promotionController=new PromotionManager();
		PromotionDetailManager promotionDetailController=new PromotionDetailManager();
		PmsBookingManager bookingController=new PmsBookingManager();
		
		
		HashMap<Double,Integer > mapAmountAccommId=new HashMap<Double, Integer>();
		HashMap<Double,Integer > mapSoldOutAmountAccommId=new HashMap<Double, Integer>();
		
		ArrayList<Integer> arlSortPropertyId=new ArrayList<Integer>();
		HashMap<Integer, Integer> mapAvailablePropertyId=new HashMap<Integer, Integer>();
		String jsonOutput = "";
		HttpServletResponse response = ServletActionContext.getResponse();
		
		response.setContentType("application/json");
		
		this.arrivalDate = getArrivalDate();
	    sessionMap.put("arrivalDate",arrivalDate); 
			
		this.departureDate = getDepartureDate();
		sessionMap.put("departureDate",departureDate); 
		 
		Calendar cal = Calendar.getInstance();
		cal.setTime(this.departureDate);
	    cal.add(Calendar.DATE, -1);
	    
	    java.util.Date dteCheckOut = cal.getTime();
	    this.departureDate=new Timestamp(dteCheckOut.getTime());
	    sessionMap.put("endDate",departureDate); 
	    
		String startDate = new SimpleDateFormat("yyyy-MM-dd").format(this.arrivalDate);
		String endDate = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
		DateTime start = DateTime.parse(startDate);
	    DateTime end = DateTime.parse(endDate);
	    
	    DateFormat f = new SimpleDateFormat("EEEE");
	    DecimalFormat df = new DecimalFormat("###.##");
    	List<PmsSmartPrice> listSmartPriceDetail=null; 
    	List<AccommodationRoom> listAccommodationRoom=null;
    	
    	List<PropertyAreas> listPropertyAreas=null;
    	PropertyReviewsManager reviewController=new PropertyReviewsManager();
    	PropertyAccommodationInventoryManager inventoryController=new PropertyAccommodationInventoryManager();
    	PropertyRatePlanDetailManager ratePlanDetailController=new PropertyRatePlanDetailManager();
		PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
		List<PmsProperty> listPmsProperty=new ArrayList<PmsProperty>();
	    int availRoomCount=0,soldRoomCount=0;
	    listPropertyAreas = areaController.listPropertyByArea(getAreaId());
	    if(listPropertyAreas.size()>0 && !listPropertyAreas.isEmpty()){
	    	for(PropertyAreas propertyareas: listPropertyAreas){
	    		int areaPropertyId=propertyareas.getPmsProperty().getPropertyId();
	    		List<PmsProperty> listProperty=propertyController.list(areaPropertyId);
	    		if(listProperty.size()>0 && !listProperty.isEmpty()){
	    			for(PmsProperty property:listProperty){
	    				listPmsProperty.add(property);
	    			}
	    		}
	    		
	    	}
	    }
	    
		for (PmsProperty property : listPmsProperty) {

			if (!jsonOutput.equalsIgnoreCase(""))
				jsonOutput += ",{";
			else
				jsonOutput += "{";
			jsonOutput += "\"DT_RowId\":\"" + property.getPropertyId()+ "\"";
			jsonOutput += ",\"city\":\"" + property.getCity() + "\"";
			jsonOutput += ",\"propertyTypeId\":\"" + property.getPropertyType().getPropertyTypeId() + "\"";
			jsonOutput += ",\"propertyTypeName\":\"" + property.getPropertyType().getPropertyTypeName() + "\"";
			jsonOutput += ",\"propertyName\":\"" + (property.getPropertyName() == null ? "": property.getPropertyName().toUpperCase().trim())+ "\"";
			jsonOutput += ",\"propertyUrl\":\"" + (property.getPropertyUrl() == null ? "": property.getPropertyUrl().toLowerCase().trim())+ "\"";
			
			if(property.getLocation()!=null){
				jsonOutput += ",\"locationName\":\"" + (property.getLocation().getLocationName() == null ? "": property.getLocation().getLocationName().toUpperCase().trim())+ "\"";	
			}else{
				jsonOutput += ",\"locationName\":\"" + "" + "\"";
			}
			
//			jsonOutput += ",\"propertyStandardPolicy\":\"" + (property.getPropertyStandardPolicy() == null ? "None": property.getPropertyStandardPolicy().trim())+ "\"";
//			jsonOutput += ",\"propertyHotelPolicy\":\"" + (property.getPropertyHotelPolicy() == null ? "None": property.getPropertyHotelPolicy().trim())+ "\"";
//			jsonOutput += ",\"propertyCancellationPolicy\":\"" + (property.getPropertyCancellationPolicy() == null ? "None": property.getPropertyCancellationPolicy().trim())+ "\"";
			jsonOutput += ",\"payAtHotelStatus\":\"" + (property.getPayAtHotelStatus() == null ? false: property.getPayAtHotelStatus())+ "\"";
			jsonOutput += ",\"coupleStatus\":\"" + ( property.getCoupleIsActive() == null? false :  property.getCoupleIsActive() )+ "\"";
			jsonOutput += ",\"breakfastStatus\":\"" + ( property.getBreakfastIsActive() == null? false:  property.getBreakfastIsActive() )+ "\"";
			jsonOutput += ",\"hotelStatus\":\"" + ( property.getHotelIsActive() == null? false:  property.getHotelIsActive() )+ "\"";
			
			SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
			Timestamp FromDate=(Timestamp)mapDate.get("arrivalDate");
			Timestamp ToDate=(Timestamp)mapDate.get("departureDate");
			jsonOutput += ",\"arrivalDate\":\"" + format2.format(FromDate) + "\"";
			jsonOutput += ",\"departureDate\":\"" + format2.format(ToDate)+ "\"";
			
			String strFromDate=format2.format(FromDate);
			String strToDate=format2.format(ToDate);
	    	DateTime startdate = DateTime.parse(strFromDate);
	        DateTime enddate = DateTime.parse(strToDate);
	        java.util.Date fromdate = startdate.toDate();
	 		java.util.Date todate = enddate.toDate();
	 		 
			int diffInDays = (int) ((todate.getTime() - fromdate.getTime()) / (1000 * 60 * 60 * 24));
			
			List arllist=new ArrayList();
			List arllistAvl=new ArrayList();
			List arllistSold=new ArrayList();
			jsonOutput += ",\"diffDays\":\"" + diffInDays+ "\"";
			
			String jsonPhotos="";
 			PropertyPhotoManager photoController = new PropertyPhotoManager();
			
		    response.setContentType("application/json");
		    List<PropertyPhoto> listPhotos =photoController.list(property.getPropertyId());
           
           if(listPhotos.size()>0)
 			{
           	for (PropertyPhoto photo : listPhotos) {

   				if (!jsonPhotos.equalsIgnoreCase(""))
   					jsonPhotos += ",{";
   				else
   					jsonPhotos += "{";
   				jsonPhotos += "\"DT_RowId\":\"" + photo.getPropertyPhotoId() + "\"";
   				jsonPhotos += ",\"isPrimary\":\"" + photo.getIsPrimary()+ "\"";
   				jsonPhotos += ",\"photoPath\":\"" + photo.getPhotoPath()+ "\"";
   				
   				
   				jsonPhotos += "}";


   			}
   			
                  jsonOutput += ",\"photos\":[" + jsonPhotos+ "]";
 			
 			}
			
			String jsonAmenities ="";
			propertyAmenityList =  amenityController.list(property.getPropertyId());
			if(propertyAmenityList.size()>0)
			{
				for (PropertyAmenity amenity : propertyAmenityList) {
					if (!jsonAmenities.equalsIgnoreCase(""))
						
						jsonAmenities += ",{";
					else
						jsonAmenities += "{";
					PropertyAmenity amentity1 = amenityController.find(amenity.getPropertyAmenityId());
				    String path = "ulowebsite/images/icons/";
					PmsAmenity pmsAmenity1 =ameController.find(amentity1.getPmsAmenity().getAmenityId());
					jsonAmenities += "\"DT_RowId\":\"" + amenity.getPropertyAmenityId()+ "\"";					
					jsonAmenities += ",\"amenityName\":\"" + pmsAmenity1.getAmenityName()+ "\"";
					jsonAmenities += ",\"amenityId\":\"" + pmsAmenity1.getAmenityId()+ "\"";
                    jsonAmenities += ",\"icon\":\"" + path + pmsAmenity1.getAmenityIcon()+ "\"";
             
					
					jsonAmenities += "}";
				}
				
				jsonOutput += ",\"amenities\":[" + jsonAmenities+ "]";
			}
			
			String jsonReviews="";
			List<PropertyReviews> listReviews=reviewController.listReview(property.getPropertyId());
			if(listReviews.size()>0 && !listReviews.isEmpty()){
				for(PropertyReviews reviews:listReviews){
					
					if (!jsonReviews.equalsIgnoreCase(""))
						
						jsonReviews += ",{";
					else
						jsonReviews += "{";

					/*jsonReviews += "\"starCount\":\"" + reviews.getStarCount().intValue()+ "\"";	
					jsonReviews += ",\"reviewCount\":\"" + reviews.getReviewCount().intValue()+ "\"";
					Double averageReviews=reviews.getAverageReview();
					jsonReviews += ",\"averageReview\":\"" + df.format(averageReviews)+ "\"";*/
					
					jsonReviews += "\"starCount\":\"" + (reviews.getStarCount()==null?0:reviews.getStarCount().intValue())+ "\"";	
					jsonReviews += ",\"reviewCount\":\"" + (reviews.getReviewCount()==null?0:reviews.getReviewCount().intValue())+ "\"";	
					if(reviews.getAverageReview()!=null){
						Double averageReviews=reviews.getAverageReview();
						jsonReviews += ",\"averageReview\":\"" + df.format(averageReviews)+ "\"";
					}else{
						jsonReviews += ",\"averageReview\":\"" + df.format(0)+ "\"";
					}
					
					jsonReviews += ",\"tripadvisorStarCount\":\"" + (reviews.getTripadvisorStarCount()==null?0:reviews.getTripadvisorStarCount().intValue())+ "\"";	
					jsonReviews += ",\"tripadvisorReviewCount\":\"" + (reviews.getTripadvisorReviewCount()==null?0:reviews.getTripadvisorReviewCount().intValue())+ "\"";
					if(reviews.getTripadvisorAverageReview()!=null){
						Double taAverageReviews=reviews.getTripadvisorAverageReview();
						jsonReviews += ",\"tripadvisorAverageReview\":\"" + df.format(taAverageReviews)+ "\"";
					}else{
						jsonReviews += ",\"tripadvisorAverageReview\":\"" + df.format(0)+ "\"";
					}
					jsonReviews += ",\"googleReviewUrl\":\"" + (reviews.getGoogleReviewUrl()==null?"":reviews.getGoogleReviewUrl())+ "\"";	
					jsonReviews += ",\"tripadvisorReviewUrl\":\"" + (reviews.getTripadvisorReviewUrl()==null?"":reviews.getTripadvisorReviewUrl())+ "\"";	
					jsonReviews += ",\"propertyRating\":\"" + (reviews.getPropertyRating()==null?"None":reviews.getPropertyRating())+ "\"";
					jsonReviews += "}";
				}
				
			}else{
				if (!jsonReviews.equalsIgnoreCase(""))
					
					jsonReviews += ",{";
				else
					jsonReviews += "{";
				
				jsonReviews += "\"starCount\":\"" + 0+ "\"";	
				jsonReviews += ",\"reviewCount\":\"" + 0+ "\"";	
				jsonReviews += ",\"averageReview\":\"" + 0+ "\"";
				
				jsonReviews += ",\"tripadvisorStarCount\":\"" + 0 + "\"";	
				jsonReviews += ",\"tripadvisorReviewCount\":\"" + 0 + "\"";	
				jsonReviews += ",\"tripadvisorAverageReview\":\"" + 0+ "\"";
				
				jsonReviews += ",\"googleReviewUrl\":\"" + "None"+ "\"";	
				jsonReviews += ",\"tripadvisorReviewUrl\":\"" + "None"+ "\"";	
				jsonReviews += ",\"propertyRating\":\"" + "None"+ "\"";
				
				jsonReviews += "}";
			}
			
			jsonOutput += ",\"reviews\":[" + jsonReviews+ "]";
			
			
			String jsonAccommodation ="";
			List<PropertyAccommodation> accommodationsList =  accommodationManager.list(property.getPropertyId());
			int soldOutCount=0,availCount=0,promotionAccommId=0;
			if(accommodationsList.size()>0 && !accommodationsList.isEmpty())
			{
				for (PropertyAccommodation accommodation : accommodationsList) {
					int roomCount=0;
					listAccommodationRoom=accommodationController.listPropertyTotalRooms(property.getPropertyId(), accommodation.getAccommodationId());
					if(listAccommodationRoom.size()>0 && !listAccommodationRoom.isEmpty()){
						for(AccommodationRoom roomsList:listAccommodationRoom){
							 roomCount=(int) roomsList.getRoomCount();
							 long minimum = getAvailableCount(property.getPropertyId(),getArrivalDate(),getDepartureDate(),roomCount,accommodation.getAccommodationId());
							 if((int)minimum>0){
								 availCount++;
							 }else if((int)minimum==0){
								 soldOutCount++;
							 }
							 break;
						 }
					 }
					
					
					if (!jsonAccommodation.equalsIgnoreCase(""))
						
						jsonAccommodation += ",{";
					else
						jsonAccommodation += "{";
						jsonAccommodation += "\"accommodationId\":\"" + accommodation.getAccommodationId()+ "\"";
						jsonAccommodation += ",\"accommodationName\":\"" + accommodation.getAccommodationType().toUpperCase()+ "\"";
					
					jsonAccommodation += "}";
				}
				
				jsonOutput += ",\"accommodations\":[" + jsonAccommodation+ "]";
			}

			
			List<PmsPromotionDetails> listPromotionDetailCheck=null;
			Boolean blnAvailable=false,blnSoldout=false,firstPromotionFlag=false,secondPromotionFlag=false,promotionFlag=false,blnPromoFlag=false;
			int arlListTotalAccomm=0,soldOutTotalAccomm=0,accommCount=0;
			String promotionType=null;
			ArrayList<String> arlListPromoType=new ArrayList<String>();
			List<PropertyAccommodation> accommodationList =  accommodationManager.list(property.getPropertyId());
			if(accommodationList.size()>0)
			{
				for (PropertyAccommodation accommodation : accommodationList) {
					
					int dateCount=0;
					double amount=0.0,extraAdultAmount=0.0,extraChildAmount=0.0;
					int roomCount=0,availableCount=0,totalCount=0;
				    double dblPercentCount=0.0;
	    			listAccommodationRoom=accommodationController.listPropertyTotalRooms(property.getPropertyId(), accommodation.getAccommodationId());
	    			List<DateTime> between = DateUtil.getDateRange(start, end); 
					if(listAccommodationRoom.size()>0 && !listAccommodationRoom.isEmpty()){
						
						for(AccommodationRoom roomsList:listAccommodationRoom){
							roomCount=(int) roomsList.getRoomCount();
							if(roomCount>0){
								long minimum = getAvailableCount(property.getPropertyId(),getArrivalDate(),getDepartureDate(),roomCount,accommodation.getAccommodationId());
								if((int)minimum>0){
									blnAvailable=true;
									blnSoldout=false;
									arlListTotalAccomm++;
								}else if((int)minimum==0){
									blnSoldout=true;
									blnAvailable=false;
									soldOutTotalAccomm++;
								}
							}
							 break;
						 }
					 }
				     for (DateTime d : between)
				     {
				    	 long rmc = 0;
			   			 this.roomCnt = rmc;
			        	 int sold=0;
			        	 java.util.Date availDate = d.toDate();
						 PropertyAccommodationInventory inventoryCount = inventoryController.findInventoryCount(availDate,accommodation.getAccommodationId());
						 if(inventoryCount != null){
						 PmsAvailableRooms roomCount1 = bookingController.findBookingCount(availDate, accommodation.getAccommodationId(),inventoryCount.getInventoryId());
						 if(roomCount1.getRoomCount() == null) {
						 	String num1 = inventoryCount.getInventoryCount();
							PmsAvailableRooms bookingRoomCount=bookingController.findCount(availDate, accommodation.getAccommodationId());
							PropertyAccommodation accomm=accommodationController.find(accommodation.getAccommodationId());
							Integer totavailable=0;
							if(accommodation!=null){
								totavailable=accommodation.getNoOfUnits();
							}
							if(bookingRoomCount.getRoomCount()!=null){
								long lngRooms=bookingRoomCount.getRoomCount();
								long num2 = Long.parseLong(num1);
								this.roomCnt = num2;
								sold=(int) (lngRooms);
								int availRooms=0;
								availRooms=totavailable-sold;
								if(Integer.parseInt(num1)>availRooms){
									this.roomCnt = availRooms;	
								}else{
									this.roomCnt = num2;
								}
										
							}else{
								 long num2 = Long.parseLong(num1);
								 this.roomCnt = num2;
								 sold=(int) (rmc);
							}
						}else{		
							PmsAvailableRooms bookingRoomCount=bookingController.findCount(availDate, accommodation.getAccommodationId());
							int intRooms=0;
							Integer totavailable=0,availableRooms=0;
							if(accommodation!=null){
								totavailable=accommodation.getNoOfUnits();
							}
							if(bookingRoomCount.getRoomCount()!=null){
								long lngRooms=bookingRoomCount.getRoomCount();
								long num = roomCount1.getRoomCount();	
								intRooms=(int)lngRooms;
								String num1 = inventoryCount.getInventoryCount();
			 					long num2 = Long.parseLong(num1);
			 					availableRooms=totavailable-intRooms;
			 					if(num2>availableRooms){
			 						this.roomCnt = availableRooms;	 
			 					}else{
			 						this.roomCnt = num2-num;
			 					}
			 					long lngSold=bookingRoomCount.getRoomCount();
			   					sold=(int)lngSold;
							}else{
								long num = roomCount1.getRoomCount();	
								String num1 = inventoryCount.getInventoryCount();
								long num2 = Long.parseLong(num1);
			  					this.roomCnt = (num2-num);
			  					long lngSold=roomCount1.getRoomCount();
			   					sold=(int)lngSold;
							}
		
						}
										
					}else{
						  PmsAvailableRooms inventoryCounts = bookingController.findCount(availDate, accommodation.getAccommodationId());							
							if(inventoryCounts.getRoomCount() == null){								
								this.roomCnt = (accommodation.getNoOfUnits()-rmc);
								sold=(int)rmc;
							}
							else{
								this.roomCnt = (accommodation.getNoOfUnits()-inventoryCounts.getRoomCount());	
								long lngSold=inventoryCounts.getRoomCount();
	   							sold=(int)lngSold;
							}
					}
										
					double totalRooms=0,intSold=0;
					totalRooms=(double)accommodation.getNoOfUnits();
					intSold=(double)sold;
					double occupancy=0.0;
					occupancy=intSold/totalRooms;
					double sellrate=0.0,otarate=0.0;
					Integer occupancyRating=(int) Math.round(occupancy * 100);
						    int rateCount=0,rateIdCount=0;
						    double minAmount=0,maxAmount=0;
						    List<PropertyRate> dateList =  rateController.listAllDates(property.getPropertyId(),accommodation.getAccommodationId(),this.sourceTypeId,d.toDate());
							if(dateList.size()>0 && !dateList.isEmpty()){
								for(PropertyRate rates : dateList) {
									int propertyRateId = rates.getPropertyRateId();
									List<PropertyRateDetail> rateDetailList =   detailController.listMinMaxRateDetail(propertyRateId,f.format(d.toDate()).toLowerCase());
	   				    		 	if(rateDetailList.size()>0 && !rateDetailList.isEmpty()){
	   				    		 		if(rateCount<1){
	   				    		 			for (PropertyRateDetail rate : rateDetailList){
		   				    					 minAmount=rate.getMinimumBaseAmount();
		   				    					 maxAmount=rate.getMaximumBaseAmount();
		   				    				    if(occupancyRating==0){
			   				 						sellrate = minAmount;
			   				 					}else if(occupancyRating>0 && occupancyRating<=30){
			   				 			        	sellrate = minAmount;
			   				 			        }else if(occupancyRating>=31 && occupancyRating<=60){
			   				 			        	sellrate = minAmount+(minAmount*10/100);
			   				 			        }else if(occupancyRating>=61 && occupancyRating<=80){
			   				 			        	sellrate = minAmount+(minAmount*15/100);
			   				 			        }else if(occupancyRating>=81 && occupancyRating<=100){
			   				 			        	sellrate = maxAmount;
			   				 			        }
		   				    					 amount+=  sellrate;
	   				    		 			}
	   				    		 			rateCount++;
	   				    		 			rateIdCount++;
	   				    		 		}
	   				    		 }else{
	   				    			 if(rateIdCount<1){
	   				    				 if(accommodation.getMinimumBaseAmount()!=null){
	   				    					minAmount=accommodation.getMinimumBaseAmount();	 
	   				    				 }
	   				    				 if(accommodation.getMaximumBaseAmount()!=null){
	   				    					maxAmount=accommodation.getMaximumBaseAmount();	 
	   				    				 }
	   				    				 
	   				    				if(occupancyRating==0){
	   				 						sellrate = minAmount;
	   				 					}else if(occupancyRating>0 && occupancyRating<=30){
	   				 			        	sellrate = minAmount;
	   				 			        }else if(occupancyRating>=31 && occupancyRating<=60){
	   				 			        	sellrate = minAmount+(minAmount*10/100);
	   				 			        }else if(occupancyRating>=61 && occupancyRating<=80){
	   				 			        	sellrate = minAmount+(minAmount*15/100);
	   				 			        }else if(occupancyRating>=81 && occupancyRating<=100){
	   				 			        	sellrate = maxAmount;
	   				 			        }
	   				    				 amount+= sellrate;
	   				    				 rateIdCount++;
	   				    				 rateCount++;
	   				    			 }
	       				    	 }
					    	}
					     }else{

					    	 if(accommodation.getMinimumBaseAmount()!=null){
		    					minAmount=accommodation.getMinimumBaseAmount();	 
		    				 }
		    				 if(accommodation.getMaximumBaseAmount()!=null){
		    					maxAmount=accommodation.getMaximumBaseAmount();	 
		    				 }
		    				 
		    				if(occupancyRating==0){
		 						sellrate = minAmount;
		 					}else if(occupancyRating>0 && occupancyRating<=30){
		 			        	sellrate = minAmount;
		 			        }else if(occupancyRating>=31 && occupancyRating<=60){
		 			        	sellrate = minAmount+(minAmount*10/100);
		 			        }else if(occupancyRating>=61 && occupancyRating<=80){
		 			        	sellrate = minAmount+(minAmount*15/100);
		 			        }else if(occupancyRating>=81 && occupancyRating<=100){
		 			        	sellrate = maxAmount;
		 			        }
		    				 amount+= sellrate;
		    			 
					     }
				    	 dateCount++;
				    	 if(dateCount==diffInDays){
				    		 if(blnAvailable){
			    				mapAmountAccommId.put(amount,accommodation.getAccommodationId());
				      			arllist.add(amount);
		      				 }else if(blnSoldout){
		      					mapSoldOutAmountAccommId.put(amount,accommodation.getAccommodationId());
				      			arllist.add(amount);
					    	 }
				    	 }
				     }
				     if(blnAvailable){
				    	 arllistAvl.add(amount);
				     }else if(!blnAvailable){
				    	 arllistSold.add(amount);
				     }
				     accommCount++;
				}
			}
			//end of accommodation
			double totalAmount=0;
			boolean blnJSONContent=false;
			Collections.sort(arllist);
			int arlListSize=0,accommId=0,intTotalAccomm=0;
		     intTotalAccomm=accommodationList.size();
		     if(intTotalAccomm==accommCount){
		    	 if(intTotalAccomm==soldOutCount){
				     arlListSize=arllist.size();
				     if(soldOutCount==arlListSize){
				    	 Collections.sort(arllist);
				    	 for(int i=0;i<arlListSize;i++){
					    	 totalAmount=(Double) arllist.get(i);
					    	 if((Double) arllist.get(0)==(Double) arllist.get(i)){
					    		 if(blnAvailable){
					    			 accommId=mapAmountAccommId.get(totalAmount);
						    		 blnJSONContent=true;
					    		 }else{
					    			 accommId=mapSoldOutAmountAccommId.get(totalAmount);
						    		 blnJSONContent=true;
					    		 }
					    		 break;
					    	 }else{
					    		 
					    	 }
					     }
				     }
		    	 }else{
				     arlListSize=arllist.size();
				     Collections.sort(arllist);
			    	 for(int i=0;i<arlListSize;i++){
			    		 if(arllistAvl.contains((Double) arllist.get(i))){
			    			 if((Double) arllist.get(0)==(Double) arllist.get(i)){
				    			 totalAmount=(Double) arllist.get(i);
				    			 accommId=mapAmountAccommId.get(totalAmount);
					    		 blnJSONContent=true;
			    			 }else{
			    				 totalAmount=(Double) arllist.get(i);
				    			 accommId=mapAmountAccommId.get(totalAmount);
					    		 blnJSONContent=true;
			    			 }
			    			 break;
			    		 }
				     }
		    	 }
		     }
		     arlListPromoType.clear();  
			if(blnJSONContent){
			    listAccommodationRoom.clear(); 
		    	listAccommodationRoom=accommodationController.listPropertyTotalRooms(property.getPropertyId(), accommId);
		    	List<DateTime> betweenAmount = DateUtil.getDateRange(start, end);
		    	double actualPromoBaseAmount=0.0,actualBaseAmount=0.0,actualTotalAmount=0.0,actualTotalAdultAmount=0.0,actualTotalChildAmount=0.0,
		    			actualAdultAmount=0.0,actualChildAmount=0.0;
			    int countDate=0; 
			    boolean blnStatus=false;
			    
			    double baseAmount=0.0,extraAdultAmount=0.0,extraChildAmount=0.0,totalBaseAmount=0.0,
			    		totalMinimumAmount=0,totalMaximumAmount=0,totalSellRate=0;
			    int roomCount=0,availableCount=0,totalCount=0,discountId=0;
			    
				if(listAccommodationRoom.size()>0 && !listAccommodationRoom.isEmpty()){
					for(AccommodationRoom roomsList:listAccommodationRoom){
						 roomCount=(int) roomsList.getRoomCount();
						 break;
					 }
				 }
				 
				ArrayList arlRateType=new ArrayList();
				 List<PropertyRatePlanDetail> listPlanDetails=ratePlanDetailController.listAccommodation(property.getPropertyId(), accommId);
				 if(listPlanDetails.size()>0 && !listPlanDetails.isEmpty()){
		        	for(PropertyRatePlanDetail ratePlanDetails:listPlanDetails){
		        		arlRateType.add(ratePlanDetails.getRatePlan().getRatePlanType());
		        		arlRateType.add(ratePlanDetails.getRatePlan().getRatePlanSymbolType());
		        		arlRateType.add(ratePlanDetails.getTariffAmount());
		        	}
				 }
				 
				 List<DateTime> between = DateUtil.getDateRange(start, end);
				 SimpleDateFormat format1=new SimpleDateFormat("yyyy-MM-dd");
				 java.util.Date currentdate=new java.util.Date();
				 String strCurrentDate = new SimpleDateFormat("yyyy-MM-dd").format(currentdate);
			   	 java.util.Date dtecurdate = format1.parse(strCurrentDate);
			   	 Calendar calStart=Calendar.getInstance();
			   	 calStart.setTime(dtecurdate);
			   	 calStart.setTimeZone(TimeZone.getTimeZone("IST"));
			   	 String StartDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calStart.getTime());
			   	 java.util.Date curdate = format1.parse(StartDate);
				List<PropertyAccommodation> listAccommodation =  accommodationManager.list(property.getPropertyId(),accommId);
				int dateCount=0;
				String jsonPrices="";
				for (PropertyAccommodation accommodations : listAccommodation) {
					
					
					for (DateTime d : betweenAmount){

				    	 long rmc = 0;
			   			 this.roomCnt = rmc;
			        	 int sold=0;
			        	 java.util.Date availDate = d.toDate();
						 PropertyAccommodationInventory inventoryCount = inventoryController.findInventoryCount(availDate,accommodations.getAccommodationId());
						 if(inventoryCount != null){
						 PmsAvailableRooms roomCount1 = bookingController.findBookingCount(availDate, accommodations.getAccommodationId(),inventoryCount.getInventoryId());
						 if(roomCount1.getRoomCount() == null) {
						 	String num1 = inventoryCount.getInventoryCount();
							PmsAvailableRooms bookingRoomCount=bookingController.findCount(availDate, accommodations.getAccommodationId());
							Integer totavailable=0;
							if(accommodations!=null){
								totavailable=accommodations.getNoOfUnits();
							}
							if(bookingRoomCount.getRoomCount()!=null){
								long lngRooms=bookingRoomCount.getRoomCount();
								long num2 = Long.parseLong(num1);
								this.roomCnt = num2;
								sold=(int) (lngRooms);
								int availRooms=0;
								availRooms=totavailable-sold;
								if(Integer.parseInt(num1)>availRooms){
									this.roomCnt = availRooms;	
								}else{
									this.roomCnt = num2;
								}
										
							}else{
								 long num2 = Long.parseLong(num1);
								 this.roomCnt = num2;
								 sold=(int) (rmc);
							}
						}else{		
							PmsAvailableRooms bookingRoomCount=bookingController.findCount(availDate, accommodations.getAccommodationId());
							int intRooms=0;
							Integer totavailable=0,availableRooms=0;
							if(accommodations!=null){
								totavailable=accommodations.getNoOfUnits();
							}
							if(bookingRoomCount.getRoomCount()!=null){
								long lngRooms=bookingRoomCount.getRoomCount();
								long num = roomCount1.getRoomCount();	
								intRooms=(int)lngRooms;
								String num1 = inventoryCount.getInventoryCount();
			 					long num2 = Long.parseLong(num1);
			 					availableRooms=totavailable-intRooms;
			 					if(num2>availableRooms){
			 						this.roomCnt = availableRooms;	 
			 					}else{
			 						this.roomCnt = num2-num;
			 					}
			 					long lngSold=bookingRoomCount.getRoomCount();
			   					sold=(int)lngSold;
							}else{
								long num = roomCount1.getRoomCount();	
								String num1 = inventoryCount.getInventoryCount();
								long num2 = Long.parseLong(num1);
			  					this.roomCnt = (num2-num);
			  					long lngSold=roomCount1.getRoomCount();
			   					sold=(int)lngSold;
							}
		
						}
										
					}else{
						  PmsAvailableRooms inventoryCounts = bookingController.findCount(availDate, accommodations.getAccommodationId());							
							if(inventoryCounts.getRoomCount() == null){								
								this.roomCnt = (accommodations.getNoOfUnits()-rmc);
								sold=(int)rmc;
							}
							else{
								this.roomCnt = (accommodations.getNoOfUnits()-inventoryCounts.getRoomCount());	
								long lngSold=inventoryCounts.getRoomCount();
	   							sold=(int)lngSold;
							}
						}
											
						double totalRooms=0,intSold=0;
						
						totalRooms=(double)accommodations.getNoOfUnits();
						intSold=(double)sold;
						double occupancy=0.0;
						occupancy=intSold/totalRooms;
						double sellrate=0.0,otarate=0.0;
						Integer occupancyRating=(int) Math.round(occupancy * 100);
					    int rateCount=0,rateIdCount=0;
					    double minAmount=0,maxAmount=0;
					    List<PropertyRate> dateList =  rateController.listAllDates(property.getPropertyId(),accommodations.getAccommodationId(),getSourceTypeId(),d.toDate());
						if(!dateList.isEmpty()){
							for(PropertyRate rates : dateList) {
					    		int propertyRateId = rates.getPropertyRateId();
				    			List<PropertyRateDetail> rateDetailList =   detailController.listMinMaxRateDetail(propertyRateId,f.format(d.toDate()).toLowerCase());
	   				    		 if(rateDetailList.size()>0 && !rateDetailList.isEmpty()){
	   				    			 if(rateCount<1){
	   				    				 for (PropertyRateDetail rate : rateDetailList){
	   				    					 minAmount=rate.getMinimumBaseAmount();
	   				    					 maxAmount=rate.getMaximumBaseAmount();
	   				    					 extraAdultAmount=rate.getExtraAdult();
	   				    					 extraChildAmount=rate.getExtraChild();
	   				    				    if(occupancyRating==0){
		   				 						sellrate = minAmount;
		   				 					}else if(occupancyRating>0 && occupancyRating<=30){
		   				 			        	sellrate = minAmount;
		   				 			        }else if(occupancyRating>=31 && occupancyRating<=60){
		   				 			        	sellrate = minAmount+(minAmount*10/100);
		   				 			        }else if(occupancyRating>=61 && occupancyRating<=80){
		   				 			        	sellrate = minAmount+(minAmount*15/100);
		   				 			        }else if(occupancyRating>=81 && occupancyRating<=100){
		   				 			        	sellrate = maxAmount;
		   				 			        }
	   				    				    baseAmount+=  sellrate;
	       				    			 }
	   				    				 rateCount++;
	   				    				rateIdCount++;
	   				    			 }
	   				    		 }else{
	   				    			 if(rateIdCount<1){
	   				    				if(accommodations.getMinimumBaseAmount()!=null){
	   				    					minAmount=accommodations.getMinimumBaseAmount();	 
	   				    				 }
	   				    				 if(accommodations.getMaximumBaseAmount()!=null){
	   				    					maxAmount=accommodations.getMaximumBaseAmount();	 
	   				    				 }
	   				    				 extraAdultAmount=accommodations.getExtraAdult();
  				    					 extraChildAmount=accommodations.getExtraChild();
	   				    				if(occupancyRating==0){
	   				 						sellrate = minAmount;
	   				 					}else if(occupancyRating>0 && occupancyRating<=30){
	   				 			        	sellrate = minAmount;
	   				 			        }else if(occupancyRating>=31 && occupancyRating<=60){
	   				 			        	sellrate = minAmount+(minAmount*10/100);
	   				 			        }else if(occupancyRating>=61 && occupancyRating<=80){
	   				 			        	sellrate = minAmount+(minAmount*15/100);
	   				 			        }else if(occupancyRating>=81 && occupancyRating<=100){
	   				 			        	sellrate = maxAmount;
	   				 			        }
	   				    				baseAmount+= sellrate;
	   				    				rateIdCount++;
	   				    				rateCount++;
	   				    			 }
	       				    	 }
					    	}
					     }else{

					    	 if(accommodations.getMinimumBaseAmount()!=null){
		    					minAmount=accommodations.getMinimumBaseAmount();	 
		    				 }
		    				 if(accommodations.getMaximumBaseAmount()!=null){
		    					maxAmount=accommodations.getMaximumBaseAmount();	 
		    				 }
		    				 extraAdultAmount=accommodations.getExtraAdult();
		    				 extraChildAmount=accommodations.getExtraChild();
		    				 
		    				if(occupancyRating==0){
		 						sellrate = minAmount;
		 					}else if(occupancyRating>0 && occupancyRating<=30){
		 			        	sellrate = minAmount;
		 			        }else if(occupancyRating>=31 && occupancyRating<=60){
		 			        	sellrate = minAmount+(minAmount*10/100);
		 			        }else if(occupancyRating>=61 && occupancyRating<=80){
		 			        	sellrate = minAmount+(minAmount*15/100);
		 			        }else if(occupancyRating>=81 && occupancyRating<=100){
		 			        	sellrate = maxAmount;
		 			        }
		    				baseAmount+= sellrate;
		    			 
					     }
						totalMinimumAmount+=minAmount;
						totalMaximumAmount+=maxAmount;
						totalSellRate+=sellrate;
						if (!jsonPrices.equalsIgnoreCase(""))
							jsonPrices += ",{";
						else
							jsonPrices += "{";
						
						
						jsonPrices += "\"inventoryRating\":\"" + (occupancyRating)+ "\"";
						jsonPrices += ",\"minimumAmount\":\"" + (minAmount)+ "\"";
						jsonPrices += ",\"maximumAmount\":\"" + + (maxAmount)+  "\"";
						jsonPrices += ",\"sellrate\":\"" + + (sellrate)+  "\"";	
						jsonPrices += ",\"extraAdult\":\"" + (extraAdultAmount)+ "\"";
						jsonPrices += ",\"extraChild\":\"" + (extraChildAmount)+  "\"";
						jsonPrices += ",\"date\":\"" +  (format1.format(availDate))+  "\"";
					
						jsonPrices += "}";
						
				    	countDate++;
				    }
					
					jsonOutput += ",\"rates\":[" + jsonPrices+ "]";
					
					double variationAmount=0,variationRating=0;
					variationAmount=totalMaximumAmount-totalSellRate;
					variationRating=variationAmount/totalMaximumAmount*100;
					String promotionFirstName=null;
//					promotionFirstName=String.valueOf(Math.round(variationRating))+" % OFF";
					/*if(variationRating>9){
						promotionFirstName=String.valueOf(Math.round(variationRating))+" % OFF";	
					}else{
						promotionFirstName="Flat "+String.valueOf(Math.round(variationAmount))+" OFF";
					}*/
					if(variationRating>0){
						promotionFirstName=String.valueOf(Math.round(variationRating))+" % OFF";	
					}
					double dblRatePlanAmount=0,dblEPAmount=0,dblTotalEPAmount=0;
			        Iterator iterRatePlan=arlRateType.iterator();
			        String strType=null,strSymbol=null;
	            	while(iterRatePlan.hasNext()){
	            		strType=(String)iterRatePlan.next();
	            		strSymbol=(String)iterRatePlan.next();
	            		dblRatePlanAmount=(Double)iterRatePlan.next();
	            		
	            		if(strType.equalsIgnoreCase("EP")){
	                		if(strSymbol.equalsIgnoreCase("plus")){
	                			dblEPAmount=baseAmount+dblRatePlanAmount;	
	                		}else{
	                			dblEPAmount=baseAmount-dblRatePlanAmount;
	                		}
	            		}
	            		
	            	}
					totalAmount=baseAmount;
					dblTotalEPAmount=dblEPAmount;
					if(dblTotalEPAmount==0){
						dblTotalEPAmount=baseAmount;
					}
					if(countDate>1){
						actualAdultAmount=extraAdultAmount/countDate;
						actualChildAmount=extraChildAmount/countDate;
					}else{
						actualAdultAmount=extraAdultAmount;
						actualChildAmount=extraChildAmount;
					}
				
					
					
					totalAmount=Math.round(totalAmount);
					actualTotalAdultAmount=Math.round(actualAdultAmount);
					actualTotalChildAmount=Math.round(actualChildAmount);
					
					if(totalMinimumAmount==0){
						totalBaseAmount=0;
					}
					if(totalMinimumAmount>0){
						totalBaseAmount=totalMinimumAmount;
					}
	            	
					long minimum = getAvailableCount(property.getPropertyId(),getArrivalDate(),getDepartureDate(),roomCount,accommId);
			    	 
			    	 if(minimum==0){
			    		 jsonOutput += ",\"available\":\"" + minimum+ "\"";
			    		 jsonOutput += ",\"minimumAmount\":\"" + + (totalMinimumAmount==0 ? totalBaseAmount : totalMinimumAmount )+  "\"";
			    		 jsonOutput += ",\"maximumAmount\":\"" + + (totalMaximumAmount==0 ? totalBaseAmount : totalMaximumAmount )+  "\"";
			    		 jsonOutput += ",\"baseActualAmount\":\"" + + (totalMaximumAmount==0 ? totalBaseAmount : totalMaximumAmount )+  "\"";
			    		 jsonOutput += ",\"baseAmount\":\"" + + (totalAmount==0 ? totalBaseAmount : totalAmount )+  "\"";
			    		 jsonOutput += ",\"totalAmount\":\"" + + (totalAmount==0 ? totalBaseAmount : totalAmount )+  "\"";
			    		 jsonOutput += ",\"cpAmount\":\"" + + (totalAmount==0 ? totalBaseAmount : totalAmount )+  "\"";
			    		 jsonOutput += ",\"epAmount\":\"" + + (dblTotalEPAmount)+  "\"";
			    		 jsonOutput += ",\"promotionFirstName\":\"" + (promotionFirstName==null? "None":promotionFirstName)+ "\"";
			    		 jsonOutput += ",\"accommodationName\":\"" + accommodations.getAccommodationType().toUpperCase()+ "\"";
						 jsonOutput += ",\"accommodationId\":\"" + accommodations.getAccommodationId()+ "\"";
				     }else{
				    	jsonOutput += ",\"accommodationName\":\"" + accommodations.getAccommodationType().toUpperCase()+ "\"";
						jsonOutput += ",\"accommodationId\":\"" + accommodations.getAccommodationId()+ "\"";
			    		jsonOutput += ",\"available\":\"" + minimum+ "\"";
			    		jsonOutput += ",\"maxAdult\":\"" + accommodations.getMaxOccupancy()+ "\"";
			    		jsonOutput += ",\"maxChild\":\"" + 0 + "\"";
			    		jsonOutput += ",\"minOccupancy\":\"" + accommodations.getMinOccupancy() + "\"";
						jsonOutput += ",\"maxOccupancy\":\"" + accommodations.getMaxOccupancy() + "\"";
						jsonOutput += ",\"minimumAmount\":\"" + + (totalMinimumAmount==0 ? totalBaseAmount : totalMinimumAmount )+  "\"";
						jsonOutput += ",\"maximumAmount\":\"" + + (totalMaximumAmount==0 ? totalBaseAmount : totalMaximumAmount )+  "\"";
						jsonOutput += ",\"baseActualAmount\":\"" +  (totalMaximumAmount==0 ? totalBaseAmount : totalMaximumAmount )+  "\"";
						jsonOutput += ",\"baseAmount\":\"" + + (totalAmount==0 ? totalBaseAmount : totalAmount )+  "\"";
			    		jsonOutput += ",\"totalAmount\":\"" + + (totalAmount==0 ? totalBaseAmount : totalAmount )+  "\"";
			    		jsonOutput += ",\"cpAmount\":\"" + + (totalAmount==0 ? totalBaseAmount : totalAmount )+  "\"";
			    		jsonOutput += ",\"epAmount\":\"" + + (dblTotalEPAmount)+  "\"";
			    		jsonOutput += ",\"promotionFirstName\":\"" + (promotionFirstName==null? "None":promotionFirstName)+ "\"";
			    		jsonOutput += ",\"extraAdultAmount\":\"" + + (actualTotalAdultAmount==0 ? accommodations.getExtraAdult() : actualTotalAdultAmount )+  "\"";
			    		jsonOutput += ",\"extraChildAmount\":\"" + + (actualTotalChildAmount==0 ? accommodations.getExtraChild() : actualTotalChildAmount )+  "\"";
						discountId=35;
						PropertyDiscountManager discountController=new PropertyDiscountManager();
						PropertyDiscount propertyDiscount=discountController.find(discountId);
			    		jsonOutput += ",\"discountPercent\":\"" + (propertyDiscount.getDiscountPercentage()==0.0? "0":propertyDiscount.getDiscountPercentage())+ "\"";
			    		jsonOutput += ",\"discountId\":\"" + (discountId==0? "0" :discountId) + "\"";
			    		
			    		
				     }
				}
		     
			}
			
			jsonOutput += "}";
			
			arllist.clear();
		
		
		}
	 
        
		response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
//		sessionMap.clear();
		

	} catch (Exception e) {
		logger.error(e);
		e.printStackTrace();
	} finally {

	}

	
  	return null;
  
   }
   
   public String getFirstAreaProperties(){
   	

 		try {
 		
 		 /* SeoContentManager seoContentController = new SeoContentManager();
 	  	  SeoContent seoContent = seoContentController.findId(getAreaId());	
 	  	        
 	  	  request.setAttribute("metaTag" ,seoContent.getTitle() == null ? "": seoContent.getTitle());
 	  	  request.setAttribute("description" ,seoContent.getDescription()  == null ? "": seoContent.getDescription());
 	  	  request.setAttribute("content" ,seoContent.getContent() == null ? "": seoContent.getContent());*/
 	  	
 		Map mapDate=new HashMap();
 		PropertyAreasManager areaController=new PropertyAreasManager();
 		DateFormat format = new SimpleDateFormat("MMM d,yyyy", Locale.ENGLISH);
 	    java.util.Date dateStart = format.parse((String)request.getAttribute("StartDate"));
 	    java.util.Date dateEnd = format.parse((String)request.getAttribute("EndDate"));
 	   if(getSourceTypeId()==null){
	    	this.sourceTypeId=1;
	    }
 	    
 	    Calendar calCheckStart=Calendar.getInstance();
 	    calCheckStart.setTime(dateStart);
 	    java.util.Date checkInDate = calCheckStart.getTime();
 	    this.arrivalDate=new Timestamp(checkInDate.getTime());
 	   
 	    
 	    Calendar calCheckEnd=Calendar.getInstance();
 	    calCheckEnd.setTime(dateEnd);
 	    java.util.Date checkOutDate = calCheckEnd.getTime();
 	    this.departureDate=new Timestamp(checkOutDate.getTime());
 	    
	  	String checkIn = new SimpleDateFormat("MMM d,yyyy").format(checkInDate);
	    String checkOut = new SimpleDateFormat("MMM d,yyyy").format(checkOutDate);
	    String checkIn1 = new SimpleDateFormat("dd MMM").format(checkInDate);
	    String checkOut1 = new SimpleDateFormat("dd MMM").format(checkOutDate);
	      
	    sessionMap.put("checkIn",checkIn);
	    sessionMap.put("checkOut",checkOut);
	    sessionMap.put("checkIn1",checkIn1);
	    sessionMap.put("checkOut1",checkOut1);
     
 	     
 		/*this.arrivalDate = getArrivalDate();
 	    sessionMap.put("arrivalDate",arrivalDate); 
 		
 		this.departureDate = getDepartureDate();
 		sessionMap.put("departureDate",departureDate); */
 		mapDate.put("arrivalDate", arrivalDate);
 		mapDate.put("departureDate", departureDate);
 		
 		PmsPropertyManager propertyController = new PmsPropertyManager();		
 		PropertyAccommodationManager accommodationManager = new PropertyAccommodationManager();
 		PropertyAmenityManager amenityController = new PropertyAmenityManager();
 		List<PropertyAmenity> propertyAmenityList;
 		PropertyTypeManager typeController =  new PropertyTypeManager();
 		PropertyRateManager rateController = new PropertyRateManager();
 		PropertyRateDetailManager detailController = new PropertyRateDetailManager();
 		PmsAmenityManager ameController = new PmsAmenityManager();
 		PromotionManager promotionController=new PromotionManager();
 		PromotionDetailManager promotionDetailController=new PromotionDetailManager();
 		PmsBookingManager bookingController=new PmsBookingManager();
 		
 		
 		HashMap<Double,Integer > mapAmountAccommId=new HashMap<Double, Integer>();
 		HashMap<Double,Integer > mapSoldOutAmountAccommId=new HashMap<Double, Integer>();
 		
 		ArrayList<Integer> arlSortPropertyId=new ArrayList<Integer>();
 		HashMap<Integer, Integer> mapAvailablePropertyId=new HashMap<Integer, Integer>();
 		String jsonOutput = "";
 		HttpServletResponse response = ServletActionContext.getResponse();
 		
 		response.setContentType("application/json");
 		
 		this.arrivalDate = getArrivalDate();
 	    sessionMap.put("arrivalDate",arrivalDate); 
 			
 		this.departureDate = getDepartureDate();
 		sessionMap.put("departureDate",departureDate); 
 		 
 		Calendar cal = Calendar.getInstance();
 		cal.setTime(this.departureDate);
 	    cal.add(Calendar.DATE, -1);
 	    
 	    java.util.Date dteCheckOut = cal.getTime();
 	    this.departureDate=new Timestamp(dteCheckOut.getTime());
 	    sessionMap.put("endDate",departureDate); 
 	    
 		String startDate = new SimpleDateFormat("yyyy-MM-dd").format(this.arrivalDate);
 		String endDate = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
 		DateTime start = DateTime.parse(startDate);
 	    DateTime end = DateTime.parse(endDate);
 	    
 	    DateFormat f = new SimpleDateFormat("EEEE");
 	    DecimalFormat df = new DecimalFormat("###.##");
     	List<PmsSmartPrice> listSmartPriceDetail=null; 
     	List<AccommodationRoom> listAccommodationRoom=null;
     	
     	List<PropertyAreas> listPropertyAreas=null;
 		PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
 		List<PmsProperty> listPmsProperty=new ArrayList<PmsProperty>();
 	    int availRoomCount=0,soldRoomCount=0;
 	    listPropertyAreas = areaController.listPropertyByArea(getAreaId(),limit);
 	    if(listPropertyAreas.size()>0 && !listPropertyAreas.isEmpty()){
 	    	for(PropertyAreas propertyareas: listPropertyAreas){
 	    		int areaPropertyId=propertyareas.getPmsProperty().getPropertyId();
 	    		List<PmsProperty> listProperty=propertyController.list(areaPropertyId);
 	    		if(listProperty.size()>0 && !listProperty.isEmpty()){
 	    			for(PmsProperty property:listProperty){
 	    				listPmsProperty.add(property);
 	    			}
 	    		}
 	    		
 	    	}
 	    }
 	    
 		for (PmsProperty property : listPmsProperty) {
 			
 			if (!jsonOutput.equalsIgnoreCase(""))
 				jsonOutput += ",{";
 			else
 				jsonOutput += "{";
 			jsonOutput += "\"DT_RowId\":\"" + property.getPropertyId()+ "\"";
 			jsonOutput += ",\"city\":\"" + property.getCity() + "\"";
 			jsonOutput += ",\"propertyTypeId\":\"" + property.getPropertyType().getPropertyTypeId() + "\"";
 			jsonOutput += ",\"propertyTypeName\":\"" + property.getPropertyType().getPropertyTypeName() + "\"";
 			jsonOutput += ",\"propertyName\":\"" + (property.getPropertyName() == null ? "": property.getPropertyName().toUpperCase().trim())+ "\"";
 			jsonOutput += ",\"propertyUrl\":\"" + (property.getPropertyUrl() == null ? "": property.getPropertyUrl().toLowerCase().trim())+ "\"";
// 			jsonOutput += ",\"propertyStandardPolicy\":\"" + (property.getPropertyStandardPolicy() == null ? "None": property.getPropertyStandardPolicy().trim())+ "\"";
// 			jsonOutput += ",\"propertyHotelPolicy\":\"" + (property.getPropertyHotelPolicy() == null ? "None": property.getPropertyHotelPolicy().trim())+ "\"";
// 			jsonOutput += ",\"propertyCancellationPolicy\":\"" + (property.getPropertyCancellationPolicy() == null ? "None": property.getPropertyCancellationPolicy().trim())+ "\"";
 			jsonOutput += ",\"payAtHotelStatus\":\"" + (property.getPayAtHotelStatus() == null ? false: property.getPayAtHotelStatus())+ "\"";
			jsonOutput += ",\"coupleStatus\":\"" + ( property.getCoupleIsActive() == null? false :  property.getCoupleIsActive() )+ "\"";
			jsonOutput += ",\"breakfastStatus\":\"" + ( property.getBreakfastIsActive() == null? false:  property.getBreakfastIsActive() )+ "\"";
			jsonOutput += ",\"hotelStatus\":\"" + ( property.getHotelIsActive() == null? false:  property.getHotelIsActive() )+ "\"";
			
 			SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
 			Timestamp FromDate=(Timestamp)mapDate.get("arrivalDate");
 			Timestamp ToDate=(Timestamp)mapDate.get("departureDate");
 			jsonOutput += ",\"arrivalDate\":\"" + format2.format(FromDate) + "\"";
 			jsonOutput += ",\"departureDate\":\"" + format2.format(ToDate)+ "\"";
 			
 			String strFromDate=format2.format(FromDate);
 			String strToDate=format2.format(ToDate);
 	    	DateTime startdate = DateTime.parse(strFromDate);
 	        DateTime enddate = DateTime.parse(strToDate);
 	        java.util.Date fromdate = startdate.toDate();
 	 		java.util.Date todate = enddate.toDate();
 	 		 
 			int diffInDays = (int) ((todate.getTime() - fromdate.getTime()) / (1000 * 60 * 60 * 24));
 			
 			List arllist=new ArrayList();
 			List arllistAvl=new ArrayList();
 			List arllistSold=new ArrayList();
 			jsonOutput += ",\"diffDays\":\"" + diffInDays+ "\"";
 			
 			String jsonPhotos="";
  			PropertyPhotoManager photoController = new PropertyPhotoManager();
			
		    response.setContentType("application/json");
		    List<PropertyPhoto> listPhotos =photoController.list(property.getPropertyId());
            
            if(listPhotos.size()>0)
  			{
            	for (PropertyPhoto photo : listPhotos) {

    				if (!jsonPhotos.equalsIgnoreCase(""))
    					jsonPhotos += ",{";
    				else
    					jsonPhotos += "{";
    				jsonPhotos += "\"DT_RowId\":\"" + photo.getPropertyPhotoId() + "\"";
    				jsonPhotos += ",\"isPrimary\":\"" + photo.getIsPrimary()+ "\"";
    				jsonPhotos += ",\"photoPath\":\"" + photo.getPhotoPath()+ "\"";
    				
    				
    				jsonPhotos += "}";


    			}
    			
                   jsonOutput += ",\"photos\":[" + jsonPhotos+ "]";
  			
  			}
 			
 			String jsonAmenities ="";
 			propertyAmenityList =  amenityController.list(property.getPropertyId());
 			if(propertyAmenityList.size()>0)
 			{
 				for (PropertyAmenity amenity : propertyAmenityList) {
 					if (!jsonAmenities.equalsIgnoreCase(""))
 						
 						jsonAmenities += ",{";
 					else
 						jsonAmenities += "{";
 					PropertyAmenity amentity1 = amenityController.find(amenity.getPropertyAmenityId());
 				    String path = "ulowebsite/images/icons/";
 					PmsAmenity pmsAmenity1 =ameController.find(amentity1.getPmsAmenity().getAmenityId());
 					jsonAmenities += "\"DT_RowId\":\"" + amenity.getPropertyAmenityId()+ "\"";					
 					jsonAmenities += ",\"amenityName\":\"" + pmsAmenity1.getAmenityName()+ "\"";
 					jsonAmenities += ",\"amenityId\":\"" + pmsAmenity1.getAmenityId()+ "\"";
                     jsonAmenities += ",\"icon\":\"" + path + pmsAmenity1.getAmenityIcon()+ "\"";
              
 					
 					jsonAmenities += "}";
 				}
 				
 				jsonOutput += ",\"amenities\":[" + jsonAmenities+ "]";
 			}
 			
 			String jsonReviews="";
			PropertyReviewsManager reviewController=new PropertyReviewsManager();
			List<PropertyReviews> listReviews=reviewController.listReview(property.getPropertyId());
			if(listReviews.size()>0 && !listReviews.isEmpty()){
				for(PropertyReviews reviews:listReviews){
					
					if (!jsonReviews.equalsIgnoreCase(""))
						
						jsonReviews += ",{";
					else
						jsonReviews += "{";

					/*jsonReviews += "\"starCount\":\"" + reviews.getStarCount().intValue()+ "\"";	
					jsonReviews += ",\"reviewCount\":\"" + reviews.getReviewCount().intValue()+ "\"";
					Double averageReviews=reviews.getAverageReview();
					jsonReviews += ",\"averageReview\":\"" + df.format(averageReviews)+ "\"";*/
					
					jsonReviews += "\"starCount\":\"" + (reviews.getStarCount()==null?0:reviews.getStarCount().intValue())+ "\"";	
					jsonReviews += ",\"reviewCount\":\"" + (reviews.getReviewCount()==null?0:reviews.getReviewCount().intValue())+ "\"";	
					if(reviews.getAverageReview()!=null){
						Double averageReviews=reviews.getAverageReview();
						jsonReviews += ",\"averageReview\":\"" + df.format(averageReviews)+ "\"";
					}else{
						jsonReviews += ",\"averageReview\":\"" + df.format(0)+ "\"";
					}
					
					jsonReviews += ",\"tripadvisorStarCount\":\"" + (reviews.getTripadvisorStarCount()==null?0:reviews.getTripadvisorStarCount().intValue())+ "\"";	
					jsonReviews += ",\"tripadvisorReviewCount\":\"" + (reviews.getTripadvisorReviewCount()==null?0:reviews.getTripadvisorReviewCount().intValue())+ "\"";
					if(reviews.getTripadvisorAverageReview()!=null){
						Double taAverageReviews=reviews.getTripadvisorAverageReview();
						jsonReviews += ",\"tripadvisorAverageReview\":\"" + df.format(taAverageReviews)+ "\"";
					}else{
						jsonReviews += ",\"tripadvisorAverageReview\":\"" + df.format(0)+ "\"";
					}
					jsonReviews += ",\"googleReviewUrl\":\"" + (reviews.getGoogleReviewUrl()==null?"":reviews.getGoogleReviewUrl())+ "\"";	
					jsonReviews += ",\"tripadvisorReviewUrl\":\"" + (reviews.getTripadvisorReviewUrl()==null?"":reviews.getTripadvisorReviewUrl())+ "\"";	
					jsonReviews += ",\"propertyRating\":\"" + (reviews.getPropertyRating()==null?"None":reviews.getPropertyRating())+ "\"";
					jsonReviews += "}";
				}
				
			}else{
				if (!jsonReviews.equalsIgnoreCase(""))
					
					jsonReviews += ",{";
				else
					jsonReviews += "{";
				
				jsonReviews += "\"starCount\":\"" + 0+ "\"";	
				jsonReviews += ",\"reviewCount\":\"" + 0+ "\"";	
				jsonReviews += ",\"averageReview\":\"" + 0+ "\"";
				
				jsonReviews += ",\"tripadvisorStarCount\":\"" + 0 + "\"";	
				jsonReviews += ",\"tripadvisorReviewCount\":\"" + 0 + "\"";	
				jsonReviews += ",\"tripadvisorAverageReview\":\"" + 0+ "\"";
				
				jsonReviews += ",\"googleReviewUrl\":\"" + "None"+ "\"";	
				jsonReviews += ",\"tripadvisorReviewUrl\":\"" + "None"+ "\"";	
				jsonReviews += ",\"propertyRating\":\"" + "None"+ "\"";
				
				jsonReviews += "}";
			}
			
			jsonOutput += ",\"reviews\":[" + jsonReviews+ "]";
			
 			
 			String jsonAccommodation ="";
 			List<PropertyAccommodation> accommodationsList =  accommodationManager.list(property.getPropertyId());
 			int soldOutCount=0,availCount=0,promotionAccommId=0;
 			if(accommodationsList.size()>0 && !accommodationsList.isEmpty())
 			{
 				for (PropertyAccommodation accommodation : accommodationsList) {
 					int roomCount=0;
 					listAccommodationRoom=accommodationController.listPropertyTotalRooms(property.getPropertyId(), accommodation.getAccommodationId());
 					if(listAccommodationRoom.size()>0 && !listAccommodationRoom.isEmpty()){
 						for(AccommodationRoom roomsList:listAccommodationRoom){
 							 roomCount=(int) roomsList.getRoomCount();
 							 long minimum = getAvailableCount(property.getPropertyId(),getArrivalDate(),getDepartureDate(),roomCount,accommodation.getAccommodationId());
 							 if((int)minimum>0){
 								 availCount++;
 							 }else if((int)minimum==0){
 								 soldOutCount++;
 							 }
 							 break;
 						 }
 					 }
 					
 					
 					if (!jsonAccommodation.equalsIgnoreCase(""))
 						
 						jsonAccommodation += ",{";
 					else
 						jsonAccommodation += "{";
 						jsonAccommodation += "\"accommodationId\":\"" + accommodation.getAccommodationId()+ "\"";
 						jsonAccommodation += ",\"accommodationName\":\"" + accommodation.getAccommodationType().toUpperCase()+ "\"";
 					
 					jsonAccommodation += "}";
 				}
 				
 				jsonOutput += ",\"accommodations\":[" + jsonAccommodation+ "]";
 			}

 			
 			List<PmsPromotionDetails> listPromotionDetailCheck=null;
 			Boolean blnAvailable=false,blnSoldout=false,firstPromotionFlag=false,secondPromotionFlag=false,promotionFlag=false,blnPromoFlag=false;
 			int arlListTotalAccomm=0,soldOutTotalAccomm=0,accommCount=0;
 			String promotionType=null;
 			ArrayList<String> arlListPromoType=new ArrayList<String>();
 			List<PropertyAccommodation> accommodationList =  accommodationManager.list(property.getPropertyId());
 			if(accommodationList.size()>0)
 			{
 				for (PropertyAccommodation accommodation : accommodationList) {
 					Integer promoAccommId=0,enablePromoId=0;
 					ArrayList<Integer> arlEnablePromo=new ArrayList<Integer>();
 					Boolean blnPromotionIsNotFlag=false;
 					Boolean blnLastMinuteIsActive=false,blnLastMinuteFirstActive=false;
 					int dateCount=0,promoCount=0;
 					String strCheckPromotionType=null,promotionFirstDiscountType=null,promotionSecondDiscountType=null,promotionHoursRange=null;
 					double amount=0.0,extraAdultAmount=0.0,extraChildAmount=0.0,totalSmartPriceAmount=0.0;
 					int roomCount=0,availableCount=0,totalCount=0;
 				    double dblPercentCount=0.0,dblSmartPricePercent=0.0,
 				    		promotionFirstPercent=0.0,promotionFirstInr=0.0,
 				    		promotionSecondPercent=0.0,promotionSecondInr=0.0,totalFlatLastAmount=0.0,flatLastAmount=0.0;
 				    Timestamp lastMinStartDate=null,lastMinEndDate=null;
 				    Boolean isPositive=false,isNegative=false,blnLastMinutePromotions=false;
 				   
 	    			
 	    			listAccommodationRoom=accommodationController.listPropertyTotalRooms(property.getPropertyId(), accommodation.getAccommodationId());
 					 
 					if(listAccommodationRoom.size()>0 && !listAccommodationRoom.isEmpty()){
 						
 						for(AccommodationRoom roomsList:listAccommodationRoom){
 							roomCount=(int) roomsList.getRoomCount();
 							if(roomCount>0){
 								long minimum = getAvailableCount(property.getPropertyId(),getArrivalDate(),getDepartureDate(),roomCount,accommodation.getAccommodationId());
 								if((int)minimum>0){
 									blnAvailable=true;
 									blnSoldout=false;
 									arlListTotalAccomm++;
 								}else if((int)minimum==0){
 									blnSoldout=true;
 									blnAvailable=false;
 									soldOutTotalAccomm++;
 								}
 							}
 							 break;
 						 }
 					 }
 					 Timestamp tsFirstStartRange=null,tsSecondStartRange=null,tsFirstEndRange=null,tsSecondEndRange=null;
					 Timestamp tsLastStartDate=null,tsLastEndDate=null;
 					 Timestamp tsPromoBookedDate=null,tsPromoStartDate=null,tsPromoEndDate=null;
 					 boolean blnLastMinutePromo=false,blnFirstActivePromo=false,blnFirstNotActivePromo=false,baseFirstPromoFlag=false,blnEarlyBirdPromo=false;
 					 boolean blnSecondActivePromo=false,blnSecondNotActivePromo=false,baseSecondPromoFlag=false;
 					 SimpleDateFormat format1=new SimpleDateFormat("yyyy-MM-dd");
 					 java.util.Date currentdate=new java.util.Date();
 					 String strCurrentDate = new SimpleDateFormat("yyyy-MM-dd").format(currentdate);
 					 java.util.Date dtecurdate = format1.parse(strCurrentDate);
				   	 Calendar calStart=Calendar.getInstance();
				   	 calStart.setTime(dtecurdate);
				   	 calStart.setTimeZone(TimeZone.getTimeZone("IST"));
				   	 String StartDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calStart.getTime());
				   	 java.util.Date curdate = format1.parse(StartDate);
				   	 
 				   	 this.displayHoursForTimer=null;
 				   	 setDisplayHoursForTimer(this.displayHoursForTimer);
 					 List<PmsPromotions> listDatePromotions= promotionController.listBookedDatePromotions(property.getPropertyId(),curdate);
					 if(listDatePromotions.size()>0 && !listDatePromotions.isEmpty()){
						for(PmsPromotions promotions:listDatePromotions){
							tsPromoBookedDate=promotions.getPromotionBookedDate();
							tsPromoStartDate=promotions.getStartDate();
							tsPromoEndDate=promotions.getEndDate();
							blnEarlyBirdPromo=checkEarlyBirdPromos(tsPromoBookedDate,tsPromoStartDate,tsPromoEndDate,getArrivalDate(),getDepartureDate());
						}
					 }else{
						 blnEarlyBirdPromo=false;
					 }
					 
					List<PmsPromotions> listLastPromotions= promotionController.listLastBookedPromotions(property.getPropertyId(),curdate);
					 if(listLastPromotions.size()>0 && !listLastPromotions.isEmpty()){
						for(PmsPromotions promotions:listLastPromotions){
							tsFirstStartRange=promotions.getFirstRangeStartDate();
							tsSecondStartRange=promotions.getSecondRangeStartDate();
							tsFirstEndRange=promotions.getFirstRangeEndDate();
							tsSecondEndRange=promotions.getSecondRangeEndDate();
							tsLastStartDate=promotions.getStartDate();
							tsLastEndDate=promotions.getEndDate();
							promotionHoursRange=promotions.getPromotionHoursRange();
							blnLastMinuteFirstActive=checkLastMinutePromos(promotionHoursRange,getArrivalDate(),getDepartureDate(),tsSecondStartRange,tsFirstEndRange,tsFirstEndRange,tsSecondStartRange);
//							blnLastMinuteIsActive=getCheckHoursRange(promotionHoursRange,getArrivalDate(),getDepartureDate());
							if(blnLastMinuteFirstActive){
								blnLastMinutePromo=true;
								enablePromoId=promotions.getPromotionId();
								arlEnablePromo.contains(enablePromoId);
							}
						}
					 }else{
						 blnLastMinutePromo=false;
						 this.displayHoursForTimer=null;
				    	 setDisplayHoursForTimer(this.displayHoursForTimer);
					 }
 					 ArrayList<Integer> arlFirstActivePromo=new ArrayList<Integer>(); 
 					 ArrayList<Integer> arlSecondActivePromo=new ArrayList<Integer>(); 
 					 List<DateTime> between = DateUtil.getDateRange(start, end);
 					 for (DateTime d : between)
 				     {
 						 if(blnEarlyBirdPromo){
 							 List<PmsPromotions> listEarlyPromotions= promotionController.listBookedDatePromotions(property.getPropertyId(),curdate);
 							 if(listEarlyPromotions.size()>0 && !listEarlyPromotions.isEmpty()){
 								 for(PmsPromotions promos:listEarlyPromotions){
 									 if(promos.getPromotionType().equalsIgnoreCase("E")){
 	 									 if(arlFirstActivePromo.isEmpty()){
 	 										arlFirstActivePromo.add(promos.getPromotionId());
 	 									 }else if(!arlFirstActivePromo.contains(promos.getPromotionId())){
 	 										arlFirstActivePromo.add(promos.getPromotionId());
 	 									 }
 	 								 }else{
 	 									blnFirstNotActivePromo=true;
 	 								 }
 								 }
 							 }
 						 }else if(!blnEarlyBirdPromo){
 							 List<PmsPromotions> listAllPromotions= promotionController.listFlatDatePromotions(property.getPropertyId(),d.toDate());
 							 if(!listAllPromotions.isEmpty() && listAllPromotions.size()>0){
 								 for(PmsPromotions promos:listAllPromotions){
 									 if(promos.getPromotionType().equalsIgnoreCase("F")){
 	 									 if(arlFirstActivePromo.isEmpty()){
 	 										arlFirstActivePromo.add(promos.getPromotionId());
 	 									 }else if(!arlFirstActivePromo.contains(promos.getPromotionId())){
 	 										arlFirstActivePromo.add(promos.getPromotionId());
 	 									 }
 	 								 }else{
 	 									blnFirstNotActivePromo=true;
 	 								 }
 								 }
 							 } 
 						 }else{
 							 blnFirstNotActivePromo=true;
 						 }
 						 
 						if(blnLastMinutePromo){
 							List<PmsPromotions> listLastPromos= promotionController.listLastBookedPromotions(property.getPropertyId(),curdate);
 							 if(listLastPromos.size()>0 && !listLastPromos.isEmpty()){
 								for(PmsPromotions promos:listLastPromos){
 									if(promos.getPromotionType().equalsIgnoreCase("L")){
 										promoAccommId=promos.getPropertyAccommodation().getAccommodationId();
 										if(arlEnablePromo.contains(promos.getPromotionId())){
											if(arlSecondActivePromo.isEmpty()){
 		  										arlSecondActivePromo.add(promos.getPromotionId());
 		  									 }else if(!arlSecondActivePromo.contains(promos.getPromotionId())){
 		  										arlSecondActivePromo.add(promos.getPromotionId());
 		  									 }
										}
 									 }
 								}
 							 }else{
 								blnSecondNotActivePromo=true;
 							 }
						}else{
							blnSecondNotActivePromo=true;
						}
 				     }
 					 int intPromoSize=arlFirstActivePromo.size();
 					 if((arlFirstActivePromo.isEmpty() || blnFirstNotActivePromo)){
 						 blnFirstActivePromo=false;
 					 }else{
 						 blnFirstActivePromo=true;
 					 }
 					 
 					 if(arlSecondActivePromo.isEmpty() || blnSecondNotActivePromo){
 						blnSecondActivePromo=false;
 					 }else{
 						blnSecondActivePromo=true;
 					 }
 					 
 					
 				     for (DateTime d : between)
 				     {
 				    	 Integer promotionId=0;
 				    	 if(blnFirstActivePromo){
 				    		 if(blnEarlyBirdPromo){
 				    			List<PmsPromotions> listEarlyPromotions= promotionController.listBookedDatePromotions(property.getPropertyId(),curdate);
 	  							 if(listEarlyPromotions.size()>0 && !listEarlyPromotions.isEmpty()){
  	  				    			for(PmsPromotions promotions:listEarlyPromotions){
  	  				    				if(promotions.getPromotionType().equalsIgnoreCase("E")){
  	 					    				promotionId=promotions.getPromotionId();
  	 					    				promotionFirstDiscountType=promotions.getPromotionDiscountType();
  	 					    				listPromotionDetailCheck=promotionDetailController.listPromotionDetails(promotions.getPromotionId(),f.format(d.toDate()).toLowerCase());
  	 					    				 for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
  	 					    					if(promoCount==0){
  	 					    						promotionFirstPercent=promoDetails.getPromotionPercentage();
  	 	 					    					promotionFirstInr=promoDetails.getPromotionInr();
  	 	 					    					promotionType=promotions.getPromotionType();
  	 	 					    					if(promotionType.equalsIgnoreCase("E")){
  	 	 					    						strCheckPromotionType="E";
  	 	 					    					}
  	 	 					    					
  	 		  						    			arlListPromoType.add(promotionType);
  	 					    					}
  	 					    					promoCount++;
  	 					    				 }
  	  				    				}
  	  				    			}
  	  				    			promotionFlag=true; 
  			    					blnPromoFlag=true;
  	  				    		}else{
  	  				    			promotionFlag=false;
  	  	  		    				promotionType=null;
  	  	  		    				blnPromoFlag=false;
  	  	  		    				promotionId=null;
  	  	  		    				baseFirstPromoFlag=true;
  	  				    		}
  				    		 
 				    		 }else if(!blnEarlyBirdPromo){

 	  				    		List<PmsPromotions> listPromotions= promotionController.listFlatDatePromotions(property.getPropertyId(),d.toDate());
 	  				    		if(listPromotions.size()>0 && !listPromotions.isEmpty()){
 	  				    			for(PmsPromotions promotions:listPromotions){
 	  				    				if(promotions.getPromotionType().equalsIgnoreCase("F")){
 	 					    				promotionId=promotions.getPromotionId();
 	 					    				promotionFirstDiscountType=promotions.getPromotionDiscountType();
 	 					    				listPromotionDetailCheck=promotionDetailController.listPromotionDetails(promotions.getPromotionId(),f.format(d.toDate()).toLowerCase());
 	 					    				 for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
 	 					    					if(promoCount==0){
 	 					    						promotionFirstPercent=promoDetails.getPromotionPercentage();
 	 	 					    					promotionFirstInr=promoDetails.getPromotionInr();
 	 	 					    					promotionType=promotions.getPromotionType();
 	 	 					    					if(promotionType.equalsIgnoreCase("F")){
 	 	 					    						strCheckPromotionType="F";
 	 	 					    					}
 	 	 					    					
 	 		  						    			arlListPromoType.add(promotionType);
 	 					    					}
 	 					    					promoCount++;
 	 					    				 }
 	  				    				}
 	  				    			}
 	  				    			promotionFlag=true; 
 			    					blnPromoFlag=true;
 	  				    		}
 				    		 }
 				    	 }
 				    	 if(blnSecondActivePromo){
 				    		List<PmsPromotions> listPmsPromotions= promotionController.listLastDatePromotions(property.getPropertyId(),accommodation.getAccommodationId(),curdate);
				    			 if(listPmsPromotions.size()>0 &&!listPmsPromotions.isEmpty()){
				    				 for(PmsPromotions pmsPromotions:listPmsPromotions){
				    					 if(pmsPromotions.getPromotionType().equalsIgnoreCase("L")){
				    						 promotionId=pmsPromotions.getPromotionId();
							    			 promoAccommId=pmsPromotions.getPropertyAccommodation().getAccommodationId();
							    			 Timestamp tsStartDate=null,tsEndDate=null;
							    			 tsStartDate=pmsPromotions.getStartDate();
							    			 tsEndDate=pmsPromotions.getEndDate();
							    			 
							    			 promotionHoursRange=pmsPromotions.getPromotionHoursRange();
							    			 if(arlEnablePromo.contains(pmsPromotions.getPromotionId())){
							    				 promotionAccommId=promoAccommId.intValue();
							    				 long minimumCount = getAvailableCount(property.getPropertyId(),getArrivalDate(),getDepartureDate(),roomCount,accommodation.getAccommodationId());
				    			    			 if((int)minimumCount>0){
				    			    				 promotionFlag=true; 
			    			    					 blnPromoFlag=true;
			    			    					 strCheckPromotionType="L";
			    			    					 promotionType=pmsPromotions.getPromotionType();
   	 		  						    		 arlListPromoType.add(promotionType);
			    			    					 promotionSecondDiscountType=pmsPromotions.getPromotionDiscountType();
   	 					    					 listPromotionDetailCheck=promotionDetailController.listPromotionDetails(pmsPromotions.getPromotionId(),f.format(curdate).toLowerCase());
   	 					    					 if(listPromotionDetailCheck.size()>0 && !listPromotionDetailCheck.isEmpty()){
   	 					    						for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
		    	 					    					promotionSecondPercent=promoDetails.getPromotionPercentage();
	    	 	 					    					promotionSecondInr=promoDetails.getPromotionInr();
		    	 					    				 }
   	 					    					 }
				    			    			 }else{
				    			    				 promotionFlag=false;
				    			    				 promotionType=null;
				    			    				 blnPromoFlag=false;
				    			    				 baseSecondPromoFlag=true;
				    			    			 }
							    			 }
				    					 }
				    					 
				    				 }
				    			 }
 				    	 }
 				    	 if(promotionFlag){
 				    		int rateCount=0,rateIdCount=0;
   						List<PropertyRate> propertyRateList =  rateController.listAllDates(property.getPropertyId(),accommodation.getAccommodationId(),getSourceTypeId(),d.toDate());
   				    	if(propertyRateList.size()>0)
   				    	{
   				    		 for (PropertyRate pr : propertyRateList)
   			    			 {
				    				 int propertyRateId = pr.getPropertyRateId();
					    			 List<PropertyRateDetail> rateDetailList =   detailController.list(propertyRateId,f.format(d.toDate()).toLowerCase());
       				    		 if(rateDetailList.size()>0){
       				    			 if(rateCount<1){
       				    				 for (PropertyRateDetail rate : rateDetailList){
       				    					 amount+=  rate.getBaseAmount();
	        				    			 }
       				    				 rateCount++;
       				    			 }
       				    		 }else{
       				    			 if(propertyRateList.size()==rateIdCount){
       				    				 amount+= accommodation.getBaseAmount();
       				    			 }
           				    	 }
   			    			 }	    		 
   				    	 }
   				    	 else{
   				    		 amount+= accommodation.getBaseAmount();
   				    	 }
   				    	double discountAmount=0;
 				    		 if(blnFirstActivePromo){
  	    				    	String strTypePercent=String.valueOf(promotionFirstPercent);
  	    				    	String strTypeInr=String.valueOf(promotionFirstInr);
					    		  if(!strTypePercent.equalsIgnoreCase("null") && promotionFirstPercent!=0){
					    			  flatLastAmount = amount-(amount*promotionFirstPercent/100);
					    			  discountAmount+=amount*promotionFirstPercent/100;
					    		  }
					    		  else if(!strTypeInr.equalsIgnoreCase("null") && promotionFirstInr!=0){
					    			  flatLastAmount = amount-promotionFirstInr;
					    			  discountAmount+=promotionFirstInr;
					    		  }
					    		  else{
					    			  flatLastAmount = amount;
					    		  } 
 				    		 }else{
 				    			flatLastAmount = amount;
 				    		 }
 				    		 if(blnSecondActivePromo){
 				    			String strTypePercent=String.valueOf(promotionSecondPercent);
  	    				    	String strTypeInr=String.valueOf(promotionSecondInr);
					    		  if(!strTypePercent.equalsIgnoreCase("null") && promotionSecondPercent!=0){
					    			  flatLastAmount = flatLastAmount-(flatLastAmount*promotionSecondPercent/100);
					    			  discountAmount+=amount*promotionSecondPercent/100;
					    		  }
					    		  else if(!strTypeInr.equalsIgnoreCase("null") && promotionSecondInr!=0){
					    			  flatLastAmount = flatLastAmount-promotionSecondInr;
					    			  discountAmount+=promotionSecondInr;
					    		  }
					    		  else{
					    			  flatLastAmount = flatLastAmount;
					    		  }
 				    		 }else{
 				    			flatLastAmount = flatLastAmount;
 				    		 }
 				    		totalFlatLastAmount=amount-discountAmount;
 				    		 
 				    	 }else{
 				    		 if(baseFirstPromoFlag){
 				    			amount += accommodation.getBaseAmount();
 				    		}else{
 				    			int rateCount=0,rateIdCount=0;
 	    						List<PropertyRate> propertyRateList =  rateController.listAllDates(property.getPropertyId(),accommodation.getAccommodationId(),getSourceTypeId(),d.toDate());
 	    				    	if(propertyRateList.size()>0)
 	    				    	{
 	    				    		 for (PropertyRate pr : propertyRateList)
 	    			    			 {
 					    				 int propertyRateId = pr.getPropertyRateId();
						    			 List<PropertyRateDetail> rateDetailList =   detailController.list(propertyRateId,f.format(d.toDate()).toLowerCase());
	        				    		 if(rateDetailList.size()>0){
	        				    			 if(rateCount<1){
	        				    				 for (PropertyRateDetail rate : rateDetailList){
	        				    					 amount +=  rate.getBaseAmount();
		        				    			 }
	        				    				 rateCount++;
	        				    			 }
	        				    		 }else{
	        				    			 if(propertyRateList.size()==rateIdCount){
	        				    				 amount += accommodation.getBaseAmount();
	        				    			 }
	            				    	 }
 	    			    			 }	    		 
 	    				    	 }
 	    				    	 else{
 	    				    		 amount += accommodation.getBaseAmount();
 	    				    	 }
 				    		}
 				    	 }
 				    	 dateCount++;
 				    	 if(dateCount==diffInDays){
 				    		 if(blnAvailable){
 				    			if(promotionType!=null && strCheckPromotionType!=null) {
 				    				if(strCheckPromotionType.equalsIgnoreCase("L") || strCheckPromotionType.equalsIgnoreCase("F") || strCheckPromotionType.equalsIgnoreCase("E")){
			    						mapAmountAccommId.put(totalFlatLastAmount,accommodation.getAccommodationId());
 						      			arllist.add(totalFlatLastAmount);
 					    			}
 					    			
 				    			}else{
 				    				mapAmountAccommId.put(amount,accommodation.getAccommodationId());
 					      			arllist.add(amount);
 				    			}
 		      				 }else if(blnSoldout){
 		      					if(promotionType!=null && strCheckPromotionType!=null) {
	  		      					if(strCheckPromotionType.equalsIgnoreCase("L") || strCheckPromotionType.equalsIgnoreCase("F") || strCheckPromotionType.equalsIgnoreCase("E")){
	  		      						mapSoldOutAmountAccommId.put(totalFlatLastAmount,accommodation.getAccommodationId());
						      			arllist.add(totalFlatLastAmount);
					    			}
 		      					}else{
 				    				mapSoldOutAmountAccommId.put(amount,accommodation.getAccommodationId());
 					      			arllist.add(amount);
 				    			}
 					    	 }
 				    	 }
 				     }
 				     if(blnAvailable){
 				    	 if(promotionType!=null && strCheckPromotionType!=null) {
 				    		if(strCheckPromotionType.equalsIgnoreCase("L") || strCheckPromotionType.equalsIgnoreCase("F") || strCheckPromotionType.equalsIgnoreCase("E")){
 				    			arllistAvl.add(totalFlatLastAmount);
			    			} 
 				    	 }else{
 		    				arllistAvl.add(amount);
 		    			}
 				     }else if(!blnAvailable){
 				    	 if(promotionType!=null && strCheckPromotionType!=null) {
 				    		if(strCheckPromotionType.equalsIgnoreCase("L") || strCheckPromotionType.equalsIgnoreCase("F") || strCheckPromotionType.equalsIgnoreCase("E")){
 					    		 arllistSold.add(totalFlatLastAmount);
 			    			}
 				    	 }else{
 		    				arllistSold.add(amount);
 		    			}
 				     }
 				     accommCount++;
 				}
 			}
 			//end of accommodation
 			double totalAmount=0;
 			boolean blnJSONContent=false;
 			Collections.sort(arllist);
 			int arlListSize=0,accommId=0,intTotalAccomm=0;
 		     intTotalAccomm=accommodationList.size();
 		     if(intTotalAccomm==accommCount){
 		    	 if(intTotalAccomm==soldOutCount){
 			    	 if(blnPromoFlag){
 				    	 if(arlListPromoType.contains("F") || arlListPromoType.contains("E")){
 				    		 arlListSize=arllist.size();
 						     if(soldOutCount==arlListSize){
 						    	 Collections.sort(arllist);
 						    	 for(int i=0;i<arlListSize;i++){
 							    	 totalAmount=(Double) arllist.get(i);
 							    	 if((Double) arllist.get(0)==(Double) arllist.get(i)){
 							    		 if(blnAvailable){
 							    			 accommId=mapAmountAccommId.get(totalAmount);
 								    		 blnJSONContent=true;
 							    		 }else{
 							    			 accommId=mapSoldOutAmountAccommId.get(totalAmount);
 								    		 blnJSONContent=true;
 							    		 }
 							    		 
 							    		 break;
 							    	 }else{
 							    		 
 							    	 }
 							     }
 						     }
 				    	 }else if(arlListPromoType.contains("L")){
 				    		 /*if(promotionAccommId>0){
 					    		 accommId=promotionAccommId;
 					    		 blnJSONContent=true;
 					    	 } */
 				    		arlListSize=arllist.size();
	 					     Collections.sort(arllist);
	 				    	 for(int i=0;i<arlListSize;i++){
	 				    		 if(arllistAvl.contains((Double) arllist.get(i))){
	 				    			 if((Double) arllist.get(0)==(Double) arllist.get(i)){
	 					    			 totalAmount=(Double) arllist.get(i);
	 					    			 accommId=mapAmountAccommId.get(totalAmount);
	 						    		 blnJSONContent=true;
	 				    			 }else{
	 				    				 totalAmount=(Double) arllist.get(i);
	 					    			 accommId=mapAmountAccommId.get(totalAmount);
	 						    		 blnJSONContent=true;
	 				    			 }
	 				    			 break;
	 				    		 }
	 					     }
 				    	 }
 				     }else{
 					     arlListSize=arllist.size();
 					     if(soldOutCount==arlListSize){
 					    	 Collections.sort(arllist);
 					    	 for(int i=0;i<arlListSize;i++){
 						    	 totalAmount=(Double) arllist.get(i);
 						    	 if((Double) arllist.get(0)==(Double) arllist.get(i)){
 						    		 if(blnAvailable){
 						    			 accommId=mapAmountAccommId.get(totalAmount);
 							    		 blnJSONContent=true;
 						    		 }else{
 						    			 accommId=mapSoldOutAmountAccommId.get(totalAmount);
 							    		 blnJSONContent=true;
 						    		 }
 						    		 break;
 						    	 }else{
 						    		 
 						    	 }
 						     }
 					     }
 					     
 				     }
 			     }else{
 			    	 if(arlListPromoType.size()>0){
 			    		 if(arlListPromoType.contains("F") || arlListPromoType.contains("E")){
// 				    	 if(promotionType.equalsIgnoreCase("F") || promotionType.equalsIgnoreCase("L")){
 				    		 arlListSize=arllist.size();
 				    		 Collections.sort(arllist);
 					    	 for(int i=0;i<arlListSize;i++){
 					    		 if(arllistAvl.contains((Double) arllist.get(i))){
 					    			 if((Double) arllist.get(0)==(Double) arllist.get(i)){
 						    			 totalAmount=(Double) arllist.get(i);
 						    			 accommId=mapAmountAccommId.get(totalAmount);
 							    		 blnJSONContent=true;
 					    			 }else{
 					    				 totalAmount=(Double) arllist.get(i);
 						    			 accommId=mapAmountAccommId.get(totalAmount);
 							    		 blnJSONContent=true;
 					    			 }
 					    			 break;
 					    		 }
 						     }
 				    	 }else if( arlListPromoType.contains("L")){
 				    		 /*if(promotionAccommId>0){
 					    		 accommId=promotionAccommId;
 					    		 blnJSONContent=true;
 					    	 } */
 				    		arlListSize=arllist.size();
	 					     Collections.sort(arllist);
	 				    	 for(int i=0;i<arlListSize;i++){
	 				    		 if(arllistAvl.contains((Double) arllist.get(i))){
	 				    			 if((Double) arllist.get(0)==(Double) arllist.get(i)){
	 					    			 totalAmount=(Double) arllist.get(i);
	 					    			 accommId=mapAmountAccommId.get(totalAmount);
	 						    		 blnJSONContent=true;
	 				    			 }else{
	 				    				 totalAmount=(Double) arllist.get(i);
	 					    			 accommId=mapAmountAccommId.get(totalAmount);
	 						    		 blnJSONContent=true;
	 				    			 }
	 				    			 break;
	 				    		 }
	 					     }
 				    	 }
 				     }else{
 					     arlListSize=arllist.size();
 					     Collections.sort(arllist);
 				    	 for(int i=0;i<arlListSize;i++){
 				    		 if(arllistAvl.contains((Double) arllist.get(i))){
 				    			 if((Double) arllist.get(0)==(Double) arllist.get(i)){
 					    			 totalAmount=(Double) arllist.get(i);
 					    			 accommId=mapAmountAccommId.get(totalAmount);
 						    		 blnJSONContent=true;
 				    			 }else{
 				    				 totalAmount=(Double) arllist.get(i);
 					    			 accommId=mapAmountAccommId.get(totalAmount);
 						    		 blnJSONContent=true;
 				    			 }
 				    			 break;
 				    		 }
 					     }
 				     }
 			     }
 		     }
 		     arlListPromoType.clear();  
 			if(blnJSONContent){
 				int promoCount=0,enablePromoId=0;
 			    listAccommodationRoom.clear(); 
 			    ArrayList<Integer> arlEnablePromo=new ArrayList<Integer>();
 			    ArrayList<String> arlPromoType=new ArrayList<String>();
 		    	listAccommodationRoom=accommodationController.listPropertyTotalRooms(property.getPropertyId(), accommId);
 		    	List<DateTime> betweenAmount = DateUtil.getDateRange(start, end);
 		    	double actualPromoBaseAmount=0.0,actualBaseAmount=0.0,actualTotalAmount=0.0,actualTotalAdultAmount=0.0,actualTotalChildAmount=0.0,
 		    			actualAdultAmount=0.0,actualChildAmount=0.0,promotionFirstPercent=0.0,promotionFirstInr=0.0,
 	  				    		promotionSecondPercent=0.0,promotionSecondInr=0.0;
 			    int countDate=0,promotionId=0,promotionHours=0,promoAccommId=0; 
 			    boolean blnStatus=false,isPositive=false,isNegative=false,
 			    		promoFlag=false,blnBookedGetRooms=false,
 			    		blnLastMinutePromotions=false,blnLastMinuteIsActive=false,blnLastMinuteFirstActive=false;
 			    String promotionFirstName=null,promotionSecondName=null,promotionHoursRange=null,
 			    		promotionSecondDiscountType=null,promotionFirstDiscountType=null,strCheckPromotionType=null;
 			    Integer promoFirstPercent=null,promoFirstInr=null,promoSecondInr=null,promoSecondPercent=null;
 			    double promoBaseAmount=0.0,promoAdultAmount=0.0,promoChildAmount=0.0,smartPricePercent=0.0;
 			    double baseAmount=0.0,extraAdultAmount=0.0,extraChildAmount=0.0,totalBaseAmount=0.0,totalSmartPriceAmount=0.0,totalFlatAmount=0.0,totalLastAmount=0.0;
 			    int roomCount=0,availableCount=0,totalCount=0,discountId=0;
 			    double dblPercentCount=0.0,dblSmartPricePercent=0.0, flatAmount=0.0,lastAmount=0.0;
 			    double dblPercentFrom=0.0,dblPercentTo=0.0,lastMinuteHours=0.0;
 			    Timestamp lastMinStartDate=null,lastMinEndDate=null;
 				if(listAccommodationRoom.size()>0 && !listAccommodationRoom.isEmpty()){
 					for(AccommodationRoom roomsList:listAccommodationRoom){
 						 roomCount=(int) roomsList.getRoomCount();
 						 break;
 					 }
 				 }
 				 Timestamp tsFirstStartRange=null,tsSecondStartRange=null,tsFirstEndRange=null,tsSecondEndRange=null;
				 Timestamp tsLastStartDate=null,tsLastEndDate=null;
 				 Timestamp tsPromoBookedDate=null,tsPromoStartDate=null,tsPromoEndDate=null;
 				 boolean blnLastMinutePromo=false,blnEarlyBirdPromo=false,blnFirstActivePromo=false,blnFirstNotActivePromo=false,basePromoFlag=false;
				 boolean blnSecondActivePromo=false,blnSecondNotActivePromo=false;
				 SimpleDateFormat format1=new SimpleDateFormat("yyyy-MM-dd");
				 java.util.Date currentdate=new java.util.Date();
				 String strCurrentDate = new SimpleDateFormat("yyyy-MM-dd").format(currentdate);
				 java.util.Date dtecurdate = format1.parse(strCurrentDate);
			   	 Calendar calStart=Calendar.getInstance();
			   	 calStart.setTime(dtecurdate);
			   	 calStart.setTimeZone(TimeZone.getTimeZone("IST"));
			   	 String StartDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calStart.getTime());
			   	 java.util.Date curdate = format1.parse(StartDate);
			   	 
				 this.displayHoursForTimer=null;
			   	 setDisplayHoursForTimer(this.displayHoursForTimer);
				 List<PmsPromotions> listDatePromotions= promotionController.listBookedDatePromotions(property.getPropertyId(),curdate);
				 if(listDatePromotions.size()>0 && !listDatePromotions.isEmpty()){
					for(PmsPromotions promotions:listDatePromotions){
						tsPromoBookedDate=promotions.getPromotionBookedDate();
						tsPromoStartDate=promotions.getStartDate();
						tsPromoEndDate=promotions.getEndDate();
						blnEarlyBirdPromo=checkEarlyBirdPromos(tsPromoBookedDate,tsPromoStartDate,tsPromoEndDate,getArrivalDate(),getDepartureDate());
					}
				 }else{
					 blnEarlyBirdPromo=false;
				 }
				 
				 List<PmsPromotions> listLastPromotions= promotionController.listLastBookedPromotions(property.getPropertyId(),curdate);
				 if(listLastPromotions.size()>0 && !listLastPromotions.isEmpty()){
					for(PmsPromotions promotions:listLastPromotions){
						tsFirstStartRange=promotions.getFirstRangeStartDate();
						tsSecondStartRange=promotions.getSecondRangeStartDate();
						tsFirstEndRange=promotions.getFirstRangeEndDate();
						tsSecondEndRange=promotions.getSecondRangeEndDate();
						tsLastStartDate=promotions.getStartDate();
						tsLastEndDate=promotions.getEndDate();
						promotionHoursRange=promotions.getPromotionHoursRange();
						blnLastMinuteFirstActive=checkLastMinutePromos(promotionHoursRange,getArrivalDate(),getDepartureDate(),tsSecondStartRange,tsFirstEndRange,tsFirstEndRange,tsSecondStartRange);
//						blnLastMinuteIsActive=getCheckHoursRange(promotionHoursRange,getArrivalDate(),getDepartureDate());
						if(blnLastMinuteFirstActive){
							blnLastMinutePromo=true;
							enablePromoId=promotions.getPromotionId();
							arlEnablePromo.add(enablePromoId);
						}
					}
				 }else{
					 blnLastMinutePromo=false;
					 this.displayHoursForTimer=null;
			    	 setDisplayHoursForTimer(this.displayHoursForTimer);
				 }
				 ArrayList<Integer> arlFirstActivePromo=new ArrayList<Integer>(); 
				 ArrayList<Integer> arlSecondActivePromo=new ArrayList<Integer>(); 
				 List<DateTime> between = DateUtil.getDateRange(start, end);
				 for (DateTime d : between)
			     {
					 if(blnEarlyBirdPromo){
						 List<PmsPromotions> listEarlyPromotions= promotionController.listBookedDatePromotions(property.getPropertyId(),curdate);
						 if(listEarlyPromotions.size()>0 && !listEarlyPromotions.isEmpty()){
							 for(PmsPromotions promos:listEarlyPromotions){
								 if(promos.getPromotionType().equalsIgnoreCase("E")){
									 if(arlFirstActivePromo.isEmpty()){
										arlFirstActivePromo.add(promos.getPromotionId());
									 }else if(!arlFirstActivePromo.contains(promos.getPromotionId())){
										arlFirstActivePromo.add(promos.getPromotionId());
									 }
								 }else{
									blnFirstNotActivePromo=true;
								 }
							 }
						 }
					 }else if(!blnEarlyBirdPromo){
						 List<PmsPromotions> listAllPromotions= promotionController.listFlatDatePromotions(property.getPropertyId(),d.toDate());
						 if(!listAllPromotions.isEmpty() && listAllPromotions.size()>0){
							 for(PmsPromotions promos:listAllPromotions){
								 if(promos.getPromotionType().equalsIgnoreCase("F")){
									 if(arlFirstActivePromo.isEmpty()){
										arlFirstActivePromo.add(promos.getPromotionId());
									 }else if(!arlFirstActivePromo.contains(promos.getPromotionId())){
										arlFirstActivePromo.add(promos.getPromotionId());
									 }
								 }else{
									blnFirstNotActivePromo=true;
								 }
							 }
						 } 
					 }else{
						 blnFirstNotActivePromo=true;
					 }
					 
					 if(blnLastMinutePromo){
							List<PmsPromotions> listLastPromos= promotionController.listLastBookedPromotions(property.getPropertyId(),curdate);
							 if(listLastPromos.size()>0 && !listLastPromos.isEmpty()){
								for(PmsPromotions promos:listLastPromos){
									if(promos.getPromotionType().equalsIgnoreCase("L")){
										promoAccommId=promos.getPropertyAccommodation().getAccommodationId();
										if(arlEnablePromo.contains(promos.getPromotionId())){
											if(arlSecondActivePromo.isEmpty()){
		  										arlSecondActivePromo.add(promos.getPromotionId());
		  									 }else if(!arlSecondActivePromo.contains(promos.getPromotionId())){
		  										arlSecondActivePromo.add(promos.getPromotionId());
		  									 }	
										}
									 }
								}
							 }else{
								blnSecondNotActivePromo=true;
							 }
						}else{
							blnSecondNotActivePromo=true;
						}
			     }
				 int intPromoSize=arlFirstActivePromo.size();
				 if((arlFirstActivePromo.isEmpty() || blnFirstNotActivePromo)){
					 blnFirstActivePromo=false;
				 }else{
					 blnFirstActivePromo=true;
				 }
				 
				 if(arlSecondActivePromo.isEmpty() || blnSecondNotActivePromo){
					blnSecondActivePromo=false;
				 }else{
					 blnSecondActivePromo=true;
				 }
 				
 				List<PropertyAccommodation> listAccommodation =  accommodationManager.list(property.getPropertyId(),accommId);
 				
 				for (PropertyAccommodation accommodations : listAccommodation) {
 					for (DateTime d : betweenAmount)
 				    {
 						boolean blnFlatPromotions=false;
 						if(blnFirstActivePromo){
 							if(blnEarlyBirdPromo){
 								List<PmsPromotions> listEarlyPromotions= promotionController.listBookedDatePromotions(property.getPropertyId(),curdate);
 								if(listEarlyPromotions.size()>0 && !listEarlyPromotions.isEmpty()){
 									for(PmsPromotions promotions:listEarlyPromotions){
	  					    			 if(promotions.getPromotionType().equalsIgnoreCase("E")){
	  					    				 promotionId=promotions.getPromotionId();
	  					    				 promotionType=promotions.getPromotionType();
	  					    				 promotionFirstDiscountType=promotions.getPromotionDiscountType();
	  					    				 listPromotionDetailCheck=promotionDetailController.listPromotionDetails(promotions.getPromotionId(),f.format(d.toDate()).toLowerCase());
	  					    				 for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
	  				    						if(promoCount==0){
	  				    							promotionFirstPercent=promoDetails.getPromotionPercentage();
	 	 					    					promotionFirstInr=promoDetails.getPromotionInr();
	 	 					    					promotionType=promotions.getPromotionType();
	  	  				    						 promotionFlag=true;
	  	  						    				 blnPromoFlag=true;
	  	  						    				 arlPromoType.add(promotionType);
	  				    						}
	  				    						promoCount++;
	  				    					 }
	  					    			 }
	  					    		}
 								}
 							}else if(!blnEarlyBirdPromo){
 								List<PmsPromotions> listFlatPromotions= promotionController.listFlatDatePromotions(property.getPropertyId(),d.toDate());
 	  					    	if(listFlatPromotions.size()>0 && !listFlatPromotions.isEmpty()){
 	  					    		for(PmsPromotions promotions:listFlatPromotions){
 	  					    			 if(promotions.getPromotionType().equalsIgnoreCase("F")){
 	  					    				 promotionId=promotions.getPromotionId();
 	  					    				 promotionType=promotions.getPromotionType();
 	  					    				 promotionFirstDiscountType=promotions.getPromotionDiscountType();
 	  					    				 listPromotionDetailCheck=promotionDetailController.listPromotionDetails(promotions.getPromotionId(),f.format(d.toDate()).toLowerCase());
 	  					    				 for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
 	  				    						if(promoCount==0){
 	  				    							promotionFirstPercent=promoDetails.getPromotionPercentage();
 	 	 					    					promotionFirstInr=promoDetails.getPromotionInr();
 	 	 					    					promotionType=promotions.getPromotionType();
 	  	  				    						 promotionFlag=true;
 	  	  						    				 blnPromoFlag=true;
 	  	  						    				 arlPromoType.add(promotionType);
 	  				    						}
 	  				    						promoCount++;
 	  				    					 }
 	  					    			 }
 	  					    		}
 	  					    	}
 							}
 					    	
 						}
 						if(blnSecondActivePromo){

				    		List<PmsPromotions> listPmsPromotions= promotionController.listLastDatePromotions(property.getPropertyId(),accommId,curdate);
				    			 if(listPmsPromotions.size()>0 &&!listPmsPromotions.isEmpty()){
				    				 for(PmsPromotions pmsPromotions:listPmsPromotions){
				    					 promotionId=pmsPromotions.getPromotionId();
						    			 promoAccommId=pmsPromotions.getPropertyAccommodation().getAccommodationId();
						    			 promotionHoursRange=pmsPromotions.getPromotionHoursRange();
						    			 Timestamp tsStartDate=null,tsEndDate=null;
						    			 tsStartDate=pmsPromotions.getStartDate();
						    			 tsEndDate=pmsPromotions.getEndDate();
						    			 
						    			 if(arlEnablePromo.contains(pmsPromotions.getPromotionId())){
						    				 promotionAccommId=promoAccommId;
						    				 long minimumCount = getAvailableCount(property.getPropertyId(),getArrivalDate(),getDepartureDate(),roomCount,accommId);
			    			    			 if((int)minimumCount>0){
		    			    					 promotionFlag=true; 
		    			    					 blnPromoFlag=true;
		    			    					 strCheckPromotionType="L";
		    			    					 promotionType=pmsPromotions.getPromotionType();
		    			    					 arlPromoType.add(promotionType);
		    			    					 promotionSecondDiscountType=pmsPromotions.getPromotionDiscountType();
	 					    					 listPromotionDetailCheck=promotionDetailController.listPromotionDetails(pmsPromotions.getPromotionId(),f.format(curdate).toLowerCase());
	 					    					 if(listPromotionDetailCheck.size()>0 && !listPromotionDetailCheck.isEmpty()){
	 					    						for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
	    	 					    					promotionSecondPercent=promoDetails.getPromotionPercentage();
   	 	 					    					promotionSecondInr=promoDetails.getPromotionInr();
	    	 					    				 }
	 					    					 }
			    			    			 }else{
			    			    				 promotionFlag=false;
			    			    				 promotionType=null;
			    			    				 blnPromoFlag=false;
			    			    				 basePromoFlag=true;
			    			    			 }
						    			 }
				    				 }
				    			 }
 						}
 						if(promotionFlag){
							int rateCount=0,rateIdCount=0;
   						List<PropertyRate> propertyRateList =  rateController.listAllDates(property.getPropertyId(),accommodations.getAccommodationId(),getSourceTypeId(),d.toDate());
   				    	if(propertyRateList.size()>0)
   				    	{
   				    		 for (PropertyRate pr : propertyRateList)
   			    			 {
				    				 int propertyRateId = pr.getPropertyRateId();
					    			 List<PropertyRateDetail> rateDetailList =   detailController.list(propertyRateId,f.format(d.toDate()).toLowerCase());
       				    		 if(rateDetailList.size()>0){
       				    			 if(rateCount<1){
       				    				 for (PropertyRateDetail rate : rateDetailList){
       				    					 baseAmount+=  rate.getBaseAmount();
       				    					 extraAdultAmount+=rate.getExtraAdult();
       	  				    				 extraChildAmount+=rate.getExtraChild();
	        				    			 }
       				    				 rateCount++;
       				    			 }
       				    		 }else{
       				    			 if(propertyRateList.size()==rateIdCount){
       				    				 baseAmount += accommodations.getBaseAmount();
       				    				 extraAdultAmount+=accommodations.getExtraAdult();
         				    				 extraChildAmount+=accommodations.getExtraChild();
       				    			 }
           				    	 }
   			    			 }	    		 
   				    	 }
   				    	 else{
   				    		baseAmount += accommodations.getBaseAmount();
   				    		extraAdultAmount+=accommodations.getExtraAdult();
			    				extraChildAmount+=accommodations.getExtraChild();
   				    	 }
   				    	double discountAmount=0;
				    		 if(blnFirstActivePromo){
 	    				    	  String strTypePercent=String.valueOf(promotionFirstPercent);
 	    				    	  String strTypeInr=String.valueOf(promotionFirstInr);
					    		  if(!strTypePercent.equalsIgnoreCase("null") && promotionFirstPercent!=0){
					    			  actualPromoBaseAmount = baseAmount-(baseAmount*promotionFirstPercent/100);
					    			  discountAmount+=baseAmount*promotionFirstPercent/100;
					    		  }
					    		  else if(!strTypeInr.equalsIgnoreCase("null") && promotionFirstInr!=0){
					    			  actualPromoBaseAmount = baseAmount-promotionFirstInr;
					    			  discountAmount+=promotionFirstInr;
					    		  }
					    		  else{
					    			  actualPromoBaseAmount = baseAmount;
					    		  } 
				    		 }else{
				    			  actualPromoBaseAmount = baseAmount;
				    		  } 
				    		 if(blnSecondActivePromo){
				    			String strTypePercent=String.valueOf(promotionSecondPercent);
 	    				    	String strTypeInr=String.valueOf(promotionSecondInr);
					    		  if(!strTypePercent.equalsIgnoreCase("null") && promotionSecondPercent!=0){
					    			  actualPromoBaseAmount = actualPromoBaseAmount-(actualPromoBaseAmount*promotionSecondPercent/100);
					    			  discountAmount+=baseAmount*promotionSecondPercent/100;
					    		  }
					    		  else if(!strTypeInr.equalsIgnoreCase("null") && promotionSecondInr!=0){
					    			  actualPromoBaseAmount= actualPromoBaseAmount-promotionSecondInr;
					    			  discountAmount+=promotionSecondInr;
					    		  }
					    		  else{
					    			  actualPromoBaseAmount = actualPromoBaseAmount;
					    		  }
				    		 }else{
				    			  actualPromoBaseAmount = actualPromoBaseAmount;
				    		 }
				    		 actualBaseAmount=baseAmount;
				    		 totalFlatAmount=baseAmount-discountAmount;
				    		 
				    	 }else{
				    		 if(basePromoFlag){
				    			baseAmount += accommodations.getBaseAmount();
				    			extraAdultAmount+=accommodations.getExtraAdult();
				    			extraChildAmount+=accommodations.getExtraChild();
				    		}else{
				    			int rateCount=0,rateIdCount=0;
	    						List<PropertyRate> propertyRateList =  rateController.listAllDates(property.getPropertyId(),accommodations.getAccommodationId(),getSourceTypeId(),d.toDate());
	    				    	if(propertyRateList.size()>0)
	    				    	{
	    				    		 for (PropertyRate pr : propertyRateList)
	    			    			 {
					    				 int propertyRateId = pr.getPropertyRateId();
						    			 List<PropertyRateDetail> rateDetailList =   detailController.list(propertyRateId,f.format(d.toDate()).toLowerCase());
	        				    		 if(rateDetailList.size()>0){
	        				    			 if(rateCount<1){
	        				    				 for (PropertyRateDetail rate : rateDetailList){
	        				    					 baseAmount +=  rate.getBaseAmount();
	        				    					 extraAdultAmount+=rate.getExtraAdult();
	        	  				    				 extraChildAmount+=rate.getExtraChild();
		        				    			 }
	        				    				 rateCount++;
	        				    			 }
	        				    		 }else{
	        				    			 if(propertyRateList.size()==rateIdCount){
	        				    				 baseAmount += accommodations.getBaseAmount();
	        				    				 extraAdultAmount+=accommodations.getExtraAdult();
	          				    				 extraChildAmount+=accommodations.getExtraChild();
	        				    			 }
	            				    	 }
	    			    			 }	    		 
	    				    	 }
	    				    	 else{
	    				    		baseAmount += accommodations.getBaseAmount();
	    				    		extraAdultAmount+=accommodations.getExtraAdult();
				    				extraChildAmount+=accommodations.getExtraChild();
	    				    	 }
				    		}
				    	 }
 						 actualBaseAmount=baseAmount;
 				    	 countDate++;
 				    }
 					Iterator<String> iterPromoType=arlPromoType.iterator();
 					while(iterPromoType.hasNext()){
 						String strPromoType=iterPromoType.next();
 						if(strPromoType.equalsIgnoreCase("F") || strPromoType.equalsIgnoreCase("E")){
 							String strTypePercent=String.valueOf(promotionFirstPercent);
   				    	String strTypeInr=String.valueOf(promotionFirstInr);
				    		if(!strTypePercent.equalsIgnoreCase("null") && promotionFirstPercent!=0){
				    			promotionFirstName=String.valueOf(Math.round(promotionFirstPercent))+" % OFF";
	  							promoFirstPercent=(int) Math.round(promotionFirstPercent);
				    		}
				    		else if(!strTypeInr.equalsIgnoreCase("null") && promotionFirstInr!=0){
				    			promotionFirstName="Flat "+String.valueOf(Math.round(promotionFirstInr))+" OFF";
	  							promoFirstInr=(int) Math.round(promotionFirstInr);
				    		}
 							
 						}
 						if(strPromoType.equalsIgnoreCase("L")){
 							  String strTypePercent=String.valueOf(promotionSecondPercent);
 							  String strTypeInr=String.valueOf(promotionSecondInr);
				    		  if(!strTypePercent.equalsIgnoreCase("null") && promotionSecondPercent!=0){
				    			  promotionSecondName=String.valueOf(Math.round(promotionSecondPercent))+" % OFF";
		  						  promoSecondPercent=(int) Math.round(promotionSecondPercent);
				    		  }
				    		  else if(!strTypeInr.equalsIgnoreCase("null") && promotionSecondInr!=0){
				    			  promotionSecondName="Flat "+String.valueOf(Math.round(promotionSecondInr))+" OFF";
		  						  promoSecondInr=(int) Math.round(promotionSecondInr);
				    		  }
 							
 						}
 					}
 					arlPromoType.clear();
 				
 					if(blnPromoFlag){
 						if(promotionType.equalsIgnoreCase("L") || promotionType.equalsIgnoreCase("E") || promotionType.equalsIgnoreCase("F")){
 							totalAmount=totalFlatAmount;
 							
 							if(countDate>1){
 								actualAdultAmount=extraAdultAmount/countDate;
 								actualChildAmount=extraChildAmount/countDate;
 							}else{
 								actualAdultAmount=extraAdultAmount;
 								actualChildAmount=extraChildAmount;
 							}
 						}
 					}else{
 						totalAmount=baseAmount;
 						
 						if(countDate>1){
 							actualAdultAmount=extraAdultAmount/countDate;
 							actualChildAmount=extraChildAmount/countDate;
 						}else{
 							actualAdultAmount=extraAdultAmount;
 							actualChildAmount=extraChildAmount;
 						}
 					}
 					
 					
 					totalAmount=Math.round(totalAmount);
 					actualTotalAdultAmount=Math.round(actualAdultAmount);
 					actualTotalChildAmount=Math.round(actualChildAmount);
 					
 					
 					long minimum = getAvailableCount(property.getPropertyId(),getArrivalDate(),getDepartureDate(),roomCount,accommId);
 			    	 
 			    	 if(minimum==0){
 			    		 jsonOutput += ",\"available\":\"" + minimum+ "\"";
 			    		 jsonOutput += ",\"baseActualAmount\":\"" + + (actualBaseAmount==0 ? accommodations.getBaseAmount() : actualBaseAmount )+  "\"";
 			    		 jsonOutput += ",\"baseAmount\":\"" + + (totalAmount==0 ? accommodations.getBaseAmount() : totalAmount )+  "\"";
 			    		 jsonOutput += ",\"totalAmount\":\"" + + (totalAmount==0 ? accommodations.getBaseAmount() : totalAmount )+  "\"";
 			    		 jsonOutput += ",\"accommodationName\":\"" + accommodations.getAccommodationType().toUpperCase()+ "\"";
 						 jsonOutput += ",\"accommodationId\":\"" + accommodations.getAccommodationId()+ "\"";
 						 jsonOutput += ",\"promotionFirstName\":\"" + (promotionFirstName==null? "None":promotionFirstName)+ "\"";
 						 jsonOutput += ",\"promotionSecondName\":\"" + (promotionSecondName==null? "None":promotionSecondName)+ "\"";
 						 jsonOutput += ",\"promoFirstPercent\":\"" + (promoFirstPercent==null? 0:promoFirstPercent)+ "\"";
 						 jsonOutput += ",\"promoFirstInr\":\"" + (promoFirstInr==null? 0:promoFirstInr)+ "\"";
						 jsonOutput += ",\"promoSecondInr\":\"" + (promoSecondInr==null? 0:promoSecondInr)+ "\"";
 						 jsonOutput += ",\"promoSecondPercent\":\"" + (promoSecondPercent==null? 0:promoSecondPercent)+ "\"";
 						 jsonOutput += ",\"promoLastMinuteTimer\":\"" + (getDisplayHoursForTimer()==null ?"None" :getDisplayHoursForTimer()  )+ "\"";
 				     }else{
 				    	jsonOutput += ",\"accommodationName\":\"" + accommodations.getAccommodationType().toUpperCase()+ "\"";
 						jsonOutput += ",\"accommodationId\":\"" + accommodations.getAccommodationId()+ "\"";
 			    		jsonOutput += ",\"available\":\"" + minimum+ "\"";
 			    		jsonOutput += ",\"maxAdult\":\"" + accommodations.getMaxOccupancy()+ "\"";
 			    		jsonOutput += ",\"maxChild\":\"" + 0 + "\"";
 			    		jsonOutput += ",\"minOccupancy\":\"" + accommodations.getMinOccupancy() + "\"";
 						jsonOutput += ",\"maxOccupancy\":\"" + accommodations.getMaxOccupancy() + "\"";
 						jsonOutput += ",\"baseActualAmount\":\"" +  (actualBaseAmount==0 ? accommodations.getBaseAmount() : actualBaseAmount )+  "\"";
 						jsonOutput += ",\"promoFirstPercent\":\"" + (promoFirstPercent==null? 0:promoFirstPercent)+ "\"";
 						jsonOutput += ",\"promoFirstInr\":\"" + (promoFirstInr==null? 0:promoFirstInr)+ "\"";
						jsonOutput += ",\"promoSecondInr\":\"" + (promoSecondInr==null? 0:promoSecondInr)+ "\"";
 						jsonOutput += ",\"promoSecondPercent\":\"" + (promoSecondPercent==null? 0:promoSecondPercent)+ "\"";
 						jsonOutput += ",\"baseAmount\":\"" + + (totalAmount==0 ? accommodations.getBaseAmount() : totalAmount )+  "\"";
 			    		jsonOutput += ",\"totalAmount\":\"" + + (totalAmount==0 ? accommodations.getBaseAmount() : totalAmount )+  "\"";
 			    		jsonOutput += ",\"extraAdultAmount\":\"" + + (actualTotalAdultAmount==0 ? accommodations.getExtraAdult() : actualTotalAdultAmount )+  "\"";
 			    		jsonOutput += ",\"extraChildAmount\":\"" + + (actualTotalChildAmount==0 ? accommodations.getExtraChild() : actualTotalChildAmount )+  "\"";
 			    		jsonOutput += ",\"promotionFirstName\":\"" + (promotionFirstName==null? "None":promotionFirstName)+ "\"";
						jsonOutput += ",\"promotionSecondName\":\"" + (promotionSecondName==null? "None":promotionSecondName)+ "\"";
 			    		jsonOutput += ",\"promotionId\":\"" + (promotionId==0? 0:promotionId) + "\"";
 			    		jsonOutput += ",\"promoLastMinuteTimer\":\"" + (getDisplayHoursForTimer()==null ?"None" :getDisplayHoursForTimer()  )+ "\"";
 						discountId=35;
 						PropertyDiscountManager discountController=new PropertyDiscountManager();
 						PropertyDiscount propertyDiscount=discountController.find(discountId);
 			    		jsonOutput += ",\"discountPercent\":\"" + (propertyDiscount.getDiscountPercentage()==0.0? "0":propertyDiscount.getDiscountPercentage())+ "\"";
 			    		jsonOutput += ",\"discountId\":\"" + (discountId==0? "0" :discountId) + "\"";
 			    		
 			    		
 				     }
 				}
 		     
 			}
 			jsonOutput += "}";
 			
 			arllist.clear();
 		}
 	 
         
 		response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
// 		sessionMap.clear();
 		

 	} catch (Exception e) {
 		logger.error(e);
 		e.printStackTrace();
 	} finally {

 	}

 	
   	return null;
   }
   
   public String getLocationProperties() throws IOException{

		try {
		
	 /* SeoContentManager seoContentController = new SeoContentManager();
	  SeoContent seoContent = seoContentController.findSeoContent(getLocationId());	
	        
	  request.setAttribute("metaTag" ,seoContent.getTitle());
	  request.setAttribute("description" ,seoContent.getDescription());
	  request.setAttribute("content" ,seoContent.getContent());*/

			
		Map mapDate=new HashMap();
		
		DateFormat format = new SimpleDateFormat("MMM d,yyyy", Locale.ENGLISH);
	    java.util.Date dateStart = format.parse((String)request.getAttribute("StartDate"));
	    java.util.Date dateEnd = format.parse((String)request.getAttribute("EndDate"));
	   
	    if(getSourceTypeId()==null){
	    	this.sourceTypeId=1;
	    }
	    
	    Calendar calCheckStart=Calendar.getInstance();
	    calCheckStart.setTime(dateStart);
	    java.util.Date checkInDate = calCheckStart.getTime();
	    this.arrivalDate=new Timestamp(checkInDate.getTime());
	   
	    
	    Calendar calCheckEnd=Calendar.getInstance();
	    calCheckEnd.setTime(dateEnd);
	    java.util.Date checkOutDate = calCheckEnd.getTime();
	    this.departureDate=new Timestamp(checkOutDate.getTime());
	    
	    String checkIn = new SimpleDateFormat("MMM d,yyyy").format(checkInDate);
       String checkOut = new SimpleDateFormat("MMM d,yyyy").format(checkOutDate);
       String checkIn1 = new SimpleDateFormat("dd MMM").format(checkInDate);
       String checkOut1 = new SimpleDateFormat("dd MMM").format(checkOutDate);
       
       sessionMap.put("checkIn",checkIn);
       sessionMap.put("checkOut",checkOut);
       sessionMap.put("checkIn1",checkIn1);
       sessionMap.put("checkOut1",checkOut1);
       
		/*this.arrivalDate = getArrivalDate();
	    sessionMap.put("arrivalDate",arrivalDate); 
		
		this.departureDate = getDepartureDate();
		sessionMap.put("departureDate",departureDate); */
		mapDate.put("arrivalDate", arrivalDate);
		mapDate.put("departureDate", departureDate);
		
		PmsPropertyManager propertyController = new PmsPropertyManager();		
		PropertyAccommodationManager accommodationManager = new PropertyAccommodationManager();
		PropertyAmenityManager amenityController = new PropertyAmenityManager();
		List<PropertyAmenity> propertyAmenityList;
		PropertyTypeManager typeController =  new PropertyTypeManager();
		PropertyRateManager rateController = new PropertyRateManager();
		PropertyRateDetailManager detailController = new PropertyRateDetailManager();
		PmsAmenityManager ameController = new PmsAmenityManager();
		PromotionManager promotionController=new PromotionManager();
		PromotionDetailManager promotionDetailController=new PromotionDetailManager();
		PmsBookingManager bookingController=new PmsBookingManager();
		
		
		HashMap<Double,Integer > mapAmountAccommId=new HashMap<Double, Integer>();
		HashMap<Double,Integer > mapSoldOutAmountAccommId=new HashMap<Double, Integer>();
		
		ArrayList<Integer> arlSortPropertyId=new ArrayList<Integer>();
		HashMap<Integer, Integer> mapAvailablePropertyId=new HashMap<Integer, Integer>();
		String jsonOutput = "";
		HttpServletResponse response = ServletActionContext.getResponse();
		
		response.setContentType("application/json");
		
		this.arrivalDate = getArrivalDate();
	    sessionMap.put("arrivalDate",arrivalDate); 
			
		this.departureDate = getDepartureDate();
		sessionMap.put("departureDate",departureDate); 
		 
		Calendar cal = Calendar.getInstance();
		cal.setTime(this.departureDate);
	    cal.add(Calendar.DATE, -1);
	    
	    java.util.Date dteCheckOut = cal.getTime();
	    this.departureDate=new Timestamp(dteCheckOut.getTime());
	    sessionMap.put("endDate",departureDate); 
	    
		String startDate = new SimpleDateFormat("yyyy-MM-dd").format(this.arrivalDate);
		String endDate = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
		DateTime start = DateTime.parse(startDate);
	    DateTime end = DateTime.parse(endDate);
	    DecimalFormat df = new DecimalFormat("###.##");
	    DateFormat f = new SimpleDateFormat("EEEE");
	    
   	List<PmsSmartPrice> listSmartPriceDetail=null; 
   	List<AccommodationRoom> listAccommodationRoom=null;
		PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
		PropertyReviewsManager reviewController=new PropertyReviewsManager();
	    int availRoomCount=0,soldRoomCount=0;
       this.propertyList = propertyController.listPropertyByLocation(getLocationId());
		for (PmsProperty property : propertyList) {
			
			
 			
 			if (!jsonOutput.equalsIgnoreCase(""))
 				jsonOutput += ",{";
 			else
 				jsonOutput += "{";
 			jsonOutput += "\"DT_RowId\":\"" + property.getPropertyId()+ "\"";
 			jsonOutput += ",\"city\":\"" + property.getCity() + "\"";
 			jsonOutput += ",\"propertyTypeId\":\"" + property.getPropertyType().getPropertyTypeId() + "\"";
 			jsonOutput += ",\"propertyTypeName\":\"" + property.getPropertyType().getPropertyTypeName() + "\"";
 			jsonOutput += ",\"propertyName\":\"" + (property.getPropertyName() == null ? "": property.getPropertyName().toUpperCase().trim())+ "\"";
 			jsonOutput += ",\"propertyUrl\":\"" + (property.getPropertyUrl() == null ? "": property.getPropertyUrl().toLowerCase().trim())+ "\"";
 			
 			if(property.getLocation()!=null){
 				jsonOutput += ",\"locationName\":\"" + (property.getLocation().getLocationName() == null ? "": property.getLocation().getLocationName().toUpperCase().trim())+ "\"";	
 			}else{
 				jsonOutput += ",\"locationName\":\"" + "" + "\"";
 			}
 			
// 			jsonOutput += ",\"propertyStandardPolicy\":\"" + (property.getPropertyStandardPolicy() == null ? "None": property.getPropertyStandardPolicy().trim())+ "\"";
// 			jsonOutput += ",\"propertyHotelPolicy\":\"" + (property.getPropertyHotelPolicy() == null ? "None": property.getPropertyHotelPolicy().trim())+ "\"";
// 			jsonOutput += ",\"propertyCancellationPolicy\":\"" + (property.getPropertyCancellationPolicy() == null ? "None": property.getPropertyCancellationPolicy().trim())+ "\"";
 			jsonOutput += ",\"payAtHotelStatus\":\"" + (property.getPayAtHotelStatus() == null ? false: property.getPayAtHotelStatus())+ "\"";
			jsonOutput += ",\"coupleStatus\":\"" + ( property.getCoupleIsActive() == null? false :  property.getCoupleIsActive() )+ "\"";
			jsonOutput += ",\"breakfastStatus\":\"" + ( property.getBreakfastIsActive() == null? false:  property.getBreakfastIsActive() )+ "\"";
			jsonOutput += ",\"hotelStatus\":\"" + ( property.getHotelIsActive() == null? false:  property.getHotelIsActive() )+ "\"";
			
 			SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
 			Timestamp FromDate=(Timestamp)mapDate.get("arrivalDate");
 			Timestamp ToDate=(Timestamp)mapDate.get("departureDate");
 			jsonOutput += ",\"arrivalDate\":\"" + format2.format(FromDate) + "\"";
 			jsonOutput += ",\"departureDate\":\"" + format2.format(ToDate)+ "\"";
 			
 			String strFromDate=format2.format(FromDate);
 			String strToDate=format2.format(ToDate);
 	    	DateTime startdate = DateTime.parse(strFromDate);
 	        DateTime enddate = DateTime.parse(strToDate);
 	        java.util.Date fromdate = startdate.toDate();
 	 		java.util.Date todate = enddate.toDate();
 	 		 
 			int diffInDays = (int) ((todate.getTime() - fromdate.getTime()) / (1000 * 60 * 60 * 24));
 			
 			List arllist=new ArrayList();
 			List arllistAvl=new ArrayList();
 			List arllistSold=new ArrayList();
 			jsonOutput += ",\"diffDays\":\"" + diffInDays+ "\"";
 			
 			String jsonPhotos="";
  			PropertyPhotoManager photoController = new PropertyPhotoManager();
			
		    response.setContentType("application/json");
		    List<PropertyPhoto> listPhotos =photoController.list(property.getPropertyId());
            
            if(listPhotos.size()>0)
  			{
            	for (PropertyPhoto photo : listPhotos) {

    				if (!jsonPhotos.equalsIgnoreCase(""))
    					jsonPhotos += ",{";
    				else
    					jsonPhotos += "{";
    				jsonPhotos += "\"DT_RowId\":\"" + photo.getPropertyPhotoId() + "\"";
    				jsonPhotos += ",\"isPrimary\":\"" + photo.getIsPrimary()+ "\"";
    				jsonPhotos += ",\"photoPath\":\"" + photo.getPhotoPath()+ "\"";
    				
    				
    				jsonPhotos += "}";


    			}
    			
                   jsonOutput += ",\"photos\":[" + jsonPhotos+ "]";
  			
  			}
 			
 			String jsonAmenities ="";
 			propertyAmenityList =  amenityController.list(property.getPropertyId());
 			if(propertyAmenityList.size()>0)
 			{
 				for (PropertyAmenity amenity : propertyAmenityList) {
 					if (!jsonAmenities.equalsIgnoreCase(""))
 						
 						jsonAmenities += ",{";
 					else
 						jsonAmenities += "{";
 					PropertyAmenity amentity1 = amenityController.find(amenity.getPropertyAmenityId());
 				    String path = "ulowebsite/images/icons/";
 					PmsAmenity pmsAmenity1 =ameController.find(amentity1.getPmsAmenity().getAmenityId());
 					jsonAmenities += "\"DT_RowId\":\"" + amenity.getPropertyAmenityId()+ "\"";					
 					jsonAmenities += ",\"amenityName\":\"" + pmsAmenity1.getAmenityName()+ "\"";
 					jsonAmenities += ",\"amenityId\":\"" + pmsAmenity1.getAmenityId()+ "\"";
                     jsonAmenities += ",\"icon\":\"" + path + pmsAmenity1.getAmenityIcon()+ "\"";
              
 					
 					jsonAmenities += "}";
 				}
 				
 				jsonOutput += ",\"amenities\":[" + jsonAmenities+ "]";
 			}
 			
 			String jsonReviews="";
			List<PropertyReviews> listReviews=reviewController.listReview(property.getPropertyId());
			if(listReviews.size()>0 && !listReviews.isEmpty()){
				for(PropertyReviews reviews:listReviews){
					
					if (!jsonReviews.equalsIgnoreCase(""))
						
						jsonReviews += ",{";
					else
						jsonReviews += "{";

					/*jsonReviews += "\"starCount\":\"" + reviews.getStarCount().intValue()+ "\"";	
					jsonReviews += ",\"reviewCount\":\"" + reviews.getReviewCount().intValue()+ "\"";
					Double averageReviews=reviews.getAverageReview();
					jsonReviews += ",\"averageReview\":\"" + df.format(averageReviews)+ "\"";*/
					
					jsonReviews += "\"starCount\":\"" + (reviews.getStarCount()==null?0:reviews.getStarCount().intValue())+ "\"";	
					jsonReviews += ",\"reviewCount\":\"" + (reviews.getReviewCount()==null?0:reviews.getReviewCount().intValue())+ "\"";	
					if(reviews.getAverageReview()!=null){
						Double averageReviews=reviews.getAverageReview();
						jsonReviews += ",\"averageReview\":\"" + df.format(averageReviews)+ "\"";
					}else{
						jsonReviews += ",\"averageReview\":\"" + df.format(0)+ "\"";
					}
					
					jsonReviews += ",\"tripadvisorStarCount\":\"" + (reviews.getTripadvisorStarCount()==null?0:reviews.getTripadvisorStarCount().intValue())+ "\"";	
					jsonReviews += ",\"tripadvisorReviewCount\":\"" + (reviews.getTripadvisorReviewCount()==null?0:reviews.getTripadvisorReviewCount().intValue())+ "\"";
					if(reviews.getTripadvisorAverageReview()!=null){
						Double taAverageReviews=reviews.getTripadvisorAverageReview();
						jsonReviews += ",\"tripadvisorAverageReview\":\"" + df.format(taAverageReviews)+ "\"";
					}else{
						jsonReviews += ",\"tripadvisorAverageReview\":\"" + df.format(0)+ "\"";
					}
					jsonReviews += ",\"googleReviewUrl\":\"" + (reviews.getGoogleReviewUrl()==null?"":reviews.getGoogleReviewUrl())+ "\"";	
					jsonReviews += ",\"tripadvisorReviewUrl\":\"" + (reviews.getTripadvisorReviewUrl()==null?"":reviews.getTripadvisorReviewUrl())+ "\"";	
					jsonReviews += ",\"propertyRating\":\"" + (reviews.getPropertyRating()==null?"None":reviews.getPropertyRating())+ "\"";
					jsonReviews += "}";
				}
				
			}else{
				if (!jsonReviews.equalsIgnoreCase(""))
					
					jsonReviews += ",{";
				else
					jsonReviews += "{";
				
				jsonReviews += "\"starCount\":\"" + 0+ "\"";	
				jsonReviews += ",\"reviewCount\":\"" + 0+ "\"";	
				jsonReviews += ",\"averageReview\":\"" + 0+ "\"";
				
				jsonReviews += ",\"tripadvisorStarCount\":\"" + 0 + "\"";	
				jsonReviews += ",\"tripadvisorReviewCount\":\"" + 0 + "\"";	
				jsonReviews += ",\"tripadvisorAverageReview\":\"" + 0+ "\"";
				
				jsonReviews += ",\"googleReviewUrl\":\"" + "None"+ "\"";	
				jsonReviews += ",\"tripadvisorReviewUrl\":\"" + "None"+ "\"";	
				jsonReviews += ",\"propertyRating\":\"" + "None"+ "\"";
				
				jsonReviews += "}";
			}
			
			jsonOutput += ",\"reviews\":[" + jsonReviews+ "]";
			
 			
 			String jsonAccommodation ="";
 			List<PropertyAccommodation> accommodationsList =  accommodationManager.list(property.getPropertyId());
 			int soldOutCount=0,availCount=0,promotionAccommId=0;
 			if(accommodationsList.size()>0 && !accommodationsList.isEmpty())
 			{
 				for (PropertyAccommodation accommodation : accommodationsList) {
 					int roomCount=0;
 					listAccommodationRoom=accommodationController.listPropertyTotalRooms(property.getPropertyId(), accommodation.getAccommodationId());
 					if(listAccommodationRoom.size()>0 && !listAccommodationRoom.isEmpty()){
 						for(AccommodationRoom roomsList:listAccommodationRoom){
 							 roomCount=(int) roomsList.getRoomCount();
 							 long minimum = getAvailableCount(property.getPropertyId(),getArrivalDate(),getDepartureDate(),roomCount,accommodation.getAccommodationId());
 							 if((int)minimum>0){
 								 availCount++;
 							 }else if((int)minimum==0){
 								 soldOutCount++;
 							 }
 							 break;
 						 }
 					 }
 					
 					
 					if (!jsonAccommodation.equalsIgnoreCase(""))
 						
 						jsonAccommodation += ",{";
 					else
 						jsonAccommodation += "{";
 						jsonAccommodation += "\"accommodationId\":\"" + accommodation.getAccommodationId()+ "\"";
 						jsonAccommodation += ",\"accommodationName\":\"" + accommodation.getAccommodationType().toUpperCase()+ "\"";
 					
 					jsonAccommodation += "}";
 				}
 				
 				jsonOutput += ",\"accommodations\":[" + jsonAccommodation+ "]";
 			}

 			
 			List<PmsPromotionDetails> listPromotionDetailCheck=null;
 			Boolean blnAvailable=false,blnSoldout=false,firstPromotionFlag=false,secondPromotionFlag=false,promotionFlag=false,blnPromoFlag=false;
 			int arlListTotalAccomm=0,soldOutTotalAccomm=0,accommCount=0;
 			String promotionType=null;
 			ArrayList<String> arlListPromoType=new ArrayList<String>();
 			List<PropertyAccommodation> accommodationList =  accommodationManager.list(property.getPropertyId());
 			if(accommodationList.size()>0)
 			{
 				for (PropertyAccommodation accommodation : accommodationList) {
 					Integer promoAccommId=0,enablePromoId=0;
 					ArrayList<Integer> arlEnablePromo=new ArrayList<Integer>();
 					Boolean blnPromotionIsNotFlag=false;
 					Boolean blnLastMinuteIsActive=false,blnLastMinuteFirstActive=false;
 					int dateCount=0,promoCount=0;
 					String strCheckPromotionType=null,promotionFirstDiscountType=null,promotionSecondDiscountType=null,promotionHoursRange=null;
 					double amount=0.0,extraAdultAmount=0.0,extraChildAmount=0.0,totalSmartPriceAmount=0.0;
 					int roomCount=0,availableCount=0,totalCount=0;
 				    double dblPercentCount=0.0,dblSmartPricePercent=0.0,
 				    		promotionFirstPercent=0.0,promotionFirstInr=0.0,
 				    		promotionSecondPercent=0.0,promotionSecondInr=0.0,totalFlatLastAmount=0.0,flatLastAmount=0.0;
 				    Timestamp lastMinStartDate=null,lastMinEndDate=null;
 				    Boolean isPositive=false,isNegative=false,blnLastMinutePromotions=false;
 				   
 	    			
 	    			listAccommodationRoom=accommodationController.listPropertyTotalRooms(property.getPropertyId(), accommodation.getAccommodationId());
 					 
 					if(listAccommodationRoom.size()>0 && !listAccommodationRoom.isEmpty()){
 						
 						for(AccommodationRoom roomsList:listAccommodationRoom){
 							roomCount=(int) roomsList.getRoomCount();
 							if(roomCount>0){
 								long minimum = getAvailableCount(property.getPropertyId(),getArrivalDate(),getDepartureDate(),roomCount,accommodation.getAccommodationId());
 								if((int)minimum>0){
 									blnAvailable=true;
 									blnSoldout=false;
 									arlListTotalAccomm++;
 								}else if((int)minimum==0){
 									blnSoldout=true;
 									blnAvailable=false;
 									soldOutTotalAccomm++;
 								}
 							}
 							 break;
 						 }
 					 }
 					 Timestamp tsPromoBookedDate=null,tsPromoStartDate=null,tsPromoEndDate=null;
 					 Timestamp tsFirstStartRange=null,tsSecondStartRange=null,tsFirstEndRange=null,tsSecondEndRange=null;
					 Timestamp tsLastStartDate=null,tsLastEndDate=null;
 					 boolean blnFirstActivePromo=false,blnFirstNotActivePromo=false,baseFirstPromoFlag=false,blnEarlyBirdPromo=false;
 					 boolean blnSecondActivePromo=false,blnSecondNotActivePromo=false,baseSecondPromoFlag=false,blnLastMinutePromo=false;
 					 java.util.Date currentdate=new java.util.Date();
 					 SimpleDateFormat format1=new SimpleDateFormat("yyyy-MM-dd");
 					 String strCurrentDate = new SimpleDateFormat("yyyy-MM-dd").format(currentdate);
 				   	 java.util.Date dtecurdate = format1.parse(strCurrentDate);
 				   	 Calendar calStart=Calendar.getInstance();
 				   	 calStart.setTime(dtecurdate);
 				   	 calStart.setTimeZone(TimeZone.getTimeZone("IST"));
 				   	 String StartDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calStart.getTime());
 				   	 java.util.Date curdate = format1.parse(StartDate);
 				   	 this.displayHoursForTimer=null;
 				   	 setDisplayHoursForTimer(this.displayHoursForTimer);
 					 List<PmsPromotions> listDatePromotions= promotionController.listBookedDatePromotions(property.getPropertyId(),curdate);
 					 if(listDatePromotions.size()>0 && !listDatePromotions.isEmpty()){
 						for(PmsPromotions promotions:listDatePromotions){
 							tsPromoBookedDate=promotions.getPromotionBookedDate();
 							tsPromoStartDate=promotions.getStartDate();
 							tsPromoEndDate=promotions.getEndDate();
 							blnEarlyBirdPromo=checkEarlyBirdPromos(tsPromoBookedDate,tsPromoStartDate,tsPromoEndDate,getArrivalDate(),getDepartureDate());
 						}
 					 }else{
 						 blnEarlyBirdPromo=false;
 					 }
 					 
 					List<PmsPromotions> listLastPromotions= promotionController.listLastBookedPromotions(property.getPropertyId(),curdate);
					 if(listLastPromotions.size()>0 && !listLastPromotions.isEmpty()){
						for(PmsPromotions promotions:listLastPromotions){
							tsFirstStartRange=promotions.getFirstRangeStartDate();
							tsSecondStartRange=promotions.getSecondRangeStartDate();
							tsFirstEndRange=promotions.getFirstRangeEndDate();
							tsSecondEndRange=promotions.getSecondRangeEndDate();
							tsLastStartDate=promotions.getStartDate();
							tsLastEndDate=promotions.getEndDate();
							promotionHoursRange=promotions.getPromotionHoursRange();
							blnLastMinuteFirstActive=checkLastMinutePromos(promotionHoursRange,getArrivalDate(),getDepartureDate(),tsSecondStartRange,tsFirstEndRange,tsFirstEndRange,tsSecondStartRange);
//							blnLastMinuteIsActive=getCheckHoursRange(promotionHoursRange,getArrivalDate(),getDepartureDate());
							if(blnLastMinuteFirstActive){
								blnLastMinutePromo=true;
								enablePromoId=promotions.getPromotionId();
								arlEnablePromo.add(enablePromoId);
							}
						}
					 }else{
						 blnLastMinutePromo=false;
						 this.displayHoursForTimer=null;
				    	 setDisplayHoursForTimer(this.displayHoursForTimer);
					 }
					 
 					 ArrayList<Integer> arlFirstActivePromo=new ArrayList<Integer>(); 
 					 ArrayList<Integer> arlSecondActivePromo=new ArrayList<Integer>(); 
 					 List<DateTime> between = DateUtil.getDateRange(start, end);
 					 for (DateTime d : between)
 				     {
 						 if(blnEarlyBirdPromo){
 							 List<PmsPromotions> listEarlyPromotions= promotionController.listBookedDatePromotions(property.getPropertyId(),curdate);
 							 if(listEarlyPromotions.size()>0 && !listEarlyPromotions.isEmpty()){
 								 for(PmsPromotions promos:listEarlyPromotions){
 									 if(promos.getPromotionType().equalsIgnoreCase("E")){
 	 									 if(arlFirstActivePromo.isEmpty()){
 	 										arlFirstActivePromo.add(promos.getPromotionId());
 	 									 }else if(!arlFirstActivePromo.contains(promos.getPromotionId())){
 	 										arlFirstActivePromo.add(promos.getPromotionId());
 	 									 }
 	 								 }else{
 	 									blnFirstNotActivePromo=true;
 	 								 }
 								 }
 							 }
 						 }else if(!blnEarlyBirdPromo){
 							 List<PmsPromotions> listAllPromotions= promotionController.listFlatDatePromotions(property.getPropertyId(),d.toDate());
 							 if(!listAllPromotions.isEmpty() && listAllPromotions.size()>0){
 								 for(PmsPromotions promos:listAllPromotions){
 									 if(promos.getPromotionType().equalsIgnoreCase("F")){
 	 									 if(arlFirstActivePromo.isEmpty()){
 	 										arlFirstActivePromo.add(promos.getPromotionId());
 	 									 }else if(!arlFirstActivePromo.contains(promos.getPromotionId())){
 	 										arlFirstActivePromo.add(promos.getPromotionId());
 	 									 }
 	 								 }else{
 	 									blnFirstNotActivePromo=true;
 	 								 }
 								 }
 							 } 
 						 }else{
 							 blnFirstNotActivePromo=true;
 						 }
 						 
 						if(blnLastMinutePromo){
 							List<PmsPromotions> listLastPromos= promotionController.listLastBookedPromotions(property.getPropertyId(),curdate);
 							 if(listLastPromos.size()>0 && !listLastPromos.isEmpty()){
 								for(PmsPromotions promos:listLastPromos){
 									if(promos.getPromotionType().equalsIgnoreCase("L")){
 										promoAccommId=promos.getPropertyAccommodation().getAccommodationId();
 										if(arlEnablePromo.contains(promos.getPromotionId())){
											if(arlSecondActivePromo.isEmpty()){
 		  										arlSecondActivePromo.add(promos.getPromotionId());
 		  									 }else if(!arlSecondActivePromo.contains(promos.getPromotionId())){
 		  										arlSecondActivePromo.add(promos.getPromotionId());
 		  									 }
										}
 									 }
 								}
 							 }else{
 								blnSecondNotActivePromo=true;
 							 }
						}else{
							blnSecondNotActivePromo=true;
						}
 				     
 				     }
 					 int intPromoSize=arlFirstActivePromo.size();
 					 if((arlFirstActivePromo.isEmpty() || blnFirstNotActivePromo)){
 						 blnFirstActivePromo=false;
 					 }else{
 						 blnFirstActivePromo=true;
 					 }
 					 
 					 if(arlSecondActivePromo.isEmpty() || blnSecondNotActivePromo){
 						blnSecondActivePromo=false;
 					 }else{
 						blnSecondActivePromo=true;
 					 }
 					 
 					
 				     for (DateTime d : between)
 				     {
 				    	 Integer promotionId=0;
 				    	 if(blnFirstActivePromo){
 				    		if(blnEarlyBirdPromo){
 				    			List<PmsPromotions> listEarlyPromotions= promotionController.listBookedDatePromotions(property.getPropertyId(),curdate);
 	  							 if(listEarlyPromotions.size()>0 && !listEarlyPromotions.isEmpty()){
 	  				    			for(PmsPromotions promotions:listEarlyPromotions){
 	  				    				if(promotions.getPromotionType().equalsIgnoreCase("E")){
 	 					    				promotionId=promotions.getPromotionId();
 	 					    				promotionFirstDiscountType=promotions.getPromotionDiscountType();
 	 					    				listPromotionDetailCheck=promotionDetailController.listPromotionDetails(promotions.getPromotionId(),f.format(curdate).toLowerCase());
 	 					    				 for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
 	 					    					if(promoCount==0){
 	 					    						promotionFirstPercent=promoDetails.getPromotionPercentage();
 	 	 					    					promotionFirstInr=promoDetails.getPromotionInr();
 	 	 					    					promotionType=promotions.getPromotionType();
 	 	 					    					if(promotionType.equalsIgnoreCase("E")){
 	 	 					    						strCheckPromotionType="E";
 	 	 					    					}
 	 	 					    					
 	 		  						    			arlListPromoType.add(promotionType);
 	 					    					}
 	 					    					promoCount++;
 	 					    				 }
 	  				    				}
 	  				    			}
 	  				    			promotionFlag=true; 
 			    					blnPromoFlag=true;
 	  				    		}else{
 	  				    			promotionFlag=false;
 	  	  		    				promotionType=null;
 	  	  		    				blnPromoFlag=false;
 	  	  		    				promotionId=null;
 	  	  		    				baseFirstPromoFlag=true;
 	  				    		}
 				    		}else if(!blnEarlyBirdPromo){
 				    			List<PmsPromotions> listPromotions= promotionController.listFlatDatePromotions(property.getPropertyId(),d.toDate());
 	  				    		if(listPromotions.size()>0 && !listPromotions.isEmpty()){
 	  				    			for(PmsPromotions promotions:listPromotions){
 	  				    				if(promotions.getPromotionType().equalsIgnoreCase("F")){
 	 					    				promotionId=promotions.getPromotionId();
 	 					    				promotionFirstDiscountType=promotions.getPromotionDiscountType();
 	 					    				listPromotionDetailCheck=promotionDetailController.listPromotionDetails(promotions.getPromotionId(),f.format(d.toDate()).toLowerCase());
 	 					    				 for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
 	 					    					if(promoCount==0){
 	 					    						promotionFirstPercent=promoDetails.getPromotionPercentage();
 	 	 					    					promotionFirstInr=promoDetails.getPromotionInr();
 	 	 					    					promotionType=promotions.getPromotionType();
 	 	 					    					if(promotionType.equalsIgnoreCase("F")){
 	 	 					    						strCheckPromotionType="F";
 	 	 					    					}
 	 	 					    					
 	 		  						    			arlListPromoType.add(promotionType);
 	 					    					}
 	 					    					promoCount++;
 	 					    				 }
 	  				    				}
 	  				    			}
 	  				    			promotionFlag=true; 
 			    					blnPromoFlag=true;
 	  				    		}
 				    		}
 				    	 }
 				    	 if(blnSecondActivePromo){
 				    		List<PmsPromotions> listPmsPromotions= promotionController.listLastDatePromotions(property.getPropertyId(),accommodation.getAccommodationId(),curdate);
				    			 if(listPmsPromotions.size()>0 &&!listPmsPromotions.isEmpty()){
				    				 for(PmsPromotions pmsPromotions:listPmsPromotions){
				    					 if(pmsPromotions.getPromotionType().equalsIgnoreCase("L")){
				    						 promotionId=pmsPromotions.getPromotionId();
							    			 promoAccommId=pmsPromotions.getPropertyAccommodation().getAccommodationId();
							    			 Timestamp tsStartDate=null,tsEndDate=null;
							    			 tsStartDate=pmsPromotions.getStartDate();
							    			 tsEndDate=pmsPromotions.getEndDate();
							    			 
							    			 promotionHoursRange=pmsPromotions.getPromotionHoursRange();
							    			 if(arlEnablePromo.contains(pmsPromotions.getPromotionId())){
							    				 promotionAccommId=promoAccommId.intValue();
							    				 long minimumCount = getAvailableCount(property.getPropertyId(),getArrivalDate(),getDepartureDate(),roomCount,accommodation.getAccommodationId());
				    			    			 if((int)minimumCount>0){
			    			    					 promotionFlag=true; 
			    			    					 blnPromoFlag=true;
			    			    					 strCheckPromotionType="L";
			    			    					 promotionType=pmsPromotions.getPromotionType();
   	 		  						    		 arlListPromoType.add(promotionType);
			    			    					 promotionSecondDiscountType=pmsPromotions.getPromotionDiscountType();
   	 					    					 listPromotionDetailCheck=promotionDetailController.listPromotionDetails(pmsPromotions.getPromotionId(),f.format(curdate).toLowerCase());
   	 					    					 if(listPromotionDetailCheck.size()>0 && !listPromotionDetailCheck.isEmpty()){
   	 					    						for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
		    	 					    					promotionSecondPercent=promoDetails.getPromotionPercentage();
	    	 	 					    					promotionSecondInr=promoDetails.getPromotionInr();
		    	 					    				 }
   	 					    					 }
				    			    			 }else{
				    			    				 promotionFlag=false;
				    			    				 promotionType=null;
				    			    				 blnPromoFlag=false;
				    			    				 baseSecondPromoFlag=true;
				    			    			 }
							    			 }
				    					 }
				    					 
				    				 }
				    			 }
 				    	 }
 				    	 if(promotionFlag){
 				    		int rateCount=0,rateIdCount=0;
   						List<PropertyRate> propertyRateList =  rateController.listAllDates(property.getPropertyId(),accommodation.getAccommodationId(),getSourceTypeId(),d.toDate());
   				    	if(propertyRateList.size()>0)
   				    	{
   				    		 for (PropertyRate pr : propertyRateList)
   			    			 {
				    				 int propertyRateId = pr.getPropertyRateId();
					    			 List<PropertyRateDetail> rateDetailList =   detailController.list(propertyRateId,f.format(d.toDate()).toLowerCase());
       				    		 if(rateDetailList.size()>0){
       				    			 if(rateCount<1){
       				    				 for (PropertyRateDetail rate : rateDetailList){
       				    					 amount+=  rate.getBaseAmount();
	        				    			 }
       				    				 rateCount++;
       				    			 }
       				    		 }else{
       				    			 if(propertyRateList.size()==rateIdCount){
       				    				 amount+= accommodation.getBaseAmount();
       				    			 }
           				    	 }
   			    			 }	    		 
   				    	 }
   				    	 else{
   				    		 amount+= accommodation.getBaseAmount();
   				    	 }
   				    	 double discountAmount=0;
 				    		 if(blnFirstActivePromo){
  	    				    	String strTypePercent=String.valueOf(promotionFirstPercent);
  	    				    	String strTypeInr=String.valueOf(promotionFirstInr);
					    		  if(!strTypePercent.equalsIgnoreCase("null") && promotionFirstPercent!=0){
					    			  flatLastAmount = amount-(amount*promotionFirstPercent/100);
					    			  discountAmount+=amount*promotionFirstPercent/100;
					    		  }
					    		  else if(!strTypeInr.equalsIgnoreCase("null") && promotionFirstInr!=0){
					    			  flatLastAmount = amount-promotionFirstInr;
					    			  discountAmount+=promotionFirstInr;
					    		  }
					    		  else{
					    			  flatLastAmount = amount;
					    		  } 
 				    		 }else{
				    			  flatLastAmount = amount;
				    		  } 
 				    		 if(blnSecondActivePromo){
 				    			String strTypePercent=String.valueOf(promotionSecondPercent);
  	    				    	String strTypeInr=String.valueOf(promotionSecondInr);
					    		  if(!strTypePercent.equalsIgnoreCase("null") && promotionSecondPercent!=0){
					    			  flatLastAmount = flatLastAmount-(flatLastAmount*promotionSecondPercent/100);
					    			  discountAmount+=amount*promotionSecondPercent/100;
					    		  }
					    		  else if(!strTypeInr.equalsIgnoreCase("null") && promotionSecondInr!=0){
					    			  flatLastAmount = flatLastAmount-promotionSecondInr;
					    			 discountAmount+=promotionSecondInr;
					    		  }
					    		  else{
					    			  flatLastAmount = flatLastAmount;
					    		  }
 				    		 }else{
				    			  flatLastAmount = flatLastAmount;
				    		  }
 				    		 
 				    		totalFlatLastAmount=flatLastAmount;
 				    		totalFlatLastAmount=amount-discountAmount; 
 				    	 }else{
 				    		 if(baseFirstPromoFlag){
 				    			amount += accommodation.getBaseAmount();
 				    		}else{
 				    			int rateCount=0,rateIdCount=0;
 	    						List<PropertyRate> propertyRateList =  rateController.listAllDates(property.getPropertyId(),accommodation.getAccommodationId(),getSourceTypeId(),d.toDate());
 	    				    	if(propertyRateList.size()>0)
 	    				    	{
 	    				    		 for (PropertyRate pr : propertyRateList)
 	    			    			 {
 					    				 int propertyRateId = pr.getPropertyRateId();
						    			 List<PropertyRateDetail> rateDetailList =   detailController.list(propertyRateId,f.format(d.toDate()).toLowerCase());
	        				    		 if(rateDetailList.size()>0){
	        				    			 if(rateCount<1){
	        				    				 for (PropertyRateDetail rate : rateDetailList){
	        				    					 amount +=  rate.getBaseAmount();
		        				    			 }
	        				    				 rateCount++;
	        				    			 }
	        				    		 }else{
	        				    			 if(propertyRateList.size()==rateIdCount){
	        				    				 amount += accommodation.getBaseAmount();
	        				    			 }
	            				    	 }
 	    			    			 }	    		 
 	    				    	 }
 	    				    	 else{
 	    				    		 amount += accommodation.getBaseAmount();
 	    				    	 }
 				    		}
 				    	 }
 				    	 dateCount++;
 				    	 if(dateCount==diffInDays){
 				    		 if(blnAvailable){
 				    			if(promotionType!=null && strCheckPromotionType!=null) {
 				    				if(strCheckPromotionType.equalsIgnoreCase("L") || strCheckPromotionType.equalsIgnoreCase("F") || strCheckPromotionType.equalsIgnoreCase("E")){
			    						mapAmountAccommId.put(totalFlatLastAmount,accommodation.getAccommodationId());
 						      			arllist.add(totalFlatLastAmount);
 					    			}
 					    			
 				    			}else{
 				    				mapAmountAccommId.put(amount,accommodation.getAccommodationId());
 					      			arllist.add(amount);
 				    			}
 		      				 }else if(blnSoldout){
 		      					if(promotionType!=null && strCheckPromotionType!=null) {
	  		      					if(strCheckPromotionType.equalsIgnoreCase("L") || strCheckPromotionType.equalsIgnoreCase("F") || strCheckPromotionType.equalsIgnoreCase("E")){
	  		      						mapSoldOutAmountAccommId.put(totalFlatLastAmount,accommodation.getAccommodationId());
						      			arllist.add(totalFlatLastAmount);
					    			}
 		      					}else{
 				    				mapSoldOutAmountAccommId.put(amount,accommodation.getAccommodationId());
 					      			arllist.add(amount);
 				    			}
 					    	 }
 				    	 }
 				     }
 				     if(blnAvailable){
 				    	 if(promotionType!=null && strCheckPromotionType!=null) {
 				    		if(strCheckPromotionType.equalsIgnoreCase("L") || strCheckPromotionType.equalsIgnoreCase("F") || strCheckPromotionType.equalsIgnoreCase("E")){
 				    			arllistAvl.add(totalFlatLastAmount);
			    			} 
 				    	 }else{
 		    				arllistAvl.add(amount);
 		    			}
 				     }else if(!blnAvailable){
 				    	 if(promotionType!=null && strCheckPromotionType!=null) {
 				    		if(strCheckPromotionType.equalsIgnoreCase("L") || strCheckPromotionType.equalsIgnoreCase("F") || strCheckPromotionType.equalsIgnoreCase("E")){
 					    		 arllistSold.add(totalFlatLastAmount);
 			    			}
 				    	 }else{
 		    				arllistSold.add(amount);
 		    			}
 				     }
 				     accommCount++;
 				}
 			}
 			//end of accommodation
 			double totalAmount=0;
 			boolean blnJSONContent=false;
 			Collections.sort(arllist);
 			int arlListSize=0,accommId=0,intTotalAccomm=0;
 		     intTotalAccomm=accommodationList.size();
 		     if(intTotalAccomm==accommCount){
 		    	 if(intTotalAccomm==soldOutCount){
 			    	 if(blnPromoFlag){
 				    	 if(arlListPromoType.contains("F") || arlListPromoType.contains("E")){
 				    		 arlListSize=arllist.size();
 						     if(soldOutCount==arlListSize){
 						    	 Collections.sort(arllist);
 						    	 for(int i=0;i<arlListSize;i++){
 							    	 totalAmount=(Double) arllist.get(i);
 							    	 if((Double) arllist.get(0)==(Double) arllist.get(i)){
 							    		 if(blnAvailable){
 							    			 accommId=mapAmountAccommId.get(totalAmount);
 								    		 blnJSONContent=true;
 							    		 }else{
 							    			 accommId=mapSoldOutAmountAccommId.get(totalAmount);
 								    		 blnJSONContent=true;
 							    		 }
 							    		 
 							    		 break;
 							    	 }else{
 							    		 
 							    	 }
 							     }
 						     }
 				    	 }else if(arlListPromoType.contains("L")){
 				    		 /*if(promotionAccommId>0){
 					    		 accommId=promotionAccommId;
 					    		 blnJSONContent=true;
 					    	 } */
 				    		arlListSize=arllist.size();
	 					     Collections.sort(arllist);
	 				    	 for(int i=0;i<arlListSize;i++){
	 				    		 if(arllistAvl.contains((Double) arllist.get(i))){
	 				    			 if((Double) arllist.get(0)==(Double) arllist.get(i)){
	 					    			 totalAmount=(Double) arllist.get(i);
	 					    			 accommId=mapAmountAccommId.get(totalAmount);
	 						    		 blnJSONContent=true;
	 				    			 }else{
	 				    				 totalAmount=(Double) arllist.get(i);
	 					    			 accommId=mapAmountAccommId.get(totalAmount);
	 						    		 blnJSONContent=true;
	 				    			 }
	 				    			 break;
	 				    		 }
	 					     }
 				    	 }
 				     }else{
 					     arlListSize=arllist.size();
 					     if(soldOutCount==arlListSize){
 					    	 Collections.sort(arllist);
 					    	 for(int i=0;i<arlListSize;i++){
 						    	 totalAmount=(Double) arllist.get(i);
 						    	 if((Double) arllist.get(0)==(Double) arllist.get(i)){
 						    		 if(blnAvailable){
 						    			 accommId=mapAmountAccommId.get(totalAmount);
 							    		 blnJSONContent=true;
 						    		 }else{
 						    			 accommId=mapSoldOutAmountAccommId.get(totalAmount);
 							    		 blnJSONContent=true;
 						    		 }
 						    		 break;
 						    	 }else{
 						    		 
 						    	 }
 						     }
 					     }
 					     
 				     }
 			     }else{
 			    	 if(arlListPromoType.size()>0){
 			    		 if(arlListPromoType.contains("F") || arlListPromoType.contains("E")){
// 				    	 if(promotionType.equalsIgnoreCase("F") || promotionType.equalsIgnoreCase("L")){
 				    		 arlListSize=arllist.size();
 				    		 Collections.sort(arllist);
 					    	 for(int i=0;i<arlListSize;i++){
 					    		 if(arllistAvl.contains((Double) arllist.get(i))){
 					    			 if((Double) arllist.get(0)==(Double) arllist.get(i)){
 						    			 totalAmount=(Double) arllist.get(i);
 						    			 accommId=mapAmountAccommId.get(totalAmount);
 							    		 blnJSONContent=true;
 					    			 }else{
 					    				 totalAmount=(Double) arllist.get(i);
 						    			 accommId=mapAmountAccommId.get(totalAmount);
 							    		 blnJSONContent=true;
 					    			 }
 					    			 break;
 					    		 }
 						     }
 				    	 }else if( arlListPromoType.contains("L")){
 				    		 /*if(promotionAccommId>0){
 					    		 accommId=promotionAccommId;
 					    		 blnJSONContent=true;
 					    	 } */
 				    		arlListSize=arllist.size();
	 					     Collections.sort(arllist);
	 				    	 for(int i=0;i<arlListSize;i++){
	 				    		 if(arllistAvl.contains((Double) arllist.get(i))){
	 				    			 if((Double) arllist.get(0)==(Double) arllist.get(i)){
	 					    			 totalAmount=(Double) arllist.get(i);
	 					    			 accommId=mapAmountAccommId.get(totalAmount);
	 						    		 blnJSONContent=true;
	 				    			 }else{
	 				    				 totalAmount=(Double) arllist.get(i);
	 					    			 accommId=mapAmountAccommId.get(totalAmount);
	 						    		 blnJSONContent=true;
	 				    			 }
	 				    			 break;
	 				    		 }
	 					     }
 				    	 }
 				     }else{
 					     arlListSize=arllist.size();
 					     Collections.sort(arllist);
 				    	 for(int i=0;i<arlListSize;i++){
 				    		 if(arllistAvl.contains((Double) arllist.get(i))){
 				    			 if((Double) arllist.get(0)==(Double) arllist.get(i)){
 					    			 totalAmount=(Double) arllist.get(i);
 					    			 accommId=mapAmountAccommId.get(totalAmount);
 						    		 blnJSONContent=true;
 				    			 }else{
 				    				 totalAmount=(Double) arllist.get(i);
 					    			 accommId=mapAmountAccommId.get(totalAmount);
 						    		 blnJSONContent=true;
 				    			 }
 				    			 break;
 				    		 }
 					     }
 				     }
 			     }
 		     }
 		     arlListPromoType.clear();  
 			if(blnJSONContent){
 				int promoCount=0,enablePromoId=0;
 			    listAccommodationRoom.clear(); 
 			    ArrayList<String> arlPromoType=new ArrayList<String>();
 			    ArrayList<Integer> arlEnablePromo=new ArrayList<Integer>();
 		    	listAccommodationRoom=accommodationController.listPropertyTotalRooms(property.getPropertyId(), accommId);
 		    	List<DateTime> betweenAmount = DateUtil.getDateRange(start, end);
 		    	double actualPromoBaseAmount=0.0,actualBaseAmount=0.0,actualTotalAmount=0.0,actualTotalAdultAmount=0.0,actualTotalChildAmount=0.0,
 		    			actualAdultAmount=0.0,actualChildAmount=0.0,promotionFirstPercent=0.0,promotionFirstInr=0.0,
 	  				    		promotionSecondPercent=0.0,promotionSecondInr=0.0;
 			    int countDate=0,promotionId=0,promotionHours=0,promoAccommId=0; 
 			    boolean blnStatus=false,isPositive=false,isNegative=false,
 			    		promoFlag=false,blnBookedGetRooms=false,
 			    		blnLastMinutePromotions=false,blnLastMinuteIsActive=false,blnLastMinuteFirstActive=false;
 			    String promotionFirstName=null,promotionSecondName=null,promotionHoursRange=null,
 			    		promotionSecondDiscountType=null,promotionFirstDiscountType=null,strCheckPromotionType=null;
 			    Integer promoFirstPercent=null,promoFirstInr=null,promoSecondInr=null,promoSecondPercent=null;
 			    double promoBaseAmount=0.0,promoAdultAmount=0.0,promoChildAmount=0.0,smartPricePercent=0.0;
 			    double baseAmount=0.0,extraAdultAmount=0.0,extraChildAmount=0.0,totalBaseAmount=0.0,totalSmartPriceAmount=0.0,totalFlatAmount=0.0,totalLastAmount=0.0;
 			    int roomCount=0,availableCount=0,totalCount=0,discountId=0;
 			    double dblPercentCount=0.0,dblSmartPricePercent=0.0, flatAmount=0.0,lastAmount=0.0;
 			    double dblPercentFrom=0.0,dblPercentTo=0.0,lastMinuteHours=0.0;
 			    Timestamp lastMinStartDate=null,lastMinEndDate=null;
 				if(listAccommodationRoom.size()>0 && !listAccommodationRoom.isEmpty()){
 					for(AccommodationRoom roomsList:listAccommodationRoom){
 						 roomCount=(int) roomsList.getRoomCount();
 						 break;
 					 }
 				 }
 				 Timestamp tsPromoBookedDate=null,tsPromoStartDate=null,tsPromoEndDate=null;
 				 Timestamp tsFirstStartRange=null,tsSecondStartRange=null,tsFirstEndRange=null,tsSecondEndRange=null;
 				 Timestamp tsLastStartDate=null,tsLastEndDate=null;
 				 boolean blnLastMinutePromo=false,blnEarlyBirdPromo=false,blnFirstActivePromo=false,blnFirstNotActivePromo=false,basePromoFlag=false;
				 boolean blnSecondActivePromo=false,blnSecondNotActivePromo=false;
				 ArrayList<Integer> arlFirstActivePromo=new ArrayList<Integer>(); 
				 ArrayList<Integer> arlSecondActivePromo=new ArrayList<Integer>(); 
				 List<DateTime> between = DateUtil.getDateRange(start, end);
				 SimpleDateFormat format1=new SimpleDateFormat("yyyy-MM-dd");
				 java.util.Date currentdate=new java.util.Date();
				 String strCurrentDate = new SimpleDateFormat("yyyy-MM-dd").format(currentdate);
			   	 java.util.Date dtecurdate = format1.parse(strCurrentDate);
			   	 Calendar calStart=Calendar.getInstance();
			   	 calStart.setTime(dtecurdate);
			   	 calStart.setTimeZone(TimeZone.getTimeZone("IST"));
			   	 String StartDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calStart.getTime());
			   	 java.util.Date curdate = format1.parse(StartDate);
			   	 
			   	 this.displayHoursForTimer=null;
			   	 setDisplayHoursForTimer(this.displayHoursForTimer);
				 List<PmsPromotions> listPromotions= promotionController.listBookedDatePromotions(property.getPropertyId(),curdate);
				 if(listPromotions.size()>0 && !listPromotions.isEmpty()){
					 for(PmsPromotions promotions:listPromotions){
						tsPromoBookedDate=promotions.getPromotionBookedDate();
						tsPromoStartDate=promotions.getStartDate();
						tsPromoEndDate=promotions.getEndDate();
						blnEarlyBirdPromo=checkEarlyBirdPromos(tsPromoBookedDate,tsPromoStartDate,tsPromoEndDate,getArrivalDate(),getDepartureDate());
					}
				 }else{
					 blnEarlyBirdPromo=false;
				 }
				 
				 List<PmsPromotions> listLastPromotions= promotionController.listLastBookedPromotions(property.getPropertyId(),curdate);
				 if(listLastPromotions.size()>0 && !listLastPromotions.isEmpty()){
					for(PmsPromotions promotions:listLastPromotions){
						tsFirstStartRange=promotions.getFirstRangeStartDate();
						tsSecondStartRange=promotions.getSecondRangeStartDate();
						tsFirstEndRange=promotions.getFirstRangeEndDate();
						tsSecondEndRange=promotions.getSecondRangeEndDate();
						tsLastStartDate=promotions.getStartDate();
						tsLastEndDate=promotions.getEndDate();
						promotionHoursRange=promotions.getPromotionHoursRange();
						blnLastMinuteFirstActive=checkLastMinutePromos(promotionHoursRange,getArrivalDate(),getDepartureDate(),tsSecondStartRange,tsFirstEndRange,tsFirstStartRange,tsSecondEndRange);
//						blnLastMinuteIsActive=getCheckHoursRange(promotionHoursRange,getArrivalDate(),getDepartureDate());
						if(blnLastMinuteFirstActive){
							blnLastMinutePromo=true;
							enablePromoId=promotions.getPromotionId();
							arlEnablePromo.add(enablePromoId);
						}
					}
				 }else{
					 blnLastMinutePromo=false;
					 this.displayHoursForTimer=null;
			    	 setDisplayHoursForTimer(this.displayHoursForTimer);
				 }
				 
				 for (DateTime d : between)
			     {
						 if(blnEarlyBirdPromo){
							 List<PmsPromotions> listEarlyPromotions= promotionController.listBookedDatePromotions(property.getPropertyId(),curdate);
							 if(listEarlyPromotions.size()>0 && !listEarlyPromotions.isEmpty()){
								 for(PmsPromotions promos:listEarlyPromotions){
									 if(promos.getPromotionType().equalsIgnoreCase("E")){
	 									 if(arlFirstActivePromo.isEmpty()){
	 										arlFirstActivePromo.add(promos.getPromotionId());
	 									 }else if(!arlFirstActivePromo.contains(promos.getPromotionId())){
	 										arlFirstActivePromo.add(promos.getPromotionId());
	 									 }
	 								 }else{
	 									blnFirstNotActivePromo=true;
	 								 }
								 }
							 }
						 }else if(!blnEarlyBirdPromo){
							 List<PmsPromotions> listAllPromotions= promotionController.listFlatDatePromotions(property.getPropertyId(),d.toDate());
							 if(!listAllPromotions.isEmpty() && listAllPromotions.size()>0){
								 for(PmsPromotions promos:listAllPromotions){
									 if(promos.getPromotionType().equalsIgnoreCase("F")){
	 									 if(arlFirstActivePromo.isEmpty()){
	 										arlFirstActivePromo.add(promos.getPromotionId());
	 									 }else if(!arlFirstActivePromo.contains(promos.getPromotionId())){
	 										arlFirstActivePromo.add(promos.getPromotionId());
	 									 }
	 								 }else{
	 									blnFirstNotActivePromo=true;
	 								 }
								 }
							 } 
						 }else{
							 blnFirstNotActivePromo=true;
						 }
						 
						 if(blnLastMinutePromo){
	  							List<PmsPromotions> listLastPromos= promotionController.listLastBookedPromotions(property.getPropertyId(),curdate);
	  							 if(listLastPromos.size()>0 && !listLastPromos.isEmpty()){
	  								for(PmsPromotions promos:listLastPromos){
	  									if(promos.getPromotionType().equalsIgnoreCase("L")){
	  										promoAccommId=promos.getPropertyAccommodation().getAccommodationId();
	  										if(arlEnablePromo.contains(promos.getPromotionId())){
 												if(arlSecondActivePromo.isEmpty()){
	  		  										arlSecondActivePromo.add(promos.getPromotionId());
	  		  									 }else if(!arlSecondActivePromo.contains(promos.getPromotionId())){
	  		  										arlSecondActivePromo.add(promos.getPromotionId());
	  		  									 }
 											}
	  									 }
	  								}
	  							 }else{
	  								blnSecondNotActivePromo=true;
	  							 }
							}else{
								blnSecondNotActivePromo=true;
							}
				     }
				 int intPromoSize=arlFirstActivePromo.size();
				 if((arlFirstActivePromo.isEmpty() || blnFirstNotActivePromo)){
					 blnFirstActivePromo=false;
				 }else{
					 blnFirstActivePromo=true;
				 }
				 
				 if(arlSecondActivePromo.isEmpty() || blnSecondNotActivePromo){
					blnSecondActivePromo=false;
				 }else{
					 blnSecondActivePromo=true;
				 }
 				
 				List<PropertyAccommodation> listAccommodation =  accommodationManager.list(property.getPropertyId(),accommId);
 				
 				for (PropertyAccommodation accommodations : listAccommodation) {
 					for (DateTime d : betweenAmount)
 				    {
 						boolean blnFlatPromotions=false;
 						if(blnFirstActivePromo){
 							if(blnEarlyBirdPromo){
 	  							List<PmsPromotions> listFlatPromotions= promotionController.listBookedDatePromotions(property.getPropertyId(),curdate);
 	  					    	if(listFlatPromotions.size()>0 && !listFlatPromotions.isEmpty()){
 	  					    		for(PmsPromotions promotions:listFlatPromotions){
 	  					    			 if(promotions.getPromotionType().equalsIgnoreCase("E")){
 	  					    				 promotionId=promotions.getPromotionId();
 	  					    				 promotionType=promotions.getPromotionType();
 	  					    				 promotionFirstDiscountType=promotions.getPromotionDiscountType();
 	  					    				 listPromotionDetailCheck=promotionDetailController.listPromotionDetails(promotions.getPromotionId(),f.format(d.toDate()).toLowerCase());
 	  					    				 for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
 	  				    						if(promoCount==0){
 	  				    							promotionFirstPercent=promoDetails.getPromotionPercentage();
 	 	 					    					promotionFirstInr=promoDetails.getPromotionInr();
 	 	 					    					promotionType=promotions.getPromotionType();
 	  	  				    						 promotionFlag=true;
 	  	  						    				 blnPromoFlag=true;
 	  	  						    				 arlPromoType.add(promotionType);
 	  				    						}
 	  				    						promoCount++;
 	  				    					 }
 	  					    			 }
 	  					    		}
 	  					    	}
 	  						
 							}else if(!blnEarlyBirdPromo){

 	  							List<PmsPromotions> listFlatPromotions= promotionController.listFlatDatePromotions(property.getPropertyId(),d.toDate());
 	  					    	if(listFlatPromotions.size()>0 && !listFlatPromotions.isEmpty()){
 	  					    		for(PmsPromotions promotions:listFlatPromotions){
 	  					    			 if(promotions.getPromotionType().equalsIgnoreCase("F")){
 	  					    				 promotionId=promotions.getPromotionId();
 	  					    				 promotionType=promotions.getPromotionType();
 	  					    				 promotionFirstDiscountType=promotions.getPromotionDiscountType();
 	  					    				 listPromotionDetailCheck=promotionDetailController.listPromotionDetails(promotions.getPromotionId(),f.format(d.toDate()).toLowerCase());
 	  					    				 for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
 	  				    						if(promoCount==0){
 	  				    							promotionFirstPercent=promoDetails.getPromotionPercentage();
 	 	 					    					promotionFirstInr=promoDetails.getPromotionInr();
 	 	 					    					promotionType=promotions.getPromotionType();
 	  	  				    						 promotionFlag=true;
 	  	  						    				 blnPromoFlag=true;
 	  	  						    				 arlPromoType.add(promotionType);
 	  				    						}
 	  				    						promoCount++;
 	  				    					 }
 	  					    			 }
 	  					    		}
 	  					    	}
 							}
 						}
 						if(blnSecondActivePromo){

				    		List<PmsPromotions> listPmsPromotions= promotionController.listLastDatePromotions(property.getPropertyId(),accommId,curdate);
				    			 if(listPmsPromotions.size()>0 &&!listPmsPromotions.isEmpty()){
				    				 for(PmsPromotions pmsPromotions:listPmsPromotions){
				    					 promotionId=pmsPromotions.getPromotionId();
						    			 promoAccommId=pmsPromotions.getPropertyAccommodation().getAccommodationId();
						    			 promotionHoursRange=pmsPromotions.getPromotionHoursRange();
						    			 Timestamp tsStartDate=null,tsEndDate=null;
						    			 tsStartDate=pmsPromotions.getStartDate();
						    			 tsEndDate=pmsPromotions.getEndDate();
						    			 
						    			 if(arlEnablePromo.contains(pmsPromotions.getPromotionId())){
						    				 promotionAccommId=promoAccommId;
						    				 long minimumCount = getAvailableCount(property.getPropertyId(),getArrivalDate(),getDepartureDate(),roomCount,accommId);
			    			    			 if((int)minimumCount>0){
		    			    					 promotionFlag=true; 
		    			    					 blnPromoFlag=true;
		    			    					 strCheckPromotionType="L";
		    			    					 promotionType=pmsPromotions.getPromotionType();
		    			    					 arlPromoType.add(promotionType);
		    			    					 promotionSecondDiscountType=pmsPromotions.getPromotionDiscountType();
	 					    					 listPromotionDetailCheck=promotionDetailController.listPromotionDetails(pmsPromotions.getPromotionId(),f.format(curdate).toLowerCase());
	 					    					 if(listPromotionDetailCheck.size()>0 && !listPromotionDetailCheck.isEmpty()){
	 					    						for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
	    	 					    					promotionSecondPercent=promoDetails.getPromotionPercentage();
   	 	 					    					promotionSecondInr=promoDetails.getPromotionInr();
	    	 					    				 }
	 					    					 }
			    			    			 }else{
			    			    				 promotionFlag=false;
			    			    				 promotionType=null;
			    			    				 blnPromoFlag=false;
			    			    				 basePromoFlag=true;
			    			    			 }
						    			 }
				    				 }
				    			 }
 						}
 						if(promotionFlag){
 							int rateCount=0,rateIdCount=0;
   						List<PropertyRate> propertyRateList =  rateController.listAllDates(property.getPropertyId(),accommodations.getAccommodationId(),getSourceTypeId(),d.toDate());
   				    	if(propertyRateList.size()>0)
   				    	{
   				    		 for (PropertyRate pr : propertyRateList)
   			    			 {
				    				 int propertyRateId = pr.getPropertyRateId();
					    			 List<PropertyRateDetail> rateDetailList =   detailController.list(propertyRateId,f.format(d.toDate()).toLowerCase());
       				    		 if(rateDetailList.size()>0){
       				    			 if(rateCount<1){
       				    				 for (PropertyRateDetail rate : rateDetailList){
       				    					 baseAmount+=  rate.getBaseAmount();
       				    					 extraAdultAmount+=rate.getExtraAdult();
       	  				    				 extraChildAmount+=rate.getExtraChild();
	        				    			 }
       				    				 rateCount++;
       				    			 }
       				    		 }else{
       				    			 if(propertyRateList.size()==rateIdCount){
       				    				 baseAmount += accommodations.getBaseAmount();
       				    				 extraAdultAmount+=accommodations.getExtraAdult();
         				    				 extraChildAmount+=accommodations.getExtraChild();
       				    			 }
           				    	 }
   			    			 }	    		 
   				    	 }
   				    	 else{
   				    		baseAmount += accommodations.getBaseAmount();
   				    		extraAdultAmount+=accommodations.getExtraAdult();
			    				extraChildAmount+=accommodations.getExtraChild();
   				    	 }
   				    	double discountAmount=0;
				    		 if(blnFirstActivePromo){
 	    				    	  String strTypePercent=String.valueOf(promotionFirstPercent);
 	    				    	  String strTypeInr=String.valueOf(promotionFirstInr);
					    		  if(!strTypePercent.equalsIgnoreCase("null") && promotionFirstPercent!=0){
					    			  actualPromoBaseAmount = baseAmount-(baseAmount*promotionFirstPercent/100);
					    			  discountAmount+=baseAmount*promotionFirstPercent/100;
					    		  }
					    		  else if(!strTypeInr.equalsIgnoreCase("null") && promotionFirstInr!=0){
					    			  actualPromoBaseAmount = baseAmount-promotionFirstInr;
					    			  discountAmount+=promotionFirstInr;
					    		  }
					    		  else{
					    			  actualPromoBaseAmount = baseAmount;
					    		  } 
				    		 }else{
				    			  actualPromoBaseAmount = baseAmount;
				    		  } 
				    		 if(blnSecondActivePromo){
				    			String strTypePercent=String.valueOf(promotionSecondPercent);
 	    				    	String strTypeInr=String.valueOf(promotionSecondInr);
					    		  if(!strTypePercent.equalsIgnoreCase("null") && promotionSecondPercent!=0){
					    			  actualPromoBaseAmount = actualPromoBaseAmount-(actualPromoBaseAmount*promotionSecondPercent/100);
					    			  discountAmount+=baseAmount*promotionSecondPercent/100;
					    		  }
					    		  else if(!strTypeInr.equalsIgnoreCase("null") && promotionSecondInr!=0){
					    			  actualPromoBaseAmount= actualPromoBaseAmount-promotionSecondInr;
					    			  discountAmount+=promotionSecondInr;
					    		  }
					    		  else{
					    			  actualPromoBaseAmount = actualPromoBaseAmount;
					    		  }
				    		 }else{
				    			  actualPromoBaseAmount = actualPromoBaseAmount;
				    		  }
				    		actualBaseAmount=baseAmount;
				    		totalFlatAmount=baseAmount-discountAmount;
				    		 
				    	 }else{
				    		 if(basePromoFlag){
				    			baseAmount += accommodations.getBaseAmount();
				    			extraAdultAmount+=accommodations.getExtraAdult();
				    			extraChildAmount+=accommodations.getExtraChild();
				    		}else{
				    			int rateCount=0,rateIdCount=0;
	    						List<PropertyRate> propertyRateList =  rateController.listAllDates(property.getPropertyId(),accommodations.getAccommodationId(),getSourceTypeId(),d.toDate());
	    				    	if(propertyRateList.size()>0)
	    				    	{
	    				    		 for (PropertyRate pr : propertyRateList)
	    			    			 {
					    				 int propertyRateId = pr.getPropertyRateId();
						    			 List<PropertyRateDetail> rateDetailList =   detailController.list(propertyRateId,f.format(d.toDate()).toLowerCase());
	        				    		 if(rateDetailList.size()>0){
	        				    			 if(rateCount<1){
	        				    				 for (PropertyRateDetail rate : rateDetailList){
	        				    					 baseAmount +=  rate.getBaseAmount();
	        				    					 extraAdultAmount+=rate.getExtraAdult();
	        	  				    				 extraChildAmount+=rate.getExtraChild();
		        				    			 }
	        				    				 rateCount++;
	        				    			 }
	        				    		 }else{
	        				    			 if(propertyRateList.size()==rateIdCount){
	        				    				 baseAmount += accommodations.getBaseAmount();
	        				    				 extraAdultAmount+=accommodations.getExtraAdult();
	          				    				 extraChildAmount+=accommodations.getExtraChild();
	        				    			 }
	            				    	 }
	    			    			 }	    		 
	    				    	 }
	    				    	 else{
	    				    		baseAmount += accommodations.getBaseAmount();
	    				    		extraAdultAmount+=accommodations.getExtraAdult();
				    				extraChildAmount+=accommodations.getExtraChild();
	    				    	 }
				    		}
				    	 }
 						 actualBaseAmount=baseAmount;
 				    	 countDate++;
 				    }
 					Iterator<String> iterPromoType=arlPromoType.iterator();
 					while(iterPromoType.hasNext()){
 						String strPromoType=iterPromoType.next();
 						if(strPromoType.equalsIgnoreCase("F") || strPromoType.equalsIgnoreCase("E")){
 							String strTypePercent=String.valueOf(promotionFirstPercent);
   				    	String strTypeInr=String.valueOf(promotionFirstInr);
				    		if(!strTypePercent.equalsIgnoreCase("null") && promotionFirstPercent!=0){
				    			promotionFirstName=String.valueOf(Math.round(promotionFirstPercent))+" % OFF";
	  							promoFirstPercent=(int) Math.round(promotionFirstPercent);
				    		}
				    		else if(!strTypeInr.equalsIgnoreCase("null") && promotionFirstInr!=0){
				    			promotionFirstName="Flat "+String.valueOf(Math.round(promotionFirstInr))+" OFF";
	  							promoFirstInr=(int) Math.round(promotionFirstInr);
				    		}
 							
 						}
 						if(strPromoType.equalsIgnoreCase("L")){
 							  String strTypePercent=String.valueOf(promotionSecondPercent);
 							  String strTypeInr=String.valueOf(promotionSecondInr);
				    		  if(!strTypePercent.equalsIgnoreCase("null") && promotionSecondPercent!=0){
				    			  promotionSecondName=String.valueOf(Math.round(promotionSecondPercent))+" % OFF";
		  						  promoSecondPercent=(int) Math.round(promotionSecondPercent);
				    		  }
				    		  else if(!strTypeInr.equalsIgnoreCase("null") && promotionSecondInr!=0){
				    			  promotionSecondName="Flat "+String.valueOf(Math.round(promotionSecondInr))+" OFF";
		  						  promoSecondInr=(int) Math.round(promotionSecondInr);
				    		  }
 							
 						}
 					}
 					arlPromoType.clear();
 				
 					if(blnPromoFlag){
 						if(promotionType.equalsIgnoreCase("L") || promotionType.equalsIgnoreCase("E") || promotionType.equalsIgnoreCase("F")){
 							totalAmount=totalFlatAmount;
 							
 							if(countDate>1){
 								actualAdultAmount=extraAdultAmount/countDate;
 								actualChildAmount=extraChildAmount/countDate;
 							}else{
 								actualAdultAmount=extraAdultAmount;
 								actualChildAmount=extraChildAmount;
 							}
 						}
 					}else{
 						totalAmount=baseAmount;
 						
 						if(countDate>1){
 							actualAdultAmount=extraAdultAmount/countDate;
 							actualChildAmount=extraChildAmount/countDate;
 						}else{
 							actualAdultAmount=extraAdultAmount;
 							actualChildAmount=extraChildAmount;
 						}
 					}
 					
 					
 					totalAmount=Math.round(totalAmount);
 					actualTotalAdultAmount=Math.round(actualAdultAmount);
 					actualTotalChildAmount=Math.round(actualChildAmount);
 					
 	            	
 	            	
 					long minimum = getAvailableCount(property.getPropertyId(),getArrivalDate(),getDepartureDate(),roomCount,accommId);
 			    	 
 			    	 if(minimum==0){
 			    		 jsonOutput += ",\"available\":\"" + minimum+ "\"";
 			    		 jsonOutput += ",\"baseActualAmount\":\"" + + (actualBaseAmount==0 ? accommodations.getBaseAmount() : actualBaseAmount )+  "\"";
 			    		 jsonOutput += ",\"baseAmount\":\"" + + (totalAmount==0 ? accommodations.getBaseAmount() : totalAmount )+  "\"";
 			    		 jsonOutput += ",\"totalAmount\":\"" + + (totalAmount==0 ? accommodations.getBaseAmount() : totalAmount )+  "\"";
 			    		 jsonOutput += ",\"accommodationName\":\"" + accommodations.getAccommodationType().toUpperCase()+ "\"";
 						 jsonOutput += ",\"accommodationId\":\"" + accommodations.getAccommodationId()+ "\"";
 						 jsonOutput += ",\"promotionFirstName\":\"" + (promotionFirstName==null? "None":promotionFirstName)+ "\"";
 						 jsonOutput += ",\"promotionSecondName\":\"" + (promotionSecondName==null? "None":promotionSecondName)+ "\"";
 						 jsonOutput += ",\"promoFirstPercent\":\"" + (promoFirstPercent==null? 0:promoFirstPercent)+ "\"";
 						 jsonOutput += ",\"promoFirstInr\":\"" + (promoFirstInr==null? 0:promoFirstInr)+ "\"";
						 jsonOutput += ",\"promoSecondInr\":\"" + (promoSecondInr==null? 0:promoSecondInr)+ "\"";
 						 jsonOutput += ",\"promoSecondPercent\":\"" + (promoSecondPercent==null? 0:promoSecondPercent)+ "\"";	
 						jsonOutput += ",\"promoLastMinuteTimer\":\"" + (getDisplayHoursForTimer()==null ?"None" :getDisplayHoursForTimer()  )+ "\"";
 				     }else{
 				    	jsonOutput += ",\"accommodationName\":\"" + accommodations.getAccommodationType().toUpperCase()+ "\"";
 						jsonOutput += ",\"accommodationId\":\"" + accommodations.getAccommodationId()+ "\"";
 			    		jsonOutput += ",\"available\":\"" + minimum+ "\"";
 			    		jsonOutput += ",\"maxAdult\":\"" + accommodations.getMaxOccupancy()+ "\"";
 			    		jsonOutput += ",\"maxChild\":\"" + 0 + "\"";
 			    		jsonOutput += ",\"minOccupancy\":\"" + accommodations.getMinOccupancy() + "\"";
 						jsonOutput += ",\"maxOccupancy\":\"" + accommodations.getMaxOccupancy() + "\"";
 						jsonOutput += ",\"baseActualAmount\":\"" +  (actualBaseAmount==0 ? accommodations.getBaseAmount() : actualBaseAmount )+  "\"";
 						jsonOutput += ",\"promoFirstPercent\":\"" + (promoFirstPercent==null? 0:promoFirstPercent)+ "\"";
 						jsonOutput += ",\"promoFirstInr\":\"" + (promoFirstInr==null? 0:promoFirstInr)+ "\"";
						jsonOutput += ",\"promoSecondInr\":\"" + (promoSecondInr==null? 0:promoSecondInr)+ "\"";
 						jsonOutput += ",\"promoSecondPercent\":\"" + (promoSecondPercent==null? 0:promoSecondPercent)+ "\"";
 						jsonOutput += ",\"baseAmount\":\"" + + (totalAmount==0 ? accommodations.getBaseAmount() : totalAmount )+  "\"";
 			    		jsonOutput += ",\"totalAmount\":\"" + + (totalAmount==0 ? accommodations.getBaseAmount() : totalAmount )+  "\"";
 			    		jsonOutput += ",\"extraAdultAmount\":\"" + + (actualTotalAdultAmount==0 ? accommodations.getExtraAdult() : actualTotalAdultAmount )+  "\"";
 			    		jsonOutput += ",\"extraChildAmount\":\"" + + (actualTotalChildAmount==0 ? accommodations.getExtraChild() : actualTotalChildAmount )+  "\"";
 			    		jsonOutput += ",\"promotionFirstName\":\"" + (promotionFirstName==null? "None":promotionFirstName)+ "\"";
						jsonOutput += ",\"promotionSecondName\":\"" + (promotionSecondName==null? "None":promotionSecondName)+ "\"";
 			    		jsonOutput += ",\"promotionId\":\"" + (promotionId==0? 0:promotionId) + "\"";
 			    		jsonOutput += ",\"promoLastMinuteTimer\":\"" + (getDisplayHoursForTimer()==null ?"None" :getDisplayHoursForTimer()  )+ "\"";
 						discountId=35;
 						PropertyDiscountManager discountController=new PropertyDiscountManager();
 						PropertyDiscount propertyDiscount=discountController.find(discountId);
 			    		jsonOutput += ",\"discountPercent\":\"" + (propertyDiscount.getDiscountPercentage()==0.0? "0":propertyDiscount.getDiscountPercentage())+ "\"";
 			    		jsonOutput += ",\"discountId\":\"" + (discountId==0? "0" :discountId) + "\"";
 			    		
 			    		
 				     }
 				}
 		     
 			}
 			jsonOutput += "}";
 			
 			arllist.clear();
 		
		}
	 
       
		response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
//		sessionMap.clear();
		

	} catch (Exception e) {
		logger.error(e);
		e.printStackTrace();
	} finally {

	}

	return null;


   }
   
   public String getNewLocationProperties(){


		try {
		
	 /* SeoContentManager seoContentController = new SeoContentManager();
	  SeoContent seoContent = seoContentController.findSeoContent(getLocationId());	
	        
	  request.setAttribute("metaTag" ,seoContent.getTitle());
	  request.setAttribute("description" ,seoContent.getDescription());
	  request.setAttribute("content" ,seoContent.getContent());*/

			
		Map mapDate=new HashMap();
		
		DateFormat format = new SimpleDateFormat("MMM d,yyyy", Locale.ENGLISH);
	    java.util.Date dateStart = format.parse((String)request.getAttribute("StartDate"));
	    java.util.Date dateEnd = format.parse((String)request.getAttribute("EndDate"));
	   
	    if(getSourceTypeId()==null){
	    	this.sourceTypeId=1;
	    }
	    
	    Calendar calCheckStart=Calendar.getInstance();
	    calCheckStart.setTime(dateStart);
	    java.util.Date checkInDate = calCheckStart.getTime();
	    this.arrivalDate=new Timestamp(checkInDate.getTime());
	   
	    
	    Calendar calCheckEnd=Calendar.getInstance();
	    calCheckEnd.setTime(dateEnd);
	    java.util.Date checkOutDate = calCheckEnd.getTime();
	    this.departureDate=new Timestamp(checkOutDate.getTime());
	    
	    String checkIn = new SimpleDateFormat("MMM d,yyyy").format(checkInDate);
      String checkOut = new SimpleDateFormat("MMM d,yyyy").format(checkOutDate);
      String checkIn1 = new SimpleDateFormat("dd MMM").format(checkInDate);
      String checkOut1 = new SimpleDateFormat("dd MMM").format(checkOutDate);
      
      sessionMap.put("checkIn",checkIn);
      sessionMap.put("checkOut",checkOut);
      sessionMap.put("checkIn1",checkIn1);
      sessionMap.put("checkOut1",checkOut1);
      
		/*this.arrivalDate = getArrivalDate();
	    sessionMap.put("arrivalDate",arrivalDate); 
		
		this.departureDate = getDepartureDate();
		sessionMap.put("departureDate",departureDate); */
		mapDate.put("arrivalDate", arrivalDate);
		mapDate.put("departureDate", departureDate);
		
		PmsPropertyManager propertyController = new PmsPropertyManager();		
		PropertyAccommodationManager accommodationManager = new PropertyAccommodationManager();
		PropertyAmenityManager amenityController = new PropertyAmenityManager();
		List<PropertyAmenity> propertyAmenityList;
		PropertyTypeManager typeController =  new PropertyTypeManager();
		PropertyRateManager rateController = new PropertyRateManager();
		PropertyRateDetailManager detailController = new PropertyRateDetailManager();
		PmsAmenityManager ameController = new PmsAmenityManager();
		PromotionManager promotionController=new PromotionManager();
		PromotionDetailManager promotionDetailController=new PromotionDetailManager();
		PmsBookingManager bookingController=new PmsBookingManager();
		PropertyRatePlanDetailManager planDetailController=new PropertyRatePlanDetailManager();
		
		HashMap<Double,Integer > mapAmountAccommId=new HashMap<Double, Integer>();
		HashMap<Double,Integer > mapSoldOutAmountAccommId=new HashMap<Double, Integer>();
		
		ArrayList<Integer> arlSortPropertyId=new ArrayList<Integer>();
		HashMap<Integer, Integer> mapAvailablePropertyId=new HashMap<Integer, Integer>();
		String jsonOutput = "";
		HttpServletResponse response = ServletActionContext.getResponse();
		
		response.setContentType("application/json");
		
		this.arrivalDate = getArrivalDate();
	    sessionMap.put("arrivalDate",arrivalDate); 
			
		this.departureDate = getDepartureDate();
		sessionMap.put("departureDate",departureDate); 
		 
		Calendar cal = Calendar.getInstance();
		cal.setTime(this.departureDate);
	    cal.add(Calendar.DATE, -1);
	    
	    java.util.Date dteCheckOut = cal.getTime();
	    this.departureDate=new Timestamp(dteCheckOut.getTime());
	    sessionMap.put("endDate",departureDate); 
	    
		String startDate = new SimpleDateFormat("yyyy-MM-dd").format(this.arrivalDate);
		String endDate = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
		DateTime start = DateTime.parse(startDate);
	    DateTime end = DateTime.parse(endDate);
	    DecimalFormat df = new DecimalFormat("###.##");
	    DateFormat f = new SimpleDateFormat("EEEE");
	    PropertyAccommodationInventoryManager inventoryController= new PropertyAccommodationInventoryManager();
	    List<PmsSmartPrice> listSmartPriceDetail=null; 
	    List<AccommodationRoom> listAccommodationRoom=null;
		PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
		PropertyReviewsManager reviewController=new PropertyReviewsManager();
	    int availRoomCount=0,soldRoomCount=0;
	    this.sourceTypeId=1;
	    this.propertyList = propertyController.listPropertyByLocation(getLocationId());
		for (PmsProperty property : propertyList) {
			
			
			
			if (!jsonOutput.equalsIgnoreCase(""))
				jsonOutput += ",{";
			else
				jsonOutput += "{";
			jsonOutput += "\"DT_RowId\":\"" + property.getPropertyId()+ "\"";
			jsonOutput += ",\"city\":\"" + property.getCity() + "\"";
			jsonOutput += ",\"propertyTypeId\":\"" + property.getPropertyType().getPropertyTypeId() + "\"";
			jsonOutput += ",\"propertyTypeName\":\"" + property.getPropertyType().getPropertyTypeName() + "\"";
			jsonOutput += ",\"propertyName\":\"" + (property.getPropertyName() == null ? "": property.getPropertyName().toUpperCase().trim())+ "\"";
			jsonOutput += ",\"propertyUrl\":\"" + (property.getPropertyUrl() == null ? "": property.getPropertyUrl().toLowerCase().trim())+ "\"";
			
			if(property.getLocation()!=null){
				jsonOutput += ",\"locationName\":\"" + (property.getLocation().getLocationName() == null ? "": property.getLocation().getLocationName().toUpperCase().trim())+ "\"";	
			}else{
				jsonOutput += ",\"locationName\":\"" + "" + "\"";
			}
			
//			jsonOutput += ",\"propertyStandardPolicy\":\"" + (property.getPropertyStandardPolicy() == null ? "None": property.getPropertyStandardPolicy().trim())+ "\"";
//			jsonOutput += ",\"propertyHotelPolicy\":\"" + (property.getPropertyHotelPolicy() == null ? "None": property.getPropertyHotelPolicy().trim())+ "\"";
//			jsonOutput += ",\"propertyCancellationPolicy\":\"" + (property.getPropertyCancellationPolicy() == null ? "None": property.getPropertyCancellationPolicy().trim())+ "\"";
			jsonOutput += ",\"payAtHotelStatus\":\"" + (property.getPayAtHotelStatus() == null ? false: property.getPayAtHotelStatus())+ "\"";
			jsonOutput += ",\"coupleStatus\":\"" + ( property.getCoupleIsActive() == null? false :  property.getCoupleIsActive() )+ "\"";
			jsonOutput += ",\"breakfastStatus\":\"" + ( property.getBreakfastIsActive() == null? false:  property.getBreakfastIsActive() )+ "\"";
			jsonOutput += ",\"hotelStatus\":\"" + ( property.getHotelIsActive() == null? false:  property.getHotelIsActive() )+ "\"";
			
			SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
			Timestamp FromDate=(Timestamp)mapDate.get("arrivalDate");
			Timestamp ToDate=(Timestamp)mapDate.get("departureDate");
			jsonOutput += ",\"arrivalDate\":\"" + format2.format(FromDate) + "\"";
			jsonOutput += ",\"departureDate\":\"" + format2.format(ToDate)+ "\"";
			
			String strFromDate=format2.format(FromDate);
			String strToDate=format2.format(ToDate);
	    	DateTime startdate = DateTime.parse(strFromDate);
	        DateTime enddate = DateTime.parse(strToDate);
	        java.util.Date fromdate = startdate.toDate();
	 		java.util.Date todate = enddate.toDate();
	 		 
			int diffInDays = (int) ((todate.getTime() - fromdate.getTime()) / (1000 * 60 * 60 * 24));
			
			List arllist=new ArrayList();
			List arllistAvl=new ArrayList();
			List arllistSold=new ArrayList();
			jsonOutput += ",\"diffDays\":\"" + diffInDays+ "\"";
			
			String jsonPhotos="";
 			PropertyPhotoManager photoController = new PropertyPhotoManager();
			
		    response.setContentType("application/json");
		    List<PropertyPhoto> listPhotos =photoController.list(property.getPropertyId());
           
           if(listPhotos.size()>0)
 			{
           	for (PropertyPhoto photo : listPhotos) {

   				if (!jsonPhotos.equalsIgnoreCase(""))
   					jsonPhotos += ",{";
   				else
   					jsonPhotos += "{";
   				jsonPhotos += "\"DT_RowId\":\"" + photo.getPropertyPhotoId() + "\"";
   				jsonPhotos += ",\"isPrimary\":\"" + photo.getIsPrimary()+ "\"";
   				jsonPhotos += ",\"photoPath\":\"" + photo.getPhotoPath()+ "\"";
   				
   				
   				jsonPhotos += "}";


   			}
   			
                  jsonOutput += ",\"photos\":[" + jsonPhotos+ "]";
 			
 			}
			
			String jsonAmenities ="";
			propertyAmenityList =  amenityController.list(property.getPropertyId());
			if(propertyAmenityList.size()>0)
			{
				for (PropertyAmenity amenity : propertyAmenityList) {
					if (!jsonAmenities.equalsIgnoreCase(""))
						
						jsonAmenities += ",{";
					else
						jsonAmenities += "{";
					PropertyAmenity amentity1 = amenityController.find(amenity.getPropertyAmenityId());
				    String path = "ulowebsite/images/icons/";
					PmsAmenity pmsAmenity1 =ameController.find(amentity1.getPmsAmenity().getAmenityId());
					jsonAmenities += "\"DT_RowId\":\"" + amenity.getPropertyAmenityId()+ "\"";					
					jsonAmenities += ",\"amenityName\":\"" + pmsAmenity1.getAmenityName()+ "\"";
					jsonAmenities += ",\"amenityId\":\"" + pmsAmenity1.getAmenityId()+ "\"";
                    jsonAmenities += ",\"icon\":\"" + path + pmsAmenity1.getAmenityIcon()+ "\"";
             
					
					jsonAmenities += "}";
				}
				
				jsonOutput += ",\"amenities\":[" + jsonAmenities+ "]";
			}
			
			String jsonReviews="";
			List<PropertyReviews> listReviews=reviewController.listReview(property.getPropertyId());
			if(listReviews.size()>0 && !listReviews.isEmpty()){
				for(PropertyReviews reviews:listReviews){
					
					if (!jsonReviews.equalsIgnoreCase(""))
						
						jsonReviews += ",{";
					else
						jsonReviews += "{";

					/*jsonReviews += "\"starCount\":\"" + reviews.getStarCount().intValue()+ "\"";	
					jsonReviews += ",\"reviewCount\":\"" + reviews.getReviewCount().intValue()+ "\"";
					Double averageReviews=reviews.getAverageReview();
					jsonReviews += ",\"averageReview\":\"" + df.format(averageReviews)+ "\"";*/
					
					jsonReviews += "\"starCount\":\"" + (reviews.getStarCount()==null?0:reviews.getStarCount().intValue())+ "\"";	
					jsonReviews += ",\"reviewCount\":\"" + (reviews.getReviewCount()==null?0:reviews.getReviewCount().intValue())+ "\"";	
					if(reviews.getAverageReview()!=null){
						Double averageReviews=reviews.getAverageReview();
						jsonReviews += ",\"averageReview\":\"" + df.format(averageReviews)+ "\"";
					}else{
						jsonReviews += ",\"averageReview\":\"" + df.format(0)+ "\"";
					}
					
					jsonReviews += ",\"tripadvisorStarCount\":\"" + (reviews.getTripadvisorStarCount()==null?0:reviews.getTripadvisorStarCount().intValue())+ "\"";	
					jsonReviews += ",\"tripadvisorReviewCount\":\"" + (reviews.getTripadvisorReviewCount()==null?0:reviews.getTripadvisorReviewCount().intValue())+ "\"";
					if(reviews.getTripadvisorAverageReview()!=null){
						Double taAverageReviews=reviews.getTripadvisorAverageReview();
						jsonReviews += ",\"tripadvisorAverageReview\":\"" + df.format(taAverageReviews)+ "\"";
					}else{
						jsonReviews += ",\"tripadvisorAverageReview\":\"" + df.format(0)+ "\"";
					}
					jsonReviews += ",\"googleReviewUrl\":\"" + (reviews.getGoogleReviewUrl()==null?"":reviews.getGoogleReviewUrl())+ "\"";	
					jsonReviews += ",\"tripadvisorReviewUrl\":\"" + (reviews.getTripadvisorReviewUrl()==null?"":reviews.getTripadvisorReviewUrl())+ "\"";	
					jsonReviews += ",\"propertyRating\":\"" + (reviews.getPropertyRating()==null?"None":reviews.getPropertyRating())+ "\"";
					jsonReviews += "}";
				}
				
			}else{
				if (!jsonReviews.equalsIgnoreCase(""))
					
					jsonReviews += ",{";
				else
					jsonReviews += "{";
				
				jsonReviews += "\"starCount\":\"" + 0+ "\"";	
				jsonReviews += ",\"reviewCount\":\"" + 0+ "\"";	
				jsonReviews += ",\"averageReview\":\"" + 0+ "\"";
				
				jsonReviews += ",\"tripadvisorStarCount\":\"" + 0 + "\"";	
				jsonReviews += ",\"tripadvisorReviewCount\":\"" + 0 + "\"";	
				jsonReviews += ",\"tripadvisorAverageReview\":\"" + 0+ "\"";
				
				jsonReviews += ",\"googleReviewUrl\":\"" + "None"+ "\"";	
				jsonReviews += ",\"tripadvisorReviewUrl\":\"" + "None"+ "\"";	
				jsonReviews += ",\"propertyRating\":\"" + "None"+ "\"";
				
				jsonReviews += "}";
			}
			
			jsonOutput += ",\"reviews\":[" + jsonReviews+ "]";
			
			
			String jsonAccommodation ="";
			List<PropertyAccommodation> accommodationsList =  accommodationManager.list(property.getPropertyId());
			int soldOutCount=0,availCount=0,promotionAccommId=0;
			if(accommodationsList.size()>0 && !accommodationsList.isEmpty())
			{
				for (PropertyAccommodation accommodation : accommodationsList) {
					int roomCount=0;
					listAccommodationRoom=accommodationController.listPropertyTotalRooms(property.getPropertyId(), accommodation.getAccommodationId());
					if(listAccommodationRoom.size()>0 && !listAccommodationRoom.isEmpty()){
						for(AccommodationRoom roomsList:listAccommodationRoom){
							 roomCount=(int) roomsList.getRoomCount();
							 long minimum = getAvailableCount(property.getPropertyId(),getArrivalDate(),getDepartureDate(),roomCount,accommodation.getAccommodationId());
							 if((int)minimum>0){
								 availCount++;
							 }else if((int)minimum==0){
								 soldOutCount++;
							 }
							 break;
						 }
					 }
					
					
					if (!jsonAccommodation.equalsIgnoreCase(""))
						
						jsonAccommodation += ",{";
					else
						jsonAccommodation += "{";
						jsonAccommodation += "\"accommodationId\":\"" + accommodation.getAccommodationId()+ "\"";
						jsonAccommodation += ",\"accommodationName\":\"" + accommodation.getAccommodationType().toUpperCase()+ "\"";
						jsonAccommodation += ",\"noOfAdult\":\"" + accommodation.getNoOfAdults()+ "\"";
						jsonAccommodation += ",\"noOfChild\":\"" + accommodation.getNoOfChild()+ "\"";
						jsonAccommodation += ",\"maxOccupancy\":\"" + accommodation.getMaxOccupancy()+ "\"";
						jsonAccommodation += ",\"minOccupancy\":\"" + accommodation.getMinOccupancy()+ "\"";
					jsonAccommodation += "}";
				}
				
				jsonOutput += ",\"accommodations\":[" + jsonAccommodation+ "]";
			}

			
			List<PmsPromotionDetails> listPromotionDetailCheck=null;
			Boolean blnAvailable=false,blnSoldout=false,firstPromotionFlag=false,secondPromotionFlag=false,promotionFlag=false,blnPromoFlag=false;
			int arlListTotalAccomm=0,soldOutTotalAccomm=0,accommCount=0;
			String promotionType=null;
			ArrayList<String> arlListPromoType=new ArrayList<String>();
			List<PropertyAccommodation> accommodationList =  accommodationManager.list(property.getPropertyId());
			if(accommodationList.size()>0)
			{
				for (PropertyAccommodation accommodation : accommodationList) {
					
					int dateCount=0;
					double amount=0.0,extraAdultAmount=0.0,extraChildAmount=0.0;
					int roomCount=0,availableCount=0,totalCount=0;
				    double dblPercentCount=0.0;
	    			listAccommodationRoom=accommodationController.listPropertyTotalRooms(property.getPropertyId(), accommodation.getAccommodationId());
	    			List<DateTime> between = DateUtil.getDateRange(start, end); 
					if(listAccommodationRoom.size()>0 && !listAccommodationRoom.isEmpty()){
						
						for(AccommodationRoom roomsList:listAccommodationRoom){
							roomCount=(int) roomsList.getRoomCount();
							if(roomCount>0){
								long minimum = getAvailableCount(property.getPropertyId(),getArrivalDate(),getDepartureDate(),roomCount,accommodation.getAccommodationId());
								if((int)minimum>0){
									blnAvailable=true;
									blnSoldout=false;
									arlListTotalAccomm++;
								}else if((int)minimum==0){
									blnSoldout=true;
									blnAvailable=false;
									soldOutTotalAccomm++;
								}
							}
							 break;
						 }
					 }
				     for (DateTime d : between)
				     {
				    	 long rmc = 0;
			   			 this.roomCnt = rmc;
			        	 int sold=0;
			        	 java.util.Date availDate = d.toDate();
						 PropertyAccommodationInventory inventoryCount = inventoryController.findInventoryCount(availDate,accommodation.getAccommodationId());
						 if(inventoryCount != null){
						 PmsAvailableRooms roomCount1 = bookingController.findBookingCount(availDate, accommodation.getAccommodationId(),inventoryCount.getInventoryId());
						 if(roomCount1.getRoomCount() == null) {
						 	String num1 = inventoryCount.getInventoryCount();
							PmsAvailableRooms bookingRoomCount=bookingController.findCount(availDate, accommodation.getAccommodationId());
							PropertyAccommodation accomm=accommodationController.find(accommodation.getAccommodationId());
							Integer totavailable=0;
							if(accommodation!=null){
								totavailable=accommodation.getNoOfUnits();
							}
							if(bookingRoomCount.getRoomCount()!=null){
								long lngRooms=bookingRoomCount.getRoomCount();
								long num2 = Long.parseLong(num1);
								this.roomCnt = num2;
								sold=(int) (lngRooms);
								int availRooms=0;
								availRooms=totavailable-sold;
								if(Integer.parseInt(num1)>availRooms){
									this.roomCnt = availRooms;	
								}else{
									this.roomCnt = num2;
								}
										
							}else{
								 long num2 = Long.parseLong(num1);
								 this.roomCnt = num2;
								 sold=(int) (rmc);
							}
						}else{		
							PmsAvailableRooms bookingRoomCount=bookingController.findCount(availDate, accommodation.getAccommodationId());
							int intRooms=0;
							Integer totavailable=0,availableRooms=0;
							if(accommodation!=null){
								totavailable=accommodation.getNoOfUnits();
							}
							if(bookingRoomCount.getRoomCount()!=null){
								long lngRooms=bookingRoomCount.getRoomCount();
								long num = roomCount1.getRoomCount();	
								intRooms=(int)lngRooms;
								String num1 = inventoryCount.getInventoryCount();
			 					long num2 = Long.parseLong(num1);
			 					availableRooms=totavailable-intRooms;
			 					if(num2>availableRooms){
			 						this.roomCnt = availableRooms;	 
			 					}else{
			 						this.roomCnt = num2-num;
			 					}
			 					long lngSold=bookingRoomCount.getRoomCount();
			   					sold=(int)lngSold;
							}else{
								long num = roomCount1.getRoomCount();	
								String num1 = inventoryCount.getInventoryCount();
								long num2 = Long.parseLong(num1);
			  					this.roomCnt = (num2-num);
			  					long lngSold=roomCount1.getRoomCount();
			   					sold=(int)lngSold;
							}
		
						}
										
					}else{
						  PmsAvailableRooms inventoryCounts = bookingController.findCount(availDate, accommodation.getAccommodationId());							
							if(inventoryCounts.getRoomCount() == null){								
								this.roomCnt = (accommodation.getNoOfUnits()-rmc);
								sold=(int)rmc;
							}
							else{
								this.roomCnt = (accommodation.getNoOfUnits()-inventoryCounts.getRoomCount());	
								long lngSold=inventoryCounts.getRoomCount();
	   							sold=(int)lngSold;
							}
					}
										
					double totalRooms=0,intSold=0;
					totalRooms=(double)accommodation.getNoOfUnits();
					intSold=(double)sold;
					double occupancy=0.0;
					occupancy=intSold/totalRooms;
					double sellrate=0.0,otarate=0.0;
					Integer occupancyRating=(int) Math.round(occupancy * 100);
						    int rateCount=0,rateIdCount=0;
						    double minAmount=0,maxAmount=0;
						    List<PropertyRate> dateList =  rateController.listAllDates(property.getPropertyId(),accommodation.getAccommodationId(),this.sourceTypeId,d.toDate());
							if(dateList.size()>0 && !dateList.isEmpty()){
								for(PropertyRate rates : dateList) {
									int propertyRateId = rates.getPropertyRateId();
									List<PropertyRateDetail> rateDetailList =   detailController.listMinMaxRateDetail(propertyRateId,f.format(d.toDate()).toLowerCase());
	   				    		 	if(rateDetailList.size()>0 && !rateDetailList.isEmpty()){
	   				    		 		if(rateCount<1){
	   				    		 			for (PropertyRateDetail rate : rateDetailList){
		   				    					 minAmount=rate.getMinimumBaseAmount();
		   				    					 maxAmount=rate.getMaximumBaseAmount();
		   				    				    if(occupancyRating==0){
			   				 						sellrate = minAmount;
			   				 					}else if(occupancyRating>0 && occupancyRating<=30){
			   				 			        	sellrate = minAmount;
			   				 			        }else if(occupancyRating>=31 && occupancyRating<=60){
			   				 			        	sellrate = minAmount+(minAmount*10/100);
			   				 			        }else if(occupancyRating>=61 && occupancyRating<=80){
			   				 			        	sellrate = minAmount+(minAmount*15/100);
			   				 			        }else if(occupancyRating>=81 && occupancyRating<=100){
			   				 			        	sellrate = maxAmount;
			   				 			        }
		   				    					 amount+=  sellrate;
	   				    		 			}
	   				    		 			rateCount++;
	   				    		 			rateIdCount++;
	   				    		 		}
	   				    		 }else{
	   				    			 if(rateIdCount<1){
	   				    				 if(accommodation.getMinimumBaseAmount()!=null){
	   				    					minAmount=accommodation.getMinimumBaseAmount();	 
	   				    				 }
	   				    				 if(accommodation.getMaximumBaseAmount()!=null){
	   				    					maxAmount=accommodation.getMaximumBaseAmount();	 
	   				    				 }
	   				    				 
	   				    				if(occupancyRating==0){
	   				 						sellrate = minAmount;
	   				 					}else if(occupancyRating>0 && occupancyRating<=30){
	   				 			        	sellrate = minAmount;
	   				 			        }else if(occupancyRating>=31 && occupancyRating<=60){
	   				 			        	sellrate = minAmount+(minAmount*10/100);
	   				 			        }else if(occupancyRating>=61 && occupancyRating<=80){
	   				 			        	sellrate = minAmount+(minAmount*15/100);
	   				 			        }else if(occupancyRating>=81 && occupancyRating<=100){
	   				 			        	sellrate = maxAmount;
	   				 			        }
	   				    				 amount+= sellrate;
	   				    				 rateIdCount++;
	   				    				rateCount++;
	   				    			 }
	       				    	 }
					    	}
					     }else{

					    	 if(accommodation.getMinimumBaseAmount()!=null){
		    					minAmount=accommodation.getMinimumBaseAmount();	 
		    				 }
		    				 if(accommodation.getMaximumBaseAmount()!=null){
		    					maxAmount=accommodation.getMaximumBaseAmount();	 
		    				 }
		    				 
		    				if(occupancyRating==0){
		 						sellrate = minAmount;
		 					}else if(occupancyRating>0 && occupancyRating<=30){
		 			        	sellrate = minAmount;
		 			        }else if(occupancyRating>=31 && occupancyRating<=60){
		 			        	sellrate = minAmount+(minAmount*10/100);
		 			        }else if(occupancyRating>=61 && occupancyRating<=80){
		 			        	sellrate = minAmount+(minAmount*15/100);
		 			        }else if(occupancyRating>=81 && occupancyRating<=100){
		 			        	sellrate = maxAmount;
		 			        }
		    				 amount+= sellrate;
		    			 
					     }
				    	 dateCount++;
				    	 if(dateCount==diffInDays){
				    		 if(blnAvailable){
			    				mapAmountAccommId.put(amount,accommodation.getAccommodationId());
				      			arllist.add(amount);
		      				 }else if(blnSoldout){
		      					mapSoldOutAmountAccommId.put(amount,accommodation.getAccommodationId());
				      			arllist.add(amount);
					    	 }
				    	 }
				     }
				     if(blnAvailable){
				    	 arllistAvl.add(amount);
				     }else if(!blnAvailable){
				    	 arllistSold.add(amount);
				     }
				     accommCount++;
				}
			}
			//end of accommodation
			double totalAmount=0;
			boolean blnJSONContent=false;
			Collections.sort(arllist);
			int arlListSize=0,accommId=0,intTotalAccomm=0;
		     intTotalAccomm=accommodationList.size();
		     if(intTotalAccomm==accommCount){
		    	 if(intTotalAccomm==soldOutCount){
				     arlListSize=arllist.size();
				     if(soldOutCount==arlListSize){
				    	 Collections.sort(arllist);
				    	 for(int i=0;i<arlListSize;i++){
					    	 totalAmount=(Double) arllist.get(i);
					    	 if((Double) arllist.get(0)==(Double) arllist.get(i)){
					    		 if(blnAvailable){
					    			 accommId=mapAmountAccommId.get(totalAmount);
						    		 blnJSONContent=true;
					    		 }else{
					    			 accommId=mapSoldOutAmountAccommId.get(totalAmount);
						    		 blnJSONContent=true;
					    		 }
					    		 break;
					    	 }else{
					    		 
					    	 }
					     }
				     }
		    	 }else{
				     arlListSize=arllist.size();
				     Collections.sort(arllist);
			    	 for(int i=0;i<arlListSize;i++){
			    		 if(arllistAvl.contains((Double) arllist.get(i))){
			    			 if((Double) arllist.get(0)==(Double) arllist.get(i)){
				    			 totalAmount=(Double) arllist.get(i);
				    			 accommId=mapAmountAccommId.get(totalAmount);
					    		 blnJSONContent=true;
			    			 }else{
			    				 totalAmount=(Double) arllist.get(i);
				    			 accommId=mapAmountAccommId.get(totalAmount);
					    		 blnJSONContent=true;
			    			 }
			    			 break;
			    		 }
				     }
		    	 }
		     }
		     arlListPromoType.clear();  
			if(blnJSONContent){
			    listAccommodationRoom.clear(); 
		    	listAccommodationRoom=accommodationController.listPropertyTotalRooms(property.getPropertyId(), accommId);
		    	List<DateTime> betweenAmount = DateUtil.getDateRange(start, end);
		    	double actualPromoBaseAmount=0.0,actualBaseAmount=0.0,actualTotalAmount=0.0,actualTotalAdultAmount=0.0,actualTotalChildAmount=0.0,
		    			actualAdultAmount=0.0,actualChildAmount=0.0;
			    int countDate=0; 
			    boolean blnStatus=false;
			    
			    double baseAmount=0.0,extraAdultAmount=0.0,extraChildAmount=0.0,totalBaseAmount=0.0,
			    		totalMinimumAmount=0,totalMaximumAmount=0,totalSellRate=0;
			    int roomCount=0,availableCount=0,totalCount=0,discountId=0;
			    
				if(listAccommodationRoom.size()>0 && !listAccommodationRoom.isEmpty()){
					for(AccommodationRoom roomsList:listAccommodationRoom){
						 roomCount=(int) roomsList.getRoomCount();
						 break;
					 }
				 }
				 
				ArrayList arlRateType=new ArrayList();
				 List<PropertyRatePlanDetail> listPlanDetails=planDetailController.listAccommodation(property.getPropertyId(), accommId);
				 if(listPlanDetails.size()>0 && !listPlanDetails.isEmpty()){
		        	for(PropertyRatePlanDetail ratePlanDetails:listPlanDetails){
		        		arlRateType.add(ratePlanDetails.getRatePlan().getRatePlanType());
		        		arlRateType.add(ratePlanDetails.getRatePlan().getRatePlanSymbolType());
		        		arlRateType.add(ratePlanDetails.getTariffAmount());
		        	}
				 }
				 
				 List<DateTime> between = DateUtil.getDateRange(start, end);
				 SimpleDateFormat format1=new SimpleDateFormat("yyyy-MM-dd");
				 java.util.Date currentdate=new java.util.Date();
				 String strCurrentDate = new SimpleDateFormat("yyyy-MM-dd").format(currentdate);
			   	 java.util.Date dtecurdate = format1.parse(strCurrentDate);
			   	 Calendar calStart=Calendar.getInstance();
			   	 calStart.setTime(dtecurdate);
			   	 calStart.setTimeZone(TimeZone.getTimeZone("IST"));
			   	 String StartDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calStart.getTime());
			   	 java.util.Date curdate = format1.parse(StartDate);
			   	String jsonPrices="";
				List<PropertyAccommodation> listAccommodation =  accommodationManager.list(property.getPropertyId(),accommId);
				int dateCount=0;
				for (PropertyAccommodation accommodations : listAccommodation) {
					
					
					for (DateTime d : betweenAmount){

				    	 long rmc = 0;
			   			 this.roomCnt = rmc;
			        	 int sold=0;
			        	 java.util.Date availDate = d.toDate();
						 PropertyAccommodationInventory inventoryCount = inventoryController.findInventoryCount(availDate,accommodations.getAccommodationId());
						 if(inventoryCount != null){
						 PmsAvailableRooms roomCount1 = bookingController.findBookingCount(availDate, accommodations.getAccommodationId(),inventoryCount.getInventoryId());
						 if(roomCount1.getRoomCount() == null) {
						 	String num1 = inventoryCount.getInventoryCount();
							PmsAvailableRooms bookingRoomCount=bookingController.findCount(availDate, accommodations.getAccommodationId());
							Integer totavailable=0;
							if(accommodations!=null){
								totavailable=accommodations.getNoOfUnits();
							}
							if(bookingRoomCount.getRoomCount()!=null){
								long lngRooms=bookingRoomCount.getRoomCount();
								long num2 = Long.parseLong(num1);
								this.roomCnt = num2;
								sold=(int) (lngRooms);
								int availRooms=0;
								availRooms=totavailable-sold;
								if(Integer.parseInt(num1)>availRooms){
									this.roomCnt = availRooms;	
								}else{
									this.roomCnt = num2;
								}
										
							}else{
								 long num2 = Long.parseLong(num1);
								 this.roomCnt = num2;
								 sold=(int) (rmc);
							}
						}else{		
							PmsAvailableRooms bookingRoomCount=bookingController.findCount(availDate, accommodations.getAccommodationId());
							int intRooms=0;
							Integer totavailable=0,availableRooms=0;
							if(accommodations!=null){
								totavailable=accommodations.getNoOfUnits();
							}
							if(bookingRoomCount.getRoomCount()!=null){
								long lngRooms=bookingRoomCount.getRoomCount();
								long num = roomCount1.getRoomCount();	
								intRooms=(int)lngRooms;
								String num1 = inventoryCount.getInventoryCount();
			 					long num2 = Long.parseLong(num1);
			 					availableRooms=totavailable-intRooms;
			 					if(num2>availableRooms){
			 						this.roomCnt = availableRooms;	 
			 					}else{
			 						this.roomCnt = num2-num;
			 					}
			 					long lngSold=bookingRoomCount.getRoomCount();
			   					sold=(int)lngSold;
							}else{
								long num = roomCount1.getRoomCount();	
								String num1 = inventoryCount.getInventoryCount();
								long num2 = Long.parseLong(num1);
			  					this.roomCnt = (num2-num);
			  					long lngSold=roomCount1.getRoomCount();
			   					sold=(int)lngSold;
							}
		
						}
										
					}else{
						  PmsAvailableRooms inventoryCounts = bookingController.findCount(availDate, accommodations.getAccommodationId());							
							if(inventoryCounts.getRoomCount() == null){								
								this.roomCnt = (accommodations.getNoOfUnits()-rmc);
								sold=(int)rmc;
							}
							else{
								this.roomCnt = (accommodations.getNoOfUnits()-inventoryCounts.getRoomCount());	
								long lngSold=inventoryCounts.getRoomCount();
	   							sold=(int)lngSold;
							}
						}
											
						double totalRooms=0,intSold=0;
						totalRooms=(double)accommodations.getNoOfUnits();
						intSold=(double)sold;
						double occupancy=0.0;
						occupancy=intSold/totalRooms;
						double sellrate=0.0,otarate=0.0;
						Integer occupancyRating=(int) Math.round(occupancy * 100);
					    int rateCount=0,rateIdCount=0;
					    double minAmount=0,maxAmount=0;
					    List<PropertyRate> dateList =  rateController.listAllDates(property.getPropertyId(),accommodations.getAccommodationId(),getSourceTypeId(),d.toDate());
						if(!dateList.isEmpty()){
							for(PropertyRate rates : dateList) {
					    		int propertyRateId = rates.getPropertyRateId();
				    			List<PropertyRateDetail> rateDetailList =   detailController.listMinMaxRateDetail(propertyRateId,f.format(d.toDate()).toLowerCase());
	   				    		 if(rateDetailList.size()>0 && !rateDetailList.isEmpty()){
	   				    			 if(rateCount<1){
	   				    				 for (PropertyRateDetail rate : rateDetailList){
	   				    					 minAmount=rate.getMinimumBaseAmount();
	   				    					 maxAmount=rate.getMaximumBaseAmount();
	   				    					 extraAdultAmount=rate.getExtraAdult();
	   				    					 extraChildAmount=rate.getExtraChild();
	   				    				    if(occupancyRating==0){
		   				 						sellrate = minAmount;
		   				 					}else if(occupancyRating>0 && occupancyRating<=30){
		   				 			        	sellrate = minAmount;
		   				 			        }else if(occupancyRating>=31 && occupancyRating<=60){
		   				 			        	sellrate = minAmount+(minAmount*10/100);
		   				 			        }else if(occupancyRating>=61 && occupancyRating<=80){
		   				 			        	sellrate = minAmount+(minAmount*15/100);
		   				 			        }else if(occupancyRating>=81 && occupancyRating<=100){
		   				 			        	sellrate = maxAmount;
		   				 			        }
	   				    				    baseAmount+=  sellrate;
	       				    			 }
	   				    				 rateCount++;
	   				    				rateIdCount++;
	   				    			 }
	   				    		 }else{
	   				    			 if(rateIdCount<1){
	   				    				if(accommodations.getMinimumBaseAmount()!=null){
	   				    					minAmount=accommodations.getMinimumBaseAmount();	 
	   				    				 }
	   				    				 if(accommodations.getMaximumBaseAmount()!=null){
	   				    					maxAmount=accommodations.getMaximumBaseAmount();	 
	   				    				 }
	   				    				 extraAdultAmount=accommodations.getExtraAdult();
  				    					 extraChildAmount=accommodations.getExtraChild();
	   				    				if(occupancyRating==0){
	   				 						sellrate = minAmount;
	   				 					}else if(occupancyRating>0 && occupancyRating<=30){
	   				 			        	sellrate = minAmount;
	   				 			        }else if(occupancyRating>=31 && occupancyRating<=60){
	   				 			        	sellrate = minAmount+(minAmount*10/100);
	   				 			        }else if(occupancyRating>=61 && occupancyRating<=80){
	   				 			        	sellrate = minAmount+(minAmount*15/100);
	   				 			        }else if(occupancyRating>=81 && occupancyRating<=100){
	   				 			        	sellrate = maxAmount;
	   				 			        }
	   				    				baseAmount+= sellrate;
	   				    				rateIdCount++;
	   				    				rateCount++;
	   				    			 }
	       				    	 }
					    	}
					     }else{

					    	 if(accommodations.getMinimumBaseAmount()!=null){
		    					minAmount=accommodations.getMinimumBaseAmount();	 
		    				 }
		    				 if(accommodations.getMaximumBaseAmount()!=null){
		    					maxAmount=accommodations.getMaximumBaseAmount();	 
		    				 }
		    				 extraAdultAmount=accommodations.getExtraAdult();
		    				 extraChildAmount=accommodations.getExtraChild();
		    				 
		    				if(occupancyRating==0){
		 						sellrate = minAmount;
		 					}else if(occupancyRating>0 && occupancyRating<=30){
		 			        	sellrate = minAmount;
		 			        }else if(occupancyRating>=31 && occupancyRating<=60){
		 			        	sellrate = minAmount+(minAmount*10/100);
		 			        }else if(occupancyRating>=61 && occupancyRating<=80){
		 			        	sellrate = minAmount+(minAmount*15/100);
		 			        }else if(occupancyRating>=81 && occupancyRating<=100){
		 			        	sellrate = maxAmount;
		 			        }
		    				baseAmount+= sellrate;
		    			 
					     }
						totalMinimumAmount+=minAmount;
						totalMaximumAmount+=maxAmount;
						totalSellRate+=sellrate;
						if (!jsonPrices.equalsIgnoreCase(""))
							jsonPrices += ",{";
						else
							jsonPrices += "{";
						
						
						jsonPrices += "\"inventoryRating\":\"" + (occupancyRating)+ "\"";
						jsonPrices += ",\"minimumAmount\":\"" + (minAmount)+ "\"";
						jsonPrices += ",\"maximumAmount\":\"" + + (maxAmount)+  "\"";
						jsonPrices += ",\"sellrate\":\"" + + (sellrate)+  "\"";	
						jsonPrices += ",\"extraAdult\":\"" + (extraAdultAmount)+ "\"";
						jsonPrices += ",\"extraChild\":\"" + (extraChildAmount)+  "\"";
						jsonPrices += ",\"date\":\"" +  (format1.format(availDate))+  "\"";
					
						jsonPrices += "}";
						
				    	countDate++;
				    }
					
					jsonOutput += ",\"rates\":[" + jsonPrices+ "]";
					
					double variationAmount=0,variationRating=0;
					variationAmount=totalMaximumAmount-totalSellRate;
					variationRating=variationAmount/totalMaximumAmount*100;
					String promotionFirstName=null;
//					promotionFirstName=String.valueOf(Math.round(variationRating))+" % OFF";
					/*if(variationRating>9){
						promotionFirstName=String.valueOf(Math.round(variationRating))+" % OFF";	
					}else{
						promotionFirstName="Flat "+String.valueOf(Math.round(variationAmount))+" OFF";
					}*/
					if(variationRating>0){
						promotionFirstName=String.valueOf(Math.round(variationRating))+" % OFF";	
					}
					double dblRatePlanAmount=0,dblEPAmount=0,dblTotalEPAmount=0;
			        Iterator iterRatePlan=arlRateType.iterator();
			        String strType=null,strSymbol=null;
	            	while(iterRatePlan.hasNext()){
	            		strType=(String)iterRatePlan.next();
	            		strSymbol=(String)iterRatePlan.next();
	            		dblRatePlanAmount=(Double)iterRatePlan.next();
	            		
	            		if(strType.equalsIgnoreCase("EP")){
	                		if(strSymbol.equalsIgnoreCase("plus")){
	                			dblEPAmount=baseAmount+dblRatePlanAmount;	
	                		}else{
	                			dblEPAmount=baseAmount-dblRatePlanAmount;
	                		}
	            		}
	            		
	            	}
					totalAmount=baseAmount;
					dblTotalEPAmount=dblEPAmount;
					if(dblTotalEPAmount==0){
						dblTotalEPAmount=baseAmount;
					}
					if(countDate>1){
						actualAdultAmount=extraAdultAmount/countDate;
						actualChildAmount=extraChildAmount/countDate;
					}else{
						actualAdultAmount=extraAdultAmount;
						actualChildAmount=extraChildAmount;
					}
				
					
					
					totalAmount=Math.round(totalAmount);
					actualTotalAdultAmount=Math.round(actualAdultAmount);
					actualTotalChildAmount=Math.round(actualChildAmount);
					
					if(totalMinimumAmount==0){
						totalBaseAmount=0;
					}
					if(totalMinimumAmount>0){
						totalBaseAmount=totalMinimumAmount;
					}
	            	
					long minimum = getAvailableCount(property.getPropertyId(),getArrivalDate(),getDepartureDate(),roomCount,accommId);
			    	 
			    	 if(minimum==0){
			    		 jsonOutput += ",\"available\":\"" + minimum+ "\"";
			    		 jsonOutput += ",\"minimumAmount\":\"" + + (totalMinimumAmount==0 ? totalBaseAmount : totalMinimumAmount )+  "\"";
			    		 jsonOutput += ",\"maximumAmount\":\"" + + (totalMaximumAmount==0 ? totalBaseAmount : totalMaximumAmount )+  "\"";
			    		 jsonOutput += ",\"baseActualAmount\":\"" + + (totalMaximumAmount==0 ? totalBaseAmount : totalMaximumAmount )+  "\"";
			    		 jsonOutput += ",\"baseAmount\":\"" + + (totalAmount==0 ? totalBaseAmount : totalAmount )+  "\"";
			    		 jsonOutput += ",\"totalAmount\":\"" + + (totalAmount==0 ? totalBaseAmount : totalAmount )+  "\"";
			    		 jsonOutput += ",\"cpAmount\":\"" + + (totalAmount==0 ? totalBaseAmount : totalAmount )+  "\"";
			    		 jsonOutput += ",\"epAmount\":\"" + + (dblTotalEPAmount)+  "\"";
			    		 jsonOutput += ",\"promotionFirstName\":\"" + (promotionFirstName==null? "None":promotionFirstName)+ "\"";
			    		 jsonOutput += ",\"accommodationName\":\"" + accommodations.getAccommodationType().toUpperCase()+ "\"";
						 jsonOutput += ",\"accommodationId\":\"" + accommodations.getAccommodationId()+ "\"";
				     }else{
				    	jsonOutput += ",\"accommodationName\":\"" + accommodations.getAccommodationType().toUpperCase()+ "\"";
						jsonOutput += ",\"accommodationId\":\"" + accommodations.getAccommodationId()+ "\"";
			    		jsonOutput += ",\"available\":\"" + minimum+ "\"";
			    		jsonOutput += ",\"maxAdult\":\"" + accommodations.getMaxOccupancy()+ "\"";
			    		jsonOutput += ",\"maxChild\":\"" + 0 + "\"";
			    		jsonOutput += ",\"minOccupancy\":\"" + accommodations.getMinOccupancy() + "\"";
						jsonOutput += ",\"maxOccupancy\":\"" + accommodations.getMaxOccupancy() + "\"";
						jsonOutput += ",\"minimumAmount\":\"" + + (totalMinimumAmount==0 ? totalBaseAmount : totalMinimumAmount )+  "\"";
						jsonOutput += ",\"maximumAmount\":\"" + + (totalMaximumAmount==0 ? totalBaseAmount : totalMaximumAmount )+  "\"";
						jsonOutput += ",\"baseActualAmount\":\"" +  (totalMaximumAmount==0 ? totalBaseAmount : totalMaximumAmount )+  "\"";
						jsonOutput += ",\"baseAmount\":\"" + + (totalAmount==0 ? totalBaseAmount : totalAmount )+  "\"";
			    		jsonOutput += ",\"totalAmount\":\"" + + (totalAmount==0 ? totalBaseAmount : totalAmount )+  "\"";
			    		jsonOutput += ",\"cpAmount\":\"" + + (totalAmount==0 ? totalBaseAmount : totalAmount )+  "\"";
			    		jsonOutput += ",\"epAmount\":\"" + + (dblTotalEPAmount)+  "\"";
			    		jsonOutput += ",\"promotionFirstName\":\"" + (promotionFirstName==null? "None":promotionFirstName)+ "\"";
			    		jsonOutput += ",\"extraAdultAmount\":\"" + + (actualTotalAdultAmount==0 ? accommodations.getExtraAdult() : actualTotalAdultAmount )+  "\"";
			    		jsonOutput += ",\"extraChildAmount\":\"" + + (actualTotalChildAmount==0 ? accommodations.getExtraChild() : actualTotalChildAmount )+  "\"";
						discountId=35;
						PropertyDiscountManager discountController=new PropertyDiscountManager();
						PropertyDiscount propertyDiscount=discountController.find(discountId);
			    		jsonOutput += ",\"discountPercent\":\"" + (propertyDiscount.getDiscountPercentage()==0.0? "0":propertyDiscount.getDiscountPercentage())+ "\"";
			    		jsonOutput += ",\"discountId\":\"" + (discountId==0? "0" :discountId) + "\"";
			    		
			    		
				     }
				}
		     
			}
			
			jsonOutput += "}";
			
			arllist.clear();
		
		}
	 
		response.getWriter().write("{\"data\":[" + jsonOutput + "]}");

//		sessionMap.clear();
		

	} catch (Exception e) {
		logger.error(e);
		e.printStackTrace();
	} finally {

	}

	return null;


  
   }
    
    public HashMap<Integer,Integer> getPropertyReviews(){
    	HashMap<Integer, Integer> mapReviews = null;
    	try{
    		Integer starCount;
    		String jsonReviews="";
			PropertyReviewsManager reviewController=new PropertyReviewsManager();
			PmsPropertyManager propertyController=new PmsPropertyManager();
			ArrayList<Integer> arlReview=new ArrayList<Integer>();
			
			List<PmsProperty> listpmsproperty=propertyController.listPropertyByLocation(getLocationId());
			if(listpmsproperty.size()>0 && !listpmsproperty.isEmpty()){
				for(PmsProperty pmsProperty:listpmsproperty){
					List<PropertyReviews> listReviews=reviewController.listReview(pmsProperty.getPropertyId());
					if(listReviews.size()>0 && !listReviews.isEmpty()){
						for(PropertyReviews reviews:listReviews){
							 starCount=reviews.getStarCount();
							 if(starCount==null || starCount==0){
								 starCount=0;
							 }
							 mapReviews.put(pmsProperty.getPropertyId(), starCount);
						}
					}
				}
			}
			
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    	
		
		return mapReviews;
    }
    
    public String getSingleAccommodationAvailabilities() throws IOException{
		try{
			HttpServletResponse response = ServletActionContext.getResponse();
			sessionMap.put("locationId",getLocationId());
			
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			
		    java.util.Date dateStart = format.parse(getStrStartDate());
		    java.util.Date dateEnd = format.parse(getStrEndDate());
		   
		    String strFromDate=format.format(dateStart);
			String strToDate=format.format(dateEnd);
	    	DateTime startdate = DateTime.parse(strFromDate);
	        DateTime enddate = DateTime.parse(strToDate);
	        java.util.Date fromdate = startdate.toDate();
	 		java.util.Date todate = enddate.toDate();
	 		 
			int diffInDays = (int) ((todate.getTime() - fromdate.getTime()) / (1000 * 60 * 60 * 24));
			DateFormat f = new SimpleDateFormat("EEEE");
		    
		    Calendar calCheckStart=Calendar.getInstance();
		    calCheckStart.setTime(dateStart);
		    java.util.Date checkIn = calCheckStart.getTime();
		    this.arrivalDate=new Timestamp(checkIn.getTime());
		    
		    
		    Calendar calCheckEnd=Calendar.getInstance();
		    calCheckEnd.setTime(dateEnd);
		    calCheckEnd.add(Calendar.DATE, -1);
		    java.util.Date checkOut = calCheckEnd.getTime();
		    this.departureDate=new Timestamp(checkOut.getTime());
		   
		    
			SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
			
	    	strStartDate=format1.format(getArrivalDate());
    		strEndDate=format1.format(getDepartureDate());
    		
			
			response.setContentType("application/json");
			
			this.arrivalDate = getArrivalDate();
		    sessionMap.put("arrivalDate",arrivalDate); 
				
			this.departureDate = getDepartureDate();
			sessionMap.put("departureDate",departureDate); 
			
			this.propertyAccommodationId=getPropertyAccommodationId();
			sessionMap.put("propertyAccommodationId",propertyAccommodationId); 
			
		    
		    PmsPropertyManager propertyController = new PmsPropertyManager();		
			PropertyAccommodationManager accommodationManager = new PropertyAccommodationManager();
			PropertyAmenityManager amenityController = new PropertyAmenityManager();
			List<PropertyAmenity> propertyAmenityList;
			PropertyTypeManager typeController =  new PropertyTypeManager();
			PropertyRateManager rateController = new PropertyRateManager();
			PropertyRateDetailManager detailController = new PropertyRateDetailManager();
			PmsAmenityManager ameController = new PmsAmenityManager();
			PromotionManager promotionController=new PromotionManager();
			PromotionDetailManager promotionDetailController=new PromotionDetailManager();
			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
			String jsonOutput = "";
			
		    
		    int promotionId=0;
		    
		    String promotionType=null,promotionName=null;
		    Integer promoPercent=null;
		    double promotionPercent=0.0;
		    
		    DateFormat dayformat = new SimpleDateFormat("EEEE");
		    
	    	List<PmsSmartPrice> listSmartPriceDetail=null; 
	    	List<AccommodationRoom> listAccommodationRoom=null;
	    	List<PmsPromotionDetails> listPromotionDetailCheck=null;
			
			
			String startDate = new SimpleDateFormat("yyyy-MM-dd").format(this.arrivalDate);
			String endDate = new SimpleDateFormat("yyyy-MM-dd").format(this.departureDate);
			
			DateTime start = DateTime.parse(startDate);
		    DateTime end = DateTime.parse(endDate);
		    List<DashBoard> listDashBoard = null;
			this.propertyList = propertyController.listPropertyByLocation(getLocationId());
			for (PmsProperty property : propertyList) {

				Boolean blnPromoFlag=false,promotionFlag=false,blnFirst=false;
				listDashBoard=accommodationController.listPropertyAccommodation(property.getPropertyId());
				if(listDashBoard.size()>0 &&!listDashBoard.isEmpty()){
					for(DashBoard dashboard:listDashBoard){
						int accommId=dashboard.getAccommodationId();
						if(accommId==(int)getPropertyAccommodationId()){
							int promotionAccommId=0;
							if (!jsonOutput.equalsIgnoreCase(""))
								jsonOutput += ",{";
							else
								jsonOutput += "{";
							jsonOutput += "\"DT_RowId\":\"" + property.getPropertyId()+ "\"";
							jsonOutput += ",\"city\":\"" + property.getCity() + "\"";
							jsonOutput += ",\"propertyName\":\"" + (property.getPropertyName() == null ? "None": property.getPropertyName().toUpperCase().trim())+ "\"";
							jsonOutput += ",\"payAtHotelStatus\":\"" + (property.getPayAtHotelStatus() == null ? false: property.getPayAtHotelStatus())+ "\"";
							jsonOutput += ",\"coupleStatus\":\"" + ( property.getCoupleIsActive() == null? false :  property.getCoupleIsActive() )+ "\"";
							jsonOutput += ",\"breakfastStatus\":\"" + ( property.getBreakfastIsActive() == null? false:  property.getBreakfastIsActive() )+ "\"";
							jsonOutput += ",\"hotelStatus\":\"" + ( property.getHotelIsActive() == null? false:  property.getHotelIsActive() )+ "\"";
							//jsonOutput += ",\"propertyStandardPolicy\":\"" + (property.getPropertyStandardPolicy() == null ? " ": property.getPropertyStandardPolicy().trim())+ "\"";
							//jsonOutput += ",\"propertyHotelPolicy\":\"" + (property.getPropertyHotelPolicy() == null ? " ": property.getPropertyHotelPolicy().trim())+ "\"";
							//jsonOutput += ",\"propertyCancellationPolicy\":\"" + (property.getPropertyCancellationPolicy() == null ? " ": property.getPropertyCancellationPolicy().trim())+ "\"";
							jsonOutput += ",\"arrivalDate\":\"" + arrivalDate + "\"";
							jsonOutput += ",\"departureDate\":\"" + departureDate + "\"";
							jsonOutput += ",\"mohan\":\"" + "Mohan" + "\"";
							String jsonAmenities ="";
							propertyAmenityList =  amenityController.list(property.getPropertyId());
							if(propertyAmenityList.size()>0)
							{
								for (PropertyAmenity amenity : propertyAmenityList) {
				  					if (!jsonAmenities.equalsIgnoreCase(""))
				  						
				  						jsonAmenities += ",{";
				  					else
				  						jsonAmenities += "{";
				  					PropertyAmenity amentity1 = amenityController.find(amenity.getPropertyAmenityId());
				  				    String path = "ulowebsite/images/icons/";
				  					PmsAmenity pmsAmenity1 =ameController.find(amentity1.getPmsAmenity().getAmenityId());
				  					jsonAmenities += "\"DT_RowId\":\"" + amenity.getPropertyAmenityId()+ "\"";					
				  					jsonAmenities += ",\"amenityName\":\"" + pmsAmenity1.getAmenityName()+ "\"";
				  					jsonAmenities += ",\"amenityId\":\"" + pmsAmenity1.getAmenityId()+ "\"";
				                    jsonAmenities += ",\"icon\":\"" + path + pmsAmenity1.getAmenityIcon()+ "\"";
				               
				  					
				  					jsonAmenities += "}";
				  				}
								
								jsonOutput += ",\"amenities\":[" + jsonAmenities+ "]";
							}
							
							List<PropertyAccommodation> accommodationList =  accommodationManager.list(property.getPropertyId(),getPropertyAccommodationId());
							if(accommodationList.size()>0)
							{
								for (PropertyAccommodation accommodations : accommodationList) {
									
									Boolean blnLastMinutePromotions=false,blnBookedGetRooms=false,blnStatus=false,promoFlag=false,blnSmartPriceFlag=false;
									int dateCount=0,promoCount=0;
									double totalAmount=0.0,extraAdultAmount=0.0,extraChildAmount=0.0;
									int roomCount=0,availableCount=0,totalCount=0,countDate=0,promotionHours=0,promoAccommId=0;
								    double dblPercentCount=0.0,dblSmartPricePercent=0.0,dblPercentFrom=0.0,dblPercentTo=0.0,lastAmount=0.0, flatAmount=0.0,
								    		smartPricePercent=0.0,discountPercent=0.0,actualAmount=0.0, totalActualAmount=0.0,totalActualExtraAdultAmount=0.0,totalActualExtraChildAmount=0.0;
								    Double totalGetNights=0.0;
								    Boolean isPositive=false,isNegative=false;
								    int smartPriceId=0,discountId=0;
					    			Double dblGetNights=0.0,dblBookNights=0.0,dblGetRooms=0.0,dblBookRooms=0.0;
					    			Timestamp lastMinStartDate=null,lastMinEndDate=null;
									sourceId = 1;
									
									ArrayList<String> arlPromoType=new ArrayList<String>();
							    	double actualBaseAmount=0.0,actualTotalAdultAmount=0.0,actualTotalChildAmount=0.0,actualTotal=0.0,
							    			actualAdultAmount=0.0,actualChildAmount=0.0, baseAmount=0.0,promoBaseAmount=0.0,promoAdultAmount=0.0,promoChildAmount=0.0,lastMinuteHours=0.0;
								   
								    
									listAccommodationRoom=accommodationController.listPropertyTotalRooms(property.getPropertyId(), accommodations.getAccommodationId());
									 
									 
									if(listAccommodationRoom.size()>0 && !listAccommodationRoom.isEmpty()){
										for(AccommodationRoom roomsList:listAccommodationRoom){
											 roomCount=(int) roomsList.getRoomCount();
											 break;
										 }
									 }
									 
									 List<DateTime> between = DateUtil.getDateRange(start, end);
									 
									 boolean blnActivePromo=false,blnNotActivePromo=false,basePromoFlag=false;
									 ArrayList<Integer> arlActivePromo=new ArrayList<Integer>(); 
									 
									 for (DateTime d : between)
								     {
										 List<PmsPromotions> listAllPromotions= promotionController.listDatePromotions(property.getPropertyId(),d.toDate());
										 if(!listAllPromotions.isEmpty() && listAllPromotions.size()>0){
											 for(PmsPromotions promos:listAllPromotions){
												 if(promos.getPromotionType().equalsIgnoreCase("F")){
													 if(arlActivePromo.isEmpty()){
														 arlActivePromo.add(promos.getPromotionId());
													 }else if(!arlActivePromo.contains(promos.getPromotionId())){
														 arlActivePromo.add(promos.getPromotionId());
													 }
												 }else{
													 if(arlActivePromo.isEmpty()){
														 arlActivePromo.add(promos.getPromotionId());
													 }else if(!arlActivePromo.contains(promos.getPromotionId())){
														 arlActivePromo.add(promos.getPromotionId());
													 }
												 }
											 }
										 }else{
											 blnNotActivePromo=true;
										 }
								     }
									 int intPromoSize=arlActivePromo.size();
									 if(arlActivePromo.isEmpty() || blnNotActivePromo){
										 blnActivePromo=false;
										 basePromoFlag=false;
									 }else{
										 blnActivePromo=true;
									 }
									 
								     for (DateTime d : between)
								     {
								    	 boolean SmartPriceEnableCheck=false;
										if(blnActivePromo){
											List<PmsPromotions> listFlatPromotions= promotionController.listDatePromotions(property.getPropertyId(),d.toDate());
									    	if(listFlatPromotions.size()>0 && !listFlatPromotions.isEmpty()){
									    		for(PmsPromotions promotions:listFlatPromotions){
									    			 if(promotions.getPromotionType().equalsIgnoreCase("F")){
									    				 promotionId=promotions.getPromotionId();
									    				 listPromotionDetailCheck=promotionDetailController.listPromotionDetails(promotions.getPromotionId(),f.format(d.toDate()).toLowerCase());
									    				 for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
								    						 if(promoCount==0){
								    							 promotionPercent=promoDetails.getPromotionPercentage();
									    						 promotionFlag=true;
											    				 blnPromoFlag=true;
											    				 promotionType=promotions.getPromotionType();
											    				 arlPromoType.add(promotionType);
								    						 }
								    						 promoCount++;
								    					 }
									    			 }else{
									    				 List<PmsPromotions> listPmsPromotions= promotionController.listDatePromotions(property.getPropertyId(),accommodations.getAccommodationId(),d.toDate());
										    			 if(listPmsPromotions.size()>0 &&!listPmsPromotions.isEmpty()){
										    				 for(PmsPromotions pmsPromotions:listPmsPromotions){
										    					 promotionId=pmsPromotions.getPromotionId();
										    					 promoAccommId=pmsPromotions.getPropertyAccommodation().getAccommodationId();
										    					 listPromotionDetailCheck=promotionDetailController.listPromotionDetails(pmsPromotions.getPromotionId(),f.format(d.toDate()).toLowerCase());
											    				 if(listPromotionDetailCheck.size()>0 && !listPromotionDetailCheck.isEmpty()){
												        			 if(listPromotionDetailCheck.size()>1){
													    				 promotionFlag=false;
													    				 promotionType=null;
													    				 blnPromoFlag=false;
													    				 basePromoFlag=true;
													    			 }else if(listPromotionDetailCheck.size()==1){
													    				 if(!pmsPromotions.getPromotionType().equalsIgnoreCase("F")){
														    				 if(promoAccommId==accommodations.getAccommodationId()){
														    					 promotionAccommId=promoAccommId;
														    					 lastMinStartDate=pmsPromotions.getStartDate();
															    				 lastMinEndDate=pmsPromotions.getEndDate();
															    				 Double nightsGet=0.0,nightsBook=0.0;
															    				 if(pmsPromotions.getPromotionType().equalsIgnoreCase("N")){
															    					 for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
															    						 nightsBook=promoDetails.getBookNights();
															    						 nightsGet=promoDetails.getGetNights();
															    						 totalGetNights=nightsBook+nightsGet;
														    			    		     Integer intTotalGetNights=totalGetNights.intValue()-1;
														    			    			 Calendar calCheckOut=Calendar.getInstance();
														    			    			 calCheckOut.setTime(getDepartureDate());
														    			    			 calCheckOut.add(Calendar.DATE,intTotalGetNights);
														    			    			 java.util.Date checkOutDte = calCheckOut.getTime();             
														    			    			 String departureDate = format1.format(checkOutDte);
														    							 
														    			    			 java.util.Date fromDate = format1.parse(departureDate);
														    			    			 Timestamp tsDepartureDate=new Timestamp(fromDate.getTime());
														    			    			 String strStartDate=format1.format(getArrivalDate());
														    			    			 String strEndDate=format1.format(tsDepartureDate);
														    			    			 DateTime Startdate = DateTime.parse(strStartDate);
														    			    			 DateTime Enddate = DateTime.parse(strEndDate);
														    			    			 java.util.Date StartDate = Startdate.toDate();
													    			    		 		 java.util.Date EndDate = Enddate.toDate();
													    			    		 		 int diffDays = (int) ((EndDate.getTime() - StartDate.getTime()) / (1000 * 60 * 60 * 24));
														    			    			 
														    			    			 long minimumCount = getAvailableCount(getPropertyId(),getArrivalDate(),tsDepartureDate,roomCount,promoAccommId);
														    			    			 
																    					 if((int)minimumCount>0){
																    						 if(nightsBook.intValue()==diffInDays){
																	    						 promotionFlag=true;
																	    						 blnPromoFlag=true;
																	    						 promotionType=pmsPromotions.getPromotionType();
																	    						 arlPromoType.add(promotionType);
																	    					 }else{
																	    						 promotionFlag=false;
																	    						 promotionType=null;
																	    						 blnPromoFlag=false;
																	    						 basePromoFlag=true;
																	    					 }
																    					 }else{
																    						 promotionFlag=false;
																    						 promotionType=null;
																    						 blnPromoFlag=false;
																    						 basePromoFlag=true;
																    					 }
																    				 }
															    				 }else if(pmsPromotions.getPromotionType().equalsIgnoreCase("R")){
																    				 for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
															    						 dblGetRooms=promoDetails.getGetRooms();
															    						 dblBookRooms=promoDetails.getBookRooms();
															    					 }
													    							 
													    							 long minimum = getAvailableCount(getPropertyId(),getArrivalDate(),getDepartureDate(),roomCount,promoAccommId); 
													    							 
													    							 blnBookedGetRooms=bookGetRooms((int)minimum,dblGetRooms,dblBookRooms);
												    						    	 if(!blnBookedGetRooms){
												    						    		 promotionFlag=false;
												    						    		 promotionType=null;
												    						    		 blnPromoFlag=false;
												    						    		 basePromoFlag=true;
												    						    	 }else{
												    						    		 promotionFlag=true;
												    						    		 blnPromoFlag=true;
												    						    		 promotionType=pmsPromotions.getPromotionType();
												    						    		 arlPromoType.add(promotionType);
												    						    	 }
															    				 }else if(pmsPromotions.getPromotionType().equalsIgnoreCase("L")){
															    					 for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
															    						 lastMinuteHours=promoDetails.getPromotionHours();
															    						 promotionPercent=promoDetails.getPromotionPercentage();
															    					 }
															    					
															    					 long minimumCount = getAvailableCount(getPropertyId(),getArrivalDate(),getDepartureDate(),roomCount,promoAccommId);
													    			    			 if((int)minimumCount>0){
													    			    				 blnLastMinutePromotions=getCurrentAvailable(getPropertyId(),getArrivalDate(),getDepartureDate(),lastMinuteHours,(long)roomCount,promoAccommId,lastMinStartDate,lastMinEndDate);
													    			    				 if(blnLastMinutePromotions){
													    			    					 promotionFlag=true; 
													    			    					 blnPromoFlag=true;
													    			    					 promotionType=pmsPromotions.getPromotionType();
													    			    					 arlPromoType.add(promotionType);
													    			    				 }
													    			    			 }else{
													    			    				 promotionFlag=false;
													    			    				 promotionType=null;
													    			    				 blnPromoFlag=false;
													    			    				 basePromoFlag=true;
													    			    			 }
															    				 }
														    				 }
													    				 }
													    			 }
												        		 }
										    				 }
										    			 }
									    			 }
									    		}
									    	}
									    	if(arlPromoType.contains("F")){
							    			    promoFlag=true;
							    			    int rateCount=0,rateIdCount=0;
					    						List<PropertyRate> propertyRateList =  rateController.listAllDates(property.getPropertyId(),accommodations.getAccommodationId(),getSourceId(),d.toDate());
					    						List<PropertyRate> currentDateSmartPriceActive=rateController.listCurrentSmartPriceActive(property.getPropertyId(),accommodations.getAccommodationId(), d.toDate());
					    				    	if(propertyRateList.size()>0)
					    				    	{
					    				    		 for (PropertyRate pr : propertyRateList)
					    			    			 {
									    				 int propertyRateId = pr.getPropertyRateId();
					        				    		 Boolean SmartPriceCheck=pr.getSmartPriceIsActive();
											    		 if(SmartPriceCheck){
											    			 List<PropertyRateDetail> rateDetailList =   detailController.list(propertyRateId,f.format(d.toDate()).toLowerCase());
												    		 if(rateDetailList.size()>0){
												    			 for (PropertyRateDetail rate : rateDetailList){
												    				 if(rateCount<1){
												    					 SmartPriceEnableCheck=true;
												    					 baseAmount =  rate.getBaseAmount();
													    				 extraAdultAmount+=rate.getExtraAdult();
													    				 extraChildAmount+=rate.getExtraChild();
													    				 rateCount++;
												    				 }
												    			 }
												    		 }
											    		 }else{
											    			 List<PropertyRateDetail> rateDetailList =   detailController.list(propertyRateId,f.format(d.toDate()).toLowerCase());
						        				    		 if(rateDetailList.size()>0){
						        				    			 if(rateCount<1){
						        				    				 for (PropertyRateDetail rate : rateDetailList){
							        				    				 if(currentDateSmartPriceActive.size()>0){
							        				    					 SmartPriceEnableCheck=true;
							        				    					 baseAmount =  rate.getBaseAmount();
						    	        				    				 extraAdultAmount+=rate.getExtraAdult();
						    	        				    				 extraChildAmount+=rate.getExtraChild();
							        				    				 }else{
							        				    					 baseAmount =  rate.getBaseAmount();
						    	        				    				 extraAdultAmount+=rate.getExtraAdult();
						    	        				    				 extraChildAmount+=rate.getExtraChild();
							        				    				 }
							        				    			 }
						        				    				 rateCount++;
						        				    			 }
						        				    		 }else{
						        				    			 if(propertyRateList.size()==rateIdCount){
						        				    				 baseAmount = accommodations.getBaseAmount();
							            				    		 extraAdultAmount+=accommodations.getExtraAdult();
							        			    				 extraChildAmount+=accommodations.getExtraChild();
						        				    			 }
						            				    	 }
											    		 }
					    			    			 }	    		 
					    				    	 }
					    				    	 else{
					    				    		 baseAmount = accommodations.getBaseAmount();
					    				    		 extraAdultAmount+=accommodations.getExtraAdult();
								    				 extraChildAmount+=accommodations.getExtraChild();
					    				    	 }
					    				    	actualBaseAmount+=baseAmount;
					    				    	String strTypePercent=String.valueOf(promotionPercent);
									    		if(!strTypePercent.equalsIgnoreCase("null")){
									    		   flatAmount = baseAmount-(baseAmount*promotionPercent/100);
									    		}
									    		else{
									    		   flatAmount = baseAmount;
									    		} 
									    		
									    		flatAmount=Math.round(flatAmount);
									    		actualTotal=flatAmount;
							    		  }else if(arlPromoType.contains("L") || arlPromoType.contains("N") || arlPromoType.contains("R")){
									    		promoFlag=true;
									    		List<PmsPromotions> listPromotions= promotionController.listDatePromotions(property.getPropertyId(), promoAccommId, arrivalDate);
												if(listPromotions.size()>0 && !listPromotions.isEmpty()){
										    		 for(PmsPromotions promotions:listPromotions){
										    			 listPromotionDetailCheck.clear();
										    			 listPromotionDetailCheck=promotionDetailController.listPromotionDetails(promotions.getPromotionId(),f.format(d.toDate()).toLowerCase());
										    			 if(listPromotionDetailCheck.size()>0 && !listPromotionDetailCheck.isEmpty()){
										    				 	promotionId=promotions.getPromotionId();
										    				 	promotionType=promotions.getPromotionType();
										    				 	if(promotionType.equalsIgnoreCase("L")){
											    					 listPromotionDetailCheck=promotionDetailController.listPromotionDetails(promotionId,f.format(d.toDate()).toLowerCase());
														    		  if(listPromotionDetailCheck.size()>0){
											    						 for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
											    							 promotionPercent=promoDetails.getPromotionPercentage();
											    							 promotionHours=promoDetails.getPromotionHours();
											    						 }
														    		} 
											    					int rateCount=0,rateIdCount=0;
										    						List<PropertyRate> propertyRateList =  rateController.listAllDates(property.getPropertyId(),accommodations.getAccommodationId(),getSourceId(),d.toDate());
										    						List<PropertyRate> currentDateSmartPriceActive=rateController.listCurrentSmartPriceActive(property.getPropertyId(),accommodations.getAccommodationId(), d.toDate());
										    				    	if(propertyRateList.size()>0)
										    				    	{
										    				    		 for (PropertyRate pr : propertyRateList)
										    			    			 {
														    				 int propertyRateId = pr.getPropertyRateId();
										        				    		 Boolean SmartPriceCheck=pr.getSmartPriceIsActive();
																    		 if(SmartPriceCheck){
																    			 List<PropertyRateDetail> rateDetailList =   detailController.list(propertyRateId,f.format(d.toDate()).toLowerCase());
																	    		 if(rateDetailList.size()>0){
																	    			 for (PropertyRateDetail rate : rateDetailList){
																	    				 if(rateCount<1){
																	    					 SmartPriceEnableCheck=true;
																	    					 baseAmount =  rate.getBaseAmount();
																		    				 extraAdultAmount+=rate.getExtraAdult();
																		    				 extraChildAmount+=rate.getExtraChild();
																		    				 rateCount++;
																	    				 }
																	    			 }
																	    		 }
																    		 }else{
																    			 List<PropertyRateDetail> rateDetailList =   detailController.list(propertyRateId,f.format(d.toDate()).toLowerCase());
											        				    		 if(rateDetailList.size()>0){
											        				    			 if(rateCount<1){
											        				    				 for (PropertyRateDetail rate : rateDetailList){
												        				    				 if(currentDateSmartPriceActive.size()>0){
												        				    					 SmartPriceEnableCheck=true;
												        				    					 baseAmount =  rate.getBaseAmount();
											    	        				    				 extraAdultAmount+=rate.getExtraAdult();
											    	        				    				 extraChildAmount+=rate.getExtraChild();
												        				    				 }else{
												        				    					 baseAmount =  rate.getBaseAmount();
											    	        				    				 extraAdultAmount+=rate.getExtraAdult();
											    	        				    				 extraChildAmount+=rate.getExtraChild();
												        				    				 }
												        				    			 }
											        				    				 rateCount++;
											        				    			 }
											        				    		 }else{
											        				    			 if(propertyRateList.size()==rateIdCount){
											        				    				 baseAmount = accommodations.getBaseAmount();
												            				    		 extraAdultAmount+=accommodations.getExtraAdult();
												        			    				 extraChildAmount+=accommodations.getExtraChild();
											        				    			 }
											            				    	 }
																    		 }
										    			    			 }	    		 
										    				    	 }
										    				    	 else{
										    				    		 baseAmount = accommodations.getBaseAmount();
										    				    		 extraAdultAmount+=accommodations.getExtraAdult();
													    				 extraChildAmount+=accommodations.getExtraChild();
										    				    	 }
										    				    	actualBaseAmount+=baseAmount;
										    				    	String strTypePercent=String.valueOf(promotionPercent);
															    	 if(!strTypePercent.equalsIgnoreCase("null")){
															    		 lastAmount = baseAmount-(baseAmount*promotionPercent/100);
															    	 }
															    	 else{
															    		 lastAmount = baseAmount;
															    	 } 
															    	 lastAmount=Math.round(lastAmount);
															    	 actualTotal=lastAmount;
										    				 }else if(promotionType.equalsIgnoreCase("R")){
										    					 listPromotionDetailCheck=promotionDetailController.listPromotionDetails(promotionId,f.format(d.toDate()).toLowerCase());
										    					 if(listPromotionDetailCheck.size()>0){
										    						 for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
										    							 dblBookRooms=promoDetails.getBookRooms();
										    							 dblGetRooms=promoDetails.getGetRooms();
										    							 promoBaseAmount =promoDetails.getBaseAmount();
										    							 promoAdultAmount =promoDetails.getExtraAdult();
										    							 promoChildAmount =promoDetails.getExtraChild(); 
										    						 }
										    					 }
										    				 }else if(promotionType.equalsIgnoreCase("N")){
										    					 listPromotionDetailCheck=promotionDetailController.listPromotionDetails(promotionId,f.format(d.toDate()).toLowerCase());
										    					 if(listPromotionDetailCheck.size()>0){
										    						 for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
										    							 dblBookNights=promoDetails.getBookNights();
										    							 dblGetNights=promoDetails.getGetNights();
										    							 promoBaseAmount =promoDetails.getBaseAmount();
										    							 promoAdultAmount =promoDetails.getExtraAdult();
										    							 promoChildAmount =promoDetails.getExtraChild(); 
										    						 }
										    					 }
										    				 }
										    			 }
										    		 }
												}
									    	}else{
									    		if(basePromoFlag){
					    				    		 baseAmount = accommodations.getBaseAmount();
					    				    		 extraAdultAmount+=accommodations.getExtraAdult();
								    				 extraChildAmount+=accommodations.getExtraChild();
								    				 actualBaseAmount+=baseAmount;
								    				 actualTotal=baseAmount;
												}else{
													int rateCount=0,rateIdCount=0;
						    						List<PropertyRate> propertyRateList =  rateController.listAllDates(property.getPropertyId(),accommodations.getAccommodationId(),getSourceId(),d.toDate());
						    						List<PropertyRate> currentDateSmartPriceActive=rateController.listCurrentSmartPriceActive(property.getPropertyId(),accommodations.getAccommodationId(), d.toDate());
						    				    	if(propertyRateList.size()>0)
						    				    	{
						    				    		 for (PropertyRate pr : propertyRateList)
						    			    			 {
										    				 int propertyRateId = pr.getPropertyRateId();
						        				    		 Boolean SmartPriceCheck=pr.getSmartPriceIsActive();
												    		 if(SmartPriceCheck){
												    			 List<PropertyRateDetail> rateDetailList =   detailController.list(propertyRateId,f.format(d.toDate()).toLowerCase());
													    		 if(rateDetailList.size()>0){
													    			 for (PropertyRateDetail rate : rateDetailList){
													    				 if(rateCount<1){
													    					 SmartPriceEnableCheck=true;
													    					 baseAmount =  rate.getBaseAmount();
														    				 extraAdultAmount+=rate.getExtraAdult();
														    				 extraChildAmount+=rate.getExtraChild();
														    				 rateCount++;
													    				 }
													    			 }
													    		 }
												    		 }else{
												    			 List<PropertyRateDetail> rateDetailList =   detailController.list(propertyRateId,f.format(d.toDate()).toLowerCase());
							        				    		 if(rateDetailList.size()>0){
							        				    			 if(rateCount<1){
							        				    				 for (PropertyRateDetail rate : rateDetailList){
								        				    				 if(currentDateSmartPriceActive.size()>0){
								        				    					 SmartPriceEnableCheck=true;
								        				    					 baseAmount =  rate.getBaseAmount();
							    	        				    				 extraAdultAmount+=rate.getExtraAdult();
							    	        				    				 extraChildAmount+=rate.getExtraChild();
								        				    				 }else{
								        				    					 baseAmount =  rate.getBaseAmount();
							    	        				    				 extraAdultAmount+=rate.getExtraAdult();
							    	        				    				 extraChildAmount+=rate.getExtraChild();
								        				    				 }
								        				    			 }
							        				    				 rateCount++;
							        				    			 }
							        				    		 }else{
							        				    			 if(propertyRateList.size()==rateIdCount){
							        				    				 baseAmount = accommodations.getBaseAmount();
								            				    		 extraAdultAmount+=accommodations.getExtraAdult();
								        			    				 extraChildAmount+=accommodations.getExtraChild();
							        				    			 }
							            				    	 }
												    		 }
						    			    			 }	    		 
						    				    	 }
						    				    	 else{
						    				    		 baseAmount = accommodations.getBaseAmount();
						    				    		 extraAdultAmount+=accommodations.getExtraAdult();
									    				 extraChildAmount+=accommodations.getExtraChild();
						    				    	 }
						    				    	actualBaseAmount+=baseAmount;
						    				    	actualTotal=baseAmount;
												}
									    	}
										} else{
												if(basePromoFlag){
					    				    		 baseAmount = accommodations.getBaseAmount();
					    				    		 extraAdultAmount+=accommodations.getExtraAdult();
								    				 extraChildAmount+=accommodations.getExtraChild();
								    				 actualBaseAmount+=baseAmount;
								    				 actualTotal=baseAmount;
												}else{
													int rateCount=0,rateIdCount=0;
						    						List<PropertyRate> propertyRateList =  rateController.listAllDates(property.getPropertyId(),accommodations.getAccommodationId(),getSourceId(),d.toDate());
						    						List<PropertyRate> currentDateSmartPriceActive=rateController.listCurrentSmartPriceActive(property.getPropertyId(),accommodations.getAccommodationId(), d.toDate());
						    				    	if(propertyRateList.size()>0)
						    				    	{
						    				    		 for (PropertyRate pr : propertyRateList)
						    			    			 {
										    				 int propertyRateId = pr.getPropertyRateId();
						        				    		 Boolean SmartPriceCheck=pr.getSmartPriceIsActive();
												    		 if(SmartPriceCheck){
												    			 List<PropertyRateDetail> rateDetailList =   detailController.list(propertyRateId,f.format(d.toDate()).toLowerCase());
													    		 if(rateDetailList.size()>0){
													    			 for (PropertyRateDetail rate : rateDetailList){
													    				 if(rateCount<1){
													    					 SmartPriceEnableCheck=true;
													    					 baseAmount =  rate.getBaseAmount();
														    				 extraAdultAmount+=rate.getExtraAdult();
														    				 extraChildAmount+=rate.getExtraChild();
														    				 rateCount++;
													    				 }
													    			 }
													    		 }
												    		 }else{
												    			 List<PropertyRateDetail> rateDetailList =   detailController.list(propertyRateId,f.format(d.toDate()).toLowerCase());
							        				    		 if(rateDetailList.size()>0){
							        				    			 if(rateCount<1){
							        				    				 for (PropertyRateDetail rate : rateDetailList){
								        				    				 if(currentDateSmartPriceActive.size()>0){
								        				    					 SmartPriceEnableCheck=true;
								        				    					 baseAmount =  rate.getBaseAmount();
							    	        				    				 extraAdultAmount+=rate.getExtraAdult();
							    	        				    				 extraChildAmount+=rate.getExtraChild();
								        				    				 }else{
								        				    					 baseAmount =  rate.getBaseAmount();
							    	        				    				 extraAdultAmount+=rate.getExtraAdult();
							    	        				    				 extraChildAmount+=rate.getExtraChild();
								        				    				 }
								        				    			 }
							        				    				 rateCount++;
							        				    			 }
							        				    		 }else{
							        				    			 if(propertyRateList.size()==rateIdCount){
							        				    				 baseAmount = accommodations.getBaseAmount();
								            				    		 extraAdultAmount+=accommodations.getExtraAdult();
								        			    				 extraChildAmount+=accommodations.getExtraChild();
							        				    			 }
							            				    	 }
												    		 }
						    			    			 }	    		 
						    				    	 }
						    				    	 else{
						    				    		 baseAmount = accommodations.getBaseAmount();
						    				    		 extraAdultAmount+=accommodations.getExtraAdult();
									    				 extraChildAmount+=accommodations.getExtraChild();
						    				    	 }
						    				    	actualBaseAmount+=baseAmount;
						    				    	actualTotal=baseAmount;
												}
									    		
									    	}
									    	double smartPriceAmount=0.0;
									    	if(SmartPriceEnableCheck){
									    		 listAccommodationRoom=accommodationController.listPropertyTotalRooms(property.getPropertyId(), accommodations.getAccommodationId());
							      				 if(listAccommodationRoom.size()>0 && !listAccommodationRoom.isEmpty()){
							      					 for(AccommodationRoom roomsList:listAccommodationRoom){
							      						 roomCount=(int) roomsList.getRoomCount();
							      						 long minimum = getAvailableCount(property.getPropertyId(),getArrivalDate(),getDepartureDate(),roomCount,accommodations.getAccommodationId()); 
							      						 availableCount=(int)minimum;
							      						 dblPercentCount=(double)((roomCount-availableCount)*100)/roomCount;
							      						 totalCount=(int) Math.round(dblPercentCount);
							      						 listSmartPriceDetail=detailController.listSmartPriceDetail(totalCount);
							      						 if(listSmartPriceDetail.size()>0 && !listSmartPriceDetail.isEmpty()){
							      							 for(PmsSmartPrice smartPrice: listSmartPriceDetail){
							      								 dblSmartPricePercent=smartPrice.getAmountPercent();
							      								 dblPercentFrom=smartPrice.getPercentFrom();
							      								 dblPercentTo=smartPrice.getPercentTo();
							      								 
							      								 if(dblPercentFrom>=0 && dblPercentTo<=59){
							      									 isNegative=true;
							      								 }else if(dblPercentFrom>=60 && dblPercentTo<=99){
							      									 isPositive=true;
							      								 }
							      								 if(isNegative){
							      									smartPriceAmount=actualTotal-(actualTotal*dblSmartPricePercent/100);
							      								 }
							      								 if(isPositive){
							      									smartPriceAmount=actualTotal+(actualTotal*dblSmartPricePercent/100);
							      								 }
							      							 }
							      						 }
							      					 }
							      				 }
							      				smartPriceAmount=Math.round(smartPriceAmount);
							      				 blnSmartPriceFlag=true;
									    	 }
											 if(smartPriceAmount>0){
												 actualAmount+=smartPriceAmount;
											 }else if(lastAmount>0){
												 actualAmount+=lastAmount;
											 }else if(flatAmount>0){
												 actualAmount+=flatAmount;
											 }else{
												 actualAmount+=baseAmount;
											 }
									    	 dateCount++;
								    	 
								     }
								     Iterator<String> iterPromoType=arlPromoType.iterator();
										while(iterPromoType.hasNext()){
											String strPromoType=iterPromoType.next();
											if(strPromoType.equalsIgnoreCase("F")){
												promotionName=String.valueOf(Math.round(promotionPercent))+" % OFF";
											}else if(strPromoType.equalsIgnoreCase("R")){
												promotionName="Book "+Math.round(dblBookRooms)+" and Get "+Math.round(dblGetRooms)+" Rooms FREE";
											}else if(strPromoType.equalsIgnoreCase("N")){
												promotionName="Book "+Math.round(dblBookNights)+" and Get "+Math.round(dblGetNights)+" Nights FREE";
											}else if(strPromoType.equalsIgnoreCase("L")){
												promotionName=String.valueOf(Math.round(promotionPercent))+" % OFF";
											}
										}
										arlPromoType.clear();
										
										if(promoFlag){
											if(promotionType.equalsIgnoreCase("R") || promotionType.equalsIgnoreCase("N")){
												totalAmount=promoBaseAmount;
												actualAdultAmount=promoAdultAmount;
												actualChildAmount=promoChildAmount; 
											}else if(promotionType.equalsIgnoreCase("F")){
												totalAmount=actualAmount;
												if(dateCount>1){
													actualAdultAmount=extraAdultAmount/dateCount;
													actualChildAmount=extraChildAmount/dateCount;
												}else{
													actualAdultAmount=extraAdultAmount;
													actualChildAmount=extraChildAmount;
												}
											}
											else if(promotionType.equalsIgnoreCase("L")){
												totalAmount=actualAmount;
												if(dateCount>1){
													actualAdultAmount=extraAdultAmount/dateCount;
													actualChildAmount=extraChildAmount/dateCount;
												}else{
													actualAdultAmount=extraAdultAmount;
													actualChildAmount=extraChildAmount;
												}
											}
										}else{
											/*if(blnSmartPriceFlag){
												totalAmount=totalSmartPriceAmount;
											}else{
												totalAmount=baseAmount;
											}*/
											totalAmount=actualAmount;
											if(dateCount>1){
												actualAdultAmount=extraAdultAmount/dateCount;
												actualChildAmount=extraChildAmount/dateCount;
											}else{
												actualAdultAmount=extraAdultAmount;
												actualChildAmount=extraChildAmount;
											}
										}
										
										
										totalAmount=Math.round(totalAmount);
										actualTotalAdultAmount=Math.round(actualAdultAmount);
										actualTotalChildAmount=Math.round(actualChildAmount);
										
								     
								     long minimum = getAvailableCount(property.getPropertyId(),getArrivalDate(),getDepartureDate(),roomCount,accommodations.getAccommodationId());
								     if(minimum==0){ 
								    	 PropertyTaxeManager propertytaxController = new PropertyTaxeManager();
				  	  					 double taxe =  totalAmount/diffInDays;
				  	  					
				  						 PropertyTaxe tax = propertytaxController.find(taxe);
				  						 double taxes = Math.round(totalAmount * tax.getTaxPercentage() / 100  ) ;
				  	 			         jsonOutput += ",\"tax\":\"" + taxes + "\"";
				  	 			         jsonOutput += ",\"roomTax\":\"" + taxes + "\"";
				  	 			         jsonOutput += ",\"taxPercentage\":\"" + tax.getTaxPercentage() + "\"";
							    		 jsonOutput += ",\"available\":\"" + minimum+ "\"";
							    		 jsonOutput += ",\"baseActualAmount\":\"" + + (actualBaseAmount==0 ? accommodations.getBaseAmount() : actualBaseAmount )+  "\"";
							    		 jsonOutput += ",\"baseAmount\":\"" + + (totalAmount==0 ? accommodations.getBaseAmount() : totalAmount )+  "\"";
							    		 jsonOutput += ",\"totalAmount\":\"" + + (totalAmount==0 ? accommodations.getBaseAmount() : totalAmount )+  "\"";
							    		 jsonOutput += ",\"accommodationName\":\"" + accommodations.getAccommodationType().toUpperCase()+ "\"";
										 jsonOutput += ",\"accommodationId\":\"" + accommodations.getAccommodationId()+ "\"";
										 jsonOutput += ",\"promotionName\":\"" + (promotionName==null? "None":promotionName)+ "\"";
										 jsonOutput += ",\"promoPercent\":\"" + (promoPercent==null? 0:promoPercent)+ "\"";	
								     }else{
								    	 jsonOutput += ",\"accommodationName\":\"" + accommodations.getAccommodationType().toUpperCase().trim()+ "\"";
											jsonOutput += ",\"accommodationId\":\"" + accommodations.getAccommodationId()+ "\"";
								    		jsonOutput += ",\"available\":\"" + minimum+ "\"";
								    		jsonOutput += ",\"maxAdult\":\"" + accommodations.getMaxOccupancy()+ "\"";
								    		jsonOutput += ",\"maxChild\":\"" + accommodations.getMaxOccupancy() + "\"";
								    		jsonOutput += ",\"minOccupancy\":\"" + accommodations.getMinOccupancy() + "\"";
											jsonOutput += ",\"maxOccupancy\":\"" + accommodations.getMaxOccupancy() + "\"";
											jsonOutput += ",\"baseActualAmount\":\"" + + (actualBaseAmount==0 ? accommodations.getBaseAmount() : actualBaseAmount )+  "\"";
								    		jsonOutput += ",\"baseAmount\":\"" + + (totalAmount==0 ? accommodations.getBaseAmount() : totalAmount )+  "\"";
								    		jsonOutput += ",\"promoPercent\":\"" + (promoPercent==null? 0:promoPercent)+ "\"";
								    		jsonOutput += ",\"totalAmount\":\"" + + (totalAmount==0 ? accommodations.getBaseAmount() : totalAmount )+  "\"";
								    		jsonOutput += ",\"extraAdultAmount\":\"" + + (actualTotalAdultAmount==0 ? accommodations.getExtraAdult() : actualTotalAdultAmount )+  "\"";
								    		jsonOutput += ",\"extraChildAmount\":\"" + + (actualTotalChildAmount==0 ? accommodations.getExtraChild() : actualTotalChildAmount )+  "\"";
								    		
								    		jsonOutput += ",\"promotionName\":\"" + (promotionName==null? "None":promotionName.trim())+ "\"";
								    		jsonOutput += ",\"promotionId\":\"" + (promotionId==0? 0 :promotionId) + "\"";
								    		jsonOutput += ",\"totalActualAmount\":\"" + + (totalActualAmount==0 ? accommodations.getBaseAmount() : totalActualAmount )+  "\"";
								    		jsonOutput += ",\"totalActualExtraAdultAmount\":\"" + + (totalActualExtraAdultAmount==0 ? accommodations.getExtraAdult() : totalActualExtraAdultAmount )+  "\"";
								    		jsonOutput += ",\"totalActualExtraChildAmount\":\"" + + (totalActualExtraChildAmount==0 ? accommodations.getExtraChild() : totalActualExtraChildAmount )+  "\"";
								    		jsonOutput += ",\"bookRooms\":\"" + (dblBookRooms==0.0? 0:dblBookRooms.intValue())+ "\"";
								    		jsonOutput += ",\"getRooms\":\"" + (dblGetRooms==0? 0:dblGetRooms.intValue()) + "\"";
								    		jsonOutput += ",\"getNights\":\"" + (dblGetNights==0.0? 0 :dblGetNights.intValue())+ "\"";
								    		jsonOutput += ",\"bookNights\":\"" + (dblBookNights==0? 0:dblBookNights.intValue()) + "\"";
								    		
								    		jsonOutput += ",\"discountPercent\":\"" + (discountPercent==0.0? "0":discountPercent)+ "\"";
								    		jsonOutput += ",\"discountId\":\"" + (discountId==0? 0 :discountId) + "\"";
								    		jsonOutput += ",\"smartPricePercent\":\"" + (smartPricePercent==0.0? "0":smartPricePercent)+ "\"";
								    		jsonOutput += ",\"smartPriceId\":\"" + (smartPriceId==0? 0:smartPriceId) + "\"";
								    		 double taxe =  totalAmount/diffInDays;
					  	  					 PropertyTaxeManager propertytaxController = new PropertyTaxeManager();
					  						 PropertyTaxe tax = propertytaxController.find(taxe);
					  						 double taxes = Math.round(totalAmount * tax.getTaxPercentage() / 100) ;
					  	 			       jsonOutput += ",\"tax\":\"" + taxes + "\"";
					  	 			      jsonOutput += ",\"roomTax\":\"" + taxes + "\"";
					  	 			      jsonOutput += ",\"taxPercentage\":\"" + tax.getTaxPercentage() + "\"";
								     
								     
								     }
								    	
								}
								
							}
							
							
							jsonOutput += "}";
						}
					}
				}
			}
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
//			sessionMap.clear();
		}
		catch(Exception e){
			logger.error(e);
			e.printStackTrace();
		}
    	return null;
    }
    
    
    
    
	public String getLocationCount() throws IOException {
 		try {
 			String jsonOutput = "";
 			HttpServletResponse response = ServletActionContext.getResponse();
 			response.setContentType("application/json");
 			LocationManager locationController=new LocationManager();
 			List<Location> listLocationCount =locationController.list(); 
 			if(listLocationCount.size()>0 && !listLocationCount.isEmpty()){
 				if (!jsonOutput.equalsIgnoreCase(""))
 					jsonOutput += ",{";
 				else
 					jsonOutput += "{";
 				
 			    jsonOutput += "\"count\":\"" + listLocationCount.size() + "\"";
 			    
 				jsonOutput += "}";
 			}
 			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
 		} catch (Exception e) {
 			logger.error(e);
 			e.printStackTrace();
 		} finally {

 		}

 		return null;

 	}
	
	public String getListPropertiesApi(Hotels hotels){
		String output=null;
		try {
			
			Algorithm algorithm = Algorithm.HMAC256("secret");
			int count=0;
			long nowMillis = System.currentTimeMillis();
		    Date now = new Date(nowMillis+15*60*1000);
		    java.util.Date dateStart = null;
		    java.util.Date dateEnd =null;
			Map mapDate=new HashMap();
			java.util.Date date=new java.util.Date();
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			if(hotels.getStartDate()!=null && hotels.getStartDate()!=null){
				dateStart = format.parse(hotels.getStartDate());
			    dateEnd = format.parse(hotels.getEndDate());	
			}else{
				Calendar calStart=Calendar.getInstance();
			    calStart.setTime(date);
			    dateStart=calStart.getTime();
			    
			    Calendar calEnd=Calendar.getInstance();
			    calEnd.setTime(date);
			    calEnd.add(Calendar.DATE, 1);
			    dateEnd=calEnd.getTime();
			}
		    
		    
		    if(getSourceTypeId()==null){
		    	this.sourceTypeId=1;
		    }
		    
		    Calendar calCheckStart=Calendar.getInstance();
		    calCheckStart.setTime(dateStart);
		    java.util.Date checkInDate = calCheckStart.getTime();
		    this.arrivalDate=new Timestamp(checkInDate.getTime());
		   
		    
		    Calendar calCheckEnd=Calendar.getInstance();
		    calCheckEnd.setTime(dateEnd);
		    java.util.Date checkOutDate = calCheckEnd.getTime();
		    this.departureDate=new Timestamp(checkOutDate.getTime());
		 
	     
			/*this.arrivalDate = getArrivalDate();
		    sessionMap.put("arrivalDate",arrivalDate); 
			
			this.departureDate = getDepartureDate();
			sessionMap.put("departureDate",departureDate); */
			mapDate.put("arrivalDate", arrivalDate);
			mapDate.put("departureDate", departureDate);
			
			PmsPropertyManager propertyController = new PmsPropertyManager();		
			PropertyAccommodationManager accommodationManager = new PropertyAccommodationManager();
			PropertyAmenityManager amenityController = new PropertyAmenityManager();
			List<PropertyAmenity> propertyAmenityList;
			PropertyTypeManager typeController =  new PropertyTypeManager();
			PropertyRateManager rateController = new PropertyRateManager();
			PropertyRateDetailManager detailController = new PropertyRateDetailManager();
			PmsAmenityManager ameController = new PmsAmenityManager();
			PromotionManager promotionController=new PromotionManager();
			PromotionDetailManager promotionDetailController=new PromotionDetailManager();
			PmsBookingManager bookingController=new PmsBookingManager();
			PropertyRatePlanDetailManager planDetailController=new PropertyRatePlanDetailManager();
			
			HashMap<Double,Integer > mapAmountAccommId=new HashMap<Double, Integer>();
			HashMap<Double,Integer > mapSoldOutAmountAccommId=new HashMap<Double, Integer>();
			
			ArrayList<Integer> arlSortPropertyId=new ArrayList<Integer>();
			HashMap<Integer, Integer> mapAvailablePropertyId=new HashMap<Integer, Integer>();
			String jsonOutput = "",id="",type="";
			HttpServletResponse response = ServletActionContext.getResponse();
			
			response.setContentType("application/json");
			
			 
			Calendar cal = Calendar.getInstance();
			cal.setTime(this.departureDate);
		    cal.add(Calendar.DATE, -1);
		    
		    java.util.Date dteCheckOut = cal.getTime();
		    this.departureDate=new Timestamp(dteCheckOut.getTime());
		    
		    java.util.Date curdate=new java.util.Date();
        	Calendar curDate=Calendar.getInstance();
        	curDate.setTime(date);
        	curDate.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
        	int bookedHours=curDate.get(Calendar.HOUR);
        	int bookedMinute=curDate.get(Calendar.MINUTE);
        	int bookedSecond=curDate.get(Calendar.SECOND);
        	int AMPM=curDate.get(Calendar.AM_PM);
        	java.util.Date cudate=curDate.getTime();
        	
        	String strBookingDate=new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH).format(curDate.getTime());
        	String strCurrentDate=new SimpleDateFormat("yyyy-MM-dd").format(curDate.getTime());
        	String strCheckInDate=new SimpleDateFormat("yyyy-MM-dd").format(checkInDate.getTime());
        	String hours="",minutes="",seconds="",timeHours="";
        	if(bookedHours<10){
        		hours=String.valueOf(bookedHours);
        		hours="0"+hours;
        	}else{
        		hours=String.valueOf(bookedHours);
        	}
        	if(bookedMinute<10){
        		minutes=String.valueOf(bookedMinute);
        		minutes="0"+minutes;
        	}else{
        		minutes=String.valueOf(bookedMinute);
        	}
        	
        	if(bookedMinute<10){
        		seconds=String.valueOf(bookedSecond);
        		seconds="0"+seconds;
        	}else{
        		seconds=String.valueOf(bookedSecond);
        	}
        	
        	if(AMPM==0){//If the current time is AM
        		timeHours=strBookingDate+" "+hours+" : "+minutes+" : "+seconds+" AM ";
        	}else if(AMPM==1){//If the current time is PM
        		timeHours=strBookingDate+" "+hours+" : "+minutes+" : "+seconds+" PM ";
        	}
        	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        	
        	String checkStartDate = new SimpleDateFormat("yyyy-MM-dd").format(curdate);
	    	String strStartTime=checkStartDate+" 12:00:00"; 
	   	    java.util.Date dteStartDate = sdf.parse(strStartTime);
	   	    Calendar calStart=Calendar.getInstance();
	   	    calStart.setTime(dteStartDate);
	   	    calStart.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
	   	    String StartDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calStart.getTime());
	    	java.util.Date lastFirst = sdf.parse(StartDate);
	    	boolean isHotelTypeFilter=false;
			String startDate = new SimpleDateFormat("yyyy-MM-dd").format(this.arrivalDate);
			String endDate = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
			DateTime start = DateTime.parse(startDate);
		    DateTime end = DateTime.parse(endDate);
		    DecimalFormat df = new DecimalFormat("###.##");
		    DateFormat f = new SimpleDateFormat("EEEE");
		    String[] weekdays={"Monday","Tuesday","Wednesday","Thursday","Sunday"};
		    String[] weekend={"Friday","Saturday"};
		    PropertyAccommodationInventoryManager inventoryController= new PropertyAccommodationInventoryManager();
		    List<AccommodationRoom> listAccommodationRoom=null;
			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
			PropertyReviewsManager reviewController=new PropertyReviewsManager();
		    int availRoomCount=0,soldRoomCount=0;
		    this.sourceTypeId=1;
		    double commissionrange1=150,commissionrange2=225,commissionrange3=300,
		    		lastdiscountpercent1=10,lastdiscountpercent2=15,lastdiscountpercent3=20;
		    SeoUrlAction urlcheck=new SeoUrlAction();
		    
		    List<GooglePropertyAreas> listPropertyAreas=null;
		    List<PmsProperty> listPmsProperty=null;
		    List<PmsProperty> listProperties=new ArrayList<PmsProperty>();
		    ArrayList<Integer> arlcheck=new ArrayList<Integer>();
		    ArrayList<Integer> arlTripTypeChk=new ArrayList<Integer>();
		    ArrayList<Integer> arlHotelTypeChk=new ArrayList<Integer>();
		    
		    if(hotels.getAreaType()!=null || hotels.getHotelType()!=null || hotels.getTripType()!=null){

	    		listPropertyAreas = propertyController.listPropertyByGoogleArea(hotels.getAreaType());
		 	    if(listPropertyAreas.size()>0 && !listPropertyAreas.isEmpty()){
		 	    	for(GooglePropertyAreas propertyareas: listPropertyAreas){
		 	    		int areaPropertyId=propertyareas.getPmsProperty().getPropertyId();
		 	    		List<PmsProperty> listProperty=propertyController.list(areaPropertyId);
		 	    		if(listProperty.size()>0 && !listProperty.isEmpty()){
		 	    			for(PmsProperty property:listProperty){
		 	    				int PropertyId=property.getPropertyId();
 	    						if(!arlcheck.contains(PropertyId)){
 	    							arlcheck.add(PropertyId);
 	    						}
		 	    			}
		 	    		}
		 	    	}
		 	    }
		 	    if(arlcheck.size()>0 && !arlcheck.isEmpty()){
		 	    	Iterator<Integer> iter=arlcheck.iterator();
		 	    	while(iter.hasNext()){
		 	    		int propertyId=iter.next();
		 	    		if(hotels.getTripType()!=null){

		    				List<PropertyTags>  listPropertyTag = propertyController.findTag(propertyId,hotels.getTripType());
	 	    				if(listPropertyTag.size()>0 && !listPropertyTag.isEmpty()){
	 	    					for(PropertyTags tag:listPropertyTag){
	 	    						int tagPropertyId=tag.getPmsProperty().getPropertyId();
 	    							List<PmsProperty> listTagProperty=propertyController.list(tagPropertyId);
	 	    						if(listTagProperty.size()>0 && !listTagProperty.isEmpty()){
	 	    							for(PmsProperty tagproperty:listTagProperty){
	 	    								Integer hotelTypeId=tagproperty.getPropertyType().getPropertyTypeId();
	 	    								Integer typePropertyId=tagproperty.getPropertyId();
	 	    								if(hotels.getHotelType()!=null){
	 	    									if(hotels.getHotelType().contains(String.valueOf(hotelTypeId))){
		 	    									isHotelTypeFilter=true;
		 	    									if(isHotelTypeFilter){
		 	    										if(!arlHotelTypeChk.contains(typePropertyId)){
		 	    											arlHotelTypeChk.add(typePropertyId);
		 	    											listProperties.add(tagproperty);
		 	    										}
		 	    											
			 	    								}
		 	    								}
	 	    								}else{
	 	    									if(!arlHotelTypeChk.contains(typePropertyId)){
 	    											arlHotelTypeChk.add(typePropertyId);
 	    											listProperties.add(tagproperty);
 	    										}
	 	    									
	 	    								}
	 	    							}
	 	    						}	
	 	    					}
	 	    				}	
		 			   }else{
		 					List<PmsProperty> listNoTagProperty=propertyController.list(propertyId);
		 					if(listNoTagProperty.size()>0 && !listNoTagProperty.isEmpty()){
		 						for(PmsProperty tagproperty:listNoTagProperty){
		 							Integer hotelTypeId=tagproperty.getPropertyType().getPropertyTypeId();
    								Integer typePropertyId=tagproperty.getPropertyId();
    								if(hotels.getHotelType()!=null){
    									if(hotels.getHotelType().contains(String.valueOf(hotelTypeId))){
	    									isHotelTypeFilter=true;
	    									if(isHotelTypeFilter){
	    										if(!arlHotelTypeChk.contains(typePropertyId)){
	    											arlHotelTypeChk.add(typePropertyId);
	    											listProperties.add(tagproperty);
	    										}
	    											
	 	    								}
	    								}
    								}else{
	    								if(!arlHotelTypeChk.contains(typePropertyId)){
 											arlHotelTypeChk.add(typePropertyId);
 											listProperties.add(tagproperty);
 										}
	    									
	    							}
		 							
		 						}
		 					}
		 		 	    }
		 	    	}
		 	    }
		 	   
	    	
		    }else{
		    	String stroutput=urlcheck.getSearchLocationIdByUrl(hotels.getLocationUrl());
			    StringTokenizer stoken=new StringTokenizer(stroutput,",");
			    while(stoken.hasMoreElements()){
			    	id=stoken.nextToken();
			    	type=stoken.nextToken();
				}
			    
			    
	 	    	if(type.equals("location")){
			    	listPmsProperty = propertyController.listPropertyByLocation(Integer.parseInt(id));
			    	if(listPmsProperty.size()>0 && !listPmsProperty.isEmpty()){
			    		for(PmsProperty property:listPmsProperty){
			    			if(hotels.getTripTypeId()!=null){
			    				List<PropertyTags>  listPropertyTag = propertyController.findTag(property.getPropertyId(),hotels.getTripTypeId());
		 	    				if(listPropertyTag.size()>0 && !listPropertyTag.isEmpty()){
		 	    					for(PropertyTags tag:listPropertyTag){
		 	    						int tagPropertyId=tag.getPmsProperty().getPropertyId();
		 	    						if(!arlcheck.contains(tagPropertyId)){
		 	    							arlcheck.add(tagPropertyId);
		 	    							List<PmsProperty> listTagProperty=propertyController.list(tagPropertyId);
			 	    						if(listTagProperty.size()>0 && !listTagProperty.isEmpty()){
			 	    							for(PmsProperty tagproperty:listTagProperty){
			 	    								listProperties.add(tagproperty);			
			 	    							}
			 	    						}	
		 	    						}
		 	    						
		 	    					}
		 	    				}	
			    			}else{
			    				List<PmsProperty> listNoTagProperty=propertyController.list(property.getPropertyId());
	    						if(listNoTagProperty.size()>0 && !listNoTagProperty.isEmpty()){
	    							for(PmsProperty tagproperty:listNoTagProperty){
	    								listProperties.add(tagproperty);			
	    							}
	    						}
			    	 	    }
			    			
			    		}
			    	}
			    }else if(type.equals("area")){
			    	
			    	listPropertyAreas = propertyController.listPropertyByGoogleArea(Integer.parseInt(id));
			 	    if(listPropertyAreas.size()>0 && !listPropertyAreas.isEmpty()){
			 	    	for(GooglePropertyAreas propertyareas: listPropertyAreas){
			 	    		int areaPropertyId=propertyareas.getPmsProperty().getPropertyId();
			 	    		List<PmsProperty> listProperty=propertyController.list(areaPropertyId);
			 	    		if(listProperty.size()>0 && !listProperty.isEmpty()){
			 	    			for(PmsProperty property:listProperty){
			 	    				if(hotels.getTripTypeId()!=null){
			 	    					List<PropertyTags>  listPropertyTag = propertyController.findTag(property.getPropertyId(),hotels.getTripTypeId());
				 	    				if(listPropertyTag.size()>0 && !listPropertyTag.isEmpty()){
				 	    					for(PropertyTags tag:listPropertyTag){
				 	    						int tagPropertyId=tag.getPmsProperty().getPropertyId();
				 	    						if(!arlcheck.contains(tagPropertyId)){
				 	    							arlcheck.add(tagPropertyId);
				 	    							List<PmsProperty> listTagProperty=propertyController.list(tagPropertyId);
					 	    						if(listTagProperty.size()>0 && !listTagProperty.isEmpty()){
					 	    							for(PmsProperty tagproperty:listTagProperty){
					 	    								listProperties.add(tagproperty);			
					 	    							}
					 	    						}
				 	    						}
				 	    						
				 	    					}
				 	    				}	
			 	    				}else{
			 	    					List<PmsProperty> listNoTagProperty=propertyController.list(property.getPropertyId());
		 	    						if(listNoTagProperty.size()>0 && !listNoTagProperty.isEmpty()){
		 	    							for(PmsProperty tagproperty:listNoTagProperty){
		 	    								listProperties.add(tagproperty);			
		 	    							}
		 	    						}
			 	    		 	    }
			 	    			}
			 	    		}
			 	    		
			 	    	}
			 	    }
			    }
		    }
			for (PmsProperty property : listProperties) {
				
				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"propertyId\":\"" + property.getPropertyId()+ "\"";
				jsonOutput += ",\"propertyName\":\"" + (property.getPropertyName() == null ? "": property.getPropertyName().toUpperCase().trim())+ "\"";
				jsonOutput += ",\"propertyUrl\":\"" + (property.getPropertyUrl() == null ? "": property.getPropertyUrl().toLowerCase().trim())+ "\"";
				if(property.getGoogleLocation()!=null){
					jsonOutput += ",\"locationUrl\":\"" + (property.getGoogleLocation().getGoogleLocationUrl() == null ? "NA": property.getGoogleLocation().getGoogleLocationUrl().toLowerCase().trim())+ "\"";	
				}else{
					jsonOutput += ",\"locationUrl\":\"" + ("NA")+ "\"";
				}
				jsonOutput += ",\"city\":\"" + property.getCity() + "\"";
				jsonOutput += ",\"hotelTypeId\":\"" + (property.getPropertyType().getPropertyTypeId() == null ? "": property.getPropertyType().getPropertyTypeId())+ "\"";
				if(property.getLocation()!=null){
					jsonOutput += ",\"locationName\":\"" + (property.getLocation().getLocationName() == null ? "": property.getLocation().getLocationName().toUpperCase().trim())+ "\"";	
				}else{
					jsonOutput += ",\"locationName\":\"" + "" + "\"";
				}
				jsonOutput += ",\"payAtHotelStatus\":\"" + (property.getPayAtHotelStatus() == null ? false: property.getPayAtHotelStatus())+ "\"";
				jsonOutput += ",\"coupleStatus\":\"" + ( property.getCoupleIsActive() == null? false :  property.getCoupleIsActive() )+ "\"";
				jsonOutput += ",\"breakfastStatus\":\"" + ( property.getBreakfastIsActive() == null? false:  property.getBreakfastIsActive() )+ "\"";
				jsonOutput += ",\"hotelStatus\":\"" + ( property.getHotelIsActive() == null? false:  property.getHotelIsActive() )+ "\"";
				jsonOutput += ",\"soldoutStatus\":\"" + (property.getStopSellIsActive() == null ? false: property.getStopSellIsActive())+ "\"";
				jsonOutput += ",\"delistStatus\":\"" + ( property.getDelistIsActive() == null? false :  property.getDelistIsActive() )+ "\"";
				String imagePath="";
				if ( property.getPropertyThumbPath() != null) {
					imagePath = getText("storage.aws.property.photo") + "/uloimg/property/"
							+ property.getPropertyId()+"/"+property.getPropertyThumbPath();
				} 
				
				jsonOutput += ",\"propertyImage\":\"" + ( imagePath )+ "\"";
				String latitude=null,longitude=null;
				
				latitude=property.getLatitude();
				longitude=property.getLongitude();
				if(latitude!=null){
					latitude=latitude.trim();
				}
				if(longitude!=null){
					longitude=longitude.trim();
				}
				jsonOutput += ",\"latitude\":\"" + ( latitude==null?"":latitude.trim())+ "\"";
				jsonOutput += ",\"longitude\":\"" + ( longitude == null? "":  longitude.trim())+ "\"";
				SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
				Timestamp FromDate=(Timestamp)mapDate.get("arrivalDate");
				Timestamp ToDate=(Timestamp)mapDate.get("departureDate");
				jsonOutput += ",\"arrivalDate\":\"" + format2.format(FromDate) + "\"";
				jsonOutput += ",\"departureDate\":\"" + format2.format(ToDate)+ "\"";
				
				String strFromDate=format2.format(FromDate);
				String strToDate=format2.format(ToDate);
		    	DateTime startdate = DateTime.parse(strFromDate);
		        DateTime enddate = DateTime.parse(strToDate);
		        java.util.Date fromdate = startdate.toDate();
		 		java.util.Date todate = enddate.toDate();
		 		 
				int diffInDays = (int) ((todate.getTime() - fromdate.getTime()) / (1000 * 60 * 60 * 24));
				
				List arllist=new ArrayList();
				List arllistAvl=new ArrayList();
				List arllistSold=new ArrayList();
				jsonOutput += ",\"diffDays\":\"" + diffInDays+ "\"";
				int starCount=0;
				
				List<PropertyReviews> listReviews=reviewController.listReview(property.getPropertyId());
				if(listReviews.size()>0 && !listReviews.isEmpty()){
					for(PropertyReviews reviews:listReviews){
						if(reviews.getStarCount()==null){
							starCount=0;
						}else{
							starCount=Math.round(reviews.getStarCount());
						}
						
					}
				}
				jsonOutput += ",\"starCount\":\"" + starCount+ "\"";
				String jsonTags="";
				List<PmsTags> tag = propertyController.listTag();
				if(tag.size()>0 && !tag.isEmpty()){
					for (PmsTags tagList : tag) {
						
						List<PropertyTags>  propertyTag = propertyController.findTag(property.getPropertyId(),tagList.getTagId());	
						if (!jsonTags.equalsIgnoreCase(""))
							jsonTags += ",{";
						else
							jsonTags += "{";
						
						jsonTags += "\"tagId\":\"" + tagList.getTagId() + "\"";	
						jsonTags += ",\"tagName\":\"" + tagList.getTagName() + "\"";	
						if(propertyTag.size() == 0){
							jsonTags += ",\"status\":\"" + false + "\"";
						}else if(propertyTag.size() != 0){
							jsonTags += ",\"status\":\"" + true + "\"";
						}
						jsonTags += "}";
					}
					jsonOutput += ",\"tags\":[" + jsonTags+ "]";
				}
				
				
				ArrayList<Integer> arlLastImage=new ArrayList<Integer>();
				String jsonPhotos="",strPhotos="",data="",strData="";
				String jsonExteriour = "",exteriorphoto="";
				String jsonRestaurant = "",restaurantphoto="";
				String jsonRooms = "",roomphoto="";
				String jsonActivityArea = "",activityareaphoto="";
				String jsonBedRoom = "",bedroomphoto="";
				String jsonRestRoom = "",restroomphoto="";
				boolean blnImgActivityArea=false,blnImgRestaurant=false,blnImgReception=false,
	 					blnImgExterior=false,blnImgRestRoom=false,blnImgBedRoom=false;
				StringBuilder imagebuilder=new StringBuilder();
				PropertyPhotoManager photoController = new PropertyPhotoManager();
				
			    
			    List<PropertyPhoto> listPhotos =photoController.list(property.getPropertyId());

			    if(listPhotos.size()>0){
		        	  for (PropertyPhoto photo : listPhotos) {
			          		if(photo.getPmsPhotoCategory()!=null && photo.getPmsPhotoCategory().getPhotoCategoryId()==5){
			          			if(!arlLastImage.contains(photo.getPmsPhotoCategory().getPhotoCategoryId())){
			          				arlLastImage.add(photo.getPmsPhotoCategory().getPhotoCategoryId());	
			          			}
			          			
			          		}
		        	  }
		        	  for (PropertyPhoto photo : listPhotos) {
			          		if(photo.getPmsPhotoCategory()!=null && photo.getPmsPhotoCategory().getPhotoCategoryId()==6){
			          			if(!arlLastImage.contains(photo.getPmsPhotoCategory().getPhotoCategoryId())){
			          				arlLastImage.add(photo.getPmsPhotoCategory().getPhotoCategoryId());	
			          			}
			          		}
		        	  }
		        	  for (PropertyPhoto photo : listPhotos) {
			          		if(photo.getPmsPhotoCategory()!=null && photo.getPmsPhotoCategory().getPhotoCategoryId()==1){
			          			if(!arlLastImage.contains(photo.getPmsPhotoCategory().getPhotoCategoryId())){
			          				arlLastImage.add(photo.getPmsPhotoCategory().getPhotoCategoryId());	
			          			}
			          		}
		        	  }
		        	  for (PropertyPhoto photo : listPhotos) {
			          		if(photo.getPmsPhotoCategory()!=null && photo.getPmsPhotoCategory().getPhotoCategoryId()==2){
			          			if(!arlLastImage.contains(photo.getPmsPhotoCategory().getPhotoCategoryId())){
			          				arlLastImage.add(photo.getPmsPhotoCategory().getPhotoCategoryId());	
			          			}
			          		}
		        	  }
		        	  for (PropertyPhoto photo : listPhotos) {
			          		if(photo.getPmsPhotoCategory()!=null && photo.getPmsPhotoCategory().getPhotoCategoryId()==3){
			          			if(!arlLastImage.contains(photo.getPmsPhotoCategory().getPhotoCategoryId())){
			          				arlLastImage.add(photo.getPmsPhotoCategory().getPhotoCategoryId());	
			          			}
			          		}
		        	  }
		        	  for (PropertyPhoto photo : listPhotos) {
			          		if(photo.getPmsPhotoCategory()!=null && photo.getPmsPhotoCategory().getPhotoCategoryId()==4){
			          			if(!arlLastImage.contains(photo.getPmsPhotoCategory().getPhotoCategoryId())){
			          				arlLastImage.add(photo.getPmsPhotoCategory().getPhotoCategoryId());	
			          			}
			          		}
		        	  }
		          }
		          int lastelement=0,nthelement=0;
		          if(arlLastImage.size()>0){
		        	  lastelement=arlLastImage.get(arlLastImage.size()-1);  
		          }
	          if(listPhotos.size()>0)
			  {
		          	for (PropertyPhoto photo : listPhotos) {
		          		if(photo.getPmsPhotoCategory()!=null && photo.getPmsPhotoCategory().getPhotoCategoryId()==5){
		          			blnImgBedRoom=true;
		          			nthelement=photo.getPmsPhotoCategory().getPhotoCategoryId();
		          			if (!jsonBedRoom.equalsIgnoreCase(""))
		          				jsonBedRoom += ",{";
							else
								jsonBedRoom += "{";
			          		this.photoPath = getText("storage.aws.property.photo") + "/uloimg/"
									+ photo.getPmsProperty().getPropertyId()+"/accommodation/"+photo.getPropertyAccommodation().getAccommodationId()
									+"/"+photo.getPmsPhotoCategory().getPhotoCategoryId()+"/"+photo.getPhotoPath();
						
			          		jsonBedRoom += "\"original\":\"" + this.photoPath + "\"";
			          		jsonBedRoom += ",\"thumbnail\":\"" + this.photoPath+ "\"";
			          		this.altName=photo.getImageAltName()==null?"NA":photo.getImageAltName();
							jsonBedRoom += ",\"altName\":\"" +this.altName + "\"";
			          		jsonBedRoom += "}";	
		          		}
		
		  			 }
		          	
		          	data="\"images\":[" + jsonBedRoom + "]";
		 			strData=data.replaceAll("\"", "\"");
		 			
		 			bedroomphoto="{\"name\":\"bedroom\"," + strData+ "}";
		 			if(blnImgBedRoom){
		 				if(nthelement==lastelement){
		 					imagebuilder.append(bedroomphoto);
		 				}else{
		 					imagebuilder.append(bedroomphoto+",");	
		 				}
		 				
		 			}
		 			for (PropertyPhoto photo : listPhotos) {
		          		if(photo.getPmsPhotoCategory()!=null && photo.getPmsPhotoCategory().getPhotoCategoryId()==6){
		          			blnImgRestRoom=true;
		          			nthelement=photo.getPmsPhotoCategory().getPhotoCategoryId();
		          			if (!jsonRestRoom.equalsIgnoreCase(""))
		          				jsonRestRoom += ",{";
							else
								jsonRestRoom += "{";
			          		this.photoPath = getText("storage.aws.property.photo") + "/uloimg/"
									+ photo.getPmsProperty().getPropertyId()+"/accommodation/"+photo.getPropertyAccommodation().getAccommodationId()
									+"/"+photo.getPmsPhotoCategory().getPhotoCategoryId()+"/"+photo.getPhotoPath();
						
			          		jsonRestRoom += "\"original\":\"" + this.photoPath + "\"";
			          		jsonRestRoom += ",\"thumbnail\":\"" + this.photoPath+ "\"";
			          		this.altName=photo.getImageAltName()==null?"NA":photo.getImageAltName();
							jsonRestRoom += ",\"altName\":\"" +this.altName + "\"";
			          		jsonRestRoom += "}";	
		          		}
		
		  			 }
		          	
		          	data="\"images\":[" + jsonRestRoom + "]";
		 			strData=data.replaceAll("\"", "\"");
		 			
		 			restroomphoto="{\"name\":\"bathroom\"," + strData+ "}";
		 			if(blnImgRestRoom){
		 				if(nthelement==lastelement){
		 					imagebuilder.append(restroomphoto);
		 				}else{
		 					imagebuilder.append(restroomphoto+",");
		 				}
		 				
		 			}
		          	for (PropertyPhoto photo : listPhotos) {
		    			
		          		if(photo.getPmsPhotoCategory()!=null && photo.getPmsPhotoCategory().getPhotoCategoryId() == 1){
		          			blnImgExterior=true;
		          			nthelement=photo.getPmsPhotoCategory().getPhotoCategoryId();
							if (!jsonExteriour.equalsIgnoreCase(""))
								jsonExteriour += ",{";
							else
								jsonExteriour += "{";
								
							this.photoPath = getText("storage.aws.property.photo") + "/uloimg/"
									+ photo.getPmsProperty().getPropertyId()+"/category/"+photo.getPmsPhotoCategory().getPhotoCategoryId()+"/"+photo.getPhotoPath();
							jsonExteriour += "\"original\":\"" + this.photoPath + "\"";
							jsonExteriour += ",\"thumbnail\":\"" + this.photoPath+ "\"";
							this.altName=photo.getImageAltName()==null?"NA":photo.getImageAltName();
							jsonExteriour += ",\"altName\":\"" +this.altName + "\"";
							jsonExteriour += "}";
		          		}
		
		          		
		
		  			 }
		          	data="";strData="";
		          	data="\"images\":[" + jsonExteriour + "]";
		 			strData=data.replaceAll("\"", "\"");
		 			
		 			exteriorphoto="{\"name\":\"exterior\"," + strData+ "}";
		 			if(blnImgExterior){
		 				if(nthelement==lastelement){
		 					imagebuilder.append(exteriorphoto);
		 				}else{
		 					imagebuilder.append(exteriorphoto+",");	
		 				}
		 				
		 			}
		          	for (PropertyPhoto photo : listPhotos) {
		    			
		          		if(photo.getPmsPhotoCategory()!=null && photo.getPmsPhotoCategory().getPhotoCategoryId() == 2){
		          			blnImgReception=true;
		          			nthelement=photo.getPmsPhotoCategory().getPhotoCategoryId();
							if (!jsonRooms.equalsIgnoreCase(""))
								jsonRooms += ",{";
							else
								jsonRooms += "{";
							this.photoPath = getText("storage.aws.property.photo") + "/uloimg/"
									+ photo.getPmsProperty().getPropertyId()+"/category/"+photo.getPmsPhotoCategory().getPhotoCategoryId()+"/"+photo.getPhotoPath();
							
							jsonRooms += "\"original\":\"" + this.photoPath + "\"";
							jsonRooms += ",\"thumbnail\":\"" + this.photoPath+ "\"";
							this.altName=photo.getImageAltName()==null?"NA":photo.getImageAltName();
							jsonRooms += ",\"altName\":\"" +this.altName + "\"";
							jsonRooms += "}";
		          		}
		  			 }
		          	data="";strData="";
		          	data="\"images\":[" + jsonRooms + "]";
		 			strData=data.replaceAll("\"", "\"");
		 			
		 			roomphoto="{\"name\":\"reception\"," + strData+ "}";
		 			if(blnImgReception){
		 				if(nthelement==lastelement){
		 					imagebuilder.append(roomphoto);
		 				}else{
		 					imagebuilder.append(roomphoto+",");	
		 				}
		 				
		 			}
		          	for (PropertyPhoto photo : listPhotos) {
		    			
		          		if(photo.getPmsPhotoCategory()!=null && photo.getPmsPhotoCategory().getPhotoCategoryId() == 3){
		          			blnImgRestaurant=true;
		          			nthelement=photo.getPmsPhotoCategory().getPhotoCategoryId();
							if (!jsonRestaurant.equalsIgnoreCase(""))
								jsonRestaurant += ",{";
							else
								jsonRestaurant += "{";
							this.photoPath = getText("storage.aws.property.photo") + "/uloimg/"
									+ photo.getPmsProperty().getPropertyId()+"/category/"+photo.getPmsPhotoCategory().getPhotoCategoryId()+"/"+photo.getPhotoPath();
							jsonRestaurant += "\"original\":\"" + this.photoPath + "\"";
							jsonRestaurant += ",\"thumbnail\":\"" + this.photoPath+ "\"";
							this.altName=photo.getImageAltName()==null?"NA":photo.getImageAltName();
							jsonRestaurant += ",\"altName\":\"" +this.altName + "\"";
							jsonRestaurant += "}";
		          		}
		  			 }
		          	data="";strData="";
		          	data="\"images\":[" + jsonRestaurant + "]";
		 			strData=data.replaceAll("\"", "\"");
		 			
		 			restaurantphoto="{\"name\":\"restaurant\"," + strData+ "}";
		 			if(blnImgRestaurant){
		 				if(nthelement==lastelement){
		 					imagebuilder.append(restaurantphoto);
		 				}else{
		 					imagebuilder.append(restaurantphoto+",");	
		 				}
		 				
		 			}
					for (PropertyPhoto photo : listPhotos) {
						
							if(photo.getPmsPhotoCategory()!=null && photo.getPmsPhotoCategory().getPhotoCategoryId() == 4){
								blnImgActivityArea=true;
								nthelement=photo.getPmsPhotoCategory().getPhotoCategoryId();
								if (!jsonActivityArea.equalsIgnoreCase(""))
									jsonActivityArea += ",{";
								else
									jsonActivityArea += "{";
							
								this.photoPath = getText("storage.aws.property.photo") + "/uloimg/"
										+ photo.getPmsProperty().getPropertyId()+"/category/"+photo.getPmsPhotoCategory().getPhotoCategoryId()+"/"+photo.getPhotoPath();
								
								jsonActivityArea += "\"original\":\"" + this.photoPath + "\"";
								jsonActivityArea += ",\"thumbnail\":\"" + this.photoPath+ "\"";
								this.altName=photo.getImageAltName()==null?"NA":photo.getImageAltName();
								jsonActivityArea += ",\"altName\":\"" +this.altName + "\"";
								jsonActivityArea += "}";
							}
					 }
					data="";strData="";
					data="\"images\":[" + jsonActivityArea + "]";
		 			strData=data.replaceAll("\"", "\"");
		 			
		 			activityareaphoto="{\"name\":\"others\"," + strData+ "}";
		 			
		 			if(blnImgActivityArea){
		 				if(nthelement==lastelement){
		 					imagebuilder.append(activityareaphoto);
		 				}else{
		 					imagebuilder.append(activityareaphoto);	
		 				}
		 				
		 			}		
		 			jsonOutput += ",\"gallery\":[" + imagebuilder+"]";
//	                 jsonOutput += ",\"gallery\":[" + bedroomphoto+","+restroomphoto+ ","+exteriorphoto+","+roomphoto+","+restaurantphoto+","+activityareaphoto+"]";
				
				}
				
				String jsonAmenities ="";
				propertyAmenityList =  amenityController.list(property.getPropertyId());
				if(propertyAmenityList.size()>0)
				{
					for (PropertyAmenity amenity : propertyAmenityList) {
						if (!jsonAmenities.equalsIgnoreCase(""))
							
							jsonAmenities += ",{";
						else
							jsonAmenities += "{";
						PropertyAmenity amentity1 = amenityController.find(amenity.getPropertyAmenityId());
					    String path = "ulowebsite/images/icons/";
						PmsAmenity pmsAmenity1 =ameController.find(amentity1.getPmsAmenity().getAmenityId());
						jsonAmenities += "\"amenityId\":\"" + amenity.getPropertyAmenityId()+ "\"";					
						jsonAmenities += ",\"amenityName\":\"" + pmsAmenity1.getAmenityName()+ "\"";
	                   jsonAmenities += ",\"amenityIcon\":\"" + pmsAmenity1.getAmenityIcon()+ "\"";
	            
						
						jsonAmenities += "}";
					}
					
					jsonOutput += ",\"amenities\":[" + jsonAmenities+ "]";
				}
				
				String jsonAccommodation ="";
				List<PropertyAccommodation> accommodationsList =  accommodationManager.list(property.getPropertyId());
				int soldOutCount=0,availCount=0,promotionAccommId=0;
				if(accommodationsList.size()>0 && !accommodationsList.isEmpty())
				{
					for (PropertyAccommodation accommodation : accommodationsList) {
						int roomCount=0;
						listAccommodationRoom=accommodationController.listPropertyTotalRooms(property.getPropertyId(), accommodation.getAccommodationId());
						if(listAccommodationRoom.size()>0 && !listAccommodationRoom.isEmpty()){
							for(AccommodationRoom roomsList:listAccommodationRoom){
								 roomCount=(int) roomsList.getRoomCount();
								 long minimum = getAvailableCount(property.getPropertyId(),getArrivalDate(),getDepartureDate(),roomCount,accommodation.getAccommodationId());
								 if((int)minimum>0){
									 availCount++;
								 }else if((int)minimum==0){
									 soldOutCount++;
								 }
								 break;
							 }
						 }
						
						
						if (!jsonAccommodation.equalsIgnoreCase(""))
							
							jsonAccommodation += ",{";
						else
							jsonAccommodation += "{";
							jsonAccommodation += "\"accommodationId\":\"" + accommodation.getAccommodationId()+ "\"";
							jsonAccommodation += ",\"accommodationName\":\"" + accommodation.getAccommodationType().toUpperCase()+ "\"";
						
						jsonAccommodation += "}";
					}
					
				}
	
				
				
				List<PmsPromotionDetails> listPromotionDetailCheck=null;
				Boolean blnAvailable=false,blnSoldout=false,firstPromotionFlag=false,secondPromotionFlag=false,promotionFlag=false,blnPromoFlag=false;
				int arlListTotalAccomm=0,soldOutTotalAccomm=0,accommCount=0;
				String promotionType="NA",discountType="NA",promotionName="NA";
				double dblDiscountINR=0,dblDiscountPercent=0;
				boolean promoStatus=false,isFlat=false,isEarly=false,isLast=false,isSoldout=false;
				ArrayList<String> arlListPromoType=new ArrayList<String>();
				List<PmsPromotions> listPromotions=promotionController.listDatePromotions(property.getPropertyId(), dateStart);
				 if(listPromotions.size()>0 && !listPromotions.isEmpty()){
				   	for(PmsPromotions promotion:listPromotions){
				   		if(promotion.getPromotionType().equals("F")){
				   			promotionType=promotion.getPromotionType();
				   			promoStatus=true;
				   			isFlat=true;
				   		}else if(promotion.getPromotionType().equals("E")){
				   			promotionType=promotion.getPromotionType();
				   			Timestamp tsStartPromo=promotion.getFirstRangeStartDate();
				   			Timestamp tsEndPromo=promotion.getFirstRangeEndDate();
				   			String startStayDate = new SimpleDateFormat("yyyy-MM-dd").format(tsStartPromo);
							String endStayDate = new SimpleDateFormat("yyyy-MM-dd").format(tsEndPromo);
							DateTime dtstart = DateTime.parse(startStayDate);
						    DateTime dtend = DateTime.parse(endStayDate);
				   			List<DateTime> between = DateUtil.getDateRange(dtstart, dtend);
				   			if(between.size()>0 && !between.isEmpty()){
				   				for(DateTime d:between){
				   					if(cudate.equals(d.toDate())){
				   						promoStatus=true;
				   						isEarly=true;
				   					}
				   				}
				   			}else{
				   				promoStatus=false;
				   			}
				   		}
				   		
						List<PmsPromotionDetails> listPromoDetails=promotionDetailController.listPromotionDetails(promotion.getPromotionId(),f.format(dateStart).toLowerCase());
						if(listPromoDetails.size()>0 && !listPromoDetails.isEmpty()){
							for(PmsPromotionDetails promodetails:listPromoDetails){
								dblDiscountINR=promodetails.getPromotionInr();
								if(dblDiscountINR>0){
									discountType="INR";
								}
								dblDiscountPercent=promodetails.getPromotionPercentage();
								if(dblDiscountPercent>0){
									discountType="PERCENT";
								}
							}
						}
					}
				 }
				 
				 
				List<PropertyAccommodation> accommodationList =  accommodationManager.list(property.getPropertyId());
				if(accommodationList.size()>0)
				{
					for (PropertyAccommodation accommodation : accommodationList) {
						
						int dateCount=0;
						double amount=0.0,extraAdultAmount=0.0,extraChildAmount=0.0;
						int roomCount=0,availableCount=0,totalCount=0;
					    double dblPercentCount=0.0;
		    			listAccommodationRoom=accommodationController.listPropertyTotalRooms(property.getPropertyId(), accommodation.getAccommodationId());
		    			List<DateTime> between = DateUtil.getDateRange(start, end); 
						if(listAccommodationRoom.size()>0 && !listAccommodationRoom.isEmpty()){
							
							for(AccommodationRoom roomsList:listAccommodationRoom){
								roomCount=(int) roomsList.getRoomCount();
								if(roomCount>0){
									long minimum = getAvailableCount(property.getPropertyId(),getArrivalDate(),getDepartureDate(),roomCount,accommodation.getAccommodationId());
									if((int)minimum>0){
										blnAvailable=true;
										blnSoldout=false;
										arlListTotalAccomm++;
									}else if((int)minimum==0){
										blnSoldout=true;
										blnAvailable=false;
										soldOutTotalAccomm++;
									}
								}
								 break;
							 }
						 }
						 
					     for (DateTime d : between)
					     {
							 
					    	 long rmc = 0;
				   			 this.roomCnt = rmc;
				        	 int sold=0;
				        	 java.util.Date availDate = d.toDate();
				        	 String days=f.format(d.toDate()).toLowerCase();
							 PropertyAccommodationInventory inventoryCount = inventoryController.findInventoryCount(availDate,accommodation.getAccommodationId());
							 if(inventoryCount != null){
							 PmsAvailableRooms roomCount1 = bookingController.findBookingCount(availDate, accommodation.getAccommodationId(),inventoryCount.getInventoryId());
							 if(roomCount1.getRoomCount() == null) {
							 	String num1 = inventoryCount.getInventoryCount();
								PmsAvailableRooms bookingRoomCount=bookingController.findCount(availDate, accommodation.getAccommodationId());
								PropertyAccommodation accomm=accommodationController.find(accommodation.getAccommodationId());
								Integer totavailable=0;
								if(accommodation!=null){
									totavailable=accommodation.getNoOfUnits();
								}
								if(bookingRoomCount.getRoomCount()!=null){
									long lngRooms=bookingRoomCount.getRoomCount();
									long num2 = Long.parseLong(num1);
									this.roomCnt = num2;
									sold=(int) (lngRooms);
									int availRooms=0;
									availRooms=totavailable-sold;
									/*if(Integer.parseInt(num1)>availRooms){
										this.roomCnt = availRooms;	
									}else{
										this.roomCnt = num2;
									}*/
									this.roomCnt = num2;		
								}else{
									 long num2 = Long.parseLong(num1);
									 this.roomCnt = num2;
									 sold=(int) (rmc);
								}
							}else{		
								PmsAvailableRooms bookingRoomCount=bookingController.findCount(availDate, accommodation.getAccommodationId());
								int intRooms=0;
								Integer totavailable=0,availableRooms=0;
								if(accommodation!=null){
									totavailable=accommodation.getNoOfUnits();
								}
								if(bookingRoomCount.getRoomCount()!=null){
									long lngRooms=bookingRoomCount.getRoomCount();
									long num = roomCount1.getRoomCount();	
									intRooms=(int)lngRooms;
									String num1 = inventoryCount.getInventoryCount();
				 					long num2 = Long.parseLong(num1);
				 					availableRooms=totavailable-intRooms;
				 					/*if(num2>availableRooms){
				 						this.roomCnt = availableRooms;	 
				 					}else{
				 						this.roomCnt = num2-num;
				 					}*/
				 					this.roomCnt = num2-num;
				 					long lngSold=bookingRoomCount.getRoomCount();
				   					sold=(int)lngSold;
								}else{
									long num = roomCount1.getRoomCount();	
									String num1 = inventoryCount.getInventoryCount();
									long num2 = Long.parseLong(num1);
				  					this.roomCnt = (num2-num);
				  					long lngSold=roomCount1.getRoomCount();
				   					sold=(int)lngSold;
								}
			
							}
											
						}else{
							  PmsAvailableRooms inventoryCounts = bookingController.findCount(availDate, accommodation.getAccommodationId());							
								if(inventoryCounts.getRoomCount() == null){								
									this.roomCnt = (accommodation.getNoOfUnits()-rmc);
									sold=(int)rmc;
								}
								else{
									this.roomCnt = (accommodation.getNoOfUnits()-inventoryCounts.getRoomCount());	
									long lngSold=inventoryCounts.getRoomCount();
		   							sold=(int)lngSold;
								}
						}
											
						double totalRooms=0,intSold=0;
						totalRooms=(double)accommodation.getNoOfUnits();
						intSold=(double)sold;
						double occupancy=0.0;
						occupancy=intSold/totalRooms;
						double sellrate=0.0,otarate=0.0;
						Integer occupancyRating=(int) Math.round(occupancy * 100);
					    double minAmount=0,maxAmount=0,promoAmount=0,firstPromoAmount=0,secondPromoAmount=0;
					    
					    
					    boolean isWeekdays=false,isWeekend=false;
						for(int a=0;a<weekdays.length;a++){
							if(days.equalsIgnoreCase(weekdays[a])){
								isWeekdays=true;
							}
						}
						
						for(int b=0;b<weekend.length;b++){
							if(days.equalsIgnoreCase(weekend[b])){
								isWeekend=true;
							}
						}
						if(property.getLocationType()!=null){
							if(property.getLocationType().getLocationTypeName().equalsIgnoreCase("Leisure")){
								if(isWeekdays){
									if(accommodation.getWeekdaySellingRate()==null){
										maxAmount=0;	
									}else{
										maxAmount=accommodation.getWeekdaySellingRate();
									}
										
								}
								
								if(isWeekend){
									if(accommodation.getWeekendSellingRate()==null){
										maxAmount=0;
									}else{
										maxAmount=accommodation.getWeekendSellingRate();	
									}
									
								}	
							}
							
							if(property.getLocationType().getLocationTypeName().equalsIgnoreCase("Metro")){
								if(accommodation.getSellingRate()==null){
									maxAmount=0;
								}else{
									maxAmount=accommodation.getSellingRate();	
								}
									
							}
							
							if(occupancyRating>=0 && occupancyRating<=30){
								sellrate=maxAmount;
							}else if(occupancyRating>30 && occupancyRating<=60){
								sellrate=maxAmount+(maxAmount*10/100);
							}else if(occupancyRating>=60 && occupancyRating<=80){
								sellrate=maxAmount+(maxAmount*15/100);
							}else if(occupancyRating>80){
								sellrate=maxAmount+(maxAmount*20/100);
							}
							
						}
						
	    				
						
						
						if(discountType.equals("INR")){
							firstPromoAmount=dblDiscountINR;
						}else if(discountType.equals("PERCENT")){
							firstPromoAmount=(maxAmount*dblDiscountPercent/100);
						}else{
							firstPromoAmount=0;
						}
						
						if(strCheckInDate.equals(strCurrentDate)){ 
							if(occupancyRating>=0 && occupancyRating<=30){
								if(this.roomCnt>0){
									isLast=true;
								}else{
									isLast=false;
								}
							}
						}
						
	    				if(isLast){
	    					if(occupancyRating>=0 && occupancyRating<=30){
	    						if(AMPM==0){//If the current time is AM
	    		            		if(bookedHours>=0 && bookedHours<=11){
	    		            			if(maxAmount<1000){
	    									if(occupancyRating>=0 && occupancyRating<=30){
	    										secondPromoAmount=commissionrange1;
	    									}
	    							    }else if(maxAmount>=1000 && maxAmount<=1500){
	    							    	if(occupancyRating>=0 && occupancyRating<=30){
	    							    		secondPromoAmount=commissionrange2;
	    									}
	    							    }else if(maxAmount>1500){
	    							    	if(occupancyRating>=0 && occupancyRating<=30){
	    							    		secondPromoAmount=commissionrange3;
	    									}
	    							    }
	    		            		}
	    		            	}
	    						if(AMPM==1){
	    							if(bookedHours>=0 && bookedHours<=3){
	    								if(occupancyRating>=0 && occupancyRating<=30){
	    									secondPromoAmount=(sellrate*lastdiscountpercent1/100);	
	    								}
	    								
	    		            		}else if(bookedHours>=4 && bookedHours<=7){
	    		            			if(occupancyRating>=0 && occupancyRating<=30){
	    		            				secondPromoAmount=(sellrate*lastdiscountpercent2/100);	
	    								}
	    		            			
	    		            		}else if(bookedHours>=8 && bookedHours<=11){
	    		            			if(occupancyRating>=0 && occupancyRating<=30){
	    		            				secondPromoAmount=(sellrate*lastdiscountpercent3/100);	
	    								}
	    		            			
	    		            		}
	    						}
	    					}
	    				}
	    				promoAmount=firstPromoAmount+secondPromoAmount;
	    				amount+=sellrate - promoAmount;
				    	 dateCount++;
				    	 if(dateCount==diffInDays){
				    		 if(blnAvailable){
			    				mapAmountAccommId.put(amount,accommodation.getAccommodationId());
				      			arllist.add(amount);
		      				 }else if(blnSoldout){
		      					mapSoldOutAmountAccommId.put(amount,accommodation.getAccommodationId());
				      			arllist.add(amount);
					    	 }
				    	 }
				     }
				     if(blnAvailable){
				    	 arllistAvl.add(amount);
				     }else if(!blnAvailable){
				    	 arllistSold.add(amount);
				     }
				     accommCount++;
					}
				}
				//end of accommodation
				double totalAmount=0;
				boolean blnJSONContent=false;
				Collections.sort(arllist);
				int arlListSize=0,accommId=0,intTotalAccomm=0;
			     intTotalAccomm=accommodationList.size();
			     if(intTotalAccomm==accommCount){
			    	 if(intTotalAccomm==soldOutCount){
					     arlListSize=arllist.size();
					     if(soldOutCount==arlListSize){
					    	 Collections.sort(arllist);
					    	 for(int i=0;i<arlListSize;i++){
						    	 totalAmount=(Double) arllist.get(i);
						    	 if((Double) arllist.get(0)==(Double) arllist.get(i)){
						    		 if(blnAvailable){
						    			 accommId=mapAmountAccommId.get(totalAmount);
							    		 blnJSONContent=true;
						    		 }else{
						    			 accommId=mapSoldOutAmountAccommId.get(totalAmount);
							    		 blnJSONContent=true;
						    		 }
						    		 break;
						    	 }else{
						    		 
						    	 }
						     }
					     }
			    	 }else{
					     arlListSize=arllist.size();
					     Collections.sort(arllist);
				    	 for(int i=0;i<arlListSize;i++){
				    		 if(arllistAvl.contains((Double) arllist.get(i))){
				    			 if((Double) arllist.get(0)==(Double) arllist.get(i)){
					    			 totalAmount=(Double) arllist.get(i);
					    			 accommId=mapAmountAccommId.get(totalAmount);
						    		 blnJSONContent=true;
				    			 }else{
				    				 totalAmount=(Double) arllist.get(i);
					    			 accommId=mapAmountAccommId.get(totalAmount);
						    		 blnJSONContent=true;
				    			 }
				    			 break;
				    		 }
					     }
			    	 }
			     }
			     arlListPromoType.clear();  
				if(blnJSONContent){
				    listAccommodationRoom.clear(); 
			    	listAccommodationRoom=accommodationController.listPropertyTotalRooms(property.getPropertyId(), accommId);
			    	List<DateTime> betweenAmount = DateUtil.getDateRange(start, end);
			    	double actualPromoBaseAmount=0.0,actualBaseAmount=0.0,actualTotalAmount=0.0,actualTotalAdultAmount=0.0,actualTotalChildAmount=0.0,
			    			actualAdultAmount=0.0,actualChildAmount=0.0;
				    int countDate=0; 
				    boolean blnStatus=false;
				    
				    double baseAmount=0.0,extraAdultAmount=0.0,extraChildAmount=0.0,totalBaseAmount=0.0,
				    		totalMinimumAmount=0,totalMaximumAmount=0,totalSellRate=0;
				    int roomCount=0,availableCount=0,totalCount=0,discountId=0;
				    
					if(listAccommodationRoom.size()>0 && !listAccommodationRoom.isEmpty()){
						for(AccommodationRoom roomsList:listAccommodationRoom){
							 roomCount=(int) roomsList.getRoomCount();
							 break;
						 }
					 }
					isLast=false;
					List<PropertyAccommodation> listAccommodation =  accommodationManager.list(property.getPropertyId(),accommId);
					int dateCount=0;
					for (PropertyAccommodation accommodations : listAccommodation) {
						
						double firstInr=0,secondInr=0;
						for (DateTime d : betweenAmount){
	
					    	 long rmc = 0;
				   			 this.roomCnt = rmc;
				        	 int sold=0;
				        	 java.util.Date availDate = d.toDate();
				        	 String days=f.format(d.toDate()).toLowerCase();
							 PropertyAccommodationInventory inventoryCount = inventoryController.findInventoryCount(availDate,accommodations.getAccommodationId());
							 if(inventoryCount != null){
							 PmsAvailableRooms roomCount1 = bookingController.findBookingCount(availDate, accommodations.getAccommodationId(),inventoryCount.getInventoryId());
							 if(roomCount1.getRoomCount() == null) {
							 	String num1 = inventoryCount.getInventoryCount();
								PmsAvailableRooms bookingRoomCount=bookingController.findCount(availDate, accommodations.getAccommodationId());
								Integer totavailable=0;
								if(accommodations!=null){
									totavailable=accommodations.getNoOfUnits();
								}
								if(bookingRoomCount.getRoomCount()!=null){
									long lngRooms=bookingRoomCount.getRoomCount();
									long num2 = Long.parseLong(num1);
									this.roomCnt = num2;
									sold=(int) (lngRooms);
									int availRooms=0;
									availRooms=totavailable-sold;
									if(Integer.parseInt(num1)>availRooms){
										this.roomCnt = availRooms;	
									}else{
										this.roomCnt = num2;
									}
											
								}else{
									 long num2 = Long.parseLong(num1);
									 this.roomCnt = num2;
									 sold=(int) (rmc);
								}
							}else{		
								PmsAvailableRooms bookingRoomCount=bookingController.findCount(availDate, accommodations.getAccommodationId());
								int intRooms=0;
								Integer totavailable=0,availableRooms=0;
								if(accommodations!=null){
									totavailable=accommodations.getNoOfUnits();
								}
								if(bookingRoomCount.getRoomCount()!=null){
									long lngRooms=bookingRoomCount.getRoomCount();
									long num = roomCount1.getRoomCount();	
									intRooms=(int)lngRooms;
									String num1 = inventoryCount.getInventoryCount();
				 					long num2 = Long.parseLong(num1);
				 					availableRooms=totavailable-intRooms;
				 					if(num2>availableRooms){
				 						this.roomCnt = availableRooms;	 
				 					}else{
				 						this.roomCnt = num2-num;
				 					}
				 					long lngSold=bookingRoomCount.getRoomCount();
				   					sold=(int)lngSold;
								}else{
									long num = roomCount1.getRoomCount();	
									String num1 = inventoryCount.getInventoryCount();
									long num2 = Long.parseLong(num1);
				  					this.roomCnt = (num2-num);
				  					long lngSold=roomCount1.getRoomCount();
				   					sold=(int)lngSold;
								}
			
							}
											
						}else{
							  PmsAvailableRooms inventoryCounts = bookingController.findCount(availDate, accommodations.getAccommodationId());							
								if(inventoryCounts.getRoomCount() == null){								
									this.roomCnt = (accommodations.getNoOfUnits()-rmc);
									sold=(int)rmc;
								}
								else{
									this.roomCnt = (accommodations.getNoOfUnits()-inventoryCounts.getRoomCount());	
									long lngSold=inventoryCounts.getRoomCount();
		   							sold=(int)lngSold;
								}
							}
												
							double totalRooms=0,intSold=0;
							totalRooms=(double)accommodations.getNoOfUnits();
							intSold=(double)sold;
							double occupancy=0.0;
							occupancy=intSold/totalRooms;
							double sellrate=0.0,otarate=0.0;
							Integer occupancyRating=(int) Math.round(occupancy * 100);
						    int rateCount=0,rateIdCount=0;
						    double minAmount=0,maxAmount=0,promoAmount=0,firstPromoAmount=0,secondPromoAmount=0;
		    				
		    				boolean isWeekdays=false,isWeekend=false;
							for(int a=0;a<weekdays.length;a++){
								if(days.equalsIgnoreCase(weekdays[a])){
									isWeekdays=true;
								}
							}
							
							for(int b=0;b<weekend.length;b++){
								if(days.equalsIgnoreCase(weekend[b])){
									isWeekend=true;
								}
							}
							if(property.getLocationType()!=null){
								if(property.getLocationType().getLocationTypeName().equalsIgnoreCase("Leisure")){
									if(isWeekdays){
										if(accommodations.getWeekdaySellingRate()==null){
											maxAmount=0;	
										}else{
											maxAmount=accommodations.getWeekdaySellingRate();
										}
											
									}
									
									if(isWeekend){
										if(accommodations.getWeekendSellingRate()==null){
											maxAmount=0;
										}else{
											maxAmount=accommodations.getWeekendSellingRate();	
										}
										
									}	
								}
								
								if(property.getLocationType().getLocationTypeName().equalsIgnoreCase("Metro")){
									if(accommodations.getSellingRate()==null){
										maxAmount=0;	
									}else{
										maxAmount=accommodations.getSellingRate();
									}
										
								}
								
								if(occupancyRating>=0 && occupancyRating<=30){
									sellrate=maxAmount;
								}else if(occupancyRating>30 && occupancyRating<=60){
									sellrate=maxAmount+(maxAmount*10/100);
								}else if(occupancyRating>=60 && occupancyRating<=80){
									sellrate=maxAmount+(maxAmount*15/100);
								}else if(occupancyRating>80){
									sellrate=maxAmount+(maxAmount*20/100);
								}
							}
		    				
//							sellrate=maxAmount;
							if(discountType.equals("INR")){
								firstPromoAmount=dblDiscountINR;
								firstInr+=dblDiscountINR;
							}else if(discountType.equals("PERCENT")){
								firstPromoAmount=(sellrate*dblDiscountPercent/100);
							}else{
								firstPromoAmount=0;
							}
							
							if(strCheckInDate.equals(strCurrentDate)){ 
								if(occupancyRating>=0 && occupancyRating<=30){
									if(this.roomCnt>0){
										isLast=true;
									}else{
										isLast=false;
									}
								}
							}
							
							if(isLast){
		    					if(occupancyRating>=0 && occupancyRating<=30){
		    						if(AMPM==0){//If the current time is AM
		    		            		if(bookedHours>=0 && bookedHours<=11){
		    		            			if(maxAmount<1000){
		    									if(occupancyRating>=0 && occupancyRating<=30){
		    										secondPromoAmount=commissionrange1;
		    										secondInr+=commissionrange1;
		    									}
		    							    }else if(maxAmount>=1000 && maxAmount<=1500){
		    							    	if(occupancyRating>=0 && occupancyRating<=30){
		    							    		secondPromoAmount=commissionrange2;
		    							    		secondInr+=commissionrange2;
		    									}
		    							    }else if(maxAmount>1500){
		    							    	if(occupancyRating>=0 && occupancyRating<=30){
		    							    		secondPromoAmount=commissionrange3;
		    							    		secondInr+=commissionrange3;
		    									}
		    							    }
		    		            		}
		    		            	}
		    						if(AMPM==1){
		    							if(bookedHours>=0 && bookedHours<=3){
		    								secondPromoAmount=(sellrate*lastdiscountpercent1/100);
		    		            		}else if(bookedHours>=4 && bookedHours<=7){
		    		            			secondPromoAmount=(sellrate*lastdiscountpercent2/100);
		    		            		}else if(bookedHours>=8 && bookedHours<=11){
		    		            			secondPromoAmount=(sellrate*lastdiscountpercent3/100);
		    		            		}
		    						}
		    					}
		    				}
		    				promoAmount=firstPromoAmount+secondPromoAmount;
		    				
		    				
							totalMinimumAmount+=sellrate-promoAmount;
							totalMaximumAmount+=sellrate;
							extraAdultAmount+=accommodations.getExtraAdult();
							extraChildAmount+=accommodations.getExtraChild();
					    	countDate++;
					    }
						
						
						String promotionFirstName=null,promotionSecondName=null,promotionThirdName=null,
								promotionFirstType=null,promotionSecondType=null;
						
						if(isFlat){
							if(discountType.equalsIgnoreCase("INR")){
								promotionFirstName=String.valueOf(Math.round(firstInr))+"";
								promotionFirstType=discountType.toLowerCase();
							}else if(discountType.equalsIgnoreCase("PERCENT")){
								promotionFirstName=String.valueOf(Math.round(dblDiscountPercent))+"";
								promotionFirstType=discountType.toLowerCase();
							}
						}
						if(isEarly){
							if(discountType.equalsIgnoreCase("INR")){
								promotionFirstName=String.valueOf(Math.round(firstInr))+"";
								promotionFirstType=discountType.toLowerCase();
							}else if(discountType.equalsIgnoreCase("PERCENT")){
								promotionFirstName=String.valueOf(Math.round(dblDiscountPercent))+"";
								promotionFirstType=discountType.toLowerCase();
							}
						}
						if(isLast){
							if(AMPM==0){//If the current time is AM
    		            		if(bookedHours>=0 && bookedHours<=11){
    		            
    		            			String displayDate = new SimpleDateFormat("MMM dd,yyyy HH:mm:ss").format(lastFirst.getTime());
    								this.displayHoursForTimer=displayDate;
    								setDisplayHoursForTimer(this.displayHoursForTimer);

    								promotionSecondName=String.valueOf(Math.round(secondInr))+"";
    								promotionSecondType="inr";
    		            		}
    		            	}
    						if(AMPM==1){
    							if(bookedHours>=0 && bookedHours<=3){
    								String firstTime=checkStartDate+" 16:00:00"; 
    						   	    java.util.Date dteFirstDate = sdf.parse(strStartTime);
    						   	    Calendar calFirst=Calendar.getInstance();
    						   	    calFirst.setTime(dteFirstDate);
    						   	    calFirst.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
    						   	    String firstDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calFirst.getTime());
    						    	java.util.Date dteFirst = sdf.parse(firstTime);
    								String displayDate = new SimpleDateFormat("MMM dd,yyyy HH:mm:ss").format(dteFirst.getTime());
    								this.displayHoursForTimer=displayDate;
    								setDisplayHoursForTimer(this.displayHoursForTimer);
    								
    								promotionSecondName=String.valueOf(Math.round(lastdiscountpercent1))+"";
    								promotionSecondType="percent";
    		            		}else if(bookedHours>=4 && bookedHours<=7){
    		            			String firstTime=checkStartDate+" 20:00:00"; 
    						   	    java.util.Date dteFirstDate = sdf.parse(strStartTime);
    						   	    Calendar calFirst=Calendar.getInstance();
    						   	    calFirst.setTime(dteFirstDate);
    						   	    calFirst.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
    						   	    String firstDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calFirst.getTime());
    						    	java.util.Date dteFirst = sdf.parse(firstTime);
    								String displayDate = new SimpleDateFormat("MMM dd,yyyy HH:mm:ss").format(dteFirst.getTime());
    								this.displayHoursForTimer=displayDate;
    								setDisplayHoursForTimer(this.displayHoursForTimer);
    								promotionSecondType="percent";
    								promotionSecondName=String.valueOf(Math.round(lastdiscountpercent2))+"";
    		            		}else if(bookedHours>=8 && bookedHours<=11){
    		            			String firstTime=checkStartDate+" 23:00:00"; 
    						   	    java.util.Date dteFirstDate = sdf.parse(strStartTime);
    						   	    Calendar calFirst=Calendar.getInstance();
    						   	    calFirst.setTime(dteFirstDate);
    						   	    calFirst.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
    						   	    String firstDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calFirst.getTime());
    						    	java.util.Date dteFirst = sdf.parse(firstTime);
    								String displayDate = new SimpleDateFormat("MMM dd,yyyy HH:mm:ss").format(dteFirst.getTime());
    								this.displayHoursForTimer=displayDate;
    								setDisplayHoursForTimer(this.displayHoursForTimer);
    								promotionSecondType="percent";
    								promotionSecondName=String.valueOf(Math.round(lastdiscountpercent3))+"";
    		            		}
    						}

						}
						actualAdultAmount=extraAdultAmount;
						actualChildAmount=extraChildAmount;
					
						
						
						double variationAmount=0,variationRating=0;
						variationAmount=totalMaximumAmount-totalMinimumAmount;
						variationRating=variationAmount/totalMaximumAmount*100;
						if(variationRating>0){
							promotionThirdName=String.valueOf(Math.round(variationRating))+"%";	
						}
						
						actualTotalAdultAmount=Math.round(actualAdultAmount);
						actualTotalChildAmount=Math.round(actualChildAmount);
						
						
		            	
						long minimum = getAvailableCount(property.getPropertyId(),getArrivalDate(),getDepartureDate(),roomCount,accommId);
				    	 if(minimum<0){

				    		 jsonOutput += ",\"available\":\"" + minimum+ "\"";
				    		 jsonOutput += ",\"minimumAmount\":\"" + (totalMinimumAmount==0 ? 0 : Math.round(totalMinimumAmount) )+  "\"";
				    		 jsonOutput += ",\"maximumAmount\":\"" +(totalMaximumAmount==0 ? 0 : Math.round(totalMaximumAmount) )+  "\"";
				    		 jsonOutput += ",\"offerAmount\":\"" +(Math.round(totalMaximumAmount-totalMinimumAmount))+  "\"";
				    		 jsonOutput += ",\"promotionFirstName\":\"" + (promotionFirstName==null? "NA":promotionFirstName)+ "\"";
				    		 jsonOutput += ",\"promotionSecondName\":\"" + (promotionSecondName==null? "NA":promotionSecondName)+ "\"";
				    		 jsonOutput += ",\"promotionThirdName\":\"" + (promotionThirdName==null? "NA":promotionThirdName)+ "\"";
				    		 jsonOutput += ",\"promotionFirstType\":\"" + (promotionFirstType==null? "NA":promotionFirstType)+ "\"";
					    	 jsonOutput += ",\"promotionSecondType\":\"" + (promotionSecondType==null? "NA":promotionSecondType)+ "\"";
				    		 jsonOutput += ",\"accommodationName\":\"" + accommodations.getAccommodationType().toUpperCase()+ "\"";
							 jsonOutput += ",\"accommodationId\":\"" + accommodations.getAccommodationId()+ "\"";
							 isSoldout=true;
							 jsonOutput += ",\"isSoldout\":\"" + isSoldout+ "\"";
							 jsonOutput += ",\"isFlat\":\"" + isFlat+ "\"";
							 jsonOutput += ",\"isEarly\":\"" + isEarly+ "\"";
							 jsonOutput += ",\"isLast\":\"" + isLast+ "\"";
							 if(isLast){
								jsonOutput += ",\"promoLastMinuteTimer\":\"" + this.displayHoursForTimer+ "\"";	
							}else{
								jsonOutput += ",\"promoLastMinuteTimer\":\"" + "NA"+ "\"";
							}
					     
				    	 }else if(minimum==0){
				    		 jsonOutput += ",\"available\":\"" + minimum+ "\"";
				    		 jsonOutput += ",\"minimumAmount\":\"" + (totalMinimumAmount==0 ? 0 : Math.round(totalMinimumAmount) )+  "\"";
				    		 jsonOutput += ",\"maximumAmount\":\"" +(totalMaximumAmount==0 ? 0 : Math.round(totalMaximumAmount) )+  "\"";
				    		 jsonOutput += ",\"offerAmount\":\"" +(Math.round(totalMaximumAmount-totalMinimumAmount))+  "\"";
				    		 jsonOutput += ",\"promotionFirstName\":\"" + (promotionFirstName==null? "NA":promotionFirstName)+ "\"";
				    		 jsonOutput += ",\"promotionSecondName\":\"" + (promotionSecondName==null? "NA":promotionSecondName)+ "\"";
				    		 jsonOutput += ",\"promotionThirdName\":\"" + (promotionThirdName==null? "NA":promotionThirdName)+ "\"";
				    		 jsonOutput += ",\"promotionFirstType\":\"" + (promotionFirstType==null? "NA":promotionFirstType)+ "\"";
					    	 jsonOutput += ",\"promotionSecondType\":\"" + (promotionSecondType==null? "NA":promotionSecondType)+ "\"";
				    		 jsonOutput += ",\"accommodationName\":\"" + accommodations.getAccommodationType().toUpperCase()+ "\"";
							 jsonOutput += ",\"accommodationId\":\"" + accommodations.getAccommodationId()+ "\"";
							 isSoldout=true;
							 jsonOutput += ",\"isSoldout\":\"" + isSoldout+ "\"";
							 jsonOutput += ",\"isFlat\":\"" + isFlat+ "\"";
							 jsonOutput += ",\"isEarly\":\"" + isEarly+ "\"";
							 jsonOutput += ",\"isLast\":\"" + isLast+ "\"";
							 if(isLast){
								jsonOutput += ",\"promoLastMinuteTimer\":\"" + this.displayHoursForTimer+ "\"";	
							}else{
								jsonOutput += ",\"promoLastMinuteTimer\":\"" + "NA"+ "\"";
							}
					     }else{
					    	 isSoldout=false;
					    	 
					    	jsonOutput += ",\"accommodationName\":\"" + accommodations.getAccommodationType().toUpperCase()+ "\"";
							jsonOutput += ",\"accommodationId\":\"" + accommodations.getAccommodationId()+ "\"";
				    		jsonOutput += ",\"available\":\"" + minimum+ "\"";
							jsonOutput += ",\"minimumAmount\":\"" +(totalMinimumAmount==0 ? 0 : Math.round(totalMinimumAmount) )+  "\"";
							jsonOutput += ",\"maximumAmount\":\"" +(totalMaximumAmount==0 ? 0 : Math.round(totalMaximumAmount) )+  "\"";
							jsonOutput += ",\"offerAmount\":\"" + (Math.round(totalMaximumAmount-totalMinimumAmount))+  "\"";
							jsonOutput += ",\"promotionFirstName\":\"" + (promotionFirstName==null? "NA":promotionFirstName)+ "\"";
				    		jsonOutput += ",\"promotionSecondName\":\"" + (promotionSecondName==null? "NA":promotionSecondName)+ "\"";
				    		jsonOutput += ",\"promotionThirdName\":\"" + (promotionThirdName==null? "NA":promotionThirdName)+ "\"";
				    		jsonOutput += ",\"promotionFirstType\":\"" + (promotionFirstType==null? "NA":promotionFirstType)+ "\"";
				    		jsonOutput += ",\"promotionSecondType\":\"" + (promotionSecondType==null? "NA":promotionSecondType)+ "\"";
				    		jsonOutput += ",\"isSoldout\":\"" + isSoldout+ "\"";
				    		jsonOutput += ",\"isFlat\":\"" + isFlat+ "\"";
							jsonOutput += ",\"isEarly\":\"" + isEarly+ "\"";
							jsonOutput += ",\"isLast\":\"" + isLast+ "\"";
							if(isLast){
								jsonOutput += ",\"promoLastMinuteTimer\":\"" + this.displayHoursForTimer+ "\"";	
							}else{
								jsonOutput += ",\"promoLastMinuteTimer\":\"" + "NA"+ "\"";
							}
							
							 
					     }
					}
			     
				}
				
				jsonOutput += "}";
				count++;
				arllist.clear();
			
			}
			String data="\"data\":[" + jsonOutput + "]";
 			String strData=data.replaceAll("\"", "\"");
 			/*String token = JWT.create()
 					.withClaim("data", strData)
 					.withExpiresAt(now)
 					.sign(algorithm);
 			output += "\"token\":\"" + token+ "\"";
 			String strOutput=output.replaceAll("\"", "\"");*/
 			
 			if(count == 0){
 				output="{\"error\":\"1\",\"message\":\"No Records Found\"}";
			}else{
//				output="{\"error\":\"0\",\"data\":{" + strData+ "}}";
				output="{\"error\":\"0\"," + strData + "}";			
			}
 			
	
		} catch (Exception e) {
			output="{\"error\":\"2\",\"message\":\"Some technical error on hotels api\"}";
			logger.error(e);
			e.printStackTrace();
		} finally {
	
		}
	
		return output;
	  
	}
	
	public String getAreaPropertiesDetails(){

		try {
			
			Algorithm algorithm = Algorithm.HMAC256("secret");
			int count=0;
			long nowMillis = System.currentTimeMillis();
		    Date now = new Date(nowMillis+15*60*1000);
			Map mapDate=new HashMap();
			java.util.Date date=new java.util.Date();
			DateFormat format = new SimpleDateFormat("MMM d,yyyy", Locale.ENGLISH);
		    java.util.Date dateStart = format.parse(getStartDate());
		    java.util.Date dateEnd = format.parse(getEndDate());
		    
		    
		    if(getSourceTypeId()==null){
		    	this.sourceTypeId=1;
		    }
		    
		    Calendar calCheckStart=Calendar.getInstance();
		    calCheckStart.setTime(dateStart);
		    java.util.Date checkInDate = calCheckStart.getTime();
		    this.arrivalDate=new Timestamp(checkInDate.getTime());
		   
		    
		    Calendar calCheckEnd=Calendar.getInstance();
		    calCheckEnd.setTime(dateEnd);
		    java.util.Date checkOutDate = calCheckEnd.getTime();
		    this.departureDate=new Timestamp(checkOutDate.getTime());
		 
	     
			/*this.arrivalDate = getArrivalDate();
		    sessionMap.put("arrivalDate",arrivalDate); 
			
			this.departureDate = getDepartureDate();
			sessionMap.put("departureDate",departureDate); */
			mapDate.put("arrivalDate", arrivalDate);
			mapDate.put("departureDate", departureDate);
			
			PmsPropertyManager propertyController = new PmsPropertyManager();		
			PropertyAccommodationManager accommodationManager = new PropertyAccommodationManager();
			PropertyAmenityManager amenityController = new PropertyAmenityManager();
			List<PropertyAmenity> propertyAmenityList;
			PropertyTypeManager typeController =  new PropertyTypeManager();
			PropertyRateManager rateController = new PropertyRateManager();
			PropertyRateDetailManager detailController = new PropertyRateDetailManager();
			PmsAmenityManager ameController = new PmsAmenityManager();
			PromotionManager promotionController=new PromotionManager();
			PromotionDetailManager promotionDetailController=new PromotionDetailManager();
			PmsBookingManager bookingController=new PmsBookingManager();
			PropertyRatePlanDetailManager planDetailController=new PropertyRatePlanDetailManager();
			
			HashMap<Double,Integer > mapAmountAccommId=new HashMap<Double, Integer>();
			HashMap<Double,Integer > mapSoldOutAmountAccommId=new HashMap<Double, Integer>();
			
			ArrayList<Integer> arlSortPropertyId=new ArrayList<Integer>();
			HashMap<Integer, Integer> mapAvailablePropertyId=new HashMap<Integer, Integer>();
			String jsonOutput = "",id="",type="";
			HttpServletResponse response = ServletActionContext.getResponse();
			
			response.setContentType("application/json");
			
			 
			Calendar cal = Calendar.getInstance();
			cal.setTime(this.departureDate);
		    cal.add(Calendar.DATE, -1);
		    
		    java.util.Date dteCheckOut = cal.getTime();
		    this.departureDate=new Timestamp(dteCheckOut.getTime());
		    
		    java.util.Date curdate=new java.util.Date();
        	Calendar curDate=Calendar.getInstance();
        	curDate.setTime(date);
        	curDate.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
        	int bookedHours=curDate.get(Calendar.HOUR);
        	int bookedMinute=curDate.get(Calendar.MINUTE);
        	int bookedSecond=curDate.get(Calendar.SECOND);
        	int AMPM=curDate.get(Calendar.AM_PM);
        	java.util.Date cudate=curDate.getTime();
        	
        	String strBookingDate=new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH).format(curDate.getTime());
        	String strCurrentDate=new SimpleDateFormat("yyyy-MM-dd").format(curDate.getTime());
        	String strCheckInDate=new SimpleDateFormat("yyyy-MM-dd").format(checkInDate.getTime());
        	String hours="",minutes="",seconds="",timeHours="";
        	if(bookedHours<10){
        		hours=String.valueOf(bookedHours);
        		hours="0"+hours;
        	}else{
        		hours=String.valueOf(bookedHours);
        	}
        	if(bookedMinute<10){
        		minutes=String.valueOf(bookedMinute);
        		minutes="0"+minutes;
        	}else{
        		minutes=String.valueOf(bookedMinute);
        	}
        	
        	if(bookedMinute<10){
        		seconds=String.valueOf(bookedSecond);
        		seconds="0"+seconds;
        	}else{
        		seconds=String.valueOf(bookedSecond);
        	}
        	
        	if(AMPM==0){//If the current time is AM
        		timeHours=strBookingDate+" "+hours+" : "+minutes+" : "+seconds+" AM ";
        	}else if(AMPM==1){//If the current time is PM
        		timeHours=strBookingDate+" "+hours+" : "+minutes+" : "+seconds+" PM ";
        	}
        	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        	GooglePlaceManager areaController = new GooglePlaceManager();
        	String checkStartDate = new SimpleDateFormat("yyyy-MM-dd").format(curdate);
	    	String strStartTime=checkStartDate+" 12:00:00"; 
	   	    java.util.Date dteStartDate = sdf.parse(strStartTime);
	   	    Calendar calStart=Calendar.getInstance();
	   	    calStart.setTime(dteStartDate);
	   	    calStart.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
	   	    String StartDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calStart.getTime());
	    	java.util.Date lastFirst = sdf.parse(StartDate);
	    	boolean isHotelTypeFilter=false;
			String startDate = new SimpleDateFormat("yyyy-MM-dd").format(this.arrivalDate);
			String endDate = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
			DateTime start = DateTime.parse(startDate);
		    DateTime end = DateTime.parse(endDate);
		    DecimalFormat df = new DecimalFormat("###.##");
		    DateFormat f = new SimpleDateFormat("EEEE");
		    String[] weekdays={"Monday","Tuesday","Wednesday","Thursday","Sunday"};
		    String[] weekend={"Friday","Saturday"};
		    PropertyAccommodationInventoryManager inventoryController= new PropertyAccommodationInventoryManager();
		    List<AccommodationRoom> listAccommodationRoom=null;
			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
			PropertyReviewsManager reviewController=new PropertyReviewsManager();
		    int availRoomCount=0,soldRoomCount=0;
		    this.sourceTypeId=1;
		    double commissionrange1=150,commissionrange2=225,commissionrange3=300,
		    		lastdiscountpercent1=10,lastdiscountpercent2=15,lastdiscountpercent3=20;
		    SeoUrlAction urlcheck=new SeoUrlAction();
		    
		    List<GooglePropertyAreas> listPropertyAreas=null;
		    List<PmsProperty> listPmsProperty=null;
		    List<PmsProperty> listProperties=new ArrayList<PmsProperty>();
		    ArrayList<Integer> arlcheck=new ArrayList<Integer>();
		    ArrayList<Integer> arlTripTypeChk=new ArrayList<Integer>();
		    ArrayList<Integer> arlHotelTypeChk=new ArrayList<Integer>();
		    
		    String stroutput=urlcheck.getSearchLocationIdByUrl(getLocationUrl());
		    StringTokenizer stoken=new StringTokenizer(stroutput,",");
		    while(stoken.hasMoreElements()){
		    	id=stoken.nextToken();
		    	type=stoken.nextToken();
			}
		    
		    int areaId = 0;
 	    	if(type.equals("location")){
		    	listPmsProperty = propertyController.listPropertyByLocation(Integer.parseInt(id));
		    	if(listPmsProperty.size()>0 && !listPmsProperty.isEmpty()){
		    		for(PmsProperty property:listPmsProperty){
		    			if(!arlcheck.contains(property.getPropertyId())){
		    				arlcheck.add(property.getPropertyId());
			    			List<PmsProperty> listNoTagProperty=propertyController.list(property.getPropertyId());
							if(listNoTagProperty.size()>0 && !listNoTagProperty.isEmpty()){
								for(PmsProperty tagproperty:listNoTagProperty){
									listProperties.add(tagproperty);			
								}
							}	
		    			}
		    		}
		    	}
		    }else if(type.equals("area")){
		    	
		    	listPropertyAreas = propertyController.listPropertyByGoogleArea(Integer.parseInt(id));
		    	
		 	    if(listPropertyAreas.size()>0 && !listPropertyAreas.isEmpty()){
		 	    	for(GooglePropertyAreas propertyareas: listPropertyAreas){
		 	    		areaId = propertyareas.getGoogleArea().getGoogleAreaId();
		 	    		int areaPropertyId=propertyareas.getPmsProperty().getPropertyId();
		 	    		if(!arlcheck.contains(areaPropertyId)){
		    				arlcheck.add(areaPropertyId);
		    				List<PmsProperty> listProperty=propertyController.list(areaPropertyId);
			 	    		if(listProperty.size()>0 && !listProperty.isEmpty()){
			 	    			for(PmsProperty property:listProperty){
			 	    				List<PmsProperty> listNoTagProperty=propertyController.list(property.getPropertyId());
	 	    						if(listNoTagProperty.size()>0 && !listNoTagProperty.isEmpty()){
	 	    							for(PmsProperty tagproperty:listNoTagProperty){
	 	    								listProperties.add(tagproperty);			
	 	    							}
	 	    						}
			 	    			}
			 	    		}
		 	    		}
		 	    	}
		 	    }
		    }	
 	    	int serialNo = 0;
			for (PmsProperty property : listProperties) {
				
				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				serialNo++;
				GoogleArea area = areaController.findArea(areaId);
				jsonOutput += "\"propertyId\":\"" + property.getPropertyId()+ "\"";
				jsonOutput += ",\"serialNo\":\"" +serialNo+ "\"";
				jsonOutput += ",\"propertyName\":\"" + (property.getPropertyName() == null ? "": property.getPropertyName().toUpperCase().trim())+ "\"";
				jsonOutput += ",\"propertyUrl\":\"" + (property.getPropertyUrl() == null ? "": property.getPropertyUrl().toLowerCase().trim())+ "\"";
				if(property.getGoogleLocation()!=null){
					jsonOutput += ",\"locationUrl\":\"" + (property.getGoogleLocation().getGoogleLocationUrl() == null ? "NA": property.getGoogleLocation().getGoogleLocationUrl().toLowerCase().trim())+ "\"";	
				}else{
					jsonOutput += ",\"locationUrl\":\"" + ("NA")+ "\"";
				}
				jsonOutput += ",\"city\":\"" + property.getCity() + "\"";
				jsonOutput += ",\"hotelTypeId\":\"" + (property.getPropertyType().getPropertyTypeId() == null ? "": property.getPropertyType().getPropertyTypeId())+ "\"";
				if(property.getLocation()!=null){
					jsonOutput += ",\"locationName\":\"" + (property.getLocation().getLocationName() == null ? "": property.getLocation().getLocationName().toUpperCase().trim())+ "\"";	
				}else{
					jsonOutput += ",\"locationName\":\"" + "" + "\"";
				}
				
				jsonOutput += ",\"areaName\":\"" + (area.getGoogleAreaDisplayName() == null ? "": area.getGoogleAreaDisplayName())+ "\"";
				jsonOutput += ",\"areaId\":\"" + (area.getGoogleAreaId() == null ? "": area.getGoogleAreaId())+ "\"";
				
				jsonOutput += ",\"payAtHotelStatus\":\"" + (property.getPayAtHotelStatus() == null ? false: property.getPayAtHotelStatus())+ "\"";
				jsonOutput += ",\"coupleStatus\":\"" + ( property.getCoupleIsActive() == null? false :  property.getCoupleIsActive() )+ "\"";
				jsonOutput += ",\"breakfastStatus\":\"" + ( property.getBreakfastIsActive() == null? false:  property.getBreakfastIsActive() )+ "\"";
				jsonOutput += ",\"hotelStatus\":\"" + ( property.getHotelIsActive() == null? false:  property.getHotelIsActive() )+ "\"";
				jsonOutput += ",\"soldoutStatus\":\"" + (property.getStopSellIsActive() == null ? false: property.getStopSellIsActive())+ "\"";
				jsonOutput += ",\"delistStatus\":\"" + ( property.getDelistIsActive() == null? false :  property.getDelistIsActive() )+ "\"";
				String imagePath="";
				if ( property.getPropertyThumbPath() != null) {
					imagePath = getText("storage.aws.property.photo") + "/uloimg/property/"
							+ property.getPropertyId()+"/"+property.getPropertyThumbPath();
				} 
				
				jsonOutput += ",\"propertyImage\":\"" + ( imagePath )+ "\"";
				String latitude=null,longitude=null;
				
				latitude=property.getLatitude();
				longitude=property.getLongitude();
				if(latitude!=null){
					latitude=latitude.trim();
				}
				if(longitude!=null){
					longitude=longitude.trim();
				}
				jsonOutput += ",\"latitude\":\"" + ( latitude==null?"":latitude.trim())+ "\"";
				jsonOutput += ",\"longitude\":\"" + ( longitude == null? "":  longitude.trim())+ "\"";
				SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
				Timestamp FromDate=(Timestamp)mapDate.get("arrivalDate");
				Timestamp ToDate=(Timestamp)mapDate.get("departureDate");
				jsonOutput += ",\"arrivalDate\":\"" + format2.format(FromDate) + "\"";
				jsonOutput += ",\"departureDate\":\"" + format2.format(ToDate)+ "\"";
				
				String strFromDate=format2.format(FromDate);
				String strToDate=format2.format(ToDate);
		    	DateTime startdate = DateTime.parse(strFromDate);
		        DateTime enddate = DateTime.parse(strToDate);
		        java.util.Date fromdate = startdate.toDate();
		 		java.util.Date todate = enddate.toDate();
		 		 
				int diffInDays = (int) ((todate.getTime() - fromdate.getTime()) / (1000 * 60 * 60 * 24));
				
				List arllist=new ArrayList();
				List arllistAvl=new ArrayList();
				List arllistSold=new ArrayList();
				jsonOutput += ",\"diffDays\":\"" + diffInDays+ "\"";
				int starCount=0;
				
				List<PropertyReviews> listReviews=reviewController.listReview(property.getPropertyId());
				if(listReviews.size()>0 && !listReviews.isEmpty()){
					for(PropertyReviews reviews:listReviews){
						if(reviews.getStarCount()==null){
							starCount=0;
						}else{
							starCount=Math.round(reviews.getStarCount());
						}
						
					}
				}
				jsonOutput += ",\"starCount\":\"" + starCount+ "\"";
				String jsonTags="";
				List<PmsTags> tag = propertyController.listTag();
				if(tag.size()>0 && !tag.isEmpty()){
					for (PmsTags tagList : tag) {
						
						List<PropertyTags>  propertyTag = propertyController.findTag(property.getPropertyId(),tagList.getTagId());	
						if (!jsonTags.equalsIgnoreCase(""))
							jsonTags += ",{";
						else
							jsonTags += "{";
						
						jsonTags += "\"tagId\":\"" + tagList.getTagId() + "\"";	
						jsonTags += ",\"tagName\":\"" + tagList.getTagName() + "\"";	
						if(propertyTag.size() == 0){
							jsonTags += ",\"status\":\"" + false + "\"";
						}else if(propertyTag.size() != 0){
							jsonTags += ",\"status\":\"" + true + "\"";
						}
						jsonTags += "}";
					}
					jsonOutput += ",\"tags\":[" + jsonTags+ "]";
				}
				
				
				ArrayList<Integer> arlLastImage=new ArrayList<Integer>();
				String jsonPhotos="",strPhotos="",data="",strData="";
				String jsonExteriour = "",exteriorphoto="";
				String jsonRestaurant = "",restaurantphoto="";
				String jsonRooms = "",roomphoto="";
				String jsonActivityArea = "",activityareaphoto="";
				String jsonBedRoom = "",bedroomphoto="";
				String jsonRestRoom = "",restroomphoto="";
				boolean blnImgActivityArea=false,blnImgRestaurant=false,blnImgReception=false,
	 					blnImgExterior=false,blnImgRestRoom=false,blnImgBedRoom=false;
				StringBuilder imagebuilder=new StringBuilder();
				PropertyPhotoManager photoController = new PropertyPhotoManager();
				
			    
			    List<PropertyPhoto> listPhotos =photoController.list(property.getPropertyId());

			    if(listPhotos.size()>0){
		        	  for (PropertyPhoto photo : listPhotos) {
			          		if(photo.getPmsPhotoCategory()!=null && photo.getPmsPhotoCategory().getPhotoCategoryId()==5){
			          			if(!arlLastImage.contains(photo.getPmsPhotoCategory().getPhotoCategoryId())){
			          				arlLastImage.add(photo.getPmsPhotoCategory().getPhotoCategoryId());	
			          			}
			          			
			          		}
		        	  }
		        	  for (PropertyPhoto photo : listPhotos) {
			          		if(photo.getPmsPhotoCategory()!=null && photo.getPmsPhotoCategory().getPhotoCategoryId()==6){
			          			if(!arlLastImage.contains(photo.getPmsPhotoCategory().getPhotoCategoryId())){
			          				arlLastImage.add(photo.getPmsPhotoCategory().getPhotoCategoryId());	
			          			}
			          		}
		        	  }
		        	  for (PropertyPhoto photo : listPhotos) {
			          		if(photo.getPmsPhotoCategory()!=null && photo.getPmsPhotoCategory().getPhotoCategoryId()==1){
			          			if(!arlLastImage.contains(photo.getPmsPhotoCategory().getPhotoCategoryId())){
			          				arlLastImage.add(photo.getPmsPhotoCategory().getPhotoCategoryId());	
			          			}
			          		}
		        	  }
		        	  for (PropertyPhoto photo : listPhotos) {
			          		if(photo.getPmsPhotoCategory()!=null && photo.getPmsPhotoCategory().getPhotoCategoryId()==2){
			          			if(!arlLastImage.contains(photo.getPmsPhotoCategory().getPhotoCategoryId())){
			          				arlLastImage.add(photo.getPmsPhotoCategory().getPhotoCategoryId());	
			          			}
			          		}
		        	  }
		        	  for (PropertyPhoto photo : listPhotos) {
			          		if(photo.getPmsPhotoCategory()!=null && photo.getPmsPhotoCategory().getPhotoCategoryId()==3){
			          			if(!arlLastImage.contains(photo.getPmsPhotoCategory().getPhotoCategoryId())){
			          				arlLastImage.add(photo.getPmsPhotoCategory().getPhotoCategoryId());	
			          			}
			          		}
		        	  }
		        	  for (PropertyPhoto photo : listPhotos) {
			          		if(photo.getPmsPhotoCategory()!=null && photo.getPmsPhotoCategory().getPhotoCategoryId()==4){
			          			if(!arlLastImage.contains(photo.getPmsPhotoCategory().getPhotoCategoryId())){
			          				arlLastImage.add(photo.getPmsPhotoCategory().getPhotoCategoryId());	
			          			}
			          		}
		        	  }
		          }
		          int lastelement=0,nthelement=0;
		          if(arlLastImage.size()>0){
		        	  lastelement=arlLastImage.get(arlLastImage.size()-1);  
		          }
	          if(listPhotos.size()>0)
			  {
		          	for (PropertyPhoto photo : listPhotos) {
		          		if(photo.getPmsPhotoCategory()!=null && photo.getPmsPhotoCategory().getPhotoCategoryId()==5){
		          			blnImgBedRoom=true;
		          			nthelement=photo.getPmsPhotoCategory().getPhotoCategoryId();
		          			if (!jsonBedRoom.equalsIgnoreCase(""))
		          				jsonBedRoom += ",{";
							else
								jsonBedRoom += "{";
			          		this.photoPath = getText("storage.aws.property.photo") + "/uloimg/"
									+ photo.getPmsProperty().getPropertyId()+"/accommodation/"+photo.getPropertyAccommodation().getAccommodationId()
									+"/"+photo.getPmsPhotoCategory().getPhotoCategoryId()+"/"+photo.getPhotoPath();
						
			          		jsonBedRoom += "\"original\":\"" + this.photoPath + "\"";
			          		jsonBedRoom += ",\"thumbnail\":\"" + this.photoPath+ "\"";
			          		this.altName=photo.getImageAltName()==null?"NA":photo.getImageAltName();
							jsonBedRoom += ",\"altName\":\"" +this.altName + "\"";
			          		jsonBedRoom += "}";	
		          		}
		
		  			 }
		          	
		          	data="\"images\":[" + jsonBedRoom + "]";
		 			strData=data.replaceAll("\"", "\"");
		 			
		 			bedroomphoto="{\"name\":\"bedroom\"," + strData+ "}";
		 			if(blnImgBedRoom){
		 				if(nthelement==lastelement){
		 					imagebuilder.append(bedroomphoto);
		 				}else{
		 					imagebuilder.append(bedroomphoto+",");	
		 				}
		 				
		 			}
		 			for (PropertyPhoto photo : listPhotos) {
		          		if(photo.getPmsPhotoCategory()!=null && photo.getPmsPhotoCategory().getPhotoCategoryId()==6){
		          			blnImgRestRoom=true;
		          			nthelement=photo.getPmsPhotoCategory().getPhotoCategoryId();
		          			if (!jsonRestRoom.equalsIgnoreCase(""))
		          				jsonRestRoom += ",{";
							else
								jsonRestRoom += "{";
			          		this.photoPath = getText("storage.aws.property.photo") + "/uloimg/"
									+ photo.getPmsProperty().getPropertyId()+"/accommodation/"+photo.getPropertyAccommodation().getAccommodationId()
									+"/"+photo.getPmsPhotoCategory().getPhotoCategoryId()+"/"+photo.getPhotoPath();
						
			          		jsonRestRoom += "\"original\":\"" + this.photoPath + "\"";
			          		jsonRestRoom += ",\"thumbnail\":\"" + this.photoPath+ "\"";
			          		this.altName=photo.getImageAltName()==null?"NA":photo.getImageAltName();
							jsonRestRoom += ",\"altName\":\"" +this.altName + "\"";
			          		jsonRestRoom += "}";	
		          		}
		
		  			 }
		          	
		          	data="\"images\":[" + jsonRestRoom + "]";
		 			strData=data.replaceAll("\"", "\"");
		 			
		 			restroomphoto="{\"name\":\"bathroom\"," + strData+ "}";
		 			if(blnImgRestRoom){
		 				if(nthelement==lastelement){
		 					imagebuilder.append(restroomphoto);
		 				}else{
		 					imagebuilder.append(restroomphoto+",");
		 				}
		 				
		 			}
		          	for (PropertyPhoto photo : listPhotos) {
		    			
		          		if(photo.getPmsPhotoCategory()!=null && photo.getPmsPhotoCategory().getPhotoCategoryId() == 1){
		          			blnImgExterior=true;
		          			nthelement=photo.getPmsPhotoCategory().getPhotoCategoryId();
							if (!jsonExteriour.equalsIgnoreCase(""))
								jsonExteriour += ",{";
							else
								jsonExteriour += "{";
								
							this.photoPath = getText("storage.aws.property.photo") + "/uloimg/"
									+ photo.getPmsProperty().getPropertyId()+"/category/"+photo.getPmsPhotoCategory().getPhotoCategoryId()+"/"+photo.getPhotoPath();
							jsonExteriour += "\"original\":\"" + this.photoPath + "\"";
							jsonExteriour += ",\"thumbnail\":\"" + this.photoPath+ "\"";
							this.altName=photo.getImageAltName()==null?"NA":photo.getImageAltName();
							jsonExteriour += ",\"altName\":\"" +this.altName + "\"";
							jsonExteriour += "}";
		          		}
		
		          		
		
		  			 }
		          	data="";strData="";
		          	data="\"images\":[" + jsonExteriour + "]";
		 			strData=data.replaceAll("\"", "\"");
		 			
		 			exteriorphoto="{\"name\":\"exterior\"," + strData+ "}";
		 			if(blnImgExterior){
		 				if(nthelement==lastelement){
		 					imagebuilder.append(exteriorphoto);
		 				}else{
		 					imagebuilder.append(exteriorphoto+",");	
		 				}
		 				
		 			}
		          	for (PropertyPhoto photo : listPhotos) {
		    			
		          		if(photo.getPmsPhotoCategory()!=null && photo.getPmsPhotoCategory().getPhotoCategoryId() == 2){
		          			blnImgReception=true;
		          			nthelement=photo.getPmsPhotoCategory().getPhotoCategoryId();
							if (!jsonRooms.equalsIgnoreCase(""))
								jsonRooms += ",{";
							else
								jsonRooms += "{";
							this.photoPath = getText("storage.aws.property.photo") + "/uloimg/"
									+ photo.getPmsProperty().getPropertyId()+"/category/"+photo.getPmsPhotoCategory().getPhotoCategoryId()+"/"+photo.getPhotoPath();
							
							jsonRooms += "\"original\":\"" + this.photoPath + "\"";
							jsonRooms += ",\"thumbnail\":\"" + this.photoPath+ "\"";
							this.altName=photo.getImageAltName()==null?"NA":photo.getImageAltName();
							jsonRooms += ",\"altName\":\"" +this.altName + "\"";
							jsonRooms += "}";
		          		}
		  			 }
		          	data="";strData="";
		          	data="\"images\":[" + jsonRooms + "]";
		 			strData=data.replaceAll("\"", "\"");
		 			
		 			roomphoto="{\"name\":\"reception\"," + strData+ "}";
		 			if(blnImgReception){
		 				if(nthelement==lastelement){
		 					imagebuilder.append(roomphoto);
		 				}else{
		 					imagebuilder.append(roomphoto+",");	
		 				}
		 				
		 			}
		          	for (PropertyPhoto photo : listPhotos) {
		    			
		          		if(photo.getPmsPhotoCategory()!=null && photo.getPmsPhotoCategory().getPhotoCategoryId() == 3){
		          			blnImgRestaurant=true;
		          			nthelement=photo.getPmsPhotoCategory().getPhotoCategoryId();
							if (!jsonRestaurant.equalsIgnoreCase(""))
								jsonRestaurant += ",{";
							else
								jsonRestaurant += "{";
							this.photoPath = getText("storage.aws.property.photo") + "/uloimg/"
									+ photo.getPmsProperty().getPropertyId()+"/category/"+photo.getPmsPhotoCategory().getPhotoCategoryId()+"/"+photo.getPhotoPath();
							jsonRestaurant += "\"original\":\"" + this.photoPath + "\"";
							jsonRestaurant += ",\"thumbnail\":\"" + this.photoPath+ "\"";
							this.altName=photo.getImageAltName()==null?"NA":photo.getImageAltName();
							jsonRestaurant += ",\"altName\":\"" +this.altName + "\"";
							jsonRestaurant += "}";
		          		}
		  			 }
		          	data="";strData="";
		          	data="\"images\":[" + jsonRestaurant + "]";
		 			strData=data.replaceAll("\"", "\"");
		 			
		 			restaurantphoto="{\"name\":\"restaurant\"," + strData+ "}";
		 			if(blnImgRestaurant){
		 				if(nthelement==lastelement){
		 					imagebuilder.append(restaurantphoto);
		 				}else{
		 					imagebuilder.append(restaurantphoto+",");	
		 				}
		 				
		 			}
					for (PropertyPhoto photo : listPhotos) {
						
							if(photo.getPmsPhotoCategory()!=null && photo.getPmsPhotoCategory().getPhotoCategoryId() == 4){
								blnImgActivityArea=true;
								nthelement=photo.getPmsPhotoCategory().getPhotoCategoryId();
								if (!jsonActivityArea.equalsIgnoreCase(""))
									jsonActivityArea += ",{";
								else
									jsonActivityArea += "{";
							
								this.photoPath = getText("storage.aws.property.photo") + "/uloimg/"
										+ photo.getPmsProperty().getPropertyId()+"/category/"+photo.getPmsPhotoCategory().getPhotoCategoryId()+"/"+photo.getPhotoPath();
								
								jsonActivityArea += "\"original\":\"" + this.photoPath + "\"";
								jsonActivityArea += ",\"thumbnail\":\"" + this.photoPath+ "\"";
								this.altName=photo.getImageAltName()==null?"NA":photo.getImageAltName();
								jsonActivityArea += ",\"altName\":\"" +this.altName + "\"";
								jsonActivityArea += "}";
							}
					 }
					data="";strData="";
					data="\"images\":[" + jsonActivityArea + "]";
		 			strData=data.replaceAll("\"", "\"");
		 			
		 			activityareaphoto="{\"name\":\"others\"," + strData+ "}";
		 			
		 			if(blnImgActivityArea){
		 				if(nthelement==lastelement){
		 					imagebuilder.append(activityareaphoto);
		 				}else{
		 					imagebuilder.append(activityareaphoto);	
		 				}
		 				
		 			}		
		 			jsonOutput += ",\"gallery\":[" + imagebuilder+"]";
//	                 jsonOutput += ",\"gallery\":[" + bedroomphoto+","+restroomphoto+ ","+exteriorphoto+","+roomphoto+","+restaurantphoto+","+activityareaphoto+"]";
				
				}
				
				String jsonAmenities ="";
				propertyAmenityList =  amenityController.list(property.getPropertyId());
				if(propertyAmenityList.size()>0)
				{
					for (PropertyAmenity amenity : propertyAmenityList) {
						if (!jsonAmenities.equalsIgnoreCase(""))
							
							jsonAmenities += ",{";
						else
							jsonAmenities += "{";
						PropertyAmenity amentity1 = amenityController.find(amenity.getPropertyAmenityId());
					    String path = "ulowebsite/images/icons/";
						PmsAmenity pmsAmenity1 =ameController.find(amentity1.getPmsAmenity().getAmenityId());
						jsonAmenities += "\"amenityId\":\"" + amenity.getPropertyAmenityId()+ "\"";					
						jsonAmenities += ",\"amenityName\":\"" + pmsAmenity1.getAmenityName()+ "\"";
	                   jsonAmenities += ",\"amenityIcon\":\"" + pmsAmenity1.getAmenityIcon()+ "\"";
	            
						
						jsonAmenities += "}";
					}
					
					jsonOutput += ",\"amenities\":[" + jsonAmenities+ "]";
				}
				
				String jsonAccommodation ="";
				List<PropertyAccommodation> accommodationsList =  accommodationManager.list(property.getPropertyId());
				int soldOutCount=0,availCount=0,promotionAccommId=0;
				if(accommodationsList.size()>0 && !accommodationsList.isEmpty())
				{
					for (PropertyAccommodation accommodation : accommodationsList) {
						int roomCount=0;
						listAccommodationRoom=accommodationController.listPropertyTotalRooms(property.getPropertyId(), accommodation.getAccommodationId());
						if(listAccommodationRoom.size()>0 && !listAccommodationRoom.isEmpty()){
							for(AccommodationRoom roomsList:listAccommodationRoom){
								 roomCount=(int) roomsList.getRoomCount();
								 long minimum = getAvailableCount(property.getPropertyId(),getArrivalDate(),getDepartureDate(),roomCount,accommodation.getAccommodationId());
								 if((int)minimum>0){
									 availCount++;
								 }else if((int)minimum==0){
									 soldOutCount++;
								 }
								 break;
							 }
						 }
						
						
						if (!jsonAccommodation.equalsIgnoreCase(""))
							
							jsonAccommodation += ",{";
						else
							jsonAccommodation += "{";
							jsonAccommodation += "\"accommodationId\":\"" + accommodation.getAccommodationId()+ "\"";
							jsonAccommodation += ",\"accommodationName\":\"" + accommodation.getAccommodationType().toUpperCase()+ "\"";
						
						jsonAccommodation += "}";
					}
					
				}
	
				
				
				List<PmsPromotionDetails> listPromotionDetailCheck=null;
				Boolean blnAvailable=false,blnSoldout=false,firstPromotionFlag=false,secondPromotionFlag=false,promotionFlag=false,blnPromoFlag=false;
				int arlListTotalAccomm=0,soldOutTotalAccomm=0,accommCount=0;
				String promotionType="NA",discountType="NA",promotionName="NA";
				double dblDiscountINR=0,dblDiscountPercent=0;
				boolean promoStatus=false,isFlat=false,isEarly=false,isLast=false,isSoldout=false;
				ArrayList<String> arlListPromoType=new ArrayList<String>();
				List<PmsPromotions> listPromotions=promotionController.listDatePromotions(property.getPropertyId(), dateStart);
				 if(listPromotions.size()>0 && !listPromotions.isEmpty()){
				   	for(PmsPromotions promotion:listPromotions){
				   		if(promotion.getPromotionType().equals("F")){
				   			promotionType=promotion.getPromotionType();
				   			promoStatus=true;
				   			isFlat=true;
				   		}else if(promotion.getPromotionType().equals("E")){
				   			promotionType=promotion.getPromotionType();
				   			Timestamp tsStartPromo=promotion.getFirstRangeStartDate();
				   			Timestamp tsEndPromo=promotion.getFirstRangeEndDate();
				   			String startStayDate = new SimpleDateFormat("yyyy-MM-dd").format(tsStartPromo);
							String endStayDate = new SimpleDateFormat("yyyy-MM-dd").format(tsEndPromo);
							DateTime dtstart = DateTime.parse(startStayDate);
						    DateTime dtend = DateTime.parse(endStayDate);
				   			List<DateTime> between = DateUtil.getDateRange(dtstart, dtend);
				   			if(between.size()>0 && !between.isEmpty()){
				   				for(DateTime d:between){
				   					if(cudate.equals(d.toDate())){
				   						promoStatus=true;
				   						isEarly=true;
				   					}
				   				}
				   			}else{
				   				promoStatus=false;
				   			}
				   		}
				   		
						List<PmsPromotionDetails> listPromoDetails=promotionDetailController.listPromotionDetails(promotion.getPromotionId(),f.format(dateStart).toLowerCase());
						if(listPromoDetails.size()>0 && !listPromoDetails.isEmpty()){
							for(PmsPromotionDetails promodetails:listPromoDetails){
								dblDiscountINR=promodetails.getPromotionInr();
								if(dblDiscountINR>0){
									discountType="INR";
								}
								dblDiscountPercent=promodetails.getPromotionPercentage();
								if(dblDiscountPercent>0){
									discountType="PERCENT";
								}
							}
						}
					}
				 }
				 
				 
				List<PropertyAccommodation> accommodationList =  accommodationManager.list(property.getPropertyId());
				if(accommodationList.size()>0)
				{
					for (PropertyAccommodation accommodation : accommodationList) {
						
						int dateCount=0;
						double amount=0.0,extraAdultAmount=0.0,extraChildAmount=0.0;
						int roomCount=0,availableCount=0,totalCount=0;
					    double dblPercentCount=0.0;
		    			listAccommodationRoom=accommodationController.listPropertyTotalRooms(property.getPropertyId(), accommodation.getAccommodationId());
		    			List<DateTime> between = DateUtil.getDateRange(start, end); 
						if(listAccommodationRoom.size()>0 && !listAccommodationRoom.isEmpty()){
							
							for(AccommodationRoom roomsList:listAccommodationRoom){
								roomCount=(int) roomsList.getRoomCount();
								if(roomCount>0){
									long minimum = getAvailableCount(property.getPropertyId(),getArrivalDate(),getDepartureDate(),roomCount,accommodation.getAccommodationId());
									if((int)minimum>0){
										blnAvailable=true;
										blnSoldout=false;
										arlListTotalAccomm++;
									}else if((int)minimum==0){
										blnSoldout=true;
										blnAvailable=false;
										soldOutTotalAccomm++;
									}
								}
								 break;
							 }
						 }
						 
					     for (DateTime d : between)
					     {
							 
					    	 long rmc = 0;
				   			 this.roomCnt = rmc;
				        	 int sold=0;
				        	 java.util.Date availDate = d.toDate();
				        	 String days=f.format(d.toDate()).toLowerCase();
							 PropertyAccommodationInventory inventoryCount = inventoryController.findInventoryCount(availDate,accommodation.getAccommodationId());
							 if(inventoryCount != null){
							 PmsAvailableRooms roomCount1 = bookingController.findBookingCount(availDate, accommodation.getAccommodationId(),inventoryCount.getInventoryId());
							 if(roomCount1.getRoomCount() == null) {
							 	String num1 = inventoryCount.getInventoryCount();
								PmsAvailableRooms bookingRoomCount=bookingController.findCount(availDate, accommodation.getAccommodationId());
								PropertyAccommodation accomm=accommodationController.find(accommodation.getAccommodationId());
								Integer totavailable=0;
								if(accommodation!=null){
									totavailable=accommodation.getNoOfUnits();
								}
								if(bookingRoomCount.getRoomCount()!=null){
									long lngRooms=bookingRoomCount.getRoomCount();
									long num2 = Long.parseLong(num1);
									this.roomCnt = num2;
									sold=(int) (lngRooms);
									int availRooms=0;
									availRooms=totavailable-sold;
									/*if(Integer.parseInt(num1)>availRooms){
										this.roomCnt = availRooms;	
									}else{
										this.roomCnt = num2;
									}*/
									this.roomCnt = num2;		
								}else{
									 long num2 = Long.parseLong(num1);
									 this.roomCnt = num2;
									 sold=(int) (rmc);
								}
							}else{		
								PmsAvailableRooms bookingRoomCount=bookingController.findCount(availDate, accommodation.getAccommodationId());
								int intRooms=0;
								Integer totavailable=0,availableRooms=0;
								if(accommodation!=null){
									totavailable=accommodation.getNoOfUnits();
								}
								if(bookingRoomCount.getRoomCount()!=null){
									long lngRooms=bookingRoomCount.getRoomCount();
									long num = roomCount1.getRoomCount();	
									intRooms=(int)lngRooms;
									String num1 = inventoryCount.getInventoryCount();
				 					long num2 = Long.parseLong(num1);
				 					availableRooms=totavailable-intRooms;
				 					/*if(num2>availableRooms){
				 						this.roomCnt = availableRooms;	 
				 					}else{
				 						this.roomCnt = num2-num;
				 					}*/
				 					this.roomCnt = num2-num;
				 					long lngSold=bookingRoomCount.getRoomCount();
				   					sold=(int)lngSold;
								}else{
									long num = roomCount1.getRoomCount();	
									String num1 = inventoryCount.getInventoryCount();
									long num2 = Long.parseLong(num1);
				  					this.roomCnt = (num2-num);
				  					long lngSold=roomCount1.getRoomCount();
				   					sold=(int)lngSold;
								}
			
							}
											
						}else{
							  PmsAvailableRooms inventoryCounts = bookingController.findCount(availDate, accommodation.getAccommodationId());							
								if(inventoryCounts.getRoomCount() == null){								
									this.roomCnt = (accommodation.getNoOfUnits()-rmc);
									sold=(int)rmc;
								}
								else{
									this.roomCnt = (accommodation.getNoOfUnits()-inventoryCounts.getRoomCount());	
									long lngSold=inventoryCounts.getRoomCount();
		   							sold=(int)lngSold;
								}
						}
											
						double totalRooms=0,intSold=0;
						totalRooms=(double)accommodation.getNoOfUnits();
						intSold=(double)sold;
						double occupancy=0.0;
						occupancy=intSold/totalRooms;
						double sellrate=0.0,otarate=0.0;
						Integer occupancyRating=(int) Math.round(occupancy * 100);
					    double minAmount=0,maxAmount=0,promoAmount=0,firstPromoAmount=0,secondPromoAmount=0;
					    
					    
					    boolean isWeekdays=false,isWeekend=false;
						for(int a=0;a<weekdays.length;a++){
							if(days.equalsIgnoreCase(weekdays[a])){
								isWeekdays=true;
							}
						}
						
						for(int b=0;b<weekend.length;b++){
							if(days.equalsIgnoreCase(weekend[b])){
								isWeekend=true;
							}
						}
						if(property.getLocationType()!=null){
							if(property.getLocationType().getLocationTypeName().equalsIgnoreCase("Leisure")){
								if(isWeekdays){
									if(accommodation.getWeekdaySellingRate()==null){
										maxAmount=0;	
									}else{
										maxAmount=accommodation.getWeekdaySellingRate();
									}
										
								}
								
								if(isWeekend){
									if(accommodation.getWeekendSellingRate()==null){
										maxAmount=0;
									}else{
										maxAmount=accommodation.getWeekendSellingRate();	
									}
									
								}	
							}
							
							if(property.getLocationType().getLocationTypeName().equalsIgnoreCase("Metro")){
								if(accommodation.getSellingRate()==null){
									maxAmount=0;
								}else{
									maxAmount=accommodation.getSellingRate();	
								}
									
							}
							
							if(occupancyRating>=0 && occupancyRating<=30){
								sellrate=maxAmount;
							}else if(occupancyRating>30 && occupancyRating<=60){
								sellrate=maxAmount+(maxAmount*10/100);
							}else if(occupancyRating>=60 && occupancyRating<=80){
								sellrate=maxAmount+(maxAmount*15/100);
							}else if(occupancyRating>80){
								sellrate=maxAmount+(maxAmount*20/100);
							}
							
						}
						
	    				
						
						
						if(discountType.equals("INR")){
							firstPromoAmount=dblDiscountINR;
						}else if(discountType.equals("PERCENT")){
							firstPromoAmount=(maxAmount*dblDiscountPercent/100);
						}else{
							firstPromoAmount=0;
						}
						
						if(strCheckInDate.equals(strCurrentDate)){ 
							if(occupancyRating>=0 && occupancyRating<=30){
								if(this.roomCnt>0){
									isLast=true;
								}else{
									isLast=false;
								}
							}
						}
						
	    				if(isLast){
	    					if(occupancyRating>=0 && occupancyRating<=30){
	    						if(AMPM==0){//If the current time is AM
	    		            		if(bookedHours>=0 && bookedHours<=11){
	    		            			if(maxAmount<1000){
	    									if(occupancyRating>=0 && occupancyRating<=30){
	    										secondPromoAmount=commissionrange1;
	    									}
	    							    }else if(maxAmount>=1000 && maxAmount<=1500){
	    							    	if(occupancyRating>=0 && occupancyRating<=30){
	    							    		secondPromoAmount=commissionrange2;
	    									}
	    							    }else if(maxAmount>1500){
	    							    	if(occupancyRating>=0 && occupancyRating<=30){
	    							    		secondPromoAmount=commissionrange3;
	    									}
	    							    }
	    		            		}
	    		            	}
	    						if(AMPM==1){
	    							if(bookedHours>=0 && bookedHours<=3){
	    								if(occupancyRating>=0 && occupancyRating<=30){
	    									secondPromoAmount=(sellrate*lastdiscountpercent1/100);	
	    								}
	    								
	    		            		}else if(bookedHours>=4 && bookedHours<=7){
	    		            			if(occupancyRating>=0 && occupancyRating<=30){
	    		            				secondPromoAmount=(sellrate*lastdiscountpercent2/100);	
	    								}
	    		            			
	    		            		}else if(bookedHours>=8 && bookedHours<=11){
	    		            			if(occupancyRating>=0 && occupancyRating<=30){
	    		            				secondPromoAmount=(sellrate*lastdiscountpercent3/100);	
	    								}
	    		            			
	    		            		}
	    						}
	    					}
	    				}
	    				promoAmount=firstPromoAmount+secondPromoAmount;
	    				amount+=sellrate - promoAmount;
				    	 dateCount++;
				    	 if(dateCount==diffInDays){
				    		 if(blnAvailable){
			    				mapAmountAccommId.put(amount,accommodation.getAccommodationId());
				      			arllist.add(amount);
		      				 }else if(blnSoldout){
		      					mapSoldOutAmountAccommId.put(amount,accommodation.getAccommodationId());
				      			arllist.add(amount);
					    	 }
				    	 }
				     }
				     if(blnAvailable){
				    	 arllistAvl.add(amount);
				     }else if(!blnAvailable){
				    	 arllistSold.add(amount);
				     }
				     accommCount++;
					}
				}
				//end of accommodation
				double totalAmount=0;
				boolean blnJSONContent=false;
				Collections.sort(arllist);
				int arlListSize=0,accommId=0,intTotalAccomm=0;
			     intTotalAccomm=accommodationList.size();
			     if(intTotalAccomm==accommCount){
			    	 if(intTotalAccomm==soldOutCount){
					     arlListSize=arllist.size();
					     if(soldOutCount==arlListSize){
					    	 Collections.sort(arllist);
					    	 for(int i=0;i<arlListSize;i++){
						    	 totalAmount=(Double) arllist.get(i);
						    	 if((Double) arllist.get(0)==(Double) arllist.get(i)){
						    		 if(blnAvailable){
						    			 accommId=mapAmountAccommId.get(totalAmount);
							    		 blnJSONContent=true;
						    		 }else{
						    			 accommId=mapSoldOutAmountAccommId.get(totalAmount);
							    		 blnJSONContent=true;
						    		 }
						    		 break;
						    	 }else{
						    		 
						    	 }
						     }
					     }
			    	 }else{
					     arlListSize=arllist.size();
					     Collections.sort(arllist);
				    	 for(int i=0;i<arlListSize;i++){
				    		 if(arllistAvl.contains((Double) arllist.get(i))){
				    			 if((Double) arllist.get(0)==(Double) arllist.get(i)){
					    			 totalAmount=(Double) arllist.get(i);
					    			 accommId=mapAmountAccommId.get(totalAmount);
						    		 blnJSONContent=true;
				    			 }else{
				    				 totalAmount=(Double) arllist.get(i);
					    			 accommId=mapAmountAccommId.get(totalAmount);
						    		 blnJSONContent=true;
				    			 }
				    			 break;
				    		 }
					     }
			    	 }
			     }
			     arlListPromoType.clear();  
				if(blnJSONContent){
				    listAccommodationRoom.clear(); 
			    	listAccommodationRoom=accommodationController.listPropertyTotalRooms(property.getPropertyId(), accommId);
			    	List<DateTime> betweenAmount = DateUtil.getDateRange(start, end);
			    	double actualPromoBaseAmount=0.0,actualBaseAmount=0.0,actualTotalAmount=0.0,actualTotalAdultAmount=0.0,actualTotalChildAmount=0.0,
			    			actualAdultAmount=0.0,actualChildAmount=0.0;
				    int countDate=0; 
				    boolean blnStatus=false;
				    
				    double baseAmount=0.0,extraAdultAmount=0.0,extraChildAmount=0.0,totalBaseAmount=0.0,
				    		totalMinimumAmount=0,totalMaximumAmount=0,totalSellRate=0;
				    int roomCount=0,availableCount=0,totalCount=0,discountId=0;
				    
					if(listAccommodationRoom.size()>0 && !listAccommodationRoom.isEmpty()){
						for(AccommodationRoom roomsList:listAccommodationRoom){
							 roomCount=(int) roomsList.getRoomCount();
							 break;
						 }
					 }
					isLast=false;
					List<PropertyAccommodation> listAccommodation =  accommodationManager.list(property.getPropertyId(),accommId);
					int dateCount=0;
					for (PropertyAccommodation accommodations : listAccommodation) {
						
						double firstInr=0,secondInr=0;
						for (DateTime d : betweenAmount){
	
					    	 long rmc = 0;
				   			 this.roomCnt = rmc;
				        	 int sold=0;
				        	 java.util.Date availDate = d.toDate();
				        	 String days=f.format(d.toDate()).toLowerCase();
							 PropertyAccommodationInventory inventoryCount = inventoryController.findInventoryCount(availDate,accommodations.getAccommodationId());
							 if(inventoryCount != null){
							 PmsAvailableRooms roomCount1 = bookingController.findBookingCount(availDate, accommodations.getAccommodationId(),inventoryCount.getInventoryId());
							 if(roomCount1.getRoomCount() == null) {
							 	String num1 = inventoryCount.getInventoryCount();
								PmsAvailableRooms bookingRoomCount=bookingController.findCount(availDate, accommodations.getAccommodationId());
								Integer totavailable=0;
								if(accommodations!=null){
									totavailable=accommodations.getNoOfUnits();
								}
								if(bookingRoomCount.getRoomCount()!=null){
									long lngRooms=bookingRoomCount.getRoomCount();
									long num2 = Long.parseLong(num1);
									this.roomCnt = num2;
									sold=(int) (lngRooms);
									int availRooms=0;
									availRooms=totavailable-sold;
									if(Integer.parseInt(num1)>availRooms){
										this.roomCnt = availRooms;	
									}else{
										this.roomCnt = num2;
									}
											
								}else{
									 long num2 = Long.parseLong(num1);
									 this.roomCnt = num2;
									 sold=(int) (rmc);
								}
							}else{		
								PmsAvailableRooms bookingRoomCount=bookingController.findCount(availDate, accommodations.getAccommodationId());
								int intRooms=0;
								Integer totavailable=0,availableRooms=0;
								if(accommodations!=null){
									totavailable=accommodations.getNoOfUnits();
								}
								if(bookingRoomCount.getRoomCount()!=null){
									long lngRooms=bookingRoomCount.getRoomCount();
									long num = roomCount1.getRoomCount();	
									intRooms=(int)lngRooms;
									String num1 = inventoryCount.getInventoryCount();
				 					long num2 = Long.parseLong(num1);
				 					availableRooms=totavailable-intRooms;
				 					if(num2>availableRooms){
				 						this.roomCnt = availableRooms;	 
				 					}else{
				 						this.roomCnt = num2-num;
				 					}
				 					long lngSold=bookingRoomCount.getRoomCount();
				   					sold=(int)lngSold;
								}else{
									long num = roomCount1.getRoomCount();	
									String num1 = inventoryCount.getInventoryCount();
									long num2 = Long.parseLong(num1);
				  					this.roomCnt = (num2-num);
				  					long lngSold=roomCount1.getRoomCount();
				   					sold=(int)lngSold;
								}
			
							}
											
						}else{
							  PmsAvailableRooms inventoryCounts = bookingController.findCount(availDate, accommodations.getAccommodationId());							
								if(inventoryCounts.getRoomCount() == null){								
									this.roomCnt = (accommodations.getNoOfUnits()-rmc);
									sold=(int)rmc;
								}
								else{
									this.roomCnt = (accommodations.getNoOfUnits()-inventoryCounts.getRoomCount());	
									long lngSold=inventoryCounts.getRoomCount();
		   							sold=(int)lngSold;
								}
							}
												
							double totalRooms=0,intSold=0;
							totalRooms=(double)accommodations.getNoOfUnits();
							intSold=(double)sold;
							double occupancy=0.0;
							occupancy=intSold/totalRooms;
							double sellrate=0.0,otarate=0.0;
							Integer occupancyRating=(int) Math.round(occupancy * 100);
						    int rateCount=0,rateIdCount=0;
						    double minAmount=0,maxAmount=0,promoAmount=0,firstPromoAmount=0,secondPromoAmount=0;
		    				
		    				boolean isWeekdays=false,isWeekend=false;
							for(int a=0;a<weekdays.length;a++){
								if(days.equalsIgnoreCase(weekdays[a])){
									isWeekdays=true;
								}
							}
							
							for(int b=0;b<weekend.length;b++){
								if(days.equalsIgnoreCase(weekend[b])){
									isWeekend=true;
								}
							}
							if(property.getLocationType()!=null){
								if(property.getLocationType().getLocationTypeName().equalsIgnoreCase("Leisure")){
									if(isWeekdays){
										if(accommodations.getWeekdaySellingRate()==null){
											maxAmount=0;	
										}else{
											maxAmount=accommodations.getWeekdaySellingRate();
										}
											
									}
									
									if(isWeekend){
										if(accommodations.getWeekendSellingRate()==null){
											maxAmount=0;
										}else{
											maxAmount=accommodations.getWeekendSellingRate();	
										}
										
									}	
								}
								
								if(property.getLocationType().getLocationTypeName().equalsIgnoreCase("Metro")){
									if(accommodations.getSellingRate()==null){
										maxAmount=0;	
									}else{
										maxAmount=accommodations.getSellingRate();
									}
										
								}
								
								if(occupancyRating>=0 && occupancyRating<=30){
									sellrate=maxAmount;
								}else if(occupancyRating>30 && occupancyRating<=60){
									sellrate=maxAmount+(maxAmount*10/100);
								}else if(occupancyRating>=60 && occupancyRating<=80){
									sellrate=maxAmount+(maxAmount*15/100);
								}else if(occupancyRating>80){
									sellrate=maxAmount+(maxAmount*20/100);
								}
							}
		    				
//							sellrate=maxAmount;
							if(discountType.equals("INR")){
								firstPromoAmount=dblDiscountINR;
								firstInr+=dblDiscountINR;
							}else if(discountType.equals("PERCENT")){
								firstPromoAmount=(sellrate*dblDiscountPercent/100);
							}else{
								firstPromoAmount=0;
							}
							
							if(strCheckInDate.equals(strCurrentDate)){ 
								if(occupancyRating>=0 && occupancyRating<=30){
									if(this.roomCnt>0){
										isLast=true;
									}else{
										isLast=false;
									}
								}
							}
							
							if(isLast){
		    					if(occupancyRating>=0 && occupancyRating<=30){
		    						if(AMPM==0){//If the current time is AM
		    		            		if(bookedHours>=0 && bookedHours<=11){
		    		            			if(maxAmount<1000){
		    									if(occupancyRating>=0 && occupancyRating<=30){
		    										secondPromoAmount=commissionrange1;
		    										secondInr+=commissionrange1;
		    									}
		    							    }else if(maxAmount>=1000 && maxAmount<=1500){
		    							    	if(occupancyRating>=0 && occupancyRating<=30){
		    							    		secondPromoAmount=commissionrange2;
		    							    		secondInr+=commissionrange2;
		    									}
		    							    }else if(maxAmount>1500){
		    							    	if(occupancyRating>=0 && occupancyRating<=30){
		    							    		secondPromoAmount=commissionrange3;
		    							    		secondInr+=commissionrange3;
		    									}
		    							    }
		    		            		}
		    		            	}
		    						if(AMPM==1){
		    							if(bookedHours>=0 && bookedHours<=3){
		    								secondPromoAmount=(sellrate*lastdiscountpercent1/100);
		    		            		}else if(bookedHours>=4 && bookedHours<=7){
		    		            			secondPromoAmount=(sellrate*lastdiscountpercent2/100);
		    		            		}else if(bookedHours>=8 && bookedHours<=11){
		    		            			secondPromoAmount=(sellrate*lastdiscountpercent3/100);
		    		            		}
		    						}
		    					}
		    				}
		    				promoAmount=firstPromoAmount+secondPromoAmount;
		    				
		    				
							totalMinimumAmount+=sellrate-promoAmount;
							totalMaximumAmount+=sellrate;
							extraAdultAmount+=accommodations.getExtraAdult();
							extraChildAmount+=accommodations.getExtraChild();
					    	countDate++;
					    }
						
						
						String promotionFirstName=null,promotionSecondName=null,promotionThirdName=null,
								promotionFirstType=null,promotionSecondType=null;
						
						if(isFlat){
							if(discountType.equalsIgnoreCase("INR")){
								promotionFirstName=String.valueOf(Math.round(firstInr))+"";
								promotionFirstType=discountType.toLowerCase();
							}else if(discountType.equalsIgnoreCase("PERCENT")){
								promotionFirstName=String.valueOf(Math.round(dblDiscountPercent))+"";
								promotionFirstType=discountType.toLowerCase();
							}
						}
						if(isEarly){
							if(discountType.equalsIgnoreCase("INR")){
								promotionFirstName=String.valueOf(Math.round(firstInr))+"";
								promotionFirstType=discountType.toLowerCase();
							}else if(discountType.equalsIgnoreCase("PERCENT")){
								promotionFirstName=String.valueOf(Math.round(dblDiscountPercent))+"";
								promotionFirstType=discountType.toLowerCase();
							}
						}
						if(isLast){
							if(AMPM==0){//If the current time is AM
    		            		if(bookedHours>=0 && bookedHours<=11){
    		            
    		            			String displayDate = new SimpleDateFormat("MMM dd,yyyy HH:mm:ss").format(lastFirst.getTime());
    								this.displayHoursForTimer=displayDate;
    								setDisplayHoursForTimer(this.displayHoursForTimer);

    								promotionSecondName=String.valueOf(Math.round(secondInr))+"";
    								promotionSecondType="inr";
    		            		}
    		            	}
    						if(AMPM==1){
    							if(bookedHours>=0 && bookedHours<=3){
    								String firstTime=checkStartDate+" 16:00:00"; 
    						   	    java.util.Date dteFirstDate = sdf.parse(strStartTime);
    						   	    Calendar calFirst=Calendar.getInstance();
    						   	    calFirst.setTime(dteFirstDate);
    						   	    calFirst.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
    						   	    String firstDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calFirst.getTime());
    						    	java.util.Date dteFirst = sdf.parse(firstTime);
    								String displayDate = new SimpleDateFormat("MMM dd,yyyy HH:mm:ss").format(dteFirst.getTime());
    								this.displayHoursForTimer=displayDate;
    								setDisplayHoursForTimer(this.displayHoursForTimer);
    								
    								promotionSecondName=String.valueOf(Math.round(lastdiscountpercent1))+"";
    								promotionSecondType="percent";
    		            		}else if(bookedHours>=4 && bookedHours<=7){
    		            			String firstTime=checkStartDate+" 20:00:00"; 
    						   	    java.util.Date dteFirstDate = sdf.parse(strStartTime);
    						   	    Calendar calFirst=Calendar.getInstance();
    						   	    calFirst.setTime(dteFirstDate);
    						   	    calFirst.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
    						   	    String firstDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calFirst.getTime());
    						    	java.util.Date dteFirst = sdf.parse(firstTime);
    								String displayDate = new SimpleDateFormat("MMM dd,yyyy HH:mm:ss").format(dteFirst.getTime());
    								this.displayHoursForTimer=displayDate;
    								setDisplayHoursForTimer(this.displayHoursForTimer);
    								promotionSecondType="percent";
    								promotionSecondName=String.valueOf(Math.round(lastdiscountpercent2))+"";
    		            		}else if(bookedHours>=8 && bookedHours<=11){
    		            			String firstTime=checkStartDate+" 23:00:00"; 
    						   	    java.util.Date dteFirstDate = sdf.parse(strStartTime);
    						   	    Calendar calFirst=Calendar.getInstance();
    						   	    calFirst.setTime(dteFirstDate);
    						   	    calFirst.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
    						   	    String firstDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calFirst.getTime());
    						    	java.util.Date dteFirst = sdf.parse(firstTime);
    								String displayDate = new SimpleDateFormat("MMM dd,yyyy HH:mm:ss").format(dteFirst.getTime());
    								this.displayHoursForTimer=displayDate;
    								setDisplayHoursForTimer(this.displayHoursForTimer);
    								promotionSecondType="percent";
    								promotionSecondName=String.valueOf(Math.round(lastdiscountpercent3))+"";
    		            		}
    						}

						}
						actualAdultAmount=extraAdultAmount;
						actualChildAmount=extraChildAmount;
					
						
						
						double variationAmount=0,variationRating=0;
						variationAmount=totalMaximumAmount-totalMinimumAmount;
						variationRating=variationAmount/totalMaximumAmount*100;
						if(variationRating>0){
							promotionThirdName=String.valueOf(Math.round(variationRating))+"%";	
						}
						
						actualTotalAdultAmount=Math.round(actualAdultAmount);
						actualTotalChildAmount=Math.round(actualChildAmount);
						
						
		            	
						long minimum = getAvailableCount(property.getPropertyId(),getArrivalDate(),getDepartureDate(),roomCount,accommId);
				    	 if(minimum<0){

				    		 jsonOutput += ",\"available\":\"" + minimum+ "\"";
				    		 jsonOutput += ",\"minimumAmount\":\"" + (totalMinimumAmount==0 ? 0 : Math.round(totalMinimumAmount) )+  "\"";
				    		 jsonOutput += ",\"maximumAmount\":\"" +(totalMaximumAmount==0 ? 0 : Math.round(totalMaximumAmount) )+  "\"";
				    		 jsonOutput += ",\"offerAmount\":\"" +(Math.round(totalMaximumAmount-totalMinimumAmount))+  "\"";
				    		 jsonOutput += ",\"promotionFirstName\":\"" + (promotionFirstName==null? "NA":promotionFirstName)+ "\"";
				    		 jsonOutput += ",\"promotionSecondName\":\"" + (promotionSecondName==null? "NA":promotionSecondName)+ "\"";
				    		 jsonOutput += ",\"promotionThirdName\":\"" + (promotionThirdName==null? "NA":promotionThirdName)+ "\"";
				    		 jsonOutput += ",\"promotionFirstType\":\"" + (promotionFirstType==null? "NA":promotionFirstType)+ "\"";
					    	 jsonOutput += ",\"promotionSecondType\":\"" + (promotionSecondType==null? "NA":promotionSecondType)+ "\"";
				    		 jsonOutput += ",\"accommodationName\":\"" + accommodations.getAccommodationType().toUpperCase()+ "\"";
							 jsonOutput += ",\"accommodationId\":\"" + accommodations.getAccommodationId()+ "\"";
							 isSoldout=true;
							 jsonOutput += ",\"isSoldout\":\"" + isSoldout+ "\"";
							 jsonOutput += ",\"isFlat\":\"" + isFlat+ "\"";
							 jsonOutput += ",\"isEarly\":\"" + isEarly+ "\"";
							 jsonOutput += ",\"isLast\":\"" + isLast+ "\"";
							 if(isLast){
								jsonOutput += ",\"promoLastMinuteTimer\":\"" + this.displayHoursForTimer+ "\"";	
							}else{
								jsonOutput += ",\"promoLastMinuteTimer\":\"" + "NA"+ "\"";
							}
					     
				    	 }else if(minimum==0){
				    		 jsonOutput += ",\"available\":\"" + minimum+ "\"";
				    		 jsonOutput += ",\"minimumAmount\":\"" + (totalMinimumAmount==0 ? 0 : Math.round(totalMinimumAmount) )+  "\"";
				    		 jsonOutput += ",\"maximumAmount\":\"" +(totalMaximumAmount==0 ? 0 : Math.round(totalMaximumAmount) )+  "\"";
				    		 jsonOutput += ",\"offerAmount\":\"" +(Math.round(totalMaximumAmount-totalMinimumAmount))+  "\"";
				    		 jsonOutput += ",\"promotionFirstName\":\"" + (promotionFirstName==null? "NA":promotionFirstName)+ "\"";
				    		 jsonOutput += ",\"promotionSecondName\":\"" + (promotionSecondName==null? "NA":promotionSecondName)+ "\"";
				    		 jsonOutput += ",\"promotionThirdName\":\"" + (promotionThirdName==null? "NA":promotionThirdName)+ "\"";
				    		 jsonOutput += ",\"promotionFirstType\":\"" + (promotionFirstType==null? "NA":promotionFirstType)+ "\"";
					    	 jsonOutput += ",\"promotionSecondType\":\"" + (promotionSecondType==null? "NA":promotionSecondType)+ "\"";
				    		 jsonOutput += ",\"accommodationName\":\"" + accommodations.getAccommodationType().toUpperCase()+ "\"";
							 jsonOutput += ",\"accommodationId\":\"" + accommodations.getAccommodationId()+ "\"";
							 isSoldout=true;
							 jsonOutput += ",\"isSoldout\":\"" + isSoldout+ "\"";
							 jsonOutput += ",\"isFlat\":\"" + isFlat+ "\"";
							 jsonOutput += ",\"isEarly\":\"" + isEarly+ "\"";
							 jsonOutput += ",\"isLast\":\"" + isLast+ "\"";
							 if(isLast){
								jsonOutput += ",\"promoLastMinuteTimer\":\"" + this.displayHoursForTimer+ "\"";	
							}else{
								jsonOutput += ",\"promoLastMinuteTimer\":\"" + "NA"+ "\"";
							}
					     }else{
					    	 isSoldout=false;
					    	 
					    	jsonOutput += ",\"accommodationName\":\"" + accommodations.getAccommodationType().toUpperCase()+ "\"";
							jsonOutput += ",\"accommodationId\":\"" + accommodations.getAccommodationId()+ "\"";
				    		jsonOutput += ",\"available\":\"" + minimum+ "\"";
							jsonOutput += ",\"minimumAmount\":\"" +(totalMinimumAmount==0 ? 0 : Math.round(totalMinimumAmount) )+  "\"";
							jsonOutput += ",\"maximumAmount\":\"" +(totalMaximumAmount==0 ? 0 : Math.round(totalMaximumAmount) )+  "\"";
							jsonOutput += ",\"offerAmount\":\"" + (Math.round(totalMaximumAmount-totalMinimumAmount))+  "\"";
							jsonOutput += ",\"promotionFirstName\":\"" + (promotionFirstName==null? "NA":promotionFirstName)+ "\"";
				    		jsonOutput += ",\"promotionSecondName\":\"" + (promotionSecondName==null? "NA":promotionSecondName)+ "\"";
				    		jsonOutput += ",\"promotionThirdName\":\"" + (promotionThirdName==null? "NA":promotionThirdName)+ "\"";
				    		jsonOutput += ",\"promotionFirstType\":\"" + (promotionFirstType==null? "NA":promotionFirstType)+ "\"";
				    		jsonOutput += ",\"promotionSecondType\":\"" + (promotionSecondType==null? "NA":promotionSecondType)+ "\"";
				    		jsonOutput += ",\"isSoldout\":\"" + isSoldout+ "\"";
				    		jsonOutput += ",\"isFlat\":\"" + isFlat+ "\"";
							jsonOutput += ",\"isEarly\":\"" + isEarly+ "\"";
							jsonOutput += ",\"isLast\":\"" + isLast+ "\"";
							if(isLast){
								jsonOutput += ",\"promoLastMinuteTimer\":\"" + this.displayHoursForTimer+ "\"";	
							}else{
								jsonOutput += ",\"promoLastMinuteTimer\":\"" + "NA"+ "\"";
							}
							
							 
					     }
					}
			     
				}
				
				jsonOutput += "}";
				count++;
				arllist.clear();
			
			}
			
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
	
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {
	
		}
	
		return null;
	  
	
	}
	
	public Integer getLocationId() {
		return locationId;
	}

	public void setLocationId(Integer locationId) {
		this.locationId = locationId;
	}

	public Integer getAreaId() {
		return areaId;
	}

	public void setAreaId(Integer areaId) {
		this.areaId = areaId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}
	
	public Integer getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(Integer propertyId) {
		this.propertyId = propertyId;
	}

	public String getPhotoPath() {
		return photoPath;
	}

	public void setPhotoPath(String photoPath) {
		this.photoPath = photoPath;
	}
	
	public String getPropertyThumbPath() {
		return propertyThumbPath;
	}

	public void setPropertyThumbPath(String propertyThumbPath) {
		this.propertyThumbPath = propertyThumbPath;
	}

	public List<Location> getLocationList() {
		return locationList;
	}

	public void setLocationList(List<Location> locationList) {
		this.locationList = locationList;
	}

	public List<LocationType> getLocationTypeList() {
		return locationTypeList;
	}

	public void setLocationTypeList(List<LocationType> locationTypeList) {
		this.locationTypeList = locationTypeList;
	}

	public Timestamp getArrivalDate() {
		return arrivalDate;
	}

	public void setArrivalDate(Timestamp arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	public Timestamp getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Timestamp departureDate) {
		this.departureDate = departureDate;
	}
	
	public String getMetaTag() {
		return metaTag;
	}

	public void setMetaTag(String metaTag) {
		this.metaTag = metaTag;
	}

	public String getMetaDescription() {
		return metaDescription;
	}

	public void setMetaDescription(String metaDescription) {
		this.metaDescription = metaDescription;
	}

	public String getWebContent() {
		return webContent;
	}

	public void setWebContent(String webContent) {
		this.webContent = webContent;
	}
	
	public Integer getSourceId() {
		return sourceId;
	}

	public void setSourceId(Integer sourceId) {
		this.sourceId = sourceId;
	}

	public long getRoomCnt() {
		return roomCnt;
	}

	public void setRoomCnt(long roomCnt) {
		this.roomCnt = roomCnt;
	}
	
	public Integer getPropertyAccommodationId() {
		return propertyAccommodationId;
	}

	public void setPropertyAccommodationId(Integer propertyAccommodationId) {
		this.propertyAccommodationId = propertyAccommodationId;
	}
	
	public String getStrStartDate() {
		return strStartDate;
	}

	public void setStrStartDate(String strStartDate) {
		this.strStartDate = strStartDate;
	}

	public String getStrEndDate() {
		return strEndDate;
	}

	public void setStrEndDate(String strEndDate) {
		this.strEndDate = strEndDate;
	}
	
	public String getStartDate() {
		return StartDate;
	}

	public void setStartDate(String startDate) {
		StartDate = startDate;
	}

	public String getEndDate() {
		return EndDate;
	}

	public void setEndDate(String endDate) {
		EndDate = endDate;
	}
	
public Long getAvailableCount(int propertyId,Timestamp arrivalDate,Timestamp departureDate,long available,int accommodationId) throws IOException {
		
		String startDate = new SimpleDateFormat("yyyy-MM-dd").format(arrivalDate);
		String endDate = new SimpleDateFormat("yyyy-MM-dd").format(departureDate);
		DateTime start = DateTime.parse(startDate);
	    DateTime end = DateTime.parse(endDate);
		PmsBookingManager bookingController = new PmsBookingManager();
		PropertyAccommodationManager accommodationController=new PropertyAccommodationManager();
		List<DateTime> between = DateUtil.getDateRange(start, end);
		List<Long> myList = new ArrayList<Long>();
		long rmc = 0;
		this.roomCnt = rmc;
		for (DateTime d : between)
	     {
			java.util.Date availDate = d.toDate();
			PropertyAccommodationInventoryManager accommodationInventoryController = new PropertyAccommodationInventoryManager();  
			PropertyAccommodationInventory inventoryCount = accommodationInventoryController.findInventoryCount(availDate,accommodationId);
				if(inventoryCount != null){
					
					
					PmsAvailableRooms roomCount1 = bookingController.findBookingCount(availDate,accommodationId,inventoryCount.getInventoryId());
					if(roomCount1.getRoomCount() == null) {
						String num1 = inventoryCount.getInventoryCount();
						long num2 = Long.parseLong(num1);
						PmsAvailableRooms bookingRoomCount=bookingController.findCount(availDate, accommodationId);
						int sold=0;
						PropertyAccommodation accommodations=accommodationController.find(accommodationId);
						Integer totavailable=0;
						 if(accommodations!=null){
							totavailable=accommodations.getNoOfUnits();
						 }
						if(bookingRoomCount.getRoomCount()!=null){
							long lngRooms=bookingRoomCount.getRoomCount();
							sold=(int) (lngRooms);
							int availRooms=0;
							availRooms=totavailable-sold;
//							if(Integer.parseInt(num1)>availRooms){
//								this.roomCnt = availRooms;	
//							}else{
								this.roomCnt = num2;
//							}
						}else{
							this.roomCnt = num2; 
						}
						 /*String num1 = inventoryCount.getInventoryCount();
						 long num2 = Long.parseLong(num1);
						this.roomCnt = num2;*/ 
					}
					else {
						PmsAvailableRooms bookingRoomCount=bookingController.findCount(availDate, accommodationId);
						int intRooms=0;
						PropertyAccommodation accommodations=accommodationController.find(accommodationId);
						Integer totavailable=0;
						 if(accommodations!=null){
							totavailable=accommodations.getNoOfUnits();
						 }
						if(bookingRoomCount.getRoomCount()!=null){
							long lngRooms=bookingRoomCount.getRoomCount();
							long num = roomCount1.getRoomCount();	
							intRooms=(int)lngRooms;
							String num1 = inventoryCount.getInventoryCount();
								long num2 = Long.parseLong(num1);
								Integer totRooms=totavailable-intRooms;
//								if(num2>totRooms){
//									this.roomCnt = totRooms;	 
//								}else{
									this.roomCnt = num2-num;
//								}							
						}else{
							long num = roomCount1.getRoomCount();
							String num1 = inventoryCount.getInventoryCount();
							long num2 = Long.parseLong(num1);
							this.roomCnt = (num2-num);	
						}
						 /*long num = roomCount1.getRoomCount();
						 String num1 = inventoryCount.getInventoryCount();
						 long num2 = Long.parseLong(num1);
						this.roomCnt = (num2-num);*/	
					}
					
				}
				else{
					
					PmsAvailableRooms roomCount = bookingController.findCount(availDate, accommodationId);
					if(roomCount.getRoomCount() == null){
						this.roomCnt = (available-rmc);
					}
					else{
						this.roomCnt = (available-roomCount.getRoomCount());
					}
				}
			myList.add(this.roomCnt);
			}			
        long availableCount = Collections.min(myList);

        return availableCount;
	          
	}

	public Boolean getCurrentAvailable(int PropertyId,Timestamp start,Timestamp end,Double dbllastMinutesHours,long available,int accommmodationId,Timestamp lastMinuteStart,Timestamp lastMinuteEnd)throws Exception{
		 int currentHours=0,AMPM=0,startHours=0,endHours=0,lastMinStartHours=0;
		 Timestamp lastStartDate=null,lastEndDate=null; 
		 SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		 SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
		 String checkFromDate = new SimpleDateFormat("yyyy-MM-dd").format(lastMinuteStart);
    	 String strCheckInTime=checkFromDate+" 10:00:00"; 
   	     java.util.Date StartDate = sdf.parse(strCheckInTime);
		 lastStartDate=new Timestamp(StartDate.getTime());
		 int lastMinutesHours=dbllastMinutesHours.intValue();
		 String checkToDate = new SimpleDateFormat("yyyy-MM-dd").format(lastMinuteEnd);
    	 String strCheckOutTime=checkToDate+" 08:00:00"; 
   	     java.util.Date EndDate = sdf.parse(strCheckOutTime);
		 lastEndDate=new Timestamp(EndDate.getTime());
		 
		 Calendar calStart=Calendar.getInstance();
		 calStart.setTime(lastStartDate);
		 startHours=calStart.get(Calendar.HOUR);
    	 calStart.add(Calendar.HOUR,-lastMinutesHours);
    	 lastMinStartHours=calStart.get(Calendar.HOUR);
	
    	 String strFromDate = new SimpleDateFormat("yyyy-MM-dd").format(calStart.getTime());
    	 java.util.Date fromDate = format1.parse(strFromDate);

    	 
		 Calendar calEnd=Calendar.getInstance();
		 calEnd.setTime(lastEndDate);
		 endHours=calEnd.get(Calendar.HOUR);
		 String strToDate = new SimpleDateFormat("yyyy-MM-dd").format(calEnd.getTime());
    	 java.util.Date toDate = format1.parse(strToDate);
		 
    	
		 Calendar calendar=Calendar.getInstance();
		 currentHours = calendar.get(Calendar.HOUR);
		 AMPM=calendar.get(Calendar.AM_PM);
		 
		 Calendar today=Calendar.getInstance();
    	 String strCurrentDate = new SimpleDateFormat("yyyy-MM-dd").format(today.getTime());
    	 java.util.Date currentDate = format1.parse(strCurrentDate);
    	 
    	 Calendar calArrivalDate=Calendar.getInstance();
    	 calArrivalDate.setTime(start);
    	 String strArrivalDate = new SimpleDateFormat("yyyy-MM-dd").format(calArrivalDate.getTime());
    	 java.util.Date ArrivalDate = format1.parse(strArrivalDate);
		 
		 /*long diff = toDate.getTime() - fromDate.getTime();
		 long diffHours = diff / (60 * 60 * 1000);
		 int diffInDays = (int) ((toDate.getTime() - fromDate.getTime()) / (1000 * 60 * 60 * 24));
		 int diffInHours=(int)diffHours;
		 boolean lastMinuteActive=false;*/
		 
		 
		if(currentDate.equals(ArrivalDate)){
			if(fromDate.equals(currentDate)){
	 			 if(AMPM==0){// If current time is AM 
		   			if(currentHours==lastMinStartHours){
		   				 return true;
		   			}else if(currentHours>=lastMinStartHours){
		   				long minimumCount = getAvailableCount(getPropertyId(),start,end,available,accommmodationId);
		   				if((int)minimumCount>0){
		   					return true;
		   				}else{
		   					return false;
		   				}
		   			}
		   		 }
		   		 else if(AMPM==1){// if current time is PM
		   			 if(currentHours<=12){
		   				 long minimumCount = getAvailableCount(getPropertyId(),start,end,available,accommmodationId);
		   				 if((int)minimumCount>0){
		   					 return true;
		   				 }else{
		   					return false; 
		   				 }
		   			}
		   		}
	 		 } else if (currentDate.after(fromDate) && currentDate.before(toDate)) {
	 			 if(AMPM==0){// If current time is AM 
		   			 if(currentHours<=lastMinStartHours){
		   				 return true;
		   			}else if(currentHours>=lastMinStartHours){
		   				long minimumCount = getAvailableCount(getPropertyId(),start,end,available,accommmodationId);
		   				if((int)minimumCount>0){
		   					return true;
		   				}else{
		   					return false;
		   				}
		   			}
		   		 }
		   		 else if(AMPM==1){// if current time is PM
		   			 if(currentHours<=12){
		   				 long minimumCount = getAvailableCount(getPropertyId(),start,end,available,accommmodationId);
		   				 if((int)minimumCount>0){
		   					 return true;
		   				 }else{
		   					return false; 
		   				 }
		   			}
		   		 }
	 			
	 		 }else if (toDate.equals(currentDate)) {
	 			 if(AMPM==0){// If current time is AM 
		   			 if(currentHours<=lastMinStartHours){
		   				 return true;
		   			}else if(currentHours>=lastMinStartHours){
		   				long minimumCount = getAvailableCount(getPropertyId(),start,end,available,accommmodationId);
		   				if((int)minimumCount>0){
		   					return true;
		   				}else{
		   					return false;
		   				}
		   			}
		   		 }
		   		 else if(AMPM==1){// if current time is PM
		   			 if(currentHours<=endHours){
		   				 long minimumCount = getAvailableCount(getPropertyId(),start,end,available,accommmodationId);
		   				 if((int)minimumCount>0){
		   					 return true;
		   				 }else{
		   					return false; 
		   				 }
		   			}
		   		 }
	  		 }
		}
		 
		 return false;
	 }
	
	 public Boolean bookGetRooms(int availableCount,Double getRoom,Double bookRoom){
		 Double totalGetRooms=0.0;
	     totalGetRooms=bookRoom+getRoom;
	     Integer intTotalGetRooms=totalGetRooms.intValue();
	    	if(intTotalGetRooms<=availableCount){
	    		sessionMap.put("promotionsRoomCount", intTotalGetRooms);
	   	     	sessionMap.put("promotionsRoomNight", "Rooms");
	    		 return true;
	    	}
	    	else{
	    		 return false;
	    	}

		
	 }
	 
	 public Boolean bookGetNights(int availableCount, Double getNight,Double bookNight,int diffDays){
		 Double totalGetNights=0.0;
		 totalGetNights=getNight+bookNight;
	     Integer intTotalGetNights=totalGetNights.intValue();
	     int roomCountDays=0;
	     roomCountDays=availableCount*diffDays;
	     if(intTotalGetNights<=roomCountDays){
    		 return true;
    	}
    	else{
    		 return false;
    	}
	 }
	 
	 
	 public String getImpressionProperties() throws IOException {
			try {
			
			PmsPropertyManager propertyController = new PmsPropertyManager();	
			PropertyAccommodationManager accommodationManager = new PropertyAccommodationManager();
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			
			response.setContentType("application/json");	
			//response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			if (!jsonOutput.equalsIgnoreCase(""))
				jsonOutput += ",{";
			
			else
				
				//jsonOutput += "\"ecommerce\":\"" + "impressionsEvent" + "\"";
				jsonOutput += "{";
	        
	        //jsonOutput += "\"ecommerce\":\"" + + "\"";
			
			//jsonOutput += "\"event\":\"" + "impressionsEvent" + "\"";
			//jsonOutput += ",\"currencyCode\":\"" + "INR" + "\"";
			
	        
			
//				jsonOutput += ",\"propertyStandardPolicy\":\"" + (property.getPropertyStandardPolicy() == null ? "None": property.getPropertyStandardPolicy().trim())+ "\"";
//				jsonOutput += ",\"propertyHotelPolicy\":\"" + (property.getPropertyHotelPolicy() == null ? "None": property.getPropertyHotelPolicy().trim())+ "\"";
//				jsonOutput += ",\"propertyCancellationPolicy\":\"" + (property.getPropertyCancellationPolicy() == null ? "None": property.getPropertyCancellationPolicy().trim())+ "\"";
				
			/*	jsonOutput += ",\"arrivalDate\":\"" + format2.format(FromDate) + "\"";
				jsonOutput += ",\"departureDate\":\"" + format2.format(ToDate)+ "\"";*/
				String jsonAmenities ="";
				this.propertyList = propertyController.listPropertyByLocation(getLocationId());
				for (PmsProperty property : propertyList) {
					
					if (!jsonAmenities.equalsIgnoreCase(""))
						jsonAmenities += ",{";
					else
						jsonAmenities += "{";
					jsonAmenities += "\"id\":\"" + property.getPropertyId()+ "\"";
					//jsonAmenities += ",\"city\":\"" + property.getCity() + "\"";
					jsonAmenities += ",\"name\":\"" + (property.getPropertyName() == null ? "": property.getPropertyName().toUpperCase().trim())+ "\"";
					
					List<PropertyAccommodation> accommodationList = accommodationManager.list(property.getPropertyId());
					for (PropertyAccommodation accommodations : accommodationList) {
					jsonAmenities += ",\"price\":\"" + accommodations.getBaseAmount() + "\"";	
					}
					jsonAmenities += "}";
					
					}
					
					jsonOutput += "\"impressions\":[" + jsonAmenities+ "]";
				
			
			  
				jsonOutput += "}";
				
				
			
		 
	        
			response.getWriter().write("{\"data\":" + jsonOutput + "}");
//			sessionMap.clear();

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	public String getMyFileContentType() {
		return myFileContentType;
	}

	public void setMyFileContentType(String myFileContentType) {
		this.myFileContentType = myFileContentType;
	}

	public String getMyFileFileName() {
		return myFileFileName;
	}

	public void setMyFileFileName(String myFileFileName) {
		this.myFileFileName = myFileFileName;
	}
	
	public File getMyFile() {
		return myFile;
	}

	public void setMyFile(File myFile) {
		this.myFile = myFile;
	}
	
	public String getEditLocationId() {
		return editLocationId;
	}

	public void setEditLocationId(String editLocationId) {
		this.editLocationId = editLocationId;
	}

	public String getEditLocationName() {
		return editLocationName;
	}

	public void setEditLocationName(String editLocationName) {
		this.editLocationName = editLocationName;
	}

	public String getEditLocationDescription() {
		return editLocationDescription;
	}

	public void setEditLocationDescription(String editLocationDescription) {
		this.editLocationDescription = editLocationDescription;
	}
	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	public Integer getOffset() {
		return offset;
	}

	public void setOffset(Integer offset) {
		this.offset = offset;
	}
	
	public String getLocationUrl() {
		return locationUrl;
	}

	public void setLocationUrl(String locationUrl) {
		this.locationUrl = locationUrl;
	}
	
	public String getLocationDescription() {
		return locationDescription;
	}

	public void setLocationDescription(String locationDescription) {
		this.locationDescription = locationDescription;
	}

	public String getDisplayHoursForTimer() {
		return displayHoursForTimer;
	}

	public void setDisplayHoursForTimer(String displayHoursForTimer) {
		this.displayHoursForTimer = displayHoursForTimer;
	}

	public Integer getSourceTypeId() {
		return sourceTypeId;
	}

	public String getFilterSearch() {
		return filterSearch;
	}

	public void setFilterSearch(String filterSearch) {
		this.filterSearch = filterSearch;
	}
	
	public void setSourceTypeId(Integer sourceTypeId) {
		this.sourceTypeId = sourceTypeId;
	}
	public Boolean checkLastMinutePromos(String promotionHoursRange,Timestamp tsArrivalDate,Timestamp tsDepartureDate,Timestamp tsStartRange,Timestamp tsEndRange,Timestamp tsFirstRangeDate,Timestamp tsSecondRangeDate){
		boolean blnCheck=false;
		try{
			ArrayList<String> arlDate=new ArrayList<String>();
			int intCount=0,lastCurrentHours=0,currentAMPM=0,firstRangeAMPM=0,secondRangeAMPM=0;
			int firstRange=0,secondRange=0,lastFirstRangeHours=0,lastSecondRangeHours=0;
			String[] arrRange=promotionHoursRange.split("-");
			ArrayList<String> arlRange=new ArrayList<String>();
			for(String str:arrRange){
				arlRange.add(str);
			}
	        Iterator<String> rangeIter=arlRange.iterator();
		    while(rangeIter.hasNext()) {
	            firstRange=Integer.parseInt(rangeIter.next());
	            secondRange=Integer.parseInt(rangeIter.next());
		    }
		    
			Integer startRangeHours=0,startRangeAMPM=0,endRangeHours=0,endRangeAMPM=0;
//			java.util.Date curdate=new java.util.Date();
			java.util.Date currentdate=new java.util.Date();
			Calendar calCurrentDate=Calendar.getInstance();
			calCurrentDate.setTime(currentdate);
			calCurrentDate.setTimeZone(TimeZone.getTimeZone("IST"));
			java.util.Date curdate=calCurrentDate.getTime();
			
			Timestamp tsStartDate=null,tsEndDate=null; 
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
	   	    
	   	    String checkArrivalDate = new SimpleDateFormat("yyyy-MM-dd").format(tsArrivalDate);
	   	    java.util.Date arrivalDate = format1.parse(checkArrivalDate);
			String checkDepartureDate = new SimpleDateFormat("yyyy-MM-dd").format(tsDepartureDate);
	   	    java.util.Date departureDate = format1.parse(checkDepartureDate);
	   	    
	   	    String strStartRangeDate = new SimpleDateFormat("yyyy-MM-dd").format(tsFirstRangeDate);
	   	    String strStartRange= sdf.format(tsStartRange);
	   	    java.util.Date dteStartRangeDate = sdf.parse(strStartRange);
	   	    Calendar calRangeStart=Calendar.getInstance();
	   	    calRangeStart.setTime(dteStartRangeDate);
	   	    calRangeStart.setTimeZone(TimeZone.getTimeZone("IST"));
	    	startRangeHours=calRangeStart.get(Calendar.HOUR_OF_DAY);
	    	startRangeAMPM=calRangeStart.get(Calendar.AM_PM);
	    	
			String strEndRangeDate = new SimpleDateFormat("yyyy-MM-dd").format(tsSecondRangeDate);
			String strEndRange = sdf.format(tsEndRange);
	   	    java.util.Date dteEndRangeDate = sdf.parse(strEndRange);
	   	    Calendar calRangeEnd=Calendar.getInstance();
	   	    calRangeEnd.setTime(dteEndRangeDate);
	   	    calRangeEnd.setTimeZone(TimeZone.getTimeZone("IST"));
	    	endRangeHours=calRangeEnd.get(Calendar.HOUR_OF_DAY);
	    	endRangeAMPM=calRangeEnd.get(Calendar.AM_PM);

	    	String checkArrival = new SimpleDateFormat("yyyy-MM-dd").format(arrivalDate);
	    	String strArrivalTime=checkArrival+" "+startRangeHours+":00:00"; 
	   	    java.util.Date dteArrivalDate = sdf.parse(strArrivalTime);
			Calendar calArrivalRange=Calendar.getInstance();
			calArrivalRange.setTime(dteArrivalDate);
			calArrivalRange.setTimeZone(TimeZone.getTimeZone("IST"));
	    	String strArrivalDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calArrivalRange.getTime());
	    	java.util.Date dteRangeArrival = sdf.parse(strArrivalDate);
	    	arlDate.add(checkArrival);
	    	
	    	Calendar today=Calendar.getInstance();
	    	today.setTimeZone(TimeZone.getTimeZone("IST"));
	    	String strTodayDate = new SimpleDateFormat("yyyy-MM-dd").format(today.getTime());
	    	java.util.Date todayDate = format1.parse(strTodayDate);
	    	
	    	DateTime start = DateTime.parse(strStartRangeDate);
	 	    DateTime end = DateTime.parse(strEndRangeDate);
	    	List<DateTime> between = DateUtil.getDateRange(start, end);
			for(DateTime d : between)
		    {
				java.util.Date availDate = d.toDate();
				String strCurrentDate = new SimpleDateFormat("yyyy-MM-dd").format(availDate);
		   	    java.util.Date currentDate = format1.parse(strCurrentDate);
				if(currentDate.equals(todayDate)){
					
					if((int)startRangeAMPM==1 && (int)endRangeAMPM==0){
						
						String checkStartDate = new SimpleDateFormat("yyyy-MM-dd").format(arrivalDate);
				    	String strStartTime=checkStartDate+" 12:00:00"; 
				   	    java.util.Date dteStartDate = sdf.parse(strStartTime);
				   	    Calendar calStart=Calendar.getInstance();
				   	    calStart.setTime(dteStartDate);
				   	    calStart.setTimeZone(TimeZone.getTimeZone("IST"));
				   	    String StartDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calStart.getTime());
				    	java.util.Date dteRangeStart = sdf.parse(StartDate);

				   	    
			    		Calendar calFirst=Calendar.getInstance();
						calFirst.setTime(dteRangeStart);
						calFirst.setTimeZone(TimeZone.getTimeZone("IST"));
						calFirst.add(Calendar.HOUR, -firstRange);
						String FirstDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calFirst.getTime());
				    	java.util.Date dteRangeFirst = sdf.parse(FirstDate);
				    	Calendar calFirstRange=Calendar.getInstance();
				    	calFirstRange.setTime(dteRangeFirst);
				    	
				    	Calendar calSecond=Calendar.getInstance();
				    	calSecond.setTime(dteRangeStart);
				    	calSecond.setTimeZone(TimeZone.getTimeZone("IST"));
				    	calSecond.add(Calendar.HOUR, -secondRange);
						String SecondDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calSecond.getTime());
				    	java.util.Date dteRangeSecond = sdf.parse(SecondDate);
				    	Calendar calSecondRange=Calendar.getInstance();
				    	calSecondRange.setTime(dteRangeSecond);
				    	
						/*Calendar calFirst=Calendar.getInstance();
						calFirst.setTime(arrivalDate);
						calFirst.add(Calendar.HOUR, -firstRange);
						String checkStartDate = new SimpleDateFormat("yyyy-MM-dd").format(arrivalDate);
				    	String strStartTime=checkStartDate+" "+startRangeHours+":00:00"; 
				   	    java.util.Date dteStartDate = sdf.parse(strStartTime);
						Calendar calStartRange=Calendar.getInstance();
						calStartRange.setTime(dteStartDate);
						calStartRange.add(Calendar.DAY_OF_MONTH, -1);
				    	String FromDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calStartRange.getTime());
				    	java.util.Date dteRangeFrom = sdf.parse(FromDate);
				    	
				    	Calendar calSecond=Calendar.getInstance();
				    	calSecond.setTime(arrivalDate);
				    	calSecond.add(Calendar.HOUR, -secondRange);
						
				    	String checkEndDate = new SimpleDateFormat("yyyy-MM-dd").format(arrivalDate);
				    	String strEndTime=checkStartDate+" "+endRangeHours+":00:00"; 
				   	    java.util.Date dteEndDate = sdf.parse(strEndTime);
				    	Calendar calEndRange=Calendar.getInstance();
				    	calEndRange.setTime(dteEndDate);
				    	String ToDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calEndRange.getTime());
				    	java.util.Date dteRangeTo = sdf.parse(ToDate);*/
				    	
				    	if(curdate.after(calSecond.getTime()) && curdate.before(calFirst.getTime())) {
				    		blnCheck=true;
				    		
				    		String displayDate = new SimpleDateFormat("MMM dd,yyyy HH:mm:ss").format(calFirst.getTime());
				    		long diff =  dteRangeFirst.getTime()-curdate.getTime();

							long diffSeconds = diff / 1000 % 60;
							long diffMinutes = diff / (60 * 1000) % 60;
							long diffHours = diff / (60 * 60 * 1000) % 24;
							long diffDays = diff / (24 * 60 * 60 * 1000);
							
							this.displayHoursForTimer=displayDate;
							setDisplayHoursForTimer(this.displayHoursForTimer);
				    	}
			    	}else{
			    		
			    		String checkStartDate = new SimpleDateFormat("yyyy-MM-dd").format(arrivalDate);
				    	String strStartTime=checkStartDate+" 12:00:00"; 
				   	    java.util.Date dteStartDate = sdf.parse(strStartTime);
				   	    Calendar calStart=Calendar.getInstance();
				   	    calStart.setTime(dteStartDate);
				   	    calStart.setTimeZone(TimeZone.getTimeZone("IST"));
				   	    String StartDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calStart.getTime());
				    	java.util.Date dteRangeStart = sdf.parse(StartDate);

				   	    
			    		Calendar calFirst=Calendar.getInstance();
						calFirst.setTime(dteRangeStart);
						calFirst.setTimeZone(TimeZone.getTimeZone("IST"));
						calFirst.add(Calendar.HOUR, -firstRange);
						String FirstDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calFirst.getTime());
				    	java.util.Date dteRangeFirst = sdf.parse(FirstDate);
				    	Calendar calFirstRange=Calendar.getInstance();
				    	calFirstRange.setTime(dteRangeFirst);
				    	
			    		/*String checkStartDate = new SimpleDateFormat("yyyy-MM-dd").format(arrivalDate);
				    	String strStartTime=checkStartDate+" "+startRangeHours+":00:00"; 
				   	    java.util.Date dteStartDate = sdf.parse(strStartTime);
						Calendar calStartRange=Calendar.getInstance();
						calStartRange.setTime(dteStartDate);
				    	String FromDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calStartRange.getTime());
				    	java.util.Date dteRangeFrom = sdf.parse(FromDate);*/
				    	
				    	Calendar calSecond=Calendar.getInstance();
				    	calSecond.setTime(dteRangeStart);
				    	calSecond.setTimeZone(TimeZone.getTimeZone("IST"));
				    	calSecond.add(Calendar.HOUR, -secondRange);
						String SecondDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calSecond.getTime());
				    	java.util.Date dteRangeSecond = sdf.parse(SecondDate);
				    	Calendar calSecondRange=Calendar.getInstance();
				    	calSecondRange.setTime(dteRangeSecond);
				    	
				    	/*String checkEndDate = new SimpleDateFormat("yyyy-MM-dd").format(arrivalDate);
				    	String strEndTime=checkStartDate+" "+endRangeHours+":00:00"; 
				   	    java.util.Date dteEndDate = sdf.parse(strEndTime);
				    	Calendar calEndRange=Calendar.getInstance();
				    	calEndRange.setTime(dteEndDate);
				    	String ToDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calEndRange.getTime());
				    	java.util.Date dteRangeTo = sdf.parse(ToDate);*/
				    	
				    	if(curdate.after(calSecond.getTime()) && curdate.before(calFirst.getTime())) {
				    		blnCheck=true;
				    		
				    		String displayDate = new SimpleDateFormat("MMM dd,yyyy HH:mm:ss").format(calFirst.getTime());
				    		long diff =  dteRangeFirst.getTime()-curdate.getTime();

							long diffSeconds = diff / 1000 % 60;
							long diffMinutes = diff / (60 * 1000) % 60;
							long diffHours = diff / (60 * 60 * 1000) % 24;
							long diffDays = diff / (24 * 60 * 60 * 1000);
							
							this.displayHoursForTimer=displayDate;
							setDisplayHoursForTimer(this.displayHoursForTimer);
				    	}
			    	}
				}
		    }
		}catch(Exception e){
			logger.equals(e);
			e.printStackTrace();
		}
		return blnCheck;
	}
	
	public Boolean checkEarlyBirdPromos(Timestamp bookedDate,Timestamp earlyStartDate,Timestamp earlyEndDate,Timestamp arrival,Timestamp departure){
		boolean blnCheck=false;
		try{
			java.util.Date curdate=new java.util.Date();
			Timestamp tsStartDate=null,tsEndDate=null; 
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
			String checkFromDate = new SimpleDateFormat("yyyy-MM-dd").format(earlyStartDate);
	   	    java.util.Date StartDate = format1.parse(checkFromDate);
			String checkToDate = new SimpleDateFormat("yyyy-MM-dd").format(earlyEndDate);
	   	    java.util.Date EndDate = format1.parse(checkToDate);
			
	   	    String checkArrivalDate = new SimpleDateFormat("yyyy-MM-dd").format(arrival);
	   	    java.util.Date arrivalDate = format1.parse(checkArrivalDate);
			String checkDepartureDate = new SimpleDateFormat("yyyy-MM-dd").format(departure);
	   	    java.util.Date departureDate = format1.parse(checkDepartureDate);
	   	    
	   	    String checkBookedDate = new SimpleDateFormat("yyyy-MM-dd").format(bookedDate);
	   	    java.util.Date dteBookedDate = format1.parse(checkBookedDate);
	   	    
	   	    DateTime start = DateTime.parse(checkFromDate);
	 	    DateTime end = DateTime.parse(checkToDate);
	    	List<DateTime> between = DateUtil.getDateRange(start, end);
			for(DateTime d : between)
		    {
				java.util.Date availDate = d.toDate();
				String strCurrentDate = new SimpleDateFormat("yyyy-MM-dd").format(curdate);
		   	    java.util.Date currentDate = format1.parse(strCurrentDate);
				if(currentDate.equals(dteBookedDate) && arrivalDate.equals(availDate)){
					
					blnCheck=true;
				}
		    }
			
		}catch(Exception e){
			logger.error(e);
			e.printStackTrace();
		}
		return blnCheck;
	}

	public Boolean getCheckHoursRange(String promotionHoursRange,Timestamp startDate,Timestamp endDate){
		boolean blnCheck=false;
		try{
			int intCount=0,lastCurrentHours=0,currentAMPM=0,firstRangeAMPM=0,secondRangeAMPM=0;
			int firstRange=0,secondRange=0,lastFirstRangeHours=0,lastSecondRangeHours=0;
			String[] arrRange=promotionHoursRange.split("-");
			ArrayList<String> arlRange=new ArrayList<String>();
			for(String str:arrRange){
				arlRange.add(str);
			}
	        Iterator<String> rangeIter=arlRange.iterator();
		    while(rangeIter.hasNext()) {
	            firstRange=Integer.parseInt(rangeIter.next());
	            secondRange=Integer.parseInt(rangeIter.next());
		    }
		    
		    int currentHours=0,AMPM=0,startHours=0,endHours=0,lastMinStartHours=0;
			Timestamp lastStartDate=null,lastEndDate=null; 
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
			String checkFromDate = new SimpleDateFormat("yyyy-MM-dd").format(startDate);
	   	    java.util.Date StartDate = format1.parse(checkFromDate);
			lastStartDate=new Timestamp(StartDate.getTime());
			String checkToDate = new SimpleDateFormat("yyyy-MM-dd").format(endDate);
	   	    java.util.Date EndDate = format1.parse(checkToDate);
			lastEndDate=new Timestamp(EndDate.getTime());
			 
			Calendar calStart=Calendar.getInstance();
			calStart.setTime(lastStartDate);
	    	String strFromDate = new SimpleDateFormat("yyyy-MM-dd").format(calStart.getTime());
	    	java.util.Date fromDate = format1.parse(strFromDate);
	
			Calendar calEnd=Calendar.getInstance();
			calEnd.setTime(lastEndDate);
			String strToDate = new SimpleDateFormat("yyyy-MM-dd").format(calEnd.getTime());
	    	java.util.Date toDate = format1.parse(strToDate);
			 
			Calendar today=Calendar.getInstance();
	    	String strCurrentDate = new SimpleDateFormat("yyyy-MM-dd").format(today.getTime());
	    	java.util.Date currentDate = format1.parse(strCurrentDate);
	
	    	DateTime start = DateTime.parse(checkFromDate);
	 	    DateTime end = DateTime.parse(checkToDate);
	    	List<DateTime> between = DateUtil.getDateRange(start, end);
			for(DateTime d : between)
		    {

				String checkStartDate = new SimpleDateFormat("yyyy-MM-dd").format(d.toDate());
		    	String strCheckInTime=checkStartDate+" 12:00:00"; 
		   	    java.util.Date dteStartDate = sdf.parse(strCheckInTime);
		   	    
		   	    java.util.Date curdate=new java.util.Date();
		   	    Calendar calCurrentDate=Calendar.getInstance();
		   	    calCurrentDate.setTime(curdate);
		    	lastCurrentHours=calCurrentDate.get(Calendar.HOUR_OF_DAY);
		    	currentAMPM=calCurrentDate.get(Calendar.AM_PM);
		    	
			   	Calendar calStartRange=Calendar.getInstance();
				calStartRange.setTime(dteStartDate);
		    	calStartRange.add(Calendar.HOUR,-firstRange);
		    	lastFirstRangeHours=calStartRange.get(Calendar.HOUR_OF_DAY);
		    	firstRangeAMPM=calStartRange.get(Calendar.AM_PM);
		    	String FromDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calStartRange.getTime());
		    	java.util.Date dteRangeFrom = sdf.parse(FromDate);
		    	
		    	Calendar calEndRange=Calendar.getInstance();
		    	calEndRange.setTime(dteStartDate);
		    	calEndRange.add(Calendar.HOUR,-secondRange);
		    	lastSecondRangeHours=calEndRange.get(Calendar.HOUR_OF_DAY);
		    	secondRangeAMPM=calEndRange.get(Calendar.AM_PM);
		    	String ToDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calEndRange.getTime());
		    	java.util.Date dteRangeTo = sdf.parse(ToDate);
		    	
		    	Timestamp tsDateRangeFrom=new Timestamp(dteRangeFrom.getTime());
		    	Timestamp tsDateRangeTo=new Timestamp(dteRangeTo.getTime());

		    	DateTime dt1 = new DateTime(dteRangeFrom);
				DateTime dt2 = new DateTime(dteRangeTo);
				DateTime dt = new DateTime(curdate);

				if(curdate.after(calEndRange.getTime()) && curdate.before(calStartRange.getTime())) {
					blnCheck=true;
					long diff = curdate.getTime() - dteRangeTo.getTime();

					long diffSeconds = diff / 1000 % 60;
					long diffMinutes = diff / (60 * 1000) % 60;
					long diffHours = diff / (60 * 60 * 1000) % 24;
					long diffDays = diff / (24 * 60 * 60 * 1000);
					String strTimerCount=String.valueOf(diffHours)+":"+String.valueOf(diffMinutes)+":"+String.valueOf(diffSeconds);
					this.displayHoursForTimer=strTimerCount;
					setDisplayHoursForTimer(this.displayHoursForTimer);
			    }else{
			    	blnCheck=false;
			    }
		    }
		}catch(Exception e){
			logger.error(e);
			e.printStackTrace();
		}
		return blnCheck;
	}
}
