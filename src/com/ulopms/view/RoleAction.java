package com.ulopms.view;

import java.io.IOException;
import java.sql.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionSupport;
import com.ulopms.controller.AccessRightManager;
import com.ulopms.controller.RoleAccessRightManager;
import com.ulopms.controller.RoleManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.AccessRight;
import com.ulopms.model.Role;
import com.ulopms.model.RoleAccessRight;
import com.ulopms.model.User;

public class RoleAction extends ActionSupport implements UserAware {

	// private static final long serialVersionUID = 9149826260758390091L;
	private List<Role> roleList;
	private List<AccessRight> accessrightList;
	private List<RoleAccessRight> roleaccessrightList;
	private String oper = "";
	private int roleId;
	private String roleName;
	private String roleDescription;
	private String checkedRoleAccess;
	
	private RoleManager linkController;
	private User user;
	private static final Logger logger = Logger.getLogger(RoleAction.class);

	// @Override
	// public User getModel() {
	// return user;
	// }

	@Override
	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	public String getOper() {
		return oper;
	}

	public void setOper(String oper) {
		this.oper = oper;
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getRoleDescription() {
		return roleDescription;
	}

	public void setRoleDescription(String roleDescription) {
		this.roleDescription = roleDescription;
	}

	public RoleAction() {
		linkController = new RoleManager();
	}

	public String getRoles() {
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();

			response.setContentType("application/json");
			// response.getWriter().write("[{\"start\":\"2015-05-11T00:00:00\",\"title\":\"test\"}]");

			List<Role> roleList = linkController.list();

			for (Role r : roleList) {

				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"DT_RowId\":\"" + r.getRoleId() + "\"";
				jsonOutput += ",\"roleName\":\"" + r.getRoleName() + "\"";
				jsonOutput += ",\"roleDescription\":\""
						+ r.getRoleDescription() + "\"";
				jsonOutput += "}";

			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");

		} catch (Exception e) {
			logger.error(e);
		} finally {

		}
		return null;
	}
	
	public String getRoleAccess() throws IOException {
		try {
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
	
			//this.propertyId = (Integer) sessionMap.get("propertyId");
			RoleManager roleController = new RoleManager();
			AccessRightManager accessrightController = new AccessRightManager();
			RoleAccessRightManager roleaccessrightController = new RoleAccessRightManager();
			response.setContentType("application/json");
			this.accessrightList = accessrightController.list();
			for (AccessRight accessright : accessrightList) {
	
			this.roleaccessrightList = roleaccessrightController.list(getRoleId(),accessright.getAccessRightsId());
	
			if (!jsonOutput.equalsIgnoreCase(""))
			jsonOutput += ",{";
			else
			jsonOutput += "{";
	
	
			jsonOutput += "\"accessRightsId\":\"" + accessright.getAccessRightsId() + "\"";
			jsonOutput += ",\"accessRights\":\"" + accessright.getAccessRights()+ "\"";
			jsonOutput += ",\"displayName\":\"" + accessright.getDisplayName()+ "\"";
			jsonOutput += ",\"rightsType\":\"" + accessright.getRightsType()+ "\"";
	
			if(roleaccessrightList.size()>0) jsonOutput += ",\"status\":\"true\""; else jsonOutput += ",\"status\":\"false\"";
	
			jsonOutput += "}";
	
			}
	
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");

		} catch (Exception e) {
		logger.error(e);
		e.printStackTrace();
		} finally {

		}

		return null;

		}
	
	public String saveRoleAccess() {
		//this.propertyId = (Integer) sessionMap.get("propertyId");
		RoleManager roleController = new RoleManager();
		AccessRightManager accessrightController = new AccessRightManager();
		RoleAccessRightManager roleaccessrightController = new RoleAccessRightManager();
		try {

			this.roleaccessrightList = roleaccessrightController.list(getRoleId());
			for (RoleAccessRight roleaccess : roleaccessrightList) {
				roleaccess.setAccessRightsPermission(false);
				roleaccessrightController.edit(roleaccess);
			}
			RoleAccessRight roleaccessrights = new RoleAccessRight();
	
			String[] stringArray = getCheckedRoleAccess().split("\\s*,\\s*");
			int[] intArray = new int[stringArray.length];
			for (int i = 0; i < stringArray.length; i++) {
				String numberAsString = stringArray[i];
				intArray[i] = Integer.parseInt(numberAsString);
			}
	
			for (int number : intArray) {
				long time = System.currentTimeMillis();
				java.sql.Date date = new java.sql.Date(time);
				roleaccessrights.setAccessRightsPermission(true);
				Role role = roleController.find(getRoleId());
				roleaccessrights.setRole(role);
				AccessRight accessright = accessrightController.find(number);
				roleaccessrights.setAccessRight(accessright);
				roleaccessrightController.add(roleaccessrights);
	
			}

		} catch (Exception e) {
		logger.error(e);
		e.printStackTrace();
		} finally {

		}
		return SUCCESS;
		}
	

	public String execute() {

		return SUCCESS;
	}

	public String add() {
		try {
			Role role = new Role();
			// role.setRoleId(1L);
			role.setRoleName(roleName);
			role.setRoleDescription(roleDescription);
			linkController.add(role);
		} catch (Exception e) {
			logger.error(e);
		} finally {

		}
		// this.roleList = linkController.list();
		return null;
	}

	public String edit() {
		try {
			Role role = new Role();
			role.setRoleId(roleId);
			role.setRoleName(roleName);
			role.setRoleDescription(roleDescription);
			linkController.edit(role);
			this.roleList = linkController.list();
		} catch (Exception e) {
			logger.error(e);
		} finally {

		}

		return SUCCESS;
	}

	public String delete() {
		try {
			linkController.delete(getRoleId());
		} catch (Exception e) {
			logger.error(e);
		} finally {

		}

		return SUCCESS;
	}

	public List<Role> getRoleList() {
		return roleList;
	}

	public void setRoleList(List<Role> rolesList) {
		this.roleList = rolesList;
	}
	
	public String getCheckedRoleAccess() {
		return checkedRoleAccess;
	}

	public void setCheckedRoleAccess(String checkedRoleAccess) {
		this.checkedRoleAccess = checkedRoleAccess;
	}


}
