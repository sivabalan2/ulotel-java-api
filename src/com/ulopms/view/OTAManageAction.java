package com.ulopms.view;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;






































import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.apache.poi.ss.formula.functions.Rate;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.derby.tools.sysinfo;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.rest.DefaultHttpHeaders;
import org.apache.struts2.rest.HttpHeaders;
import org.w3c.dom.Element;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.ulopms.controller.BookingDetailManager;
import com.ulopms.controller.BookingGuestDetailManager;
import com.ulopms.controller.CountryManager;
import com.ulopms.controller.OtaHotelsManager;
import com.ulopms.controller.PmsBookingManager;
import com.ulopms.controller.PmsGuestManager;
import com.ulopms.controller.PmsPropertyManager;
import com.ulopms.controller.PmsSourceManager;
import com.ulopms.controller.PmsStatusManager;
import com.ulopms.controller.PropertyAccommodationManager;
import com.ulopms.controller.PropertyContractTypeManager;
import com.ulopms.controller.PropertyDiscountManager;
import com.ulopms.controller.PropertyRatePlanManager;
import com.ulopms.controller.PropertyTypeManager;
import com.ulopms.controller.StateManager;
import com.ulopms.controller.UserLoginManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.BookedDetail;
import com.ulopms.model.BookingDetail;
import com.ulopms.model.BookingGuestDetail;
import com.ulopms.model.Country;
import com.ulopms.model.OtaBookingDetails;
import com.ulopms.model.OtaHotelDetails;
import com.ulopms.model.OtaHotels;
import com.ulopms.model.PmsBookedDetails;
import com.ulopms.model.PmsBooking;
import com.ulopms.model.PmsGuest;
import com.ulopms.model.PmsPromotions;
import com.ulopms.model.PmsProperty;
import com.ulopms.model.PmsSmartPrice;
import com.ulopms.model.PmsSource;
import com.ulopms.model.PmsStatus;
import com.ulopms.model.PropertyAccommodation;
import com.ulopms.model.PropertyAccommodationInventory;
import com.ulopms.model.PropertyContractType;
import com.ulopms.model.PropertyDiscount;
import com.ulopms.model.PropertyRatePlan;
import com.ulopms.model.PropertyType;
import com.ulopms.model.State;
import com.ulopms.model.User;
import com.ulopms.util.Email;

import freemarker.template.Configuration;
import freemarker.template.Template;


public class OTAManageAction extends ActionSupport implements
ServletRequestAware,SessionAware, UserAware,ModelDriven<Object>{
     
	

	private static final long serialVersionUID = 914982665758390091L;
	
	List<OtaBookingDetails> BookingListing = new ArrayList<OtaBookingDetails>();
	
	

	private static final Logger logger = Logger.getLogger(OTAManageAction.class);
	
	
	private String strStartDate;
	
	private String strEndDate;
	
	private Long hotelCode;
	private int guestId;
	private Integer bookingId;
	
	private Integer propertyId;
	private String otaHotelCode;
	private String otaBearerToken;
	private String otaChannelToken;
	private String otaRatePlanId;
	private String otaRoomTypeId;
	private Integer propertyAccommodationId;
	private Integer ratePlanId;
	private Integer otaHotelDetailsId;
	

	
	

	private HttpSession session;

	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}
	
	private SessionMap<String,Object> sessionMap;  
	  
	  
	@Override  
	public void setSession(Map<String, Object> map) {  
	    sessionMap=(SessionMap)map;  
	}  
	
	private User user;

	

	@Override
	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	
	private HttpServletRequest request;

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	public OTAManageAction() {

	}

	public String execute() {
		return SUCCESS;
	}
	
	
	public String saveOtaHotels() {
		try {
			
			this.propertyId = (Integer) sessionMap.get("propertyId");
			OtaHotelsManager otaHotelController = new OtaHotelsManager();
			PmsPropertyManager proprtyController = new PmsPropertyManager(); 
			PmsSourceManager sourceController = new PmsSourceManager();
		    
			int sourceId = 3;
			
			OtaHotels otaHotels = otaHotelController.getOtaHotels(getPropertyId(),sourceId);
			if(otaHotels != null){
				otaHotels.setOtaHotelCode(getOtaHotelCode());
				otaHotels.setOtaBearerToken(getOtaBearerToken());
				otaHotels.setOtaChannelToken(getOtaChannelToken());
				otaHotels.setOtaIsActive(true);
				otaHotelController.edit(otaHotels);
				
			}
			else{
				OtaHotels otaHotel = new OtaHotels();
				otaHotel.setOtaHotelCode(getOtaHotelCode());
				otaHotel.setOtaBearerToken(getOtaBearerToken());
				otaHotel.setOtaChannelToken(getOtaChannelToken());
				otaHotel.setOtaIsActive(true);
				PmsProperty property = proprtyController.find(getPropertyId());
				otaHotel.setPmsProperty(property);
				PmsSource pmsSource = sourceController.find(sourceId);
				otaHotel.setPmsSource(pmsSource);
				otaHotelController.add(otaHotel);
			}
		
			
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}
	
	public String saveOtaHotelDetails() {
		try {
			
			this.propertyId = (Integer) sessionMap.get("propertyId");
			OtaHotelsManager otaHotelController = new OtaHotelsManager();
			PmsPropertyManager proprtyController = new PmsPropertyManager(); 
			PmsSourceManager sourceController = new PmsSourceManager();
			PropertyRatePlanManager ratePlanController = new PropertyRatePlanManager(); 
			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
		    
			OtaHotelDetails otaHotelDetails = new OtaHotelDetails();
			
			otaHotelDetails.setOtaHotelCode(getOtaHotelCode());
			otaHotelDetails.setOtaRateplanId(getOtaRatePlanId());
			otaHotelDetails.setOtaRoomTypeId(getOtaRoomTypeId());
			otaHotelDetails.setIsActive(true);
			PmsProperty property = proprtyController.find(getPropertyId());
			otaHotelDetails.setPmsProperty(property);
			PropertyAccommodation accommodation = accommodationController.find(getPropertyAccommodationId());
			otaHotelDetails.setPropertyAccommodation(accommodation);
			PropertyRatePlan ratePlan = ratePlanController.find(getRatePlanId());
			otaHotelDetails.setPropertyRatePlan(ratePlan);
			otaHotelController.add(otaHotelDetails);
			
		
			
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}
	
	public String deleteOtaHotelDetails() {
		try {
			
			//this.propertyId = (Integer) sessionMap.get("propertyId");
			OtaHotelsManager otaHotelController = new OtaHotelsManager();
		    
			OtaHotelDetails otaHotelDetails = otaHotelController.findDetailId(getOtaHotelDetailsId());
			
			otaHotelDetails.setIsActive(false);
			
			otaHotelController.edit(otaHotelDetails);
			
		
			
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}
	
	public String getOtaHotels() {
		try {
			
		String jsonOutput = "";
		HttpServletResponse response = ServletActionContext.getResponse();
		
		this.propertyId = (Integer) sessionMap.get("propertyId");
		PmsPropertyManager propertyController = new PmsPropertyManager();
		OtaHotelsManager otaHotelController = new OtaHotelsManager();
		int sourceId = 3;
		//this.familyRegisterList = familyController.list(getUser().getUserid());
		//model = familyRegisterList;
		response.setContentType("application/json");
		OtaHotels otaHotels = otaHotelController.getOtaHotels(getPropertyId(),sourceId);
		
		if (!jsonOutput.equalsIgnoreCase(""))
			jsonOutput += ",{";
		else
			jsonOutput += "{";
		
		
		jsonOutput += "\"otaHotelCode\":\"" + (otaHotels.getOtaHotelCode() == null ? "": otaHotels.getOtaHotelCode())+ "\"";
		jsonOutput += ",\"otaBearerToken\":\"" + (otaHotels.getOtaBearerToken() == null ? "": otaHotels.getOtaBearerToken())+ "\"";
		jsonOutput += ",\"otaChannelToken\":\"" + (otaHotels.getOtaChannelToken() == null ? "": otaHotels.getOtaChannelToken())+ "\"";
		
		
		jsonOutput += "}";

		

		response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
		
      } catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}
	
	public String getOtaFullHotelDetails() {
		try {
			
		String jsonOutput = "";
		HttpServletResponse response = ServletActionContext.getResponse();
		
		this.propertyId = (Integer) sessionMap.get("propertyId");
		PmsPropertyManager propertyController = new PmsPropertyManager();
		OtaHotelsManager otaHotelController = new OtaHotelsManager();
		//int sourceId = 3;
		//this.familyRegisterList = familyController.list(getUser().getUserid());
		//model = familyRegisterList;
		response.setContentType("application/json");
		PropertyRatePlanManager ratePlanController = new PropertyRatePlanManager(); 
		PropertyAccommodationManager propertyAccommodationController = new PropertyAccommodationManager();
		List <OtaHotelDetails> otaHotellist = otaHotelController.list(getPropertyId());
		
			for(OtaHotelDetails detailList : otaHotellist){
		if (!jsonOutput.equalsIgnoreCase(""))
			jsonOutput += ",{";
		else
			jsonOutput += "{";
		
		jsonOutput += "\"DT_RowId\":\"" + detailList.getOtaHotelDetailsId()+ "\"";
		jsonOutput += ",\"otaRoomTypeId\":\"" + (detailList.getOtaRoomTypeId())+ "\"";
		jsonOutput += ",\"otaRatePlanId\":\"" + (detailList.getOtaRateplanId())+ "\"";
		PropertyRatePlan ratePlan = ratePlanController.find(detailList.getPropertyRatePlan().getRatePlanId());
		
		PropertyAccommodation accommodation = propertyAccommodationController.find(detailList.getPropertyAccommodation().getAccommodationId());
		jsonOutput += ",\"ratePlanName\":\"" + (ratePlan.getRatePlanType())+ "\"";
		jsonOutput += ",\"accommodationName\":\"" + (accommodation.getAccommodationType())+ "\"";
		jsonOutput += ",\"isActive\":\"" + (detailList.getIsActive())+ "\"";
		
		
		jsonOutput += "}";

			}

		response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
		
      } catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}

	public String getOtaHotelDetails() {
		String res = "";
		try {
			
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			
			this.propertyId = (Integer) sessionMap.get("propertyId");
			PmsPropertyManager propertyController = new PmsPropertyManager();
			OtaHotelsManager otaHotelController = new OtaHotelsManager();
			int sourceId = 3;
			OtaHotels otaHotel = otaHotelController.getOtaHotels(getPropertyId(), sourceId);
			response.setContentType("application/json");
			if(otaHotel != null){
				HttpClient httpClient = new DefaultHttpClient();
				HttpPost postRequest = new HttpPost("https://in.goibibo.com/api/chmv2/gethotellisting/?bearer_token="+otaHotel.getOtaBearerToken()+"&channel_token="+otaHotel.getOtaChannelToken()+"");
				String strXMLBodyFormat="<?xml version='1.0' encoding='UTF-8' ?>"+
						" <Website Name='ingoibibo' HotelCode='"+otaHotel.getOtaHotelCode()+"' Version='1'>"+
							"<HotelCode>"+otaHotel.getOtaHotelCode()+"</HotelCode>"+
						"</Website>";
				
				
				StringEntity input = new StringEntity(strXMLBodyFormat);
				input.setContentType("text/json");
				postRequest.setEntity(input);
				HttpResponse httpResponse = httpClient.execute(postRequest);
				HttpEntity entity = httpResponse.getEntity();
		        // Read the contents of an entity and return it as a String.
		        String content = EntityUtils.toString(entity);
		        
				//if(content.contains("BookingId")){
					JSONObject bookingListJsonObject = XML.toJSONObject(content);
					JSONObject jsonValues = (JSONObject) bookingListJsonObject.get("HotelListing");
					JSONObject jsonValues1 = (JSONObject) jsonValues.get("RatePlanList");
					JSONArray jsonBookingId = jsonValues1.getJSONArray("RatePlan");
		          
		            for(int i=0;i<jsonBookingId.length();i++){
		            	if (!jsonOutput.equalsIgnoreCase(""))
							jsonOutput += ",{";
						else
							jsonOutput += "{";
		            	
//		            	JSONObject jsonObject = jsonBookingId.getJSONObject(i);
//		                String jsonObjectAsString = jsonObject.toString();
		                jsonOutput += "\"roomTypeName\":\"" + jsonBookingId.getJSONObject(i).getString("RoomTypeName")+ "\"";
		                jsonOutput += ",\"roomTypeCode\":\"" + jsonBookingId.getJSONObject(i).getLong("RoomTypeCode")+ "\"";
		                jsonOutput += ",\"isActive\":\"" + jsonBookingId.getJSONObject(i).getBoolean("IsActive")+ "\"";
		                jsonOutput += ",\"ratePlanCode\":\"" + jsonBookingId.getJSONObject(i).getLong("RatePlanCode")+ "\"";
		                jsonOutput += ",\"ratePlanName\":\"" + jsonBookingId.getJSONObject(i).getString("RatePlanName")+ "\"";
		                jsonOutput += ",\"meanPlan\":\"" + jsonBookingId.getJSONObject(i).getString("MealPlan")+ "\"";
		                //jsonOutput += ",\"payMode\":\"" + jsonBookingId.getJSONObject(i).getString("PayMode")+ "\"";
		               // jsonOutput += ",\"noOfNights\":\"" + jsonBookingId.getJSONObject(i).getLong("NoOfNights")+ "\"";
		              
		                jsonOutput += "}";
		            }
		            res = SUCCESS;
				}
	
			else{
				res = ERROR;
			}
			
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
      } catch (Exception e) {
    	  res = ERROR;
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
		
	}

	
	
	public String getOtaBookingList(){
		try{
			Map<String, String> mapJson=new HashMap<String, String>();
			String jsonOutput = "";
			DateFormat format = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
			SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
			java.util.Date dateStart = format.parse(getStrStartDate());
			java.util.Date dateEnd = format.parse(getStrEndDate());
			Calendar calCheckStart=Calendar.getInstance();
		    calCheckStart.setTime(dateStart);
		    java.util.Date checkInDate = calCheckStart.getTime();
		   
		    
		    Calendar calCheckEnd=Calendar.getInstance();
		    calCheckEnd.setTime(dateEnd);
		    java.util.Date checkOutDate = calCheckEnd.getTime();
		    this.strStartDate=format1.format(checkInDate);
		    this.strEndDate=format1.format(checkOutDate);
		    		
		    
		    
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("application/json");
			HttpClient httpClient = new DefaultHttpClient();
			HttpPost postRequest = new HttpPost("https://ppin.goibibo.com/api/chmv2/getbookinglisting/?bearer_token=31ff186ee4&channel_token=i3jiqz44rb");
			String strXMLBodyFormat="<?xml version='1.0' encoding='UTF-8' ?>"+
					" <Website Name='ingoibibo' HotelCode='1000100943'>"+
					"<StartDate Format='yyyy-mm-dd'>"+this.strStartDate+"</StartDate>"+
					"<EndDate Format='yyyy-mm-dd'>"+this.strEndDate+"</EndDate>"+
					"<BookingType booking='True' cancellation='True' checkin='False'></BookingType>"+
					" <BookingStatus>pending</BookingStatus>"+
					"</Website>";
			
			
			StringEntity input = new StringEntity(strXMLBodyFormat);
			input.setContentType("text/json");
			postRequest.setEntity(input);
			HttpResponse httpResponse = httpClient.execute(postRequest);
			HttpEntity entity = httpResponse.getEntity();
            // Read the contents of an entity and return it as a String.
			String content = EntityUtils.toString(entity);
			
			
			if(content.contains("BookingId")){
				JSONObject bookingListJsonObject = XML.toJSONObject(content);
				JSONObject jsonValues = (JSONObject) bookingListJsonObject.get("BookingListing");
				JSONArray jsonBookingId = jsonValues.getJSONArray("Booking");
	          
	            for(int i=0;i<jsonBookingId.length();i++){
	            	if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += ",{";
					else
						jsonOutput += "{";
	            	
	            	JSONObject jsonObject = jsonBookingId.getJSONObject(i);
	                String jsonObjectAsString = jsonObject.toString();
	                jsonOutput += "\"bookingId\":\"" + jsonBookingId.getJSONObject(i).getString("BookingId")+ "\"";
	                jsonOutput += ",\"status\":\"" + jsonBookingId.getJSONObject(i).getString("Status")+ "\"";
	                jsonOutput += ",\"roomTypeName\":\"" + jsonBookingId.getJSONObject(i).getString("RoomTypeName")+ "\"";
	                jsonOutput += ",\"checkInDate\":\"" + jsonBookingId.getJSONObject(i).getString("CheckInDate")+ "\"";
	                jsonOutput += ",\"customerName\":\"" + jsonBookingId.getJSONObject(i).getString("CustomerName")+ "\"";
	                jsonOutput += ",\"noOfRooms\":\"" + jsonBookingId.getJSONObject(i).getLong("NoOfRooms")+ "\"";
	                jsonOutput += ",\"payAtHotelFlag\":\"" + jsonBookingId.getJSONObject(i).getBoolean("PayAtHotelFlag")+ "\"";
	                jsonOutput += ",\"noOfNights\":\"" + jsonBookingId.getJSONObject(i).getLong("NoOfNights")+ "\"";
	              
	                jsonOutput += "}";
	            }
	            jsonOutput= "{\"BookingListing\":[" + jsonOutput + "]}";
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return null;
	}
	
	
	public String getOtaBookedDetails(){
		
		try{
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("application/json");
			HttpClient httpClient = new DefaultHttpClient();
			HttpPost postRequest = new HttpPost("https://ppin.goibibo.com/api/chmv2/getbookingdetail/?bearer_token=31ff186ee4&channel_token=i3jiqz44rb");
			String strXMLBodyFormat="<?xml version='1.0' encoding='UTF-8' ?>"+
					" <Website Name='ingoibibo' HotelCode='1000100943' Version='2'>"+
						"<BookingId>0000067712</BookingId>"+
						"<PaymentDetailsRequired>True</PaymentDetailsRequired>"+
						"<FetchBookingAddOns>True</FetchBookingAddOns>"+
					"</Website>";
			
			
			StringEntity input = new StringEntity(strXMLBodyFormat);
			input.setContentType("text/json");
			postRequest.setEntity(input);
			HttpResponse httpResponse = httpClient.execute(postRequest);
			HttpEntity entity = httpResponse.getEntity();
            // Read the contents of an entity and return it as a String.
            String content = EntityUtils.toString(entity);
            
            JSONObject bookingDetailJsonObject = XML.toJSONObject(content);
            response.getWriter().write("{\"data\":[" + bookingDetailJsonObject + "]}");
            
            JSONObject jsonValues = (JSONObject) bookingDetailJsonObject.get("BookingDetail");
            JSONObject jsonValues1 = (JSONObject) jsonValues.get("Booking");
            JSONObject jsonValues2 = (JSONObject) jsonValues1.get("PriceDetails");
            JSONObject jsonValues3 = (JSONObject) jsonValues1.get("RoomStay");
            JSONObject jsonValues4 = (JSONObject) jsonValues3.get("Room");
            //JSONObject jsonBookingId = (JSONObject) jsonValues2.get("SellAmount");
			String val = jsonValues1.getString("CheckInDate");
			DateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
			
			java.util.Date arrivalDate = outputFormat.parse(jsonValues1.getString("CheckInDate"));
			java.util.Date departureDate = outputFormat.parse(jsonValues1.getString("CheckoutDate"));
			Timestamp aDate =new Timestamp(arrivalDate.getTime());
			Timestamp dDate=new Timestamp(departureDate.getTime());
			PmsPropertyManager propertyController = new PmsPropertyManager();
			PmsBookingManager bookingController = new PmsBookingManager();
			OtaHotelsManager otaHotelsController = new OtaHotelsManager();
			PmsGuestManager guestController = new PmsGuestManager();
			BookingDetailManager bookingDetailController = new BookingDetailManager();
			BookingGuestDetailManager bookingGuestDetailController = new BookingGuestDetailManager();
			PmsStatusManager statusController = new PmsStatusManager();
			PmsSourceManager sourceController = new PmsSourceManager();
			PmsBooking booking = new PmsBooking();
			OtaHotelDetails otahotelDetail = otaHotelsController.findPropertyId(jsonValues1.getLong("HotelCode"),jsonValues1.getLong("RoomTypeCode"));
			
			
			booking.setArrivalDate(aDate);
			booking.setDepartureDate(dDate);
			booking.setRooms(jsonValues1.getInt("NumberofRooms"));
			booking.setIsActive(true);
			booking.setIsDeleted(false);
			booking.setTotalAmount(jsonValues2.getInt("TotalPayAtHotelAmount"));
			booking.setTotalTax(jsonValues2.getInt("GST"));
			/*booking.setOtaCommission(totalOtaCommissionAmount);
			booking.setOtaTax(totalOtaTaxAmount);
			booking.setTotalActualAmount(totalActualBaseAmount);*/
			
			PmsSource source = sourceController.find(jsonValues1.getString("BookingVendorName"));
			booking.setPmsSource(source);
			PmsProperty property = propertyController.find(otahotelDetail.getPmsProperty().getPropertyId());
			booking.setPmsProperty(property);
			PmsStatus status = statusController.find(2);
			booking.setPmsStatus(status);
			PmsBooking book = bookingController.add(booking);
			sessionMap.remove("bookingId");
			this.bookingId = book.getBookingId();
			sessionMap.put("bookingId",bookingId);
			
			this.bookingId = (Integer) sessionMap.get("bookingId");
			
			/*add guest table*/
			
			PmsGuest guest = new PmsGuest();
			
			
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			
			guest.setEmailId(jsonValues1.getString("GuestEmail"));
			guest.setFirstName(jsonValues1.getString("GuestName")); 
			guest.setIsActive(true);
			guest.setIsDeleted(false);
			guest.setPhone(jsonValues1.getString("GuestPhoneNo")); 
			
			
			SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
			Calendar today=Calendar.getInstance();
		    String strCurrentDate = new SimpleDateFormat("yyyy-MM-dd").format(today.getTime());
		    java.util.Date currentDate = format1.parse(strCurrentDate);
		    Timestamp currentTS = new Timestamp(currentDate.getTime());
		    guest.setCreatedDate(currentTS);
		    
			PmsGuest guestli = guestController.add(guest);
			
			this.guestId = guestli.getGuestId();
			sessionMap.put("guestId",guestId);	
			
			/*add booking detail table*/ 
			
			
			 BookingGuestDetail bookingGuestDetail = new BookingGuestDetail();
             PmsBooking bookingg = bookingController.find(getBookingId());
             bookingGuestDetail.setPmsBooking(bookingg);
             PmsGuest guestId = guestController.find(getGuestId());
             bookingGuestDetail.setPmsGuest(guestId);
             bookingGuestDetail.setIsPrimary(true);
             /*if(preBookFlag){
             	bookingGuestDetail.setIsActive(false);
                 bookingGuestDetail.setIsDeleted(true);
             }else{
             	bookingGuestDetail.setIsActive(true);
                 bookingGuestDetail.setIsDeleted(false);
             }*/
             
             bookingGuestDetail.setSpecialRequest(jsonValues1.getString("SpecialRequest"));
             bookingGuestDetailController.add(bookingGuestDetail);
			
			DateTime start = DateTime.parse(jsonValues1.getString("CheckInDate"));
            DateTime end = DateTime.parse(jsonValues1.getString("CheckoutDate"));
          
            
           	

             List<DateTime> between = getDateRange(start, end);
             for (DateTime d : between)
             {
            	 
            	 
            
            	/* java.util.Date bookingDate=  d.toDate();              
                 BookingDetail bookingDetail = new BookingDetail();
                
                SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                java.util.Date date=new java.util.Date(); 
                Calendar today=Calendar.getInstance();
      			today.setTime(date);
      			today.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
      		    String strCurrentDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(today.getTime());
      		    java.util.Date currentDate = format1.parse(strCurrentDate);
      		    Timestamp currentTS = new Timestamp(currentDate.getTime());
                 
                 bookingDetail.setAdultCount(jsonValues4.getInt("Adult"));
                 bookingDetail.setChildCount(jsonValues4.getInt("Child"));
                 bookingDetail.setRefund(array.get(i).getRefund());
                 bookingDetail.setAdvanceAmount(array.get(i).getAdvanceAmount()/array.get(i).getDiffDays());
                 bookingDetail.setAmount(array.get(i).getTotal()/array.get(i).getDiffDays());
                 bookingDetail.setTax(array.get(i).getTax()/array.get(i).getDiffDays());  
                 bookingDetail.setOtaCommission(array.get(i).getOtaCommission()/array.get(i).getDiffDays());
                 bookingDetail.setOtaTax(array.get(i).getOtaTax()/array.get(i).getDiffDays());
                 if(this.userId!=null){
                 	bookingDetail.setCreatedBy(this.userId);
                 }
                 bookingDetail.setCreatedDate(currentTS);
                 bookingDetail.setBookingDate(bookingDate);
                 bookingDetail.setIsActive(true);
                 bookingDetail.setIsDeleted(false);
                 PmsStatus status = statusController.find(array.get(i).getStatusId());
                 bookingDetail.setPmsStatus(status);
                 PropertyAccommodation propertyAccommodation = propertyAccommodationController.find(array.get(i).getAccommodationId());

                 bookingDetail.setPropertyAccommodation(propertyAccommodation);
                 PmsBooking booking = bookingController.find(getBookingId());
                 bookingDetail.setPmsBooking(booking);
                 bookingDetail.setRandomNo(array.get(i).getRandomNo());
                 bookingDetail.setInfantCount(array.get(i).getInfantCount());
                 bookingDetail.setRoomCount(array.get(i).getRooms());
                 bookingDetail.setActualAmount(Math.round(array.get(i).getBaseAmount() * array.get(i).getRooms() /array.get(i).getDiffDays()));
                 
                
                 	
                 	
                 	PropertyAccommodationInventory inventoryCount = accommodationInventoryController.findInventoryCount(bookingDate,array.get(i).getAccommodationId());
                 	 if(inventoryCount.getInventoryId() != null){
                 	PropertyAccommodationInventory inventory = accommodationInventoryController.find(inventoryCount.getInventoryId());
                     bookingDetail.setPropertyAccommodationInventory(inventory);
                 	
                 	}
                
                 // for promotion
                 sessionMap.put("promotionName", array.get(i).getPromotionName());
                 
                 if(array.get(i).getDiscountId() != null){
                     PropertyDiscount discount = discountController.find(array.get(i).getDiscountId());
                     bookingDetail.setPropertyDiscount(discount);
                 }else{
                    	
                 	//bookingDetail.setPmsBooking(booking);
                 	Integer frontDiscountId = null,frontSmartPriceId=null,frontPromotionId=null;
                 	frontSmartPriceId= array.get(0).getSmartPriceId();
                 	frontPromotionId= array.get(0).getPromotionId();
                 	frontDiscountId = array.get(0).getDiscountId();                   	
                 	
                 	
                 	if(frontDiscountId != null && frontDiscountId!=0){  
                 		//frontDiscountId = array.get(0).getDiscountId();	
                 		PropertyDiscount discount = discountController.find(frontDiscountId);
                         bookingDetail.setPropertyDiscount(discount);
                 	
                 	}
                 	
                 	if(frontSmartPriceId != null && frontSmartPriceId!=0){
                 		//frontDiscountId = array.get(0).getDiscountId();	
                 		PmsSmartPrice pmsSmartPrice = smartPriceController.find(frontSmartPriceId);
                         bookingDetail.setPmsSmartPrice(pmsSmartPrice);
                 	
                 	}
                 	
                 	if(frontPromotionId != null && frontPromotionId!=0){
                 		//frontDiscountId = array.get(0).getDiscountId();	
                 		PmsPromotions pmsPromotions = promotionController.find(frontPromotionId);
                         bookingDetail.setPmsPromotions(pmsPromotions);
                 	
                 	}
                 }

                
                 //PmsRoomDetail roomDetail = bookingDetailController.findRoom(arrival, departure,array.get(i).getAccommodationId());
                 //PropertyAccommodationRoom accommodationRoom = propertyAccommodationRoomController.find(roomDetail.getRoomId());
                 //bookingDetail.setPropertyAccommodationRoom(accommodationRoom);
                 //bookingDetail.set(roomDetail.getRoomId());
                 bookingDetailController.add(bookingDetail);*/
                 
             }
			
			
             //getBookedDetails();
			
            
            
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return null;
	}
	
	public String getOtaBookingNotificationDetails(){
		try{
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("application/json");
			HttpClient httpClient = new DefaultHttpClient();
			HttpPost postRequest = new HttpPost("https://localhost:8080/ulopms/api/Goibibo/bookingnotification");
			/*String strXMLBodyFormat="<?xml version='1.0' encoding='UTF-8' ?>"+
					" <Website Name='ingoibibo' HotelCode='1000100943' Version='2'>"+
						"<StartDate Format='yyyy-mm-dd'>2013-01-10</StartDate>"+
						"<EndDate Format='yyyy-mm-dd'>2013-01-11</EndDate>"+
					"</Website>";
			
			StringEntity input = new StringEntity(strXMLBodyFormat);
			input.setContentType("text/json");
			postRequest.setEntity(input);*/
			
			HttpResponse httpResponse = httpClient.execute(postRequest);
			HttpEntity entity = httpResponse.getEntity();
            // Read the contents of an entity and return it as a String.
            String content = EntityUtils.toString(entity);
            
            JSONObject bookingDetailJsonObject = XML.toJSONObject(content);
            response.getWriter().write("{\"data\":[" + bookingDetailJsonObject + "]}");
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return null;
	}
	
	 /*public void getBookedDetails() throws IOException {
			
		    
		
			 try {

				String jsonOutput = "";
				HttpServletResponse response = ServletActionContext.getResponse();
				DecimalFormat df = new DecimalFormat("###.##");
				PmsPropertyManager propertyController = new PmsPropertyManager();
				PropertyAccommodationManager propertyAccommodationController = new PropertyAccommodationManager();
				BookingDetailManager bookingDetailController = new BookingDetailManager();
				BookingGuestDetailManager bookingGuestDetailController = new BookingGuestDetailManager();
				PmsGuestManager guestController = new PmsGuestManager();
				PmsStatusManager statusController = new PmsStatusManager();
				PropertyDiscountManager propertyDiscountController = new PropertyDiscountManager();  
				PmsBookingManager bookingController = new PmsBookingManager();
				
				response.setContentType("application/json");
				this.bookedList = bookingDetailController.bookedList(getBookingId());
				
				StringBuilder accommodationType = new StringBuilder();
				
				StringBuilder accommodationRate = new StringBuilder();
				
				this.totalAdultCount = (Integer) sessionMap.get("totalAdultCount");
				
				//this.totalInfantCount = (Integer) sessionMap.get("totalInfantCount");
				
				this.totalChildCount = (Integer) sessionMap.get("totalChildCount");
				this.txtGrandTotal=(Double)sessionMap.get("txtGrandTotal");
				
				StringBuilder smsAccommodationTypes = new StringBuilder();
				List accommodationBooked = new ArrayList();
	            for (PmsBookedDetails booked : bookedList) {
	            	this.bookingId = getBookingId();
	            	this.propertyDiscountId = booked.getPropertyDiscountId();
	            	PropertyDiscount discount = propertyDiscountController.findId(this.propertyDiscountId);
	            	this.propertyDiscountName = discount.getDiscountName();
	            	PmsProperty pmsProperty = propertyController.find(booked.getPropertyId());
	            	this.propertyName = pmsProperty.getPropertyName();//resort name and location name is missing
	            	this.propertyId = pmsProperty.getPropertyId();
	            	this.latitude = pmsProperty.getLatitude();
	            	this.longitude = pmsProperty.getLongitude();
	            	this.address1=pmsProperty.getAddress1();//property address1
	            	this.address2=pmsProperty.getAddress2();//property address2
	            	this.phoneNumber = pmsProperty.getPropertyContact();//property phone number
	            	this.propertyEmail = pmsProperty.getPropertyEmail();
	            	this.locationName=pmsProperty.getLocation().getLocationName();//property location
	            	this.reservationManagerEmail=pmsProperty.getReservationManagerEmail();
	            	this.reservationManagerContact = pmsProperty.getReservationManagerContact();
	            	this.routeMap = pmsProperty.getRouteMap();
	            	java.util.Date date=new java.util.Date();
	            	Calendar calDate=Calendar.getInstance();
	            	calDate.setTime(date);
	            	calDate.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
	            	int bookedHours=calDate.get(Calendar.HOUR);
	            	int bookedMinute=calDate.get(Calendar.MINUTE);
	            	int bookedSecond=calDate.get(Calendar.SECOND);
	            	int AMPM=calDate.get(Calendar.AM_PM);
	            	
	            	String strBookingDate=new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH).format(calDate.getTime());
	            	String hours="",minutes="",seconds="";
	            	if(bookedHours<10){
	            		hours=String.valueOf(bookedHours);
	            		hours="0"+hours;
	            	}else{
	            		hours=String.valueOf(bookedHours);
	            	}
	            	if(bookedMinute<10){
	            		minutes=String.valueOf(bookedMinute);
	            		minutes="0"+minutes;
	            	}else{
	            		minutes=String.valueOf(bookedMinute);
	            	}
	            	
	            	if(bookedMinute<10){
	            		seconds=String.valueOf(bookedSecond);
	            		seconds="0"+seconds;
	            	}else{
	            		seconds=String.valueOf(bookedSecond);
	            	}
	            	if(AMPM==0){//If the current time is AM
	            		this.timeHours=strBookingDate+" "+hours+" : "+minutes+" : "+seconds+" AM ";
	            	}else if(AMPM==1){//If the current time is PM
	            		this.timeHours=strBookingDate+" "+hours+" : "+minutes+" : "+seconds+" PM ";
	            	}
	            	if(this.mailSource == true){
	            		if(this.sourceId==12){
	            			this.sourceName = "offline";
	            		}else{
	            			this.sourceName = "resort";
	            		}
	            	}else{
	            		
	            		this.sourceName = "online";
	            	}
	            	
	            	
	                
	            	this.arrivalDate = booked.getArrivalDate();
	            	this.departureDate = booked.getDepartureDate();
	            	PmsGuest guest = guestController.find(booked.getGuestId());
	            	this.firstName = guest.getFirstName();
	            	this.lastName = guest.getLastName();
	            	this.phone = guest.getPhone();
	            	this.emailId = guest.getEmailId();
	            	this.specialRequest = booked.getSpecialRequest();
	            	
	            	String checkIn = new SimpleDateFormat("MM/dd/yyyy").format(this.arrivalDate);
					String checkOut = new SimpleDateFormat("MM/dd/yyyy").format(this.departureDate);
					
					LocalDate date1 = LocalDate.parse(checkIn, DateTimeFormat.forPattern("MM/dd/yyyy"));
	                LocalDate date2 = LocalDate.parse(checkOut, DateTimeFormat.forPattern("MM/dd/yyyy"));
	                this.days = Days.daysBetween(date1, date2).getDays();
	            	
              this.bookList =  bookingDetailController.listAccommodation(getBookingId(),booked.getAccommodationId());
              if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += ",{";
					
					else
						jsonOutput += "{";
              
              	String jsonTypes="";
          		jsonOutput += "\"bookingid\":\"" + getBookingId().toString() + "\"";
          	
          		for (BookedDetail bookedDetail : bookList) {
	            		
		            	PropertyAccommodation accommodation = propertyAccommodationController.find(bookedDetail.getAccommodationId());
		            	this.roomCount = bookedDetail.getRoomCount()/this.days;
		            	accommodationBooked.add(new BookedDetail(accommodation.getAccommodationType(),bookedDetail.getTax(),
		            			this.roomCount,bookedDetail.getAdultCount(),bookedDetail.getChildCount() ,bookedDetail.getAmount())); 
		            	 
		            	if (!jsonTypes.equalsIgnoreCase(""))
							jsonTypes += ",{";
		            	
						else
							jsonTypes += "{";
	            		
		            	
		            	smsAccommodationTypes.append(accommodation.getAccommodationType().trim());
		            	smsAccommodationTypes.append(",");
	            		jsonTypes += "\"accommodationType\":\"" + accommodation.getAccommodationType().trim()+ "\"";
	            		jsonTypes += ",\"rooms\":\"" + this.roomCount+ "\"";
	            		jsonTypes += ",\"adultCount\":\"" + bookedDetail.getAdultCount()+ "\"";
	            		jsonTypes += ",\"childCount\":\"" + bookedDetail.getChildCount()+ "\"";
	            		jsonTypes += ",\"amount\":\"" + bookedDetail.getAmount()+ "\"";
	            		jsonTypes += ",\"tax\":\"" + bookedDetail.getTax()+ "\"";
	            		jsonTypes += ",\"total\":\"" + (bookedDetail.getTax() + bookedDetail.getAmount())  + "\"";
	            		jsonTypes += "}";
	            		
	         	    }
          		jsonOutput += ",\"types\":[" + jsonTypes+ "]";
	            	
	            	
	            	this.rooms = booked.getRooms();
	            	this.adults = this.totalAdultCount;
	            	this.infant = this.totalInfantCount;
	            	this.child = this.totalChildCount;
	            	this.tax = booked.getTax();
	            	this.amount = booked.getAmount();
	            	this.totalAmount = this.tax + this.amount;
	            	String strTotal=df.format(this.totalAmount);
	            	this.totalAmount=Double.valueOf(strTotal);
	            	
                 this.totalAdvanceAmount = booked.getAdvanceAmount();
                 if(this.totalAdvanceAmount == 0){
                 	this.hotelPay = 0;
	            	}
	            	else{
	            		this.hotelPay = (this.totalAmount-this.totalAdvanceAmount);
	            	}
                 this.hotelPay = (this.totalAmount-this.totalAdvanceAmount); 
	            	if(this.totalAmount == booked.getAdvanceAmount()){
	            		
	            		this.paymentStatus = "Paid";
	            	}
	            	else{
	            		if(this.mailSource == true){
	            			if(this.sourceId==12){
	            				if(this.totalAmount == booked.getAdvanceAmount()){
	        	            		this.paymentStatus = "Paid";
	        	            	}else{
	        	            		this.paymentStatus = "Partially Paid";
	        	            	}
	            			}else{
	            				this.paymentStatus = "Paid";
	            			}
		            	}else{
		            		this.paymentStatus = "Partially Paid";
		            	}
	            		
	            		
	            	}
	               
	                jsonOutput += ",\"propertyName\":\"" + this.propertyName+ "\"";
		            jsonOutput += ",\"locationName\":\"" + this.locationName+ "\"";
					jsonOutput += ",\"address1\":\"" + this.address1+ "\"";
					jsonOutput += ",\"address2\":\"" + this.address2+ "\"";
					jsonOutput += ",\"phoneNumber\":\"" + this.phoneNumber+ "\"";
					jsonOutput += ",\"propertyEmail\":\"" + this.propertyEmail+ "\"";
					jsonOutput += ",\"reservationManagerEmail\":\"" + this.reservationManagerEmail+ "\"";
					jsonOutput += ",\"reservationManagerContact\":\"" + this.reservationManagerContact+ "\"";
					
					
	            	String CheckIn = new SimpleDateFormat("MMM d,yyyy").format(this.arrivalDate);// check in 
	                String CheckOut = new SimpleDateFormat("MMM d,yyyy").format(this.departureDate);//check out
	                
	                jsonOutput += ",\"checkIn\":\"" + CheckIn+ "\"";
					jsonOutput += ",\"checkOut\":\"" + CheckOut+ "\"";
					jsonOutput += ",\"timeHours\":\"" + this.timeHours+ "\"";
					jsonOutput += ",\"days\":\"" + this.days.toString()+ "\"";
					jsonOutput += ",\"guestname\":\"" + this.firstName+ "\"";
                  String strFirstName="",firstNameLetterUpperCase="";
					String strLastName="",lastNameLetterUpperCase="",strFullName="";
					strFirstName=this.firstName;
					strLastName=this.lastName;
					if(strFirstName!=null && strFirstName!="" && !strFirstName.equalsIgnoreCase("undefined")){
						strFirstName=strFirstName.trim();
						firstNameLetterUpperCase=strFirstName.substring(0, 1).toUpperCase()+strFirstName.substring(1);
						strFirstName=firstNameLetterUpperCase;
					}else{
						strFirstName="-";
					}
					if(strLastName!=null && strLastName!=""  && !strLastName.equalsIgnoreCase("undefined")){
						strLastName=strLastName.trim();
						lastNameLetterUpperCase=strLastName.substring(0, 1).toUpperCase() +  strLastName.substring(1);
						strLastName=lastNameLetterUpperCase;
					}else{
						strLastName="";
					}
					strFullName=strFirstName+" "+strLastName;
					
					 String promotionName = (String) sessionMap.get("promotionName");
					    if(promotionName==null || promotionName==""){
					    	promotionName="Nil";
					    }
					    if(this.propertyDiscountId == 35){
					    	this.propertyDiscountName = "Nil";
					    }
					
					jsonOutput += ",\"guestname\":\"" + strFullName+ "\"";
					jsonOutput += ",\"sourcename\":\"" + this.sourceName+ "\"";
					jsonOutput += ",\"paymentstatus\":\"" + this.paymentStatus+ "\"";
					jsonOutput += ",\"specialrequest\":\"" + this.specialRequest+ "\"";
					jsonOutput += ",\"promotionName\":\"" + promotionName + "\"";
					jsonOutput += ",\"propertyDiscountName\":\"" + this.propertyDiscountName + "\"";
					jsonOutput += ",\"mobile\":\"" + this.phone+ "\"";
					jsonOutput += ",\"emailId\":\"" + this.emailId+ "\"";
					jsonOutput += ",\"rooms\":\"" + this.rooms.toString()+ "\"";
					jsonOutput += ",\"adults\":\"" + String.valueOf(this.adults)+ "\"";
					jsonOutput += ",\"child\":\"" + String.valueOf(this.child)+ "\"";
					jsonOutput += ",\"tax\":\"" + df.format(this.tax) + "\"";
					jsonOutput += ",\"amount\":\"" + df.format(this.amount)+ "\"";
					jsonOutput += ",\"totalamount\":\"" + df.format(this.totalAmount)+ "\"";
					//jsonOutput += ",\"totaladvanceamount\":\"" + Double.valueOf(this.totalAdvanceAmount)+ "\"";
					
					if(getDiscountId()!=null){
						jsonOutput += ",\"totalamount\":\"" + String.valueOf(this.txtGrandTotal)+ "\"";
					}else{
						jsonOutput += ",\"totalamount\":\"" + String.valueOf(this.totalAmount)+ "\"";	
					}
					
					
					
					
					jsonOutput += "}";

				}
	           
	                String checkIn = new SimpleDateFormat("MM/dd/yyyy").format(this.arrivalDate);
				    String checkOut = new SimpleDateFormat("MM/dd/yyyy").format(getDepartureDate());
				    String promotionName = (String) sessionMap.get("promotionName");
				    if(promotionName==null || promotionName==""){
				    	promotionName="Nil";
				    }
				    
				    String strFullName="";
				    
	            if(this.mailFlag == true)
	            {
	            	try
	 				{
	 				//email template
	 				Configuration cfg = new Configuration();
	 				cfg.setClassForTemplateLoading(BookingDetailAction.class, "../../../");
	 				Template template = cfg.getTemplate(getText("reservation.template"));
	 				Map<String, Object> rootMap = new HashMap<String, Object>();
	 				//Map<String, String> rootMap = new HashMap<String, String>();	 				
	 				rootMap.put("bookingid",getBookingId().toString());
	 				rootMap.put("propertyname",this.propertyName);
	 				rootMap.put("propertyid",this.propertyId);
	 				rootMap.put("latitude",this.latitude.trim());
	 				rootMap.put("longitude",this.longitude.trim());
                 rootMap.put("sourcename",this.sourceName);
	 				rootMap.put("paymentstatus",this.paymentStatus);
	 				rootMap.put("propertyDiscountName",this.propertyDiscountName);
	 				rootMap.put("reservationManagerEmail",this.reservationManagerEmail);
	 				rootMap.put("reservationManagerContact",this.reservationManagerContact);
	 				rootMap.put("propertyEmail",this.propertyEmail);
	 				rootMap.put("specialrequest",this.specialRequest);
	 				rootMap.put("guestname",this.firstName);
	 				rootMap.put("locationName",this.locationName);
	 				rootMap.put("address1",this.address1);
	 				rootMap.put("address2",this.address2);
	 				rootMap.put("hotelpay",this.hotelPay);
	 				rootMap.put("phoneNumber",this.phoneNumber);
	 				rootMap.put("checkIn", checkIn);
	 				rootMap.put("checkOut",checkOut);
	 				rootMap.put("timeHours",this.timeHours);
	 				
	 				String strFirstName="",firstNameLetterUpperCase="";
					String strLastName="",lastNameLetterUpperCase="";
					
					strFirstName=this.firstName;
					strLastName=this.lastName;
					if(strFirstName!=null && strFirstName!="" && !strFirstName.equalsIgnoreCase("undefined")){
						strFirstName=strFirstName.trim();
						firstNameLetterUpperCase=strFirstName.substring(0, 1).toUpperCase()+strFirstName.substring(1);
						strFirstName=firstNameLetterUpperCase;
					}else{
						strFirstName="-";
					}
					if(strLastName!=null && strLastName!=""  && !strLastName.equalsIgnoreCase("undefined")){
						strLastName=strLastName.trim();
						lastNameLetterUpperCase=strLastName.substring(0, 1).toUpperCase() +  strLastName.substring(1);
						strLastName=lastNameLetterUpperCase;
					}else{
						strLastName="";
					}
					strFullName=strFirstName+" "+strLastName;
	 				rootMap.put("guestname",strFullName);
	 				//rootMap.put("guestname",this.firstName + " " + this.lastName);
	 				rootMap.put("mobile",this.phone);
	 				rootMap.put("emailId",this.emailId);	 				
	 				rootMap.put("checkin",checkIn);
	 				rootMap.put("days",this.days.toString());
	 				rootMap.put("checkout",checkOut);
	 				rootMap.put("promotionName",promotionName);	 				
	 				
	            	
	 				rootMap.put("accommodationBooked", accommodationBooked);
	 				//rootMap.put("rooms", this.roomCount);
	 				rootMap.put("adults", String.valueOf(this.adults));
	 				rootMap.put("child",String.valueOf(this.child));
	 				rootMap.put("infant", String.valueOf(this.infant));
	 				rootMap.put("tax",df.format(this.tax));
	 				rootMap.put("amount",df.format(this.amount));
	 				rootMap.put("totalamount",df.format(this.totalAmount));
	 				rootMap.put("totaladvanceamount",df.format(this.totalAdvanceAmount));
	 				
	 				//rootMap.put("totalamount",String.valueOf(this.txtGrandTotal));
	 				
	 				rootMap.put("from", getText("notification.from"));
	 				Writer out = new StringWriter();
	 				template.process(rootMap, out);

	 				String strReservationMailId=null,strPropertyMailId=null,strResortMailId=null,strContractMailId=null,strRevenueMailId=null;
	 				PmsPropertyManager pmsPropertyController=new PmsPropertyManager();
	 				for (PmsBookedDetails booked : bookedList) {
	 					PmsProperty pmsProperty=pmsPropertyController.find(booked.getPropertyId());
	 					
	 					strReservationMailId=pmsProperty.getReservationManagerEmail();
	 					strResortMailId=pmsProperty.getResortManagerEmail();
	 					strPropertyMailId=pmsProperty.getPropertyEmail();
	 					strRevenueMailId=pmsProperty.getRevenueManagerEmail();
	 					strContractMailId=pmsProperty.getContractManagerEmail();
	 				}
	 				String strSubject="Hotel Confirmation for Booking Id "+getBookingId().toString()+" "+this.propertyName;
	 				//send email
	 				Email em = new Email();
	 				em.set_to(this.emailId);
	 				if(strReservationMailId!=null && strContractMailId!=null && strRevenueMailId!=null){
	 					em.set_cc(strContractMailId);
		 				em.set_cc2(strRevenueMailId);
		 				em.set_cc3(strReservationMailId);
		 				em.set_cc4(getText("bookings.notification.email"));
		 				em.set_cc5(getText("operations.notification.email"));
		 				em.set_cc6(getText("finance.notification.email"));
	 				}else{
	 					em.set_cc(strContractMailId);
		 				em.set_cc2(strRevenueMailId);
		 				em.set_cc3(strReservationMailId);
		 				em.set_cc4(getText("bookings.notification.email"));
		 				em.set_cc5(getText("operations.notification.email"));
		 				em.set_cc6(getText("finance.notification.email"));
	 				}
	 				
	 				em.set_from(getText("email.from"));
	 				//em.set_host(getText("email.host"));
	 				//em.set_password(getText("email.password"));
	 				//em.set_port(getText("email.port"));
	 				em.set_username(getText("email.username"));
	 				em.set_subject(strSubject);
	 				em.set_bodyContent(out);
	 				em.send();		
	 				}
	 				catch(Exception e1)
	 				{
	 					e1.printStackTrace();
	 				}
	            	
	            	
	            	getHotelierBookedDetails();
	            	
	            	String message = "&sms_text=" + "Confirmation from Ulo Hotels"+
	    					
	    					"Booking ID - "+ getBookingId().toString() +
	    					 "Guest Name � "+ strFullName  + 
	    					 "Property Name �"+ this.propertyName +
	    					"Room Category/s �"+  smsAccommodationTypes.toString()  +
	    					 "No. of Adults �"+ this.adults +
	    					 "No. of Kids �"+ this.child +
	    					 "Check-in Date-"+ checkIn + 
	    					"Check out Date-"+ checkOut +
	    				   "Contact Number � "+ this.phone;

	    			//String sender = "&sender=" + "TXTLCL";
	    			String numbers = "&sms_to=" + "+91"+this.phone;
	    			String from = "&sms_from=" + "ULOHTL";
	    			String type = "&sms_type=" + "trans"; 
	    			// Send data
	    			HttpURLConnection conn = (HttpURLConnection) new URL("https://secureets.knowlarity.com/ets/dom/clients/ulohotels/api/ulohotels_sms_api.py").openConnection();
	    			//"http://etsrds.kapps.in/webapi/ulohotels/api/ulohotels_sms_api.py?sms_text=hi%20this%20gopinath%20from%20ulo&sms_to=%20918220211068&sms_from=ULOHTL&sms_type=promo"
	    			String data = numbers + message +  from + type;
	    			conn.setDoOutput(true);
	    			conn.setRequestMethod("POST");
	    			conn.addRequestProperty("auth_key", "Basic dWxvaG90ZWxzX1Q6VUxPSFRMQDIwMTg=");
	    			conn.setRequestProperty("Content-Length", Integer.toString(data.length()));
	    			conn.getOutputStream().write(data.getBytes("UTF-8"));
	    			final BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	    			final StringBuffer stringBuffer = new StringBuffer();
	    			String line;
	    			
	    			while ((line = rd.readLine()) != null) {
	    				stringBuffer.append(line);
	    				
	    			}
	    			rd.close();
	    			
	    			getRouteMapSms(this.routeMap,this.phone);
	 		     
	            }
	            
				response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
				

			} catch (Exception e) {
				logger.error(e);
				e.printStackTrace();
			} finally {

			}

//			return null;
		}*/
	
	public static List<DateTime> getDateRange(DateTime start, DateTime end) {

        List<DateTime> ret = new ArrayList<DateTime>();
        DateTime tmp = start;
        while(tmp.isBefore(end) || tmp.equals(end)) {
            ret.add(tmp);
            tmp = tmp.plusDays(1);
        }
        return ret;
    }
	
	
	public String getStrStartDate() {
		return strStartDate;
	}

	public void setStrStartDate(String strStartDate) {
		this.strStartDate = strStartDate;
	}

	public String getStrEndDate() {
		return strEndDate;
	}

	public void setStrEndDate(String strEndDate) {
		this.strEndDate = strEndDate;
	}
	
	public Long getHotelCode() {
		return hotelCode;
	}

	public void setHotelCode(Long hotelCode) {
		this.hotelCode = hotelCode;
	}

	public int getGuestId() {
		return guestId;
	}

	public void setGuestId(int guestId) {
		this.guestId = guestId;
	}
	
	public Integer getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(Integer propertyId) {
		this.propertyId = propertyId;
	}

	public String getOtaHotelCode() {
		return otaHotelCode;
	}

	public void setOtaHotelCode(String otaHotelCode) {
		this.otaHotelCode = otaHotelCode;
	}

	public String getOtaBearerToken() {
		return otaBearerToken;
	}

	public void setOtaBearerToken(String otaBearerToken) {
		this.otaBearerToken = otaBearerToken;
	}

	public String getOtaChannelToken() {
		return otaChannelToken;
	}

	public void setOtaChannelToken(String otaChannelToken) {
		this.otaChannelToken = otaChannelToken;
	}

	public Integer getPropertyAccommodationId() {
		return propertyAccommodationId;
	}

	public void setPropertyAccommodationId(Integer propertyAccommodationId) {
		this.propertyAccommodationId = propertyAccommodationId;
	}

	public Integer getRatePlanId() {
		return ratePlanId;
	}

	public void setRatePlanId(Integer ratePlanId) {
		this.ratePlanId = ratePlanId;
	}

	public Integer getOtaHotelDetailsId() {
		return otaHotelDetailsId;
	}

	public void setOtaHotelDetailsId(Integer otaHotelDetailsId) {
		this.otaHotelDetailsId = otaHotelDetailsId;
	}

	public String getOtaRatePlanId() {
		return otaRatePlanId;
	}

	public void setOtaRatePlanId(String otaRatePlanId) {
		this.otaRatePlanId = otaRatePlanId;
	}

	public String getOtaRoomTypeId() {
		return otaRoomTypeId;
	}

	public void setOtaRoomTypeId(String otaRoomTypeId) {
		this.otaRoomTypeId = otaRoomTypeId;
	}

	public Integer getBookingId() {
		return bookingId;
	}

	public void setBookingId(Integer bookingId) {
		this.bookingId = bookingId;
	}

	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return null;
	}


}
