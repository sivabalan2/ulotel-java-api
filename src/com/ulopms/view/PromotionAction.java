package com.ulopms.view;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Collections;














import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.joda.time.DateTime;

import com.opensymphony.xwork2.ActionSupport;



import com.ulopms.controller.AreaManager;
import com.ulopms.controller.PmsDayManager;
import com.ulopms.controller.PmsPropertyManager;
import com.ulopms.controller.PromotionDetailManager;
import com.ulopms.controller.PromotionManager;
import com.ulopms.controller.PropertyAccommodationManager;
import com.ulopms.controller.UserLoginManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.Area;
import com.ulopms.model.PmsAvailableRooms;
import com.ulopms.model.PmsDays;
import com.ulopms.model.PmsPromotionDetails;
import com.ulopms.model.PmsPromotions;
import com.ulopms.model.PmsProperty;
import com.ulopms.model.PropertyAccommodation;
import com.ulopms.model.PropertyRateDetail;
import com.ulopms.model.User;
import com.ulopms.util.DateUtil;

public class PromotionAction extends ActionSupport implements
ServletRequestAware,SessionAware, UserAware{
     
	

	private static final long serialVersionUID = 914982665758390091L;
    
	private Integer promotionId;
	private Integer propertyId;
	private Integer propertyAccommodationId;
	private String  checkedDays;
	private Timestamp  startDate;
	private Timestamp  endDate;
	private String promotionName;
	private String promotionType;
	private String checkedAccommodations;
	private String updatePromotionId;
	private String updatePromotionIds;
	private Integer promotionHours;
	private long roomCnt;
	private Integer userId;
	private String strStartDate;
	private String strEndDate;
	private String checkedProperty;
	private Integer promotionDays;
	private Timestamp earlyDate;
	private String earlyPromoDate;
	private Integer areaId;
	private String hoursRange;
	private String discountType;
	private List<PropertyAccommodation> accommodationList;
	private String stayStartDate;
	private String stayEndDate;
	private String bookingStartDate;
	private String bookingEndDate;
	private Timestamp earlyStayStartDate;
	private Timestamp earlyStayEndDate;
	private Timestamp earlyBookingStartDate;
	private Timestamp earlyBookingEndDate;
	
	private List<PmsAvailableRooms> availableList;
	

	private List<PmsPromotions> promotionList;
	
	private List<PmsPromotionDetails> promotionDetailList;
	
	
	private List<PmsDays> daylist;
	
	

	private static final Logger logger = Logger.getLogger(PromotionAction.class);
	
	

	private HttpSession session;

	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}
	
	private SessionMap<String,Object> sessionMap;  
	  
	//getters and setters  
	  
	@Override  
	public void setSession(Map<String, Object> map) {  
	    sessionMap=(SessionMap)map;  
	}  
	
	private User user;

	
	// @Override
	// public User getModel() {
	// return user;
	// }

	@Override
	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	
	private HttpServletRequest request;

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	public PromotionAction() {

	}

	public String execute() {

		return SUCCESS;
	}
	
	public Integer getPromotionId(){
		return promotionId;
	}
	
	public void setPromotionId(Integer promotionId){
		this.promotionId=promotionId;
	}
	public Integer getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(Integer propertyId) {
		this.propertyId = propertyId;
	}
	
	public Integer getPropertyAccommodationId() {
		return propertyAccommodationId;
	}

	public void setPropertyAccommodationId(Integer propertyAccommodationId) {
		this.propertyAccommodationId = propertyAccommodationId;
	}
	
	
	public Timestamp getStartDate() {
		return startDate;
	}

	public void setStartDate(Timestamp startDate) {
		this.startDate = startDate;
	}

	public Timestamp getEndDate() {
		return endDate;
	}

	public void setEndDate(Timestamp endDate) {
		this.endDate = endDate;
	}
	
	public String getCheckedDays() {
		return checkedDays;
	}

	public void setCheckedDays(String checkedDays) {
		this.checkedDays = checkedDays;
	}
	 
	public String getPromotionName() {
		return promotionName;
	}

	public void setPromotionName(String promotionName) {
		this.promotionName = promotionName;
	}
	
	public String getPromotionType(){
		return promotionType;
	}

	public void setPromotionType(String promotionType){
		this.promotionType=promotionType;
	}
	
	public String getCheckedAccommodations(){
		return checkedAccommodations;
	}

	public void setCheckedAccommodations(String checkedAccommodations){
		this.checkedAccommodations=checkedAccommodations;
	}
	
	public String getUpdatePromotionId() {
		return updatePromotionId;
	}

	public void setUpdatePromotionId(String updatePromotionId) {
		this.updatePromotionId = updatePromotionId;
	}
	
	public String getUpdatePromotionIds() {
		return updatePromotionIds;
	}

	public void setUpdatePromotionIds(String updatePromotionIds) {
		this.updatePromotionIds = updatePromotionIds;
	}
	
	public long getRoomCnt() {
		return roomCnt;
	}

	public void setRoomCnt(long roomCnt) {
		this.roomCnt = roomCnt;
	}
	
	public Integer getPromotionHours() {
		return promotionHours;
	}

	public void setPromotionHours(Integer promotionHours) {
		this.promotionHours = promotionHours;
	}
	
	public String getStrStartDate() {
		return strStartDate;
	}

	public void setStrStartDate(String strStartDate) {
		this.strStartDate = strStartDate;
	}

	public String getStrEndDate() {
		return strEndDate;
	}

	public void setStrEndDate(String strEndDate) {
		this.strEndDate = strEndDate;
	}
	
	
	public String getDay() throws IOException {
		
		this.propertyId = (Integer) sessionMap.get("propertyId");
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PmsDayManager dayController = new PmsDayManager();
			response.setContentType("application/json");

			this.daylist = dayController.list();
			for (PmsDays days : daylist) {

				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"dayId\":\"" + days.getDayId() + "\"";
				String strDays = days.getDaysOfWeek();
				String firstLetterUpperCase=strDays.substring(0, 1).toUpperCase() +  strDays.substring(1);
				jsonOutput += ",\"daysOfWeek\":\"" + firstLetterUpperCase+ "\"";
				jsonOutput += ",\"dayPart\":\"" + days.getDayPart()+ "\"";
				//jsonOutput += ",\"taxCode\":\"" + taxe.getTaxCode()+ "\"";
				//jsonOutput += ",\"taxAmount\":\"" + taxe.getTaxAmount()+ "\"";
				jsonOutput += "}";


			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	 
	public  boolean equalLists(List<String> one, List<String> two){     
	    if (one == null && two == null){
	        return true;
	    }

	    if((one == null && two != null) || one != null && two == null){
	        return false;
	    }

	    //to avoid messing the order of the lists we will use a copy
	    //as noted in comments by A. R. S.
	    one = new ArrayList<String>(one); 
	    two = new ArrayList<String>(two);   

	    Collections.sort(one);
	    Collections.sort(two);      
	    return one.equals(two);
	}
	
	public String addPromotions(){
		try{
			this.propertyId = (Integer) sessionMap.get("propertyId");
			this.userId=(Integer)sessionMap.get("userId");
			/*this.startDate = getStartDate();
			sessionMap.put("startDate",startDate);
			 
			this.endDate = getEndDate();
			sessionMap.put("endDate",endDate);*/
			 
			this.promotionName = getPromotionName();
			sessionMap.put("promotionName",promotionName);
			
			this.promotionType=getPromotionType();
			sessionMap.put("promotionType", promotionType);
			
			this.propertyAccommodationId=getPropertyAccommodationId();
			sessionMap.put("propertyAccommodationId", propertyAccommodationId);
			
			this.promotionHours=getPromotionHours();
			sessionMap.put("promotionHours", promotionHours);
			
			this.checkedDays=getCheckedDays();
			sessionMap.put("checkedDays", checkedDays);
			
			SimpleDateFormat sdfformat = new SimpleDateFormat("yyyy-MM-dd");
			java.util.Date date=new java.util.Date();
        	Calendar calDate=Calendar.getInstance();
        	calDate.setTime(date);
        	calDate.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
        	String strCurrentDate = new SimpleDateFormat("yyyy-MM-dd").format(calDate.getTime());
		    java.util.Date currentDate = sdfformat.parse(strCurrentDate);
		    Timestamp tsCurrentDate = new Timestamp(currentDate.getTime());
    	    
			String jsonOutput="";
	    	HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("application/json");
			Timestamp lastEndDate=null,lastStartDate=null;
			ArrayList<String> arlDaysOfWeek=new ArrayList<String>();
			ArrayList<String> arlCurrentDaysOfWeek=new ArrayList<String>();
			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
			PmsPropertyManager propertyController = new PmsPropertyManager();
			PmsPromotions promotions=new PmsPromotions();
			PromotionManager promotionController=new PromotionManager();
			PromotionDetailManager promotionDetailController=new PromotionDetailManager();
			PmsDayManager dayController = new PmsDayManager();
			
			Boolean promotionChk=false;
			int PromotionId=0;
			String strStartDate=null,strEndDate=null;
			List<PmsPromotions> listPromotionCheckAll=null;
			List<PmsPromotionDetails> listPromotionDetailCheck=null;
			String strPromotionId="";
			ArrayList<Integer> arrlist=new ArrayList<Integer>();
			DateFormat dayformat = new SimpleDateFormat("EEEE");
		
			DateFormat format = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
		    java.util.Date dateStart = format.parse(getStrStartDate());
		    java.util.Date dateEnd = format.parse(getStrEndDate());
		   
		    
		    Calendar calCheckStart=Calendar.getInstance();
		    calCheckStart.setTime(dateStart);
		    java.util.Date checkInDate = calCheckStart.getTime();
		    this.startDate=new Timestamp(checkInDate.getTime());
		    sessionMap.put("startDate",startDate); 
		    
		    Calendar calCheckEnd=Calendar.getInstance();
		    calCheckEnd.setTime(dateEnd);
		    java.util.Date checkOutDate = calCheckEnd.getTime();
		    this.endDate=new Timestamp(checkOutDate.getTime());
		    sessionMap.put("endDate",endDate); 
		    
		    String[] stringArray = getCheckedDays().split("\\s*,\\s*");
		    int[] intArray = new int[stringArray.length];
		    for (int i = 0; i < stringArray.length; i++) {
		    	intArray[i] = Integer.parseInt(stringArray[i]);
		    	if(intArray[i]>0){
					PmsDays days = dayController.find(intArray[i]);
					arlCurrentDaysOfWeek.add(days.getDaysOfWeek());
		    	}
		    }
		    
			SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
			
	    	strStartDate=format1.format(getStartDate());
    		strEndDate=format1.format(getEndDate());
    		DateTime FromDate = DateTime.parse(strStartDate);
	        DateTime ToDate = DateTime.parse(strEndDate);
	        if(promotionType.equalsIgnoreCase("L")){
				
	        	String checkDate = new SimpleDateFormat("yyyy-MM-dd").format(startDate);
	        	String strCheckInTime=checkDate+" 10:00:00"; 
	        	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

	        	Calendar calCheckIn =Calendar.getInstance();
	        	calCheckIn.setTime(sdf.parse(strCheckInTime));
	        	calCheckIn.get(Calendar.HOUR);
	        	
	        	calCheckIn.add(Calendar.HOUR, -promotionHours);
	        	String testStartDate = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(calCheckIn.getTime());
	        	
	        	String checkStartDate = new SimpleDateFormat("yyyy-MM-dd").format(calCheckIn.getTime());
	        	java.util.Date fromDate = format1.parse(checkStartDate);
				lastStartDate=new Timestamp(fromDate.getTime());
				
				String checkEndDate = new SimpleDateFormat("yyyy-MM-dd").format(endDate);
	        	String strCheckInTillTime=checkEndDate; 
	        	
	        	java.util.Date toDate = format1.parse(strCheckInTillTime);
				lastEndDate=new Timestamp(toDate.getTime());
				
				String StartDate=new SimpleDateFormat("yyyy-MM-dd").format(lastStartDate);
				String EndDate=new SimpleDateFormat("yyyy-MM-dd").format(lastEndDate);
				DateTime lastFromDate = DateTime.parse(StartDate);
		        DateTime lastToDate = DateTime.parse(EndDate);
		        
		        List<DateTime> dateLastBetween = DateUtil.getDateRange(lastFromDate, lastToDate);
		        Iterator<DateTime> iterLastDateValues=dateLastBetween.iterator();
		        while(iterLastDateValues.hasNext()){
		        	DateTime dateValues=iterLastDateValues.next();
		        	java.util.Date DateValues = dateValues.toDate();
		        	listPromotionCheckAll=promotionController.listPromotions(propertyId, DateValues,propertyAccommodationId);
		        	if(listPromotionCheckAll.size()>0 && !listPromotionCheckAll.isEmpty()){
		        		for(PmsPromotions promotion:listPromotionCheckAll){
		        			int promotionId=promotion.getPromotionId();
		        			listPromotionDetailCheck=promotionDetailController.listPromotionDetails(promotionId,dayformat.format(DateValues).toLowerCase());
		        			if(listPromotionDetailCheck.size()>0 && !listPromotionDetailCheck.isEmpty()){
		        				for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
		        					if(!arlDaysOfWeek.contains(promoDetails.getDaysOfWeek())){
		        						arlDaysOfWeek.add(promoDetails.getDaysOfWeek());
		        					}
		        				}
//		        				promotionChk=true;
		        			}
		        		}
		        	} else {
		        		listPromotionCheckAll=promotionController.listPromotions(propertyId, DateValues);
			        	if(listPromotionCheckAll.size()>0 && !listPromotionCheckAll.isEmpty()){
			        		for(PmsPromotions promotion:listPromotionCheckAll){
			        			String strType=promotion.getPromotionType();
			        			int promotionId=promotion.getPromotionId();
			        			if(strType.equalsIgnoreCase("F")){
				        			listPromotionDetailCheck=promotionDetailController.listPromotionDetails(promotionId,dayformat.format(DateValues).toLowerCase());
				        			if(listPromotionDetailCheck.size()>0 && !listPromotionDetailCheck.isEmpty()){
				        				for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
//				        					arlDaysOfWeek.add(promoDetails.getDaysOfWeek());
				        					if(!arlDaysOfWeek.contains(promoDetails.getDaysOfWeek())){
				        						arlDaysOfWeek.add(promoDetails.getDaysOfWeek());
				        					}
				        				}
				        				//promotionChk=true;
				        			}
			        			}
			        		}
			        	}
		        	}
		        }
			}
	        List<DateTime> dateBetween = DateUtil.getDateRange(FromDate, ToDate);
	        Iterator<DateTime> iterDateValues=dateBetween.iterator();
	        while(iterDateValues.hasNext()){
	        	DateTime dateValues=iterDateValues.next();
	        	java.util.Date DateValues = dateValues.toDate();
	        	if(promotionType.equalsIgnoreCase("F")){
	        		listPromotionCheckAll=promotionController.listPromotions(propertyId, DateValues);
		        	if(listPromotionCheckAll.size()>0 && !listPromotionCheckAll.isEmpty()){
		        		for(PmsPromotions promotion:listPromotionCheckAll){
		        			int promotionId=promotion.getPromotionId();
		        			listPromotionDetailCheck=promotionDetailController.listPromotionDetails(promotionId,dayformat.format(DateValues).toLowerCase());
		        			if(listPromotionDetailCheck.size()>0 && !listPromotionDetailCheck.isEmpty()){
		        				for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
//		        					arlDaysOfWeek.add(promoDetails.getDaysOfWeek());
		        					if(!arlDaysOfWeek.contains(promoDetails.getDaysOfWeek())){
		        						arlDaysOfWeek.add(promoDetails.getDaysOfWeek());
		        					}
		        				}
		        				//promotionChk=true;
		        			}
		        		}
		        	}
	        	}else if(promotionType.equalsIgnoreCase("R") || promotionType.equalsIgnoreCase("N") || promotionType.equalsIgnoreCase("L")){
	        		listPromotionCheckAll=promotionController.listPromotions(propertyId, DateValues,propertyAccommodationId);
		        	if(listPromotionCheckAll.size()>0 && !listPromotionCheckAll.isEmpty()){
		        		for(PmsPromotions promotion:listPromotionCheckAll){
		        			int promotionId=promotion.getPromotionId();
		        			listPromotionDetailCheck=promotionDetailController.listPromotionDetails(promotionId,dayformat.format(DateValues).toLowerCase());
		        			if(listPromotionDetailCheck.size()>0 && !listPromotionDetailCheck.isEmpty()){
		        				for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
//		        					arlDaysOfWeek.add(promoDetails.getDaysOfWeek());
		        					if(!arlDaysOfWeek.contains(promoDetails.getDaysOfWeek())){
		        						arlDaysOfWeek.add(promoDetails.getDaysOfWeek());
		        					}
		        				}
//		        				promotionChk=true;
		        			}
		        		}
		        	}else {
		        		listPromotionCheckAll=promotionController.listPromotions(propertyId, DateValues);
			        	if(listPromotionCheckAll.size()>0 && !listPromotionCheckAll.isEmpty()){
			        		for(PmsPromotions promotion:listPromotionCheckAll){
			        			String strType=promotion.getPromotionType();
			        			int promotionId=promotion.getPromotionId();
			        			if(strType.equalsIgnoreCase("F")){
				        			listPromotionDetailCheck=promotionDetailController.listPromotionDetails(promotionId,dayformat.format(DateValues).toLowerCase());
				        			if(listPromotionDetailCheck.size()>0 && !listPromotionDetailCheck.isEmpty()){
				        				for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
//				        					arlDaysOfWeek.add(promoDetails.getDaysOfWeek());
				        					if(!arlDaysOfWeek.contains(promoDetails.getDaysOfWeek())){
				        						arlDaysOfWeek.add(promoDetails.getDaysOfWeek());
				        					}
				        				}
				        				//promotionChk=true;
				        			}
			        			}
			        		}
			        	}
		        	}
	        	}
	        } 
	        
			if (!jsonOutput.equalsIgnoreCase(""))
				jsonOutput += ",{";
			else
				jsonOutput += "{";
			/*if(arlCurrentDaysOfWeek.contains(arlDaysOfWeek)){
				promotionChk=true;
			}else{
				promotionChk=false;
			}*/
//	        promotionChk= equalLists(arlCurrentDaysOfWeek,arlDaysOfWeek);
			promotionChk=arlCurrentDaysOfWeek.contains(arlDaysOfWeek);
	        promotionChk=false;
	        
			if(promotionChk){
				jsonOutput += "\"check\":\"" + promotionChk+ "\"";
			}
			else if(!promotionChk){
				if(!promotionType.equalsIgnoreCase("F")){
					if(propertyAccommodationId==0){
						this.accommodationList=accommodationController.list(propertyId);
						if(accommodationList.size()>0 && !accommodationList.isEmpty()){
							for(PropertyAccommodation accommodations:accommodationList){
								int accommodationId=accommodations.getAccommodationId();
								
								if(promotionType.equalsIgnoreCase("L")){
									promotions.setStartDate(lastStartDate);
									promotions.setEndDate(lastEndDate);
								}
								else{
									promotions.setStartDate(startDate);
									promotions.setEndDate(endDate);
								}
								
								promotions.setIsActive(true);
								promotions.setIsDeleted(false);
								promotions.setPromotionIsActive(false);
								PmsProperty property = propertyController.find(propertyId);
								promotions.setPmsProperty(property);
								if(!promotionType.equalsIgnoreCase("F")){
									PropertyAccommodation accommodation=accommodationController.find(accommodationId);
									promotions.setPropertyAccommodation(accommodation);
								}
								promotions.setCreatedBy(this.userId);
								promotions.setCreatedDate(tsCurrentDate);
								promotions.setPromotionName(promotionName);
								promotions.setPromotionType(promotionType);
								promotionController.add(promotions);
								
								
								PromotionId=promotions.getPromotionId();
								strPromotionId=String.valueOf(PromotionId)+","+strPromotionId;
								
								this.promotionId = promotions.getPromotionId();
								sessionMap.put("promotionId",promotionId);
								
								sessionMap.put("strPromotionId",strPromotionId);
							}
						}
						jsonOutput += "\"check\":\"" + promotionChk+ "\"";
					}
					else{
						if(promotionType.equalsIgnoreCase("L")){
							promotions.setStartDate(lastStartDate);
							promotions.setEndDate(lastEndDate);
						}
						else{
							promotions.setStartDate(startDate);
							promotions.setEndDate(endDate);
						}
						promotions.setIsActive(true);
						promotions.setIsDeleted(false);
						promotions.setPromotionIsActive(false);
						PmsProperty property = propertyController.find(propertyId);
						promotions.setPmsProperty(property);
						if(!promotionType.equalsIgnoreCase("F")){
							PropertyAccommodation accommodation=accommodationController.find(propertyAccommodationId);
							promotions.setPropertyAccommodation(accommodation);
						}
						promotions.setCreatedBy(this.userId);
						promotions.setCreatedDate(tsCurrentDate);
						promotions.setPromotionName(promotionName);
						promotions.setPromotionType(promotionType);
						promotionController.add(promotions);
		
						
						PromotionId=promotions.getPromotionId();
						strPromotionId=String.valueOf(PromotionId)+","+strPromotionId;
						
						this.promotionId = promotions.getPromotionId();
						sessionMap.put("promotionId",promotionId);
						jsonOutput += "\"check\":\"" + promotionChk+ "\"";
						sessionMap.put("strPromotionId",strPromotionId);
						
					}
				}
				else{
					if(promotionType.equalsIgnoreCase("L")){
						promotions.setStartDate(lastStartDate);
						promotions.setEndDate(lastEndDate);
					}
					else{
						promotions.setStartDate(startDate);
						promotions.setEndDate(endDate);
					}
					promotions.setIsActive(true);
					promotions.setIsDeleted(false);
					promotions.setPromotionIsActive(false);
					PmsProperty property = propertyController.find(propertyId);
					promotions.setPmsProperty(property);
					if(!promotionType.equalsIgnoreCase("F")){
						PropertyAccommodation accommodation=accommodationController.find(propertyAccommodationId);
						promotions.setPropertyAccommodation(accommodation);
					}
					promotions.setCreatedBy(this.userId);
					promotions.setCreatedDate(tsCurrentDate);
					promotions.setPromotionName(promotionName);
					promotions.setPromotionType(promotionType);
					promotionController.add(promotions);
	
					
					PromotionId=promotions.getPromotionId();
					strPromotionId=String.valueOf(PromotionId)+","+strPromotionId;
					
					this.promotionId = promotions.getPromotionId();
					sessionMap.put("promotionId",promotionId);
					jsonOutput += "\"check\":\"" + promotionChk+ "\"";
					sessionMap.put("strPromotionId",strPromotionId);
				}
			}
			jsonOutput += "}";
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			
		}
		catch(Exception e){
			logger.error(e);
			e.printStackTrace();
		}finally{
			
		}
		return null;
	}
	
	public String addNewPromotions(){

		try{
			this.userId=(Integer)sessionMap.get("userId");
			int PromotionId=0;
			String strPromotionId="";
			
			this.promotionName = getPromotionName();
			sessionMap.put("promotionName",promotionName);
			
			this.promotionType=getPromotionType();
			sessionMap.put("promotionType", promotionType);
			
			String jsonOutput="";
	    	HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("application/json");
			
			SimpleDateFormat sdfformat = new SimpleDateFormat("yyyy-MM-dd");
			java.util.Date date=new java.util.Date();
        	Calendar calDate=Calendar.getInstance();
        	calDate.setTime(date);
        	calDate.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
        	String strCurrentDate = new SimpleDateFormat("yyyy-MM-dd").format(calDate.getTime());
		    java.util.Date currentDate = sdfformat.parse(strCurrentDate);
		    Timestamp tsCurrentDate = new Timestamp(currentDate.getTime());
    	    
		    
		    
		   
		    this.promotionType=getPromotionType();
		    if(this.promotionType.equalsIgnoreCase("F")){
		    	DateFormat format = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
			    java.util.Date dateStart = format.parse(getStrStartDate());
			    java.util.Date dateEnd = format.parse(getStrEndDate());
			    
		    	Calendar calCheckStart=Calendar.getInstance();
			    calCheckStart.setTime(dateStart);
			    java.util.Date checkInDate = calCheckStart.getTime();
			    this.startDate=new Timestamp(checkInDate.getTime());
			    
			    Calendar calCheckEnd=Calendar.getInstance();
			    calCheckEnd.setTime(dateEnd);
			    java.util.Date checkOutDate = calCheckEnd.getTime();
			    this.endDate=new Timestamp(checkOutDate.getTime());	
		    }else if(this.promotionType.equalsIgnoreCase("E")){
		    	DateFormat format = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
			    java.util.Date bookingStart = format.parse(getStrStartDate());
			    java.util.Date bookingEnd = format.parse(getStrEndDate());
			    java.util.Date stayStart = format.parse(getStayStartDate());
			    java.util.Date stayEnd = format.parse(getStayEndDate());
		    	Calendar calbookingStart=Calendar.getInstance();
		    	calbookingStart.setTime(bookingStart);
			    java.util.Date checkInDate = calbookingStart.getTime();
			    this.earlyBookingStartDate=new Timestamp(checkInDate.getTime());
			    
			    Calendar calBookingEnd=Calendar.getInstance();
			    calBookingEnd.setTime(bookingEnd);
			    java.util.Date checkOutDate = calBookingEnd.getTime();
			    this.earlyBookingEndDate=new Timestamp(checkOutDate.getTime());
			    
			    Calendar calStayStart=Calendar.getInstance();
		    	calStayStart.setTime(stayStart);
			    java.util.Date checkInStayDate = calStayStart.getTime();
			    this.earlyStayStartDate=new Timestamp(checkInStayDate.getTime());
			    
			    Calendar calStayEnd=Calendar.getInstance();
			    calStayEnd.setTime(stayEnd);
			    java.util.Date checkOutStayDate = calStayEnd.getTime();
			    this.earlyStayEndDate=new Timestamp(checkOutStayDate.getTime());
		    }
		    
		    
		    
		    
		    
		    Timestamp tsBookedDate=null; 
		    AreaManager areaController=new AreaManager();
		    PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
			PmsPropertyManager propertyController = new PmsPropertyManager();
			PmsPromotions promotions=new PmsPromotions();
			PromotionManager promotionController=new PromotionManager();
			
			if (!jsonOutput.equalsIgnoreCase(""))
				jsonOutput += ",{";
			else
				jsonOutput += "{";
			
			jsonOutput += "\"check\":\"" + false+ "\"";
			
			
			if(this.promotionType.equalsIgnoreCase("F")){

				String[] propertyArray = getCheckedProperty().split("\\s*,\\s*");
			    int[] intPropertyArray = new int[propertyArray.length];
			    for (int i = 0; i < propertyArray.length; i++) {
			    	intPropertyArray[i] = Integer.parseInt(propertyArray[i]);
			    	if(intPropertyArray[i]!=0){
			    		
			    		promotions.setStartDate(startDate);
						promotions.setEndDate(endDate);
						promotions.setIsActive(true);
						promotions.setIsDeleted(false);
						promotions.setPromotionIsActive(false);
						promotions.setPromotionDiscountType(getDiscountType());
						PmsProperty property = propertyController.find(intPropertyArray[i]);
						promotions.setPmsProperty(property);
						
						Area area=areaController.find(getAreaId());
						promotions.setArea(area);
						
						promotions.setPromotionHoursRange(getHoursRange());
						promotions.setCreatedBy(this.userId);
						promotions.setCreatedDate(tsCurrentDate);
						promotions.setPromotionName(promotionName);
						promotions.setPromotionType(promotionType);
						PmsPromotions pmsPromotions=promotionController.add(promotions);
						PromotionId=pmsPromotions.getPromotionId();
						strPromotionId=String.valueOf(PromotionId)+","+strPromotionId;
						
			    	}
			    }
				sessionMap.put("strPromotionId",strPromotionId);
			
			}else if(promotionType.equalsIgnoreCase("E")){

				String[] propertyArray = getCheckedProperty().split("\\s*,\\s*");
			    int[] intPropertyArray = new int[propertyArray.length];
			    for (int i = 0; i < propertyArray.length; i++) {
			    	intPropertyArray[i] = Integer.parseInt(propertyArray[i]);
			    	if(intPropertyArray[i]!=0){

			    		promotions.setStartDate(earlyBookingStartDate);
						promotions.setEndDate(earlyBookingEndDate);
						promotions.setFirstRangeStartDate(earlyStayStartDate);
						promotions.setFirstRangeEndDate(earlyStayEndDate);
						promotions.setIsActive(true);
						promotions.setIsDeleted(false);
						promotions.setPromotionIsActive(false);
						promotions.setPromotionDiscountType(getDiscountType());
						PmsProperty property = propertyController.find(intPropertyArray[i]);
						promotions.setPmsProperty(property);
						
						Area area=areaController.find(getAreaId());
						promotions.setArea(area);
						
						promotions.setPromotionHoursRange(getHoursRange());
						promotions.setCreatedBy(this.userId);
						promotions.setCreatedDate(tsCurrentDate);
						promotions.setPromotionName(promotionName);
						promotions.setPromotionType(promotionType);
						PmsPromotions pmsPromotions=promotionController.add(promotions);
						PromotionId=pmsPromotions.getPromotionId();
						strPromotionId=String.valueOf(PromotionId)+","+strPromotionId;
						
			    	}
			    }
				sessionMap.put("strPromotionId",strPromotionId);
			}
			
			jsonOutput += "}";
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
		}
		catch(Exception e){
			logger.error(e);
			e.printStackTrace();
		}finally{
			
		}
		return null;
	
	
	}
	
	public String getLogPromotions(){

		try{
			this.propertyId=getPropertyId();
			if(this.propertyId==null){
				this.propertyId = (Integer) sessionMap.get("propertyId");
			}
			Calendar calendar=Calendar.getInstance();
			SimpleDateFormat format1 = new SimpleDateFormat("dd-MMM-yyyy");
	 		String currentDate = format1.format(calendar.getTime());
	    	java.util.Date curdate=format1.parse(currentDate);
	    	java.sql.Timestamp tsCurrentDate = new java.sql.Timestamp(curdate.getTime());
			String jsonOutput = "",strAccommodationType="";
			String strPromotionsType="",strPromotionName="",strDiscountType="";
			
			HttpServletResponse response = ServletActionContext.getResponse();
			PromotionManager promotionController=new PromotionManager();
			response.setContentType("application/json");
			ArrayList<Integer> arlPromoStatus=new ArrayList<Integer>();
			UserLoginManager userController=new UserLoginManager();
			PromotionDetailManager promotionDetailController=new PromotionDetailManager();
			List<PmsPromotions> listPromoStatus=promotionController.listPromotions();
			if(listPromoStatus.size()>0 && !listPromoStatus.isEmpty()){
				for(PmsPromotions promoStatus:listPromoStatus){
					arlPromoStatus.add(promoStatus.getPromotionId());
				}
			}
			this.promotionList=promotionController.listPromotionsDetails(this.propertyId,tsCurrentDate);
			int serialno=1;
			if(promotionList.size()>0 && !promotionList.isEmpty()){
				for(PmsPromotions promotions:promotionList){
					if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += ",{";
					else
						jsonOutput += "{";
					strPromotionsType=promotions.getPromotionType();
					jsonOutput += "\"promotionId\":\"" + promotions.getPromotionId() + "\"";
					
					strDiscountType=promotions.getPromotionDiscountType();
					
					jsonOutput += ",\"promotionDiscountType\":\"" + strDiscountType + "\"";
					jsonOutput += ",\"serialno\":\"" + serialno + "\"";
					
					if(strDiscountType==null){
						strDiscountType="None";
					}
					int firstRange=0,secondRange=0;
					String strHoursRange="";
					if(promotions.getPromotionHoursRange()!=null){
						String[] arrRange=promotions.getPromotionHoursRange().split("-");
						ArrayList<String> arlRange=new ArrayList<String>();
						for(String str:arrRange){
							arlRange.add(str);
						}
				        Iterator<String> rangeIter=arlRange.iterator();
					    while(rangeIter.hasNext()) {
				            firstRange=Integer.parseInt(rangeIter.next());
				            secondRange=Integer.parseInt(rangeIter.next());
					    }
					    if(firstRange!=0){
					    	firstRange=firstRange+1;	
					    }
					    strHoursRange=firstRange+"-"+secondRange;
					    
					}
					
					PmsPromotionDetails promotionsDetail=promotionDetailController.list(promotions.getPromotionId());
					if(strPromotionsType.equalsIgnoreCase("F")){
						if(strDiscountType.equalsIgnoreCase("Percent") ){
							if( promotionsDetail.getPromotionPercentage()!=null){
								strPromotionName="Flat "+String.valueOf(promotionsDetail.getPromotionPercentage().intValue()).trim()+" Percent";
								jsonOutput += ",\"promotionName\":\"" + strPromotionName.trim() + "\"";	
							}else{
								strPromotionName="Not Available";
								jsonOutput += ",\"promotionName\":\"" + strPromotionName.trim() + "\"";
							}
							
						}else if(strDiscountType.equalsIgnoreCase("INR")){
							if( promotionsDetail.getPromotionInr()!=null){
								strPromotionName="Flat "+String.valueOf(promotionsDetail.getPromotionInr().intValue()).trim()+" INR ";
								jsonOutput += ",\"promotionName\":\"" + strPromotionName.trim() + "\"";	
							}else{
								strPromotionName="Not Available";
								jsonOutput += ",\"promotionName\":\"" + strPromotionName.trim() + "\"";
							}
							
						}else{
							strPromotionName="Not Available";
							jsonOutput += ",\"promotionName\":\"" + strPromotionName.trim() + "\"";
						}
						
						jsonOutput += ",\"promotionType\":\"" + strPromotionsType.trim() + "\"";
						String start = new SimpleDateFormat("dd-MMM-yyyy").format(promotions.getStartDate());
						String end = new SimpleDateFormat("dd-MMM-yyyy").format(promotions.getEndDate());
						jsonOutput += ",\"startDate\":\"" + start + "\"";
						jsonOutput += ",\"endDate\":\"" + end + "\"";
					}else if(strPromotionsType.equalsIgnoreCase("L")){
						if(strDiscountType.equalsIgnoreCase("Percent")){
							strAccommodationType= promotions.getPropertyAccommodation().getAccommodationType().trim().toUpperCase();
							strPromotionName=""+strAccommodationType+" Range "+strHoursRange+" - "+promotions.getPromotionName()+" "+ promotionsDetail.getPromotionPercentage().intValue()+" Percent";
							jsonOutput += ",\"promotionName\":\"" + strPromotionName.trim() + "\"";
						}else if(strDiscountType.equalsIgnoreCase("INR")){
							strAccommodationType= promotions.getPropertyAccommodation().getAccommodationType().trim().toUpperCase();
							strPromotionName=""+strAccommodationType+" Range "+strHoursRange+" - "+promotions.getPromotionName()+" "+ promotionsDetail.getPromotionInr().intValue()+" INR";
							jsonOutput += ",\"promotionName\":\"" + strPromotionName.trim() + "\"";
						}
						
						jsonOutput += ",\"promotionType\":\"" + strPromotionsType.trim() + "\"";
						String start = new SimpleDateFormat("dd-MMM-yyyy").format(promotions.getStartDate());
						String end = new SimpleDateFormat("dd-MMM-yyyy").format(promotions.getEndDate());
						jsonOutput += ",\"startDate\":\"" + start + "\"";
						jsonOutput += ",\"endDate\":\"" + end + "\"";

					}else if(strPromotionsType.equalsIgnoreCase("E")){
						if(strDiscountType.equalsIgnoreCase("Percent")){
							if( promotionsDetail.getPromotionPercentage()!=null){
								strPromotionName="Early Bird "+String.valueOf(promotionsDetail.getPromotionPercentage().intValue()).trim()+" Percent";
								jsonOutput += ",\"promotionName\":\"" + strPromotionName.trim() + "\"";	
							}else{
								strPromotionName="Not Available";
								jsonOutput += ",\"promotionName\":\"" + strPromotionName.trim() + "\"";
							}
							
						}else if(strDiscountType.equalsIgnoreCase("INR") ){
							if(promotionsDetail.getPromotionInr()!=null){
								strPromotionName="Early Bird "+String.valueOf(promotionsDetail.getPromotionInr().intValue()).trim()+" INR";
								jsonOutput += ",\"promotionName\":\"" + strPromotionName.trim() + "\"";	
							}else{
								strPromotionName="Not Available";
								jsonOutput += ",\"promotionName\":\"" + strPromotionName.trim() + "\"";
							}
							
						}else{
							strPromotionName="Not Available";
							jsonOutput += ",\"promotionName\":\"" + strPromotionName.trim() + "\"";
						}
						
						jsonOutput += ",\"promotionType\":\"" + strPromotionsType.trim() + "\"";
						String start = new SimpleDateFormat("dd-MMM-yyyy").format(promotions.getStartDate());
						String end = new SimpleDateFormat("dd-MMM-yyyy").format(promotions.getEndDate());
						jsonOutput += ",\"startDate\":\"" + start + "\"";
						jsonOutput += ",\"endDate\":\"" + end + "\"";

					}
					String strCreateDate=new SimpleDateFormat("dd-MMM-yyyy").format(promotions.getCreatedDate());
					int createdBy=promotions.getCreatedBy();
					jsonOutput += ",\"createdDate\":\"" + strCreateDate + "\"";
					User user=userController.find(createdBy);
					jsonOutput += ",\"createdBy\":\"" + user.getUserName() + "\"";     
					serialno++;	
					jsonOutput += "}";
					
				}
				
			}
			
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
		}
		catch(Exception e){
			logger.error(e);
			e.printStackTrace();
		}
		finally{
			
		}
		return null;
	
	
	}
	public String getAllTypePromotions(){
		try{
			this.propertyId=getPropertyId();
			if(this.propertyId==null){
				this.propertyId = (Integer) sessionMap.get("propertyId");
			}
			Calendar calendar=Calendar.getInstance();
			SimpleDateFormat format1 = new SimpleDateFormat("dd-MMM-yyyy");
	 		String currentDate = format1.format(calendar.getTime());
	    	java.util.Date curdate=format1.parse(currentDate);
	    	java.sql.Timestamp tsCurrentDate = new java.sql.Timestamp(curdate.getTime());
			String jsonOutput = "",strAccommodationType="";
			String strPromotionsType="",strPromotionName="",strDiscountType="";
			
			HttpServletResponse response = ServletActionContext.getResponse();
			PromotionManager promotionController=new PromotionManager();
			response.setContentType("application/json");
			ArrayList<Integer> arlPromoStatus=new ArrayList<Integer>();
			PromotionDetailManager promotionDetailController=new PromotionDetailManager();
			List<PmsPromotions> listPromoStatus=promotionController.listPromotions();
			if(listPromoStatus.size()>0 && !listPromoStatus.isEmpty()){
				for(PmsPromotions promoStatus:listPromoStatus){
					arlPromoStatus.add(promoStatus.getPromotionId());
				}
			}
			this.promotionList=promotionController.listPromotionsDetails(this.propertyId,tsCurrentDate);
			int serialno=1;
			if(promotionList.size()>0 && !promotionList.isEmpty()){
				for(PmsPromotions promotions:promotionList){
					if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += ",{";
					else
						jsonOutput += "{";
					strPromotionsType=promotions.getPromotionType();
					jsonOutput += "\"promotionId\":\"" + promotions.getPromotionId() + "\"";
					
					strDiscountType=promotions.getPromotionDiscountType();
					
					jsonOutput += ",\"promotionDiscountType\":\"" + strDiscountType + "\"";
					jsonOutput += ",\"serialno\":\"" + serialno + "\"";
					
					if(strDiscountType==null){
						strDiscountType="None";
					}
					int firstRange=0,secondRange=0;
					String strHoursRange="";
					if(promotions.getPromotionHoursRange()!=null){
						String[] arrRange=promotions.getPromotionHoursRange().split("-");
						ArrayList<String> arlRange=new ArrayList<String>();
						for(String str:arrRange){
							arlRange.add(str);
						}
				        Iterator<String> rangeIter=arlRange.iterator();
					    while(rangeIter.hasNext()) {
				            firstRange=Integer.parseInt(rangeIter.next());
				            secondRange=Integer.parseInt(rangeIter.next());
					    }
					    if(firstRange!=0){
					    	firstRange=firstRange+1;	
					    }
					    strHoursRange=firstRange+"-"+secondRange;
					    
					}
					PmsPromotionDetails promotionsDetail=promotionDetailController.list(promotions.getPromotionId());
					if(strPromotionsType.equalsIgnoreCase("F") ){
						if(strDiscountType.equalsIgnoreCase("Percent")){
							if( promotionsDetail.getPromotionPercentage()!=null){
								strPromotionName="Flat "+String.valueOf(promotionsDetail.getPromotionPercentage().intValue()).trim()+" Percent";
								jsonOutput += ",\"promotionName\":\"" + strPromotionName.trim() + "\"";	
							}else{
								jsonOutput += ",\"promotionName\":\"" + "Flat not Available" + "\"";
							}
							
						}else if(strDiscountType.equalsIgnoreCase("INR")){
							if( promotionsDetail.getPromotionInr()!=null){
								strPromotionName="Flat "+String.valueOf(promotionsDetail.getPromotionInr().intValue()).trim()+" INR" ;
								jsonOutput += ",\"promotionName\":\"" + strPromotionName.trim() + "\"";	
							}else{
								jsonOutput += ",\"promotionName\":\"" + "Flat not Available" + "\"";
							}
							
						}else{
							jsonOutput += ",\"promotionName\":\"" + "Flat not Available" + "\"";
						}
						
						jsonOutput += ",\"promotionType\":\"" + strPromotionsType.trim() + "\"";
						String start = new SimpleDateFormat("dd-MMM-yyyy").format(promotions.getStartDate());
						String end = new SimpleDateFormat("dd-MMM-yyyy").format(promotions.getEndDate());
						jsonOutput += ",\"startDate\":\"" + start + "\"";
						jsonOutput += ",\"endDate\":\"" + end + "\"";
					}else if(strPromotionsType.equalsIgnoreCase("L")){
						if(strDiscountType.equalsIgnoreCase("Percent")){
							strAccommodationType= promotions.getPropertyAccommodation().getAccommodationType().trim().toUpperCase();
							strPromotionName=""+strAccommodationType+" Range "+strHoursRange+" - "+promotions.getPromotionName()+" "+ promotionsDetail.getPromotionPercentage().intValue()+" Percent";
							jsonOutput += ",\"promotionName\":\"" + strPromotionName.trim() + "\"";
						}else if(strDiscountType.equalsIgnoreCase("INR")){
							strAccommodationType= promotions.getPropertyAccommodation().getAccommodationType().trim().toUpperCase();
							strPromotionName=""+strAccommodationType+" Range "+strHoursRange+" - "+promotions.getPromotionName()+" "+ promotionsDetail.getPromotionInr().intValue()+" INR";
							jsonOutput += ",\"promotionName\":\"" + strPromotionName.trim() + "\"";
						}
						
						jsonOutput += ",\"promotionType\":\"" + strPromotionsType.trim() + "\"";
						String start = new SimpleDateFormat("dd-MMM-yyyy").format(promotions.getStartDate());
						String end = new SimpleDateFormat("dd-MMM-yyyy").format(promotions.getEndDate());
						jsonOutput += ",\"startDate\":\"" + start + "\"";
						jsonOutput += ",\"endDate\":\"" + end + "\"";

					}else if(strPromotionsType.equalsIgnoreCase("E")){
						if(strDiscountType.equalsIgnoreCase("Percent")){
							if( promotionsDetail.getPromotionPercentage()!=null){
								strPromotionName="Early Bird "+String.valueOf(promotionsDetail.getPromotionPercentage().intValue()).trim()+" Percent ";
								jsonOutput += ",\"promotionName\":\"" + strPromotionName.trim() + "\"";	
							}else{
								jsonOutput += ",\"promotionName\":\"" + "Early Bird not Available" + "\"";
							}
							
						}else if(strDiscountType.equalsIgnoreCase("INR")){
							if( promotionsDetail.getPromotionInr()!=null){
								strPromotionName="Early Bird "+String.valueOf(promotionsDetail.getPromotionInr().intValue()).trim()+" INR ";
								jsonOutput += ",\"promotionName\":\"" + strPromotionName.trim() + "\"";	
							}else{
								jsonOutput += ",\"promotionName\":\"" + "Early Bird not Available" + "\"";
							}
							
						}else{
							jsonOutput += ",\"promotionName\":\"" + "Early Bird not Available" + "\"";
						}
						
						jsonOutput += ",\"promotionType\":\"" + strPromotionsType.trim() + "\"";
						String start = new SimpleDateFormat("dd-MMM-yyyy").format(promotions.getStartDate());
						String end = new SimpleDateFormat("dd-MMM-yyyy").format(promotions.getEndDate());
						jsonOutput += ",\"startDate\":\"" + start + "\"";
						jsonOutput += ",\"endDate\":\"" + end + "\"";

					}
					boolean blnPromo=arlPromoStatus.contains(promotions.getPromotionId()); 
					jsonOutput += ",\"status\":\""+blnPromo+"\"";     
					serialno++;	
					jsonOutput += "}";
					
				}
				
			}
			
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
		}
		catch(Exception e){
			logger.error(e);
			e.printStackTrace();
		}
		finally{
			
		}
		return null;
	
	}
	
	public String getActivePromotions(){

		this.propertyId = (Integer) sessionMap.get("propertyId");
		try{
			Calendar calendar=Calendar.getInstance();
			SimpleDateFormat format1 = new SimpleDateFormat("dd-MMM-yyyy");
	 		String currentDate = format1.format(calendar.getTime());
	    	java.util.Date curdate=format1.parse(currentDate);
	    	java.sql.Timestamp tsCurrentDate = new java.sql.Timestamp(curdate.getTime());
			String jsonOutput = "",strAccommodationType="";
			String strPromotionsType="",strPromotionName="";
			HttpServletResponse response = ServletActionContext.getResponse();
			PromotionManager promotionController=new PromotionManager();
			response.setContentType("application/json");
			ArrayList<Integer> arlPromoStatus=new ArrayList<Integer>();
			PromotionDetailManager promotionDetailController=new PromotionDetailManager();
			List<PmsPromotions> listPromoStatus=promotionController.listPromotions(propertyId);
			if(listPromoStatus.size()>0 && !listPromoStatus.isEmpty()){
				for(PmsPromotions promoStatus:listPromoStatus){
					arlPromoStatus.add(promoStatus.getPromotionId());
				}
			}
			this.promotionList=promotionController.listPromotionsDetails(propertyId,tsCurrentDate);
			int serialno=1;
			for(PmsPromotions promotions:promotionList){
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				strPromotionsType=promotions.getPromotionType();
				jsonOutput += "\"promotionId\":\"" + promotions.getPromotionId() + "\"";
				String start = new SimpleDateFormat("dd-MMM-yyyy").format(promotions.getStartDate());
				String end = new SimpleDateFormat("dd-MMM-yyyy").format(promotions.getEndDate());
				jsonOutput += ",\"startDate\":\"" + start + "\"";
				jsonOutput += ",\"endDate\":\"" + end + "\"";
				jsonOutput += ",\"promotionName\":\"" + promotions.getPromotionName() + "\"";
				jsonOutput += ",\"serialNo\":\"" + serialno + "\"";
				PmsPromotionDetails promotionsDetail=promotionDetailController.list(promotions.getPromotionId());
				if(strPromotionsType.equalsIgnoreCase("F")){
					strPromotionName="Property Promos - Flat "+String.valueOf(promotionsDetail.getPromotionPercentage().intValue()).trim();
					jsonOutput += ",\"promotionName\":\"" + strPromotionName.trim() + "\"";
					jsonOutput += ",\"promotionType\":\"" + strPromotionsType.trim() + "\"";
				}else if(strPromotionsType.equalsIgnoreCase("L")){
					strAccommodationType= promotions.getPropertyAccommodation().getAccommodationType().trim().toUpperCase();
					strPromotionName="Category Promos - "+strAccommodationType+" - Last Minute "+String.valueOf(promotionsDetail.getPromotionHours()).trim()+" Hours";
					jsonOutput += ",\"promotionName\":\"" + strPromotionName.trim() + "\"";
					jsonOutput += ",\"promotionType\":\"" + strPromotionsType.trim() + "\"";
				}else if(strPromotionsType.equalsIgnoreCase("E")){
					strAccommodationType= promotions.getPropertyAccommodation().getAccommodationType().trim().toUpperCase();
					strPromotionName="Category Promos - "+strAccommodationType+" - Book "+promotionsDetail.getBookNights().intValue()+" and Get "+promotionsDetail.getGetNights().intValue()+" Nights";
					jsonOutput += ",\"promotionName\":\"" + strPromotionName.trim() + "\"";
					jsonOutput += ",\"promotionType\":\"" + strPromotionsType.trim() + "\"";
				}
				boolean blnPromo=arlPromoStatus.contains(promotions.getPromotionId()); 
				jsonOutput += ",\"status\":\""+blnPromo+"\"";     
								
				jsonOutput += "}";
				serialno++;
			}
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
		}
		catch(Exception e){
			logger.error(e);
			e.printStackTrace();
		}
		finally{
			
		}
		return null;
	
	}
	
	public String getAllPromotions(){
		this.propertyId = (Integer) sessionMap.get("propertyId");
		try{
			Calendar calendar=Calendar.getInstance();
			SimpleDateFormat format1 = new SimpleDateFormat("dd-MMM-yyyy");
	 		String currentDate = format1.format(calendar.getTime());
	    	java.util.Date curdate=format1.parse(currentDate);
	    	java.sql.Timestamp tsCurrentDate = new java.sql.Timestamp(curdate.getTime());
			String jsonOutput = "",strAccommodationType="";
			String strPromotionsType="",strPromotionName="";
			HttpServletResponse response = ServletActionContext.getResponse();
			PromotionManager promotionController=new PromotionManager();
			response.setContentType("application/json");
			ArrayList<Integer> arlPromoStatus=new ArrayList<Integer>();
			PromotionDetailManager promotionDetailController=new PromotionDetailManager();
			List<PmsPromotions> listPromoStatus=promotionController.listPromotions(propertyId);
			if(listPromoStatus.size()>0 && !listPromoStatus.isEmpty()){
				for(PmsPromotions promoStatus:listPromoStatus){
					arlPromoStatus.add(promoStatus.getPromotionId());
				}
			}
			this.promotionList=promotionController.listPromotionsDetails(propertyId,tsCurrentDate);
			
			for(PmsPromotions promotions:promotionList){
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				strPromotionsType=promotions.getPromotionType();
				jsonOutput += "\"promotionId\":\"" + promotions.getPromotionId() + "\"";
				PmsPromotionDetails promotionsDetail=promotionDetailController.list(promotions.getPromotionId());
				if(strPromotionsType.equalsIgnoreCase("F")){
					strPromotionName="Property Promos - Flat "+String.valueOf(promotionsDetail.getPromotionPercentage().intValue()).trim();
					jsonOutput += ",\"promotionName\":\"" + strPromotionName.trim() + "\"";
					jsonOutput += ",\"promotionType\":\"" + strPromotionsType.trim() + "\"";
				}else if(strPromotionsType.equalsIgnoreCase("L")){
					strAccommodationType= promotions.getPropertyAccommodation().getAccommodationType().trim().toUpperCase();
					strPromotionName="Category Promos - "+strAccommodationType+" - Last Minute "+String.valueOf(promotionsDetail.getPromotionHours()).trim()+" Hours";
					jsonOutput += ",\"promotionName\":\"" + strPromotionName.trim() + "\"";
					jsonOutput += ",\"promotionType\":\"" + strPromotionsType.trim() + "\"";
				}else if(strPromotionsType.equalsIgnoreCase("N")){
					strAccommodationType= promotions.getPropertyAccommodation().getAccommodationType().trim().toUpperCase();
					strPromotionName="Category Promos - "+strAccommodationType+" - Book "+promotionsDetail.getBookNights().intValue()+" and Get "+promotionsDetail.getGetNights().intValue()+" Nights";
					jsonOutput += ",\"promotionName\":\"" + strPromotionName.trim() + "\"";
					jsonOutput += ",\"promotionType\":\"" + strPromotionsType.trim() + "\"";
				}else if(strPromotionsType.equalsIgnoreCase("R")){
					strAccommodationType= promotions.getPropertyAccommodation().getAccommodationType().trim().toUpperCase();
					strPromotionName="Category Promos - "+strAccommodationType+" - Book "+promotionsDetail.getBookRooms().intValue()+" and Get "+promotionsDetail.getGetRooms().intValue()+" Rooms";
					jsonOutput += ",\"promotionName\":\"" + strPromotionName.trim() + "\"";
					jsonOutput += ",\"promotionType\":\"" + strPromotionsType.trim() + "\"";
				}
				boolean blnPromo=arlPromoStatus.contains(promotions.getPromotionId()); 
				jsonOutput += ",\"status\":\""+blnPromo+"\"";     
								
				jsonOutput += "}";
				
			}
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
		}
		catch(Exception e){
			logger.error(e);
			e.printStackTrace();
		}
		finally{
			
		}
		return null;
	}
	
	public String getPromotions(){
		this.propertyId = (Integer) sessionMap.get("propertyId");
		try{
			Calendar calendar=Calendar.getInstance();
			SimpleDateFormat format1 = new SimpleDateFormat("dd-MMM-yyyy");
	 		String currentDate = format1.format(calendar.getTime());
	    	java.util.Date curdate=format1.parse(currentDate);
	    	java.sql.Timestamp tsCurrentDate = new java.sql.Timestamp(curdate.getTime());
			String jsonOutput = "";
			String strPromotionsType="";
			HttpServletResponse response = ServletActionContext.getResponse();
			PromotionManager promotionController=new PromotionManager();
			response.setContentType("application/json");
			int serialno=1;
			this.promotionList=promotionController.listPromotionsDetails(propertyId,tsCurrentDate);
			for(PmsPromotions promotions:promotionList){
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				strPromotionsType=promotions.getPromotionType();
				jsonOutput += "\"promotionId\":\"" + promotions.getPromotionId() + "\"";
				String start = new SimpleDateFormat("dd-MMM-yyyy").format(promotions.getStartDate());
				String end = new SimpleDateFormat("dd-MMM-yyyy").format(promotions.getEndDate());
				jsonOutput += ",\"startDate\":\"" + start + "\"";
				jsonOutput += ",\"endDate\":\"" + end + "\"";
				jsonOutput += ",\"promotionName\":\"" + promotions.getPromotionName() + "\"";
				jsonOutput += ",\"serialNo\":\"" + serialno + "\"";
				if(strPromotionsType.equalsIgnoreCase("F")){
					jsonOutput += ",\"accommodationType\":\" All\"";
				}else{
					jsonOutput += ",\"accommodationType\":\"" + promotions.getPropertyAccommodation().getAccommodationType().trim() + "\"";
				}
				jsonOutput += "}";
				serialno++;
			}
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
		}
		catch(Exception e){
			logger.error(e);
			e.printStackTrace();
		}
		finally{
			
		}
		return null;
	}

	public String getUpdatePromotions(){
		this.propertyId = (Integer) sessionMap.get("propertyId");
		
		this.updatePromotionId = getUpdatePromotionId();
		sessionMap.put("updatePromotionId",updatePromotionId);
		try{
			String jsonOutput = "";
			String strPromotionsType="";
			HttpServletResponse response = ServletActionContext.getResponse();
			PromotionManager promotionController=new PromotionManager();
			PromotionDetailManager promotionDetailController=new PromotionDetailManager();
			response.setContentType("application/json");
			this.promotionList=promotionController.listUpdatePromotions(Integer.parseInt(updatePromotionId));
			if(promotionList.size()>0 && !promotionList.isEmpty()){
				for(PmsPromotions promotions:promotionList){
					if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += ",{";
					else
						jsonOutput += "{";
					strPromotionsType=promotions.getPromotionType();
					if(strPromotionsType.equalsIgnoreCase("F")){
						jsonOutput += "\"accommodationType\":\" All\"";
					}else{
						jsonOutput += "\"accommodationType\":\"" + promotions.getPropertyAccommodation().getAccommodationType().trim() + "\"";
					}
					
					
					if(strPromotionsType.equalsIgnoreCase("L")){
						jsonOutput += ",\"promotionType\":\" Last Minute Promotions\"";
					}else if(strPromotionsType.equalsIgnoreCase("R")){
						jsonOutput += ",\"promotionType\":\" Room Promotions\"";
					}else if(strPromotionsType.equalsIgnoreCase("N")){
						jsonOutput += ",\"promotionType\":\" Night Promotions\"";
					}else if(strPromotionsType.equalsIgnoreCase("F")){
						jsonOutput += ",\"promotionType\":\" Flat Promotions\"";
					}
					String start = new SimpleDateFormat("dd-MMM-yyyy").format(promotions.getStartDate());
					String end = new SimpleDateFormat("dd-MMM-yyyy").format(promotions.getEndDate());
					jsonOutput += ",\"startDate\":\"" + start + "\"";
					jsonOutput += ",\"endDate\":\"" + end + "\"";
					
					
					jsonOutput += "}";

				}
			}
			
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
		}
		catch(Exception e){
			logger.error(e);
			e.printStackTrace();
		}
		finally{
			
		}
		return null;
	}
	public String getViewPromotions(){
			this.propertyId = (Integer) sessionMap.get("propertyId");
			
			this.updatePromotionId = getUpdatePromotionId();
			sessionMap.put("updatePromotionId",updatePromotionId);
			try{
				String jsonOutput = "";
				String strPromotionsType="";
				String strDays="";
				String days ="";
				HttpServletResponse response = ServletActionContext.getResponse();
				PromotionManager promotionController=new PromotionManager();
				PromotionDetailManager promotionDetailController=new PromotionDetailManager();
				response.setContentType("application/json");
				this.promotionList=promotionController.listUpdatePromotions(Integer.parseInt(updatePromotionId));
				if(promotionList.size()>0 && !promotionList.isEmpty()){
					for(PmsPromotions promotions:promotionList){
						if (!jsonOutput.equalsIgnoreCase(""))
							jsonOutput += ",{";
						else
							jsonOutput += "{";
						strPromotionsType=promotions.getPromotionType();
						if(strPromotionsType.equalsIgnoreCase("F")){
							jsonOutput += "\"accommodationType\":\" All\"";
						}else{
							jsonOutput += "\"accommodationType\":\"" + promotions.getPropertyAccommodation().getAccommodationType().trim() + "\"";
						}
						if(strPromotionsType.equalsIgnoreCase("L")){
							jsonOutput += ",\"promotionType\":\" Last Minute Promotions\"";
						}else if(strPromotionsType.equalsIgnoreCase("R")){
							jsonOutput += ",\"promotionType\":\" Room Promotions\"";
						}else if(strPromotionsType.equalsIgnoreCase("N")){
							jsonOutput += ",\"promotionType\":\" Night Promotions\"";
						}else if(strPromotionsType.equalsIgnoreCase("F")){
							jsonOutput += ",\"promotionType\":\" Flat Promotions\"";
						}
						String start = new SimpleDateFormat("dd-MMM-yyyy").format(promotions.getStartDate());
						String end = new SimpleDateFormat("dd-MMM-yyyy").format(promotions.getEndDate());
						jsonOutput += ",\"startDate\":\"" + start + "\"";
						jsonOutput += ",\"endDate\":\"" + end + "\"";
						List<PmsPromotionDetails> listPromotionDays=promotionDetailController.listPromotionDetails(promotions.getPromotionId());
						if(listPromotionDays.size()>0 && !listPromotionDays.isEmpty()){
							for(PmsPromotionDetails promoDetails:listPromotionDays){
								days = promoDetails.getDaysOfWeek();
								String firstLetterUpperCase=days.substring(0, 1).toUpperCase() +  days.substring(1);
								strDays=firstLetterUpperCase+" "+strDays;
							}
						}
						PmsPromotionDetails promotionsDetail=promotionDetailController.list(promotions.getPromotionId());
						if(strPromotionsType.equalsIgnoreCase("L")){
							jsonOutput += ",\"promotionPercent\":\"" + promotionsDetail.getPromotionPercentage() + "\"";
							jsonOutput += ",\"promotionHours\":\"" + promotionsDetail.getPromotionHours().intValue() + "\"";
							jsonOutput += ",\"promotionAdultAmount\":\"" + 0 + "\"";
							jsonOutput += ",\"promotionChildAmount\":\"" + 0 + "\"";
							jsonOutput += ",\"promotionBaseAmount\":\"" + 0 + "\"";
							jsonOutput += ",\"promotionBookRooms\":\"" + 0 + "\"";
							jsonOutput += ",\"promotionGetRooms\":\"" + 0 + "\"";
							jsonOutput += ",\"promotionBookNights\":\"" + 0 + "\"";
							jsonOutput += ",\"promotionGetNights\":\"" + 0+ "\"";
							jsonOutput += ",\"promotionDays\":\"" + strDays+ "\"";
						}else if(strPromotionsType.equalsIgnoreCase("R")){
							jsonOutput += ",\"promotionBaseAmount\":\"" + promotionsDetail.getBaseAmount() + "\"";
							jsonOutput += ",\"promotionAdultAmount\":\"" + promotionsDetail.getExtraAdult() + "\"";
							jsonOutput += ",\"promotionChildAmount\":\"" + promotionsDetail.getExtraChild() + "\"";
							jsonOutput += ",\"promotionBookRooms\":\"" + promotionsDetail.getBookRooms().intValue() + "\"";
							jsonOutput += ",\"promotionGetRooms\":\"" + promotionsDetail.getGetRooms().intValue() + "\"";
							jsonOutput += ",\"promotionPercent\":\"" + 0 + "\"";
							jsonOutput += ",\"promotionHours\":\"" + 0 + "\"";
							jsonOutput += ",\"promotionBookNights\":\"" + 0 + "\"";
							jsonOutput += ",\"promotionGetNights\":\"" + 0 + "\"";
							jsonOutput += ",\"promotionDays\":\"" + strDays+ "\"";
						}else if(strPromotionsType.equalsIgnoreCase("N")){
							jsonOutput += ",\"promotionBaseAmount\":\"" + promotionsDetail.getBaseAmount() + "\"";
							jsonOutput += ",\"promotionAdultAmount\":\"" + promotionsDetail.getExtraAdult() + "\"";
							jsonOutput += ",\"promotionChildAmount\":\"" + promotionsDetail.getExtraChild() + "\"";
							jsonOutput += ",\"promotionBookNights\":\"" + promotionsDetail.getBookNights().intValue() + "\"";
							jsonOutput += ",\"promotionGetNights\":\"" + promotionsDetail.getGetNights().intValue() + "\"";
							jsonOutput += ",\"promotionBookRooms\":\"" + 0 + "\"";
							jsonOutput += ",\"promotionGetRooms\":\"" + 0 + "\"";
							jsonOutput += ",\"promotionPercent\":\"" + 0 + "\"";
							jsonOutput += ",\"promotionHours\":\"" + 0 + "\"";
							jsonOutput += ",\"promotionDays\":\"" + strDays+ "\"";
						}else if(strPromotionsType.equalsIgnoreCase("F")){
							jsonOutput += ",\"promotionPercent\":\"" + promotionsDetail.getPromotionPercentage() + "\"";
							jsonOutput += ",\"promotionHours\":\"" + 0 + "\"";
							jsonOutput += ",\"promotionBaseAmount\":\"" + 0 + "\"";
							jsonOutput += ",\"promotionAdultAmount\":\"" + 0 + "\"";
							jsonOutput += ",\"promotionChildAmount\":\"" + 0 + "\"";
							jsonOutput += ",\"promotionBookNights\":\"" + 0 + "\"";
							jsonOutput += ",\"promotionGetNights\":\"" + 0 + "\"";
							jsonOutput += ",\"promotionBookRooms\":\"" + 0 + "\"";
							jsonOutput += ",\"promotionGetRooms\":\"" + 0 + "\"";
							jsonOutput += ",\"promotionDays\":\"" + strDays+ "\"";
						}
						jsonOutput += "}";

					}
				}
				
				response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
		}catch(Exception e){
			logger.error(e);
			e.printStackTrace();
		}finally{
			
		}
		return null;
	}
	
	public String updatePromotions(){
		try{
			String jsonOutput="";
	    	HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("application/json");
			this.propertyId = (Integer) sessionMap.get("propertyId");
			String promotionsType=null,promoType=null;
			List<PmsPromotionDetails> listPromotionDetailCheck=null;
			int promoCount=0,dayCount=0,promoIdCount=0,promoAccommId=0;
			boolean blnPromoCheck=false,promotionChk=false,blnPromoDateCheck=false;
			ArrayList<Integer> arlAccommId=new ArrayList<Integer>();
			ArrayList<String> arlDaysOfWeek=new ArrayList<String>();
			ArrayList<String> arlPromoType=new ArrayList<String>();
			ArrayList<String> arlPromoDate=new ArrayList<String>();
			SimpleDateFormat sdfFormat=new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat sdfDays=new SimpleDateFormat("EEEE");
			DateFormat df = new SimpleDateFormat("EEEE");
			PromotionManager promotionController=new PromotionManager();
			PromotionDetailManager promotionDetailController=new PromotionDetailManager();
			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
			PmsPropertyManager propertyController = new PmsPropertyManager();
			String[] arrayPromoIds = getUpdatePromotionIds().split("\\s*,\\s*");
			
			
			int arrlen=arrayPromoIds.length;
			Timestamp startDate=null,endDate=null;
			boolean blnIsBreak=false;
			for (int i = 0; i < arrayPromoIds.length; i++) {
				if(Integer.parseInt(arrayPromoIds[i])!=0 && !blnIsBreak){
					PmsPromotions promos=promotionController.find(Integer.parseInt(arrayPromoIds[i]));
					startDate=promos.getStartDate();
					endDate=promos.getEndDate();
					boolean blnIsFirst=false;
			        
					String StartDate=new SimpleDateFormat("yyyy-MM-dd").format(startDate);
					String EndDate=new SimpleDateFormat("yyyy-MM-dd").format(endDate);
					DateTime lastFromDate = DateTime.parse(StartDate);
			        DateTime lastToDate = DateTime.parse(EndDate);
			        promoType=promos.getPromotionType();
			        arlPromoType.add(promoType);
			        if(!promoType.equalsIgnoreCase("F")){
						promoAccommId=promos.getPropertyAccommodation().getAccommodationId();
					}
			        List<DateTime> dateLastBetween = DateUtil.getDateRange(lastFromDate, lastToDate);
			        Iterator<DateTime> iterLastDateValues=dateLastBetween.iterator();
			        while(iterLastDateValues.hasNext()){
			        	DateTime dateValues=iterLastDateValues.next();
			        	java.util.Date DateValues = dateValues.toDate();
			        	String strDate=sdfFormat.format(DateValues);
			        	String strDay=sdfDays.format(DateValues).toLowerCase();
			        	String strPromoDay=null;
			        	if(promoType.equalsIgnoreCase("F")){
			        		listPromotionDetailCheck=promotionDetailController.listPromotionDetails(Integer.parseInt(arrayPromoIds[i]),df.format(dateValues.toDate()).toLowerCase());
		        			if(listPromotionDetailCheck.size()>0 && !listPromotionDetailCheck.isEmpty()){
		        				for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
		        					strPromoDay=promoDetails.getDaysOfWeek();
		        					if(arlDaysOfWeek.isEmpty()){
		        						arlDaysOfWeek.add(promoDetails.getDaysOfWeek());
		        					}else{
		        						if(!arlDaysOfWeek.contains(promoDetails.getDaysOfWeek())){
		        							arlDaysOfWeek.add(promoDetails.getDaysOfWeek());
		        						}
		        					}
		        				}
		        			}
		        			if(strPromoDay!=null && strPromoDay.equalsIgnoreCase(strDay)){
		        				if(arlPromoDate.contains(strDate) && arlDaysOfWeek.contains(strPromoDay)){
		    						blnPromoDateCheck=true;
		    						blnIsBreak=true;
									break;
		    					}else{
		    						arlPromoDate.add(strDate);
		    						blnPromoDateCheck=false;
		    					}
		        			}
		        			
			        	}else {
			        		if(promoType.equalsIgnoreCase("L") || promoType.equalsIgnoreCase("R") || promoType.equalsIgnoreCase("N")){
				        		listPromotionDetailCheck=promotionDetailController.listPromotionDetails(Integer.parseInt(arrayPromoIds[i]),df.format(dateValues.toDate()).toLowerCase());
			        			if(listPromotionDetailCheck.size()>0 && !listPromotionDetailCheck.isEmpty()){
			        				for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
			        					strPromoDay=promoDetails.getDaysOfWeek();
			        					if(arlDaysOfWeek.isEmpty()){
			        						arlDaysOfWeek.add(promoDetails.getDaysOfWeek());
			        					}else{
			        						if(!arlDaysOfWeek.contains(promoDetails.getDaysOfWeek())){
			        							arlDaysOfWeek.add(promoDetails.getDaysOfWeek());
			        						}
			        					}
			        				}
			        			}
			        			if(arlAccommId.isEmpty()){
			        				arlAccommId.add(0);
			        			}
			        			if(strPromoDay!=null && strPromoDay.equalsIgnoreCase(strDay)){
			        				if(arlPromoDate.contains(strDate) && arlDaysOfWeek.contains(strPromoDay)){
			        					if(arlAccommId.contains(promos.getPropertyAccommodation().getAccommodationId()) && !blnIsFirst){
				    						blnPromoDateCheck=true;
				    						blnIsBreak=true;
											break;
				    					}else{
				    						arlPromoDate.add(strDate);
				    						if(!arlAccommId.contains(promos.getPropertyAccommodation().getAccommodationId())){
				    							arlAccommId.add(promos.getPropertyAccommodation().getAccommodationId());
					    						blnPromoDateCheck=false;
				    						}
				    						
				    					}
			    					}else{
			    						arlPromoDate.add(strDate);
			    						if(!arlAccommId.contains(promos.getPropertyAccommodation().getAccommodationId())){
			    							arlAccommId.add(promos.getPropertyAccommodation().getAccommodationId());
				    						blnPromoDateCheck=false;
			    						}
//			    						arlAccommId.add(promos.getPropertyAccommodation().getAccommodationId());
//			    						blnPromoDateCheck=false;
			    					}
			        			}
				        	}
			        		
			        	}
			        	blnIsFirst=true;
			        }
				}
			}
		
			
			if(blnPromoDateCheck){
				blnPromoCheck=true;
			}
			
			
			if (!jsonOutput.equalsIgnoreCase(""))
				jsonOutput += ",{";
			else
				jsonOutput += "{";
//			blnPromoCheck=true;
			if(blnPromoCheck){
				jsonOutput += "\"check\":\"" + blnPromoCheck+ "\"";
			}else if(!blnPromoCheck){
				jsonOutput += "\"check\":\"" + blnPromoCheck+ "\"";
				
				List<PmsPromotions> listAllPromotions=promotionController.listActive(propertyId);
				if(listAllPromotions.size()>0 && !listAllPromotions.isEmpty()){
					for(PmsPromotions allpromotions:listAllPromotions){
						allpromotions.setIsActive(true);
						allpromotions.setIsDeleted(false);
						allpromotions.setPromotionIsActive(false);
						promotionController.edit(allpromotions);
					}
				}
				

				for (int i = 0; i < arrayPromoIds.length; i++) {
					if(Integer.parseInt(arrayPromoIds[i])!=0){
						this.promotionList=promotionController.listUpdatePromotions(Integer.parseInt(arrayPromoIds[i]));
						if(promotionList.size()>0 && !promotionList.isEmpty()){
							for(PmsPromotions promotions:promotionList){
								promotions.setIsActive(true);
								promotions.setIsDeleted(false);
								promotions.setPromotionIsActive(true);
								PmsProperty property = propertyController.find(propertyId);
								promotions.setPmsProperty(property);
								promotionsType=promotions.getPromotionType();
								if(!promotionsType.equalsIgnoreCase("F")){
									PropertyAccommodation accommodation=accommodationController.find(promotions.getPropertyAccommodation().getAccommodationId());
									promotions.setPropertyAccommodation(accommodation);
								}
								
								promotions.setStartDate(promotions.getStartDate());
								promotions.setEndDate(promotions.getEndDate());
								promotions.setPromotionName(promotions.getPromotionName());
								promotions.setPromotionType(promotions.getPromotionType());
								promotionController.edit(promotions);

							}
						}
					}
				}
			
				
			}
			jsonOutput += "}";
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
		}catch(Exception e){
			logger.error(e);
			e.printStackTrace();
		}finally{
			
		}
		
		return null;
	}
	
	public String getDisablePromotions(){
		try{
			this.propertyId = (Integer) sessionMap.get("propertyId");
			
			this.updatePromotionId = getUpdatePromotionId();
			sessionMap.put("updatePromotionId",updatePromotionId);
			
			String promotionsType=null;
			PromotionManager promotionController=new PromotionManager();
			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
			PmsPropertyManager propertyController = new PmsPropertyManager();
			this.promotionList=promotionController.listUpdatePromotions(Integer.parseInt(updatePromotionId));
			if(promotionList.size()>0 && !promotionList.isEmpty()){
				for(PmsPromotions promotions:promotionList){
					promotions.setIsActive(false);
					promotions.setIsDeleted(true);
					promotions.setPromotionIsActive(false);
					PmsProperty property = propertyController.find(propertyId);
					promotions.setPmsProperty(property);
					promotionsType=promotions.getPromotionType();
					if(!promotionsType.equalsIgnoreCase("F")){
						PropertyAccommodation accommodation=accommodationController.find(promotions.getPropertyAccommodation().getAccommodationId());
						promotions.setPropertyAccommodation(accommodation);
					}
					
					promotions.setStartDate(promotions.getStartDate());
					promotions.setEndDate(promotions.getEndDate());
					promotions.setPromotionName(promotions.getPromotionName());
					promotions.setPromotionType(promotions.getPromotionType());
					promotionController.edit(promotions);

				}
			}
			
		}catch(Exception e){
			logger.error(e);
			e.printStackTrace();
		}finally{
			
		}
		
		return null;
	}

	public String updateDoublePromotions(){


		try{
			String jsonOutput="";
	    	HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("application/json");
			String promotionsType=null,promoType=null,hoursRange=null;
			List<PmsPromotionDetails> listPromotionDetailCheck=null;
			List<PmsPromotions> listPromotionCheck=null;
			this.propertyId=getPropertyId();
			
			boolean blnFirstPromoCheck=false,blnFirstPromotionChk=false,blnFirstPromoDateCheck=false,blnPromoCheck=false;
			ArrayList<String> arlHoursRange=new ArrayList<String>();
			ArrayList<String> arlFirstDaysOfWeek=new ArrayList<String>();
			ArrayList<String> arlFirstPromoDate=new ArrayList<String>();
			ArrayList<Integer> arlSecondAccommId=new ArrayList<Integer>();
			ArrayList<String> arlSecondDaysOfWeek=new ArrayList<String>();
			ArrayList<String> arlPromoType=new ArrayList<String>();
			ArrayList<String> arlSecondPromoDate=new ArrayList<String>();
			SimpleDateFormat sdfFormat=new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat sdfDays=new SimpleDateFormat("EEEE");
			DateFormat df = new SimpleDateFormat("EEEE");
			PromotionManager promotionController=new PromotionManager();
			PromotionDetailManager promotionDetailController=new PromotionDetailManager();
			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
			PmsPropertyManager propertyController = new PmsPropertyManager();
			Timestamp startDate=null,endDate=null;
			boolean blnIsBreak=false;
			String[] arrayPromoIds = getUpdatePromotionIds().split("\\s*,\\s*");
			for (int i = 0; i < arrayPromoIds.length; i++) {
				if(Integer.parseInt(arrayPromoIds[i])!=0 && !blnIsBreak){

					PmsPromotions promos=promotionController.find(Integer.parseInt(arrayPromoIds[i]));
					startDate=promos.getStartDate();
					endDate=promos.getEndDate();
					boolean blnIsFirst=false;
					
					String StartDate=new SimpleDateFormat("yyyy-MM-dd").format(startDate);
					String EndDate=new SimpleDateFormat("yyyy-MM-dd").format(endDate);
					
					
					DateTime lastFromDate = DateTime.parse(StartDate);
			        DateTime lastToDate = DateTime.parse(EndDate);
			        promoType=promos.getPromotionType();
			        arlPromoType.add(promoType);
			        int diffInDays=0,dayCount=0; 
			        diffInDays = (int) ((endDate.getTime() - startDate.getTime()) / (1000 * 60 * 60 * 24));
			        diffInDays=diffInDays+1;
			        
			        List<DateTime> dateLastBetween = DateUtil.getDateRange(lastFromDate, lastToDate);
			        Iterator<DateTime> iterLastDateValues=dateLastBetween.iterator();
			        while(iterLastDateValues.hasNext()){
			        	DateTime dateValues=iterLastDateValues.next();
			        	java.util.Date DateValues = dateValues.toDate();
			        	String strFirstDate=sdfFormat.format(DateValues);
			        	String strFirstDay=sdfDays.format(DateValues).toLowerCase();
			        	String strFirstPromoDay=null;
			        	dayCount++;	
			        	
			        	if(promoType.equalsIgnoreCase("F") || promoType.equalsIgnoreCase("E")){
			        		listPromotionDetailCheck=promotionDetailController.listPromotionDetails(Integer.parseInt(arrayPromoIds[i]),df.format(dateValues.toDate()).toLowerCase());
		        			if(listPromotionDetailCheck.size()>0 && !listPromotionDetailCheck.isEmpty()){
		        				for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
		        					strFirstPromoDay=promoDetails.getDaysOfWeek();
		        					if(arlFirstDaysOfWeek.isEmpty()){
		        						arlFirstDaysOfWeek.add(promoDetails.getDaysOfWeek());
		        					}else{
		        						if(!arlFirstDaysOfWeek.contains(promoDetails.getDaysOfWeek())){
		        							arlFirstDaysOfWeek.add(promoDetails.getDaysOfWeek());
		        						}
		        					}
		        				}
		        			}
		        			if(strFirstPromoDay!=null && strFirstPromoDay.equalsIgnoreCase(strFirstDay)){
		        				if(arlFirstPromoDate.contains(strFirstDate) && arlFirstDaysOfWeek.contains(strFirstPromoDay)){
		    						blnFirstPromoDateCheck=true;
		    						blnIsBreak=true;
									break;
		    					}else{
		    						arlFirstPromoDate.add(strFirstDate);
		    						blnFirstPromoDateCheck=false;
		    					}
		        			}
		        			
			        	}
		        		blnIsFirst=true;
			        }
				
				}
			}
		
			
			if(blnFirstPromoDateCheck){
				blnPromoCheck=true;
			}
			
			if(!blnFirstPromoDateCheck){
				blnPromoCheck=false;
			}
			
			if (!jsonOutput.equalsIgnoreCase(""))
				jsonOutput += ",{";
			else
				jsonOutput += "{";
//			blnPromoCheck=true;
			if(blnPromoCheck){
				jsonOutput += "\"check\":\"" + blnPromoCheck+ "\"";
			}else if(!blnFirstPromoCheck){
				jsonOutput += "\"check\":\"" + blnPromoCheck+ "\"";


				List<PmsPromotions> listAllPromotions=promotionController.listActive(propertyId);
				if(listAllPromotions.size()>0 && !listAllPromotions.isEmpty()){
					for(PmsPromotions allpromotions:listAllPromotions){
						allpromotions.setIsActive(true);
						allpromotions.setIsDeleted(false);
						allpromotions.setPromotionIsActive(false);
						promotionController.edit(allpromotions);
					}
				}
				

				for (int i = 0; i < arrayPromoIds.length; i++) {
					if(Integer.parseInt(arrayPromoIds[i])!=0){
						this.promotionList=promotionController.listUpdatePromotions(Integer.parseInt(arrayPromoIds[i]));
						if(promotionList.size()>0 && !promotionList.isEmpty()){
							for(PmsPromotions promotions:promotionList){
								promotions.setIsActive(true);
								promotions.setIsDeleted(false);
								promotions.setPromotionIsActive(true);
								PmsProperty property = propertyController.find(propertyId);
								promotions.setPmsProperty(property);
								promotionsType=promotions.getPromotionType();
								if(!promotionsType.equalsIgnoreCase("F") && !promotionsType.equalsIgnoreCase("E")){
									PropertyAccommodation accommodation=accommodationController.find(promotions.getPropertyAccommodation().getAccommodationId());
									promotions.setPropertyAccommodation(accommodation);
								}
								
								promotions.setStartDate(promotions.getStartDate());
								promotions.setEndDate(promotions.getEndDate());
								promotions.setPromotionName(promotions.getPromotionName());
								promotions.setPromotionType(promotions.getPromotionType());
								promotionController.edit(promotions);

							}
						}
					}
				}
			}
			jsonOutput += "}";
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
		}catch(Exception e){
			logger.error(e);
			e.printStackTrace();
		}finally{
			
		}
		
		return null;
	
	
	}
	
	public String getStayStartDate() {
		return stayStartDate;
	}

	public void setStayStartDate(String stayStartDate) {
		this.stayStartDate = stayStartDate;
	}

	public String getStayEndDate() {
		return stayEndDate;
	}

	public void setStayEndDate(String stayEndDate) {
		this.stayEndDate = stayEndDate;
	}

	public String getBookingStartDate() {
		return bookingStartDate;
	}

	public void setBookingStartDate(String bookingStartDate) {
		this.bookingStartDate = bookingStartDate;
	}

	public String getBookingEndDate() {
		return bookingEndDate;
	}

	public void setBookingEndDate(String bookingEndDate) {
		this.bookingEndDate = bookingEndDate;
	}

	public Timestamp getEarlyStayStartDate() {
		return earlyStayStartDate;
	}

	public void setEarlyStayStartDate(Timestamp earlyStayStartDate) {
		this.earlyStayStartDate = earlyStayStartDate;
	}

	public Timestamp getEarlyStayEndDate() {
		return earlyStayEndDate;
	}

	public void setEarlyStayEndDate(Timestamp earlyStayEndDate) {
		this.earlyStayEndDate = earlyStayEndDate;
	}

	public Timestamp getEarlyBookingStartDate() {
		return earlyBookingStartDate;
	}

	public void setEarlyBookingStartDate(Timestamp earlyBookingStartDate) {
		this.earlyBookingStartDate = earlyBookingStartDate;
	}

	public Timestamp getEarlyBookingEndDate() {
		return earlyBookingEndDate;
	}

	public void setEarlyBookingEndDate(Timestamp earlyBookingEndDate) {
		this.earlyBookingEndDate = earlyBookingEndDate;
	}
	
	public Integer getAreaId() {
		return areaId;
	}

	public void setAreaId(Integer areaId) {
		this.areaId = areaId;
	}

	public String getHoursRange() {
		return hoursRange;
	}

	public void setHoursRange(String hoursRange) {
		this.hoursRange = hoursRange;
	}

	public String getCheckedProperty() {
		return checkedProperty;
	}

	public void setCheckedProperty(String checkedProperty) {
		this.checkedProperty = checkedProperty;
	}
	
	public String getDiscountType() {
		return discountType;
	}

	public void setDiscountType(String discountType) {
		this.discountType = discountType;
	}
	
	public Integer getPromotionDays() {
		return promotionDays;
	}

	public void setPromotionDays(Integer promotionDays) {
		this.promotionDays = promotionDays;
	}
	
	public String getEarlyPromoDate() {
		return earlyPromoDate;
	}

	public void setEarlyPromoDate(String earlyPromoDate) {
		this.earlyPromoDate = earlyPromoDate;
	}
}
