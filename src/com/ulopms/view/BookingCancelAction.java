package com.ulopms.view;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;
import org.docx4j.org.xhtmlrenderer.pdf.ITextFontResolver;
import org.docx4j.org.xhtmlrenderer.pdf.ITextRenderer;
import org.joda.time.DateTime;
import org.jsoup.select.Evaluator.IsEmpty;
import org.w3c.tidy.Tidy;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.html.simpleparser.HTMLWorker;
import com.lowagie.text.pdf.PdfWriter;
import com.opensymphony.xwork2.ActionSupport;
import com.ulopms.api.Booking;
import com.ulopms.controller.BookingCancellationManager;
import com.ulopms.controller.BookingDetailManager;
import com.ulopms.controller.BookingGuestDetailManager;
import com.ulopms.controller.PmsBookingManager;
import com.ulopms.controller.PmsGuestManager;
import com.ulopms.controller.PmsPropertyManager;
import com.ulopms.controller.PmsSourceManager;
import com.ulopms.controller.PmsStatusManager;
import com.ulopms.controller.PropertyAccommodationManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.BookingDetail;
import com.ulopms.model.BookingDetailReport;
import com.ulopms.model.BookingGuestDetail;
import com.ulopms.model.PmsBookedDetails;
import com.ulopms.model.PmsBooking;
import com.ulopms.model.PmsGuest;
import com.ulopms.model.PmsProperty;
import com.ulopms.model.PmsSource;
import com.ulopms.model.PmsStatus;
import com.ulopms.model.PmsTags;
import com.ulopms.model.PropertyAccommodation;
import com.ulopms.model.User;
import com.ulopms.util.Email;

import freemarker.template.Configuration;
import freemarker.template.Template;

public class BookingCancelAction extends ActionSupport implements
ServletRequestAware, SessionAware, UserAware{

	private static final long serialVersionUID = 914982665758390091L;
	private static final String DEST = "target/MailAttachment.pdf";
	
	private Date fromDate;
	private Date toDate;
	private Integer accommodationId;
	private String strFromDate;
	private String strToDate;
	private Integer propertyId;
	private Integer bookingId;
	private List<BookingDetailReport> listBookingDetails;
	List<BookingDetailReport> listCancelBookingDetails=new ArrayList<BookingDetailReport>();
	public Integer statusId;
	private Integer userId;
	private HttpSession session;
	private String filterDateName;
	private String filterBookingId;
	private double baseAmount;
	private double taxAmount;
	private String timeHours; 
	private String filterAccommId;
	private boolean mailFlag;
	private long roomCount;
	private long adultCount;
	private long childCount;
	private double totalBaseAmount=0.0;
	private double totalBaseTaxAmount=0.0;
	private double totalTaxAmount=0.0;
	private double totalAdvanceAmount=0.0;
	private double reducedAmount=0.0;
	private double refundAmount=0.0;
	private double promotionAmount=0.0;
	private double couponAmount=0.0;
	private Timestamp arrivalDate;
	private Timestamp departureDate;
	private String strAccommodationType;
	private Integer bookingDetailId;
	private String diffDays;
	private String diffInsDays;
	private String strGuestName;
	private String strEmailId;
	private String strMobileNo;
	private String propertyName;
	private String bookingInTime;
	private String bookingOutTime;
	private String sourceName;
	private Integer sourceId;
	private String paymentStatus;
	private String tariffStatus;
	private String checkIn;
	private String checkOut;
	private Integer bookingPropertyId;
	private Integer diffInDays;
	private String strRemarks;
	private Timestamp checkInTime;
	private Timestamp checkOutTime;
	private String voucherType;
	private Integer diffInHours;
	private String cancelledDate;
	private String checkInDays;
	private String checkOutDays;
	private String strCheckInTime;
	private String strCheckOutTime;
	private String strArrivalDate;
	private String strDepartureDate;
	private String latitude;
	private String longitude;
	private String phoneNumber;
	private String locationName;
	private String routeMap;
	private String reservationManagerEmail;
	private String reservationManagerContact;
	private String propertyEmail;
	private String address1;
	private String address2;
	private String propertyUrl;
	private String roomType;
	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}

	private static final Logger logger = Logger.getLogger(BookingCancelAction.class);

	
	private User user;

	
	// @Override
	// public User sgetModel() {
	// return user;
	// }

	@Override
	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}
	private SessionMap<String,Object> sessionMap;  
	  
	//getters and setters  
	  
	@SuppressWarnings("unchecked")
	@Override
	public void setSession(Map<String, Object> map) {
		// TODO Auto-generated method stub
		 sessionMap=(SessionMap)map;  
	}
	

	private List<BookingDetail> bookingDetailList;
	
	private HttpServletRequest request;

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	public BookingCancelAction() {

	}

	public String execute() {
		
		return SUCCESS;
	}

	
	public Integer getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(Integer propertyId) {
		this.propertyId = propertyId;
	}
	
	public String getStrFromDate() {
		return strFromDate;
	}

	public void setStrFromDate(String strFromDate) {
		this.strFromDate = strFromDate;
	}

	public String getStrToDate() {
		return strToDate;
	}

	public void setStrToDate(String strToDate) {
		this.strToDate = strToDate;
	}
	
	public String getBooking() throws IOException {
		
		try {
			
			this.filterDateName=getFilterDateName();
		    this.filterBookingId=getFilterBookingId();
		    int bookingId=Integer.parseInt(filterBookingId);
		   
		    if(bookingId==0){
		    	DateFormat format = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
			    java.util.Date dateStart = format.parse(getStrFromDate());
			    java.util.Date dateEnd = format.parse(getStrToDate());
			   
			    
			    Calendar calCheckStart=Calendar.getInstance();
			    calCheckStart.setTime(dateStart);
			    java.util.Date checkInDate = calCheckStart.getTime();
			    this.fromDate=new Date(checkInDate.getTime());
			    sessionMap.put("fromDate",fromDate); 
			    
			    Calendar calCheckEnd=Calendar.getInstance();
			    calCheckEnd.setTime(dateEnd);
			    java.util.Date checkOutDate = calCheckEnd.getTime();
			    this.toDate=new Date(checkOutDate.getTime());
			    sessionMap.put("endDate",toDate);
		    }
			
			 
		    
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			DecimalFormat df = new DecimalFormat("###.##");
			
			BookingCancellationManager cancellationController = new BookingCancellationManager();
			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
			
			response.setContentType("application/json");
			SimpleDateFormat format1 = new SimpleDateFormat("dd-MMM-yyyy");
			if(bookingId==0){
				this.listBookingDetails=cancellationController.listBooking((Integer) sessionMap.get("propertyId"),this.getFromDate(),this.getToDate(),this.getAccommodationId(),this.filterDateName,Integer.parseInt(this.filterBookingId));
			}else{
				this.listBookingDetails=cancellationController.listBooking((Integer) sessionMap.get("propertyId"),Integer.parseInt(this.filterBookingId));
			}
			//this.listBookingDetails=cancellationController.listBooking((Integer) sessionMap.get("propertyId"),this.getFromDate(),this.getToDate(),this.getAccommodationId());
			if(listBookingDetails.size()>0 && !listBookingDetails.isEmpty()){
				for(BookingDetailReport bookingDetailReport:listBookingDetails){
					if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += ",{";
					else
						jsonOutput += "{";
					
					
					jsonOutput += "\"bookingId\":\"" + bookingDetailReport.getBookingId()+ "\"";
					
					String strFirstName="",firstNameLetterUpperCase="";
					String strLastName="",lastNameLetterUpperCase="";
					strFirstName=bookingDetailReport.getFirstName();
					strLastName=bookingDetailReport.getLastName();
					if(strFirstName!=null && strFirstName!="" && !strFirstName.equalsIgnoreCase("undefined")){
						strFirstName=strFirstName.trim();
						firstNameLetterUpperCase=strFirstName.substring(0, 1).toUpperCase()+strFirstName.substring(1);
						strFirstName=firstNameLetterUpperCase;
					}else{
						strFirstName="-";
					}
					if(strLastName!=null && strLastName!="" && !strLastName.equalsIgnoreCase("undefined")){
						strLastName=strLastName.trim();
						lastNameLetterUpperCase=strLastName.substring(0, 1).toUpperCase() +  strLastName.substring(1);
						strLastName=lastNameLetterUpperCase;
					}else{
						strLastName="";
					}
					
					jsonOutput += ",\"guestName\":\"" + strFirstName+" "+strLastName + "\"";
					
					String strAccommodationType=null;
					Integer accommId=0;
					accommodationId=bookingDetailReport.getAccommodationId();
					if(accommodationId>0)
					{
						PropertyAccommodation accommodations=accommodationController.find(accommodationId);
						strAccommodationType=accommodations.getAccommodationType();
						accommId=accommodations.getAccommodationId();
						jsonOutput += ",\"accommodationType\":\" "+strAccommodationType+"\"";
						jsonOutput += ",\"accommodationId\":\" "+accommId+"\"";
					}
					else{
						jsonOutput += ",\"accommodationType\":\" ALL\"";
						jsonOutput += ",\"accommodationId\":\" "+0+"\"";
					}
					
					Double dblBaseAmount=0.0,dblTaxAmount=0.0,dblBaseTaxAmount=0.0,dblTotalAmount=0.0;
					Integer rooms=0;
					long arrivalDate =bookingDetailReport.getArrivalDate().getTime();
					java.util.Date dateArrival = new java.util.Date(arrivalDate);
					String strArrivalDate=format1.format(dateArrival); 
					
					jsonOutput += ",\"checkIn\":\"" + strArrivalDate+ "\"";
					
					long departureDate =bookingDetailReport.getDepartureDate().getTime();
					java.util.Date dateDeparture = new java.util.Date(departureDate);
					String strDepartureDate=format1.format(dateDeparture); 
					
					jsonOutput += ",\"checkOut\":\"" + strDepartureDate+ "\"";
					SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
					
					Timestamp FromDate=bookingDetailReport.getArrivalDate();
					Timestamp ToDate=bookingDetailReport.getDepartureDate();
					
					String strFromDate=format2.format(FromDate);
					String strToDate=format2.format(ToDate);
					
			    	DateTime startdate = DateTime.parse(strFromDate);
			        DateTime enddate = DateTime.parse(strToDate);
			        java.util.Date fromdate = startdate.toDate();
			 		java.util.Date todate = enddate.toDate();
			 		
					int diffInDays = (int) ((todate.getTime() - fromdate.getTime()) / (1000 * 60 * 60 * 24));
					
					jsonOutput += ",\"statusId\":\"" +bookingDetailReport.getStatusId()+ "\"";
					
					dblBaseAmount=bookingDetailReport.getAmount();
					dblTaxAmount=bookingDetailReport.getTaxAmount();
					long lngRooms=bookingDetailReport.getRoomCount();
					
					rooms=(int)lngRooms;
					jsonOutput += ",\"rooms\":\"" + rooms/diffInDays+ "\"";
					
					dblBaseTaxAmount=dblBaseAmount+dblTaxAmount;
					
					dblTotalAmount=dblBaseTaxAmount;
					jsonOutput += ",\"amount\":\"" + df.format(dblTotalAmount)+ "\"";
					
					jsonOutput += "}";
				}
			}
			

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	
	public String editBookingCancelStatus() {
		PmsBookingManager bookingController = new PmsBookingManager();
		BookingDetailManager bookingDetailController=new BookingDetailManager();
		List<BookingDetail> listBookingDetail=null;
		PmsStatusManager statusController = new PmsStatusManager();
		this.userId=(Integer)sessionMap.get("userId");
		try {
			listBookingDetail=bookingDetailController.findId(getBookingId(),getStatusId());
			if(listBookingDetail.size()>0 && !listBookingDetail.isEmpty()){
				if(listBookingDetail.size()>0){
					if((int)getStatusId() == 1){
						PmsBooking booking = bookingController.find(getBookingId());
						long time = System.currentTimeMillis();
						java.sql.Date date = new java.sql.Date(time);
//						booking.setIsActive(false);
//						booking.setIsDeleted(true);
						booking.setModifiedBy(this.userId);
						booking.setModifiedDate(new java.sql.Timestamp(date.getTime()));
						PmsStatus status = statusController.find(4);
						booking.setPmsStatus(status);
						bookingController.edit(booking);
					}else if((int)getStatusId() == 2){
						PmsBooking booking = bookingController.find(getBookingId());
						long time = System.currentTimeMillis();
						java.sql.Date date = new java.sql.Date(time);
//						booking.setIsActive(false);
//						booking.setIsDeleted(true);
						booking.setModifiedBy(this.userId);
						booking.setModifiedDate(new java.sql.Timestamp(date.getTime()));
						PmsStatus status = statusController.find(4);
						booking.setPmsStatus(status);
						bookingController.edit(booking);
					}else if((int)getStatusId() == 3){
						PmsBooking booking = bookingController.find(getBookingId());
						long time = System.currentTimeMillis();
						java.sql.Date date = new java.sql.Date(time);
//						booking.setIsActive(false);
//						booking.setIsDeleted(true);
						booking.setModifiedBy(this.userId);
						booking.setModifiedDate(new java.sql.Timestamp(date.getTime()));
						PmsStatus status = statusController.find(4);
						booking.setPmsStatus(status);
						bookingController.edit(booking);
					}else if((int)getStatusId() == 5){
						PmsBooking booking = bookingController.find(getBookingId());
						long time = System.currentTimeMillis();
						java.sql.Date date = new java.sql.Date(time);
//						booking.setIsActive(false);
//						booking.setIsDeleted(true);
						booking.setModifiedBy(this.userId);
						booking.setModifiedDate(new java.sql.Timestamp(date.getTime()));
						PmsStatus status = statusController.find(4);
						booking.setPmsStatus(status);
						bookingController.edit(booking);
					}
				}
			}
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}
	
    public String editBookingCancelDetailsStatus() {
		BookingDetailManager detailController = new BookingDetailManager();
		PmsStatusManager statusController = new PmsStatusManager();
		this.userId=(Integer)sessionMap.get("userId");
		try {
			if((int)getStatusId() == 1){
				this.bookingDetailList = detailController.findId(getBookingId());
				for (BookingDetail bookingDetails : bookingDetailList){	
					int accommId=bookingDetails.getPropertyAccommodation().getAccommodationId();
					if(accommId==(int)getAccommodationId()){
//						bookingDetails.setIsActive(false);
//						bookingDetails.setIsDeleted(true);
						bookingDetails.setModifiedBy(this.userId);
						long time = System.currentTimeMillis();
						java.sql.Date date = new java.sql.Date(time);
						bookingDetails.setModifiedDate(new java.sql.Timestamp(date.getTime()));
						PmsStatus status = statusController.find(4);
						bookingDetails.setPmsStatus(status);
						detailController.edit(bookingDetails);
					}
				}
			}else if((int)getStatusId() == 2){
				this.bookingDetailList = detailController.findId(getBookingId());
				for (BookingDetail bookingDetails : bookingDetailList){	
					int accommId=bookingDetails.getPropertyAccommodation().getAccommodationId();
					if(accommId==(int)getAccommodationId()){
//						bookingDetails.setIsActive(false);
//						bookingDetails.setIsDeleted(true);
						bookingDetails.setModifiedBy(this.userId);
						long time = System.currentTimeMillis();
						java.sql.Date date = new java.sql.Date(time);
						bookingDetails.setModifiedDate(new java.sql.Timestamp(date.getTime()));
						PmsStatus status = statusController.find(4);
						bookingDetails.setPmsStatus(status);
						detailController.edit(bookingDetails);
					}
				}
			}else if((int)getStatusId() == 3){
				this.bookingDetailList = detailController.findId(getBookingId());
				for (BookingDetail bookingDetails : bookingDetailList){	
					int accommId=bookingDetails.getPropertyAccommodation().getAccommodationId();
					if(accommId==(int)getAccommodationId()){
//						bookingDetails.setIsActive(false);
//						bookingDetails.setIsDeleted(true);
						bookingDetails.setModifiedBy(this.userId);
						long time = System.currentTimeMillis();
						java.sql.Date date = new java.sql.Date(time);
						bookingDetails.setModifiedDate(new java.sql.Timestamp(date.getTime()));
						PmsStatus status = statusController.find(4);
						bookingDetails.setPmsStatus(status);
						detailController.edit(bookingDetails);
					}
				}
			}else if((int)getStatusId() == 5){
				this.bookingDetailList = detailController.findId(getBookingId());
				for (BookingDetail bookingDetails : bookingDetailList){	
					int accommId=bookingDetails.getPropertyAccommodation().getAccommodationId();
					if(accommId==(int)getAccommodationId()){
//						bookingDetails.setIsActive(false);
//						bookingDetails.setIsDeleted(true);
						bookingDetails.setModifiedBy(this.userId);
						long time = System.currentTimeMillis();
						java.sql.Date date = new java.sql.Date(time);
						bookingDetails.setModifiedDate(new java.sql.Timestamp(date.getTime()));
						PmsStatus status = statusController.find(4);
						bookingDetails.setPmsStatus(status);
						detailController.edit(bookingDetails);
					}
				}
			}
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}
	
	public String getBookingCancelDetails(){
		try{
			BookingCancellationManager cancellationController = new BookingCancellationManager();
			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
			String strMobileNumber="",statusName="",strEmailId="",sourceName="",strStatusName="";
			String strFirstName="",firstNameLetterUpperCase="";
			String strLastName="",lastNameLetterUpperCase="";
			Integer rooms=0,adultCount=0,childCount=0,statusId=0,sourceId=0,noOfRooms=0;
			SimpleDateFormat format1 = new SimpleDateFormat("dd-MMM-yyyy");
			SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
			DecimalFormat df = new DecimalFormat("###.##");
			
			java.util.Date date=new java.util.Date();
        	Calendar calDate=Calendar.getInstance();
        	calDate.setTime(date);
        	calDate.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
        	int bookedHours=calDate.get(Calendar.HOUR);
        	int bookedMinute=calDate.get(Calendar.MINUTE);
        	int AMPM=calDate.get(Calendar.AM_PM);
        	String strBookingDate=new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH).format(date);
        	String hours="",minutes="";
        	if(bookedHours<10){
        		hours=String.valueOf(bookedHours);
        		hours="0"+hours;
        	}else{
        		hours=String.valueOf(bookedHours);
        	}
        	if(bookedMinute<10){
        		minutes=String.valueOf(bookedMinute);
        		minutes="0"+minutes;
        	}else{
        		minutes=String.valueOf(bookedMinute);
        	}
        	if(AMPM==0){//If the current time is AM
        		this.timeHours=strBookingDate+" "+hours+" : "+minutes+" AM ";
        	}else if(AMPM==1){//If the current time is PM
        		this.timeHours=strBookingDate+" "+hours+" : "+minutes+" PM ";
        	}
        	
			this.propertyId=(Integer) sessionMap.get("propertyId");
			PmsPropertyManager pmsPropertyController=new PmsPropertyManager();
			
			
			
			this.listBookingDetails=cancellationController.listBooking((Integer) sessionMap.get("propertyId"),getBookingId());
			
			if(listBookingDetails.size()>0 && !listBookingDetails.isEmpty()){
				for(BookingDetailReport bookingDetailReport:listBookingDetails){
					
					
					strFirstName=bookingDetailReport.getFirstName();
					strLastName=bookingDetailReport.getLastName();
					Double dblBaseAmount=0.0,dblTaxAmount=0.0,dblTotalAmount=0.0,dblAdvanceAmount=0.0;
					if(strFirstName!=null && strFirstName!="" && !strFirstName.equalsIgnoreCase("undefined")){
						strFirstName=strFirstName.trim();
						firstNameLetterUpperCase=strFirstName.substring(0, 1).toUpperCase()+strFirstName.substring(1);
						strFirstName=firstNameLetterUpperCase;
					}else{
						strFirstName="-";
					}
					if(strLastName!=null && strLastName!=""  && !strLastName.equalsIgnoreCase("undefined")){
						strLastName=strLastName.trim();
						lastNameLetterUpperCase=strLastName.substring(0, 1).toUpperCase() +  strLastName.substring(1);
						strLastName=lastNameLetterUpperCase;
					}else{
						strLastName="";
					}
					strMobileNumber=bookingDetailReport.getMobileNumber();
					strEmailId=bookingDetailReport.getEmailId();
					long arrivalDate =bookingDetailReport.getArrivalDate().getTime();
					java.util.Date dateArrival = new java.util.Date(arrivalDate);
					String strArrivalDate=format1.format(dateArrival); 
					
					
					long departureDate =bookingDetailReport.getDepartureDate().getTime();
					java.util.Date dateDeparture = new java.util.Date(departureDate);
					String strDepartureDate=format1.format(dateDeparture); 
					
					
					
					Timestamp FromDate=bookingDetailReport.getArrivalDate();
					Timestamp ToDate=bookingDetailReport.getDepartureDate();
					
					String strFromDate=format2.format(FromDate);
					String strToDate=format2.format(ToDate);
					
			    	DateTime startdate = DateTime.parse(strFromDate);
			        DateTime enddate = DateTime.parse(strToDate);
			        java.util.Date fromdate = startdate.toDate();
			 		java.util.Date todate = enddate.toDate();
			 		
					int diffInDays = (int) ((todate.getTime() - fromdate.getTime()) / (1000 * 60 * 60 * 24));
					
					
					String strAccommodationType=null;
					Integer accommId=0;
					accommId=bookingDetailReport.getAccommodationId();
					if(accommId>0)
					{
						PropertyAccommodation accommodations=accommodationController.find(accommId);
						strAccommodationType=accommodations.getAccommodationType();
					}
					
					long lngrooms=bookingDetailReport.getRoomCount();
					rooms=(int)lngrooms;
					noOfRooms=rooms/diffInDays;
			 		Integer diffRoomInDays=noOfRooms*diffInDays;
			 		adultCount=bookingDetailReport.getAdultCount();
			 		childCount=bookingDetailReport.getChildCount();
			 		
			 		
			 		dblAdvanceAmount=bookingDetailReport.getAdvanceAmount();
			 		dblTotalAmount=dblBaseAmount+dblTaxAmount;
			 		
			 		if(dblAdvanceAmount!=null  && dblAdvanceAmount>=0.0){
						dblAdvanceAmount=bookingDetailReport.getAdvanceAmount();
					}else{
						if(sourceId==1){
							dblAdvanceAmount=dblTotalAmount;
						}else{
							dblAdvanceAmount=0.0;
						}
					}
					String strTotal=df.format(dblTotalAmount);
					
					boolean compareValues=Double.valueOf(strTotal).equals(dblAdvanceAmount);
					
					
					if(dblAdvanceAmount!=null && dblAdvanceAmount>=0.0){
						if(compareValues){
							strStatusName="CASH PAID";
						}else{
							strStatusName="PAY AT HOTEL";
						}
					}else{
						if(sourceId==1){
							strStatusName="CASH PAID";
						}else{
							if(compareValues){
								strStatusName="CASH PAID";
							}else{
								strStatusName="PAY AT HOTEL";
							}
						}
					}
					
					baseAmount+=bookingDetailReport.getAmount();
					taxAmount+=bookingDetailReport.getTaxAmount();
				}
			}
			
		}catch(Exception e){
			logger.error(e);
			e.printStackTrace();
		}
		return null;
	}
	
	public String getCancelBookingIdDetails() throws IOException {
		
		try {
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			this.filterBookingId=getFilterBookingId();
			DecimalFormat df = new DecimalFormat("###.##");
			
			BookingCancellationManager cancelController=new BookingCancellationManager();
			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
			PmsSourceManager sourceController=new PmsSourceManager();
			PmsStatusManager statusController=new PmsStatusManager();
		
			
			response.setContentType("application/json");
			
			this.listBookingDetails=cancelController.listBookingDetail(Integer.parseInt(this.filterBookingId),(Integer) sessionMap.get("propertyId"));
			if(listBookingDetails.size()>0 && !listBookingDetails.isEmpty()){
				for(BookingDetailReport bookingDetailReport:listBookingDetails){
					if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += ",{";
					else
						jsonOutput += "{";
					
					jsonOutput += "\"bookingId\":\"" + bookingDetailReport.getBookingId()+ "\"";
					String strFirstName="",firstNameLetterUpperCase="";
					String strLastName="",lastNameLetterUpperCase="";
					strFirstName=bookingDetailReport.getFirstName();
					strLastName=bookingDetailReport.getLastName();
					if(strFirstName!=null && strFirstName!="" && !strFirstName.equalsIgnoreCase("undefined")){
						strFirstName=strFirstName.trim();
						firstNameLetterUpperCase=strFirstName.substring(0, 1).toUpperCase()+strFirstName.substring(1);
						strFirstName=firstNameLetterUpperCase;
					}else{
						strFirstName="-";
					}
					if(strLastName!=null && strLastName!=""  && !strLastName.equalsIgnoreCase("undefined")){
						strLastName=strLastName.trim();
						lastNameLetterUpperCase=strLastName.substring(0, 1).toUpperCase() +  strLastName.substring(1);
						strLastName=lastNameLetterUpperCase;
					}else{
						strLastName="";
					}
					jsonOutput += ",\"guestId\":\"" + bookingDetailReport.getGuestId() + "\"";
					jsonOutput += ",\"guestName\":\"" + strFirstName+" "+strLastName + "\"";
					
					
					
//					jsonOutput += ",\"status\":\"false\"";
					Double dblBaseAmount=0.0,dblTaxAmount=0.0,dblBaseTaxAmount=0.0,dblTotalAmount=0.0,
							dblAdvanceAmount=0.0,dblDueAmount=0.0,dblTotalBaseAmount=0.0,dblPromotionAmount=0.0,dblCouponAmount=0.0;
					
					Integer rooms=0,adultCount=0,childCount=0,statusId=0,sourceId=0;
					String strMobileNumber="",statusName="",strEmailId="",sourceName="";
					strMobileNumber=bookingDetailReport.getMobileNumber();
					jsonOutput += ",\"mobileNumber\":\"" + strMobileNumber+ "\"";
					
					strEmailId=bookingDetailReport.getEmailId();
					jsonOutput += ",\"emailId\":\"" + strEmailId+ "\"";
					
					SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
					SimpleDateFormat format2 = new SimpleDateFormat("dd-MMM-yyyy");
					java.util.Date date=new java.util.Date();
					Calendar today=Calendar.getInstance();
					today.setTime(date);
					today.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
				    String strCurrentDate = new SimpleDateFormat("yyyy-MM-dd").format(today.getTime());
				    java.util.Date currentDate = format1.parse(strCurrentDate);
				    Timestamp currentTS = new Timestamp(currentDate.getTime());
					
					long arrivalDate =bookingDetailReport.getArrivalDate().getTime();
					java.util.Date dateArrival = new java.util.Date(arrivalDate);
					long departureDate =bookingDetailReport.getDepartureDate().getTime();
					java.util.Date dateDeparture = new java.util.Date(departureDate);
				    String strArrivalDate=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(arrivalDate);
				    String strDepartureDate=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(departureDate);

				    jsonOutput += ",\"strArrivalDate\":\"" +strArrivalDate+ "\"";
					jsonOutput += ",\"strDepartureDate\":\"" +strDepartureDate+ "\"";
					
					
					
					String ArrivalDate=format2.format(dateArrival); 
					jsonOutput += ",\"checkIn\":\"" + ArrivalDate+ "\"";
					
					
					String DepartureDate=format2.format(dateDeparture); 
					jsonOutput += ",\"checkOut\":\"" + DepartureDate+ "\"";
					
					long diff=dateArrival.getTime()-currentDate.getTime();
					
					long diffHours = diff / (60 * 60 * 1000) % 24;
					long diffDays = diff / (24 * 60 * 60 * 1000);
					int intDiffDays=0;
					long diffInDays=dateDeparture.getTime()-dateArrival.getTime();
					this.checkInTime=bookingDetailReport.getCheckInTime();
					this.checkOutTime=bookingDetailReport.getCheckOutTime();
					
					if(this.checkInTime!=null && this.checkOutTime!=null){
						jsonOutput += ",\"strCheckInTime\":\"" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(this.checkInTime)+ "\"";
						jsonOutput += ",\"strCheckOutTime\":\"" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(this.checkOutTime)+ "\"";	
						String strFromDate=format1.format(this.checkInTime);
						String strToDate=format1.format(this.checkOutTime);
						
						
				    	DateTime startdate = DateTime.parse(strFromDate);
				        DateTime enddate = DateTime.parse(strToDate);
				        java.util.Date fromdate = startdate.toDate();
				 		java.util.Date todate = enddate.toDate();
				 		long lngHours=todate.getTime()-fromdate.getTime();
	                	long diffHours1 = lngHours / (60 * 60 * 1000) % 24;
	                	this.diffInHours=(int)diffHours1;
	                	this.voucherType="hourly-wise";
					}else{
						Timestamp FromDate=bookingDetailReport.getArrivalDate();
						Timestamp ToDate=bookingDetailReport.getDepartureDate();
						
						String strFromDate=format1.format(FromDate);
						String strToDate=format1.format(ToDate);
						
						
				    	DateTime startdate = DateTime.parse(strFromDate);
				        DateTime enddate = DateTime.parse(strToDate);
				        java.util.Date fromdate = startdate.toDate();
				 		java.util.Date todate = enddate.toDate();
				 		
						intDiffDays = (int) ((todate.getTime() - fromdate.getTime()) / (1000 * 60 * 60 * 24));
						this.voucherType="day-wise";
						this.diffInHours=0;
						jsonOutput += ",\"strCheckInTime\":\"" + this.checkInTime+ "\"";
						jsonOutput += ",\"strCheckOutTime\":\"" + this.checkOutTime+ "\"";
					}
					
					jsonOutput += ",\"hours\":\"" + this.diffInHours+ "\"";
					jsonOutput += ",\"voucherType\":\"" + this.voucherType+ "\"";
					jsonOutput += ",\"propertyId\":\"" + bookingDetailReport.getPropertyId() + "\"";
					jsonOutput += ",\"accommodationId\":\"" + bookingDetailReport.getAccommodationId() + "\"";
					PropertyAccommodation accommodations=accommodationController.find(bookingDetailReport.getAccommodationId());
					jsonOutput += ",\"accommodationType\":\"" + accommodations.getAccommodationType().trim()+ "\"";
					
					
					long lngrooms=bookingDetailReport.getRoomCount();
					rooms=(int)lngrooms;
					if(intDiffDays == 0){
						intDiffDays = 1;
					}
					jsonOutput += ",\"rooms\":\"" + rooms/intDiffDays + "\"";
					jsonOutput += ",\"adultCount\":\"" +  bookingDetailReport.getAdultCount() + "\"";
					jsonOutput += ",\"childCount\":\"" + bookingDetailReport.getChildCount() + "\"";
					jsonOutput += ",\"advanceAmount\":\"" + df.format(bookingDetailReport.getAdvanceAmount()) + "\"";

					dblBaseAmount=bookingDetailReport.getAmount();
					dblTaxAmount=bookingDetailReport.getTaxAmount();
					dblAdvanceAmount=bookingDetailReport.getAdvanceAmount();
					dblTotalBaseAmount=dblBaseAmount+dblTaxAmount;
					dblPromotionAmount=bookingDetailReport.getPromotionAmount();
					dblCouponAmount=bookingDetailReport.getCouponAmount();
					if(dblPromotionAmount==null){
						jsonOutput += ",\"promotionAmount\":\"" + 0 + "\"";
					}else{
						jsonOutput += ",\"promotionAmount\":\"" + df.format(dblPromotionAmount) + "\"";
					}
					
					if(dblCouponAmount==null){
						jsonOutput += ",\"couponAmount\":\"" + 0 + "\"";
					}else{
						jsonOutput += ",\"couponAmount\":\"" + df.format(dblCouponAmount) + "\"";
					}
					
					String strTotal=df.format(dblTotalBaseAmount);
					
					dblDueAmount=Double.valueOf(strTotal)-dblAdvanceAmount;
					
					if(bookingDetailReport.getAdvanceAmount()!= null){
						double dblTotalDue=dblDueAmount;
						jsonOutput += ",\"dueAmount\":\"" + df.format(dblTotalDue) + "\"";
					}else{
						jsonOutput += ",\"dueAmount\":\"" + 0 + "\"";
					}
					jsonOutput += ",\"diffDays\":\"" + diffDays + "\"";
					jsonOutput += ",\"diffInDays\":\"" + intDiffDays + "\"";
					
					dblBaseAmount=bookingDetailReport.getAmount();
					dblTaxAmount=bookingDetailReport.getTaxAmount();
					
					jsonOutput += ",\"baseAmount\":\"" + df.format(dblBaseAmount) + "\"";
					jsonOutput += ",\"taxAmount\":\"" + df.format(dblTaxAmount) + "\"";
					
					dblBaseTaxAmount=dblBaseAmount+dblTaxAmount;
					dblTotalAmount=dblBaseTaxAmount;
					
					if(diffDays<=1){
						String strRemarks="No Refund";
						jsonOutput += ",\"refundTerms\":\"" + (strRemarks) + "\"";
						jsonOutput += ",\"reducedAmount\":\"" + df.format(dblBaseTaxAmount) + "\"";
						jsonOutput += ",\"refundAmount\":\"" + df.format(dblTotalAmount-dblBaseTaxAmount) + "\"";
					}else if(diffDays>=2){
						Double dblDiscountPer=0.0; 
						Double dblDiscountAmt=0.0;
						dblDiscountAmt=dblTotalAmount-dblDiscountPer/100;
						String strRemarks="No Cancellation Fee";
						jsonOutput += ",\"refundTerms\":\"" + (strRemarks) + "\"";
						jsonOutput += ",\"reducedAmount\":\"" +  dblDiscountAmt + "\"";
						jsonOutput += ",\"refundAmount\":\"" + df.format(dblTotalAmount-dblDiscountAmt) + "\"";
					}
					
					statusId=bookingDetailReport.getStatusId();
					PmsStatus status=statusController.find(statusId);
					statusName=status.getStatus();
					jsonOutput += ",\"statusId\":\"" + statusId + "\"";
					
					sourceId=bookingDetailReport.getSourceId();
					PmsSource source=sourceController.find(sourceId);
					sourceName=source.getSourceName();
					
					jsonOutput += ",\"sourceId\":\"" + sourceId + "\"";
					
					jsonOutput += "}";
				}
			}
			
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}

	public String editCancelBookingDetails(){
		try{
			Integer bookingDetailId,accommId;
			BookingDetailManager bookingDetailController=new BookingDetailManager();
			PmsStatusManager statusController=new PmsStatusManager();
			this.filterBookingId=getFilterBookingId();
			List<Integer> arlAccommId=new ArrayList<Integer>();
			String[] stringArray = getFilterAccommId().split("\\s*,\\s*");
			for (int i = 0; i < stringArray.length; i++) {
				arlAccommId.add(Integer.parseInt(stringArray[i]));
				
			}
			
			List<BookingDetail> listBookingDetails=bookingDetailController.bookingList(Integer.parseInt(this.filterBookingId));
			if(listBookingDetails.size()>0 && !listBookingDetails.isEmpty()){
				for(BookingDetail bookingDetails:listBookingDetails){
					bookingDetailId=bookingDetails.getBookingDetailsId();
					accommId=bookingDetails.getPropertyAccommodation().getAccommodationId();
					if(arlAccommId.contains(accommId)){
						BookingDetail bookingDetail=bookingDetailController.find(bookingDetailId);
//						bookingDetail.setIsActive(false);
//						bookingDetail.setIsDeleted(true);
						bookingDetail.setModifiedBy(this.userId);
						long time = System.currentTimeMillis();
						java.sql.Date date = new java.sql.Date(time);
						bookingDetail.setModifiedDate(new java.sql.Timestamp(date.getTime()));
						PmsStatus status = statusController.find(4);
						bookingDetail.setPmsStatus(status);
						bookingDetailController.edit(bookingDetail);
					}
				}
			}
		}catch(Exception e){
			logger.error(e);
			e.printStackTrace();
		}finally{
			
		}
		return null;
	}
	
	public void cancelBookingDetailsVoucher(){
		try{
			DecimalFormat df = new DecimalFormat("###.##");
			DateFormat f = new SimpleDateFormat("EEEE");
			DateFormat f1 = new SimpleDateFormat("EEE");
			PropertyAccommodationManager propertyAccommodationController=new PropertyAccommodationManager();
			StringBuilder accommodationTypes = new StringBuilder();
			BookingDetailManager bookingDetailController=new BookingDetailManager();
			PmsPropertyManager propertyController=new PmsPropertyManager();
			ArrayList<Integer> listAccommId=new ArrayList<Integer>();
			String strAccommType="";
			SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat sdfformat=new SimpleDateFormat("MMM dd");
		    SimpleDateFormat sdfformat1=new SimpleDateFormat("MMM dd, yyyy");
			String[] stringArray = getFilterAccommId().split("\\s*,\\s*");
			for (int j = 0; j < stringArray.length; j++) {
				listAccommId.add(Integer.parseInt(stringArray[j]));
			}
			
			for (int i = 0; i < listCancelBookingDetails.size(); i++){
				accommodationId=listCancelBookingDetails.get(i).getAccommodationId();
				if(listAccommId.contains(accommodationId)){

					
					accommodationId=listCancelBookingDetails.get(i).getAccommodationId();
					totalBaseAmount+=listCancelBookingDetails.get(i).getBaseAmount();
					totalTaxAmount+=listCancelBookingDetails.get(i).getTaxAmount();
					totalAdvanceAmount=listCancelBookingDetails.get(i).getAdvanceAmount();
//					reducedAmount+=listCancelBookingDetails.get(i).getReducedAmount();
//					refundAmount+=listCancelBookingDetails.get(i).getRefundAmount();
					strAccommType=listCancelBookingDetails.get(i).getAccommodationType()+","+strAccommType;
					
					roomCount+=listCancelBookingDetails.get(i).getRooms();
					adultCount+=listCancelBookingDetails.get(i).getAdultCount();
					childCount+=listCancelBookingDetails.get(i).getChildCount();
					checkIn=listCancelBookingDetails.get(i).getCheckIn();
					checkOut=listCancelBookingDetails.get(i).getCheckOut();
					diffInsDays=listCancelBookingDetails.get(i).getDiffInDays();
					diffDays=listCancelBookingDetails.get(i).getDiffDays();
					bookingId=listCancelBookingDetails.get(i).getBookingId();
					strGuestName=listCancelBookingDetails.get(i).getGuestName();
					strEmailId=listCancelBookingDetails.get(i).getEmailId();
					strMobileNo=listCancelBookingDetails.get(i).getMobileNumber();
					strAccommodationType=strAccommType;
					
					sourceId=listCancelBookingDetails.get(i).getSourceId();
					bookingPropertyId=listCancelBookingDetails.get(i).getPropertyId();
					voucherType=listCancelBookingDetails.get(i).getVoucherType();
					diffInHours=listCancelBookingDetails.get(i).getHours();
					strArrivalDate=listCancelBookingDetails.get(i).getStrArrivalDate();
					strDepartureDate=listCancelBookingDetails.get(i).getStrDepartureDate();
					strCheckInTime=listCancelBookingDetails.get(i).getStrCheckInTime();
					strCheckOutTime=listCancelBookingDetails.get(i).getStrCheckOutTime();
					promotionAmount=listCancelBookingDetails.get(i).getPromotionAmount();
					couponAmount=listCancelBookingDetails.get(i).getCouponAmount();
				
					PropertyAccommodation accommodation = propertyAccommodationController.find(accommodationId);
	            	long rooms = listCancelBookingDetails.get(i).getRooms();
	            	accommodationTypes.append(rooms+" "+accommodation.getAccommodationType().trim());
	            	accommodationTypes.append(",");
				}
			}
			
			java.util.Date date=new java.util.Date();
        	Calendar calDate=Calendar.getInstance();
        	calDate.setTime(date);
        	calDate.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
        	
		    String strCurrentDate = new SimpleDateFormat("yyyy-MM-dd").format(calDate.getTime());
		    java.util.Date currentDate = format1.parse(strCurrentDate);
		    Timestamp currentTS = new Timestamp(currentDate.getTime());
        	int bookedHours=calDate.get(Calendar.HOUR);
        	int bookedMinute=calDate.get(Calendar.MINUTE);
        	int AMPM=calDate.get(Calendar.AM_PM);
        	String strBookingDate=new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH).format(date);
        	String hours="",minutes="";
        	if(bookedHours<10){
        		hours=String.valueOf(bookedHours);
        		hours="0"+hours;
        	}else{
        		hours=String.valueOf(bookedHours);
        	}
        	if(bookedMinute<10){
        		minutes=String.valueOf(bookedMinute);
        		minutes="0"+minutes;
        	}else{
        		minutes=String.valueOf(bookedMinute);
        	}
        	
        	if(AMPM==0){//If the current time is AM
        		this.timeHours=f.format(calDate.getTime())+","+strBookingDate+" at "+hours+" : "+minutes+" AM ";
        	}else if(AMPM==1){//If the current time is PM
        		this.timeHours=f.format(calDate.getTime())+","+strBookingDate+" at "+hours+" : "+minutes+" PM ";
        	}
        	
        	if(this.sourceId==12){
    			this.sourceName = "Offline";
    		}else{
    			this.sourceName = "Online";
    		}
        	
        	SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
        	SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		    java.util.Date dateStart = format.parse(strArrivalDate);
		    java.util.Date dateEnd = format.parse(strDepartureDate);
  			String strFromDate=format2.format(dateStart);
  			String strToDate=format2.format(dateEnd);
  	    	DateTime startdate = DateTime.parse(strFromDate);
  	        DateTime enddate = DateTime.parse(strToDate);
  	        
  	        java.util.Date fromdate = startdate.toDate();
  	 		java.util.Date todate = enddate.toDate();
  	 		 
  			int diffInDays = (int) ((todate.getTime() - fromdate.getTime()) / (1000 * 60 * 60 * 24));
  			
  			
        	if(checkInTime!=null && checkOutTime!=null){
        		java.util.Date dateStartTime = format.parse(strCheckInTime);
    		    java.util.Date dateEndTime = format.parse(strCheckOutTime);
            	this.checkInTime=new Timestamp(dateStartTime.getTime());
            	this.checkOutTime=new Timestamp(dateEndTime.getTime());
    		    java.util.Date timeStart = format.parse(strCheckInTime);
    		    java.util.Date timeEnd = format.parse(strCheckOutTime);
        		Calendar calCheckIn=Calendar.getInstance();
        		calCheckIn.setTime(timeStart);
//        		calCheckIn.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
            	int bookedHours2=calCheckIn.get(Calendar.HOUR);
            	int bookedMinute2=calCheckIn.get(Calendar.MINUTE);
            	int bookedSecond2=calCheckIn.get(Calendar.SECOND);
            	int AMPM2=calCheckIn.get(Calendar.AM_PM);
            	
            	Calendar calCheckOut=Calendar.getInstance();
            	calCheckOut.setTime(timeEnd);
//            	calCheckOut.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
            	int bookedHours3=calCheckOut.get(Calendar.HOUR);
            	int bookedMinute3=calCheckOut.get(Calendar.MINUTE);
            	int bookedSecond3=calCheckOut.get(Calendar.SECOND);
            	int AMPM3=calCheckOut.get(Calendar.AM_PM);
            	
            	String strFromTime=format2.format(timeStart);
	  			String strToTime=format2.format(timeEnd);
	  	    	DateTime starttime = DateTime.parse(strFromTime);
	  	        DateTime endtime = DateTime.parse(strToTime);
	  	        
	  	        java.util.Date fromtime = starttime.toDate();
	  	 		java.util.Date totime = endtime.toDate();
	  	 		
	  	 		if(AMPM2==0){
	  	 			if(bookedHours2<10){
	  	 				if(bookedHours2==0){
	  	 					this.bookingInTime="12:"+"00AM";
	  	 				}else{
	  	 					this.bookingInTime="0"+bookedHours2+":"+"00AM";	
	  	 				}
                		
                	}else{
                		this.bookingInTime=bookedHours2+":"+"00AM";
                	}
	  	 				
            	}else if(AMPM2==1){
            		if(bookedHours2<10){
            			if(bookedHours2==0){
            				this.bookingInTime="12:"+"00PM";	
	  	 				}else{
	  	 					this.bookingInTime="0"+bookedHours2+":"+"00PM";
	  	 				}
            			
                	}else{
                		this.bookingInTime=bookedHours2+":"+"00PM";
                	}
            		
            	}
	  	 		if(AMPM3==0){
	  	 			if(bookedHours3<10){
	  	 				if(bookedHours3==0){
	  	 					this.bookingOutTime="12:"+"00AM";
	  	 				}else{
	  	 					this.bookingOutTime="0"+bookedHours3+":"+"00AM";		
	  	 				}
                		
                	}else{
                		this.bookingOutTime=bookedHours3+":"+"00AM";
                	}
	  	 				
	  	 		}else if(AMPM3==1){
	  	 			if(bookedHours3<10){
	  	 				if(bookedHours3==0){
	  	 					this.bookingOutTime="12:"+"00PM";	
	  	 				}else{
	  	 					this.bookingOutTime="0"+bookedHours3+":"+"00PM";
	  	 				}
                		
                	}else{
                		this.bookingOutTime=bookedHours3+":"+"00PM";
                	}
	  	 			
	  	 		}
	  	 		
	  	 		
	  	 		long lngHours=checkOutTime.getTime()-checkInTime.getTime();
            	long diffHours = lngHours / (60 * 60 * 1000) % 24;
            	
            	this.checkIn=sdfformat.format(fromtime);
            	this.checkInDays=f.format(fromtime);
            	this.checkOut=sdfformat.format(fromtime);
            	this.checkOutDays=f.format(totime);
            	this.diffInHours=(int)diffHours;
            	this.voucherType="hourly-wise";
        	}else{
        		this.bookingInTime="00"+":"+"00";
	  	 		this.bookingOutTime="00"+":"+"00";
        		this.checkIn=sdfformat.format(fromdate);
            	this.checkInDays=f.format(fromdate);
            	this.checkOut=sdfformat.format(todate);
            	this.checkOutDays=f.format(todate);
            	this.voucherType="day-wise";
            	this.diffInHours=0;
        	}
        	cancelledDate=sdfformat1.format(calDate.getTime());
        	this.diffInDays=diffInDays;
        	
        	
        	totalBaseTaxAmount=totalBaseAmount+totalTaxAmount;
        	if(totalBaseTaxAmount == totalAdvanceAmount){
        		this.paymentStatus = "Paid";
        		this.tariffStatus="";
        	}else if(totalAdvanceAmount==0){
        		this.paymentStatus = "Hotel Pay";
        		double totalamt=totalBaseTaxAmount-totalAdvanceAmount;
        		this.tariffStatus= "Hotel Pay ("+String.valueOf(df.format(totalamt))+")";
        	}else{
        		this.paymentStatus = "Partially Paid";
        		double totalamt=totalBaseTaxAmount-totalAdvanceAmount;
        		this.tariffStatus= "Partially Paid ("+String.valueOf(df.format(totalamt))+")";
        	}
        	
        	diffInDays=Integer.parseInt(diffDays);
        	
			
        	if(diffInDays<=1){
				strRemarks="No Refund";
				String strReduced=df.format(totalAdvanceAmount);
				reducedAmount=Double.parseDouble(strReduced);
				refundAmount=totalAdvanceAmount-totalAdvanceAmount;
				String strRefund=df.format(refundAmount);
				refundAmount=Double.parseDouble(strRefund);
			
			}else if(diffInDays>=2){
				Double dblDiscountPer=0.0; 
				Double dblDiscountAmt=0.0;
				strRemarks="No Cancellation Fee";
				dblDiscountAmt=totalBaseTaxAmount*dblDiscountPer/100;
				String strReduced=df.format(dblDiscountAmt);
				reducedAmount=Double.parseDouble(strReduced);
				
				refundAmount=totalBaseTaxAmount-dblDiscountAmt;
				String strRefund=df.format(refundAmount);
				refundAmount=Double.parseDouble(strRefund);
				
			}
        	
        	if(this.roomCount==1){
        		this.roomType=this.roomCount+" room";
        	}
        	if(this.roomCount>1){
        		this.roomType=this.roomCount+" rooms";
        	}
			
        		String strContractMailId=null,strRevenueMailId=null;
				PmsPropertyManager pmsPropertyController=new PmsPropertyManager();
				PmsProperty pmsProperty=pmsPropertyController.find(bookingPropertyId);
				
				this.reservationManagerEmail=pmsProperty.getReservationManagerEmail();
				this.propertyEmail=pmsProperty.getPropertyEmail();
				strRevenueMailId=pmsProperty.getRevenueManagerEmail();
				strContractMailId=pmsProperty.getContractManagerEmail();
				this.propertyName=pmsProperty.getPropertyName();
				this.reservationManagerContact = pmsProperty.getReservationManagerContact();
				this.address1=pmsProperty.getAddress1();
				this.address2=pmsProperty.getAddress2();
				this.propertyId = pmsProperty.getPropertyId();
            	this.latitude = pmsProperty.getLatitude();
            	this.longitude = pmsProperty.getLongitude();
            	this.phoneNumber = pmsProperty.getPropertyContact();//property phone number
            	this.locationName=pmsProperty.getLocation().getLocationName();//property location
            	this.routeMap = pmsProperty.getRouteMap();
            	this.propertyUrl=pmsProperty.getPropertyUrl();
            	
				ArrayList<String> arlEmail=new ArrayList<String>();
				
				StringTokenizer stoken=new StringTokenizer(this.reservationManagerEmail,",");
				while(stoken.hasMoreElements()){
					arlEmail.add(stoken.nextToken());
				}
				
				/*StringTokenizer stoken2=new StringTokenizer(this.propertyEmail,",");
				while(stoken2.hasMoreElements()){
					arlEmail.add(stoken2.nextToken());
				}*/
			
			
				StringTokenizer stoken3=new StringTokenizer(strRevenueMailId,",");
				while(stoken3.hasMoreElements()){
					arlEmail.add(stoken3.nextToken());
				}
				
				StringTokenizer stoken4=new StringTokenizer(strContractMailId,",");
				while(stoken4.hasMoreElements()){
					arlEmail.add(stoken4.nextToken());
				}
				arlEmail.add(getText("bookings.notification.email"));
				arlEmail.add(getText("operations.notification.email"));
				arlEmail.add(getText("finance.notification.email"));
				
				Integer emailSize=arlEmail.size();
				
				//send emails
				Email em = new Email();
				em.set_to(this.strEmailId);
				Iterator<String> iterEmail=arlEmail.iterator();
				while(iterEmail.hasNext()){
					if(emailSize==5){
						em.set_cc(iterEmail.next().trim());
		 				em.set_cc2(iterEmail.next().trim());
		 				em.set_cc3(iterEmail.next().trim());
		 				em.set_cc4(iterEmail.next().trim());
		 				em.set_cc5(iterEmail.next().trim());

					}else if(emailSize==6){
						em.set_cc(iterEmail.next().trim());
		 				em.set_cc2(iterEmail.next().trim());
		 				em.set_cc3(iterEmail.next().trim());
		 				em.set_cc4(iterEmail.next().trim());
		 				em.set_cc5(iterEmail.next().trim());
		 				em.set_cc6(iterEmail.next().trim());
					}else if(emailSize==7){
						em.set_cc(iterEmail.next().trim());
		 				em.set_cc2(iterEmail.next().trim());
		 				em.set_cc3(iterEmail.next().trim());
		 				em.set_cc4(iterEmail.next().trim());
		 				em.set_cc5(iterEmail.next().trim());
		 				em.set_cc6(iterEmail.next().trim());
		 				em.set_cc7(iterEmail.next().trim());
					}else if(emailSize==8){
						em.set_cc(iterEmail.next().trim());
		 				em.set_cc2(iterEmail.next().trim());
		 				em.set_cc3(iterEmail.next().trim());
		 				em.set_cc4(iterEmail.next().trim());
		 				em.set_cc5(iterEmail.next().trim());
		 				em.set_cc6(iterEmail.next().trim());
		 				em.set_cc7(iterEmail.next().trim());
		 				em.set_cc8(iterEmail.next().trim());
					}else if(emailSize==9){
						em.set_cc(iterEmail.next().trim());
		 				em.set_cc2(iterEmail.next().trim());
		 				em.set_cc3(iterEmail.next().trim());
		 				em.set_cc4(iterEmail.next().trim());
		 				em.set_cc5(iterEmail.next().trim());
		 				em.set_cc6(iterEmail.next().trim());
		 				em.set_cc7(iterEmail.next().trim());
		 				em.set_cc8(iterEmail.next().trim());
		 				em.set_cc9(iterEmail.next().trim());
					}else if(emailSize==10){
						em.set_cc(iterEmail.next().trim());
		 				em.set_cc2(iterEmail.next().trim());
		 				em.set_cc3(iterEmail.next().trim());
		 				em.set_cc4(iterEmail.next().trim());
		 				em.set_cc5(iterEmail.next().trim());
		 				em.set_cc6(iterEmail.next().trim());
		 				em.set_cc7(iterEmail.next().trim());
		 				em.set_cc8(iterEmail.next().trim());
		 				em.set_cc9(iterEmail.next().trim());
		 				em.set_cc10(iterEmail.next().trim());
					}else if(emailSize==11){
						em.set_cc(iterEmail.next().trim());
		 				em.set_cc2(iterEmail.next().trim());
		 				em.set_cc3(iterEmail.next().trim());
		 				em.set_cc4(iterEmail.next().trim());
		 				em.set_cc5(iterEmail.next().trim());
		 				em.set_cc6(iterEmail.next().trim());
		 				em.set_cc7(iterEmail.next().trim());
		 				em.set_cc8(iterEmail.next().trim());
		 				em.set_cc9(iterEmail.next().trim());
		 				em.set_cc10(iterEmail.next().trim());
		 				em.set_cc11(iterEmail.next().trim());
					}else if(emailSize==12){
						em.set_cc(iterEmail.next().trim());
		 				em.set_cc2(iterEmail.next().trim());
		 				em.set_cc3(iterEmail.next().trim());
		 				em.set_cc4(iterEmail.next().trim());
		 				em.set_cc5(iterEmail.next().trim());
		 				em.set_cc6(iterEmail.next().trim());
		 				em.set_cc7(iterEmail.next().trim());
		 				em.set_cc8(iterEmail.next().trim());
		 				em.set_cc9(iterEmail.next().trim());
		 				em.set_cc10(iterEmail.next().trim());
		 				em.set_cc11(iterEmail.next().trim());
		 				em.set_cc12(iterEmail.next().trim());
					}
				}
				
				Configuration cfg = new Configuration();
 				cfg.setClassForTemplateLoading(BookingCancelAction.class, "../../../");
 				Template template = null;
 				if(this.voucherType.equalsIgnoreCase("hourly-wise")){
 					template=cfg.getTemplate(getText("guest.booking.cancel.hourly.voucher.template"));	
 				}else{
 					template=cfg.getTemplate(getText("guest.booking.cancel.voucher.template"));
 				}
 				Integer guests=(int)adultCount+(int)childCount;
 				Map<String, Object> rootMap = new HashMap<String, Object>();
 				rootMap.put("voucherDate",this.timeHours);
 				rootMap.put("rooms",roomCount);
 				rootMap.put("adults",adultCount);
 				rootMap.put("child",childCount);
 				rootMap.put("guests",guests);
 				rootMap.put("roomType",this.roomType);
                rootMap.put("checkIn", this.checkIn);
 				rootMap.put("checkInDays", this.checkInDays);
 				rootMap.put("checkOut",this.checkOut);
 				rootMap.put("checkOutDays",this.checkOutDays);
 				rootMap.put("days",this.diffInsDays);
 				rootMap.put("bookingId",bookingId.toString());
 				rootMap.put("guestName",strGuestName);
 				rootMap.put("phoneNumber",this.phoneNumber);
 				rootMap.put("latitude",this.latitude);
 				rootMap.put("longitude",this.longitude);
 				rootMap.put("reservationEmail",this.reservationManagerEmail);
 				rootMap.put("reservationContact",this.reservationManagerContact);
 				rootMap.put("propertyEmail",this.propertyEmail);
 				rootMap.put("address1",this.address1);
 				rootMap.put("address2",this.address2);
 				rootMap.put("propertyUrl",this.propertyUrl);
 				rootMap.put("propertyName",this.propertyName);
 				rootMap.put("paymentStatus",paymentStatus);
 				rootMap.put("tariffStatus", this.tariffStatus);
 				rootMap.put("sourceName",sourceName);
 				rootMap.put("hours",this.diffInHours);
 				rootMap.put("timeHours",this.timeHours);
 				rootMap.put("bookingInTime",this.bookingInTime);
 				rootMap.put("bookingOutTime",this.bookingOutTime);
 				rootMap.put("accommodationType",accommodationTypes);
 				rootMap.put("totalBaseTaxAmount",this.totalAdvanceAmount);
 				rootMap.put("reducedAmount",this.reducedAmount);
 				rootMap.put("refundAmount",this.refundAmount);
 				rootMap.put("remarks",strRemarks);
 				
 				rootMap.put("from", getText("notification.from"));
 				Writer out = new StringWriter();
 				template.process(rootMap, out);
 				
 				
 				/*Writer fileWriter = new FileWriter (new File("E:/cancel-template-input.xhtml"));
 	            template.process(rootMap, fileWriter);
 	            fileWriter.flush();
 	            fileWriter.close();
 	            String inputFile="E:/cancel-template-input.xhtml";
 	            String outputFile="E:/cancel-template-output.html";
 	            File file = new File(inputFile);
 	            FileOutputStream fos = null;
 	            FileInputStream fis = null;
 	            FileWriter outputStream = null;
 	            
 	            fos = new FileOutputStream(outputFile);
 	            fis = new FileInputStream(file);
 	            outputStream = new FileWriter(outputFile);
 	            Tidy tidy = new Tidy(); //HTML parser and pretty printer.
 	            tidy.setXHTML(true); //true if tidy should output XHTML
 	            
 	            tidy.parse(fis, fos);
 				//out.flush();
 				createPdf(out.toString(), getText("storage.pdf.cancel.output.path"));
 				em.set_fileName(getText("storage.pdf.cancel.output.path"));
 				*/
 				
 				String strSubject="Hotel Cancellation for Booking Id "+bookingId.toString()+" "+this.propertyName;
 				em.set_from(getText("email.from"));
 				em.set_username(getText("email.username"));
 				em.set_subject(strSubject);
 				em.set_bodyContent(out);
 				em.set_emailSize(emailSize);
 				em.sendEmailWithoutAttachment();
 				getPartnerCancellationVoucher();
		}catch(Exception e){
			logger.error(e);
			e.printStackTrace();
		}finally{
			
		}
		
	}
	
	public String getPartnerCancellationVoucher(){

		try{
			DecimalFormat df = new DecimalFormat("###.##");
			DateFormat f = new SimpleDateFormat("EEEE");
			DateFormat f1 = new SimpleDateFormat("EEE");
			PropertyAccommodationManager propertyAccommodationController=new PropertyAccommodationManager();
			StringBuilder accommodationTypes = new StringBuilder();
			BookingDetailManager bookingDetailController=new BookingDetailManager();
			PmsPropertyManager propertyController=new PmsPropertyManager();
			ArrayList<Integer> listAccommId=new ArrayList<Integer>();
			String strAccommType="";
			SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat sdfformat=new SimpleDateFormat("MMM dd");
		    SimpleDateFormat sdfformat1=new SimpleDateFormat("MMM dd, yyyy");
			String[] stringArray = getFilterAccommId().split("\\s*,\\s*");
			for (int j = 0; j < stringArray.length; j++) {
				listAccommId.add(Integer.parseInt(stringArray[j]));
			}
			this.totalBaseAmount=0;this.totalTaxAmount=0;this.roomCount=0;this.adultCount=0;this.childCount=0;
			for (int i = 0; i < listCancelBookingDetails.size(); i++){
				accommodationId=listCancelBookingDetails.get(i).getAccommodationId();
				if(listAccommId.contains(accommodationId)){

					
					accommodationId=listCancelBookingDetails.get(i).getAccommodationId();
					totalBaseAmount+=listCancelBookingDetails.get(i).getBaseAmount();
					totalTaxAmount+=listCancelBookingDetails.get(i).getTaxAmount();
					totalAdvanceAmount=listCancelBookingDetails.get(i).getAdvanceAmount();
//					reducedAmount+=listCancelBookingDetails.get(i).getReducedAmount();
//					refundAmount+=listCancelBookingDetails.get(i).getRefundAmount();
					strAccommType=listCancelBookingDetails.get(i).getAccommodationType()+","+strAccommType;
					
					roomCount+=listCancelBookingDetails.get(i).getRooms();
					adultCount+=listCancelBookingDetails.get(i).getAdultCount();
					childCount+=listCancelBookingDetails.get(i).getChildCount();
					checkIn=listCancelBookingDetails.get(i).getCheckIn();
					checkOut=listCancelBookingDetails.get(i).getCheckOut();
					diffInsDays=listCancelBookingDetails.get(i).getDiffInDays();
					diffDays=listCancelBookingDetails.get(i).getDiffDays();
					bookingId=listCancelBookingDetails.get(i).getBookingId();
					strGuestName=listCancelBookingDetails.get(i).getGuestName();
					strEmailId=listCancelBookingDetails.get(i).getEmailId();
					strMobileNo=listCancelBookingDetails.get(i).getMobileNumber();
					strAccommodationType=strAccommType;
					
					sourceId=listCancelBookingDetails.get(i).getSourceId();
					bookingPropertyId=listCancelBookingDetails.get(i).getPropertyId();
					voucherType=listCancelBookingDetails.get(i).getVoucherType();
					diffInHours=listCancelBookingDetails.get(i).getHours();
					strArrivalDate=listCancelBookingDetails.get(i).getStrArrivalDate();
					strDepartureDate=listCancelBookingDetails.get(i).getStrDepartureDate();
					strCheckInTime=listCancelBookingDetails.get(i).getStrCheckInTime();
					strCheckOutTime=listCancelBookingDetails.get(i).getStrCheckOutTime();
					promotionAmount=listCancelBookingDetails.get(i).getPromotionAmount();
					couponAmount=listCancelBookingDetails.get(i).getCouponAmount();
				
					PropertyAccommodation accommodation = propertyAccommodationController.find(accommodationId);
	            	long rooms = listCancelBookingDetails.get(i).getRooms();
	            	accommodationTypes.append(rooms+" "+accommodation.getAccommodationType().trim());
	            	accommodationTypes.append(",");
				}
			}
			
			java.util.Date date=new java.util.Date();
        	Calendar calDate=Calendar.getInstance();
        	calDate.setTime(date);
        	calDate.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
        	
		    String strCurrentDate = new SimpleDateFormat("yyyy-MM-dd").format(calDate.getTime());
		    java.util.Date currentDate = format1.parse(strCurrentDate);
		    Timestamp currentTS = new Timestamp(currentDate.getTime());
        	int bookedHours=calDate.get(Calendar.HOUR);
        	int bookedMinute=calDate.get(Calendar.MINUTE);
        	int AMPM=calDate.get(Calendar.AM_PM);
        	String strBookingDate=new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH).format(date);
        	String hours="",minutes="";
        	if(bookedHours<10){
        		hours=String.valueOf(bookedHours);
        		hours="0"+hours;
        	}else{
        		hours=String.valueOf(bookedHours);
        	}
        	if(bookedMinute<10){
        		minutes=String.valueOf(bookedMinute);
        		minutes="0"+minutes;
        	}else{
        		minutes=String.valueOf(bookedMinute);
        	}
        	
        	if(AMPM==0){//If the current time is AM
        		this.timeHours=f.format(calDate.getTime())+","+strBookingDate+" at "+hours+" : "+minutes+" AM ";
        	}else if(AMPM==1){//If the current time is PM
        		this.timeHours=f.format(calDate.getTime())+","+strBookingDate+" at "+hours+" : "+minutes+" PM ";
        	}
        	
        	if(this.sourceId==12){
    			this.sourceName = "Offline";
    		}else{
    			this.sourceName = "Online";
    		}
        	
        	SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
        	SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		    java.util.Date dateStart = format.parse(strArrivalDate);
		    java.util.Date dateEnd = format.parse(strDepartureDate);
  			String strFromDate=format2.format(dateStart);
  			String strToDate=format2.format(dateEnd);
  	    	DateTime startdate = DateTime.parse(strFromDate);
  	        DateTime enddate = DateTime.parse(strToDate);
  	        
  	        java.util.Date fromdate = startdate.toDate();
  	 		java.util.Date todate = enddate.toDate();
  	 		 
  			int diffInDays = (int) ((todate.getTime() - fromdate.getTime()) / (1000 * 60 * 60 * 24));
  			
  			
        	if(checkInTime!=null && checkOutTime!=null){
        		java.util.Date dateStartTime = format.parse(strCheckInTime);
    		    java.util.Date dateEndTime = format.parse(strCheckOutTime);
            	this.checkInTime=new Timestamp(dateStartTime.getTime());
            	this.checkOutTime=new Timestamp(dateEndTime.getTime());
    		    java.util.Date timeStart = format.parse(strCheckInTime);
    		    java.util.Date timeEnd = format.parse(strCheckOutTime);
        		Calendar calCheckIn=Calendar.getInstance();
        		calCheckIn.setTime(timeStart);
//        		calCheckIn.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
            	int bookedHours2=calCheckIn.get(Calendar.HOUR);
            	int bookedMinute2=calCheckIn.get(Calendar.MINUTE);
            	int bookedSecond2=calCheckIn.get(Calendar.SECOND);
            	int AMPM2=calCheckIn.get(Calendar.AM_PM);
            	
            	Calendar calCheckOut=Calendar.getInstance();
            	calCheckOut.setTime(timeEnd);
//            	calCheckOut.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
            	int bookedHours3=calCheckOut.get(Calendar.HOUR);
            	int bookedMinute3=calCheckOut.get(Calendar.MINUTE);
            	int bookedSecond3=calCheckOut.get(Calendar.SECOND);
            	int AMPM3=calCheckOut.get(Calendar.AM_PM);
            	
            	String strFromTime=format2.format(timeStart);
	  			String strToTime=format2.format(timeEnd);
	  	    	DateTime starttime = DateTime.parse(strFromTime);
	  	        DateTime endtime = DateTime.parse(strToTime);
	  	        
	  	        java.util.Date fromtime = starttime.toDate();
	  	 		java.util.Date totime = endtime.toDate();
	  	 		
	  	 		if(AMPM2==0){
	  	 			if(bookedHours2<10){
	  	 				if(bookedHours2==0){
	  	 					this.bookingInTime="12:"+"00AM";
	  	 				}else{
	  	 					this.bookingInTime="0"+bookedHours2+":"+"00AM";	
	  	 				}
                		
                	}else{
                		this.bookingInTime=bookedHours2+":"+"00AM";
                	}
	  	 				
            	}else if(AMPM2==1){
            		if(bookedHours2<10){
            			if(bookedHours2==0){
            				this.bookingInTime="12:"+"00PM";	
	  	 				}else{
	  	 					this.bookingInTime="0"+bookedHours2+":"+"00PM";
	  	 				}
            			
                	}else{
                		this.bookingInTime=bookedHours2+":"+"00PM";
                	}
            		
            	}
	  	 		if(AMPM3==0){
	  	 			if(bookedHours3<10){
	  	 				if(bookedHours3==0){
	  	 					this.bookingOutTime="12:"+"00AM";
	  	 				}else{
	  	 					this.bookingOutTime="0"+bookedHours3+":"+"00AM";		
	  	 				}
                		
                	}else{
                		this.bookingOutTime=bookedHours3+":"+"00AM";
                	}
	  	 				
	  	 		}else if(AMPM3==1){
	  	 			if(bookedHours3<10){
	  	 				if(bookedHours3==0){
	  	 					this.bookingOutTime="12:"+"00PM";	
	  	 				}else{
	  	 					this.bookingOutTime="0"+bookedHours3+":"+"00PM";
	  	 				}
                		
                	}else{
                		this.bookingOutTime=bookedHours3+":"+"00PM";
                	}
	  	 			
	  	 		}
	  	 		
	  	 		
	  	 		long lngHours=checkOutTime.getTime()-checkInTime.getTime();
            	long diffHours = lngHours / (60 * 60 * 1000) % 24;
            	
            	this.checkIn=sdfformat.format(fromtime);
            	this.checkInDays=f.format(fromtime);
            	this.checkOut=sdfformat.format(fromtime);
            	this.checkOutDays=f.format(totime);
            	this.diffInHours=(int)diffHours;
            	this.voucherType="hourly-wise";
        	}else{
        		this.bookingInTime="00"+":"+"00";
	  	 		this.bookingOutTime="00"+":"+"00";
        		this.checkIn=sdfformat.format(fromdate);
            	this.checkInDays=f.format(fromdate);
            	this.checkOut=sdfformat.format(todate);
            	this.checkOutDays=f.format(todate);
            	this.voucherType="day-wise";
            	this.diffInHours=0;
        	}
        	cancelledDate=sdfformat1.format(calDate.getTime());
        	this.diffInDays=diffInDays;
        	
        	
        	totalBaseTaxAmount=totalBaseAmount+totalTaxAmount;
        	if(totalBaseTaxAmount == totalAdvanceAmount){
        		this.paymentStatus = "Paid";
        		this.tariffStatus="";
        	}else if(totalAdvanceAmount==0){
        		this.paymentStatus = "Hotel Pay";
        		double totalamt=totalBaseTaxAmount-totalAdvanceAmount;
        		this.tariffStatus= "Hotel Pay ("+String.valueOf(df.format(totalamt))+")";
        	}else{
        		this.paymentStatus = "Partially Paid";
        		double totalamt=totalBaseTaxAmount-totalAdvanceAmount;
        		this.tariffStatus= "Partially Paid ("+String.valueOf(df.format(totalamt))+")";
        	}
        	
        	diffInDays=Integer.parseInt(diffDays);
        	
			
        	if(diffInDays<=1){
				strRemarks="No Refund";
				String strReduced=df.format(totalAdvanceAmount);
				reducedAmount=Double.parseDouble(strReduced);
				refundAmount=totalAdvanceAmount-totalAdvanceAmount;
				String strRefund=df.format(refundAmount);
				refundAmount=Double.parseDouble(strRefund);
			
			}else if(diffInDays>=2){
				Double dblDiscountPer=0.0; 
				Double dblDiscountAmt=0.0;
				strRemarks="No Cancellation Fee";
				dblDiscountAmt=totalBaseTaxAmount*dblDiscountPer/100;
				String strReduced=df.format(dblDiscountAmt);
				reducedAmount=Double.parseDouble(strReduced);
				
				refundAmount=totalBaseTaxAmount-dblDiscountAmt;
				String strRefund=df.format(refundAmount);
				refundAmount=Double.parseDouble(strRefund);
				
			}
        	
        	if(this.roomCount==1){
        		this.roomType=this.roomCount+" room";
        	}
        	if(this.roomCount>1){
        		this.roomType=this.roomCount+" rooms";
        	}
			
        		String strContractMailId=null,strRevenueMailId=null;
				PmsPropertyManager pmsPropertyController=new PmsPropertyManager();
				PmsProperty pmsProperty=pmsPropertyController.find(bookingPropertyId);
				
				this.reservationManagerEmail=pmsProperty.getReservationManagerEmail();
				this.propertyEmail=pmsProperty.getPropertyEmail();
				strRevenueMailId=pmsProperty.getRevenueManagerEmail();
				strContractMailId=pmsProperty.getContractManagerEmail();
				this.propertyName=pmsProperty.getPropertyName();
				this.reservationManagerContact = pmsProperty.getReservationManagerContact();
				this.address1=pmsProperty.getAddress1();
				this.address2=pmsProperty.getAddress2();
				this.propertyId = pmsProperty.getPropertyId();
            	this.latitude = pmsProperty.getLatitude();
            	this.longitude = pmsProperty.getLongitude();
            	this.phoneNumber = pmsProperty.getPropertyContact();//property phone number
            	this.locationName=pmsProperty.getLocation().getLocationName();//property location
            	this.routeMap = pmsProperty.getRouteMap();
            	this.propertyUrl=pmsProperty.getPropertyUrl();
            	
				ArrayList<String> arlEmail=new ArrayList<String>();
				
				/*StringTokenizer stoken=new StringTokenizer(this.reservationManagerEmail,",");
				while(stoken.hasMoreElements()){
					arlEmail.add(stoken.nextToken());
				}*/
				
				StringTokenizer stoken2=new StringTokenizer(this.propertyEmail,",");
				while(stoken2.hasMoreElements()){
					arlEmail.add(stoken2.nextToken());
				}
			
			
				/*StringTokenizer stoken3=new StringTokenizer(strRevenueMailId,",");
				while(stoken3.hasMoreElements()){
					arlEmail.add(stoken3.nextToken());
				}*/
				
				StringTokenizer stoken4=new StringTokenizer(strContractMailId,",");
				while(stoken4.hasMoreElements()){
					arlEmail.add(stoken4.nextToken());
				}
				arlEmail.add(getText("bookings.notification.email"));
				arlEmail.add(getText("operations.notification.email"));
				arlEmail.add(getText("finance.notification.email"));
				
				Integer emailSize=arlEmail.size();
				
				//send emails
				Email em = new Email();
				
				Iterator<String> iterEmail=arlEmail.iterator();
				while(iterEmail.hasNext()){
					if(emailSize==4){
						em.set_to(iterEmail.next().trim());
						em.set_cc(iterEmail.next().trim());
		 				em.set_cc2(iterEmail.next().trim());
		 				em.set_cc3(iterEmail.next().trim());
		 				emailSize=3;
					}else if(emailSize==5){
						em.set_to(iterEmail.next().trim());
						em.set_cc(iterEmail.next().trim());
		 				em.set_cc2(iterEmail.next().trim());
		 				em.set_cc3(iterEmail.next().trim());
		 				em.set_cc4(iterEmail.next().trim());
		 				emailSize=4;
					}else if(emailSize==6){
						em.set_to(iterEmail.next().trim());
						em.set_cc(iterEmail.next().trim());
		 				em.set_cc2(iterEmail.next().trim());
		 				em.set_cc3(iterEmail.next().trim());
		 				em.set_cc4(iterEmail.next().trim());
		 				em.set_cc5(iterEmail.next().trim());
		 				emailSize=5;
					}else if(emailSize==7){
						em.set_to(iterEmail.next().trim());
						em.set_cc(iterEmail.next().trim());
		 				em.set_cc2(iterEmail.next().trim());
		 				em.set_cc3(iterEmail.next().trim());
		 				em.set_cc4(iterEmail.next().trim());
		 				em.set_cc5(iterEmail.next().trim());
		 				em.set_cc6(iterEmail.next().trim());
		 				emailSize=6;
					}else if(emailSize==8){
						em.set_to(iterEmail.next().trim());
						em.set_cc(iterEmail.next().trim());
		 				em.set_cc2(iterEmail.next().trim());
		 				em.set_cc3(iterEmail.next().trim());
		 				em.set_cc4(iterEmail.next().trim());
		 				em.set_cc5(iterEmail.next().trim());
		 				em.set_cc6(iterEmail.next().trim());
		 				em.set_cc7(iterEmail.next().trim());
		 				emailSize=7;
					}else if(emailSize==9){
						em.set_to(iterEmail.next().trim());
						em.set_cc(iterEmail.next().trim());
		 				em.set_cc2(iterEmail.next().trim());
		 				em.set_cc3(iterEmail.next().trim());
		 				em.set_cc4(iterEmail.next().trim());
		 				em.set_cc5(iterEmail.next().trim());
		 				em.set_cc6(iterEmail.next().trim());
		 				em.set_cc7(iterEmail.next().trim());
		 				em.set_cc8(iterEmail.next().trim());
		 				emailSize=8;
					}else if(emailSize==10){
						em.set_to(iterEmail.next().trim());
						em.set_cc(iterEmail.next().trim());
		 				em.set_cc2(iterEmail.next().trim());
		 				em.set_cc3(iterEmail.next().trim());
		 				em.set_cc4(iterEmail.next().trim());
		 				em.set_cc5(iterEmail.next().trim());
		 				em.set_cc6(iterEmail.next().trim());
		 				em.set_cc7(iterEmail.next().trim());
		 				em.set_cc8(iterEmail.next().trim());
		 				em.set_cc9(iterEmail.next().trim());
		 				emailSize=9;
					}else if(emailSize==11){
						em.set_to(iterEmail.next().trim());
						em.set_cc(iterEmail.next().trim());
		 				em.set_cc2(iterEmail.next().trim());
		 				em.set_cc3(iterEmail.next().trim());
		 				em.set_cc4(iterEmail.next().trim());
		 				em.set_cc5(iterEmail.next().trim());
		 				em.set_cc6(iterEmail.next().trim());
		 				em.set_cc7(iterEmail.next().trim());
		 				em.set_cc8(iterEmail.next().trim());
		 				em.set_cc9(iterEmail.next().trim());
		 				em.set_cc10(iterEmail.next().trim());
		 				emailSize=10;
					}else if(emailSize==12){
						em.set_to(iterEmail.next().trim());
						em.set_cc(iterEmail.next().trim());
		 				em.set_cc2(iterEmail.next().trim());
		 				em.set_cc3(iterEmail.next().trim());
		 				em.set_cc4(iterEmail.next().trim());
		 				em.set_cc5(iterEmail.next().trim());
		 				em.set_cc6(iterEmail.next().trim());
		 				em.set_cc7(iterEmail.next().trim());
		 				em.set_cc8(iterEmail.next().trim());
		 				em.set_cc9(iterEmail.next().trim());
		 				em.set_cc10(iterEmail.next().trim());
		 				em.set_cc11(iterEmail.next().trim());
		 				emailSize=11;
					}
				}
				
				Configuration cfg = new Configuration();
 				cfg.setClassForTemplateLoading(BookingCancelAction.class, "../../../");
 				Template template = null;
 				if(this.voucherType.equalsIgnoreCase("hourly-wise")){
 					template=cfg.getTemplate(getText("partner.booking.cancel.hourly.voucher.template"));	
 				}else{
 					template=cfg.getTemplate(getText("partner.booking.cancel.voucher.template"));
 				}
 				Integer guests=(int)adultCount+(int)childCount;
 				Map<String, Object> rootMap = new HashMap<String, Object>();
 				rootMap.put("voucherDate",this.timeHours);
 				rootMap.put("rooms",roomCount);
 				rootMap.put("adults",adultCount);
 				rootMap.put("child",childCount);
 				rootMap.put("guests",guests);
 				rootMap.put("roomType",this.roomType);
                rootMap.put("checkIn", this.checkIn);
 				rootMap.put("checkInDays", this.checkInDays);
 				rootMap.put("checkOut",this.checkOut);
 				rootMap.put("checkOutDays",this.checkOutDays);
 				rootMap.put("days",this.diffInsDays);
 				rootMap.put("bookingId",bookingId.toString());
 				rootMap.put("guestName",strGuestName);
 				rootMap.put("phoneNumber",this.phoneNumber);
 				rootMap.put("latitude",this.latitude);
 				rootMap.put("longitude",this.longitude);
 				rootMap.put("reservationEmail",this.reservationManagerEmail);
 				rootMap.put("reservationContact",this.reservationManagerContact);
 				rootMap.put("propertyEmail",this.propertyEmail);
 				rootMap.put("address1",this.address1);
 				rootMap.put("address2",this.address2);
 				rootMap.put("propertyUrl",this.propertyUrl);
 				rootMap.put("propertyName",this.propertyName);
 				rootMap.put("paymentStatus",paymentStatus);
 				rootMap.put("tariffStatus", this.tariffStatus);
 				rootMap.put("sourceName",sourceName);
 				rootMap.put("hours",this.diffInHours);
 				rootMap.put("timeHours",this.timeHours);
 				rootMap.put("bookingInTime",this.bookingInTime);
 				rootMap.put("bookingOutTime",this.bookingOutTime);
 				rootMap.put("accommodationType",accommodationTypes);
 				rootMap.put("totalBaseTaxAmount",this.totalAdvanceAmount);
 				rootMap.put("reducedAmount",this.reducedAmount);
 				rootMap.put("refundAmount",this.refundAmount);
 				rootMap.put("remarks",strRemarks);
 				
 				rootMap.put("from", getText("notification.from"));
 				Writer out = new StringWriter();
 				template.process(rootMap, out);
 				
 				
 				/*Writer fileWriter = new FileWriter (new File("E:/cancel-template-input.xhtml"));
 	            template.process(rootMap, fileWriter);
 	            fileWriter.flush();
 	            fileWriter.close();
 	            String inputFile="E:/cancel-template-input.xhtml";
 	            String outputFile="E:/cancel-template-output.html";
 	            File file = new File(inputFile);
 	            FileOutputStream fos = null;
 	            FileInputStream fis = null;
 	            FileWriter outputStream = null;
 	            
 	            fos = new FileOutputStream(outputFile);
 	            fis = new FileInputStream(file);
 	            outputStream = new FileWriter(outputFile);
 	            Tidy tidy = new Tidy(); //HTML parser and pretty printer.
 	            tidy.setXHTML(true); //true if tidy should output XHTML
 	            
 	            tidy.parse(fis, fos);
 				//out.flush();
 				createPdf(out.toString(), getText("storage.pdf.cancel.output.path"));
 				em.set_fileName(getText("storage.pdf.cancel.output.path"));
 				*/
 				
 				String strSubject="Hotel Cancellation for Booking Id "+bookingId.toString()+" "+this.propertyName;
 				em.set_from(getText("email.from"));
 				em.set_username(getText("email.username"));
 				em.set_subject(strSubject);
 				em.set_bodyContent(out);
 				em.set_emailSize(emailSize);
 				em.sendEmailWithoutAttachment();
			
		}catch(Exception e){
			logger.error(e);
			e.printStackTrace();
		}finally{
			
		}
		return null;
	
	}
	
	public String cancelledRequestVoucher(Booking booking){
		String output="",jsonOutput="";
		try{
			boolean status=false;
			int count=0,guestId=0;
			String strReservationMailId3=null,strReservationMailId1=null,strReservationMailId2=null;
			BookingGuestDetailManager guestDetailController=new BookingGuestDetailManager();
			PmsGuestManager guestController=new PmsGuestManager();
			PmsStatusManager statusController=new PmsStatusManager();
			PmsBookingManager bookingController=new PmsBookingManager();
			BookingDetailManager bookingDetailController=new BookingDetailManager();
			PmsPropertyManager propertyController=new PmsPropertyManager();
			SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
			this.bookingId=booking.getBookingId();
			PmsBooking bookings=bookingController.find(this.bookingId);
			if(bookings!=null){
				this.propertyId=bookings.getPmsProperty().getPropertyId();
				PmsStatus pmsstatus=statusController.find(6);
				bookings.setPmsStatus(pmsstatus);
				bookingController.edit(bookings);
				
				
				List<BookingDetail> listBookingDetails=bookingDetailController.findId(this.bookingId);
				if(listBookingDetails.size()>0 && !listBookingDetails.isEmpty()){
					for(BookingDetail bookingdetails:listBookingDetails){
						BookingDetail details=bookingDetailController.find(bookingdetails.getBookingDetailsId());
						PmsStatus detailstatus=statusController.find(6);
						details.setPmsStatus(detailstatus);
						bookingDetailController.edit(details);
					}
				}
				
				PmsProperty pmsProperty=propertyController.find(this.propertyId);
				this.propertyName=pmsProperty.getPropertyName();
				List<BookingGuestDetail> listGuest=guestDetailController.findSmsGuestId(this.bookingId);
				if(listGuest.size()>0 && !listGuest.isEmpty()){
					for(BookingGuestDetail details:listGuest){
						BookingGuestDetail guestDetails=guestDetailController.find(details.getBookingGuestId());
						guestId=guestDetails.getPmsGuest().getGuestId();
					}
					
				}
				PmsGuest guest=guestController.find(guestId);
				this.strGuestName=guest.getFirstName();
				this.strEmailId=guest.getEmailId();
				this.strMobileNo = guest.getPhone();
				count++;
				status=true;
			}else{
				count=0;
				status=false;
			}
			
			java.util.Date date=new java.util.Date();
        	Calendar calDate=Calendar.getInstance();
        	calDate.setTime(date);
        	calDate.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
        	
		    String strCurrentDate = new SimpleDateFormat("yyyy-MM-dd").format(calDate.getTime());
		    java.util.Date currentDate = format1.parse(strCurrentDate);
		    Timestamp currentTS = new Timestamp(currentDate.getTime());
        	int bookedHours=calDate.get(Calendar.HOUR);
        	int bookedMinute=calDate.get(Calendar.MINUTE);
        	int AMPM=calDate.get(Calendar.AM_PM);
        	String strBookingDate=new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH).format(date);
        	String hours="",minutes="";
        	if(bookedHours<10){
        		hours=String.valueOf(bookedHours);
        		hours="0"+hours;
        	}else{
        		hours=String.valueOf(bookedHours);
        	}
        	if(bookedMinute<10){
        		minutes=String.valueOf(bookedMinute);
        		minutes="0"+minutes;
        	}else{
        		minutes=String.valueOf(bookedMinute);
        	}
        	
        	if(AMPM==0){//If the current time is AM
        		this.timeHours=strBookingDate+" "+hours+" : "+minutes+" AM ";
        	}else if(AMPM==1){//If the current time is PM
        		this.timeHours=strBookingDate+" "+hours+" : "+minutes+" PM ";
        	}
			
			strReservationMailId1="sivabalan@ulohotels.com";		//bookings@ulohotels.com			
			strReservationMailId2="sivabalan@ulohotels.com";//<reservations@ulohotels.com>
			strReservationMailId3="dwarakesh@ulohotels.com";//Booking Vouchers <bookingvouchers@ulohotels.com>
			
			
			this.mailFlag=true;
			
			if(this.mailFlag == true)
            {

 				//email template
 				Configuration cfg = new Configuration();
 				cfg.setClassForTemplateLoading(BookingCancelAction.class, "../../../");
 				Template template = cfg.getTemplate(getText("cancelled.request.voucher.template"));
 				Map<String, Object> rootMap = new HashMap<String, Object>();
 				rootMap.put("voucherDate",this.timeHours);
 				rootMap.put("bookingId",this.bookingId.toString());
 				rootMap.put("guestName",this.strGuestName);
 				rootMap.put("emailId",this.strEmailId);
 				rootMap.put("mobile",this.strMobileNo);
 				rootMap.put("propertyName",this.propertyName);
 				rootMap.put("remarks","Cancellation Request");
 				rootMap.put("from", getText("notification.from"));
 				Writer out = new StringWriter();
 				template.process(rootMap, out);
 				out.flush();
 				//createPdf(out.toString(), DEST);
 				
 				
 				String strSubject="Hotel Cancellation Request for Booking Id "+bookingId.toString()+" "+this.propertyName;

 				Email em = new Email();
 				em.set_to(strReservationMailId1);
 				em.set_cc(strReservationMailId2);
 				em.set_cc2(getText("bookings.notification.email"));
 				em.set_cc3(getText("operations.notification.email"));
 				em.set_cc4(getText("finance.notification.email"));
 				em.set_cc5(strReservationMailId3);
 				em.set_cc6(strReservationMailId1);
 				em.set_from(getText("email.from"));
 				em.set_username(getText("email.username"));
 				em.set_subject(strSubject);
 				em.set_bodyContent(out);
 				//em.set_filename(DEST);
 				
 				em.sendCanceledRequest();
 				cancelledRequestforGuest(this.bookingId.toString(),this.strEmailId,this.strGuestName,this.timeHours,this.propertyName);
            }
			if (!jsonOutput.equalsIgnoreCase(""))
				jsonOutput += ",{";
			else
				jsonOutput += "{";
			
			
				jsonOutput += "\"bookingId\":\"" + this.bookingId+ "\"";
				jsonOutput += ",\"status\":\"" + status+ "\"";
				
			jsonOutput += "}";
			
			String data="\"data\":[" + jsonOutput + "]";
 			String strData=data.replaceAll("\"", "\"");
 			
 			
 			if(count == 0){
 				output="{\"error\":\"1\",\"status\":\""+status+"\"}";
			}else{
				output="{\"error\":\"0\"," + strData + "}";
			}
 			
		}catch(Exception e){
			output="{\"error\":\"2\",\"message\":\"Some technical error on canceled request api\"}";
			logger.error(e);
			e.printStackTrace();
		}finally{
			
		}
		return output;
	
	}
	
	public void cancelledRequestforGuest(String bookingId,String emailId,String guestName,String canceledTime,String propertyName){

		try{
				//email template
				Configuration cfg = new Configuration();
				cfg.setClassForTemplateLoading(BookingCancelAction.class, "../../../");
				Template template = cfg.getTemplate(getText("cancelled.request.guest.voucher.template"));
				Map<String, Object> rootMap = new HashMap<String, Object>();
				rootMap.put("voucherDate",canceledTime);
				rootMap.put("bookingId",bookingId);
				rootMap.put("guestName",guestName);
				rootMap.put("propertyName",this.propertyName);
				rootMap.put("remarks","Cancellation Request");
				rootMap.put("from", getText("notification.from"));
				Writer out = new StringWriter();
				template.process(rootMap, out);
				out.flush();
				//createPdf(out.toString(), DEST);
								
				String strSubject="Hotel Cancellation Request for Booking Id "+bookingId+" "+propertyName;

				Email em = new Email();
				em.set_to(emailId);
				em.set_from(getText("email.from"));
				em.set_username(getText("email.username"));
				em.set_subject(strSubject);
				em.set_bodyContent(out);
				
				em.send();
				
        
 			
		}catch(Exception e){
			logger.error(e);
			e.printStackTrace();
		}finally{
			
		}
	
	}
	
	private void createPdf(String content, String dest)
			throws IOException, DocumentException, com.lowagie.text.DocumentException {
		try{
			FileOutputStream fileout=new FileOutputStream(new File(dest));   
			
			//String k = "<html><body> This is my Project </body></html>";
		    Document document = new Document();
		    PdfWriter.getInstance(document, fileout);
		    document.open();
		    HTMLWorker htmlWorker = new HTMLWorker(document);
		    htmlWorker.parse(new StringReader(content));
		    document.close();
		    fileout.close();
		    
			/*ITextRenderer render = new ITextRenderer();
			String url = new File("D:\\voucher-template-output.xhtml").toURI().toURL().toString();
			
			render.setDocument(url);
			render.layout();
			render.createPDF(fileout,true);
			fileout.flush();
			fileout.close();*/
			
		}catch(Exception e){
			logger.error(e);
			e.printStackTrace();
		}
	}
	
    public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Integer getAccommodationId() {
		return accommodationId;
	}

	public void setAccommodationId(int accommodationId) {
		this.accommodationId = accommodationId;
	}
	
	
	
	public Integer getBookingId() {
		return bookingId;
	}

	public void setBookingId(Integer bookingId) {
		this.bookingId = bookingId;
	}

	public Integer getStatusId() {
		return statusId;
	}

	public void setStatusId(Integer statusId) {
		this.statusId = statusId;
	}
	
	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	
	public String getFilterDateName() {
		return filterDateName;
	}

	public void setFilterDateName(String filterDateName) {
		this.filterDateName = filterDateName;
	}

	public String getFilterBookingId() {
		return filterBookingId;
	}

	public void setFilterBookingId(String filterBookingId) {
		this.filterBookingId = filterBookingId;
	}
	
	public String getFilterAccommId() {
		return filterAccommId;
	}

	public void setFilterAccommId(String filterAccommId) {
		this.filterAccommId = filterAccommId;
	}
	
	public List<BookingDetailReport> getListCancelBookingDetails() {
		return listCancelBookingDetails;
	}
	
	public void setListCancelBookingDetails(
			List<BookingDetailReport> listCancelBookingDetails) {
		this.listCancelBookingDetails = listCancelBookingDetails;
	}
	
	public boolean isMailFlag() {
		return mailFlag;
	}

	public void setMailFlag(boolean mailFlag) {
		this.mailFlag = mailFlag;
	}
	
}
