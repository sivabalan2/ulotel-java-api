package com.ulopms.view;

import java.io.IOException;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.catalina.startup.UserConfig;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;

import com.opensymphony.xwork2.ActionSupport;

import com.ulopms.controller.HelpTopicManager;
import com.ulopms.controller.UserLoginManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.HelpTopic;
import com.ulopms.model.User;

import javax.servlet.http.HttpServletRequest;

public class BookingReportsAction extends ActionSupport implements ServletRequestAware,
UserAware {

	private static final long serialVersionUID = 9149826260758390091L;

	private User user;
	
	
	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}

	private HttpSession session;

	private static final Logger logger = Logger.getLogger(BookingReportsAction.class);

	// @Override
	// public User getModel() {
	// return user;
	// }

	@Override
	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	private HttpServletRequest request;

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	public BookingReportsAction() {

	}



	public String execute() {
		
		return SUCCESS;
	}

}
