package com.ulopms.view;

import java.io.IOException;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;
import org.joda.time.DateTime;
import org.json.JSONObject;
import org.jsoup.select.Evaluator.IsEmpty;

import sun.invoke.empty.Empty;

import com.lowagie.text.pdf.codec.Base64.InputStream;
import com.opensymphony.xwork2.ActionSupport;
import com.razorpay.Payment;
import com.razorpay.RazorpayClient;
import com.sun.org.apache.bcel.internal.generic.GETSTATIC;
import com.ulopms.controller.AccessRightManager;
import com.ulopms.controller.BookingDetailManager;
import com.ulopms.controller.BookingGuestDetailManager;
import com.ulopms.controller.PmsBookingManager;
import com.ulopms.controller.PmsGuestManager;
import com.ulopms.controller.PmsPropertyManager;
import com.ulopms.controller.PmsSourceManager;
import com.ulopms.controller.PmsStatusManager;
import com.ulopms.controller.PropertyAccommodationManager;
import com.ulopms.controller.PropertyDiscountManager;
import com.ulopms.controller.ReportManager;
import com.ulopms.controller.RoleAccessRightManager;
import com.ulopms.controller.RoleManager;
import com.ulopms.controller.UserLoginManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.AccessRight;
import com.ulopms.model.BookingDetailReport;
import com.ulopms.model.PmsBooking;
import com.ulopms.model.PmsProperty;
import com.ulopms.model.PmsSource;
import com.ulopms.model.PmsStatus;
import com.ulopms.model.PropertyAccommodation;
import com.ulopms.model.Role;
import com.ulopms.model.RoleAccessRight;
import com.ulopms.model.User;
import com.ulopms.model.UserGuestDetailReport;
import com.ulopms.util.DateUtil;

public class ReportAction extends ActionSupport implements
ServletRequestAware, SessionAware, UserAware{

	private static final long serialVersionUID = 914982665758390091L;
	
	private static final String[] reportNameTitle= {"PARTNER BOOKING REPORTS","REVENUE REPORTS","USER REPORTS",
		"CANCELLATION REPORTS","ULO BOOKING REPORTS","WALKIN BOOKING REPORTS"};

	private static final String[] admin={"PARTNER BOOKING REPORTS","REVENUE REPORTS","USER REPORTS","CANCELLATION REPORTS",
		"ULO BOOKING REPORTS","WALKIN BOOKING REPORTS"};
	 
	private static final String[] revenue={"PARTNER BOOKING REPORTS","REVENUE REPORTS","CANCELLATION REPORTS",
		"ULO BOOKING REPORTS","WALKIN BOOKING REPORTS"};
	
	private static final String[] reservation={"REVENUE REPORTS","ULO BOOKING REPORTS","CANCELLATION REPORTS"};
	
	private static final String[] seo={"PARTNER BOOKING REPORTS","REVENUE REPORTS","USER REPORTS","CANCELLATION REPORTS",
		"ULO BOOKING REPORTS","WALKIN BOOKING REPORTS"};
	
	private static final String[] finance={"PARTNER BOOKING REPORTS","REVENUE REPORTS","CANCELLATION REPORTS",
		"ULO BOOKING REPORTS","WALKIN BOOKING REPORTS"};
	
	private static final String[] operation={"ULO BOOKING REPORTS"};
	
	private static final String[] corporate={};
	
	private static final String[] digital={"ULO BOOKING REPORTS"};
	
	private static final String[] bookingReportColumns={"Booking Id","Booking Date","Guest Name","Check-In","Check-Out","Accommodations","Rooms","Nights","Room Nights",
		"Adult","Child","Revenue Amount","Tax Amount","Total Revenue","Ulo Commission","Ulo Tax","Payable to Ulo","Hotel Revenue","Tax Amount","Hotel Pay","Payable to Hotel","Status"};
	
	private static final String[] revenueReportColumns={"Property Name","Location Name","Room Nights","Total Revenue"};
	
	private static final String[] userReportColumns={"Guest Name","Email Id","Mobile Number"};
	
	private static final String[] cancellationReportColumns={"Booking Id","Booking Date","Cancellation Date","Guest Name","Mobile Number","Email Id","Check-In","Check-Out",
		"Accommodations","Rooms","Nights","Room Nights","Tariff Amount",
		"Tax Amount","Total Revenue","Cancellation Charges","Cancellation Amount","Remarks","User Name"};
	
	private static final String[] uloBookingReportColumns={"Booking Id","Booking Date","OTA Booking Id","Guest Name","Mobile Number","Email Id","Check-In","Check-Out","Accommodations","Rooms","Nights",
		"Room Nights","Adult","Child","Tariff","Tax","Total Amount","Advance","Hotel Pay","OTA Commission","OTA Tax","OTA Revenue","Revenue","Tax","Total Revenue","Net Rate Revenue",
		"Ulo Commission","Ulo Tax","Ulo Payable","Net Hotel Payable","Hotel Payable","Addon Amount","Addon Remarks","Remarks","Status","Source","User Name"};
	
	private static final String[] hotelBookingReportColumns={"Booking Id","Booking Date","Guest","Mobile Number","Email Id","Check-In","Check-Out","Accommodations","Rooms","Nights","Room Nights",
		"Adult","Child","Actual Amount","Hotel Amount","Tax","Revenue Amount","Varying Amount","Status"};
	
	private static final String[] revenueReportKey={"columnListA1","columnListA2","columnListA3","columnListA4"};
	
	private static final String[] userReportKey={"columnListA1","columnListA2","columnListA3"};
	
	private static final String[] bookingReportKey={"columnListA1","columnListA2","columnListA3","columnListA4","columnListA5","columnListA6","columnListA7","columnListA8","columnListA9","columnListB10","columnListB11",
		"columnListB12","columnListB13","columnListB14","columnListB15","columnListB16","columnListB17","columnListB18","columnListB19","columnListC20","columnListC21","columnListC22"};
	
	private static final String[] cancellationReportKey={"columnListA1","columnListA2","columnListA3","columnListA4","columnListA5","columnListA6","columnListA7","columnListA8",
		"columnListA9","columnListB10","columnListB11","columnListB12","columnListB13","columnListB14","columnListB15","columnListB16","columnListB17","columnListB18","columnListB19"};
	
	private static final String[] uloBookingReportKey={"columnListA1","columnListA2","columnListA3","columnListA4","columnListA5","columnListA6","columnListA7","columnListA8","columnListA9","columnListB10","columnListB11",
		"columnListB12","columnListB13","columnListB14","columnListB15","columnListB16","columnListB17","columnListB18","columnListB19","columnListC20","columnListC21","columnListC22","columnListC23","columnListC24","columnListC25","columnListC26",
		"columnListC27","columnListC28","columnListC29","columnListD30","columnListD31","columnListD32","columnListD33","columnListD34","columnListD35","columnListD36","columnListD37"};
	
	private static final String[] hotelBookingReportKey={"columnListA1","columnListA2","columnListA3","columnListA4","columnListA5","columnListA6","columnListA7","columnListA8","columnListA9","columnListB10","columnListB11",
		"columnListB12","columnListB13","columnListB14","columnListB15","columnListB16","columnListB17","columnListB18","columnListB19"};
	
	
	private Date fromDate;
	private Date toDate;
	private Date availDate;
	
	private int accommodationId;
	
	private String strFromDate;

	private String strToDate;
	
	private Integer propertyId;
	
	private String filterDateName;
	
	private String filterBookingId;
	
	private String invoiceId;
	private Integer userId;
	
	private Integer partnerPropertyId;
	private Integer selectPartnerPropertyId;
	
	private List<BookingDetailReport> listBookingDetails;

	private boolean reportEnabled;
	
	private HttpSession session;

	private InputStream fileInputStream = null; 
	
	private String fileName=null;
	
	private Integer reportId;
	
	private String reportName;
	
	private String checkedProperty;
	
	private String statusId;
	
	private String searchDateType;
	private String locationType;
	private Integer locationTypeId;
	private Integer contractTypeId;
	private String contractType;
	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}

	private static final Logger logger = Logger.getLogger(ReportAction.class);

	
	private User user;

	
	// @Override
	// public User getModel() {
	// return user;
	// }

	@Override
	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}
	private SessionMap<String,Object> sessionMap;  
	  
	//getters and setters  
	  
	@SuppressWarnings("unchecked")
	@Override
	public void setSession(Map<String, Object> map) {
		// TODO Auto-generated method stub
		 sessionMap=(SessionMap)map;  
	}
	

	
	private HttpServletRequest request;

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	public ReportAction() {

	}

	public String execute() {
		
		return SUCCESS;
	}

	
	public Integer getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(Integer propertyId) {
		this.propertyId = propertyId;
	}
	
	public String getStrFromDate() {
		return strFromDate;
	}

	public void setStrFromDate(String strFromDate) {
		this.strFromDate = strFromDate;
	}

	public String getStrToDate() {
		return strToDate;
	}

	public void setStrToDate(String strToDate) {
		this.strToDate = strToDate;
	}
	
	public String getPartnerBookingReport() throws IOException{
		try {
			this.partnerPropertyId=(Integer)sessionMap.get("partnerPropertyId");
	 		this.selectPartnerPropertyId=(Integer)sessionMap.get("selectedPropertyId");
			if(this.selectPartnerPropertyId!=null){
				this.partnerPropertyId=this.selectPartnerPropertyId;	
			}else{
				this.partnerPropertyId=this.partnerPropertyId;
			}
			
			this.filterDateName=getFilterDateName();
		    this.filterBookingId=getFilterBookingId();
		    int bookingId=Integer.parseInt(filterBookingId);
		    if(bookingId==0){
		    	DateFormat format = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
			    java.util.Date dateStart = format.parse(getStrFromDate());
			    java.util.Date dateEnd = format.parse(getStrToDate());
			    Calendar calCheckStart=Calendar.getInstance();
			    calCheckStart.setTime(dateStart);
			    java.util.Date checkInDate = calCheckStart.getTime();
			    this.fromDate=new Date(checkInDate.getTime());
			    sessionMap.put("fromDate",fromDate); 
			    Calendar calCheckEnd=Calendar.getInstance();
			    calCheckEnd.setTime(dateEnd);
			    java.util.Date checkOutDate = calCheckEnd.getTime();
			    this.toDate=new Date(checkOutDate.getTime());
			    sessionMap.put("endDate",toDate); 
		    }
		    
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			DecimalFormat df = new DecimalFormat("###.##");
			PmsPropertyManager pmsPropertyController=new PmsPropertyManager();
			ReportManager reportController = new ReportManager();
			PmsBookingManager bookingController = new PmsBookingManager();
			BookingDetailManager detailController = new BookingDetailManager();
			PmsGuestManager guestController = new PmsGuestManager();
			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
			PmsSourceManager sourceController = new PmsSourceManager();
			PmsStatusManager statusController=new PmsStatusManager();
			BookingGuestDetailManager bookingGuestController = new BookingGuestDetailManager();
			PropertyDiscountManager discountController=new PropertyDiscountManager();
			PmsProperty pmsProperty=new PmsProperty();
			
			response.setContentType("application/json");
			SimpleDateFormat format1 = new SimpleDateFormat("dd-MMM-yyyy");
			SimpleDateFormat format3 = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss a ");
			//this.listBookingDetails=reportController.listBookingDetail((Integer) sessionMap.get("propertyId"),this.getFromDate(),this.getToDate(),this.getAccommodationId());
			if(bookingId==0){
				this.listBookingDetails=reportController.listBookingDetail(this.partnerPropertyId,this.getFromDate(),this.getToDate(),this.getAccommodationId(),this.filterDateName,Integer.parseInt(this.filterBookingId));	
			}else if(bookingId>0){
				this.listBookingDetails=reportController.listBookingDetail(this.partnerPropertyId,Integer.parseInt(this.filterBookingId));
			}


			if(listBookingDetails.size()>0 && !listBookingDetails.isEmpty()){
				for(BookingDetailReport bookingDetailReport:listBookingDetails){
					if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += ",{";
					else
						jsonOutput += "{";
					
					jsonOutput += "\"bookingId\":\"" + bookingDetailReport.getBookingId()+ "\"";
					String strFirstName="",firstNameLetterUpperCase="";
					String strLastName="",lastNameLetterUpperCase="";
					strFirstName=bookingDetailReport.getFirstName();
					strLastName=bookingDetailReport.getLastName();
					if(strFirstName!=null && strFirstName!="" && !strFirstName.equalsIgnoreCase("undefined")){
						strFirstName=strFirstName.trim();
						firstNameLetterUpperCase=strFirstName.substring(0, 1).toUpperCase()+strFirstName.substring(1);
						strFirstName=firstNameLetterUpperCase;
					}else{
						strFirstName="NA";
					}
					if(strLastName!=null && strLastName!=""  && !strLastName.equalsIgnoreCase("undefined")){
						strLastName=strLastName.trim();
						lastNameLetterUpperCase=strLastName.substring(0, 1).toUpperCase() +  strLastName.substring(1);
						strLastName=lastNameLetterUpperCase;
					}else{
						strLastName="";
					}
					
					jsonOutput += ",\"guestName\":\"" + strFirstName.trim()+" "+strLastName.trim() + "\"";
					
					Double dblBaseAmount=0.0,dblTaxAmount=0.0,dblTotalAmount=0.0,dblCommission=0.0,
							dblTotalUloCommission=0.0,dblUloCommission=0.0,dblUloTaxAmount=0.0,dblAdvanceAmount=0.0,dblDueAmount=0.0;
					Double dblOtaCommission=0.0,dblOtaTax=0.0,dblTotalOtaAmout=0.0,dblRevenueAmount=0.0,dblTotalRevenueTaxAmount=0.0;
					Integer rooms=0,adultCount=0,childCount=0,statusId=0,noOfRooms=0,sourceId;
					
					String strMobileNumber="",statusName="",strEmailId="";
					strMobileNumber=bookingDetailReport.getMobileNumber();
//					jsonOutput += ",\"mobileNumber\":\"" + strMobileNumber.trim()+ "\"";
					
					strEmailId=bookingDetailReport.getEmailId().trim();
//					jsonOutput += ",\"emailId\":\"" + strEmailId+ "\"";
					String strBookingDate="";
					String timeHours="";
					if(bookingDetailReport.getCreatedDate()!=null){
						Timestamp bookingDate=bookingDetailReport.getCreatedDate();
						java.util.Date dateBooking = new java.util.Date(bookingDate.getTime());
						Calendar calDate=Calendar.getInstance();
						calDate.setTime(dateBooking);
						calDate.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
		            	int bookedHours=calDate.get(Calendar.HOUR);
		            	int bookedMinute=calDate.get(Calendar.MINUTE);
		            	int bookedSecond=calDate.get(Calendar.SECOND);
		            	int AMPM=calDate.get(Calendar.AM_PM);
		            	
		            	strBookingDate=format1.format(dateBooking);
		            	String hours="",minutes="",seconds="";
		            	if(bookedHours<10){
		            		hours=String.valueOf(bookedHours);
		            		hours="0"+hours;
		            	}else{
		            		hours=String.valueOf(bookedHours);
		            	}
		            	if(bookedSecond<10){
		            		seconds=String.valueOf(bookedSecond);
		            		seconds="0"+seconds;
		            	}else{
		            		seconds=String.valueOf(bookedSecond);
		            	}
		            	
		            	if(bookedMinute<10){
		            		minutes=String.valueOf(bookedMinute);
		            		minutes="0"+minutes;
		            	}else{
		            		minutes=String.valueOf(bookedMinute);
		            	}
		            	if(AMPM==0){//If the current time is AM
		            		timeHours=strBookingDate+"-"+hours+" : "+minutes+" : "+seconds+" AM ";
		            	}else if(AMPM==1){//If the current time is PM
		            		timeHours=strBookingDate+"-"+hours+" : "+minutes+" : "+seconds+" PM ";
		            	}
		            	
						strBookingDate=format3.format(dateBooking); 
						jsonOutput += ",\"bookingDate\":\"" + timeHours+ "\"";
					}else{
						jsonOutput += ",\"bookingDate\":\"NA\"";
					}
					
					long arrivalDate =bookingDetailReport.getArrivalDate().getTime();
					java.util.Date dateArrival = new java.util.Date(arrivalDate);
					String strArrivalDate=format1.format(dateArrival); 
					
					jsonOutput += ",\"checkIn\":\"" + strArrivalDate+ "\"";
					
					long departureDate =bookingDetailReport.getDepartureDate().getTime();
					java.util.Date dateDeparture = new java.util.Date(departureDate);
					String strDepartureDate=format1.format(dateDeparture); 
					
					jsonOutput += ",\"checkOut\":\"" + strDepartureDate+ "\"";
					SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
					
					Timestamp FromDate=bookingDetailReport.getArrivalDate();
					Timestamp ToDate=bookingDetailReport.getDepartureDate();
					
					String strFromDate=format2.format(FromDate);
					String strToDate=format2.format(ToDate);
					
			    	DateTime startdate = DateTime.parse(strFromDate);
			        DateTime enddate = DateTime.parse(strToDate);
			        java.util.Date fromdate = startdate.toDate();
			 		java.util.Date todate = enddate.toDate();
			 		
					int diffInDays = (int) ((todate.getTime() - fromdate.getTime()) / (1000 * 60 * 60 * 24));
					
					String strAccommodationType=null;
					Integer accommId=0;
					accommId=bookingDetailReport.getAccommodationId();
					if(accommId>0)
					{
						PropertyAccommodation accommodations=accommodationController.find(accommId);
						strAccommodationType=accommodations.getAccommodationType();
						jsonOutput += ",\"accommodationType\":\" "+strAccommodationType+"\"";
					}
					
					long lngrooms=bookingDetailReport.getRoomCount();
					rooms=(int)lngrooms;
					
					noOfRooms=rooms/diffInDays;
			 		Integer diffRoomInDays=noOfRooms*diffInDays;
					jsonOutput += ",\"rooms\":\"" + rooms/diffInDays+ "\"";
					jsonOutput += ",\"diffDays\":\"" + diffInDays+ "\"";
					jsonOutput += ",\"roomNights\":\"" + diffRoomInDays+ "\"";
					jsonOutput += ",\"adultCount\":\"" + bookingDetailReport.getAdultCount()+ "\"";
					jsonOutput += ",\"childCount\":\"" + bookingDetailReport.getChildCount()+ "\"";
					
					dblBaseAmount=bookingDetailReport.getAmount();
					dblTaxAmount=bookingDetailReport.getTaxAmount();
					
					if(dblBaseAmount!=null && dblBaseAmount>=0.0){
						dblBaseAmount=bookingDetailReport.getAmount();
					}else{
						dblBaseAmount=0.0;
					}
					
					if(dblTaxAmount!=null && dblTaxAmount>=0.0){
						dblTaxAmount=bookingDetailReport.getTaxAmount();
					}else{
						dblTaxAmount=0.0;
					}
					sourceId=bookingDetailReport.getSourceId();
					dblTotalAmount=dblBaseAmount+dblTaxAmount;
					
					dblAdvanceAmount=bookingDetailReport.getAdvanceAmount();
					
					dblTotalAmount=dblBaseAmount+dblTaxAmount;
					
					
					if(dblAdvanceAmount!=null  && dblAdvanceAmount>=0.0){
						dblAdvanceAmount=bookingDetailReport.getAdvanceAmount();
					}else{
						if(sourceId==1){
							dblAdvanceAmount=dblTotalAmount;
						}else{
							dblAdvanceAmount=0.0;
						}
					}
					String strTotal=df.format(dblTotalAmount);
					
					boolean compareValues=Double.valueOf(strTotal).equals(dblAdvanceAmount);
					
					
					dblDueAmount=Double.valueOf(strTotal)-dblAdvanceAmount;
					
					
					
//					jsonOutput += ",\"baseAmount\":\"" + dblBaseAmount + "\"";
//					jsonOutput += ",\"taxAmount\":\"" + dblTaxAmount + "\"";
					
					dblTotalAmount=dblBaseAmount+dblTaxAmount;
					
					//jsonOutput += ",\"revenueAmount\":\"" + (dblBaseAmount-dblTaxAmount)+ "\"";

					dblOtaCommission=bookingDetailReport.getOtaCommission();
					dblOtaTax=bookingDetailReport.getOtaTax();
					if(dblOtaCommission!=null && dblOtaCommission>=0.0){
						dblOtaCommission=bookingDetailReport.getOtaCommission();
					}else{
						dblOtaCommission=0.0;
					}
					if(dblOtaTax!=null && dblOtaTax>=0.0){
						dblOtaTax=bookingDetailReport.getOtaTax();
					}else{
						dblOtaTax=0.0;
					}
					
					dblTotalOtaAmout=dblOtaCommission+dblOtaTax;
					
//					jsonOutput += ",\"otaCommission\":\" "+df.format(dblOtaCommission)+"\"";
//					jsonOutput += ",\"otaTax\":\" "+df.format(dblOtaTax)+"\"";
//					jsonOutput += ",\"otaRevenueAmount\":\" "+df.format(dblTotalOtaAmout)+"\"";
					
					dblRevenueAmount=dblBaseAmount-dblTotalOtaAmout;
					dblTotalRevenueTaxAmount=dblRevenueAmount+dblTaxAmount;
					jsonOutput += ",\"revenueAmount\":\" "+df.format(dblRevenueAmount)+"\"";
					jsonOutput += ",\"taxAmount\":\"" + df.format(dblTaxAmount) + "\"";
					jsonOutput += ",\"revenueTaxAmount\":\" "+df.format(dblTotalRevenueTaxAmount)+"\"";
					
					PmsProperty pmsPropertyDetails=pmsPropertyController.find(this.partnerPropertyId);
					dblCommission=pmsPropertyDetails.getPropertyCommission();
					if(dblCommission!=null){
						dblUloCommission=dblRevenueAmount*dblCommission/100;
					}
					
					jsonOutput += ",\"uloCommission\":\"" + df.format(dblUloCommission)+ "\"";
					
					dblUloTaxAmount=dblUloCommission*18/100;
					jsonOutput += ",\"uloTaxAmount\":\"" + df.format(dblUloTaxAmount)+ "\"";
					
					dblTotalUloCommission=dblUloCommission+dblUloTaxAmount;
					jsonOutput += ",\"uloRevenue\":\"" + df.format(dblTotalUloCommission)+ "\"";
					
					Double dblUloRevenue=dblRevenueAmount-dblTotalUloCommission;
					jsonOutput += ",\"hotelPayRevenue\":\"" + df.format(dblUloRevenue)+ "\"";
					jsonOutput += ",\"taxTotalAmount\":\"" + df.format(dblTaxAmount) + "\"";
					Double dblPayatHotel=dblUloRevenue+dblTaxAmount;
					jsonOutput += ",\"dueAmount\":\" "+df.format(dblDueAmount)+"\"";
					jsonOutput += ",\"hotelRevenue\":\"" + df.format(dblPayatHotel) + "\"";
					
					
					statusId=bookingDetailReport.getStatusId();
					PmsStatus status=statusController.find(statusId);
					statusName=status.getStatus();
					jsonOutput += ",\"status\":\"" + statusName+ "\"";
					jsonOutput += "}";
				}
			}	
		
			

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	
	}
	
	public String bookingReport() throws IOException {
		
		try {
			
			this.filterDateName=getFilterDateName();
		    this.filterBookingId=getFilterBookingId();
		    int bookingId=Integer.parseInt(filterBookingId);
		    if(bookingId==0){
		    	DateFormat format = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
			    java.util.Date dateStart = format.parse(getStrFromDate());
			    java.util.Date dateEnd = format.parse(getStrToDate());
			    Calendar calCheckStart=Calendar.getInstance();
			    calCheckStart.setTime(dateStart);
			    java.util.Date checkInDate = calCheckStart.getTime();
			    this.fromDate=new Date(checkInDate.getTime());
			    sessionMap.put("fromDate",fromDate); 
			    Calendar calCheckEnd=Calendar.getInstance();
			    calCheckEnd.setTime(dateEnd);
			    java.util.Date checkOutDate = calCheckEnd.getTime();
			    this.toDate=new Date(checkOutDate.getTime());
			    sessionMap.put("endDate",toDate); 
		    }
		    
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			DecimalFormat df = new DecimalFormat("###.##");
			PmsPropertyManager pmsPropertyController=new PmsPropertyManager();
			ReportManager reportController = new ReportManager();
			PmsBookingManager bookingController = new PmsBookingManager();
			BookingDetailManager detailController = new BookingDetailManager();
			PmsGuestManager guestController = new PmsGuestManager();
			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
			PmsSourceManager sourceController = new PmsSourceManager();
			PmsStatusManager statusController=new PmsStatusManager();
			BookingGuestDetailManager bookingGuestController = new BookingGuestDetailManager();
			PropertyDiscountManager discountController=new PropertyDiscountManager();
			PmsProperty pmsProperty=new PmsProperty();
			
			response.setContentType("application/json");
			SimpleDateFormat format1 = new SimpleDateFormat("dd-MMM-yyyy");
			SimpleDateFormat format3 = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss a ");
			//this.listBookingDetails=reportController.listBookingDetail((Integer) sessionMap.get("propertyId"),this.getFromDate(),this.getToDate(),this.getAccommodationId());
			if(bookingId==0){
				this.listBookingDetails=reportController.listBookingDetail(getPropertyId(),this.getFromDate(),this.getToDate(),this.getAccommodationId(),this.filterDateName,Integer.parseInt(this.filterBookingId));	
			}else if(bookingId>0){
				this.listBookingDetails=reportController.listBookingDetail(getPropertyId(),Integer.parseInt(this.filterBookingId));
			}
			if(reportEnabled){

				if(listBookingDetails.size()>0 && !listBookingDetails.isEmpty()){
					for(BookingDetailReport bookingDetailReport:listBookingDetails){
						int i=0;
						while(i<bookingReportKey.length){

							
							if (!jsonOutput.equalsIgnoreCase(""))
								jsonOutput += ",{";
							else
								jsonOutput += "{";
							
							jsonOutput += "\""+bookingReportKey[i++]+"\":\"" + bookingDetailReport.getBookingId()+ "\"";
							String strFirstName="",firstNameLetterUpperCase="";
							String strLastName="",lastNameLetterUpperCase="";
							strFirstName=bookingDetailReport.getFirstName();
							strLastName=bookingDetailReport.getLastName();
							if(strFirstName!=null && strFirstName!="" && !strFirstName.equalsIgnoreCase("undefined")){
								strFirstName=strFirstName.trim();
								firstNameLetterUpperCase=strFirstName.substring(0, 1).toUpperCase()+strFirstName.substring(1);
								strFirstName=firstNameLetterUpperCase;
							}else{
								strFirstName="NA";
							}
							if(strLastName!=null && strLastName!=""  && !strLastName.equalsIgnoreCase("undefined")){
								strLastName=strLastName.trim();
								lastNameLetterUpperCase=strLastName.substring(0, 1).toUpperCase() +  strLastName.substring(1);
								strLastName=lastNameLetterUpperCase;
							}else{
								strLastName="";
							}
							
							jsonOutput += ",\""+bookingReportKey[i++]+"\":\"" + strFirstName.trim()+" "+strLastName.trim() + "\"";
							
							String strBookingDate="";
							String timeHours="";
							if(bookingDetailReport.getCreatedDate()!=null){
								Timestamp bookingDate=bookingDetailReport.getCreatedDate();
								java.util.Date dateBooking = new java.util.Date(bookingDate.getTime());
								Calendar calDate=Calendar.getInstance();
								calDate.setTime(dateBooking);
								calDate.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
				            	int bookedHours=calDate.get(Calendar.HOUR);
				            	int bookedMinute=calDate.get(Calendar.MINUTE);
				            	int bookedSecond=calDate.get(Calendar.SECOND);
				            	int AMPM=calDate.get(Calendar.AM_PM);
				            	
				            	strBookingDate=format1.format(dateBooking);
				            	String hours="",minutes="",seconds="";
				            	if(bookedHours<10){
				            		hours=String.valueOf(bookedHours);
				            		hours="0"+hours;
				            	}else{
				            		hours=String.valueOf(bookedHours);
				            	}
				            	if(bookedSecond<10){
				            		seconds=String.valueOf(bookedSecond);
				            		seconds="0"+seconds;
				            	}else{
				            		seconds=String.valueOf(bookedSecond);
				            	}
				            	
				            	if(bookedMinute<10){
				            		minutes=String.valueOf(bookedMinute);
				            		minutes="0"+minutes;
				            	}else{
				            		minutes=String.valueOf(bookedMinute);
				            	}
				            	if(AMPM==0){//If the current time is AM
				            		timeHours=strBookingDate+"-"+hours+" : "+minutes+" : "+seconds+" AM ";
				            	}else if(AMPM==1){//If the current time is PM
				            		timeHours=strBookingDate+"-"+hours+" : "+minutes+" : "+seconds+" PM ";
				            	}
				            	
								strBookingDate=format3.format(dateBooking); 
								jsonOutput += ",\""+bookingReportKey[i++]+"\":\"" + timeHours+ "\"";
							}else{
								jsonOutput += ",\""+bookingReportKey[i++]+"\":\"NA\"";
							}
							
							Double dblBaseAmount=0.0,dblTaxAmount=0.0,dblTotalAmount=0.0,dblCommission=0.0,dblPromotionAmount=0.0,dblCouponAmount=0.0,
									dblTotalUloCommission=0.0,dblUloCommission=0.0,dblUloTaxAmount=0.0,dblAdvanceAmount=0.0,dblDueAmount=0.0;
							Double dblOtaCommission=0.0,dblOtaTax=0.0,dblTotalOtaAmout=0.0,dblRevenueAmount=0.0,dblTotalRevenueTaxAmount=0.0;
							Integer rooms=0,adultCount=0,childCount=0,statusId=0,noOfRooms=0,sourceId;
							
							String strMobileNumber="",statusName="",strEmailId="";
							strMobileNumber=bookingDetailReport.getMobileNumber();
//							jsonOutput += ",\""+bookingReportKey[i++]+"\":\"" + strMobileNumber.trim()+ "\"";
							
							strEmailId=bookingDetailReport.getEmailId().trim();
//							jsonOutput += ",\""+bookingReportKey[i++]+"\":\"" + strEmailId+ "\"";
							
							
							long arrivalDate =bookingDetailReport.getArrivalDate().getTime();
							java.util.Date dateArrival = new java.util.Date(arrivalDate);
							String strArrivalDate=format1.format(dateArrival); 
							
							jsonOutput += ",\""+bookingReportKey[i++]+"\":\"" + strArrivalDate+ "\"";
							
							long departureDate =bookingDetailReport.getDepartureDate().getTime();
							java.util.Date dateDeparture = new java.util.Date(departureDate);
							String strDepartureDate=format1.format(dateDeparture); 
							
							jsonOutput += ",\""+bookingReportKey[i++]+"\":\"" + strDepartureDate+ "\"";
							SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
							
							Timestamp FromDate=bookingDetailReport.getArrivalDate();
							Timestamp ToDate=bookingDetailReport.getDepartureDate();
							
							String strFromDate=format2.format(FromDate);
							String strToDate=format2.format(ToDate);
							
					    	DateTime startdate = DateTime.parse(strFromDate);
					        DateTime enddate = DateTime.parse(strToDate);
					        java.util.Date fromdate = startdate.toDate();
					 		java.util.Date todate = enddate.toDate();
					 		
							int diffInDays = (int) ((todate.getTime() - fromdate.getTime()) / (1000 * 60 * 60 * 24));
							
							String strAccommodationType=null;
							Integer accommId=0;
							accommId=bookingDetailReport.getAccommodationId();
							if(accommId>0)
							{
								PropertyAccommodation accommodations=accommodationController.find(accommId);
								strAccommodationType=accommodations.getAccommodationType();
								jsonOutput += ",\""+bookingReportKey[i++]+"\":\" "+strAccommodationType+"\"";
							}
							
							long lngrooms=bookingDetailReport.getRoomCount();
							rooms=(int)lngrooms;
							
							noOfRooms=rooms/diffInDays;
					 		Integer diffRoomInDays=noOfRooms*diffInDays;
							jsonOutput += ",\""+bookingReportKey[i++]+"\":\"" + rooms/diffInDays+ "\"";
							jsonOutput += ",\""+bookingReportKey[i++]+"\":\"" + diffInDays+ "\"";
							jsonOutput += ",\""+bookingReportKey[i++]+"\":\"" + diffRoomInDays+ "\"";
							jsonOutput += ",\""+bookingReportKey[i++]+"\":\"" + bookingDetailReport.getAdultCount()+ "\"";
							jsonOutput += ",\""+bookingReportKey[i++]+"\":\"" + bookingDetailReport.getChildCount()+ "\"";
							
							dblBaseAmount=bookingDetailReport.getAmount();
							dblTaxAmount=bookingDetailReport.getTaxAmount();
							
							if(dblBaseAmount!=null && dblBaseAmount>=0.0){
								dblBaseAmount=bookingDetailReport.getAmount();
							}else{
								dblBaseAmount=0.0;
							}
							
							if(dblTaxAmount!=null && dblTaxAmount>=0.0){
								dblTaxAmount=bookingDetailReport.getTaxAmount();
							}else{
								dblTaxAmount=0.0;
							}
							sourceId=bookingDetailReport.getSourceId();
							dblTotalAmount=dblBaseAmount+dblTaxAmount;
							if(bookingDetailReport.getCouponAmount()!=null){
								dblCouponAmount=bookingDetailReport.getCouponAmount();
							}else{
								dblCouponAmount=0.0;
							}
							dblAdvanceAmount=bookingDetailReport.getAdvanceAmount();
							
							dblTotalAmount=dblBaseAmount+dblTaxAmount-dblCouponAmount;
							
							
							if(dblAdvanceAmount!=null  && dblAdvanceAmount>=0.0){
								dblAdvanceAmount=bookingDetailReport.getAdvanceAmount();
							}else{
								if(sourceId==1){
									dblAdvanceAmount=dblTotalAmount;
								}else{
									dblAdvanceAmount=0.0;
								}
							}
							String strTotal=df.format(dblTotalAmount);
							
							boolean compareValues=Double.valueOf(strTotal).equals(dblAdvanceAmount);
							
							
							dblDueAmount=Double.valueOf(strTotal)-dblAdvanceAmount;
							
							
							
//							jsonOutput += ",\"baseAmount\":\"" + dblBaseAmount + "\"";
//							jsonOutput += ",\"taxAmount\":\"" + dblTaxAmount + "\"";
							
							dblTotalAmount=dblBaseAmount+dblTaxAmount;
							
							//jsonOutput += ",\"revenueAmount\":\"" + (dblBaseAmount-dblTaxAmount)+ "\"";

							dblOtaCommission=bookingDetailReport.getOtaCommission();
							dblOtaTax=bookingDetailReport.getOtaTax();
							if(dblOtaCommission!=null && dblOtaCommission>=0.0){
								dblOtaCommission=bookingDetailReport.getOtaCommission();
							}else{
								dblOtaCommission=0.0;
							}
							if(dblOtaTax!=null && dblOtaTax>=0.0){
								dblOtaTax=bookingDetailReport.getOtaTax();
							}else{
								dblOtaTax=0.0;
							}
							
							dblTotalOtaAmout=dblOtaCommission+dblOtaTax;
							
//							jsonOutput += ",\"otaCommission\":\" "+df.format(dblOtaCommission)+"\"";
//							jsonOutput += ",\"otaTax\":\" "+df.format(dblOtaTax)+"\"";
//							jsonOutput += ",\"otaRevenueAmount\":\" "+df.format(dblTotalOtaAmout)+"\"";
							
							dblRevenueAmount=dblBaseAmount-dblTotalOtaAmout;
							dblTotalRevenueTaxAmount=dblRevenueAmount+dblTaxAmount;
							jsonOutput += ",\""+bookingReportKey[i++]+"\":\" "+df.format(dblRevenueAmount)+"\"";
							jsonOutput += ",\""+bookingReportKey[i++]+"\":\"" + df.format(dblTaxAmount) + "\"";
							jsonOutput += ",\""+bookingReportKey[i++]+"\":\" "+df.format(dblTotalRevenueTaxAmount)+"\"";
							
							PmsProperty pmsPropertyDetails=pmsPropertyController.find(getPropertyId());
							dblCommission=pmsPropertyDetails.getPropertyCommission();
							if(dblCommission!=null){
								dblUloCommission=dblRevenueAmount*dblCommission/100;
							}
							
							jsonOutput += ",\""+bookingReportKey[i++]+"\":\"" + df.format(dblUloCommission)+ "\"";
							
							dblUloTaxAmount=dblUloCommission*18/100;
							jsonOutput += ",\""+bookingReportKey[i++]+"\":\"" + df.format(dblUloTaxAmount)+ "\"";
							
							dblTotalUloCommission=dblUloCommission+dblUloTaxAmount;
							jsonOutput += ",\""+bookingReportKey[i++]+"\":\"" + df.format(dblTotalUloCommission)+ "\"";
							
							Double dblUloRevenue=dblRevenueAmount-dblTotalUloCommission;
							jsonOutput += ",\""+bookingReportKey[i++]+"\":\"" + df.format(dblUloRevenue)+ "\"";
							jsonOutput += ",\""+bookingReportKey[i++]+"\":\"" + df.format(dblTaxAmount) + "\"";
							Double dblPayatHotel=dblUloRevenue+dblTaxAmount;
							jsonOutput += ",\""+bookingReportKey[i++]+"\":\" "+df.format(dblDueAmount)+"\"";
							jsonOutput += ",\""+bookingReportKey[i++]+"\":\"" + df.format(dblPayatHotel) + "\"";
							
							
							statusId=bookingDetailReport.getStatusId();
							PmsStatus status=statusController.find(statusId);
							statusName=status.getStatus();
							jsonOutput += ",\""+bookingReportKey[i++]+"\":\"" + statusName+ "\"";
							jsonOutput += "}";
						
						}
					}
				}
			}else{
				if(listBookingDetails.size()>0 && !listBookingDetails.isEmpty()){
					for(BookingDetailReport bookingDetailReport:listBookingDetails){
						if (!jsonOutput.equalsIgnoreCase(""))
							jsonOutput += ",{";
						else
							jsonOutput += "{";
						
						jsonOutput += "\"bookingId\":\"" + bookingDetailReport.getBookingId()+ "\"";
						String strFirstName="",firstNameLetterUpperCase="";
						String strLastName="",lastNameLetterUpperCase="";
						strFirstName=bookingDetailReport.getFirstName();
						strLastName=bookingDetailReport.getLastName();
						if(strFirstName!=null && strFirstName!="" && !strFirstName.equalsIgnoreCase("undefined")){
							strFirstName=strFirstName.trim();
							firstNameLetterUpperCase=strFirstName.substring(0, 1).toUpperCase()+strFirstName.substring(1);
							strFirstName=firstNameLetterUpperCase;
						}else{
							strFirstName="NA";
						}
						if(strLastName!=null && strLastName!=""  && !strLastName.equalsIgnoreCase("undefined")){
							strLastName=strLastName.trim();
							lastNameLetterUpperCase=strLastName.substring(0, 1).toUpperCase() +  strLastName.substring(1);
							strLastName=lastNameLetterUpperCase;
						}else{
							strLastName="";
						}
						
						jsonOutput += ",\"guestName\":\"" + strFirstName.trim()+" "+strLastName.trim() + "\"";
						
						Double dblBaseAmount=0.0,dblTaxAmount=0.0,dblTotalAmount=0.0,dblCommission=0.0,purchaseRate=0.0,dblTotalBaseAmount=0.0,
								dblTotalUloCommission=0.0,dblUloCommission=0.0,dblUloTaxAmount=0.0,dblAdvanceAmount=0.0,dblDueAmount=0.0;
						Double dblOtaCommission=0.0,dblOtaTax=0.0,dblTotalOtaAmout=0.0,dblRevenueAmount=0.0,
								dblTotalRevenueTaxAmount=0.0,promotionAmount=0.0,couponAmount=0.0;
						Integer rooms=0,adultCount=0,childCount=0,statusId=0,noOfRooms=0,sourceId;
						
						String strMobileNumber="",statusName="",strEmailId="";
						strMobileNumber=bookingDetailReport.getMobileNumber();
//						jsonOutput += ",\"mobileNumber\":\"" + strMobileNumber.trim()+ "\"";
						
						strEmailId=bookingDetailReport.getEmailId().trim();
//						jsonOutput += ",\"emailId\":\"" + strEmailId+ "\"";
						String strBookingDate="";
						String timeHours="";
						if(bookingDetailReport.getCreatedDate()!=null){
							Timestamp bookingDate=bookingDetailReport.getCreatedDate();
							java.util.Date dateBooking = new java.util.Date(bookingDate.getTime());
							Calendar calDate=Calendar.getInstance();
							calDate.setTime(dateBooking);
							calDate.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
			            	int bookedHours=calDate.get(Calendar.HOUR);
			            	int bookedMinute=calDate.get(Calendar.MINUTE);
			            	int bookedSecond=calDate.get(Calendar.SECOND);
			            	int AMPM=calDate.get(Calendar.AM_PM);
			            	
			            	strBookingDate=format1.format(dateBooking);
			            	String hours="",minutes="",seconds="";
			            	if(bookedHours<10){
			            		hours=String.valueOf(bookedHours);
			            		hours="0"+hours;
			            	}else{
			            		hours=String.valueOf(bookedHours);
			            	}
			            	if(bookedSecond<10){
			            		seconds=String.valueOf(bookedSecond);
			            		seconds="0"+seconds;
			            	}else{
			            		seconds=String.valueOf(bookedSecond);
			            	}
			            	
			            	if(bookedMinute<10){
			            		minutes=String.valueOf(bookedMinute);
			            		minutes="0"+minutes;
			            	}else{
			            		minutes=String.valueOf(bookedMinute);
			            	}
			            	if(AMPM==0){//If the current time is AM
			            		timeHours=strBookingDate+"-"+hours+" : "+minutes+" : "+seconds+" AM ";
			            	}else if(AMPM==1){//If the current time is PM
			            		timeHours=strBookingDate+"-"+hours+" : "+minutes+" : "+seconds+" PM ";
			            	}
			            	
							strBookingDate=format3.format(dateBooking); 
							jsonOutput += ",\"bookingDate\":\"" + timeHours+ "\"";
						}else{
							jsonOutput += ",\"bookingDate\":\"NA\"";
						}
						
						long arrivalDate =bookingDetailReport.getArrivalDate().getTime();
						java.util.Date dateArrival = new java.util.Date(arrivalDate);
						String strArrivalDate=format1.format(dateArrival); 
						
						jsonOutput += ",\"checkIn\":\"" + strArrivalDate+ "\"";
						
						long departureDate =bookingDetailReport.getDepartureDate().getTime();
						java.util.Date dateDeparture = new java.util.Date(departureDate);
						String strDepartureDate=format1.format(dateDeparture); 
						
						jsonOutput += ",\"checkOut\":\"" + strDepartureDate+ "\"";
						SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
						
						Timestamp FromDate=bookingDetailReport.getArrivalDate();
						Timestamp ToDate=bookingDetailReport.getDepartureDate();
						
						String strFromDate=format2.format(FromDate);
						String strToDate=format2.format(ToDate);
						
				    	DateTime startdate = DateTime.parse(strFromDate);
				        DateTime enddate = DateTime.parse(strToDate);
				        java.util.Date fromdate = startdate.toDate();
				 		java.util.Date todate = enddate.toDate();
				 		
						int diffInDays = (int) ((todate.getTime() - fromdate.getTime()) / (1000 * 60 * 60 * 24));
						
						String strAccommodationType=null;
						Integer accommId=0,propertyId=null;
						accommId=bookingDetailReport.getAccommodationId();
						if(accommId>0)
						{
							PropertyAccommodation accommodations=accommodationController.find(accommId);
							strAccommodationType=accommodations.getAccommodationType();
							jsonOutput += ",\"accommodationType\":\" "+strAccommodationType+"\"";
						}
						propertyId=bookingDetailReport.getPropertyId();
						
						long lngrooms=bookingDetailReport.getRoomCount();
						rooms=(int)lngrooms;
						
						noOfRooms=rooms/diffInDays;
				 		Integer diffRoomInDays=noOfRooms*diffInDays;
						jsonOutput += ",\"rooms\":\"" + rooms/diffInDays+ "\"";
						jsonOutput += ",\"diffDays\":\"" + diffInDays+ "\"";
						jsonOutput += ",\"roomNights\":\"" + diffRoomInDays+ "\"";
						jsonOutput += ",\"adultCount\":\"" + bookingDetailReport.getAdultCount()+ "\"";
						jsonOutput += ",\"childCount\":\"" + bookingDetailReport.getChildCount()+ "\"";
						
						PropertyAccommodation propertyAccommodation = accommodationController.find(bookingDetailReport.getAccommodationId());
	                    if(propertyAccommodation.getPropertyContractType()!=null){
	                     	contractType=propertyAccommodation.getPropertyContractType().getContractTypeName();
	                     	contractTypeId=propertyAccommodation.getPropertyContractType().getContractTypeId();
	                    }
	                    if(contractTypeId==7){//B-join
	                 		purchaseRate= bookingDetailReport.getPurchaseRate()==null?0:bookingDetailReport.getPurchaseRate();
	                 		contractType="join";
	                 	}else if(contractTypeId==6){//B-sure
	                 		purchaseRate= bookingDetailReport.getPurchaseRate()==null?0:bookingDetailReport.getPurchaseRate();
	                 		contractType="sure";
	                 	}else if(contractTypeId==8){//B-stay
	                 		purchaseRate= bookingDetailReport.getPurchaseRate()==null?0:bookingDetailReport.getPurchaseRate();
	                 		contractType="stay";
	                 	}else if(contractTypeId==9){//B-join flat
	                 		purchaseRate=bookingDetailReport.getAmount();
	                 		contractType="flat";
	                 	}
	                    
	                    if(contractType.equalsIgnoreCase("flat")){
	                    	dblBaseAmount=bookingDetailReport.getAmount();
							dblTaxAmount=bookingDetailReport.getTaxAmount();
							
							if(dblBaseAmount!=null && dblBaseAmount>=0.0){
								dblBaseAmount=bookingDetailReport.getAmount();
							}else{
								dblBaseAmount=0.0;
							}
							
							if(dblTaxAmount!=null && dblTaxAmount>=0.0){
								dblTaxAmount=bookingDetailReport.getTaxAmount();
							}else{
								dblTaxAmount=0.0;
							}
							sourceId=bookingDetailReport.getSourceId();
							if(bookingDetailReport.getCouponAmount()!=null){
								couponAmount=bookingDetailReport.getCouponAmount(); 
							}else{
								couponAmount=0.0;
							}
							dblAdvanceAmount=bookingDetailReport.getAdvanceAmount();
							
							dblTotalBaseAmount=dblBaseAmount+dblTaxAmount-couponAmount;
							
							
							if(dblAdvanceAmount!=null  && dblAdvanceAmount>=0.0){
								dblAdvanceAmount=bookingDetailReport.getAdvanceAmount();
							}else{
								if(sourceId==1){
									dblAdvanceAmount=dblTotalBaseAmount;
								}else{
									dblAdvanceAmount=0.0;
								}
							}
							String strTotal=df.format(dblTotalBaseAmount);
							
							boolean compareValues=Double.valueOf(strTotal).equals(dblAdvanceAmount);
							
							
							dblDueAmount=Double.valueOf(strTotal)-dblAdvanceAmount;
							
							
							
//							jsonOutput += ",\"baseAmount\":\"" + dblBaseAmount + "\"";
//							jsonOutput += ",\"baseTax\":\"" + dblTaxAmount + "\"";
							
							dblTotalBaseAmount=dblBaseAmount+dblTaxAmount;
							
//							jsonOutput += ",\"revenueBaseAmount\":\"" + (dblTotalBaseAmount)+ "\"";

							dblOtaCommission=bookingDetailReport.getOtaCommission();
							dblOtaTax=bookingDetailReport.getOtaTax();
							if(dblOtaCommission!=null && dblOtaCommission>=0.0){
								dblOtaCommission=bookingDetailReport.getOtaCommission();
							}else{
								dblOtaCommission=0.0;
							}
							if(dblOtaTax!=null && dblOtaTax>=0.0){
								dblOtaTax=bookingDetailReport.getOtaTax();
							}else{
								dblOtaTax=0.0;
							}
							
							dblTotalOtaAmout=dblOtaCommission+dblOtaTax;
							
//							jsonOutput += ",\"otaCommission\":\" "+df.format(dblOtaCommission)+"\"";
//							jsonOutput += ",\"otaTax\":\" "+df.format(dblOtaTax)+"\"";
//							jsonOutput += ",\"otaRevenueAmount\":\" "+df.format(dblTotalOtaAmout)+"\"";
							
							dblRevenueAmount=dblBaseAmount-dblTotalOtaAmout;
							dblTotalRevenueTaxAmount=dblRevenueAmount+dblTaxAmount;
							jsonOutput += ",\"revenueAmount\":\" "+df.format(dblRevenueAmount)+"\"";
							jsonOutput += ",\"taxAmount\":\"" + df.format(dblTaxAmount) + "\"";
							jsonOutput += ",\"revenueTaxAmount\":\" "+df.format(dblTotalRevenueTaxAmount)+"\"";
							
							PmsProperty pmsPropertyDetails=pmsPropertyController.find(getPropertyId());
							dblCommission=pmsPropertyDetails.getPropertyCommission();
							if(dblCommission!=null){
								dblUloCommission=dblRevenueAmount*dblCommission/100;
							}else{
								dblUloCommission=0.0;
							}
							
							jsonOutput += ",\"uloCommission\":\"" + df.format(dblUloCommission)+ "\"";
							
							dblUloTaxAmount=dblUloCommission*18/100;
							jsonOutput += ",\"uloTaxAmount\":\"" + df.format(dblUloTaxAmount)+ "\"";
							
							dblTotalUloCommission=dblUloCommission+dblUloTaxAmount;
							jsonOutput += ",\"uloRevenue\":\"" + df.format(dblTotalUloCommission)+ "\"";
							
							Double dblUloRevenue=dblRevenueAmount-dblTotalUloCommission;
							jsonOutput += ",\"hotelPayRevenue\":\"" + df.format(dblUloRevenue)+ "\"";
							jsonOutput += ",\"taxTotalAmount\":\"" + df.format(dblTaxAmount) + "\"";
							Double dblPayatHotel=dblUloRevenue+dblTaxAmount;
							jsonOutput += ",\"dueAmount\":\" "+df.format(dblDueAmount)+"\"";
							jsonOutput += ",\"hotelRevenue\":\"" + df.format(dblPayatHotel) + "\"";
							purchaseRate=bookingDetailReport.getPurchaseRate();
							if(purchaseRate==null){
								purchaseRate=0.0;
							}
							dblTotalAmount=dblRevenueAmount-purchaseRate;
							jsonOutput += ",\"profitLossAmount\":" + df.format(dblTotalAmount)+ "";
	
	                    }else{
	                    	dblBaseAmount=bookingDetailReport.getAmount();
							dblTaxAmount=bookingDetailReport.getTaxAmount();
							
							if(dblBaseAmount!=null && dblBaseAmount>=0.0){
								dblBaseAmount=bookingDetailReport.getAmount();
							}else{
								dblBaseAmount=0.0;
							}
							
							if(dblTaxAmount!=null && dblTaxAmount>=0.0){
								dblTaxAmount=bookingDetailReport.getTaxAmount();
							}else{
								dblTaxAmount=0.0;
							}
							sourceId=bookingDetailReport.getSourceId();
							if(bookingDetailReport.getCouponAmount()!=null){
								couponAmount=bookingDetailReport.getCouponAmount(); 
							}else{
								couponAmount=0.0;
							}
							dblAdvanceAmount=bookingDetailReport.getAdvanceAmount();
							
							dblTotalBaseAmount=dblBaseAmount+dblTaxAmount-couponAmount;
							
							
							if(dblAdvanceAmount!=null  && dblAdvanceAmount>=0.0){
								dblAdvanceAmount=bookingDetailReport.getAdvanceAmount();
							}else{
								if(sourceId==1){
									dblAdvanceAmount=dblTotalBaseAmount;
								}else{
									dblAdvanceAmount=0.0;
								}
							}
							String strTotal=df.format(dblTotalBaseAmount);
							
							boolean compareValues=Double.valueOf(strTotal).equals(dblAdvanceAmount);
							
							
							dblDueAmount=Double.valueOf(strTotal)-dblAdvanceAmount;
							
							
							
//							jsonOutput += ",\"baseAmount\":\"" + dblBaseAmount + "\"";
//							jsonOutput += ",\"baseTax\":\"" + dblTaxAmount + "\"";
							
							dblTotalBaseAmount=dblBaseAmount+dblTaxAmount;
							
//							jsonOutput += ",\"revenueBaseAmount\":\"" + (dblTotalBaseAmount)+ "\"";

							dblOtaCommission=bookingDetailReport.getOtaCommission();
							dblOtaTax=bookingDetailReport.getOtaTax();
							if(dblOtaCommission!=null && dblOtaCommission>=0.0){
								dblOtaCommission=bookingDetailReport.getOtaCommission();
							}else{
								dblOtaCommission=0.0;
							}
							if(dblOtaTax!=null && dblOtaTax>=0.0){
								dblOtaTax=bookingDetailReport.getOtaTax();
							}else{
								dblOtaTax=0.0;
							}
							
							dblTotalOtaAmout=dblOtaCommission+dblOtaTax;
							
//							jsonOutput += ",\"otaCommission\":\" "+df.format(dblOtaCommission)+"\"";
//							jsonOutput += ",\"otaTax\":\" "+df.format(dblOtaTax)+"\"";
//							jsonOutput += ",\"otaRevenueAmount\":\" "+df.format(dblTotalOtaAmout)+"\"";
							
							dblRevenueAmount=dblBaseAmount-dblTotalOtaAmout;
							dblTotalRevenueTaxAmount=dblRevenueAmount+dblTaxAmount;
							jsonOutput += ",\"revenueAmount\":\" "+0+"\"";
							jsonOutput += ",\"taxAmount\":\"" + 0 + "\"";
							jsonOutput += ",\"revenueTaxAmount\":\" "+0+"\"";
							
							PmsProperty pmsPropertyDetails=pmsPropertyController.find(getPropertyId());
							dblCommission=pmsPropertyDetails.getPropertyCommission();
							if(dblCommission!=null){
								dblUloCommission=dblRevenueAmount*dblCommission/100;
							}else{
								dblUloCommission=0.0;
							}
							
							jsonOutput += ",\"uloCommission\":\"" + 0+ "\"";
							
							dblUloTaxAmount=dblUloCommission*18/100;
							jsonOutput += ",\"uloTaxAmount\":\"" + 0+ "\"";
							
							dblTotalUloCommission=dblUloCommission+dblUloTaxAmount;
							jsonOutput += ",\"uloRevenue\":\"" + 0+ "\"";
							
							Double dblUloRevenue=dblRevenueAmount-dblTotalUloCommission;
							jsonOutput += ",\"hotelPayRevenue\":\"" + 0+ "\"";
							jsonOutput += ",\"taxTotalAmount\":\"" + 0 + "\"";
							Double dblPayatHotel=dblUloRevenue+dblTaxAmount;
							jsonOutput += ",\"dueAmount\":\" "+0+"\"";
							
							purchaseRate=bookingDetailReport.getPurchaseRate();
							if(purchaseRate==null){
								purchaseRate=0.0;
							}
							dblTotalAmount=dblRevenueAmount-purchaseRate;
							jsonOutput += ",\"hotelRevenue\":\"" + purchaseRate + "\"";
	                    }
												
						statusId=bookingDetailReport.getStatusId();
						PmsStatus status=statusController.find(statusId);
						statusName=status.getStatus();
						jsonOutput += ",\"status\":\"" + statusName+ "\"";
						jsonOutput += "}";
					}
				}	
			}
			
			

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	public String propertyBookingReport() throws IOException {
		
		try {
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			this.userId=(Integer)sessionMap.get("userId");
			DecimalFormat df = new DecimalFormat("###.##");
			PmsPropertyManager pmsPropertyController=new PmsPropertyManager();
			ReportManager reportController = new ReportManager();
			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
			PmsStatusManager statusController=new PmsStatusManager();
			PmsSourceManager sourceController=new PmsSourceManager();
			UserLoginManager  userController = new UserLoginManager();
			response.setContentType("application/json");
			DateFormat format = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
			this.filterBookingId=getFilterBookingId();
		    int bookingId=Integer.parseInt(filterBookingId);
		    if(bookingId==0){
		    	java.util.Date dateStart = format.parse(getStrFromDate());
				java.util.Date dateEnd = format.parse(getStrToDate());
			    Calendar calCheckStart=Calendar.getInstance();
			    calCheckStart.setTime(dateStart);
			    java.util.Date checkInDate = calCheckStart.getTime();
			    this.fromDate=new Date(checkInDate.getTime());
			    Calendar calCheckEnd=Calendar.getInstance();
			    calCheckEnd.setTime(dateEnd);
			    java.util.Date checkOutDate = calCheckEnd.getTime();
			    this.toDate=new Date(checkOutDate.getTime());
			    
					
		    }
		    User user = userController.find(this.userId);
		    if(bookingId==0){
		    	if(user.getRole().getRoleId() == 6) {
		    		this.listBookingDetails=reportController.listGuestBookingDetail(getPropertyId(),this.fromDate,this.toDate,this.userId);
		    	}else {
		    		this.listBookingDetails=reportController.listStatusBookingDetail(getPropertyId(),this.fromDate,this.toDate,getStatusId());
		    	}
		    }else if(bookingId>0){
				this.listBookingDetails=reportController.listBookingDetail(Integer.parseInt(this.filterBookingId));
			}	
				
			if(listBookingDetails.size()>0 && !listBookingDetails.isEmpty()){
				for(BookingDetailReport bookingDetailReport:listBookingDetails){
					if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += ",{";
					else
						jsonOutput += "{";
					jsonOutput += "\"bookingId\":\"" + bookingDetailReport.getBookingId()+ "\"";
					if(bookingDetailReport.getOtaBookingId()!=null){
						jsonOutput += ",\"otaBookingId\":\"" + bookingDetailReport.getOtaBookingId()+ "\"";
					}else{
						jsonOutput += ",\"otaBookingId\":\"" + "NA"+ "\"";
					}
					PmsProperty property = pmsPropertyController.find(bookingDetailReport.getPropertyId());
					jsonOutput += ",\"propertyName\":\"" + property.getPropertyName()+ "\"";
					String strFirstName="",firstNameLetterUpperCase="";
					String strLastName="",lastNameLetterUpperCase="";
					strFirstName=bookingDetailReport.getFirstName();
					strLastName=bookingDetailReport.getLastName();
					if(strFirstName!=null && strFirstName!="" && !strFirstName.equalsIgnoreCase("undefined")){
						strFirstName=strFirstName.trim();
						firstNameLetterUpperCase=strFirstName.substring(0, 1).toUpperCase()+strFirstName.substring(1);
						strFirstName=firstNameLetterUpperCase;
					}else{
						strFirstName="-";
					}
					if(strLastName!=null && strLastName!=""  && !strLastName.equalsIgnoreCase("undefined")){
						strLastName=strLastName.trim();
						lastNameLetterUpperCase=strLastName.substring(0, 1).toUpperCase() +  strLastName.substring(1);
						strLastName=lastNameLetterUpperCase;
					}else{
						strLastName="";
					}
//					jsonOutput += ",\"guestId\":\"" + bookingDetailReport.getGuestId() + "\"";
					jsonOutput += ",\"guestName\":\"" + strFirstName+" "+strLastName + "\"";
					
					
					Double dblBaseAmount=0.0,dblTaxAmount=0.0,dblBaseTaxAmount=0.0,dblTotalAmount=0.0,purchaseRate=0.0,
							dblAdvanceAmount=0.0,dblDueAmount=0.0,dblTotalBaseAmount=0.0,dblOtaCommission=0.0,dblOtaTax=0.0,
									dblTotalOtaAmout=0.0,dblRevenueAmount=0.0,dblTotalRevenueTaxAmount=0.0,promotionAmount=0.0,couponAmount=0.0;
					Integer rooms=0,adultCount=0,childCount=0,statusId=0,sourceId=0;
					String strMobileNumber="",statusName="",strEmailId="",sourceName="";
					strMobileNumber=bookingDetailReport.getMobileNumber();
					jsonOutput += ",\"mobileNumber\":\"" + strMobileNumber+ "\"";
					
					strEmailId=bookingDetailReport.getEmailId();
					jsonOutput += ",\"emailId\":\"" + strEmailId+ "\"";
					String strBookingDate="",timeHours="";
					long arrivalDate =bookingDetailReport.getArrivalDate().getTime();
					java.util.Date dateArrival = new java.util.Date(arrivalDate);
					long departureDate =bookingDetailReport.getDepartureDate().getTime();
					java.util.Date dateDeparture = new java.util.Date(departureDate);
					SimpleDateFormat format1 = new SimpleDateFormat("dd-MMM-yyyy");
					SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
					 
					if(bookingDetailReport.getCreatedDate()!=null){
						Timestamp bookingDate=bookingDetailReport.getCreatedDate();
						java.util.Date dateBooking = new java.util.Date(bookingDate.getTime());
						Calendar calDate=Calendar.getInstance();
						calDate.setTime(dateBooking);
						calDate.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
		            	int bookedHours=calDate.get(Calendar.HOUR);
		            	int bookedMinute=calDate.get(Calendar.MINUTE);
		            	int bookedSecond=calDate.get(Calendar.SECOND);
		            	int AMPM=calDate.get(Calendar.AM_PM);
		            	
		            	strBookingDate=format1.format(dateBooking);
		            	String hours="",minutes="",seconds="";
		            	if(bookedHours<10){
		            		hours=String.valueOf(bookedHours);
		            		hours="0"+hours;
		            	}else{
		            		hours=String.valueOf(bookedHours);
		            	}
		            	if(bookedSecond<10){
		            		seconds=String.valueOf(bookedSecond);
		            		seconds="0"+seconds;
		            	}else{
		            		seconds=String.valueOf(bookedSecond);
		            	}
		            	
		            	if(bookedMinute<10){
		            		minutes=String.valueOf(bookedMinute);
		            		minutes="0"+minutes;
		            	}else{
		            		minutes=String.valueOf(bookedMinute);
		            	}
		            	if(AMPM==0){//If the current time is AM
		            		timeHours=strBookingDate+"-"+hours+" : "+minutes+" : "+seconds+" AM ";
		            	}else if(AMPM==1){//If the current time is PM
		            		timeHours=strBookingDate+"-"+hours+" : "+minutes+" : "+seconds+" PM ";
		            	}
		            	
						jsonOutput += ",\"bookingDate\":\"" + timeHours+ "\"";
					}else{
						jsonOutput += ",\"bookingDate\":\"NA\"";
					} 
					
					Timestamp FromDate=bookingDetailReport.getArrivalDate();
					Timestamp ToDate=bookingDetailReport.getDepartureDate();
					
					String strFromDate=format2.format(FromDate);
					String strToDate=format2.format(ToDate);
					String strArrivalDate=format1.format(FromDate);
					String strDepartureDate=format1.format(ToDate);
					
			    	DateTime startdate = DateTime.parse(strFromDate);
			        DateTime enddate = DateTime.parse(strToDate);
			        java.util.Date fromdate = startdate.toDate();
			 		java.util.Date todate = enddate.toDate();
			 		
					int diffInDays = (int) ((todate.getTime() - fromdate.getTime()) / (1000 * 60 * 60 * 24));
					jsonOutput += ",\"checkIn\":\"" + strArrivalDate+ "\"";
					jsonOutput += ",\"checkOut\":\"" + strDepartureDate+ "\"";
					
//					jsonOutput += ",\"accommodationId\":" + bookingDetailReport.getAccommodationId()+ "";
					PropertyAccommodation accommodations=accommodationController.find(bookingDetailReport.getAccommodationId());
					jsonOutput += ",\"accommodationType\":\"" + accommodations.getAccommodationType().trim()+ "\"";
					
					/*String strAccommodationType=null;
					accommodationId=bookingDetailReport.getAccommodationId();
					if(accommodationId>0)
					{
						PropertyAccommodation accommodations=accommodationController.find(accommodationId);
						strAccommodationType=accommodations.getAccommodationType();
						jsonOutput += ",\"accommodationType\":\" "+strAccommodationType+"\"";
						jsonOutput += ",\"accommodationId\":\"" + bookingDetailReport.getAccommodationId()+ "\"";
					}
					else{
						jsonOutput += ",\"accommodationType\":\" ALL\"";
						jsonOutput += ",\"accommodationId\":\"" + bookingDetailReport.getAccommodationId()+ "\"";
					}*/
					
					long lngrooms=bookingDetailReport.getRoomCount();
					rooms=(int)lngrooms;
					if(diffInDays == 0){
						diffInDays = 1;
					}
					jsonOutput += ",\"diffDays\":\"" + diffInDays+ "\"";
					jsonOutput += ",\"rooms\":" + rooms/diffInDays+ "";
					jsonOutput += ",\"adultsCount\":" + bookingDetailReport.getAdultCount()+ "";
					jsonOutput += ",\"childCount\":" + bookingDetailReport.getChildCount() + "";
					//jsonOutput += ",\"advanceAmount\":" + df.format(bookingDetailReport.getAdvanceAmount())+ "";
					
					dblBaseAmount=bookingDetailReport.getAmount();
					dblTaxAmount=bookingDetailReport.getTaxAmount();
					dblAdvanceAmount=bookingDetailReport.getAdvanceAmount();
					dblTotalBaseAmount=dblBaseAmount+dblTaxAmount;
					
					String strTotal=df.format(dblTotalBaseAmount);
					if(bookingDetailReport.getCouponAmount()!=null){
						couponAmount=bookingDetailReport.getCouponAmount();
					}
					
					dblDueAmount=Double.valueOf(strTotal)-dblAdvanceAmount;
					dblBaseTaxAmount=dblBaseAmount+dblTaxAmount-couponAmount;
					
					jsonOutput += ",\"totalAmount\":" + df.format(dblBaseTaxAmount)+ "";
					dblOtaCommission=bookingDetailReport.getOtaCommission();
					dblOtaTax=bookingDetailReport.getOtaTax();
					if(dblOtaCommission!=null && dblOtaCommission>=0.0){
						dblOtaCommission=bookingDetailReport.getOtaCommission();
					}else{
						dblOtaCommission=0.0;
					}
					if(dblOtaTax!=null && dblOtaTax>=0.0){
						dblOtaTax=bookingDetailReport.getOtaTax();
					}else{
						dblOtaTax=0.0;
					}
					
					dblTotalOtaAmout=dblOtaCommission+dblOtaTax;
					
					dblRevenueAmount=dblBaseAmount-dblTotalOtaAmout;
					dblTotalRevenueTaxAmount=dblRevenueAmount+dblTaxAmount;
					purchaseRate=bookingDetailReport.getPurchaseRate();
					if(purchaseRate==null){
						purchaseRate=0.0;
					}
					dblTotalAmount=dblRevenueAmount-purchaseRate;
					jsonOutput += ",\"profitLossAmount\":" + df.format(dblTotalAmount)+ "";
					if((double)dblTotalBaseAmount == (double)dblAdvanceAmount){
						jsonOutput += ",\"paymentRemarks\":\" CASH PAID\"";
					}else{
						jsonOutput += ",\"paymentRemarks\":\" PAY AT HOTEL\"";
					}
					
					statusId=bookingDetailReport.getStatusId();
					PmsStatus status=statusController.find(statusId);
					statusName=status.getStatus();
					jsonOutput += ",\"statusName\":\"" + statusName + "\"";
					sourceId=bookingDetailReport.getSourceId();
					PmsSource source=sourceController.find(sourceId);
					sourceName=source.getSourceName();
					
					jsonOutput += ",\"sourceName\":\"" + sourceName + "\"";
					jsonOutput += "}";
				}
			}			
			
			

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}

	public String getPartnerRevenueReport() throws IOException{

		try {
			this.partnerPropertyId=(Integer)sessionMap.get("partnerPropertyId");
	 		this.selectPartnerPropertyId=(Integer)sessionMap.get("selectedPropertyId");
			if(this.selectPartnerPropertyId!=null){
				this.partnerPropertyId=this.selectPartnerPropertyId;	
			}else{
				this.partnerPropertyId=this.partnerPropertyId;
			}
			
			DateFormat format = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
		    java.util.Date dateStart = format.parse(getStrFromDate());
		    java.util.Date dateEnd = format.parse(getStrToDate());
		   
		    PmsPropertyManager propertyController=new PmsPropertyManager();
		    Calendar calCheckStart=Calendar.getInstance();
		    calCheckStart.setTime(dateStart);
		    java.util.Date checkInDate = calCheckStart.getTime();
		    this.fromDate=new Date(checkInDate.getTime());
		    sessionMap.put("fromDate",fromDate); 
		    
		    Calendar calCheckEnd=Calendar.getInstance();
		    calCheckEnd.setTime(dateEnd);
		    java.util.Date checkOutDate = calCheckEnd.getTime();
		    this.toDate=new Date(checkOutDate.getTime());
		    sessionMap.put("endDate",toDate); 

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			DecimalFormat df = new DecimalFormat("###.##");
			ReportManager reportController = new ReportManager();
			
			
			response.setContentType("application/json");
			Integer accommodationId=0;
			String strAccommodationType=null;
			PropertyAccommodationManager accommodationController=new PropertyAccommodationManager();
			Double totalRoomNights=0.0,dblTotalRevenueTaxAmount=0.0;
			this.listBookingDetails=reportController.listBookingDetail(this.partnerPropertyId,this.getFromDate(),this.getToDate(),this.getAccommodationId());
			if(listBookingDetails.size()>0 && !listBookingDetails.isEmpty()){
				for(BookingDetailReport bookingDetailReport:listBookingDetails){
					Integer rooms=0,noOfRooms=0; 
					SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
					
					Timestamp FromDate=bookingDetailReport.getArrivalDate();
					Timestamp ToDate=bookingDetailReport.getDepartureDate();
					
					String strFromDate=format2.format(FromDate);
					String strToDate=format2.format(ToDate);
					
			    	DateTime startdate = DateTime.parse(strFromDate);
			        DateTime enddate = DateTime.parse(strToDate);
			        java.util.Date fromdate = startdate.toDate();
			 		java.util.Date todate = enddate.toDate();
			 		
			 		int diffInDays  = (int) ((todate.getTime() - fromdate.getTime()) / (1000 * 60 * 60 * 24));
			 		
			 		long lngrooms=bookingDetailReport.getRoomCount();
					rooms=(int)lngrooms;
					noOfRooms=rooms/diffInDays;
			 		Integer diffRoomInDays=noOfRooms*diffInDays;
			 		
			 		totalRoomNights+=Double.valueOf(String.valueOf(diffRoomInDays));
			 		
			 		Double dblBaseAmount=0.0,dblTaxAmount=0.0;
					Double dblOtaCommission=0.0,dblOtaTax=0.0,dblTotalOtaAmout=0.0,dblRevenueAmount=0.0;
					
					
					dblBaseAmount=bookingDetailReport.getAmount();
					dblTaxAmount=bookingDetailReport.getTaxAmount();
					if(dblBaseAmount!=null && dblBaseAmount>=0.0){
						dblBaseAmount=bookingDetailReport.getAmount();
					}else{
						dblBaseAmount=0.0;
					}
					
					if(dblTaxAmount!=null && dblTaxAmount>=0.0){
						dblTaxAmount=bookingDetailReport.getTaxAmount();
					}else{
						dblTaxAmount=0.0;
					}
					
					
					dblOtaCommission=bookingDetailReport.getOtaCommission();
					dblOtaTax=bookingDetailReport.getOtaTax();
					if(dblOtaCommission!=null && dblOtaCommission>=0.0){
						dblOtaCommission=bookingDetailReport.getOtaCommission();
					}else{
						dblOtaCommission=0.0;
					}
					if(dblOtaTax!=null && dblOtaTax>=0.0){
						dblOtaTax=bookingDetailReport.getOtaTax();
					}else{
						dblOtaTax=0.0;
					}
					
					dblTotalOtaAmout=dblOtaCommission+dblOtaTax;
					
					dblRevenueAmount=dblBaseAmount-dblTotalOtaAmout;
					
					dblTotalRevenueTaxAmount+=dblRevenueAmount+dblTaxAmount;
					
				}
			}
			
			
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				
				accommodationId=this.getAccommodationId();
				if(accommodationId>0)
				{
					PropertyAccommodation accommodations=accommodationController.find(accommodationId);
					strAccommodationType=accommodations.getAccommodationType();
					jsonOutput += "\"accommodationType\":\" "+strAccommodationType.trim()+"\"";
				}
				else{
					jsonOutput += "\"accommodationType\":\" ALL\"";
				}
				
				String start = new SimpleDateFormat("dd-MMM-yyyy").format(this.getFromDate());
				String end = new SimpleDateFormat("dd-MMM-yyyy").format(this.getToDate());
				jsonOutput += ",\"startDate\":\"" + start + "\"";
				jsonOutput += ",\"endDate\":\"" + end + "\"";
				
				jsonOutput += ",\"nights\":\"" + totalRoomNights.intValue()+ "\"";
				jsonOutput += ",\"amount\":\"" + df.format(dblTotalRevenueTaxAmount)+ "\"";
							
				
				jsonOutput += "}";	
			
			
			

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	
	}
	
	public String RevenueReport() throws IOException {
		try {
			
			DateFormat format = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
		    java.util.Date dateStart = format.parse(getStrFromDate());
		    java.util.Date dateEnd = format.parse(getStrToDate());
		   
		    PmsPropertyManager propertyController=new PmsPropertyManager();
		    Calendar calCheckStart=Calendar.getInstance();
		    calCheckStart.setTime(dateStart);
		    java.util.Date checkInDate = calCheckStart.getTime();
		    this.fromDate=new Date(checkInDate.getTime());
		    sessionMap.put("fromDate",fromDate); 
		    
		    Calendar calCheckEnd=Calendar.getInstance();
		    calCheckEnd.setTime(dateEnd);
		    java.util.Date checkOutDate = calCheckEnd.getTime();
		    this.toDate=new Date(checkOutDate.getTime());
		    sessionMap.put("endDate",toDate); 

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			DecimalFormat df = new DecimalFormat("###.##");
			ReportManager reportController = new ReportManager();
			
			
			response.setContentType("application/json");
			Integer accommodationId=0;
			String strAccommodationType=null;
			PmsProperty pmsProperty=propertyController.find(this.propertyId);
            if(pmsProperty.getLocationType()!=null){
				this.locationType=pmsProperty.getLocationType().getLocationTypeName();
				this.locationTypeId=pmsProperty.getLocationType().getLocationTypeId();
			}
			PropertyAccommodationManager accommodationController=new PropertyAccommodationManager();
			Double totalRoomNights=0.0,dblTotalRevenueTaxAmount=0.0;
			this.listBookingDetails=reportController.listBookingDetail(getPropertyId(),this.getFromDate(),this.getToDate(),this.getAccommodationId());
			if(listBookingDetails.size()>0 && !listBookingDetails.isEmpty()){
				for(BookingDetailReport bookingDetailReport:listBookingDetails){
					Integer rooms=0,noOfRooms=0; 
					SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
					
					Timestamp FromDate=bookingDetailReport.getArrivalDate();
					Timestamp ToDate=bookingDetailReport.getDepartureDate();
					
					String strFromDate=format2.format(FromDate);
					String strToDate=format2.format(ToDate);
					
			    	DateTime startdate = DateTime.parse(strFromDate);
			        DateTime enddate = DateTime.parse(strToDate);
			        java.util.Date fromdate = startdate.toDate();
			 		java.util.Date todate = enddate.toDate();
			 		
			 		int diffInDays  = (int) ((todate.getTime() - fromdate.getTime()) / (1000 * 60 * 60 * 24));
			 		
			 		long lngrooms=bookingDetailReport.getRoomCount();
					rooms=(int)lngrooms;
					noOfRooms=rooms/diffInDays;
			 		Integer diffRoomInDays=noOfRooms*diffInDays;
			 		
			 		totalRoomNights+=Double.valueOf(String.valueOf(diffRoomInDays));
			 		
			 		Double dblBaseAmount=0.0,dblTaxAmount=0.0,dblPromotionAmount=0.0,dblCouponAmount=0.0,purchaseRate=0.0;
					Double dblOtaCommission=0.0,dblOtaTax=0.0,dblTotalOtaAmout=0.0,dblRevenueAmount=0.0;
					
					PropertyAccommodation propertyAccommodation = accommodationController.find(bookingDetailReport.getAccommodationId());
                    if(propertyAccommodation.getPropertyContractType()!=null){
                     	contractType=propertyAccommodation.getPropertyContractType().getContractTypeName();
                     	contractTypeId=propertyAccommodation.getPropertyContractType().getContractTypeId();
                    }
                    if(contractTypeId==7){//B-join
                 		purchaseRate= bookingDetailReport.getPurchaseRate()==null?0:bookingDetailReport.getPurchaseRate();
                 		contractType="join";
                 	}else if(contractTypeId==6){//B-sure
                 		purchaseRate= bookingDetailReport.getPurchaseRate()==null?0:bookingDetailReport.getPurchaseRate();
                 		contractType="sure";
                 	}else if(contractTypeId==8){//B-stay
                 		purchaseRate= bookingDetailReport.getPurchaseRate()==null?0:bookingDetailReport.getPurchaseRate();
                 		contractType="stay";
                 	}else if(contractTypeId==9){//B-join flat
                 		purchaseRate=bookingDetailReport.getAmount();
                 		contractType="flat";
                 	}
                    if(contractType.equalsIgnoreCase("flat")){
                    	dblBaseAmount=purchaseRate;
    					dblTaxAmount=bookingDetailReport.getTaxAmount();
    					if(dblBaseAmount!=null && dblBaseAmount>=0.0){
    						dblBaseAmount=bookingDetailReport.getAmount();
    					}else{
    						dblBaseAmount=0.0;
    					}
    					
    					if(dblTaxAmount!=null && dblTaxAmount>=0.0){
    						dblTaxAmount=bookingDetailReport.getTaxAmount();
    					}else{
    						dblTaxAmount=0.0;
    					}
    					if(bookingDetailReport.getCouponAmount()!=null){
    						dblCouponAmount=bookingDetailReport.getCouponAmount();
    					}else{
    						dblCouponAmount=0.0;
    					}
    					
    					dblOtaCommission=bookingDetailReport.getOtaCommission();
    					dblOtaTax=bookingDetailReport.getOtaTax();
    					if(dblOtaCommission!=null && dblOtaCommission>=0.0){
    						dblOtaCommission=bookingDetailReport.getOtaCommission();
    					}else{
    						dblOtaCommission=0.0;
    					}
    					if(dblOtaTax!=null && dblOtaTax>=0.0){
    						dblOtaTax=bookingDetailReport.getOtaTax();
    					}else{
    						dblOtaTax=0.0;
    					}
    					
    					dblTotalOtaAmout=dblOtaCommission+dblOtaTax;
    					
    					dblRevenueAmount=dblBaseAmount-dblTotalOtaAmout;
    					
    					
    					dblTotalRevenueTaxAmount+=dblRevenueAmount+dblTaxAmount;	
                    }else{
                    	dblBaseAmount=purchaseRate;
                    	dblTotalRevenueTaxAmount+=dblBaseAmount;
                    }
                    
					
					
				}
			}
			
			
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				
				if(reportEnabled){
					int i=0;
					while(i<revenueReportKey.length){
						PmsProperty property=propertyController.find(getPropertyId());   
						jsonOutput += "\""+revenueReportKey[i++]+"\":\"" + property.getPropertyName()+ "\"";
						jsonOutput += ",\""+revenueReportKey[i++]+"\":\"" + property.getLocation().getLocationName() + "\"";
						
						jsonOutput += ",\""+revenueReportKey[i++]+"\":\"" + totalRoomNights.intValue()+ "\"";
						jsonOutput += ",\""+revenueReportKey[i++]+"\":\"" + df.format(dblTotalRevenueTaxAmount)+ "\"";	
					}
					
				}else{
					accommodationId=this.getAccommodationId();
					if(accommodationId>0)
					{
						PropertyAccommodation accommodations=accommodationController.find(accommodationId);
						strAccommodationType=accommodations.getAccommodationType();
						jsonOutput += "\"accommodationType\":\" "+strAccommodationType.trim()+"\"";
					}
					else{
						jsonOutput += "\"accommodationType\":\" ALL\"";
					}
					
					String start = new SimpleDateFormat("dd-MMM-yyyy").format(this.getFromDate());
					String end = new SimpleDateFormat("dd-MMM-yyyy").format(this.getToDate());
					jsonOutput += ",\"startDate\":\"" + start + "\"";
					jsonOutput += ",\"endDate\":\"" + end + "\"";
					
					jsonOutput += ",\"nights\":\"" + totalRoomNights.intValue()+ "\"";
					jsonOutput += ",\"amount\":\"" + df.format(dblTotalRevenueTaxAmount)+ "\"";

				}
							
				
				jsonOutput += "}";	
			
			
			

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	

	public String userGuestReport() throws IOException {
		try {
			
			DateFormat format = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
		    java.util.Date dateStart = format.parse(getStrFromDate());
		    java.util.Date dateEnd = format.parse(getStrToDate());
		   
		    
		    Calendar calCheckStart=Calendar.getInstance();
		    calCheckStart.setTime(dateStart);
		    java.util.Date checkInDate = calCheckStart.getTime();
		    this.fromDate=new Date(checkInDate.getTime());
		    sessionMap.put("fromDate",fromDate); 
		    
		    Calendar calCheckEnd=Calendar.getInstance();
		    calCheckEnd.setTime(dateEnd);
		    java.util.Date checkOutDate = calCheckEnd.getTime();
		    this.toDate=new Date(checkOutDate.getTime());
		    sessionMap.put("endDate",toDate); 

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			ReportManager reportController = new ReportManager();
			
			response.setContentType("application/json");
			
			List<UserGuestDetailReport> listUserGuestAll=new ArrayList<UserGuestDetailReport>();
			response.setContentType("application/json");
			List<UserGuestDetailReport> userList=reportController.UserDetails(this.fromDate,this.toDate);
			String strPhoneNumber="",strAddress1="",strAddress2="",strEmailId="",strGuestUser="";
			String strViewPhoneNumber="",strViewAddress1="",strViewAddress2="",strViewEmailId="",strViewGuestUser="";
			List<UserGuestDetailReport> guestList=reportController.GuestDetails(this.fromDate,this.toDate);
			
			listUserGuestAll.addAll(guestList);
			listUserGuestAll.addAll(userList);
			
			if(listUserGuestAll.size()>0 && !listUserGuestAll.isEmpty()){
				for (UserGuestDetailReport userguest : listUserGuestAll) {

					
					if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += ",{";
					else
						jsonOutput += "{";
					
					strPhoneNumber=userguest.getPhoneNumber();
					strAddress1=userguest.getAddress1();
					strAddress2=userguest.getAddress2();
					strGuestUser=userguest.getUserName();
					strEmailId=userguest.getUserName();
					if(strPhoneNumber==null || strPhoneNumber==""){
						strViewPhoneNumber="NA";
					}else{
						strViewPhoneNumber=userguest.getPhoneNumber().trim();
					}
					
					if(strGuestUser==null || strGuestUser==""){
						strViewGuestUser="NA";
					}else{
						strViewGuestUser=userguest.getUserName().trim();
					}
					
					if(strEmailId==null || strEmailId==""){
						strViewEmailId="NA";
					}else{
						strViewEmailId=userguest.getEmailId().trim();
					}
					if(reportEnabled){
						int i=0;
						while(i<userReportKey.length){
							jsonOutput += "\""+userReportKey[i++]+"\":\"" + strViewGuestUser + "\"";
							jsonOutput += ",\""+userReportKey[i++]+"\":\"" + strViewEmailId + "\"";
							
							jsonOutput += ",\""+userReportKey[i++]+"\":\"" + strViewPhoneNumber + "\"";
						}
					}else{
						jsonOutput += "\"userName\":\"" + strViewGuestUser + "\"";
						jsonOutput += ",\"emailId\":\"" + strViewEmailId + "\"";
						
						jsonOutput += ",\"phoneNumber\":\"" + strViewPhoneNumber + "\"";	
					}
					
					
					
					
					jsonOutput += "}";

				}
			}
			
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	public String UserReport() throws IOException {
		try {
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			ReportManager reportController = new ReportManager();
			
			List<UserGuestDetailReport> listUserGuestAll=new ArrayList<UserGuestDetailReport>();
			response.setContentType("application/json");
			List<UserGuestDetailReport> userList=reportController.UserDetails();
			String strPhoneNumber="",strAddress1="",strAddress2="";
			List<UserGuestDetailReport> guestList=reportController.GuestDetails();
			
			listUserGuestAll.addAll(guestList);
			listUserGuestAll.addAll(userList);
			
			if(listUserGuestAll.size()>0 && !listUserGuestAll.isEmpty()){
				for (UserGuestDetailReport userguest : listUserGuestAll) {

					
					if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += ",{";
					else
						jsonOutput += "{";
					
					strPhoneNumber=userguest.getPhoneNumber();
					strAddress1=userguest.getAddress1();
					strAddress2=userguest.getAddress2();
					
					if(userguest.getPhoneNumber()==null ){
						strPhoneNumber="NA";
					}
					
					
					
					jsonOutput += "\"userName\":\"" + userguest.getUserName().trim()+ "\"";
					jsonOutput += ",\"emailId\":\"" + userguest.getEmailId().trim()+ "\"";
					
					jsonOutput += ",\"phoneNumber\":\"" + strPhoneNumber.trim()+ "\"";
					
					
					jsonOutput += "}";

				}
			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	public String cancellationReport() throws IOException
	{
		try{
			this.filterDateName=getFilterDateName();
			this.filterBookingId=getFilterBookingId();
			int bookingId=Integer.parseInt(filterBookingId);
			if(bookingId==0){
				DateFormat format = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
			    java.util.Date dateStart = format.parse(getStrFromDate());
			    java.util.Date dateEnd = format.parse(getStrToDate());
			   
			    
			    Calendar calCheckStart=Calendar.getInstance();
			    calCheckStart.setTime(dateStart);
			    java.util.Date checkInDate = calCheckStart.getTime();
			    this.fromDate=new Date(checkInDate.getTime());
			    sessionMap.put("fromDate",fromDate); 
			    
			    Calendar calCheckEnd=Calendar.getInstance();
			    calCheckEnd.setTime(dateEnd);
			    java.util.Date checkOutDate = calCheckEnd.getTime();
			    this.toDate=new Date(checkOutDate.getTime());
			    sessionMap.put("endDate",toDate); 
			}
			DecimalFormat df = new DecimalFormat("###.##");
		    SimpleDateFormat format3 = new SimpleDateFormat("dd-MMM-yyyy-HH:mm:ss");
		    SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			ReportManager reportController = new ReportManager();
			PropertyAccommodationManager accommodationController=new PropertyAccommodationManager();
			response.setContentType("application/json");
			UserLoginManager userController=new UserLoginManager();
			
			SimpleDateFormat format1 = new SimpleDateFormat("dd-MMM-yyyy");
			
			if(bookingId==0){
				this.listBookingDetails=reportController.listCancellation(getPropertyId(),this.getFromDate(),this.getToDate(),this.getAccommodationId(),this.filterDateName,Integer.parseInt(this.filterBookingId));
			}else if(bookingId>0){
				this.listBookingDetails=reportController.listCancellation(getPropertyId(),Integer.parseInt(this.filterBookingId));
			}
//			this.listBookingDetails=reportController.listCancellation((Integer) sessionMap.get("propertyId"),this.getFromDate(),this.getToDate(),this.getAccommodationId());
			if(reportEnabled){
				if(listBookingDetails.size()>0 && !listBookingDetails.isEmpty()){
					for(BookingDetailReport bookingDetailReport:listBookingDetails){
						int i=0;
						while(i<cancellationReportKey.length){

							if (!jsonOutput.equalsIgnoreCase(""))
								jsonOutput += ",{";
							else
								jsonOutput += "{";
							
							
							jsonOutput += "\""+cancellationReportKey[i++]+"\":\"" + bookingDetailReport.getBookingId()+ "\"";//booking id
							
							String timeHours="",strBookingDate="";
							if(bookingDetailReport.getCreatedDate()!=null){
								Timestamp bookingDate=bookingDetailReport.getCreatedDate();
								java.util.Date dateBooking = new java.util.Date(bookingDate.getTime());
								Calendar calDate=Calendar.getInstance();
								calDate.setTime(dateBooking);
								calDate.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
				            	int bookedHours=calDate.get(Calendar.HOUR);
				            	int bookedMinute=calDate.get(Calendar.MINUTE);
				            	int bookedSecond=calDate.get(Calendar.SECOND);
				            	int AMPM=calDate.get(Calendar.AM_PM);
				            	
				            	strBookingDate=format1.format(dateBooking);
				            	String hours="",minutes="",seconds="";
				            	if(bookedHours<10){
				            		hours=String.valueOf(bookedHours);
				            		hours="0"+hours;
				            	}else{
				            		hours=String.valueOf(bookedHours);
				            	}
				            	if(bookedSecond<10){
				            		seconds=String.valueOf(bookedSecond);
				            		seconds="0"+seconds;
				            	}else{
				            		seconds=String.valueOf(bookedSecond);
				            	}
				            	
				            	if(bookedMinute<10){
				            		minutes=String.valueOf(bookedMinute);
				            		minutes="0"+minutes;
				            	}else{
				            		minutes=String.valueOf(bookedMinute);
				            	}
				            	if(AMPM==0){//If the current time is AM
				            		timeHours=strBookingDate+"-"+hours+" : "+minutes+" : "+seconds+" AM ";
				            	}else if(AMPM==1){//If the current time is PM
				            		timeHours=strBookingDate+"-"+hours+" : "+minutes+" : "+seconds+" PM ";
				            	}
				            	
								strBookingDate=format3.format(dateBooking); 
								jsonOutput += ",\""+cancellationReportKey[i++]+"\":\"" + timeHours+ "\"";//booking date
							}else{
								jsonOutput += ",\""+cancellationReportKey[i++]+"\":\"NA\"";
								
							}
							
							if(bookingDetailReport.getModifiedDate()!=null){

								Timestamp bookingDate=bookingDetailReport.getModifiedDate();
								java.util.Date dateBooking = new java.util.Date(bookingDate.getTime());
								Calendar calDate=Calendar.getInstance();
								calDate.setTime(dateBooking);
								calDate.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
				            	int bookedHours=calDate.get(Calendar.HOUR);
				            	int bookedMinute=calDate.get(Calendar.MINUTE);
				            	int bookedSecond=calDate.get(Calendar.SECOND);
				            	int AMPM=calDate.get(Calendar.AM_PM);
				            	
				            	strBookingDate=format1.format(dateBooking);
				            	String hours="",minutes="",seconds="";
				            	if(bookedHours<10){
				            		hours=String.valueOf(bookedHours);
				            		hours="0"+hours;
				            	}else{
				            		hours=String.valueOf(bookedHours);
				            	}
				            	if(bookedSecond<10){
				            		seconds=String.valueOf(bookedSecond);
				            		seconds="0"+seconds;
				            	}else{
				            		seconds=String.valueOf(bookedSecond);
				            	}
				            	
				            	if(bookedMinute<10){
				            		minutes=String.valueOf(bookedMinute);
				            		minutes="0"+minutes;
				            	}else{
				            		minutes=String.valueOf(bookedMinute);
				            	}
				            	if(AMPM==0){//If the current time is AM
				            		timeHours=strBookingDate+"-"+hours+" : "+minutes+" : "+seconds+" AM ";
				            	}else if(AMPM==1){//If the current time is PM
				            		timeHours=strBookingDate+"-"+hours+" : "+minutes+" : "+seconds+" PM ";
				            	}
				            	
								strBookingDate=format3.format(dateBooking); 
								jsonOutput += ",\""+cancellationReportKey[i++]+"\":\"" + timeHours+ "\"";//booking date
							
							}else{
								jsonOutput += ",\""+cancellationReportKey[i++]+"\":\"NA\"";
							}
							
							String strFirstName="",firstNameLetterUpperCase="",strRemarks="";
							String strLastName="",lastNameLetterUpperCase="";
							strFirstName=bookingDetailReport.getFirstName();
							strLastName=bookingDetailReport.getLastName();
							if(strFirstName!=null && strFirstName!="" && !strFirstName.equalsIgnoreCase("undefined")){
								strFirstName=strFirstName.trim();
								firstNameLetterUpperCase=strFirstName.substring(0, 1).toUpperCase()+strFirstName.substring(1);
								strFirstName=firstNameLetterUpperCase;
							}else{
								strFirstName="NA";
							}
							if(strLastName!=null && strLastName!="" && !strLastName.equalsIgnoreCase("undefined")){
								strLastName=strLastName.trim();
								lastNameLetterUpperCase=strLastName.substring(0, 1).toUpperCase() +  strLastName.substring(1);
								strLastName=lastNameLetterUpperCase;
							}else{
								strLastName="";
							}
							
							jsonOutput += ",\""+cancellationReportKey[i++]+"\":\"" + strFirstName.trim()+" "+strLastName.trim() + "\"";//guest name
							
							String strMobileNumber="",strEmailId="";
							strMobileNumber=bookingDetailReport.getMobileNumber();
							jsonOutput += ",\""+cancellationReportKey[i++]+"\":\"" + strMobileNumber.trim()+ "\"";//mobile number
							
							strEmailId=bookingDetailReport.getEmailId();
							jsonOutput += ",\""+cancellationReportKey[i++]+"\":\"" + strEmailId.trim()+ "\"";//email
							
							Double dblBaseAmount=0.0,dblTaxAmount=0.0,dblTotalAmount=0.0,dblAdvanceAmount=0.0;
							Integer rooms=0,noOfRooms=0,sourceId=0;
							long arrivalDate =bookingDetailReport.getArrivalDate().getTime();
							java.util.Date dateArrival = new java.util.Date(arrivalDate);
							String strArrivalDate=format1.format(dateArrival); 

							jsonOutput += ",\""+cancellationReportKey[i++]+"\":\"" + strArrivalDate+ "\"";//check in
							
							long departureDate =bookingDetailReport.getDepartureDate().getTime();
							java.util.Date dateDeparture = new java.util.Date(departureDate);
							String strDepartureDate=format1.format(dateDeparture); 
							
							jsonOutput += ",\""+cancellationReportKey[i++]+"\":\"" + strDepartureDate+ "\"";//check out
							
							String strAccommodationType=null;
							Integer accommId=0;
							accommId=bookingDetailReport.getAccommodationId();
							if(accommId>0)
							{
								PropertyAccommodation accommodations=accommodationController.find(accommId);
								strAccommodationType=accommodations.getAccommodationType();
								accommId=accommodations.getAccommodationId();
								jsonOutput += ",\""+cancellationReportKey[i++]+"\":\" "+strAccommodationType.trim()+"\"";// accommodation type
							}
							
							Timestamp FromDate=bookingDetailReport.getArrivalDate();
							Timestamp ToDate=bookingDetailReport.getDepartureDate();
							Timestamp cancelDate=bookingDetailReport.getModifiedDate();
							java.util.Date canceldate =null;
							String strFromDate=format2.format(FromDate);
							String strToDate=format2.format(ToDate);
							if(bookingDetailReport.getModifiedDate()!=null){
								String strModifiedDate=format2.format(cancelDate);
						        DateTime modifieddate = DateTime.parse(strModifiedDate);
						        canceldate = modifieddate.toDate();
							}
					    	DateTime startdate = DateTime.parse(strFromDate);
					        DateTime enddate = DateTime.parse(strToDate);
					        java.util.Date fromdate = startdate.toDate();
					 		java.util.Date todate = enddate.toDate();
					 		
							int diffInDays = (int) ((todate.getTime() - fromdate.getTime()) / (1000 * 60 * 60 * 24));
							
							dblBaseAmount=bookingDetailReport.getAmount();
							dblTaxAmount=bookingDetailReport.getTaxAmount();
							long lngRooms=bookingDetailReport.getRoomCount();
							
							rooms=(int)lngRooms;
							noOfRooms=rooms/diffInDays;
					 		Integer diffRoomInDays=noOfRooms*diffInDays;
							
							jsonOutput += ",\""+cancellationReportKey[i++]+"\":\"" + rooms/diffInDays+ "\"";//rooms
							jsonOutput += ",\""+cancellationReportKey[i++]+"\":\"" + diffInDays+ "\"";//nights
							jsonOutput += ",\""+cancellationReportKey[i++]+"\":\"" + diffRoomInDays+ "\"";//roomnights
							dblTotalAmount=dblBaseAmount+dblTaxAmount;
							jsonOutput += ",\""+cancellationReportKey[i++]+"\":\"" + df.format(dblBaseAmount)+ "\"";//revenue
							jsonOutput += ",\""+cancellationReportKey[i++]+"\":\"" + df.format(dblTaxAmount)+ "\"";//tax
							jsonOutput += ",\""+cancellationReportKey[i++]+"\":\"" + df.format(dblTotalAmount)+ "\"";//total revenue
							
							dblAdvanceAmount=bookingDetailReport.getAdvanceAmount();
							if(dblAdvanceAmount!=null  && dblAdvanceAmount>=0.0){
								dblAdvanceAmount=bookingDetailReport.getAdvanceAmount();
							}else{
								if(sourceId==1){
									dblAdvanceAmount=dblTotalAmount;
								}else{
									dblAdvanceAmount=0.0;
								}
							}
							
							int checkDiffInDays = 0;
							if(bookingDetailReport.getModifiedDate()!=null){
								checkDiffInDays=(int) ((fromdate.getTime() - canceldate.getTime()) / (1000 * 60 * 60 * 24));	
							}
							
				        	
							
				        	if(checkDiffInDays<=1){
								strRemarks="No Refund";
								jsonOutput += ",\""+cancellationReportKey[i++]+"\":\"" + dblAdvanceAmount + "\"";//charges
								jsonOutput += ",\""+cancellationReportKey[i++]+"\":\"" + (dblAdvanceAmount-dblAdvanceAmount)+ "\"";//reduced amount
								jsonOutput += ",\""+cancellationReportKey[i++]+"\":\"" + strRemarks+ "\"";//remarks
							}else if(checkDiffInDays>=2){
								Double dblDiscountPer=0.0; 
								Double dblDiscountAmt=0.0;
								strRemarks="No Cancellation Fee";
								dblDiscountAmt=dblTotalAmount*dblDiscountPer/100;
								
								jsonOutput += ",\""+cancellationReportKey[i++]+"\":\"" + dblDiscountAmt + "\"";
								jsonOutput += ",\""+cancellationReportKey[i++]+"\":\"" + (dblTotalAmount-dblDiscountAmt)+ "\"";
								jsonOutput += ",\""+cancellationReportKey[i++]+"\":\"" + strRemarks+ "\"";
							}
							
							
							Integer loginUserId;
							loginUserId=bookingDetailReport.getModifiedBy();
							if(loginUserId!=null){
								loginUserId=bookingDetailReport.getModifiedBy();
								User userDetails=userController.find(loginUserId);
								jsonOutput += ",\""+cancellationReportKey[i++]+"\":\"" + userDetails.getUserName()+ "\"";//user name
							}else{
								jsonOutput += ",\""+cancellationReportKey[i++]+"\":\"NA\"";
							}
							
							jsonOutput += "}";
						
						}
					}
				}
			
			}else{
				if(listBookingDetails.size()>0 && !listBookingDetails.isEmpty()){
					for(BookingDetailReport bookingDetailReport:listBookingDetails){
						if (!jsonOutput.equalsIgnoreCase(""))
							jsonOutput += ",{";
						else
							jsonOutput += "{";
						
						
						jsonOutput += "\"bookingId\":\"" + bookingDetailReport.getBookingId()+ "\"";
						
						String strFirstName="",firstNameLetterUpperCase="",strRemarks="";
						String strLastName="",lastNameLetterUpperCase="",strBookingDate="";
						strFirstName=bookingDetailReport.getFirstName();
						strLastName=bookingDetailReport.getLastName();
						if(strFirstName!=null && strFirstName!="" && !strFirstName.equalsIgnoreCase("undefined")){
							strFirstName=strFirstName.trim();
							firstNameLetterUpperCase=strFirstName.substring(0, 1).toUpperCase()+strFirstName.substring(1);
							strFirstName=firstNameLetterUpperCase;
						}else{
							strFirstName="NA";
						}
						if(strLastName!=null && strLastName!="" && !strLastName.equalsIgnoreCase("undefined")){
							strLastName=strLastName.trim();
							lastNameLetterUpperCase=strLastName.substring(0, 1).toUpperCase() +  strLastName.substring(1);
							strLastName=lastNameLetterUpperCase;
						}else{
							strLastName="";
						}
						
						jsonOutput += ",\"guestName\":\"" + strFirstName.trim()+" "+strLastName.trim() + "\"";
						
						String strMobileNumber="",strEmailId="";
						strMobileNumber=bookingDetailReport.getMobileNumber();
						jsonOutput += ",\"mobileNumber\":\"" + strMobileNumber.trim()+ "\"";
						
						strEmailId=bookingDetailReport.getEmailId();
						jsonOutput += ",\"emailId\":\"" + strEmailId.trim()+ "\"";
						
						Double dblBaseAmount=0.0,dblTaxAmount=0.0,dblTotalAmount=0.0,dblAdvanceAmount=0.0;
						Integer rooms=0,noOfRooms=0,sourceId=0;
						long arrivalDate =bookingDetailReport.getArrivalDate().getTime();
						java.util.Date dateArrival = new java.util.Date(arrivalDate);
						String strArrivalDate=format1.format(dateArrival); 
						
						
						String timeHours="";
						if(bookingDetailReport.getCreatedDate()!=null){
							Timestamp bookingDate=bookingDetailReport.getCreatedDate();
							java.util.Date dateBooking = new java.util.Date(bookingDate.getTime());
							Calendar calDate=Calendar.getInstance();
							calDate.setTime(dateBooking);
							calDate.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
			            	int bookedHours=calDate.get(Calendar.HOUR);
			            	int bookedMinute=calDate.get(Calendar.MINUTE);
			            	int bookedSecond=calDate.get(Calendar.SECOND);
			            	int AMPM=calDate.get(Calendar.AM_PM);
			            	
			            	strBookingDate=format1.format(dateBooking);
			            	String hours="",minutes="",seconds="";
			            	if(bookedHours<10){
			            		hours=String.valueOf(bookedHours);
			            		hours="0"+hours;
			            	}else{
			            		hours=String.valueOf(bookedHours);
			            	}
			            	if(bookedSecond<10){
			            		seconds=String.valueOf(bookedSecond);
			            		seconds="0"+seconds;
			            	}else{
			            		seconds=String.valueOf(bookedSecond);
			            	}
			            	
			            	if(bookedMinute<10){
			            		minutes=String.valueOf(bookedMinute);
			            		minutes="0"+minutes;
			            	}else{
			            		minutes=String.valueOf(bookedMinute);
			            	}
			            	if(AMPM==0){//If the current time is AM
			            		timeHours=strBookingDate+"-"+hours+" : "+minutes+" : "+seconds+" AM ";
			            	}else if(AMPM==1){//If the current time is PM
			            		timeHours=strBookingDate+"-"+hours+" : "+minutes+" : "+seconds+" PM ";
			            	}
			            	
							strBookingDate=format3.format(dateBooking); 
							jsonOutput += ",\"bookingDate\":\"" + timeHours+ "\"";
						}else{
							jsonOutput += ",\"bookingDate\":\"NA\"";
						}
						
						jsonOutput += ",\"checkIn\":\"" + strArrivalDate+ "\"";
						
						long departureDate =bookingDetailReport.getDepartureDate().getTime();
						java.util.Date dateDeparture = new java.util.Date(departureDate);
						String strDepartureDate=format1.format(dateDeparture); 
						
						jsonOutput += ",\"checkOut\":\"" + strDepartureDate+ "\"";
						
						String strAccommodationType=null;
						Integer accommId=0;
						accommId=bookingDetailReport.getAccommodationId();
						if(accommId>0)
						{
							PropertyAccommodation accommodations=accommodationController.find(accommId);
							strAccommodationType=accommodations.getAccommodationType();
							accommId=accommodations.getAccommodationId();
							jsonOutput += ",\"accommodationType\":\" "+strAccommodationType.trim()+"\"";
						}
						
						Timestamp FromDate=bookingDetailReport.getArrivalDate();
						Timestamp ToDate=bookingDetailReport.getDepartureDate();
						Timestamp cancelDate=bookingDetailReport.getModifiedDate();
						java.util.Date canceldate =null;
						String strFromDate=format2.format(FromDate);
						String strToDate=format2.format(ToDate);
						if(bookingDetailReport.getModifiedDate()!=null){
							String strModifiedDate=format2.format(cancelDate);
					        DateTime modifieddate = DateTime.parse(strModifiedDate);
					        canceldate = modifieddate.toDate();
						}
				    	DateTime startdate = DateTime.parse(strFromDate);
				        DateTime enddate = DateTime.parse(strToDate);
				        java.util.Date fromdate = startdate.toDate();
				 		java.util.Date todate = enddate.toDate();
				 		
						int diffInDays = (int) ((todate.getTime() - fromdate.getTime()) / (1000 * 60 * 60 * 24));
						
						dblBaseAmount=bookingDetailReport.getAmount();
						dblTaxAmount=bookingDetailReport.getTaxAmount();
						long lngRooms=bookingDetailReport.getRoomCount();
						
						rooms=(int)lngRooms;
						noOfRooms=rooms/diffInDays;
				 		Integer diffRoomInDays=noOfRooms*diffInDays;
						
						jsonOutput += ",\"rooms\":\"" + rooms/diffInDays+ "\"";
						jsonOutput += ",\"diffDays\":\"" + diffInDays+ "\"";
						jsonOutput += ",\"roomNights\":\"" + diffRoomInDays+ "\"";
						dblTotalAmount=dblBaseAmount+dblTaxAmount;
						jsonOutput += ",\"amount\":\"" + df.format(dblTotalAmount)+ "\"";
						
						dblAdvanceAmount=bookingDetailReport.getAdvanceAmount();
						if(dblAdvanceAmount!=null  && dblAdvanceAmount>=0.0){
							dblAdvanceAmount=bookingDetailReport.getAdvanceAmount();
						}else{
							if(sourceId==1){
								dblAdvanceAmount=dblTotalAmount;
							}else{
								dblAdvanceAmount=0.0;
							}
						}
						
						int checkDiffInDays = 0;
						if(bookingDetailReport.getModifiedDate()!=null){
							checkDiffInDays=(int) ((fromdate.getTime() - canceldate.getTime()) / (1000 * 60 * 60 * 24));	
						}
						
			        	
						
			        	if(checkDiffInDays<=1){
							strRemarks="No Refund";
							jsonOutput += ",\"remarks\":\"" + strRemarks+ "\"";
							jsonOutput += ",\"reducedAmount\":\"" + dblAdvanceAmount + "\"";
							jsonOutput += ",\"refudAmount\":\"" + (dblAdvanceAmount-dblAdvanceAmount)+ "\"";
						
						}else if(checkDiffInDays>=2){
							Double dblDiscountPer=0.0; 
							Double dblDiscountAmt=0.0;
							strRemarks="No Cancellation Fee";
							dblDiscountAmt=dblTotalAmount*dblDiscountPer/100;
							jsonOutput += ",\"remarks\":\"" + strRemarks+ "\"";
							jsonOutput += ",\"reducedAmount\":\"" + dblDiscountAmt + "\"";
							jsonOutput += ",\"refudAmount\":\"" + (dblTotalAmount-dblDiscountAmt)+ "\"";
							
						}
						
						
						Integer loginUserId;
						loginUserId=bookingDetailReport.getModifiedBy();
						if(loginUserId!=null){
							loginUserId=bookingDetailReport.getModifiedBy();
							User userDetails=userController.find(loginUserId);
							jsonOutput += ",\"userName\":\"" + userDetails.getUserName()+ "\"";
						}else{
							jsonOutput += ",\"userName\":\"NA\"";
						}
						
						jsonOutput += "}";
					}
				}	
			}
			
			

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");

			
			
		}
		catch(Exception e){
			logger.error(e);
			e.printStackTrace();
		}
		return null;
	}
	
	public String resortBookingReport() throws IOException {
		
		try {
			
			this.filterDateName=getFilterDateName();
			this.filterBookingId=getFilterBookingId();
			int bookingId=Integer.parseInt(filterBookingId);
			if(bookingId==0){
				DateFormat format = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
			    java.util.Date dateStart = format.parse(getStrFromDate());
			    java.util.Date dateEnd = format.parse(getStrToDate());
			   
			    
			    Calendar calCheckStart=Calendar.getInstance();
			    calCheckStart.setTime(dateStart);
			    java.util.Date checkInDate = calCheckStart.getTime();
			    this.fromDate=new Date(checkInDate.getTime());
			    sessionMap.put("fromDate",fromDate); 
			    
			    Calendar calCheckEnd=Calendar.getInstance();
			    calCheckEnd.setTime(dateEnd);
			    java.util.Date checkOutDate = calCheckEnd.getTime();
			    this.toDate=new Date(checkOutDate.getTime());
			    sessionMap.put("endDate",toDate); 
			}
		    
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			DecimalFormat df = new DecimalFormat("###.##");
			
			ReportManager reportController = new ReportManager();
			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
			PmsStatusManager statusController=new PmsStatusManager();
			
			response.setContentType("application/json");
			SimpleDateFormat format1 = new SimpleDateFormat("dd-MMM-yyyy");
			SimpleDateFormat format3 = new SimpleDateFormat("dd-MMM-yyyy-HH:mm:ss");
			if(bookingId==0){
				this.listBookingDetails=reportController.listResortBookingDetail(getPropertyId(),this.getFromDate(),this.getToDate(),this.getAccommodationId(),filterDateName,Integer.parseInt(filterBookingId));
			}else if(bookingId>0){
				this.listBookingDetails=reportController.listResortBookingDetail(getPropertyId(),Integer.parseInt(filterBookingId));
			}
//			this.listBookingDetails=reportController.listResortBookingDetail((Integer) sessionMap.get("propertyId"),this.getFromDate(),this.getToDate(),this.getAccommodationId());
			if(reportEnabled){
				if(listBookingDetails.size()>0 && !listBookingDetails.isEmpty()){
					for(BookingDetailReport bookingDetailReport:listBookingDetails){
						int i=0;
						while(i<hotelBookingReportKey.length){

							if (!jsonOutput.equalsIgnoreCase(""))
								jsonOutput += ",{";
							else
								jsonOutput += "{";
							
							jsonOutput += "\""+hotelBookingReportKey[i++]+"\":\"" + bookingDetailReport.getBookingId()+ "\"";
							
							String strBookingDate="";
							String timeHours="";
							if(bookingDetailReport.getCreatedDate()!=null){
								Timestamp bookingDate=bookingDetailReport.getCreatedDate();
								java.util.Date dateBooking = new java.util.Date(bookingDate.getTime());
								Calendar calDate=Calendar.getInstance();
								calDate.setTime(dateBooking);
								calDate.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
				            	int bookedHours=calDate.get(Calendar.HOUR);
				            	int bookedMinute=calDate.get(Calendar.MINUTE);
				            	int bookedSecond=calDate.get(Calendar.SECOND);
				            	int AMPM=calDate.get(Calendar.AM_PM);
				            	
				            	strBookingDate=format1.format(dateBooking);
				            	String hours="",minutes="",seconds="";
				            	if(bookedHours<10){
				            		hours=String.valueOf(bookedHours);
				            		hours="0"+hours;
				            	}else{
				            		hours=String.valueOf(bookedHours);
				            	}
				            	if(bookedSecond<10){
				            		seconds=String.valueOf(bookedSecond);
				            		seconds="0"+seconds;
				            	}else{
				            		seconds=String.valueOf(bookedSecond);
				            	}
				            	
				            	if(bookedMinute<10){
				            		minutes=String.valueOf(bookedMinute);
				            		minutes="0"+minutes;
				            	}else{
				            		minutes=String.valueOf(bookedMinute);
				            	}
				            	if(AMPM==0){//If the current time is AM
				            		timeHours=strBookingDate+"-"+hours+" : "+minutes+" : "+seconds+" AM ";
				            	}else if(AMPM==1){//If the current time is PM
				            		timeHours=strBookingDate+"-"+hours+" : "+minutes+" : "+seconds+" PM ";
				            	}
				            	
								strBookingDate=format3.format(dateBooking); 
								jsonOutput += ",\""+hotelBookingReportKey[i++]+"\":\"" + timeHours+ "\"";
							}else{
								jsonOutput += ",\""+hotelBookingReportKey[i++]+"\":\"NA\"";
							}
							
							String strFirstName="",firstNameLetterUpperCase="";
							String strLastName="",lastNameLetterUpperCase="";
							strFirstName=bookingDetailReport.getFirstName();
							strLastName=bookingDetailReport.getLastName();
							if(strFirstName!=null && strFirstName!="" && !strFirstName.equalsIgnoreCase("undefined")){
								strFirstName=strFirstName.trim();
								firstNameLetterUpperCase=strFirstName.substring(0, 1).toUpperCase()+strFirstName.substring(1);
								strFirstName=firstNameLetterUpperCase;
							}else{
								strFirstName="NA";
							}
							if(strLastName!=null && strLastName!=""  && !strLastName.equalsIgnoreCase("undefined")){
								strLastName=strLastName.trim();
								lastNameLetterUpperCase=strLastName.substring(0, 1).toUpperCase() +  strLastName.substring(1);
								strLastName=lastNameLetterUpperCase;
							}else{
								strLastName="";
							}
							
							jsonOutput += ",\""+hotelBookingReportKey[i++]+"\":\"" + strFirstName.trim()+" "+strLastName.trim() + "\"";
							
							String strMobileNumber="",statusName="",strEmailId="";
							strMobileNumber=bookingDetailReport.getMobileNumber();
							jsonOutput += ",\""+hotelBookingReportKey[i++]+"\":\"" + strMobileNumber.trim()+ "\"";
							
							strEmailId=bookingDetailReport.getEmailId();
							jsonOutput += ",\""+hotelBookingReportKey[i++]+"\":\"" + strEmailId.trim()+ "\"";
							
							Integer rooms=0,statusId=0,noOfRooms=0;
							Double dblBaseAmount=0.0,dblTaxAmount=0.0,dblBaseTaxAmount=0.0,dblTotalAmount=0.0,dblActualAmount=0.0;
							
							
							long arrivalDate =bookingDetailReport.getArrivalDate().getTime();
							java.util.Date dateArrival = new java.util.Date(arrivalDate);
							String strArrivalDate=format1.format(dateArrival); 
							
							jsonOutput += ",\""+hotelBookingReportKey[i++]+"\":\"" + strArrivalDate+ "\"";
							SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
							
							long departureDate =bookingDetailReport.getDepartureDate().getTime();
							java.util.Date dateDeparture = new java.util.Date(departureDate);
							String strDepartureDate=format1.format(dateDeparture); 
							
							jsonOutput += ",\""+hotelBookingReportKey[i++]+"\":\"" + strDepartureDate+ "\"";
							
							Timestamp FromDate=bookingDetailReport.getArrivalDate();
							Timestamp ToDate=bookingDetailReport.getDepartureDate();
							
							String strFromDate=format2.format(FromDate);
							String strToDate=format2.format(ToDate);
							
					    	DateTime startdate = DateTime.parse(strFromDate);
					        DateTime enddate = DateTime.parse(strToDate);
					        java.util.Date fromdate = startdate.toDate();
					 		java.util.Date todate = enddate.toDate();
					 		
							int diffInDays = (int) ((todate.getTime() - fromdate.getTime()) / (1000 * 60 * 60 * 24));
							
							dblBaseAmount=bookingDetailReport.getAmount();
							dblTaxAmount=bookingDetailReport.getTaxAmount();
							
							String strAccommodationType=null;
							Integer accommId=0;
							accommId=bookingDetailReport.getAccommodationId();
							if(accommId>0)
							{
								PropertyAccommodation accommodations=accommodationController.find(accommId);
								strAccommodationType=accommodations.getAccommodationType();
								jsonOutput += ",\""+hotelBookingReportKey[i++]+"\":\" "+strAccommodationType.trim()+"\"";
							}
							
							long lngrooms=bookingDetailReport.getRoomCount();
							rooms=(int)lngrooms;
							dblBaseTaxAmount=dblBaseAmount+dblTaxAmount;
							
							dblBaseAmount=bookingDetailReport.getAmount();
							dblTaxAmount=bookingDetailReport.getTaxAmount();
							if(dblBaseAmount!=null && dblBaseAmount!=0.0){
								dblBaseAmount=bookingDetailReport.getAmount();
							}else{
								dblBaseAmount=0.0;
							}
							
							if(dblTaxAmount!=null && dblTaxAmount!=0.0){
								dblTaxAmount=bookingDetailReport.getTaxAmount();
							}else{
								dblTaxAmount=0.0;
							}
							
							dblBaseTaxAmount=dblBaseAmount+dblTaxAmount;
							dblActualAmount=bookingDetailReport.getActualAmount();
							
							int compareValues=dblActualAmount.compareTo(dblBaseAmount);
							if(compareValues==1){
								dblTotalAmount=dblActualAmount-dblBaseAmount;
							}else if(compareValues==-1){
								dblTotalAmount=dblBaseAmount-dblActualAmount;
							}
							
							noOfRooms=rooms/diffInDays;
					 		Integer diffRoomInDays=noOfRooms*diffInDays;
					 		
							jsonOutput += ",\""+hotelBookingReportKey[i++]+"\":\"" + rooms/diffInDays+ "\"";
							jsonOutput += ",\""+hotelBookingReportKey[i++]+"\":\"" + diffInDays+ "\"";
							jsonOutput += ",\""+hotelBookingReportKey[i++]+"\":\"" + diffRoomInDays+ "\"";
							jsonOutput += ",\""+hotelBookingReportKey[i++]+"\":\"" + bookingDetailReport.getAdultCount()+ "\"";
							jsonOutput += ",\""+hotelBookingReportKey[i++]+"\":\"" + bookingDetailReport.getChildCount()+ "\"";
							
							jsonOutput += ",\""+hotelBookingReportKey[i++]+"\":\"" + df.format(dblActualAmount)+ "\"";
							jsonOutput += ",\""+hotelBookingReportKey[i++]+"\":\"" + df.format(dblBaseAmount)+ "\"";
							jsonOutput += ",\""+hotelBookingReportKey[i++]+"\":\"" + df.format(dblTaxAmount)+ "\"";
							
							jsonOutput += ",\""+hotelBookingReportKey[i++]+"\":\"" + df.format(dblBaseTaxAmount)+ "\"";
							jsonOutput += ",\""+hotelBookingReportKey[i++]+"\":\"" + df.format(dblTotalAmount)+ "\"";
							
							
							statusId=bookingDetailReport.getStatusId();
							PmsStatus status=statusController.find(statusId);
							statusName=status.getStatus();
							jsonOutput += ",\""+hotelBookingReportKey[i++]+"\":\"" + statusName+ "\"";
							
							jsonOutput += "}";
													
						}
					}
				}
			
				
			}else{
				if(listBookingDetails.size()>0 && !listBookingDetails.isEmpty()){
					for(BookingDetailReport bookingDetailReport:listBookingDetails){
						if (!jsonOutput.equalsIgnoreCase(""))
							jsonOutput += ",{";
						else
							jsonOutput += "{";
						
						jsonOutput += "\"bookingId\":\"" + bookingDetailReport.getBookingId()+ "\"";
						String strFirstName="",firstNameLetterUpperCase="";
						String strLastName="",lastNameLetterUpperCase="";
						strFirstName=bookingDetailReport.getFirstName();
						strLastName=bookingDetailReport.getLastName();
						if(strFirstName!=null && strFirstName!="" && !strFirstName.equalsIgnoreCase("undefined")){
							strFirstName=strFirstName.trim();
							firstNameLetterUpperCase=strFirstName.substring(0, 1).toUpperCase()+strFirstName.substring(1);
							strFirstName=firstNameLetterUpperCase;
						}else{
							strFirstName="NA";
						}
						if(strLastName!=null && strLastName!=""  && !strLastName.equalsIgnoreCase("undefined")){
							strLastName=strLastName.trim();
							lastNameLetterUpperCase=strLastName.substring(0, 1).toUpperCase() +  strLastName.substring(1);
							strLastName=lastNameLetterUpperCase;
						}else{
							strLastName="";
						}
						
						jsonOutput += ",\"guestName\":\"" + strFirstName.trim()+" "+strLastName.trim() + "\"";
						
						String strMobileNumber="",statusName="",strEmailId="";
						strMobileNumber=bookingDetailReport.getMobileNumber();
						jsonOutput += ",\"mobileNumber\":\"" + strMobileNumber.trim()+ "\"";
						
						strEmailId=bookingDetailReport.getEmailId();
						jsonOutput += ",\"emailId\":\"" + strEmailId.trim()+ "\"";
						
						Integer rooms=0,statusId=0,noOfRooms=0;
						Double dblBaseAmount=0.0,dblTaxAmount=0.0,dblBaseTaxAmount=0.0,dblTotalAmount=0.0,dblActualAmount=0.0;
						
						/*String strBookingDate="";
						if(bookingDetailReport.getCreatedDate()!=null){
							Timestamp bookingDate=bookingDetailReport.getCreatedDate();
							java.util.Date dateBooking = new java.util.Date(bookingDate.getTime());
							strBookingDate=format3.format(dateBooking); 
							jsonOutput += ",\"bookingDate\":\"" +strBookingDate+ "\"";
						}else{
							jsonOutput += ",\"bookingDate\":\"-\"";
						}*/
						
						String strBookingDate="";
						String timeHours="";
						if(bookingDetailReport.getCreatedDate()!=null){
							Timestamp bookingDate=bookingDetailReport.getCreatedDate();
							java.util.Date dateBooking = new java.util.Date(bookingDate.getTime());
							Calendar calDate=Calendar.getInstance();
							calDate.setTime(dateBooking);
							calDate.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
			            	int bookedHours=calDate.get(Calendar.HOUR);
			            	int bookedMinute=calDate.get(Calendar.MINUTE);
			            	int bookedSecond=calDate.get(Calendar.SECOND);
			            	int AMPM=calDate.get(Calendar.AM_PM);
			            	
			            	strBookingDate=format1.format(dateBooking);
			            	String hours="",minutes="",seconds="";
			            	if(bookedHours<10){
			            		hours=String.valueOf(bookedHours);
			            		hours="0"+hours;
			            	}else{
			            		hours=String.valueOf(bookedHours);
			            	}
			            	if(bookedSecond<10){
			            		seconds=String.valueOf(bookedSecond);
			            		seconds="0"+seconds;
			            	}else{
			            		seconds=String.valueOf(bookedSecond);
			            	}
			            	
			            	if(bookedMinute<10){
			            		minutes=String.valueOf(bookedMinute);
			            		minutes="0"+minutes;
			            	}else{
			            		minutes=String.valueOf(bookedMinute);
			            	}
			            	if(AMPM==0){//If the current time is AM
			            		timeHours=strBookingDate+"-"+hours+" : "+minutes+" : "+seconds+" AM ";
			            	}else if(AMPM==1){//If the current time is PM
			            		timeHours=strBookingDate+"-"+hours+" : "+minutes+" : "+seconds+" PM ";
			            	}
			            	
							strBookingDate=format3.format(dateBooking); 
							jsonOutput += ",\"bookingDate\":\"" + timeHours+ "\"";
						}else{
							jsonOutput += ",\"bookingDate\":\"NA\"";
						}
						
						long arrivalDate =bookingDetailReport.getArrivalDate().getTime();
						java.util.Date dateArrival = new java.util.Date(arrivalDate);
						String strArrivalDate=format1.format(dateArrival); 
						
						jsonOutput += ",\"checkIn\":\"" + strArrivalDate+ "\"";
						SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
						
						long departureDate =bookingDetailReport.getDepartureDate().getTime();
						java.util.Date dateDeparture = new java.util.Date(departureDate);
						String strDepartureDate=format1.format(dateDeparture); 
						
						jsonOutput += ",\"checkOut\":\"" + strDepartureDate+ "\"";
						
						Timestamp FromDate=bookingDetailReport.getArrivalDate();
						Timestamp ToDate=bookingDetailReport.getDepartureDate();
						
						String strFromDate=format2.format(FromDate);
						String strToDate=format2.format(ToDate);
						
				    	DateTime startdate = DateTime.parse(strFromDate);
				        DateTime enddate = DateTime.parse(strToDate);
				        java.util.Date fromdate = startdate.toDate();
				 		java.util.Date todate = enddate.toDate();
				 		
						int diffInDays = (int) ((todate.getTime() - fromdate.getTime()) / (1000 * 60 * 60 * 24));
						
						dblBaseAmount=bookingDetailReport.getAmount();
						dblTaxAmount=bookingDetailReport.getTaxAmount();
						
						String strAccommodationType=null;
						Integer accommId=0;
						accommId=bookingDetailReport.getAccommodationId();
						if(accommId>0)
						{
							PropertyAccommodation accommodations=accommodationController.find(accommId);
							strAccommodationType=accommodations.getAccommodationType();
							jsonOutput += ",\"accommodationType\":\" "+strAccommodationType.trim()+"\"";
						}
						
						long lngrooms=bookingDetailReport.getRoomCount();
						rooms=(int)lngrooms;
						dblBaseTaxAmount=dblBaseAmount+dblTaxAmount;
						
						dblBaseAmount=bookingDetailReport.getAmount();
						dblTaxAmount=bookingDetailReport.getTaxAmount();
						if(dblBaseAmount!=null && dblBaseAmount!=0.0){
							dblBaseAmount=bookingDetailReport.getAmount();
						}else{
							dblBaseAmount=0.0;
						}
						
						if(dblTaxAmount!=null && dblTaxAmount!=0.0){
							dblTaxAmount=bookingDetailReport.getTaxAmount();
						}else{
							dblTaxAmount=0.0;
						}
						
						dblBaseTaxAmount=dblBaseAmount+dblTaxAmount;
						dblActualAmount=bookingDetailReport.getActualAmount();
						
						int compareValues=dblActualAmount.compareTo(dblBaseAmount);
						if(compareValues==1){
							dblTotalAmount=dblActualAmount-dblBaseAmount;
						}else if(compareValues==-1){
							dblTotalAmount=dblBaseAmount-dblActualAmount;
						}
						
						noOfRooms=rooms/diffInDays;
				 		Integer diffRoomInDays=noOfRooms*diffInDays;
				 		
						jsonOutput += ",\"rooms\":\"" + rooms/diffInDays+ "\"";
						jsonOutput += ",\"diffDays\":\"" + diffInDays+ "\"";
						jsonOutput += ",\"roomNights\":\"" + diffRoomInDays+ "\"";
						jsonOutput += ",\"adultCount\":\"" + bookingDetailReport.getAdultCount()+ "\"";
						jsonOutput += ",\"childCount\":\"" + bookingDetailReport.getChildCount()+ "\"";
						
						jsonOutput += ",\"actualAmount\":\"" + df.format(dblActualAmount)+ "\"";
						jsonOutput += ",\"resortOfferAmount\":\"" + df.format(dblBaseAmount)+ "\"";
						jsonOutput += ",\"taxAmount\":\"" + df.format(dblTaxAmount)+ "\"";
						
						jsonOutput += ",\"totalAmount\":\"" + df.format(dblBaseTaxAmount)+ "\"";
						jsonOutput += ",\"actualTotalAmount\":\"" + df.format(dblTotalAmount)+ "\"";
						
						
						statusId=bookingDetailReport.getStatusId();
						PmsStatus status=statusController.find(statusId);
						statusName=status.getStatus();
						jsonOutput += ",\"status\":\"" + statusName+ "\"";
						
						jsonOutput += "}";
					}
				}
			}
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	public String uloBookingReport() throws IOException {
	try {
		
		this.filterDateName=getFilterDateName();
	    this.filterBookingId=getFilterBookingId();
	    int bookingId=Integer.parseInt(filterBookingId);
	    if(bookingId==0){
	    	DateFormat format = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
		    java.util.Date dateStart = format.parse(getStrFromDate());
		    java.util.Date dateEnd = format.parse(getStrToDate());
		    
		    Calendar calCheckStart=Calendar.getInstance();
		    calCheckStart.setTime(dateStart);
		    java.util.Date checkInDate = calCheckStart.getTime();
		    this.fromDate=new Date(checkInDate.getTime());
		    sessionMap.put("fromDate",fromDate); 
		    
		    Calendar calCheckEnd=Calendar.getInstance();
		    calCheckEnd.setTime(dateEnd);
		    java.util.Date checkOutDate = calCheckEnd.getTime();
		    this.toDate=new Date(checkOutDate.getTime());
		    sessionMap.put("endDate",toDate); 
	    }
	    
		String jsonOutput = "";
		HttpServletResponse response = ServletActionContext.getResponse();
	
		DecimalFormat df = new DecimalFormat("###.##");
		PmsPropertyManager pmsPropertyController=new PmsPropertyManager();
		
		ReportManager reportController = new ReportManager();
		PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
		PmsStatusManager statusController=new PmsStatusManager();
		PmsSourceManager sourceController=new PmsSourceManager();
		UserLoginManager userController=new UserLoginManager();
		response.setContentType("application/json");
		SimpleDateFormat format1 = new SimpleDateFormat("dd-MMM-yyyy");
		SimpleDateFormat format3 = new SimpleDateFormat("dd-MMM-yyyy-HH:mm:ss");
		if(bookingId==0){
			this.listBookingDetails=reportController.listUloBookingDetail(getPropertyId(),this.getFromDate(),this.getToDate(),this.getAccommodationId(),this.filterDateName,Integer.parseInt(this.filterBookingId));	
		}else if(bookingId>0){
			this.listBookingDetails=reportController.listUloBookingDetail(getPropertyId(),Integer.parseInt(this.filterBookingId));
		}
		//this.listBookingDetails=reportController.listUloBookingDetail((Integer) sessionMap.get("propertyId"),this.getFromDate(),this.getToDate(),this.getAccommodationId());
		if(reportEnabled){
			if(listBookingDetails.size()>0 && !listBookingDetails.isEmpty()){
				for(BookingDetailReport bookingDetailReport:listBookingDetails){
					int i=0;
					while(i<uloBookingReportKey.length){

						if (!jsonOutput.equalsIgnoreCase(""))
							jsonOutput += ",{";
						else
							jsonOutput += "{";
						
						jsonOutput += "\""+uloBookingReportKey[i++]+"\":\"" + bookingDetailReport.getBookingId()+ "\"";
						
						String strBookingDate="";
						String timeHours="";
						if(bookingDetailReport.getCreatedDate()!=null){
							Timestamp bookingDate=bookingDetailReport.getCreatedDate();
							java.util.Date dateBooking = new java.util.Date(bookingDate.getTime());
							Calendar calDate=Calendar.getInstance();
							calDate.setTime(dateBooking);
							calDate.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
			            	int bookedHours=calDate.get(Calendar.HOUR);
			            	int bookedMinute=calDate.get(Calendar.MINUTE);
			            	int bookedSecond=calDate.get(Calendar.SECOND);
			            	int AMPM=calDate.get(Calendar.AM_PM);
			            	
			            	strBookingDate=format1.format(dateBooking);
			            	String hours="",minutes="",seconds="";
			            	if(bookedHours<10){
			            		hours=String.valueOf(bookedHours);
			            		hours="0"+hours;
			            	}else{
			            		hours=String.valueOf(bookedHours);
			            	}
			            	if(bookedSecond<10){
			            		seconds=String.valueOf(bookedSecond);
			            		seconds="0"+seconds;
			            	}else{
			            		seconds=String.valueOf(bookedSecond);
			            	}
			            	
			            	if(bookedMinute<10){
			            		minutes=String.valueOf(bookedMinute);
			            		minutes="0"+minutes;
			            	}else{
			            		minutes=String.valueOf(bookedMinute);
			            	}
			            	if(AMPM==0){//If the current time is AM
			            		timeHours=strBookingDate+"-"+hours+" : "+minutes+" : "+seconds+" AM ";
			            	}else if(AMPM==1){//If the current time is PM
			            		timeHours=strBookingDate+"-"+hours+" : "+minutes+" : "+seconds+" PM ";
			            	}
			            	
							strBookingDate=format3.format(dateBooking); 
							jsonOutput += ",\""+uloBookingReportKey[i++]+"\":\"" + timeHours+ "\"";
						}else{
							jsonOutput += ",\""+uloBookingReportKey[i++]+"\":\"NA\"";
						}
						
						if(bookingDetailReport.getOtaBookingId()!=null){
							String strOtaId=bookingDetailReport.getOtaBookingId().trim();
							if(strOtaId.equalsIgnoreCase("Nil")){
								jsonOutput += ",\""+uloBookingReportKey[i++]+"\":\"NA\"";	
							}else{
								jsonOutput += ",\""+uloBookingReportKey[i++]+"\":\"" + bookingDetailReport.getOtaBookingId()+ "\"";	
							}
						}else{
							jsonOutput += ",\""+uloBookingReportKey[i++]+"\":\"NA\"";
						}
						
						String strFirstName="",firstNameLetterUpperCase="";
						String strLastName="",lastNameLetterUpperCase="";
						strFirstName=bookingDetailReport.getFirstName();
						strLastName=bookingDetailReport.getLastName();
						if(strFirstName!=null && strFirstName!="" && !strFirstName.equalsIgnoreCase("undefined")){
							strFirstName=strFirstName.trim();
							firstNameLetterUpperCase=strFirstName.substring(0, 1).toUpperCase()+strFirstName.substring(1);
							strFirstName=firstNameLetterUpperCase;
						}else{
							strFirstName="NA";
						}
						if(strLastName!=null && strLastName!=""  && !strLastName.equalsIgnoreCase("undefined")){
							strLastName=strLastName.trim();
							lastNameLetterUpperCase=strLastName.substring(0, 1).toUpperCase() +  strLastName.substring(1);
							strLastName=lastNameLetterUpperCase;
						}else{
							strLastName="";
						}
						
						jsonOutput += ",\""+uloBookingReportKey[i++]+"\":\"" + strFirstName.trim()+" "+strLastName.trim() + "\"";
						
						
						Double dblBaseAmount=0.0,dblTaxAmount=0.0,dblTotalAmount=0.0,dblAdvanceAmount=0.0,dblDueAmount=0.0;
						Double dblOtaCommission=0.0,dblOtaTax=0.0,dblTotalOtaAmout=0.0,dblRevenueAmount=0.0,dblTotalRevenueTaxAmount=0.0,dblNetHotelPayable=0.0;
						Double dblCommission=0.0,dblTotalUloCommission=0.0,dblUloCommission=0.0,dblUloTaxAmount=0.0,dblHotelPayable=0.0,dblHotelTaxPayable=0.0;
						Integer rooms=0,adultCount=0,childCount=0,statusId=0,sourceId=0,noOfRooms=0;
						
						String strMobileNumber="",statusName="",strEmailId="",sourceName="";
						strMobileNumber=bookingDetailReport.getMobileNumber();
						jsonOutput += ",\""+uloBookingReportKey[i++]+"\":\"" + strMobileNumber.trim()+ "\"";
						
						strEmailId=bookingDetailReport.getEmailId();
						jsonOutput += ",\""+uloBookingReportKey[i++]+"\":\"" + strEmailId.trim()+ "\"";
						
						
						long arrivalDate =bookingDetailReport.getArrivalDate().getTime();
						java.util.Date dateArrival = new java.util.Date(arrivalDate);
						String strArrivalDate=format1.format(dateArrival); 
						
						jsonOutput += ",\""+uloBookingReportKey[i++]+"\":\"" + strArrivalDate+ "\"";
						
						long departureDate =bookingDetailReport.getDepartureDate().getTime();
						java.util.Date dateDeparture = new java.util.Date(departureDate);
						String strDepartureDate=format1.format(dateDeparture); 
						
						jsonOutput += ",\""+uloBookingReportKey[i++]+"\":\"" + strDepartureDate+ "\"";
						SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
						
						Timestamp FromDate=bookingDetailReport.getArrivalDate();
						Timestamp ToDate=bookingDetailReport.getDepartureDate();
						
						String strFromDate=format2.format(FromDate);
						String strToDate=format2.format(ToDate);
						
				    	DateTime startdate = DateTime.parse(strFromDate);
				        DateTime enddate = DateTime.parse(strToDate);
				        java.util.Date fromdate = startdate.toDate();
				 		java.util.Date todate = enddate.toDate();
				 		
						int diffInDays = (int) ((todate.getTime() - fromdate.getTime()) / (1000 * 60 * 60 * 24));
						
						
						String strAccommodationType=null;
						Integer accommId=0;
						accommId=bookingDetailReport.getAccommodationId();
						if(accommId>0)
						{
							PropertyAccommodation accommodations=accommodationController.find(accommId);
							strAccommodationType=accommodations.getAccommodationType();
							jsonOutput += ",\""+uloBookingReportKey[i++]+"\":\" "+strAccommodationType.trim()+"\"";
						}
						
						long lngrooms=bookingDetailReport.getRoomCount();
						rooms=(int)lngrooms;
						noOfRooms=rooms/diffInDays;
				 		Integer diffRoomInDays=noOfRooms*diffInDays;
				 		
						jsonOutput += ",\""+uloBookingReportKey[i++]+"\":\"" + rooms/diffInDays+ "\"";
						jsonOutput += ",\""+uloBookingReportKey[i++]+"\":\"" + diffInDays+ "\"";
						jsonOutput += ",\""+uloBookingReportKey[i++]+"\":\"" + diffRoomInDays+ "\"";
						jsonOutput += ",\""+uloBookingReportKey[i++]+"\":\"" + bookingDetailReport.getAdultCount()+ "\"";
						jsonOutput += ",\""+uloBookingReportKey[i++]+"\":\"" + bookingDetailReport.getChildCount()+ "\"";
						
						dblBaseAmount=bookingDetailReport.getAmount();
						dblTaxAmount=bookingDetailReport.getTaxAmount();
						
						if(dblBaseAmount!=null && dblBaseAmount>=0.0){
							dblBaseAmount=bookingDetailReport.getAmount();
						}else{
							dblBaseAmount=0.0;
						}
						
						if(dblTaxAmount!=null && dblTaxAmount>=0.0){
							dblTaxAmount=bookingDetailReport.getTaxAmount();
						}else{
							dblTaxAmount=0.0;
						}
						sourceId=bookingDetailReport.getSourceId();
						dblTotalAmount=dblBaseAmount+dblTaxAmount;
						
						dblAdvanceAmount=bookingDetailReport.getAdvanceAmount();
						
						jsonOutput += ",\""+uloBookingReportKey[i++]+"\":\" "+df.format(dblBaseAmount)+"\"";
						jsonOutput += ",\""+uloBookingReportKey[i++]+"\":\" "+df.format(dblTaxAmount)+"\"";
						
						dblTotalAmount=dblBaseAmount+dblTaxAmount;
						
						
						if(dblAdvanceAmount!=null  && dblAdvanceAmount>=0.0){
							dblAdvanceAmount=bookingDetailReport.getAdvanceAmount();
						}else{
							if(sourceId==1){
								dblAdvanceAmount=dblTotalAmount;
							}else{
								dblAdvanceAmount=0.0;
							}
						}
						String strTotal=df.format(dblTotalAmount);
						
						boolean compareValues=Double.valueOf(strTotal).equals(dblAdvanceAmount);
						jsonOutput += ",\""+uloBookingReportKey[i++]+"\":\" "+df.format(dblTotalAmount)+"\"";
						
						
						dblDueAmount=Double.valueOf(strTotal)-dblAdvanceAmount;
						
						jsonOutput += ",\""+uloBookingReportKey[i++]+"\":\" "+df.format(dblAdvanceAmount)+"\"";
						jsonOutput += ",\""+uloBookingReportKey[i++]+"\":\" "+df.format(dblDueAmount)+"\"";
						
						dblOtaCommission=bookingDetailReport.getOtaCommission();
						dblOtaTax=bookingDetailReport.getOtaTax();
						if(dblOtaCommission!=null && dblOtaCommission>=0.0){
							dblOtaCommission=bookingDetailReport.getOtaCommission();
						}else{
							dblOtaCommission=0.0;
						}
						if(dblOtaTax!=null && dblOtaTax>=0.0){
							dblOtaTax=bookingDetailReport.getOtaTax();
						}else{
							dblOtaTax=0.0;
						}
						
						dblTotalOtaAmout=dblOtaCommission+dblOtaTax;
						
						jsonOutput += ",\""+uloBookingReportKey[i++]+"\":\" "+df.format(dblOtaCommission)+"\"";
						jsonOutput += ",\""+uloBookingReportKey[i++]+"\":\" "+df.format(dblOtaTax)+"\"";
						jsonOutput += ",\""+uloBookingReportKey[i++]+"\":\" "+df.format(dblTotalOtaAmout)+"\"";
						
						dblRevenueAmount=dblBaseAmount-dblTotalOtaAmout;
						dblTotalRevenueTaxAmount=dblRevenueAmount+dblTaxAmount;
						jsonOutput += ",\""+uloBookingReportKey[i++]+"\":\" "+df.format(dblRevenueAmount)+"\"";
						jsonOutput += ",\""+uloBookingReportKey[i++]+"\":\" "+df.format(dblTaxAmount)+"\"";
						jsonOutput += ",\""+uloBookingReportKey[i++]+"\":\" "+df.format(dblTotalRevenueTaxAmount)+"\"";
						
						PmsProperty pmsPropertyDetails=pmsPropertyController.find(getPropertyId());
						dblCommission=pmsPropertyDetails.getPropertyCommission();
						if(dblCommission!=null){
							dblUloCommission=dblRevenueAmount*dblCommission/100;
						}
						
						PropertyAccommodation accommodation=accommodationController.find(bookingDetailReport.getAccommodationId());
						double netRateAmount=0;
						if(accommodation.getNetRateRevenue()!=null){
							netRateAmount=accommodation.getNetRateRevenue();	
						}
						
						
						
						double dblNetRevenue=0,dblRoomNightsRevenue=0,dblbaseamount=0,dblotarevenue=0;
							
						dblRoomNightsRevenue=dblRevenueAmount/diffRoomInDays;
						dblNetRevenue=dblRoomNightsRevenue-netRateAmount;
						
						jsonOutput += ",\""+uloBookingReportKey[i++]+"\":\" "+df.format(dblNetRevenue*diffRoomInDays)+"\"";
				
						jsonOutput += ",\""+uloBookingReportKey[i++]+"\":\" "+df.format(dblUloCommission)+"\"";
						
						dblUloTaxAmount=dblUloCommission*18/100;
						jsonOutput += ",\""+uloBookingReportKey[i++]+"\":\"" + df.format(dblUloTaxAmount)+ "\"";
						
						dblTotalUloCommission=dblUloCommission+dblUloTaxAmount;
						jsonOutput += ",\""+uloBookingReportKey[i++]+"\":\"" + df.format(dblTotalUloCommission)+ "\"";
						
						dblHotelPayable=dblRevenueAmount-dblTotalUloCommission;
						dblHotelTaxPayable=dblHotelPayable+dblTaxAmount;
						dblNetHotelPayable=dblHotelTaxPayable-dblDueAmount;
						jsonOutput += ",\""+uloBookingReportKey[i++]+"\":\"" + df.format(dblNetHotelPayable)+ "\"";
						jsonOutput += ",\""+uloBookingReportKey[i++]+"\":\"" + df.format(dblHotelTaxPayable)+ "\"";
						
						Double dblAddonAmount=null;
						dblAddonAmount=bookingDetailReport.getAddonAmount();
						String strAddonRemarks=null;
						strAddonRemarks=bookingDetailReport.getAddonDescription();
						if(dblAddonAmount!=null){
							jsonOutput += ",\""+uloBookingReportKey[i++]+"\":\"" + df.format(bookingDetailReport.getAddonAmount()==null?"0":bookingDetailReport.getAddonAmount())+ "\"";	
						}else{
							jsonOutput += ",\""+uloBookingReportKey[i++]+"\":\"" + 0 + "\"";
						}
						
						jsonOutput += ",\""+uloBookingReportKey[i++]+"\":\"" + (strAddonRemarks==null?"NA":strAddonRemarks)+ "\"";
						
						
						if(dblAdvanceAmount!=null && dblAdvanceAmount>=0.0){
							if(compareValues){
								jsonOutput += ",\""+uloBookingReportKey[i++]+"\":\" CASH PAID\"";
							}else{
								jsonOutput += ",\""+uloBookingReportKey[i++]+"\":\" PAY AT HOTEL\"";
							}
						}else{
							if(sourceId==1){
								jsonOutput += ",\""+uloBookingReportKey[i++]+"\":\" CASH PAID \"";
							}else{
								if(compareValues){
									jsonOutput += ",\""+uloBookingReportKey[i++]+"\":\" CASH PAID\"";
								}else{
									jsonOutput += ",\""+uloBookingReportKey[i++]+"\":\" PAY AT HOTEL\"";
								}
							}
						}
						
						statusId=bookingDetailReport.getStatusId();
						PmsStatus status=statusController.find(statusId);
						statusName=status.getStatus();
						jsonOutput += ",\""+uloBookingReportKey[i++]+"\":\"" + statusName+ "\"";
						sourceId=bookingDetailReport.getSourceId();
						PmsSource source=sourceController.find(sourceId);
						sourceName=source.getSourceName().trim();
						jsonOutput += ",\""+uloBookingReportKey[i++]+"\":\"" + sourceName+ "\"";
						
						Integer loginUserId;
						loginUserId=bookingDetailReport.getCreatedBy();
						if(loginUserId!=null){
							loginUserId=bookingDetailReport.getCreatedBy();
							User userDetails=userController.find(loginUserId);
							jsonOutput += ",\""+uloBookingReportKey[i++]+"\":\"" + userDetails.getUserName()+ "\"";
						}else{
							jsonOutput += ",\""+uloBookingReportKey[i++]+"\":\"NA\"";
						}
						
						jsonOutput += "}";
					
					}
				}
			}
		
		}else{
			if(listBookingDetails.size()>0 && !listBookingDetails.isEmpty()){
				for(BookingDetailReport bookingDetailReport:listBookingDetails){
					if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += ",{";
					else
						jsonOutput += "{";
					
					jsonOutput += "\"bookingId\":\"" + bookingDetailReport.getBookingId()+ "\"";
					
					if(bookingDetailReport.getOtaBookingId()!=null){
						String strOtaId=bookingDetailReport.getOtaBookingId().trim();
						if(strOtaId.equalsIgnoreCase("Nil")){
							jsonOutput += ",\"otaBookingId\":\"NA\"";	
						}else{
							jsonOutput += ",\"otaBookingId\":\"" + bookingDetailReport.getOtaBookingId()+ "\"";	
						}
					}else{
						jsonOutput += ",\"otaBookingId\":\"NA\"";
					}
					
					String strFirstName="",firstNameLetterUpperCase="";
					String strLastName="",lastNameLetterUpperCase="";
					strFirstName=bookingDetailReport.getFirstName();
					strLastName=bookingDetailReport.getLastName();
					if(strFirstName!=null && strFirstName!="" && !strFirstName.equalsIgnoreCase("undefined")){
						strFirstName=strFirstName.trim();
						firstNameLetterUpperCase=strFirstName.substring(0, 1).toUpperCase()+strFirstName.substring(1);
						strFirstName=firstNameLetterUpperCase;
					}else{
						strFirstName="NA";
					}
					if(strLastName!=null && strLastName!=""  && !strLastName.equalsIgnoreCase("undefined")){
						strLastName=strLastName.trim();
						lastNameLetterUpperCase=strLastName.substring(0, 1).toUpperCase() +  strLastName.substring(1);
						strLastName=lastNameLetterUpperCase;
					}else{
						strLastName="";
					}
					
					jsonOutput += ",\"guestName\":\"" + strFirstName.trim()+" "+strLastName.trim() + "\"";
					
					
					Double dblBaseAmount=0.0,dblTaxAmount=0.0,dblTotalAmount=0.0,dblAdvanceAmount=0.0,dblDueAmount=0.0;
					Double dblOtaCommission=0.0,dblOtaTax=0.0,dblTotalOtaAmout=0.0,dblRevenueAmount=0.0,dblTotalRevenueTaxAmount=0.0,dblNetHotelPayable=0.0;
					Double dblCommission=0.0,dblTotalUloCommission=0.0,dblUloCommission=0.0,dblUloTaxAmount=0.0,dblHotelPayable=0.0,dblHotelTaxPayable=0.0;
					Integer rooms=0,adultCount=0,childCount=0,statusId=0,sourceId=0,noOfRooms=0;
					
					String strMobileNumber="",statusName="",strEmailId="",sourceName="";
					strMobileNumber=bookingDetailReport.getMobileNumber();
					jsonOutput += ",\"mobileNumber\":\"" + strMobileNumber.trim()+ "\"";
					
					strEmailId=bookingDetailReport.getEmailId();
					jsonOutput += ",\"emailId\":\"" + strEmailId.trim()+ "\"";
					
					
					
					String strBookingDate="";
					String timeHours="";
					if(bookingDetailReport.getCreatedDate()!=null){
						Timestamp bookingDate=bookingDetailReport.getCreatedDate();
						java.util.Date dateBooking = new java.util.Date(bookingDate.getTime());
						Calendar calDate=Calendar.getInstance();
						calDate.setTime(dateBooking);
						calDate.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
		            	int bookedHours=calDate.get(Calendar.HOUR);
		            	int bookedMinute=calDate.get(Calendar.MINUTE);
		            	int bookedSecond=calDate.get(Calendar.SECOND);
		            	int AMPM=calDate.get(Calendar.AM_PM);
		            	
		            	strBookingDate=format1.format(dateBooking);
		            	String hours="",minutes="",seconds="";
		            	if(bookedHours<10){
		            		hours=String.valueOf(bookedHours);
		            		hours="0"+hours;
		            	}else{
		            		hours=String.valueOf(bookedHours);
		            	}
		            	if(bookedSecond<10){
		            		seconds=String.valueOf(bookedSecond);
		            		seconds="0"+seconds;
		            	}else{
		            		seconds=String.valueOf(bookedSecond);
		            	}
		            	
		            	if(bookedMinute<10){
		            		minutes=String.valueOf(bookedMinute);
		            		minutes="0"+minutes;
		            	}else{
		            		minutes=String.valueOf(bookedMinute);
		            	}
		            	if(AMPM==0){//If the current time is AM
		            		timeHours=strBookingDate+"-"+hours+" : "+minutes+" : "+seconds+" AM ";
		            	}else if(AMPM==1){//If the current time is PM
		            		timeHours=strBookingDate+"-"+hours+" : "+minutes+" : "+seconds+" PM ";
		            	}
		            	
						strBookingDate=format3.format(dateBooking); 
						jsonOutput += ",\"bookingDate\":\"" + timeHours+ "\"";
					}else{
						jsonOutput += ",\"bookingDate\":\"NA\"";
					}
					
					
					long arrivalDate =bookingDetailReport.getArrivalDate().getTime();
					java.util.Date dateArrival = new java.util.Date(arrivalDate);
					String strArrivalDate=format1.format(dateArrival); 
					
					jsonOutput += ",\"checkIn\":\"" + strArrivalDate+ "\"";
					
					long departureDate =bookingDetailReport.getDepartureDate().getTime();
					java.util.Date dateDeparture = new java.util.Date(departureDate);
					String strDepartureDate=format1.format(dateDeparture); 
					
					jsonOutput += ",\"checkOut\":\"" + strDepartureDate+ "\"";
					SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
					
					Timestamp FromDate=bookingDetailReport.getArrivalDate();
					Timestamp ToDate=bookingDetailReport.getDepartureDate();
					
					String strFromDate=format2.format(FromDate);
					String strToDate=format2.format(ToDate);
					
			    	DateTime startdate = DateTime.parse(strFromDate);
			        DateTime enddate = DateTime.parse(strToDate);
			        java.util.Date fromdate = startdate.toDate();
			 		java.util.Date todate = enddate.toDate();
			 		
					int diffInDays = (int) ((todate.getTime() - fromdate.getTime()) / (1000 * 60 * 60 * 24));
					
					
					String strAccommodationType=null;
					Integer accommId=0;
					accommId=bookingDetailReport.getAccommodationId();
					if(accommId>0)
					{
						PropertyAccommodation accommodations=accommodationController.find(accommId);
						strAccommodationType=accommodations.getAccommodationType();
						jsonOutput += ",\"accommodationType\":\" "+strAccommodationType.trim()+"\"";
					}
					
					long lngrooms=bookingDetailReport.getRoomCount();
					rooms=(int)lngrooms;
					noOfRooms=rooms/diffInDays;
			 		Integer diffRoomInDays=noOfRooms*diffInDays;
			 		
					jsonOutput += ",\"rooms\":\"" + rooms/diffInDays+ "\"";
					jsonOutput += ",\"diffDays\":\"" + diffInDays+ "\"";
					jsonOutput += ",\"roomNights\":\"" + diffRoomInDays+ "\"";
					jsonOutput += ",\"adultCount\":\"" + bookingDetailReport.getAdultCount()+ "\"";
					jsonOutput += ",\"childCount\":\"" + bookingDetailReport.getChildCount()+ "\"";
					
					dblBaseAmount=bookingDetailReport.getAmount();
					dblTaxAmount=bookingDetailReport.getTaxAmount();
					
					if(dblBaseAmount!=null && dblBaseAmount>=0.0){
						dblBaseAmount=bookingDetailReport.getAmount();
					}else{
						dblBaseAmount=0.0;
					}
					
					if(dblTaxAmount!=null && dblTaxAmount>=0.0){
						dblTaxAmount=bookingDetailReport.getTaxAmount();
					}else{
						dblTaxAmount=0.0;
					}
					sourceId=bookingDetailReport.getSourceId();
					dblTotalAmount=dblBaseAmount+dblTaxAmount;
					
					dblAdvanceAmount=bookingDetailReport.getAdvanceAmount();
					
					jsonOutput += ",\"tariffAmount\":\" "+df.format(dblBaseAmount)+"\"";
					jsonOutput += ",\"taxAmount\":\" "+df.format(dblTaxAmount)+"\"";
					
					dblTotalAmount=dblBaseAmount+dblTaxAmount;
					
					
					if(dblAdvanceAmount!=null  && dblAdvanceAmount>=0.0){
						dblAdvanceAmount=bookingDetailReport.getAdvanceAmount();
					}else{
						if(sourceId==1){
							dblAdvanceAmount=dblTotalAmount;
						}else{
							dblAdvanceAmount=0.0;
						}
					}
					String strTotal=df.format(dblTotalAmount);
					
					boolean compareValues=Double.valueOf(strTotal).equals(dblAdvanceAmount);
					jsonOutput += ",\"totalAmount\":\" "+df.format(dblTotalAmount)+"\"";
					
					
					dblDueAmount=Double.valueOf(strTotal)-dblAdvanceAmount;
					
					jsonOutput += ",\"advanceAmount\":\" "+df.format(dblAdvanceAmount)+"\"";
					jsonOutput += ",\"dueAmount\":\" "+df.format(dblDueAmount)+"\"";
					
					dblOtaCommission=bookingDetailReport.getOtaCommission();
					dblOtaTax=bookingDetailReport.getOtaTax();
					if(dblOtaCommission!=null && dblOtaCommission>=0.0){
						dblOtaCommission=bookingDetailReport.getOtaCommission();
					}else{
						dblOtaCommission=0.0;
					}
					if(dblOtaTax!=null && dblOtaTax>=0.0){
						dblOtaTax=bookingDetailReport.getOtaTax();
					}else{
						dblOtaTax=0.0;
					}
					
					dblTotalOtaAmout=dblOtaCommission+dblOtaTax;
					
					jsonOutput += ",\"otaCommission\":\" "+df.format(dblOtaCommission)+"\"";
					jsonOutput += ",\"otaTax\":\" "+df.format(dblOtaTax)+"\"";
					jsonOutput += ",\"otaRevenueAmount\":\" "+df.format(dblTotalOtaAmout)+"\"";
					
					dblRevenueAmount=dblBaseAmount-dblTotalOtaAmout;
					dblTotalRevenueTaxAmount=dblRevenueAmount+dblTaxAmount;
					jsonOutput += ",\"revenueAmount\":\" "+df.format(dblRevenueAmount)+"\"";
					jsonOutput += ",\"taxrevenueAmount\":\" "+df.format(dblTaxAmount)+"\"";
					jsonOutput += ",\"revenueTaxAmount\":\" "+df.format(dblTotalRevenueTaxAmount)+"\"";
					
					PmsProperty pmsPropertyDetails=pmsPropertyController.find(getPropertyId());
					dblCommission=pmsPropertyDetails.getPropertyCommission();
					if(dblCommission!=null){
						dblUloCommission=dblRevenueAmount*dblCommission/100;
					}
					
					PropertyAccommodation accommodation=accommodationController.find(bookingDetailReport.getAccommodationId());
					double netRateAmount=0;
					if(accommodation.getNetRateRevenue()!=null){
						netRateAmount=accommodation.getNetRateRevenue();	
					}
					
					
					
					double dblNetRevenue=0,dblRoomNightsRevenue=0,dblbaseamount=0,dblotarevenue=0;
						
					dblRoomNightsRevenue=dblRevenueAmount/diffRoomInDays;
					dblNetRevenue=dblRoomNightsRevenue-netRateAmount;
					
					jsonOutput += ",\"netRateRevenue\":\" "+df.format(dblNetRevenue*diffRoomInDays)+"\"";
			
					jsonOutput += ",\"uloCommission\":\" "+df.format(dblUloCommission)+"\"";
					
					dblUloTaxAmount=dblUloCommission*18/100;
					jsonOutput += ",\"uloTaxAmount\":\"" + df.format(dblUloTaxAmount)+ "\"";
					
					dblTotalUloCommission=dblUloCommission+dblUloTaxAmount;
					jsonOutput += ",\"uloPayable\":\"" + df.format(dblTotalUloCommission)+ "\"";
					
					dblHotelPayable=dblRevenueAmount-dblTotalUloCommission;
					dblHotelTaxPayable=dblHotelPayable+dblTaxAmount;
					dblNetHotelPayable=dblHotelTaxPayable-dblDueAmount;
					jsonOutput += ",\"netHotelPayable\":\"" + df.format(dblNetHotelPayable)+ "\"";
					jsonOutput += ",\"hotelPayable\":\"" + df.format(dblHotelTaxPayable)+ "\"";
					
					Double dblAddonAmount=null;
					dblAddonAmount=bookingDetailReport.getAddonAmount();
					String strAddonRemarks=null;
					strAddonRemarks=bookingDetailReport.getAddonDescription();
					if(dblAddonAmount!=null){
						jsonOutput += ",\"addonAmount\":\"" + df.format(bookingDetailReport.getAddonAmount()==null?"0":bookingDetailReport.getAddonAmount())+ "\"";	
					}else{
						jsonOutput += ",\"addonAmount\":\"" + 0 + "\"";
					}
					
					jsonOutput += ",\"addonRemarks\":\"" + (strAddonRemarks==null?"NA":strAddonRemarks)+ "\"";
					
					
					if(dblAdvanceAmount!=null && dblAdvanceAmount>=0.0){
						if(compareValues){
							jsonOutput += ",\"paymentRemarks\":\" CASH PAID\"";
						}else{
							jsonOutput += ",\"paymentRemarks\":\" PAY AT HOTEL\"";
						}
					}else{
						if(sourceId==1){
							jsonOutput += ",\"paymentRemarks\":\" CASH PAID \"";
						}else{
							if(compareValues){
								jsonOutput += ",\"paymentRemarks\":\" CASH PAID\"";
							}else{
								jsonOutput += ",\"paymentRemarks\":\" PAY AT HOTEL\"";
							}
						}
					}
					
					statusId=bookingDetailReport.getStatusId();
					PmsStatus status=statusController.find(statusId);
					statusName=status.getStatus();
					jsonOutput += ",\"status\":\"" + statusName+ "\"";
					sourceId=bookingDetailReport.getSourceId();
					PmsSource source=sourceController.find(sourceId);
					sourceName=source.getSourceName().trim();
					jsonOutput += ",\"source\":\"" + sourceName+ "\"";
					
					Integer loginUserId;
					loginUserId=bookingDetailReport.getCreatedBy();
					if(loginUserId!=null){
						loginUserId=bookingDetailReport.getCreatedBy();
						User userDetails=userController.find(loginUserId);
						jsonOutput += ",\"userName\":\"" + userDetails.getUserName()+ "\"";
					}else{
						jsonOutput += ",\"userName\":\"NA\"";
					}
					
					jsonOutput += "}";
				}
			}
		}
		response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
	} catch (Exception e) {
		logger.error(e);
		e.printStackTrace();
	} finally {

	}

	return null;

}

	public String getPaymentStatus(){
		try{
			RazorpayClient razorpayClient = new RazorpayClient("rzp_live_vuYF9peaDXMoQF", "mEmSEzreAQqw8iH9YGHTTwBI");
			JSONObject paymentRequest = new JSONObject();
			
			java.util.Date date = new java.util.Date();
			Calendar calCheckStart=Calendar.getInstance();
			Calendar calCheckEnd=Calendar.getInstance();
			calCheckStart.setTime(date);
			calCheckEnd.setTime(date);
			calCheckStart.add(Calendar.MONTH, -1);
			calCheckEnd.add(Calendar.MONTH, 1);
		    java.util.Date checkStartDate = calCheckStart.getTime();
		    java.util.Date checkEndDate = calCheckEnd.getTime();
			int checkStart = (int) (checkStartDate.getTime()/1000);
			int checkEnd = (int) (checkEndDate.getTime()/1000);
			   
			paymentRequest.put("count", 100);
			//paymentRequest.put("from", checkStart);
			//paymentRequest.put("to", checkEnd);
			//paymentRequest.put("currency", "INR");
			//paymentRequest.put("skip", 1);
			
			
			List<Payment> payments = razorpayClient.Payments.fetchAll(paymentRequest);
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("application/json");
			int totalPaymentSize=payments.size();
			if(payments.size()>0 && !payments.isEmpty()){
				for(int i=0;i<totalPaymentSize;i++){
				}
			}
			response.getWriter().write("{\"data\":" + payments + "}");
		}catch(Exception e){
			logger.error(e);
			e.printStackTrace();
		}finally{
			
		}
		return null;
	}

	public String getReportColumns(){
		try{
			RoleAccessRightManager roleAccessController = new RoleAccessRightManager();
			AccessRightManager accessController=new AccessRightManager();
			String jsonOutput="";
			String[] reportColumnList={};
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("application/json");
			if(getReportName().equalsIgnoreCase("HOTEL BOOKING REPORTS")){
				reportColumnList=bookingReportColumns;
			}else if(getReportName().equalsIgnoreCase("REVENUE REPORTS")){
				reportColumnList=revenueReportColumns;
			}else if(getReportName().equalsIgnoreCase("USER REPORTS")){
				reportColumnList=userReportColumns;
			}else if(getReportName().equalsIgnoreCase("CANCELLATION REPORTS")){
				reportColumnList=cancellationReportColumns;
			}else if(getReportName().equalsIgnoreCase("ULO BOOKING REPORTS")){
				reportColumnList=uloBookingReportColumns;
			}else if(getReportName().equalsIgnoreCase("RESORT BOOKING REPORTS")){
				reportColumnList=hotelBookingReportColumns;
			}
			
			for(int i=0;i<reportColumnList.length;i++){
				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				
				
				jsonOutput += "\"columnId\":\"" + i+ "\"";
				jsonOutput += ",\"columnName\":\"" + reportColumnList[i] + "\"";
				
				jsonOutput += "}";
				
			}
			
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
		}catch(Exception e){
			logger.error(e);
			e.printStackTrace();
		}
		return null;
	}
	
	public String getReportDetails(){
		try{
			UserLoginManager linkController = new UserLoginManager();
			User user1 =linkController.findUser(user.getUserId());
			RoleAccessRightManager roleAccessController = new RoleAccessRightManager();
			AccessRightManager accessController=new AccessRightManager();
			RoleManager roleController =new RoleManager();
			String jsonOutput="",roleName=null;
			String reportList[]={};
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("application/json");
			if(user1!=null){
				Role role=roleController.find(user1.getRole().getRoleId());
				roleName=role.getRoleName();
			}else{
				roleName="NA";
			}
			
			if(roleName!=null){
				
				if(roleName.contains("Admin")){
					reportList=admin;
				}else if(roleName.contains("Revenue")){
					reportList=revenue;
				}else if(roleName.contains("Reservation")){
					reportList=reservation;
				}else if(roleName.contains("SEO")){
					reportList=seo;
				}else if(roleName.contains("Finance")){
					reportList=finance;
				}else if(roleName.contains("Operation")){
					reportList=operation;
				}else if(roleName.contains("Digital")){
					reportList=digital;
				}else if(roleName.contains("NA")){
					reportList=reservation;
				}
				int i=0;
				while(i<reportList.length){
					if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += ",{";
					else
						jsonOutput += "{";
					jsonOutput += "\"reportName\":\"" + reportList[i++] + "\"";
					jsonOutput += ",\"reportId\":\"" + i+ "\"";
					jsonOutput += ",\"isEnable\":\"" + false + "\"";
					jsonOutput += "}";
				}
			}
			
			/*List<RoleAccessRight> roleAccessList = roleAccessController.listReports(user1.getRole().getRoleId());
			if(roleAccessList.size()>0 && !roleAccessList.isEmpty()){
				for(RoleAccessRight roleaccess:roleAccessList){
					
					AccessRight accessRight=accessController.find(roleaccess.getAccessRight().getAccessRightsId());
					String displayName=accessRight.getDisplayName();
					if(displayName.contains("REPORTS") && !displayName.equalsIgnoreCase("ULO REPORTS") && !displayName.equalsIgnoreCase("ULO CONSOLIDATE REPORTS")){
						
						if (!jsonOutput.equalsIgnoreCase(""))
							jsonOutput += ",{";
						else
							jsonOutput += "{";
						
						jsonOutput += "\"reportId\":\"" + accessRight.getAccessRightsId()+ "\"";
						jsonOutput += ",\"reportName\":\"" + displayName + "\"";
						jsonOutput += ",\"isEnable\":\"" + false + "\"";
						jsonOutput += "}";	
					}
					
				}
			}*/
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
		}catch(Exception e){
			logger.error(e);
			e.printStackTrace();
		}
		return null;
	}
	public String getInvoiceDetails(){
		try{
			this.invoiceId=getInvoiceId();
			String jsonOutput="";
			PmsBookingManager bookingController=new PmsBookingManager();
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("application/json");
			List<PmsBooking> listInvoiceDetails=bookingController.findInvoiceId(this.invoiceId);
			if(listInvoiceDetails.size()>0 && !listInvoiceDetails.isEmpty()){
				for(PmsBooking pmsbooking:listInvoiceDetails){
					if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += ",{";
					else
						jsonOutput += "{";
					
					jsonOutput += "\"bookingId\":\"" + pmsbooking.getBookingId()+ "\"";
					
					jsonOutput += "}";
				}
			}
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			
		}catch(Exception e){
			logger.error(e);
			e.printStackTrace();
		}finally{
			
		}
		return null;
	}
	
	public String getPropertyAccommodations(){
		try {
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			
			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
			response.setContentType("application/json");

			List<PropertyAccommodation>accommodationList = accommodationController.list(getPropertyId());
			for (PropertyAccommodation accommodation : accommodationList) {

				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				
				jsonOutput += "\"accommodationId\":\"" + accommodation.getAccommodationId() + "\"";
				jsonOutput += ",\"accommodationType\":\"" + accommodation.getAccommodationType()+ "\"";

				jsonOutput += "}";


			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;
		
	}
	
	public String getConsolidateBookingRecords(){
		try{
			
			DateFormat format = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
		    java.util.Date dateStart = format.parse(getStrFromDate());
		    java.util.Date dateEnd = format.parse(getStrToDate());
		   
		    PmsPropertyManager propertyController=new PmsPropertyManager();
		    Calendar calCheckStart=Calendar.getInstance();
		    calCheckStart.setTime(dateStart);
		    java.util.Date checkInDate = calCheckStart.getTime();
		    this.fromDate=new Date(checkInDate.getTime());
		    sessionMap.put("fromDate",fromDate); 
		    
		    Calendar calCheckEnd=Calendar.getInstance();
		    calCheckEnd.setTime(dateEnd);
		    calCheckEnd.add(Calendar.DATE, -1);
		    java.util.Date checkOutDate = calCheckEnd.getTime();
		    this.toDate=new Date(checkOutDate.getTime());
		    sessionMap.put("endDate",toDate); 

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			DecimalFormat df = new DecimalFormat("###.##");
			ReportManager reportController = new ReportManager();
			
			reportEnabled=true;
			
			response.setContentType("application/json");
			Integer accommodationId=0;
			String strAccommodationType=null;
			PropertyAccommodationManager accommodationController=new PropertyAccommodationManager();
			this.accommodationId=0;
			String[] propertyArray = getCheckedProperty().split("\\s*,\\s*");
		    int[] intPropertyArray = new int[propertyArray.length];
		    for (int i = 0; i < propertyArray.length; i++) {
		    	intPropertyArray[i] = Integer.parseInt(propertyArray[i]);
			    if(intPropertyArray[i]!=0){
			    	
			    	
			    	Double totalRoomNights=0.0,dblTotalRevenueTaxAmount=0.0;
			    	
			    	this.listBookingDetails=reportController.listBookingDetail(intPropertyArray[i],this.getFromDate(),this.getToDate(),getFilterDateName());
					if(listBookingDetails.size()>0 && !listBookingDetails.isEmpty()){
						for(BookingDetailReport bookingDetailReport:listBookingDetails){
							Integer rooms=0,noOfRooms=0; 
							SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
							
							Timestamp FromDate=bookingDetailReport.getArrivalDate();
							Timestamp ToDate=bookingDetailReport.getDepartureDate();
							
							String strFromDate=format2.format(FromDate);
							String strToDate=format2.format(ToDate);
							
					    	DateTime startdate = DateTime.parse(strFromDate);
					        DateTime enddate = DateTime.parse(strToDate);
					        java.util.Date fromdate = startdate.toDate();
					 		java.util.Date todate = enddate.toDate();
					 		
					 		int diffInDays  = (int) ((todate.getTime() - fromdate.getTime()) / (1000 * 60 * 60 * 24));
					 		
					 		long lngrooms=bookingDetailReport.getRoomCount();
							rooms=(int)lngrooms;
							noOfRooms=rooms/diffInDays;
					 		Integer diffRoomInDays=noOfRooms*diffInDays;
					 		
					 		
					 		
					 		if(getSearchDateType().equalsIgnoreCase("Single")){
					 			totalRoomNights+=Double.valueOf(String.valueOf(noOfRooms));
					 		}else if(getSearchDateType().equalsIgnoreCase("Multiple")){
					 			totalRoomNights+=Double.valueOf(String.valueOf(diffRoomInDays));
					 		}
					 		Double dblBaseAmount=0.0,dblTaxAmount=0.0;
							Double dblOtaCommission=0.0,dblOtaTax=0.0,dblTotalOtaAmout=0.0,dblRevenueAmount=0.0;
							
							
							dblBaseAmount=bookingDetailReport.getAmount();
							dblTaxAmount=bookingDetailReport.getTaxAmount();
							if(dblBaseAmount!=null && dblBaseAmount>=0.0){
								dblBaseAmount=bookingDetailReport.getAmount();
							}else{
								dblBaseAmount=0.0;
							}
							
							if(dblTaxAmount!=null && dblTaxAmount>=0.0){
								dblTaxAmount=bookingDetailReport.getTaxAmount();
							}else{
								dblTaxAmount=0.0;
							}
							
							
							dblOtaCommission=bookingDetailReport.getOtaCommission();
							dblOtaTax=bookingDetailReport.getOtaTax();
							if(dblOtaCommission!=null && dblOtaCommission>=0.0){
								dblOtaCommission=bookingDetailReport.getOtaCommission();
							}else{
								dblOtaCommission=0.0;
							}
							if(dblOtaTax!=null && dblOtaTax>=0.0){
								dblOtaTax=bookingDetailReport.getOtaTax();
							}else{
								dblOtaTax=0.0;
							}
							
							dblTotalOtaAmout=dblOtaCommission+dblOtaTax;
							
							dblRevenueAmount=dblBaseAmount-dblTotalOtaAmout;
							
							double revenueTaxAmount=dblRevenueAmount+dblTaxAmount;
							if(getSearchDateType().equalsIgnoreCase("Single")){
					 			dblTotalRevenueTaxAmount+=revenueTaxAmount/diffInDays;
					 		}else if(getSearchDateType().equalsIgnoreCase("Multiple")){
					 			dblTotalRevenueTaxAmount+=revenueTaxAmount;
					 		}
							
						}
					}
	
					if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += ",{";
					else
						jsonOutput += "{";
					if(reportEnabled){
						int j=0;
						while(j<revenueReportKey.length){
							PmsProperty property=propertyController.find(intPropertyArray[i]);   
							jsonOutput += "\""+revenueReportKey[j++]+"\":\"" + property.getPropertyName()+ "\"";
							jsonOutput += ",\""+revenueReportKey[j++]+"\":\"" + property.getLocation().getLocationName() + "\"";
							
							jsonOutput += ",\""+revenueReportKey[j++]+"\":\"" + totalRoomNights.intValue()+ "\"";
							jsonOutput += ",\""+revenueReportKey[j++]+"\":\"" + df.format(dblTotalRevenueTaxAmount)+ "\"";	
						}
						
					}
								
					
					jsonOutput += "}";
	
						
				
			    
		    	}
		    }
		    
		    response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
		}catch(Exception e){
			logger.error(e);
			e.printStackTrace();
		}
		
		return null;
	}
	
	public String getBookingRecords(){
		try{
			String[] reportColumnList={};
			reportEnabled=false;
			if(getReportName().equalsIgnoreCase("HOTEL BOOKING REPORTS")){
				reportColumnList=bookingReportColumns;
				reportEnabled=true;
				bookingReport();
			}else if(getReportName().equalsIgnoreCase("REVENUE REPORTS")){
				reportColumnList=revenueReportColumns;
				reportEnabled=true;
				RevenueReport();
			}else if(getReportName().equalsIgnoreCase("USER REPORTS")){
				reportColumnList=userReportColumns;
				reportEnabled=true;
				userGuestReport();
			}else if(getReportName().equalsIgnoreCase("CANCELLATION REPORTS")){
				reportColumnList=cancellationReportColumns;
				reportEnabled=true;
				cancellationReport();
			}else if(getReportName().equalsIgnoreCase("ULO BOOKING REPORTS")){
				reportColumnList=uloBookingReportColumns;
				reportEnabled=true;
				uloBookingReport();
			}else if(getReportName().equalsIgnoreCase("RESORT BOOKING REPORTS")){
				reportColumnList=hotelBookingReportColumns;
				reportEnabled=true;
				resortBookingReport();
			}
			
			
		}catch(Exception e){
			logger.error(e);
			e.printStackTrace();
		}
		return null;
	}
	
	public String getChartInventoryRates() throws IOException{
		try{
			DecimalFormat df = new DecimalFormat("###.##");
			this.propertyId=getPropertyId();
			if(this.propertyId==null){
				this.propertyId = (Integer) sessionMap.get("propertyId");
			}
			DateFormat format = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
		    java.util.Date dateStart = format.parse(getStrFromDate());
		    java.util.Date dateEnd = format.parse(getStrToDate());
		    SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
		    SimpleDateFormat format3 = new SimpleDateFormat("MMM d");
		    Calendar calCheckStart=Calendar.getInstance();
		    calCheckStart.setTime(dateStart);
		    java.util.Date checkInDate = calCheckStart.getTime();
		    this.fromDate=new Date(checkInDate.getTime());
		    sessionMap.put("fromDate",fromDate); 
		    
		    Calendar calCheckEnd=Calendar.getInstance();
		    calCheckEnd.setTime(dateEnd);
		    java.util.Date checkOutDate = calCheckEnd.getTime();
		    this.toDate=new Date(checkOutDate.getTime());
		    sessionMap.put("endDate",toDate);
		    String strFromDate=format2.format(fromDate);
			String strToDate=format2.format(toDate);
			DateTime startdate = DateTime.parse(strFromDate);
	        DateTime enddate = DateTime.parse(strToDate);
			java.util.Date fromdate = startdate.toDate();
	 		java.util.Date todate = enddate.toDate();
	 		
	 		int diffInDays  = (int) ((todate.getTime() - fromdate.getTime()) / (1000 * 60 * 60 * 24));
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("application/json");
			ReportManager reportController=new ReportManager();
			PropertyAccommodationManager accommodationController=new PropertyAccommodationManager();
			
  			
			List<DateTime> between = DateUtil.getDateRange(startdate, enddate);
			for(DateTime dt:between){
				this.availDate=new Date(dt.toDate().getTime());
				int totalRoomCount=0;
				double totalReveune=0;
				List<BookingDetailReport> listInventoryRevenue=reportController.getInventoryRates(propertyId, availDate, accommodationId);
				if(listInventoryRevenue.size()>0 && !listInventoryRevenue.isEmpty()){
					for(BookingDetailReport inventoryRates:listInventoryRevenue){
						int rooms=0;
						double revenueAmount=0;
						long lngrooms=inventoryRates.getRoomCount();
						rooms=(int)lngrooms;
						totalRoomCount+=rooms;	
						revenueAmount=inventoryRates.getAmount();
						totalReveune+=revenueAmount;
					}
				}
				if (!jsonOutput.equalsIgnoreCase(""))
	  				jsonOutput += ",{";
	  			else
	  				jsonOutput += "{";
					
				jsonOutput += "\"accommodationId\":\"" + accommodationId+ "\"";
				PropertyAccommodation accommodation=accommodationController.find(accommodationId);
				jsonOutput += ",\"accommodationType\":\"" + accommodation.getAccommodationType() + "\"";
	  			jsonOutput += ",\"roomCount\":\"" + accommodation.getNoOfUnits() + "\"";
				
				if(listInventoryRevenue.size()>0 && !listInventoryRevenue.isEmpty()){
					double revenueRange=0.0;
					revenueRange=totalReveune/totalRoomCount;
					if(revenueRange>0){
						String strTotal=df.format(revenueRange);
						revenueRange=Double.valueOf(strTotal);
					}
					int range=0;
					if(revenueRange<1000){
						range=1;
					}else if(revenueRange>1000 && revenueRange<2500){
						range=2;
					}else if(revenueRange>2500 && revenueRange<7500){
						range=3;
					}else if(revenueRange>7500){
						range=4;
					}
					
					jsonOutput += ",\"occupancy\":\"" + totalRoomCount+ "\"";
					jsonOutput += ",\"daysmonth\":\"" + format3.format(availDate)+ "\"";
					jsonOutput += ",\"revenueRange\":\"" + range + "\"";
					jsonOutput += ",\"revenueAmount\":\"" + revenueRange + "\"";	
				}else{
					double revenueRange=0;
					int range=0;
					jsonOutput += ",\"occupancy\":\"" + totalRoomCount+ "\"";
					jsonOutput += ",\"daysmonth\":\"" + format3.format(availDate)+ "\"";
					jsonOutput += ",\"revenueRange\":\"" + range + "\"";
					jsonOutput += ",\"revenueAmount\":\"" + revenueRange + "\"";
				}
				
	  			
				jsonOutput += "}";
				
			}
//			response.getWriter().write(jsonRevenue);
		    
		    response.getWriter().write("{\"data\":[" + jsonOutput+ "]}");
		}catch(Exception e){
			logger.error(e);
			e.printStackTrace();
		}
		return null;
		
	}
	
	public String getSubscriberMailList(){

		try {
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			ReportManager reportController = new ReportManager();
			int count=1;
		    DateFormat format1 = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
		    java.util.Date dateStart = format1.parse(getStrFromDate());
		    java.util.Date dateEnd = format1.parse(getStrToDate());
		   	DateFormat format = new SimpleDateFormat("dd-MMM-yyyy");
			response.setContentType("application/json");
			List<UserGuestDetailReport> subscriberUserList=reportController.subscriberUserDetails(dateStart,dateEnd);
			
			if(subscriberUserList.size()>0 && !subscriberUserList.isEmpty()){
				for (UserGuestDetailReport userguest : subscriberUserList) {

					
					if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += ",{";
					else
						jsonOutput += "{";
					
					jsonOutput += "\"serialNo\":\"" + count+ "\"";
					jsonOutput += ",\"subscriberMailId\":\"" + userguest.getEmailId()+ "\"";
					jsonOutput += ",\"subscriberDate\":\"" + format.format(userguest.getCreatedDate())+ "\"";
					
					
					jsonOutput += "}";
					count++;

				}
			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	
	}
	
	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	
	public int getAccommodationId() {
		return accommodationId;
	}

	public void setAccommodationId(int accommodationId) {
		this.accommodationId = accommodationId;
	}
	
	public InputStream getFileInputStream() {
		return fileInputStream;
	}

	public void setFileInputStream(InputStream fileInputStream) {
		this.fileInputStream = fileInputStream;
	}
	
	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	public String getFilterDateName() {
		return filterDateName;
	}

	public void setFilterDateName(String filterDateName) {
		this.filterDateName = filterDateName;
	}

	public String getFilterBookingId() {
		return filterBookingId;
	}

	public void setFilterBookingId(String filterBookingId) {
		this.filterBookingId = filterBookingId;
	}
	
	public String getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(String invoiceId) {
		this.invoiceId = invoiceId;
	}
	
	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getReportId() {
		return reportId;
	}

	public void setReportId(Integer reportId) {
		this.reportId = reportId;
	}

	public String getReportName() {
		return reportName;
	}

	public void setReportName(String reportName) {
		this.reportName = reportName;
	}
	
	public boolean isReportEnabled() {
		return reportEnabled;
	}

	public void setReportEnabled(boolean reportEnabled) {
		this.reportEnabled = reportEnabled;
	}
	
	public String getCheckedProperty() {
		return checkedProperty;
	}

	public void setCheckedProperty(String checkedProperty) {
		this.checkedProperty = checkedProperty;
	}
	
	public String getStatusId() {
		return statusId;
	}

	public void setStatusId(String statusId) {
		this.statusId = statusId;
	}

	public String getSearchDateType() {
		return searchDateType;
	}

	public void setSearchDateType(String searchDateType) {
		this.searchDateType = searchDateType;
	}
	
	public Integer getPartnerPropertyId() {
		return partnerPropertyId;
	}

	public void setPartnerPropertyId(Integer partnerPropertyId) {
		this.partnerPropertyId = partnerPropertyId;
	}

	public Integer getSelectPartnerPropertyId() {
		return selectPartnerPropertyId;
	}

	public void setSelectPartnerPropertyId(Integer selectPartnerPropertyId) {
		this.selectPartnerPropertyId = selectPartnerPropertyId;
	}
}
