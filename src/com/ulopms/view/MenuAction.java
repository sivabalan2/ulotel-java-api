package com.ulopms.view;

import java.sql.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;

import com.opensymphony.xwork2.ActionSupport;
import com.ulopms.controller.MenuManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.Menu;
import com.ulopms.model.User;

import javax.servlet.http.HttpServletRequest;

public class MenuAction extends ActionSupport implements ServletRequestAware,
UserAware {

	private static final long serialVersionUID = 9149826260758390091L;

	private Menu menu;

	private int menuId;
	private String menuName;
	private User user;
	private List<Menu> menuList;

	private static final Logger logger = Logger.getLogger(MenuAction.class);

	private MenuManager menuController = new MenuManager();

	// @Override
	// public User getModel() {
	// return user;
	// }

	@Override
	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	private HttpServletRequest request;

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	public MenuAction() {

	}

	public String execute() {

		try {
			// this.screenList = screenController.list();
			if (request.getParameter("id") != null) {
				Menu s = menuController.find(Long.parseLong(request
						.getParameter("id")));
				this.setMenuId(s.getMenuId());
				this.setMenuName(s.getMenuName());
				s = null;

			}
		} catch (Exception e) {
			logger.error(e);
		} finally {

		}
		return SUCCESS;
	}

	public String MenuSave() {

		return SUCCESS;
	}

	public String MenuList() {
		try {
			this.menuList = menuController.list();
		} catch (Exception e) {
			logger.error(e);
		} finally {

		}
		return SUCCESS;
	}

	

	public Menu getMenu() {
		return menu;
	}

	public void setMenu(Menu menu) {
		this.menu = menu;
	}

	public int getMenuId() {
		return menuId;
	}

	public void setMenuId(int menuId) {
		this.menuId = menuId;
	}

	public String getMenuName() {
		return menuName;
	}

	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}

	public List<Menu> getMenuList() {
		return menuList;
	}

	public void setMenuList(List<Menu> menuList) {
		this.menuList = menuList;
	}

}
