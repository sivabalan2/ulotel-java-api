package com.ulopms.view;


import java.io.IOException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.struts2.interceptor.ServletRequestAware;

import com.opensymphony.xwork2.ActionSupport;
import com.ulopms.controller.PmsPropertyManager;
import com.ulopms.controller.PropertyAccommodationManager;
import com.ulopms.controller.PropertyRatePlanDetailManager;
import com.ulopms.controller.PropertyRatePlanManager;
import com.ulopms.controller.UserLoginManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.BookingDetailReport;
import com.ulopms.model.PmsProperty;
import com.ulopms.model.PropertyAccommodation;
import com.ulopms.model.PropertyRatePlan;
import com.ulopms.model.PropertyRatePlanDetail;
import com.ulopms.model.User;

public class PropertyRatePlanAction extends ActionSupport implements
ServletRequestAware,SessionAware, UserAware{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private HttpSession session;
	private String ratePlanName;
	private String ratePlanType;
	private Integer ratePlanId;
	private boolean planIsActive;
	private boolean taxIsActive;
	

	private Integer accommodationId;
	

	private Timestamp startDate;
	private Timestamp endDate;
	private Integer propertyId;
	private Integer userId;
	private String strStartDate;
	private String strEndDate;
	List<BookingListAction> array = new ArrayList<BookingListAction>();
	private Integer limit;
	private Integer offset;
	
	
	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}
	
	private SessionMap<String,Object> sessionMap;  
	  
	private static final Logger logger = Logger.getLogger(PropertyRateDetailAction.class);
	
	//getters and setters  
	  
	@Override  
	public void setSession(Map<String, Object> map) {  
	    sessionMap=(SessionMap)map;  
	}  
	
	private User user;


	@Override
	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	
	private HttpServletRequest request;

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	public PropertyRatePlanAction() {

	}

	public String execute() {//.
		return SUCCESS;
	}

	public String addRatePlan(){
		try{
			this.propertyId = (Integer) sessionMap.get("propertyId");
			this.userId=(Integer)sessionMap.get("userId");
			
			DateFormat format = new SimpleDateFormat("MMM d,yyyy", Locale.ENGLISH);
		    java.util.Date dateStart = format.parse(getStrStartDate());
		    java.util.Date dateEnd = format.parse(getStrEndDate());
		   
		   
		    Calendar calCheckStart=Calendar.getInstance();
		    calCheckStart.setTime(dateStart);
		    java.util.Date checkInDate = calCheckStart.getTime();
		    this.startDate=new Timestamp(checkInDate.getTime());
		   
		    
		    Calendar calCheckEnd=Calendar.getInstance();
		    calCheckEnd.setTime(dateEnd);
		    java.util.Date checkOutDate = calCheckEnd.getTime();
		    this.endDate=new Timestamp(checkOutDate.getTime());
			
			PropertyRatePlan pmsRatePlan=new PropertyRatePlan();
			PropertyRatePlanManager ratePlanController=new PropertyRatePlanManager();
			pmsRatePlan.setRatePlanName(getRatePlanName());
			pmsRatePlan.setRatePlanType(getRatePlanType());
			pmsRatePlan.setStartDate(this.startDate);
			pmsRatePlan.setEndDate(this.endDate);
			pmsRatePlan.setIsActive(true);
			pmsRatePlan.setIsDeleted(false);
			pmsRatePlan.setRateIsActive(true);
			pmsRatePlan.setTaxIsActive(isTaxIsActive());
			pmsRatePlan.setCreatedBy(this.userId);
			ratePlanController.add(pmsRatePlan);
		}catch(Exception ex){
			logger.error(ex);
			ex.printStackTrace();
		}
		return null;
	}
	
	public String editRatePlanId(){
		try{
			this.propertyId = (Integer) sessionMap.get("propertyId");
			this.userId=(Integer)sessionMap.get("userId");
			
			DateFormat format = new SimpleDateFormat("MMM d,yyyy", Locale.ENGLISH);
		    java.util.Date dateStart = format.parse(getStrStartDate());
		    java.util.Date dateEnd = format.parse(getStrEndDate());
		   
		   
		    Calendar calCheckStart=Calendar.getInstance();
		    calCheckStart.setTime(dateStart);
		    java.util.Date checkInDate = calCheckStart.getTime();
		    this.startDate=new Timestamp(checkInDate.getTime());
		   
		    
		    Calendar calCheckEnd=Calendar.getInstance();
		    calCheckEnd.setTime(dateEnd);
		    java.util.Date checkOutDate = calCheckEnd.getTime();
		    this.endDate=new Timestamp(checkOutDate.getTime());
			
			PropertyRatePlanManager ratePlanController=new PropertyRatePlanManager();
			
			PropertyRatePlan ratePlan=ratePlanController.find(getRatePlanId());
			ratePlan.setRatePlanName(getRatePlanName());
			ratePlan.setRatePlanType(getRatePlanType());
			ratePlan.setStartDate(this.startDate);
			ratePlan.setEndDate(this.endDate);
			ratePlan.setIsActive(true);
			ratePlan.setIsDeleted(false);
			ratePlan.setRateIsActive(isPlanIsActive());
			ratePlan.setTaxIsActive(isTaxIsActive());
			ratePlan.setCreatedBy(this.userId);
			ratePlanController.add(ratePlan);
		}catch(Exception ex){
			logger.error(ex);
			ex.printStackTrace();
		}
		return null;
	}
	
	public String editRatePlan(){
		try{
			PropertyRatePlanDetail ratePlanDetail=new PropertyRatePlanDetail();
			PropertyRatePlanDetailManager ratePlanDetailController=new PropertyRatePlanDetailManager();
			PmsPropertyManager propertyController=new PmsPropertyManager();
			PropertyAccommodationManager accommodationController=new PropertyAccommodationManager();
			PropertyRatePlanManager ratePlanController=new PropertyRatePlanManager();
			this.propertyId = (Integer) sessionMap.get("propertyId");
			this.userId=(Integer)sessionMap.get("userId");
			
			SimpleDateFormat sdfformat = new SimpleDateFormat("yyyy-MM-dd");
			java.util.Date date=new java.util.Date();
        	Calendar calDate=Calendar.getInstance();
        	calDate.setTime(date);
        	calDate.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
        	String strCurrentDate = new SimpleDateFormat("yyyy-MM-dd").format(calDate.getTime());
		    java.util.Date currentDate = sdfformat.parse(strCurrentDate);
		    Timestamp tsCurrentDate = new Timestamp(currentDate.getTime());
		    int count=1;
		    int arrSize=array.size();
		    for (int i = 0; i < array.size(); i++) 
            {
		    	if(count==arrSize){
		    		List<PropertyRatePlanDetail> listRateDetails=ratePlanDetailController.list(this.propertyId, array.get(i).getAccommodationId());
			    	if(listRateDetails.size()>0 && !listRateDetails.isEmpty()){
			    		for(PropertyRatePlanDetail rateDetails:listRateDetails){
			    			rateDetails.setIsActive(false);
			    			rateDetails.setIsDeleted(true);
			    			ratePlanDetailController.edit(rateDetails);
			    		}
			    	}
		    	}
		    	
		    	count++;
            }
		    
		    for(int i=0;i<array.size(); i++){
	    		PropertyRatePlan ratePlans=ratePlanController.find(array.get(i).getRatePlanId());
	    		ratePlans.setRatePlanSymbolType(array.get(i).getRatePlanSymbolType());
	    		ratePlans.setRateIsActive(array.get(i).isPlanIsActive());
	    		ratePlanController.edit(ratePlans);
	    	}
		    
		    for (int i = 0; i < array.size(); i++) 
            {
		    	
		    	ratePlanDetail.setIsActive(true);
	    		ratePlanDetail.setIsDeleted(false);
	    		
	    		DateFormat format = new SimpleDateFormat("MMM d,yyyy", Locale.ENGLISH);
			    java.util.Date dateStart = format.parse(array.get(i).getStrStartDate());
			    java.util.Date dateEnd = format.parse(array.get(i).getStrEndDate());
			   
			    Calendar calCheckStart=Calendar.getInstance();
			    calCheckStart.setTime(dateStart);
			    java.util.Date checkInDate = calCheckStart.getTime();
			    this.startDate=new Timestamp(checkInDate.getTime());
			    
			    Calendar calCheckEnd=Calendar.getInstance();
			    calCheckEnd.setTime(dateEnd);
			    java.util.Date checkOutDate = calCheckEnd.getTime();
			    this.endDate=new Timestamp(checkOutDate.getTime());
			    
			    ratePlanDetail.setStartDate(this.startDate);
			    ratePlanDetail.setEndDate(this.endDate);
			    ratePlanDetail.setPlanIsActive(array.get(i).isPlanIsActive());
			    ratePlanDetail.setCreatedBy(this.userId);
			    ratePlanDetail.setCreatedOn(tsCurrentDate);
			    PmsProperty pmsProperty=propertyController.find(this.propertyId);
			    ratePlanDetail.setPmsProperty(pmsProperty);
			    PropertyRatePlan ratePlan=ratePlanController.find(array.get(i).getRatePlanId());
			    ratePlanDetail.setRatePlan(ratePlan);
			    PropertyAccommodation accommodation=accommodationController.find(array.get(i).getAccommodationId());
			    ratePlanDetail.setPropertyAccommodation(accommodation);
			    ratePlanDetail.setTariffAmount(array.get(i).getRatePlanTariff());
			    ratePlanDetailController.add(ratePlanDetail);
			    
            }
		    
		    
		}catch(Exception e){
			logger.error(e);
			e.printStackTrace();
		}
		return null;
	}
	
	public String getRatePlanIdDetails(){
		try{
			PropertyRatePlan RatePlan=new PropertyRatePlan();
			PropertyRatePlanManager ratePlanController=new PropertyRatePlanManager();
			HttpServletResponse response = ServletActionContext.getResponse();
			String jsonOutput="";
			response.setContentType("application/json");
		
			List<PropertyRatePlan> listRatePlan=ratePlanController.list(getRatePlanId());
			if(listRatePlan.size()>0 && !listRatePlan.isEmpty()){
				for(PropertyRatePlan ratePlan:listRatePlan){
					if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += ",{";
					else
						jsonOutput += "{";
					
					jsonOutput += "\"ratePlanId\":\"" + ratePlan.getRatePlanId()+ "\"";
					jsonOutput += ",\"ratePlanType\":\"" + ratePlan.getRatePlanType()+ "\"";
					jsonOutput += ",\"ratePlanName\":\"" + ratePlan.getRatePlanName()+ "\"";
					String from = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH).format(ratePlan.getStartDate());
					String to = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH).format(ratePlan.getEndDate());
					jsonOutput += ",\"startDate\":\"" +from + "\"";
					jsonOutput += ",\"endDate\":\"" + to + "\"";
					jsonOutput += ",\"planIsActive\":\"" + ratePlan.getRateIsActive()+ "\"";
					jsonOutput += ",\"taxIsActive\":\"" + ratePlan.getTaxIsActive()+ "\"";
					jsonOutput += "}";
				}
			}
			
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
		}catch(Exception e){
			logger.error(e);
			e.printStackTrace();
		}
		return null;
	}
	
	public String getRatePlan(){
		try{
			PropertyRatePlan RatePlan=new PropertyRatePlan();
			PropertyRatePlanManager ratePlanController=new PropertyRatePlanManager();
			HttpServletResponse response = ServletActionContext.getResponse();
			String jsonOutput="";
			response.setContentType("application/json");
			int serialno=1;
			UserLoginManager userController=new UserLoginManager();
			List<PropertyRatePlan> listRatePlan=ratePlanController.list();
			if(listRatePlan.size()>0 && !listRatePlan.isEmpty()){
				for(PropertyRatePlan ratePlan:listRatePlan){
					if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += ",{";
					else
						jsonOutput += "{";
					
					jsonOutput += "\"ratePlanId\":\"" + ratePlan.getRatePlanId()+ "\"";
					jsonOutput += ",\"serialNo\":\"" +serialno+ "\"";
					jsonOutput += ",\"ratePlanType\":\"" + ratePlan.getRatePlanType()+ "\"";
					jsonOutput += ",\"ratePlanName\":\"" + ratePlan.getRatePlanName()+ "\"";
					String from = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH).format(ratePlan.getStartDate());
					String to = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH).format(ratePlan.getEndDate());
					jsonOutput += ",\"startDate\":\"" +from + "\"";
					jsonOutput += ",\"endDate\":\"" + to + "\"";
					jsonOutput += ",\"planIsActive\":\"" + ratePlan.getRateIsActive()+ "\"";
					jsonOutput += ",\"taxIsActive\":\"" + ratePlan.getTaxIsActive()+ "\"";
					jsonOutput += ",\"ratePlanTariff\":\"" + 0 + "\"";
					if(ratePlan.getCreatedBy()!=null){
						User user=userController.find(ratePlan.getCreatedBy());
						String strFirstName=user.getUserName().trim();
						String firstNameLetterUpperCase=strFirstName.substring(0, 1).toUpperCase()+strFirstName.substring(1);
						jsonOutput += ",\"createdByName\":\"" + firstNameLetterUpperCase+ "\"";
					}else{
						jsonOutput += ",\"createdByName\":\"" +"- "+ "\"";
					}
					jsonOutput += "}";
					serialno++;
				}
			}
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
		}catch(Exception e){
			logger.error(e);
			e.printStackTrace();
		}
		return null;
	}
	
	public String getRatePlanList(){
		try{
			PropertyRatePlanDetail ratePlanDetails=new PropertyRatePlanDetail();
			PropertyRatePlanDetailManager ratePlanDetailsController=new PropertyRatePlanDetailManager();
			PropertyRatePlan ratePlan=new PropertyRatePlan();
			PropertyRatePlanManager ratePlanController=new PropertyRatePlanManager();
			PropertyAccommodationManager accommodationController=new PropertyAccommodationManager();
			HttpServletResponse response = ServletActionContext.getResponse();
			String jsonOutput="";
			response.setContentType("application/json");
			Integer serialno=1;
			ArrayList<String> arlRateplan=new ArrayList<String>();
			
			this.propertyId = (Integer) sessionMap.get("propertyId");
			
			List<PropertyRatePlan> listRatePlans=ratePlanController.list();
			if(listRatePlans.size()>0 && !listRatePlans.isEmpty()){
				for(PropertyRatePlan rateplan: listRatePlans){
					if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += ",{";
					else
						jsonOutput += "{";
					List<PropertyRatePlanDetail> listRatePlanDetails=ratePlanDetailsController.list(this.propertyId,getAccommodationId(),rateplan.getRatePlanId());
					if(listRatePlanDetails.size()>0 && !listRatePlanDetails.isEmpty()){
						for(PropertyRatePlanDetail ratePlanDetail:listRatePlanDetails){
							jsonOutput += "\"ratePlanId\":\"" + ratePlanDetail.getRatePlan().getRatePlanId()+ "\"";
							jsonOutput += ",\"serialNo\":\"" +serialno+ "\"";
							jsonOutput += ",\"ratePlanType\":\"" + ratePlanDetail.getRatePlan().getRatePlanType()+ "\"";
							jsonOutput += ",\"ratePlanName\":\"" + ratePlanDetail.getRatePlan().getRatePlanName()+ "\"";
							jsonOutput += ",\"ratePlanSymbolType\":\"" + ratePlanDetail.getRatePlan().getRatePlanSymbolType()+ "\"";	
							String from = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH).format(ratePlanDetail.getRatePlan().getStartDate());
							String to = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH).format(ratePlanDetail.getRatePlan().getEndDate());
							jsonOutput += ",\"strStartDate\":\"" +from + "\"";
							jsonOutput += ",\"strEndDate\":\"" + to + "\"";
							jsonOutput += ",\"planIsActive\":\"" + ratePlanDetail.getPlanIsActive()+ "\"";
							jsonOutput += ",\"ratePlanTariff\":\"" + ratePlanDetail.getTariffAmount() + "\"";
							jsonOutput += ",\"accommodationId\":\"" + getAccommodationId() + "\"";
						}
					}else{
						List<PropertyRatePlan> listRatePlan=ratePlanController.list(rateplan.getRatePlanId());
						if(listRatePlan.size()>0 && !listRatePlan.isEmpty()){
							for(PropertyRatePlan ratePlans:listRatePlan){
								jsonOutput += "\"ratePlanId\":\"" + ratePlans.getRatePlanId()+ "\"";
								jsonOutput += ",\"serialNo\":\"" +serialno+ "\"";
								jsonOutput += ",\"ratePlanType\":\"" + ratePlans.getRatePlanType()+ "\"";
								jsonOutput += ",\"ratePlanName\":\"" + ratePlans.getRatePlanName()+ "\"";
								jsonOutput += ",\"ratePlanSymbolType\":\"" + ratePlans.getRatePlanSymbolType()+ "\"";
								String from = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH).format(ratePlans.getStartDate());
								String to = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH).format(ratePlans.getEndDate());
								jsonOutput += ",\"strStartDate\":\"" +from + "\"";
								jsonOutput += ",\"strEndDate\":\"" + to + "\"";
								jsonOutput += ",\"planIsActive\":\"" + ratePlans.getRateIsActive()+ "\"";
								PropertyAccommodation accommodations=accommodationController.find(getAccommodationId());
								jsonOutput += ",\"ratePlanTariff\":\"" + accommodations.getBaseAmount() + "\"";
								jsonOutput += ",\"accommodationId\":\"" + getAccommodationId() + "\"";
							}
						}
					}
					
					jsonOutput += "}";
					serialno++;
					
				}
			}
			
			
			
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
		}catch(Exception e){
			logger.error(e);
			e.printStackTrace();
		}
		return null;
	}
	
	public String getRatePlanRevenueList(){
		try{
			PropertyRatePlanDetail ratePlanDetails=new PropertyRatePlanDetail();
			PropertyRatePlanDetailManager ratePlanDetailsController=new PropertyRatePlanDetailManager();
			PropertyRatePlan ratePlan=new PropertyRatePlan();
			PropertyRatePlanManager ratePlanController=new PropertyRatePlanManager();
			PropertyAccommodationManager accommodationController=new PropertyAccommodationManager();
			HttpServletResponse response = ServletActionContext.getResponse();
			String jsonOutput="";
			response.setContentType("application/json");
			Integer serialno=0;
			this.propertyId = (Integer) sessionMap.get("propertyId");
			List<PropertyRatePlanDetail> listRatePlanDetails=ratePlanDetailsController.list(this.propertyId,getAccommodationId());
			if(listRatePlanDetails.size()>0 && !listRatePlanDetails.isEmpty()){
				for(PropertyRatePlanDetail ratePlanDetail:listRatePlanDetails){
					if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += ",{";
					else
						jsonOutput += "{";
					
					jsonOutput += "\"ratePlanId\":\"" + ratePlanDetail.getRatePlan().getRatePlanId()+ "\"";
					jsonOutput += ",\"serialNo\":\"" +serialno+ "\"";
					jsonOutput += ",\"ratePlanType\":\"" + ratePlanDetail.getRatePlan().getRatePlanType()+ "\"";
					jsonOutput += ",\"ratePlanName\":\"" + ratePlanDetail.getRatePlan().getRatePlanName()+ "\"";
					jsonOutput += ",\"ratePlanSymbolType\":\"" + ratePlanDetail.getRatePlan().getRatePlanSymbolType()+ "\"";	
					String from = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH).format(ratePlanDetail.getRatePlan().getStartDate());
					String to = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH).format(ratePlanDetail.getRatePlan().getEndDate());
					jsonOutput += ",\"strStartDate\":\"" +from + "\"";
					jsonOutput += ",\"strEndDate\":\"" + to + "\"";
					jsonOutput += ",\"planIsActive\":\"" + ratePlanDetail.getPlanIsActive()+ "\"";
					jsonOutput += ",\"ratePlanTariff\":\"" + ratePlanDetail.getTariffAmount() + "\"";
					jsonOutput += ",\"accommodationId\":\"" + getAccommodationId() + "\"";
					jsonOutput += "}";
					serialno++;
				}
			}else{
				List<PropertyRatePlan> listRatePlan=ratePlanController.list();
				if(listRatePlan.size()>0 && !listRatePlan.isEmpty()){
					for(PropertyRatePlan ratePlans:listRatePlan){
						if (!jsonOutput.equalsIgnoreCase(""))
							jsonOutput += ",{";
						else
							jsonOutput += "{";
						
						jsonOutput += "\"ratePlanId\":\"" + ratePlans.getRatePlanId()+ "\"";
						jsonOutput += ",\"serialNo\":\"" +serialno+ "\"";
						jsonOutput += ",\"ratePlanType\":\"" + ratePlans.getRatePlanType()+ "\"";
						jsonOutput += ",\"ratePlanName\":\"" + ratePlans.getRatePlanName()+ "\"";
						jsonOutput += ",\"ratePlanSymbolType\":\"" + ratePlans.getRatePlanSymbolType()+ "\"";
						String from = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH).format(ratePlans.getStartDate());
						String to = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH).format(ratePlans.getEndDate());
						jsonOutput += ",\"strStartDate\":\"" +from + "\"";
						jsonOutput += ",\"strEndDate\":\"" + to + "\"";
						jsonOutput += ",\"planIsActive\":\"" + ratePlans.getRateIsActive()+ "\"";
						PropertyAccommodation accommodations=accommodationController.find(getAccommodationId());
						jsonOutput += ",\"ratePlanTariff\":\"" + accommodations.getBaseAmount() + "\"";
						jsonOutput += ",\"accommodationId\":\"" + getAccommodationId() + "\"";
						jsonOutput += "}";
						serialno++;
					}
				}
			}
			
			
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
		}catch(Exception e){
			logger.error(e);
			e.printStackTrace();
		}
		return null;
	}
	public String getRatePlans(){
		try{
			PropertyRatePlan RatePlan=new PropertyRatePlan();
			PropertyRatePlanManager ratePlanController=new PropertyRatePlanManager();
			HttpServletResponse response = ServletActionContext.getResponse();
			String jsonOutput="";
			response.setContentType("application/json");
			List<PropertyRatePlan> listRatePlan=ratePlanController.list();
			if(listRatePlan.size()>0 && !listRatePlan.isEmpty()){
				for(PropertyRatePlan ratePlan:listRatePlan){
					if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += ",{";
					else
						jsonOutput += "{";
					
					jsonOutput += "\"ratePlanId\":\"" + ratePlan.getRatePlanId()+ "\"";
					
					jsonOutput += ",\"ratePlanType\":\"" + ratePlan.getRatePlanType()+ "\"";
					jsonOutput += ",\"ratePlanName\":\"" + ratePlan.getRatePlanName()+ "\"";
					
					jsonOutput += ",\"planIsActive\":\"" + ratePlan.getRateIsActive()+ "\"";
					jsonOutput += ",\"ratePlanTariff\":\"" + 0 + "\"";
					
					jsonOutput += "}";
				}
			}
			
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
		}catch(Exception e){
			logger.error(e);
			e.printStackTrace();
		}
		return null;
	}
	
	
	public String getRatePlanDetails(){
		try{
			PropertyRatePlan pmsRatePlan=new PropertyRatePlan();
			PropertyRatePlanManager ratePlanController=new PropertyRatePlanManager();
			HttpServletResponse response = ServletActionContext.getResponse();
			String jsonOutput="";
			List<PropertyRatePlan> listRatePlan=ratePlanController.list(limit,offset);
			response.setContentType("application/json");
			int serialno=1;
			if(listRatePlan.size()>0 && !listRatePlan.isEmpty()){
				for(PropertyRatePlan ratePlan:listRatePlan){
					if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += ",{";
					else
						jsonOutput += "{";
					
					jsonOutput += "\"ratePlanId\":\"" + ratePlan.getRatePlanId()+ "\"";
					jsonOutput += ",\"ratePlanType\":\"" + ratePlan.getRatePlanType()+ "\"";
					jsonOutput += ",\"ratePlanName\":\"" + ratePlan.getRatePlanName()+ "\"";
					String from = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH).format(ratePlan.getStartDate());
					String to = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH).format(ratePlan.getEndDate());
					jsonOutput += ",\"startDate\":\"" +from + "\"";
					jsonOutput += ",\"endDate\":\"" + to + "\"";
					jsonOutput += ",\"planIsActive\":\"" + ratePlan.getRateIsActive()+ "\"";
					jsonOutput += ",\"ratePlanTariff\":\"" + 0 + "\"";
					
					
					jsonOutput += "}";

				}
			}
			
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
		}catch(Exception e){
			logger.error(e);
			e.printStackTrace();
		}
		return null;
	}
	
	public String getPropertyRatePlanCount() throws IOException {
 		try {
 			String jsonOutput = "";
 			HttpServletResponse response = ServletActionContext.getResponse();
 			response.setContentType("application/json");
 			PropertyRatePlan RatePlan=new PropertyRatePlan();
			PropertyRatePlanManager ratePlanController=new PropertyRatePlanManager();
 			List<PropertyRatePlan> listRatePlanCount =ratePlanController.list();
 			if(listRatePlanCount.size()>0 && !listRatePlanCount.isEmpty()){
 				if (!jsonOutput.equalsIgnoreCase(""))
 					jsonOutput += ",{";
 				else
 					jsonOutput += "{";
 				
 			    jsonOutput += "\"count\":\"" + listRatePlanCount.size() + "\"";
 			    
 				jsonOutput += "}";
 			}
 			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
 		} catch (Exception e) {
 			logger.error(e);
 			e.printStackTrace();
 		} finally {

 		}

 		return null;

 	}
	
	
	public String getRatePlanName() {
		return ratePlanName;
	}

	public void setRatePlanName(String ratePlanName) {
		this.ratePlanName = ratePlanName;
	}

	public Timestamp getStartDate() {
		return startDate;
	}

	public void setStartDate(Timestamp startDate) {
		this.startDate = startDate;
	}

	public Timestamp getEndDate() {
		return endDate;
	}

	public void setEndDate(Timestamp endDate) {
		this.endDate = endDate;
	}

	public Integer getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(Integer propertyId) {
		this.propertyId = propertyId;
	}
	
	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	
	public String getStrStartDate() {
		return strStartDate;
	}

	public void setStrStartDate(String strStartDate) {
		this.strStartDate = strStartDate;
	}

	public String getStrEndDate() {
		return strEndDate;
	}

	public void setStrEndDate(String strEndDate) {
		this.strEndDate = strEndDate;
	}
	
	public String getRatePlanType() {
		return ratePlanType;
	}

	public void setRatePlanType(String ratePlanType) {
		this.ratePlanType = ratePlanType;
	}


	public List<BookingListAction> getArray() {
		return array;
	}

	public void setArray(List<BookingListAction> array) {
		this.array = array;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	public Integer getOffset() {
		return offset;
	}

	public void setOffset(Integer offset) {
		this.offset = offset;
	}
	
	public Integer getRatePlanId() {
		return ratePlanId;
	}

	public void setRatePlanId(Integer ratePlanId) {
		this.ratePlanId = ratePlanId;
	}
	
	public boolean isPlanIsActive() {
		return planIsActive;
	}

	public void setPlanIsActive(boolean planIsActive) {
		this.planIsActive = planIsActive;
	}

	public Integer getAccommodationId() {
		return accommodationId;
	}

	public void setAccommodationId(Integer accommodationId) {
		this.accommodationId = accommodationId;
	}

	public boolean isTaxIsActive() {
		return taxIsActive;
	}

	public void setTaxIsActive(boolean taxIsActive) {
		this.taxIsActive = taxIsActive;
	}
}
