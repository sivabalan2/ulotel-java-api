package com.ulopms.view;


import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TimeZone;

import javax.mail.internet.InternetAddress;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;
import org.joda.time.DateTime;

import com.lowagie.text.pdf.codec.Base64.InputStream;
import com.opensymphony.xwork2.ActionSupport;
import com.ulopms.controller.BookingCancellationManager;
import com.ulopms.controller.BookingDetailManager;
import com.ulopms.controller.PmsBookingManager;
import com.ulopms.controller.PmsPropertyManager;
import com.ulopms.controller.PmsStatusManager;
import com.ulopms.controller.PropertyAccommodationManager;
import com.ulopms.controller.PropertyCommissionInvoiceManager;
import com.ulopms.controller.PropertyDiscountManager;
import com.ulopms.controller.PropertyVoucherManager;
import com.ulopms.controller.ReportManager;
import com.ulopms.controller.UserLoginManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.BookingDetail;
import com.ulopms.model.BookingDetailReport;
import com.ulopms.model.PmsBookedDetails;
import com.ulopms.model.PmsBooking;
import com.ulopms.model.PmsProperty;
import com.ulopms.model.PmsStatus;
import com.ulopms.model.PropertyAccommodation;
import com.ulopms.model.PropertyCommissionInvoice;
import com.ulopms.model.PropertyDiscount;
import com.ulopms.model.User;
import com.ulopms.util.Email;

import freemarker.template.Configuration;
import freemarker.template.Template;

public class PropertyVoucherAction extends ActionSupport implements
ServletRequestAware, SessionAware, UserAware{

	private static final long serialVersionUID = 914982665758390091L;

	
	private Date fromDate;
	private Date toDate;
	private Integer accommodationId;
	
	private String strFromDate;

	private String strToDate;
	
	private Integer propertyId;
	
	private Integer bookingId;
	
	List<BookingDetailReport> listRevenueDetails=new ArrayList<BookingDetailReport>();
	
	private String timeHours; 

	public Integer statusId;

	private boolean mailFlag;

	private String filterTaxName;
	
	private boolean invoiceDateFlag;

	private String invoiceDate; 
	

	private HttpSession session;


	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}

	private static final Logger logger = Logger.getLogger(ReportAction.class);

	
	private User user;

	@Override
	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}
	private SessionMap<String,Object> sessionMap;  
	  
	//getters and setters  
	  
	@SuppressWarnings("unchecked")
	@Override
	public void setSession(Map<String, Object> map) {
		// TODO Auto-generated method stub
		 sessionMap=(SessionMap)map;  
	}
	

	
	
	private HttpServletRequest request;

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	public PropertyVoucherAction() {

	}

	public String execute() {
		
		return SUCCESS;
	}

	private String address1;
	
	private String address2;
	
	private String propertyName;
	
	private String propertyGstNo;
	
	private String propertyFinanceEmail;
	
	private String durationPeriod=null;
	
	private double revenueAmount=0;
	
	private double totalTaxAmount=0;
	
	private double revenueTaxAmount=0;
	
	private double uloCommission=0;
	
	private double uloTaxAmount=0;
	
	private double uloRevenue=0;
	
	private double hotelDueRevenue=0;
	
	private double netHotelPayable=0;
	
	private double centralGstTax=0;
	
	private double stateGstTax=0;
	
	private double totalUloCommission=0;
	
	private String taxStatus;
	
	private String filterInvoiceDate;
	
	public Integer getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(Integer propertyId) {
		this.propertyId = propertyId;
	}
	
	public String getStrFromDate() {
		return strFromDate;
	}

	public void setStrFromDate(String strFromDate) {
		this.strFromDate = strFromDate;
	}

	public String getStrToDate() {
		return strToDate;
	}

	public void setStrToDate(String strToDate) {
		this.strToDate = strToDate;
	}
	
	public String getPropertyRevenueDetails(){
		try{
			this.propertyId=(Integer) sessionMap.get("propertyId");
			DateFormat format = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
			java.util.Date dateStart = format.parse(getStrFromDate());
		    java.util.Date dateEnd = format.parse(getStrToDate());
		    String taxEnable="";
		    taxEnable=getFilterTaxName();
		    
		    String strInvoiceDate="";
		    strInvoiceDate=getFilterInvoiceDate();
		    
		    PropertyVoucherManager voucherController=new PropertyVoucherManager();
		    PmsPropertyManager pmsPropertyController=new PmsPropertyManager();
		    Calendar calCheckStart=Calendar.getInstance();
		    calCheckStart.setTime(dateStart);
		    java.util.Date checkInDate = calCheckStart.getTime();
		    this.fromDate=new Date(checkInDate.getTime());
		    sessionMap.put("fromDate",fromDate); 
		    SimpleDateFormat format1 = new SimpleDateFormat("dd-MMM-yyyy");
		    String strFromDate=format1.format(this.fromDate);
		    Calendar calCheckEnd=Calendar.getInstance();
		    calCheckEnd.setTime(dateEnd);
		    java.util.Date checkOutDate = calCheckEnd.getTime();
		    this.toDate=new Date(checkOutDate.getTime());
		    sessionMap.put("toDate",toDate); 
		    String strToDate=format1.format(this.toDate);
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			DecimalFormat df = new DecimalFormat("###.##");
			
			response.setContentType("application/json");
			
			
			int sourceId=0;
			boolean blnJsonData=false;
			Double dblTotalUloTaxCommission=0.0,dblTotalUloCommission=0.0
					,dblTotalRevenueAmount=0.0,dblTotalRevenueTaxAmount=0.0,dblTotalNetHotelPayable=0.0
					,dblTotalHotelTaxPayable=0.0,dblTotalTaxAmount=0.0,dblTotalDueAmount=0.0,dblTotalUloTax=0.0
					,dblTotalUloCentralTaxAmount=0.0,dblTotalUloStateTaxAmount=0.0,totalUloCommissionAmount=0.0;
			
			
			List<BookingDetailReport> listPropertyRevenue=voucherController.listBookingDetail(this.propertyId, this.fromDate, this.toDate);
			if(listPropertyRevenue.size()>0 && !listPropertyRevenue.isEmpty()){
				for(BookingDetailReport bookingDetailReport:listPropertyRevenue){
					
					Double dblBaseAmount=0.0,dblTaxAmount=0.0,dblTotalAmount=0.0,dblCommission=0.0,dblDueAmount=0.0,dblAdvanceAmount=0.0,
							dblHotelPayable=0.0, dblOtaCommission=0.0,dblOtaTax=0.0,dblRevenueAmount=0.0,dblRevenueTaxAmount=0.0,dblTotalOtaAmout=0.0
									,dblUloTaxAmount=0.0,dblUloCommission=0.0,dblUloTaxCommission=0.0,dblHotelTaxPayable=0.0,dblNetHotelPayable=0.0;
					
					sourceId=bookingDetailReport.getSourceId();
					
					dblBaseAmount=bookingDetailReport.getAmount();
					dblTaxAmount=bookingDetailReport.getTaxAmount();
					
					dblTotalAmount=dblBaseAmount+dblTaxAmount;
					
					dblTotalTaxAmount+=dblTaxAmount;
					dblAdvanceAmount=bookingDetailReport.getAdvanceAmount();
					
					if(dblAdvanceAmount!=null  && dblAdvanceAmount>=0.0){
						dblAdvanceAmount=bookingDetailReport.getAdvanceAmount();
					}else{
						if(sourceId==1){
							dblAdvanceAmount=dblTotalAmount;
						}else{
							dblAdvanceAmount=0.0;
						}
					}
					
				
					if(dblTotalAmount>0){
						String strTotal=df.format(dblTotalAmount);
						dblTotalAmount=Double.valueOf(strTotal);
					}
					if(dblAdvanceAmount>0){
						String strAdvance=df.format(dblAdvanceAmount);
						dblAdvanceAmount=Double.valueOf(strAdvance);
					}
					dblDueAmount=dblTotalAmount-dblAdvanceAmount;
					
					dblTotalDueAmount+=dblDueAmount;
					
					dblOtaCommission=bookingDetailReport.getOtaCommission();
					dblOtaTax=bookingDetailReport.getOtaTax();
					
					if(dblOtaCommission!=null && dblOtaCommission>=0.0){
						dblOtaCommission=bookingDetailReport.getOtaCommission();
					}else{
						dblOtaCommission=0.0;
					}
					if(dblOtaTax!=null && dblOtaTax>=0.0){
						dblOtaTax=bookingDetailReport.getOtaTax();
					}else{
						dblOtaTax=0.0;
					}
					
					dblTotalOtaAmout=dblOtaCommission+dblOtaTax;
					
					dblRevenueAmount=dblBaseAmount-dblTotalOtaAmout;
					
					dblTotalRevenueAmount+=dblRevenueAmount;
					dblRevenueTaxAmount=dblRevenueAmount+dblTaxAmount;
					
					dblTotalRevenueTaxAmount+=dblRevenueTaxAmount;
					PmsProperty pmsPropertyDetails=pmsPropertyController.find((Integer) sessionMap.get("propertyId"));
					dblCommission=pmsPropertyDetails.getPropertyCommission();
					if(dblCommission!=null){
						if(taxEnable.equalsIgnoreCase("Taxable")){
							dblUloCommission=dblRevenueTaxAmount*dblCommission/100;
						}else if(taxEnable.equalsIgnoreCase("NonTaxable")){
							dblUloCommission=dblRevenueAmount*dblCommission/100;
						}
//						dblUloCommission=dblRevenueAmount*dblCommission/100;
					}
					dblTotalUloCommission+=dblUloCommission;
					
					dblUloTaxAmount=dblUloCommission*18/100;
					
					dblTotalUloTax+=dblUloTaxAmount;
					
					dblUloTaxCommission=dblUloCommission+dblUloTaxAmount;
					
					dblTotalUloTaxCommission+=dblUloTaxCommission;
					
					dblHotelPayable=dblRevenueAmount-dblUloTaxCommission;
					dblHotelTaxPayable=dblHotelPayable+dblTaxAmount;
					
					dblTotalHotelTaxPayable+=dblHotelTaxPayable;
					dblNetHotelPayable=dblHotelTaxPayable-dblDueAmount;
					
					dblTotalNetHotelPayable+=dblNetHotelPayable;
					
					blnJsonData=true;
					
					
				}
			}
			if(blnJsonData)
			{
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				String strDate=strFromDate+" - "+strToDate;
				jsonOutput += "\"durationPeriod\":\" "+strDate  +"\"";
				jsonOutput += ",\"revenueAmount\":\" "+df.format(dblTotalRevenueAmount)+"\"";
				jsonOutput += ",\"totalTaxAmount\":\"" + df.format(dblTotalTaxAmount) + "\"";
				jsonOutput += ",\"revenueTaxAmount\":\" "+df.format(dblTotalRevenueTaxAmount)+"\"";
				jsonOutput += ",\"uloCommission\":\"" + df.format(dblTotalUloCommission)+ "\"";
				jsonOutput += ",\"uloTaxAmount\":\"" + df.format(dblTotalUloTax)+ "\"";
				jsonOutput += ",\"uloRevenue\":\"" + df.format(dblTotalUloTaxCommission)+ "\"";
				jsonOutput += ",\"hotelDueRevenue\":\"" + df.format(dblTotalDueAmount)+ "\"";
				jsonOutput += ",\"netHotelPayable\":\"" +df.format(dblTotalNetHotelPayable)+ "\"";
				jsonOutput += ",\"hotelPayable\":\"" + df.format(dblTotalHotelTaxPayable)+ "\"";
				dblTotalUloCentralTaxAmount=dblTotalUloCommission*9/100;
				dblTotalUloStateTaxAmount=dblTotalUloCommission*9/100;
				jsonOutput += ",\"centralGstTax\":\"" + df.format(dblTotalUloCentralTaxAmount)+ "\"";
				jsonOutput += ",\"stateGstTax\":\"" + df.format(dblTotalUloStateTaxAmount)+ "\"";
				jsonOutput += ",\"uloTotalCommission\":\"" +  df.format(dblTotalUloCommission)+ "\"";
				totalUloCommissionAmount=dblTotalUloCommission+(dblTotalUloCentralTaxAmount+dblTotalUloStateTaxAmount);
				jsonOutput += ",\"totalUloCommission\":\"" + df.format(totalUloCommissionAmount)+ "\"";
				jsonOutput += ",\"taxStatus\":\"" +taxEnable+ "\"";
				jsonOutput += ",\"filterInvoiceDate\":\"" +strInvoiceDate+ "\"";
				jsonOutput += "}";
				response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
				
			}
			
			
		}catch(Exception e){
			logger.error(e);
			e.printStackTrace();
		}finally{
			
		}
		return null;
	}
	
	public String getRevenueVoucherDetails(){
		try{
			
			java.util.Date date=new java.util.Date();
        	Calendar calDate=Calendar.getInstance();
        	calDate.setTime(date);
        	calDate.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
        	int bookedHours=calDate.get(Calendar.HOUR);
        	int bookedMinute=calDate.get(Calendar.MINUTE);
        	int AMPM=calDate.get(Calendar.AM_PM);
        	
        	
        	String strBookingDate=new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH).format(date);
        	String hours="",minutes="";
        	if(bookedHours<10){
        		hours=String.valueOf(bookedHours);
        		hours="0"+hours;
        	}else{
        		hours=String.valueOf(bookedHours);
        	}
        	if(bookedMinute<10){
        		minutes=String.valueOf(bookedMinute);
        		minutes="0"+minutes;
        	}else{
        		minutes=String.valueOf(bookedMinute);
        	}
        	if(AMPM==0){//If the current time is AM
        		this.timeHours=strBookingDate+" "+hours+" : "+minutes+" AM ";
        	}else if(AMPM==1){//If the current time is PM
        		this.timeHours=strBookingDate+" "+hours+" : "+minutes+" PM ";
        	}
        	
			this.propertyId=(Integer) sessionMap.get("propertyId");
			PmsPropertyManager pmsPropertyController=new PmsPropertyManager();
			for (int i = 0; i < listRevenueDetails.size(); i++) 
		    {
				durationPeriod=listRevenueDetails.get(i).getDurationPeriod();
				revenueAmount=listRevenueDetails.get(i).getRevenueAmount();
				totalTaxAmount=listRevenueDetails.get(i).getTotalTaxAmount();
				revenueTaxAmount=listRevenueDetails.get(i).getRevenueTaxAmount();
				uloCommission=listRevenueDetails.get(i).getUloCommission();
				uloTaxAmount=listRevenueDetails.get(i).getUloTaxAmount();
				uloRevenue=listRevenueDetails.get(i).getUloRevenue();
				hotelDueRevenue=listRevenueDetails.get(i).getHotelDueRevenue();
				netHotelPayable=listRevenueDetails.get(i).getNetHotelPayable();
				taxStatus=listRevenueDetails.get(i).getTaxStatus();
				
		    }
			
			
			
			
			if(this.mailFlag == true)
            {
            	try
 				{
 				//email template
 				Configuration cfg = new Configuration();
 				cfg.setClassForTemplateLoading(BookingDetailAction.class, "../../../");
// 				Template template = cfg.getTemplate(getText("dsfr.voucher.template"));
 				Template template =null;
 				if(taxStatus.equalsIgnoreCase("Taxable")){
 					template = cfg.getTemplate(getText("dsfr.revenue.tax.voucher.template"));
				}else if(taxStatus.equalsIgnoreCase("NonTaxable")){
					template = cfg.getTemplate(getText("dsfr.voucher.template"));
				}
 				Map<String, Object> rootMap = new HashMap<String, Object>();
 				rootMap.put("voucherDate",this.timeHours);
 				rootMap.put("durationPeriod",this.durationPeriod.toUpperCase());
 				rootMap.put("revenueAmount",this.revenueAmount);
 				rootMap.put("totalTaxAmount",this.totalTaxAmount);
 				rootMap.put("revenueTaxAmount",this.revenueTaxAmount);
                rootMap.put("uloCommission",this.uloCommission);
 				rootMap.put("uloTaxAmount",this.uloTaxAmount);
 				rootMap.put("uloRevenue",this.uloRevenue);
 				rootMap.put("hotelDueRevenue",this.hotelDueRevenue);
 				rootMap.put("netHotelPayable",this.netHotelPayable);
 				PmsProperty pmsPropertyDetails=pmsPropertyController.find((Integer) sessionMap.get("propertyId"));
 				this.address1=pmsPropertyDetails.getAddress1();
 				this.address2=pmsPropertyDetails.getAddress2();
 				this.propertyName=pmsPropertyDetails.getPropertyName();
 				this.propertyGstNo=pmsPropertyDetails.getPropertyGstNo();
 				this.propertyFinanceEmail=pmsPropertyDetails.getPropertyFinanceEmail();
 				
 				String propertyAddress=this.address1+","+this.address2;
 				rootMap.put("propertyAddress",propertyAddress);
 				rootMap.put("propertyName",this.propertyName);
 				rootMap.put("propertyGstNo",this.propertyGstNo);
 				
 				rootMap.put("from", getText("notification.from"));
 				Writer out = new StringWriter();
 				template.process(rootMap, out);
 				String[] arrString=null;
 				ArrayList<String> arlToken=new ArrayList<String>();
 				StringTokenizer stoken=new StringTokenizer(this.propertyFinanceEmail,",");
 				String strPropertyMailId1=null,strPropertyMailId2=null,strPropertyMailId3=null,strPropertyMailId4=null,strPropertyMailId5=null;
 				while(stoken.hasMoreElements()){
 					arlToken.add(stoken.nextToken());
 				}
 				int intCount=0, arlSize=0;
 				arrString=new String[arlToken.size()];
 				Iterator<String> iterListValues=arlToken.iterator();
 			    while(iterListValues.hasNext()){
 		    		String strTemp=iterListValues.next();
 		    		arrString[intCount]=strTemp.trim();
 		    		intCount++;		
 			    }
 			    
 				int i=0;
 				arlSize=arlToken.size();
 				for(String s : arrString) {
 			          String[] s2 = s.split(" ");
 			          for(String results : s2) {
 			        	  if(i==0){
 			        		 strPropertyMailId1=results;
 			        	  }else if(i==1){
 			        		 strPropertyMailId2=results;
 			        	  }else if(i==2){
 			        		 strPropertyMailId3=results;
 			        	  }else if(i==3){
 			        		 strPropertyMailId4=results;
 			        	  }else if(i==4){
 			        		 strPropertyMailId5=results;
 			        	  }else if(i==5){
 			        		  break;
 			        	  }
 			        	
 			        	 i++;
 			        	 
 			        	
 			          }
 			      }
 				
 				if(strPropertyMailId2==null){
 					strPropertyMailId2=strPropertyMailId1;
 				}
 				if(strPropertyMailId3==null){
 					strPropertyMailId3=strPropertyMailId1;
 				}
 				if(strPropertyMailId4==null){
 					strPropertyMailId4=strPropertyMailId1;
 				}
 				if(strPropertyMailId5==null){
 					strPropertyMailId5=strPropertyMailId1;
 				}
 				
 				//send email
 				Email em = new Email();
 				em.set_to(strPropertyMailId1);
 				em.set_cc(getText("superadmin.notification.email"));
 				em.set_cc2(getText("operations.notification.email"));
 				em.set_cc3(strPropertyMailId2);
 				em.set_cc4(strPropertyMailId3);
 				em.set_cc5(strPropertyMailId4);
 				em.set_cc6(strPropertyMailId5);
 				
 				
 				em.set_from(getText("email.from"));
 				em.set_username(getText("email.username"));
 				em.set_subject(getText("dsfr.subject"));
 				em.set_bodyContent(out);
 				em.sendVoucher();		
 				}
 				catch(Exception e1)
 				{
 					e1.printStackTrace();
 				}
            }
			
		}catch(Exception e){
			logger.error(e);
			e.printStackTrace();
		}
		return null;
	}
	
	public String getCommissionVoucherDetails(){

		try{
			this.fromDate=(Date)sessionMap.get("fromDate");
			this.toDate=(Date)sessionMap.get("toDate");
			
			Timestamp tsFromDate=new Timestamp(this.fromDate.getTime());
			Timestamp tsToDate=new Timestamp(this.toDate.getTime());
			PropertyCommissionInvoice propertyCommissionInvoice=new PropertyCommissionInvoice();
			PropertyCommissionInvoiceManager commissionInvoiceController=new PropertyCommissionInvoiceManager();
			propertyCommissionInvoice.setFromDate(tsFromDate);
			propertyCommissionInvoice.setToDate(tsToDate);
			
			commissionInvoiceController.add(propertyCommissionInvoice);
			
			
			java.util.Date date=new java.util.Date();
        	Calendar calDate=Calendar.getInstance();
        	calDate.setTime(date);
        	calDate.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
        	int bookedHours=calDate.get(Calendar.HOUR);
        	int bookedMinute=calDate.get(Calendar.MINUTE);
        	int AMPM=calDate.get(Calendar.AM_PM);
        	String strBookingDate=new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH).format(date);
        	String hours="",minutes="";
        	if(bookedHours<10){
        		hours=String.valueOf(bookedHours);
        		hours="0"+hours;
        	}else{
        		hours=String.valueOf(bookedHours);
        	}
        	if(bookedMinute<10){
        		minutes=String.valueOf(bookedMinute);
        		minutes="0"+minutes;
        	}else{
        		minutes=String.valueOf(bookedMinute);
        	}
        	/*if(AMPM==0){//If the current time is AM
        		this.timeHours=strBookingDate+" "+hours+" : "+minutes+" AM ";
        	}else if(AMPM==1){//If the current time is PM
        		this.timeHours=strBookingDate+" "+hours+" : "+minutes+" PM ";
        	}*/
        	
			this.propertyId=(Integer) sessionMap.get("propertyId");
			PmsPropertyManager pmsPropertyController=new PmsPropertyManager();
			for (int i = 0; i < listRevenueDetails.size(); i++) 
		    {
				durationPeriod=listRevenueDetails.get(i).getDurationPeriod();
				uloCommission=listRevenueDetails.get(i).getUloTotalCommission();
				centralGstTax=listRevenueDetails.get(i).getCentralGstTax();
				stateGstTax=listRevenueDetails.get(i).getStateGstTax();
				totalUloCommission=listRevenueDetails.get(i).getTotalUloCommission();
				invoiceDate=listRevenueDetails.get(i).getFilterInvoiceDate();
		    }
			
			if(!isInvoiceDateFlag()){
				if(AMPM==0){//If the current time is AM
	        		this.timeHours=strBookingDate+" "+hours+" : "+minutes+" AM ";
	        	}else if(AMPM==1){//If the current time is PM
	        		this.timeHours=strBookingDate+" "+hours+" : "+minutes+" PM ";
	        	}
        	}
			if(isInvoiceDateFlag()){
				if(AMPM==0){//If the current time is AM
	        		this.timeHours=invoiceDate+" "+hours+" : "+minutes+" AM ";
	        	}else if(AMPM==1){//If the current time is PM
	        		this.timeHours=invoiceDate+" "+hours+" : "+minutes+" PM ";
	        	}
			}
			
			if(this.mailFlag == true)
            {
            	try
 				{
 				//email template
 				Configuration cfg = new Configuration();
 				cfg.setClassForTemplateLoading(BookingDetailAction.class, "../../../");
 				Template template = cfg.getTemplate(getText("commission.voucher.template"));
 				Map<String, Object> rootMap = new HashMap<String, Object>();
 				rootMap.put("durationPeriod",this.durationPeriod.toUpperCase());
                rootMap.put("uloCommission",this.uloCommission);
 				rootMap.put("centralGstTax",this.centralGstTax);
 				rootMap.put("stateGstTax",this.stateGstTax);
 				rootMap.put("totalUloCommission",this.totalUloCommission);
 				
 				PmsProperty pmsPropertyDetails=pmsPropertyController.find((Integer) sessionMap.get("propertyId"));
 				this.address1=pmsPropertyDetails.getAddress1();
 				this.address2=pmsPropertyDetails.getAddress2();
 				this.propertyName=pmsPropertyDetails.getPropertyName();
 				this.propertyGstNo=pmsPropertyDetails.getPropertyGstNo();
 				this.propertyFinanceEmail=pmsPropertyDetails.getPropertyFinanceEmail();
 				
 				Integer invoiceNumber=0;
 				List<BookingDetailReport> listInvoiceDetails=commissionInvoiceController.list();
 				if(listInvoiceDetails.size()>0 && !listInvoiceDetails.isEmpty()){
 					for(BookingDetailReport invoiceDetails:listInvoiceDetails){
 						invoiceNumber=invoiceDetails.getInvoiceNumber();
 					}
 				}
 				
 				String propertyAddress=this.address1+","+this.address2;
 				rootMap.put("voucherDate",this.timeHours);
 				rootMap.put("propertyAddress",propertyAddress);
 				rootMap.put("propertyName",this.propertyName);
 				rootMap.put("propertyGstNo",this.propertyGstNo);
 				rootMap.put("invoiceNumber",invoiceNumber);
 				rootMap.put("from", getText("notification.from"));
 				Writer out = new StringWriter();
 				template.process(rootMap, out);

 				String[] arrString=null;
 				ArrayList<String> arlToken=new ArrayList<String>();
 				StringTokenizer stoken=new StringTokenizer(this.propertyFinanceEmail,",");
 				String strPropertyMailId1=null,strPropertyMailId2=null,strPropertyMailId3=null,strPropertyMailId4=null,strPropertyMailId5=null;
 				while(stoken.hasMoreElements()){
 					arlToken.add(stoken.nextToken());
 				}
 				
 				int intCount=0, arlSize=0;
 				arrString=new String[arlToken.size()];
 				Iterator<String> iterListValues=arlToken.iterator();
 			    while(iterListValues.hasNext()){
 		    		String strTemp=iterListValues.next();
 		    		arrString[intCount]=strTemp.trim();
 		    		intCount++;		
 			    }
 			    
 				int i=0;
 				arlSize=arlToken.size();
 				for(String s : arrString) {
 			          String[] s2 = s.split(" ");
 			          for(String results : s2) {
 			        	  if(i==0){
 			        		 strPropertyMailId1=results;
 			        	  }else if(i==1){
 			        		 strPropertyMailId2=results;
 			        	  }else if(i==2){
 			        		 strPropertyMailId3=results;
 			        	  }else if(i==3){
 			        		 strPropertyMailId4=results;
 			        	  }else if(i==4){
 			        		 strPropertyMailId5=results;
 			        	  }else if(i==5){
 			        		  break;
 			        	  }
 			        	
 			        	 i++;
 			        	 
 			        	
 			          }
 			      }
 				
 				if(strPropertyMailId2==null){
 					strPropertyMailId2=strPropertyMailId1;
 				}
 				if(strPropertyMailId3==null){
 					strPropertyMailId3=strPropertyMailId1;
 				}
 				if(strPropertyMailId4==null){
 					strPropertyMailId4=strPropertyMailId1;
 				}
 				if(strPropertyMailId5==null){
 					strPropertyMailId5=strPropertyMailId1;
 				}
 				
 				//send email
 				Email em = new Email();
 				em.set_to(strPropertyMailId1);
 				em.set_cc(getText("superadmin.notification.email"));
 				em.set_cc2(getText("operations.notification.email"));
 				em.set_cc3(strPropertyMailId2);
 				em.set_cc4(strPropertyMailId3);
 				em.set_cc5(strPropertyMailId4);
 				em.set_cc6(strPropertyMailId5);
 				
 				
 				
 				em.set_from(getText("email.from"));
 				em.set_username(getText("email.username"));
 				em.set_subject(getText("commission.voucher.subject"));
 				em.set_bodyContent(out);
 				em.sendVoucher();		
 				}
 				catch(Exception e1)
 				{
 					e1.printStackTrace();
 				}
            }
			
		}catch(Exception e){
			logger.error(e);
			e.printStackTrace();
		}
		return null;
	
	}
	
	public String getPropertyRevenueTaxDetails(){
		try{
			this.propertyId=(Integer) sessionMap.get("propertyId");
			DateFormat format = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
			java.util.Date dateStart = format.parse(getStrFromDate());
		    java.util.Date dateEnd = format.parse(getStrToDate());
		    
		    PropertyVoucherManager voucherController=new PropertyVoucherManager();
		    PmsPropertyManager pmsPropertyController=new PmsPropertyManager();
		    Calendar calCheckStart=Calendar.getInstance();
		    calCheckStart.setTime(dateStart);
		    java.util.Date checkInDate = calCheckStart.getTime();
		    this.fromDate=new Date(checkInDate.getTime());
		    sessionMap.put("fromDate",fromDate); 
		    SimpleDateFormat format1 = new SimpleDateFormat("dd-MMM-yyyy");
		    String strFromDate=format1.format(this.fromDate);
		    Calendar calCheckEnd=Calendar.getInstance();
		    calCheckEnd.setTime(dateEnd);
		    java.util.Date checkOutDate = calCheckEnd.getTime();
		    this.toDate=new Date(checkOutDate.getTime());
		    sessionMap.put("toDate",toDate); 
		    String strToDate=format1.format(this.toDate);
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			DecimalFormat df = new DecimalFormat("###.##");
			
			response.setContentType("application/json");
			
			
			int sourceId=0;
			boolean blnJsonData=false;
			Double dblTotalUloTaxCommission=0.0,dblTotalUloCommission=0.0
					,dblTotalRevenueAmount=0.0,dblTotalRevenueTaxAmount=0.0,dblTotalNetHotelPayable=0.0
					,dblTotalHotelTaxPayable=0.0,dblTotalTaxAmount=0.0,dblTotalDueAmount=0.0,dblTotalUloTax=0.0
					,dblTotalUloCentralTaxAmount=0.0,dblTotalUloStateTaxAmount=0.0,totalUloCommissionAmount=0.0;
			
			
			List<BookingDetailReport> listPropertyRevenue=voucherController.listBookingDetail(this.propertyId, this.fromDate, this.toDate);
			if(listPropertyRevenue.size()>0 && !listPropertyRevenue.isEmpty()){
				for(BookingDetailReport bookingDetailReport:listPropertyRevenue){
					
					Double dblBaseAmount=0.0,dblTaxAmount=0.0,dblTotalAmount=0.0,dblCommission=0.0,dblDueAmount=0.0,dblAdvanceAmount=0.0,
							dblHotelPayable=0.0, dblOtaCommission=0.0,dblOtaTax=0.0,dblRevenueAmount=0.0,dblRevenueTaxAmount=0.0,dblTotalOtaAmout=0.0
									,dblUloTaxAmount=0.0,dblUloCommission=0.0,dblUloTaxCommission=0.0,dblHotelTaxPayable=0.0,dblNetHotelPayable=0.0;
					
					sourceId=bookingDetailReport.getSourceId();
					
					dblBaseAmount=bookingDetailReport.getAmount();
					dblTaxAmount=bookingDetailReport.getTaxAmount();
					
					dblTotalAmount=dblBaseAmount+dblTaxAmount;
					
					dblTotalTaxAmount+=dblTaxAmount;
					dblAdvanceAmount=bookingDetailReport.getAdvanceAmount();
					if(dblAdvanceAmount!=null  && dblAdvanceAmount>=0.0 ){
						dblAdvanceAmount=bookingDetailReport.getAdvanceAmount();
					}else{
						if(sourceId==1){
							dblAdvanceAmount=dblTotalAmount;
						}else{
							dblAdvanceAmount=0.0;
						}
					}
					
				
					
					dblDueAmount=dblTotalAmount-dblAdvanceAmount;
					
					dblTotalDueAmount+=dblDueAmount;
					
					dblOtaCommission=bookingDetailReport.getOtaCommission();
					dblOtaTax=bookingDetailReport.getOtaTax();
					
					if(dblOtaCommission!=null && dblOtaCommission>=0.0){
						dblOtaCommission=bookingDetailReport.getOtaCommission();
					}else{
						dblOtaCommission=0.0;
					}
					if(dblOtaTax!=null && dblOtaTax>=0.0){
						dblOtaTax=bookingDetailReport.getOtaTax();
					}else{
						dblOtaTax=0.0;
					}
					
					dblTotalOtaAmout=dblOtaCommission+dblOtaTax;
					
					dblRevenueAmount=dblBaseAmount-dblTotalOtaAmout;
					
					dblTotalRevenueAmount+=dblRevenueAmount;
					dblRevenueTaxAmount=dblRevenueAmount+dblTaxAmount;
					
					dblTotalRevenueTaxAmount+=dblRevenueTaxAmount;
					PmsProperty pmsPropertyDetails=pmsPropertyController.find((Integer) sessionMap.get("propertyId"));
					dblCommission=pmsPropertyDetails.getPropertyCommission();
					if(dblCommission!=null){
						dblUloCommission=dblRevenueTaxAmount*dblCommission/100;
					}
					dblTotalUloCommission+=dblUloCommission;
					
					dblUloTaxAmount=dblUloCommission*18/100;
					
					dblTotalUloTax+=dblUloTaxAmount;
					
					dblUloTaxCommission=dblUloCommission+dblUloTaxAmount;
					
					dblTotalUloTaxCommission+=dblUloTaxCommission;
					
					dblHotelPayable=dblRevenueTaxAmount-dblUloTaxCommission;
//					dblHotelTaxPayable=dblHotelPayable+dblTaxAmount;
					
					dblTotalHotelTaxPayable+=dblHotelPayable;
					dblNetHotelPayable=dblHotelPayable-dblDueAmount;
					
					dblTotalNetHotelPayable+=dblNetHotelPayable;
					
					blnJsonData=true;
					
					
				}
			}
			if(blnJsonData)
			{
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				String strDate=strFromDate+" - "+strToDate;
				jsonOutput += "\"durationPeriod\":\" "+strDate  +"\"";
				jsonOutput += ",\"revenueAmount\":\" "+df.format(dblTotalRevenueAmount)+"\"";
				jsonOutput += ",\"totalTaxAmount\":\"" + df.format(dblTotalTaxAmount) + "\"";
				jsonOutput += ",\"revenueTaxAmount\":\" "+df.format(dblTotalRevenueTaxAmount)+"\"";
				jsonOutput += ",\"uloCommission\":\"" + df.format(dblTotalUloCommission)+ "\"";
				jsonOutput += ",\"uloTaxAmount\":\"" + df.format(dblTotalUloTax)+ "\"";
				jsonOutput += ",\"uloRevenue\":\"" + df.format(dblTotalUloTaxCommission)+ "\"";
				jsonOutput += ",\"hotelDueRevenue\":\"" + df.format(dblTotalDueAmount)+ "\"";
				jsonOutput += ",\"netHotelPayable\":\"" +df.format(dblTotalNetHotelPayable)+ "\"";
				jsonOutput += ",\"hotelPayable\":\"" + df.format(dblTotalHotelTaxPayable)+ "\"";
				dblTotalUloCentralTaxAmount=dblTotalUloCommission*9/100;
				dblTotalUloStateTaxAmount=dblTotalUloCommission*9/100;
				jsonOutput += ",\"centralGstTax\":\"" + df.format(dblTotalUloCentralTaxAmount)+ "\"";
				jsonOutput += ",\"stateGstTax\":\"" + df.format(dblTotalUloStateTaxAmount)+ "\"";
				jsonOutput += ",\"uloTotalCommission\":\"" +  df.format(dblTotalUloCommission)+ "\"";
				totalUloCommissionAmount=dblTotalUloCommission+(dblTotalUloCentralTaxAmount+dblTotalUloStateTaxAmount);
				jsonOutput += ",\"totalUloCommission\":\"" + df.format(totalUloCommissionAmount)+ "\"";
				jsonOutput += "}";
				response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
				
			}
			
			
		}catch(Exception e){
			logger.error(e);
			e.printStackTrace();
		}finally{
			
		}
		return null;
	}
	
	public String getRevenueVoucherTaxDetails(){
		try{
			
			java.util.Date date=new java.util.Date();
        	Calendar calDate=Calendar.getInstance();
        	calDate.setTime(date);
        	calDate.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
        	int bookedHours=calDate.get(Calendar.HOUR);
        	int bookedMinute=calDate.get(Calendar.MINUTE);
        	int AMPM=calDate.get(Calendar.AM_PM);
        	String strBookingDate=new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH).format(date);
        	String hours="",minutes="";
        	if(bookedHours<10){
        		hours=String.valueOf(bookedHours);
        		hours="0"+hours;
        	}else{
        		hours=String.valueOf(bookedHours);
        	}
        	if(bookedMinute<10){
        		minutes=String.valueOf(bookedMinute);
        		minutes="0"+minutes;
        	}else{
        		minutes=String.valueOf(bookedMinute);
        	}
        	if(AMPM==0){//If the current time is AM
        		this.timeHours=strBookingDate+" "+hours+" : "+minutes+" AM ";
        	}else if(AMPM==1){//If the current time is PM
        		this.timeHours=strBookingDate+" "+hours+" : "+minutes+" PM ";
        	}
        	
			this.propertyId=(Integer) sessionMap.get("propertyId");
			PmsPropertyManager pmsPropertyController=new PmsPropertyManager();
			for (int i = 0; i < listRevenueDetails.size(); i++) 
		    {
				durationPeriod=listRevenueDetails.get(i).getDurationPeriod();
				revenueAmount=listRevenueDetails.get(i).getRevenueAmount();
				totalTaxAmount=listRevenueDetails.get(i).getTotalTaxAmount();
				revenueTaxAmount=listRevenueDetails.get(i).getRevenueTaxAmount();
				uloCommission=listRevenueDetails.get(i).getUloCommission();
				uloTaxAmount=listRevenueDetails.get(i).getUloTaxAmount();
				uloRevenue=listRevenueDetails.get(i).getUloRevenue();
				hotelDueRevenue=listRevenueDetails.get(i).getHotelDueRevenue();
				netHotelPayable=listRevenueDetails.get(i).getNetHotelPayable();
		    }
			
			
			if(this.mailFlag == true)
            {
            	try
 				{
 				//email template
 				Configuration cfg = new Configuration();
 				cfg.setClassForTemplateLoading(BookingDetailAction.class, "../../../");
 				Template template = cfg.getTemplate(getText("dsfr.revenue.tax.voucher.template"));
 				Map<String, Object> rootMap = new HashMap<String, Object>();
 				rootMap.put("voucherDate",this.timeHours);
 				rootMap.put("durationPeriod",this.durationPeriod.toUpperCase());
 				rootMap.put("revenueAmount",this.revenueAmount);
 				rootMap.put("totalTaxAmount",this.totalTaxAmount);
 				rootMap.put("revenueTaxAmount",this.revenueTaxAmount);
                rootMap.put("uloCommission",this.uloCommission);
 				rootMap.put("uloTaxAmount",this.uloTaxAmount);
 				rootMap.put("uloRevenue",this.uloRevenue);
 				rootMap.put("hotelDueRevenue",this.hotelDueRevenue);
 				rootMap.put("netHotelPayable",this.netHotelPayable);
 				PmsProperty pmsPropertyDetails=pmsPropertyController.find((Integer) sessionMap.get("propertyId"));
 				this.address1=pmsPropertyDetails.getAddress1();
 				this.address2=pmsPropertyDetails.getAddress2();
 				this.propertyName=pmsPropertyDetails.getPropertyName();
 				this.propertyGstNo=pmsPropertyDetails.getPropertyGstNo();
 				this.propertyFinanceEmail=pmsPropertyDetails.getPropertyFinanceEmail();
 				
 				String propertyAddress=this.address1+","+this.address2;
 				rootMap.put("propertyAddress",propertyAddress);
 				rootMap.put("propertyName",this.propertyName);
 				rootMap.put("propertyGstNo",this.propertyGstNo);
 				
 				rootMap.put("from", getText("notification.from"));
 				Writer out = new StringWriter();
 				template.process(rootMap, out);
 				String[] arrString=null;
 				ArrayList<String> arlToken=new ArrayList<String>();
 				StringTokenizer stoken=new StringTokenizer(this.propertyFinanceEmail,",");
 				String strPropertyMailId1=null,strPropertyMailId2=null,strPropertyMailId3=null,strPropertyMailId4=null,strPropertyMailId5=null;
 				while(stoken.hasMoreElements()){
 					arlToken.add(stoken.nextToken());
 				}
 				int intCount=0, arlSize=0;
 				arrString=new String[arlToken.size()];
 				Iterator<String> iterListValues=arlToken.iterator();
 			    while(iterListValues.hasNext()){
 		    		String strTemp=iterListValues.next();
 		    		arrString[intCount]=strTemp.trim();
 		    		intCount++;		
 			    }
 			    
 				int i=0;
 				arlSize=arlToken.size();
 				for(String s : arrString) {
 			          String[] s2 = s.split(" ");
 			          for(String results : s2) {
 			        	  if(i==0){
 			        		 strPropertyMailId1=results;
 			        	  }else if(i==1){
 			        		 strPropertyMailId2=results;
 			        	  }else if(i==2){
 			        		 strPropertyMailId3=results;
 			        	  }else if(i==3){
 			        		 strPropertyMailId4=results;
 			        	  }else if(i==4){
 			        		 strPropertyMailId5=results;
 			        	  }else if(i==5){
 			        		  break;
 			        	  }
 			        	
 			        	 i++;
 			        	 
 			        	
 			          }
 			      }
 				
 				if(strPropertyMailId2==null){
 					strPropertyMailId2=strPropertyMailId1;
 				}
 				if(strPropertyMailId3==null){
 					strPropertyMailId3=strPropertyMailId1;
 				}
 				if(strPropertyMailId4==null){
 					strPropertyMailId4=strPropertyMailId1;
 				}
 				if(strPropertyMailId5==null){
 					strPropertyMailId5=strPropertyMailId1;
 				}
 				
 				//send email
 				Email em = new Email();
 				em.set_to(strPropertyMailId1);
 				em.set_cc(getText("superadmin.notification.email"));
 				em.set_cc2(getText("operations.notification.email"));
 				em.set_cc3(strPropertyMailId2);
 				em.set_cc4(strPropertyMailId3);
 				em.set_cc5(strPropertyMailId4);
 				em.set_cc6(strPropertyMailId5);
 				
 				
 				em.set_from(getText("email.from"));
 				em.set_username(getText("email.username"));
 				em.set_subject(getText("dsfr.subject"));
 				em.set_bodyContent(out);
 				em.sendVoucher();		
 				}
 				catch(Exception e1)
 				{
 					e1.printStackTrace();
 				}
            }
			
		}catch(Exception e){
			logger.error(e);
			e.printStackTrace();
		}
		return null;
	}
	
	
    public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Integer getAccommodationId() {
		return accommodationId;
	}

	public void setAccommodationId(int accommodationId) {
		this.accommodationId = accommodationId;
	}
	
	
	
	public Integer getBookingId() {
		return bookingId;
	}

	public void setBookingId(Integer bookingId) {
		this.bookingId = bookingId;
	}

	public Integer getStatusId() {
		return statusId;
	}

	public void setStatusId(Integer statusId) {
		this.statusId = statusId;
	}
	
	public boolean isMailFlag() {
		return mailFlag;
	}

	public void setMailFlag(boolean mailFlag) {
		this.mailFlag = mailFlag;
	}
	
	public List<BookingDetailReport> getListRevenueDetails() {
		return listRevenueDetails;
	}

	public void setListRevenueDetails(List<BookingDetailReport> listRevenueDetails) {
		this.listRevenueDetails = listRevenueDetails;
	}
	
	public String getFilterTaxName() {
		return filterTaxName;
	}

	public void setFilterTaxName(String filterTaxName) {
		this.filterTaxName = filterTaxName;
	}
	
	public boolean isInvoiceDateFlag() {
		return invoiceDateFlag;
	}

	public void setInvoiceDateFlag(boolean invoiceDateFlag) {
		this.invoiceDateFlag = invoiceDateFlag;
	}
	
	public String getFilterInvoiceDate() {
		return filterInvoiceDate;
	}

	public void setFilterInvoiceDate(String filterInvoiceDate) {
		this.filterInvoiceDate = filterInvoiceDate;
	}
}
