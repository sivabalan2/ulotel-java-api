package com.ulopms.view;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.quartz.CronExpression;
import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;
 
/**
 * This class is used for executing quartz job 
 * using CronTrigger(Quartz 2.1.5).
 * @author javawithease
 */
public class QuartzListener implements ServletContextListener {
 
	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
	}
 
	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		try{
    		//Set job details.
    		JobDetail job = JobBuilder.newJob(HelloJob.class)
    			.withIdentity("helloJob", "group1").build();
 
        	//Set the scheduler timings.
    		Trigger trigger = TriggerBuilder.newTrigger()
    			.withIdentity("cronTrigger", "group1")
    			.withSchedule(CronScheduleBuilder
    			  .cronSchedule("0 30 10 * * ?")).build(); /*0/10 * * * * ?*/ /*0 55 18 * * ?*/
 
        	//Execute the job.
    		Scheduler scheduler = new StdSchedulerFactory().getScheduler();
        	scheduler.start();
        	scheduler.scheduleJob(job, trigger);
        	
        	  JobDetail job2 = JobBuilder.newJob(BookingVoucher.class)
                      .withIdentity("bokingVoucher", "group2").build();
               
              Trigger trigger2 = TriggerBuilder.newTrigger()
                      .withIdentity("cronTrigger2", "group2")
                      .withSchedule(CronScheduleBuilder
                      .cronSchedule("0/10 * * * * ?")).build(); // 0 20 22 * * ?  //  0/10 * * * * ?
               
//              Scheduler scheduler2 = new StdSchedulerFactory().getScheduler();
//              scheduler2.start();
//              scheduler2.scheduleJob(job2, trigger2);
              
    	}catch(Exception e){ 
    		e.printStackTrace();
    	}    		
	}
}