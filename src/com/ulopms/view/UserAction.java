package com.ulopms.view;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Blob;
import java.io.StringWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.NumberUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.hibernate.Hibernate;



//import com.medwecare.controller.AwsObjectManager;
//import com.medwecare.controller.CommentManager;
//import com.medwecare.controller.CommentUserManager;
////import com.medwecare.controller.NotificationManager;
//import com.medwecare.controller.ProjectManager;

//import com.medwecare.model.AwsObject;
//import com.medwecare.model.Notification;
//import com.medwecare.model.ObjectComment;
//import com.medwecare.model.ObjectCommentUser;
//import com.medwecare.model.Project;






import org.joda.time.DateTime;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.SdkClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.ulopms.api.Login;
import com.ulopms.api.Search;
import com.ulopms.api.Users;
import com.ulopms.controller.PmsPropertyManager;
import com.ulopms.controller.UserLoginManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.PmsProperty;
import com.ulopms.model.User;
import com.ulopms.security.passwordDecrypt;
import com.ulopms.util.Image;




import com.opensymphony.xwork2.ActionSupport;

import freemarker.template.Configuration;
import freemarker.template.Template;

import javax.servlet.http.HttpServletRequest;

import com.opensymphony.xwork2.ModelDriven;
import com.ulopms.controller.AccessRightManager;
import com.ulopms.controller.BookingDetailManager;
import com.ulopms.controller.BookingGuestDetailManager;
import com.ulopms.controller.LocationManager;
import com.ulopms.controller.PmsBookingManager;
import com.ulopms.controller.PmsGuestManager;
import com.ulopms.controller.PmsPhotoCategoryManager;
import com.ulopms.controller.PmsPropertyManager;
import com.ulopms.controller.PmsStatusManager;
import com.ulopms.controller.PropertyAccommodationManager;
import com.ulopms.controller.PropertyAccommodationRoomManager;
import com.ulopms.controller.PropertyItemCategoryManager;
import com.ulopms.controller.PropertyPhotoManager;
import com.ulopms.controller.RewardsManager;
import com.ulopms.controller.RoleManager;
import com.ulopms.controller.UserAccessRightManager;
import com.ulopms.controller.UserLoginManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.AccessRight;
import com.ulopms.model.BookingDetailReport;
import com.ulopms.model.BookingGuestDetail;
import com.ulopms.model.CorporateEnquiry;
import com.ulopms.model.Location;
import com.ulopms.model.PmsAvailableRooms;
import com.ulopms.model.PmsBookedDetails;
import com.ulopms.model.PmsGuest;
import com.ulopms.model.PmsPhotoCategory;
import com.ulopms.model.PmsProperty;
import com.ulopms.model.PropertyAccommodation;
import com.ulopms.model.PropertyAccommodationRoom;
import com.ulopms.model.PropertyItemCategory;
import com.ulopms.model.PropertyPhoto;
import com.ulopms.model.RewardDetails;
import com.ulopms.model.RewardUserDetails;
import com.ulopms.model.Role;
import com.ulopms.model.User;
import com.ulopms.model.UserAccessRight;
import com.ulopms.security.passwordDecrypt;
import com.ulopms.util.Email;
import com.ulopms.util.Image;




public class UserAction extends ActionSupport implements
ServletRequestAware,SessionAware,UserAware{

	private static final long serialVersionUID = 914982665758390091L;
	
	private String oper;
	private Integer userId;
	private User user = new User();
	private List<User> userList;
	private Integer roleId;
	private String userName;
	private String address1;
	private String address2;
	private String address3;
	private String phone;
	private String phone1;
	private String emailId;
	private String emailId1;
	private String isActive;
	private String isDelete;
	private Timestamp createdDate;
	private Integer createdBy;
	private Integer modifiedBy;
	private Timestamp modifiedDate;
	private String profilePic;
	private String hashPassword;
	private String saltKey;
	private String travelerType;
	
	
	private Integer accommodationId;
	private Timestamp arrivalDate;
	private Timestamp departureDate;
	private Integer adultCount;
	private Integer childCount;
	private double amount;
	private double refund;
	public Integer statusId;
	public double tax;
	private Integer bookingId;
	private Integer guestId;
	private Integer roomId;
	private String propertyName;
	private Integer propertyId;
	private String firstName;
	private String lastName;
	private Integer rooms;
	private long adults;
	private long child;
	private double totalAmount;
	
	private String employeeName;
	private String corporateName;
	private String corporateEmail;
	private String corporateNumber;
	private static final String TOKEN_SECRET="s4T2zOIWHMM1sxq";
	private String clientRegion = "ap-south-1";
    private String bucketName = "ulohotelsuploads/uloimg";
    private String bucketNameResume = "ulohotelsuploads/uloresume/resume";
    private String keyName = "ulohotelsuploads";
	
	private List<PmsBookedDetails> bookedList;
	
	public User getModel() {
		return user;
	}  
	
	private HttpServletRequest request;
	
	public HttpSession getSession() {
		return session;
	}
	
	public void setSession(HttpSession session) {
		this.session = session;
	}

	private HttpSession session;
	
	
	
	private SessionMap<String,Object> sessionMap;

	//getters and setters

	@Override
	public void setSession(Map<String, Object> map) {
	sessionMap=(SessionMap)map;
	} 
	

	
	private File myFile;

	private String myFileContentType;
	private String myFileFileName;
	private String propertyReviewLink;
	private String city;
	
	private List<BookingDetailReport> userBookingList;
	
	private static final Logger logger = Logger.getLogger(UserAction.class);

	
	

	
	// @Override
	// public User getModel() {
	// return user;
	// }

	@Override
	public void setServletRequest(HttpServletRequest request) {
		// TODO Auto-generated method stub
		this.request = request;
		
	}

	private UserLoginManager linkController;
	

	public UserAction() {
		linkController = new UserLoginManager();
	}

	public String execute() throws Exception {
		try {

			if (getOper() != null && getOper().equalsIgnoreCase("edit")) {
				editFamilyUser();

			} else if (getOper() != null && getOper().equalsIgnoreCase("add")) {
				add();
			} else if (getOper() != null
					&& getOper().equalsIgnoreCase("delete")) {
			}

			this.userList = linkController.list();
		} catch (Exception e) {
			logger.error(e);
		} finally {

		}
		return SUCCESS;
	}
	
	public String add() {
		try {
			// user.setUserId(100L);
		//	user.setPhone(phone);
			//user.setPassword(password);
			user.setUserName(userName);
			user.setEmailId(emailId);
			linkController.add(getUser());
			this.userList = linkController.list();
		} catch (Exception e) {
			logger.error(e);
		} finally {

		}

		return SUCCESS;
	}
	
	public String userProfileUpdate() throws Exception {
		try {
			//this.projectList = linkController.list();
			this.session = request.getSession();
			//Company company  = (Company)session.getAttribute("COMPANY");
			
			// user.setUserId(getModel().getUserId());

		//	user.setPhone(getModel().getPhone());
			// user.setPassword(getModel().getPassword());
			user.setUserName(getModel().getUserName());
			user.setEmailId(getModel().getEmailId());
			linkController.edit(getModel());
			this.user = getModel();
		//	this.companyName = company.getCompanyName();
		} catch (Exception e) {
			logger.error(e);
		} finally {

		}
		return SUCCESS;
	}
	
	public String userProfilePic() throws IOException {
		try {

			//User u = new User();
			// FileInputStream fis;
			// fis = new FileInputStream(myFile);
			// @SuppressWarnings("deprecation")
			// Blob blob = Hibernate.createBlob(fis);
			// user.setProfilePicture(blob);
			// linkController.edit(getModel());
			// this.user = getModel();
			File fileZip1 = new File(getText("storage.user.logopath") + "/"
					+ getModel().getUserId() + "/", this.getMyFileFileName());
			FileUtils.copyFile(this.myFile, fileZip1);// copying image in the
			
			User u =linkController.findUser(user.getUserId()); 
			
			u.setImgPath(this.getMyFileFileName());
			
			//u.setProfilePicName(this.getMyFileFileName());
			linkController.edit(u);
			this.user = getModel();
		} catch (Exception e) {
			logger.error(e);
		} finally {

		}
		return SUCCESS;
	}
	
	@SuppressWarnings("deprecation")
	public String GetProfilePic() {

		HttpServletResponse response = ServletActionContext.getResponse();

		HttpServletRequest request = ServletActionContext.getRequest();
		String imagePath = "";
		try {
			if (request.getParameter("id") != null
					&& request.getParameter("id") != ""
					&& NumberUtils.isNumber(request.getParameter("id"))) {
				User usr = linkController.findUser(Integer.parseInt(request
						.getParameter("id")));
				if (usr.getImgPath() != null) {
					imagePath = getText("storage.path") + "/" + usr.getUserId()							
							+ "/" + usr.getImgPath();
					Image im = new Image();
					response.getOutputStream().write(
							im.getCustomImageInBytes(imagePath));
					response.getOutputStream().flush();
				} else {
					imagePath = getText("storage.path")
							+ "/emptyprofilepic.png";
					Image im = new Image();
					response.getOutputStream().write(
							im.getCustomImageInBytes(imagePath));
					response.getOutputStream().flush();
				}
			} else {
				imagePath = getText("storage.path") + "/emptyprofilepic.png";
				Image im = new Image();
				response.getOutputStream().write(
						im.getCustomImageInBytes(imagePath));
				response.getOutputStream().flush();
			}

		} catch (Exception e1) {
			logger.error(e1);
		} finally {

		}
		return null;
	}
	
	 public String getUserProfile() throws IOException {
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
	
	
			UserLoginManager userController = new UserLoginManager();
	
			response.setContentType("application/json");
	
			this.userId = (Integer) sessionMap.get("userId");
			User user = userController.find(this.userId);
	
	
			if (!jsonOutput.equalsIgnoreCase(""))
			jsonOutput += ",{";
			else
			jsonOutput += "{";
			jsonOutput += "\"userId\":\"" + user.getUserId() + "\"";
	
	
			jsonOutput += ",\"userName\":\"" + user.getUserName()+ "\"";
			jsonOutput += ",\"address1\":\"" + (user.getAddress1()==null?"":user.getAddress1())+ "\"";
			jsonOutput += ",\"address2\":\"" + (user.getAddress2()==null?"":user.getAddress2())+ "\"";
			jsonOutput += ",\"phone\":\"" + (user.getPhone()==null?"":user.getPhone())+ "\"";
			jsonOutput += ",\"emailId\":\"" + (user.getEmailId()==null?"":user.getEmailId())+ "\"";
	
			jsonOutput += "}";
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");

		} catch (Exception e) {
		logger.error(e);
		e.printStackTrace();
		} finally {

		}

		return null;

		}		
	 
	 public String getPartnerUserProfile() throws IOException {
			try {

				String jsonOutput = "";
				HttpServletResponse response = ServletActionContext.getResponse();
		
		
				UserLoginManager userController = new UserLoginManager();
		
				response.setContentType("application/json");
		
				this.userId = (Integer) sessionMap.get("partnerUserId");
				User user = userController.find(this.userId);
		
		
				if (!jsonOutput.equalsIgnoreCase(""))
				jsonOutput += ",{";
				else
				jsonOutput += "{";
				jsonOutput += "\"userId\":\"" + user.getUserId() + "\"";
		
		
				jsonOutput += ",\"userName\":\"" + user.getUserName()+ "\"";
				jsonOutput += ",\"address1\":\"" + (user.getAddress1()==null?"":user.getAddress1())+ "\"";
				jsonOutput += ",\"address2\":\"" + (user.getAddress2()==null?"":user.getAddress2())+ "\"";
				jsonOutput += ",\"phone\":\"" + (user.getPhone()==null?"":user.getPhone())+ "\"";
				jsonOutput += ",\"emailId\":\"" + (user.getEmailId()==null?"":user.getEmailId())+ "\"";
		
				jsonOutput += "}";
				response.getWriter().write("{\"data\":[" + jsonOutput + "]}");

			} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
			} finally {

			}

			return null;

			}		

	public String getUserProfilePic() {
		HttpServletResponse response = ServletActionContext.getResponse();

		String imagePath = "";
		// response.setContentType("");
		try {
			User usr =linkController.findUser(user.getUserId());
			if ( usr.getImgPath() != null) {
				imagePath = getText("storage.user.logopath") + "/"
						+ usr.getUserId() + "/"
						+ usr.getImgPath();
				Image im = new Image();
				response.getOutputStream().write(im.getCustomImageInBytes(imagePath));
				response.getOutputStream().flush();
			} else {
				imagePath =  "contents/images/avatar.png";
				Image im = new Image();
				
				response.getOutputStream().write(
						im.getCustomImageInBytes(imagePath));
				response.getOutputStream().flush();
			}
		} catch (Exception e1) {
//			logger.error(e1);
//			e1.printStackTrace();
		} finally {

		}
		return null;

	}

	
	public String editFamilyUser() {
		try {
			UserLoginManager  userController = new UserLoginManager();
			User user = userController.findUser(getUserId());
			if(user!=null){
				user.setUserName(getUserName());
				user.setAddress1(getAddress1());
				user.setAddress2(getAddress2());
				user.setPhone(getPhone());
				user.setEmailId(getEmailId());
				
				userController.edit(user);	
			}
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}
	
	public String GetProjectUserAll()
	{
		
		return null;
	}
	public String userPasswordUpdate() throws Exception {
		try {
			// user.setUserId(getModel().getUserId());
			// user.setPhone(getModel().getPhone());
			//String oldhashpassword=passwordDecrypt.getDigestvalid(getOldPassword(),getModel().getSaltkey());
			//getModel().getHashpassword()
			/*if (getOldPassword().equalsIgnoreCase(getModel().getPassword())) {
				user.setPassword(getModel().getPassword());
			}*/
			//if( oldhashpassword.equalsIgnoreCase( getModel().getHashpassword())){
				String PUBLIC_SALT = passwordDecrypt.secrandom();
				//String newhashpassword=passwordDecrypt.getDigestvalid(this.getNewPassword(),PUBLIC_SALT);
				User u = linkController.findUser(user.getUserId());
				//u.setHashPassword(newhashpassword);
				u.setSaltkey(PUBLIC_SALT);
				
			//}
			// user.setUserName(getModel().getUserName());
			// user.setEmailId(getModel().getEmailId());
			linkController.edit(u);
			this.user = getModel();
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return SUCCESS;
	}
	
	public String edituser() {
		try {
			user.setUserId(getUserId());
			//user.setPhone(phone);
			//user.setPassword(password);
			user.setUserName(userName);
			user.setEmailId(emailId);
			linkController.edit(getUser());
		} catch (Exception e) {
			logger.error(e);
		} finally {

		}
		this.userList = linkController.list();
		return SUCCESS;
	}
	
	public String delete() {
		linkController.delete(getUserId());
		return SUCCESS;
	}
	
	public String getUserBookings() throws IOException {
		
		
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			this.totalAmount=0;
			PmsPropertyManager propertyController = new PmsPropertyManager();
			PropertyAccommodationManager propertyAccommodationController = new PropertyAccommodationManager();
			BookingDetailManager bookingDetailController = new BookingDetailManager();
			BookingGuestDetailManager bookingGuestDetailController = new BookingGuestDetailManager();
			PmsGuestManager guestController = new PmsGuestManager();
			PmsStatusManager statusController = new PmsStatusManager();
			PmsBookingManager bookingController = new PmsBookingManager();
			UserLoginManager  userController = new UserLoginManager();
			BookingGuestDetailManager bookingGuestController = new BookingGuestDetailManager();
			
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			//response.setContentType("application/json");
			
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date date = new Date();// Now use today date.
			String todayDate = sdf.format(date.getTime());
			Timestamp tsDate=new Timestamp(date.getTime());
			this.userBookingList = bookingGuestController.userBookingList(getEmailId(),tsDate);
			String bookingIds = this.userBookingList.toString().replaceAll("\\[|\\]","");
			StringBuilder accommodationType = new StringBuilder();
			DecimalFormat df = new DecimalFormat("###.##");
			response.setContentType("application/json");
			 
			for (int i = 0; i<bookingIds.split(",").length; i++) 
		      {
				/*SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				Date date = new Date();// Now use today date.
				String todayDate = sdf.format(date.getTime());
				*/
			  
			   
			   this.bookedList = bookingDetailController.userBookedList(Integer.parseInt(bookingIds.split(",")[i].trim()));
			   for (PmsBookedDetails booked : bookedList) {
	           
            	PmsProperty pmsProperty = propertyController.find(booked.getPropertyId());
            	this.propertyName = pmsProperty.getPropertyName().replaceAll("\\s", "");
            	this.propertyId = pmsProperty.getPropertyId();
            	this.propertyReviewLink = pmsProperty.getPropertyReviewLink();
            	this.arrivalDate = booked.getArrivalDate();
            	this.departureDate = booked.getDepartureDate();
            	this.address1=pmsProperty.getAddress1();
            	this.address2=pmsProperty.getAddress2();
            	this.city = pmsProperty.getCity();
            	/*PmsGuest guest = guestController.find(booked.getGuestId());
            	this.firstName = guest.getFirstName();
            	this.lastName = guest.getLastName();
            	this.phone = guest.getPhone();*/
            	//this.emailId = guest.getEmailId();
            	PropertyAccommodation accommodation = propertyAccommodationController.find(booked.getAccommodationId());
            	accommodationType.append(accommodation.getAccommodationType().trim());
            	accommodationType.append(",");
            	this.rooms = booked.getRooms();
            	this.adults = booked.getAdults();
            	this.child = booked.getChild();
            	this.tax = booked.getTax();
            	this.amount = booked.getAmount();
            	this.totalAmount= this.amount + this.tax;
			   }
			   SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
			   String strFromDate=format2.format(this.arrivalDate);
				String strToDate=format2.format(this.departureDate);
				
		    	DateTime startdate = DateTime.parse(strFromDate);
		        DateTime enddate = DateTime.parse(strToDate);
		        java.util.Date fromdate = startdate.toDate();
		 		java.util.Date todate = enddate.toDate();
		 		Timestamp fromDateST=new Timestamp(fromdate.getTime());
		 		Timestamp toDateST=new Timestamp(todate.getTime());
				int diffInDays = (int) ((todate.getTime() - fromdate.getTime()) / (1000 * 60 * 60 * 24));
			   if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				
				else
					jsonOutput += "{";
				//jsonOutput += "\"availableId\":\"" + availableId+ "\"";
				jsonOutput += "\"bookingId\":\"" + bookingIds.split(",")[i].replaceAll("\\s", "") + "\"";
				jsonOutput += ",\"propertyName\":\"" + this.propertyName + "\"";
				jsonOutput += ",\"propertyId\":\"" + this.propertyId + "\"";
				String checkIn = new SimpleDateFormat("dd-MMM-yyyy").format(this.arrivalDate);
				String checkOut = new SimpleDateFormat("dd-MMM-yyyy").format(getDepartureDate());
				jsonOutput += ",\"arrivalDate\":\"" + checkIn + "\"";
				jsonOutput += ",\"departureDate\":\"" + checkOut+ "\"";
				jsonOutput += ",\"address1\":\"" + this.address1+ "\"";
				jsonOutput += ",\"address2\":\"" + this.address2+ "\"";
				jsonOutput += ",\"propertyReviewLink\":\"" + this.propertyReviewLink+ "\"";
				//jsonOutput += ",\"firstName\":\"" + this.firstName+ "\"";
				//jsonOutput += ",\"lastName\":\"" + this.lastName+ "\"";
				//jsonOutput += ",\"phone\":\"" + this.phone+ "\"";
				//jsonOutput += ",\"emailId\":\"" + this.emailId+ "\"";
				jsonOutput += ",\"accommodationType\":\"" + accommodationType.toString()+ "\"";
				jsonOutput += ",\"rooms\":\"" + this.rooms+ "\"";
				jsonOutput += ",\"city\":\"" + this.city+ "\"";
				jsonOutput += ",\"adultCount\":\"" + this.adults+ "\"";
				jsonOutput += ",\"childCount\":\"" + this.child+ "\"";
				//jsonOutput += ",\"tax\":\"" + this.tax+ "\"";
				jsonOutput += ",\"amount\":\"" + df.format(this.totalAmount)+ "\"";
				jsonOutput += ",\"diffDays\":\"" + diffInDays+ "\"";
				jsonOutput += "}";
				
		      }
			 
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	
	 public String getUpcomingBookings() throws IOException {
			
			try {

				String jsonOutput = "";
				HttpServletResponse response = ServletActionContext.getResponse();
				this.totalAmount=0;
				DecimalFormat df = new DecimalFormat("###.##");
				PmsPropertyManager propertyController = new PmsPropertyManager();
				PropertyAccommodationManager propertyAccommodationController = new PropertyAccommodationManager();
				BookingDetailManager bookingDetailController = new BookingDetailManager();
				BookingGuestDetailManager bookingGuestDetailController = new BookingGuestDetailManager();
				PmsGuestManager guestController = new PmsGuestManager();
				PmsStatusManager statusController = new PmsStatusManager();
				PmsBookingManager bookingController = new PmsBookingManager();
				UserLoginManager  userController = new UserLoginManager();
				BookingGuestDetailManager bookingGuestController = new BookingGuestDetailManager();
				
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				Date date = new Date();// Now use today date.
				String todayDate = sdf.format(date.getTime());
				
				this.userBookingList = bookingGuestController.upcomingBookingList(getEmailId(),todayDate);
				String bookingIds = this.userBookingList.toString().replaceAll("\\[|\\]","");
				
				StringBuilder accommodationType = new StringBuilder();
				 
				response.setContentType("application/json");
				 
				for (int i = 0; i<bookingIds.split(",").length; i++) 
			      {
					/*SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
					Date date = new Date();// Now use today date.
					String todayDate = sdf.format(date.getTime());
					 */
				  
				   
				   this.bookedList = bookingDetailController.userBookedList(Integer.parseInt(bookingIds.split(",")[i].trim()));
				   for (PmsBookedDetails booked : bookedList) {
							           
	            	PmsProperty pmsProperty = propertyController.find(booked.getPropertyId());
	            	this.propertyName = pmsProperty.getPropertyName().replaceAll("\\s", "");	            	            	
	            	this.propertyId = pmsProperty.getPropertyId();
	            	this.arrivalDate = booked.getArrivalDate();
	            	this.departureDate = booked.getDepartureDate();
	            	this.address1=pmsProperty.getAddress1();
	            	this.address2=pmsProperty.getAddress2();
	            	this.city=pmsProperty.getCity();
	            	/*PmsGuest guest = guestController.find(booked.getGuestId());
	            	this.firstName = guest.getFirstName();
	            	this.lastName = guest.getLastName();
	            	this.phone = guest.getPhone();*/
	            	//this.emailId = guest.getEmailId();
	            	PropertyAccommodation accommodation = propertyAccommodationController.find(booked.getAccommodationId());
	            	accommodationType.append(accommodation.getAccommodationType().trim());
	            	accommodationType.append(",");
	            	this.rooms = booked.getRooms();
	            	this.adults = booked.getAdults();
	            	this.child = booked.getChild();
	            	this.tax = booked.getTax();
	            	this.amount = booked.getAmount();
	            	this.totalAmount= this.amount + this.tax;   
				   }
				   SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
				   String strFromDate=format2.format(this.arrivalDate);
					String strToDate=format2.format(this.departureDate);
					
			    	DateTime startdate = DateTime.parse(strFromDate);
			        DateTime enddate = DateTime.parse(strToDate);
			        java.util.Date fromdate = startdate.toDate();
			 		java.util.Date todate = enddate.toDate();
			 		Timestamp fromDateST=new Timestamp(fromdate.getTime());
			 		Timestamp toDateST=new Timestamp(todate.getTime());
					int diffInDays = (int) ((todate.getTime() - fromdate.getTime()) / (1000 * 60 * 60 * 24));
					
				   
				   if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += ",{";
					
					else
						jsonOutput += "{";
					//jsonOutput += "\"availableId\":\"" + availableId+ "\"";
					jsonOutput += "\"bookingId\":\"" + bookingIds.split(",")[i].replaceAll("\\s", "") + "\"";
					jsonOutput += ",\"propertyName\":\"" + this.propertyName + "\"";
					jsonOutput += ",\"propertyId\":\"" + this.propertyId + "\"";
					String checkIn = new SimpleDateFormat("dd-MMM-yyyy").format(this.arrivalDate);
					String checkOut = new SimpleDateFormat("dd-MMM-yyyy").format(getDepartureDate());
					jsonOutput += ",\"arrivalDate\":\"" + checkIn + "\"";
					jsonOutput += ",\"departureDate\":\"" + checkOut+ "\"";
					jsonOutput += ",\"address1\":\"" + this.address1+ "\"";
					jsonOutput += ",\"address2\":\"" + this.address2+ "\"";
					jsonOutput += ",\"city\":\"" + this.city+ "\"";
					//jsonOutput += ",\"firstName\":\"" + this.firstName+ "\"";
					//jsonOutput += ",\"lastName\":\"" + this.lastName+ "\"";
					//jsonOutput += ",\"phone\":\"" + this.phone+ "\"";
					//jsonOutput += ",\"emailId\":\"" + this.emailId+ "\"";
					jsonOutput += ",\"accommodationType\":\"" + accommodationType.toString()+ "\"";
					jsonOutput += ",\"rooms\":\"" + this.rooms+ "\"";
					jsonOutput += ",\"adultCount\":\"" + this.adults+ "\"";
					jsonOutput += ",\"childCount\":\"" + this.child+ "\"";
					jsonOutput += ",\"tax\":\"" + this.tax+ "\"";
					jsonOutput += ",\"amount\":\"" + df.format(this.totalAmount)+ "\"";
					jsonOutput += ",\"diffDays\":\"" + diffInDays+ "\"";
					jsonOutput += "}";
					
			      }
				 
				response.getWriter().write("{\"data\":[" + jsonOutput + "]}");

			} catch (Exception e) {
				logger.error(e);
				e.printStackTrace();
			} finally {

			}

			return null;

		}
	 
	 public String getPastBooking(Users user){
		 
		 	String output="";
			try {
				int count=0;
				String jsonOutput = "";
				HttpServletResponse response = ServletActionContext.getResponse();
				this.totalAmount=0;
				PmsPropertyManager propertyController = new PmsPropertyManager();
				PropertyAccommodationManager propertyAccommodationController = new PropertyAccommodationManager();
				BookingDetailManager bookingDetailController = new BookingDetailManager();
				BookingGuestDetailManager bookingGuestController = new BookingGuestDetailManager();
				
				java.util.Date date=new java.util.Date();
				Calendar calDate=Calendar.getInstance();
         		calDate.setTime(date);
         		calDate.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
         		Date todayDate=calDate.getTime();
				Timestamp tsDate=new Timestamp(todayDate.getTime());
				this.userBookingList = bookingGuestController.userBookingList(user.getEmailId(),tsDate);
				DecimalFormat df = new DecimalFormat("###.##");
				response.setContentType("application/json");
				if(this.userBookingList.size()>0 && !this.userBookingList.isEmpty()){
					for(BookingDetailReport booking:userBookingList){

						this.bookedList = bookingDetailController.userBookedList(booking.getBookingId());
						   for (PmsBookedDetails booked : bookedList) {
				            	PmsProperty pmsProperty = propertyController.find(booked.getPropertyId());
				            	this.propertyName = pmsProperty.getPropertyName();
				            	this.propertyId = pmsProperty.getPropertyId();
				            	this.propertyReviewLink = pmsProperty.getPropertyReviewLink();
				            	this.arrivalDate = booked.getArrivalDate();
				            	this.departureDate = booked.getDepartureDate();
				            	this.address1=pmsProperty.getAddress1();
				            	this.address2=pmsProperty.getAddress2();
				            	this.city = pmsProperty.getCity();

				            	PropertyAccommodation accommodation = propertyAccommodationController.find(booked.getAccommodationId());
				            	this.rooms = booked.getRooms();
				            	this.adults = booked.getAdults();
				            	this.child = booked.getChild();
				            	this.tax = booked.getTax();
				            	this.amount = booked.getAmount();
				            	this.totalAmount= this.amount + this.tax;
				            	
				            	String imagePath="";
				            	if ( pmsProperty.getPropertyThumbPath() != null) {
									imagePath = getText("storage.aws.property.photo") + "/uloimg/property/"
											+ pmsProperty.getPropertyId()+"/"+pmsProperty.getPropertyThumbPath();
								}
								
				            	SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
				            	String strFromDate=format2.format(this.arrivalDate);
								String strToDate=format2.format(this.departureDate);
								
						    	DateTime startdate = DateTime.parse(strFromDate);
						        DateTime enddate = DateTime.parse(strToDate);
						        java.util.Date fromdate = startdate.toDate();
						 		java.util.Date todate = enddate.toDate();
								int diffInDays = (int) ((todate.getTime() - fromdate.getTime()) / (1000 * 60 * 60 * 24));
							    if (!jsonOutput.equalsIgnoreCase(""))
									jsonOutput += ",{";
								
								else
									jsonOutput += "{";
								jsonOutput += "\"bookingId\":\"" + booking.getBookingId() + "\"";
								jsonOutput += ",\"propertyName\":\"" + this.propertyName + "\"";
								jsonOutput += ",\"propertyId\":\"" + this.propertyId + "\"";
								String checkIn = new SimpleDateFormat("dd-MMM-yyyy").format(this.arrivalDate);
								String checkOut = new SimpleDateFormat("dd-MMM-yyyy").format(getDepartureDate());
								jsonOutput += ",\"arrivalDate\":\"" + checkIn + "\"";
								jsonOutput += ",\"departureDate\":\"" + checkOut+ "\"";
								jsonOutput += ",\"address1\":\"" + this.address1+ "\"";
								jsonOutput += ",\"address2\":\"" + this.address2+ "\"";
								jsonOutput += ",\"propertyReviewLink\":\"" + this.propertyReviewLink+ "\"";
								jsonOutput += ",\"accommodationType\":\"" + accommodation.getAccommodationType()+ "\"";
								jsonOutput += ",\"propertyImage\":\"" + ( imagePath )+ "\"";
								jsonOutput += ",\"rooms\":\"" + this.rooms+ "\"";
								jsonOutput += ",\"city\":\"" + this.city+ "\"";
								jsonOutput += ",\"adultCount\":\"" + this.adults+ "\"";
								jsonOutput += ",\"childCount\":\"" + this.child+ "\"";
								jsonOutput += ",\"amount\":\"" + df.format(this.totalAmount)+ "\"";
								jsonOutput += ",\"diffDays\":\"" + diffInDays+ "\"";
								jsonOutput += "}";
								count++;
				            	
						   }
					
					}
				}
			
				 
				String data="\"data\":[" + jsonOutput + "]";
	 			String strData=data.replaceAll("\"", "\"");
	 			
	 			if(count == 0){
	 				output="{\"error\":\"1\",\"message\":\"No Records Found\"}";
				}else{
					output="{\"error\":\"0\"," + strData + "}";
				}
			} catch (Exception e) {
				output="{\"error\":\"2\",\"message\":\"Some technical error on past booking api\"}";
				logger.error(e);
				e.printStackTrace();
			} finally {

			}

			return output;
	 }
	 
	 public String getGuestBookingUsers() {
			try {

				String jsonOutput = "";
				HttpServletResponse response = ServletActionContext.getResponse();

				response.setContentType("application/json");
				// response.getWriter().write("[{\"start\":\"2015-05-11T00:00:00\",\"title\":\"test\"}]");

				
				List<User> userList = linkController.listBookingGuest();

				for (User u : userList) {

					if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += ",{";
					else
						jsonOutput += "{";
					jsonOutput += "\"userId\":\"" + u.getUserId() + "\"";
					jsonOutput += ",\"userName\":\"" + u.getUserName() + "\"";
					jsonOutput += ",\"userEmail\":\"" + u.getEmailId()+ "\"";
					jsonOutput += ",\"roleId\":\"" + u.getRole().getRoleId()+ "\"";
					jsonOutput += ",\"roleName\":\"" + u.getRole().getRoleName()+ "\"";
							
					jsonOutput += "}";

				}

				response.getWriter().write("{\"data\":[" + jsonOutput + "]}");

				// this.setArea(a);
			} catch (Exception e) {
				//logger.error(e);
				e.printStackTrace();
			} finally {

			}
			return null;
		}
	 
	 public String getUpcomingBooking(Users user){
		 	String output="";
			try {
				int count=0;
				String jsonOutput = "";
				HttpServletResponse response = ServletActionContext.getResponse();
				this.totalAmount=0;
				DecimalFormat df = new DecimalFormat("###.##");
				PmsPropertyManager propertyController = new PmsPropertyManager();
				PropertyAccommodationManager propertyAccommodationController = new PropertyAccommodationManager();
				BookingDetailManager bookingDetailController = new BookingDetailManager();
				BookingGuestDetailManager bookingGuestController = new BookingGuestDetailManager();
				
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				
				java.util.Date date=new java.util.Date();
				Calendar calDate=Calendar.getInstance();
				calDate.setTime(date);
				calDate.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
				String todayDate=sdf.format(calDate.getTime());
         	
				this.userBookingList = bookingGuestController.upcomingBookingList(user.getEmailId(),todayDate);
				if(userBookingList.size()>0 && !userBookingList.isEmpty()){
					for(BookingDetailReport booking:userBookingList){
						this.bookedList = bookingDetailController.userBookedList(booking.getBookingId());
						   for (PmsBookedDetails booked : bookedList) {
				            	PmsProperty pmsProperty = propertyController.find(booked.getPropertyId());
				            	this.propertyName = pmsProperty.getPropertyName();
				            	this.propertyId = pmsProperty.getPropertyId();
				            	this.propertyReviewLink = pmsProperty.getPropertyReviewLink();
				            	this.arrivalDate = booked.getArrivalDate();
				            	this.departureDate = booked.getDepartureDate();
				            	this.address1=pmsProperty.getAddress1();
				            	this.address2=pmsProperty.getAddress2();
				            	this.city = pmsProperty.getCity();
				            	String imagePath="";
				            	if ( pmsProperty.getPropertyThumbPath() != null) {
									imagePath = getText("storage.aws.property.photo") + "/uloimg/property/"
											+ pmsProperty.getPropertyId()+"/"+pmsProperty.getPropertyThumbPath();
								}
				            	
								
								
								
				            	PropertyAccommodation accommodation = propertyAccommodationController.find(booked.getAccommodationId());
				            	this.rooms = booked.getRooms();
				            	this.adults = booked.getAdults();
				            	this.child = booked.getChild();
				            	this.tax = booked.getTax();
				            	this.amount = booked.getAmount();
				            	this.totalAmount= this.amount + this.tax;
				            	
				            	SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
				            	String strFromDate=format2.format(this.arrivalDate);
								String strToDate=format2.format(this.departureDate);
								
						    	DateTime startdate = DateTime.parse(strFromDate);
						        DateTime enddate = DateTime.parse(strToDate);
						        java.util.Date fromdate = startdate.toDate();
						 		java.util.Date todate = enddate.toDate();
								int diffInDays = (int) ((todate.getTime() - fromdate.getTime()) / (1000 * 60 * 60 * 24));
							    if (!jsonOutput.equalsIgnoreCase(""))
									jsonOutput += ",{";
								
								else
									jsonOutput += "{";
								jsonOutput += "\"bookingId\":\"" + booking.getBookingId() + "\"";
								jsonOutput += ",\"propertyName\":\"" + this.propertyName + "\"";
								jsonOutput += ",\"propertyId\":\"" + this.propertyId + "\"";
								String checkIn = new SimpleDateFormat("dd-MMM-yyyy").format(this.arrivalDate);
								String checkOut = new SimpleDateFormat("dd-MMM-yyyy").format(getDepartureDate());
								jsonOutput += ",\"arrivalDate\":\"" + checkIn + "\"";
								jsonOutput += ",\"departureDate\":\"" + checkOut+ "\"";
								jsonOutput += ",\"address1\":\"" + this.address1+ "\"";
								jsonOutput += ",\"address2\":\"" + this.address2+ "\"";
								jsonOutput += ",\"propertyReviewLink\":\"" + this.propertyReviewLink+ "\"";
								jsonOutput += ",\"accommodationType\":\"" + accommodation.getAccommodationType()+ "\"";
								jsonOutput += ",\"propertyImage\":\"" + ( imagePath )+ "\"";
								jsonOutput += ",\"rooms\":\"" + this.rooms+ "\"";
								jsonOutput += ",\"city\":\"" + this.city+ "\"";
								jsonOutput += ",\"adultCount\":\"" + this.adults+ "\"";
								jsonOutput += ",\"childCount\":\"" + this.child+ "\"";
								jsonOutput += ",\"amount\":\"" + df.format(this.totalAmount)+ "\"";
								jsonOutput += ",\"diffDays\":\"" + diffInDays+ "\"";
								jsonOutput += "}";
								count++;
				            	
						   }
					}
				}
				String data="\"data\":[" + jsonOutput + "]";
	 			String strData=data.replaceAll("\"", "\"");
	 			
	 			if(count == 0){
	 				output="{\"error\":\"1\",\"message\":\"No Records Found\"}";
				}else{
					output="{\"error\":\"0\"," + strData + "}";
				}

			} catch (Exception e) {
				output="{\"error\":\"2\",\"message\":\"Some technical error on upcoming booking api\"}";
				logger.error(e);
				e.printStackTrace();
			} finally {

			}

			return output;

		
	 }
	
	 public String addSubcriberMail(Search search){
		 String data="";
		 try{
			 this.emailId=search.getEmailId();
			 String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."
						+ "[a-zA-Z0-9_+&*-]+)*@" + "(?:[a-zA-Z0-9-]+\\.)+[a-z"
						+ "A-Z]{2,7}$";
			 SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
			 long time = System.currentTimeMillis();
			 java.sql.Date date = new java.sql.Date(time);
				
			 Calendar today=Calendar.getInstance();
			 today.setTime(date);
			 today.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
		     String strCurrentDate = new SimpleDateFormat("yyyy-MM-dd").format(today.getTime());
		     java.util.Date currentDate = format1.parse(strCurrentDate);
			 Timestamp tsCurrentDate=new Timestamp(currentDate.getTime());	
				
				
			 Pattern pat = Pattern.compile(emailRegex);
			 Boolean bool = pat.matcher(this.emailId).matches();
			 if(bool){
				 UserLoginManager userController=new UserLoginManager();
				 RoleManager roleController=new RoleManager();
				 List<User> finduser=userController.findUserByEmailId(search.getEmailId());
				 if(finduser.size()>0 && !finduser.isEmpty()){
					 for(User user:finduser){
						 data="success";
					 }
					 
				 }else{
					 User user=new User();
					 user.setEmailId(this.emailId);
					 Role role=roleController.find(15);
					 user.setRole(role);
					 user.setIsActive(true);
					 user.setCreatedDate(tsCurrentDate);
					 user.setIsDeleted(false);
					 userController.add(user);
					 data="success";
					 
				 }	 
			 }else{
				 data="error";
			 }
			 
		 }catch(Exception e){
			 data="Some technical issue on subscriber api";
			 e.printStackTrace();
			 data="error";
		 }
		 return data;
	 }
	 
	 public String addUserProfile(Users users){
		 String output="";
		 try{
			 boolean status=false,blnMobile=false,blnEmail=false;
			 
			 this.emailId=users.getEmailId();
			 this.phone=users.getMobileNumber();
			 String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."
						+ "[a-zA-Z0-9_+&*-]+)*@" + "(?:[a-zA-Z0-9-]+\\.)+[a-z"
						+ "A-Z]{2,7}$";
			 
			 Pattern pat = Pattern.compile(emailRegex);
			 Boolean bool = pat.matcher(this.emailId).matches();
			 
			 if(bool){
				 this.emailId=users.getEmailId();
			 }else{
				 this.emailId=null;
			 }
			 
			 String token=null,jsonOutput="";
			 int count=0,errorcount=1;
			 Algorithm algorithm = Algorithm.HMAC256("secret");
			 RoleManager roleController=new RoleManager(); 
			 UserLoginManager userController=new UserLoginManager();
			 List<User> finduserMobile=userController.findUserByPhone(this.phone);
			 if(finduserMobile.size()>0 && !finduserMobile.isEmpty()){
				 blnMobile=true;
			 }
			 
			 List<User> finduser=userController.findUserByEmailId(this.emailId);
			 if(finduser.size()>0 && !finduser.isEmpty()){
				 blnEmail=true;
			 }
			 
			 if(blnMobile || blnEmail){
				 status=true;
			 }else{
				 if(finduser.size()>0 && !finduser.isEmpty() && !blnMobile){
					 for(User user:finduser){
						 
						 if(count==0){
							 status=true;
							 token = JWT.create().withClaim("email", (user.getEmailId()==null?"NA":user.getEmailId()))
					        		  	.withClaim("userId", user.getUserId())
										.withClaim("userName", user.getUserName())
										.withClaim("roleType", user.getRole().getRoleName())
										.sign(algorithm);
							 
							 if(!jsonOutput.equalsIgnoreCase(""))
					        	  jsonOutput+=",{";
					          else
					        	  jsonOutput+="{";
					          jsonOutput+="\"token\" : \""+token+"\"";
					          jsonOutput+=",\"userId\" : \""+user.getUserId()+"\"";
					          jsonOutput+=",\"userName\" : \""+user.getUserName()+"\"";
					          jsonOutput+=",\"emailId\" : \""+(user.getEmailId()==null?"NA":user.getEmailId())+"\"";
					          jsonOutput+=",\"mobileNumber\" : \""+(user.getPhone()==null?"NA":user.getPhone())+"\"";
					          jsonOutput+=",\"address\" : \""+(user.getAddress1()==null?"NA":user.getAddress1())+"\"";
					          if(user.getPmsTags()==null){
									jsonOutput += ",\"tripTypeId\":\"" + 0 + "\"";
								}else{
									jsonOutput += ",\"tripTypeId\":\"" + user.getPmsTags().getTagId()+ "\"";	
								}
					          jsonOutput+=",\"alreadyUser\" : \""+(status)+"\"";
					          jsonOutput+="}";
						 }
						 
						 count++;
						 errorcount=1;
					 }
					 
				 }else{
					 User user=new User();
					 user.setEmailId(this.emailId);
					 user.setUserName(users.getFullName());
					 user.setPhone(users.getMobileNumber());
					 Role role=roleController.find(4);
					 user.setRole(role);
					 user.setIsActive(true);
					 user.setIsDeleted(false);
					 User user1=userController.add(user);
					 status=false;
					 token = JWT.create().withClaim("email",(user1.getEmailId()==null?"NA":user1.getEmailId()))
			        		  	.withClaim("userId", user1.getUserId())
								.withClaim("userName", user1.getUserName())
								.withClaim("roleType", user1.getRole().getRoleName())
								.sign(algorithm);
					 errorcount=0;
					 
					 if(!jsonOutput.equalsIgnoreCase(""))
			        	  jsonOutput+=",{";
			          else
			        	  jsonOutput+="{";
			          jsonOutput+="\"token\" : \""+token+"\"";
			          jsonOutput+=",\"userId\" : \""+user1.getUserId()+"\"";
			          jsonOutput+=",\"userName\" : \""+user1.getUserName()+"\"";
			          jsonOutput+=",\"emailId\" : \""+(user1.getEmailId()==null?"NA":user1.getEmailId())+"\"";
			          jsonOutput+=",\"mobileNumber\" : \""+(user1.getPhone()==null?"NA":user1.getPhone())+"\"";
			          jsonOutput+=",\"address\" : \""+(user.getAddress1()==null?"NA":user.getAddress1())+"\"";
			          if(user1.getPmsTags()==null){
							jsonOutput += ",\"tripTypeId\":\"" + 0 + "\"";
						}else{
							jsonOutput += ",\"tripTypeId\":\"" + user1.getPmsTags().getTagId()+ "\"";	
						}
			          jsonOutput+=",\"alreadyUser\" : \""+(status)+"\"";
			          jsonOutput+="}";
			          this.userId=user1.getUserId();
			          sendUserSignupVouchers(this.userId);
				 }	 
			 }
			 
	          
	          

	          String data="\"data\":[" + jsonOutput + "]";
	 			String strData=data.replaceAll("\"", "\"");
	 			
	 			if(errorcount == 1 && status){
	 				output="{\"error\":\"1\",\"message\":\"You are already registered with us, Please login\"}";
				}else{
					output="{\"error\":\"0\"," + strData + "}";			
				}
	        	  
	          return output;
			 
		 }catch(Exception e){
			 output="{\"error\":\"2\",\"message\":\"Some technical error on add user api\"}";
			 logger.error(e);
			 e.printStackTrace();
		 }
		 return null;
	 }

	 
	 public String addSocialUser(Users users){
		 	String output="";
			
			try {
				
				boolean status=false,blnMobile=false,blnEmail=false;
				this.emailId=users.getEmailId();
				 String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."
							+ "[a-zA-Z0-9_+&*-]+)*@" + "(?:[a-zA-Z0-9-]+\\.)+[a-z"
							+ "A-Z]{2,7}$";
				 
				 Pattern pat = Pattern.compile(emailRegex);
				 Boolean bool = pat.matcher(this.emailId).matches();
				 
				 if(bool){
					 this.emailId=users.getEmailId();
				 }else{
					 this.emailId=null;
				 }
				 String token=null,jsonOutput="";
				 int count=0,errorcount=0;
				 Algorithm algorithm = Algorithm.HMAC256("secret");
				 RoleManager roleController=new RoleManager(); 
				 UserLoginManager userController=new UserLoginManager();
				 
				 
				 List<User> finduser=userController.findUserByEmailId(this.emailId);
				 if(finduser.size()>0 && !finduser.isEmpty()){
					 blnEmail=true;
				 }
				 
				 if(blnEmail){

					 if(finduser.size()>0 && !finduser.isEmpty() && !blnMobile){
						 for(User user:finduser){
							 if(count==0){
								 status=true;
								 token = JWT.create().withClaim("email", user.getEmailId())
						        		  	.withClaim("userId", user.getUserId())
											.withClaim("userName", user.getUserName())
											.withClaim("roleType", user.getRole().getRoleName())
											.sign(algorithm);
								 
								 if(!jsonOutput.equalsIgnoreCase(""))
						        	  jsonOutput+=",{";
						          else
						        	  jsonOutput+="{";
						          jsonOutput+="\"token\" : \""+token+"\"";
						          jsonOutput+=",\"userId\" : \""+user.getUserId()+"\"";
						          jsonOutput+=",\"userName\" : \""+user.getUserName()+"\"";
						          jsonOutput+=",\"emailId\" : \""+(user.getEmailId()==null?"NA":user.getEmailId())+"\"";
						          jsonOutput+=",\"mobileNumber\" : \""+(user.getPhone()==null?"NA":user.getPhone())+"\"";
						          jsonOutput+=",\"address\" : \""+(user.getAddress1()==null?"NA":user.getAddress1())+"\"";
						          if(user.getPmsTags()==null){
										jsonOutput += ",\"tripTypeId\":\"" + 0 + "\"";
									}else{
										jsonOutput += ",\"tripTypeId\":\"" + user.getPmsTags().getTagId()+ "\"";	
									}
						          jsonOutput+=",\"alreadyUser\" : \""+(status)+"\"";
						          jsonOutput+="}";
							 }
							 
							 count++;
							 errorcount=0;
						 }
						 
					 }
				 }else{

					 User user=new User();
					 user.setEmailId(this.emailId);
					 user.setUserName(users.getFullName());
					 user.setPhone(users.getMobileNumber());
					 Role role=roleController.find(4);
					 user.setRole(role);
					 user.setIsActive(true);
					 user.setIsDeleted(false);
					 User user1=userController.add(user);
					 status=false;
					 token = JWT.create().withClaim("email",user1.getEmailId())
			        		  	.withClaim("userId", user1.getUserId())
								.withClaim("userName", user1.getUserName())
								.withClaim("roleType", user1.getRole().getRoleName())
								.sign(algorithm);
					 errorcount=0;
					 
					 if(!jsonOutput.equalsIgnoreCase(""))
			        	  jsonOutput+=",{";
			          else
			        	  jsonOutput+="{";
			          jsonOutput+="\"token\" : \""+token+"\"";
			          jsonOutput+=",\"userId\" : \""+user1.getUserId()+"\"";
			          jsonOutput+=",\"userName\" : \""+user1.getUserName()+"\"";
			          jsonOutput+=",\"emailId\" : \""+(user1.getEmailId()==null?"NA":user1.getEmailId())+"\"";
			          jsonOutput+=",\"mobileNumber\" : \""+(user1.getPhone()==null?"NA":user1.getPhone())+"\"";
			          jsonOutput+=",\"address\" : \""+(user.getAddress1()==null?"NA":user.getAddress1())+"\"";
			          if(user1.getPmsTags()==null){
							jsonOutput += ",\"tripTypeId\":\"" + 0 + "\"";
						}else{
							jsonOutput += ",\"tripTypeId\":\"" + user1.getPmsTags().getTagId()+ "\"";	
						}
			          jsonOutput+=",\"alreadyUser\" : \""+( status)+"\"";
			          jsonOutput+="}";
			          this.userId=user1.getUserId();
			          sendUserSignupVouchers(this.userId);
				 
				 }
				 
				 
		          
		          
		          String data="\"data\":[" + jsonOutput + "]";		        	  
		 			String strData=data.replaceAll("\"", "\"");
		 			
		 			if(errorcount == 1){
		 				output="{\"error\":\"1\",\"message\":\"You are already registered with us, Please login\"}";
					}else{
						output="{\"error\":\"0\"," + strData + "}";			
					}
		          return output;
				 
			 } catch (Exception e) {
				 output="{\"error\":\"2\",\"message\":\"Some technical error on add social login api\"}";
				 logger.error(e);
				e.printStackTrace();
			} finally {

			}

			return null;

		
	 }
	 
	 public String updateProfileImage(Users users){
			try {
				this.myFile=users.getMyFile();			
				this.userId=Integer.parseInt(users.getUserId());
				UserLoginManager userController=new UserLoginManager();
				long time = System.currentTimeMillis();
				java.sql.Date date = new java.sql.Date(time);
				
				AWSCredentials credentials = new BasicAWSCredentials("AKIA3BSHXMURUP3WEKWH", "BjOPBijiDBysvDDbOLJOTzEK+k7yl/mxxWF28k/Y");
				if(this.myFile!=null){
					AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
		                    .withRegion(clientRegion)
		                    .withCredentials(new AWSStaticCredentialsProvider(credentials))
		                    .build();
					String propertyBucketName=null;
					if(this.userId!=null){
						propertyBucketName=bucketName+"/users/"+this.userId;	
					}
					
					
		            s3Client.putObject(propertyBucketName, this.getMyFileFileName(), this.myFile);

					if(this.getMyFileFileName()!=null && !this.getMyFileFileName().equalsIgnoreCase(""))
					{
						User user=userController.find(this.userId);
						user.setImgPath(this.getMyFileFileName());
						user.setIsActive(true);
						user.setIsDeleted(false);
						userController.add(user);
					}	
				}
			
	        }
	        catch(AmazonServiceException e) {
	            e.printStackTrace();
	        }
	        catch(SdkClientException e) {
	            e.printStackTrace();
	        }catch(Exception e){
	        	e.printStackTrace();
	        }
			
			return SUCCESS;
		
	 }
	 
	 public String sendUserSignupVouchers(Integer userid){
		 try{

				User searchUser = linkController.find(userid);
				if(searchUser != null)
				{
					SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
					Calendar today=Calendar.getInstance();
			     	String strCurrentDate = new SimpleDateFormat("yyyy-MM-dd").format(today.getTime());
			     	java.util.Date currentDate = format1.parse(strCurrentDate);
					
					long time = System.currentTimeMillis();
					java.sql.Date date = new java.sql.Date(time);
					
					
					Configuration cfg = new Configuration();
					cfg.setClassForTemplateLoading(UserAddAction.class, "../../../");
					Template template = cfg.getTemplate(getText("signup.user.invitation.template"));
					Map<String, String> rootMap = new HashMap<String, String>();
					rootMap.put("guestName", searchUser.getUserName());
					rootMap.put("from", getText("notification.from"));
					Writer out = new StringWriter();
					template.process(rootMap, out);

					//send email
					Email em = new Email();
					em.set_to(searchUser.getEmailId());
					em.set_from(getText("email.from"));
					em.set_cc(getText("reservation.notification.email"));
					em.set_cc2(getText("bookings.notification.email"));
	 				
					em.set_username(getText("email.username"));
					em.set_subject(getText("userinvitation.subject"));
					em.set_bodyContent(out);
					em.send();
					
					
					this.phone=searchUser.getPhone();
					 
						String message = "&sms_text=" + "Thank you for signing up with Ulo Hotels. Our goal is offer best guest experience in budget stays. Want to know more about us? Check this link:https://www.ulohotels.com/";
						String numbers = "&sms_to=" + "+91"+this.phone ;
						String from = "&sms_from=" + "ULOHTL";
						String type = "&sms_type=" + "trans"; 
						
						// Send data
						HttpURLConnection conn = (HttpURLConnection) new URL("https://secureets.knowlarity.com/ets/dom/clients/ulohotels/api/ulohotels_sms_api.py").openConnection();
						//"http://etsrds.kapps.in/webapi/ulohotels/api/ulohotels_sms_api.py?sms_text=hi%20this%20gopinath%20from%20ulo&sms_to=%20918220211068&sms_from=ULOHTL&sms_type=promo"
						String data = numbers + message +  from + type;
						conn.setDoOutput(true);
						conn.setRequestMethod("POST");
						conn.addRequestProperty("auth_key", "Basic dWxvaG90ZWxzX1Q6VUxPSFRMQDIwMTg=");
						conn.setRequestProperty("Content-Length", Integer.toString(data.length()));
						conn.getOutputStream().write(data.getBytes("UTF-8"));
						final BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
						final StringBuffer stringBuffer = new StringBuffer();
						String line;
						
						while ((line = rd.readLine()) != null) {
							stringBuffer.append(line);
							
						}
						rd.close();
					 
					 
					 
				}
				else
				{
					
				}
				
			
		 }catch(Exception e){
			 e.printStackTrace();
		 }
		 
		 return null;
	 }
	 public String sendCorporateEnquiry() {
			
			//PmsPropertyManager propertyController = new PmsPropertyManager();
			UserLoginManager  userController = new UserLoginManager();
			
			try {
				
				long time = System.currentTimeMillis();
				java.sql.Date date = new java.sql.Date(time);
				
				CorporateEnquiry corporate = new CorporateEnquiry();
				corporate.setEmployeeName(getEmployeeName());
				corporate.setCorporateName(getCorporateName());
				corporate.setCorporateNumber(getCorporateNumber());
				corporate.setCorporateEmail(getCorporateEmail());
				corporate.setIsActive(true);
				corporate.setIsDeleted(false);
				corporate.setCreatedDate(new java.sql.Timestamp(date.getTime()));					
				
				userController.add(corporate);
				
				Configuration cfg = new Configuration();
				cfg.setClassForTemplateLoading(UserAddAction.class, "../../../");
				Template template = cfg.getTemplate(getText("corporateenquiry.template"));
				Map<String, String> rootMap = new HashMap<String, String>();
				//rootMap.put("password", password);
				//rootMap.put("", getText("url.forgetpassword")+uniqueKey.toString());
				rootMap.put("corporateName", getCorporateName());
				rootMap.put("employeeName", getEmployeeName());
				rootMap.put("corporateNumber", getCorporateNumber());
				rootMap.put("corporateEmail", getCorporateEmail());
				
				rootMap.put("from", getText("notification.from"));
				//rootMap.put("createdBy", getUser().getUserName());
				//rootMap.put("logoFileName", c1.getLogoPath());
				rootMap.put("url", getText("app.api.url"));
				Writer out = new StringWriter();
				template.process(rootMap, out);

				//send email
				Email em = new Email();
				em.set_to("corporatesales@ulohotels.com");
				em.set_from(getText("email.from"));
				/*em.set_cc(getText("reservation.notification.email"));
				em.set_cc2(getText("bookings.notification.email"));*/
 				//ArrayList<String> x = new ArrayList<>(Arrays.asList("rajalakshmi.ar@ulohotels.com", "support@ulohotels.com"));
 				//em.set_bcc(x);
 				//em.set_cc3(getText("bookings2.notification.email"));
 				//em.set_cc4(getText("support.notification.email"));
				//em.set_host(getText("email.host"));
				//em.set_password(getText("email.password"));
				//em.set_port(getText("email.port"));
				em.set_username(getText("email.username"));
				em.set_subject(getText("corporateenquiry.subject"));
				em.set_bodyContent(out);
				em.send();
				
			} catch (Exception e) {
				logger.error(e);
				e.printStackTrace();
			} finally {

			}
			return null;
		}

	 	public String sendCorporateEnquiryApi(Search search){
	 		String data="error";
			try {
				int count=0;
				UserLoginManager  userController = new UserLoginManager();	
				long time = System.currentTimeMillis();
				java.sql.Date date = new java.sql.Date(time);
				
				this.emailId=search.getCorporateEmail();
				String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."
							+ "[a-zA-Z0-9_+&*-]+)*@" + "(?:[a-zA-Z0-9-]+\\.)+[a-z"
							+ "A-Z]{2,7}$";
				 
				Pattern pat = Pattern.compile(emailRegex);
				Boolean bool = pat.matcher(this.emailId).matches();
				if(bool){
					List<CorporateEnquiry> listCorporate = userController.findCorporateByEmailId(this.emailId);
					if(listCorporate.size()>0 && !listCorporate.isEmpty()){
						for(CorporateEnquiry enquiry:listCorporate){
							if(count==0){
								CorporateEnquiry corporate =userController.findCorporate(enquiry.getCorporateId());
								if(corporate!=null){
									corporate.setEmployeeName(search.getEmployeeName());
									corporate.setCorporateName(search.getCorporateName());
									corporate.setCorporateNumber(search.getCorporateNumber());
									corporate.setCorporateEmail(search.getCorporateEmail());
									corporate.setIsActive(true);
									corporate.setIsDeleted(false);
									corporate.setCreatedDate(new java.sql.Timestamp(date.getTime()));					
										
									userController.edit(corporate);	
								}	
							}
							count++;
						}
					}else{
						CorporateEnquiry corporate = new CorporateEnquiry();
						corporate.setEmployeeName(search.getEmployeeName());
						corporate.setCorporateName(search.getCorporateName());
						corporate.setCorporateNumber(search.getCorporateNumber());
						corporate.setCorporateEmail(search.getCorporateEmail());
						corporate.setIsActive(true);
						corporate.setIsDeleted(false);
						corporate.setCreatedDate(new java.sql.Timestamp(date.getTime()));					
							
						userController.add(corporate);
							
					}
					
					String support="support@ulohotels.com",corporate="corporatesales@ulohotels.com";
					Configuration cfg = new Configuration();
					cfg.setClassForTemplateLoading(UserAction.class, "../../../");
					Template template = cfg.getTemplate(getText("corporateenquiry.template"));
					Map<String, String> rootMap = new HashMap<String, String>();
					rootMap.put("corporateName", search.getCorporateName());
					rootMap.put("employeeName", search.getEmployeeName());
					rootMap.put("corporateNumber", search.getCorporateNumber());
					rootMap.put("corporateEmail", search.getCorporateEmail());
					
					rootMap.put("from", getText("notification.from"));
					rootMap.put("url", getText("app.api.url"));
					Writer out = new StringWriter();
					template.process(rootMap, out);

					//send email
					Email em = new Email();
					em.set_to(this.emailId);
					em.set_from(getText("email.from"));
					em.set_cc(support);
					em.set_cc2(corporate);
					
					em.set_username(getText("email.username"));
					em.set_subject(getText("corporateenquiry.subject"));
					em.set_bodyContent(out);
					em.sendCorporate();
					data="success";
				 }else{
					 data="error";
				 }
				
				
				
			} catch (Exception e) {
				data="Some technical issue on corporate api";
				logger.error(e);
				e.printStackTrace();
			} finally {

			}
			return data;
		
	 }
	 	
	 public String getUserDetails(String email){
	 	String output="";
		try {
			UserLoginManager userController=new UserLoginManager();
			int count=0;
			String jsonOutput = "";
			this.emailId=email;
			List<User> userlist=userController.findUserByEmailId(emailId);
			if(userlist.size()>0 && !userlist.isEmpty()){
				for(User user:userlist){
					if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += ",{";
					
					else
						jsonOutput += "{";

				    jsonOutput += "\"userId\":\"" + user.getUserId() + "\"";
					jsonOutput += ",\"userName\":\"" + user.getUserName() + "\"";
					jsonOutput += ",\"emailId\":\"" + user.getEmailId() + "\"";
					jsonOutput += ",\"mobileNumber\":\"" + (user.getPhone()==null?"NA":user.getPhone()) + "\"";
					jsonOutput+="}";
					
					count++;
					
				}
			}

			String data="\"data\":[" + jsonOutput + "]";
 			String strData=data.replaceAll("\"", "\"");
 			
 			if(count == 0){
 				output="{\"error\":\"1\",\"message\":\"No Records Found\"}";
			}else{
//				output="{\"error\":\"0\",\"data\":{" + strData + "}}";
				output="{\"error\":\"0\"," + strData + "}";
			}
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return output;
 
	}
	
	@Override
	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	public List<User> getUserList() {
		return userList;
	}

	public void setUserList(List<User> usersList) {
		this.userList = usersList;
	} 
	
	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
    public String getUserName() {
		return userName;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	public Integer getRoleId() {
		return roleId;
	}
	
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	
	public String getAddress1() {
		return address1;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getAddress2() {
		return address2;
	}
	
	public String getAddress3() {
		return address3;
	}

	public void setAddress3(String address3) {
		this.address3 = address3;
	}

	public String getTravelerType() {
		return travelerType;
	}

	public void setTravelerType(String travelerType) {
		this.travelerType = travelerType;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	
	public String getEmailId() {
		return emailId;
	}
	
	public String getPhone1() {
		return phone1;
	}

	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}

	public String getEmailId1() {
		return emailId1;
	}

	public void setEmailId1(String emailId1) {
		this.emailId1 = emailId1;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPhone() {
		return phone;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setOper(String oper) {
		this.oper = oper;
	}

	public String getOper() {
		return oper;
	}
    
	public void setIsDeleted(String isDelete) {
		this.isDelete = isDelete;
	}
	
	public String getIsDeleted() {
		return isDelete;
	}
	
	public File getMyFile() {
		return myFile;
	}

	public void setMyFile(File myFile) {
		this.myFile = myFile;
	}

	public String getMyFileContentType() {
		return myFileContentType;
	}

	public void setMyFileContentType(String myFileContentType) {
		this.myFileContentType = myFileContentType;
	}

	public String getMyFileFileName() {
		return myFileFileName;
	}

	public void setMyFileFileName(String myFileFileName) {
		this.myFileFileName = myFileFileName;
	}
	

	public String getPropertyReviewLink() {
		return propertyReviewLink;
	}

	public void setPropertyReviewLink(String propertyReviewLink) {
		this.propertyReviewLink = propertyReviewLink;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	} 
	
	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	} 
	
	public Timestamp getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Integer getModifiedBy() {
		return modifiedBy;
	}

	public void setImgPath(String profilePic) {
		this.profilePic = profilePic;
	}

	public String getImgPath() {
		return profilePic;
	}
	
	public void setHashPassword(String hashPassword) {
		this.hashPassword = hashPassword;
	}

	public String getHashPassword() {
		return hashPassword;
	}
	
	public void setSaltKey(String saltKey) {
		this.saltKey = saltKey;
	}

	public String getSaltKey() {
		return saltKey;
	}
	
	public List<PmsBookedDetails> getBookedList() {
		return bookedList;
	}

	public void setBookedList(List<PmsBookedDetails> bookedList) {
		this.bookedList = bookedList;
	}
	
	public Integer getAccommodationId() {
		return accommodationId;
	}

	public void setAccommodationId(Integer accommodationId) {
		this.accommodationId = accommodationId;
	}

	public Integer getAdultCount() {
		return adultCount;
	}

	public void setAdultCount(Integer adultCount) {
		this.adultCount = adultCount;
	}

	public Integer getChildCount() {
		return childCount;
	}

	public void setChildCount(Integer childCount) {
		this.childCount = childCount;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public double getTax() {
		return tax;
	}
	
	public void setTax(double tax) {
		this.tax = tax;
	}
	
	public double getRefund() {
		return refund;
	}

	public void setRefund(double refund) {
		this.refund = refund;
	}

	public Integer getStatusId() {
		return statusId;
	}

	public void setStatusId(Integer statusId) {
		this.statusId = statusId;
	}

	public Integer getBookingId() {
		return bookingId;
	}

	public void setBookingId(Integer bookingId) {
		this.bookingId = bookingId;
	}
	
	public Integer getGuestId() {
		return guestId;
	}

	public void setGuestId(Integer guestId) {
		this.guestId = guestId;
	}

	public Integer getRoomId() {
		return roomId;
	}

	public void setRoomId(Integer roomId) {
		this.roomId = roomId;
	}
	
	public Timestamp getArrivalDate() {
		return arrivalDate;
	}

	public void setArrivalDate(Timestamp arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	public Timestamp getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Timestamp departureDate) {
		this.departureDate = departureDate;
	}
	
	public Integer getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(Integer propertyId) {
		this.propertyId = propertyId;
	}

	public String getCorporateName() {
		return corporateName;
	}

	public void setCorporateName(String corporateName) {
		this.corporateName = corporateName;
	}

	public String getCorporateEmail() {
		return corporateEmail;
	}

	public void setCorporateEmail(String corporateEmail) {
		this.corporateEmail = corporateEmail;
	}

	public String getCorporateNumber() {
		return corporateNumber;
	}

	public void setCorporateNumber(String corporateNumber) {
		this.corporateNumber = corporateNumber;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

}
