package com.ulopms.view;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TimeZone;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
//import org.json.JSONObject;
import org.w3c.tidy.Tidy;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.commons.io.FileUtils;
import org.apache.commons.net.ntp.TimeStamp;
import org.apache.derby.tools.sysinfo;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.docx4j.org.xhtmlrenderer.pdf.ITextRenderer;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.html.simpleparser.HTMLWorker;
import com.lowagie.text.pdf.PdfWriter;
import com.opensymphony.xwork2.ActionSupport;

import freemarker.template.Configuration;
import freemarker.template.Template;

import javax.servlet.http.HttpServletRequest;

import com.opensymphony.xwork2.ModelDriven;
import com.ulopms.view.BookingListAction;
import com.ulopms.controller.OtaHotelsManager;
import com.ulopms.controller.PmsBookingManager;
import com.ulopms.controller.PmsDayManager;
import com.ulopms.controller.PmsPropertyManager;
import com.ulopms.controller.PmsGuestManager;
import com.ulopms.controller.PmsSmartPriceManager;
import com.ulopms.controller.PmsSourceManager;
import com.ulopms.controller.PmsStatusManager;
import com.ulopms.controller.PmsTagManager;
import com.ulopms.controller.PromotionManager;
import com.ulopms.controller.PropertyAccommodationInventoryManager;
import com.ulopms.controller.PropertyAccommodationManager;
import com.ulopms.controller.PropertyAccommodationRoomManager;
import com.ulopms.controller.BookingDetailManager;
import com.ulopms.controller.BookingGuestDetailManager;
import com.ulopms.controller.PropertyContractTypeManager;
import com.ulopms.controller.PropertyDiscountManager;
import com.ulopms.controller.PropertyRatePlanDetailManager;
import com.ulopms.controller.PropertyTaxeManager;
import com.ulopms.controller.ReportManager;
import com.ulopms.controller.UserLoginManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.BookingDetailReport;
import com.ulopms.model.OtaHotelDetails;
import com.ulopms.model.OtaHotels;
import com.ulopms.model.PmsBookedDetails;
import com.ulopms.model.PmsBooking;
import com.ulopms.model.BookedDetail;
import com.ulopms.model.BookingDetail;
import com.ulopms.model.BookingGuestDetail;
import com.ulopms.model.PmsAvailableRooms;
import com.ulopms.model.PmsDays;
import com.ulopms.model.PmsPromotions;
import com.ulopms.model.PmsRoomDetail;
import com.ulopms.model.PmsGuest;
import com.ulopms.model.PmsProperty;
import com.ulopms.model.PmsSmartPrice;
import com.ulopms.model.PmsSource;
import com.ulopms.model.PmsStatus;
import com.ulopms.model.PmsTags;
import com.ulopms.model.PropertyAccommodation;
import com.ulopms.model.PropertyAccommodationInventory;
import com.ulopms.model.PropertyAccommodationRoom;
import com.ulopms.model.PropertyContractType;
import com.ulopms.model.PropertyDiscount;
import com.ulopms.model.PropertyRatePlanDetail;
import com.ulopms.model.PropertyTaxe;
import com.ulopms.model.User;
import com.ulopms.util.Email;
import com.ulopms.util.Image;

public class BookingDetailAction extends ActionSupport implements
ServletRequestAware, SessionAware, UserAware{

	private static final long serialVersionUID = 914982665758390091L;

	private Integer bookingDetailsId;
	private Integer accommodationId;
	private Timestamp arrivalDate;
	private Timestamp departureDate;
	private Timestamp arrivalTime;
	private Timestamp departureTime;
	private Timestamp tscheckInTime;
	private Timestamp tscheckOutTime;
	
	private Integer adultCount;
	private Integer childCount;
	private double tariffAmount;
	private double amount=0;
	private double totalAmount;
	private double refund;
	private double grandTotal;
	private String checkedDays;
	private String voucherType;
	private double minimumAmount;
	private double maximumAmount;
	private double actualAmount;
	public Integer statusId;
	public double tax=0;
	public double partnerTax=0;
	public double partnerTariff=0;
	public double partnerTotal=0;
	private Integer bookingId;
	private Integer guestBookingId;
    private String filterBookingId;
	private Integer guestId;
	private Integer roomId;
	private String propertyName;
	private Integer propertyId;
	private String latitude;
	private String longitude;
	private String firstName;
	private String lastName;
	private String phone;
	private String emailId;
	private String gstNo;
	private Integer rooms;
	private Integer days;
	private Integer discountId;
	private double txtGrandTotal;
	private double hotelPay;
    private String specialRequest;
	private String sourceName;
	private String sourceType;
	private String roomNights;
	private String paymentStatus;
	private Integer userId;
	private Integer sourceId;
	private Integer contractTypeId;
	private String contractType;
	private String routeMap;
	private boolean blockFlag;
	private String roomType;
	private String roomNightsType;
	private String address1;
	private String address2;
	private String checkInTime;
	private String checkOutTime;
	private String phoneNumber;
	private String locationName;
	private String locationUrl;
	private String reservationManagerEmail;
	private String resortManagerEmail;
	private String reservationManagerContact;
	private String propertyEmail;
	private Integer propertyDiscountId;
	private String propertyDiscountName;
	private String promotionFirstName;
	private String promotionSecondName;
	private String couponName;
	private String timeHours;
	private String tagName;
	private String checkIn;
	private String checkOut;
	private String checkInDays;
	private String checkOutDays;
	private String bookingInTime;
	private String bookingOutTime;
	private String bookingDate;
	private String bookingTime;
	private String locationType;
	private String contractModel;
	private String rateModel;
	private long adults;
	private long child;
	private long infant;
	private long guests;
	private long roomCount;
	private String galleryPath;
	private boolean mailFlag;
	private boolean mailSubjectFlag;
	private boolean mailSource;
	private Integer diffInDays;
	private Integer diffInHours;
    int totalAdultCount = 0;
	int totalInfantCount = 0;
	int totalChildCount = 0;
	double totalTaxAmount = 0;
    double totalBaseAmount = 0;
    double totalAdvanceAmount = 0;
    private Double couponAmount=0.0;
    private Double promotionAmount=0.0;
    private Double purchaseRate;
    private Double extraAdultRate;
    private Double extraChildRate;
    private Double extraCharges=0.0;
    private Double addonAmount;
    private String propertyUrl;
    private double dblOtaCommission=0.0;
    private double dblOtaTax=0.0;
    private double dblTotalOtaAmout=0.0;
    private double dblTax=0.0;
    private double dblAmount=0.0;
    private double dblRevenueAmount=0.0;
    private double dblTotalRevenueTaxAmount=0.0;
    private double dblCommission=0.0;
    private double dblTotalUloCommission=0.0;
    private double dblUloCommission=0.0;
    private double dblUloTaxAmount=0.0;
    private double dblHotelPayable=0.0;
    private double dblHotelTaxPayable=0.0;
    private double dbltotalhotelrevenue=0.0;
    private double dbltaxes=0.0;
    private long roomCnt;
    private String promotionName="";
    private Integer locationTypeId;
    private Integer locationId;
    private String bookingType;
    private Integer taxType;
	private BookingDetail bookingDetail;
	
	 private List<PmsBookedDetails> bookedList;
	 
	 private List<PmsRoomDetail> roomList;
	 private List<BookingDetailReport> listBookingDetails;
	 List<BookingListAction> array = new ArrayList<BookingListAction>();

     public List<BookingListAction> getArray() {
         return array;
     }
     
     public void setArray(List<BookingListAction> array) {
         this.array = array;
     }
	
    
	
	private List<BookingDetail> bookingDetailList;
	
	private List<BookedDetail> bookList;
	
	private List<PropertyAccommodation> accommodationList;

	private static final Logger logger = Logger.getLogger(BookingDetailAction.class);

	private HttpSession session;

	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}
	
	private SessionMap<String,Object> sessionMap;  
	  
	//getters and setters  
	  
	@Override  
	public void setSession(Map<String, Object> map) {  
	    sessionMap=(SessionMap)map;  
	}  
	
	
	private User user;

	
	// @Override
	// public User getModel() {
	// return user;
	// }

	@Override
	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	
	private HttpServletRequest request;

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	public BookingDetailAction() {

	}

	public String execute() {

		
		return SUCCESS;
	}

	
	 public String viewBookingDetails() {
 		try {
 			long time = System.currentTimeMillis();
 			java.sql.Date date = new java.sql.Date(time);
 			
 			this.bookingId = (Integer) sessionMap.get("bookingId");
			this.guestId = (Integer) sessionMap.get("guestId");
 	        for (int i = 0; i < array.size(); i++) {
 	        }
 		} catch (Exception e) {
 			logger.error(e);
 			e.printStackTrace();
 		} finally {

 		}
 		return SUCCESS;
 	}
  
	
	 public String  getHotelierBookedDetails() throws IOException {
			
		    
		
			 try {
				DecimalFormat df = new DecimalFormat("###.##");
				String jsonOutput = "";
				HttpServletResponse response = ServletActionContext.getResponse();
			
				PmsPropertyManager propertyController = new PmsPropertyManager();
				PropertyAccommodationManager propertyAccommodationController = new PropertyAccommodationManager();
				BookingDetailManager bookingDetailController = new BookingDetailManager();
				BookingGuestDetailManager bookingGuestDetailController = new BookingGuestDetailManager();
				PmsGuestManager guestController = new PmsGuestManager();
				PmsStatusManager statusController = new PmsStatusManager();
				PropertyDiscountManager propertyDiscountController = new PropertyDiscountManager();  
				PmsBookingManager bookingController = new PmsBookingManager();
				
				response.setContentType("application/json");
				this.bookedList = bookingDetailController.bookedList(this.bookingId);
				
				StringBuilder accommodationType = new StringBuilder();
				
				StringBuilder accommodationRate = new StringBuilder();
				
				this.totalAdultCount = (Integer) sessionMap.get("totalAdultCount");
				
				//this.totalInfantCount = (Integer) sessionMap.get("totalInfantCount");
				
				this.totalChildCount = (Integer) sessionMap.get("totalChildCount");
				this.txtGrandTotal=(Double)sessionMap.get("txtGrandTotal");
				
				
				List accommodationBooked = new ArrayList();
	            for (PmsBookedDetails booked : bookedList) {
	            	this.bookingId = getBookingId();
	            	this.sourceId=booked.getSourceId();
	            	this.propertyDiscountId = booked.getPropertyDiscountId();
	            	PropertyDiscount discount = propertyDiscountController.findId(this.propertyDiscountId);
	            	this.propertyDiscountName = discount.getDiscountName();
	            	PmsProperty pmsProperty = propertyController.find(booked.getPropertyId());
	            	this.propertyName = pmsProperty.getPropertyName();//resort name and location name is missing
	            	this.propertyId = pmsProperty.getPropertyId();
	            	this.latitude = pmsProperty.getLatitude();
	            	this.longitude = pmsProperty.getLongitude();
	            	this.address1=pmsProperty.getAddress1();//property address1
	            	this.address2=pmsProperty.getAddress2();//property address2
	            	this.phoneNumber = pmsProperty.getPropertyContact();//property phone number
	            	this.propertyEmail = pmsProperty.getPropertyEmail();
	            	this.locationName=pmsProperty.getLocation().getLocationName();//property location
	            	this.reservationManagerEmail=pmsProperty.getReservationManagerEmail();
	            	this.resortManagerEmail=pmsProperty.getResortManagerEmail();
	            	this.reservationManagerContact = pmsProperty.getReservationManagerContact();
	            	java.util.Date date=new java.util.Date();
	            	Calendar calDate=Calendar.getInstance();
	            	calDate.setTime(date);
	            	calDate.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
	            	int bookedHours=calDate.get(Calendar.HOUR);
	            	int bookedMinute=calDate.get(Calendar.MINUTE);
	            	int bookedSecond=calDate.get(Calendar.SECOND);
	            	int AMPM=calDate.get(Calendar.AM_PM);
	            	String strBookingDate=new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH).format(calDate.getTime());
	            	String hours="",minutes="",seconds="";
	            	if(bookedHours<10){
	            		hours=String.valueOf(bookedHours);
	            		hours="0"+hours;
	            	}else{
	            		hours=String.valueOf(bookedHours);
	            	}
	            	if(bookedMinute<10){
	            		minutes=String.valueOf(bookedMinute);
	            		minutes="0"+minutes;
	            	}else{
	            		minutes=String.valueOf(bookedMinute);
	            	}
	            	
	            	if(bookedMinute<10){
	            		seconds=String.valueOf(bookedSecond);
	            		seconds="0"+seconds;
	            	}else{
	            		seconds=String.valueOf(bookedSecond);
	            	}
	            	
	            	if(AMPM==0){//If the current time is AM
	            		this.timeHours=strBookingDate+" "+hours+" : "+minutes+" : "+seconds+" AM ";
	            	}else if(AMPM==1){//If the current time is PM
	            		this.timeHours=strBookingDate+" "+hours+" : "+minutes+" : "+seconds+" PM ";
	            	}
	            	if(this.mailSource == true){
	            		if(this.sourceId==12){
	            			this.sourceName = "offline";
	            		}else{
	            			this.sourceName = "resort";
	            		}
	            	}else{
	            		if(this.sourceId==12){
	            			this.sourceName = "offline";
	            		}else{
	            			this.sourceName = "online";
	            		}
	            		
	            	}
	            	
	            	
	                
	            	this.arrivalDate = booked.getArrivalDate();
	            	this.departureDate = booked.getDepartureDate();
	            	PmsGuest guest = guestController.find(booked.getGuestId());
	            	this.firstName = guest.getFirstName();
	            	this.lastName = guest.getLastName();
	            	this.phone = guest.getPhone();
	            	this.emailId = guest.getEmailId();
	            	this.specialRequest = booked.getSpecialRequest();
	            	if(this.specialRequest==null){
	            		this.specialRequest="Not Available";
	            	}
	            	
	            	String checkIn = new SimpleDateFormat("MM/dd/yyyy").format(this.arrivalDate);
					String checkOut = new SimpleDateFormat("MM/dd/yyyy").format(this.departureDate);
					
					LocalDate date1 = LocalDate.parse(checkIn, DateTimeFormat.forPattern("MM/dd/yyyy"));
	                LocalDate date2 = LocalDate.parse(checkOut, DateTimeFormat.forPattern("MM/dd/yyyy"));
	                this.days = Days.daysBetween(date1, date2).getDays();
	            	
	              this.bookList =  bookingDetailController.listAccommodation(getBookingId(),booked.getAccommodationId());
	              if (!jsonOutput.equalsIgnoreCase(""))
							jsonOutput += ",{";
						
						else
							jsonOutput += "{";
	              
	              	String jsonTypes="";
	          		jsonOutput += "\"bookingid\":\"" + getBookingId().toString() + "\"";
          	
          		for (BookedDetail bookedDetail : bookList) {
	            		
		            	PropertyAccommodation accommodation = propertyAccommodationController.find(bookedDetail.getAccommodationId());
		            	this.roomCount = bookedDetail.getRoomCount()/this.days;
		            	this.dblOtaCommission=bookedDetail.getOtaCommission();
	                    this.dblOtaTax=bookedDetail.getOtaTax();
	                    this.amount=bookedDetail.getAmount();
	                    this.dblTotalOtaAmout=this.dblOtaCommission+this.dblOtaTax;
	                    
	                    this.dblRevenueAmount=this.amount-this.dblTotalOtaAmout;
	                    String strRevenueAmount=df.format(this.dblRevenueAmount);
	                    this.dblRevenueAmount=Double.valueOf(strRevenueAmount);
	                    
	                    this.dblTax=bookedDetail.getTax();
	                    String strTax=df.format(this.dblTax);
	                    this.dblTax=Double.valueOf(strTax);
		            	
		            	accommodationBooked.add(new BookedDetail(accommodation.getAccommodationType(),this.dblTax,this.roomCount,
		            			bookedDetail.getAdultCount(),bookedDetail.getChildCount() ,this.dblRevenueAmount)); 
		            	 
		            	if (!jsonTypes.equalsIgnoreCase(""))
							jsonTypes += ",{";
		            	
						else
							jsonTypes += "{";
	            		
	            		 
	            		jsonTypes += "\"accommodationType\":\"" + accommodation.getAccommodationType().trim()+ "\"";
	            		jsonTypes += ",\"rooms\":\"" + this.roomCount+ "\"";
	            		jsonTypes += ",\"adultCount\":\"" + bookedDetail.getAdultCount()+ "\"";
	            		jsonTypes += ",\"childCount\":\"" + bookedDetail.getChildCount()+ "\"";
	            		jsonTypes += ",\"amount\":\"" + bookedDetail.getAmount()+ "\"";
	            		jsonTypes += ",\"tax\":\"" + bookedDetail.getTax()+ "\"";
	            		jsonTypes += ",\"total\":\"" + (bookedDetail.getTax() + bookedDetail.getAmount())  + "\"";
	            		jsonTypes += "}";
	            		
	         	 }
          		jsonOutput += ",\"types\":[" + jsonTypes+ "]";
	            	
	            	
	            	this.rooms = booked.getRooms();
	            	this.adults = this.totalAdultCount;
	            	this.infant = this.totalInfantCount;
	            	this.child = this.totalChildCount;
	            	this.tax = booked.getTax();
	            	this.amount = booked.getAmount();
	            	this.totalAmount = this.tax + this.amount;
	            	
	            	String strTotal=df.format(this.totalAmount);
	            	this.totalAmount=Double.valueOf(strTotal);
	            	
                    this.totalAdvanceAmount = booked.getAdvanceAmount();
                    
//                    OTA Share
                    this.dblOtaCommission=booked.getOtaCommission();
                    this.dblOtaTax=booked.getOtaTax();
    				
                    this.dblTotalOtaAmout=this.dblOtaCommission+this.dblOtaTax;
    				
                    this.dblRevenueAmount=this.amount-this.dblTotalOtaAmout;
                    this.dblTotalRevenueTaxAmount=this.dblRevenueAmount+this.tax;
    				
//    				ULO Share
                    this.dblCommission=pmsProperty.getPropertyCommission();
                    this.dblUloCommission=this.dblRevenueAmount*this.dblCommission/100;
    				
    				
                    this.dblUloTaxAmount=this.dblUloCommission*18/100;
    				
                    this.dblTotalUloCommission=this.dblUloCommission+this.dblUloTaxAmount; //Payable to Ulo
    				
    				
                    this.dblHotelPayable=this.dblRevenueAmount-this.dblTotalUloCommission;
    				
                    PropertyTaxeManager propertytaxController = new PropertyTaxeManager();
	  					 double taxe =  this.dblHotelPayable/this.days;
	  					
						 PropertyTaxe tax = propertytaxController.find(taxe);
						 this.dbltaxes = Math.round(this.dblHotelPayable * tax.getTaxPercentage() / 100  ) ;
                    this.dblHotelTaxPayable=this.dblHotelPayable+this.dbltaxes; //Payable to Hotelier
    				
    				
    				
                 /*if(this.totalAdvanceAmount == 0){
                 	this.hotelPay = 0;
	            	}
	            	else{
	            		this.hotelPay = (this.totalAmount-this.totalAdvanceAmount);
	            	}*/
                 this.hotelPay = (this.totalAmount-this.totalAdvanceAmount); 
	            	if(this.totalAmount == booked.getAdvanceAmount()){
	            		this.paymentStatus = "Paid";
	            	}
	            	else{
	            		if(this.mailSource == true){
	            			if(this.sourceId==12){
	            				if(this.totalAmount == booked.getAdvanceAmount()){
	        	            		this.paymentStatus = "Paid";
	        	            	}else{
	        	            		this.paymentStatus = "Partially Paid";
	        	            	}
	            			}else{
	            				this.paymentStatus = "Paid";
	            			}
		            	}else{
		            		this.paymentStatus = "Partially Paid";
		            	}
	            	}
	            	
	                
	               
	                jsonOutput += ",\"propertyName\":\"" + this.propertyName+ "\"";
		            jsonOutput += ",\"locationName\":\"" + this.locationName+ "\"";
					jsonOutput += ",\"address1\":\"" + this.address1+ "\"";
					jsonOutput += ",\"address2\":\"" + this.address2+ "\"";
					jsonOutput += ",\"phoneNumber\":\"" + this.phoneNumber+ "\"";
					jsonOutput += ",\"propertyEmail\":\"" + this.propertyEmail+ "\"";
					jsonOutput += ",\"reservationManagerEmail\":\"" + this.reservationManagerEmail+ "\"";
					jsonOutput += ",\"reservationManagerContact\":\"" + this.reservationManagerContact+ "\"";
					
					
	            	String CheckIn = new SimpleDateFormat("MMM d,yyyy").format(this.arrivalDate);// check in 
	                String CheckOut = new SimpleDateFormat("MMM d,yyyy").format(this.departureDate);//check out
	                
	                jsonOutput += ",\"checkIn\":\"" + CheckIn+ "\"";
					jsonOutput += ",\"checkOut\":\"" + CheckOut+ "\"";
					jsonOutput += ",\"timeHours\":\"" + this.timeHours+ "\"";
					jsonOutput += ",\"days\":\"" + this.days.toString()+ "\"";
					jsonOutput += ",\"guestname\":\"" + this.firstName+ "\"";
                  String strFirstName="",firstNameLetterUpperCase="";
					String strLastName="",lastNameLetterUpperCase="",strFullName="";
					strFirstName=this.firstName;
					strLastName=this.lastName;
					if(strFirstName!=null && strFirstName!="" && !strFirstName.equalsIgnoreCase("undefined")){
						strFirstName=strFirstName.trim();
						firstNameLetterUpperCase=strFirstName.substring(0, 1).toUpperCase()+strFirstName.substring(1);
						strFirstName=firstNameLetterUpperCase;
					}else{
						strFirstName="Not Available";
					}
					if(strLastName!=null && strLastName!=""  && !strLastName.equalsIgnoreCase("undefined")){
						strLastName=strLastName.trim();
						lastNameLetterUpperCase=strLastName.substring(0, 1).toUpperCase() +  strLastName.substring(1);
						strLastName=lastNameLetterUpperCase;
					}else{
						strLastName="";
					}
					strFullName=strFirstName+" "+strLastName;
					
					 this.promotionName = (String) sessionMap.get("promotionName");
					    if(promotionName==null || promotionName==""){
					    	promotionName="NA";
					    }
					    if(this.propertyDiscountId == 35){
					    	this.propertyDiscountName = "NA";
					    }
					
					jsonOutput += ",\"guestname\":\"" + strFullName+ "\"";
					jsonOutput += ",\"sourcename\":\"" + this.sourceName+ "\"";
					jsonOutput += ",\"paymentstatus\":\"" + this.paymentStatus+ "\"";
					jsonOutput += ",\"specialrequest\":\"" + this.specialRequest+ "\"";
					jsonOutput += ",\"promotionName\":\"" + this.promotionName + "\"";
					jsonOutput += ",\"propertyDiscountName\":\"" + this.propertyDiscountName + "\"";
					jsonOutput += ",\"mobile\":\"" + this.phone+ "\"";
					jsonOutput += ",\"emailId\":\"" + this.emailId+ "\"";
					jsonOutput += ",\"rooms\":\"" + this.rooms.toString()+ "\"";
					jsonOutput += ",\"adults\":\"" + String.valueOf(this.adults)+ "\"";
					jsonOutput += ",\"child\":\"" + String.valueOf(this.child)+ "\"";
					jsonOutput += ",\"tax\":\"" + String.valueOf(this.tax) + "\"";
					jsonOutput += ",\"amount\":\"" + String.valueOf(this.amount)+ "\"";
					jsonOutput += ",\"totalamount\":\"" + Double.valueOf(this.totalAmount)+ "\"";
					//jsonOutput += ",\"totaladvanceamount\":\"" + Double.valueOf(this.totalAdvanceAmount)+ "\"";
					
					/*if(getDiscountId()!=null){
						jsonOutput += ",\"totalamount\":\"" + String.valueOf(this.txtGrandTotal)+ "\"";
					}else{
						jsonOutput += ",\"totalamount\":\"" + String.valueOf(this.totalAmount)+ "\"";	
					}*/
					
					
					
					
					jsonOutput += "}";

				}
	           
	                String checkIn = new SimpleDateFormat("MM/dd/yyyy").format(this.arrivalDate);
				    String checkOut = new SimpleDateFormat("MM/dd/yyyy").format(getDepartureDate());
				    this.promotionName = (String) sessionMap.get("promotionName");
				    if(promotionName==null || promotionName==""){
				    	promotionName="NA";
				    }
				    
	            if(this.mailFlag == true)
	            {
	            	try
	 				{
	 				//email template
	 				Configuration cfg = new Configuration();
	 				cfg.setClassForTemplateLoading(BookingDetailAction.class, "../../../");
	 				Template template = cfg.getTemplate(getText("hotelier.template"));
	 				Map<String, Object> rootMap = new HashMap<String, Object>();
	 				//Map<String, String> rootMap = new HashMap<String, String>();	 				
	 				rootMap.put("bookingid",getBookingId().toString());
	 				rootMap.put("propertyname",this.propertyName);
	 				rootMap.put("propertyid",this.propertyId);
	 				rootMap.put("latitude",this.latitude.trim());
	 				rootMap.put("longitude",this.longitude.trim());
	 				rootMap.put("sourcename",this.sourceName);
	 				rootMap.put("paymentstatus",this.paymentStatus);
	 				rootMap.put("propertyDiscountName",this.propertyDiscountName);
	 				rootMap.put("reservationManagerEmail",this.reservationManagerEmail);
	 				rootMap.put("reservationManagerContact",this.reservationManagerContact);
	 				rootMap.put("propertyEmail",this.propertyEmail);
	 				rootMap.put("specialrequest",this.specialRequest);
	 				rootMap.put("guestname",this.firstName);
	 				rootMap.put("locationName",this.locationName);
	 				rootMap.put("address1",this.address1);
	 				rootMap.put("address2",this.address2);
	 				rootMap.put("hotelpay",this.hotelPay);
	 				rootMap.put("phoneNumber",this.phoneNumber);
	 				rootMap.put("checkIn", checkIn);
	 				rootMap.put("checkOut",checkOut);
	 				rootMap.put("timeHours",this.timeHours);
	 				
	 				String strFirstName="",firstNameLetterUpperCase="";
					String strLastName="",lastNameLetterUpperCase="",strFullName="";
					strFirstName=this.firstName;
					strLastName=this.lastName;
					if(strFirstName!=null && strFirstName!="" && !strFirstName.equalsIgnoreCase("undefined")){
						strFirstName=strFirstName.trim();
						firstNameLetterUpperCase=strFirstName.substring(0, 1).toUpperCase()+strFirstName.substring(1);
						strFirstName=firstNameLetterUpperCase;
					}else{
						strFirstName="Not Available";
					}
					if(strLastName!=null && strLastName!=""  && !strLastName.equalsIgnoreCase("undefined")){
						strLastName=strLastName.trim();
						lastNameLetterUpperCase=strLastName.substring(0, 1).toUpperCase() +  strLastName.substring(1);
						strLastName=lastNameLetterUpperCase;
					}else{
						strLastName="";
					}
					
					
					strFullName=strFirstName+" "+strLastName;
	 				rootMap.put("guestname",strFullName);
	 				//rootMap.put("guestname",this.firstName + " " + this.lastName);
	 				rootMap.put("mobile",this.phone);
	 				rootMap.put("emailId",this.emailId);	 				
	 				rootMap.put("checkin",checkIn);
	 				rootMap.put("days",this.days.toString());
	 				rootMap.put("checkout",checkOut);
	 				rootMap.put("promotionName",promotionName);
	 				
	 				
	 				
	 				rootMap.put("accommodationBooked", accommodationBooked);
	 				//rootMap.put("rooms", this.roomCount);
	 				rootMap.put("adults", String.valueOf(this.adults));
	 				rootMap.put("child",String.valueOf(this.child));
	 				rootMap.put("infant", String.valueOf(this.infant));
	 				rootMap.put("tax",df.format(this.tax));
	 				rootMap.put("amount",df.format(this.dblRevenueAmount));
	 				rootMap.put("totalamount",df.format(this.dblTotalRevenueTaxAmount));
	 				rootMap.put("totaladvanceamount",df.format(this.totalAdvanceAmount));
	 				
	 				rootMap.put("otaCommission",df.format(this.dblOtaCommission));
	 				rootMap.put("otaTax",df.format(this.dblOtaTax));
	 				rootMap.put("totalOtaRevenue",df.format(this.dblTotalOtaAmout));
	 				rootMap.put("totalRevenueAmount",df.format(this.dblRevenueAmount));
	 				rootMap.put("uloCommission",df.format(this.dblUloCommission));
	 				rootMap.put("uloTax",df.format(this.dblUloTaxAmount));
	 				rootMap.put("totalUloRevenue",df.format(this.dblTotalUloCommission));
	 				rootMap.put("hotelPayable",df.format(this.dblHotelPayable));
	 				rootMap.put("hotelTariff",df.format(this.dblHotelPayable));
	 				rootMap.put("hotelTax",df.format(this.dbltaxes));
	 				rootMap.put("totalHotelRevenue",df.format(this.dblHotelTaxPayable));
	 				
	 				//rootMap.put("totalamount",String.valueOf(this.txtGrandTotal));
	 				
	 				String[] arrString=null;
	 				ArrayList<String> arlToken=new ArrayList<String>();
	 				StringTokenizer stoken=new StringTokenizer(this.resortManagerEmail,",");
	 				String strResortMailId1=null,strResortMailId2=null,strResortMailId3=null,strResortMailId4=null,strResortMailId5=null;
	 				while(stoken.hasMoreElements()){
	 					arlToken.add(stoken.nextToken());
	 				}
	 				
	 				int intCount=0, arlSize=0;
	 				arrString=new String[arlToken.size()];
	 				Iterator<String> iterListValues=arlToken.iterator();
	 			    while(iterListValues.hasNext()){
	 		    		String strTemp=iterListValues.next();
	 		    		arrString[intCount]=strTemp.trim();
	 		    		intCount++;		
	 			    }
	 			    
	 				int i=0;
	 				arlSize=arlToken.size();
	 				for(String s : arrString) {
	 			          String[] s2 = s.split(" ");
	 			          for(String results : s2) {
	 			        	  if(i==0){
	 			        		 strResortMailId1=results;
	 			        	  }else if(i==1){
	 			        		 strResortMailId2=results;
	 			        	  }else if(i==2){
	 			        		 strResortMailId3=results;
	 			        	  }else if(i==3){
	 			        		 strResortMailId4=results;
	 			        	  }else if(i==4){
	 			        		 strResortMailId5=results;
	 			        	  }else if(i==5){
	 			        		  break;
	 			        	  }
	 			        	
	 			        	 i++;
	 			        	 
	 			        	
	 			          }
	 			      }
	 				
	 				if(strResortMailId2==null){
	 					strResortMailId2=strResortMailId1;
	 				}
	 				if(strResortMailId3==null){
	 					strResortMailId3=strResortMailId1;
	 				}
	 				if(strResortMailId4==null){
	 					strResortMailId4=strResortMailId1;
	 				}
	 				if(strResortMailId5==null){
	 					strResortMailId5=strResortMailId1;
	 				}
	 				
	 				
	 				rootMap.put("from", getText("notification.from"));
	 				Writer out = new StringWriter();
	 				template.process(rootMap, out);

	 				String strReservationMailId=null,strPropertyMailId=null,strResortMailId=null,strContractMailId=null,strRevenueMailId=null;
	 				PmsPropertyManager pmsPropertyController=new PmsPropertyManager();
	 				for (PmsBookedDetails booked : bookedList) {
	 					PmsProperty pmsProperty=pmsPropertyController.find(booked.getPropertyId());
	 					
	 					strReservationMailId=pmsProperty.getReservationManagerEmail();
	 					strResortMailId=pmsProperty.getResortManagerEmail();
	 					strPropertyMailId=pmsProperty.getPropertyEmail();
	 					strRevenueMailId=pmsProperty.getRevenueManagerEmail();
	 					strContractMailId=pmsProperty.getContractManagerEmail();
	 				}
	 				String strSubject="Hotel Confirmation for Booking Id "+getBookingId().toString()+" "+this.propertyName;
	 				//send email
	 				Email em = new Email();
	 				
	 				if(strPropertyMailId!=null && strResortMailId!=null && strContractMailId!=null){
	 					em.set_to(strPropertyMailId);
	 					em.set_cc(strContractMailId);
		 				em.set_cc2(getText("bookings.notification.email"));
		 				em.set_cc3(getText("operations.notification.email"));
		 				em.set_cc4(getText("finance.notification.email"));
		 				em.set_cc5(strResortMailId1);
		 				em.set_cc6(strResortMailId2);
		 				em.set_cc7(strResortMailId3);
		 				em.set_cc8(strResortMailId4);
		 				em.set_cc9(strResortMailId5);
		 				em.set_cc10(strContractMailId);
		 				em.set_cc11(strContractMailId);
		 				em.set_cc12(strContractMailId);
		 				em.set_cc13(strContractMailId);
		 				em.set_cc14(strContractMailId);
	 				}else{
	 					em.set_to(strPropertyMailId);
	 					em.set_cc(strContractMailId);
		 				em.set_cc2(getText("bookings.notification.email"));
		 				em.set_cc3(getText("operations.notification.email"));
		 				em.set_cc4(getText("finance.notification.email"));
		 				em.set_cc5(strResortMailId1);
		 				em.set_cc6(strResortMailId2);
		 				em.set_cc7(strResortMailId3);
		 				em.set_cc8(strResortMailId4);
		 				em.set_cc9(strResortMailId5);
		 				em.set_cc10(strContractMailId);
		 				em.set_cc11(strContractMailId);
		 				em.set_cc12(strContractMailId);
		 				em.set_cc13(strContractMailId);
		 				em.set_cc14(strContractMailId);
	 				}
	 				
	 				
	 				em.set_from(getText("email.from"));
	 				//em.set_host(getText("email.host"));
	 				//em.set_password(getText("email.password"));
	 				//em.set_port(getText("email.port"));
	 				em.set_username(getText("email.username"));
	 				em.set_subject(strSubject);
	 				em.set_bodyContent(out);
	 				em.send();		
	 				}
	 				catch(Exception e1)
	 				{
	 					e1.printStackTrace();
	 				}
	 		     
	            }
	            
	           // return null;
//				response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
				

			} catch (Exception e) {
				logger.error(e);
				e.printStackTrace();
			} finally {

			}

           return null;
		}
	 
	 public String getPartnerBookedDetails(){
			
		 try {
			String strFullName=""; 
			DecimalFormat df = new DecimalFormat("###.##");
			HttpServletResponse response = ServletActionContext.getResponse();
			DateFormat f = new SimpleDateFormat("EEEE");
			DateFormat f1 = new SimpleDateFormat("EEE");
		    SimpleDateFormat sdfformat=new SimpleDateFormat("MMM dd");
		    SimpleDateFormat sdfformat1=new SimpleDateFormat("MMM dd, yyyy");
		    PmsTagManager tagController=new PmsTagManager();
			PmsPropertyManager propertyController = new PmsPropertyManager();
			PropertyAccommodationManager propertyAccommodationController = new PropertyAccommodationManager();
			BookingDetailManager bookingDetailController = new BookingDetailManager();
			PmsGuestManager guestController = new PmsGuestManager();
			PropertyContractTypeManager contractController=new PropertyContractTypeManager();
			PmsSourceManager sourceController=new PmsSourceManager();
			StringBuilder accommodationTypes=new StringBuilder();
			response.setContentType("application/json");
			this.guests=0;this.adults=0;this.child=0;
			this.bookedList = bookingDetailController.bookedList(this.bookingId);
			
            for (PmsBookedDetails booked : bookedList) {
            	this.bookingId = getBookingId();
            	this.sourceId=booked.getSourceId();
            	PmsProperty pmsProperty = propertyController.find(booked.getPropertyId());
            	this.propertyName = pmsProperty.getPropertyName();//resort name and location name is missing
            	this.propertyId = pmsProperty.getPropertyId();
            	this.latitude = pmsProperty.getLatitude();
            	this.longitude = pmsProperty.getLongitude();
            	this.address1=pmsProperty.getAddress1();//property address1
            	this.address2=pmsProperty.getAddress2();//property address2
            	this.phoneNumber = pmsProperty.getPropertyContact();//property phone number
            	this.propertyEmail = pmsProperty.getPropertyEmail();
            	this.locationName=pmsProperty.getLocation().getLocationName();//property location
            	this.reservationManagerEmail=pmsProperty.getReservationManagerEmail();
            	this.resortManagerEmail=pmsProperty.getResortManagerEmail();
            	this.reservationManagerContact = pmsProperty.getReservationManagerContact();
            	if(pmsProperty.getLocationType()!=null){
            		this.locationType=pmsProperty.getLocationType().getLocationTypeName();	
            	}
            	
            	java.util.Date date=new java.util.Date();
            	Calendar calDate=Calendar.getInstance();
            	calDate.setTime(date);
            	calDate.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
            	int bookedHours=calDate.get(Calendar.HOUR);
            	int bookedMinute=calDate.get(Calendar.MINUTE);
            	int bookedSecond=calDate.get(Calendar.SECOND);
            	int AMPM=calDate.get(Calendar.AM_PM);
            	String strBookingDate=new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH).format(calDate.getTime());
            	String hours="",minutes="",seconds="";
            	if(bookedHours<10){
            		hours=String.valueOf(bookedHours);
            		hours="0"+hours;
            	}else{
            		hours=String.valueOf(bookedHours);
            	}
            	if(bookedMinute<10){
            		minutes=String.valueOf(bookedMinute);
            		minutes="0"+minutes;
            	}else{
            		minutes=String.valueOf(bookedMinute);
            	}
            	
            	if(bookedMinute<10){
            		seconds=String.valueOf(bookedSecond);
            		seconds="0"+seconds;
            	}else{
            		seconds=String.valueOf(bookedSecond);
            	}
            	
            	if(AMPM==0){//If the current time is AM
            		this.timeHours=strBookingDate+" "+hours+" : "+minutes+" : "+seconds+" AM ";
            	}else if(AMPM==1){//If the current time is PM
            		this.timeHours=strBookingDate+" "+hours+" : "+minutes+" : "+seconds+" PM ";
            	}
            	
            	if(this.sourceId==12){
         			this.sourceName = "Offline";
         		}else{
         			this.sourceName = "Online";
         		}
            	
            	/*if(this.sourceId!=null){
            		PmsSource source=sourceController.find(sourceId);
            		this.sourceType=source.getSourceName();
            	}*/
            	if(this.sourceName.equalsIgnoreCase("online")){
            		if(this.sourceId> 1 && this.sourceId!=12){
            			this.sourceType="OTA";
            		}else if(this.sourceId==12){
            			this.sourceType="Offline";
            		}else{
            			this.sourceType="Offline";
            		}
            	}else{
            		if(this.sourceId> 1 && this.sourceId!=12){
            			this.sourceType="OTA";
            		}else if(this.sourceId==12){
            			this.sourceType="Offline";
            		}else{
            			this.sourceType="Offline";
            		}
            	}
            	
            	this.arrivalDate = booked.getArrivalDate();
            	this.departureDate = booked.getDepartureDate();
            	PmsGuest guest = guestController.find(booked.getGuestId());
            	this.firstName = guest.getFirstName();
            	this.lastName = guest.getLastName();
            	this.phone = guest.getPhone();
            	this.emailId = guest.getEmailId();
            	this.specialRequest = booked.getSpecialRequest();
            	if(this.specialRequest==null){
            		this.specialRequest="Not Available";
            	}
            	
            	this.promotionFirstName=booked.getPromotionFirstName()==null?"":booked.getPromotionFirstName();
            	this.promotionSecondName=booked.getPromotionSecondName()==null?"":booked.getPromotionSecondName();
            	this.couponName=booked.getCouponName()==null?"":booked.getCouponName();
            	this.promotionAmount=booked.getPromotionAmount();
            	this.couponAmount=booked.getCouponAmount();;
            	
            	SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
	  			Timestamp FromDate=booked.getArrivalDate();
	  			Timestamp ToDate=booked.getDepartureDate();
	  			String strFromDate=format2.format(FromDate);
	  			String strToDate=format2.format(ToDate);
	  	    	DateTime startdate = DateTime.parse(strFromDate);
	  	        DateTime enddate = DateTime.parse(strToDate);
	  	        
	  	        java.util.Date fromdate = startdate.toDate();
	  	 		java.util.Date todate = enddate.toDate();
	  	 		 
	  			
            	this.arrivalDate = booked.getArrivalDate();
            	this.departureDate = booked.getDepartureDate();
            	if(booked.getTagId()!=null){
	  				PmsTags tags=tagController.find(booked.getTagId());
	  				this.tagName=tags.getTagName();
	  			}else{
	  				this.tagName="Not Applicable";
	  			}
            	
            	Calendar calBookingDate=Calendar.getInstance();
            	calBookingDate.setTime(booked.getCreatedDate());
            	calBookingDate.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
            	int bookedHours1=calBookingDate.get(Calendar.HOUR);
            	int bookedMinute1=calBookingDate.get(Calendar.MINUTE);
            	int bookedSecond1=calBookingDate.get(Calendar.SECOND);
            	int AMPM1=calBookingDate.get(Calendar.AM_PM);
            	
            	String hours1="",minutes1="",seconds1="";
            	if(bookedHours1<10){
            		hours1=String.valueOf(bookedHours1);
            		hours1="0"+hours1;
            	}else{
            		hours1=String.valueOf(bookedHours1);
            	}
            	if(bookedMinute1<10){
            		minutes1=String.valueOf(bookedMinute1);
            		minutes1="0"+minutes1;
            	}else{
            		minutes1=String.valueOf(bookedMinute1);
            	}
            	
            	if(bookedSecond1<10){
            		seconds1=String.valueOf(bookedSecond1);
            		seconds1="0"+seconds1;
            	}else{
            		seconds1=String.valueOf(bookedSecond1);
            	}
            	if(AMPM1==0){//If the current time is AM
            		this.bookingTime=hours1+" : "+minutes1+" : "+seconds1+" AM ";
            	}else if(AMPM1==1){//If the current time is PM
            		this.bookingTime=hours1+" : "+minutes1+" : "+seconds1+" PM ";
            	}
            	this.tscheckInTime=booked.getCheckInTime();
            	this.tscheckOutTime=booked.getCheckOutTime();
            	
            	if(tscheckInTime!=null && tscheckOutTime!=null){
            		Calendar calCheckIn=Calendar.getInstance();
            		calCheckIn.setTime(this.tscheckInTime);
//            		calCheckIn.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
                	int bookedHours2=calCheckIn.get(Calendar.HOUR);
                	int bookedMinute2=calCheckIn.get(Calendar.MINUTE);
                	int bookedSecond2=calCheckIn.get(Calendar.SECOND);
                	int AMPM2=calCheckIn.get(Calendar.AM_PM);
                	
                	Calendar calCheckOut=Calendar.getInstance();
                	calCheckOut.setTime(this.tscheckOutTime);
//                	calCheckOut.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
                	int bookedHours3=calCheckOut.get(Calendar.HOUR);
                	int bookedMinute3=calCheckOut.get(Calendar.MINUTE);
                	int bookedSecond3=calCheckOut.get(Calendar.SECOND);
                	int AMPM3=calCheckOut.get(Calendar.AM_PM);
                	
                	String strFromTime=format2.format(tscheckInTime);
    	  			String strToTime=format2.format(tscheckOutTime);
    	  	    	DateTime starttime = DateTime.parse(strFromTime);
    	  	        DateTime endtime = DateTime.parse(strToTime);
    	  	        
    	  	        java.util.Date fromtime = starttime.toDate();
    	  	 		java.util.Date totime = endtime.toDate();
    	  	 		
    	  	 		if(AMPM2==0){
    	  	 			if(bookedHours2<10){
    	  	 				if(bookedHours2==0){
    	  	 					this.bookingInTime="12:"+"00AM";
    	  	 				}else{
    	  	 					this.bookingInTime="0"+bookedHours2+":"+"00AM";	
    	  	 				}
                    		
                    	}else{
                    		this.bookingInTime=bookedHours2+":"+"00AM";
                    	}
    	  	 				
                	}else if(AMPM2==1){
                		if(bookedHours2<10){
                			if(bookedHours2==0){
                				this.bookingInTime="12:"+"00PM";	
    	  	 				}else{
    	  	 					this.bookingInTime="0"+bookedHours2+":"+"00PM";
    	  	 				}
                			
                    	}else{
                    		this.bookingInTime=bookedHours2+":"+"00PM";
                    	}
                		
                	}
    	  	 		if(AMPM3==0){
    	  	 			if(bookedHours3<10){
    	  	 				if(bookedHours3==0){
    	  	 					this.bookingOutTime="12:"+"00AM";
    	  	 				}else{
    	  	 					this.bookingOutTime="0"+bookedHours3+":"+"00AM";		
    	  	 				}
                    		
                    	}else{
                    		this.bookingOutTime=bookedHours3+":"+"00AM";
                    	}
    	  	 				
    	  	 		}else if(AMPM3==1){
    	  	 			if(bookedHours3<10){
    	  	 				if(bookedHours3==0){
    	  	 					this.bookingOutTime="12:"+"00PM";	
    	  	 				}else{
    	  	 					this.bookingOutTime="0"+bookedHours3+":"+"00PM";
    	  	 				}
                    		
                    	}else{
                    		this.bookingOutTime=bookedHours3+":"+"00PM";
                    	}
    	  	 			
    	  	 		}
    	  	 		
    	  	 		long lngHours=tscheckOutTime.getTime()-tscheckInTime.getTime();
                	long diffHours = lngHours / (60 * 60 * 1000) % 24;
                	
                	this.checkIn=sdfformat.format(tscheckInTime);
                	this.checkInDays=f.format(fromtime);
                	this.checkOut=sdfformat.format(tscheckOutTime);
                	this.checkOutDays=f.format(totime);
                	
                	this.diffInHours=(int)diffHours;
                	this.voucherType="hourly-wise";
            	}else{
            		this.bookingInTime="00"+":"+"00";
    	  	 		this.bookingOutTime="00"+":"+"00";
            		this.checkIn=sdfformat.format(FromDate);
                	this.checkInDays=f.format(fromdate);
                	this.checkOut=sdfformat.format(ToDate);
                	this.checkOutDays=f.format(todate);
                	this.voucherType="day-wise";
            	}
            	
            	this.bookingDate=sdfformat1.format(booked.getCreatedDate());
            	
            	
            	
            	String checkIn = new SimpleDateFormat("MM/dd/yyyy").format(this.arrivalDate);
				String checkOut = new SimpleDateFormat("MM/dd/yyyy").format(this.departureDate);
				
				LocalDate date1 = LocalDate.parse(checkIn, DateTimeFormat.forPattern("MM/dd/yyyy"));
                LocalDate date2 = LocalDate.parse(checkOut, DateTimeFormat.forPattern("MM/dd/yyyy"));
                this.days = Days.daysBetween(date1, date2).getDays();
                this.accommodationId=booked.getAccommodationId();
              this.bookList =  bookingDetailController.listAccommodation(getBookingId(),booked.getAccommodationId());
      	
      		for (BookedDetail bookedDetail : bookList) {
            		
	            	PropertyAccommodation accommodation = propertyAccommodationController.find(bookedDetail.getAccommodationId());
	            	this.roomCount=bookedDetail.getRoomCount()/this.days;
	            	accommodationTypes.append(this.roomCount+" "+accommodation.getAccommodationType().trim());
	            	accommodationTypes.append(",");
	            	this.extraAdultRate=0.0;
	            	this.extraChildRate=0.0;
	            	int roomNight=(int)this.roomCount*this.days;
	            	if(booked.getExtraAdultRate()==null || booked.getExtraAdultRate()==0.0){
	                	this.extraAdultRate=0.0;	
	                }else{
	                	this.extraAdultRate=accommodation.getExtraAdult()*roomNight;
	                }
	                
	                
	                if(booked.getExtraChildRate()==null || booked.getExtraChildRate()==0.0){
	                	this.extraChildRate=0.0;	
	                }else{
	                	this.extraChildRate=accommodation.getExtraChild()*roomNight;
	                }
	                
	            	this.extraCharges+=this.extraAdultRate+this.extraChildRate;
	            	if(accommodation.getPropertyContractType()!=null){
	            		this.contractTypeId=accommodation.getPropertyContractType().getContractTypeId();
		            	PropertyContractType contracts=contractController.find(this.contractTypeId);
		            	this.contractType=contracts.getContractTypeName();	
	            	}else{
	            		this.contractTypeId=9;
		            	PropertyContractType contracts=contractController.find(this.contractTypeId);
		            	this.contractType=contracts.getContractTypeName();
	            	}
	            	
            		if(this.locationType.equalsIgnoreCase("metro")){
            			if(this.contractTypeId==7){
    	                	this.contractModel="Join";
    	            	}
            			
            			if(this.contractTypeId==6){
    	                	this.contractModel="Sure";
            			}
    	            	
    	            	if(this.contractTypeId==9){
    	                	this.contractModel="Flat";
    	            	}	
            		}
            		
            		if(this.locationType.equalsIgnoreCase("leisure")){

                    	String days=f.format(this.arrivalDate);
                        String[] weekdays={"Monday","Tuesday","Wednesday","Thursday","Sunday"};
            		    String[] weekend={"Friday","Saturday"};
            		    boolean isWeekdays=false,isWeekend=false;
        				for(int a=0;a<weekdays.length;a++){
        					if(days.equalsIgnoreCase(weekdays[a])){
        						isWeekdays=true;
        					}
        				}
        				
        				for(int b=0;b<weekend.length;b++){
        					if(days.equalsIgnoreCase(weekend[b])){
        						isWeekend=true;
        					}
        				}
        				
        				if(isWeekdays){
        					if(contractTypeId==7){//B-join
        	                	this.contractModel="Join";
                        	}else if(contractTypeId==6){//B-sure
        	                	this.contractModel="Sure";
                        	}else if(contractTypeId==8){//B-stay
        	                	this.contractModel="Stay";
                        	}else if(contractTypeId==9){//B-join flat
        	                	this.contractModel="Flat";
                        	}
        				}
        				
        				if(isWeekend){
        					if(contractTypeId==7){//B-join
        	                	this.contractModel="Join";
                        	}else if(contractTypeId==6){//B-sure
        	                	this.contractModel="Sure";
                        	}else if(contractTypeId==8){//B-stay
        	                	this.contractModel="Stay";
                        	}else if(contractTypeId==9){//B-join flat
        	                	this.contractModel="Flat";
                        	}
        				}
                    
            		}
	            	
         	 }
            	this.rooms = booked.getRooms();
            	if(this.rooms==1){
            		this.roomType=this.rooms+" room";
            	}
            	if(this.rooms>1){
            		this.roomType=this.rooms+" rooms";
            	}
            	if(this.rooms>1 && this.days>1){
            		this.roomNightsType=this.rooms+" Rooms X "+this.days+" Nights";
            	}else if(this.rooms>1 && this.days==1){
            		this.roomNightsType=this.rooms+" Rooms X "+this.days+" Night";
            	}else if(this.rooms==1 && this.days>1){
            		this.roomNightsType=this.rooms+" Room X "+this.days+" Nights";
            	}else if(this.rooms==1 && this.days==1){
            		this.roomNightsType=this.rooms+" Room X "+this.days+" Night";
            	}else{
            		this.roomNightsType=this.rooms+" Room X "+this.days+" Night";
            	}
            	
            	if(this.days>1){
            		this.roomNights=this.days+" Nights";
            	}else{
            		this.roomNights=this.days+" Night";
            	}
            	this.adults += booked.getAdults()/this.days;
            	this.child += booked.getChild()/this.days;
            	this.amount=booked.getAmount();
            	this.tax=booked.getTax();
            	this.promotionAmount=booked.getPromotionAmount();
            	this.couponAmount=booked.getCouponAmount();
            	if(this.promotionAmount==null){
            		this.promotionAmount=0.0;
            	}else{
            		this.promotionAmount=booked.getPromotionAmount();
            	}
            	if(this.couponAmount==null){
            		this.couponAmount=0.0;	
            	}else{
            		this.couponAmount=booked.getCouponAmount();
            	}
            	
            	
            	this.guests=(int)this.adults+(int)this.child;
//            	this.totalAmount = this.tax + this.amount-discountAmount;
           
            	this.purchaseRate=booked.getPurchaseRate();
            	
                PropertyAccommodation accommodations = propertyAccommodationController.find(this.accommodationId);
                this.taxType=accommodations.getTaxType();
                double taxPercent=0;
                double dayPerTariff=0;
                double roomdays=0;
                double purchasePerDay=0;
                purchasePerDay=this.purchaseRate/this.days;
                roomdays=this.days*this.roomCount;
                dayPerTariff=this.purchaseRate/roomdays;
                if(dayPerTariff<1000){
            		taxPercent=0;
            	}else if(dayPerTariff>=1000 && dayPerTariff<=2500){
            		taxPercent=12;
            	}else if(dayPerTariff>2500 && dayPerTariff<=7500){
            		taxPercent=18;
            	}else if(dayPerTariff>7500){
            		taxPercent=28;
            	}
                for(int d=0;d<this.days;d++){
                	
                	if(this.taxType==1){//Inclusive
                    	this.partnerTax+=purchasePerDay*taxPercent/100;
                    	this.partnerTariff+=purchasePerDay-this.partnerTax;
                    	this.partnerTotal=this.partnerTariff+this.partnerTax;
                    }else if(this.taxType==2){//Exclusive
                    	this.partnerTax+=purchasePerDay*taxPercent/100;
                    	this.partnerTariff+=purchasePerDay;
                    	this.partnerTotal=this.partnerTariff+this.partnerTax;
                    }	
                }
                
//            	this.partnerTotal= this.purchaseRate;
            	
//            	String strTotal=df.format(this.totalAmount);
//            	this.totalAmount=Double.valueOf(strTotal);
            	this.tariffAmount=this.actualAmount-this.couponAmount-this.promotionAmount;
                /*if(booked.getExtraAdultRate()==null || booked.getExtraAdultRate()==0.0){
                	this.extraAdultRate=0.0;	
                }else{
                	this.extraAdultRate=booked.getExtraAdultRate();
                }
                
                
                if(booked.getExtraChildRate()==null || booked.getExtraChildRate()==0.0){
                	this.extraChildRate=0.0;	
                }else{
                	this.extraChildRate=booked.getExtraChildRate();
                }
                this.extraCharges=this.extraAdultRate+this.extraChildRate;
                */
                
                if(booked.getAddonAmount()==null){
                	this.addonAmount=0.0;
                }else{
                	this.addonAmount=booked.getAddonAmount();
                }
                this.totalAdvanceAmount = booked.getAdvanceAmount();
                
//                OTA Share
                if(booked.getOtaCommission()!=null && booked.getOtaTax()!=null){
                	this.dblOtaCommission=booked.getOtaCommission();
                    this.dblOtaTax=booked.getOtaTax();	
                }else{
                	this.dblOtaCommission=0;
                    this.dblOtaTax=0;
                }
                
				
                this.dblTotalOtaAmout=this.dblOtaCommission+this.dblOtaTax;
				
                this.dblRevenueAmount=this.amount-this.dblTotalOtaAmout;
                this.dblTotalRevenueTaxAmount=this.dblRevenueAmount+this.tax;
				
//				ULO Share
                this.dblCommission=20;
                this.dblUloCommission=this.dblRevenueAmount*this.dblCommission/100;
				
				
                this.dblUloTaxAmount=this.dblUloCommission*18/100;
				
                this.dblTotalUloCommission=this.dblUloCommission+this.dblUloTaxAmount; //Payable to Ulo
				
				
                this.dblHotelPayable=this.dblRevenueAmount-this.dblTotalUloCommission;
				
                this.dblHotelTaxPayable=this.dblHotelPayable+this.tax; //Payable to Hotelier
                if(this.sourceType.equalsIgnoreCase("OTA")){
                	this.totalAmount=this.amount+this.tax;
                }else{
                	this.totalAmount=this.amount+this.tax+this.extraCharges;	
                }
             this.hotelPay = (this.totalAmount-this.totalAdvanceAmount);
            /*if(this.purchaseRate<this.totalAmount){
     			this.hotelPay = (this.totalAmount-this.totalAdvanceAmount);	
     		}else{
     			this.hotelPay = 0;	
     		}*/
//             this.dbltotalhotelrevenue=this.partnerTotal-this.hotelPay;
            	if(this.totalAmount == booked.getAdvanceAmount()){
            		this.paymentStatus = "Paid";
            	}else if(this.totalAdvanceAmount==0){
            		this.paymentStatus = "Hotel Pay";
            	}else{
            		this.paymentStatus = "Partially Paid";
            	}

			}
            
            String strFirstName="",firstNameLetterUpperCase="";
			String strLastName="",lastNameLetterUpperCase="";
			strFirstName=this.firstName;
			strLastName=this.lastName;
			if(strFirstName!=null && strFirstName!="" && !strFirstName.equalsIgnoreCase("undefined")){
				strFirstName=strFirstName.trim();
				firstNameLetterUpperCase=strFirstName.substring(0, 1).toUpperCase()+strFirstName.substring(1);
				strFirstName=firstNameLetterUpperCase;
			}else{
				strFirstName="Not Available";
			}
			if(strLastName!=null && strLastName!=""  && !strLastName.equalsIgnoreCase("undefined")){
				strLastName=strLastName.trim();
				lastNameLetterUpperCase=strLastName.substring(0, 1).toUpperCase() +  strLastName.substring(1);
				strLastName=lastNameLetterUpperCase;
			}else{
				strLastName="";
			}
			strFullName=strFirstName+" "+strLastName;
			 				
			
            if(this.contractModel.equalsIgnoreCase("Join")){
            	this.rateModel="Net Rate";
            }else{
            	this.rateModel="Net Rate";
            }
            PropertyTaxeManager propertytaxController = new PropertyTaxeManager();
				 double taxe =  this.dblHotelPayable/this.days;
				
			 PropertyTaxe tax = propertytaxController.find(taxe);
			 this.dbltaxes = Math.round(this.dblHotelPayable * tax.getTaxPercentage() / 100  ) ;
			 this.dblHotelTaxPayable=this.dblHotelPayable+this.dbltaxes; //Payable to Hotelier
		
        
            
            String strContractMailId=null,strRevenueMailId=null;
				PmsPropertyManager pmsPropertyController=new PmsPropertyManager();
				for (PmsBookedDetails booked : bookedList) {
					PmsProperty pmsProperty=pmsPropertyController.find(booked.getPropertyId());
					
					this.reservationManagerEmail=pmsProperty.getReservationManagerEmail();
					this.propertyEmail=pmsProperty.getPropertyEmail();
					strRevenueMailId=pmsProperty.getRevenueManagerEmail();
					strContractMailId=pmsProperty.getContractManagerEmail();
					this.propertyUrl=pmsProperty.getPropertyUrl();
 					if(pmsProperty.getGoogleLocation()!=null){
 						this.locationId=pmsProperty.getGoogleLocation().getGoogleLocationId();
 	 					this.locationUrl=pmsProperty.getGoogleLocation().getGoogleLocationUrl();
 	 					
 	 					this.galleryPath=this.locationUrl+"/"+this.propertyUrl+"?tripTypeId=1";	
 					}else{
 						this.locationId=pmsProperty.getGoogleLocation().getGoogleLocationId();
 	 					this.locationUrl=pmsProperty.getGoogleLocation().getGoogleLocationUrl();
 	 					
 	 					this.galleryPath=this.propertyUrl;
 					}
				}
				
				ArrayList<String> arlEmail=new ArrayList<String>();
				
				StringTokenizer stoken2=new StringTokenizer(this.propertyEmail,",");
				while(stoken2.hasMoreElements()){
					arlEmail.add(stoken2.nextToken());
				}
				/*StringTokenizer stoken3=new StringTokenizer(strRevenueMailId,",");
				while(stoken3.hasMoreElements()){
					arlEmail.add(stoken3.nextToken());
				}
				*/
			
				StringTokenizer stoken4=new StringTokenizer(strContractMailId,",");
				while(stoken4.hasMoreElements()){
					arlEmail.add(stoken4.nextToken());
				}
				arlEmail.add(getText("bookings.notification.email"));
				arlEmail.add(getText("operations.notification.email"));
				arlEmail.add(getText("finance.notification.email"));
				Integer emailSize=arlEmail.size();
				
 				//email template
 				Configuration cfg = new Configuration();
 				cfg.setClassForTemplateLoading(BookingDetailAction.class, "../../../");
 				Template template =null;
 				if(this.contractModel.equalsIgnoreCase("Flat")){
 					if(this.voucherType.equalsIgnoreCase("hourly-wise")){
						template = cfg.getTemplate(getText("partner.flat.booking.hourly.voucher.template"));	
	 				}else{
	 					template = cfg.getTemplate(getText("partner.flat.booking.voucher.template"));
	 				}
 				}else{
 					if(this.voucherType.equalsIgnoreCase("hourly-wise")){
						template = cfg.getTemplate(getText("partner.booking.hourly.voucher.template"));	
	 				}else{
	 					template = cfg.getTemplate(getText("partner.booking.voucher.template"));
	 				}
 					
 				}
 				
 				Map<String, Object> rootMap = new HashMap<String, Object>();
 				//Map<String, String> rootMap = new HashMap<String, String>();	 				
 				rootMap.put("bookingId",getBookingId().toString());
 				rootMap.put("propertyName",this.propertyName);
 				rootMap.put("propertyid",this.propertyId);
 				rootMap.put("latitude",this.latitude.trim());
 				rootMap.put("longitude",this.longitude.trim());
 				rootMap.put("sourceName",this.sourceName);
 				rootMap.put("paymentStatus",this.paymentStatus);
 				rootMap.put("reservationEmail",this.reservationManagerEmail);
 				rootMap.put("reservationContact",this.reservationManagerContact);
 				rootMap.put("propertyEmail",this.propertyEmail);
 				rootMap.put("specialrequest",this.specialRequest);
 				rootMap.put("locationName",this.locationName);
 				rootMap.put("address1",this.address1);
 				rootMap.put("address2",this.address2);
 				rootMap.put("hotelpay",this.hotelPay);
 				rootMap.put("phoneNumber",this.phoneNumber);
 				rootMap.put("checkIn", this.checkIn);
 				rootMap.put("checkInDays", this.checkInDays);
 				rootMap.put("checkOut",this.checkOut);
 				rootMap.put("checkOutDays",this.checkOutDays);
 				rootMap.put("timeHours",this.timeHours);
 				rootMap.put("bookingTime", this.bookingTime);
 				rootMap.put("hours", this.diffInHours);
				rootMap.put("roomNights", this.roomNights);
 				rootMap.put("propertyUrl", this.galleryPath);
 				rootMap.put("bookingInTime",this.bookingInTime);
 				rootMap.put("bookingOutTime",this.bookingOutTime);
 								
 				rootMap.put("guestName",strFullName);
 				rootMap.put("mobile",this.phone);
 				rootMap.put("emailId",this.emailId);	 				
 				rootMap.put("days",this.days.toString());
 				rootMap.put("roomnighttype", this.roomNightsType);
 				
 				
 				rootMap.put("accommodationType", accommodationTypes);
 				rootMap.put("rateModel", this.rateModel);
 				rootMap.put("adults", String.valueOf(this.adults));
 				rootMap.put("child",String.valueOf(this.child));
 				rootMap.put("guests", this.guests);
 				rootMap.put("roomType", this.roomType);
 				rootMap.put("tagName", this.tagName);
 				rootMap.put("tax",df.format(this.tax));
 				rootMap.put("amount",df.format(this.dblRevenueAmount));
 				rootMap.put("totalamount",df.format(this.dblTotalRevenueTaxAmount));
 				rootMap.put("partnerTax",df.format(this.partnerTax));
 				rootMap.put("partnerTariff",df.format(this.partnerTariff));
 				rootMap.put("partnerTotal",df.format(this.partnerTotal));
 				rootMap.put("totalAdvanceAmount",df.format(this.totalAdvanceAmount));
 				rootMap.put("bookingDate", this.bookingDate);
 				rootMap.put("otaCommission",df.format(this.dblOtaCommission));
 				rootMap.put("otaTax",df.format(this.dblOtaTax));
 				rootMap.put("totalOtaRevenue",df.format(this.dblTotalOtaAmout));
 				rootMap.put("totalRevenueAmount",df.format(this.dblRevenueAmount));
 				rootMap.put("uloCommission",df.format(this.dblUloCommission));
 				rootMap.put("uloTax",df.format(this.dblUloTaxAmount));
 				rootMap.put("totalUloRevenue",df.format(this.dblTotalUloCommission));
 				rootMap.put("hotelPayable",df.format(this.dblHotelPayable));
 				rootMap.put("totalhotelrevenue",this.dbltotalhotelrevenue);
 				rootMap.put("totalHotelRevenue",df.format(this.dblHotelTaxPayable));
 				rootMap.put("hotelTariff",df.format(this.dblHotelPayable));
 				rootMap.put("hotelTax",df.format(this.dbltaxes));
 				rootMap.put("tariffamount",df.format(this.tariffAmount));
 				rootMap.put("extracharges",df.format(this.extraCharges));
 				
 				
 				rootMap.put("from", getText("notification.from"));
 				Writer out = new StringWriter();
 				template.process(rootMap, out);
 				
 				String strSubject=null;
 				if(this.mailSubjectFlag){
 					strSubject="Booking Modification for Booking Id "+getBookingId().toString()+" "+this.propertyName;
 				}else{
 					strSubject="Hotel Confirmation for Booking Id "+getBookingId().toString()+" "+this.propertyName;
 				}
 				
 				//send email
 				Email em = new Email();
 				int i=0;
 				String emailArray[]=new String[emailSize];
 				Iterator<String> iterEmail=arlEmail.iterator();
 				while(iterEmail.hasNext()){
 					emailArray[i]=iterEmail.next().trim();
 					i++;
 				}

 				/*Writer fileWriter = new FileWriter (new File("E:/partner-booking-template-input.xhtml"));
 	            template.process(rootMap, fileWriter);
 	            fileWriter.flush();
 	            fileWriter.close();
 	            String inputFile="E:/partner-booking-template-input.xhtml";
 	            String outputFile="E:/partner-booking-template-output.html";
 	            File file = new File(inputFile);
 	            FileOutputStream fos = null;
 	            FileInputStream fis = null;
 	            FileWriter outputStream = null;
 	            
 	            fos = new FileOutputStream(outputFile);
 	            fis = new FileInputStream(file);
 	            outputStream = new FileWriter(outputFile);
 	            Tidy tidy = new Tidy(); //HTML parser and pretty printer.
 	            tidy.setXHTML(true); //true if tidy should output XHTML
 	            
 	            tidy.parse(fis, fos);
 				//out.flush();
 				createPdf(out.toString(), getText("storage.pdf.booking.partner.path"));
 				em.set_fileName(getText("storage.pdf.booking.partner.path"));
 				*/
 				em.set_from(getText("email.from"));
 				em.set_username(getText("email.username"));
 				em.set_subject(strSubject);
 				em.set_bodyContent(out);
 				em.set_emailSize(emailSize);
 				em.setEmailArray(emailArray);
 				em.sendBookingVoucher();		
 				
 				
 				
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

       return null;
	
	 }
	 public void checkedPaymentStatus(String invoiceId) throws IOException {
		 try{
			 PmsBookingManager pmsBookingController = new PmsBookingManager();
			 BookingDetailManager bookingDetailController = new BookingDetailManager();
			 PmsBooking booking = pmsBookingController.findGuestInvoiceId(invoiceId);
			
			 if(booking != null){
//				 System.out.println("booking id-----------"+booking.getBookingId());
				 
				 PmsBooking bookings = pmsBookingController.find(booking.getBookingId());
				 bookings.setIsActive(true);
				 bookings.setIsDeleted(false);
				 pmsBookingController.edit(bookings);
				 this.bookingDetailList = bookingDetailController.findId(booking.getBookingId());
				 for (BookingDetail bookingDetail : bookingDetailList)
				 {			
					BookingDetail detail = bookingDetailController.find(bookingDetail.getBookingDetailsId());
					detail.setIsActive(true);
					detail.setIsDeleted(false);
					bookingDetailController.edit(detail);
				 }
				 getOfflineGuestBookedDetails(booking.getBookingId());
			 }
 		 }catch (Exception e) {
				logger.error(e);
				e.printStackTrace();
			} finally {

			}
		 
		//return null;
		 
	 }
	 
	 public void  getOfflineHotelierGuestBookedDetails(int bookingId) throws IOException {
			
		 try {
			String strFullName=""; 
			DecimalFormat df = new DecimalFormat("###.##");
			//HttpServletResponse response = ServletActionContext.getResponse();
			DateFormat f = new SimpleDateFormat("EEEE");
			DateFormat f1 = new SimpleDateFormat("EEE");
		    SimpleDateFormat sdfformat=new SimpleDateFormat("MMM dd");
		    SimpleDateFormat sdfformat1=new SimpleDateFormat("MMM dd, yyyy");
		    PmsTagManager tagController=new PmsTagManager();
			PmsPropertyManager propertyController = new PmsPropertyManager();
			PropertyAccommodationManager propertyAccommodationController = new PropertyAccommodationManager();
			BookingDetailManager bookingDetailController = new BookingDetailManager();
			PmsGuestManager guestController = new PmsGuestManager();
			PropertyContractTypeManager contractController=new PropertyContractTypeManager();
			PmsSourceManager sourceController=new PmsSourceManager();
			StringBuilder accommodationTypes=new StringBuilder();
			//response.setContentType("application/json");
			this.guests=0;this.adults=0;this.child=0;
			this.bookedList = bookingDetailController.bookedList(bookingId);
			
         for (PmsBookedDetails booked : bookedList) {
         	this.bookingId = getBookingId();
         	this.sourceId=booked.getSourceId();
         	this.accommodationId = booked.getAccommodationId();
         	PmsProperty pmsProperty = propertyController.find(booked.getPropertyId());
         	this.propertyName = pmsProperty.getPropertyName();//resort name and location name is missing
         	this.propertyId = pmsProperty.getPropertyId();
         	this.latitude = pmsProperty.getLatitude();
         	this.longitude = pmsProperty.getLongitude();
         	this.address1=pmsProperty.getAddress1();//property address1
         	this.address2=pmsProperty.getAddress2();//property address2
         	this.phoneNumber = pmsProperty.getPropertyContact();//property phone number
         	this.propertyEmail = pmsProperty.getPropertyEmail();
         	this.locationName=pmsProperty.getLocation().getLocationName();//property location
         	this.reservationManagerEmail=pmsProperty.getReservationManagerEmail();
         	this.resortManagerEmail=pmsProperty.getResortManagerEmail();
         	this.reservationManagerContact = pmsProperty.getReservationManagerContact();
         	if(pmsProperty.getLocationType()!=null){
         		this.locationType=pmsProperty.getLocationType().getLocationTypeName();	
         	}
         	
         	java.util.Date date=new java.util.Date();
         	Calendar calDate=Calendar.getInstance();
         	calDate.setTime(date);
         	calDate.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
         	int bookedHours=calDate.get(Calendar.HOUR);
         	int bookedMinute=calDate.get(Calendar.MINUTE);
         	int bookedSecond=calDate.get(Calendar.SECOND);
         	int AMPM=calDate.get(Calendar.AM_PM);
         	String strBookingDate=new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH).format(calDate.getTime());
         	String hours="",minutes="",seconds="";
         	if(bookedHours<10){
         		hours=String.valueOf(bookedHours);
         		hours="0"+hours;
         	}else{
         		hours=String.valueOf(bookedHours);
         	}
         	if(bookedMinute<10){
         		minutes=String.valueOf(bookedMinute);
         		minutes="0"+minutes;
         	}else{
         		minutes=String.valueOf(bookedMinute);
         	}
         	
         	if(bookedMinute<10){
         		seconds=String.valueOf(bookedSecond);
         		seconds="0"+seconds;
         	}else{
         		seconds=String.valueOf(bookedSecond);
         	}
         	
         	if(AMPM==0){//If the current time is AM
         		this.timeHours=strBookingDate+" "+hours+" : "+minutes+" : "+seconds+" AM ";
         	}else if(AMPM==1){//If the current time is PM
         		this.timeHours=strBookingDate+" "+hours+" : "+minutes+" : "+seconds+" PM ";
         	}
         	if(this.mailSource == true){
         		if(this.sourceId==12){
         			this.sourceName = "offline";
         		}else{
         			this.sourceName = "resort";
         		}
         	}else{
         		if(this.sourceId==12){
         			this.sourceName = "offline";
         		}else{
         			this.sourceName = "online";
         		}
         		
         	}
         	
         	/*if(this.sourceId!=null){
        		PmsSource source=sourceController.find(sourceId);
        		this.sourceType=source.getSourceName();
        	}*/
             
         	if(this.sourceName.equalsIgnoreCase("online")){
        		if(this.sourceId> 1 && this.sourceId!=12){
        			this.sourceType="OTA";
        		}else if(this.sourceId==12){
        			this.sourceType="Offline";
        		}else{
        			this.sourceType="Offline";
        		}
        	}else{
        		if(this.sourceId> 1 && this.sourceId!=12){
        			this.sourceType="OTA";
        		}else if(this.sourceId==12){
        			this.sourceType="Offline";
        		}else{
        			this.sourceType="Offline";
        		}
        	}
         	
         	
         	this.arrivalDate = booked.getArrivalDate();
         	this.departureDate = booked.getDepartureDate();
         	PmsGuest guest = guestController.find(booked.getGuestId());
         	this.firstName = guest.getFirstName();
         	this.lastName = guest.getLastName();
         	this.phone = guest.getPhone();
         	this.emailId = guest.getEmailId();
         	this.specialRequest = booked.getSpecialRequest();
        	if(this.specialRequest==null){
        		this.specialRequest="Not Available";
        	}
         	
         	this.promotionFirstName=booked.getPromotionFirstName()==null?"":booked.getPromotionFirstName();
         	this.promotionSecondName=booked.getPromotionSecondName()==null?"":booked.getPromotionSecondName();
         	this.couponName=booked.getCouponName()==null?"":booked.getCouponName();
         	this.promotionAmount=booked.getPromotionAmount();
         	this.couponAmount=booked.getCouponAmount();;
         	
         	SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
	  			Timestamp FromDate=booked.getArrivalDate();
	  			Timestamp ToDate=booked.getDepartureDate();
	  			String strFromDate=format2.format(FromDate);
	  			String strToDate=format2.format(ToDate);
	  	    	DateTime startdate = DateTime.parse(strFromDate);
	  	        DateTime enddate = DateTime.parse(strToDate);
	  	        
	  	        java.util.Date fromdate = startdate.toDate();
	  	 		java.util.Date todate = enddate.toDate();
	  	 		 
	  			
         	this.arrivalDate = booked.getArrivalDate();
         	this.departureDate = booked.getDepartureDate();
         	if(booked.getTagId()!=null){
	  				PmsTags tags=tagController.find(booked.getTagId());
	  				this.tagName=tags.getTagName();
	  			}else{
	  				this.tagName="Not Applicable";
	  			}
         	
         	Calendar calBookingDate=Calendar.getInstance();
         	calBookingDate.setTime(booked.getCreatedDate());
         	calBookingDate.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
         	int bookedHours1=calBookingDate.get(Calendar.HOUR);
         	int bookedMinute1=calBookingDate.get(Calendar.MINUTE);
         	int bookedSecond1=calBookingDate.get(Calendar.SECOND);
         	int AMPM1=calBookingDate.get(Calendar.AM_PM);
         	
         	String hours1="",minutes1="",seconds1="";
         	if(bookedHours1<10){
         		hours1=String.valueOf(bookedHours1);
         		hours1="0"+hours1;
         	}else{
         		hours1=String.valueOf(bookedHours1);
         	}
         	if(bookedMinute1<10){
         		minutes1=String.valueOf(bookedMinute1);
         		minutes1="0"+minutes1;
         	}else{
         		minutes1=String.valueOf(bookedMinute1);
         	}
         	
         	if(bookedSecond1<10){
         		seconds1=String.valueOf(bookedSecond1);
         		seconds1="0"+seconds1;
         	}else{
         		seconds1=String.valueOf(bookedSecond1);
         	}
         	if(AMPM1==0){//If the current time is AM
         		this.bookingTime=hours1+" : "+minutes1+" : "+seconds1+" AM ";
         	}else if(AMPM1==1){//If the current time is PM
         		this.bookingTime=hours1+" : "+minutes1+" : "+seconds1+" PM ";
         	}
         	
         	this.tscheckInTime=booked.getCheckInTime();
        	this.tscheckOutTime=booked.getCheckOutTime();
        	
        	if(tscheckInTime!=null && tscheckOutTime!=null){
        		Calendar calCheckIn=Calendar.getInstance();
        		calCheckIn.setTime(this.tscheckInTime);
//        		calCheckIn.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
            	int bookedHours2=calCheckIn.get(Calendar.HOUR);
            	int bookedMinute2=calCheckIn.get(Calendar.MINUTE);
            	int bookedSecond2=calCheckIn.get(Calendar.SECOND);
            	int AMPM2=calCheckIn.get(Calendar.AM_PM);
            	
            	Calendar calCheckOut=Calendar.getInstance();
            	calCheckOut.setTime(this.tscheckOutTime);
//            	calCheckOut.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
            	int bookedHours3=calCheckOut.get(Calendar.HOUR);
            	int bookedMinute3=calCheckOut.get(Calendar.MINUTE);
            	int bookedSecond3=calCheckOut.get(Calendar.SECOND);
            	int AMPM3=calCheckOut.get(Calendar.AM_PM);
            	
            	String strFromTime=format2.format(tscheckInTime);
	  			String strToTime=format2.format(tscheckOutTime);
	  	    	DateTime starttime = DateTime.parse(strFromTime);
	  	        DateTime endtime = DateTime.parse(strToTime);
	  	        
	  	        java.util.Date fromtime = starttime.toDate();
	  	 		java.util.Date totime = endtime.toDate();
	  	 		
	  	 		if(AMPM2==0){
	  	 			if(bookedHours2<10){
	  	 				if(bookedHours2==0){
	  	 					this.bookingInTime="12:"+"00AM";
	  	 				}else{
	  	 					this.bookingInTime="0"+bookedHours2+":"+"00AM";	
	  	 				}
                		
                	}else{
                		this.bookingInTime=bookedHours2+":"+"00AM";
                	}
	  	 				
            	}else if(AMPM2==1){
            		if(bookedHours2<10){
            			if(bookedHours2==0){
            				this.bookingInTime="12:"+"00PM";	
	  	 				}else{
	  	 					this.bookingInTime="0"+bookedHours2+":"+"00PM";
	  	 				}
            			
                	}else{
                		this.bookingInTime=bookedHours2+":"+"00PM";
                	}
            		
            	}
	  	 		if(AMPM3==0){
	  	 			if(bookedHours3<10){
	  	 				if(bookedHours3==0){
	  	 					this.bookingOutTime="12:"+"00AM";
	  	 				}else{
	  	 					this.bookingOutTime="0"+bookedHours3+":"+"00AM";	
	  	 				}
                		
                	}else{
                		this.bookingOutTime=bookedHours3+":"+"00AM";
                	}
	  	 				
	  	 		}else if(AMPM3==1){
	  	 			if(bookedHours3<10){
	  	 				if(bookedHours3==0){
	  	 					this.bookingOutTime="12:"+"00PM";	
	  	 				}else{
	  	 					this.bookingOutTime="0"+bookedHours3+":"+"00PM";
	  	 				}
                		
                	}else{
                		this.bookingOutTime=bookedHours3+":"+"00PM";
                	}
	  	 			
	  	 		}
	  	 		
	  	 		long lngHours=tscheckOutTime.getTime()-tscheckInTime.getTime();
            	long diffHours = lngHours / (60 * 60 * 1000) % 24;
            	
            	this.checkIn=sdfformat.format(tscheckInTime);
            	this.checkInDays=f.format(fromtime);
            	this.checkOut=sdfformat.format(tscheckOutTime);
            	this.checkOutDays=f.format(totime);
            	
            	this.diffInHours=(int)diffHours;
            	this.voucherType="hourly-wise";
        	}else{
        		this.checkIn=sdfformat.format(FromDate);
            	this.checkInDays=f.format(fromdate);
            	this.checkOut=sdfformat.format(ToDate);
            	this.checkOutDays=f.format(todate);
            	this.voucherType="day-wise";
            	this.bookingInTime="00"+":"+"00";
	  	 		this.bookingOutTime="00"+":"+"00";
        	}
         	this.bookingDate=sdfformat1.format(booked.getCreatedDate());
         	
         	
         	
         	String checkIn = new SimpleDateFormat("MM/dd/yyyy").format(this.arrivalDate);
			String checkOut = new SimpleDateFormat("MM/dd/yyyy").format(this.departureDate);
				
				LocalDate date1 = LocalDate.parse(checkIn, DateTimeFormat.forPattern("MM/dd/yyyy"));
             LocalDate date2 = LocalDate.parse(checkOut, DateTimeFormat.forPattern("MM/dd/yyyy"));
             this.days = Days.daysBetween(date1, date2).getDays();
         	
           this.bookList =  bookingDetailController.listAccommodation(getBookingId(),booked.getAccommodationId());
   	
   		for (BookedDetail bookedDetail : bookList) {
         		
	            	PropertyAccommodation accommodation = propertyAccommodationController.find(bookedDetail.getAccommodationId());
	            	this.roomCount=bookedDetail.getRoomCount()/this.days;
	            	accommodationTypes.append(this.roomCount+" "+accommodation.getAccommodationType().trim());
	            	accommodationTypes.append(",");
	            	
	            	this.extraAdultRate=0.0;
	            	this.extraChildRate=0.0;
	            	int roomNight=(int)this.roomCount*this.days;
	            	if(booked.getExtraAdultRate()==null || booked.getExtraAdultRate()==0.0){
	                	this.extraAdultRate=0.0;	
	                }else{
	                	this.extraAdultRate=accommodation.getExtraAdult()*roomNight;
	                }
	                
	                
	                if(booked.getExtraChildRate()==null || booked.getExtraChildRate()==0.0){
	                	this.extraChildRate=0.0;	
	                }else{
	                	this.extraChildRate=accommodation.getExtraChild()*roomNight;
	                }
	                
	            	this.extraCharges+=this.extraAdultRate+this.extraChildRate;
	            	
	            	if(accommodation.getPropertyContractType()!=null){
	            		this.contractTypeId=accommodation.getPropertyContractType().getContractTypeId();
		            	PropertyContractType contracts=contractController.find(this.contractTypeId);
		            	this.contractType=contracts.getContractTypeName();	
	            	}else{
	            		this.contractTypeId=9;
		            	PropertyContractType contracts=contractController.find(this.contractTypeId);
		            	this.contractType=contracts.getContractTypeName();
	            	}
	            	
         		if(this.locationType.equalsIgnoreCase("metro")){
         			if(this.contractTypeId==7){
 	                	this.contractModel="Join";
 	            	}
         			
         			if(this.contractTypeId==6){
 	                	this.contractModel="Sure";
         			}
 	            	
 	            	if(this.contractTypeId==9){
 	                	this.contractModel="Flat";
 	            	}	
         		}
         		
         		if(this.locationType.equalsIgnoreCase("leisure")){

                 	String days=f.format(this.arrivalDate);
                     String[] weekdays={"Monday","Tuesday","Wednesday","Thursday","Sunday"};
         		    String[] weekend={"Friday","Saturday"};
         		    boolean isWeekdays=false,isWeekend=false;
     				for(int a=0;a<weekdays.length;a++){
     					if(days.equalsIgnoreCase(weekdays[a])){
     						isWeekdays=true;
     					}
     				}
     				
     				for(int b=0;b<weekend.length;b++){
     					if(days.equalsIgnoreCase(weekend[b])){
     						isWeekend=true;
     					}
     				}
     				
     				if(isWeekdays){
     					if(contractTypeId==7){//B-join
     	                	this.contractModel="Join";
                     	}else if(contractTypeId==6){//B-sure
     	                	this.contractModel="Sure";
                     	}else if(contractTypeId==8){//B-stay
     	                	this.contractModel="Stay";
                     	}else if(contractTypeId==9){//B-join flat
     	                	this.contractModel="Flat";
                     	}
     				}
     				
     				if(isWeekend){
     					if(contractTypeId==7){//B-join
     	                	this.contractModel="Join";
                     	}else if(contractTypeId==6){//B-sure
     	                	this.contractModel="Sure";
                     	}else if(contractTypeId==8){//B-stay
     	                	this.contractModel="Stay";
                     	}else if(contractTypeId==9){//B-join flat
     	                	this.contractModel="Flat";
                     	}
     				}
                 
         		}
	            	
      	 }
         	this.rooms = booked.getRooms();
         	if(this.rooms==1){
        		this.roomType=this.rooms+" room";
        	}
        	if(this.rooms>1){
        		this.roomType=this.rooms+" rooms";
        	}
        	
        	if(this.rooms>1 && this.days>1){
        		this.roomNightsType=this.rooms+" Rooms X "+this.days+" Nights";
        	}else if(this.rooms>1 && this.days==1){
        		this.roomNightsType=this.rooms+" Rooms X "+this.days+" Night";
        	}else if(this.rooms==1 && this.days>1){
        		this.roomNightsType=this.rooms+" Room X "+this.days+" Nights";
        	}else if(this.rooms==1 && this.days==1){
        		this.roomNightsType=this.rooms+" Room X "+this.days+" Night";
        	}else{
        		this.roomNightsType=this.rooms+" Room X "+this.days+" Night";
        	}
        	
        	if(this.days>1){
        		this.roomNights=this.days+" Nights";
        	}else{
        		this.roomNights=this.days+" Night";
        	}
        	
         	this.adults += booked.getAdults()/this.days;
         	this.child += booked.getChild()/this.days;
         	this.guests=(int)this.adults+(int)this.child;
         	this.totalAmount = this.tax + this.amount;
         	this.purchaseRate=booked.getPurchaseRate();
//        	this.partnerTotal= this.purchaseRate;
            PropertyAccommodation accommodations = propertyAccommodationController.find(this.accommodationId);
            this.taxType=accommodations.getTaxType();
            double taxPercent=0;
            double dayPerTariff=0;
            double roomdays=0;
            roomdays=this.days*this.roomCount;
            double purchasePerDay=0;
            purchasePerDay=this.purchaseRate/this.days;
            dayPerTariff=this.purchaseRate/roomdays;
            if(dayPerTariff<1000){
        		taxPercent=0;
        	}else if(dayPerTariff>=1000 && dayPerTariff<=2500){
        		taxPercent=12;
        	}else if(dayPerTariff>2500 && dayPerTariff<=7500){
        		taxPercent=18;
        	}else if(dayPerTariff>7500){
        		taxPercent=28;
        	}
            for(int d=0;d<this.days;d++){
            	
            	if(this.taxType==1){//Inclusive
                	this.partnerTax+=purchasePerDay*taxPercent/100;
                	this.partnerTariff+=purchasePerDay-this.partnerTax;
                	this.partnerTotal=this.partnerTariff+this.partnerTax;
                }else if(this.taxType==2){//Exclusive
                	this.partnerTax+=purchasePerDay*taxPercent/100;
                	this.partnerTariff+=purchasePerDay;
                	this.partnerTotal=this.partnerTariff+this.partnerTax;
                }	
            }
         	String strTotal=df.format(this.totalAmount);
         	this.totalAmount=Double.valueOf(strTotal);
         	
             this.totalAdvanceAmount = booked.getAdvanceAmount();
             this.tariffAmount=this.actualAmount-this.couponAmount-this.promotionAmount;
             /*if(booked.getExtraAdultRate()==null || booked.getExtraAdultRate()==0.0){
             	this.extraAdultRate=0.0;	
             }else{
             	this.extraAdultRate=booked.getExtraAdultRate();
             }
             
             
             if(booked.getExtraChildRate()==null || booked.getExtraChildRate()==0.0){
             	this.extraChildRate=0.0;	
             }else{
             	this.extraChildRate=booked.getExtraChildRate();
             }
             this.extraCharges=this.extraAdultRate+this.extraChildRate;
             */
             if(booked.getAddonAmount()==null){
             	this.addonAmount=0.0;
             }else{
             	this.addonAmount=booked.getAddonAmount();
             }
//             OTA Share
             if(booked.getOtaCommission()!=null && booked.getOtaTax()!=null){
             	this.dblOtaCommission=booked.getOtaCommission();
                 this.dblOtaTax=booked.getOtaTax();	
             }else{
             	this.dblOtaCommission=0;
                 this.dblOtaTax=0;
             }
             
				
             this.dblTotalOtaAmout=this.dblOtaCommission+this.dblOtaTax;
				
             this.dblRevenueAmount=this.amount-this.dblTotalOtaAmout;
             this.dblTotalRevenueTaxAmount=this.dblRevenueAmount+this.tax;
				
//				ULO Share
             this.dblCommission=20;
             this.dblUloCommission=this.dblRevenueAmount*this.dblCommission/100;
				
				
             this.dblUloTaxAmount=this.dblUloCommission*18/100;
				
             this.dblTotalUloCommission=this.dblUloCommission+this.dblUloTaxAmount; //Payable to Ulo
				
				
             this.dblHotelPayable=this.dblRevenueAmount-this.dblTotalUloCommission;
				
             this.dblHotelTaxPayable=this.dblHotelPayable+this.tax; //Payable to Hotelier
             if(this.sourceType.equalsIgnoreCase("OTA")){
             	this.totalAmount=this.amount+this.tax;
             }else{
             	this.totalAmount=this.tariffAmount+this.tax+this.extraCharges;	
             }
             
          this.hotelPay = (this.totalAmount-this.totalAdvanceAmount);
           /* if(this.purchaseRate<this.totalAmount){
      			this.hotelPay = (this.totalAmount-this.totalAdvanceAmount);	
      		}else{
      			this.hotelPay = 0;	
      		}*/
//          this.dbltotalhotelrevenue=this.partnerTotal-this.hotelPay;
         	if(this.totalAmount == booked.getAdvanceAmount()){
         		this.paymentStatus = "Paid";
         	}else if(this.totalAdvanceAmount==0){
        		this.paymentStatus = "Hotel Pay";
        	}else{
         		this.paymentStatus = "Partially Paid";
         	}

			}
         if(this.contractModel.equalsIgnoreCase("Join")){
         	this.rateModel="Net Rate";
         }else{
         	this.rateModel="Net Rate";
         }
         String strFirstName="",firstNameLetterUpperCase="";
			String strLastName="",lastNameLetterUpperCase="";
			strFirstName=this.firstName;
			strLastName=this.lastName;
			if(strFirstName!=null && strFirstName!="" && !strFirstName.equalsIgnoreCase("undefined")){
				strFirstName=strFirstName.trim();
				firstNameLetterUpperCase=strFirstName.substring(0, 1).toUpperCase()+strFirstName.substring(1);
				strFirstName=firstNameLetterUpperCase;
			}else{
				strFirstName="Not Available";
			}
			if(strLastName!=null && strLastName!=""  && !strLastName.equalsIgnoreCase("undefined")){
				strLastName=strLastName.trim();
				lastNameLetterUpperCase=strLastName.substring(0, 1).toUpperCase() +  strLastName.substring(1);
				strLastName=lastNameLetterUpperCase;
			}else{
				strLastName="";
			}
			
			
			strFullName=strFirstName+" "+strLastName;
         PropertyTaxeManager propertytaxController = new PropertyTaxeManager();
			 double taxe =  this.dblHotelPayable/this.days;
			
		 PropertyTaxe tax = propertytaxController.find(taxe);
		 this.dbltaxes = Math.round(this.dblHotelPayable * tax.getTaxPercentage() / 100  ) ;
		 this.dblHotelTaxPayable=this.dblHotelPayable+this.dbltaxes; //Payable to Hotelier
	
    
         String strContractMailId=null,strRevenueMailId=null;
			PmsPropertyManager pmsPropertyController=new PmsPropertyManager();
			for (PmsBookedDetails booked : bookedList) {
				PmsProperty pmsProperty=pmsPropertyController.find(booked.getPropertyId());
				
				this.reservationManagerEmail=pmsProperty.getReservationManagerEmail();
				this.propertyEmail=pmsProperty.getPropertyEmail();
				strRevenueMailId=pmsProperty.getRevenueManagerEmail();
				strContractMailId=pmsProperty.getContractManagerEmail();
				this.propertyUrl=pmsProperty.getPropertyUrl();
				if(pmsProperty.getGoogleLocation()!=null){
					this.locationId=pmsProperty.getGoogleLocation().getGoogleLocationId();
 					this.locationUrl=pmsProperty.getGoogleLocation().getGoogleLocationUrl();
 					
 					this.galleryPath=this.locationUrl+"/"+this.propertyUrl+"?tripTypeId=1";	
				}else{
					this.locationId=pmsProperty.getGoogleLocation().getGoogleLocationId();
 					this.locationUrl=pmsProperty.getGoogleLocation().getGoogleLocationUrl();
 					
 					this.galleryPath=this.propertyUrl;
				}
			}
			
			
			
			ArrayList<String> arlEmail=new ArrayList<String>();
			
			StringTokenizer stoken2=new StringTokenizer(this.propertyEmail,",");
			while(stoken2.hasMoreElements()){
				arlEmail.add(stoken2.nextToken());
			}
			StringTokenizer stoken3=new StringTokenizer(strRevenueMailId,",");
			while(stoken3.hasMoreElements()){
				arlEmail.add(stoken3.nextToken());
			}
			
			StringTokenizer stoken4=new StringTokenizer(strContractMailId,",");
			while(stoken4.hasMoreElements()){
				arlEmail.add(stoken4.nextToken());
			}
			arlEmail.add("support@ulohotels.com");
			arlEmail.add("vinodhini@ulohotels.com");
			arlEmail.add("finance@ulohotels.com");
			
			
			Integer emailSize=arlEmail.size();
			
				//email template
				Configuration cfg = new Configuration();
				cfg.setClassForTemplateLoading(BookingDetailAction.class, "../../../");
				Template template =null;
				if(this.contractModel.equalsIgnoreCase("Flat")){
					if(this.voucherType.equalsIgnoreCase("hourly-wise")){
						template = cfg.getTemplate("partnerflatbookinghourlyvoucher.ftl");	
	 				}else{
	 					template = cfg.getTemplate("partnerflatbookingvoucher.ftl");
	 				}
				}else{
					if(this.voucherType.equalsIgnoreCase("hourly-wise")){
						template = cfg.getTemplate("partnerbookinghourlyvoucher.ftl");	
	 				}else{
	 					template = cfg.getTemplate("partnerbookingvoucher.ftl");
	 				}
					
				}
				
				Map<String, Object> rootMap = new HashMap<String, Object>();
				//Map<String, String> rootMap = new HashMap<String, String>();	 				
				rootMap.put("bookingId",getBookingId().toString());
				rootMap.put("propertyName",this.propertyName);
				rootMap.put("propertyid",this.propertyId);
				rootMap.put("latitude",this.latitude.trim());
				rootMap.put("longitude",this.longitude.trim());
				rootMap.put("sourceName",this.sourceName);
				rootMap.put("paymentStatus",this.paymentStatus);
				rootMap.put("reservationEmail",this.reservationManagerEmail);
				rootMap.put("reservationContact",this.reservationManagerContact);
				rootMap.put("propertyEmail",this.propertyEmail);
				rootMap.put("specialrequest",this.specialRequest);
				rootMap.put("locationName",this.locationName);
				rootMap.put("roomnighttype", this.roomNightsType);
				rootMap.put("address1",this.address1);
				rootMap.put("address2",this.address2);
				rootMap.put("hotelpay",this.hotelPay);
				rootMap.put("phoneNumber",this.phoneNumber);
				rootMap.put("checkIn", this.checkIn);
				rootMap.put("checkInDays", this.checkInDays);
				rootMap.put("checkOut",this.checkOut);
				rootMap.put("checkOutDays",this.checkOutDays);
				rootMap.put("timeHours",this.timeHours);
				rootMap.put("hours", this.diffInHours);
				rootMap.put("roomNights", this.roomNights);
				rootMap.put("propertyUrl",this.galleryPath);
				rootMap.put("bookingTime", this.bookingTime);
				rootMap.put("bookingInTime",this.bookingInTime);
 				rootMap.put("bookingOutTime",this.bookingOutTime);
				
				rootMap.put("guestName",strFullName);
				rootMap.put("mobile",this.phone);
				rootMap.put("emailId",this.emailId);	 				
				rootMap.put("days",this.days.toString());
				
				
				
				rootMap.put("accommodationType", accommodationTypes);
				rootMap.put("rateModel", this.rateModel);
				rootMap.put("adults", String.valueOf(this.adults));
				rootMap.put("child",String.valueOf(this.child));
				rootMap.put("guests", this.guests);
				rootMap.put("roomType", this.roomType);
				rootMap.put("tagName", this.tagName);
				rootMap.put("tax",df.format(this.tax));
				rootMap.put("amount",df.format(this.dblRevenueAmount));
				rootMap.put("totalamount",df.format(this.dblTotalRevenueTaxAmount));
				rootMap.put("partnerTax",df.format(this.partnerTax));
				rootMap.put("partnerTariff",df.format(this.partnerTariff));
				rootMap.put("partnerTotal",df.format(this.partnerTotal));
				rootMap.put("totalAdvanceAmount",df.format(this.totalAdvanceAmount));
				rootMap.put("bookingDate", this.bookingDate);
				rootMap.put("otaCommission",df.format(this.dblOtaCommission));
				rootMap.put("otaTax",df.format(this.dblOtaTax));
				rootMap.put("totalOtaRevenue",df.format(this.dblTotalOtaAmout));
				rootMap.put("totalRevenueAmount",df.format(this.dblRevenueAmount));
				rootMap.put("uloCommission",df.format(this.dblUloCommission));
				rootMap.put("uloTax",df.format(this.dblUloTaxAmount));
				rootMap.put("totalUloRevenue",df.format(this.dblTotalUloCommission));
				rootMap.put("hotelPayable",df.format(this.dblHotelPayable));
				rootMap.put("totalhotelrevenue",this.dbltotalhotelrevenue);
				rootMap.put("totalHotelRevenue",df.format(this.dblHotelTaxPayable));
				rootMap.put("hotelTariff",df.format(this.dblHotelPayable));
 				rootMap.put("hotelTax",df.format(this.dbltaxes));
 				rootMap.put("tariffamount",df.format(this.tariffAmount));
 				rootMap.put("extracharges",df.format(this.extraCharges));
				
				
				rootMap.put("from", "ULO Hotels Support");
				Writer out = new StringWriter();
				template.process(rootMap, out);
				
				String strSubject=null;
 				if(this.mailSubjectFlag){
 					strSubject="Booking Modification for Booking Id "+getBookingId().toString()+" "+this.propertyName;
 				}else{
 					strSubject="Hotel Confirmation for Booking Id "+getBookingId().toString()+" "+this.propertyName;
 				}
 				
 				//send email
 				Email em = new Email();
 				int i=0;
 				String emailArray[]=new String[emailSize];
 				Iterator<String> iterEmail=arlEmail.iterator();
 				while(iterEmail.hasNext()){
 					emailArray[i]=iterEmail.next().trim();
 					i++;
 				}

 				/*Writer fileWriter = new FileWriter (new File("E:/partner-booking-template-input.xhtml"));
 	            template.process(rootMap, fileWriter);
 	            fileWriter.flush();
 	            fileWriter.close();
 	            String inputFile="E:/partner-booking-template-input.xhtml";
 	            String outputFile="E:/partner-booking-template-output.html";
 	            File file = new File(inputFile);
 	            FileOutputStream fos = null;
 	            FileInputStream fis = null;
 	            FileWriter outputStream = null;
 	            
 	            fos = new FileOutputStream(outputFile);
 	            fis = new FileInputStream(file);
 	            outputStream = new FileWriter(outputFile);
 	            Tidy tidy = new Tidy(); //HTML parser and pretty printer.
 	            tidy.setXHTML(true); //true if tidy should output XHTML
 	            
 	            tidy.parse(fis, fos);
 				//out.flush();
 				createPdf(out.toString(), getText("storage.pdf.booking.partner.path"));
 				em.set_fileName(getText("storage.pdf.booking.partner.path"));
 				*/
 				em.set_from("donotreply@ulohotels.com");
 				em.set_username("Support");
 				em.set_subject(strSubject);
 				em.set_bodyContent(out);
 				em.set_emailSize(emailSize);
 				em.setEmailArray(emailArray);
 				em.sendBookingVoucher();		
				
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		//return null;
	
	}
	 
	 
	 public void getGuestBookedDetails(){//online vouchers
		 try {
			String strFullName="";
			HttpServletResponse response = ServletActionContext.getResponse();
			DecimalFormat df = new DecimalFormat("###.##");
			DateFormat f = new SimpleDateFormat("EEEE");
			DateFormat f1 = new SimpleDateFormat("EEE");
		    SimpleDateFormat sdfformat=new SimpleDateFormat("MMM dd");
		    SimpleDateFormat sdfformat1=new SimpleDateFormat("MMM dd, yyyy");
		    SimpleDateFormat sdfformat2=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			PmsPropertyManager propertyController = new PmsPropertyManager();
			PropertyAccommodationManager propertyAccommodationController = new PropertyAccommodationManager();
			BookingDetailManager bookingDetailController = new BookingDetailManager();
			PmsGuestManager guestController = new PmsGuestManager();
			PmsTagManager tagController=new PmsTagManager();
			PmsSourceManager sourceController = new PmsSourceManager();
			response.setContentType("application/json");
			this.bookedList = bookingDetailController.bookedList(this.bookingId);
			this.guests=0;this.adults=0;this.child=0;
			
			StringBuilder smsAccommodationTypes = new StringBuilder();
            for (PmsBookedDetails booked : bookedList) {
            	this.bookingId = getBookingId();
            	this.sourceId=booked.getSourceId();
            	PmsProperty pmsProperty = propertyController.find(booked.getPropertyId());
            	this.propertyName = pmsProperty.getPropertyName();//resort name and location name is missing
            	this.propertyId = pmsProperty.getPropertyId();
            	this.latitude = pmsProperty.getLatitude();
            	this.longitude = pmsProperty.getLongitude();
            	this.address1=pmsProperty.getAddress1();//property address1
            	this.address2=pmsProperty.getAddress2();//property address2
            	this.phoneNumber = pmsProperty.getPropertyContact();//property phone number
            	this.propertyEmail = pmsProperty.getPropertyEmail();
            	this.locationName=pmsProperty.getLocation().getLocationName();//property location
            	this.reservationManagerEmail=pmsProperty.getReservationManagerEmail();
            	this.reservationManagerContact = pmsProperty.getReservationManagerContact();
            	this.routeMap = pmsProperty.getRouteMap();
            	java.util.Date date=new java.util.Date();
            	Calendar calDate=Calendar.getInstance();
            	calDate.setTime(date);
            	calDate.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
            	int bookedHours=calDate.get(Calendar.HOUR);
            	int bookedMinute=calDate.get(Calendar.MINUTE);
            	int bookedSecond=calDate.get(Calendar.SECOND);
            	int AMPM=calDate.get(Calendar.AM_PM);
            	
            	String strBookingDate=new SimpleDateFormat("MMM d, yyyy", Locale.ENGLISH).format(calDate.getTime());
            	String hours="",minutes="",seconds="";
            	if(bookedHours<10){
            		hours=String.valueOf(bookedHours);
            		hours="0"+hours;
            	}else{
            		hours=String.valueOf(bookedHours);
            	}
            	if(bookedMinute<10){
            		minutes=String.valueOf(bookedMinute);
            		minutes="0"+minutes;
            	}else{
            		minutes=String.valueOf(bookedMinute);
            	}
            	
            	if(bookedMinute<10){
            		seconds=String.valueOf(bookedSecond);
            		seconds="0"+seconds;
            	}else{
            		seconds=String.valueOf(bookedSecond);
            	}
            	if(AMPM==0){//If the current time is AM
            		this.timeHours=f.format(calDate.getTime())+","+strBookingDate+" at "+hours+" : "+minutes+" AM ";
            	}else if(AMPM==1){//If the current time is PM
            		this.timeHours=f.format(calDate.getTime())+","+strBookingDate+" at "+hours+" : "+minutes+" PM ";
            	}
            	Calendar calBookingDate=Calendar.getInstance();
            	calBookingDate.setTime(booked.getCreatedDate());
            	calBookingDate.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
            	int bookedHours1=calBookingDate.get(Calendar.HOUR);
            	int bookedMinute1=calBookingDate.get(Calendar.MINUTE);
            	int bookedSecond1=calBookingDate.get(Calendar.SECOND);
            	int AMPM1=calBookingDate.get(Calendar.AM_PM);
            	
            	String hours1="",minutes1="",seconds1="";
            	if(bookedHours1<10){
            		hours1=String.valueOf(bookedHours1);
            		hours1="0"+hours1;
            	}else{
            		hours1=String.valueOf(bookedHours1);
            	}
            	if(bookedMinute1<10){
            		minutes1=String.valueOf(bookedMinute1);
            		minutes1="0"+minutes1;
            	}else{
            		minutes1=String.valueOf(bookedMinute1);
            	}
            	
            	if(bookedSecond1<10){
            		seconds1=String.valueOf(bookedSecond1);
            		seconds1="0"+seconds1;
            	}else{
            		seconds1=String.valueOf(bookedSecond1);
            	}
            	if(AMPM1==0){//If the current time is AM
            		this.bookingTime=hours1+" : "+minutes1+" : "+seconds1+" AM ";
            	}else if(AMPM1==1){//If the current time is PM
            		this.bookingTime=hours1+" : "+minutes1+" : "+seconds1+" PM ";
            	}
            	this.tscheckInTime=booked.getCheckInTime();
            	this.tscheckOutTime=booked.getCheckOutTime();
            	
            	
            	if(this.sourceId==12){
         			this.sourceName = "Offline";
         		}else{
         			this.sourceName = "Online";
         		}
            	if(this.sourceName.equalsIgnoreCase("online")){
            		if(this.sourceId> 1 && this.sourceId!=12){
            			this.sourceType="OTA";
            		}else if(this.sourceId==12){
            			this.sourceType="Offline";
            		}else{
            			this.sourceType="Offline";
            		}
            	}else{
            		if(this.sourceId> 1 && this.sourceId!=12){
            			this.sourceType="OTA";
            		}else if(this.sourceId==12){
            			this.sourceType="Offline";
            		}else{
            			this.sourceType="Offline";
            		}
            	}
            	/*if(this.sourceId!=null){
            		PmsSource source=sourceController.find(sourceId);
            		this.sourceType=source.getSourceName();
            	}*/
            	
            	if(this.sourceName.equalsIgnoreCase("online")){
            		if(this.sourceId> 1 && this.sourceId!=12){
            			this.sourceType="OTA";
            		}else if(this.sourceId==12){
            			this.sourceType="Offline";
            		}else{
            			this.sourceType="Offline";
            		}
            	}else{
            		if(this.sourceId> 1 && this.sourceId!=12){
            			this.sourceType="OTA";
            		}else if(this.sourceId==12){
            			this.sourceType="Offline";
            		}else{
            			this.sourceType="Offline";
            		}
            	}
            	
            	SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
	  			Timestamp FromDate=booked.getArrivalDate();
	  			Timestamp ToDate=booked.getDepartureDate();
	  			String strFromDate=format2.format(FromDate);
	  			String strToDate=format2.format(ToDate);
	  	    	DateTime startdate = DateTime.parse(strFromDate);
	  	        DateTime enddate = DateTime.parse(strToDate);
	  	        
	  	        java.util.Date fromdate = startdate.toDate();
	  	 		java.util.Date todate = enddate.toDate();
	  	 		 
	  			int diffInDays = (int) ((todate.getTime() - fromdate.getTime()) / (1000 * 60 * 60 * 24));
	  			
            	this.arrivalDate = booked.getArrivalDate();
            	this.departureDate = booked.getDepartureDate();
            	if(booked.getTagId()!=null){
	  				PmsTags tags=tagController.find(booked.getTagId());
	  				this.tagName=tags.getTagName();
	  			}else{
	  				this.tagName="Not Applicable";
	  			}
            	if(tscheckInTime!=null && tscheckOutTime!=null){
            		Calendar calCheckIn=Calendar.getInstance();
            		calCheckIn.setTime(this.tscheckInTime);
//            		calCheckIn.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
                	int bookedHours2=calCheckIn.get(Calendar.HOUR);
                	int bookedMinute2=calCheckIn.get(Calendar.MINUTE);
                	int bookedSecond2=calCheckIn.get(Calendar.SECOND);
                	int AMPM2=calCheckIn.get(Calendar.AM_PM);
                	
                	Calendar calCheckOut=Calendar.getInstance();
                	calCheckOut.setTime(this.tscheckOutTime);
//                	calCheckOut.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
                	int bookedHours3=calCheckOut.get(Calendar.HOUR);
                	int bookedMinute3=calCheckOut.get(Calendar.MINUTE);
                	int bookedSecond3=calCheckOut.get(Calendar.SECOND);
                	int AMPM3=calCheckOut.get(Calendar.AM_PM);
                	
                	String strFromTime=format2.format(tscheckInTime);
    	  			String strToTime=format2.format(tscheckOutTime);
    	  	    	DateTime starttime = DateTime.parse(strFromTime);
    	  	        DateTime endtime = DateTime.parse(strToTime);
    	  	        
    	  	        java.util.Date fromtime = starttime.toDate();
    	  	 		java.util.Date totime = endtime.toDate();
                	
    	  	 		if(AMPM2==0){
    	  	 			if(bookedHours2<10){
    	  	 				if(bookedHours2==0){
    	  	 					this.bookingInTime="12:"+"00AM";
    	  	 				}else{
    	  	 					this.bookingInTime="0"+bookedHours2+":"+"00AM";	
    	  	 				}
                    		
                    	}else{
                    		this.bookingInTime=bookedHours2+":"+"00AM";
                    	}
    	  	 				
                	}else if(AMPM2==1){
                		if(bookedHours2<10){
                			if(bookedHours2==0){
                				this.bookingInTime="12:"+"00PM";	
                			}else{
                				this.bookingInTime="0"+bookedHours2+":"+"00PM";
                			}
                			
                    	}else{
                    		this.bookingInTime=bookedHours2+":"+"00PM";
                    	}
                		
                	}
    	  	 		if(AMPM3==0){
    	  	 			if(bookedHours3<10){
    	  	 				if(bookedHours3==0){
    	  	 					this.bookingOutTime="12:"+"00AM";
    	  	 				}else{
    	  	 					this.bookingOutTime="0"+bookedHours3+":"+"00AM";	
    	  	 				}
                    		
                    	}else{
                    		this.bookingOutTime=bookedHours3+":"+"00AM";
                    	}
    	  	 				
    	  	 		}else if(AMPM3==1){
    	  	 			if(bookedHours3<10){
    	  	 				if(bookedHours3==0){
    	  	 					this.bookingOutTime="12:"+"00PM";	
    	  	 				}else{
    	  	 					this.bookingOutTime="0"+bookedHours3+":"+"00PM";
    	  	 				}
                    		
                    	}else{
                    		this.bookingOutTime=bookedHours3+":"+"00PM";
                    	}
    	  	 			
    	  	 		}
    	  	 		
    	  	 		long lngHours=tscheckOutTime.getTime()-tscheckInTime.getTime();
                	long diffHours = lngHours / (60 * 60 * 1000) % 24;
                	
                	this.checkIn=sdfformat.format(tscheckInTime);
                	this.checkInDays=f.format(fromtime);
                	this.checkOut=sdfformat.format(tscheckOutTime);
                	this.checkOutDays=f.format(totime);
                	
                	this.diffInHours=(int)diffHours;
                	this.voucherType="hourly-wise";
            	}else{
            		this.bookingInTime="00"+":"+"00";
    	  	 		this.bookingOutTime="00"+":"+"00";
            		this.checkIn=sdfformat.format(FromDate);
                	this.checkInDays=f.format(fromdate);
                	this.checkOut=sdfformat.format(ToDate);
                	this.checkOutDays=f.format(todate);
                	this.voucherType="day-wise";
            	}
            	
            	this.bookingDate=sdfformat1.format(booked.getCreatedDate());
            	this.diffInDays=diffInDays;
            	
            	PmsGuest guest = guestController.find(booked.getGuestId());
            	this.firstName = guest.getFirstName();
            	this.lastName = guest.getLastName();
            	this.phone = guest.getPhone();
            	this.emailId = guest.getEmailId();
            	if(guest.getGstNo()==null){
            		this.gstNo="Not Available";
            	}else{
            		this.gstNo=guest.getGstNo();	
            	}
            	
            	this.specialRequest = booked.getSpecialRequest();
            	if(this.specialRequest==null){
            		this.specialRequest="Not Available";
            	}
            	this.promotionFirstName=booked.getPromotionFirstName()==null?"":booked.getPromotionFirstName();
            	this.promotionSecondName=booked.getPromotionSecondName()==null?"":booked.getPromotionSecondName();
            	this.couponName=booked.getCouponName()==null?"":booked.getCouponName();
            	this.promotionAmount=booked.getPromotionAmount()==null?0:booked.getPromotionAmount();
            	this.couponAmount=booked.getCouponAmount()==null?0:booked.getCouponAmount();
            	
            	String checkIn = new SimpleDateFormat("MM/dd/yyyy").format(this.arrivalDate);
				String checkOut = new SimpleDateFormat("MM/dd/yyyy").format(this.departureDate);
				
				LocalDate date1 = LocalDate.parse(checkIn, DateTimeFormat.forPattern("MM/dd/yyyy"));
                LocalDate date2 = LocalDate.parse(checkOut, DateTimeFormat.forPattern("MM/dd/yyyy"));
                this.days = Days.daysBetween(date1, date2).getDays();
            	
             this.bookList =  bookingDetailController.listAccommodation(this.bookingId,booked.getAccommodationId());
             
         		for (BookedDetail bookedDetail : bookList) {
            		
	            	PropertyAccommodation accommodation = propertyAccommodationController.find(bookedDetail.getAccommodationId());
	            	this.roomCount = bookedDetail.getRoomCount()/this.days;
	            	smsAccommodationTypes.append(this.roomCount+" "+accommodation.getAccommodationType().trim());
	            	smsAccommodationTypes.append(",");
	          
         	    }
            	
            	
            	this.rooms = booked.getRooms();
            	
            	
            	if(this.rooms==1){
            		this.roomType=this.rooms+" room";
            	}
            	if(this.rooms>1){
            		this.roomType=this.rooms+" rooms";
            	}
            	
            	if(this.rooms>1 && this.days>1){
            		this.roomNightsType=this.rooms+" Rooms X "+this.days+" Nights";
            	}else if(this.rooms>1 && this.days==1){
            		this.roomNightsType=this.rooms+" Rooms X "+this.days+" Night";
            	}else if(this.rooms==1 && this.days>1){
            		this.roomNightsType=this.rooms+" Room X "+this.days+" Nights";
            	}else if(this.rooms==1 && this.days==1){
            		this.roomNightsType=this.rooms+" Room X "+this.days+" Night";
            	}else{
            		this.roomNightsType=this.rooms+" Room X "+this.days+" Night";
            	}
            	
            	
            	if(this.days>1){
            		this.roomNights=this.days+" Nights";
            	}else{
            		this.roomNights=this.days+" Night";
            	}
            	this.adults += booked.getAdults()/this.days;
            	this.child += booked.getChild()/this.days;
            	this.guests=(int)this.adults+(int)this.child;
            	this.tax = booked.getTax();
            	this.amount = booked.getAmount();
            	
            	//this.totalAmount = this.tax + this.amount;
            	/*String strTotal=df.format(this.totalAmount);
            	this.totalAmount=Double.valueOf(strTotal);*/
            	this.actualAmount=booked.getActualAmount();
                this.totalAdvanceAmount = booked.getAdvanceAmount();
                
                
                if(booked.getExtraAdultRate()==null || booked.getExtraAdultRate()==0.0){
                	this.extraAdultRate=0.0;	
                }else{
                	this.extraAdultRate=booked.getExtraAdultRate();
                }
                
                
                if(booked.getExtraChildRate()==null || booked.getExtraChildRate()==0.0){
                	this.extraChildRate=0.0;	
                }else{
                	this.extraChildRate=booked.getExtraChildRate();
                }
                this.extraCharges=this.extraAdultRate+this.extraChildRate;
                
                this.tariffAmount=this.actualAmount-this.couponAmount-this.promotionAmount;
                if(booked.getAddonAmount()==null){
                	this.addonAmount=0.0;
                }else{
                	this.addonAmount=booked.getAddonAmount();
                }
                
                if(this.sourceType.equalsIgnoreCase("OTA")){
                	this.totalAmount=this.amount+this.tax;
                }else{
                	this.totalAmount=this.amount+this.tax+this.extraCharges;	
                }
                
                

                /*if(this.totalAdvanceAmount == 0){
            	}
                	this.hotelPay = 0;
            	else{
            		this.hotelPay = (this.totalAmount-this.totalAdvanceAmount);
            	}*/
                this.hotelPay = (this.totalAmount-this.totalAdvanceAmount); 
            	if(this.totalAmount == this.totalAdvanceAmount){
            		this.paymentStatus = "Paid";
            	}else if(this.totalAdvanceAmount==0){
            		this.paymentStatus = "Hotel Pay";
            	}else{
            		this.paymentStatus = "Partially Paid";
            	}
               
				
                
                 String strFirstName="",firstNameLetterUpperCase="";
				String strLastName="",lastNameLetterUpperCase="";
				strFirstName=this.firstName;
				strLastName=this.lastName;
				if(strFirstName!=null && strFirstName!="" && !strFirstName.equalsIgnoreCase("undefined")){
					strFirstName=strFirstName.trim();
					firstNameLetterUpperCase=strFirstName.substring(0, 1).toUpperCase()+strFirstName.substring(1);
					strFirstName=firstNameLetterUpperCase;
				}else{
					strFirstName="Not Available";
				}
				if(strLastName!=null && strLastName!=""  && !strLastName.equalsIgnoreCase("undefined")){
					strLastName=strLastName.trim();
					lastNameLetterUpperCase=strLastName.substring(0, 1).toUpperCase() +  strLastName.substring(1);
					strLastName=lastNameLetterUpperCase;
				}else{
					strLastName="";
				}
				strFullName=strFirstName+" "+strLastName;
				 				
				

			}
           
			    
			    this.grandTotal=this.totalAmount;
			    
			    String strContractMailId=null,strRevenueMailId=null;
 				PmsPropertyManager pmsPropertyController=new PmsPropertyManager();
 				for (PmsBookedDetails booked : bookedList) {
 					PmsProperty pmsProperty=pmsPropertyController.find(booked.getPropertyId());
 					
 					this.reservationManagerEmail=pmsProperty.getReservationManagerEmail();
 					this.propertyEmail=pmsProperty.getPropertyEmail();
 					strRevenueMailId=pmsProperty.getRevenueManagerEmail();
 					strContractMailId=pmsProperty.getContractManagerEmail();
 					this.propertyUrl=pmsProperty.getPropertyUrl();
 					if(pmsProperty.getGoogleLocation()!=null){
 						this.locationId=pmsProperty.getGoogleLocation().getGoogleLocationId();
 	 					this.locationUrl=pmsProperty.getGoogleLocation().getGoogleLocationUrl();
 	 					
 	 					this.galleryPath=this.locationUrl+"/"+this.propertyUrl+"?tripTypeId=1";	
 					}else{
 						this.locationId=pmsProperty.getGoogleLocation().getGoogleLocationId();
 	 					this.locationUrl=pmsProperty.getGoogleLocation().getGoogleLocationUrl();
 	 					
 	 					this.galleryPath=this.propertyUrl;
 					}
 					
 					
 				}
 				
 				ArrayList<String> arlEmail=new ArrayList<String>();
 				
 				StringTokenizer stoken=new StringTokenizer(this.reservationManagerEmail,",");
 				while(stoken.hasMoreElements()){
 					arlEmail.add(stoken.nextToken());
 				}
 				
				/*StringTokenizer stoken2=new StringTokenizer(this.propertyEmail,",");
				while(stoken2.hasMoreElements()){
					arlEmail.add(stoken2.nextToken());
				}*/
				
				
				StringTokenizer stoken3=new StringTokenizer(strRevenueMailId,",");
 				while(stoken3.hasMoreElements()){
 					arlEmail.add(stoken3.nextToken());
 				}
 				
 				StringTokenizer stoken4=new StringTokenizer(strContractMailId,",");
 				while(stoken4.hasMoreElements()){
 					arlEmail.add(stoken4.nextToken());
 				}
 				arlEmail.add(getText("bookings.notification.email"));
 				arlEmail.add(getText("operations.notification.email"));
 				arlEmail.add(getText("finance.notification.email"));
 				
 				Integer emailSize=arlEmail.size();
 				
 				//email template
 				Configuration cfg = new Configuration();
 				cfg.setClassForTemplateLoading(BookingDetailAction.class, "../../../");
 				Template template = null;
 				if(this.voucherType.equalsIgnoreCase("hourly-wise")){
 					if(this.sourceType.equalsIgnoreCase("OTA")){
 						template = cfg.getTemplate(getText("guest.ota.booking.hourly.voucher.template"));	
 					}else if(this.sourceType.equalsIgnoreCase("offline")){
 						template = cfg.getTemplate(getText("guest.offline.booking.hourly.voucher.template"));
 					}else{
 						template = cfg.getTemplate(getText("guest.booking.hourly.voucher.template"));
 					}
 				}else{
 					if(this.sourceType.equalsIgnoreCase("OTA")){
 						template = cfg.getTemplate(getText("guest.ota.booking.voucher.template"));	
 					}else if(this.sourceType.equalsIgnoreCase("offline")){
 						template = cfg.getTemplate(getText("guest.offline.booking.voucher.template"));
 					}else{
 						template = cfg.getTemplate(getText("guest.booking.voucher.template"));
 					}
 					
 				}
 				
 				Map<String, Object> rootMap = new HashMap<String, Object>();
 				//Map<String, String> rootMap = new HashMap<String, String>();	 				
 				rootMap.put("bookingId",getBookingId().toString());
 				rootMap.put("propertyName",this.propertyName);
 				rootMap.put("propertyid",this.propertyId);
 				rootMap.put("latitude",this.latitude.trim());
 				rootMap.put("longitude",this.longitude.trim());
                rootMap.put("sourceName",this.sourceName);
 				rootMap.put("paymentStatus",this.paymentStatus);
 				rootMap.put("promotionFirstName",this.promotionFirstName);
 				rootMap.put("promotionSecondName",this.promotionSecondName);
 				rootMap.put("couponName",this.couponName);
 				rootMap.put("promotionAmount",this.promotionAmount);
 				rootMap.put("couponAmount",this.couponAmount);
 				rootMap.put("reservationManagerEmail",this.reservationManagerEmail);
 				rootMap.put("reservationManagerContact",this.reservationManagerContact);
 				rootMap.put("propertyEmail",this.propertyEmail);
 				rootMap.put("specialrequest",this.specialRequest);
 				rootMap.put("guestName",strFullName);
 				rootMap.put("gstno",this.gstNo);
 				rootMap.put("locationName",this.locationName);
 				rootMap.put("address1",this.address1);
 				rootMap.put("address2",this.address2);
 				rootMap.put("hotelpay",this.hotelPay);
 				rootMap.put("phoneNumber",this.phoneNumber);
 				rootMap.put("checkIn", this.checkIn);
 				rootMap.put("checkInDays", this.checkInDays);
 				rootMap.put("checkOut",this.checkOut);
 				rootMap.put("checkOutDays",this.checkOutDays);
 				rootMap.put("timeHours",this.timeHours);
 				rootMap.put("bookingDate",this.bookingDate);
 				rootMap.put("bookingTime",this.bookingTime);
 				rootMap.put("hours", this.diffInHours);
				rootMap.put("roomNights", this.roomNights);
 				rootMap.put("reservationEmail",this.reservationManagerEmail);
 				rootMap.put("reservationContact",this.reservationManagerContact);
 				rootMap.put("tagName", this.tagName);
 				rootMap.put("emailId",this.emailId);
 				rootMap.put("mobile",this.phone);
 				rootMap.put("days",this.days.toString());
 				rootMap.put("propertyUrl",this.galleryPath);
 				rootMap.put("bookingInTime",this.bookingInTime);
 				rootMap.put("bookingOutTime",this.bookingOutTime);
 				
 				rootMap.put("accommodationType", smsAccommodationTypes);
 				rootMap.put("adults", String.valueOf(this.adults));
 				rootMap.put("child",String.valueOf(this.child));
 				rootMap.put("guests",String.valueOf(this.guests));
 				rootMap.put("roomType", this.roomType);
 				rootMap.put("roomnighttype", this.roomNightsType);
 				rootMap.put("tax",df.format(this.tax));
 				rootMap.put("amount",df.format(this.amount));
 				rootMap.put("totalamount",df.format(this.grandTotal));
 				rootMap.put("actualamount",df.format(this.actualAmount));
 				rootMap.put("totaladvanceamount",df.format(this.totalAdvanceAmount));
 				rootMap.put("tariffamount",df.format(this.tariffAmount));
 				rootMap.put("extracharges",df.format(this.extraCharges));
 				rootMap.put("from", getText("notification.from"));
 				Writer out = new StringWriter();
 				template.process(rootMap, out);
 				String strSubject=null;
 				if(this.mailSubjectFlag){
 					strSubject="Booking Modification for Booking Id "+getBookingId().toString()+" "+this.propertyName;
 				}else{
 					strSubject="Hotel Confirmation for Booking Id "+getBookingId().toString()+" "+this.propertyName;
 				}
 				
 				//send email
 				Email em = new Email();
 				em.set_to(this.emailId);
 				int i=0;
 				String emailArray[]=new String[emailSize];
 				Iterator<String> iterEmail=arlEmail.iterator();
 				while(iterEmail.hasNext()){
 					emailArray[i]=iterEmail.next().toLowerCase();
 					i++;
 				}

 				/*Writer fileWriter = new FileWriter (new File("E:/guest-booking-template-input.xhtml"));
 	            template.process(rootMap, fileWriter);
 	            fileWriter.flush();
 	            fileWriter.close();
 	            String inputFile="E:/guest-booking-template-input.xhtml";
 	            String outputFile="E:/guest-booking-template-output.html";
 	            File file = new File(inputFile);
 	            FileOutputStream fos = null;
 	            FileInputStream fis = null;
 	            FileWriter outputStream = null;
 	            
 	            fos = new FileOutputStream(outputFile);
 	            fis = new FileInputStream(file);
 	            outputStream = new FileWriter(outputFile);
 	            Tidy tidy = new Tidy(); //HTML parser and pretty printer.
 	            tidy.setXHTML(true); //true if tidy should output XHTML
 	            
 	            tidy.parse(fis, fos);
 				
 				createPdf(out.toString(), getText("storage.pdf.booking.guests.path"));
 				em.set_fileName(getText("storage.pdf.booking.guests.path"));
 				*/
 				em.set_from(getText("email.from"));
 				em.set_username(getText("email.username"));
 				em.set_subject(strSubject);
 				em.set_bodyContent(out);
 				em.set_emailSize(emailSize);
 				em.setEmailArray(emailArray);
 				em.sendBookingVoucher();		
 				
 				getPartnerBookedDetails();
 				
 				
 				String message = "&sms_text=" + "Confirmation from Ulo Hotels \n\n"+
    					
    					"Booking ID : "+ getBookingId().toString() +"\n"+
    					 "Name : "+ strFullName  +"\n"+ 
    					 "Hotel : "+ this.propertyName +"\n"+
    					"Room :"+  smsAccommodationTypes.toString()  +"\n"+
    					 "Check-In :"+ checkIn + "\n"+
    					"Check-Out :"+ checkOut+"\n\n"+ "We hope to see you soon.\n\n Find more hotel booking offers on www.ulohotels.com";

    			//String sender = "&sender=" + "TXTLCL";
    			String numbers = "&sms_to=" + "+91"+this.phone;
    			String from = "&sms_from=" + "ULOHTL";
    			String type = "&sms_type=" + "trans"; 
    			// Send data
    			HttpURLConnection conn = (HttpURLConnection) new URL("https://secureets.knowlarity.com/ets/dom/clients/ulohotels/api/ulohotels_sms_api.py").openConnection();
    			//"http://etsrds.kapps.in/webapi/ulohotels/api/ulohotels_sms_api.py?sms_text=hi%20this%20gopinath%20from%20ulo&sms_to=%20918220211068&sms_from=ULOHTL&sms_type=promo"
    			String data = numbers + message +  from + type;
    			conn.setDoOutput(true);
    			conn.setRequestMethod("POST");
    			conn.addRequestProperty("auth_key", "Basic dWxvaG90ZWxzX1Q6VUxPSFRMQDIwMTg=");
    			conn.setRequestProperty("Content-Length", Integer.toString(data.length()));
    			conn.getOutputStream().write(data.getBytes("UTF-8"));
    			final BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
    			final StringBuffer stringBuffer = new StringBuffer();
    			/*String line;
    			
    			while ((line = rd.readLine()) != null) {
    				stringBuffer.append(line);
    				
    			}*/
    			rd.close();
    			
    			getRouteMapSms(this.routeMap,this.phone);
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

	
	 }
	 public void getBookedDetails() throws IOException {
			
		    
		
			 try {

				String jsonOutput = "";
				HttpServletResponse response = ServletActionContext.getResponse();
				DecimalFormat df = new DecimalFormat("###.##");
				PmsPropertyManager propertyController = new PmsPropertyManager();
				PropertyAccommodationManager propertyAccommodationController = new PropertyAccommodationManager();
				BookingDetailManager bookingDetailController = new BookingDetailManager();
				BookingGuestDetailManager bookingGuestDetailController = new BookingGuestDetailManager();
				PmsGuestManager guestController = new PmsGuestManager();
				PmsStatusManager statusController = new PmsStatusManager();
				PropertyDiscountManager propertyDiscountController = new PropertyDiscountManager();  
				PmsBookingManager bookingController = new PmsBookingManager();
				
				response.setContentType("application/json");
				this.bookedList = bookingDetailController.bookedList(this.bookingId);
				
				StringBuilder accommodationType = new StringBuilder();
				
				StringBuilder accommodationRate = new StringBuilder();
				
				this.totalAdultCount = (Integer) sessionMap.get("totalAdultCount");
				
				//this.totalInfantCount = (Integer) sessionMap.get("totalInfantCount");
				
				this.totalChildCount = (Integer) sessionMap.get("totalChildCount");
				this.txtGrandTotal=(Double)sessionMap.get("txtGrandTotal");
				
				StringBuilder smsAccommodationTypes = new StringBuilder();
				List accommodationBooked = new ArrayList();
	            for (PmsBookedDetails booked : bookedList) {
	            	this.bookingId = getBookingId();
	            	this.sourceId=booked.getSourceId();
	            	this.propertyDiscountId = booked.getPropertyDiscountId();
	            	PropertyDiscount discount = propertyDiscountController.findId(this.propertyDiscountId);
	            	this.propertyDiscountName = discount.getDiscountName();
	            	PmsProperty pmsProperty = propertyController.find(booked.getPropertyId());
	            	this.propertyName = pmsProperty.getPropertyName();//resort name and location name is missing
	            	this.propertyId = pmsProperty.getPropertyId();
	            	this.latitude = pmsProperty.getLatitude();
	            	this.longitude = pmsProperty.getLongitude();
	            	this.address1=pmsProperty.getAddress1();//property address1
	            	this.address2=pmsProperty.getAddress2();//property address2
	            	this.phoneNumber = pmsProperty.getPropertyContact();//property phone number
	            	this.propertyEmail = pmsProperty.getPropertyEmail();
	            	this.locationName=pmsProperty.getLocation().getLocationName();//property location
	            	this.reservationManagerEmail=pmsProperty.getReservationManagerEmail();
	            	this.reservationManagerContact = pmsProperty.getReservationManagerContact();
	            	this.routeMap = pmsProperty.getRouteMap();
	            	java.util.Date date=new java.util.Date();
	            	Calendar calDate=Calendar.getInstance();
	            	calDate.setTime(date);
	            	calDate.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
	            	int bookedHours=calDate.get(Calendar.HOUR);
	            	int bookedMinute=calDate.get(Calendar.MINUTE);
	            	int bookedSecond=calDate.get(Calendar.SECOND);
	            	int AMPM=calDate.get(Calendar.AM_PM);
	            	
	            	String strBookingDate=new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH).format(calDate.getTime());
	            	String hours="",minutes="",seconds="";
	            	if(bookedHours<10){
	            		hours=String.valueOf(bookedHours);
	            		hours="0"+hours;
	            	}else{
	            		hours=String.valueOf(bookedHours);
	            	}
	            	if(bookedMinute<10){
	            		minutes=String.valueOf(bookedMinute);
	            		minutes="0"+minutes;
	            	}else{
	            		minutes=String.valueOf(bookedMinute);
	            	}
	            	
	            	if(bookedMinute<10){
	            		seconds=String.valueOf(bookedSecond);
	            		seconds="0"+seconds;
	            	}else{
	            		seconds=String.valueOf(bookedSecond);
	            	}
	            	if(AMPM==0){//If the current time is AM
	            		this.timeHours=strBookingDate+" "+hours+" : "+minutes+" : "+seconds+" AM ";
	            	}else if(AMPM==1){//If the current time is PM
	            		this.timeHours=strBookingDate+" "+hours+" : "+minutes+" : "+seconds+" PM ";
	            	}
	            	if(this.mailSource == true){
	            		if(this.sourceId==12){
	            			this.sourceName = "offline";
	            		}else{
	            			this.sourceName = "resort";
	            		}
	            	}else{
	            		if(this.sourceId==12){
	            			this.sourceName = "offline";
	            		}else{
	            			this.sourceName = "online";
	            		}
	            		
	            	}
	            	
	            	
	                
	            	this.arrivalDate = booked.getArrivalDate();
	            	this.departureDate = booked.getDepartureDate();
	            	PmsGuest guest = guestController.find(booked.getGuestId());
	            	this.firstName = guest.getFirstName();
	            	this.lastName = guest.getLastName();
	            	this.phone = guest.getPhone();
	            	this.emailId = guest.getEmailId();
	            	this.specialRequest = booked.getSpecialRequest();
	            	if(this.specialRequest==null){
	            		this.specialRequest="Not Available";
	            	}
	            	
	            	String checkIn = new SimpleDateFormat("MM/dd/yyyy").format(this.arrivalDate);
					String checkOut = new SimpleDateFormat("MM/dd/yyyy").format(this.departureDate);
					
					LocalDate date1 = LocalDate.parse(checkIn, DateTimeFormat.forPattern("MM/dd/yyyy"));
	                LocalDate date2 = LocalDate.parse(checkOut, DateTimeFormat.forPattern("MM/dd/yyyy"));
	                this.days = Days.daysBetween(date1, date2).getDays();
	            	
                 this.bookList =  bookingDetailController.listAccommodation(getBookingId(),booked.getAccommodationId());
                 if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += ",{";
					
					else
						jsonOutput += "{";
                 
                 	String jsonTypes="";
             		jsonOutput += "\"bookingid\":\"" + getBookingId().toString() + "\"";
             	
             		for (BookedDetail bookedDetail : bookList) {
	            		
		            	PropertyAccommodation accommodation = propertyAccommodationController.find(bookedDetail.getAccommodationId());
		            	this.roomCount = bookedDetail.getRoomCount()/this.days;
		            	
		            	this.dblTax=bookedDetail.getTax();
	                    String strTax=df.format(this.dblTax);
	                    this.dblTax=Double.valueOf(strTax);
	                    
	                    
	                    this.dblAmount=bookedDetail.getAmount();
	                    String strAmount=df.format(this.dblAmount);
	                    this.dblAmount=Double.valueOf(strAmount);
	                    
		            	accommodationBooked.add(new BookedDetail(accommodation.getAccommodationType(),this.dblTax,
		            			this.roomCount,bookedDetail.getAdultCount(),bookedDetail.getChildCount() ,this.dblAmount)); 
		            	 
		            	if (!jsonTypes.equalsIgnoreCase(""))
							jsonTypes += ",{";
		            	
						else
							jsonTypes += "{";
	            		
		            	
		            	smsAccommodationTypes.append(accommodation.getAccommodationType().trim());
		            	smsAccommodationTypes.append(",");
	            		jsonTypes += "\"accommodationType\":\"" + accommodation.getAccommodationType().trim()+ "\"";
	            		jsonTypes += ",\"rooms\":\"" + this.roomCount+ "\"";
	            		jsonTypes += ",\"adultCount\":\"" + bookedDetail.getAdultCount()+ "\"";
	            		jsonTypes += ",\"childCount\":\"" + bookedDetail.getChildCount()+ "\"";
	            		jsonTypes += ",\"amount\":\"" + this.dblAmount+ "\"";
	            		jsonTypes += ",\"tax\":\"" + this.dblTax+ "\"";
	            		jsonTypes += ",\"total\":\"" + (this.dblTax + this.dblAmount)  + "\"";
	            		jsonTypes += "}";
	            		
	         	    }
             		jsonOutput += ",\"types\":[" + jsonTypes+ "]";
	            	
	            	
	            	this.rooms = booked.getRooms();
	            	this.adults = this.totalAdultCount;
	            	this.infant = this.totalInfantCount;
	            	this.child = this.totalChildCount;
	            	this.tax = booked.getTax();
	            	this.amount = booked.getAmount();
	            	this.totalAmount = this.tax + this.amount;
	            	String strTotal=df.format(this.totalAmount);
	            	this.totalAmount=Double.valueOf(strTotal);
	            	
                    this.totalAdvanceAmount = booked.getAdvanceAmount();
                    /*if(this.totalAdvanceAmount == 0){
                    	this.hotelPay = 0;
	            	}
	            	else{
	            		this.hotelPay = (this.totalAmount-this.totalAdvanceAmount);
	            	}*/
                    this.hotelPay = (this.totalAmount-this.totalAdvanceAmount); 
	            	if(this.totalAmount == booked.getAdvanceAmount()){
	            		
	            		this.paymentStatus = "Paid";
	            	}
	            	else{
	            		if(this.mailSource == true){
	            			if(this.sourceId==12){
	            				if(this.totalAmount == booked.getAdvanceAmount()){
	        	            		this.paymentStatus = "Paid";
	        	            	}else{
	        	            		this.paymentStatus = "Partially Paid";
	        	            	}
	            			}else{
	            				this.paymentStatus = "Paid";
	            			}
		            	}else{
		            		this.paymentStatus = "Partially Paid";
		            	}
	            		
	            		
	            	}
	               
	                jsonOutput += ",\"propertyName\":\"" + this.propertyName.trim()+ "\"";
		            jsonOutput += ",\"locationName\":\"" + this.locationName.trim()+ "\"";
					jsonOutput += ",\"address1\":\"" + this.address1.trim()+ "\"";
					jsonOutput += ",\"address2\":\"" + this.address2.trim()+ "\"";
					jsonOutput += ",\"phoneNumber\":\"" + this.phoneNumber+ "\"";
					jsonOutput += ",\"propertyEmail\":\"" + this.propertyEmail.trim()+ "\"";
					jsonOutput += ",\"reservationManagerEmail\":\"" + this.reservationManagerEmail.trim()+ "\"";
					jsonOutput += ",\"reservationManagerContact\":\"" + this.reservationManagerContact.trim()+ "\"";
					
					
	            	String CheckIn = new SimpleDateFormat("MMM d,yyyy").format(this.arrivalDate);// check in 
	                String CheckOut = new SimpleDateFormat("MMM d,yyyy").format(this.departureDate);//check out
	                
	                jsonOutput += ",\"checkIn\":\"" + CheckIn+ "\"";
					jsonOutput += ",\"checkOut\":\"" + CheckOut+ "\"";
					jsonOutput += ",\"timeHours\":\"" + this.timeHours+ "\"";
					jsonOutput += ",\"days\":\"" + this.days.toString()+ "\"";
					//jsonOutput += ",\"guestname\":\"" + this.firstName+ "\"";
                     String strFirstName="",firstNameLetterUpperCase="";
					String strLastName="",lastNameLetterUpperCase="",strFullName="";
					strFirstName=this.firstName;
					strLastName=this.lastName;
					if(strFirstName!=null && strFirstName!="" && !strFirstName.equalsIgnoreCase("undefined")){
						strFirstName=strFirstName.trim();
						firstNameLetterUpperCase=strFirstName.substring(0, 1).toUpperCase()+strFirstName.substring(1);
						strFirstName=firstNameLetterUpperCase;
					}else{
						strFirstName="Not Available";
					}
					if(strLastName!=null && strLastName!=""  && !strLastName.equalsIgnoreCase("undefined")){
						strLastName=strLastName.trim();
						lastNameLetterUpperCase=strLastName.substring(0, 1).toUpperCase() +  strLastName.substring(1);
						strLastName=lastNameLetterUpperCase;
					}else{
						strLastName="";
					}
					strFullName=strFirstName+" "+strLastName;
					
					 this.promotionName = (String) sessionMap.get("promotionName");
					    if(promotionName==null || promotionName==""){
					    	promotionName="NA";
					    }
					    if(this.propertyDiscountId == 35){
					    	this.propertyDiscountName = "NA";
					    }
					
					jsonOutput += ",\"guestname\":\"" + strFullName.trim()+ "\"";
					jsonOutput += ",\"sourcename\":\"" + this.sourceName.trim()+ "\"";
					jsonOutput += ",\"paymentstatus\":\"" + this.paymentStatus+ "\"";
					jsonOutput += ",\"specialrequest\":\"" + this.specialRequest.trim()+ "\"";
					jsonOutput += ",\"promotionName\":\"" + promotionName.trim()+ "\"";
					jsonOutput += ",\"propertyDiscountName\":\"" + this.propertyDiscountName.trim() + "\"";
					jsonOutput += ",\"mobile\":\"" + this.phone+ "\"";
					jsonOutput += ",\"emailId\":\"" + this.emailId.trim()+ "\"";
					jsonOutput += ",\"rooms\":\"" + this.rooms.toString()+ "\"";
					jsonOutput += ",\"adults\":\"" + String.valueOf(this.adults)+ "\"";
					jsonOutput += ",\"child\":\"" + String.valueOf(this.child)+ "\"";
					jsonOutput += ",\"tax\":\"" + df.format(this.tax) + "\"";
					jsonOutput += ",\"amount\":\"" + df.format(this.amount)+ "\"";
					jsonOutput += ",\"totalamount\":\"" + df.format(this.totalAmount)+ "\"";
					//jsonOutput += ",\"totaladvanceamount\":\"" + Double.valueOf(this.totalAdvanceAmount)+ "\"";
					
					/*if(getDiscountId()!=null){
						jsonOutput += ",\"totalamount\":\"" + String.valueOf(this.txtGrandTotal)+ "\"";
					}else{
						jsonOutput += ",\"totalamount\":\"" + String.valueOf(this.totalAmount)+ "\"";	
					}*/
					
					
					
					
					jsonOutput += "}";

				}
	           
	                String checkIn = new SimpleDateFormat("MM/dd/yyyy").format(this.arrivalDate);
				    String checkOut = new SimpleDateFormat("MM/dd/yyyy").format(getDepartureDate());
				    this.promotionName = (String) sessionMap.get("promotionName");
				    if(promotionName==null || promotionName==""){
				    	promotionName="NA";
				    }
				    
				    String strFullName="";
				    
	            if(this.mailFlag == true)
	            {
	            	try
	 				{
	 				//email template
	 				Configuration cfg = new Configuration();
	 				cfg.setClassForTemplateLoading(BookingDetailAction.class, "../../../");
	 				Template template = cfg.getTemplate(getText("reservation.template"));
	 				Map<String, Object> rootMap = new HashMap<String, Object>();
	 				//Map<String, String> rootMap = new HashMap<String, String>();	 				
	 				rootMap.put("bookingid",getBookingId().toString());
	 				rootMap.put("propertyname",this.propertyName);
	 				rootMap.put("propertyid",this.propertyId);
	 				rootMap.put("latitude",this.latitude.trim());
	 				rootMap.put("longitude",this.longitude.trim());
                    rootMap.put("sourcename",this.sourceName);
	 				rootMap.put("paymentstatus",this.paymentStatus);
	 				rootMap.put("propertyDiscountName",this.propertyDiscountName);
	 				rootMap.put("reservationManagerEmail",this.reservationManagerEmail);
	 				rootMap.put("reservationManagerContact",this.reservationManagerContact);
	 				rootMap.put("propertyEmail",this.propertyEmail);
	 				rootMap.put("specialrequest",this.specialRequest);
	 				rootMap.put("guestname",this.firstName);
	 				rootMap.put("locationName",this.locationName);
	 				rootMap.put("address1",this.address1);
	 				rootMap.put("address2",this.address2);
	 				rootMap.put("hotelpay",this.hotelPay);
	 				rootMap.put("phoneNumber",this.phoneNumber);
	 				rootMap.put("checkIn", checkIn);
	 				rootMap.put("checkOut",checkOut);
	 				rootMap.put("timeHours",this.timeHours);
	 				
	 				String strFirstName="",firstNameLetterUpperCase="";
					String strLastName="",lastNameLetterUpperCase="";
					
					strFirstName=this.firstName;
					strLastName=this.lastName;
					if(strFirstName!=null && strFirstName!="" && !strFirstName.equalsIgnoreCase("undefined")){
						strFirstName=strFirstName.trim();
						firstNameLetterUpperCase=strFirstName.substring(0, 1).toUpperCase()+strFirstName.substring(1);
						strFirstName=firstNameLetterUpperCase;
					}else{
						strFirstName="Not Available";
					}
					if(strLastName!=null && strLastName!=""  && !strLastName.equalsIgnoreCase("undefined")){
						strLastName=strLastName.trim();
						lastNameLetterUpperCase=strLastName.substring(0, 1).toUpperCase() +  strLastName.substring(1);
						strLastName=lastNameLetterUpperCase;
					}else{
						strLastName="";
					}
					strFullName=strFirstName+" "+strLastName;
	 				rootMap.put("guestname",strFullName);
	 				//rootMap.put("guestname",this.firstName + " " + this.lastName);
	 				rootMap.put("mobile",this.phone);
	 				rootMap.put("emailId",this.emailId);	 				
	 				rootMap.put("checkin",checkIn);
	 				rootMap.put("days",this.days.toString());
	 				rootMap.put("checkout",checkOut);
	 				rootMap.put("promotionName",promotionName);	 				
	 				
	            	
	 				rootMap.put("accommodationBooked", accommodationBooked);
	 				//rootMap.put("rooms", this.roomCount);
	 				rootMap.put("adults", String.valueOf(this.adults));
	 				rootMap.put("child",String.valueOf(this.child));
	 				rootMap.put("infant", String.valueOf(this.infant));
	 				rootMap.put("tax",df.format(this.tax));
	 				rootMap.put("amount",df.format(this.amount));
	 				rootMap.put("totalamount",df.format(this.totalAmount));
	 				rootMap.put("totaladvanceamount",df.format(this.totalAdvanceAmount));
	 				
	 				//rootMap.put("totalamount",String.valueOf(this.txtGrandTotal));
	 				
	 				rootMap.put("from", getText("notification.from"));
	 				Writer out = new StringWriter();
	 				template.process(rootMap, out);

	 				String strPropertyMailId=null,strResortMailId=null,strContractMailId=null,strRevenueMailId=null;
	 				PmsPropertyManager pmsPropertyController=new PmsPropertyManager();
	 				for (PmsBookedDetails booked : bookedList) {
	 					PmsProperty pmsProperty=pmsPropertyController.find(booked.getPropertyId());
	 					
	 					this.reservationManagerEmail=pmsProperty.getReservationManagerEmail();
	 					strResortMailId=pmsProperty.getResortManagerEmail();
	 					this.propertyEmail=pmsProperty.getPropertyEmail();
	 					strRevenueMailId=pmsProperty.getRevenueManagerEmail();
	 					strContractMailId=pmsProperty.getContractManagerEmail();
	 				}
	 				
	 				
	 				String[] arrString=null;
	 				ArrayList<String> arlToken=new ArrayList<String>();
	 				StringTokenizer stoken=new StringTokenizer(this.reservationManagerEmail,",");
	 				String strReservationMailId1=null,strReservationMailId2=null,strReservationMailId3=null,strReservationMailId4=null,strReservationMailId5=null;
	 				while(stoken.hasMoreElements()){
	 					arlToken.add(stoken.nextToken());
	 				}
	 				
	 				int intCount=0, arlSize=0;
	 				arrString=new String[arlToken.size()];
	 				Iterator<String> iterListValues=arlToken.iterator();
	 			    while(iterListValues.hasNext()){
	 		    		String strTemp=iterListValues.next();
	 		    		arrString[intCount]=strTemp.trim();
	 		    		intCount++;		
	 			    }
	 			    
	 				int i=0;
	 				arlSize=arlToken.size();
	 				for(String s : arrString) {
	 			          String[] s2 = s.split(" ");
	 			          for(String results : s2) {
	 			        	  if(i==0){
	 			        		 strReservationMailId1=results;
	 			        	  }else if(i==1){
	 			        		 strReservationMailId2=results;
	 			        	  }else if(i==2){
	 			        		 strReservationMailId3=results;
	 			        	  }else if(i==3){
	 			        		 strReservationMailId4=results;
	 			        	  }else if(i==4){
	 			        		 strReservationMailId5=results;
	 			        	  }else if(i==5){
	 			        		  break;
	 			        	  }
	 			        	
	 			        	 i++;
	 			        	 
	 			        	
	 			          }
	 			      }
	 				
	 				if(strReservationMailId2==null){
	 					strReservationMailId2=strReservationMailId1;
	 				}
	 				if(strReservationMailId3==null){
	 					strReservationMailId3=strReservationMailId1;
	 				}
	 				if(strReservationMailId4==null){
	 					strReservationMailId4=strReservationMailId1;
	 				}
	 				if(strReservationMailId5==null){
	 					strReservationMailId5=strReservationMailId1;
	 				}
	 				
	 				String[] arrString2=null;
					ArrayList<String> arlToken2=new ArrayList<String>();
					StringTokenizer stoken2=new StringTokenizer(this.propertyEmail,",");
					String strPropertyMailId1=null,strPropertyMailId2=null,strPropertyMailId3=null,
							strPropertyMailId4=null,strPropertyMailId5=null;
					while(stoken2.hasMoreElements()){
						arlToken2.add(stoken2.nextToken());
					}
					
					int intCount2=0, arlSize2=0;
					arrString2=new String[arlToken2.size()];
					Iterator<String> iterListValues2=arlToken2.iterator();
				    while(iterListValues2.hasNext()){
			    		String strTemp=iterListValues2.next();
			    		arrString2[intCount2]=strTemp.trim();
			    		intCount2++;		
				    }
				    
					int k=0;
					arlSize2=arlToken2.size();
					for(String s : arrString2) {
				          String[] s2 = s.split(" ");
				          for(String results : s2) {
				        	  if(k==0){
				        		 strPropertyMailId1=results;
				        	  }else if(k==1){
				        		 strPropertyMailId2=results;
				        	  }else if(k==2){
				        		 strPropertyMailId3=results;
				        	  }else if(k==3){
				        		 strPropertyMailId4=results;
				        	  }else if(k==4){
				        		 strPropertyMailId5=results;
				        	  }else if(k==5){
				        		  break;
				        	  }
				        	
				        	 k++;
				        	 
				        	
				          }
				      }
					
					if(strPropertyMailId2==null){
						strPropertyMailId2=strPropertyMailId1;
					}
					if(strPropertyMailId3==null){
						strPropertyMailId3=strPropertyMailId1;
					}
					if(strPropertyMailId4==null){
						strPropertyMailId4=strPropertyMailId1;
					}
					if(strPropertyMailId5==null){
						strPropertyMailId5=strPropertyMailId1;
					}
	 				
	 				
	 				
	 				String strSubject="Hotel Confirmation for Booking Id "+getBookingId().toString()+" "+this.propertyName;
	 				//send email
	 				Email em = new Email();
	 				em.set_to(this.emailId);
	 				em.set_cc(strContractMailId);
	 				em.set_cc2(strRevenueMailId);
	 				em.set_cc3(strReservationMailId1);
	 				em.set_cc4(getText("bookings.notification.email"));
	 				em.set_cc5(getText("operations.notification.email"));
	 				em.set_cc6(getText("finance.notification.email"));
	 				em.set_cc7(strPropertyMailId2);
	 				em.set_cc8(strReservationMailId2);
	 				em.set_cc9(strPropertyMailId3);
	 				em.set_cc10(strReservationMailId3);
	 				em.set_cc11(strPropertyMailId4);
	 				em.set_cc12(strReservationMailId4);
	 				em.set_cc13(strPropertyMailId5);
	 				em.set_cc14(strReservationMailId5);
	 				
	 				em.set_from(getText("email.from"));
	 				//em.set_host(getText("email.host"));
	 				//em.set_password(getText("email.password"));
	 				//em.set_port(getText("email.port"));
	 				em.set_username(getText("email.username"));
	 				em.set_subject(strSubject);
	 				em.set_bodyContent(out);
	 				em.send();		
	 				
	 				getHotelierBookedDetails();
	 				
	 				
	 				String message = "&sms_text=" + "Confirmation from Ulo Hotels"+
	    					
	    					"Booking ID - "+ getBookingId().toString() +
	    					 "Guest Name � "+ strFullName  + 
	    					 "Property Name �"+ this.propertyName +
	    					"Room Category/s �"+  smsAccommodationTypes.toString()  +
	    					 "No. of Adults �"+ this.adults +
	    					 "No. of Kids �"+ this.child +
	    					 "Check-in Date-"+ checkIn + 
	    					"Check out Date-"+ checkOut +
	    				   "Contact Number � "+ this.phone;

		    			//String sender = "&sender=" + "TXTLCL";
		    			String numbers = "&sms_to=" + "+91"+this.phone;
		    			String from = "&sms_from=" + "ULOHTL";
		    			String type = "&sms_type=" + "trans"; 
		    			// Send data
		    			HttpURLConnection conn = (HttpURLConnection) new URL("https://secureets.knowlarity.com/ets/dom/clients/ulohotels/api/ulohotels_sms_api.py").openConnection();
		    			//"http://etsrds.kapps.in/webapi/ulohotels/api/ulohotels_sms_api.py?sms_text=hi%20this%20gopinath%20from%20ulo&sms_to=%20918220211068&sms_from=ULOHTL&sms_type=promo"
		    			String data = numbers + message +  from + type;
		    			conn.setDoOutput(true);
		    			conn.setRequestMethod("POST");
		    			conn.addRequestProperty("auth_key", "Basic dWxvaG90ZWxzX1Q6VUxPSFRMQDIwMTg=");
		    			conn.setRequestProperty("Content-Length", Integer.toString(data.length()));
		    			conn.getOutputStream().write(data.getBytes("UTF-8"));
		    			final BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		    			final StringBuffer stringBuffer = new StringBuffer();
		    			/*String line;
		    			
		    			while ((line = rd.readLine()) != null) {
		    				stringBuffer.append(line);
		    				
		    			}*/
		    			rd.close();
		    			
		    			getRouteMapSms(this.routeMap,this.phone);
	    			
	 				}
	 				catch(Exception e1)
	 				{
	 					e1.printStackTrace();
	 				}
	 		     
	            }
	            
				response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
				

			} catch (Exception e) {
				logger.error(e);
				e.printStackTrace();
			} finally {

			}

//			return null;
		}
	  
	 
	 public void getOfflineGuestBookedDetails(int bookingId) throws IOException {//offline bookings
		 try {
			String strFullName="";
			//HttpServletResponse response = ServletActionContext.getResponse();
			DecimalFormat df = new DecimalFormat("###.##");
			DateFormat f = new SimpleDateFormat("EEEE");
			DateFormat f1 = new SimpleDateFormat("EEE");
		    SimpleDateFormat sdfformat=new SimpleDateFormat("MMM dd");
		    SimpleDateFormat sdfformat1=new SimpleDateFormat("MMM dd, yyyy");
			PmsPropertyManager propertyController = new PmsPropertyManager();
			PropertyAccommodationManager propertyAccommodationController = new PropertyAccommodationManager();
			BookingDetailManager bookingDetailController = new BookingDetailManager();
			BookingGuestDetailManager bookingGuestDetailController = new BookingGuestDetailManager();
			PmsGuestManager guestController = new PmsGuestManager();
			PmsStatusManager statusController = new PmsStatusManager();
			PmsSourceManager sourceController = new PmsSourceManager();
			PropertyDiscountManager propertyDiscountController = new PropertyDiscountManager();  
			PmsBookingManager bookingController = new PmsBookingManager();
			PmsTagManager tagController=new PmsTagManager();
			//response.setContentType("application/json");
			this.guests=0;this.adults=0;this.child=0;
			this.bookedList = bookingDetailController.bookedList(bookingId);
			
			
			StringBuilder smsAccommodationTypes = new StringBuilder();
            for (PmsBookedDetails booked : bookedList) {
            	this.bookingId = bookingId;
            	this.sourceId=booked.getSourceId();
            	PmsProperty pmsProperty = propertyController.find(booked.getPropertyId());
            	this.propertyName = pmsProperty.getPropertyName();//resort name and location name is missing
            	this.propertyId = pmsProperty.getPropertyId();
            	this.latitude = pmsProperty.getLatitude();
            	this.longitude = pmsProperty.getLongitude();
            	this.address1=pmsProperty.getAddress1();//property address1
            	this.address2=pmsProperty.getAddress2();//property address2
            	this.phoneNumber = pmsProperty.getPropertyContact();//property phone number
            	this.propertyEmail = pmsProperty.getPropertyEmail();
            	this.locationName=pmsProperty.getLocation().getLocationName();//property location
            	this.reservationManagerEmail=pmsProperty.getReservationManagerEmail();
            	this.reservationManagerContact = pmsProperty.getReservationManagerContact();
            	this.routeMap = pmsProperty.getRouteMap();
            	java.util.Date date=new java.util.Date();
            	Calendar calDate=Calendar.getInstance();
            	calDate.setTime(date);
            	calDate.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
            	int bookedHours=calDate.get(Calendar.HOUR);
            	int bookedMinute=calDate.get(Calendar.MINUTE);
            	int bookedSecond=calDate.get(Calendar.SECOND);
            	int AMPM=calDate.get(Calendar.AM_PM);
            	
            	String strBookingDate=new SimpleDateFormat("MMM d, yyyy", Locale.ENGLISH).format(calDate.getTime());
            	String hours="",minutes="",seconds="";
            	if(bookedHours<10){
            		hours=String.valueOf(bookedHours);
            		hours="0"+hours;
            	}else{
            		hours=String.valueOf(bookedHours);
            	}
            	if(bookedMinute<10){
            		minutes=String.valueOf(bookedMinute);
            		minutes="0"+minutes;
            	}else{
            		minutes=String.valueOf(bookedMinute);
            	}
            	
            	if(bookedMinute<10){
            		seconds=String.valueOf(bookedSecond);
            		seconds="0"+seconds;
            	}else{
            		seconds=String.valueOf(bookedSecond);
            	}
            	if(AMPM==0){//If the current time is AM
            		this.timeHours=f.format(calDate.getTime())+","+strBookingDate+" at "+hours+" : "+minutes+" AM ";
            	}else if(AMPM==1){//If the current time is PM
            		this.timeHours=f.format(calDate.getTime())+","+strBookingDate+" at "+hours+" : "+minutes+" PM ";
            	}
            	Calendar calBookingDate=Calendar.getInstance();
            	calBookingDate.setTime(booked.getCreatedDate());
            	calBookingDate.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
            	int bookedHours1=calBookingDate.get(Calendar.HOUR);
            	int bookedMinute1=calBookingDate.get(Calendar.MINUTE);
            	int bookedSecond1=calBookingDate.get(Calendar.SECOND);
            	int AMPM1=calBookingDate.get(Calendar.AM_PM);
            	
            	String hours1="",minutes1="",seconds1="";
            	if(bookedHours1<10){
            		hours1=String.valueOf(bookedHours1);
            		hours1="0"+hours1;
            	}else{
            		hours1=String.valueOf(bookedHours1);
            	}
            	if(bookedMinute1<10){
            		minutes1=String.valueOf(bookedMinute1);
            		minutes1="0"+minutes1;
            	}else{
            		minutes1=String.valueOf(bookedMinute1);
            	}
            	
            	if(bookedSecond1<10){
            		seconds1=String.valueOf(bookedSecond1);
            		seconds1="0"+seconds1;
            	}else{
            		seconds1=String.valueOf(bookedSecond1);
            	}
            	if(AMPM1==0){//If the current time is AM
            		this.bookingTime=hours1+" : "+minutes1+" : "+seconds1+" AM ";
            	}else if(AMPM1==1){//If the current time is PM
            		this.bookingTime=hours1+" : "+minutes1+" : "+seconds1+" PM ";
            	}
            	
            
            	if(this.sourceId==12){
         			this.sourceName = "Offline";
         		}else{
         			this.sourceName = "Online";
         		}
            	/*if(this.sourceId!=null){
            		PmsSource source=sourceController.find(sourceId);
            		this.sourceType=source.getSourceName();
            	}*/
            	
            	if(this.sourceName.equalsIgnoreCase("online")){
            		if(this.sourceId> 1 && this.sourceId!=12){
            			this.sourceType="OTA";
            		}else if(this.sourceId==12){
            			this.sourceType="Offline";
            		}else{
            			this.sourceType="Offline";
            		}
            	}else{
            		if(this.sourceId> 1 && this.sourceId!=12){
            			this.sourceType="OTA";
            		}else if(this.sourceId==12){
            			this.sourceType="Offline";
            		}else{
            			this.sourceType="Offline";
            		}
            	}
            	
            	SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
	  			Timestamp FromDate=booked.getArrivalDate();
	  			Timestamp ToDate=booked.getDepartureDate();
	  			String strFromDate=format2.format(FromDate);
	  			String strToDate=format2.format(ToDate);
	  	    	DateTime startdate = DateTime.parse(strFromDate);
	  	        DateTime enddate = DateTime.parse(strToDate);
	  	        
	  	        java.util.Date fromdate = startdate.toDate();
	  	 		java.util.Date todate = enddate.toDate();
	  	 		 
	  			int diffInDays = (int) ((todate.getTime() - fromdate.getTime()) / (1000 * 60 * 60 * 24));
	  			
            	this.arrivalDate = booked.getArrivalDate();
            	this.departureDate = booked.getDepartureDate();
            	if(booked.getTagId()!=null){
	  				PmsTags tags=tagController.find(booked.getTagId());
	  				this.tagName=tags.getTagName();
	  			}else{
	  				this.tagName="Not Applicable";
	  			}
            	this.tscheckInTime=booked.getCheckInTime();
            	this.tscheckOutTime=booked.getCheckOutTime();
            	
            	if(tscheckInTime!=null && tscheckOutTime!=null){
            		Calendar calCheckIn=Calendar.getInstance();
            		calCheckIn.setTime(this.tscheckInTime);
//            		calCheckIn.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
                	int bookedHours2=calCheckIn.get(Calendar.HOUR);
                	int bookedMinute2=calCheckIn.get(Calendar.MINUTE);
                	int bookedSecond2=calCheckIn.get(Calendar.SECOND);
                	int AMPM2=calCheckIn.get(Calendar.AM_PM);
                	
                	Calendar calCheckOut=Calendar.getInstance();
                	calCheckOut.setTime(this.tscheckOutTime);
//                	calCheckOut.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
                	int bookedHours3=calCheckOut.get(Calendar.HOUR);
                	int bookedMinute3=calCheckOut.get(Calendar.MINUTE);
                	int bookedSecond3=calCheckOut.get(Calendar.SECOND);
                	int AMPM3=calCheckOut.get(Calendar.AM_PM);
                	
                	String strFromTime=format2.format(tscheckInTime);
    	  			String strToTime=format2.format(tscheckOutTime);
    	  	    	DateTime starttime = DateTime.parse(strFromTime);
    	  	        DateTime endtime = DateTime.parse(strToTime);
    	  	        
    	  	        java.util.Date fromtime = starttime.toDate();
    	  	 		java.util.Date totime = endtime.toDate();
    	  	 		
    	  	 		if(AMPM2==0){
    	  	 			if(bookedHours2<10){
    	  	 				if(bookedHours2==0){
    	  	 					this.bookingInTime="12:"+"00AM";
    	  	 				}else{
    	  	 					this.bookingInTime="0"+bookedHours2+":"+"00AM";	
    	  	 				}
                    		
                    	}else{
                    		this.bookingInTime=bookedHours2+":"+"00AM";
                    	}
    	  	 				
                	}else if(AMPM2==1){
                		if(bookedHours2<10){
                			if(bookedHours2==0){
                				this.bookingInTime="12:"+"00PM";	
    	  	 				}else{
    	  	 					this.bookingInTime="0"+bookedHours2+":"+"00PM";
    	  	 				}
                			
                    	}else{
                    		this.bookingInTime=bookedHours2+":"+"00PM";
                    	}
                		
                	}
    	  	 		if(AMPM3==0){
    	  	 			if(bookedHours3<10){
    	  	 				if(bookedHours3==0){
    	  	 					this.bookingOutTime="12:"+"00AM";
    	  	 				}else{
    	  	 					this.bookingOutTime="0"+bookedHours3+":"+"00AM";	
    	  	 				}
                    		
                    	}else{
                    		this.bookingOutTime=bookedHours3+":"+"00AM";
                    	}
    	  	 				
    	  	 		}else if(AMPM3==1){
    	  	 			if(bookedHours3<10){
    	  	 				if(bookedHours3==0){
    	  	 					this.bookingOutTime="12:"+"00PM";	
    	  	 				}else{
    	  	 					this.bookingOutTime="0"+bookedHours3+":"+"00PM";
    	  	 				}
                    		
                    	}else{
                    		this.bookingOutTime=bookedHours3+":"+"00PM";
                    	}
    	  	 			
    	  	 		}
    	  	 		
    	  	 		long lngHours=tscheckOutTime.getTime()-tscheckInTime.getTime();
                	long diffHours = lngHours / (60 * 60 * 1000) % 24;
                	
                	this.checkIn=sdfformat.format(tscheckInTime);
                	this.checkInDays=f.format(fromtime);
                	this.checkOut=sdfformat.format(tscheckOutTime);
                	this.checkOutDays=f.format(totime);
                	
                	this.diffInHours=(int)diffHours;
                	this.voucherType="hourly-wise";
            	}else{
            		this.bookingInTime="00"+":"+"00";
    	  	 		this.bookingOutTime="00"+":"+"00";
            		this.checkIn=sdfformat.format(FromDate);
                	this.checkInDays=f.format(fromdate);
                	this.checkOut=sdfformat.format(ToDate);
                	this.checkOutDays=f.format(todate);
                	this.voucherType="day-wise";
            	}
            	this.bookingDate=sdfformat1.format(booked.getCreatedDate());
            	this.diffInDays=diffInDays;
            	
            	PmsGuest guest = guestController.find(booked.getGuestId());
            	this.firstName = guest.getFirstName();
            	this.lastName = guest.getLastName();
            	this.phone = guest.getPhone();
            	this.emailId = guest.getEmailId();
            	this.specialRequest = booked.getSpecialRequest();
            	if(this.specialRequest==null){
            		this.specialRequest="Not Available";
            	}
            	if(guest.getGstNo()==null){		
            		this.gstNo="Not Available";		
            	}else{		
            		this.gstNo=guest.getGstNo();			
            	}
            	this.promotionFirstName=booked.getPromotionFirstName()==null?"":booked.getPromotionFirstName();
            	this.promotionSecondName=booked.getPromotionSecondName()==null?"":booked.getPromotionSecondName();
            	this.couponName=booked.getCouponName()==null?"":booked.getCouponName();
            	this.promotionAmount=booked.getPromotionAmount();
            	
            	if(booked.getCouponAmount()!=null){
            		this.couponAmount=booked.getCouponAmount();	
            	}else{
            		this.couponAmount=0.0;
            	}
            	String checkIn = new SimpleDateFormat("MM/dd/yyyy").format(this.arrivalDate);
				String checkOut = new SimpleDateFormat("MM/dd/yyyy").format(this.departureDate);
				
				LocalDate date1 = LocalDate.parse(checkIn, DateTimeFormat.forPattern("MM/dd/yyyy"));
                LocalDate date2 = LocalDate.parse(checkOut, DateTimeFormat.forPattern("MM/dd/yyyy"));
                this.days = Days.daysBetween(date1, date2).getDays();
            	
                this.bookList =  bookingDetailController.listAccommodation(bookingId,booked.getAccommodationId());
             
         		for (BookedDetail bookedDetail : bookList) {
            		
	            	PropertyAccommodation accommodation = propertyAccommodationController.find(bookedDetail.getAccommodationId());
	            	this.roomCount = bookedDetail.getRoomCount()/this.days;
	            	smsAccommodationTypes.append(this.roomCount+" "+accommodation.getAccommodationType().trim());
	            	smsAccommodationTypes.append(",");
	          
         	    }
            	
            	
            	this.rooms = booked.getRooms();
            	if(this.rooms==1){
            		this.roomType=this.rooms+" room";
            	}
            	if(this.rooms>1){
            		this.roomType=this.rooms+" rooms";
            	}
            	
            	if(this.rooms>1 && this.days>1){
            		this.roomNightsType=this.rooms+" Rooms X "+this.days+" Nights";
            	}else if(this.rooms>1 && this.days==1){
            		this.roomNightsType=this.rooms+" Rooms X "+this.days+" Night";
            	}else if(this.rooms==1 && this.days>1){
            		this.roomNightsType=this.rooms+" Room X "+this.days+" Nights";
            	}else if(this.rooms==1 && this.days==1){
            		this.roomNightsType=this.rooms+" Room X "+this.days+" Night";
            	}else{
            		this.roomNightsType=this.rooms+" Room X "+this.days+" Night";
            	}
            	if(this.days>1){
            		this.roomNights=this.days+" Nights";
            	}else{
            		this.roomNights=this.days+" Night";
            	}
            	this.adults+= booked.getAdults()/this.days;
            	this.child+= booked.getChild()/this.days;
            	this.guests=(int)this.adults+(int)this.child;
            	this.tax = booked.getTax();
            	this.amount = booked.getAmount();
            	/*this.totalAmount = this.tax + this.amount;
            	String strTotal=df.format(this.totalAmount);
            	this.totalAmount=Double.valueOf(strTotal);*/
            	this.actualAmount=booked.getActualAmount();
                this.totalAdvanceAmount = booked.getAdvanceAmount();
                
                this.tariffAmount=this.actualAmount-this.couponAmount-this.promotionAmount;
                if(booked.getExtraAdultRate()==null || booked.getExtraChildRate()==0.0){
                	this.extraAdultRate=0.0;	
                }else{
                	this.extraAdultRate=booked.getExtraAdultRate();
                }
                
                
                if(booked.getExtraChildRate()==null || booked.getExtraChildRate()==0.0){
                	this.extraChildRate=0.0;	
                }else{
                	this.extraChildRate=booked.getExtraChildRate();
                }
                this.extraCharges=this.extraAdultRate+this.extraChildRate;
                this.totalAmount=this.tariffAmount+this.tax+this.extraCharges;
                
                if(booked.getAddonAmount()==null){
                	this.addonAmount=0.0;
                }else{
                	this.addonAmount=booked.getAddonAmount();
                }
                
                /*if(this.totalAdvanceAmount == 0){
                	this.hotelPay = 0;
            	}
            	else{
            		this.hotelPay = (this.totalAmount-this.totalAdvanceAmount);
            	}*/
                if(this.sourceType.equalsIgnoreCase("OTA")){
                	this.totalAmount=this.amount+this.tax;
                }else{
                	this.totalAmount=this.tariffAmount+this.tax+this.extraCharges;	
                }
                
                this.hotelPay = (this.totalAmount-this.totalAdvanceAmount); 
            	if(this.totalAmount == booked.getAdvanceAmount()){
            		this.paymentStatus = "Paid";
            	}else if(this.totalAdvanceAmount==0){
            		this.paymentStatus = "Hotel Pay";
            	}else{
            		this.paymentStatus="Partially Paid";
            	}
               
            	PropertyTaxeManager propertytaxController = new PropertyTaxeManager();
				 double taxe =  this.dblHotelPayable/this.days;
					
				 PropertyTaxe tax = propertytaxController.find(taxe);
				 this.dbltaxes = Math.round(this.dblHotelPayable * tax.getTaxPercentage() / 100  ) ;
				 this.dblHotelTaxPayable=this.dblHotelPayable+this.dbltaxes; //Payable to Hotelier
			
                
                 String strFirstName="",firstNameLetterUpperCase="";
				String strLastName="",lastNameLetterUpperCase="";
				strFirstName=this.firstName;
				strLastName=this.lastName;
				if(strFirstName!=null && strFirstName!="" && !strFirstName.equalsIgnoreCase("undefined")){
					strFirstName=strFirstName.trim();
					firstNameLetterUpperCase=strFirstName.substring(0, 1).toUpperCase()+strFirstName.substring(1);
					strFirstName=firstNameLetterUpperCase;
				}else{
					strFirstName="Not Available";
				}
				if(strLastName!=null && strLastName!=""  && !strLastName.equalsIgnoreCase("undefined")){
					strLastName=strLastName.trim();
					lastNameLetterUpperCase=strLastName.substring(0, 1).toUpperCase() +  strLastName.substring(1);
					strLastName=lastNameLetterUpperCase;
				}else{
					strLastName="";
				}
				strFullName=strFirstName+" "+strLastName;
				 				
				

			}
           
			    
			    this.grandTotal=this.totalAmount;
			    
			    String strContractMailId=null,strRevenueMailId=null;
 				PmsPropertyManager pmsPropertyController=new PmsPropertyManager();
 				for (PmsBookedDetails booked : bookedList) {
 					PmsProperty pmsProperty=pmsPropertyController.find(booked.getPropertyId());
 					
 					this.reservationManagerEmail=pmsProperty.getReservationManagerEmail();
 					this.propertyEmail=pmsProperty.getPropertyEmail();
 					strRevenueMailId=pmsProperty.getRevenueManagerEmail();
 					strContractMailId=pmsProperty.getContractManagerEmail();
 					this.propertyUrl=pmsProperty.getPropertyUrl();
 					if(pmsProperty.getGoogleLocation()!=null){
 						this.locationId=pmsProperty.getGoogleLocation().getGoogleLocationId();
 	 					this.locationUrl=pmsProperty.getGoogleLocation().getGoogleLocationUrl();
 	 					
 	 					this.galleryPath=this.locationUrl+"/"+this.propertyUrl+"?tripTypeId=1";	
 					}else{
 						this.locationId=pmsProperty.getGoogleLocation().getGoogleLocationId();
 	 					this.locationUrl=pmsProperty.getGoogleLocation().getGoogleLocationUrl();
 	 					
 	 					this.galleryPath=this.propertyUrl;
 					}
 				}
 				
 				
 				ArrayList<String> arlEmail=new ArrayList<String>();
 				
 				StringTokenizer stoken=new StringTokenizer(this.reservationManagerEmail,",");
 				while(stoken.hasMoreElements()){
 					arlEmail.add(stoken.nextToken());
 				}
 				
				/*StringTokenizer stoken2=new StringTokenizer(this.propertyEmail,",");
				while(stoken2.hasMoreElements()){
					arlEmail.add(stoken2.nextToken());
				}*/
				
				
				StringTokenizer stoken3=new StringTokenizer(strRevenueMailId,",");
 				while(stoken3.hasMoreElements()){
 					arlEmail.add(stoken3.nextToken());
 				}
 				
 				StringTokenizer stoken4=new StringTokenizer(strContractMailId,",");
 				while(stoken4.hasMoreElements()){
 					arlEmail.add(stoken4.nextToken());
 				}
 				arlEmail.add("support@ulohotels.com");
 				arlEmail.add("vinodhini@ulohotels.com");
 				arlEmail.add("finance@ulohotels.com");
 				
 				
 				Integer emailSize=arlEmail.size();

 				//email template
 				Configuration cfg = new Configuration();
 				cfg.setClassForTemplateLoading(BookingDetailAction.class, "../../../");
 				Template template = null;
 				if(this.voucherType.equalsIgnoreCase("hourly-wise")){
 					template = cfg.getTemplate("guestofflinebookinghourlyvoucher.ftl");	
 				}else{
 					template = cfg.getTemplate("guestofflinebookingvoucher.ftl");
 				}
 				Map<String, Object> rootMap = new HashMap<String, Object>();
 				//Map<String, String> rootMap = new HashMap<String, String>();	 				
 				rootMap.put("bookingId",this.bookingId.toString());
 				rootMap.put("propertyName",this.propertyName);
 				rootMap.put("propertyid",this.propertyId);
 				rootMap.put("latitude",this.latitude.trim());
 				rootMap.put("longitude",this.longitude.trim());
 				rootMap.put("gstno",this.gstNo);
                rootMap.put("sourceName",this.sourceName);
 				rootMap.put("paymentStatus",this.paymentStatus);
 				rootMap.put("promotionFirstName",this.promotionFirstName);
 				rootMap.put("promotionSecondName",this.promotionSecondName);
 				rootMap.put("couponName",this.couponName);
 				rootMap.put("promotionAmount",this.promotionAmount);
 				rootMap.put("couponAmount",this.couponAmount);
 				rootMap.put("reservationManagerEmail",this.reservationManagerEmail);
 				rootMap.put("reservationManagerContact",this.reservationManagerContact);
 				rootMap.put("propertyEmail",this.propertyEmail);
 				rootMap.put("specialrequest",this.specialRequest);
 				rootMap.put("guestName",strFullName);
 				rootMap.put("locationName",this.locationName);
 				rootMap.put("address1",this.address1);
 				rootMap.put("address2",this.address2);
 				rootMap.put("hotelpay",this.hotelPay);
 				rootMap.put("phoneNumber",this.phoneNumber);
 				rootMap.put("checkIn", this.checkIn);
 				rootMap.put("checkInDays", this.checkInDays);
 				rootMap.put("checkOut",this.checkOut);
 				rootMap.put("checkOutDays",this.checkOutDays);
 				rootMap.put("timeHours",this.timeHours);
 				rootMap.put("bookingDate",this.bookingDate);
 				rootMap.put("bookingTime",this.bookingTime);
 				rootMap.put("reservationEmail",this.reservationManagerEmail);
 				rootMap.put("reservationContact",this.reservationManagerContact);
 				rootMap.put("tagName", this.tagName);
 				rootMap.put("emailId",this.emailId);
 				rootMap.put("mobile",this.phone);
 				rootMap.put("days",this.days.toString());
 				rootMap.put("roomnighttype", this.roomNightsType);
 				rootMap.put("hours", this.diffInHours);
				rootMap.put("roomNights", this.roomNights);
 				rootMap.put("propertyUrl",this.galleryPath);
 				rootMap.put("bookingInTime",this.bookingInTime);
 				rootMap.put("bookingOutTime",this.bookingOutTime);
 				rootMap.put("accommodationType", smsAccommodationTypes);
 				rootMap.put("adults", String.valueOf(this.adults));
 				rootMap.put("child",String.valueOf(this.child));
 				rootMap.put("guests",String.valueOf(this.guests));
 				rootMap.put("roomType", this.roomType);
 				rootMap.put("tax",df.format(this.tax));
 				rootMap.put("amount",df.format(this.amount));
 				rootMap.put("totalamount",df.format(this.grandTotal));
 				rootMap.put("actualamount",df.format(this.actualAmount));
 				rootMap.put("totaladvanceamount",df.format(this.totalAdvanceAmount));
 				rootMap.put("hotelTariff",df.format(this.dblHotelPayable));
 				rootMap.put("hotelTax",df.format(this.dbltaxes));
 				rootMap.put("tariffamount",df.format(this.tariffAmount));
 				rootMap.put("extracharges",df.format(this.extraCharges));
 				
 				rootMap.put("from", "ULO Hotels Support");
 				Writer out = new StringWriter();
 				template.process(rootMap, out);

 				
 				
 				String strSubject=null;
 				if(this.mailSubjectFlag){
 					strSubject="Booking Modification for Booking Id "+getBookingId().toString()+" "+this.propertyName;
 				}else{
 					strSubject="Hotel Confirmation for Booking Id "+getBookingId().toString()+" "+this.propertyName;
 				}
 				
 				//send email
 				Email em = new Email();
 				em.set_to(this.emailId);
 				int i=0;
 				String emailArray[]=new String[emailSize];
 				Iterator<String> iterEmail=arlEmail.iterator();
 				while(iterEmail.hasNext()){
 					emailArray[i]=iterEmail.next().trim();
 					i++;
 				}

 				/*Writer fileWriter = new FileWriter (new File("E:/guest-booking-template-input.xhtml"));
 	            template.process(rootMap, fileWriter);
 	            fileWriter.flush();
 	            fileWriter.close();
 	            String inputFile="E:/guest-booking-template-input.xhtml";
 	            String outputFile="E:/guest-booking-template-output.html";
 	            File file = new File(inputFile);
 	            FileOutputStream fos = null;
 	            FileInputStream fis = null;
 	            FileWriter outputStream = null;
 	            
 	            fos = new FileOutputStream(outputFile);
 	            fis = new FileInputStream(file);
 	            outputStream = new FileWriter(outputFile);
 	            Tidy tidy = new Tidy(); //HTML parser and pretty printer.
 	            tidy.setXHTML(true); //true if tidy should output XHTML
 	            
 	            tidy.parse(fis, fos);
 				//out.flush();
 				createPdf(out.toString(), getText("storage.pdf.booking.guests.path"));
 				em.set_fileName(getText("storage.pdf.booking.guests.path"));
 				*/
 				em.set_from("donotreply@ulohotels.com");
 				em.set_username("Support");
 				em.set_subject(strSubject);
 				em.set_bodyContent(out);
 				em.set_emailSize(emailSize);
 				em.setEmailArray(emailArray);
 				em.sendBookingVoucher();		
 				
 				getOfflineHotelierGuestBookedDetails(bookingId);
 				
 				
 				String message = "&sms_text=" + "Confirmation from Ulo Hotels"+
    					
    					"Booking ID - "+ getBookingId().toString() +
    					 "Guest Name � "+ strFullName  + 
    					 "Property Name �"+ this.propertyName +
    					"Room Category/s �"+  smsAccommodationTypes.toString()  +
    					 "No. of Adults �"+ this.adults +
    					 "No. of Kids �"+ this.child +
    					 "Check-in Date-"+ checkIn + 
    					"Check out Date-"+ checkOut +
    				   "Contact Number � "+ this.phone;

    			//String sender = "&sender=" + "TXTLCL";
    			String numbers = "&sms_to=" + "+91"+this.phone;
    			String from = "&sms_from=" + "ULOHTL";
    			String type = "&sms_type=" + "trans"; 
    			// Send data
    			HttpURLConnection conn = (HttpURLConnection) new URL("https://secureets.knowlarity.com/ets/dom/clients/ulohotels/api/ulohotels_sms_api.py").openConnection();
    			//"http://etsrds.kapps.in/webapi/ulohotels/api/ulohotels_sms_api.py?sms_text=hi%20this%20gopinath%20from%20ulo&sms_to=%20918220211068&sms_from=ULOHTL&sms_type=promo"
    			String data = numbers + message +  from + type;
    			conn.setDoOutput(true);
    			conn.setRequestMethod("POST");
    			conn.addRequestProperty("auth_key", "Basic dWxvaG90ZWxzX1Q6VUxPSFRMQDIwMTg=");
    			conn.setRequestProperty("Content-Length", Integer.toString(data.length()));
    			conn.getOutputStream().write(data.getBytes("UTF-8"));
    			final BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
    			final StringBuffer stringBuffer = new StringBuffer();
    			/*String line;
    			
    			while ((line = rd.readLine()) != null) {
    				stringBuffer.append(line);
    				
    			}*/
    			rd.close();
    			
    			getRouteMapSms(this.routeMap,this.phone);
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

	}
	  
	  
	  
	public String getBookingDetails() throws IOException {
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			BookingDetailManager bookingDetailController = new BookingDetailManager();
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");

			BookingDetail bookingDetail = bookingDetailController.find(getBookingDetailsId());
			
				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"DT_RowId\":\"" + bookingDetail.getBookingDetailsId() + "\"";
				jsonOutput += ",\"accommodatioId\":\"" + bookingDetail.getPropertyAccommodation().getAccommodationId()+ "\"";
				jsonOutput += ",\"adultCount\":\"" + bookingDetail.getAdultCount()+ "\"";
				jsonOutput += ",\"childCount\":\"" +bookingDetail.getChildCount()+ "\"";
				jsonOutput += ",\"amount\":\"" + bookingDetail.getAmount() + "\"";
				jsonOutput += ",\"refund\":\"" + bookingDetail.getRefund() + "\"";
				jsonOutput += ",\"statusId\":\"" + bookingDetail.getPmsStatus().getStatusId() + "\"";
				jsonOutput += ",\"tax\":\"" + bookingDetail.getTax() + "\"";
				jsonOutput += ",\"bookingId\":\"" + bookingDetail.getPmsBooking().getBookingId() + "\"";
				
				jsonOutput += "}";

			

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	public String getAllBookingDetails() throws IOException {
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			BookingDetailManager bookingDetailController = new BookingDetailManager();
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");

			this.bookingDetailList = bookingDetailController.list();
			for (BookingDetail bookingDetail : bookingDetailList) {

				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"DT_RowId\":\"" + bookingDetail.getBookingDetailsId() + "\"";
				jsonOutput += ",\"accommodatioId\":\"" + bookingDetail.getPropertyAccommodation().getAccommodationId()+ "\"";
				jsonOutput += ",\"adultCount\":\"" + bookingDetail.getAdultCount()+ "\"";
				jsonOutput += ",\"childCount\":\"" +bookingDetail.getChildCount()+ "\"";
				jsonOutput += ",\"amount\":\"" + bookingDetail.getAmount() + "\"";
				jsonOutput += ",\"refund\":\"" + bookingDetail.getRefund() + "\"";
				jsonOutput += ",\"statusId\":\"" + bookingDetail.getPmsStatus().getStatusId() + "\"";
				jsonOutput += ",\"tax\":\"" + bookingDetail.getTax() + "\"";
				jsonOutput += ",\"bookingId\":\"" + bookingDetail.getPmsBooking().getBookingId() + "\"";
				
				jsonOutput += "}";

			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
     public String getBookingIdDetails() throws IOException {
		
		try {
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			this.filterBookingId=getFilterBookingId();
			DecimalFormat df = new DecimalFormat("###.##");
			ReportManager reportController = new ReportManager();
			PmsBookingManager bookingController = new PmsBookingManager();
			BookingDetailManager detailController = new BookingDetailManager();
			PmsGuestManager guestController = new PmsGuestManager();
			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
			PmsPropertyManager pmsPropertyController=new PmsPropertyManager();
			
			PmsStatusManager statusController=new PmsStatusManager();
			BookingGuestDetailManager bookingGuestController = new BookingGuestDetailManager();
			PropertyDiscountManager discountController=new PropertyDiscountManager();
			PmsProperty pmsProperty=new PmsProperty();
			
			response.setContentType("application/json");
			this.listBookingDetails=reportController.listBookingDetail(Integer.parseInt(this.filterBookingId));
			if(listBookingDetails.size()>0 && !listBookingDetails.isEmpty()){
				for(BookingDetailReport bookingDetailReport:listBookingDetails){
					if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += ",{";
					else
						jsonOutput += "{";
					
					jsonOutput += "\"bookingId\":\"" + bookingDetailReport.getBookingId()+ "\"";
					if(bookingDetailReport.getOtaBookingId()!=null){
						jsonOutput += ",\"otaBookingId\":\"" + bookingDetailReport.getOtaBookingId()+ "\"";
					}else{
						jsonOutput += ",\"otaBookingId\":\"" + bookingDetailReport.getBookingId()+ "\"";
					}
					PmsBooking booking = bookingController.find(bookingDetailReport.getBookingId());
					jsonOutput += ",\"addonDescription\":\"" + (booking.getAddonDescription()== null ? "NA" : booking.getAddonDescription())+ "\"";
					jsonOutput += ",\"addonAmount\":\"" +(booking.getAddonAmount()== null ? 0 : booking.getAddonAmount()) + "\"";
					PmsProperty property = pmsPropertyController.find(bookingDetailReport.getPropertyId());
					jsonOutput += ",\"propertyName\":\"" + property.getPropertyName()+ "\"";
					jsonOutput += ",\"propertyId\":\"" + property.getPropertyId()+ "\"";
					String strFirstName="",firstNameLetterUpperCase="";
					String strLastName="",lastNameLetterUpperCase="";
					strFirstName=bookingDetailReport.getFirstName();
					strLastName=bookingDetailReport.getLastName();
					if(strFirstName!=null && strFirstName!="" && !strFirstName.equalsIgnoreCase("undefined")){
						strFirstName=strFirstName.trim();
						firstNameLetterUpperCase=strFirstName.substring(0, 1).toUpperCase()+strFirstName.substring(1);
						strFirstName=firstNameLetterUpperCase;
					}else{
						strFirstName="-";
					}
					if(strLastName!=null && strLastName!=""  && !strLastName.equalsIgnoreCase("undefined")){
						strLastName=strLastName.trim();
						lastNameLetterUpperCase=strLastName.substring(0, 1).toUpperCase() +  strLastName.substring(1);
						strLastName=lastNameLetterUpperCase;
					}else{
						strLastName="";
					}
					jsonOutput += ",\"guestId\":\"" + bookingDetailReport.getGuestId() + "\"";
					jsonOutput += ",\"specialRequest\":\"" + bookingDetailReport.getSpecialRequest() + "\"";
					jsonOutput += ",\"guestName\":\"" + strFirstName+" "+strLastName + "\"";
					
					
					Double dblBaseAmount=0.0,dblTaxAmount=0.0,dblBaseTaxAmount=0.0,dblTotalAmount=0.0,
							dblAdvanceAmount=0.0,dblDueAmount=0.0,dblTotalBaseAmount=0.0;
					Integer rooms=0,adultCount=0,childCount=0,statusId=0;
					String strMobileNumber="",statusName="",strEmailId="";
					strMobileNumber=bookingDetailReport.getMobileNumber();
					jsonOutput += ",\"mobileNumber\":\"" + strMobileNumber+ "\"";
					
					strEmailId=bookingDetailReport.getEmailId();
					jsonOutput += ",\"emailId\":\"" + strEmailId+ "\"";
					String strBookingDate="";
					long arrivalDate =bookingDetailReport.getArrivalDate().getTime();
					java.util.Date dateArrival = new java.util.Date(arrivalDate);
					long departureDate =bookingDetailReport.getDepartureDate().getTime();
					java.util.Date dateDeparture = new java.util.Date(departureDate);
					 
					SimpleDateFormat format1 = new SimpleDateFormat("dd-MMM-yyyy");
					SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
					
					Timestamp FromDate=bookingDetailReport.getArrivalDate();
					Timestamp ToDate=bookingDetailReport.getDepartureDate();
					
					String strFromDate=format2.format(FromDate);
					String strToDate=format2.format(ToDate);
					String strArrivalDate=format1.format(FromDate);
					String strDepartureDate=format1.format(ToDate);
					
			    	DateTime startdate = DateTime.parse(strFromDate);
			        DateTime enddate = DateTime.parse(strToDate);
			        java.util.Date fromdate = startdate.toDate();
			 		java.util.Date todate = enddate.toDate();
			 		
					int diffInDays = (int) ((todate.getTime() - fromdate.getTime()) / (1000 * 60 * 60 * 24));
					
					jsonOutput += ",\"accommodationId\":" + bookingDetailReport.getAccommodationId()+ "";
					PropertyAccommodation accommodations=accommodationController.find(bookingDetailReport.getAccommodationId());
					jsonOutput += ",\"accommodationType\":\"" + accommodations.getAccommodationType().trim()+ "\"";
					jsonOutput += ",\"checkIn\":\"" + strArrivalDate+ "\"";
					jsonOutput += ",\"checkOut\":\"" + strDepartureDate+ "\"";
					/*String strAccommodationType=null;
					accommodationId=bookingDetailReport.getAccommodationId();
					if(accommodationId>0)
					{
						PropertyAccommodation accommodations=accommodationController.find(accommodationId);
						strAccommodationType=accommodations.getAccommodationType();
						jsonOutput += ",\"accommodationType\":\" "+strAccommodationType+"\"";
						jsonOutput += ",\"accommodationId\":\"" + bookingDetailReport.getAccommodationId()+ "\"";
					}
					else{
						jsonOutput += ",\"accommodationType\":\" ALL\"";
						jsonOutput += ",\"accommodationId\":\"" + bookingDetailReport.getAccommodationId()+ "\"";
					}*/
					
					long lngrooms=bookingDetailReport.getRoomCount();
					rooms=(int)lngrooms;
					
					//jsonOutput += ",\"bookingId\":\"" + bookingDetailReport.getBookingId()+ "\"";
					//jsonOutput += ",\"accommdationId\":\"" + rooms/diffInDays+ "\"";
					jsonOutput += ",\"rooms\":" + rooms/diffInDays+ "";
					jsonOutput += ",\"adultsCount\":" + bookingDetailReport.getAdultCount()+ "";
					jsonOutput += ",\"childCount\":" + bookingDetailReport.getChildCount() + "";
					jsonOutput += ",\"advanceAmount\":" + df.format(bookingDetailReport.getAdvanceAmount())+ "";
					
					dblBaseAmount=bookingDetailReport.getAmount();
					dblTaxAmount=bookingDetailReport.getTaxAmount();
					dblAdvanceAmount=bookingDetailReport.getAdvanceAmount();
					dblTotalBaseAmount=dblBaseAmount+dblTaxAmount;
					
					String strTotal=df.format(dblTotalBaseAmount);
					
					dblDueAmount=Double.valueOf(strTotal)-dblAdvanceAmount;
					
					if(bookingDetailReport.getAdvanceAmount()!= null){
						double dblTotalDue=dblDueAmount;
						jsonOutput += ",\"due\":\"" + df.format(dblTotalDue) + "\"";
					}else{
						jsonOutput += ",\"due\":\"" + 0 + "\"";
					}
					if(dblDueAmount == 0.0){
						jsonOutput += ",\"paymentRemarks\":\" CASH PAID\"";
					}else{
						jsonOutput += ",\"paymentRemarks\":\" PAY AT HOTEL\"";
					}
					//jsonOutput += ",\"due\":\"" + ((bookingDetailReport.getAmount()+bookingDetailReport.getTaxAmount()) - bookingDetailReport.getAdvanceAmount()) + "\"";
					jsonOutput += ",\"diffDays\":" + diffInDays+ "";
					
					dblBaseAmount=bookingDetailReport.getAmount();
					dblTaxAmount=bookingDetailReport.getTaxAmount();
					
					jsonOutput += ",\"total\":" + df.format(dblBaseAmount) + "";
					jsonOutput += ",\"tax\":" + df.format(dblTaxAmount) + "";
					
					dblBaseTaxAmount=dblBaseAmount+dblTaxAmount;
					dblTotalAmount=dblBaseTaxAmount;
					
					//jsonOutput += ",\"amount\":\"" + dblTotalAmount+ "\"";
					statusId=bookingDetailReport.getStatusId();
					PmsStatus status=statusController.find(statusId);
					statusName=status.getStatus();
					jsonOutput += ",\"statusId\":" + statusId + "";
					jsonOutput += "}";
				}
			}
			
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}

   public String addBookingDetails() {  
	   String strReturn = "";
        try {
        	PropertyAccommodationManager propertyAccommodationController = new PropertyAccommodationManager();
            PropertyAccommodationRoomManager propertyAccommodationRoomController = new PropertyAccommodationRoomManager();     
            BookingDetailManager bookingDetailController = new BookingDetailManager();
            BookingGuestDetailManager bookingGuestDetailController = new BookingGuestDetailManager();
            PmsGuestManager guestController = new PmsGuestManager();
            PmsStatusManager statusController = new PmsStatusManager();
            PmsBookingManager bookingController = new PmsBookingManager();
            PmsPropertyManager propertyController=new PmsPropertyManager();
            PropertyTaxeManager taxController = new PropertyTaxeManager(); 
            PropertyDiscountManager discountController = new PropertyDiscountManager();
            PromotionManager promotionController=new PromotionManager();
            PmsSmartPriceManager smartPriceController=new PmsSmartPriceManager();
            PropertyAccommodationInventoryManager accommodationInventoryController = new PropertyAccommodationInventoryManager();
            
            DecimalFormat df = new DecimalFormat("###.##");
            //UserLoginManager  userController = new UserLoginManager();
            if(getUserId() != null){
    			this.userId = getUserId();
    		}
    		else{
    		this.userId=(Integer)sessionMap.get("userId");
    		}
            DateFormat f = new SimpleDateFormat("EEEE");
            Calendar cal = Calendar.getInstance();    
            this.blockFlag=isBlockFlag();
            SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			java.util.Date date=new java.util.Date();
			Calendar today=Calendar.getInstance();
			today.setTime(date);
			today.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
		    String strCurrentDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(today.getTime());
		    java.util.Date currentDate = format1.parse(strCurrentDate);
		    Timestamp currentTS = new Timestamp(currentDate.getTime());
		    SimpleDateFormat dayformat=new SimpleDateFormat("EEEE");
		    
		    if(getCheckInTime() !=null && !getCheckInTime().equalsIgnoreCase("NA") && getCheckOutTime() !=null && !getCheckOutTime().equalsIgnoreCase("NA")){
		 	    java.util.Date timeStart = format1.parse(getCheckInTime());
		 	    java.util.Date timeEnd = format1.parse(getCheckOutTime());
		 	   
		 	    
		 	    Calendar calCheckStartTime=Calendar.getInstance();
		 	    calCheckStartTime.setTime(timeStart);
		 	    calCheckStartTime.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
		 	    java.util.Date checkInTime = calCheckStartTime.getTime();
		 	    this.arrivalTime=new Timestamp(checkInTime.getTime());
		 	    
		 	    Calendar calCheckEndTime=Calendar.getInstance();
		 	    calCheckEndTime.setTime(timeEnd);
		 	    calCheckEndTime.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
		 	    java.util.Date checkOutTime = calCheckEndTime.getTime();
		 	    this.departureTime=new Timestamp(checkOutTime.getTime());
		 	    this.bookingType="Hourly";
		    }else{
		    	this.bookingType="Day";
		    }
		    
            this.bookingId = (Integer) sessionMap.get("bookingId");
            this.guestId = (Integer) sessionMap.get("guestId");         
            this.arrivalDate = (Timestamp) sessionMap.get("arrivalDate");
            this.departureDate = (Timestamp) sessionMap.get("departureDate");     
            
            
           
            /*String startDate = this.arrivalDate.toString();
            String endDate =   this.departureDate.toString();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            //String datey = this.arrivalDate;  
            java.util.Date sd = format.parse(startDate);
            java.util.Date ed = format.parse(endDate);
            //String sd = sd.substring(0, this.startDate.indexOf('.'));     

            DateTime start = DateTime.parse(sd.toString());
            DateTime end = DateTime.parse(sd.toString());

            */
            
            this.txtGrandTotal = getTxtGrandTotal();
            sessionMap.put("txtGrandTotal", txtGrandTotal);
            
           
            BookingGuestDetail bookingGuestDetail = new BookingGuestDetail();
            PmsBooking bookingg = bookingController.find(getBookingId());
            this.propertyId=bookingg.getPmsProperty().getPropertyId();
            PmsProperty pmsProperty=propertyController.find(this.propertyId);
            if(pmsProperty.getLocationType()!=null){
				this.locationType=pmsProperty.getLocationType().getLocationTypeName();
				this.locationTypeId=pmsProperty.getLocationType().getLocationTypeId();
			}
            bookingGuestDetail.setPmsBooking(bookingg);
            PmsGuest guest = guestController.find(getGuestId());
            bookingGuestDetail.setPmsGuest(guest);
            bookingGuestDetail.setIsPrimary(true);
            bookingGuestDetail.setIsActive(true);
            bookingGuestDetail.setIsDeleted(false);
            bookingGuestDetail.setSpecialRequest(getSpecialRequest());
            for (int i = 0; i < array.size(); i++)
            {
            	bookingGuestDetail.setUsedRewardPoints(array.get(i).getUsedRewardPoints());
               
            }
            bookingGuestDetailController.add(bookingGuestDetail);
            BookingDetail bookingDetail = new BookingDetail();
           
            /*  double totalBaseAmount = 0;
              Timestamp arrivalDatey = null;
              for (int i = 0; i < array.size(); i++)
              {
               PropertyAccommodation accommodation = propertyAccommodationController.find(array.get(i).getAccommodationId());

               arrivalDatey = Timestamp.valueOf(array.get(i).getArrival());
               totalBaseAmount +=  accommodation.getBaseAmount();              

              }
              

              */
            // PmsRoomDetail roomDetail = bookingDetailController.findRoom(arrival, departure,16);
            // this.roomList = bookingDetailController.roomList(arrival, departure,array.get(i).getAccommodationId());
            // for (PmsBookedDetails booked : bookedList) {}            

             /* for (int i = 0; i < array.size(); i++) 

              {

               PmsRoomDetail roomDetail = bookingDetailController.findRoom(arrival, departure,array.get(i).getAccommodationId());

               array.get(i).setRoomId(roomDetail.getRoomId());
              


              }*/             

              for (int i = 0; i < array.size(); i++) 
              {
                    PropertyAccommodation accommodation = propertyAccommodationController.find(array.get(i).getAccommodationId());
                    //PropertyTaxe propertyTaxe = taxController.findAccommodationTax(array.get(i).getAccommodationId());                 
                    	
                    
                    String dateArrival = array.get(i).getArrival();
       			   String dateDeparture = array.get(i).getDeparture();
       			   
//       			DateFormat format = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
       			   SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
       			   java.util.Date dateStart = format.parse(dateArrival);
       			   java.util.Date dateEnd = format.parse(dateDeparture);
       			   //java.util.Date dateEnd = format.parse(getStrEndDate());


       			   Calendar calCheckStart=Calendar.getInstance();
       			   Calendar calCheckEnd=Calendar.getInstance();
       			   calCheckStart.setTime(dateStart);
       			   calCheckEnd.setTime(dateEnd);
       			   java.util.Date checkInDate = calCheckStart.getTime();
       			   java.util.Date checkInEnd = calCheckEnd.getTime();
       			   this.arrivalDate =new Timestamp(checkInDate.getTime());
       			   this.departureDate =new Timestamp(checkInEnd.getTime());
                    
                    
                    cal.setTime(this.departureDate);
                    cal.add(Calendar.DATE, -1);
                    String startDate = new SimpleDateFormat("yyyy-MM-dd").format(this.arrivalDate);
                    String endDate = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
                    
                    DateTime start = DateTime.parse(startDate);
                    DateTime end = DateTime.parse(endDate);
                    java.util.Date arrival = start.toDate();
                    java.util.Date departure = end.toDate();

                     List<DateTime> between = getDateRange(start, end);
                     for (DateTime d : between)
                 	{
	                    
	                      java.util.Date bookingDate=  d.toDate();
	                    
	                    checkedDays=dayformat.format(d.toDate());
	                    PropertyAccommodationInventory inventoryCount = accommodationInventoryController.findInventoryCount(bookingDate,array.get(i).getAccommodationId());
	                    if(inventoryCount != null){
	                	PropertyAccommodationInventory inventory = accommodationInventoryController.find(inventoryCount.getInventoryId());
	                    bookingDetail.setPropertyAccommodationInventory(inventory);
	                	
	                	}
	                    
	                    bookingDetail.setAdultCount(array.get(i).getAdultsCount());
	                    
	                    bookingDetail.setChildCount(array.get(i).getChildCount());
	                    bookingDetail.setRefund(array.get(i).getRefund());
	                    bookingDetail.setAdvanceAmount(array.get(i).getAdvanceAmount()/array.get(i).getDiffDays());
	                    bookingDetail.setAmount(array.get(i).getTotal()/array.get(i).getDiffDays());
	                    bookingDetail.setTax(array.get(i).getTax()/array.get(i).getDiffDays());  
	                    bookingDetail.setOtaCommission(array.get(i).getOtaCommission()/array.get(i).getDiffDays());
	                    bookingDetail.setOtaTax(array.get(i).getOtaTax()/array.get(i).getDiffDays());
	                    bookingDetail.setAddonAmount(array.get(i).getAddonAmount());
	                    bookingDetail.setAddonDescription(array.get(i).getAddonDescription());
	                    
	                    if(this.userId!=null){
	                    	bookingDetail.setCreatedBy(this.userId);
	                    }
	                    bookingDetail.setCreatedDate(currentTS);
	                    bookingDetail.setBookingDate(bookingDate);
	                    if(blockFlag){
	                    	bookingDetail.setIsActive(false);
	                        bookingDetail.setIsDeleted(true);
	                    }else if(!blockFlag){
	                    	bookingDetail.setIsActive(true);
	                        bookingDetail.setIsDeleted(false);	
	                    }
	                    
	                    PmsStatus status = statusController.find(array.get(i).getStatusId());
	                    bookingDetail.setPmsStatus(status);
	                    PropertyAccommodation propertyAccommodation = propertyAccommodationController.find(array.get(i).getAccommodationId());
	                    if(propertyAccommodation.getPropertyContractType()!=null){
	                     	contractType=propertyAccommodation.getPropertyContractType().getContractTypeName();
	                     	contractTypeId=propertyAccommodation.getPropertyContractType().getContractTypeId();
	                     }
	                     if(this.locationType.equalsIgnoreCase("Metro")){
	                     	if(contractTypeId==7){//B-join
	                     		purchaseRate= propertyAccommodation.getNetRate()==null?0:propertyAccommodation.getNetRate();		
	                     	}else if(contractTypeId==6){//B-sure
	                     		purchaseRate= propertyAccommodation.getPurchaseRate()==null?0:propertyAccommodation.getPurchaseRate();
	                     	}else if(contractTypeId==8){//B-stay
	                     		purchaseRate= propertyAccommodation.getPurchaseRate()==null?0:propertyAccommodation.getPurchaseRate();
	                     	}else if(contractTypeId==9){//B-join flat
	                     		purchaseRate= propertyAccommodation.getBaseAmount()==null?0:propertyAccommodation.getBaseAmount();
	                     	}
	                     	
	                     }else if(this.locationType.equalsIgnoreCase("Leisure")){
	                     	String days=f.format(this.arrivalDate);
	                         String[] weekdays={"Monday","Tuesday","Wednesday","Thursday","Sunday"};
	             		    String[] weekend={"Friday","Saturday"};
	             		    boolean isWeekdays=false,isWeekend=false;
	         				for(int a=0;a<weekdays.length;a++){
	         					if(days.equalsIgnoreCase(weekdays[a])){
	         						isWeekdays=true;
	         					}
	         				}
	         				
	         				for(int b=0;b<weekend.length;b++){
	         					if(days.equalsIgnoreCase(weekend[b])){
	         						isWeekend=true;
	         					}
	         				}
	         				
	         				if(isWeekdays){
	         					if(contractTypeId==7){//B-join
	                         		purchaseRate= propertyAccommodation.getWeekdayNetRate()==null?0:propertyAccommodation.getWeekdayNetRate();		
	                         	}else if(contractTypeId==6){//B-sure
	                         		purchaseRate= propertyAccommodation.getWeekdayPurchaseRate()==null?0:propertyAccommodation.getWeekdayPurchaseRate();
	                         	}else if(contractTypeId==8){//B-stay
	                         		purchaseRate= propertyAccommodation.getWeekdayPurchaseRate()==null?0:propertyAccommodation.getWeekdayPurchaseRate();
	                         	}else if(contractTypeId==9){//B-join flat
	                         		purchaseRate= propertyAccommodation.getWeekdayBaseAmount()==null?0:propertyAccommodation.getWeekdayBaseAmount();
	                         	}
	         				}
	         				
	         				if(isWeekend){
	         					if(contractTypeId==7){//B-join
	                         		purchaseRate= propertyAccommodation.getWeekendNetRate()==null?0:propertyAccommodation.getWeekendNetRate();		
	                         	}else if(contractTypeId==6){//B-sure
	                         		purchaseRate= propertyAccommodation.getWeekendPurchaseRate()==null?0:propertyAccommodation.getWeekendPurchaseRate();
	                         	}else if(contractTypeId==8){//B-stay
	                         		purchaseRate= propertyAccommodation.getWeekendPurchaseRate()==null?0:propertyAccommodation.getWeekendPurchaseRate();
	                         	}else if(contractTypeId==9){//B-join flat
	                         		purchaseRate= propertyAccommodation.getWeekendBaseAmount()==null?0:propertyAccommodation.getWeekendBaseAmount();
	                         	}
	         				}
	                     }
	                    double purchaseNet=purchaseRate*array.get(i).getRooms(); 
	     				bookingDetail.setPurchaseRate(purchaseNet);
	     				bookingDetail.setExtraAdultRate(array.get(i).getExtraAdultChildAmount());
	     				bookingDetail.setExtraChildRate(0.0);
	                    bookingDetail.setPropertyAccommodation(propertyAccommodation);
	                    PmsBooking booking = bookingController.find(getBookingId());
	                    bookingDetail.setPmsBooking(booking);
	                    if(this.arrivalTime!= null && this.departureTime!=null){
	                    bookingDetail.setCheckInTime(this.arrivalTime);
	                    bookingDetail.setCheckOutTime(this.departureTime);
	                    }
	                    bookingDetail.setRandomNo(array.get(i).getRandomNo());
	                    bookingDetail.setInfantCount(array.get(i).getInfantCount());
	                    bookingDetail.setRoomCount(array.get(i).getRooms());
	                    bookingDetail.setActualAmount(Math.round(array.get(i).getMaximumAmount() /array.get(i).getDiffDays()));
	                    
	                    
	                    // for promotion
	                    sessionMap.put("promotionName", array.get(i).getPromotionName());
	                    this.promotionName=array.get(i).getPromotionName();
	                    if(array.get(i).getDiscountId() != null){
	                        PropertyDiscount discount = discountController.find(array.get(i).getDiscountId());
	                        bookingDetail.setPropertyDiscount(discount);
	                    }else{
	                       	
	                    	//bookingDetail.setPmsBooking(booking);
	                    	Integer frontDiscountId = null,frontSmartPriceId=null,frontPromotionId=null;
	                    	frontSmartPriceId= array.get(0).getSmartPriceId();
	                    	frontPromotionId= array.get(0).getPromotionId();
	                    	frontDiscountId = array.get(0).getDiscountId();
	                    	
	                    	if(frontDiscountId != null && frontDiscountId!=0){
	                    		//frontDiscountId = array.get(0).getDiscountId();	
	                    		PropertyDiscount discount = discountController.find(frontDiscountId);
	                            bookingDetail.setPropertyDiscount(discount);
	                    	
	                    	}
	                    	
	                    	if(frontSmartPriceId != null && frontSmartPriceId!=0){
	                    		//frontDiscountId = array.get(0).getDiscountId();	
	                    		PmsSmartPrice pmsSmartPrice = smartPriceController.find(frontSmartPriceId);
	                            bookingDetail.setPmsSmartPrice(pmsSmartPrice);
	                    	
	                    	}
	                    	
	                    	if(frontPromotionId != null && frontPromotionId!=0){
	                    		//frontDiscountId = array.get(0).getDiscountId();	
	                    		PmsPromotions pmsPromotions = promotionController.find(frontPromotionId);
	                            bookingDetail.setPmsPromotions(pmsPromotions);
	                    	
	                    	}
	                    }
	
	                   
	                    //PmsRoomDetail roomDetail = bookingDetailController.findRoom(arrival, departure,array.get(i).getAccommodationId());
	                    //PropertyAccommodationRoom accommodationRoom = propertyAccommodationRoomController.find(roomDetail.getRoomId());
	                    //bookingDetail.setPropertyAccommodationRoom(accommodationRoom);
	                    //bookingDetail.set(roomDetail.getRoomId());
	                    bookingDetailController.add(bookingDetail);
	                    
	                    
	                    
	                    OtaHotelsManager otaHotelsController = new OtaHotelsManager();
	                    
	                    
	                    
	                    
	                    PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
	                    
/*	                    
 * 						
 * 						long rmc = 0;
	    				this.roomCnt = rmc;
	    				 PropertyAccommodation propertyAccommodation1 = accommodationController.find(array.get(i).getAccommodationId());
	    				if(inventoryCount != null){
	  						//jsonOutput += ",\"inventoryId\":\"" + inventoryCount.getInventoryId() + "\"";
	  						PmsAvailableRooms roomCount1 = bookingController.findBookingCount(bookingDate, array.get(i).getAccommodationId(),inventoryCount.getInventoryId());
	  						if(roomCount1.getRoomCount() == null) {
	  							 String num1 = inventoryCount.getInventoryCount();
	  							 long num2 = Long.parseLong(num1);
	  							this.roomCnt = num2; 
	  						}
	  						else {
	  							 long num = roomCount1.getRoomCount();
	  							 String num1 = inventoryCount.getInventoryCount();
	  							 long num2 = Long.parseLong(num1);
	  							this.roomCnt = (num2-num);	
	  						}
	  							
	  					}
	  					else{
	  						//jsonOutput += ",\"inventoryId\":\"" + 0 + "\""; 
	  						  PmsAvailableRooms roomCount = bookingController.findCount(bookingDate,array.get(i).getAccommodationId());							
	  							if(roomCount.getRoomCount() == null){								
	  								this.roomCnt = (propertyAccommodation1.getNoOfUnits()-rmc);		
	  							}
	  							else{
	  								
	  								this.roomCnt = (propertyAccommodation1.getNoOfUnits()-roomCount.getRoomCount());						
	  							}
	  					}*/
	  					
	                    PropertyAccommodationInventoryManager inventoryController= new PropertyAccommodationInventoryManager();
				    	 long rmc = 0;
			   			 this.roomCnt = rmc;
			        	 int sold=0;
			        	 java.util.Date availDate = d.toDate();
			        	 PropertyAccommodation accommodation1 = accommodationController.find(array.get(i).getAccommodationId());
						 PropertyAccommodationInventory inventoryDetails = inventoryController.findInventoryCount(availDate,array.get(i).getAccommodationId());
						 if(inventoryDetails != null){
						 PmsAvailableRooms roomCount1 = bookingController.findBookingCount(availDate, array.get(i).getAccommodationId(),inventoryDetails.getInventoryId());
						 if(roomCount1.getRoomCount() == null) {
						 	String num1 = inventoryDetails.getInventoryCount();
							PmsAvailableRooms bookingRoomCount=bookingController.findCount(availDate, array.get(i).getAccommodationId());
							Integer totavailable=0;
							if(accommodation1!=null){
								totavailable=accommodation1.getNoOfUnits();
							}
							if(bookingRoomCount.getRoomCount()!=null){
								long lngRooms=bookingRoomCount.getRoomCount();
								long num2 = Long.parseLong(num1);
								this.roomCnt = num2;
								sold=(int) (lngRooms);
								int availRooms=0;
								availRooms=totavailable-sold;
//								if(Integer.parseInt(num1)>availRooms){
//									this.roomCnt = availRooms;	
//								}else{
									this.roomCnt = num2;
//								}
										
							}else{
								 long num2 = Long.parseLong(num1);
								 this.roomCnt = num2;
								 sold=(int) (rmc);
							}
						}else{		
							PmsAvailableRooms bookingRoomCount=bookingController.findCount(availDate, array.get(i).getAccommodationId());
							int intRooms=0;
							
							Integer totavailable=0,availableRooms=0;
							if(accommodation1!=null){
								totavailable=accommodation1.getNoOfUnits();
							}
							if(bookingRoomCount.getRoomCount()!=null){
								long lngRooms=bookingRoomCount.getRoomCount();
								long num = roomCount1.getRoomCount();	
								intRooms=(int)lngRooms;
								String num1 = inventoryDetails.getInventoryCount();
			 					long num2 = Long.parseLong(num1);
			 					availableRooms=totavailable-intRooms;
//			 					if(num2>availableRooms){
//			 						this.roomCnt = availableRooms;	 
//			 					}else{
			 						this.roomCnt = num2-num;
//			 					}
			 					long lngSold=bookingRoomCount.getRoomCount();
			   					sold=(int)lngSold;
							}else{
								long num = roomCount1.getRoomCount();	
								String num1 = inventoryDetails.getInventoryCount();
								long num2 = Long.parseLong(num1);
			  					this.roomCnt = (num2-num);
			  					long lngSold=roomCount1.getRoomCount();
			   					sold=(int)lngSold;
							}
		
						}
										
					}else{
						PmsAvailableRooms inventoryCounts = bookingController.findCount(availDate, array.get(i).getAccommodationId());							
						if(inventoryCounts.getRoomCount() == null){								
							this.roomCnt = (accommodation1.getNoOfUnits()-rmc);
							sold=(int)rmc;
						}
						else{
							this.roomCnt = (accommodation1.getNoOfUnits()-inventoryCounts.getRoomCount());	
							long lngSold=inventoryCounts.getRoomCount();
   							sold=(int)lngSold;
						}
					}
						 
						double totalRooms=0,intSold=0;
							
						totalRooms=(double)accommodation1.getNoOfUnits();
						intSold=(double)sold;
						double occupancy=0.0;
						occupancy=intSold/totalRooms;
						double sellrate=0.0,otarate=0.0;
						Integer occupancyRating=(int) Math.round(occupancy * 100);
							
	  			
						boolean otaEnable=true;
						int limit = 1;
						this.propertyId=array.get(i).getPropertyId();
						Integer[] arr={63,64,65};
						for(int in=0;in<arr.length;in++){
							if((int)this.propertyId==(int)arr[in]){
								otaEnable=false;
							}
						}
	  				

  						OtaHotelDetails otaHotelDetails = otaHotelsController.getOtaHotelDetails(array.get(i).getAccommodationId(),limit);
			  			if(otaHotelDetails!=null){
			  				int propertyId = otaHotelDetails.getPmsProperty().getPropertyId();
				  			int sourceId = 3;
				  			OtaHotels otaHotel = otaHotelsController.getOtaHotels(propertyId,sourceId);
				  			DefaultHttpClient httpClient = new DefaultHttpClient();
		  					HttpPost postRequest = new HttpPost("https://in.goibibo.com/api/chmv2/updateroominventory/?bearer_token="+otaHotel.getOtaBearerToken()+"&channel_token="+otaHotel.getOtaChannelToken()+"");
		  					
		  					String xmlBody = "<?xml version='1.0' encoding='UTF-8' ?>"+
		  						    "<Website Name='ingoibibo' HotelCode='"+otaHotelDetails.getOtaHotelCode()+"' Version='1'>"+
		  						            "<Room>"+
		  						            "<RoomTypeCode>"+ otaHotelDetails.getOtaRoomTypeId() +"</RoomTypeCode>"+
		  						            "<StartDate Format='yyyy-mm-dd'>"+startDate+"</StartDate>"+
		  						            "<EndDate Format='yyyy-mm-dd'>"+endDate+"</EndDate>"+						            
		  						            "<DaysOfWeek Mon='"+"true"+"' Tue='"+"true"+"' Wed='"+"true"+"' Thu='"+"true"+"' Fri='"+"true"+"' Sat='"+"true"+"' Sun='"+"true"+"'></DaysOfWeek>"+
//		  						            "<MinLOS>"+"1"+"</MinLOS>"+
//		  						            "<CutOff>"+"1h"+"</CutOff>"+
		  						            "<Available> "+ this.roomCnt +"</Available>"+
		  						            "<StopSell>"+"False"+"</StopSell>"+
		  						            "</Room>"+
		  						     
		  						            "</Website>";
		  					
		  				    
		  					StringEntity input = new StringEntity(xmlBody);
		  					input.setContentType("text/xml");
		  					postRequest.setEntity(input);
		  					HttpResponse httpResponse = httpClient.execute(postRequest);
		  					HttpEntity entity = httpResponse.getEntity();
		  	                // Read the contents of an entity and return it as a String.
		  		            String content = EntityUtils.toString(entity);
			  			}
  						
			  			
			  			
			  			double extraAdult=0,extraChild=0,extraInfant=0;
			  			extraAdult=array.get(i).getExtraAdult()/array.get(i).getDiffDays();
			  			extraChild=array.get(i).getExtraChild()/array.get(i).getDiffDays();
//			  			extraInfant=array.get(i).getExtraInfant()/array.get(i).getDiffDays();
			  			extraInfant=0;
			  			
			  			
			  			int sourceId = 3;
	        			this.minimumAmount=array.get(i).getMinimumAmount();
	        			this.maximumAmount=array.get(i).getMaximumAmount();
	        			if(occupancyRating==0){
	 						sellrate = minimumAmount;
	 					}else if(occupancyRating>0 && occupancyRating<=30){
	 			        	sellrate = minimumAmount;
	 			        }else if(occupancyRating>=31 && occupancyRating<=60){
	 			        	sellrate = minimumAmount+(minimumAmount*10/100);
	 			        }else if(occupancyRating>=61 && occupancyRating<=80){
	 			        	sellrate = minimumAmount+(minimumAmount*15/100);
	 			        }else if(occupancyRating>=81 && occupancyRating<=100){
	 			        	sellrate = maximumAmount;
	 			        }
	        			checkedDays=checkedDays.toLowerCase();
	        			double otaAmount=sellrate+(sellrate*15/100);
	        			otaEnable=true;
	    				for(int in=0;in<arr.length;in++){
	    					if((int)this.propertyId==(int)arr[in]){
	    						otaEnable=false;
	    					}
	    				}
	    				
	        			if(otaEnable){/*
	        				OtaHotelsManager otaController=new OtaHotelsManager();
		        			PropertyRatePlanDetailManager ratePlanDetailsController=new PropertyRatePlanDetailManager();
		        			OtaHotels otaHotels =  otaController.getOtaHotels(array.get(i).getPropertyId(),sourceId);
		        			DefaultHttpClient httpClient = new DefaultHttpClient();
		        			HttpPost postRequest = new HttpPost("https://in.goibibo.com/api/chmv2/updateroomrates/?bearer_token="+otaHotels.getOtaBearerToken()+"&channel_token="+otaHotels.getOtaChannelToken()+"");
		        			
		        			List <OtaHotelDetails> otaDetailList = otaController.listDetails(otaHotels.getOtaHotelCode(),array.get(i).getAccommodationId());
		        			
		        			for(OtaHotelDetails otaDetail : otaDetailList){

		        				if(otaDetail.getPropertyRatePlan().getRatePlanId() == 1){
		        					
		        					int rateid = 1;
		        					PropertyRatePlanDetail ratePlanDetails=ratePlanDetailsController.listRateDetails(array.get(i).getPropertyId(),array.get(i).getAccommodationId(),rateid);
		        					double epTotal = otaAmount - ratePlanDetails.getTariffAmount();
		        				String xmlBody = "<?xml version='1.0' encoding='UTF-8' ?>"+
		        			    "<Website Name='ingoibibo' HotelCode='"+otaHotels.getOtaHotelCode()+"' Version='1'>"+
		        			            "<RatePlan>"+
		        			            "<RatePlanCode>"+otaDetail.getOtaRateplanId()+"</RatePlanCode>"+
		        			            "<StartDate Format='yyyy-mm-dd'>"+startDate+"</StartDate>"+
		        			            "<EndDate Format='yyyy-mm-dd'>"+endDate+"</EndDate>"+
		        			            "<SingleOccupancyRates>"+				            
		        			            "<SellRate>"+epTotal+"</SellRate>"+
		        			            "</SingleOccupancyRates>"+
		        			            "<DoubleOccupancyRates>"+				            
		        			            "<SellRate>"+epTotal+"</SellRate>"+
		        			            "</DoubleOccupancyRates>"+
		        			            "<TripleOccupancyRates>"+				            
		        			            "<SellRate>"+epTotal+"</SellRate>"+
		        			            "</TripleOccupancyRates>"+
		        			            "<ExtraChildCharge>"+extraAdult+"</ExtraChildCharge>"+
		        			            "<ExtraAdultCharge>"+extraChild+"</ExtraAdultCharge>"+
		        			            "<InfantCharge>"+extraInfant+"</InfantCharge>"+
		        			            "<DaysOfWeek Mon='"+getDaysOfWeek(checkedDays)+"' Tue='"+getDaysOfWeek(checkedDays)+"' Wed='"+getDaysOfWeek(checkedDays)+"' Thu='"+getDaysOfWeek(checkedDays)+"' Fri='"+getDaysOfWeek(checkedDays)+"' Sat='"+getDaysOfWeek(checkedDays)+"' Sun='"+getDaysOfWeek(checkedDays)+"'></DaysOfWeek>"+
		        			            "<ContractType b2b='False' b2c='True' mobile='False' fph='False'></ContractType>"+
		        			            "<Currency>INR</Currency>"+
		        			            "</RatePlan>"+
		        			      "</Website>";
		        					
		        					StringEntity input = new StringEntity(xmlBody);
		        					input.setContentType("text/xml");
		        					postRequest.setEntity(input);
		        					HttpResponse httpResponse = httpClient.execute(postRequest);
		        					HttpEntity entity = httpResponse.getEntity();
		        		            // Read the contents of an entity and return it as a String.
		        		            String content = EntityUtils.toString(entity);
		        				}else if(otaDetail.getPropertyRatePlan().getRatePlanId() == 2){
		        					
		        					String xmlBody = "<?xml version='1.0' encoding='UTF-8' ?>"+
		        						    "<Website Name='ingoibibo' HotelCode='"+otaHotels.getOtaHotelCode()+"' Version='1'>"+
		        						            "<RatePlan>"+
		        						            "<RatePlanCode>"+otaDetail.getOtaRateplanId()+"</RatePlanCode>"+
		        						            "<StartDate Format='yyyy-mm-dd'>"+startDate+"</StartDate>"+
		        						            "<EndDate Format='yyyy-mm-dd'>"+endDate+"</EndDate>"+
		        						            "<SingleOccupancyRates>"+				            
		        						            "<SellRate>"+otaAmount+"</SellRate>"+
		        						            "</SingleOccupancyRates>"+
		        						            "<DoubleOccupancyRates>"+				            
		        						            "<SellRate>"+otaAmount+"</SellRate>"+
		        						            "</DoubleOccupancyRates>"+
		        						            "<TripleOccupancyRates>"+				            
		        						            "<SellRate>"+otaAmount+"</SellRate>"+
		        						            "</TripleOccupancyRates>"+
		        						            "<ExtraChildCharge>"+extraChild+"</ExtraChildCharge>"+
		        						            "<ExtraAdultCharge>"+extraAdult+"</ExtraAdultCharge>"+
		        						            "<InfantCharge>"+extraInfant+"</InfantCharge>"+
		        						            "<DaysOfWeek Mon='"+getDaysOfWeek(checkedDays)+"' Tue='"+getDaysOfWeek(checkedDays)+"' Wed='"+getDaysOfWeek(checkedDays)+"' Thu='"+getDaysOfWeek(checkedDays)+"' Fri='"+getDaysOfWeek(checkedDays)+"' Sat='"+getDaysOfWeek(checkedDays)+"' Sun='"+getDaysOfWeek(checkedDays)+"'></DaysOfWeek>"+
		        						            "<ContractType b2b='False' b2c='True' mobile='False' fph='False'></ContractType>"+
		        						            "<Currency>INR</Currency>"+
		        						            "</RatePlan>"+
		        						      "</Website>";
		        								
		        								
		        						StringEntity input = new StringEntity(xmlBody);
		        						input.setContentType("text/xml");
		        						postRequest.setEntity(input);
		        						HttpResponse httpResponse = httpClient.execute(postRequest);
		        						HttpEntity entity = httpResponse.getEntity();
		        			            // Read the contents of an entity and return it as a String.
		        			            String content = EntityUtils.toString(entity);
		        				}
		        		    	
		        			}	
	        			*/}
	        			
	               }       

                    

             }            

             //send email         

             //SendConfirmation();        

             //ends here
             /*this.setMailFlag(true);         

              getBookedDetails(); */
              
//              this.blockFlag=getBlockFlag();
              if(this.blockFlag==false){
            	  this.setMailFlag(true);         
//                  getBookedDetails(); 
                  getBookingVoucher("online",this.bookingId);
              }
              
//              getHotelierBookedDetails();
            /*String jsonOutput = "";
            HttpServletResponse response = ServletActionContext.getResponse();

            response.setContentType("application/json");

            if (!jsonOutput.equalsIgnoreCase(""))

                jsonOutput += ",{";

            else

                jsonOutput += "{";

            

            jsonOutput += "\"bookingId\":\"" + getBookingId() + "\"";          
            jsonOutput += ",\"guestId\":\"" + getGuestId()+ "\"";
            jsonOutput += ",\"guestName\":\"" + guest.getFirstName()+ "\"";
            jsonOutput += ",\"guestNumber\":\"" + guest.getPhone()+ "\"";
            jsonOutput += ",\"guestEmail\":\"" + guest.getEmailId()+ "\"";
            jsonOutput += ",\"rooms\":\"" + array.get(i).getAdultsCount() + "\"";
            jsonOutput += ",\"adults\":\"" + array.get(i).getAdultsCount() + "\"";
            jsonOutput += ",\"child\":\"" + array.get(i).getChildCount()+ "\"";
            jsonOutput += ",\"tax\":\"" + array.get(i).getChildCount()+ "\"";
            jsonOutput += ",\"total\":\"" + array.get(i).getTotal()+ "\"";
            jsonOutput += "}";
            response.getWriter().write("{\"data\":[" + jsonOutput + "]}");*/           
              strReturn=null;
        } catch (Exception e) {
            logger.error(e);
            e.printStackTrace();
            strReturn="error";
        } finally {

        }
        return strReturn;

    }
	
	public String editBookingDetails() {
		
		BookingDetailManager bookingDetailController = new BookingDetailManager();
		PmsStatusManager statusController = new PmsStatusManager();
		PmsBookingManager bookingController = new PmsBookingManager();
		UserLoginManager  userController = new UserLoginManager();
		try {
			
						
			BookingDetail bookingDetail = bookingDetailController.find(getBookingDetailsId());
			
			
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			bookingDetail.setAdultCount(getAdultCount());
			bookingDetail.setAmount(getAmount());
			bookingDetail.setChildCount(getChildCount());
			bookingDetail.setRefund(getRefund());
			bookingDetail.setTax(getTax());
			bookingDetail.setIsActive(true);
			bookingDetail.setIsDeleted(false);
			PmsStatus status = statusController.find(getStatusId());
			bookingDetail.setPmsStatus(status);
			PmsBooking booking = bookingController.find(getBookingId());
			bookingDetail.setPmsBooking(booking);
			
			bookingDetailController.edit(bookingDetail);
		
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return SUCCESS;
	}
	
	 public String editBookedDetails() {
		        
    	 
    	        
    	        PmsBookingManager bookingController = new PmsBookingManager();
    			PmsStatusManager statusController = new PmsStatusManager();
    			PmsSourceManager sourceController = new PmsSourceManager();
    			PmsPropertyManager propertyController = new PmsPropertyManager();
    			PropertyTaxeManager taxController = new PropertyTaxeManager();
    			PropertyAccommodationManager propertyAccommodationController = new PropertyAccommodationManager();
    			try {
    				
    				  for (int i = 0; i < array.size(); i++) 
    			      {
    					
    				   totalBaseAmount +=   array.get(i).getTotal();
    				   totalAdvanceAmount +=   array.get(i).getAdvanceAmount();
    				   totalTaxAmount +=   array.get(i).getTax();
    				   totalAdultCount +=  array.get(i).getAdultsCount();
    				   totalChildCount +=  array.get(i).getChildCount();
    				  
    				   }
    				  
    				  this.txtGrandTotal = 0;
    		          sessionMap.put("txtGrandTotal", txtGrandTotal);
    				  sessionMap.put("totalAdultCount",this.totalAdultCount);
    				  sessionMap.put("totalChildCount",this.totalChildCount);			  
    				   
    				PmsBooking booking = bookingController.find(getBookingId());
    			    long time = System.currentTimeMillis();
    				java.sql.Date date = new java.sql.Date(time);
    				
    				booking.setTotalAmount(totalBaseAmount);
    				booking.setAdvanceAmount(totalAdvanceAmount);
    				booking.setTotalTax(totalTaxAmount);
    				
    			    PmsBooking book = bookingController.edit(booking);
    			    
    			    //updating booking Details table
    			    
    			    BookingDetailManager bookingDetailController = new BookingDetailManager();
    				BookingGuestDetailManager bookingGuestDetailController = new BookingGuestDetailManager();
    			    
    			    for (int i = 0; i < array.size(); i++) 
  			       {
    			    
    				this.bookingDetailList = bookingDetailController.detailList(getBookingId(),array.get(i).getAccommodationId());
    				for (BookingDetail bookingDetails : bookingDetailList) {
    					
    					BookingDetail bookingDetail = bookingDetailController.find(bookingDetails.getBookingDetailsId());
    					bookingDetail.setAmount(array.get(i).getTotal()/array.get(i).getDiffDays());
    					bookingDetail.setAdvanceAmount(array.get(i).getAdvanceAmount()/array.get(i).getDiffDays());
    					bookingDetail.setTax(array.get(i).getTax()/array.get(i).getDiffDays());
    					bookingDetail.setAdultCount(array.get(i).getAdultsCount());
    					bookingDetail.setChildCount(array.get(i).getChildCount());
    					bookingDetailController.edit(bookingDetail);
    					
    				}
  			      
  			       }
    			    List<BookingGuestDetail> guestDetails =  bookingGuestDetailController.findListBookingId(getBookingId());
    			   for(BookingGuestDetail guest : guestDetails){
    				   guest.setSpecialRequest(getSpecialRequest());
    			       bookingGuestDetailController.edit(guest);
    			   }
    			    this.setMailFlag(getMailFlag());         
    			    
    	            if(this.mailFlag){
    	            	getBookingVoucher("online",getBookingId());	
    	            }
    	            
//    	            getHotelierBookedDetails();
    				/*this.bookingId = book.getBookingId();
    				sessionMap.put("bookingId",bookingId);
    				
    				this.bookingId = (Integer) sessionMap.get("bookingId");
    				*/
    			    
    				
    			} catch (Exception e) {
    				logger.error(e);
    				e.printStackTrace();
    			} finally {

    			}
    			return null;
    		
	}
	 
	 public String getRouteMapSms(String routeMap, String phone)throws Exception{
			
	 		
			
			String number = phone;
			String message = "&sms_text=" + routeMap ;


			//String sender = "&sender=" + "TXTLCL";
			String numbers = "&sms_to=" + "+91"+number ;
			String from = "&sms_from=" + "ULOHTL";
			String type = "&sms_type=" + "trans"; 
			// Send data
			HttpURLConnection conn = (HttpURLConnection) new URL("https://secureets.knowlarity.com/ets/dom/clients/ulohotels/api/ulohotels_sms_api.py").openConnection();
			//"http://etsrds.kapps.in/webapi/ulohotels/api/ulohotels_sms_api.py?sms_text=hi%20this%20gopinath%20from%20ulo&sms_to=%20918220211068&sms_from=ULOHTL&sms_type=promo"
			String data = numbers + message +  from + type;
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.addRequestProperty("auth_key", "Basic dWxvaG90ZWxzX1Q6VUxPSFRMQDIwMTg=");
			conn.setRequestProperty("Content-Length", Integer.toString(data.length()));
			conn.getOutputStream().write(data.getBytes("UTF-8"));
			
			final BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			final StringBuffer stringBuffer = new StringBuffer();
			/*String line;
			
			while ((line = rd.readLine()) != null) {
				stringBuffer.append(line);
				
			}*/
			rd.close();
			
			return null;
			
		
		}
		/*else{
		}*/
	public void getBookingVoucher(String bookingType,int bookingId){
		try{
			if(bookingType.equalsIgnoreCase("Online")){
				this.bookingId=bookingId;
				getGuestBookedDetails();
			}else if(bookingType.equalsIgnoreCase("Offline")){
				this.bookingId=bookingId;
				getGuestBookedDetails();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	public String editBookingDetailsStatus() {
		PmsBookingManager bookingController = new PmsBookingManager();
		BookingDetailManager detailController = new BookingDetailManager();
		PmsStatusManager statusController = new PmsStatusManager();
		UserLoginManager  userController = new UserLoginManager();
		try {
			if(getStatusId() == 3){
				this.bookingDetailList = detailController.findId(getBookingId());
				 for (BookingDetail bookingDetail : bookingDetailList)
				 {			
					PmsStatus status = statusController.find(getStatusId());
					bookingDetail.setPmsStatus(status);
					detailController.edit(bookingDetail);
				 }
			}else if(getStatusId() == 5){
				this.bookingDetailList = detailController.findId(getBookingId());
				 for (BookingDetail bookingDetail : bookingDetailList)
				 {			
					PmsStatus status = statusController.find(getStatusId());
					bookingDetail.setPmsStatus(status);
					detailController.edit(bookingDetail);
				 }
			}else if(getStatusId() == 1){
				this.bookingDetailList = detailController.findId(getBookingId());
				 for (BookingDetail bookingDetail : bookingDetailList)
				 {			
					PmsStatus status = statusController.find(getStatusId());
					bookingDetail.setPmsStatus(status);
					detailController.edit(bookingDetail);
				 }
			}else if(getStatusId() == 2){
				this.bookingDetailList = detailController.findId(getBookingId());
				 for (BookingDetail bookingDetail : bookingDetailList)
				 {			
					PmsStatus status = statusController.find(getStatusId());
					bookingDetail.setPmsStatus(status);
					detailController.edit(bookingDetail);
				 }
			}
			/*else if(getStatusId() == 4){
				this.bookingDetailList = detailController.findId(getBookingId());
				 for (BookingDetail bookingDetail : bookingDetailList)
				 {			
					PmsStatus status = statusController.find(getStatusId());
					bookingDetail.setPmsStatus(status);
					detailController.edit(bookingDetail);
				 }
			}*/
			
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}
	
	public String deleteBookingDetails() {
		BookingDetailManager bookingDetailController = new BookingDetailManager();
		PmsStatusManager statusController = new PmsStatusManager();
		PmsBookingManager bookingController = new PmsBookingManager();
		try {
			BookingDetail bookingDetail = bookingDetailController.find(getBookingDetailsId());
						
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			bookingDetail.setIsActive(false);
			bookingDetail.setIsDeleted(true);
			bookingDetail.setModifiedDate(new java.sql.Timestamp(date.getTime()));
			bookingDetailController.edit(bookingDetail);
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}
	
	public static List<DateTime> getDateRange(DateTime start, DateTime end) {

        List<DateTime> ret = new ArrayList<DateTime>();
        DateTime tmp = start;
        while(tmp.isBefore(end) || tmp.equals(end)) {
            ret.add(tmp);
            tmp = tmp.plusDays(1);
        }
        return ret;
    }
	
	
	
	public Integer getBookingDetailsId() {
		return bookingDetailsId;
	}

	public void setBookingDetailId(Integer bookingDetailsId) {
		this.bookingDetailsId = bookingDetailsId;
	}
	
	
	public List<PropertyAccommodation> getAccommodationList() {
		return accommodationList;
	}

	public void setAccomadationList(List<PropertyAccommodation> accommodationList) {
		this.accommodationList = accommodationList;
	}

	public Integer getAccommodationId() {
		return accommodationId;
	}

	public void setAccommodationId(Integer accommodationId) {
		this.accommodationId = accommodationId;
	}

	public Integer getAdultCount() {
		return adultCount;
	}

	public void setAdultCount(Integer adultCount) {
		this.adultCount = adultCount;
	}
	
	public Integer getDays() {
		return days;
	}

	public void setDays(Integer days) {
		this.days = days;
	}
	
	public Integer getDiscountId() {
		return discountId;
	}

	public void setDiscountId(Integer discountId) {
		this.discountId = discountId;
	}
	

	public Integer getChildCount() {
		return childCount;
	}

	public void setChildCount(Integer childCount) {
		this.childCount = childCount;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	
	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public double getTax() {
		return tax;
	}
	
	public void setTax(double tax) {
		this.tax = tax;
	}
	
	public double getRefund() {
		return refund;
	}

	public void setRefund(double refund) {
		this.refund = refund;
	}

	public Integer getStatusId() {
		return statusId;
	}

	public void setStatusId(Integer statusId) {
		this.statusId = statusId;
	}

	public Integer getBookingId() {
		return bookingId;
	}

	public void setBookingId(Integer bookingId) {
		this.bookingId = bookingId;
	}
	
	public Integer getGuestBookingId() {
		return guestBookingId;
	}

	public void setGuestBookingId(Integer guestBookingId) {
		this.guestBookingId = guestBookingId;
	}

	public Integer getGuestId() {
		return guestId;
	}

	public void setGuestId(Integer guestId) {
		this.guestId = guestId;
	}

	public Integer getRoomId() {
		return roomId;
	}

	public void setRoomId(Integer roomId) {
		this.roomId = roomId;
	}
	
	public Timestamp getArrivalDate() {
		return arrivalDate;
	}

	public void setArrivalDate(Timestamp arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	public Timestamp getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Timestamp departureDate) {
		this.departureDate = departureDate;
	}
    public Timestamp getArrivalTime() {
		return arrivalTime;
	}

	public void setArrivalTime(Timestamp arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public Timestamp getDepartureTime() {
		return departureTime;
	}

	public void setDepartureTime(Timestamp departureTime) {
		this.departureTime = departureTime;
	}

	public String getSpecialRequest() {
		return specialRequest;
	}

	public void setSpecialRequest(String specialRequest) {
		this.specialRequest = specialRequest;
	}
	
	

	
	/*public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}*/

	public String getRouteMap() {
		return routeMap;
	}

	public void setRouteMap(String routeMap) {
		this.routeMap = routeMap;
	}

	public List<BookingDetail> getBookingDetailList() {
		return bookingDetailList;
	}

	public void setBookingDetailList(List<BookingDetail> bookingDetailList) {
		this.bookingDetailList = bookingDetailList;
	}

	public void setBookingDetailsId(Integer bookingDetailsId) {
		this.bookingDetailsId = bookingDetailsId;
	}

	public void setAccommodationList(List<PropertyAccommodation> accommodationList) {
		this.accommodationList = accommodationList;
	}
	
	public List<PmsBookedDetails> getBookedList() {
		return bookedList;
	}

	public void setBookedList(List<PmsBookedDetails> bookedList) {
		this.bookedList = bookedList;
	}
	
	public List<PmsRoomDetail> getRoomList() {
		return roomList;
	}

	public void setRoomList(List<PmsRoomDetail> roomList) {
		this.roomList = roomList;
	}
	
    public String getFilterBookingId() {
			return filterBookingId;
		}

	public void setFilterBookingId(String filterBookingId) {
			this.filterBookingId = filterBookingId;
		}
	/*public List < BookingListAction > getData() {
	        return data;
	}
	 
	public void setData(List < BookingListAction > data) {
	        this.data = data;
	} 
	  */
		public boolean isMailSubjectFlag() {
			return mailSubjectFlag;
		}
	
		public void setMailSubjectFlag(boolean mailSubjectFlag) {
			this.mailSubjectFlag = mailSubjectFlag;
		}
		
	    public boolean getMailFlag() {
			return mailFlag;
		}

		public void setMailFlag(boolean mailFlag) {
			this.mailFlag = mailFlag;
		}
	

		public double getTxtGrandTotal() {
			return txtGrandTotal;
		}

		public void setTxtGrandTotal(double txtGrandTotal) {
			this.txtGrandTotal = txtGrandTotal;
		}
		
		public boolean isMailSource() {
			return mailSource;
		}

		public void setMailSource(boolean mailSource) {
			this.mailSource = mailSource;
		}
		
		public Integer getSourceId() {
			return sourceId;
		}

		public void setSourceId(Integer sourceId) {
			this.sourceId = sourceId;
		}

		public boolean isBlockFlag() {
			return blockFlag;
		}

		public void setBlockFlag(boolean blockFlag) {
			this.blockFlag = blockFlag;
		}

		public long getRoomCnt() {
			return roomCnt;
		}

		public void setRoomCnt(long roomCnt) {
			this.roomCnt = roomCnt;
		}
	
		public String getCheckInTime() {
			return checkInTime;
		}

		public void setCheckInTime(String checkInTime) {
			this.checkInTime = checkInTime;
		}

		public String getCheckOutTime() {
			return checkOutTime;
		}

		public void setCheckOutTime(String checkOutTime) {
			this.checkOutTime = checkOutTime;
		}

		public Integer getUserId() {
			return userId;
		}

		public void setUserId(Integer userId) {
			this.userId = userId;
		}

		public double getMinimumAmount() {
			return minimumAmount;
		}

		public void setMinimumAmount(double minimumAmount) {
			this.minimumAmount = minimumAmount;
		}
		
		public String getDaysOfWeek(String checkedDays) {	    	
	    	
			PmsDayManager dayController=new PmsDayManager();
			List<PmsDays> listDays=dayController.list();
			ArrayList<String> arlDaysList=new ArrayList<String>();
			if(listDays.size()>0 && !listDays.isEmpty())
			{
				for(PmsDays days:listDays){
					arlDaysList.add(days.getDaysOfWeek());	
				}
			}
		   	 if(arlDaysList.contains(checkedDays)){    		
		   		 return "True";    		 
		   	 }    	 
		   	 else{
		   		 
		   		 return "False"; 
		   		 
		   	 }
		   	
		}
		
		private void createPdf(String content, String dest)
				throws IOException, DocumentException, com.lowagie.text.DocumentException {
			try{
				
								
				FileOutputStream fileout=new FileOutputStream(new File(dest));   
			    Document document = new Document();
			    PdfWriter.getInstance(document, fileout);
			    document.open();
			    HTMLWorker htmlWorker = new HTMLWorker(document);
			    htmlWorker.parse(new StringReader(content));
			    document.close();
			    fileout.close();
			    
				/*ITextRenderer render = new ITextRenderer();
				String url = new File("D:\\voucher-template-output.xhtml").toURI().toURL().toString();
				
				render.setDocument(url);
				render.layout();
				render.createPDF(fileout,true);
				fileout.flush();
				fileout.close();*/
				/*String url = new File(dest).toURI().toURL().toString();
		        String HTML_TO_PDF = getText("storage.pdf.output.path");
		        OutputStream os = new FileOutputStream(HTML_TO_PDF);       
		        ITextRenderer renderer = new ITextRenderer();
		        renderer.setDocument(url);      
		        renderer.layout();
		        renderer.createPDF(os);        
		        os.close();*/
		 
			}catch(Exception e){
				logger.error(e);
				e.printStackTrace();
			}
		}
}
