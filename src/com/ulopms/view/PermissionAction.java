package com.ulopms.view;

import java.io.IOException;
import java.sql.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;
import com.opensymphony.xwork2.ActionSupport;
import com.ulopms.controller.PermissionManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.Permission;
import com.ulopms.model.User;
import javax.servlet.http.HttpServletRequest;

public class PermissionAction extends ActionSupport implements
		ServletRequestAware, UserAware {

	private static final long serialVersionUID = 914982665758390091L;

	private static final Logger logger = Logger
			.getLogger(PermissionAction.class);

	private User user;

	// @Override
	// public User getModel() {
	// return user;
	// }

	@Override
	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	private PermissionManager permissionController = new PermissionManager();

	private HttpServletRequest request;

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	public PermissionAction() {

	}

	public String execute() {

		return SUCCESS;
	}

	public String GetPermission() throws IOException {
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			// String commentId="";
			// if(request.getParameter("id")!=null) commentId =
			// request.getParameter("id");
			response.setContentType("application/json");
			// response.getWriter().write("[{\"start\":\"2015-05-11T00:00:00\",\"title\":\"test\"}]");

			List<Permission> perList = permissionController.list();

			for (Permission p : perList) {

				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"DT_RowId\":\"" + p.getPermissionId() + "\"";
				jsonOutput += ",\"o_permission\":\"" + p.getPermissionName()
						+ "\"";
				// jsonOutput +=
				// ",\"o_user\":\""+c.getCreatedUser().getUserName()+"\"";
				// jsonOutput +=
				// ",\"o_userid\":\""+c.getCreatedUser().getUserId()+"\"";
				// jsonOutput += ",\"o_date\":\""+ c.getCreatedOn() + "\"";
				jsonOutput += "}";

			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");

		} catch (Exception e) {
			logger.error(e);
		} finally {

		}
		// this.setArea(a);

		return null;

	}

}
