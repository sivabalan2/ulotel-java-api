package com.ulopms.view;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;




//import com.medwecare.controller.AwsObjectManager;
//import com.medwecare.controller.CommentManager;
//import com.medwecare.controller.CommentUserManager;
////import com.medwecare.controller.NotificationManager;
//import com.medwecare.controller.ProjectManager;

//import com.medwecare.model.AwsObject;
//import com.medwecare.model.Notification;
//import com.medwecare.model.ObjectComment;
//import com.medwecare.model.ObjectCommentUser;
//import com.medwecare.model.Project;




import com.opensymphony.xwork2.ActionSupport;

import freemarker.template.Configuration;
import freemarker.template.Template;

import javax.servlet.http.HttpServletRequest;

import com.opensymphony.xwork2.ModelDriven;
import com.ulopms.controller.PmsGuestManager;
import com.ulopms.controller.PropertyTypeManager;
import com.ulopms.controller.UserLoginManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.PmsGuest;
import com.ulopms.model.PropertyType;
import com.ulopms.model.User;
import com.ulopms.util.Email;
import com.ulopms.util.Image;

public class PropertyTypeAction extends ActionSupport implements
ServletRequestAware, UserAware{

	private static final long serialVersionUID = 914982665758390091L;

	private Integer propertyTypeId;
	private String propertyTypeName;
	private String typeDescription;

	private PropertyType propertyType;
	
	private List<PropertyType> propertyTypeList;
	

	private static final Logger logger = Logger.getLogger(PropertyTypeAction.class);

	
	private User user;

	
	// @Override
	// public User getModel() {
	// return user;
	// }

	@Override
	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	
	private HttpServletRequest request;

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	public PropertyTypeAction() {

	}

	public String execute() {

		//this.patientId = getPatientId();
		return SUCCESS;
	}

	
	
	
	public String getPropertyTypes() throws IOException {
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PropertyTypeManager typeController = new PropertyTypeManager();
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");

			this.propertyTypeList = typeController.list();
			for (PropertyType propertyType : propertyTypeList) {

				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"DT_RowId\":\"" + propertyType.getPropertyTypeId()+ "\"";
				jsonOutput += ",\"propertyTypeName\":\"" + propertyType.getPropertyTypeName()+ "\"";
				jsonOutput += ",\"typeDescription\":\"" + propertyType.getTypeDescription()+ "\"";
				jsonOutput += ",\"isActive\":\"" + propertyType.getIsActive()+ "\"";
				jsonOutput += ",\"isDeleted\":\"" + propertyType.getIsDeleted()+ "\"";
				
				
				jsonOutput += "}";
			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}


	public Integer getPropertyTypeId() {
		return propertyTypeId;
	}

	public void setPropertyTypeId(Integer propertyTypeId) {
		this.propertyTypeId = propertyTypeId;
	}

	public String getTypeDescription() {
		return typeDescription;
	}

	public void setTypeDescription(String typeDescription) {
		this.typeDescription = typeDescription;
	}

	public PropertyType getPropertyType() {
		return propertyType;
	}

	public void setPropertyType(PropertyType propertyType) {
		this.propertyType = propertyType;
	}

	public List<PropertyType> getPropertyTypeList() {
		return propertyTypeList;
	}

	public void setPropertyTypeList(List<PropertyType> propertyTypeList) {
		this.propertyTypeList = propertyTypeList;
	}

	

}
