package com.ulopms.view;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
//import java.sql.Date;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.w3c.tidy.ParserImpl.ParseInline;

import com.ulopms.api.Booking;
import com.ulopms.api.Property;
import com.ulopms.controller.AccommodationAmenityManager;
import com.ulopms.controller.BookingDetailManager;
import com.ulopms.controller.BookingGuestDetailManager;
import com.ulopms.controller.GooglePlaceManager;
import com.ulopms.controller.OtaHotelsManager;
import com.ulopms.controller.PmsAmenityManager;
import com.ulopms.controller.PmsBookingManager;
import com.ulopms.controller.PmsGuestManager;
import com.ulopms.controller.PmsPropertyManager;
import com.ulopms.controller.PmsSourceManager;
import com.ulopms.controller.PmsStatusManager;
import com.ulopms.controller.PmsTagManager;
import com.ulopms.controller.PromotionDetailManager;
import com.ulopms.controller.PromotionManager;
import com.ulopms.controller.PropertyAccommodationInventoryManager;
import com.ulopms.controller.PropertyAccommodationManager;
import com.ulopms.controller.PropertyAccommodationRoomManager;
import com.ulopms.controller.PropertyAmenityManager;
import com.ulopms.controller.PropertyDiscountManager;
import com.ulopms.controller.PropertyGuestReviewManager;
import com.ulopms.controller.PropertyLandmarkManager;
import com.ulopms.controller.PropertyPhotoManager;
import com.ulopms.controller.PropertyRateDetailManager;
import com.ulopms.controller.PropertyRateManager;
import com.ulopms.controller.PropertyRatePlanDetailManager;
import com.ulopms.controller.PropertyReviewsManager;
import com.ulopms.controller.PropertyTaxeManager;
import com.ulopms.controller.PropertyTypeManager;
import com.ulopms.controller.ReportManager;
import com.ulopms.controller.UserLoginManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.AccommodationAmenity;
import com.ulopms.model.AccommodationRoom;
import com.ulopms.model.BookedDetail;
import com.ulopms.model.BookingDetail;
import com.ulopms.model.BookingDetailReport;
import com.ulopms.model.BookingGuestDetail;
import com.ulopms.model.DashBoard;
import com.ulopms.model.GoogleArea;
import com.ulopms.model.GooglePropertyAreas;
import com.ulopms.model.OtaHotelDetails;
import com.ulopms.model.OtaHotels;
import com.ulopms.model.PmsAmenity;
import com.ulopms.model.PmsAvailableRooms;
import com.ulopms.model.PmsBookedDetails;
import com.ulopms.model.PmsBooking;
import com.ulopms.model.PmsGuest;
import com.ulopms.model.PmsPromotionDetails;
import com.ulopms.model.PmsPromotions;
import com.ulopms.model.PmsProperty;
import com.ulopms.model.PmsSmartPrice;
import com.ulopms.model.PmsSource;
import com.ulopms.model.PmsStatus;
import com.ulopms.model.PmsTags;
import com.ulopms.model.PropertyAccommodation;
import com.ulopms.model.PropertyAccommodationInventory;
import com.ulopms.model.PropertyAmenity;
import com.ulopms.model.PropertyDiscount;
import com.ulopms.model.PropertyGuestReview;
import com.ulopms.model.PropertyLandmark;
import com.ulopms.model.PropertyPhoto;
import com.ulopms.model.PropertyRate;
import com.ulopms.model.PropertyRateDetail;
import com.ulopms.model.PropertyRatePlanDetail;
import com.ulopms.model.PropertyReviews;
import com.ulopms.model.PropertyTags;
import com.ulopms.model.PropertyTaxe;
import com.ulopms.model.User;
import com.ulopms.util.DateUtil;
import com.ulopms.util.Email;
import com.amazonaws.http.HttpResponse;
import com.auth0.jwt.algorithms.Algorithm;
//import com.medwecare.controller.AwsObjectManager;
//import com.medwecare.controller.CommentManager;
//import com.medwecare.controller.CommentUserManager;
////import com.medwecare.controller.NotificationManager;
//import com.medwecare.controller.ProjectManager;
//import com.medwecare.model.AwsObject;
//import com.medwecare.model.Notification;
//import com.medwecare.model.ObjectComment;
//import com.medwecare.model.ObjectCommentUser;
//import com.medwecare.model.Project;
import com.opensymphony.xwork2.ActionSupport;

import freemarker.template.Configuration;
import freemarker.template.Template;

public class BookingAction extends ActionSupport implements
ServletRequestAware, SessionAware, UserAware{

	private static final long serialVersionUID = 914982665758390091L;
	private List<PmsBookedDetails> bookedList;
	private List<BookedDetail> bookList;
	private Integer bookingId;
	private Timestamp arrivalDate;
	private Timestamp departureDate;
	private Timestamp nightDepartureDate;
	private Integer sourceTypeId;
	private Integer rooms;
	private Integer total;
	private Integer tax;
	private Long roomCount;
	private File myFile;
	private String myFileContentType;
	private String myFileFileName;
	private String altName;
	private double totalAmount;
	private double totalTax;
	private double securityDeposit;
	public Integer propertyId;
	private Integer userId;
	private String invoiceId;
	public Integer accommodationId;
	private Integer partnerPropertyId;
	private Integer selectPartnerPropertyId;	
	public String accommodationTypeId;
	private Integer adult;
	private Integer child;
	private double base;
	private Integer min;
	private double extraadult;
	private double extrachild;
	private double extracharges;
	private Integer minadult;
	private Integer minchild;
	private boolean blockFlag;
	private Integer areaId;
	private Integer propertyAccommodationId;
	private String startBlockDate;
	private String endBlockDate;	
	private boolean statusFlag;
	
	private Integer statusId;
	private Integer sourceId;
	private Double lastAmount;
	private Double lastExtraChild;
	private Double lastExtraAdult;
	private Double roomAmount;
	private Double roomExtraChild;
	private Double roomExtraAdult;
	private Double nightAmount;
	private Double nightExtraChild;
	private Double nightExtraAdult;
	private Double flatAmount;
	private Double flatExtraChild;
	private Double flatExtraAdult;
	private Double getRooms=0.0;
	private Double bookRooms=0.0;
	private Double getNights=0.0;
	private Double bookNights=0.0;
	private String promotionsType;
	private String promotionsRoomNight;
	private Integer promotionsRoomCount;
	private Integer promotionAccommodationId;
	private Integer promotionId;
	private String strStartDate;
	private String strEndDate;
	private String displayHoursForTimer;
	
	private String strArrivalDate;
	private String strDepartureDate;
	private String startDate;
	private String otaBookingId;
	private String endDate;
	//email variable
	private String propertyName;
	private Integer propertyDiscountId;
	private String propertyDiscountName;
	private String latitude;
	private String longitude;
	private String addressLine1;
	private String addressLine2;
	private String mobileNumber;
	private String emailId;
	private String customerName;
	private String propertyEmail;
	private String locationName;
	private String reservationManagerEmail;
	private String reservationManagerContact;
	private String routeMap;
	private String timeHours;
	private String sourceName;
	private String specialRequest;
	private Double dblTaxAmount;
	private Double dblAmount;
	private Double hotelPay;
	private String paymentStatus;
	private Integer days;
	private String promotionFirstName;
	private String promotionSecondName;
	
	
	private List<PmsProperty> propertyList;
	

	private long roomCnt;
	private long soldCnt;
	private long canceledCnt;
	
	public long getRoomCnt() {
		return roomCnt;
	}

	

	public void setRoomCnt(long roomCnt) {
		this.roomCnt = roomCnt;
	}

	int totalRoomCount = 0;
	int totalAdultCount = 0;
	int totalChildCount = 0;
	double totalTaxAmount = 0;
	double totalBaseAmount = 0;
	double totalAdvanceAmount = 0;
	double totalRoomBaseAmount = 0;
	double totalActualBaseAmount = 0;
	double totalOtaCommissionAmount=0;
	double totalOtaTaxAmount=0;
	Integer paynowDiscount = 0;
	double addonAmount = 0;		
	String addonDescription;
	
	Timestamp arrivalDatey = null;
	
	private PmsBooking booking;
	
	private String chartDate;
	
	private Timestamp dateChart;

	

	private String strDate;
	//TimeZone timeZone = TimeZone.getDefault();
	
	

	List<BookingListAction> array = new ArrayList<BookingListAction>();
  
	List<BookingListAction> types = new ArrayList<BookingListAction>();
	
	private List<PmsBooking> bookingList;
	
	private List<PmsAvailableRooms> availableList;
	
	private List<PropertyAccommodation> accommodationList;
	
     private List<BookingDetail> bookingDetailList;
	
	private List<DashBoard> dashBoardList;


	private static final Logger logger = Logger.getLogger(BookingAction.class);

	private HttpSession session;

	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}
	
	private SessionMap<String,Object> sessionMap;  
	  
	//getters and setters  
	  
	@Override  
	public void setSession(Map<String, Object> map) {  
	    sessionMap=(SessionMap)map;  
	}  
	
	
	private User user;

	
	// @Override
	// public User getModel() {
	// return user;
	// }

	@Override
	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	
	private HttpServletRequest request;
	
	private DashBoard arrival;

	private DashBoard departure;

	private DashBoard occupancy;

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	public BookingAction() {

	}

	public String execute() {

		
		return SUCCESS;
	}

	public String getAvailability() throws IOException {
		try {
			
			DateFormat format = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
		    java.util.Date dateStart = format.parse(getStrStartDate());
		    java.util.Date dateEnd = format.parse(getStrEndDate());
		   
		    
		    Calendar calCheckStart=Calendar.getInstance();
		    calCheckStart.setTime(dateStart);
		    java.util.Date checkInDate = calCheckStart.getTime();
		    this.arrivalDate=new Timestamp(checkInDate.getTime());
		    sessionMap.put("arrivalDate",arrivalDate);  
		    
		    Calendar calCheckEnd=Calendar.getInstance();
		    calCheckEnd.setTime(dateEnd);
		    java.util.Date checkOutDate = calCheckEnd.getTime();
		    this.departureDate=new Timestamp(checkOutDate.getTime());
		    sessionMap.put("departureDate",departureDate);
	
		    Map mapDate=new HashMap();  
			mapDate.put("arrivalDate", arrivalDate);
			mapDate.put("departureDate", departureDate);
			
			/*this.arrivalDate = getArrivalDate();
			sessionMap.put("arrivalDate",arrivalDate);
	
			this.departureDate = getDepartureDate();
			sessionMap.put("departureDate",departureDate);
			*/
			Timestamp timestamp = new Timestamp(System.currentTimeMillis());
	
			Calendar cal = Calendar.getInstance();
			cal.setTime(this.departureDate);
			cal.add(Calendar.DATE, -1);
			String startDate = new SimpleDateFormat("yyyy-MM-dd").format(this.arrivalDate);
			String endDate = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
			DateTime start = DateTime.parse(startDate);
			DateTime end = DateTime.parse(endDate);
			java.util.Date arrival = start.toDate();
			java.util.Date departure = end.toDate();
	
			this.propertyId = (Integer) sessionMap.get("propertyId");
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
	
			int roomCount=0,availableCount=0,totalCount=0;
	    	Double dblPercentCount=0.0,dblSmartPricePercent=0.0;
	    	Double dblPercentFrom,dblPercentTo;
	    	Boolean isPositive=false,isNegative=false;
	    	List<PmsSmartPrice> listSmartPriceDetail=null; 
	    	List<AccommodationRoom> listAccommodationRoom=null;
			
			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
			PmsBookingManager bookingController = new PmsBookingManager();
			
			PropertyTaxeManager taxController = new PropertyTaxeManager();
			PropertyRateManager rateController = new PropertyRateManager();
			PmsSourceManager sourceController = new PmsSourceManager();
			PmsPropertyManager propertyController=new PmsPropertyManager();
			PropertyRateDetailManager detailController = new PropertyRateDetailManager();
			PmsSource pmsSource = sourceController.find(1);
			response.setContentType("application/json");
			this.availableList = bookingController.list(getPropertyId(),getArrivalDate(),getDepartureDate());
			List<DateTime> between = DateUtil.getDateRange(start, end);
			for (PmsAvailableRooms available : availableList) {
	
				if (!jsonOutput.equalsIgnoreCase(""))
				jsonOutput += ",{";
		
				else
				jsonOutput += "{";
				
				PmsProperty property = propertyController.find(getPropertyId());
				Timestamp todayDate = new Timestamp(System.currentTimeMillis());
				String currDate = new SimpleDateFormat("yyyy-MM-dd").format(todayDate);
				String fromDate = new SimpleDateFormat("yyyy-MM-dd").format(property.getTaxValidFrom());
			    String toDate = new SimpleDateFormat("yyyy-MM-dd").format(property.getTaxValidTo());
			    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			    Date FromDate1 = sdf.parse(fromDate);
			    Date ToDate1 = sdf.parse(toDate);
			    Date CurrDate = sdf.parse(currDate);
		        Date date2 = sdf.parse(toDate);
		        int comparedVal1 = CurrDate.compareTo(FromDate1);
		        int comparedVal2 = CurrDate.compareTo(ToDate1);
			    jsonOutput += "\"propertyId\":\"" + getPropertyId() + "\"";  			   
		
				int dateCount=0;
				double amount=0;
				double totalAmount=0;
				double totalExtraChildAmount=0;
				double totalExtraAdultAmount=0;
				double totalExtraInfantAmount=0;
				double extraChildAmount=0,extraAdultAmount=0,extraInfantAmount=0;
				
				DateFormat f = new SimpleDateFormat("EEEE");
				Boolean SmartPriceEnableCheck=false;
				double baseTotalAmount=0.0,smartPricePercent=0.0,discountPercent=0.0;
				int smartPriceId=0,discountId=0;
				//unchanged date
				SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
				Timestamp FromDate=(Timestamp)mapDate.get("arrivalDate");
				Timestamp ToDate=(Timestamp)mapDate.get("departureDate");
				
				String strFromDate=format2.format(FromDate);
				String strToDate=format2.format(ToDate);
				
		    	DateTime startdate = DateTime.parse(strFromDate);
		        DateTime enddate = DateTime.parse(strToDate);
		        java.util.Date fromdate = startdate.toDate();
		 		java.util.Date todate = enddate.toDate();
		 		
				int diffInDays = (int) ((todate.getTime() - fromdate.getTime()) / (1000 * 60 * 60 * 24));
				jsonOutput += ",\"diffDays\":\"" + diffInDays+ "\"";
				
				
				
				for (DateTime d : between)
				{

					int rateCount=0,rateIdCount=0;
					List<PropertyRate> propertyRateList =  rateController.listAllDates(property.getPropertyId(),available.getAccommodationId(),pmsSource.getSourceId(),d.toDate());
					//List<PropertyRate> propertyRateList = rateController.list(accommodation.getAccommodationId(),getSourceId(), d.toDate());
					List<PropertyRate> currentDateSmartPriceActive=rateController.listCurrentSmartPriceActive(property.getPropertyId(),available.getAccommodationId(), d.toDate());
			    	if(propertyRateList.size()>0)
			    	{
			    		 for (PropertyRate pr : propertyRateList)
		    			 {
		    				 int propertyRateId = pr.getPropertyRateId();
				    		 Boolean SmartPriceCheck=pr.getSmartPriceIsActive();
				    		 if(SmartPriceCheck){
				    			 List<PropertyRateDetail> rateDetailList =   detailController.list(propertyRateId,f.format(d.toDate()).toLowerCase());
					    		 if(rateDetailList.size()>0){
					    			 for (PropertyRateDetail rate : rateDetailList){
					    				 if(rateCount<1){
					    					 SmartPriceEnableCheck=true;
						    				 amount +=  rate.getBaseAmount();
						    				 extraAdultAmount+=rate.getExtraAdult();
						    				 extraChildAmount+=rate.getExtraChild();
						    				 rateCount++;
					    				 }
					    			 }
					    		 }
				    		 }else{
				    			 List<PropertyRateDetail> rateDetailList =   detailController.list(propertyRateId,f.format(d.toDate()).toLowerCase());
    				    		 if(rateDetailList.size()>0){
    				    			 if(rateCount<1){
    				    				 for (PropertyRateDetail rate : rateDetailList){
        				    				 if(currentDateSmartPriceActive.size()>0){
        				    					 SmartPriceEnableCheck=true;
        						    			 amount +=  rate.getBaseAmount();
	        				    				 extraAdultAmount+=rate.getExtraAdult();
	        				    				 extraChildAmount+=rate.getExtraChild();
        				    				 }else{
        				    					 amount +=  rate.getBaseAmount();
	        				    				 extraAdultAmount+=rate.getExtraAdult();
	        				    				 extraChildAmount+=rate.getExtraChild();
        				    				 }
        				    			 }
    				    				 rateCount++;
    				    			 }
    				    		 }else{
    				    			 if(propertyRateList.size()==rateIdCount){
    				    				 amount += available.getBaseAmount();
            				    		 extraAdultAmount+=available.getExtraAdult();
        			    				 extraChildAmount+=available.getExtraChild();
    				    			 }
        				    	 }
				    		 }
		    			 }	    		 
			    	 }
			    	 else{
			    		 amount += available.getBaseAmount();
			    		 extraAdultAmount+=available.getExtraAdult();
	    				 extraChildAmount+=available.getExtraChild();
			    	 }
					dateCount++;
				
				}
		
				if(SmartPriceEnableCheck){
			    	 listAccommodationRoom=accommodationController.listPropertyTotalRooms(propertyId, available.getAccommodationId());
      				 if(listAccommodationRoom.size()>0 && !listAccommodationRoom.isEmpty()){
      					 for(AccommodationRoom roomsList:listAccommodationRoom){
      						 roomCount=(int) roomsList.getRoomCount();
      						 long minimum = getAvailableCount(propertyId,getArrivalDate(),getDepartureDate(),available.getRoomAvailable(),available.getAccommodationId()); 
      						 availableCount=(int)minimum;
      						 dblPercentCount=(double)((roomCount-availableCount)*100)/roomCount;
      						 totalCount=(int) Math.round(dblPercentCount);
      						 listSmartPriceDetail=detailController.listSmartPriceDetail(totalCount);
      						 if(listSmartPriceDetail.size()>0 && !listSmartPriceDetail.isEmpty()){
      							 for(PmsSmartPrice smartPrice: listSmartPriceDetail){
      								 dblSmartPricePercent=smartPrice.getAmountPercent();
      								 sessionMap.put("baseTotalAmount", amount);
		    				    	 sessionMap.put("smartPriceId", smartPrice.getSmartPriceId());
		    				    	 sessionMap.put("smartPricePercent", dblSmartPricePercent);
      								 dblPercentFrom=smartPrice.getPercentFrom();
      								 dblPercentTo=smartPrice.getPercentTo();
      								 if(dblPercentFrom>=0 && dblPercentTo<=59){
      									 isNegative=true;
      								 }else if(dblPercentFrom>=60 && dblPercentTo<=99){
      									 isPositive=true;
      								 }
      								 if(isNegative){
      									amount=amount-(amount*dblSmartPricePercent/100);
      								 }
      								 if(isPositive){
      									amount=amount+(amount*dblSmartPricePercent/100);
      								 }
      							 }
      						 }
      					 }
      				 }
      				totalAmount = amount;
      				if(dateCount>1){
				    	 totalExtraAdultAmount=Math.round(extraAdultAmount/dateCount);
				    	 totalExtraChildAmount=Math.round(extraChildAmount/dateCount);
				    	 totalExtraInfantAmount=Math.round(extraInfantAmount/dateCount);
				     }
				     else{
				    	 totalExtraAdultAmount=extraAdultAmount;
				    	 totalExtraChildAmount=extraChildAmount;
				    	 totalExtraInfantAmount=extraInfantAmount;
				     }
			     } else{
					 jsonOutput += ",\"baseTotalAmount\":\"" + amount + "\"";
					totalAmount = amount;
				     if(dateCount>1){
				    	 totalExtraAdultAmount=Math.round(extraAdultAmount/dateCount);
				    	 totalExtraChildAmount=Math.round(extraChildAmount/dateCount);
				    	 totalExtraInfantAmount=Math.round(extraInfantAmount/dateCount);
				     }
				     else{
				    	 totalExtraAdultAmount=extraAdultAmount;
				    	 totalExtraChildAmount=extraChildAmount;
				    	 totalExtraInfantAmount=extraInfantAmount;
				     }
				}
				
				if(property.getTaxIsActive() == true ){
		            double taxe =  (totalAmount==0 ? available.getBaseAmount() : totalAmount );
		            double taxs = taxe/diffInDays;
		            PropertyTaxe tax = taxController.find(taxs);
				    double taxes = Math.round(tax.getTaxPercentage() / 100 * taxs) ;
				    jsonOutput += ",\"tax\":\"" + taxes + "\"";
	 			    jsonOutput += ",\"taxPercentage\":\"" + tax.getTaxPercentage() + "\"";
	 		    } else{
 		 		    jsonOutput += ",\"tax\":\"" + 0 + "\"";
	 		 		jsonOutput += ",\"taxPercentage\":\"" + 0 + "\"";
	 		    }
				
				SimpleDateFormat format1=new SimpleDateFormat("yyyy-MM-dd");
				jsonOutput += ",\"baseAmount\":\"" + (totalAmount==0 ? available.getBaseAmount() : totalAmount ) + "\"";
				jsonOutput += ",\"totalAmount\":\"" + (totalAmount==0 ? available.getBaseAmount() : totalAmount ) + "\"";
				String strArrivalDate=format1.format(getArrivalDate());
				String strDepartureDate=format1.format(getDepartureDate());
				
				jsonOutput += ",\"arrivalDate\":\"" + strArrivalDate+ "\"";
				jsonOutput += ",\"departureDate\":\"" + strDepartureDate+ "\"";
				
				jsonOutput += ",\"rooms\":\"" + 1 + "\"";
				//jsonOutput += ",\"roomCount\":\"" + (available.getRoomCount() == null ? available.getNoOfUnits() : available.getRoomCount()) + "\"";
				//jsonOutput += ",\"roomCount\":\"" + available.getRoomCount() + "\"";
				jsonOutput += ",\"accommodationId\":\"" + available.getAccommodationId() + "\"";
				jsonOutput += ",\"accommodationType\":\"" + available.getAccommodationType().toUpperCase() + "\"";
				jsonOutput += ",\"minOccupancy\":\"" + available.getMinOccupancy() + "\"";
				jsonOutput += ",\"maxOccupancy\":\"" + available.getMaxOccupancy() + "\"";
				jsonOutput += ",\"noOfAdults\":\"" + available.getNoOfAdults() + "\"";
				jsonOutput += ",\"noOfChild\":\"" + available.getNoOfChild() + "\"";
				jsonOutput += ",\"extraAdult\":\"" + (totalExtraAdultAmount==0 ? available.getExtraAdult() : totalExtraAdultAmount ) + "\"";
				jsonOutput += ",\"extraChild\":\"" + (totalExtraChildAmount==0 ? available.getExtraChild() : totalExtraChildAmount )+ "\"";
				jsonOutput += ",\"extraInfant\":\"" + (totalExtraInfantAmount==0 ? available.getExtraInfant() : totalExtraInfantAmount )+ "\"";
		
				if(SmartPriceEnableCheck){
	    			smartPricePercent=(Double)sessionMap.get("smartPricePercent");
	    			smartPriceId=(Integer)sessionMap.get("smartPriceId");
	    			baseTotalAmount=(Double)sessionMap.get("baseTotalAmount");
	    			jsonOutput += ",\"baseTotalAmount\":\"" + baseTotalAmount + "\"";
					jsonOutput += ",\"smartPriceId\":\"" + smartPriceId + "\"";
					jsonOutput += ",\"smartPricePercent\":\"" + smartPricePercent + "\"";
	    		}else{
	    			jsonOutput += ",\"baseAmount\":\"" + (totalAmount==0 ? available.getBaseAmount() : totalAmount ) + "\"";
	    			jsonOutput += ",\"smartPricePercent\":\"" + (smartPricePercent==0.0? "0":smartPricePercent)+ "\"";
		    		jsonOutput += ",\"smartPriceId\":\"" + (smartPriceId==0?"0":smartPriceId) + "\"";
	    		}
				
	    		jsonOutput += ",\"discountPercent\":\"" + (discountPercent==0.0? "0":discountPercent)+ "\"";
	    		jsonOutput += ",\"discountId\":\"" + (discountId==0?"0":discountId) + "\"";
				//jsonOutput += ",\"tax\":\"" + 0 + "\"";
		
				long rmc = 0;
				this.roomCnt = rmc;
				List<DateTime> between1 = DateUtil.getDateRange(start, end);
				List<Long> myList1 = new ArrayList<Long>();
				for (DateTime d : between)
				{
					java.util.Date availDate = d.toDate();
					
					PmsAvailableRooms roomAvaliableCount = bookingController.findCount(availDate, available.getAccommodationId());
					if(roomAvaliableCount.getRoomCount() == null){
						this.roomCnt = (available.getRoomAvailable()-rmc);
					}
					else{
						this.roomCnt = (available.getRoomAvailable()-roomAvaliableCount.getRoomCount());
					}
					myList1.add(this.roomCnt);
				}
				
		
				//jsonOutput += ",\"available\":\"" + available.getAvailable() + "\"";
				jsonOutput += ",\"available\":\"" + Collections.min(myList1) + "\"";
				jsonOutput += "}";
			}
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
		} catch (Exception e) {
		logger.error(e);
		e.printStackTrace();
		} finally {

		}

		return null;

		}
	public String getNewRoomAvailability(){

		   String output=null;
		   try{
			   ArrayList arlAccommId=new ArrayList();
				 String[] parts = getAccommodationTypeId().split(",");
		 		 int[] intArray = new int[parts.length];
		 		   for (int i = 0; i < parts.length; i++) {
	 		         String numberAsString = parts[i];
	 		         intArray[i] = Integer.parseInt(numberAsString);
	 		         arlAccommId.add(intArray[i]);
	 		      }
		 		   
		 		  Timestamp timestamp = new Timestamp(System.currentTimeMillis());
			 	    DateFormat format = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH); 
			 	    java.util.Date dateStart = format.parse(getStrArrivalDate());
			 	    java.util.Date dateEnd = format.parse(getStrDepartureDate());
			 	   
			 	    
		 		   
		 		   
			   Calendar calCheckStart=Calendar.getInstance();
		 	    calCheckStart.setTime(dateStart);
		 	    java.util.Date checkInDate = calCheckStart.getTime();
		 	    this.arrivalDate=new Timestamp(checkInDate.getTime());
		 	   String strCheckInDate=new SimpleDateFormat("yyyy-MM-dd").format(checkInDate.getTime());
		 	    Calendar calCheckEnd=Calendar.getInstance();
		 	    calCheckEnd.setTime(dateEnd);
		 	    java.util.Date checkOutDate = calCheckEnd.getTime();
		 	    this.departureDate=new Timestamp(checkOutDate.getTime());
		 	    
		 		
		 		Calendar calto = Calendar.getInstance();
		 		calto.setTime(this.departureDate);
		 	    calto.add(Calendar.DATE, -1);
		 		SimpleDateFormat format1=new SimpleDateFormat("yyyy-MM-dd");
		 		String startDate = new SimpleDateFormat("yyyy-MM-dd").format(this.arrivalDate);
		 		String endDate = new SimpleDateFormat("yyyy-MM-dd").format(calto.getTime());
		 		DateTime start = DateTime.parse(startDate);
		 		DateTime end = DateTime.parse(endDate);
		 		java.util.Date arrival = start.toDate();
		 		java.util.Date departure = end.toDate();
		 		
			    DecimalFormat df = new DecimalFormat("###.##");
			    DateFormat f = new SimpleDateFormat("EEEE");
			    
			   if(getSourceTypeId()==null){
				   this.sourceTypeId=1;
			   }
			    
			      
				this.propertyId = getPropertyId();
				
				
			   
				Calendar cal = Calendar.getInstance();
				cal.setTime(this.departureDate);
		        cal.add(Calendar.DATE, -1);
				PropertyAccommodationInventoryManager inventoryController= new PropertyAccommodationInventoryManager();
				
				String jsonOutput = "";
				HttpServletResponse response = ServletActionContext.getResponse();
				response.setContentType("application/json");
				java.util.Date currentdate=new java.util.Date();
	         	Calendar curDate=Calendar.getInstance();
	         	curDate.setTime(currentdate);
	         	curDate.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
	         	int bookedHours=curDate.get(Calendar.HOUR);
	         	int bookedMinute=curDate.get(Calendar.MINUTE);
	         	int bookedSecond=curDate.get(Calendar.SECOND);
	         	int AMPM=curDate.get(Calendar.AM_PM);
	         	java.util.Date cudate=curDate.getTime();
	         	String strBookingDate=new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH).format(curDate.getTime());
	         	String hours="",minutes="",seconds="",timeHours="";
	         	if(bookedHours<10){
	         		hours=String.valueOf(bookedHours);
	         		hours="0"+hours;
	         	}else{
	         		hours=String.valueOf(bookedHours);
	         	}
	         	if(bookedMinute<10){
	         		minutes=String.valueOf(bookedMinute);
	         		minutes="0"+minutes;
	         	}else{
	         		minutes=String.valueOf(bookedMinute);
	         	}
	         	
	         	if(bookedMinute<10){
	         		seconds=String.valueOf(bookedSecond);
	         		seconds="0"+seconds;
	         	}else{
	         		seconds=String.valueOf(bookedSecond);
	         	}
	         	
	         	if(AMPM==0){//If the current time is AM
	         		timeHours=strBookingDate+" "+hours+" : "+minutes+" : "+seconds+" AM ";
	         	}else if(AMPM==1){//If the current time is PM
	         		timeHours=strBookingDate+" "+hours+" : "+minutes+" : "+seconds+" PM ";
	         	}
	         	
	         	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	         	
	         	String checkStartDate = new SimpleDateFormat("yyyy-MM-dd").format(currentdate);
	 	    	String strStartTime=checkStartDate+" 12:00:00"; 
	 	   	    java.util.Date dteStartDate = sdf.parse(strStartTime);
	 	   	    Calendar calStart=Calendar.getInstance();
	 	   	    calStart.setTime(dteStartDate);
	 	   	    calStart.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
	 	   	    String StartDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calStart.getTime());
	 	    	java.util.Date lastFirst = sdf.parse(StartDate);
	 	    	String[] weekdays={"Monday","Tuesday","Wednesday","Thursday","Sunday"};
	 		    String[] weekend={"Friday","Saturday"};
				PmsPropertyManager propertyController = new PmsPropertyManager();		
				PropertyAccommodationManager accommodationManager = new PropertyAccommodationManager();
				PromotionManager promotionController=new PromotionManager();
				PromotionDetailManager promotionDetailController=new PromotionDetailManager();
				PmsBookingManager bookingController=new PmsBookingManager();
				
				
				
				PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
			    double commissionrange1=150,commissionrange2=225,commissionrange3=300,lastdiscountpercent1=10,lastdiscountpercent2=15,lastdiscountpercent3=20;
			    
			    this.propertyList=propertyController.list(this.propertyId);
			    if(this.propertyList.size()>0 && !this.propertyList.isEmpty()){
			    	for(PmsProperty property:this.propertyList){

			  			SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
			  			
			  			
			  			String strFromDate=format2.format(this.arrivalDate);
			  			String strToDate=format2.format(this.departureDate);
			  	    	DateTime startdate = DateTime.parse(strFromDate);
			  	        DateTime enddate = DateTime.parse(strToDate);
			  	        java.util.Date fromdate = startdate.toDate();
			  	 		java.util.Date todate = enddate.toDate();
			  	 		 
			  			int diffInDays = (int) ((todate.getTime() - fromdate.getTime()) / (1000 * 60 * 60 * 24));
			  			
			  			
			  			
			  			
			  			int arlListTotalAccomm=0,soldOutTotalAccomm=0,accommCount=0;
			  			ArrayList<String> arlListPromoType=new ArrayList<String>();
			  			
			  			String promotionType="NA",discountType="NA",promotionFirstName=null,promotionSecondName=null,
			  					promotionFirstType=null,promotionSecondType=null;
						double dblDiscountINR=0,dblDiscountPercent=0;
						boolean promoStatus=false,isFlat=false,isEarly=false,isLast=false,isSoldout=false;
						List<PmsPromotions> listPromotions=promotionController.listDatePromotions(property.getPropertyId(), dateStart);
						 if(listPromotions.size()>0 && !listPromotions.isEmpty()){
						   	for(PmsPromotions promotion:listPromotions){
						   		if(promotion.getPromotionType().equals("F")){
						   			promotionType=promotion.getPromotionType();
						   			promoStatus=true;
						   			isFlat=true;
						   		}else if(promotion.getPromotionType().equals("E")){
						   			promotionType=promotion.getPromotionType();
						   			Timestamp tsStartPromo=promotion.getFirstRangeStartDate();
						   			Timestamp tsEndPromo=promotion.getFirstRangeEndDate();
						   			String startStayDate = new SimpleDateFormat("yyyy-MM-dd").format(tsStartPromo);
									String endStayDate = new SimpleDateFormat("yyyy-MM-dd").format(tsEndPromo);
									DateTime dtstart = DateTime.parse(startStayDate);
								    DateTime dtend = DateTime.parse(endStayDate);
						   			List<DateTime> between = DateUtil.getDateRange(dtstart, dtend);
						   			if(between.size()>0 && !between.isEmpty()){
						   				for(DateTime d:between){
						   					if(cudate.equals(d.toDate())){
						   						promoStatus=true;
						   						isEarly=true;
						   					}
						   				}
						   			}else{
						   				promoStatus=false;
						   			}
						   		}
						   		
								List<PmsPromotionDetails> listPromoDetails=promotionDetailController.listPromotionDetails(promotion.getPromotionId(),f.format(dateStart).toLowerCase());
								if(listPromoDetails.size()>0 && !listPromoDetails.isEmpty()){
									for(PmsPromotionDetails promodetails:listPromoDetails){
										dblDiscountINR=promodetails.getPromotionInr();
										if(dblDiscountINR>0){
											discountType="INR";
										}
										dblDiscountPercent=promodetails.getPromotionPercentage();
										if(dblDiscountPercent>0){
											discountType="PERCENT";
										}
									}
								}
							}
						 }
						String sellingType="NA",sellingDays="NA";
						sellingDays=f.format(dateStart).toLowerCase();
						boolean blnWeekdays=false,blnWeekend=false;
						for(int a=0;a<weekdays.length;a++){
							if(sellingDays.equalsIgnoreCase(weekdays[a])){
								blnWeekdays=true;
							}
						}
						
						for(int b=0;b<weekend.length;b++){
							if(sellingDays.equalsIgnoreCase(weekend[b])){
								blnWeekend=true;
							}
						}
						
	    				if(property.getLocationType()!=null){
	    					if(property.getLocationType().getLocationTypeName().equalsIgnoreCase("Leisure")){
								if(blnWeekdays){
									sellingType="weekdays";
								}
								
								if(blnWeekend){
									sellingType="weekend";
								}	
							}
							
							if(property.getLocationType().getLocationTypeName().equalsIgnoreCase("Metro")){
								sellingType="metro";
							}
	    				}
			  			List<PropertyAccommodation> accommodationList =  accommodationManager.listSortByPrice(property.getPropertyId(), sellingType);
			  			if(accommodationList.size()>0)
			  			{
			  				for (PropertyAccommodation accommodation : accommodationList) {
			  					if(arlAccommId.contains(accommodation.getAccommodationId())){
				  					

				  					int dateCount=0;
				  					double amount=0.0,extraAdultAmount=0.0,extraChildAmount=0.0,totalMaximumAmount=0.0,totalMinimumAmount=0.0,diffAmount=0;
				  					int roomCount=0,availableCount=0,totalCount=0;
				  				    double dblPercentCount=0.0;
				  				  List<AccommodationRoom> listAccommodationRoom=null;
				  					 List<DateTime> between = DateUtil.getDateRange(start, end);
				  					 
				  					double sellrate=0,minAmount=0,maxAmount=0,promoAmount=0,firstPromoAmount=0,secondPromoAmount=0,firstInr=0,secondInr=0;
				  					listAccommodationRoom=accommodationController.listPropertyTotalRooms(property.getPropertyId(), accommodation.getAccommodationId());
				  					 boolean blnAvailable=false,blnSoldout=false;
				  					if(listAccommodationRoom.size()>0 && !listAccommodationRoom.isEmpty()){
				  						
				  						for(AccommodationRoom roomsList:listAccommodationRoom){
				  							roomCount=(int) roomsList.getRoomCount();
				  							if(roomCount>0){
				  								long minimum = getAvailableCount(property.getPropertyId(),getArrivalDate(),getDepartureDate(),roomCount,accommodation.getAccommodationId());
				  								if((int)minimum>0){
				  									blnAvailable=true;
				  									blnSoldout=false;
				  									arlListTotalAccomm++;
				  								}else if((int)minimum==0){
				  									blnSoldout=true;
				  									blnAvailable=false;
				  									soldOutTotalAccomm++;
				  								}
				  							}
				  							 break;
				  						 }
				  					 }
				  					
				  				     for (DateTime d : between)
				  				     {

								    	 long rmc = 0;
							   			 this.roomCnt = rmc;
							        	 int sold=0;
							        	 java.util.Date availDate = d.toDate();
							        	 String days=f.format(d.toDate()).toLowerCase();
										 PropertyAccommodationInventory inventoryCount = inventoryController.findInventoryCount(availDate,accommodation.getAccommodationId());
										 if(inventoryCount != null){
										 PmsAvailableRooms roomCount1 = bookingController.findBookingCount(availDate, accommodation.getAccommodationId(),inventoryCount.getInventoryId());
										 if(roomCount1.getRoomCount() == null) {
										 	String num1 = inventoryCount.getInventoryCount();
											PmsAvailableRooms bookingRoomCount=bookingController.findCount(availDate, accommodation.getAccommodationId());
											PropertyAccommodation accomm=accommodationController.find(accommodation.getAccommodationId());
											Integer totavailable=0;
											if(accommodation!=null){
												totavailable=accommodation.getNoOfUnits();
											}
											if(bookingRoomCount.getRoomCount()!=null){
												long lngRooms=bookingRoomCount.getRoomCount();
												long num2 = Long.parseLong(num1);
												this.roomCnt = num2;
												sold=(int) (lngRooms);
												int availRooms=0;
												availRooms=totavailable-sold;
												/*if(Integer.parseInt(num1)>availRooms){
													this.roomCnt = availRooms;	
												}else{
													this.roomCnt = num2;
												}*/
												this.roomCnt = num2;
														
											}else{
												 long num2 = Long.parseLong(num1);
												 this.roomCnt = num2;
												 sold=(int) (rmc);
											}
										}else{		
											PmsAvailableRooms bookingRoomCount=bookingController.findCount(availDate, accommodation.getAccommodationId());
											int intRooms=0;
											Integer totavailable=0,availableRooms=0;
											if(accommodation!=null){
												totavailable=accommodation.getNoOfUnits();
											}
											if(bookingRoomCount.getRoomCount()!=null){
												long lngRooms=bookingRoomCount.getRoomCount();
												long num = roomCount1.getRoomCount();	
												intRooms=(int)lngRooms;
												String num1 = inventoryCount.getInventoryCount();
							 					long num2 = Long.parseLong(num1);
							 					availableRooms=totavailable-intRooms;
							 					/*if(num2>availableRooms){
							 						this.roomCnt = availableRooms;	 
							 					}else{
							 						this.roomCnt = num2-num;
							 					}*/
							 					this.roomCnt = num2-num;
							 					long lngSold=bookingRoomCount.getRoomCount();
							   					sold=(int)lngSold;
											}else{
												long num = roomCount1.getRoomCount();	
												String num1 = inventoryCount.getInventoryCount();
												long num2 = Long.parseLong(num1);
							  					this.roomCnt = (num2-num);
							  					long lngSold=roomCount1.getRoomCount();
							   					sold=(int)lngSold;
											}
						
										}
														
									}else{
										  PmsAvailableRooms inventoryCounts = bookingController.findCount(availDate, accommodation.getAccommodationId());							
											if(inventoryCounts.getRoomCount() == null){								
												this.roomCnt = (accommodation.getNoOfUnits()-rmc);
												sold=(int)rmc;
											}
											else{
												this.roomCnt = (accommodation.getNoOfUnits()-inventoryCounts.getRoomCount());	
												long lngSold=inventoryCounts.getRoomCount();
					   							sold=(int)lngSold;
											}
									}
														
									double totalRooms=0,intSold=0;
									totalRooms=(double)accommodation.getNoOfUnits();
									intSold=(double)sold;
									double occupancy=0.0;
									occupancy=intSold/totalRooms;
									Integer occupancyRating=(int) Math.round(occupancy * 100);
									boolean isWeekdays=false,isWeekend=false;
									for(int a=0;a<weekdays.length;a++){
										if(days.equalsIgnoreCase(weekdays[a])){
											isWeekdays=true;
										}
									}
									
									for(int b=0;b<weekend.length;b++){
										if(days.equalsIgnoreCase(weekend[b])){
											isWeekend=true;
										}
									}
									if(property.getLocationType()!=null){
										if(property.getLocationType().getLocationTypeName().equalsIgnoreCase("Leisure")){
											if(isWeekdays){
												if(accommodation.getWeekdaySellingRate()==null){
													maxAmount=0;
												}else{
													maxAmount=accommodation.getWeekdaySellingRate();	
												}
													
											}
											
											if(isWeekend){
												if(accommodation.getWeekendSellingRate()==null){
													maxAmount=0;
												}else{
													maxAmount=accommodation.getWeekendSellingRate();	
												}
												
											}	
										}
										
										if(property.getLocationType().getLocationTypeName().equalsIgnoreCase("Metro")){
											if(accommodation.getSellingRate()==null){
												maxAmount=0;
											}else{
												maxAmount=accommodation.getSellingRate();	
											}
												
										}
										
										if(occupancyRating>=0 && occupancyRating<=30){
											sellrate=maxAmount;
										}else if(occupancyRating>30 && occupancyRating<=60){
											sellrate=maxAmount+(maxAmount*10/100);
										}else if(occupancyRating>=60 && occupancyRating<=80){
											sellrate=maxAmount+(maxAmount*15/100);
										}else if(occupancyRating>80){
											sellrate=maxAmount+(maxAmount*20/100);
										}
									}
				    				
									
										
										if(discountType.equals("INR")){
											firstPromoAmount=dblDiscountINR;
											firstInr+=dblDiscountINR;
										}else if(discountType.equals("PERCENT")){
											firstPromoAmount=(sellrate*dblDiscountPercent/100);
										}else{
											firstPromoAmount=0;
										}
										
																				
										if(checkStartDate.equals(strCheckInDate)){ 
											if(occupancyRating>=0 && occupancyRating<=30){
												if(this.roomCnt>0){
													isLast=true;
												}else{
													isLast=false;
												}
											}
										}
										
					    				if(isLast){
					    					if(occupancyRating>=0 && occupancyRating<=30){
					    						if(AMPM==0){//If the current time is AM
					    		            		if(bookedHours>=0 && bookedHours<=11){
					    		            			if(maxAmount<1000){
					    									if(occupancyRating>=0 && occupancyRating<=30){
					    										secondPromoAmount=commissionrange1;
					    										secondInr+=commissionrange1;
					    									}
					    							    }else if(maxAmount>=1000 && maxAmount<=1500){
					    							    	if(occupancyRating>=0 && occupancyRating<=30){
					    							    		secondPromoAmount=commissionrange2;
					    							    		secondInr+=commissionrange2;
					    									}
					    							    }else if(maxAmount>1500){
					    							    	if(occupancyRating>=0 && occupancyRating<=30){
					    							    		secondPromoAmount=commissionrange3;
					    							    		secondInr+=commissionrange3;
					    									}
					    							    }
					    		            		}
					    		            	}
					    						if(AMPM==1){
					    							if(bookedHours>=0 && bookedHours<=3){
					    								if(occupancyRating>=0 && occupancyRating<=30){
					    									secondPromoAmount=(sellrate*lastdiscountpercent1/100);	
					    								}
					    		            		}else if(bookedHours>=4 && bookedHours<=7){
					    		            			if(occupancyRating>=0 && occupancyRating<=30){
					    		            				secondPromoAmount=(sellrate*lastdiscountpercent2/100);	
					    		            			}
					    		            		}else if(bookedHours>=8 && bookedHours<=11){
					    		            			if(occupancyRating>=0 && occupancyRating<=30){
					    		            				secondPromoAmount=(sellrate*lastdiscountpercent3/100);	
					    		            			}
					    		            		}
					    						}
					    					}
					    				}
					    				promoAmount=firstPromoAmount+secondPromoAmount;
					    				totalMinimumAmount+=sellrate-promoAmount;
					  				    totalMaximumAmount+=sellrate;
								    	 dateCount++;
								    	extraAdultAmount+=accommodation.getExtraAdult(); 
								    	extraChildAmount+=accommodation.getExtraChild();
				  				     }
				  				     
				  				    
				  				    diffAmount=totalMaximumAmount-totalMinimumAmount;
				  				   if(isFlat){
										if(discountType.equalsIgnoreCase("INR")){
											promotionFirstName=String.valueOf(Math.round(firstInr))+"";
											promotionFirstType="inr";
										}else if(discountType.equalsIgnoreCase("PERCENT")){
											promotionFirstName=String.valueOf(Math.round(dblDiscountPercent))+"";
											promotionFirstType="percent";
										}
									}
									if(isEarly){
										if(discountType.equalsIgnoreCase("INR")){
											promotionFirstName=String.valueOf(Math.round(firstInr))+"";
											promotionFirstType="inr";
										}else if(discountType.equalsIgnoreCase("PERCENT")){
											promotionFirstName=String.valueOf(Math.round(dblDiscountPercent))+"";
											promotionFirstType="percent";
										}
									}
									if(isLast){

										if(AMPM==0){//If the current time is AM
			    		            		if(bookedHours>=0 && bookedHours<=11){
			    		            
			    		            			String displayDate = new SimpleDateFormat("MMM dd,yyyy HH:mm:ss").format(lastFirst.getTime());
			    								this.displayHoursForTimer=displayDate;
			    								setDisplayHoursForTimer(this.displayHoursForTimer);
			    								promotionSecondType="inr";
			    								promotionSecondName=String.valueOf(Math.round(secondInr))+"";
			    		            		}
			    		            	}
			    						if(AMPM==1){
			    							if(bookedHours>=0 && bookedHours<=3){
			    								String firstTime=checkStartDate+" 16:00:00"; 
			    						   	    java.util.Date dteFirstDate = sdf.parse(strStartTime);
			    						   	    Calendar calFirst=Calendar.getInstance();
			    						   	    calFirst.setTime(dteFirstDate);
			    						   	    calFirst.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
			    						   	    String firstDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calFirst.getTime());
			    						    	java.util.Date dteFirst = sdf.parse(firstTime);
			    								String displayDate = new SimpleDateFormat("MMM dd,yyyy HH:mm:ss").format(dteFirst.getTime());
			    								this.displayHoursForTimer=displayDate;
			    								setDisplayHoursForTimer(this.displayHoursForTimer);
			    								promotionSecondType="percent";
			    								promotionSecondName=String.valueOf(Math.round(lastdiscountpercent1))+"";
			    		            		}else if(bookedHours>=4 && bookedHours<=7){
			    		            			String firstTime=checkStartDate+" 20:00:00"; 
			    						   	    java.util.Date dteFirstDate = sdf.parse(strStartTime);
			    						   	    Calendar calFirst=Calendar.getInstance();
			    						   	    calFirst.setTime(dteFirstDate);
			    						   	    calFirst.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
			    						   	    String firstDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calFirst.getTime());
			    						    	java.util.Date dteFirst = sdf.parse(firstTime);
			    								String displayDate = new SimpleDateFormat("MMM dd,yyyy HH:mm:ss").format(dteFirst.getTime());
			    								this.displayHoursForTimer=displayDate;
			    								setDisplayHoursForTimer(this.displayHoursForTimer);
			    								promotionSecondType="percent";
			    								promotionSecondName=String.valueOf(Math.round(lastdiscountpercent2))+"";
			    		            		}else if(bookedHours>=8 && bookedHours<=11){
			    		            			String firstTime=checkStartDate+" 23:00:00"; 
			    						   	    java.util.Date dteFirstDate = sdf.parse(strStartTime);
			    						   	    Calendar calFirst=Calendar.getInstance();
			    						   	    calFirst.setTime(dteFirstDate);
			    						   	    calFirst.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
			    						   	    String firstDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calFirst.getTime());
			    						    	java.util.Date dteFirst = sdf.parse(firstTime);
			    								String displayDate = new SimpleDateFormat("MMM dd,yyyy HH:mm:ss").format(dteFirst.getTime());
			    								this.displayHoursForTimer=displayDate;
			    								setDisplayHoursForTimer(this.displayHoursForTimer);
			    								promotionSecondType="percent";
			    								promotionSecondName=String.valueOf(Math.round(lastdiscountpercent3))+"";
			    		            		}
			    						}

									
									}
																	
									PropertyTaxeManager propertytaxController = new PropertyTaxeManager();
			  	  					 double taxe =  totalMinimumAmount/diffInDays;
			  	  					
			  						 PropertyTaxe tax = propertytaxController.find(taxe);
			  						 double taxes = Math.round(totalMinimumAmount * tax.getTaxPercentage() / 100  ) ;
			  	 			         
			  						if (!jsonOutput.equalsIgnoreCase(""))
						  				jsonOutput += ",{";
						  			else
						  				jsonOutput += "{";
						  			jsonOutput += "\"propertyId\":\"" + property.getPropertyId()+ "\"";
						  			SimpleDateFormat sdfformat=new SimpleDateFormat("yyyy-MM-dd");
					 				String strArrivalDate=sdfformat.format(getArrivalDate());
					 				String strDepartureDate=sdfformat.format(getDepartureDate());
					 				
					 				jsonOutput += ",\"arrivalDate\":\"" + strArrivalDate+ "\"";
					 				jsonOutput += ",\"departureDate\":\"" + strDepartureDate+ "\"";			  					
				  					long minimum = getAvailableCount(property.getPropertyId(),getArrivalDate(),getDepartureDate(),roomCount,accommodation.getAccommodationId());
				  					jsonOutput += ",\"available\":\"" + minimum+ "\""; 
			  				    	jsonOutput += ",\"accommodationType\":\"" + accommodation.getAccommodationType().toUpperCase()+ "\"";
			  				    	jsonOutput += ",\"accommodationId\":\"" + accommodation.getAccommodationId()+ "\"";
			  				    	jsonOutput += ",\"minimumAmount\":\"" + (totalMinimumAmount==0 ? 0 : Math.round(totalMinimumAmount) )+  "\"";
			  				    	jsonOutput += ",\"maximumAmount\":\"" + (totalMaximumAmount==0 ? 0 : Math.round(totalMaximumAmount) )+  "\"";
			  				    	jsonOutput += ",\"baseActualAmount\":\"" + + (totalMaximumAmount==0 ? totalMaximumAmount : totalMaximumAmount )+  "\"";
			  			    		 jsonOutput += ",\"baseAmount\":\"" + + (totalMaximumAmount==0 ? totalMaximumAmount : totalMaximumAmount)+  "\"";
			  			    		 jsonOutput += ",\"totalAmount\":\"" + + (totalMaximumAmount==0 ? totalMaximumAmount : totalMaximumAmount)+  "\"";
			  				    	jsonOutput += ",\"rooms\":\"" + 0 + "\"";
			  				    	jsonOutput += ",\"difference\":\"" + (diffAmount==0 ? 0  : Math.round(diffAmount) )+  "\"";
			  		 				jsonOutput += ",\"minOccupancy\":\"" + accommodation.getMinOccupancy() + "\"";
			  		 				jsonOutput += ",\"maxOccupancy\":\"" + accommodation.getMaxOccupancy() + "\"";
			  		 				jsonOutput += ",\"noOfAdults\":\"" + accommodation.getNoOfAdults() + "\"";
			  		 				jsonOutput += ",\"noOfChild\":\"" + accommodation.getNoOfChild() + "\"";
			  		 				jsonOutput += ",\"noOfInfant\":\"" + accommodation.getNoOfInfant() + "\"";
			  		 				jsonOutput += ",\"extraChild\":\"" + (extraChildAmount==0 ? 0 : Math.round(extraChildAmount)) + "\"";
			  		 				jsonOutput += ",\"extraAdult\":\"" + (extraAdultAmount==0 ? 0 : Math.round(extraAdultAmount))+ "\"";
				  			    	jsonOutput += "}";
				  				
			  					}
			  				}
			  			}
			    	}
			    	
			    }

				response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
		   }catch(Exception ex){
			   logger.error(ex);
			   ex.printStackTrace();
		   }
		   return output;
	   
	   
		
	
		
	}
	public String getOldRoomAvailability() throws IOException{

 		
	 	   
	 	   try {
	 	    /*this.arrivalDate = getArrivalDate();
	 	    sessionMap.put("arrivalDate",arrivalDate); 
	 			
	 		this.departureDate = getDepartureDate();
	 		sessionMap.put("departureDate",departureDate); */
	 	      
	 		//String numberAsString = String.valueOf(getAccommodationId());
	 		String[] parts = getAccommodationTypeId().split(",");
	 		 int[] intArray = new int[parts.length];
	 		   for (int i = 0; i < parts.length; i++) {
	 		         String numberAsString = parts[i];
	 		         intArray[i] = Integer.parseInt(numberAsString);
	 		      }
	 		this.sourceTypeId=1;
	 			
	 	    Timestamp timestamp = new Timestamp(System.currentTimeMillis());
	 	    DateFormat format = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);   // change to siva //"MMMM d, yyyy", Locale.ENGLISH   // "yyyy-MM-dd HH:mm:ss"
	 	    java.util.Date dateStart = format.parse(getStrArrivalDate());
	 	    java.util.Date dateEnd = format.parse(getStrDepartureDate());
	 	   
	 	    
	 	    Calendar calCheckStart=Calendar.getInstance();
	 	    calCheckStart.setTime(dateStart);
	 	    java.util.Date checkInDate = calCheckStart.getTime();
	 	    this.arrivalDate=new Timestamp(checkInDate.getTime());
	 	    sessionMap.put("startDate",arrivalDate); 
	 	    
	 	    Calendar calCheckEnd=Calendar.getInstance();
	 	    calCheckEnd.setTime(dateEnd);
	 	    java.util.Date checkOutDate = calCheckEnd.getTime();
	 	    this.departureDate=new Timestamp(checkOutDate.getTime());
	 	    sessionMap.put("endDate",departureDate); 
	 	    
	 		
	 		Calendar cal = Calendar.getInstance();
	 		cal.setTime(this.departureDate);
	 	    cal.add(Calendar.DATE, -1);
	 		SimpleDateFormat format1=new SimpleDateFormat("yyyy-MM-dd");
	 		String startDate = new SimpleDateFormat("yyyy-MM-dd").format(this.arrivalDate);
	 		String endDate = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
	 		DateTime start = DateTime.parse(startDate);
	 		DateTime end = DateTime.parse(endDate);
	 		java.util.Date arrival = start.toDate();
	 		java.util.Date departure = end.toDate();
	 		
	 		
	 		this.propertyId = (Integer) sessionMap.get("propertyId");

	 		String jsonOutput = "",jsonPrices="";
	 		HttpServletResponse response = ServletActionContext.getResponse();
	 		
	 		List<DateTime> betweenAmount = DateUtil.getDateRange(start, end);
	 		PropertyAccommodationInventoryManager inventoryController=new PropertyAccommodationInventoryManager();
	 		PropertyRatePlanDetailManager planDetailController=new PropertyRatePlanDetailManager();
	 		PropertyRateManager rateController=new PropertyRateManager();
	 		PropertyRateDetailManager detailController=new PropertyRateDetailManager();
	 		
	 			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
	 			PropertyAccommodationInventoryManager accommodationInventoryController = new PropertyAccommodationInventoryManager();  
	 			PmsBookingManager bookingController = new PmsBookingManager();
	 			PropertyTaxeManager taxController = new PropertyTaxeManager();
	 			DateFormat f = new SimpleDateFormat("EEEE");
	 			response.setContentType("application/json");
	 			double baseAmount=0.0,extraAdultAmount=0.0,extraChildAmount=0.0,totalBaseAmount=0.0,actualBaseAmount=0.0,
			    		totalMinimumAmount=0,totalMaximumAmount=0,totalSellRate=0,actualAdultAmount=0.0,actualChildAmount=0.0,
			    				actualTotalAdultAmount=0.0,actualTotalChildAmount=0.0;
	 			
	 			for (int accomId : intArray)
	 			{ 
	 				this.availableList = bookingController.list(getPropertyId(),getArrivalDate(),getDepartureDate(),accomId);
	 				for (PmsAvailableRooms available : availableList) {
	 					int countDate=0;
	 					ArrayList arlRateType=new ArrayList();
						 List<PropertyRatePlanDetail> listPlanDetails=planDetailController.listAccommodation(getPropertyId(), available.getAccommodationId());
						 if(listPlanDetails.size()>0 && !listPlanDetails.isEmpty()){
				        	for(PropertyRatePlanDetail ratePlanDetails:listPlanDetails){
				        		arlRateType.add(ratePlanDetails.getRatePlan().getRatePlanType());
				        		arlRateType.add(ratePlanDetails.getRatePlan().getRatePlanSymbolType());
				        		arlRateType.add(ratePlanDetails.getTariffAmount());
				        	}
						 }
						 
						 if (!jsonOutput.equalsIgnoreCase(""))
			 					jsonOutput += ",{";
			 				
			 				else
			 					jsonOutput += "{";
			 				
			 				
			 				//jsonOutput += "\"availableId\":\"" + availableId+ "\"";
			 				jsonOutput += "\"propertyId\":\"" + getPropertyId() + "\"";
			 				SimpleDateFormat sdfformat=new SimpleDateFormat("yyyy-MM-dd");
			 				String strArrivalDate=sdfformat.format(getArrivalDate());
			 				String strDepartureDate=sdfformat.format(getDepartureDate());
			 				
			 				jsonOutput += ",\"arrivalDate\":\"" + strArrivalDate+ "\"";
			 				jsonOutput += ",\"departureDate\":\"" + strDepartureDate+ "\"";
			 				
	 					for (DateTime d : betweenAmount){

	 						
	 						 
					    	 long rmc = 0;
				   			 this.roomCnt = rmc;
				        	 int sold=0;
				        	 java.util.Date availDate = d.toDate();
							 PropertyAccommodationInventory inventoryCount = inventoryController.findInventoryCount(availDate,available.getAccommodationId());
							 if(inventoryCount != null){
							 PmsAvailableRooms roomCount1 = bookingController.findBookingCount(availDate, available.getAccommodationId(),inventoryCount.getInventoryId());
							 if(roomCount1.getRoomCount() == null) {
							 	String num1 = inventoryCount.getInventoryCount();
								PmsAvailableRooms bookingRoomCount=bookingController.findCount(availDate, available.getAccommodationId());
								Integer totavailable=0;
								if(available!=null){
									totavailable=available.getNoOfUnits();
								}
								if(bookingRoomCount.getRoomCount()!=null){
									long lngRooms=bookingRoomCount.getRoomCount();
									long num2 = Long.parseLong(num1);
									this.roomCnt = num2;
									sold=(int) (lngRooms);
									int availRooms=0;
									availRooms=totavailable-sold;
//									if(Integer.parseInt(num1)>availRooms){
//										this.roomCnt = availRooms;	
//									}else{
										this.roomCnt = num2;
//									}
											
								}else{
									 long num2 = Long.parseLong(num1);
									 this.roomCnt = num2;
									 sold=(int) (rmc);
								}
							}else{		
								PmsAvailableRooms bookingRoomCount=bookingController.findCount(availDate, available.getAccommodationId());
								int intRooms=0;
								Integer totavailable=0,availableRooms=0;
								if(available!=null){
									totavailable=available.getNoOfUnits();
								}
								if(bookingRoomCount.getRoomCount()!=null){
									long lngRooms=bookingRoomCount.getRoomCount();
									long num = roomCount1.getRoomCount();	
									intRooms=(int)lngRooms;
									String num1 = inventoryCount.getInventoryCount();
				 					long num2 = Long.parseLong(num1);
				 					availableRooms=totavailable-intRooms;
//				 					if(num2>availableRooms){
//				 						this.roomCnt = availableRooms;	 
//				 					}else{
				 						this.roomCnt = num2-num;
//				 					}
				 					long lngSold=bookingRoomCount.getRoomCount();
				   					sold=(int)lngSold;
								}else{
									long num = roomCount1.getRoomCount();	
									String num1 = inventoryCount.getInventoryCount();
									long num2 = Long.parseLong(num1);
				  					this.roomCnt = (num2-num);
				  					long lngSold=roomCount1.getRoomCount();
				   					sold=(int)lngSold;
								}
			
							}
											
						}else{
							  PmsAvailableRooms inventoryCounts = bookingController.findCount(availDate, available.getAccommodationId());							
								if(inventoryCounts.getRoomCount() == null){								
									this.roomCnt = (available.getNoOfUnits()-rmc);
									sold=(int)rmc;
								}
								else{
									this.roomCnt = (available.getNoOfUnits()-inventoryCounts.getRoomCount());	
									long lngSold=inventoryCounts.getRoomCount();
		   							sold=(int)lngSold;
								}
							}
												
							double totalRooms=0,intSold=0;
							totalRooms=(double)available.getNoOfUnits();
							intSold=(double)sold;
							double occupancy=0.0;
							occupancy=intSold/totalRooms;
							double sellrate=0.0,otarate=0.0;
							Integer occupancyRating=(int) Math.round(occupancy * 100);
						    int rateCount=0,rateIdCount=0;
						    double minAmount=0,maxAmount=0;
						    List<PropertyRate> dateList =  rateController.listAllDates(getPropertyId(),available.getAccommodationId(),sourceTypeId,d.toDate());
							if(!dateList.isEmpty()){
								for(PropertyRate rates : dateList) {
						    		int propertyRateId = rates.getPropertyRateId();
					    			List<PropertyRateDetail> rateDetailList =   detailController.listMinMaxRateDetail(propertyRateId,f.format(d.toDate()).toLowerCase());
		   				    		 if(rateDetailList.size()>0 && !rateDetailList.isEmpty()){
		   				    			 if(rateCount<1){
		   				    				 for (PropertyRateDetail rate : rateDetailList){
		   				    					 minAmount=rate.getMinimumBaseAmount();
		   				    					 maxAmount=rate.getMaximumBaseAmount();
		   				    					 extraAdultAmount=rate.getExtraAdult();
		   				    					 extraChildAmount=rate.getExtraChild();
		   				    				    if(occupancyRating==0){
			   				 						sellrate = minAmount;
			   				 					}else if(occupancyRating>0 && occupancyRating<=30){
			   				 			        	sellrate = minAmount;
			   				 			        }else if(occupancyRating>=31 && occupancyRating<=60){
			   				 			        	sellrate = minAmount+(minAmount*10/100);
			   				 			        }else if(occupancyRating>=61 && occupancyRating<=80){
			   				 			        	sellrate = minAmount+(minAmount*15/100);
			   				 			        }else if(occupancyRating>=81 && occupancyRating<=100){
			   				 			        	sellrate = maxAmount;
			   				 			        }
		   				    				    baseAmount+=  sellrate;
		       				    			 }
		   				    				 rateCount++;
		   				    				rateIdCount++;
		   				    			 }
		   				    		 }
						    	}
								if(rateIdCount<1){
   				    				PropertyAccommodation accommodations=accommodationController.find(accomId); 
   				    				if(accommodations.getMinimumBaseAmount()!=null){
   				    					minAmount=accommodations.getMinimumBaseAmount();	 
   				    				 }
   				    				 if(accommodations.getMaximumBaseAmount()!=null){
   				    					maxAmount=accommodations.getMaximumBaseAmount();	 
   				    				 }
   				    				 extraAdultAmount=accommodations.getExtraAdult();
				    					 extraChildAmount=accommodations.getExtraChild();
   				    				if(occupancyRating==0){
   				 						sellrate = minAmount;
   				 					}else if(occupancyRating>0 && occupancyRating<=30){
   				 			        	sellrate = minAmount;
   				 			        }else if(occupancyRating>=31 && occupancyRating<=60){
   				 			        	sellrate = minAmount+(minAmount*10/100);
   				 			        }else if(occupancyRating>=61 && occupancyRating<=80){
   				 			        	sellrate = minAmount+(minAmount*15/100);
   				 			        }else if(occupancyRating>=81 && occupancyRating<=100){
   				 			        	sellrate = maxAmount;
   				 			        }
   				    				baseAmount+= sellrate;
   				    				rateIdCount++;
   				    				rateCount++;
   				    			 }
						     }else{
						    	 PropertyAccommodation accommodations=accommodationController.find(accomId);
						    	 if(accommodations.getMinimumBaseAmount()!=null){
			    					minAmount=accommodations.getMinimumBaseAmount();	 
			    				 }
			    				 if(accommodations.getMaximumBaseAmount()!=null){
			    					maxAmount=accommodations.getMaximumBaseAmount();	 
			    				 }
			    				 extraAdultAmount=accommodations.getExtraAdult();
			    				 extraChildAmount=accommodations.getExtraChild();
			    				 
			    				if(occupancyRating==0){
			 						sellrate = minAmount;
			 					}else if(occupancyRating>0 && occupancyRating<=30){
			 			        	sellrate = minAmount;
			 			        }else if(occupancyRating>=31 && occupancyRating<=60){
			 			        	sellrate = minAmount+(minAmount*10/100);
			 			        }else if(occupancyRating>=61 && occupancyRating<=80){
			 			        	sellrate = minAmount+(minAmount*15/100);
			 			        }else if(occupancyRating>=81 && occupancyRating<=100){
			 			        	sellrate = maxAmount;
			 			        }
			    				baseAmount+= sellrate;
			    			 
						     }
							totalMinimumAmount+=minAmount;
							totalMaximumAmount+=maxAmount;
							totalSellRate+=sellrate;
							if (!jsonPrices.equalsIgnoreCase(""))
								jsonPrices += ",{";
							else
								jsonPrices += "{";
							
							
							jsonPrices += "\"inventoryRating\":\"" + (occupancyRating)+ "\"";
							jsonPrices += ",\"minimumAmount\":\"" + (minAmount)+ "\"";
							jsonPrices += ",\"maximumAmount\":\"" + + (maxAmount)+  "\"";
							jsonPrices += ",\"sellrate\":\"" + + (sellrate)+  "\"";	
							jsonPrices += ",\"extraAdult\":\"" + (extraAdultAmount)+ "\"";
							jsonPrices += ",\"extraChild\":\"" + (extraChildAmount)+  "\"";
							jsonPrices += ",\"date\":\"" +  (format1.format(availDate))+  "\"";
						
							jsonPrices += "}";
							
					    	countDate++;
					    }	
	 				
	 					jsonOutput += ",\"rates\":[" + jsonPrices+ "]";
						
						double variationAmount=0,variationRating=0;
						variationAmount=totalMaximumAmount-totalSellRate;
						variationRating=variationAmount/totalMaximumAmount*100;
						String promotionFirstName=null;
						if(variationRating>0){
							promotionFirstName=String.valueOf(Math.round(variationRating))+" % OFF";	
						}
						/*if(variationRating>9){
							promotionFirstName=String.valueOf(Math.round(variationRating))+" % OFF";	
						}else{
							promotionFirstName="Flat "+String.valueOf(Math.round(variationAmount))+" OFF";
						}*/
						
						double dblRatePlanAmount=0,dblEPAmount=0,dblTotalEPAmount=0;
				        Iterator iterRatePlan=arlRateType.iterator();
				        String strType=null,strSymbol=null;
		            	while(iterRatePlan.hasNext()){
		            		strType=(String)iterRatePlan.next();
		            		strSymbol=(String)iterRatePlan.next();
		            		dblRatePlanAmount=(Double)iterRatePlan.next();
		            		
		            		if(strType.equalsIgnoreCase("EP")){
		                		if(strSymbol.equalsIgnoreCase("plus")){
		                			dblEPAmount=baseAmount+dblRatePlanAmount;	
		                		}else{
		                			dblEPAmount=baseAmount-dblRatePlanAmount;
		                		}
		            		}
		            		
		            	}
						totalAmount=baseAmount;
						dblTotalEPAmount=dblEPAmount;
						if(dblTotalEPAmount==0){
							dblTotalEPAmount=baseAmount;
						}
						/*if(countDate>1){
							actualAdultAmount=extraAdultAmount/countDate;
							actualChildAmount=extraChildAmount/countDate;
						}else{
							actualAdultAmount=extraAdultAmount;
							actualChildAmount=extraChildAmount;
						}*/
					
						actualAdultAmount=extraAdultAmount;
						actualChildAmount=extraChildAmount;
						
						actualBaseAmount=Math.round(totalAmount);
						actualTotalAdultAmount=Math.round(actualAdultAmount);
						actualTotalChildAmount=Math.round(actualChildAmount);
						
						if(totalMinimumAmount==0){
							totalBaseAmount=0;
						}
						if(totalMinimumAmount>0){
							totalBaseAmount=totalMinimumAmount;
						}
						
					jsonOutput += ",\"minimumAmount\":\"" + + (totalMinimumAmount==0 ? totalBaseAmount : totalMinimumAmount )+  "\"";
					jsonOutput += ",\"maximumAmount\":\"" + + (totalMaximumAmount==0 ? totalBaseAmount : totalMaximumAmount )+  "\"";
	 				jsonOutput += ",\"baseActualAmount\":\"" + + (totalMaximumAmount==0 ? totalBaseAmount : totalMaximumAmount )+  "\"";
		    		 jsonOutput += ",\"baseAmount\":\"" + + (actualBaseAmount==0 ? totalBaseAmount : actualBaseAmount )+  "\"";
		    		 jsonOutput += ",\"totalAmount\":\"" + + (actualBaseAmount==0 ? totalBaseAmount : actualBaseAmount)+  "\"";
		    		 jsonOutput += ",\"cpAmount\":\"" + + (actualBaseAmount==0 ? totalBaseAmount : actualBaseAmount )+  "\"";
		    		 jsonOutput += ",\"epAmount\":\"" + + (dblTotalEPAmount)+  "\"";
		    		 jsonOutput += ",\"promotionFirstName\":\"" + (promotionFirstName==null? "None":promotionFirstName)+ "\"";
	 				jsonOutput += ",\"rooms\":\"" + 0 + "\"";
	 				jsonOutput += ",\"accommodationId\":\"" + available.getAccommodationId() + "\"";
	 				jsonOutput += ",\"accommodationType\":\"" + available.getAccommodationType().toUpperCase() + "\"";
	 				jsonOutput += ",\"minOccupancy\":\"" + available.getMinOccupancy() + "\"";
	 				jsonOutput += ",\"maxOccupancy\":\"" + available.getMaxOccupancy() + "\"";
	 				jsonOutput += ",\"noOfAdults\":\"" + available.getNoOfAdults() + "\"";
	 				jsonOutput += ",\"noOfChild\":\"" + available.getNoOfChild() + "\"";
	 				jsonOutput += ",\"noOfInfant\":\"" + available.getNoOfInfant() + "\"";
	 				jsonOutput += ",\"extraChild\":\"" + available.getExtraChild() + "\"";
	 				jsonOutput += ",\"extraAdult\":\"" + available.getExtraAdult() + "\"";

	 				long rmc = 0;
	 				this.roomCnt = rmc;
	 				
	 				
	 				List<DateTime> between = DateUtil.getDateRange(start, end);
	 				List<Long> myList = new ArrayList<Long>();
	 				for (DateTime d : between)
	 				  {
	 					java.util.Date availDate = d.toDate();
	 					PropertyAccommodationInventory inventoryCount = accommodationInventoryController.findInventoryCount(availDate,available.getAccommodationId());
	 					if(inventoryCount != null){
	 						//jsonOutput += ",\"inventoryId\":\"" + inventoryCount.getInventoryId() + "\"";
	 						PmsAvailableRooms roomCount1 = bookingController.findBookingCount(availDate, available.getAccommodationId(),inventoryCount.getInventoryId());
	 						if(roomCount1.getRoomCount() == null) {
	 							
	 							String num1 = inventoryCount.getInventoryCount();
	 							long num2 = Long.parseLong(num1);
	 							PmsAvailableRooms bookingRoomCount=bookingController.findCount(availDate, available.getAccommodationId());
	 							int sold=0;
	 							PropertyAccommodation accommodations=accommodationController.find(available.getAccommodationId());
	 							Integer totavailable=0;
	   							if(accommodations!=null){
	   								totavailable=accommodations.getNoOfUnits();
	   							}
	 							if(bookingRoomCount.getRoomCount()!=null){
	 								long lngRooms=bookingRoomCount.getRoomCount();
	 								sold=(int) (lngRooms);
	 								int availRooms=0;
	 								availRooms=totavailable-sold;
	 								
//	 								if(Integer.parseInt(num1)>availRooms){
//	 									this.roomCnt = availRooms;	
//	 								}else{
	 									this.roomCnt = num2;
//	 								}
	 							}else{
	 								this.roomCnt = num2; 
	 							}
	 							
	 							 /*String num1 = inventoryCount.getInventoryCount();
	 							 long num2 = Long.parseLong(num1);
	 							this.roomCnt = num2;*/ 
	 						}
	 						else {
	 							PmsAvailableRooms bookingRoomCount=bookingController.findCount(availDate,available.getAccommodationId());
	 							int intRooms=0;
	 							PropertyAccommodation accommodations=accommodationController.find(available.getAccommodationId());
	   							Integer totavailable=0;
	   							 if(accommodations!=null){
	   								totavailable=accommodations.getNoOfUnits();
	   							 }
	 							if(bookingRoomCount.getRoomCount()!=null){
	 								long lngRooms=bookingRoomCount.getRoomCount();
	 								long num = roomCount1.getRoomCount();	
	 								intRooms=(int)lngRooms;
	 								String num1 = inventoryCount.getInventoryCount();
	 	 							long num2 = Long.parseLong(num1);
	 	 							Integer totRooms=0;
	 	 							totRooms=totavailable-intRooms;
//	 	 							if(num2>totRooms){
//	 	 								this.roomCnt = totRooms;	 
//	 	 							}else{
	 	 								this.roomCnt = num2-num;
//	 	 							}							
	 							}else{
	 								long num = roomCount1.getRoomCount();
	 								String num1 = inventoryCount.getInventoryCount();
	 								long num2 = Long.parseLong(num1);
	 								this.roomCnt = (num2-num);	
	 							}
	 							/* long num = roomCount1.getRoomCount();
	 							 String num1 = inventoryCount.getInventoryCount();
	 							 long num2 = Long.parseLong(num1);
	 							this.roomCnt = (num2-num);*/	
	 						}
	 							
	 					}
	 					else{
	 						//jsonOutput += ",\"inventoryId\":\"" + 0 + "\""; 
	 						  PmsAvailableRooms roomCount = bookingController.findCount(availDate, available.getAccommodationId());							
	 							if(roomCount.getRoomCount() == null){								
	 								this.roomCnt = (available.getRoomAvailable()-rmc);		
	 							}
	 							else{
	 								
	 								this.roomCnt = (available.getRoomAvailable()-roomCount.getRoomCount());						
	 							}
	 					}
	 							
	 							myList.add(this.roomCnt);
	 							
	 						
	 			        //long availableCount = Collections.min(myList);
	 			        

	 			        //return availableCount;
	 			     }
	 				/*PmsAvailableRooms roomCount = bookingController.findCount(arrival, departure,available.getAccommodationId());
	 				*/
	 				
	 				
	 				//jsonOutput += ",\"available\":\"" + available.getAvailable() + "\"";
	 				jsonOutput += ",\"available\":\"" + Collections.min(myList) + "\"";
	 				
	 				
	 				jsonOutput += "}";
	 				
	 				
	 				
	          }
	 				

	 			}
	          
	 			

	 			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");

	 		} catch (Exception e) {
	 			logger.error(e);
	 			e.printStackTrace();
	 		} finally {

	 		}

	 		return null;

	 	
	}
	
	@SuppressWarnings("unused")
	 public String getRoomAvailability() throws IOException {
	 		
	 	   
	 	   try {
	 	    /*this.arrivalDate = getArrivalDate();
	 	    sessionMap.put("arrivalDate",arrivalDate); 
	 			
	 		this.departureDate = getDepartureDate();
	 		sessionMap.put("departureDate",departureDate); */
	 	      
	 		//String numberAsString = String.valueOf(getAccommodationId());
	 		String[] parts = getAccommodationTypeId().split(",");
	 		 int[] intArray = new int[parts.length];
	 		   for (int i = 0; i < parts.length; i++) {
	 		         String numberAsString = parts[i];
	 		         intArray[i] = Integer.parseInt(numberAsString);
	 		      }
	 		
	 			
	 	    Timestamp timestamp = new Timestamp(System.currentTimeMillis());
	 	    
	 	    DateFormat format = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
	 	    java.util.Date dateStart = format.parse(getStrArrivalDate());
	 	    java.util.Date dateEnd = format.parse(getStrDepartureDate());
	 	   
	 	    
	 	    Calendar calCheckStart=Calendar.getInstance();
	 	    calCheckStart.setTime(dateStart);
	 	    java.util.Date checkInDate = calCheckStart.getTime();
	 	    this.arrivalDate=new Timestamp(checkInDate.getTime());
	 	    sessionMap.put("startDate",arrivalDate); 
	 	    
	 	    Calendar calCheckEnd=Calendar.getInstance();
	 	    calCheckEnd.setTime(dateEnd);
	 	    java.util.Date checkOutDate = calCheckEnd.getTime();
	 	    this.departureDate=new Timestamp(checkOutDate.getTime());
	 	    sessionMap.put("endDate",departureDate); 
	 	    
	 		
	 		Calendar cal = Calendar.getInstance();
	 		cal.setTime(this.departureDate);
	 	    cal.add(Calendar.DATE, -1);
	 		
	 		String startDate = new SimpleDateFormat("yyyy-MM-dd").format(this.arrivalDate);
	 		String endDate = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
	 		DateTime start = DateTime.parse(startDate);
	 		DateTime end = DateTime.parse(endDate);
	 		java.util.Date arrival = start.toDate();
	 		java.util.Date departure = end.toDate();
	 		
	 		
	 		this.propertyId = (Integer) sessionMap.get("propertyId");

	 		String jsonOutput = "";
	 		HttpServletResponse response = ServletActionContext.getResponse();
	 		
	 			
	 			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
	 			PropertyAccommodationInventoryManager accommodationInventoryController = new PropertyAccommodationInventoryManager();  
	 			PmsBookingManager bookingController = new PmsBookingManager();
	 			PropertyTaxeManager taxController = new PropertyTaxeManager();
	 			
	 			response.setContentType("application/json");
	 			
	 			for (int accomId : intArray)
	 			{ 
	 				this.availableList = bookingController.list(getPropertyId(),getArrivalDate(),getDepartureDate(),accomId);
	 				for (PmsAvailableRooms available : availableList) {
	          
	 				
	 				if (!jsonOutput.equalsIgnoreCase(""))
	 					jsonOutput += ",{";
	 				
	 				else
	 					jsonOutput += "{";
	 				//jsonOutput += "\"availableId\":\"" + availableId+ "\"";
	 				jsonOutput += "\"propertyId\":\"" + getPropertyId() + "\"";
	 				SimpleDateFormat sdfformat=new SimpleDateFormat("yyyy-MM-dd");
	 				String strArrivalDate=sdfformat.format(getArrivalDate());
	 				String strDepartureDate=sdfformat.format(getDepartureDate());
	 				
	 				jsonOutput += ",\"arrivalDate\":\"" + strArrivalDate+ "\"";
	 				jsonOutput += ",\"departureDate\":\"" + strDepartureDate+ "\"";
	 				jsonOutput += ",\"baseAmount\":\"" + available.getBaseAmount() + "\"";
	 				
	 				jsonOutput += ",\"rooms\":\"" + 0 + "\"";
	 				//jsonOutput += ",\"roomCount\":\"" + (available.getRoomCount() == null ? available.getNoOfUnits() : available.getRoomCount()) + "\"";
	 				//jsonOutput += ",\"roomCount\":\"" + available.getRoomCount() + "\"";
	 				jsonOutput += ",\"accommodationId\":\"" + available.getAccommodationId() + "\"";
	 				jsonOutput += ",\"accommodationType\":\"" + available.getAccommodationType().toUpperCase() + "\"";
	 				jsonOutput += ",\"minOccupancy\":\"" + available.getMinOccupancy() + "\"";
	 				jsonOutput += ",\"maxOccupancy\":\"" + available.getMaxOccupancy() + "\"";
	 				jsonOutput += ",\"noOfAdults\":\"" + available.getNoOfAdults() + "\"";
	 				jsonOutput += ",\"noOfChild\":\"" + available.getNoOfChild() + "\"";
	 				jsonOutput += ",\"noOfInfant\":\"" + available.getNoOfInfant() + "\"";
	 				jsonOutput += ",\"extraChild\":\"" + available.getExtraChild() + "\"";
	 				jsonOutput += ",\"extraAdult\":\"" + available.getExtraAdult() + "\"";
	 				//jsonOutput += ",\"available\":\"" + available.getRoomAvailable() + "\"";
	 				//shiva add next 2 lines
	 				/*PropertyTaxe propertyTaxe = taxController.findAccommodationTax(available.getAccommodationId());
	 	              jsonOutput += ",\"tax\":\"" + propertyTaxe.getTaxAmount() + "\"";*/
	 				long rmc = 0;
	 				this.roomCnt = rmc;
	 				List<DateTime> between = DateUtil.getDateRange(start, end);
	 				List<Long> myList = new ArrayList<Long>();
	 				for (DateTime d : between)
	 				  {
	 					java.util.Date availDate = d.toDate();
	 					PropertyAccommodationInventory inventoryCount = accommodationInventoryController.findInventoryCount(availDate,available.getAccommodationId());
	 					if(inventoryCount != null){
	 						//jsonOutput += ",\"inventoryId\":\"" + inventoryCount.getInventoryId() + "\"";
	 						PmsAvailableRooms roomCount1 = bookingController.findBookingCount(availDate, available.getAccommodationId(),inventoryCount.getInventoryId());
	 						if(roomCount1.getRoomCount() == null) {
	 							
	 							String num1 = inventoryCount.getInventoryCount();
	 							long num2 = Long.parseLong(num1);
	 							PmsAvailableRooms bookingRoomCount=bookingController.findCount(availDate, available.getAccommodationId());
	 							int sold=0;
	 							PropertyAccommodation accommodations=accommodationController.find(available.getAccommodationId());
	 							Integer totavailable=0;
	   							if(accommodations!=null){
	   								totavailable=accommodations.getNoOfUnits();
	   							}
	 							if(bookingRoomCount.getRoomCount()!=null){
	 								long lngRooms=bookingRoomCount.getRoomCount();
	 								sold=(int) (lngRooms);
	 								int availRooms=0;
	 								availRooms=totavailable-sold;
	 								
	 								if(Integer.parseInt(num1)>availRooms){
	 									this.roomCnt = availRooms;	
	 								}else{
	 									this.roomCnt = num2;
	 								}
	 							}else{
	 								this.roomCnt = num2; 
	 							}
	 							
	 							 /*String num1 = inventoryCount.getInventoryCount();
	 							 long num2 = Long.parseLong(num1);
	 							this.roomCnt = num2;*/ 
	 						}
	 						else {
	 							PmsAvailableRooms bookingRoomCount=bookingController.findCount(availDate,available.getAccommodationId());
	 							int intRooms=0;
	 							PropertyAccommodation accommodations=accommodationController.find(available.getAccommodationId());
	   							Integer totavailable=0;
	   							 if(accommodations!=null){
	   								totavailable=accommodations.getNoOfUnits();
	   							 }
	 							if(bookingRoomCount.getRoomCount()!=null){
	 								long lngRooms=bookingRoomCount.getRoomCount();
	 								long num = roomCount1.getRoomCount();	
	 								intRooms=(int)lngRooms;
	 								String num1 = inventoryCount.getInventoryCount();
	 	 							long num2 = Long.parseLong(num1);
	 	 							Integer totRooms=0;
	 	 							totRooms=totavailable-intRooms;
	 	 							if(num2>totRooms){
	 	 								this.roomCnt = totRooms;	 
	 	 							}else{
	 	 								this.roomCnt = num2-num;
	 	 							}							
	 							}else{
	 								long num = roomCount1.getRoomCount();
	 								String num1 = inventoryCount.getInventoryCount();
	 								long num2 = Long.parseLong(num1);
	 								this.roomCnt = (num2-num);	
	 							}
	 							/* long num = roomCount1.getRoomCount();
	 							 String num1 = inventoryCount.getInventoryCount();
	 							 long num2 = Long.parseLong(num1);
	 							this.roomCnt = (num2-num);*/	
	 						}
	 							
	 					}
	 					else{
	 						//jsonOutput += ",\"inventoryId\":\"" + 0 + "\""; 
	 						  PmsAvailableRooms roomCount = bookingController.findCount(availDate, available.getAccommodationId());							
	 							if(roomCount.getRoomCount() == null){								
	 								this.roomCnt = (available.getRoomAvailable()-rmc);		
	 							}
	 							else{
	 								
	 								this.roomCnt = (available.getRoomAvailable()-roomCount.getRoomCount());						
	 							}
	 					}
	 							
	 							myList.add(this.roomCnt);
	 							
	 						
	 			        //long availableCount = Collections.min(myList);
	 			        

	 			        //return availableCount;
	 			     }
	 				/*PmsAvailableRooms roomCount = bookingController.findCount(arrival, departure,available.getAccommodationId());
	 				*/
	 				
	 				
	 				//jsonOutput += ",\"available\":\"" + available.getAvailable() + "\"";
	 				jsonOutput += ",\"available\":\"" + Collections.min(myList) + "\"";
	 				jsonOutput += "}";
	 				
	 				
	 				
	          }
	 				

	 			}
	          
	 			

	 			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
	 			

	 		} catch (Exception e) {
	 			logger.error(e);
	 			e.printStackTrace();
	 		} finally {

	 		}

	 		return null;

	 	}

   
   public String getPartnerBulkBlockInventory()
   {
	   try{

		   this.partnerPropertyId=(Integer)sessionMap.get("partnerPropertyId");
	 		this.selectPartnerPropertyId=(Integer)sessionMap.get("selectedPropertyId");
			if(this.selectPartnerPropertyId!=null){
				this.partnerPropertyId=this.selectPartnerPropertyId;	
			}else{
				this.partnerPropertyId=this.partnerPropertyId;
			}
	   			
	   	    Timestamp timestamp = new Timestamp(System.currentTimeMillis());
	   	    
	   	    DateFormat format = new SimpleDateFormat("MMM d,yyyy" , Locale.ENGLISH);
	   	    
	   	    java.util.Date dateStart = format.parse(getStartBlockDate());
	   	    java.util.Date dateEnd = format.parse(getEndBlockDate());
	   	   
	   	    
	   	    Calendar calCheckStart=Calendar.getInstance();
	   	    calCheckStart.setTime(dateStart);
	   	    java.util.Date checkInDate = calCheckStart.getTime();
	   	    this.arrivalDate=new Timestamp(checkInDate.getTime());
	   	    sessionMap.put("startDate",arrivalDate); 
	   	    
	   	    Calendar calCheckEnd=Calendar.getInstance();
	   	    calCheckEnd.setTime(dateEnd);
	   	    java.util.Date checkOutDate = calCheckEnd.getTime();
	   	    this.departureDate=new Timestamp(checkOutDate.getTime());
	   	    sessionMap.put("endDate",departureDate); 
	   	    
	   		
	   		/*Calendar cal = Calendar.getInstance();
	   		cal.setTime(this.departureDate);
	   	    cal.add(Calendar.DATE, -1);*/
	   		
	   		String startDate = new SimpleDateFormat("yyyy-MM-dd").format(this.arrivalDate);
	   		String endDate = new SimpleDateFormat("yyyy-MM-dd").format(this.departureDate);
	   		DateTime start = DateTime.parse(startDate);
	   		DateTime end = DateTime.parse(endDate);
	   		java.util.Date arrival = start.toDate();
	   		java.util.Date departure = end.toDate();
	   		
	   		
	   		//this.propertyId = (Integer) sessionMap.get("propertyId");
	   		String jsonOutput = "";
	   		HttpServletResponse response = ServletActionContext.getResponse();
	   		
	   			
	   			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
	   			PropertyAccommodationInventoryManager accommodationInventoryController = new PropertyAccommodationInventoryManager();  
	   			PmsBookingManager bookingController = new PmsBookingManager();
	   			PropertyTaxeManager taxController = new PropertyTaxeManager();
	   			
	   			response.setContentType("application/json");
	   			
	   			
	   				
	   			
	   			this.availableList = bookingController.list(this.partnerPropertyId,getArrivalDate(),getDepartureDate(),getPropertyAccommodationId());
	   				for (PmsAvailableRooms available : availableList) {  
	   					
	   					if (!jsonOutput.equalsIgnoreCase(""))
	   						jsonOutput += ",{";
	   					
	   					else
	   						jsonOutput += "{";
	            
	   				
	   					jsonOutput += "\"accommodationType\":\"" + available.getAccommodationType().toUpperCase() + "\"";   					
	   					jsonOutput += ",\"accommodationId\":\"" + available.getAccommodationId() + "\"";
	   				//jsonOutput += "\"availableId\":\"" + availableId+ "\"";
	   				
	   				long rmc = 0;
	   				this.roomCnt = rmc;
	   				List<DateTime> between = DateUtil.getDateRange(start, end);
	   				List<Long> myList = new ArrayList<Long>();
	   				String jsonAvailable ="";
	   				for (DateTime d : between)
	   				  {
	   					int sold=0;
	   					
	   					if (!jsonAvailable.equalsIgnoreCase(""))
							
	   						jsonAvailable += ",{";
						else
							jsonAvailable += "{";
	   					
	   					
	   					java.util.Date availDate = d.toDate();
	   					PropertyAccommodationInventory inventoryCount = accommodationInventoryController.findInventoryCount(availDate,available.getAccommodationId());
	   					if(inventoryCount != null){
	   						PmsAvailableRooms roomCount1 = bookingController.findBookingCount(availDate, available.getAccommodationId(),inventoryCount.getInventoryId());
	   						if(roomCount1.getRoomCount() == null) {
	   							String num1 = inventoryCount.getInventoryCount();
	   							PmsAvailableRooms bookingRoomCount=bookingController.findCount(availDate, available.getAccommodationId());
	   							PropertyAccommodation accommodations=accommodationController.find(available.getAccommodationId());
	   							Integer totavailable=0;
	   							 if(accommodations!=null){
	   								totavailable=accommodations.getNoOfUnits();
	   							 }
								if(bookingRoomCount.getRoomCount()!=null){
									long lngRooms=bookingRoomCount.getRoomCount();
									jsonAvailable += "\"sold\":\"" + (int)(lngRooms) + "\"";
									long num2 = Long.parseLong(num1);
									this.roomCnt = num2;
									sold=(int) (lngRooms);
									int availRooms=0;
									availRooms=totavailable-sold;
//									if(Integer.parseInt(num1)>availRooms){
//										this.roomCnt = availRooms;	
//									}else{
										this.roomCnt = num2;
//									}
									
								}else{
									 long num2 = Long.parseLong(num1);
									this.roomCnt = num2;
									jsonAvailable += "\"sold\":\"" + rmc + "\"";
									sold=(int) (rmc);
								}
								
	   							 /*String num1 = inventoryCount.getInventoryCount();
	   							 long num2 = Long.parseLong(num1);
	   							this.roomCnt = num2; 
	   							jsonAvailable += "\"sold\":\"" + rmc + "\"";
	   							sold=(int) (rmc);*/
	   						}
	   						else {
	   							
	   							PmsAvailableRooms bookingRoomCount=bookingController.findCount(availDate, available.getAccommodationId());
	   							int intRooms=0;
	   							PropertyAccommodation accommodations=accommodationController.find(available.getAccommodationId());
	   							Integer totavailable=0,availableRooms=0;
	   							 if(accommodations!=null){
	   								totavailable=accommodations.getNoOfUnits();
	   							 }
	   							if(bookingRoomCount.getRoomCount()!=null){
	   								long lngRooms=bookingRoomCount.getRoomCount();
	   								long num = roomCount1.getRoomCount();	
	   								intRooms=(int)lngRooms;
	   								String num1 = inventoryCount.getInventoryCount();
	     							 long num2 = Long.parseLong(num1);
	     							availableRooms=totavailable-intRooms;
//	     							 if(num2>availableRooms){
//	     								this.roomCnt = availableRooms;	 
//	     							 }else{
	     								this.roomCnt = num2-num;
//	     							 }
	     							jsonAvailable += "\"sold\":\"" + intRooms + "\"";
	     							long lngSold=bookingRoomCount.getRoomCount();
	       							sold=(int)lngSold;
	   							}else{
	   								long num = roomCount1.getRoomCount();	
	   								String num1 = inventoryCount.getInventoryCount();
	      							 long num2 = Long.parseLong(num1);
	      							this.roomCnt = (num2-num);
	      							jsonAvailable += "\"sold\":\"" + roomCount1.getRoomCount() + "\"";
	      							long lngSold=roomCount1.getRoomCount();
	       							sold=(int)lngSold;
	   							}
	   							/* long num = roomCount1.getRoomCount();
	   							 String num1 = inventoryCount.getInventoryCount();
	   							 long num2 = Long.parseLong(num1);
	   							this.roomCnt = (num2-num);
	   							jsonAvailable += "\"sold\":\"" + roomCount1.getRoomCount() + "\"";
	   							long lngSold=roomCount1.getRoomCount();
	   							sold=(int)lngSold;*/
	   						}
	   							
	   					}else{
	   						  PmsAvailableRooms roomCount = bookingController.findCount(availDate, available.getAccommodationId());							
	   							if(roomCount.getRoomCount() == null){								
	   								this.roomCnt = (available.getRoomAvailable()-rmc);
	   								jsonAvailable += "\"sold\":\"" + rmc + "\"";
	   								sold=(int)rmc;
	   							}
	   							else{
	   								this.roomCnt = (available.getRoomAvailable()-roomCount.getRoomCount());	
	   								jsonAvailable += "\"sold\":\"" + roomCount.getRoomCount() + "\"";
	   								long lngSold=roomCount.getRoomCount();
	   	   							sold=(int)lngSold;
	   							}
	   					}
	   							
	   					myList.add(this.roomCnt);
	   							
	   						
	   			        //long availableCount = Collections.min(myList);
	   							String startDate1 = new SimpleDateFormat("MMMM d, yyyy").format(availDate);
	   							jsonAvailable += ",\"available\":\"" + this.roomCnt + "\"";
	   							jsonAvailable += ",\"date\":\"" + startDate1 + "\"";
	   							jsonAvailable += ",\"totalRoomCount\":\"" + available.getNoOfUnits() + "\"";
	   							jsonAvailable += ",\"totalAvailable\":\"" + this.roomCnt + "\"";
	   							jsonAvailable += ",\"totalRooms\":\"" + available.getRoomAvailable() + "\"";
	   							jsonAvailable += ",\"soldRooms\":\"" + sold + "\"";	
	   							double totalRooms=0,intSold=0;
	   							totalRooms=(double)available.getNoOfUnits();
	   							intSold=(double)sold;
	   							double occupancy=0.0;
	   							occupancy=intSold/totalRooms;
	   							jsonAvailable += ",\"occupancy\":\"" + Math.round(occupancy * 100) +"%" + "\"";		
	   							jsonAvailable += "}";
	   			        //return availableCount;
	   				  
	   				  }
	   				/*PmsAvailableRooms roomCount = bookingController.findCount(arrival, departure,available.getAccommodationId());
	   				*/
	   					
	   				
	   				
	   				//jsonOutput += ",\"available\":\"" + available.getAvailable() + "\"";
	   				jsonOutput += ",\"roomAvailable\":[" + jsonAvailable+ "]";
	   				
	   				
	   				
	            }
	   				
	   				jsonOutput += "}";
	   		
	   			

	   			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
	   			

	   		
	   }catch(Exception e){
		   e.printStackTrace();
		   logger.error(e);
	   }
	   return null;
   }
   
   public String getBulkBlockInventory() throws IOException {
   	   
   	   try {
   	    /*this.arrivalDate = getArrivalDate();
   	    sessionMap.put("arrivalDate",arrivalDate); 
   			
   		this.departureDate = getDepartureDate();
   		sessionMap.put("departureDate",departureDate); */
   	      
   		//String numberAsString = String.valueOf(getAccommodationId());
   		/*String[] parts = getAccommodationTypeId().split(",");
   		 int[] intArray = new int[parts.length];
   		   for (int i = 0; i < parts.length; i++) {
   		         String numberAsString = parts[i];
   		         intArray[i] = Integer.parseInt(numberAsString);
   		      }*/
   			
   	    Timestamp timestamp = new Timestamp(System.currentTimeMillis());
   	    
   	    DateFormat format = new SimpleDateFormat("MMM d,yyyy" , Locale.ENGLISH);
   	    
   	    java.util.Date dateStart = format.parse(getStartBlockDate());
   	    java.util.Date dateEnd = format.parse(getEndBlockDate());
   	   
   	    
   	    Calendar calCheckStart=Calendar.getInstance();
   	    calCheckStart.setTime(dateStart);
   	    java.util.Date checkInDate = calCheckStart.getTime();
   	    this.arrivalDate=new Timestamp(checkInDate.getTime());
   	    sessionMap.put("startDate",arrivalDate); 
   	    
   	    Calendar calCheckEnd=Calendar.getInstance();
   	    calCheckEnd.setTime(dateEnd);
   	    java.util.Date checkOutDate = calCheckEnd.getTime();
   	    this.departureDate=new Timestamp(checkOutDate.getTime());
   	    sessionMap.put("endDate",departureDate); 
   	    
   		
   		/*Calendar cal = Calendar.getInstance();
   		cal.setTime(this.departureDate);
   	    cal.add(Calendar.DATE, -1);*/
   		
   		String startDate = new SimpleDateFormat("yyyy-MM-dd").format(this.arrivalDate);
   		String endDate = new SimpleDateFormat("yyyy-MM-dd").format(this.departureDate);
   		DateTime start = DateTime.parse(startDate);
   		DateTime end = DateTime.parse(endDate);
   		java.util.Date arrival = start.toDate();
   		java.util.Date departure = end.toDate();
   		
   		
   		this.propertyId = (Integer) sessionMap.get("propertyId");
   		String jsonOutput = "";
   		HttpServletResponse response = ServletActionContext.getResponse();
   		
   			
   			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
   			PropertyAccommodationInventoryManager accommodationInventoryController = new PropertyAccommodationInventoryManager();  
   			PmsBookingManager bookingController = new PmsBookingManager();
   			PropertyTaxeManager taxController = new PropertyTaxeManager();
   			
   			response.setContentType("application/json");
   			
   			
   				
   			
   			this.availableList = bookingController.list(getPropertyId(),getArrivalDate(),getDepartureDate(),getPropertyAccommodationId());
   				for (PmsAvailableRooms available : availableList) {  
   					
   					if (!jsonOutput.equalsIgnoreCase(""))
   						jsonOutput += ",{";
   					
   					else
   						jsonOutput += "{";
            
   				
   					jsonOutput += "\"accommodationType\":\"" + available.getAccommodationType().toUpperCase() + "\"";   					
   					jsonOutput += ",\"accommodationId\":\"" + available.getAccommodationId() + "\"";
   				//jsonOutput += "\"availableId\":\"" + availableId+ "\"";
   				
   				long rmc = 0;
   				this.roomCnt = rmc;
   				List<DateTime> between = DateUtil.getDateRange(start, end);
   				List<Long> myList = new ArrayList<Long>();
   				String jsonAvailable ="";
   				for (DateTime d : between)
   				  {
   					int sold=0;
   					
   					if (!jsonAvailable.equalsIgnoreCase(""))
						
   						jsonAvailable += ",{";
					else
						jsonAvailable += "{";
   					
   					Integer totavailable=0;
   					java.util.Date availDate = d.toDate();
   					PropertyAccommodationInventory inventoryCount = accommodationInventoryController.findInventoryCount(availDate,available.getAccommodationId());
   					if(inventoryCount != null){
   						//sivabalan
   						PmsAvailableRooms roomCount1 = bookingController.findBookingCount(availDate, available.getAccommodationId(),inventoryCount.getInventoryId());
   						if(roomCount1.getRoomCount() == null) {
   							String num1 = inventoryCount.getInventoryCount();
   							PmsAvailableRooms bookingRoomCount=bookingController.findCount(availDate, available.getAccommodationId());
   							PropertyAccommodation accommodations=accommodationController.find(available.getAccommodationId());
   							 if(accommodations!=null){
   								totavailable=accommodations.getNoOfUnits();
   							 }
							if(bookingRoomCount.getRoomCount()!=null){
								long lngRooms=bookingRoomCount.getRoomCount();
								jsonAvailable += "\"sold\":\"" + (int)(lngRooms) + "\"";
								long num2 = Long.parseLong(num1);
								this.roomCnt = num2;
								sold=(int) (lngRooms);
								int availRooms=0;
								availRooms=totavailable-sold;
//								if(Integer.parseInt(num1)>availRooms){
//									this.roomCnt = availRooms;	
//								}else{
									this.roomCnt = num2;
//								}
								
							}else{
								 long num2 = Long.parseLong(num1);
								this.roomCnt = num2;
								sold=(int) (rmc);
								jsonAvailable += "\"sold\":\"" + rmc + "\"";
								
							}
							
   							 /*String num1 = inventoryCount.getInventoryCount();
   							 long num2 = Long.parseLong(num1);
   							this.roomCnt = num2; 
   							jsonAvailable += "\"sold\":\"" + rmc + "\"";
   							sold=(int) (rmc);*/
   						}
   						else {
   							
   							PmsAvailableRooms bookingRoomCount=bookingController.findCount(availDate, available.getAccommodationId());
   							int intRooms=0;
   							PropertyAccommodation accommodations=accommodationController.find(available.getAccommodationId());
   							Integer availableRooms=0;
   							 if(accommodations!=null){
   								totavailable=accommodations.getNoOfUnits();
   							 }
   							if(bookingRoomCount.getRoomCount()!=null){
   								long lngRooms=bookingRoomCount.getRoomCount();
   								long num = roomCount1.getRoomCount();	
   								intRooms=(int)lngRooms;
   								String num1 = inventoryCount.getInventoryCount();
     							 long num2 = Long.parseLong(num1);
     							availableRooms=totavailable-intRooms;
//     							 if(num2>availableRooms){
//     								this.roomCnt = availableRooms;	 
//     							 }else{
     								this.roomCnt = num2-num;
//     							 }
     							jsonAvailable += "\"sold\":\"" + intRooms + "\"";
     							long lngSold=bookingRoomCount.getRoomCount();
       							sold=(int)lngSold;
   							}else{
   								long num = roomCount1.getRoomCount();	
   								String num1 = inventoryCount.getInventoryCount();
      							 long num2 = Long.parseLong(num1);
      							this.roomCnt = (num2-num);
      							jsonAvailable += "\"sold\":\"" + roomCount1.getRoomCount() + "\"";
      							long lngSold=roomCount1.getRoomCount();
       							sold=(int)lngSold;
   							}
   							/* long num = roomCount1.getRoomCount();
   							 String num1 = inventoryCount.getInventoryCount();
   							 long num2 = Long.parseLong(num1);
   							this.roomCnt = (num2-num);
   							jsonAvailable += "\"sold\":\"" + roomCount1.getRoomCount() + "\"";
   							long lngSold=roomCount1.getRoomCount();
   							sold=(int)lngSold;*/
   						}
   							
   					}else{
   						  PmsAvailableRooms roomCount = bookingController.findCount(availDate, available.getAccommodationId());							
   							if(roomCount.getRoomCount() == null){								
   								this.roomCnt = (available.getRoomAvailable()-rmc);
   								jsonAvailable += "\"sold\":\"" + rmc + "\"";
   								sold=(int)rmc;
   							}
   							else{
   								this.roomCnt = (available.getRoomAvailable()-roomCount.getRoomCount());	
   								jsonAvailable += "\"sold\":\"" + roomCount.getRoomCount() + "\"";
   								long lngSold=roomCount.getRoomCount();
   	   							sold=(int)lngSold;
   							}
   					}
   					myList.add(this.roomCnt);
   					int val1 = 0;
   					if(inventoryCount != null){
   						if(Integer.parseInt(inventoryCount.getInventoryCount())!=0){
   							val1 = (totavailable - Integer.parseInt(inventoryCount.getInventoryCount()) );
   						}else if(Integer.parseInt(inventoryCount.getInventoryCount())==0){
   							val1 = (totavailable - Integer.parseInt(inventoryCount.getInventoryCount()));
   						}
   						jsonAvailable += ",\"blockInventoryCount\":\"" + val1 + "\"";
   					}else{
   						jsonAvailable += ",\"blockInventoryCount\":\"" + "NA" + "\"";
   					}
   							 
   						
   							String startDate1 = new SimpleDateFormat("MMMM d, yyyy").format(availDate);
   							jsonAvailable += ",\"available\":\"" + this.roomCnt + "\"";
   							jsonAvailable += ",\"date\":\"" + startDate1 + "\"";
   							jsonAvailable += ",\"totalRoomCount\":\"" + available.getNoOfUnits() + "\"";
   							jsonAvailable += ",\"totalAvailable\":\"" + this.roomCnt + "\"";
   							jsonAvailable += ",\"totalRooms\":\"" + available.getRoomAvailable() + "\"";
   							
   							DateFormat dateFormat = new SimpleDateFormat("MMMM d, yyyy");
   							String strDate = "";
   							if(inventoryCount !=null){
   							strDate = dateFormat.format(inventoryCount.getCreatedDate());
   							}
   							//String startDate2 = new SimpleDateFormat("MMMM d, yyyy").format(inventoryCount.getBlockedDate());
   							jsonAvailable += ",\"blockInventoryDate\":\"" + (inventoryCount == null ? "N/R" : strDate)+ "\"";
   							jsonAvailable += ",\"soldRooms\":\"" + sold + "\"";	
   							double totalRooms=0,intSold=0;
   							totalRooms=(double)available.getNoOfUnits();
   							intSold=(double)sold;
   							double occupancy=0.0;
   							occupancy=intSold/totalRooms;
   							jsonAvailable += ",\"occupancy\":\"" + Math.round(occupancy * 100) +"%" + "\"";		
   							jsonAvailable += "}";
   				  
   				  }
   				
   				jsonOutput += ",\"roomAvailable\":[" + jsonAvailable+ "]";
   				
   				
   				
            }
   				
   				jsonOutput += "}";
   		
   			

   			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
   			

   		} catch (Exception e) {
   			logger.error(e);
   			e.printStackTrace();
   		} finally {

   		}

   		return null;

   	} 
   
public String getAllPartnerBlock() throws IOException {
   	   
   	   try {
   		   
   		this.propertyId = (Integer) sessionMap.get("propertyId");
   		String jsonOutput = "";
   		HttpServletResponse response = ServletActionContext.getResponse();
   		
   			
   			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
   			PropertyAccommodationInventoryManager accommodationInventoryController = new PropertyAccommodationInventoryManager();  
   			PmsBookingManager bookingController = new PmsBookingManager();
   			PropertyTaxeManager taxController = new PropertyTaxeManager();
   			
   			response.setContentType("application/json");
   			
   			
   				
   			
   			/*List<PropertyAccommodation> accommodationList = accommodationController.list(getPropertyId());
   				for (PropertyAccommodation available : accommodationList) {*/  
   					
   					if (!jsonOutput.equalsIgnoreCase(""))
   						jsonOutput += ",{";
   					
   					else
   						jsonOutput += "{";
            
   				
   					/*jsonOutput += "\"accommodationType\":\"" + available.getAccommodationType().toUpperCase() + "\"";   					
   					jsonOutput += ",\"accommodationId\":\"" + available.getAccommodationId() + "\"";*/
   				
   				
   				String jsonAvailable ="";
   					
   					List<PropertyAccommodationInventory> inventoryCount = accommodationInventoryController.listInventory(1460);
   					for(PropertyAccommodationInventory inventory : inventoryCount){
   						
   						if (!jsonAvailable.equalsIgnoreCase(""))
   							
   	   						jsonAvailable += ",{";
   						else
   							jsonAvailable += "{";
   						
							jsonAvailable += "\"inventoryId\":\"" + inventory.getInventoryId() + "\"";
							DateFormat dateFormat = new SimpleDateFormat("MMMM d, yyyy");
							String format1 = dateFormat.format(inventory.getBlockedDate());
							String format2 = dateFormat.format(inventory.getCreatedDate());
							jsonAvailable += ",\"blockedDate\":\"" + format1 + "\"";
							UserLoginManager loginController = new UserLoginManager();
							User user = loginController.find(inventory.getCreatedBy());
							jsonAvailable += ",\"blockedPerson\":\"" + user.getUserName() + "\"";
							jsonAvailable += ",\"availableCount\":\"" + inventory.getInventoryCount() + "\"";
							jsonAvailable += ",\"createdDate\":\"" + format2 + "\"";
							jsonAvailable += "}";
   						
   					}
   						
   				           // PropertyAccommodationInventory blockInventoryCount = accommodationInventoryController.findId(availDate,getPropertyAccommodationId());
   							

   							
   			        //return availableCount;
   				  
   				  
   					
   				jsonOutput += "\"roomAvailable\":[" + jsonAvailable+ "]";
   				jsonOutput += "}";
   				
   				
            //}
   				
   				
   		
   			

   			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
   			

   		} catch (Exception e) {
   			logger.error(e);
   			e.printStackTrace();
   		} finally {

   		}

   		return null;

   	} 
   

   public String getPromotionRoomAvailability(){

		try {

	 	    /*this.arrivalDate = getArrivalDate();
	 	    sessionMap.put("arrivalDate",arrivalDate); 
	 			
	 		this.departureDate = getDepartureDate();
	 		sessionMap.put("departureDate",departureDate);*/
	 		
	 		DateFormat format = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
		    java.util.Date dateStart = format.parse(getStrStartDate());
		    java.util.Date dateEnd = format.parse(getStrEndDate());
		   
		    
		    Calendar calCheckStart=Calendar.getInstance();
		    calCheckStart.setTime(dateStart);
		    java.util.Date checkInDate = calCheckStart.getTime();
		    this.arrivalDate=new Timestamp(checkInDate.getTime());
		    sessionMap.put("arrivalDate",arrivalDate); 
		    
		    Calendar calCheckEnd=Calendar.getInstance();
		    calCheckEnd.setTime(dateEnd);
		    java.util.Date checkOutDate = calCheckEnd.getTime();
		    this.departureDate=new Timestamp(checkOutDate.getTime());
		    sessionMap.put("departureDate",departureDate); 
		    
	 		String[] parts = getAccommodationTypeId().split(",");
	 		 int[] intArray = new int[parts.length];
	 		   for (int i = 0; i < parts.length; i++) {
	 		         String numberAsString = parts[i];
	 		         intArray[i] = Integer.parseInt(numberAsString);
	 		      }
	 	
	 	    Timestamp timestamp = new Timestamp(System.currentTimeMillis());
	 	    
	 		String startDate = new SimpleDateFormat("yyyy-MM-dd").format(this.arrivalDate);
	 		String endDate = new SimpleDateFormat("yyyy-MM-dd").format(this.departureDate);
	 		DateTime start = DateTime.parse(startDate);
	 		DateTime end = DateTime.parse(endDate);
	 		java.util.Date arrival = start.toDate();
	 		java.util.Date departure = end.toDate();
	 		
	 		
	 		this.propertyId = (Integer) sessionMap.get("propertyId");
	 		
	 		
	 			String jsonOutput = "";
	 			HttpServletResponse response = ServletActionContext.getResponse();
	 		
	 			
	 			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
	 			PmsBookingManager bookingController = new PmsBookingManager();
	 			PropertyTaxeManager taxController = new PropertyTaxeManager();
	 			response.setContentType("application/json");
	 			
	 			for (int accomId : intArray)
	 			{ 
	 				this.availableList = bookingController.list(getPropertyId(),getArrivalDate(),getDepartureDate(),accomId);
	 			
	 			
	          for (PmsAvailableRooms available : availableList) {
	          
	 				
	 				if (!jsonOutput.equalsIgnoreCase(""))
	 					jsonOutput += ",{";
	 				
	 				else
	 					jsonOutput += "{";
	 				
	 				jsonOutput += "\"propertyId\":\"" + getPropertyId() + "\"";
	 				jsonOutput += ",\"baseAmount\":\"" + available.getBaseAmount() + "\"";
	 				jsonOutput += ",\"arrivalDate\":\"" + getArrivalDate()+ "\"";
	 				jsonOutput += ",\"departureDate\":\"" + getDepartureDate()+ "\"";
	 				jsonOutput += ",\"rooms\":\"" + 0 + "\"";
	 			
	 				jsonOutput += ",\"accommodationId\":\"" + available.getAccommodationId() + "\"";
	 				jsonOutput += ",\"accommodationType\":\"" + available.getAccommodationType().toUpperCase() + "\"";
	 				jsonOutput += ",\"minOccupancy\":\"" + available.getMinOccupancy() + "\"";
	 				jsonOutput += ",\"maxOccupancy\":\"" + available.getMaxOccupancy() + "\"";
	 				jsonOutput += ",\"noOfAdults\":\"" + available.getNoOfAdults() + "\"";
	 				jsonOutput += ",\"noOfChild\":\"" + available.getNoOfChild() + "\"";
	 				jsonOutput += ",\"noOfInfant\":\"" + available.getNoOfInfant() + "\"";
	 				jsonOutput += ",\"extraChild\":\"" + available.getExtraChild() + "\"";
	 				jsonOutput += ",\"extraAdult\":\"" + available.getExtraAdult() + "\"";
	 				int diffNights=10;
	 				jsonOutput += ",\"diffnights\":\"" + diffNights + "\"";
	 				
	 				long rmc = 0;
	 				this.roomCnt = rmc;
	 				List<DateTime> between = DateUtil.getDateRange(start, end);
	 				List<Long> myList = new ArrayList<Long>();
	 				for (DateTime d : between)
	 			     {
	 					java.util.Date availDate = d.toDate();
	 					       
	 						  PmsAvailableRooms roomCount = bookingController.findCount(availDate, available.getAccommodationId());							
	 							if(roomCount.getRoomCount() == null){								
	 								this.roomCnt = (available.getRoomAvailable()-rmc);		
	 				               
	 							}
	 							else{
	 								
	 								this.roomCnt = (available.getRoomAvailable()-roomCount.getRoomCount());						
	 							}					
	 							
	 							myList.add(this.roomCnt);
	 							
	 						
	 			       
	 			     }
	 				
	 				jsonOutput += ",\"available\":\"" + Collections.min(myList) + "\"";
	 				jsonOutput += "}";
	 				
	 				
	 				
	          }
	 				

	 			}
	          
	 			

	 			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
	 			

	 		} catch (Exception e) {
	 			logger.error(e);
	 			e.printStackTrace();
	 		} finally {

	 		}

	   return null;
   }
   public String getAccommodationAvailabilities() throws IOException{

	   try{
		   	DateFormat format = new SimpleDateFormat("MMM d,yyyy", Locale.ENGLISH);
		    java.util.Date dateStart = format.parse(getStrStartDate());
		    java.util.Date dateEnd = format.parse(getStrEndDate());
		   
		    DecimalFormat df = new DecimalFormat("###.##");
		    DateFormat f = new SimpleDateFormat("EEEE");
		    
		    Calendar calCheckStart=Calendar.getInstance();
		    calCheckStart.setTime(dateStart);
		    java.util.Date checkInDate = calCheckStart.getTime();
		    this.arrivalDate=new Timestamp(checkInDate.getTime());
		   
		    if(getSourceTypeId()==null){
				   this.sourceTypeId=1;
			   }
		    Calendar calCheckEnd=Calendar.getInstance();
		    calCheckEnd.setTime(dateEnd);
		    java.util.Date checkOutDate = calCheckEnd.getTime();
		    this.departureDate=new Timestamp(checkOutDate.getTime());
			
			Map mapDate=new HashMap();  
			mapDate.put("arrivalDate", arrivalDate);
			mapDate.put("departureDate", departureDate); 
		      
			this.propertyId = getPropertyId();
			sessionMap.put("uloPropertyId",propertyId); 
			
			String checkIn = new SimpleDateFormat("MMM d,yyyy").format(getArrivalDate());
		    String checkOut = new SimpleDateFormat("MMM d,yyyy").format(getDepartureDate());
		    sessionMap.put("checkIn",checkIn);
			sessionMap.put("checkOut",checkOut);
			 
			
		    Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		   
			Calendar cal = Calendar.getInstance();
			cal.setTime(this.departureDate);
	        cal.add(Calendar.DATE, -1);
			String startDate = new SimpleDateFormat("yyyy-MM-dd").format(this.arrivalDate);
			String endDate = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
			DateTime start = DateTime.parse(startDate);
	        DateTime end = DateTime.parse(endDate);
	        java.util.Date arrival = start.toDate();
			java.util.Date departure = end.toDate();
			String iconPath  = "ulowebsite/images/icons/";
			
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("application/json");
			
			AccommodationAmenityManager accommodationAmenityController = new AccommodationAmenityManager();
			PmsPropertyManager propertyController = new PmsPropertyManager();		
			PropertyAccommodationManager accommodationManager = new PropertyAccommodationManager();
			PropertyAmenityManager amenityController = new PropertyAmenityManager();
			List<PropertyAmenity> propertyAmenityList;
			PropertyRateManager rateController = new PropertyRateManager();
			PropertyRateDetailManager detailController = new PropertyRateDetailManager();
			PmsAmenityManager ameController = new PmsAmenityManager();
			PromotionManager promotionController=new PromotionManager();
			PromotionDetailManager promotionDetailController=new PromotionDetailManager();
			
	    	List<AccommodationRoom> listAccommodationRoom=null;
			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
			PropertyReviewsManager reviewController=new PropertyReviewsManager();
		    this.propertyList=propertyController.list(getPropertyId());
		    if(this.propertyList.size()>0 && !this.propertyList.isEmpty()){
		    	for(PmsProperty property:this.propertyList){

					
					
		  			
		  			if (!jsonOutput.equalsIgnoreCase(""))
		  				jsonOutput += ",{";
		  			else
		  				jsonOutput += "{";
		  			jsonOutput += "\"DT_RowId\":\"" + property.getPropertyId()+ "\"";
		  			jsonOutput += ",\"city\":\"" + property.getCity() + "\"";
		  			jsonOutput += ",\"address1\":\"" + property.getAddress1() + "\"";
		  			jsonOutput += ",\"address2\":\"" + property.getAddress2() + "\"";
		  			jsonOutput += ",\"propertyName\":\"" + (property.getPropertyName() == null ? "": property.getPropertyName().toUpperCase().trim())+ "\"";
		  			jsonOutput += ",\"propertyUrl\":\"" + (property.getPropertyUrl() == null ? "": property.getPropertyUrl().toLowerCase().trim())+ "\"";
//		  			jsonOutput += ",\"propertyStandardPolicy\":\"" + (property.getPropertyStandardPolicy() == null ? "None": property.getPropertyStandardPolicy().trim())+ "\"";
//		  			jsonOutput += ",\"propertyHotelPolicy\":\"" + (property.getPropertyHotelPolicy() == null ? "None": property.getPropertyHotelPolicy().trim())+ "\"";
//		  			jsonOutput += ",\"propertyCancellationPolicy\":\"" + (property.getPropertyCancellationPolicy() == null ? "None": property.getPropertyCancellationPolicy().trim())+ "\"";
		  			jsonOutput += ",\"payAtHotelStatus\":\"" + (property.getPayAtHotelStatus() == null ? false: property.getPayAtHotelStatus())+ "\"";
					jsonOutput += ",\"coupleStatus\":\"" + ( property.getCoupleIsActive() == null? false :  property.getCoupleIsActive() )+ "\"";
					jsonOutput += ",\"breakfastStatus\":\"" + ( property.getBreakfastIsActive() == null? false:  property.getBreakfastIsActive() )+ "\"";
					jsonOutput += ",\"hotelStatus\":\"" + ( property.getHotelIsActive() == null? false:  property.getHotelIsActive() )+ "\"";
					
		  			SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
		  			Timestamp FromDate=(Timestamp)mapDate.get("arrivalDate");
		  			Timestamp ToDate=(Timestamp)mapDate.get("departureDate");
		  			jsonOutput += ",\"arrivalDate\":\"" + format2.format(FromDate) + "\"";
		  			jsonOutput += ",\"departureDate\":\"" + format2.format(ToDate)+ "\"";
		  			
		  			String strFromDate=format2.format(FromDate);
		  			String strToDate=format2.format(ToDate);
		  	    	DateTime startdate = DateTime.parse(strFromDate);
		  	        DateTime enddate = DateTime.parse(strToDate);
		  	        java.util.Date fromdate = startdate.toDate();
		  	 		java.util.Date todate = enddate.toDate();
		  	 		 
		  			int diffInDays = (int) ((todate.getTime() - fromdate.getTime()) / (1000 * 60 * 60 * 24));
		  			
		  			List arllist=new ArrayList();
		  			List arllistAvl=new ArrayList();
		  			List arllistSold=new ArrayList();
		  			jsonOutput += ",\"diffDays\":\"" + diffInDays+ "\"";
		  			
		  			String jsonAmenities ="";
		  			propertyAmenityList =  amenityController.list(property.getPropertyId());
		  			if(propertyAmenityList.size()>0)
		  			{
		  				for (PropertyAmenity amenity : propertyAmenityList) {
		  					if (!jsonAmenities.equalsIgnoreCase(""))
		  						
		  						jsonAmenities += ",{";
		  					else
		  						jsonAmenities += "{";
		  					PropertyAmenity amentity1 = amenityController.find(amenity.getPropertyAmenityId());
		  				    String path = "ulowebsite/images/icons/";
		  					PmsAmenity pmsAmenity1 =ameController.find(amentity1.getPmsAmenity().getAmenityId());
		  					jsonAmenities += "\"DT_RowId\":\"" + amenity.getPropertyAmenityId()+ "\"";					
		  					jsonAmenities += ",\"amenityName\":\"" + pmsAmenity1.getAmenityName()+ "\"";
		  					jsonAmenities += ",\"amenityId\":\"" + pmsAmenity1.getAmenityId()+ "\"";
		                      jsonAmenities += ",\"icon\":\"" + path + pmsAmenity1.getAmenityIcon()+ "\"";
		               
		  					
		  					jsonAmenities += "}";
		  				}
		  				
		  				jsonOutput += ",\"amenities\":[" + jsonAmenities+ "]";
		  			}
		  			String jsonGuestReview="";
		  			String jsonReviews="";
					List<PropertyReviews> listReviews=reviewController.listReview(property.getPropertyId());
					if(listReviews.size()>0 && !listReviews.isEmpty()){
						for(PropertyReviews reviews:listReviews){
							
							if (!jsonReviews.equalsIgnoreCase(""))
								
								jsonReviews += ",{";
							else
								jsonReviews += "{";

							/*jsonReviews += "\"starCount\":\"" + reviews.getStarCount().intValue()+ "\"";	
							jsonReviews += ",\"reviewCount\":\"" + reviews.getReviewCount().intValue()+ "\"";
							Double averageReviews=reviews.getAverageReview();
							jsonReviews += ",\"averageReview\":\"" + df.format(averageReviews)+ "\"";*/
							
							jsonReviews += "\"starCount\":\"" + (reviews.getStarCount()==null?0:reviews.getStarCount().intValue())+ "\"";	
							jsonReviews += ",\"reviewCount\":\"" + (reviews.getReviewCount()==null?0:reviews.getReviewCount().intValue())+ "\"";	
							if(reviews.getAverageReview()!=null){
								Double averageReviews=reviews.getAverageReview();
								jsonReviews += ",\"averageReview\":\"" + df.format(averageReviews)+ "\"";
							}else{
								jsonReviews += ",\"averageReview\":\"" + df.format(0)+ "\"";
							}
							
							jsonReviews += ",\"tripadvisorStarCount\":\"" + (reviews.getTripadvisorStarCount()==null?0:reviews.getTripadvisorStarCount().intValue())+ "\"";	
							jsonReviews += ",\"tripadvisorReviewCount\":\"" + (reviews.getTripadvisorReviewCount()==null?0:reviews.getTripadvisorReviewCount().intValue())+ "\"";
							if(reviews.getTripadvisorAverageReview()!=null){
								Double taAverageReviews=reviews.getTripadvisorAverageReview();
								jsonReviews += ",\"tripadvisorAverageReview\":\"" + df.format(taAverageReviews)+ "\"";
							}else{
								jsonReviews += ",\"tripadvisorAverageReview\":\"" + df.format(0)+ "\"";
							}
							jsonReviews += ",\"googleReviewUrl\":\"" + (reviews.getGoogleReviewUrl()==null?"":reviews.getGoogleReviewUrl())+ "\"";	
							jsonReviews += ",\"tripadvisorReviewUrl\":\"" + (reviews.getTripadvisorReviewUrl()==null?"":reviews.getTripadvisorReviewUrl())+ "\"";	
							jsonReviews += ",\"propertyRating\":\"" + (reviews.getPropertyRating()==null?"None":reviews.getPropertyRating())+ "\"";
							jsonReviews += "}";
						}
						
					}else{
						if (!jsonReviews.equalsIgnoreCase(""))
							
							jsonReviews += ",{";
						else
							jsonReviews += "{";
						
						jsonReviews += "\"starCount\":\"" + 0+ "\"";	
						jsonReviews += ",\"reviewCount\":\"" + 0+ "\"";	
						jsonReviews += ",\"averageReview\":\"" + 0+ "\"";
						
						jsonReviews += ",\"tripadvisorStarCount\":\"" + 0 + "\"";	
						jsonReviews += ",\"tripadvisorReviewCount\":\"" + 0 + "\"";	
						jsonReviews += ",\"tripadvisorAverageReview\":\"" + 0+ "\"";
						
						jsonReviews += ",\"googleReviewUrl\":\"" + "None"+ "\"";	
						jsonReviews += ",\"tripadvisorReviewUrl\":\"" + "None"+ "\"";	
						jsonReviews += ",\"propertyRating\":\"" + "None"+ "\"";
						
						jsonReviews += "}";
					}
					
					jsonOutput += ",\"reviews\":[" + jsonReviews+ "]";
					
					
					PropertyGuestReviewManager guestReviewController=new PropertyGuestReviewManager();
					List<PropertyGuestReview> listGuestReviews=guestReviewController.listReview(property.getPropertyId());
					if(listGuestReviews.size()>0 && !listGuestReviews.isEmpty()){
						for(PropertyGuestReview reviews:listGuestReviews){
							if (!jsonGuestReview.equalsIgnoreCase(""))
								
								jsonGuestReview += ",{";
							else
								jsonGuestReview += "{";
							
							jsonGuestReview += "\"reviewerGuestName\":\"" + (reviews.getReviewGuestName()==null?"":reviews.getReviewGuestName())+ "\"";	
							jsonGuestReview += ",\"reviewerDescription\":\"" + (reviews.getReviewDescription()==null?"":reviews.getReviewDescription())+ "\"";	
							jsonGuestReview += ",\"reviewerCount\":\"" + (reviews.getReviewCount()==null?0:reviews.getReviewCount())+ "\"";
							jsonGuestReview += ",\"reviewerName\":\"" + (reviews.getReviews().getReviewerName()==null?"":reviews.getReviews().getReviewerName())+ "\"";
							jsonGuestReview += ",\"reviewerId\":\"" + (reviews.getReviews().getReviewerId()==null?"":reviews.getReviews().getReviewerId())+ "\"";
							jsonGuestReview += "}";
						}
					}else{
						if (!jsonGuestReview.equalsIgnoreCase(""))
							
							jsonGuestReview += ",{";
						else
							jsonGuestReview += "{";
						
						jsonGuestReview += "\"reviewerGuestName\":\"" + ""+ "\"";	
						jsonGuestReview += ",\"reviewerDescription\":\"" + ""+ "\"";	
						jsonGuestReview += ",\"reviewerCount\":\"" + 0+ "\"";
						jsonGuestReview += ",\"reviewerName\":\"" + ""+ "\"";
						jsonGuestReview += ",\"reviewerId\":\"" + "" + "\"";
						
						jsonGuestReview += "}";
					}
				
					jsonOutput += ",\"guestReviews\":[" + jsonGuestReview+ "]";
		  			
		  			String jsonAccommodation ="";
		  			List<PropertyAccommodation> accommodationsList =  accommodationManager.list(property.getPropertyId());
		  			int soldOutCount=0,availCount=0,promotionAccommId=0;
		  			if(accommodationsList.size()>0 && !accommodationsList.isEmpty())
		  			{
		  				for (PropertyAccommodation accommodation : accommodationsList) {
		  					int roomCount=0;
		  					listAccommodationRoom=accommodationController.listPropertyTotalRooms(property.getPropertyId(), accommodation.getAccommodationId());
		  					if(listAccommodationRoom.size()>0 && !listAccommodationRoom.isEmpty()){
		  						for(AccommodationRoom roomsList:listAccommodationRoom){
		  							 roomCount=(int) roomsList.getRoomCount();
		  							 long minimum = getAvailableCount(property.getPropertyId(),getArrivalDate(),getDepartureDate(),roomCount,accommodation.getAccommodationId());
		  							 if((int)minimum>0){
		  								 availCount++;
		  							 }else if((int)minimum==0){
		  								 soldOutCount++;
		  							 }
		  							 break;
		  						 }
		  					 }
		  					
		  					
		  					if (!jsonAccommodation.equalsIgnoreCase(""))
		  						
		  						jsonAccommodation += ",{";
		  					else
		  						jsonAccommodation += "{";
		  						jsonAccommodation += "\"accommodationId\":\"" + accommodation.getAccommodationId()+ "\"";
		  						jsonAccommodation += ",\"accommodationName\":\"" + accommodation.getAccommodationType().toUpperCase()+ "\"";
		  					
		  					jsonAccommodation += "}";
		  				}
		  				
		  				jsonOutput += ",\"jsonAccommodations\":[" + jsonAccommodation+ "]";
		  			}

		  			
		  			List<PmsPromotionDetails> listPromotionDetailCheck=null;
		  			Boolean promotionFlag=false,blnPromoFlag=false;
		  			String promotionType=null;
		  			List<PropertyAccommodation> accommodationList =  accommodationManager.list(property.getPropertyId(),getPropertyAccommodationId());
		  			if(accommodationList.size()>0)
		  			{
		  				for (PropertyAccommodation accommodations : accommodationList) {

			  				int promoCount=0,enablePromoId=0;
			  			    listAccommodationRoom.clear(); 
			  			    ArrayList<String> arlPromoType=new ArrayList<String>();
			  			    ArrayList<Integer> arlEnablePromo=new ArrayList<Integer>();
			  		    	listAccommodationRoom=accommodationController.listPropertyTotalRooms(property.getPropertyId(), accommodations.getAccommodationId());
			  		    	List<DateTime> betweenAmount = DateUtil.getDateRange(start, end);
			  		    	double actualPromoBaseAmount=0.0,actualBaseAmount=0.0,actualTotalAmount=0.0,actualTotalAdultAmount=0.0,actualTotalChildAmount=0.0,
			  		    			actualAdultAmount=0.0,actualChildAmount=0.0,promotionFirstPercent=0.0,promotionFirstInr=0.0,
			  	  				    		promotionSecondPercent=0.0,promotionSecondInr=0.0;
			  			    int countDate=0,promotionId=0,promoAccommId=0; 
			  			    boolean blnLastMinuteFirstActive=false;
			  			    String promotionFirstName=null,promotionSecondName=null,promotionHoursRange=null,
			  			    		promotionSecondDiscountType=null,promotionFirstDiscountType=null,strCheckPromotionType=null;
			  			    Integer promoFirstPercent=null,promoFirstInr=null,promoSecondInr=null,promoSecondPercent=null;
			  			    double baseAmount=0.0,extraAdultAmount=0.0,extraChildAmount=0.0,totalBaseAmount=0.0,
			  			    		totalFlatAmount=0.0;
			  			    int roomCount=0,availableCount=0,totalCount=0,discountId=0;
			  				if(listAccommodationRoom.size()>0 && !listAccommodationRoom.isEmpty()){
			  					for(AccommodationRoom roomsList:listAccommodationRoom){
			  						 roomCount=(int) roomsList.getRoomCount();
			  						 break;
			  					 }
			  				 }
			  				 Timestamp tsPromoBookedDate=null,tsPromoStartDate=null,tsPromoEndDate=null;
			  				 Timestamp tsFirstStartRange=null,tsSecondStartRange=null,tsFirstEndRange=null,tsSecondEndRange=null;
			  				 Timestamp tsLastStartDate=null,tsLastEndDate=null;
			  				 boolean blnLastMinutePromo=false,blnEarlyBirdPromo=false,blnFirstActivePromo=false,blnFirstNotActivePromo=false,basePromoFlag=false;
							 boolean blnSecondActivePromo=false,blnSecondNotActivePromo=false;
							 ArrayList<Integer> arlFirstActivePromo=new ArrayList<Integer>(); 
							 ArrayList<Integer> arlSecondActivePromo=new ArrayList<Integer>(); 
							 List<DateTime> between = DateUtil.getDateRange(start, end);
							 SimpleDateFormat format1=new SimpleDateFormat("yyyy-MM-dd");
							 java.util.Date currentdate=new java.util.Date();
							 String strCurrentDate = new SimpleDateFormat("yyyy-MM-dd").format(currentdate);
							 java.util.Date dtecurdate = format1.parse(strCurrentDate);
		 				   	 Calendar calStart=Calendar.getInstance();
		 				   	 calStart.setTime(dtecurdate);
		 				   	 calStart.setTimeZone(TimeZone.getTimeZone("IST"));
		 				   	 String StartDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calStart.getTime());
		 				   	 java.util.Date curdate = format1.parse(StartDate);
		 				   	 
						   	 this.displayHoursForTimer=null;
						   	 setDisplayHoursForTimer(this.displayHoursForTimer);
							 List<PmsPromotions> listPromotions= promotionController.listBookedDatePromotions(property.getPropertyId(),curdate);
							 if(listPromotions.size()>0 && !listPromotions.isEmpty()){
								 for(PmsPromotions promotions:listPromotions){
									tsPromoBookedDate=promotions.getPromotionBookedDate();
									tsPromoStartDate=promotions.getStartDate();
									tsPromoEndDate=promotions.getEndDate();
									blnEarlyBirdPromo=checkEarlyBirdPromos(tsPromoBookedDate,tsPromoStartDate,tsPromoEndDate,getArrivalDate(),getDepartureDate());
								}
							 }else{
								 blnEarlyBirdPromo=false;
							 }
							 
							 List<PmsPromotions> listLastPromotions= promotionController.listLastBookedPromotions(property.getPropertyId(),curdate);
							 if(listLastPromotions.size()>0 && !listLastPromotions.isEmpty()){
								for(PmsPromotions promotions:listLastPromotions){
									tsFirstStartRange=promotions.getFirstRangeStartDate();
									tsSecondStartRange=promotions.getSecondRangeStartDate();
									tsFirstEndRange=promotions.getFirstRangeEndDate();
									tsSecondEndRange=promotions.getSecondRangeEndDate();
									tsLastStartDate=promotions.getStartDate();
									tsLastEndDate=promotions.getEndDate();
									promotionHoursRange=promotions.getPromotionHoursRange();
									blnLastMinuteFirstActive=checkLastMinutePromos(promotionHoursRange,getArrivalDate(),getDepartureDate(),tsSecondStartRange,tsFirstEndRange,tsFirstStartRange,tsSecondEndRange);
//									blnLastMinuteIsActive=getCheckHoursRange(promotionHoursRange,getArrivalDate(),getDepartureDate());
									if(blnLastMinuteFirstActive){
										blnLastMinutePromo=true;
										enablePromoId=promotions.getPromotionId();
										arlEnablePromo.add(enablePromoId);
									}
								}
							 }else{
								 blnLastMinutePromo=false;
								 this.displayHoursForTimer=null;
						    	 setDisplayHoursForTimer(this.displayHoursForTimer);
							 }
							 
							 for (DateTime d : between)
						     {
									 if(blnEarlyBirdPromo){
										 List<PmsPromotions> listEarlyPromotions= promotionController.listBookedDatePromotions(property.getPropertyId(),curdate);
										 if(listEarlyPromotions.size()>0 && !listEarlyPromotions.isEmpty()){
											 for(PmsPromotions promos:listEarlyPromotions){
												 if(promos.getPromotionType().equalsIgnoreCase("E")){
				 									 if(arlFirstActivePromo.isEmpty()){
				 										arlFirstActivePromo.add(promos.getPromotionId());
				 									 }else if(!arlFirstActivePromo.contains(promos.getPromotionId())){
				 										arlFirstActivePromo.add(promos.getPromotionId());
				 									 }
				 								 }else{
				 									blnFirstNotActivePromo=true;
				 								 }
											 }
										 }
									 }else if(!blnEarlyBirdPromo){
										 List<PmsPromotions> listAllPromotions= promotionController.listFlatDatePromotions(property.getPropertyId(),d.toDate());
										 if(!listAllPromotions.isEmpty() && listAllPromotions.size()>0){
											 for(PmsPromotions promos:listAllPromotions){
												 if(promos.getPromotionType().equalsIgnoreCase("F")){
				 									 if(arlFirstActivePromo.isEmpty()){
				 										arlFirstActivePromo.add(promos.getPromotionId());
				 									 }else if(!arlFirstActivePromo.contains(promos.getPromotionId())){
				 										arlFirstActivePromo.add(promos.getPromotionId());
				 									 }
				 								 }else{
				 									blnFirstNotActivePromo=true;
				 								 }
											 }
										 } 
									 }else{
										 blnFirstNotActivePromo=true;
									 }
									 
									 if(blnLastMinutePromo){
				  							List<PmsPromotions> listLastPromos= promotionController.listLastBookedPromotions(property.getPropertyId(),curdate);
				  							 if(listLastPromos.size()>0 && !listLastPromos.isEmpty()){
				  								for(PmsPromotions promos:listLastPromos){
				  									if(promos.getPromotionType().equalsIgnoreCase("L")){
				  										promoAccommId=promos.getPropertyAccommodation().getAccommodationId();
				  										if(arlEnablePromo.contains(promos.getPromotionId())){
			  												if(arlSecondActivePromo.isEmpty()){
				  		  										arlSecondActivePromo.add(promos.getPromotionId());
				  		  									 }else if(!arlSecondActivePromo.contains(promos.getPromotionId())){
				  		  										arlSecondActivePromo.add(promos.getPromotionId());
				  		  									 }
			  											}
				  									 }
				  								}
				  							 }else{
				  								blnSecondNotActivePromo=true;
				  							 }
										}else{
											blnSecondNotActivePromo=true;
										}
							     }
							 int intPromoSize=arlFirstActivePromo.size();
							 if((arlFirstActivePromo.isEmpty() || blnFirstNotActivePromo)){
								 blnFirstActivePromo=false;
							 }else{
								 blnFirstActivePromo=true;
							 }
							 
							 if(arlSecondActivePromo.isEmpty() || blnSecondNotActivePromo){
								blnSecondActivePromo=false;
							 }else{
								 blnSecondActivePromo=true;
							 }
			  					

			  					for (DateTime d : betweenAmount)
			  				    {
			  						boolean blnFlatPromotions=false;
			  						if(blnFirstActivePromo){
			  							if(blnEarlyBirdPromo){
			  	  							List<PmsPromotions> listFlatPromotions= promotionController.listBookedDatePromotions(property.getPropertyId(),curdate);
			  	  					    	if(listFlatPromotions.size()>0 && !listFlatPromotions.isEmpty()){
			  	  					    		for(PmsPromotions promotions:listFlatPromotions){
			  	  					    			 if(promotions.getPromotionType().equalsIgnoreCase("E")){
			  	  					    				 promotionId=promotions.getPromotionId();
			  	  					    				 promotionType=promotions.getPromotionType();
			  	  					    				 promotionFirstDiscountType=promotions.getPromotionDiscountType();
			  	  					    				 listPromotionDetailCheck=promotionDetailController.listPromotionDetails(promotions.getPromotionId(),f.format(d.toDate()).toLowerCase());
			  	  					    				 for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
			  	  				    						if(promoCount==0){
			  	  				    							promotionFirstPercent=promoDetails.getPromotionPercentage();
			  	 	 					    					promotionFirstInr=promoDetails.getPromotionInr();
			  	 	 					    					promotionType=promotions.getPromotionType();
			  	  	  				    						 promotionFlag=true;
			  	  	  						    				 blnPromoFlag=true;
			  	  	  						    				 arlPromoType.add(promotionType);
			  	  				    						}
			  	  				    						promoCount++;
			  	  				    					 }
			  	  					    			 }
			  	  					    		}
			  	  					    	}
			  	  						
			  							}else if(!blnEarlyBirdPromo){

			  	  							List<PmsPromotions> listFlatPromotions= promotionController.listFlatDatePromotions(property.getPropertyId(),d.toDate());
			  	  					    	if(listFlatPromotions.size()>0 && !listFlatPromotions.isEmpty()){
			  	  					    		for(PmsPromotions promotions:listFlatPromotions){
			  	  					    			 if(promotions.getPromotionType().equalsIgnoreCase("F")){
			  	  					    				 promotionId=promotions.getPromotionId();
			  	  					    				 promotionType=promotions.getPromotionType();
			  	  					    				 promotionFirstDiscountType=promotions.getPromotionDiscountType();
			  	  					    				 listPromotionDetailCheck=promotionDetailController.listPromotionDetails(promotions.getPromotionId(),f.format(d.toDate()).toLowerCase());
			  	  					    				 for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
			  	  				    						if(promoCount==0){
			  	  				    							promotionFirstPercent=promoDetails.getPromotionPercentage();
			  	 	 					    					promotionFirstInr=promoDetails.getPromotionInr();
			  	 	 					    					promotionType=promotions.getPromotionType();
			  	  	  				    						 promotionFlag=true;
			  	  	  						    				 blnPromoFlag=true;
			  	  	  						    				 arlPromoType.add(promotionType);
			  	  				    						}
			  	  				    						promoCount++;
			  	  				    					 }
			  	  					    			 }
			  	  					    		}
			  	  					    	}
			  							}
			  						}
			  						if(blnSecondActivePromo){

							    		List<PmsPromotions> listPmsPromotions= promotionController.listLastDatePromotions(property.getPropertyId(),accommodations.getAccommodationId(),curdate);
							    			 if(listPmsPromotions.size()>0 &&!listPmsPromotions.isEmpty()){
							    				 for(PmsPromotions pmsPromotions:listPmsPromotions){
							    					 promotionId=pmsPromotions.getPromotionId();
									    			 promoAccommId=pmsPromotions.getPropertyAccommodation().getAccommodationId();
									    			 promotionHoursRange=pmsPromotions.getPromotionHoursRange();
									    			 Timestamp tsStartDate=null,tsEndDate=null;
									    			 tsStartDate=pmsPromotions.getStartDate();
									    			 tsEndDate=pmsPromotions.getEndDate();
									    			 
									    			 if(arlEnablePromo.contains(pmsPromotions.getPromotionId())){
									    				 promotionAccommId=promoAccommId;
									    				 long minimumCount = getAvailableCount(getPropertyId(),getArrivalDate(),getDepartureDate(),roomCount,accommodations.getAccommodationId());
						    			    			 if((int)minimumCount>0){
					    			    					 promotionFlag=true; 
					    			    					 blnPromoFlag=true;
					    			    					 strCheckPromotionType="L";
					    			    					 promotionType=pmsPromotions.getPromotionType();
					    			    					 arlPromoType.add(promotionType);
					    			    					 promotionSecondDiscountType=pmsPromotions.getPromotionDiscountType();
				 					    					 listPromotionDetailCheck=promotionDetailController.listPromotionDetails(pmsPromotions.getPromotionId(),f.format(curdate).toLowerCase());
				 					    					 if(listPromotionDetailCheck.size()>0 && !listPromotionDetailCheck.isEmpty()){
				 					    						for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
				    	 					    					promotionSecondPercent=promoDetails.getPromotionPercentage();
			    	 	 					    					promotionSecondInr=promoDetails.getPromotionInr();
				    	 					    				 }
				 					    					 }
						    			    			 }else{
						    			    				 promotionFlag=false;
						    			    				 promotionType=null;
						    			    				 blnPromoFlag=false;
						    			    				 basePromoFlag=true;
						    			    			 }
									    			 }
							    				 }
							    			 }
			  						}
			  						if(promotionFlag){
			  							int rateCount=0,rateIdCount=0;
			    						List<PropertyRate> propertyRateList =  rateController.listAllDates(property.getPropertyId(),accommodations.getAccommodationId(),getSourceTypeId(),d.toDate());
			    				    	if(propertyRateList.size()>0)
			    				    	{
			    				    		 for (PropertyRate pr : propertyRateList)
			    			    			 {
							    				 int propertyRateId = pr.getPropertyRateId();
								    			 List<PropertyRateDetail> rateDetailList =   detailController.list(propertyRateId,f.format(d.toDate()).toLowerCase());
			        				    		 if(rateDetailList.size()>0){
			        				    			 if(rateCount<1){
			        				    				 for (PropertyRateDetail rate : rateDetailList){
			        				    					 baseAmount+=  rate.getBaseAmount();
			        				    					 extraAdultAmount+=rate.getExtraAdult();
			        	  				    				 extraChildAmount+=rate.getExtraChild();
				        				    			 }
			        				    				 rateCount++;
			        				    			 }
			        				    		 }else{
			        				    			 if(propertyRateList.size()==rateIdCount){
			        				    				 baseAmount += accommodations.getBaseAmount();
			        				    				 extraAdultAmount+=accommodations.getExtraAdult();
			          				    				 extraChildAmount+=accommodations.getExtraChild();
			        				    			 }
			            				    	 }
			    			    			 }	    		 
			    				    	 }
			    				    	 else{
			    				    		baseAmount += accommodations.getBaseAmount();
			    				    		extraAdultAmount+=accommodations.getExtraAdult();
						    				extraChildAmount+=accommodations.getExtraChild();
			    				    	 }
			    				    	double discountAmount=0;
			 				    		 if(blnFirstActivePromo){
			  	    				    	  String strTypePercent=String.valueOf(promotionFirstPercent);
			  	    				    	  String strTypeInr=String.valueOf(promotionFirstInr);
								    		  if(!strTypePercent.equalsIgnoreCase("null") && promotionFirstPercent!=0){
								    			  actualPromoBaseAmount = baseAmount-(baseAmount*promotionFirstPercent/100);
								    			  discountAmount+=baseAmount*promotionFirstPercent/100;
								    		  }
								    		  else if(!strTypeInr.equalsIgnoreCase("null") && promotionFirstInr!=0){
								    			  actualPromoBaseAmount = baseAmount-promotionFirstInr;
								    			  discountAmount+=promotionFirstInr;
								    		  }
								    		  else{
								    			  actualPromoBaseAmount = baseAmount;
								    		  } 
			 				    		 }else{
							    			  actualPromoBaseAmount = baseAmount;
							    		  } 
			 				    		 if(blnSecondActivePromo){
			 				    			String strTypePercent=String.valueOf(promotionSecondPercent);
			  	    				    	String strTypeInr=String.valueOf(promotionSecondInr);
								    		  if(!strTypePercent.equalsIgnoreCase("null") && promotionSecondPercent!=0){
								    			  actualPromoBaseAmount = actualPromoBaseAmount-(actualPromoBaseAmount*promotionSecondPercent/100);
								    			  discountAmount+=baseAmount*promotionSecondPercent/100;
								    		  }
								    		  else if(!strTypeInr.equalsIgnoreCase("null") && promotionSecondInr!=0){
								    			  actualPromoBaseAmount= actualPromoBaseAmount-promotionSecondInr;
								    			  discountAmount+=promotionSecondInr;
								    		  }
								    		  else{
								    			  actualPromoBaseAmount = actualPromoBaseAmount;
								    		  }
			 				    		 }else{
							    			  actualPromoBaseAmount = actualPromoBaseAmount;
							    		  }
			 				    		actualBaseAmount=baseAmount;
			 				    		totalFlatAmount=baseAmount-discountAmount;
			 				    		 
			 				    	 }else{
			 				    		 if(basePromoFlag){
			 				    			baseAmount += accommodations.getBaseAmount();
			 				    			extraAdultAmount+=accommodations.getExtraAdult();
			 				    			extraChildAmount+=accommodations.getExtraChild();
			 				    		}else{
			 				    			int rateCount=0,rateIdCount=0;
			 	    						List<PropertyRate> propertyRateList =  rateController.listAllDates(property.getPropertyId(),accommodations.getAccommodationId(),getSourceTypeId(),d.toDate());
			 	    				    	if(propertyRateList.size()>0)
			 	    				    	{
			 	    				    		 for (PropertyRate pr : propertyRateList)
			 	    			    			 {
			 					    				 int propertyRateId = pr.getPropertyRateId();
									    			 List<PropertyRateDetail> rateDetailList =   detailController.list(propertyRateId,f.format(d.toDate()).toLowerCase());
				        				    		 if(rateDetailList.size()>0){
				        				    			 if(rateCount<1){
				        				    				 for (PropertyRateDetail rate : rateDetailList){
				        				    					 baseAmount +=  rate.getBaseAmount();
				        				    					 extraAdultAmount+=rate.getExtraAdult();
				        	  				    				 extraChildAmount+=rate.getExtraChild();
					        				    			 }
				        				    				 rateCount++;
				        				    			 }
				        				    		 }else{
				        				    			 if(propertyRateList.size()==rateIdCount){
				        				    				 baseAmount += accommodations.getBaseAmount();
				        				    				 extraAdultAmount+=accommodations.getExtraAdult();
				          				    				 extraChildAmount+=accommodations.getExtraChild();
				        				    			 }
				            				    	 }
			 	    			    			 }	    		 
			 	    				    	 }
			 	    				    	 else{
			 	    				    		baseAmount += accommodations.getBaseAmount();
			 	    				    		extraAdultAmount+=accommodations.getExtraAdult();
			 				    				extraChildAmount+=accommodations.getExtraChild();
			 	    				    	 }
			 				    		}
			 				    	 }
			  						 actualBaseAmount=baseAmount;
			  				    	 countDate++;
			  				    }
			  					Iterator<String> iterPromoType=arlPromoType.iterator();
			  					while(iterPromoType.hasNext()){
			  						String strPromoType=iterPromoType.next();
			  						if(strPromoType.equalsIgnoreCase("F") || strPromoType.equalsIgnoreCase("E")){
			  							String strTypePercent=String.valueOf(promotionFirstPercent);
			    				    	String strTypeInr=String.valueOf(promotionFirstInr);
							    		if(!strTypePercent.equalsIgnoreCase("null") && promotionFirstPercent!=0){
							    			promotionFirstName=String.valueOf(Math.round(promotionFirstPercent))+" % OFF";
				  							promoFirstPercent=(int) Math.round(promotionFirstPercent);
							    		}
							    		else if(!strTypeInr.equalsIgnoreCase("null") && promotionFirstInr!=0){
							    			promotionFirstName="Flat "+String.valueOf(Math.round(promotionFirstInr))+" OFF";
				  							promoFirstInr=(int) Math.round(promotionFirstInr);
							    		}
			  							
			  						}
			  						if(strPromoType.equalsIgnoreCase("L")){
			  							  String strTypePercent=String.valueOf(promotionSecondPercent);
			  							  String strTypeInr=String.valueOf(promotionSecondInr);
							    		  if(!strTypePercent.equalsIgnoreCase("null") && promotionSecondPercent!=0){
							    			  promotionSecondName=String.valueOf(Math.round(promotionSecondPercent))+" % OFF";
					  						  promoSecondPercent=(int) Math.round(promotionSecondPercent);
							    		  }
							    		  else if(!strTypeInr.equalsIgnoreCase("null") && promotionSecondInr!=0){
							    			  promotionSecondName="Flat "+String.valueOf(Math.round(promotionSecondInr))+" OFF";
					  						  promoSecondInr=(int) Math.round(promotionSecondInr);
							    		  }
			  							
			  						}
			  					}
			  					arlPromoType.clear();
			  				
			  					if(blnPromoFlag){
			  						if(promotionType.equalsIgnoreCase("L") || promotionType.equalsIgnoreCase("E") || promotionType.equalsIgnoreCase("F")){
			  							totalAmount=totalFlatAmount;
			  							
			  							if(countDate>1){
			  								actualAdultAmount=extraAdultAmount/countDate;
			  								actualChildAmount=extraChildAmount/countDate;
			  							}else{
			  								actualAdultAmount=extraAdultAmount;
			  								actualChildAmount=extraChildAmount;
			  							}
			  						}
			  					}else{
			  						totalAmount=baseAmount;
			  						
			  						if(countDate>1){
			  							actualAdultAmount=extraAdultAmount/countDate;
			  							actualChildAmount=extraChildAmount/countDate;
			  						}else{
			  							actualAdultAmount=extraAdultAmount;
			  							actualChildAmount=extraChildAmount;
			  						}
			  					}
			  					
			  					
			  					totalAmount=Math.round(totalAmount);
			  					actualTotalAdultAmount=Math.round(actualAdultAmount);
			  					actualTotalChildAmount=Math.round(actualChildAmount);
			  					
			  					PropertyRatePlanDetailManager planDetailController=new PropertyRatePlanDetailManager();
			 					
			 					ArrayList arlRateType=new ArrayList();
			 			        List<PropertyRatePlanDetail> listPlanDetails=planDetailController.listAccommodation(property.getPropertyId(), accommodations.getAccommodationId());
			 			        if(listPlanDetails.size()>0 && !listPlanDetails.isEmpty()){
			 			        	for(PropertyRatePlanDetail ratePlanDetails:listPlanDetails){
			 			        		arlRateType.add(ratePlanDetails.getRatePlan().getRatePlanType());
			 			        		arlRateType.add(ratePlanDetails.getRatePlan().getRatePlanSymbolType());
			 			        		arlRateType.add(ratePlanDetails.getTariffAmount());
			 			        	}
			 			        }
			 			       String strType="EP",strSymbol=null;
			 	            	double dblRatePlanAmount=0;
			 			       Iterator iterRatePlan=arlRateType.iterator();
			 			       if(arlRateType.size()>0 && !arlRateType.isEmpty()){
			 			    	  while(iterRatePlan.hasNext()){
			 	 	            		strType=(String)iterRatePlan.next();
			 	 	            		strSymbol=(String)iterRatePlan.next();
			 	 	            		dblRatePlanAmount=(Double)iterRatePlan.next();
			 	 	            		
			 	 	            		if(strType.equalsIgnoreCase("CP")){
			 	 	            			jsonOutput += ",\"ratePlanType\":\"" + strType+"\"";
			 	 	            			
			 	 	                		if(strSymbol.equalsIgnoreCase("plus")){
			 	 	                			Double dblRate=0.0;
			 	 	                			dblRate=totalAmount+dblRatePlanAmount;
			 	 	                			jsonOutput += ",\"ratePlanAmount\":\"" + dblRate +  "\"";
			 	 	                		}else{
			 	 	                			Double dblRate=0.0;
			 	 	                			dblRate=totalAmount-dblRatePlanAmount;
			 	 	                			jsonOutput += ",\"ratePlanAmount\":\"" + dblRate +  "\"";
			 	 	                		}
			 	 	            		}
			 	 	            		
			 	 	            	}
			 			    	   
			 			       }else{
			 			    	  jsonOutput += ",\"ratePlanType\":\"" + strType +"\"";
			 			    	  jsonOutput += ",\"ratePlanAmount\":\"" + totalAmount +  "\"";
			 			       }
			 			       
			  					
			  					long minimum = getAvailableCount(property.getPropertyId(),getArrivalDate(),getDepartureDate(),roomCount,accommodations.getAccommodationId());
			  			    	 
			  					
			  					 PropertyTaxeManager propertytaxController = new PropertyTaxeManager();
			  	  					 double taxe =  totalAmount/diffInDays;
			  	  					
			  						 PropertyTaxe tax = propertytaxController.find(taxe);
			  						 double taxes = Math.round(totalAmount * tax.getTaxPercentage() / 100  ) ;
			  	 			         jsonOutput += ",\"tax\":\"" + taxes + "\"";
			  	 			         jsonOutput += ",\"roomTax\":\"" + taxes + "\"";
			  	 			         jsonOutput += ",\"taxPercentage\":\"" + tax.getTaxPercentage() + "\"";
			  			    	 if(minimum==0){
			  			    		 jsonOutput += ",\"available\":\"" + minimum+ "\"";
			  			    		 jsonOutput += ",\"baseActualAmount\":\"" + + (actualBaseAmount==0 ? accommodations.getBaseAmount() : actualBaseAmount )+  "\"";
			  			    		 jsonOutput += ",\"baseAmount\":\"" + + (totalAmount==0 ? accommodations.getBaseAmount() : totalAmount )+  "\"";
			  			    		 jsonOutput += ",\"totalAmount\":\"" + + (totalAmount==0 ? accommodations.getBaseAmount() : totalAmount )+  "\"";
			  			    		 jsonOutput += ",\"accommodationName\":\"" + accommodations.getAccommodationType().toUpperCase()+ "\"";
			  						 jsonOutput += ",\"accommodationId\":\"" + accommodations.getAccommodationId()+ "\"";
			  						 jsonOutput += ",\"promotionFirstName\":\"" + (promotionFirstName==null? "None":promotionFirstName)+ "\"";
			  						 jsonOutput += ",\"promotionSecondName\":\"" + (promotionSecondName==null? "None":promotionSecondName)+ "\"";
			  						 jsonOutput += ",\"promoFirstPercent\":\"" + (promoFirstPercent==null? 0:promoFirstPercent)+ "\"";
			  						 jsonOutput += ",\"promoFirstInr\":\"" + (promoFirstInr==null? 0:promoFirstInr)+ "\"";
			 						 jsonOutput += ",\"promoSecondInr\":\"" + (promoSecondInr==null? 0:promoSecondInr)+ "\"";
			  						 jsonOutput += ",\"promoSecondPercent\":\"" + (promoSecondPercent==null? 0:promoSecondPercent)+ "\"";	
			  						jsonOutput += ",\"promoLastMinuteTimer\":\"" + (getDisplayHoursForTimer()==null ?"None" :getDisplayHoursForTimer()  )+ "\"";
			  				     }else{
			  				    	jsonOutput += ",\"accommodationName\":\"" + accommodations.getAccommodationType().toUpperCase()+ "\"";
			  						jsonOutput += ",\"accommodationId\":\"" + accommodations.getAccommodationId()+ "\"";
			  			    		jsonOutput += ",\"available\":\"" + minimum+ "\"";
			  			    		jsonOutput += ",\"maxAdult\":\"" + accommodations.getMaxOccupancy()+ "\"";
			  			    		jsonOutput += ",\"maxChild\":\"" + 0 + "\"";
			  			    		jsonOutput += ",\"adultsCount\":\"" + 1 + "\"";
			  						jsonOutput += ",\"childCount\":\"" + 0 + "\"";
			  						jsonOutput += ",\"rooms\":\"" + 1 + "\"";
			  						jsonOutput += ",\"noOfAdults\":\"" + accommodations.getNoOfAdults() + "\"";
			  						jsonOutput += ",\"noOfChild\":\"" + accommodations.getNoOfChild() + "\"";
			  			    		jsonOutput += ",\"minOccupancy\":\"" + accommodations.getMinOccupancy() + "\"";
			  						jsonOutput += ",\"maxOccupancy\":\"" + accommodations.getMaxOccupancy() + "\"";
			  						jsonOutput += ",\"baseActualAmount\":\"" +  (actualBaseAmount==0 ? accommodations.getBaseAmount() : actualBaseAmount )+  "\"";
			  						jsonOutput += ",\"promoFirstPercent\":\"" + (promoFirstPercent==null? 0:promoFirstPercent)+ "\"";
			  						jsonOutput += ",\"promoFirstInr\":\"" + (promoFirstInr==null? 0:promoFirstInr)+ "\"";
			 						jsonOutput += ",\"promoSecondInr\":\"" + (promoSecondInr==null? 0:promoSecondInr)+ "\"";
			  						jsonOutput += ",\"promoSecondPercent\":\"" + (promoSecondPercent==null? 0:promoSecondPercent)+ "\"";
			  						jsonOutput += ",\"baseAmount\":\"" + + (totalAmount==0 ? accommodations.getBaseAmount() : totalAmount )+  "\"";
			  			    		jsonOutput += ",\"totalAmount\":\"" + + (totalAmount==0 ? accommodations.getBaseAmount() : totalAmount )+  "\"";
			  			    		jsonOutput += ",\"extraAdultAmount\":\"" + + (actualTotalAdultAmount==0 ? accommodations.getExtraAdult() : actualTotalAdultAmount )+  "\"";
			  			    		jsonOutput += ",\"extraChildAmount\":\"" + + (actualTotalChildAmount==0 ? accommodations.getExtraChild() : actualTotalChildAmount )+  "\"";
			  			    		jsonOutput += ",\"promotionFirstName\":\"" + (promotionFirstName==null? "None":promotionFirstName)+ "\"";
			 						jsonOutput += ",\"promotionSecondName\":\"" + (promotionSecondName==null? "None":promotionSecondName)+ "\"";
			  			    		jsonOutput += ",\"promotionId\":\"" + (promotionId==0? 0:promotionId) + "\"";
			  			    		jsonOutput += ",\"promoLastMinuteTimer\":\"" + (getDisplayHoursForTimer()==null ?"None" :getDisplayHoursForTimer()  )+ "\"";
			  						discountId=35;
			  						PropertyDiscountManager discountController=new PropertyDiscountManager();
			  						PropertyDiscount propertyDiscount=discountController.find(discountId);
			  			    		jsonOutput += ",\"discountPercent\":\"" + (propertyDiscount.getDiscountPercentage()==0.0? "0":propertyDiscount.getDiscountPercentage())+ "\"";
			  			    		jsonOutput += ",\"discountId\":\"" + (discountId==0? "0" :discountId) + "\"";
			  			    		
			  			    		
			  				     }
			  			    	 
			  			    	//jsonOutput += ",\"tax\":\"" + 0 + "\"";
			  					String jsonAccommAmenities ="";
			  					
			  					List<AccommodationAmenity> accommodationAmenityList =  accommodationAmenityController.list(accommodations.getAccommodationId());
			  					if(propertyAmenityList.size()>0)
			  					{
			  						for (AccommodationAmenity amenity : accommodationAmenityList) {
			  							if (!jsonAccommAmenities.equalsIgnoreCase(""))
			  								
			  								jsonAccommAmenities += ",{";
			  							else
			  								jsonAccommAmenities += "{";
			  							
			  							PmsAmenity pmsAmenity1 =ameController.find(amenity.getPmsAmenity().getAmenityId());
			  							jsonAccommAmenities += "\"DT_RowId\":\"" + pmsAmenity1.getAmenityId()+ "\"";					
			  							jsonAccommAmenities += ",\"amenityName\":\"" + pmsAmenity1.getAmenityName()+ "\"";
			  							jsonAccommAmenities += ",\"amenityId\":\"" + pmsAmenity1.getAmenityId()+ "\"";
			  							jsonAccommAmenities += ",\"icon\":\"" + iconPath + pmsAmenity1.getAmenityIcon()+ "\"";
			  							
			  							jsonAccommAmenities += "}";
			  						}
			  						
			  						jsonOutput += ",\"accommodationamenities\":[" + jsonAccommAmenities+ "]";
			  					}
			  					
			  					
			  					jsonOutput += ",\"available\":\"" + minimum + "\"";
			  					//jsonOutput += ",\"available\":\"" + (roomCount == null ? available.getAvailable() : (available.getAvailable()-roomCount.getRoomCount() )) + "\"";
			  				
			  			
		  				}
		  			}
		  			jsonOutput += "}";
		  			
		  			arllist.clear();
		  		
				
		    	}
		    	
		    }
		    response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
		    
	   }catch(Exception ex){
		   logger.error(ex);
		   ex.printStackTrace();
	   }
	   return null;
   
	}
   
   public String getNewAccommodationAvailability() throws IOException{


	   try{
		   	DateFormat format = new SimpleDateFormat("MMM d,yyyy", Locale.ENGLISH);
		    java.util.Date dateStart = format.parse(getStrStartDate());
		    java.util.Date dateEnd = format.parse(getStrEndDate());
		   
		    DecimalFormat df = new DecimalFormat("###.##");
		    DateFormat f = new SimpleDateFormat("EEEE");
		    
		    Calendar calCheckStart=Calendar.getInstance();
		    calCheckStart.setTime(dateStart);
		    java.util.Date checkInDate = calCheckStart.getTime();
		    this.arrivalDate=new Timestamp(checkInDate.getTime());
		   
		    if(getSourceTypeId()==null){
				   this.sourceTypeId=1;
			   }
		    Calendar calCheckEnd=Calendar.getInstance();
		    calCheckEnd.setTime(dateEnd);
		    java.util.Date checkOutDate = calCheckEnd.getTime();
		    this.departureDate=new Timestamp(checkOutDate.getTime());
			
			Map mapDate=new HashMap();  
			mapDate.put("arrivalDate", arrivalDate);
			mapDate.put("departureDate", departureDate); 
		      
			this.propertyId = getPropertyId();
			sessionMap.put("uloPropertyId",propertyId); 
			
			String checkIn = new SimpleDateFormat("MMM d,yyyy").format(getArrivalDate());
		    String checkOut = new SimpleDateFormat("MMM d,yyyy").format(getDepartureDate());
		    sessionMap.put("checkIn",checkIn);
			sessionMap.put("checkOut",checkOut);
			 
			
		    Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		   
			Calendar cal = Calendar.getInstance();
			cal.setTime(this.departureDate);
	        cal.add(Calendar.DATE, -1);
			String startDate = new SimpleDateFormat("yyyy-MM-dd").format(this.arrivalDate);
			String endDate = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
			DateTime start = DateTime.parse(startDate);
	        DateTime end = DateTime.parse(endDate);
	        java.util.Date arrival = start.toDate();
			java.util.Date departure = end.toDate();
			String iconPath  = "ulowebsite/images/icons/";
			
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("application/json");
			
			AccommodationAmenityManager accommodationAmenityController = new AccommodationAmenityManager();
			PmsPropertyManager propertyController = new PmsPropertyManager();		
			PropertyAccommodationManager accommodationManager = new PropertyAccommodationManager();
			PropertyAmenityManager amenityController = new PropertyAmenityManager();
			List<PropertyAmenity> propertyAmenityList;
			PropertyRateManager rateController = new PropertyRateManager();
			PropertyRateDetailManager detailController = new PropertyRateDetailManager();
			PmsAmenityManager ameController = new PmsAmenityManager();
			PromotionManager promotionController=new PromotionManager();
			PromotionDetailManager promotionDetailController=new PromotionDetailManager();
			PmsBookingManager bookingController=new PmsBookingManager();
			PropertyRatePlanDetailManager planDetailController=new PropertyRatePlanDetailManager();
			PropertyAccommodationInventoryManager inventoryController=new PropertyAccommodationInventoryManager();
	    	List<AccommodationRoom> listAccommodationRoom=null;
			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
			PropertyReviewsManager reviewController=new PropertyReviewsManager();
		    this.propertyList=propertyController.list(getPropertyId());
		    if(this.propertyList.size()>0 && !this.propertyList.isEmpty()){
		    	for(PmsProperty property:this.propertyList){

					
					
		  			
		  			if (!jsonOutput.equalsIgnoreCase(""))
		  				jsonOutput += ",{";
		  			else
		  				jsonOutput += "{";
		  			jsonOutput += "\"DT_RowId\":\"" + property.getPropertyId()+ "\"";
		  			jsonOutput += ",\"city\":\"" + property.getCity() + "\"";
		  			jsonOutput += ",\"address1\":\"" + property.getAddress1() + "\"";
		  			jsonOutput += ",\"address2\":\"" + property.getAddress2() + "\"";
		  			jsonOutput += ",\"propertyName\":\"" + (property.getPropertyName() == null ? "": property.getPropertyName().toUpperCase().trim())+ "\"";
		  			jsonOutput += ",\"propertyUrl\":\"" + (property.getPropertyUrl() == null ? "": property.getPropertyUrl().toLowerCase().trim())+ "\"";
//		  			jsonOutput += ",\"propertyStandardPolicy\":\"" + (property.getPropertyStandardPolicy() == null ? "None": property.getPropertyStandardPolicy().trim())+ "\"";
//		  			jsonOutput += ",\"propertyHotelPolicy\":\"" + (property.getPropertyHotelPolicy() == null ? "None": property.getPropertyHotelPolicy().trim())+ "\"";
//		  			jsonOutput += ",\"propertyCancellationPolicy\":\"" + (property.getPropertyCancellationPolicy() == null ? "None": property.getPropertyCancellationPolicy().trim())+ "\"";
		  			jsonOutput += ",\"payAtHotelStatus\":\"" + (property.getPayAtHotelStatus() == null ? false: property.getPayAtHotelStatus())+ "\"";
					jsonOutput += ",\"coupleStatus\":\"" + ( property.getCoupleIsActive() == null? false :  property.getCoupleIsActive() )+ "\"";
					jsonOutput += ",\"breakfastStatus\":\"" + ( property.getBreakfastIsActive() == null? false:  property.getBreakfastIsActive() )+ "\"";
					jsonOutput += ",\"hotelStatus\":\"" + ( property.getHotelIsActive() == null? false:  property.getHotelIsActive() )+ "\"";
					
		  			SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
		  			Timestamp FromDate=(Timestamp)mapDate.get("arrivalDate");
		  			Timestamp ToDate=(Timestamp)mapDate.get("departureDate");
		  			jsonOutput += ",\"arrivalDate\":\"" + format2.format(FromDate) + "\"";
		  			jsonOutput += ",\"departureDate\":\"" + format2.format(ToDate)+ "\"";
		  			
		  			String strFromDate=format2.format(FromDate);
		  			String strToDate=format2.format(ToDate);
		  	    	DateTime startdate = DateTime.parse(strFromDate);
		  	        DateTime enddate = DateTime.parse(strToDate);
		  	        java.util.Date fromdate = startdate.toDate();
		  	 		java.util.Date todate = enddate.toDate();
		  	 		 
		  			int diffInDays = (int) ((todate.getTime() - fromdate.getTime()) / (1000 * 60 * 60 * 24));
		  			
		  			List arllist=new ArrayList();
		  			List arllistAvl=new ArrayList();
		  			List arllistSold=new ArrayList();
		  			jsonOutput += ",\"diffDays\":\"" + diffInDays+ "\"";
		  			
		  			String jsonAmenities ="";
		  			propertyAmenityList =  amenityController.list(property.getPropertyId());
		  			if(propertyAmenityList.size()>0)
		  			{
		  				for (PropertyAmenity amenity : propertyAmenityList) {
		  					if (!jsonAmenities.equalsIgnoreCase(""))
		  						
		  						jsonAmenities += ",{";
		  					else
		  						jsonAmenities += "{";
		  					PropertyAmenity amentity1 = amenityController.find(amenity.getPropertyAmenityId());
		  				    String path = "ulowebsite/images/icons/";
		  					PmsAmenity pmsAmenity1 =ameController.find(amentity1.getPmsAmenity().getAmenityId());
		  					jsonAmenities += "\"DT_RowId\":\"" + amenity.getPropertyAmenityId()+ "\"";					
		  					jsonAmenities += ",\"amenityName\":\"" + pmsAmenity1.getAmenityName()+ "\"";
		  					jsonAmenities += ",\"amenityId\":\"" + pmsAmenity1.getAmenityId()+ "\"";
		                      jsonAmenities += ",\"icon\":\"" + path + pmsAmenity1.getAmenityIcon()+ "\"";
		               
		  					
		  					jsonAmenities += "}";
		  				}
		  				
		  				jsonOutput += ",\"amenities\":[" + jsonAmenities+ "]";
		  			}
		  			String jsonGuestReview="";
		  			String jsonReviews="";
					List<PropertyReviews> listReviews=reviewController.listReview(property.getPropertyId());
					if(listReviews.size()>0 && !listReviews.isEmpty()){
						for(PropertyReviews reviews:listReviews){
							
							if (!jsonReviews.equalsIgnoreCase(""))
								
								jsonReviews += ",{";
							else
								jsonReviews += "{";

							/*jsonReviews += "\"starCount\":\"" + reviews.getStarCount().intValue()+ "\"";	
							jsonReviews += ",\"reviewCount\":\"" + reviews.getReviewCount().intValue()+ "\"";
							Double averageReviews=reviews.getAverageReview();
							jsonReviews += ",\"averageReview\":\"" + df.format(averageReviews)+ "\"";*/
							
							jsonReviews += "\"starCount\":\"" + (reviews.getStarCount()==null?0:reviews.getStarCount().intValue())+ "\"";	
							jsonReviews += ",\"reviewCount\":\"" + (reviews.getReviewCount()==null?0:reviews.getReviewCount().intValue())+ "\"";	
							if(reviews.getAverageReview()!=null){
								Double averageReviews=reviews.getAverageReview();
								jsonReviews += ",\"averageReview\":\"" + df.format(averageReviews)+ "\"";
							}else{
								jsonReviews += ",\"averageReview\":\"" + df.format(0)+ "\"";
							}
							
							jsonReviews += ",\"tripadvisorStarCount\":\"" + (reviews.getTripadvisorStarCount()==null?0:reviews.getTripadvisorStarCount().intValue())+ "\"";	
							jsonReviews += ",\"tripadvisorReviewCount\":\"" + (reviews.getTripadvisorReviewCount()==null?0:reviews.getTripadvisorReviewCount().intValue())+ "\"";
							if(reviews.getTripadvisorAverageReview()!=null){
								Double taAverageReviews=reviews.getTripadvisorAverageReview();
								jsonReviews += ",\"tripadvisorAverageReview\":\"" + df.format(taAverageReviews)+ "\"";
							}else{
								jsonReviews += ",\"tripadvisorAverageReview\":\"" + df.format(0)+ "\"";
							}
							jsonReviews += ",\"googleReviewUrl\":\"" + (reviews.getGoogleReviewUrl()==null?"":reviews.getGoogleReviewUrl())+ "\"";	
							jsonReviews += ",\"tripadvisorReviewUrl\":\"" + (reviews.getTripadvisorReviewUrl()==null?"":reviews.getTripadvisorReviewUrl())+ "\"";	
							jsonReviews += ",\"propertyRating\":\"" + (reviews.getPropertyRating()==null?"None":reviews.getPropertyRating())+ "\"";
							jsonReviews += "}";
						}
						
					}else{
						if (!jsonReviews.equalsIgnoreCase(""))
							
							jsonReviews += ",{";
						else
							jsonReviews += "{";
						
						jsonReviews += "\"starCount\":\"" + 0+ "\"";	
						jsonReviews += ",\"reviewCount\":\"" + 0+ "\"";	
						jsonReviews += ",\"averageReview\":\"" + 0+ "\"";
						
						jsonReviews += ",\"tripadvisorStarCount\":\"" + 0 + "\"";	
						jsonReviews += ",\"tripadvisorReviewCount\":\"" + 0 + "\"";	
						jsonReviews += ",\"tripadvisorAverageReview\":\"" + 0+ "\"";
						
						jsonReviews += ",\"googleReviewUrl\":\"" + "None"+ "\"";	
						jsonReviews += ",\"tripadvisorReviewUrl\":\"" + "None"+ "\"";	
						jsonReviews += ",\"propertyRating\":\"" + "None"+ "\"";
						
						jsonReviews += "}";
					}
					
					jsonOutput += ",\"reviews\":[" + jsonReviews+ "]";
					
					
					PropertyGuestReviewManager guestReviewController=new PropertyGuestReviewManager();
					List<PropertyGuestReview> listGuestReviews=guestReviewController.listReview(property.getPropertyId());
					if(listGuestReviews.size()>0 && !listGuestReviews.isEmpty()){
						for(PropertyGuestReview reviews:listGuestReviews){
							if (!jsonGuestReview.equalsIgnoreCase(""))
								
								jsonGuestReview += ",{";
							else
								jsonGuestReview += "{";
							
							jsonGuestReview += "\"reviewerGuestName\":\"" + (reviews.getReviewGuestName()==null?"":reviews.getReviewGuestName())+ "\"";	
							jsonGuestReview += ",\"reviewerDescription\":\"" + (reviews.getReviewDescription()==null?"":reviews.getReviewDescription())+ "\"";	
							jsonGuestReview += ",\"reviewerCount\":\"" + (reviews.getReviewCount()==null?0:reviews.getReviewCount())+ "\"";
							jsonGuestReview += ",\"reviewerName\":\"" + (reviews.getReviews().getReviewerName()==null?"":reviews.getReviews().getReviewerName())+ "\"";
							jsonGuestReview += ",\"reviewerId\":\"" + (reviews.getReviews().getReviewerId()==null?"":reviews.getReviews().getReviewerId())+ "\"";
							jsonGuestReview += "}";
						}
					}else{
						if (!jsonGuestReview.equalsIgnoreCase(""))
							
							jsonGuestReview += ",{";
						else
							jsonGuestReview += "{";
						
						jsonGuestReview += "\"reviewerGuestName\":\"" + ""+ "\"";	
						jsonGuestReview += ",\"reviewerDescription\":\"" + ""+ "\"";	
						jsonGuestReview += ",\"reviewerCount\":\"" + 0+ "\"";
						jsonGuestReview += ",\"reviewerName\":\"" + ""+ "\"";
						jsonGuestReview += ",\"reviewerId\":\"" + "" + "\"";
						
						jsonGuestReview += "}";
					}
				
					jsonOutput += ",\"guestReviews\":[" + jsonGuestReview+ "]";
		  			
		  			String jsonAccommodation ="";
		  			List<PropertyAccommodation> accommodationsList =  accommodationManager.list(property.getPropertyId());
		  			int soldOutCount=0,availCount=0,promotionAccommId=0;
		  			if(accommodationsList.size()>0 && !accommodationsList.isEmpty())
		  			{
		  				for (PropertyAccommodation accommodation : accommodationsList) {
		  					int roomCount=0;
		  					listAccommodationRoom=accommodationController.listPropertyTotalRooms(property.getPropertyId(), accommodation.getAccommodationId());
		  					if(listAccommodationRoom.size()>0 && !listAccommodationRoom.isEmpty()){
		  						for(AccommodationRoom roomsList:listAccommodationRoom){
		  							 roomCount=(int) roomsList.getRoomCount();
		  							 long minimum = getAvailableCount(property.getPropertyId(),getArrivalDate(),getDepartureDate(),roomCount,accommodation.getAccommodationId());
		  							 if((int)minimum>0){
		  								 availCount++;
		  							 }else if((int)minimum==0){
		  								 soldOutCount++;
		  							 }
		  							 break;
		  						 }
		  					 }
		  					
		  					
		  					if (!jsonAccommodation.equalsIgnoreCase(""))
		  						
		  						jsonAccommodation += ",{";
		  					else
		  						jsonAccommodation += "{";
		  						jsonAccommodation += "\"accommodationId\":\"" + accommodation.getAccommodationId()+ "\"";
		  						jsonAccommodation += ",\"accommodationName\":\"" + accommodation.getAccommodationType().toUpperCase()+ "\"";
		  					
		  					jsonAccommodation += "}";
		  				}
		  				
		  				jsonOutput += ",\"jsonAccommodations\":[" + jsonAccommodation+ "]";
		  			}

		  			
		  			List<PmsPromotionDetails> listPromotionDetailCheck=null;
		  			Boolean promotionFlag=false,blnPromoFlag=false;
		  			String promotionType=null;
		  			List<PropertyAccommodation> accommodationList =  accommodationManager.list(property.getPropertyId(),getPropertyAccommodationId());
		  			if(accommodationList.size()>0)
		  			{
		  				for (PropertyAccommodation accommodations : accommodationList) {

			  			    listAccommodationRoom.clear(); 
			  		    	listAccommodationRoom=accommodationController.listPropertyTotalRooms(property.getPropertyId(), accommodations.getAccommodationId());
			  		    	List<DateTime> betweenAmount = DateUtil.getDateRange(start, end);
			  		    	double actualBaseAmount=0.0,actualTotalAmount=0.0,actualTotalAdultAmount=0.0,actualTotalChildAmount=0.0,
			  		    			actualAdultAmount=0.0,actualChildAmount=0.0,totalMinimumAmount=0,totalMaximumAmount=0,totalSellRate=0;
			  			    int countDate=0; 
			  			    double baseAmount=0.0,extraAdultAmount=0.0,extraChildAmount=0.0,totalBaseAmount=0.0;
			  			    int roomCount=0,availableCount=0,totalCount=0,discountId=0;
			  				if(listAccommodationRoom.size()>0 && !listAccommodationRoom.isEmpty()){
			  					for(AccommodationRoom roomsList:listAccommodationRoom){
			  						 roomCount=(int) roomsList.getRoomCount();
			  						 break;
			  					 }
			  				 }
			  				
			  				 ArrayList arlRateType=new ArrayList();
		  					 List<PropertyRatePlanDetail> listPlanDetails=planDetailController.listAccommodation(property.getPropertyId(), accommodations.getAccommodationId());
		  					 if(listPlanDetails.size()>0 && !listPlanDetails.isEmpty()){
		  			        	for(PropertyRatePlanDetail ratePlanDetails:listPlanDetails){
		  			        		arlRateType.add(ratePlanDetails.getRatePlan().getRatePlanType());
		  			        		arlRateType.add(ratePlanDetails.getRatePlan().getRatePlanSymbolType());
		  			        		arlRateType.add(ratePlanDetails.getTariffAmount());
		  			        	}
		  					 }
		  					 
							 List<DateTime> between = DateUtil.getDateRange(start, end);
							 SimpleDateFormat format1=new SimpleDateFormat("yyyy-MM-dd");
							 java.util.Date currentdate=new java.util.Date();
							 String strCurrentDate = new SimpleDateFormat("yyyy-MM-dd").format(currentdate);
							 java.util.Date dtecurdate = format1.parse(strCurrentDate);
		 				   	 Calendar calStart=Calendar.getInstance();
		 				   	 calStart.setTime(dtecurdate);
		 				   	 calStart.setTimeZone(TimeZone.getTimeZone("IST"));
		 				   	 String StartDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calStart.getTime());
		 				   	 java.util.Date curdate = format1.parse(StartDate);
		 				   	String jsonPrices="";
			  					for (DateTime d : betweenAmount)
			  				    {


							    	 long rmc = 0;
						   			 this.roomCnt = rmc;
						        	 int sold=0;
						        	 java.util.Date availDate = d.toDate();
									 PropertyAccommodationInventory inventoryCount = inventoryController.findInventoryCount(availDate,accommodations.getAccommodationId());
									 if(inventoryCount != null){
									 PmsAvailableRooms roomCount1 = bookingController.findBookingCount(availDate, accommodations.getAccommodationId(),inventoryCount.getInventoryId());
									 if(roomCount1.getRoomCount() == null) {
									 	String num1 = inventoryCount.getInventoryCount();
										PmsAvailableRooms bookingRoomCount=bookingController.findCount(availDate, accommodations.getAccommodationId());
										Integer totavailable=0;
										if(accommodations!=null){
											totavailable=accommodations.getNoOfUnits();
										}
										if(bookingRoomCount.getRoomCount()!=null){
											long lngRooms=bookingRoomCount.getRoomCount();
											long num2 = Long.parseLong(num1);
											this.roomCnt = num2;
											sold=(int) (lngRooms);
											int availRooms=0;
											availRooms=totavailable-sold;
											if(Integer.parseInt(num1)>availRooms){
												this.roomCnt = availRooms;	
											}else{
												this.roomCnt = num2;
											}
													
										}else{
											 long num2 = Long.parseLong(num1);
											 this.roomCnt = num2;
											 sold=(int) (rmc);
										}
									}else{		
										PmsAvailableRooms bookingRoomCount=bookingController.findCount(availDate, accommodations.getAccommodationId());
										int intRooms=0;
										Integer totavailable=0,availableRooms=0;
										if(accommodations!=null){
											totavailable=accommodations.getNoOfUnits();
										}
										if(bookingRoomCount.getRoomCount()!=null){
											long lngRooms=bookingRoomCount.getRoomCount();
											long num = roomCount1.getRoomCount();	
											intRooms=(int)lngRooms;
											String num1 = inventoryCount.getInventoryCount();
						 					long num2 = Long.parseLong(num1);
						 					availableRooms=totavailable-intRooms;
						 					if(num2>availableRooms){
						 						this.roomCnt = availableRooms;	 
						 					}else{
						 						this.roomCnt = num2-num;
						 					}
						 					long lngSold=bookingRoomCount.getRoomCount();
						   					sold=(int)lngSold;
										}else{
											long num = roomCount1.getRoomCount();	
											String num1 = inventoryCount.getInventoryCount();
											long num2 = Long.parseLong(num1);
						  					this.roomCnt = (num2-num);
						  					long lngSold=roomCount1.getRoomCount();
						   					sold=(int)lngSold;
										}
					
									}
													
								}else{
									  PmsAvailableRooms inventoryCounts = bookingController.findCount(availDate, accommodations.getAccommodationId());							
										if(inventoryCounts.getRoomCount() == null){								
											this.roomCnt = (accommodations.getNoOfUnits()-rmc);
											sold=(int)rmc;
										}
										else{
											this.roomCnt = (accommodations.getNoOfUnits()-inventoryCounts.getRoomCount());	
											long lngSold=inventoryCounts.getRoomCount();
				   							sold=(int)lngSold;
										}
									}
														
									double totalRooms=0,intSold=0;
									totalRooms=(double)accommodations.getNoOfUnits();
									intSold=(double)sold;
									double occupancy=0.0;
									occupancy=intSold/totalRooms;
									double sellrate=0.0,otarate=0.0;
									Integer occupancyRating=(int) Math.round(occupancy * 100);
								    int rateCount=0,rateIdCount=0;
								    double minAmount=0,maxAmount=0;
								    List<PropertyRate> dateList =  rateController.listAllDates(property.getPropertyId(),accommodations.getAccommodationId(),getSourceTypeId(),d.toDate());
									if(!dateList.isEmpty()){
										for(PropertyRate rates : dateList) {
								    		int propertyRateId = rates.getPropertyRateId();
							    			List<PropertyRateDetail> rateDetailList =   detailController.listMinMaxRateDetail(propertyRateId,f.format(d.toDate()).toLowerCase());
				   				    		 if(rateDetailList.size()>0 && !rateDetailList.isEmpty()){
				   				    			 if(rateCount<1){
				   				    				 for (PropertyRateDetail rate : rateDetailList){
				   				    					 minAmount=rate.getMinimumBaseAmount();
				   				    					 maxAmount=rate.getMaximumBaseAmount();
				   				    					 extraAdultAmount=rate.getExtraAdult();
				   				    					 extraChildAmount=rate.getExtraChild();
				   				    				    if(occupancyRating==0){
					   				 						sellrate = minAmount;
					   				 					}else if(occupancyRating>0 && occupancyRating<=30){
					   				 			        	sellrate = minAmount;
					   				 			        }else if(occupancyRating>=31 && occupancyRating<=60){
					   				 			        	sellrate = minAmount+(minAmount*10/100);
					   				 			        }else if(occupancyRating>=61 && occupancyRating<=80){
					   				 			        	sellrate = minAmount+(minAmount*15/100);
					   				 			        }else if(occupancyRating>=81 && occupancyRating<=100){
					   				 			        	sellrate = maxAmount;
					   				 			        }
				   				    				    baseAmount+=  sellrate;
				       				    			 }
				   				    				 rateCount++;
				   				    				rateIdCount++;
				   				    			 }
				   				    		 }
								    	}if(rateIdCount < 1){
			   				    			 //if(rateIdCount<1){
			   				    				if(accommodations.getMinimumBaseAmount()!=null){
			   				    					minAmount=accommodations.getMinimumBaseAmount();	 
			   				    				 }
			   				    				 if(accommodations.getMaximumBaseAmount()!=null){
			   				    					maxAmount=accommodations.getMaximumBaseAmount();	 
			   				    				 }
			   				    				 extraAdultAmount=accommodations.getExtraAdult();
		  				    					 extraChildAmount=accommodations.getExtraChild();
			   				    				if(occupancyRating==0){
			   				 						sellrate = minAmount;
			   				 					}else if(occupancyRating>0 && occupancyRating<=30){
			   				 			        	sellrate = minAmount;
			   				 			        }else if(occupancyRating>=31 && occupancyRating<=60){
			   				 			        	sellrate = minAmount+(minAmount*10/100);
			   				 			        }else if(occupancyRating>=61 && occupancyRating<=80){
			   				 			        	sellrate = minAmount+(minAmount*15/100);
			   				 			        }else if(occupancyRating>=81 && occupancyRating<=100){
			   				 			        	sellrate = maxAmount;
			   				 			        }
			   				    				baseAmount+= sellrate;
			   				    				//rateIdCount++;
			   				    				//rateCount++;
			   				    			 //}
			       				    	 }
								     }else{

								    	 if(accommodations.getMinimumBaseAmount()!=null){
					    					minAmount=accommodations.getMinimumBaseAmount();	 
					    				 }
					    				 if(accommodations.getMaximumBaseAmount()!=null){
					    					maxAmount=accommodations.getMaximumBaseAmount();	 
					    				 }
					    				 extraAdultAmount=accommodations.getExtraAdult();
					    				 extraChildAmount=accommodations.getExtraChild();
					    				 
					    				if(occupancyRating==0){
					 						sellrate = minAmount;
					 					}else if(occupancyRating>0 && occupancyRating<=30){
					 			        	sellrate = minAmount;
					 			        }else if(occupancyRating>=31 && occupancyRating<=60){
					 			        	sellrate = minAmount+(minAmount*10/100);
					 			        }else if(occupancyRating>=61 && occupancyRating<=80){
					 			        	sellrate = minAmount+(minAmount*15/100);
					 			        }else if(occupancyRating>=81 && occupancyRating<=100){
					 			        	sellrate = maxAmount;
					 			        }
					    				baseAmount+= sellrate;
					    			 
								     }
									totalMinimumAmount+=minAmount;
									totalMaximumAmount+=maxAmount;
									totalSellRate+=sellrate;
									if (!jsonPrices.equalsIgnoreCase(""))
										jsonPrices += ",{";
									else
										jsonPrices += "{";
									
									
									jsonPrices += "\"inventoryRating\":\"" + (occupancyRating)+ "\"";
									jsonPrices += ",\"minimumAmount\":\"" + (minAmount)+ "\"";
									jsonPrices += ",\"maximumAmount\":\"" + + (maxAmount)+  "\"";
									jsonPrices += ",\"sellrate\":\"" + + (sellrate)+  "\"";	
									jsonPrices += ",\"extraAdult\":\"" + (extraAdultAmount)+ "\"";
									jsonPrices += ",\"extraChild\":\"" + (extraChildAmount)+  "\"";
									jsonPrices += ",\"date\":\"" +  (format1.format(availDate))+  "\"";
								
									jsonPrices += "}";
									
							    	countDate++;
							    
			  				    }
			  					
			  					jsonOutput += ",\"rates\":[" + jsonPrices+ "]";
								
								double variationAmount=0,variationRating=0;
								variationAmount=totalMaximumAmount-totalSellRate;
								variationRating=variationAmount/totalMaximumAmount*100;
								String promotionFirstName=null;
								/*if(variationRating>9){
									promotionFirstName=String.valueOf(Math.round(variationRating))+" % OFF";	
								}else{
									promotionFirstName="Flat "+String.valueOf(Math.round(variationAmount))+" OFF";
								}*/
//								promotionFirstName=String.valueOf(Math.round(variationRating))+" % OFF";
								if(variationRating>0){
									promotionFirstName=String.valueOf(Math.round(variationRating))+" % OFF";	
								}
								double dblRatePlanAmount=0,dblEPAmount=0,dblTotalEPAmount=0;
						        Iterator iterRatePlan=arlRateType.iterator();
						        String strType=null,strSymbol=null;
				            	while(iterRatePlan.hasNext()){
				            		strType=(String)iterRatePlan.next();
				            		strSymbol=(String)iterRatePlan.next();
				            		dblRatePlanAmount=(Double)iterRatePlan.next();
				            		
				            		if(strType.equalsIgnoreCase("EP")){
				                		if(strSymbol.equalsIgnoreCase("plus")){
				                			dblEPAmount=baseAmount+dblRatePlanAmount;	
				                		}else{
				                			dblEPAmount=baseAmount-dblRatePlanAmount;
				                		}
				            		}
				            		
				            	}
								totalAmount=baseAmount;
								dblTotalEPAmount=dblEPAmount;
								if(dblTotalEPAmount==0){
									dblTotalEPAmount=baseAmount;
								}
								/*if(countDate>1){
									//siva update
//									actualAdultAmount=extraAdultAmount/countDate;
//									actualChildAmount=extraChildAmount/countDate;
									actualAdultAmount=extraAdultAmount;
									actualChildAmount=extraChildAmount;
								}else{
									actualAdultAmount=extraAdultAmount;
									actualChildAmount=extraChildAmount;
								}*/
							
								actualAdultAmount=extraAdultAmount;
								actualChildAmount=extraChildAmount;
								
								totalAmount=Math.round(totalAmount);
								actualTotalAdultAmount=Math.round(actualAdultAmount);
								actualTotalChildAmount=Math.round(actualChildAmount);
								
								if(totalMinimumAmount==0){
									totalBaseAmount=0;
								}
								if(totalMinimumAmount>0){
									totalBaseAmount=totalMinimumAmount;
								}
			  					
			  					
			 			       
			  					
			  					long minimum = getAvailableCount(property.getPropertyId(),getArrivalDate(),getDepartureDate(),roomCount,accommodations.getAccommodationId());
			  			    	 
			  					
			  					 PropertyTaxeManager propertytaxController = new PropertyTaxeManager();
			  	  					 double taxe =  totalAmount/diffInDays;
			  	  					
			  						 PropertyTaxe tax = propertytaxController.find(taxe);
			  						 double taxes = Math.round(totalAmount * tax.getTaxPercentage() / 100  ) ;
			  	 			         jsonOutput += ",\"tax\":\"" + taxes + "\"";
			  	 			         jsonOutput += ",\"roomTax\":\"" + taxes + "\"";
			  	 			         jsonOutput += ",\"taxPercentage\":\"" + tax.getTaxPercentage() + "\"";
			  			    	 if(minimum==0){
			  			    		 jsonOutput += ",\"available\":\"" + minimum+ "\"";
			  			    		jsonOutput += ",\"minimumAmount\":\"" + + (totalMinimumAmount==0 ? totalBaseAmount : totalMinimumAmount )+  "\"";
			  			    		jsonOutput += ",\"maximumAmount\":\"" + + (totalMaximumAmount==0 ? totalBaseAmount : totalMaximumAmount )+  "\"";
			  			    		 jsonOutput += ",\"baseActualAmount\":\"" + + (totalMaximumAmount==0 ? totalBaseAmount : totalMaximumAmount )+  "\"";
						    		 jsonOutput += ",\"baseAmount\":\"" + + (totalAmount==0 ? totalBaseAmount : totalAmount )+  "\"";
						    		 jsonOutput += ",\"totalAmount\":\"" + + (totalAmount==0 ? totalBaseAmount : totalAmount )+  "\"";
						    		 jsonOutput += ",\"cpAmount\":\"" + + (totalAmount==0 ? totalBaseAmount : totalAmount )+  "\"";
						    		 jsonOutput += ",\"epAmount\":\"" + + (dblTotalEPAmount)+  "\"";
						    		 jsonOutput += ",\"promotionFirstName\":\"" + (promotionFirstName==null? "None":promotionFirstName)+ "\"";
			  			    		 jsonOutput += ",\"accommodationName\":\"" + accommodations.getAccommodationType().toUpperCase()+ "\"";
			  						 jsonOutput += ",\"accommodationId\":\"" + accommodations.getAccommodationId()+ "\"";
			  						 	
			  				     }else{
			  				    	jsonOutput += ",\"accommodationName\":\"" + accommodations.getAccommodationType().toUpperCase()+ "\"";
			  						jsonOutput += ",\"accommodationId\":\"" + accommodations.getAccommodationId()+ "\"";
			  			    		jsonOutput += ",\"available\":\"" + minimum+ "\"";
			  			    		jsonOutput += ",\"minimumAmount\":\"" + + (totalMinimumAmount==0 ? totalBaseAmount : totalMinimumAmount )+  "\"";
			  			    		jsonOutput += ",\"maximumAmount\":\"" + + (totalMaximumAmount==0 ? totalBaseAmount : totalMaximumAmount )+  "\"";
			  			    		jsonOutput += ",\"maxAdult\":\"" + accommodations.getMaxOccupancy()+ "\"";
			  			    		jsonOutput += ",\"maxChild\":\"" + 0 + "\"";
			  			    		jsonOutput += ",\"adultsCount\":\"" + 1 + "\"";
			  						jsonOutput += ",\"childCount\":\"" + 0 + "\"";
			  						jsonOutput += ",\"rooms\":\"" + 1 + "\"";
			  						jsonOutput += ",\"noOfAdults\":\"" + accommodations.getNoOfAdults() + "\"";
			  						jsonOutput += ",\"noOfChild\":\"" + accommodations.getNoOfChild() + "\"";
			  			    		jsonOutput += ",\"minOccupancy\":\"" + accommodations.getMinOccupancy() + "\"";
			  						jsonOutput += ",\"maxOccupancy\":\"" + accommodations.getMaxOccupancy() + "\"";
			  						jsonOutput += ",\"baseActualAmount\":\"" +  (totalMaximumAmount==0 ? totalBaseAmount : totalMaximumAmount )+  "\"";
									jsonOutput += ",\"baseAmount\":\"" + + (totalAmount==0 ? totalBaseAmount : totalAmount )+  "\"";
						    		jsonOutput += ",\"totalAmount\":\"" + + (totalAmount==0 ? totalBaseAmount : totalAmount )+  "\"";
						    		jsonOutput += ",\"cpAmount\":\"" + + (totalAmount==0 ? totalBaseAmount : totalAmount )+  "\"";
						    		jsonOutput += ",\"epAmount\":\"" + + (dblTotalEPAmount)+  "\"";
						    		jsonOutput += ",\"promotionFirstName\":\"" + (promotionFirstName==null? "None":promotionFirstName)+ "\"";
						    		
			  			    		jsonOutput += ",\"extraAdultAmount\":\"" + + (actualTotalAdultAmount==0 ? accommodations.getExtraAdult() : actualTotalAdultAmount )+  "\"";
			  			    		jsonOutput += ",\"extraChildAmount\":\"" + + (actualTotalChildAmount==0 ? accommodations.getExtraChild() : actualTotalChildAmount )+  "\"";
			  						discountId=35;
			  						PropertyDiscountManager discountController=new PropertyDiscountManager();
			  						PropertyDiscount propertyDiscount=discountController.find(discountId);
			  			    		jsonOutput += ",\"discountPercent\":\"" + (propertyDiscount.getDiscountPercentage()==0.0? "0":propertyDiscount.getDiscountPercentage())+ "\"";
			  			    		jsonOutput += ",\"discountId\":\"" + (discountId==0? "0" :discountId) + "\"";
			  			    		
			  			    		
			  				     }
			  			    	 
			  			    	//jsonOutput += ",\"tax\":\"" + 0 + "\"";
			  					String jsonAccommAmenities ="";
			  					
			  					List<AccommodationAmenity> accommodationAmenityList =  accommodationAmenityController.list(accommodations.getAccommodationId());
			  					if(propertyAmenityList.size()>0)
			  					{
			  						for (AccommodationAmenity amenity : accommodationAmenityList) {
			  							if (!jsonAccommAmenities.equalsIgnoreCase(""))
			  								
			  								jsonAccommAmenities += ",{";
			  							else
			  								jsonAccommAmenities += "{";
			  							
			  							PmsAmenity pmsAmenity1 =ameController.find(amenity.getPmsAmenity().getAmenityId());
			  							jsonAccommAmenities += "\"DT_RowId\":\"" + pmsAmenity1.getAmenityId()+ "\"";					
			  							jsonAccommAmenities += ",\"amenityName\":\"" + pmsAmenity1.getAmenityName()+ "\"";
			  							jsonAccommAmenities += ",\"amenityId\":\"" + pmsAmenity1.getAmenityId()+ "\"";
			  							jsonAccommAmenities += ",\"icon\":\"" + iconPath + pmsAmenity1.getAmenityIcon()+ "\"";
			  							
			  							jsonAccommAmenities += "}";
			  						}
			  						
			  						jsonOutput += ",\"accommodationamenities\":[" + jsonAccommAmenities+ "]";
			  					}
			  					
			  					
//			  					jsonOutput += ",\"available\":\"" + minimum + "\"";
			  					//jsonOutput += ",\"available\":\"" + (roomCount == null ? available.getAvailable() : (available.getAvailable()-roomCount.getRoomCount() )) + "\"";
			  				
			  			
		  				}
		  			}
		  			jsonOutput += "}";
		  			
		  			arllist.clear();
		  		
				
		    	}
		    	
		    }
		    response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
		    
	   }catch(Exception ex){
		   logger.error(ex);
		   ex.printStackTrace();
	   }
	   return null;
   
	
   }
   
   public String getNewUloFrontAvaility() throws IOException{

	   try{
		   	DateFormat format = new SimpleDateFormat("MMM d,yyyy", Locale.ENGLISH);
		    java.util.Date dateStart = format.parse(getStartDate());
		    java.util.Date dateEnd = format.parse(getEndDate());
		   
		    DecimalFormat df = new DecimalFormat("###.##");
		    DateFormat f = new SimpleDateFormat("EEEE");
		    
		    Calendar calCheckStart=Calendar.getInstance();
		    calCheckStart.setTime(dateStart);
		    java.util.Date checkInDate = calCheckStart.getTime();
		    this.arrivalDate=new Timestamp(checkInDate.getTime());
		   if(getSourceTypeId()==null){
			   this.sourceTypeId=1;
		   }
		    
		    Calendar calCheckEnd=Calendar.getInstance();
		    calCheckEnd.setTime(dateEnd);
		    java.util.Date checkOutDate = calCheckEnd.getTime();
		    this.departureDate=new Timestamp(checkOutDate.getTime());
			
			Map mapDate=new HashMap();  
			mapDate.put("arrivalDate", arrivalDate);
			mapDate.put("departureDate", departureDate); 
		      
			this.propertyId = getPropertyId();
			sessionMap.put("uloPropertyId",propertyId); 
			
			String checkIn = new SimpleDateFormat("MMM d,yyyy").format(getArrivalDate());
		    String checkOut = new SimpleDateFormat("MMM d,yyyy").format(getDepartureDate());
		    sessionMap.put("checkIn",checkIn);
			sessionMap.put("checkOut",checkOut);
			 
			
		    Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		   
			Calendar cal = Calendar.getInstance();
			cal.setTime(this.departureDate);
	        cal.add(Calendar.DATE, -1);
			String startDate = new SimpleDateFormat("yyyy-MM-dd").format(this.arrivalDate);
			String endDate = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
			DateTime start = DateTime.parse(startDate);
	        DateTime end = DateTime.parse(endDate);
	        java.util.Date arrival = start.toDate();
			java.util.Date departure = end.toDate();
			PropertyAccommodationInventoryManager inventoryController= new PropertyAccommodationInventoryManager();
			String iconPath  = "ulowebsite/images/icons/";
			
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("application/json");
			
			AccommodationAmenityManager accommodationAmenityController = new AccommodationAmenityManager();
			PmsPropertyManager propertyController = new PmsPropertyManager();		
			PropertyAccommodationManager accommodationManager = new PropertyAccommodationManager();
			PropertyAmenityManager amenityController = new PropertyAmenityManager();
			List<PropertyAmenity> propertyAmenityList;
			PropertyTypeManager typeController =  new PropertyTypeManager();
			PropertyRateManager rateController = new PropertyRateManager();
			PropertyRateDetailManager detailController = new PropertyRateDetailManager();
			PmsAmenityManager ameController = new PmsAmenityManager();
			PromotionManager promotionController=new PromotionManager();
			PromotionDetailManager promotionDetailController=new PromotionDetailManager();
			PmsBookingManager bookingController=new PmsBookingManager();
			
			
			HashMap<Double,Integer > mapAmountAccommId=new HashMap<Double, Integer>();
			HashMap<Double,Integer > mapSoldOutAmountAccommId=new HashMap<Double, Integer>();
			
			ArrayList<Integer> arlSortPropertyId=new ArrayList<Integer>();
			HashMap<Integer, Integer> mapAvailablePropertyId=new HashMap<Integer, Integer>();
			
	    	List<AccommodationRoom> listAccommodationRoom=null;
			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
			PropertyReviewsManager reviewController=new PropertyReviewsManager();
		    int availRoomCount=0,soldRoomCount=0;
		    this.propertyList=propertyController.list(getPropertyId());
		    if(this.propertyList.size()>0 && !this.propertyList.isEmpty()){
		    	for(PmsProperty property:this.propertyList){

					
					
		  			
		  			if (!jsonOutput.equalsIgnoreCase(""))
		  				jsonOutput += ",{";
		  			else
		  				jsonOutput += "{";
		  			jsonOutput += "\"DT_RowId\":\"" + property.getPropertyId()+ "\"";
		  			jsonOutput += ",\"city\":\"" + property.getCity() + "\"";
		  			jsonOutput += ",\"address1\":\"" + property.getAddress1() + "\"";
		  			jsonOutput += ",\"address2\":\"" + property.getAddress2() + "\"";
		  			jsonOutput += ",\"propertyName\":\"" + (property.getPropertyName() == null ? "": property.getPropertyName().toUpperCase().trim())+ "\"";
		  			jsonOutput += ",\"paynowDiscount\":\"" + (property.getPaynowDiscount() == null ? "": property.getPaynowDiscount())+ "\"";
		  			jsonOutput += ",\"propertyUrl\":\"" + (property.getPropertyUrl() == null ? "": property.getPropertyUrl().toLowerCase().trim())+ "\"";
//		  			jsonOutput += ",\"propertyStandardPolicy\":\"" + (property.getPropertyStandardPolicy() == null ? "None": property.getPropertyStandardPolicy().trim())+ "\"";
//		  			jsonOutput += ",\"propertyHotelPolicy\":\"" + (property.getPropertyHotelPolicy() == null ? "None": property.getPropertyHotelPolicy().trim())+ "\"";
//		  			jsonOutput += ",\"propertyCancellationPolicy\":\"" + (property.getPropertyCancellationPolicy() == null ? "None": property.getPropertyCancellationPolicy().trim())+ "\"";
		  			jsonOutput += ",\"payAtHotelStatus\":\"" + (property.getPayAtHotelStatus() == null ? false: property.getPayAtHotelStatus())+ "\"";
					jsonOutput += ",\"coupleStatus\":\"" + ( property.getCoupleIsActive() == null? false :  property.getCoupleIsActive() )+ "\"";
					jsonOutput += ",\"breakfastStatus\":\"" + ( property.getBreakfastIsActive() == null? false:  property.getBreakfastIsActive() )+ "\"";
					jsonOutput += ",\"hotelStatus\":\"" + ( property.getHotelIsActive() == null? false:  property.getHotelIsActive() )+ "\"";
					
		  			SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
		  			Timestamp FromDate=(Timestamp)mapDate.get("arrivalDate");
		  			Timestamp ToDate=(Timestamp)mapDate.get("departureDate");
		  			jsonOutput += ",\"arrivalDate\":\"" + format2.format(FromDate) + "\"";
		  			jsonOutput += ",\"departureDate\":\"" + format2.format(ToDate)+ "\"";
		  			
		  			jsonOutput += ",\"startDate\":\"" + format.format(FromDate) + "\"";
		  			jsonOutput += ",\"endDate\":\"" + format.format(ToDate)+ "\"";
		  			
		  			String strFromDate=format2.format(FromDate);
		  			String strToDate=format2.format(ToDate);
		  	    	DateTime startdate = DateTime.parse(strFromDate);
		  	        DateTime enddate = DateTime.parse(strToDate);
		  	        java.util.Date fromdate = startdate.toDate();
		  	 		java.util.Date todate = enddate.toDate();
		  	 		 
		  			int diffInDays = (int) ((todate.getTime() - fromdate.getTime()) / (1000 * 60 * 60 * 24));
		  			
		  			List arllist=new ArrayList();
		  			List arllistAvl=new ArrayList();
		  			List arllistSold=new ArrayList();
		  			jsonOutput += ",\"diffDays\":\"" + diffInDays+ "\"";
		  			
		  			String jsonAmenities ="";
		  			propertyAmenityList =  amenityController.list(property.getPropertyId());
		  			if(propertyAmenityList.size()>0)
		  			{
		  				for (PropertyAmenity amenity : propertyAmenityList) {
		  					if (!jsonAmenities.equalsIgnoreCase(""))
		  						
		  						jsonAmenities += ",{";
		  					else
		  						jsonAmenities += "{";
		  					PropertyAmenity amentity1 = amenityController.find(amenity.getPropertyAmenityId());
		  					String path = "contents/images/icons/";
		  					//String path = "ulowebsite/images/icons/";
		  					PmsAmenity pmsAmenity1 =ameController.find(amentity1.getPmsAmenity().getAmenityId());
		  					jsonAmenities += "\"DT_RowId\":\"" + amenity.getPropertyAmenityId()+ "\"";					
		  					jsonAmenities += ",\"amenityName\":\"" + pmsAmenity1.getAmenityName()+ "\"";
		  					jsonAmenities += ",\"amenityId\":\"" + pmsAmenity1.getAmenityId()+ "\"";
		                      jsonAmenities += ",\"icon\":\"" + path + pmsAmenity1.getAmenityIcon()+ "\"";
		               
		  					
		  					jsonAmenities += "}";
		  				}
		  				
		  				jsonOutput += ",\"amenities\":[" + jsonAmenities+ "]";
		  			}
		  			String jsonGuestReview="";
		  			String jsonReviews="";
					List<PropertyReviews> listReviews=reviewController.listReview(property.getPropertyId());
					if(listReviews.size()>0 && !listReviews.isEmpty()){
						for(PropertyReviews reviews:listReviews){
							
							if (!jsonReviews.equalsIgnoreCase(""))
								
								jsonReviews += ",{";
							else
								jsonReviews += "{";

							/*jsonReviews += "\"starCount\":\"" + reviews.getStarCount().intValue()+ "\"";	
							jsonReviews += ",\"reviewCount\":\"" + reviews.getReviewCount().intValue()+ "\"";
							Double averageReviews=reviews.getAverageReview();
							jsonReviews += ",\"averageReview\":\"" + df.format(averageReviews)+ "\"";*/
							
							jsonReviews += "\"starCount\":\"" + (reviews.getStarCount()==null?0:reviews.getStarCount().intValue())+ "\"";	
							jsonReviews += ",\"reviewCount\":\"" + (reviews.getReviewCount()==null?0:reviews.getReviewCount().intValue())+ "\"";	
							if(reviews.getAverageReview()!=null){
								Double averageReviews=reviews.getAverageReview();
								jsonReviews += ",\"averageReview\":\"" + df.format(averageReviews)+ "\"";
							}else{
								jsonReviews += ",\"averageReview\":\"" + df.format(0)+ "\"";
							}
							
							jsonReviews += ",\"tripadvisorStarCount\":\"" + (reviews.getTripadvisorStarCount()==null?0:reviews.getTripadvisorStarCount().intValue())+ "\"";	
							jsonReviews += ",\"tripadvisorReviewCount\":\"" + (reviews.getTripadvisorReviewCount()==null?0:reviews.getTripadvisorReviewCount().intValue())+ "\"";
							if(reviews.getTripadvisorAverageReview()!=null){
								Double taAverageReviews=reviews.getTripadvisorAverageReview();
								jsonReviews += ",\"tripadvisorAverageReview\":\"" + df.format(taAverageReviews)+ "\"";
							}else{
								jsonReviews += ",\"tripadvisorAverageReview\":\"" + df.format(0)+ "\"";
							}
							jsonReviews += ",\"googleReviewUrl\":\"" + (reviews.getGoogleReviewUrl()==null?"":reviews.getGoogleReviewUrl())+ "\"";	
							jsonReviews += ",\"tripadvisorReviewUrl\":\"" + (reviews.getTripadvisorReviewUrl()==null?"":reviews.getTripadvisorReviewUrl())+ "\"";	
							jsonReviews += ",\"propertyRating\":\"" + (reviews.getPropertyRating()==null?"None":reviews.getPropertyRating())+ "\"";
							jsonReviews += "}";
						}
						
					}else{
						if (!jsonReviews.equalsIgnoreCase(""))
							
							jsonReviews += ",{";
						else
							jsonReviews += "{";
						
						jsonReviews += "\"starCount\":\"" + 0+ "\"";	
						jsonReviews += ",\"reviewCount\":\"" + 0+ "\"";	
						jsonReviews += ",\"averageReview\":\"" + 0+ "\"";
						
						jsonReviews += ",\"tripadvisorStarCount\":\"" + 0 + "\"";	
						jsonReviews += ",\"tripadvisorReviewCount\":\"" + 0 + "\"";	
						jsonReviews += ",\"tripadvisorAverageReview\":\"" + 0+ "\"";
						
						jsonReviews += ",\"googleReviewUrl\":\"" + "None"+ "\"";	
						jsonReviews += ",\"tripadvisorReviewUrl\":\"" + "None"+ "\"";	
						jsonReviews += ",\"propertyRating\":\"" + "None"+ "\"";
						
						jsonReviews += "}";
					}
					
					jsonOutput += ",\"reviews\":[" + jsonReviews+ "]";
					
					
					PropertyGuestReviewManager guestReviewController=new PropertyGuestReviewManager();
					List<PropertyGuestReview> listGuestReviews=guestReviewController.listReview(property.getPropertyId());
					if(listGuestReviews.size()>0 && !listGuestReviews.isEmpty()){
						for(PropertyGuestReview reviews:listGuestReviews){
							if (!jsonGuestReview.equalsIgnoreCase(""))
								
								jsonGuestReview += ",{";
							else
								jsonGuestReview += "{";
							
							jsonGuestReview += "\"reviewerGuestName\":\"" + (reviews.getReviewGuestName()==null?"":reviews.getReviewGuestName())+ "\"";	
							jsonGuestReview += ",\"reviewerDescription\":\"" + (reviews.getReviewDescription()==null?"":reviews.getReviewDescription())+ "\"";	
							jsonGuestReview += ",\"reviewerCount\":\"" + (reviews.getReviewCount()==null?0:reviews.getReviewCount())+ "\"";
							jsonGuestReview += ",\"reviewerName\":\"" + (reviews.getReviews().getReviewerName()==null?"":reviews.getReviews().getReviewerName())+ "\"";
							jsonGuestReview += ",\"reviewerId\":\"" + (reviews.getReviews().getReviewerId()==null?"":reviews.getReviews().getReviewerId())+ "\"";
							jsonGuestReview += "}";
						}
					}else{
						if (!jsonGuestReview.equalsIgnoreCase(""))
							
							jsonGuestReview += ",{";
						else
							jsonGuestReview += "{";
						
						jsonGuestReview += "\"reviewerGuestName\":\"" + ""+ "\"";	
						jsonGuestReview += ",\"reviewerDescription\":\"" + ""+ "\"";	
						jsonGuestReview += ",\"reviewerCount\":\"" + 0+ "\"";
						jsonGuestReview += ",\"reviewerName\":\"" + ""+ "\"";
						jsonGuestReview += ",\"reviewerId\":\"" + "" + "\"";
						
						jsonGuestReview += "}";
					}
				
					jsonOutput += ",\"guestReviews\":[" + jsonGuestReview+ "]";
		  			
		  			String jsonAccommodation ="";
		  			List<PropertyAccommodation> accommodationsList =  accommodationManager.list(property.getPropertyId());
		  			int soldOutCount=0,availCount=0,promotionAccommId=0;
		  			if(accommodationsList.size()>0 && !accommodationsList.isEmpty())
		  			{
		  				for (PropertyAccommodation accommodation : accommodationsList) {
		  					int roomCount=0;
		  					listAccommodationRoom=accommodationController.listPropertyTotalRooms(property.getPropertyId(), accommodation.getAccommodationId());
		  					if(listAccommodationRoom.size()>0 && !listAccommodationRoom.isEmpty()){
		  						for(AccommodationRoom roomsList:listAccommodationRoom){
		  							 roomCount=(int) roomsList.getRoomCount();
		  							 long minimum = getAvailableCount(property.getPropertyId(),getArrivalDate(),getDepartureDate(),roomCount,accommodation.getAccommodationId());
		  							 if((int)minimum>0){
		  								 availCount++;
		  							 }else if((int)minimum==0){
		  								 soldOutCount++;
		  							 }
		  							 break;
		  						 }
		  					 }
		  					
		  					
		  					if (!jsonAccommodation.equalsIgnoreCase(""))
		  						
		  						jsonAccommodation += ",{";
		  					else
		  						jsonAccommodation += "{";
		  						jsonAccommodation += "\"accommodationId\":\"" + accommodation.getAccommodationId()+ "\"";
		  						jsonAccommodation += ",\"accommodationName\":\"" + accommodation.getAccommodationType().toUpperCase()+ "\"";
		  					
		  					jsonAccommodation += "}";
		  				}
		  				
		  				jsonOutput += ",\"jsonAccommodations\":[" + jsonAccommodation+ "]";
		  			}

		  			
		  			List<PmsPromotionDetails> listPromotionDetailCheck=null;
		  			Boolean blnAvailable=false,blnSoldout=false,firstPromotionFlag=false,secondPromotionFlag=false,promotionFlag=false,blnPromoFlag=false;
		  			int arlListTotalAccomm=0,soldOutTotalAccomm=0,accommCount=0;
		  			String promotionType=null;
		  			ArrayList<String> arlListPromoType=new ArrayList<String>();
		  			List<PropertyAccommodation> accommodationList =  accommodationManager.list(property.getPropertyId());
		  			if(accommodationList.size()>0)
		  			{
		  				for (PropertyAccommodation accommodation : accommodationList) {
		  					int dateCount=0;
		  					double amount=0.0,extraAdultAmount=0.0,extraChildAmount=0.0,totalSmartPriceAmount=0.0;
		  					int roomCount=0,availableCount=0,totalCount=0;
		  				    double dblPercentCount=0.0;
		  	    			listAccommodationRoom=accommodationController.listPropertyTotalRooms(property.getPropertyId(), accommodation.getAccommodationId());
		  					 
		  					if(listAccommodationRoom.size()>0 && !listAccommodationRoom.isEmpty()){
		  						
		  						for(AccommodationRoom roomsList:listAccommodationRoom){
		  							roomCount=(int) roomsList.getRoomCount();
		  							if(roomCount>0){
		  								long minimum = getAvailableCount(property.getPropertyId(),getArrivalDate(),getDepartureDate(),roomCount,accommodation.getAccommodationId());
		  								if((int)minimum>0){
		  									blnAvailable=true;
		  									blnSoldout=false;
		  									arlListTotalAccomm++;
		  								}else if((int)minimum==0){
		  									blnSoldout=true;
		  									blnAvailable=false;
		  									soldOutTotalAccomm++;
		  								}
		  							}
		  							 break;
		  						 }
		  					 }
		  					 java.util.Date currentdate=new java.util.Date();
		  					 SimpleDateFormat format1=new SimpleDateFormat("yyyy-MM-dd");
		  					 String strCurrentDate = new SimpleDateFormat("yyyy-MM-dd").format(currentdate);
		  					 java.util.Date dtecurdate = format1.parse(strCurrentDate);
		 				   	 Calendar calStart=Calendar.getInstance();
		 				   	 calStart.setTime(dtecurdate);
		 				   	 calStart.setTimeZone(TimeZone.getTimeZone("IST"));
		 				   	 String StartDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calStart.getTime());
		 				   	 java.util.Date curdate = format1.parse(StartDate);
		  					 List<DateTime> between = DateUtil.getDateRange(start, end);
		  					 
		  					
		  				     for (DateTime d : between)
		  				     {

						    	 long rmc = 0;
					   			 this.roomCnt = rmc;
					        	 int sold=0;
					        	 java.util.Date availDate = d.toDate();
								 PropertyAccommodationInventory inventoryCount = inventoryController.findInventoryCount(availDate,accommodation.getAccommodationId());
								 if(inventoryCount != null){
								 PmsAvailableRooms roomCount1 = bookingController.findBookingCount(availDate, accommodation.getAccommodationId(),inventoryCount.getInventoryId());
								 if(roomCount1.getRoomCount() == null) {
								 	String num1 = inventoryCount.getInventoryCount();
									PmsAvailableRooms bookingRoomCount=bookingController.findCount(availDate, accommodation.getAccommodationId());
									PropertyAccommodation accomm=accommodationController.find(accommodation.getAccommodationId());
									Integer totavailable=0;
									if(accommodation!=null){
										totavailable=accommodation.getNoOfUnits();
									}
									if(bookingRoomCount.getRoomCount()!=null){
										long lngRooms=bookingRoomCount.getRoomCount();
										long num2 = Long.parseLong(num1);
										this.roomCnt = num2;
										sold=(int) (lngRooms);
										int availRooms=0;
										availRooms=totavailable-sold;
										if(Integer.parseInt(num1)>availRooms){
											this.roomCnt = availRooms;	
										}else{
											this.roomCnt = num2;
										}
												
									}else{
										 long num2 = Long.parseLong(num1);
										 this.roomCnt = num2;
										 sold=(int) (rmc);
									}
								}else{		
									PmsAvailableRooms bookingRoomCount=bookingController.findCount(availDate, accommodation.getAccommodationId());
									int intRooms=0;
									Integer totavailable=0,availableRooms=0;
									if(accommodation!=null){
										totavailable=accommodation.getNoOfUnits();
									}
									if(bookingRoomCount.getRoomCount()!=null){
										long lngRooms=bookingRoomCount.getRoomCount();
										long num = roomCount1.getRoomCount();	
										intRooms=(int)lngRooms;
										String num1 = inventoryCount.getInventoryCount();
					 					long num2 = Long.parseLong(num1);
					 					availableRooms=totavailable-intRooms;
					 					if(num2>availableRooms){
					 						this.roomCnt = availableRooms;	 
					 					}else{
					 						this.roomCnt = num2-num;
					 					}
					 					long lngSold=bookingRoomCount.getRoomCount();
					   					sold=(int)lngSold;
									}else{
										long num = roomCount1.getRoomCount();	
										String num1 = inventoryCount.getInventoryCount();
										long num2 = Long.parseLong(num1);
					  					this.roomCnt = (num2-num);
					  					long lngSold=roomCount1.getRoomCount();
					   					sold=(int)lngSold;
									}
				
								}
												
							}else{
								  PmsAvailableRooms inventoryCounts = bookingController.findCount(availDate, accommodation.getAccommodationId());							
									if(inventoryCounts.getRoomCount() == null){								
										this.roomCnt = (accommodation.getNoOfUnits()-rmc);
										sold=(int)rmc;
									}
									else{
										this.roomCnt = (accommodation.getNoOfUnits()-inventoryCounts.getRoomCount());	
										long lngSold=inventoryCounts.getRoomCount();
			   							sold=(int)lngSold;
									}
							}
												
							double totalRooms=0,intSold=0;
							totalRooms=(double)accommodation.getNoOfUnits();
							intSold=(double)sold;
							double occupancy=0.0;
							occupancy=intSold/totalRooms;
							double sellrate=0.0,otarate=0.0;
							Integer occupancyRating=(int) Math.round(occupancy * 100);
								    int rateCount=0,rateIdCount=0;
								    double minAmount=0,maxAmount=0;
								    List<PropertyRate> dateList =  rateController.listAllDates(property.getPropertyId(),accommodation.getAccommodationId(),this.sourceTypeId,d.toDate());
									if(dateList.size()>0 && !dateList.isEmpty()){
										for(PropertyRate rates : dateList) {
											int propertyRateId = rates.getPropertyRateId();
											List<PropertyRateDetail> rateDetailList =   detailController.listMinMaxRateDetail(propertyRateId,f.format(d.toDate()).toLowerCase());
			   				    		 	if(rateDetailList.size()>0 && !rateDetailList.isEmpty()){
			   				    		 		if(rateCount<1){
			   				    		 			for (PropertyRateDetail rate : rateDetailList){
				   				    					 minAmount=rate.getMinimumBaseAmount();
				   				    					 maxAmount=rate.getMaximumBaseAmount();
				   				    				    if(occupancyRating==0){
					   				 						sellrate = minAmount;
					   				 					}else if(occupancyRating>0 && occupancyRating<=30){
					   				 			        	sellrate = minAmount;
					   				 			        }else if(occupancyRating>=31 && occupancyRating<=60){
					   				 			        	sellrate = minAmount+(minAmount*10/100);
					   				 			        }else if(occupancyRating>=61 && occupancyRating<=80){
					   				 			        	sellrate = minAmount+(minAmount*15/100);
					   				 			        }else if(occupancyRating>=81 && occupancyRating<=100){
					   				 			        	sellrate = maxAmount;
					   				 			        }
				   				    					 amount+=  sellrate;
			   				    		 			}
			   				    		 			rateCount++;
			   				    		 		rateIdCount++;
			   				    		 		}
			   				    		 }
							    	}

		   				    			 if(rateIdCount<1){
		   				    				 if(accommodation.getMinimumBaseAmount()!=null){
		   				    					minAmount=accommodation.getMinimumBaseAmount();	 
		   				    				 }
		   				    				 if(accommodation.getMaximumBaseAmount()!=null){
		   				    					maxAmount=accommodation.getMaximumBaseAmount();	 
		   				    				 }
		   				    				 
		   				    				if(occupancyRating==0){
		   				 						sellrate = minAmount;
		   				 					}else if(occupancyRating>0 && occupancyRating<=30){
		   				 			        	sellrate = minAmount;
		   				 			        }else if(occupancyRating>=31 && occupancyRating<=60){
		   				 			        	sellrate = minAmount+(minAmount*10/100);
		   				 			        }else if(occupancyRating>=61 && occupancyRating<=80){
		   				 			        	sellrate = minAmount+(minAmount*15/100);
		   				 			        }else if(occupancyRating>=81 && occupancyRating<=100){
		   				 			        	sellrate = maxAmount;
		   				 			        }
		   				    				 amount+= sellrate;
		   				    				 rateIdCount++;
		   				    				 rateCount++;
		   				    			 }
		       				    	 	
							     }else{

							    	 if(accommodation.getMinimumBaseAmount()!=null){
				    					minAmount=accommodation.getMinimumBaseAmount();	 
				    				 }
				    				 if(accommodation.getMaximumBaseAmount()!=null){
				    					maxAmount=accommodation.getMaximumBaseAmount();	 
				    				 }
				    				 
				    				if(occupancyRating==0){
				 						sellrate = minAmount;
				 					}else if(occupancyRating>0 && occupancyRating<=30){
				 			        	sellrate = minAmount;
				 			        }else if(occupancyRating>=31 && occupancyRating<=60){
				 			        	sellrate = minAmount+(minAmount*10/100);
				 			        }else if(occupancyRating>=61 && occupancyRating<=80){
				 			        	sellrate = minAmount+(minAmount*15/100);
				 			        }else if(occupancyRating>=81 && occupancyRating<=100){
				 			        	sellrate = maxAmount;
				 			        }
				    				 amount+= sellrate;
				    			 
							     }
						    	 dateCount++;
						    	 if(dateCount==diffInDays){
						    		 if(blnAvailable){
					    				mapAmountAccommId.put(amount,accommodation.getAccommodationId());
						      			arllist.add(amount);
				      				 }else if(blnSoldout){
				      					mapSoldOutAmountAccommId.put(amount,accommodation.getAccommodationId());
						      			arllist.add(amount);
							    	 }
						    	 }
						     
		  				     }
		  				   if(blnAvailable){
						    	 arllistAvl.add(amount);
						     }else if(!blnAvailable){
						    	 arllistSold.add(amount);
						     }
						     accommCount++;
		  				}
		  			}
		  			//end of accommodation
		  			double totalAmount=0;
		  			boolean blnJSONContent=false;
		  			Collections.sort(arllist);
		  			int arlListSize=0,accommId=0,intTotalAccomm=0;
		  		     intTotalAccomm=accommodationList.size();
		  		   if(intTotalAccomm==accommCount){
				    	 if(intTotalAccomm==soldOutCount){
						     arlListSize=arllist.size();
						     if(soldOutCount==arlListSize){
						    	 Collections.sort(arllist);
						    	 for(int i=0;i<arlListSize;i++){
							    	 totalAmount=(Double) arllist.get(i);
							    	 if((Double) arllist.get(0)==(Double) arllist.get(i)){
							    		 if(blnAvailable){
							    			 accommId=mapAmountAccommId.get(totalAmount);
								    		 blnJSONContent=true;
							    		 }else{
							    			 accommId=mapSoldOutAmountAccommId.get(totalAmount);
								    		 blnJSONContent=true;
							    		 }
							    		 break;
							    	 }else{
							    		 
							    	 }
							     }
						     }
				    	 }else{
						     arlListSize=arllist.size();
						     Collections.sort(arllist);
					    	 for(int i=0;i<arlListSize;i++){
					    		 if(arllistAvl.contains((Double) arllist.get(i))){
					    			 if((Double) arllist.get(0)==(Double) arllist.get(i)){
						    			 totalAmount=(Double) arllist.get(i);
						    			 accommId=mapAmountAccommId.get(totalAmount);
							    		 blnJSONContent=true;
					    			 }else{
					    				 totalAmount=(Double) arllist.get(i);
						    			 accommId=mapAmountAccommId.get(totalAmount);
							    		 blnJSONContent=true;
					    			 }
					    			 break;
					    		 }
						     }
				    	 }
				     }
		  			if(blnJSONContent){
		  			    listAccommodationRoom.clear(); 
		  		    	listAccommodationRoom=accommodationController.listPropertyTotalRooms(property.getPropertyId(), accommId);
		  		    	List<DateTime> betweenAmount = DateUtil.getDateRange(start, end);
		  		    	double actualPromoBaseAmount=0.0,actualBaseAmount=0.0,actualTotalAmount=0.0,actualTotalAdultAmount=0.0,actualTotalChildAmount=0.0,
		  		    			actualAdultAmount=0.0,actualChildAmount=0.0;
		  			    int countDate=0; 
		  			    double baseAmount=0.0,extraAdultAmount=0.0,extraChildAmount=0.0,totalBaseAmount=0.0,totalMinimumAmount=0,totalMaximumAmount=0,totalSellRate=0;
		  			    int roomCount=0,availableCount=0,totalCount=0,discountId=0;
		  				if(listAccommodationRoom.size()>0 && !listAccommodationRoom.isEmpty()){
		  					for(AccommodationRoom roomsList:listAccommodationRoom){
		  						 roomCount=(int) roomsList.getRoomCount();
		  						 break;
		  					 }
		  				 }
		  				PropertyRatePlanDetailManager planDetailController=new PropertyRatePlanDetailManager();
		  				ArrayList arlRateType=new ArrayList();
						 List<PropertyRatePlanDetail> listPlanDetails=planDetailController.listAccommodation(property.getPropertyId(), accommId);
						 if(listPlanDetails.size()>0 && !listPlanDetails.isEmpty()){
				        	for(PropertyRatePlanDetail ratePlanDetails:listPlanDetails){
				        		arlRateType.add(ratePlanDetails.getRatePlan().getRatePlanType());
				        		arlRateType.add(ratePlanDetails.getRatePlan().getRatePlanSymbolType());
				        		arlRateType.add(ratePlanDetails.getTariffAmount());
				        	}
						 }
						 
						 List<DateTime> between = DateUtil.getDateRange(start, end);
						 SimpleDateFormat format1=new SimpleDateFormat("yyyy-MM-dd");
						 java.util.Date currentdate=new java.util.Date();
						 String strCurrentDate = new SimpleDateFormat("yyyy-MM-dd").format(currentdate);
						 java.util.Date dtecurdate = format1.parse(strCurrentDate);
	 				   	 Calendar calStart=Calendar.getInstance();
	 				   	 calStart.setTime(dtecurdate);
	 				   	 calStart.setTimeZone(TimeZone.getTimeZone("IST"));
	 				   	 String StartDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calStart.getTime());
	 				   	 java.util.Date curdate = format1.parse(StartDate);
	 				   	 
		  				List<PropertyAccommodation> listAccommodation =  accommodationManager.list(property.getPropertyId(),accommId);
		  				String jsonPrices="";
		  				for (PropertyAccommodation accommodations : listAccommodation) {
		  					for (DateTime d : betweenAmount)
		  				    {


						    	 long rmc = 0;
					   			 this.roomCnt = rmc;
					        	 int sold=0;
					        	 java.util.Date availDate = d.toDate();
								 PropertyAccommodationInventory inventoryCount = inventoryController.findInventoryCount(availDate,accommodations.getAccommodationId());
								 if(inventoryCount != null){
								 PmsAvailableRooms roomCount1 = bookingController.findBookingCount(availDate, accommodations.getAccommodationId(),inventoryCount.getInventoryId());
								 if(roomCount1.getRoomCount() == null) {
								 	String num1 = inventoryCount.getInventoryCount();
									PmsAvailableRooms bookingRoomCount=bookingController.findCount(availDate, accommodations.getAccommodationId());
									Integer totavailable=0;
									if(accommodations!=null){
										totavailable=accommodations.getNoOfUnits();
									}
									if(bookingRoomCount.getRoomCount()!=null){
										long lngRooms=bookingRoomCount.getRoomCount();
										long num2 = Long.parseLong(num1);
										this.roomCnt = num2;
										sold=(int) (lngRooms);
										int availRooms=0;
										availRooms=totavailable-sold;
										if(Integer.parseInt(num1)>availRooms){
											this.roomCnt = availRooms;	
										}else{
											this.roomCnt = num2;
										}
												
									}else{
										 long num2 = Long.parseLong(num1);
										 this.roomCnt = num2;
										 sold=(int) (rmc);
									}
								}else{		
									PmsAvailableRooms bookingRoomCount=bookingController.findCount(availDate, accommodations.getAccommodationId());
									int intRooms=0;
									Integer totavailable=0,availableRooms=0;
									if(accommodations!=null){
										totavailable=accommodations.getNoOfUnits();
									}
									if(bookingRoomCount.getRoomCount()!=null){
										long lngRooms=bookingRoomCount.getRoomCount();
										long num = roomCount1.getRoomCount();	
										intRooms=(int)lngRooms;
										String num1 = inventoryCount.getInventoryCount();
					 					long num2 = Long.parseLong(num1);
					 					availableRooms=totavailable-intRooms;
					 					if(num2>availableRooms){
					 						this.roomCnt = availableRooms;	 
					 					}else{
					 						this.roomCnt = num2-num;
					 					}
					 					long lngSold=bookingRoomCount.getRoomCount();
					   					sold=(int)lngSold;
									}else{
										long num = roomCount1.getRoomCount();	
										String num1 = inventoryCount.getInventoryCount();
										long num2 = Long.parseLong(num1);
					  					this.roomCnt = (num2-num);
					  					long lngSold=roomCount1.getRoomCount();
					   					sold=(int)lngSold;
									}
				
								}
												
							}else{
								  PmsAvailableRooms inventoryCounts = bookingController.findCount(availDate, accommodations.getAccommodationId());							
									if(inventoryCounts.getRoomCount() == null){								
										this.roomCnt = (accommodations.getNoOfUnits()-rmc);
										sold=(int)rmc;
									}
									else{
										this.roomCnt = (accommodations.getNoOfUnits()-inventoryCounts.getRoomCount());	
										long lngSold=inventoryCounts.getRoomCount();
			   							sold=(int)lngSold;
									}
								}
													
								double totalRooms=0,intSold=0;
								totalRooms=(double)accommodations.getNoOfUnits();
								intSold=(double)sold;
								double occupancy=0.0;
								occupancy=intSold/totalRooms;
								double sellrate=0.0,otarate=0.0;
								Integer occupancyRating=(int) Math.round(occupancy * 100);
							    int rateCount=0,rateIdCount=0;
							    double minAmount=0,maxAmount=0;
							    List<PropertyRate> dateList =  rateController.listAllDates(property.getPropertyId(),accommodations.getAccommodationId(),getSourceTypeId(),d.toDate());
								if(!dateList.isEmpty()){
									for(PropertyRate rates : dateList) {
							    		int propertyRateId = rates.getPropertyRateId();
						    			List<PropertyRateDetail> rateDetailList =   detailController.listMinMaxRateDetail(propertyRateId,f.format(d.toDate()).toLowerCase());
						    			if(rateDetailList.size()>0 && !rateDetailList.isEmpty()){
			   				    			 if(rateCount<1){
			   				    				 for (PropertyRateDetail rate : rateDetailList){
			   				    					 minAmount=rate.getMinimumBaseAmount();
			   				    					 maxAmount=rate.getMaximumBaseAmount();
			   				    					 extraAdultAmount=rate.getExtraAdult();
			   				    					 extraChildAmount=rate.getExtraChild();
			   				    				    if(occupancyRating==0){
				   				 						sellrate = minAmount;
				   				 					}else if(occupancyRating>0 && occupancyRating<=30){
				   				 			        	sellrate = minAmount;
				   				 			        }else if(occupancyRating>=31 && occupancyRating<=60){
				   				 			        	sellrate = minAmount+(minAmount*10/100);
				   				 			        }else if(occupancyRating>=61 && occupancyRating<=80){
				   				 			        	sellrate = minAmount+(minAmount*15/100);
				   				 			        }else if(occupancyRating>=81 && occupancyRating<=100){
				   				 			        	sellrate = maxAmount;
				   				 			        }
			   				    				    baseAmount+=  sellrate;
			       				    			 }
			   				    				 rateCount++;
			   				    				 rateIdCount++;
			   				    			 }
			   				    		 } 
							    	}
									if(rateIdCount < 1){
		   				    			 //if(rateIdCount<1){
		   				    				if(accommodations.getMinimumBaseAmount()!=null){
		   				    					minAmount=accommodations.getMinimumBaseAmount();	 
		   				    				 }
		   				    				 if(accommodations.getMaximumBaseAmount()!=null){
		   				    					maxAmount=accommodations.getMaximumBaseAmount();	 
		   				    				 }
		   				    				 extraAdultAmount=accommodations.getExtraAdult();
	  				    					 extraChildAmount=accommodations.getExtraChild();
		   				    				if(occupancyRating==0){
		   				 						sellrate = minAmount;
		   				 					}else if(occupancyRating>0 && occupancyRating<=30){
		   				 			        	sellrate = minAmount;
		   				 			        }else if(occupancyRating>=31 && occupancyRating<=60){
		   				 			        	sellrate = minAmount+(minAmount*10/100);
		   				 			        }else if(occupancyRating>=61 && occupancyRating<=80){
		   				 			        	sellrate = minAmount+(minAmount*15/100);
		   				 			        }else if(occupancyRating>=81 && occupancyRating<=100){
		   				 			        	sellrate = maxAmount;
		   				 			        }
		   				    				baseAmount+= sellrate;
		   				    				rateIdCount++;
		   				    				rateCount++;
		   				    			 //}
		       				    	 }
									
							     }
								else{

							    	 if(accommodations.getMinimumBaseAmount()!=null){
				    					minAmount=accommodations.getMinimumBaseAmount();	 
				    				 }
				    				 if(accommodations.getMaximumBaseAmount()!=null){
				    					maxAmount=accommodations.getMaximumBaseAmount();	 
				    				 }
				    				 extraAdultAmount=accommodations.getExtraAdult();
				    				 extraChildAmount=accommodations.getExtraChild();
				    				 
				    				if(occupancyRating==0){
				 						sellrate = minAmount;
				 					}else if(occupancyRating>0 && occupancyRating<=30){
				 			        	sellrate = minAmount;
				 			        }else if(occupancyRating>=31 && occupancyRating<=60){
				 			        	sellrate = minAmount+(minAmount*10/100);
				 			        }else if(occupancyRating>=61 && occupancyRating<=80){
				 			        	sellrate = minAmount+(minAmount*15/100);
				 			        }else if(occupancyRating>=81 && occupancyRating<=100){
				 			        	sellrate = maxAmount;
				 			        }
				    				baseAmount+= sellrate;
				    			 
							     }
								totalMinimumAmount+=minAmount;
								totalMaximumAmount+=maxAmount;
								totalSellRate+=sellrate;
								if (!jsonPrices.equalsIgnoreCase(""))
									jsonPrices += ",{";
								else
									jsonPrices += "{";
								
								
								jsonPrices += "\"inventoryRating\":\"" + (occupancyRating)+ "\"";
								jsonPrices += ",\"minimumAmount\":\"" + (minAmount)+ "\"";
								jsonPrices += ",\"maximumAmount\":\"" + + (maxAmount)+  "\"";
								jsonPrices += ",\"sellrate\":\"" + + (sellrate)+  "\"";	
								jsonPrices += ",\"extraAdult\":\"" + (extraAdultAmount)+ "\"";
								jsonPrices += ",\"extraChild\":\"" + (extraChildAmount)+  "\"";
								jsonPrices += ",\"date\":\"" +  (format1.format(availDate))+  "\"";
							
								jsonPrices += "}";
								
						    	countDate++;
						    
		  				    }
		  					
		  					jsonOutput += ",\"rates\":[" + jsonPrices+ "]";
		  					
		  					double variationAmount=0,variationRating=0;
							variationAmount=totalMaximumAmount-totalSellRate;
							variationRating=variationAmount/totalMaximumAmount*100;
							String promotionFirstName=null;
							/*if(variationRating>9){
								promotionFirstName=String.valueOf(Math.round(variationRating))+" % OFF";	
							}else{
								promotionFirstName="Flat "+String.valueOf(Math.round(variationAmount))+" OFF";
							}*/
							if(variationRating>0){
								promotionFirstName=String.valueOf(Math.round(variationRating))+" % OFF";	
							}
							double dblRatePlanAmount=0,dblEPAmount=0,dblTotalEPAmount=0;
					        Iterator iterRatePlan=arlRateType.iterator();
					        String strType=null,strSymbol=null;
			            	while(iterRatePlan.hasNext()){
			            		strType=(String)iterRatePlan.next();
			            		strSymbol=(String)iterRatePlan.next();
			            		dblRatePlanAmount=(Double)iterRatePlan.next();
			            		
			            		if(strType.equalsIgnoreCase("EP")){
			                		if(strSymbol.equalsIgnoreCase("plus")){
			                			dblEPAmount=baseAmount+dblRatePlanAmount;	
			                		}else{
			                			dblEPAmount=baseAmount-dblRatePlanAmount;
			                		}
			            		}
			            		
			            	}
			            	
							totalAmount=baseAmount;
							dblTotalEPAmount=dblEPAmount;
							if(dblTotalEPAmount==0){
								dblTotalEPAmount=baseAmount;
							}
							/*if(countDate>1){
								//siva update
//								actualAdultAmount=extraAdultAmount/countDate;
//								actualChildAmount=extraChildAmount/countDate;
								actualAdultAmount=extraAdultAmount;
								actualChildAmount=extraChildAmount;
							}else{
								actualAdultAmount=extraAdultAmount;
								actualChildAmount=extraChildAmount;
							}*/
		  					
							actualAdultAmount=extraAdultAmount;
							actualChildAmount=extraChildAmount;
							
		  					totalAmount=Math.round(totalAmount);
		  					actualTotalAdultAmount=Math.round(actualAdultAmount);
		  					actualTotalChildAmount=Math.round(actualChildAmount);
		  					
		  					if(totalMinimumAmount==0){
								totalBaseAmount=0;
							}
							if(totalMinimumAmount>0){
								totalBaseAmount=totalMinimumAmount;
							}
		  					
		  					long minimum = getAvailableCount(property.getPropertyId(),getArrivalDate(),getDepartureDate(),roomCount,accommId);
		  			    	
		  					 PropertyTaxeManager propertytaxController = new PropertyTaxeManager();
		  	  					 double taxe =  totalAmount/diffInDays;
		  	  					
		  						 PropertyTaxe tax = propertytaxController.find(taxe);
		  						 double taxes = Math.round(totalAmount * tax.getTaxPercentage() / 100  ) ;
		  	 			         jsonOutput += ",\"tax\":\"" + taxes + "\"";
		  	 			         jsonOutput += ",\"roomTax\":\"" + taxes + "\"";
		  	 			         jsonOutput += ",\"taxPercentage\":\"" + tax.getTaxPercentage() + "\"";
		  			    	 if(minimum==0){
		  			    		 jsonOutput += ",\"available\":\"" + minimum+ "\"";
		  			    		jsonOutput += ",\"minimumAmount\":\"" + + (totalMinimumAmount==0 ? totalBaseAmount : totalMinimumAmount )+  "\"";
		  			    		jsonOutput += ",\"maximumAmount\":\"" + + (totalMaximumAmount==0 ? totalBaseAmount : totalMaximumAmount )+  "\"";
		  			    		 jsonOutput += ",\"baseActualAmount\":\"" + + (totalMaximumAmount==0 ? totalBaseAmount : totalMaximumAmount )+  "\"";
		  			    		jsonOutput += ",\"baseAmount\":\"" + + (totalAmount==0 ? totalBaseAmount : totalAmount )+  "\"";
					    		 jsonOutput += ",\"totalAmount\":\"" + + (totalAmount==0 ? totalBaseAmount : totalAmount )+  "\"";
					    		 jsonOutput += ",\"cpAmount\":\"" + + (totalAmount==0 ? totalBaseAmount : totalAmount )+  "\"";
					    		 jsonOutput += ",\"epAmount\":\"" + + (dblTotalEPAmount)+  "\"";
					    		 jsonOutput += ",\"promotionFirstName\":\"" + (promotionFirstName==null? "None":promotionFirstName)+ "\"";
		  			    		 jsonOutput += ",\"accommodationName\":\"" + accommodations.getAccommodationType().toUpperCase()+ "\"";
		  						 jsonOutput += ",\"accommodationId\":\"" + accommodations.getAccommodationId()+ "\"";
		  				     }else{
		  				    	jsonOutput += ",\"accommodationName\":\"" + accommodations.getAccommodationType().toUpperCase()+ "\"";
		  						jsonOutput += ",\"accommodationId\":\"" + accommodations.getAccommodationId()+ "\"";
		  			    		jsonOutput += ",\"available\":\"" + minimum+ "\"";
		  			    		jsonOutput += ",\"minimumAmount\":\"" + + (totalMinimumAmount==0 ? totalBaseAmount : totalMinimumAmount )+  "\"";
		  			    		jsonOutput += ",\"maximumAmount\":\"" + + (totalMaximumAmount==0 ? totalBaseAmount : totalMaximumAmount )+  "\"";
		  			    		jsonOutput += ",\"maxAdult\":\"" + accommodations.getMaxOccupancy()+ "\"";
		  			    		jsonOutput += ",\"maxChild\":\"" + 0 + "\"";
		  			    		jsonOutput += ",\"adultsCount\":\"" + 1 + "\"";
		  						jsonOutput += ",\"childCount\":\"" + 0 + "\"";
		  						jsonOutput += ",\"rooms\":\"" + 1 + "\"";
		  						jsonOutput += ",\"noOfAdults\":\"" + accommodations.getNoOfAdults() + "\"";
		  						jsonOutput += ",\"noOfChild\":\"" + accommodations.getNoOfChild() + "\"";
		  			    		jsonOutput += ",\"minOccupancy\":\"" + accommodations.getMinOccupancy() + "\"";
		  						jsonOutput += ",\"maxOccupancy\":\"" + accommodations.getMaxOccupancy() + "\"";
		  						jsonOutput += ",\"baseActualAmount\":\"" +  (totalMaximumAmount==0 ? totalBaseAmount : totalMaximumAmount )+  "\"";
								jsonOutput += ",\"baseAmount\":\"" + + (totalAmount==0 ? totalBaseAmount : totalAmount )+  "\"";
					    		jsonOutput += ",\"totalAmount\":\"" + + (totalAmount==0 ? totalBaseAmount : totalAmount )+  "\"";
					    		jsonOutput += ",\"cpAmount\":\"" + + (totalAmount==0 ? totalBaseAmount : totalAmount )+  "\"";
					    		jsonOutput += ",\"epAmount\":\"" + + (dblTotalEPAmount)+  "\"";
					    		jsonOutput += ",\"promotionFirstName\":\"" + (promotionFirstName==null? "None":promotionFirstName)+ "\"";
		  						
		  			    		jsonOutput += ",\"discountBaseAmount\":\"" + + (totalAmount==0 ? totalBaseAmount : totalAmount )+  "\"";
		  			    		jsonOutput += ",\"extraAdultAmount\":\"" + + (actualTotalAdultAmount==0 ? accommodations.getExtraAdult() : actualTotalAdultAmount )+  "\"";
		  			    		jsonOutput += ",\"extraChildAmount\":\"" + + (actualTotalChildAmount==0 ? accommodations.getExtraChild() : actualTotalChildAmount )+  "\"";
		  						discountId=35;
		  						PropertyDiscountManager discountController=new PropertyDiscountManager();
		  						PropertyDiscount propertyDiscount=discountController.find(discountId);
		  			    		jsonOutput += ",\"discountPercent\":\"" + (propertyDiscount.getDiscountPercentage()==0.0? "0":propertyDiscount.getDiscountPercentage())+ "\"";
		  			    		jsonOutput += ",\"discountName\":\"" + propertyDiscount.getDiscountName()+ "\"";
		  			    		jsonOutput += ",\"discountId\":\"" + (discountId==0? "0" :discountId) + "\"";
		  			    		
		  			    		
		  				     }
		  			    	 
		  			    	//jsonOutput += ",\"tax\":\"" + 0 + "\"";
		  					String jsonAccommAmenities ="";
		  					
		  					List<AccommodationAmenity> accommodationAmenityList =  accommodationAmenityController.list(accommodations.getAccommodationId());
		  					if(propertyAmenityList.size()>0)
		  					{
		  						for (AccommodationAmenity amenity : accommodationAmenityList) {
		  							if (!jsonAccommAmenities.equalsIgnoreCase(""))
		  								
		  								jsonAccommAmenities += ",{";
		  							else
		  								jsonAccommAmenities += "{";
		  							
		  							PmsAmenity pmsAmenity1 =ameController.find(amenity.getPmsAmenity().getAmenityId());
		  							jsonAccommAmenities += "\"DT_RowId\":\"" + pmsAmenity1.getAmenityId()+ "\"";					
		  							jsonAccommAmenities += ",\"amenityName\":\"" + pmsAmenity1.getAmenityName()+ "\"";
		  							jsonAccommAmenities += ",\"amenityId\":\"" + pmsAmenity1.getAmenityId()+ "\"";
		  							jsonAccommAmenities += ",\"icon\":\"" + iconPath + pmsAmenity1.getAmenityIcon()+ "\"";
		  							
		  							jsonAccommAmenities += "}";
		  						}
		  						
		  						jsonOutput += ",\"accommodationamenities\":[" + jsonAccommAmenities+ "]";
		  					}
		  					
		  					
		  					//jsonOutput += ",\"available\":\"" + minimum + "\"";
		  					//jsonOutput += ",\"available\":\"" + (roomCount == null ? available.getAvailable() : (available.getAvailable()-roomCount.getRoomCount() )) + "\"";
		  				}
		  		     
		  			}
		  			jsonOutput += "}";
		  			
		  			arllist.clear();
		  		
				
		    	}
		    	
		    }
		    response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
		    
	   }catch(Exception ex){
		   logger.error(ex);
		   ex.printStackTrace();
	   }
	   return null;
   
   }
   public String getUloFrontAvailability() throws IOException{
	   try{
		   	DateFormat format = new SimpleDateFormat("MMM d,yyyy", Locale.ENGLISH);
		    java.util.Date dateStart = format.parse(getStartDate());
		    java.util.Date dateEnd = format.parse(getEndDate());
		   
		    DecimalFormat df = new DecimalFormat("###.##");
		    DateFormat f = new SimpleDateFormat("EEEE");
		    
		    Calendar calCheckStart=Calendar.getInstance();
		    calCheckStart.setTime(dateStart);
		    java.util.Date checkInDate = calCheckStart.getTime();
		    this.arrivalDate=new Timestamp(checkInDate.getTime());
		   if(getSourceTypeId()==null){
			   this.sourceTypeId=1;
		   }
		    
		    Calendar calCheckEnd=Calendar.getInstance();
		    calCheckEnd.setTime(dateEnd);
		    java.util.Date checkOutDate = calCheckEnd.getTime();
		    this.departureDate=new Timestamp(checkOutDate.getTime());
			
			Map mapDate=new HashMap();  
			mapDate.put("arrivalDate", arrivalDate);
			mapDate.put("departureDate", departureDate); 
		      
			this.propertyId = getPropertyId();
			sessionMap.put("uloPropertyId",propertyId); 
			
			String checkIn = new SimpleDateFormat("MMM d,yyyy").format(getArrivalDate());
		    String checkOut = new SimpleDateFormat("MMM d,yyyy").format(getDepartureDate());
		    sessionMap.put("checkIn",checkIn);
			sessionMap.put("checkOut",checkOut);
			 
			
		    Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		   
			Calendar cal = Calendar.getInstance();
			cal.setTime(this.departureDate);
	        cal.add(Calendar.DATE, -1);
			String startDate = new SimpleDateFormat("yyyy-MM-dd").format(this.arrivalDate);
			String endDate = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
			DateTime start = DateTime.parse(startDate);
	        DateTime end = DateTime.parse(endDate);
	        java.util.Date arrival = start.toDate();
			java.util.Date departure = end.toDate();
			
			String iconPath  = "ulowebsite/images/icons/";
			
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("application/json");
			
			AccommodationAmenityManager accommodationAmenityController = new AccommodationAmenityManager();
			PmsPropertyManager propertyController = new PmsPropertyManager();		
			PropertyAccommodationManager accommodationManager = new PropertyAccommodationManager();
			PropertyAmenityManager amenityController = new PropertyAmenityManager();
			List<PropertyAmenity> propertyAmenityList;
			PropertyTypeManager typeController =  new PropertyTypeManager();
			PropertyRateManager rateController = new PropertyRateManager();
			PropertyRateDetailManager detailController = new PropertyRateDetailManager();
			PmsAmenityManager ameController = new PmsAmenityManager();
			PromotionManager promotionController=new PromotionManager();
			PromotionDetailManager promotionDetailController=new PromotionDetailManager();
			PmsBookingManager bookingController=new PmsBookingManager();
			
			
			HashMap<Double,Integer > mapAmountAccommId=new HashMap<Double, Integer>();
			HashMap<Double,Integer > mapSoldOutAmountAccommId=new HashMap<Double, Integer>();
			
			ArrayList<Integer> arlSortPropertyId=new ArrayList<Integer>();
			HashMap<Integer, Integer> mapAvailablePropertyId=new HashMap<Integer, Integer>();
			
	    	List<AccommodationRoom> listAccommodationRoom=null;
			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
			PropertyReviewsManager reviewController=new PropertyReviewsManager();
		    int availRoomCount=0,soldRoomCount=0;
		    this.propertyList=propertyController.list(getPropertyId());
		    if(this.propertyList.size()>0 && !this.propertyList.isEmpty()){
		    	for(PmsProperty property:this.propertyList){

					
					
		  			
		  			if (!jsonOutput.equalsIgnoreCase(""))
		  				jsonOutput += ",{";
		  			else
		  				jsonOutput += "{";
		  			jsonOutput += "\"DT_RowId\":\"" + property.getPropertyId()+ "\"";
		  			jsonOutput += ",\"city\":\"" + property.getCity() + "\"";
		  			jsonOutput += ",\"address1\":\"" + property.getAddress1() + "\"";
		  			jsonOutput += ",\"address2\":\"" + property.getAddress2() + "\"";
		  			jsonOutput += ",\"propertyName\":\"" + (property.getPropertyName() == null ? "": property.getPropertyName().toUpperCase().trim())+ "\"";
		  			jsonOutput += ",\"paynowDiscount\":\"" + (property.getPaynowDiscount() == null ? "": property.getPaynowDiscount())+ "\"";
		  			jsonOutput += ",\"propertyUrl\":\"" + (property.getPropertyUrl() == null ? "": property.getPropertyUrl().toLowerCase().trim())+ "\"";
//		  			jsonOutput += ",\"propertyStandardPolicy\":\"" + (property.getPropertyStandardPolicy() == null ? "None": property.getPropertyStandardPolicy().trim())+ "\"";
//		  			jsonOutput += ",\"propertyHotelPolicy\":\"" + (property.getPropertyHotelPolicy() == null ? "None": property.getPropertyHotelPolicy().trim())+ "\"";
//		  			jsonOutput += ",\"propertyCancellationPolicy\":\"" + (property.getPropertyCancellationPolicy() == null ? "None": property.getPropertyCancellationPolicy().trim())+ "\"";
		  			jsonOutput += ",\"payAtHotelStatus\":\"" + (property.getPayAtHotelStatus() == null ? false: property.getPayAtHotelStatus())+ "\"";
					jsonOutput += ",\"coupleStatus\":\"" + ( property.getCoupleIsActive() == null? false :  property.getCoupleIsActive() )+ "\"";
					jsonOutput += ",\"breakfastStatus\":\"" + ( property.getBreakfastIsActive() == null? false:  property.getBreakfastIsActive() )+ "\"";
					jsonOutput += ",\"hotelStatus\":\"" + ( property.getHotelIsActive() == null? false:  property.getHotelIsActive() )+ "\"";
					
		  			SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
		  			Timestamp FromDate=(Timestamp)mapDate.get("arrivalDate");
		  			Timestamp ToDate=(Timestamp)mapDate.get("departureDate");
		  			jsonOutput += ",\"arrivalDate\":\"" + format2.format(FromDate) + "\"";
		  			jsonOutput += ",\"departureDate\":\"" + format2.format(ToDate)+ "\"";
		  			
		  			jsonOutput += ",\"startDate\":\"" + format.format(FromDate) + "\"";
		  			jsonOutput += ",\"endDate\":\"" + format.format(ToDate)+ "\"";
		  			
		  			String strFromDate=format2.format(FromDate);
		  			String strToDate=format2.format(ToDate);
		  	    	DateTime startdate = DateTime.parse(strFromDate);
		  	        DateTime enddate = DateTime.parse(strToDate);
		  	        java.util.Date fromdate = startdate.toDate();
		  	 		java.util.Date todate = enddate.toDate();
		  	 		 
		  			int diffInDays = (int) ((todate.getTime() - fromdate.getTime()) / (1000 * 60 * 60 * 24));
		  			
		  			List arllist=new ArrayList();
		  			List arllistAvl=new ArrayList();
		  			List arllistSold=new ArrayList();
		  			jsonOutput += ",\"diffDays\":\"" + diffInDays+ "\"";
		  			
		  			String jsonAmenities ="";
		  			propertyAmenityList =  amenityController.list(property.getPropertyId());
		  			if(propertyAmenityList.size()>0)
		  			{
		  				for (PropertyAmenity amenity : propertyAmenityList) {
		  					if (!jsonAmenities.equalsIgnoreCase(""))
		  						
		  						jsonAmenities += ",{";
		  					else
		  						jsonAmenities += "{";
		  					PropertyAmenity amentity1 = amenityController.find(amenity.getPropertyAmenityId());
		  					String path = "contents/images/icons/";
		  					//String path = "ulowebsite/images/icons/";
		  					PmsAmenity pmsAmenity1 =ameController.find(amentity1.getPmsAmenity().getAmenityId());
		  					jsonAmenities += "\"DT_RowId\":\"" + amenity.getPropertyAmenityId()+ "\"";					
		  					jsonAmenities += ",\"amenityName\":\"" + pmsAmenity1.getAmenityName()+ "\"";
		  					jsonAmenities += ",\"amenityId\":\"" + pmsAmenity1.getAmenityId()+ "\"";
		                      jsonAmenities += ",\"icon\":\"" + path + pmsAmenity1.getAmenityIcon()+ "\"";
		               
		  					
		  					jsonAmenities += "}";
		  				}
		  				
		  				jsonOutput += ",\"amenities\":[" + jsonAmenities+ "]";
		  			}
		  			String jsonGuestReview="";
		  			String jsonReviews="";
					List<PropertyReviews> listReviews=reviewController.listReview(property.getPropertyId());
					if(listReviews.size()>0 && !listReviews.isEmpty()){
						for(PropertyReviews reviews:listReviews){
							
							if (!jsonReviews.equalsIgnoreCase(""))
								
								jsonReviews += ",{";
							else
								jsonReviews += "{";

							/*jsonReviews += "\"starCount\":\"" + reviews.getStarCount().intValue()+ "\"";	
							jsonReviews += ",\"reviewCount\":\"" + reviews.getReviewCount().intValue()+ "\"";
							Double averageReviews=reviews.getAverageReview();
							jsonReviews += ",\"averageReview\":\"" + df.format(averageReviews)+ "\"";*/
							
							jsonReviews += "\"starCount\":\"" + (reviews.getStarCount()==null?0:reviews.getStarCount().intValue())+ "\"";	
							jsonReviews += ",\"reviewCount\":\"" + (reviews.getReviewCount()==null?0:reviews.getReviewCount().intValue())+ "\"";	
							if(reviews.getAverageReview()!=null){
								Double averageReviews=reviews.getAverageReview();
								jsonReviews += ",\"averageReview\":\"" + df.format(averageReviews)+ "\"";
							}else{
								jsonReviews += ",\"averageReview\":\"" + df.format(0)+ "\"";
							}
							
							jsonReviews += ",\"tripadvisorStarCount\":\"" + (reviews.getTripadvisorStarCount()==null?0:reviews.getTripadvisorStarCount().intValue())+ "\"";	
							jsonReviews += ",\"tripadvisorReviewCount\":\"" + (reviews.getTripadvisorReviewCount()==null?0:reviews.getTripadvisorReviewCount().intValue())+ "\"";
							if(reviews.getTripadvisorAverageReview()!=null){
								Double taAverageReviews=reviews.getTripadvisorAverageReview();
								jsonReviews += ",\"tripadvisorAverageReview\":\"" + df.format(taAverageReviews)+ "\"";
							}else{
								jsonReviews += ",\"tripadvisorAverageReview\":\"" + df.format(0)+ "\"";
							}
							jsonReviews += ",\"googleReviewUrl\":\"" + (reviews.getGoogleReviewUrl()==null?"":reviews.getGoogleReviewUrl())+ "\"";	
							jsonReviews += ",\"tripadvisorReviewUrl\":\"" + (reviews.getTripadvisorReviewUrl()==null?"":reviews.getTripadvisorReviewUrl())+ "\"";	
							jsonReviews += ",\"propertyRating\":\"" + (reviews.getPropertyRating()==null?"None":reviews.getPropertyRating())+ "\"";
							jsonReviews += "}";
						}
						
					}else{
						if (!jsonReviews.equalsIgnoreCase(""))
							
							jsonReviews += ",{";
						else
							jsonReviews += "{";
						
						jsonReviews += "\"starCount\":\"" + 0+ "\"";	
						jsonReviews += ",\"reviewCount\":\"" + 0+ "\"";	
						jsonReviews += ",\"averageReview\":\"" + 0+ "\"";
						
						jsonReviews += ",\"tripadvisorStarCount\":\"" + 0 + "\"";	
						jsonReviews += ",\"tripadvisorReviewCount\":\"" + 0 + "\"";	
						jsonReviews += ",\"tripadvisorAverageReview\":\"" + 0+ "\"";
						
						jsonReviews += ",\"googleReviewUrl\":\"" + "None"+ "\"";	
						jsonReviews += ",\"tripadvisorReviewUrl\":\"" + "None"+ "\"";	
						jsonReviews += ",\"propertyRating\":\"" + "None"+ "\"";
						
						jsonReviews += "}";
					}
					
					jsonOutput += ",\"reviews\":[" + jsonReviews+ "]";
					
					
					PropertyGuestReviewManager guestReviewController=new PropertyGuestReviewManager();
					List<PropertyGuestReview> listGuestReviews=guestReviewController.listReview(property.getPropertyId());
					if(listGuestReviews.size()>0 && !listGuestReviews.isEmpty()){
						for(PropertyGuestReview reviews:listGuestReviews){
							if (!jsonGuestReview.equalsIgnoreCase(""))
								
								jsonGuestReview += ",{";
							else
								jsonGuestReview += "{";
							
							jsonGuestReview += "\"reviewerGuestName\":\"" + (reviews.getReviewGuestName()==null?"":reviews.getReviewGuestName())+ "\"";	
							jsonGuestReview += ",\"reviewerDescription\":\"" + (reviews.getReviewDescription()==null?"":reviews.getReviewDescription())+ "\"";	
							jsonGuestReview += ",\"reviewerCount\":\"" + (reviews.getReviewCount()==null?0:reviews.getReviewCount())+ "\"";
							jsonGuestReview += ",\"reviewerName\":\"" + (reviews.getReviews().getReviewerName()==null?"":reviews.getReviews().getReviewerName())+ "\"";
							jsonGuestReview += ",\"reviewerId\":\"" + (reviews.getReviews().getReviewerId()==null?"":reviews.getReviews().getReviewerId())+ "\"";
							jsonGuestReview += "}";
						}
					}else{
						if (!jsonGuestReview.equalsIgnoreCase(""))
							
							jsonGuestReview += ",{";
						else
							jsonGuestReview += "{";
						
						jsonGuestReview += "\"reviewerGuestName\":\"" + ""+ "\"";	
						jsonGuestReview += ",\"reviewerDescription\":\"" + ""+ "\"";	
						jsonGuestReview += ",\"reviewerCount\":\"" + 0+ "\"";
						jsonGuestReview += ",\"reviewerName\":\"" + ""+ "\"";
						jsonGuestReview += ",\"reviewerId\":\"" + "" + "\"";
						
						jsonGuestReview += "}";
					}
				
					jsonOutput += ",\"guestReviews\":[" + jsonGuestReview+ "]";
		  			
		  			String jsonAccommodation ="";
		  			List<PropertyAccommodation> accommodationsList =  accommodationManager.list(property.getPropertyId());
		  			int soldOutCount=0,availCount=0,promotionAccommId=0;
		  			if(accommodationsList.size()>0 && !accommodationsList.isEmpty())
		  			{
		  				for (PropertyAccommodation accommodation : accommodationsList) {
		  					int roomCount=0;
		  					listAccommodationRoom=accommodationController.listPropertyTotalRooms(property.getPropertyId(), accommodation.getAccommodationId());
		  					if(listAccommodationRoom.size()>0 && !listAccommodationRoom.isEmpty()){
		  						for(AccommodationRoom roomsList:listAccommodationRoom){
		  							 roomCount=(int) roomsList.getRoomCount();
		  							 long minimum = getAvailableCount(property.getPropertyId(),getArrivalDate(),getDepartureDate(),roomCount,accommodation.getAccommodationId());
		  							 if((int)minimum>0){
		  								 availCount++;
		  							 }else if((int)minimum==0){
		  								 soldOutCount++;
		  							 }
		  							 break;
		  						 }
		  					 }
		  					
		  					
		  					if (!jsonAccommodation.equalsIgnoreCase(""))
		  						
		  						jsonAccommodation += ",{";
		  					else
		  						jsonAccommodation += "{";
		  						jsonAccommodation += "\"accommodationId\":\"" + accommodation.getAccommodationId()+ "\"";
		  						jsonAccommodation += ",\"accommodationName\":\"" + accommodation.getAccommodationType().toUpperCase()+ "\"";
		  					
		  					jsonAccommodation += "}";
		  				}
		  				
		  				jsonOutput += ",\"jsonAccommodations\":[" + jsonAccommodation+ "]";
		  			}

		  			
		  			List<PmsPromotionDetails> listPromotionDetailCheck=null;
		  			Boolean blnAvailable=false,blnSoldout=false,firstPromotionFlag=false,secondPromotionFlag=false,promotionFlag=false,blnPromoFlag=false;
		  			int arlListTotalAccomm=0,soldOutTotalAccomm=0,accommCount=0;
		  			String promotionType=null;
		  			ArrayList<String> arlListPromoType=new ArrayList<String>();
		  			List<PropertyAccommodation> accommodationList =  accommodationManager.list(property.getPropertyId());
		  			if(accommodationList.size()>0)
		  			{
		  				for (PropertyAccommodation accommodation : accommodationList) {
		  					Integer promoAccommId=0,enablePromoId=0;
		  					ArrayList<Integer> arlEnablePromo=new ArrayList<Integer>();
		  					Boolean blnPromotionIsNotFlag=false;
		  					Boolean blnLastMinuteIsActive=false,blnLastMinuteFirstActive=false;
		  					int dateCount=0,promoCount=0;
		  					String strCheckPromotionType=null,promotionFirstDiscountType=null,promotionSecondDiscountType=null,promotionHoursRange=null;
		  					double amount=0.0,extraAdultAmount=0.0,extraChildAmount=0.0,totalSmartPriceAmount=0.0;
		  					int roomCount=0,availableCount=0,totalCount=0;
		  				    double dblPercentCount=0.0,dblSmartPricePercent=0.0,
		  				    		promotionFirstPercent=0.0,promotionFirstInr=0.0,
		  				    		promotionSecondPercent=0.0,promotionSecondInr=0.0,totalFlatLastAmount=0.0,flatLastAmount=0.0;
		  				    Timestamp lastMinStartDate=null,lastMinEndDate=null;
		  				    Boolean isPositive=false,isNegative=false,blnLastMinutePromotions=false;
		  				   
		  	    			
		  	    			listAccommodationRoom=accommodationController.listPropertyTotalRooms(property.getPropertyId(), accommodation.getAccommodationId());
		  					 
		  					if(listAccommodationRoom.size()>0 && !listAccommodationRoom.isEmpty()){
		  						
		  						for(AccommodationRoom roomsList:listAccommodationRoom){
		  							roomCount=(int) roomsList.getRoomCount();
		  							if(roomCount>0){
		  								long minimum = getAvailableCount(property.getPropertyId(),getArrivalDate(),getDepartureDate(),roomCount,accommodation.getAccommodationId());
		  								if((int)minimum>0){
		  									blnAvailable=true;
		  									blnSoldout=false;
		  									arlListTotalAccomm++;
		  								}else if((int)minimum==0){
		  									blnSoldout=true;
		  									blnAvailable=false;
		  									soldOutTotalAccomm++;
		  								}
		  							}
		  							 break;
		  						 }
		  					 }
		  					 Timestamp tsPromoBookedDate=null,tsPromoStartDate=null,tsPromoEndDate=null;
		  					 Timestamp tsFirstStartRange=null,tsSecondStartRange=null,tsFirstEndRange=null,tsSecondEndRange=null;
		 					 Timestamp tsLastStartDate=null,tsLastEndDate=null;
		  					 boolean blnFirstActivePromo=false,blnFirstNotActivePromo=false,baseFirstPromoFlag=false,blnEarlyBirdPromo=false;
		  					 boolean blnSecondActivePromo=false,blnSecondNotActivePromo=false,baseSecondPromoFlag=false,blnLastMinutePromo=false;
		  					 java.util.Date currentdate=new java.util.Date();
		  					 SimpleDateFormat format1=new SimpleDateFormat("yyyy-MM-dd");
		  					 String strCurrentDate = new SimpleDateFormat("yyyy-MM-dd").format(currentdate);
		  					 java.util.Date dtecurdate = format1.parse(strCurrentDate);
		 				   	 Calendar calStart=Calendar.getInstance();
		 				   	 calStart.setTime(dtecurdate);
		 				   	 calStart.setTimeZone(TimeZone.getTimeZone("IST"));
		 				   	 String StartDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calStart.getTime());
		 				   	 java.util.Date curdate = format1.parse(StartDate);
		  					 this.displayHoursForTimer=null;
		  				   	 setDisplayHoursForTimer(this.displayHoursForTimer);
		  					 List<PmsPromotions> listDatePromotions= promotionController.listBookedDatePromotions(property.getPropertyId(),curdate);
		  					 if(listDatePromotions.size()>0 && !listDatePromotions.isEmpty()){
		  						for(PmsPromotions promotions:listDatePromotions){
		  							tsPromoBookedDate=promotions.getPromotionBookedDate();
		  							tsPromoStartDate=promotions.getStartDate();
		  							tsPromoEndDate=promotions.getEndDate();
		  							blnEarlyBirdPromo=checkEarlyBirdPromos(tsPromoBookedDate,tsPromoStartDate,tsPromoEndDate,getArrivalDate(),getDepartureDate());
		  						}
		  					 }else{
		  						 blnEarlyBirdPromo=false;
		  					 }
		  					 
		  					List<PmsPromotions> listLastPromotions= promotionController.listLastBookedPromotions(property.getPropertyId(),curdate);
							 if(listLastPromotions.size()>0 && !listLastPromotions.isEmpty()){
								for(PmsPromotions promotions:listLastPromotions){
									tsFirstStartRange=promotions.getFirstRangeStartDate();
									tsSecondStartRange=promotions.getSecondRangeStartDate();
									tsFirstEndRange=promotions.getFirstRangeEndDate();
									tsSecondEndRange=promotions.getSecondRangeEndDate();
									tsLastStartDate=promotions.getStartDate();
									tsLastEndDate=promotions.getEndDate();
									promotionHoursRange=promotions.getPromotionHoursRange();
									blnLastMinuteFirstActive=checkLastMinutePromos(promotionHoursRange,getArrivalDate(),getDepartureDate(),tsSecondStartRange,tsFirstEndRange,tsFirstEndRange,tsSecondStartRange);
//									blnLastMinuteIsActive=getCheckHoursRange(promotionHoursRange,getArrivalDate(),getDepartureDate());
									if(blnLastMinuteFirstActive){
										blnLastMinutePromo=true;
										enablePromoId=promotions.getPromotionId();
										arlEnablePromo.add(enablePromoId);
									}
								}
							 }else{
								 blnLastMinutePromo=false;
								 this.displayHoursForTimer=null;
						    	 setDisplayHoursForTimer(this.displayHoursForTimer);
							 }
							 
		  					 ArrayList<Integer> arlFirstActivePromo=new ArrayList<Integer>(); 
		  					 ArrayList<Integer> arlSecondActivePromo=new ArrayList<Integer>(); 
		  					 List<DateTime> between = DateUtil.getDateRange(start, end);
		  					 for (DateTime d : between)
		  				     {
		  						 if(blnEarlyBirdPromo){
		  							 List<PmsPromotions> listEarlyPromotions= promotionController.listBookedDatePromotions(property.getPropertyId(),curdate);
		  							 if(listEarlyPromotions.size()>0 && !listEarlyPromotions.isEmpty()){
		  								 for(PmsPromotions promos:listEarlyPromotions){
		  									 if(promos.getPromotionType().equalsIgnoreCase("E")){
		  	 									 if(arlFirstActivePromo.isEmpty()){
		  	 										arlFirstActivePromo.add(promos.getPromotionId());
		  	 									 }else if(!arlFirstActivePromo.contains(promos.getPromotionId())){
		  	 										arlFirstActivePromo.add(promos.getPromotionId());
		  	 									 }
		  	 								 }else{
		  	 									blnFirstNotActivePromo=true;
		  	 								 }
		  								 }
		  							 }
		  						 }else if(!blnEarlyBirdPromo){
		  							 List<PmsPromotions> listAllPromotions= promotionController.listFlatDatePromotions(property.getPropertyId(),d.toDate());
		  							 if(!listAllPromotions.isEmpty() && listAllPromotions.size()>0){
		  								 for(PmsPromotions promos:listAllPromotions){
		  									 if(promos.getPromotionType().equalsIgnoreCase("F")){
		  	 									 if(arlFirstActivePromo.isEmpty()){
		  	 										arlFirstActivePromo.add(promos.getPromotionId());
		  	 									 }else if(!arlFirstActivePromo.contains(promos.getPromotionId())){
		  	 										arlFirstActivePromo.add(promos.getPromotionId());
		  	 									 }
		  	 								 }else{
		  	 									blnFirstNotActivePromo=true;
		  	 								 }
		  								 }
		  							 } 
		  						 }else{
		  							 blnFirstNotActivePromo=true;
		  						 }
		  						 
		  						if(blnLastMinutePromo){
		  							List<PmsPromotions> listLastPromos= promotionController.listLastBookedPromotions(property.getPropertyId(),curdate);
		  							 if(listLastPromos.size()>0 && !listLastPromos.isEmpty()){
		  								for(PmsPromotions promos:listLastPromos){
		  									if(promos.getPromotionType().equalsIgnoreCase("L")){
		  										promoAccommId=promos.getPropertyAccommodation().getAccommodationId();
		  										if(arlEnablePromo.contains(promos.getPromotionId())){
													if(arlSecondActivePromo.isEmpty()){
		  		  										arlSecondActivePromo.add(promos.getPromotionId());
		  		  									 }else if(!arlSecondActivePromo.contains(promos.getPromotionId())){
		  		  										arlSecondActivePromo.add(promos.getPromotionId());
		  		  									 }
												}
		  									 }
		  								}
		  							 }else{
		  								blnSecondNotActivePromo=true;
		  							 }
								}else{
									blnSecondNotActivePromo=true;
								}
		  				     
		  				     }
		  					 int intPromoSize=arlFirstActivePromo.size();
		  					 if((arlFirstActivePromo.isEmpty() || blnFirstNotActivePromo)){
		  						 blnFirstActivePromo=false;
		  					 }else{
		  						 blnFirstActivePromo=true;
		  					 }
		  					 
		  					 if(arlSecondActivePromo.isEmpty() || blnSecondNotActivePromo){
		  						blnSecondActivePromo=false;
		  					 }else{
		  						blnSecondActivePromo=true;
		  					 }
		  					 
		  					
		  				     for (DateTime d : between)
		  				     {
		  				    	 Integer promotionId=0;
		  				    	 if(blnFirstActivePromo){
		  				    		if(blnEarlyBirdPromo){
		  				    			List<PmsPromotions> listEarlyPromotions= promotionController.listBookedDatePromotions(property.getPropertyId(),curdate);
		  	  							 if(listEarlyPromotions.size()>0 && !listEarlyPromotions.isEmpty()){
		  	  				    			for(PmsPromotions promotions:listEarlyPromotions){
		  	  				    				if(promotions.getPromotionType().equalsIgnoreCase("E")){
		  	 					    				promotionId=promotions.getPromotionId();
		  	 					    				promotionFirstDiscountType=promotions.getPromotionDiscountType();
		  	 					    				listPromotionDetailCheck=promotionDetailController.listPromotionDetails(promotions.getPromotionId(),f.format(d.toDate()).toLowerCase());
		  	 					    				 for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
		  	 					    					if(promoCount==0){
		  	 					    						promotionFirstPercent=promoDetails.getPromotionPercentage();
		  	 	 					    					promotionFirstInr=promoDetails.getPromotionInr();
		  	 	 					    					promotionType=promotions.getPromotionType();
		  	 	 					    					if(promotionType.equalsIgnoreCase("E")){
		  	 	 					    						strCheckPromotionType="E";
		  	 	 					    					}
		  	 	 					    					
		  	 		  						    			arlListPromoType.add(promotionType);
		  	 					    					}
		  	 					    					promoCount++;
		  	 					    				 }
		  	  				    				}
		  	  				    			}
		  	  				    			promotionFlag=true; 
		  			    					blnPromoFlag=true;
		  	  				    		}else{
		  	  				    			promotionFlag=false;
		  	  	  		    				promotionType=null;
		  	  	  		    				blnPromoFlag=false;
		  	  	  		    				promotionId=null;
		  	  	  		    				baseFirstPromoFlag=true;
		  	  				    		}
		  				    		}else if(!blnEarlyBirdPromo){
		  				    			List<PmsPromotions> listPromotions= promotionController.listFlatDatePromotions(property.getPropertyId(),d.toDate());
		  	  				    		if(listPromotions.size()>0 && !listPromotions.isEmpty()){
		  	  				    			for(PmsPromotions promotions:listPromotions){
		  	  				    				if(promotions.getPromotionType().equalsIgnoreCase("F")){
		  	 					    				promotionId=promotions.getPromotionId();
		  	 					    				promotionFirstDiscountType=promotions.getPromotionDiscountType();
		  	 					    				listPromotionDetailCheck=promotionDetailController.listPromotionDetails(promotions.getPromotionId(),f.format(d.toDate()).toLowerCase());
		  	 					    				 for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
		  	 					    					if(promoCount==0){
		  	 					    						promotionFirstPercent=promoDetails.getPromotionPercentage();
		  	 	 					    					promotionFirstInr=promoDetails.getPromotionInr();
		  	 	 					    					promotionType=promotions.getPromotionType();
		  	 	 					    					if(promotionType.equalsIgnoreCase("F")){
		  	 	 					    						strCheckPromotionType="F";
		  	 	 					    					}
		  	 	 					    					
		  	 		  						    			arlListPromoType.add(promotionType);
		  	 					    					}
		  	 					    					promoCount++;
		  	 					    				 }
		  	  				    				}
		  	  				    			}
		  	  				    			promotionFlag=true; 
		  			    					blnPromoFlag=true;
		  	  				    		}
		  				    		}
		  				    	 }
		  				    	 if(blnSecondActivePromo){
		  				    		List<PmsPromotions> listPmsPromotions= promotionController.listLastDatePromotions(property.getPropertyId(),accommodation.getAccommodationId(),curdate);
						    			 if(listPmsPromotions.size()>0 &&!listPmsPromotions.isEmpty()){
						    				 for(PmsPromotions pmsPromotions:listPmsPromotions){
						    					 if(pmsPromotions.getPromotionType().equalsIgnoreCase("L")){
						    						 promotionId=pmsPromotions.getPromotionId();
									    			 promoAccommId=pmsPromotions.getPropertyAccommodation().getAccommodationId();
									    			 Timestamp tsStartDate=null,tsEndDate=null;
									    			 tsStartDate=pmsPromotions.getStartDate();
									    			 tsEndDate=pmsPromotions.getEndDate();
									    			 
									    			 promotionHoursRange=pmsPromotions.getPromotionHoursRange();
									    			 if(arlEnablePromo.contains(pmsPromotions.getPromotionId())){
									    				 promotionAccommId=promoAccommId.intValue();
									    				 long minimumCount = getAvailableCount(property.getPropertyId(),getArrivalDate(),getDepartureDate(),roomCount,accommodation.getAccommodationId());
						    			    			 if((int)minimumCount>0){
					    			    					 promotionFlag=true; 
					    			    					 blnPromoFlag=true;
					    			    					 strCheckPromotionType="L";
					    			    					 promotionType=pmsPromotions.getPromotionType();
		    	 		  						    		 arlListPromoType.add(promotionType);
					    			    					 promotionSecondDiscountType=pmsPromotions.getPromotionDiscountType();
		    	 					    					 listPromotionDetailCheck=promotionDetailController.listPromotionDetails(pmsPromotions.getPromotionId(),f.format(curdate).toLowerCase());
		    	 					    					 if(listPromotionDetailCheck.size()>0 && !listPromotionDetailCheck.isEmpty()){
		    	 					    						for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
				    	 					    					promotionSecondPercent=promoDetails.getPromotionPercentage();
			    	 	 					    					promotionSecondInr=promoDetails.getPromotionInr();
				    	 					    				 }
		    	 					    					 }
						    			    			 }else{
						    			    				 promotionFlag=false;
						    			    				 promotionType=null;
						    			    				 blnPromoFlag=false;
						    			    				 baseSecondPromoFlag=true;
						    			    			 }
									    			 }
						    					 }
						    					 
						    				 }
						    			 }
		  				    	 }
		  				    	 if(promotionFlag){
		  				    		int rateCount=0,rateIdCount=0;
		    						List<PropertyRate> propertyRateList =  rateController.listAllDates(property.getPropertyId(),accommodation.getAccommodationId(),getSourceTypeId(),d.toDate());
		    				    	if(propertyRateList.size()>0)
		    				    	{
		    				    		 for (PropertyRate pr : propertyRateList)
		    			    			 {
						    				 int propertyRateId = pr.getPropertyRateId();
							    			 List<PropertyRateDetail> rateDetailList =   detailController.list(propertyRateId,f.format(d.toDate()).toLowerCase());
		        				    		 if(rateDetailList.size()>0){
		        				    			 if(rateCount<1){
		        				    				 for (PropertyRateDetail rate : rateDetailList){
		        				    					 amount+=  rate.getBaseAmount();
			        				    			 }
		        				    				 rateCount++;
		        				    			 }
		        				    		 }else{
		        				    			 if(propertyRateList.size()==rateIdCount){
		        				    				 amount+= accommodation.getBaseAmount();
		        				    			 }
		            				    	 }
		    			    			 }	    		 
		    				    	 }
		    				    	 else{
		    				    		 amount+= accommodation.getBaseAmount();
		    				    	 }
		    				    	 double discountAmount=0;
		  				    		 if(blnFirstActivePromo){
		   	    				    	String strTypePercent=String.valueOf(promotionFirstPercent);
		   	    				    	String strTypeInr=String.valueOf(promotionFirstInr);
							    		  if(!strTypePercent.equalsIgnoreCase("null") && promotionFirstPercent!=0){
							    			  flatLastAmount = amount-(amount*promotionFirstPercent/100);
							    			  discountAmount+=amount*promotionFirstPercent/100;
							    		  }
							    		  else if(!strTypeInr.equalsIgnoreCase("null") && promotionFirstInr!=0){
		 					    			  flatLastAmount = amount-promotionFirstInr;
		 					    			  discountAmount+=promotionFirstInr;
		 					    		  }
		 					    		  else{
		 					    			  flatLastAmount = amount;
		 					    		  } 
		  				    		 }else{
						    			  flatLastAmount = amount;
						    		  } 
		  				    		 if(blnSecondActivePromo){
		  				    			String strTypePercent=String.valueOf(promotionSecondPercent);
		   	    				    	String strTypeInr=String.valueOf(promotionSecondInr);
							    		  if(!strTypePercent.equalsIgnoreCase("null") && promotionSecondPercent!=0){
							    			  flatLastAmount = flatLastAmount-(flatLastAmount*promotionSecondPercent/100);
							    			  discountAmount+=amount*promotionSecondPercent/100;
							    		  }
							    		  else if(!strTypeInr.equalsIgnoreCase("null") && promotionSecondInr!=0){
		 					    			  flatLastAmount = flatLastAmount-promotionSecondInr;
		 					    			 discountAmount+=promotionSecondInr;
		 					    		  }
		 					    		  else{
		 					    			  flatLastAmount = flatLastAmount;
		 					    		  }
		  				    		 }else{
						    			  flatLastAmount = flatLastAmount;
						    		  }
		  				    		 
		  				    		totalFlatLastAmount=flatLastAmount;
		  				    		totalFlatLastAmount=amount-discountAmount; 
		  				    	 }else{
		  				    		 if(baseFirstPromoFlag){
		  				    			amount += accommodation.getBaseAmount();
		  				    		}else{
		  				    			int rateCount=0,rateIdCount=0;
		  	    						List<PropertyRate> propertyRateList =  rateController.listAllDates(property.getPropertyId(),accommodation.getAccommodationId(),getSourceTypeId(),d.toDate());
		  	    				    	if(propertyRateList.size()>0)
		  	    				    	{
		  	    				    		 for (PropertyRate pr : propertyRateList)
		  	    			    			 {
		  					    				 int propertyRateId = pr.getPropertyRateId();
								    			 List<PropertyRateDetail> rateDetailList =   detailController.list(propertyRateId,f.format(d.toDate()).toLowerCase());
			        				    		 if(rateDetailList.size()>0){
			        				    			 if(rateCount<1){
			        				    				 for (PropertyRateDetail rate : rateDetailList){
			        				    					 amount +=  rate.getBaseAmount();
				        				    			 }
			        				    				 rateCount++;
			        				    			 }
			        				    		 }else{
			        				    			 if(propertyRateList.size()==rateIdCount){
			        				    				 amount += accommodation.getBaseAmount();
			        				    			 }
			            				    	 }
		  	    			    			 }	    		 
		  	    				    	 }
		  	    				    	 else{
		  	    				    		 amount += accommodation.getBaseAmount();
		  	    				    	 }
		  				    		}
		  				    	 }
		  				    	 dateCount++;
		  				    	 if(dateCount==diffInDays){
		  				    		 if(blnAvailable){
		  				    			if(promotionType!=null && strCheckPromotionType!=null) {
		  				    				if(strCheckPromotionType.equalsIgnoreCase("L") || strCheckPromotionType.equalsIgnoreCase("F") || strCheckPromotionType.equalsIgnoreCase("E")){
					    						mapAmountAccommId.put(totalFlatLastAmount,accommodation.getAccommodationId());
		  						      			arllist.add(totalFlatLastAmount);
		  					    			}
		  					    			
		  				    			}else{
		  				    				mapAmountAccommId.put(amount,accommodation.getAccommodationId());
		  					      			arllist.add(amount);
		  				    			}
		  		      				 }else if(blnSoldout){
		  		      					if(promotionType!=null && strCheckPromotionType!=null) {
			  		      					if(strCheckPromotionType.equalsIgnoreCase("L") || strCheckPromotionType.equalsIgnoreCase("F") || strCheckPromotionType.equalsIgnoreCase("E")){
			  		      						mapSoldOutAmountAccommId.put(totalFlatLastAmount,accommodation.getAccommodationId());
								      			arllist.add(totalFlatLastAmount);
							    			}
		  		      					}else{
		  				    				mapSoldOutAmountAccommId.put(amount,accommodation.getAccommodationId());
		  					      			arllist.add(amount);
		  				    			}
		  					    	 }
		  				    	 }
		  				     }
		  				     if(blnAvailable){
		  				    	 if(promotionType!=null && strCheckPromotionType!=null) {
		  				    		if(strCheckPromotionType.equalsIgnoreCase("L") || strCheckPromotionType.equalsIgnoreCase("F") || strCheckPromotionType.equalsIgnoreCase("E")){
		  				    			arllistAvl.add(totalFlatLastAmount);
					    			} 
		  				    	 }else{
		  		    				arllistAvl.add(amount);
		  		    			}
		  				     }else if(!blnAvailable){
		  				    	 if(promotionType!=null && strCheckPromotionType!=null) {
		  				    		if(strCheckPromotionType.equalsIgnoreCase("L") || strCheckPromotionType.equalsIgnoreCase("F") || strCheckPromotionType.equalsIgnoreCase("E")){
		  					    		 arllistSold.add(totalFlatLastAmount);
		  			    			}
		  				    	 }else{
		  		    				arllistSold.add(amount);
		  		    			}
		  				     }
		  				     accommCount++;
		  				}
		  			}
		  			//end of accommodation
		  			double totalAmount=0;
		  			boolean blnJSONContent=false;
		  			Collections.sort(arllist);
		  			int arlListSize=0,accommId=0,intTotalAccomm=0;
		  		     intTotalAccomm=accommodationList.size();
		  		     if(intTotalAccomm==accommCount){
		  		    	 if(intTotalAccomm==soldOutCount){
		  			    	 if(blnPromoFlag){
		  				    	 if(arlListPromoType.contains("F") || arlListPromoType.contains("E")){
		  				    		 arlListSize=arllist.size();
		  						     if(soldOutCount==arlListSize){
		  						    	 Collections.sort(arllist);
		  						    	 for(int i=0;i<arlListSize;i++){
		  							    	 totalAmount=(Double) arllist.get(i);
		  							    	 if((Double) arllist.get(0)==(Double) arllist.get(i)){
		  							    		 if(blnAvailable){
		  							    			 accommId=mapAmountAccommId.get(totalAmount);
		  								    		 blnJSONContent=true;
		  							    		 }else{
		  							    			 accommId=mapSoldOutAmountAccommId.get(totalAmount);
		  								    		 blnJSONContent=true;
		  							    		 }
		  							    		 
		  							    		 break;
		  							    	 }else{
		  							    		 
		  							    	 }
		  							     }
		  						     }
		  				    	 }else if(arlListPromoType.contains("L")){
		  				    		 /*if(promotionAccommId>0){
		  					    		 accommId=promotionAccommId;
		  					    		 blnJSONContent=true;
		  					    	 } */
		  				    		arlListSize=arllist.size();
		 	 					     Collections.sort(arllist);
		 	 				    	 for(int i=0;i<arlListSize;i++){
		 	 				    		 if(arllistAvl.contains((Double) arllist.get(i))){
		 	 				    			 if((Double) arllist.get(0)==(Double) arllist.get(i)){
		 	 					    			 totalAmount=(Double) arllist.get(i);
		 	 					    			 accommId=mapAmountAccommId.get(totalAmount);
		 	 						    		 blnJSONContent=true;
		 	 				    			 }else{
		 	 				    				 totalAmount=(Double) arllist.get(i);
		 	 					    			 accommId=mapAmountAccommId.get(totalAmount);
		 	 						    		 blnJSONContent=true;
		 	 				    			 }
		 	 				    			 break;
		 	 				    		 }
		 	 					     }
		  				    	 }
		  				     }else{
		  					     arlListSize=arllist.size();
		  					     if(soldOutCount==arlListSize){
		  					    	 Collections.sort(arllist);
		  					    	 for(int i=0;i<arlListSize;i++){
		  						    	 totalAmount=(Double) arllist.get(i);
		  						    	 if((Double) arllist.get(0)==(Double) arllist.get(i)){
		  						    		 if(blnAvailable){
		  						    			 accommId=mapAmountAccommId.get(totalAmount);
		  							    		 blnJSONContent=true;
		  						    		 }else{
		  						    			 accommId=mapSoldOutAmountAccommId.get(totalAmount);
		  							    		 blnJSONContent=true;
		  						    		 }
		  						    		 break;
		  						    	 }else{
		  						    		 
		  						    	 }
		  						     }
		  					     }
		  					     
		  				     }
		  			     }else{
		  			    	 if(arlListPromoType.size()>0){
		  			    		 if(arlListPromoType.contains("F") || arlListPromoType.contains("E")){
//		  				    	 if(promotionType.equalsIgnoreCase("F") || promotionType.equalsIgnoreCase("L")){
		  				    		 arlListSize=arllist.size();
		  				    		 Collections.sort(arllist);
		  					    	 for(int i=0;i<arlListSize;i++){
		  					    		 if(arllistAvl.contains((Double) arllist.get(i))){
		  					    			 if((Double) arllist.get(0)==(Double) arllist.get(i)){
		  						    			 totalAmount=(Double) arllist.get(i);
		  						    			 accommId=mapAmountAccommId.get(totalAmount);
		  							    		 blnJSONContent=true;
		  					    			 }else{
		  					    				 totalAmount=(Double) arllist.get(i);
		  						    			 accommId=mapAmountAccommId.get(totalAmount);
		  							    		 blnJSONContent=true;
		  					    			 }
		  					    			 break;
		  					    		 }
		  						     }
		  				    	 }else if( arlListPromoType.contains("L")){
		  				    		 /*if(promotionAccommId>0){
		  					    		 accommId=promotionAccommId;
		  					    		 blnJSONContent=true;
		  					    	 } */
		  				    		arlListSize=arllist.size();
		 	 					     Collections.sort(arllist);
		 	 				    	 for(int i=0;i<arlListSize;i++){
		 	 				    		 if(arllistAvl.contains((Double) arllist.get(i))){
		 	 				    			 if((Double) arllist.get(0)==(Double) arllist.get(i)){
		 	 					    			 totalAmount=(Double) arllist.get(i);
		 	 					    			 accommId=mapAmountAccommId.get(totalAmount);
		 	 						    		 blnJSONContent=true;
		 	 				    			 }else{
		 	 				    				 totalAmount=(Double) arllist.get(i);
		 	 					    			 accommId=mapAmountAccommId.get(totalAmount);
		 	 						    		 blnJSONContent=true;
		 	 				    			 }
		 	 				    			 break;
		 	 				    		 }
		 	 					     }
		  				    	 }
		  				     }else{
		  					     arlListSize=arllist.size();
		  					     Collections.sort(arllist);
		  				    	 for(int i=0;i<arlListSize;i++){
		  				    		 if(arllistAvl.contains((Double) arllist.get(i))){
		  				    			 if((Double) arllist.get(0)==(Double) arllist.get(i)){
		  					    			 totalAmount=(Double) arllist.get(i);
		  					    			 accommId=mapAmountAccommId.get(totalAmount);
		  						    		 blnJSONContent=true;
		  				    			 }else{
		  				    				 totalAmount=(Double) arllist.get(i);
		  					    			 accommId=mapAmountAccommId.get(totalAmount);
		  						    		 blnJSONContent=true;
		  				    			 }
		  				    			 break;
		  				    		 }
		  					     }
		  				     }
		  			     }
		  		     }
		  		     arlListPromoType.clear();  
		  			if(blnJSONContent){
		  				int promoCount=0,enablePromoId=0;
		  			    listAccommodationRoom.clear(); 
		  			    ArrayList<String> arlPromoType=new ArrayList<String>();
		  			    ArrayList<Integer> arlEnablePromo=new ArrayList<Integer>();
		  		    	listAccommodationRoom=accommodationController.listPropertyTotalRooms(property.getPropertyId(), accommId);
		  		    	List<DateTime> betweenAmount = DateUtil.getDateRange(start, end);
		  		    	double actualPromoBaseAmount=0.0,actualBaseAmount=0.0,actualTotalAmount=0.0,actualTotalAdultAmount=0.0,actualTotalChildAmount=0.0,
		  		    			actualAdultAmount=0.0,actualChildAmount=0.0,promotionFirstPercent=0.0,promotionFirstInr=0.0,
		  	  				    		promotionSecondPercent=0.0,promotionSecondInr=0.0;
		  			    int countDate=0,promotionId=0,promotionHours=0,promoAccommId=0; 
		  			    boolean blnStatus=false,isPositive=false,isNegative=false,
		  			    		promoFlag=false,blnBookedGetRooms=false,
		  			    		blnLastMinutePromotions=false,blnLastMinuteIsActive=false,blnLastMinuteFirstActive=false;
		  			    String promotionFirstName=null,promotionSecondName=null,promotionHoursRange=null,
		  			    		promotionSecondDiscountType=null,promotionFirstDiscountType=null,strCheckPromotionType=null;
		  			    Integer promoFirstPercent=null,promoFirstInr=null,promoSecondInr=null,promoSecondPercent=null;
		  			    double promoBaseAmount=0.0,promoAdultAmount=0.0,promoChildAmount=0.0,smartPricePercent=0.0;
		  			    double baseAmount=0.0,extraAdultAmount=0.0,extraChildAmount=0.0,totalBaseAmount=0.0,totalSmartPriceAmount=0.0,totalFlatAmount=0.0,totalLastAmount=0.0;
		  			    int roomCount=0,availableCount=0,totalCount=0,discountId=0;
		  			    double dblPercentCount=0.0,dblSmartPricePercent=0.0, flatAmount=0.0,lastAmount=0.0;
		  			    double dblPercentFrom=0.0,dblPercentTo=0.0,lastMinuteHours=0.0;
		  			    Timestamp lastMinStartDate=null,lastMinEndDate=null;
		  				if(listAccommodationRoom.size()>0 && !listAccommodationRoom.isEmpty()){
		  					for(AccommodationRoom roomsList:listAccommodationRoom){
		  						 roomCount=(int) roomsList.getRoomCount();
		  						 break;
		  					 }
		  				 }
		  				 Timestamp tsPromoBookedDate=null,tsPromoStartDate=null,tsPromoEndDate=null;
		  				 Timestamp tsFirstStartRange=null,tsSecondStartRange=null,tsFirstEndRange=null,tsSecondEndRange=null;
		  				 Timestamp tsLastStartDate=null,tsLastEndDate=null;
		  				 boolean blnLastMinutePromo=false,blnEarlyBirdPromo=false,blnFirstActivePromo=false,blnFirstNotActivePromo=false,basePromoFlag=false;
						 boolean blnSecondActivePromo=false,blnSecondNotActivePromo=false;
						 ArrayList<Integer> arlFirstActivePromo=new ArrayList<Integer>(); 
						 ArrayList<Integer> arlSecondActivePromo=new ArrayList<Integer>(); 
						 List<DateTime> between = DateUtil.getDateRange(start, end);
						 SimpleDateFormat format1=new SimpleDateFormat("yyyy-MM-dd");
						 java.util.Date currentdate=new java.util.Date();
						 String strCurrentDate = new SimpleDateFormat("yyyy-MM-dd").format(currentdate);
						 java.util.Date dtecurdate = format1.parse(strCurrentDate);
	 				   	 Calendar calStart=Calendar.getInstance();
	 				   	 calStart.setTime(dtecurdate);
	 				   	 calStart.setTimeZone(TimeZone.getTimeZone("IST"));
	 				   	 String StartDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calStart.getTime());
	 				   	 java.util.Date curdate = format1.parse(StartDate);
	 				   	 
						 this.displayHoursForTimer=null;
					   	 setDisplayHoursForTimer(this.displayHoursForTimer);
						 List<PmsPromotions> listPromotions= promotionController.listBookedDatePromotions(property.getPropertyId(),curdate);
						 if(listPromotions.size()>0 && !listPromotions.isEmpty()){
							 for(PmsPromotions promotions:listPromotions){
								tsPromoBookedDate=promotions.getPromotionBookedDate();
								tsPromoStartDate=promotions.getStartDate();
								tsPromoEndDate=promotions.getEndDate();
								blnEarlyBirdPromo=checkEarlyBirdPromos(tsPromoBookedDate,tsPromoStartDate,tsPromoEndDate,getArrivalDate(),getDepartureDate());
							}
						 }else{
							 blnEarlyBirdPromo=false;
						 }
						 
						 List<PmsPromotions> listLastPromotions= promotionController.listLastBookedPromotions(property.getPropertyId(),curdate);
						 if(listLastPromotions.size()>0 && !listLastPromotions.isEmpty()){
							for(PmsPromotions promotions:listLastPromotions){
								tsFirstStartRange=promotions.getFirstRangeStartDate();
								tsSecondStartRange=promotions.getSecondRangeStartDate();
								tsFirstEndRange=promotions.getFirstRangeEndDate();
								tsSecondEndRange=promotions.getSecondRangeEndDate();
								tsLastStartDate=promotions.getStartDate();
								tsLastEndDate=promotions.getEndDate();
								promotionHoursRange=promotions.getPromotionHoursRange();
								blnLastMinuteFirstActive=checkLastMinutePromos(promotionHoursRange,getArrivalDate(),getDepartureDate(),tsSecondStartRange,tsFirstEndRange,tsFirstStartRange,tsSecondEndRange);
//								blnLastMinuteIsActive=getCheckHoursRange(promotionHoursRange,getArrivalDate(),getDepartureDate());
								if(blnLastMinuteFirstActive){
									blnLastMinutePromo=true;
									enablePromoId=promotions.getPromotionId();
									arlEnablePromo.add(enablePromoId);
								}
							}
						 }else{
							 blnLastMinutePromo=false;
							 this.displayHoursForTimer=null;
					    	 setDisplayHoursForTimer(this.displayHoursForTimer);
						 }
						 
						 for (DateTime d : between)
					     {
								 if(blnEarlyBirdPromo){
									 List<PmsPromotions> listEarlyPromotions= promotionController.listBookedDatePromotions(property.getPropertyId(),curdate);
									 if(listEarlyPromotions.size()>0 && !listEarlyPromotions.isEmpty()){
										 for(PmsPromotions promos:listEarlyPromotions){
											 if(promos.getPromotionType().equalsIgnoreCase("E")){
			 									 if(arlFirstActivePromo.isEmpty()){
			 										arlFirstActivePromo.add(promos.getPromotionId());
			 									 }else if(!arlFirstActivePromo.contains(promos.getPromotionId())){
			 										arlFirstActivePromo.add(promos.getPromotionId());
			 									 }
			 								 }else{
			 									blnFirstNotActivePromo=true;
			 								 }
										 }
									 }
								 }else if(!blnEarlyBirdPromo){
									 List<PmsPromotions> listAllPromotions= promotionController.listFlatDatePromotions(property.getPropertyId(),d.toDate());
									 if(!listAllPromotions.isEmpty() && listAllPromotions.size()>0){
										 for(PmsPromotions promos:listAllPromotions){
											 if(promos.getPromotionType().equalsIgnoreCase("F")){
			 									 if(arlFirstActivePromo.isEmpty()){
			 										arlFirstActivePromo.add(promos.getPromotionId());
			 									 }else if(!arlFirstActivePromo.contains(promos.getPromotionId())){
			 										arlFirstActivePromo.add(promos.getPromotionId());
			 									 }
			 								 }else{
			 									blnFirstNotActivePromo=true;
			 								 }
										 }
									 } 
								 }else{
									 blnFirstNotActivePromo=true;
								 }
								 
								 if(blnLastMinutePromo){
			  							List<PmsPromotions> listLastPromos= promotionController.listLastBookedPromotions(property.getPropertyId(),curdate);
			  							 if(listLastPromos.size()>0 && !listLastPromos.isEmpty()){
			  								for(PmsPromotions promos:listLastPromos){
			  									if(promos.getPromotionType().equalsIgnoreCase("L")){
			  										promoAccommId=promos.getPropertyAccommodation().getAccommodationId();
			  										if(arlEnablePromo.contains(promos.getPromotionId())){
		  												if(arlSecondActivePromo.isEmpty()){
			  		  										arlSecondActivePromo.add(promos.getPromotionId());
			  		  									 }else if(!arlSecondActivePromo.contains(promos.getPromotionId())){
			  		  										arlSecondActivePromo.add(promos.getPromotionId());
			  		  									 }
		  											}
			  									 }
			  								}
			  							 }else{
			  								blnSecondNotActivePromo=true;
			  							 }
									}else{
										blnSecondNotActivePromo=true;
									}
						     }
						 int intPromoSize=arlFirstActivePromo.size();
						 if((arlFirstActivePromo.isEmpty() || blnFirstNotActivePromo)){
							 blnFirstActivePromo=false;
						 }else{
							 blnFirstActivePromo=true;
						 }
						 
						 if(arlSecondActivePromo.isEmpty() || blnSecondNotActivePromo){
							blnSecondActivePromo=false;
						 }else{
							 blnSecondActivePromo=true;
						 }
		  				
		  				List<PropertyAccommodation> listAccommodation =  accommodationManager.list(property.getPropertyId(),accommId);
		  				
		  				for (PropertyAccommodation accommodations : listAccommodation) {
		  					for (DateTime d : betweenAmount)
		  				    {
		  						boolean blnFlatPromotions=false;
		  						if(blnFirstActivePromo){
		  							if(blnEarlyBirdPromo){
		  	  							List<PmsPromotions> listFlatPromotions= promotionController.listBookedDatePromotions(property.getPropertyId(),curdate);
		  	  					    	if(listFlatPromotions.size()>0 && !listFlatPromotions.isEmpty()){
		  	  					    		for(PmsPromotions promotions:listFlatPromotions){
		  	  					    			 if(promotions.getPromotionType().equalsIgnoreCase("E")){
		  	  					    				 promotionId=promotions.getPromotionId();
		  	  					    				 promotionType=promotions.getPromotionType();
		  	  					    				 promotionFirstDiscountType=promotions.getPromotionDiscountType();
		  	  					    				 listPromotionDetailCheck=promotionDetailController.listPromotionDetails(promotions.getPromotionId(),f.format(d.toDate()).toLowerCase());
		  	  					    				 for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
		  	  				    						if(promoCount==0){
		  	  				    							promotionFirstPercent=promoDetails.getPromotionPercentage();
		  	 	 					    					promotionFirstInr=promoDetails.getPromotionInr();
		  	 	 					    					promotionType=promotions.getPromotionType();
		  	  	  				    						 promotionFlag=true;
		  	  	  						    				 blnPromoFlag=true;
		  	  	  						    				 arlPromoType.add(promotionType);
		  	  				    						}
		  	  				    						promoCount++;
		  	  				    					 }
		  	  					    			 }
		  	  					    		}
		  	  					    	}
		  	  						
		  							}else if(!blnEarlyBirdPromo){

		  	  							List<PmsPromotions> listFlatPromotions= promotionController.listFlatDatePromotions(property.getPropertyId(),d.toDate());
		  	  					    	if(listFlatPromotions.size()>0 && !listFlatPromotions.isEmpty()){
		  	  					    		for(PmsPromotions promotions:listFlatPromotions){
		  	  					    			 if(promotions.getPromotionType().equalsIgnoreCase("F")){
		  	  					    				 promotionId=promotions.getPromotionId();
		  	  					    				 promotionType=promotions.getPromotionType();
		  	  					    				 promotionFirstDiscountType=promotions.getPromotionDiscountType();
		  	  					    				 listPromotionDetailCheck=promotionDetailController.listPromotionDetails(promotions.getPromotionId(),f.format(d.toDate()).toLowerCase());
		  	  					    				 for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
		  	  				    						if(promoCount==0){
		  	  				    							promotionFirstPercent=promoDetails.getPromotionPercentage();
		  	 	 					    					promotionFirstInr=promoDetails.getPromotionInr();
		  	 	 					    					promotionType=promotions.getPromotionType();
		  	  	  				    						 promotionFlag=true;
		  	  	  						    				 blnPromoFlag=true;
		  	  	  						    				 arlPromoType.add(promotionType);
		  	  				    						}
		  	  				    						promoCount++;
		  	  				    					 }
		  	  					    			 }
		  	  					    		}
		  	  					    	}
		  							}
		  						}
		  						if(blnSecondActivePromo){

						    		List<PmsPromotions> listPmsPromotions= promotionController.listLastDatePromotions(property.getPropertyId(),accommId,d.toDate());
						    			 if(listPmsPromotions.size()>0 &&!listPmsPromotions.isEmpty()){
						    				 for(PmsPromotions pmsPromotions:listPmsPromotions){
						    					 promotionId=pmsPromotions.getPromotionId();
								    			 promoAccommId=pmsPromotions.getPropertyAccommodation().getAccommodationId();
								    			 promotionHoursRange=pmsPromotions.getPromotionHoursRange();
								    			 Timestamp tsStartDate=null,tsEndDate=null;
								    			 tsStartDate=pmsPromotions.getStartDate();
								    			 tsEndDate=pmsPromotions.getEndDate();
								    			 
								    			 if(arlEnablePromo.contains(pmsPromotions.getPromotionId())){
								    				 promotionAccommId=promoAccommId;
								    				 long minimumCount = getAvailableCount(property.getPropertyId(),getArrivalDate(),getDepartureDate(),roomCount,accommId);
					    			    			 if((int)minimumCount>0){
				    			    					 promotionFlag=true; 
				    			    					 blnPromoFlag=true;
				    			    					 strCheckPromotionType="L";
				    			    					 promotionType=pmsPromotions.getPromotionType();
				    			    					 arlPromoType.add(promotionType);
				    			    					 promotionSecondDiscountType=pmsPromotions.getPromotionDiscountType();
			 					    					 listPromotionDetailCheck=promotionDetailController.listPromotionDetails(pmsPromotions.getPromotionId(),f.format(curdate).toLowerCase());
			 					    					 if(listPromotionDetailCheck.size()>0 && !listPromotionDetailCheck.isEmpty()){
			 					    						for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
			    	 					    					promotionSecondPercent=promoDetails.getPromotionPercentage();
		    	 	 					    					promotionSecondInr=promoDetails.getPromotionInr();
			    	 					    				 }
			 					    					 }
					    			    			 }else{
					    			    				 promotionFlag=false;
					    			    				 promotionType=null;
					    			    				 blnPromoFlag=false;
					    			    				 basePromoFlag=true;
					    			    			 }
								    			 }
						    				 }
						    			 }
		  						}
		  						if(promotionFlag){
		  							int rateCount=0,rateIdCount=0;
		    						List<PropertyRate> propertyRateList =  rateController.listAllDates(property.getPropertyId(),accommodations.getAccommodationId(),getSourceTypeId(),d.toDate());
		    				    	if(propertyRateList.size()>0)
		    				    	{
		    				    		 for (PropertyRate pr : propertyRateList)
		    			    			 {
						    				 int propertyRateId = pr.getPropertyRateId();
							    			 List<PropertyRateDetail> rateDetailList =   detailController.list(propertyRateId,f.format(d.toDate()).toLowerCase());
		        				    		 if(rateDetailList.size()>0){
		        				    			 if(rateCount<1){
		        				    				 for (PropertyRateDetail rate : rateDetailList){
		        				    					 baseAmount+=  rate.getBaseAmount();
		        				    					 extraAdultAmount+=rate.getExtraAdult();
		        	  				    				 extraChildAmount+=rate.getExtraChild();
			        				    			 }
		        				    				 rateCount++;
		        				    			 }
		        				    		 }else{
		        				    			 if(propertyRateList.size()==rateIdCount){
		        				    				 baseAmount += accommodations.getBaseAmount();
		        				    				 extraAdultAmount+=accommodations.getExtraAdult();
		          				    				 extraChildAmount+=accommodations.getExtraChild();
		        				    			 }
		            				    	 }
		    			    			 }	    		 
		    				    	 }
		    				    	 else{
		    				    		baseAmount += accommodations.getBaseAmount();
		    				    		extraAdultAmount+=accommodations.getExtraAdult();
					    				extraChildAmount+=accommodations.getExtraChild();
		    				    	 }
		    				    	double discountAmount=0;
		 				    		 if(blnFirstActivePromo){
		  	    				    	  String strTypePercent=String.valueOf(promotionFirstPercent);
		  	    				    	  String strTypeInr=String.valueOf(promotionFirstInr);
							    		  if(!strTypePercent.equalsIgnoreCase("null") && promotionFirstPercent!=0){
							    			  actualPromoBaseAmount = baseAmount-(baseAmount*promotionFirstPercent/100);
							    			  discountAmount+=baseAmount*promotionFirstPercent/100;
							    		  }
							    		  else if(!strTypeInr.equalsIgnoreCase("null") && promotionFirstInr!=0){
							    			  actualPromoBaseAmount = baseAmount-promotionFirstInr;
							    			  discountAmount+=promotionFirstInr;
							    		  }
							    		  else{
							    			  actualPromoBaseAmount = baseAmount;
							    		  } 
		 				    		 }else{
						    			  actualPromoBaseAmount = baseAmount;
						    		  } 
		 				    		 if(blnSecondActivePromo){
		 				    			String strTypePercent=String.valueOf(promotionSecondPercent);
		  	    				    	String strTypeInr=String.valueOf(promotionSecondInr);
							    		  if(!strTypePercent.equalsIgnoreCase("null") && promotionSecondPercent!=0){
							    			  actualPromoBaseAmount = actualPromoBaseAmount-(actualPromoBaseAmount*promotionSecondPercent/100);
							    			  discountAmount+=baseAmount*promotionSecondPercent/100;
							    		  }
							    		  else if(!strTypeInr.equalsIgnoreCase("null") && promotionSecondInr!=0){
							    			  actualPromoBaseAmount= actualPromoBaseAmount-promotionSecondInr;
							    			  discountAmount+=promotionSecondInr;
							    		  }
							    		  else{
							    			  actualPromoBaseAmount = actualPromoBaseAmount;
							    		  }
		 				    		 }else{
						    			  actualPromoBaseAmount = actualPromoBaseAmount;
						    		  }
		 				    		actualBaseAmount=baseAmount;
		 				    		totalFlatAmount=baseAmount-discountAmount;
		 				    		 
		 				    	 }else{
		 				    		 if(basePromoFlag){
		 				    			baseAmount += accommodations.getBaseAmount();
		 				    			extraAdultAmount+=accommodations.getExtraAdult();
		 				    			extraChildAmount+=accommodations.getExtraChild();
		 				    		}else{
		 				    			int rateCount=0,rateIdCount=0;
		 	    						List<PropertyRate> propertyRateList =  rateController.listAllDates(property.getPropertyId(),accommodations.getAccommodationId(),getSourceTypeId(),d.toDate());
		 	    				    	if(propertyRateList.size()>0)
		 	    				    	{
		 	    				    		 for (PropertyRate pr : propertyRateList)
		 	    			    			 {
		 					    				 int propertyRateId = pr.getPropertyRateId();
								    			 List<PropertyRateDetail> rateDetailList =   detailController.list(propertyRateId,f.format(d.toDate()).toLowerCase());
			        				    		 if(rateDetailList.size()>0){
			        				    			 if(rateCount<1){
			        				    				 for (PropertyRateDetail rate : rateDetailList){
			        				    					 baseAmount +=  rate.getBaseAmount();
			        				    					 extraAdultAmount+=rate.getExtraAdult();
			        	  				    				 extraChildAmount+=rate.getExtraChild();
				        				    			 }
			        				    				 rateCount++;
			        				    			 }
			        				    		 }else{
			        				    			 if(propertyRateList.size()==rateIdCount){
			        				    				 baseAmount += accommodations.getBaseAmount();
			        				    				 extraAdultAmount+=accommodations.getExtraAdult();
			          				    				 extraChildAmount+=accommodations.getExtraChild();
			        				    			 }
			            				    	 }
		 	    			    			 }	    		 
		 	    				    	 }
		 	    				    	 else{
		 	    				    		baseAmount += accommodations.getBaseAmount();
		 	    				    		extraAdultAmount+=accommodations.getExtraAdult();
		 				    				extraChildAmount+=accommodations.getExtraChild();
		 	    				    	 }
		 				    		}
		 				    	 }
		  						 actualBaseAmount=baseAmount;
		  				    	 countDate++;
		  				    }
		  					Iterator<String> iterPromoType=arlPromoType.iterator();
		  					while(iterPromoType.hasNext()){
		  						String strPromoType=iterPromoType.next();
		  						if(strPromoType.equalsIgnoreCase("F") || strPromoType.equalsIgnoreCase("E")){
		  							String strTypePercent=String.valueOf(promotionFirstPercent);
		    				    	String strTypeInr=String.valueOf(promotionFirstInr);
						    		if(!strTypePercent.equalsIgnoreCase("null") && promotionFirstPercent!=0){
						    			promotionFirstName=String.valueOf(Math.round(promotionFirstPercent))+" % OFF";
			  							promoFirstPercent=(int) Math.round(promotionFirstPercent);
						    		}
						    		else if(!strTypeInr.equalsIgnoreCase("null") && promotionFirstInr!=0){
						    			promotionFirstName="Flat "+String.valueOf(Math.round(promotionFirstInr))+" OFF";
			  							promoFirstInr=(int) Math.round(promotionFirstInr);
						    		}
		  							
		  						}
		  						if(strPromoType.equalsIgnoreCase("L")){
		  							  String strTypePercent=String.valueOf(promotionSecondPercent);
		  							  String strTypeInr=String.valueOf(promotionSecondInr);
						    		  if(!strTypePercent.equalsIgnoreCase("null") && promotionSecondPercent!=0){
						    			  promotionSecondName=String.valueOf(Math.round(promotionSecondPercent))+" % OFF";
				  						  promoSecondPercent=(int) Math.round(promotionSecondPercent);
						    		  }
						    		  else if(!strTypeInr.equalsIgnoreCase("null") && promotionSecondInr!=0){
						    			  promotionSecondName="Flat "+String.valueOf(Math.round(promotionSecondInr))+" OFF";
				  						  promoSecondInr=(int) Math.round(promotionSecondInr);
						    		  }
		  							
		  						}
		  					}
		  					arlPromoType.clear();
		  				
		  					if(blnPromoFlag){
		  						if(promotionType.equalsIgnoreCase("L") || promotionType.equalsIgnoreCase("E") || promotionType.equalsIgnoreCase("F")){
		  							totalAmount=totalFlatAmount;
		  							
		  							if(countDate>1){
		  								actualAdultAmount=extraAdultAmount/countDate;
		  								actualChildAmount=extraChildAmount/countDate;
		  							}else{
		  								actualAdultAmount=extraAdultAmount;
		  								actualChildAmount=extraChildAmount;
		  							}
		  						}
		  					}else{
		  						totalAmount=baseAmount;
		  						
		  						if(countDate>1){
		  							actualAdultAmount=extraAdultAmount/countDate;
		  							actualChildAmount=extraChildAmount/countDate;
		  						}else{
		  							actualAdultAmount=extraAdultAmount;
		  							actualChildAmount=extraChildAmount;
		  						}
		  					}
		  					
		  					
		  					totalAmount=Math.round(totalAmount);
		  					actualTotalAdultAmount=Math.round(actualAdultAmount);
		  					actualTotalChildAmount=Math.round(actualChildAmount);
		  					
		  					PropertyRatePlanDetailManager planDetailController=new PropertyRatePlanDetailManager();
		 					
		 					ArrayList arlRateType=new ArrayList();
		 			        List<PropertyRatePlanDetail> listPlanDetails=planDetailController.listAccommodation(property.getPropertyId(), accommId);
		 			        if(listPlanDetails.size()>0 && !listPlanDetails.isEmpty()){
		 			        	for(PropertyRatePlanDetail ratePlanDetails:listPlanDetails){
		 			        		arlRateType.add(ratePlanDetails.getRatePlan().getRatePlanType());
		 			        		arlRateType.add(ratePlanDetails.getRatePlan().getRatePlanSymbolType());
		 			        		arlRateType.add(ratePlanDetails.getTariffAmount());
		 			        	}
		 			        }
		 			       String strType="EP",strSymbol=null;
		 	            	double dblRatePlanAmount=0;
		 			       Iterator iterRatePlan=arlRateType.iterator();
		 			       if(arlRateType.size()>0 && !arlRateType.isEmpty()){
		 			    	  while(iterRatePlan.hasNext()){
		 	 	            		strType=(String)iterRatePlan.next();
		 	 	            		strSymbol=(String)iterRatePlan.next();
		 	 	            		dblRatePlanAmount=(Double)iterRatePlan.next();
		 	 	            		
		 	 	            		if(strType.equalsIgnoreCase("CP")){
		 	 	            			jsonOutput += ",\"ratePlanType\":\"" + strType+"\"";
		 	 	            			
		 	 	                		if(strSymbol.equalsIgnoreCase("plus")){
		 	 	                			Double dblRate=0.0;
		 	 	                			dblRate=totalAmount+dblRatePlanAmount;
		 	 	                			jsonOutput += ",\"ratePlanAmount\":\"" + dblRate +  "\"";
		 	 	                		}else{
		 	 	                			Double dblRate=0.0;
		 	 	                			dblRate=totalAmount-dblRatePlanAmount;
		 	 	                			jsonOutput += ",\"ratePlanAmount\":\"" + dblRate +  "\"";
		 	 	                		}
		 	 	            		}
		 	 	            		
		 	 	            	}
		 			    	   
		 			       }else{
		 			    	  jsonOutput += ",\"ratePlanType\":\"" + strType +"\"";
		 			    	  jsonOutput += ",\"ratePlanAmount\":\"" + totalAmount +  "\"";
		 			       }
		  					
		  					long minimum = getAvailableCount(property.getPropertyId(),getArrivalDate(),getDepartureDate(),roomCount,accommId);
		  			    	
		  					 PropertyTaxeManager propertytaxController = new PropertyTaxeManager();
		  	  					 double taxe =  totalAmount/diffInDays;
		  	  					
		  						 PropertyTaxe tax = propertytaxController.find(taxe);
		  						 double taxes = Math.round(totalAmount * tax.getTaxPercentage() / 100  ) ;
		  	 			         jsonOutput += ",\"tax\":\"" + taxes + "\"";
		  	 			         jsonOutput += ",\"roomTax\":\"" + taxes + "\"";
		  	 			         jsonOutput += ",\"taxPercentage\":\"" + tax.getTaxPercentage() + "\"";
		  			    	 if(minimum==0){
		  			    		 jsonOutput += ",\"available\":\"" + minimum+ "\"";
		  			    		 jsonOutput += ",\"baseActualAmount\":\"" + + (actualBaseAmount==0 ? accommodations.getBaseAmount() : actualBaseAmount )+  "\"";
		  			    		 jsonOutput += ",\"baseAmount\":\"" + + (totalAmount==0 ? accommodations.getBaseAmount() : totalAmount )+  "\"";
		  			    		 jsonOutput += ",\"totalAmount\":\"" + + (totalAmount==0 ? accommodations.getBaseAmount() : totalAmount )+  "\"";
		  			    		 jsonOutput += ",\"accommodationName\":\"" + accommodations.getAccommodationType().toUpperCase()+ "\"";
		  						 jsonOutput += ",\"accommodationId\":\"" + accommodations.getAccommodationId()+ "\"";
		  						 jsonOutput += ",\"promotionFirstName\":\"" + (promotionFirstName==null? "None":promotionFirstName)+ "\"";
		  						 jsonOutput += ",\"promotionSecondName\":\"" + (promotionSecondName==null? "None":promotionSecondName)+ "\"";
		  						 jsonOutput += ",\"promoFirstPercent\":\"" + (promoFirstPercent==null? 0:promoFirstPercent)+ "\"";
		  						 jsonOutput += ",\"promoFirstInr\":\"" + (promoFirstInr==null? 0:promoFirstInr)+ "\"";
		 						 jsonOutput += ",\"promoSecondInr\":\"" + (promoSecondInr==null? 0:promoSecondInr)+ "\"";
		  						 jsonOutput += ",\"promoSecondPercent\":\"" + (promoSecondPercent==null? 0:promoSecondPercent)+ "\"";	
		  						jsonOutput += ",\"promoLastMinuteTimer\":\"" + (getDisplayHoursForTimer()==null ?"None" :getDisplayHoursForTimer()  )+ "\"";
		  				     }else{
		  				    	jsonOutput += ",\"accommodationName\":\"" + accommodations.getAccommodationType().toUpperCase()+ "\"";
		  						jsonOutput += ",\"accommodationId\":\"" + accommodations.getAccommodationId()+ "\"";
		  			    		jsonOutput += ",\"available\":\"" + minimum+ "\"";
		  			    		jsonOutput += ",\"maxAdult\":\"" + accommodations.getMaxOccupancy()+ "\"";
		  			    		jsonOutput += ",\"maxChild\":\"" + 0 + "\"";
		  			    		jsonOutput += ",\"adultsCount\":\"" + 1 + "\"";
		  						jsonOutput += ",\"childCount\":\"" + 0 + "\"";
		  						jsonOutput += ",\"rooms\":\"" + 1 + "\"";
		  						jsonOutput += ",\"noOfAdults\":\"" + accommodations.getNoOfAdults() + "\"";
		  						jsonOutput += ",\"noOfChild\":\"" + accommodations.getNoOfChild() + "\"";
		  			    		jsonOutput += ",\"minOccupancy\":\"" + accommodations.getMinOccupancy() + "\"";
		  						jsonOutput += ",\"maxOccupancy\":\"" + accommodations.getMaxOccupancy() + "\"";
		  						jsonOutput += ",\"baseActualAmount\":\"" +  (actualBaseAmount==0 ? accommodations.getBaseAmount() : actualBaseAmount )+  "\"";
		  						jsonOutput += ",\"promoFirstPercent\":\"" + (promoFirstPercent==null? 0:promoFirstPercent)+ "\"";
		  						jsonOutput += ",\"promoFirstInr\":\"" + (promoFirstInr==null? 0:promoFirstInr)+ "\"";
		 						jsonOutput += ",\"promoSecondInr\":\"" + (promoSecondInr==null? 0:promoSecondInr)+ "\"";
		  						jsonOutput += ",\"promoSecondPercent\":\"" + (promoSecondPercent==null? 0:promoSecondPercent)+ "\"";
		  						jsonOutput += ",\"baseAmount\":\"" + + (totalAmount==0 ? accommodations.getBaseAmount() : totalAmount )+  "\"";
		  			    		jsonOutput += ",\"totalAmount\":\"" + + (totalAmount==0 ? accommodations.getBaseAmount() : totalAmount )+  "\"";
		  			    		jsonOutput += ",\"discountBaseAmount\":\"" + + (totalAmount==0 ? accommodations.getBaseAmount() : totalAmount )+  "\"";
		  			    		jsonOutput += ",\"extraAdultAmount\":\"" + + (actualTotalAdultAmount==0 ? accommodations.getExtraAdult() : actualTotalAdultAmount )+  "\"";
		  			    		jsonOutput += ",\"extraChildAmount\":\"" + + (actualTotalChildAmount==0 ? accommodations.getExtraChild() : actualTotalChildAmount )+  "\"";
		  			    		jsonOutput += ",\"promotionFirstName\":\"" + (promotionFirstName==null? "None":promotionFirstName)+ "\"";
		 						jsonOutput += ",\"promotionSecondName\":\"" + (promotionSecondName==null? "None":promotionSecondName)+ "\"";
		  			    		jsonOutput += ",\"promotionId\":\"" + (promotionId==0? 0:promotionId) + "\"";
		  			    		jsonOutput += ",\"promoLastMinuteTimer\":\"" + (getDisplayHoursForTimer()==null ?"None" :getDisplayHoursForTimer()  )+ "\"";
		  						discountId=35;
		  						PropertyDiscountManager discountController=new PropertyDiscountManager();
		  						PropertyDiscount propertyDiscount=discountController.find(discountId);
		  			    		jsonOutput += ",\"discountPercent\":\"" + (propertyDiscount.getDiscountPercentage()==0.0? "0":propertyDiscount.getDiscountPercentage())+ "\"";
		  			    		jsonOutput += ",\"discountName\":\"" + propertyDiscount.getDiscountName()+ "\"";
		  			    		jsonOutput += ",\"discountId\":\"" + (discountId==0? "0" :discountId) + "\"";
		  			    		
		  			    		
		  				     }
		  			    	 
		  			    	//jsonOutput += ",\"tax\":\"" + 0 + "\"";
		  					String jsonAccommAmenities ="";
		  					
		  					List<AccommodationAmenity> accommodationAmenityList =  accommodationAmenityController.list(accommodations.getAccommodationId());
		  					if(propertyAmenityList.size()>0)
		  					{
		  						for (AccommodationAmenity amenity : accommodationAmenityList) {
		  							if (!jsonAccommAmenities.equalsIgnoreCase(""))
		  								
		  								jsonAccommAmenities += ",{";
		  							else
		  								jsonAccommAmenities += "{";
		  							
		  							PmsAmenity pmsAmenity1 =ameController.find(amenity.getPmsAmenity().getAmenityId());
		  							jsonAccommAmenities += "\"DT_RowId\":\"" + pmsAmenity1.getAmenityId()+ "\"";					
		  							jsonAccommAmenities += ",\"amenityName\":\"" + pmsAmenity1.getAmenityName()+ "\"";
		  							jsonAccommAmenities += ",\"amenityId\":\"" + pmsAmenity1.getAmenityId()+ "\"";
		  							jsonAccommAmenities += ",\"icon\":\"" + iconPath + pmsAmenity1.getAmenityIcon()+ "\"";
		  							
		  							jsonAccommAmenities += "}";
		  						}
		  						
		  						jsonOutput += ",\"accommodationamenities\":[" + jsonAccommAmenities+ "]";
		  					}
		  					
		  					
		  					//jsonOutput += ",\"available\":\"" + minimum + "\"";
		  					//jsonOutput += ",\"available\":\"" + (roomCount == null ? available.getAvailable() : (available.getAvailable()-roomCount.getRoomCount() )) + "\"";
		  				}
		  		     
		  			}
		  			jsonOutput += "}";
		  			
		  			arllist.clear();
		  		
				
		    	}
		    	
		    }
		    response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
		    
	   }catch(Exception ex){
		   logger.error(ex);
		   ex.printStackTrace();
	   }
	   return null;
   }
   
   public String getUloAvailability() throws IOException {
		try {
			DateFormat format = new SimpleDateFormat("MMM d,yyyy", Locale.ENGLISH);
		    java.util.Date dateStart = format.parse(getStartDate());
		    java.util.Date dateEnd = format.parse(getEndDate());
		   
		    DecimalFormat df = new DecimalFormat("###.##");
		    Calendar calCheckStart=Calendar.getInstance();
		    calCheckStart.setTime(dateStart);
		    java.util.Date checkInDate = calCheckStart.getTime();
		    this.arrivalDate=new Timestamp(checkInDate.getTime());
		   
		    
		    Calendar calCheckEnd=Calendar.getInstance();
		    calCheckEnd.setTime(dateEnd);
		    java.util.Date checkOutDate = calCheckEnd.getTime();
		    this.departureDate=new Timestamp(checkOutDate.getTime());
			
			Map mapDate=new HashMap();  
			mapDate.put("arrivalDate", arrivalDate);
			mapDate.put("departureDate", departureDate); 
		      
			this.propertyId = getPropertyId();
			sessionMap.put("uloPropertyId",propertyId); 
			
			String checkIn = new SimpleDateFormat("MMM d,yyyy").format(getArrivalDate());
		    String checkOut = new SimpleDateFormat("MMM d,yyyy").format(getDepartureDate());
		    sessionMap.put("checkIn",checkIn);
			sessionMap.put("checkOut",checkOut);
			 
			
		    Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		   
			Calendar cal = Calendar.getInstance();
			cal.setTime(this.departureDate);
	        cal.add(Calendar.DATE, -1);
			String startDate = new SimpleDateFormat("yyyy-MM-dd").format(this.arrivalDate);
			String endDate = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
			DateTime start = DateTime.parse(startDate);
	        DateTime end = DateTime.parse(endDate);
	        java.util.Date arrival = start.toDate();
			java.util.Date departure = end.toDate();
			String iconPath  = "ulowebsite/images/icons/";
			
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			AccommodationAmenityManager amenityController = new AccommodationAmenityManager();
			PmsAmenityManager ameController = new PmsAmenityManager();
			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
			PmsBookingManager bookingController = new PmsBookingManager();
			PropertyTaxeManager taxController = new PropertyTaxeManager();
			
			
	    	Double dblPercentFrom,dblPercentTo;
	    	Boolean isPositive=false,isNegative=false;
	    	List<PmsSmartPrice> listSmartPriceDetail=null; 
	    	List<AccommodationRoom> listAccommodationRoom=null;
	    	
			PmsSourceManager sourceController = new PmsSourceManager();
			PropertyRateManager rateController = new PropertyRateManager();
			PromotionManager promotionController= new PromotionManager();
			PromotionDetailManager promotionDetailController= new PromotionDetailManager();
			PmsPropertyManager propertyController=new PmsPropertyManager();
			PropertyRateDetailManager detailController = new PropertyRateDetailManager();
			PmsSource pmsSource = sourceController.find(1);
			SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
			response.setContentType("application/json");
			List<PmsPromotionDetails> listPromotionDetailCheck=null;
			this.availableList = bookingController.list(getPropertyId(),getArrivalDate(),getDepartureDate());
			List<DateTime> between = DateUtil.getDateRange(start, end);
          for (PmsAvailableRooms available : availableList) {
         
       	   	Boolean blnPromoFlag=false,promotionFlag=false,promoFlag=false,blnSmartPriceFlag=false;
				int promotionAccommId=0,promoAccommId=0,promotionHours=0,dateCount=0,roomCount=0,availableCount=0,totalCount=0;
				Integer smartPriceId =0,discountId=0,promoCount=0;
		    	Double dblPercentCount=0.0,dblSmartPricePercent=0.0, promotionPercent=0.0;
				ArrayList<String> arlPromoType=new ArrayList<String>();
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				
				else
					jsonOutput += "{";
				 jsonOutput += "\"propertyId\":\"" + getPropertyId() + "\"";
				 PmsProperty property=propertyController.find(getPropertyId());
				 jsonOutput += ",\"propertyName\":\"" + (property.getPropertyName() == null ? "": property.getPropertyName().toUpperCase().trim())+ "\"";
				 jsonOutput += ",\"payAtHotelStatus\":\"" + (property.getPayAtHotelStatus() == null ? false: property.getPayAtHotelStatus())+ "\"";
				 jsonOutput += ",\"coupleStatus\":\"" + ( property.getCoupleIsActive() == null? false :  property.getCoupleIsActive() )+ "\"";
				 jsonOutput += ",\"breakfastStatus\":\"" + ( property.getBreakfastIsActive() == null? false:  property.getBreakfastIsActive() )+ "\"";
				 jsonOutput += ",\"hotelStatus\":\"" + ( property.getHotelIsActive() == null? false:  property.getHotelIsActive() )+ "\"";
				 
				 //actual calculation 
				 double actualBaseAmount=0.0,flatAmount=0.0,actualTotal=0.0,lastAmount=0.0,promoBaseAmount=0.0,
						 promoAdultAmount=0.0,promoChildAmount=0.0,totalAmount=0;
				 double baseAmount=0.0,extraChildAmount=0.0,extraAdultAmount=0.0;
				 String promotionType="",promotionName="";
				 int lastMinuteHours=0, promotionId=0;
				 DateFormat f = new SimpleDateFormat("EEEE");
				 Timestamp lastMinStartDate,lastMinEndDate;
				 Double dblGetNights=0.0,dblBookNights=0.0,dblGetRooms=0.0,dblBookRooms=0.0;
				 Boolean blnBookedGetRooms=false,blnLastMinutePromotions=false;
				 double smartPricePercent=0.0,discountPercent=0.0,
						 actualAmount=0.0, actualTotalAdultAmount=0.0,actualAdultAmount=0.0,actualTotalChildAmount=0.0,
						 actualChildAmount=0.0,totalActualAmount=0.0;
				 
				//unchanged date
				SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
				Timestamp FromDate=(Timestamp)mapDate.get("arrivalDate");
				Timestamp ToDate=(Timestamp)mapDate.get("departureDate");
				
				String strFromDate=format2.format(FromDate);
				String strToDate=format2.format(ToDate);
				
		    	DateTime startdate = DateTime.parse(strFromDate);
		        DateTime enddate = DateTime.parse(strToDate);
		        java.util.Date fromdate = startdate.toDate();
		 		java.util.Date todate = enddate.toDate();
		 		Timestamp fromDateST=new Timestamp(fromdate.getTime());
		 		Timestamp toDateST=new Timestamp(todate.getTime());
				int diffInDays = (int) ((todate.getTime() - fromdate.getTime()) / (1000 * 60 * 60 * 24));
				jsonOutput += ",\"diffDays\":\"" + diffInDays+ "\"";
				
				
					String jsonReviews="";
					String jsonGuestReview="";
					PropertyReviewsManager reviewController=new PropertyReviewsManager();
					List<PropertyReviews> listReviews=reviewController.listReview(property.getPropertyId());
					if(listReviews.size()>0 && !listReviews.isEmpty()){
						for(PropertyReviews reviews:listReviews){
							
							if (!jsonReviews.equalsIgnoreCase(""))
								
								jsonReviews += ",{";
							else
								jsonReviews += "{";
							
							/*jsonReviews += "\"starCount\":\"" + reviews.getStarCount().intValue()+ "\"";	
							jsonReviews += ",\"reviewCount\":\"" + reviews.getReviewCount().intValue()+ "\"";	
							Double averageReviews=reviews.getAverageReview();
							jsonReviews += ",\"averageReview\":\"" + df.format(averageReviews)+ "\"";*/
							
							jsonReviews += "\"starCount\":\"" + (reviews.getStarCount()==null?0:reviews.getStarCount().intValue())+ "\"";	
							jsonReviews += ",\"reviewCount\":\"" + (reviews.getReviewCount()==null?0:reviews.getReviewCount().intValue())+ "\"";	
							if(reviews.getAverageReview()!=null){
								Double averageReviews=reviews.getAverageReview();
								jsonReviews += ",\"averageReview\":\"" + df.format(averageReviews)+ "\"";
							}else{
								jsonReviews += ",\"averageReview\":\"" + df.format(0)+ "\"";
							}
							
							jsonReviews += ",\"tripadvisorStarCount\":\"" + (reviews.getTripadvisorStarCount()==null?0:reviews.getTripadvisorStarCount().intValue())+ "\"";	
							jsonReviews += ",\"tripadvisorReviewCount\":\"" + (reviews.getTripadvisorReviewCount()==null?0:reviews.getTripadvisorReviewCount().intValue())+ "\"";
							if(reviews.getTripadvisorAverageReview()!=null){
								Double taAverageReviews=reviews.getTripadvisorAverageReview();
								jsonReviews += ",\"tripadvisorAverageReview\":\"" + df.format(taAverageReviews)+ "\"";
							}else{
								jsonReviews += ",\"tripadvisorAverageReview\":\"" + df.format(0)+ "\"";
							}
							jsonReviews += ",\"googleReviewUrl\":\"" + (reviews.getGoogleReviewUrl()==null?"":reviews.getGoogleReviewUrl())+ "\"";	
							jsonReviews += ",\"tripadvisorReviewUrl\":\"" + (reviews.getTripadvisorReviewUrl()==null?"":reviews.getTripadvisorReviewUrl())+ "\"";	
							jsonReviews += ",\"propertyRating\":\"" + (reviews.getPropertyRating()==null?"None":reviews.getPropertyRating())+ "\"";
							
							jsonReviews += "}";
						}
					}else{
						if (!jsonReviews.equalsIgnoreCase(""))
							
							jsonReviews += ",{";
						else
							jsonReviews += "{";
						
						jsonReviews += "\"starCount\":\"" + 0+ "\"";	
						jsonReviews += ",\"reviewCount\":\"" + 0+ "\"";	
						jsonReviews += ",\"averageReview\":\"" + 0+ "\"";
						
						jsonReviews += ",\"tripadvisorStarCount\":\"" + 0 + "\"";	
						jsonReviews += ",\"tripadvisorReviewCount\":\"" + 0 + "\"";	
						jsonReviews += ",\"tripadvisorAverageReview\":\"" + 0+ "\"";
						
						jsonReviews += ",\"googleReviewUrl\":\"" + "None"+ "\"";	
						jsonReviews += ",\"tripadvisorReviewUrl\":\"" + "None"+ "\"";	
						jsonReviews += ",\"propertyRating\":\"" + "None"+ "\"";
						
						jsonReviews += "}";
					}
					
					jsonOutput += ",\"reviews\":[" + jsonReviews+ "]";
					PropertyGuestReviewManager guestReviewController=new PropertyGuestReviewManager();
					List<PropertyGuestReview> listGuestReviews=guestReviewController.listReview(property.getPropertyId());
					if(listGuestReviews.size()>0 && !listGuestReviews.isEmpty()){
						for(PropertyGuestReview reviews:listGuestReviews){
							if (!jsonGuestReview.equalsIgnoreCase(""))
								
								jsonGuestReview += ",{";
							else
								jsonGuestReview += "{";
							
							jsonGuestReview += "\"reviewerGuestName\":\"" + (reviews.getReviewGuestName()==null?"":reviews.getReviewGuestName())+ "\"";	
							jsonGuestReview += ",\"reviewerDescription\":\"" + (reviews.getReviewDescription()==null?"":reviews.getReviewDescription())+ "\"";	
							jsonGuestReview += ",\"reviewerCount\":\"" + (reviews.getReviewCount()==null?0:reviews.getReviewCount())+ "\"";
							jsonGuestReview += ",\"reviewerName\":\"" + (reviews.getReviews().getReviewerName()==null?"":reviews.getReviews().getReviewerName())+ "\"";
							jsonGuestReview += ",\"reviewerId\":\"" + (reviews.getReviews().getReviewerId()==null?"":reviews.getReviews().getReviewerId())+ "\"";
							jsonGuestReview += "}";
						}
					}else{
						if (!jsonGuestReview.equalsIgnoreCase(""))
							
							jsonGuestReview += ",{";
						else
							jsonGuestReview += "{";
						
						jsonGuestReview += "\"reviewerGuestName\":\"" + ""+ "\"";	
						jsonGuestReview += ",\"reviewerDescription\":\"" + ""+ "\"";	
						jsonGuestReview += ",\"reviewerCount\":\"" + 0+ "\"";
						jsonGuestReview += ",\"reviewerName\":\"" + ""+ "\"";
						jsonGuestReview += ",\"reviewerId\":\"" + "" + "\"";
						
						jsonGuestReview += "}";
					}
				
					jsonOutput += ",\"guestReviews\":[" + jsonGuestReview+ "]";
				
				boolean blnActivePromo=false,blnNotActivePromo=false,basePromoFlag=false;
				 ArrayList<Integer> arlActivePromo=new ArrayList<Integer>(); 
				
				 for (DateTime d : between)
			     {
					 List<PmsPromotions> listAllPromotions= promotionController.listDatePromotions(property.getPropertyId(),d.toDate());
					 if(!listAllPromotions.isEmpty() && listAllPromotions.size()>0){
						 for(PmsPromotions promos:listAllPromotions){
							 if(promos.getPromotionType().equalsIgnoreCase("F")){
								 if(arlActivePromo.isEmpty()){
									 arlActivePromo.add(promos.getPromotionId());
								 }else if(!arlActivePromo.contains(promos.getPromotionId())){
									 arlActivePromo.add(promos.getPromotionId());
								 }
							 }else{
								 if(arlActivePromo.isEmpty()){
									 arlActivePromo.add(promos.getPromotionId());
								 }else if(!arlActivePromo.contains(promos.getPromotionId())){
									 arlActivePromo.add(promos.getPromotionId());
								 } 
							 }
						 }
					 }else{
						 blnNotActivePromo=true;
					 }
			     }
				 int intPromoSize=arlActivePromo.size();
				 if(arlActivePromo.isEmpty() || blnNotActivePromo){
					 blnActivePromo=false;
					 basePromoFlag=false;
				 }else{
					 blnActivePromo=true;
				 }
				 
			     for (DateTime d : between)
			     {
						boolean SmartPriceEnableCheck=false;
						if(blnActivePromo){
							List<PmsPromotions> listFlatPromotions= promotionController.listDatePromotions(property.getPropertyId(),d.toDate());
					    	if(listFlatPromotions.size()>0 && !listFlatPromotions.isEmpty()){
					    		for(PmsPromotions promotions:listFlatPromotions){
					    			 //PmsPromotions pmsPromotions=promotionController.find(promotions.getPromotionId());
					    			if(promotions.getPromotionType().equalsIgnoreCase("F")){
					    				promotionId=promotions.getPromotionId();
					    				listPromotionDetailCheck=promotionDetailController.listPromotionDetails(promotions.getPromotionId(),f.format(d.toDate()).toLowerCase());
						    			 if(listPromotionDetailCheck.size()>0 && !listPromotionDetailCheck.isEmpty()){
						    				 for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
					    						 if(promoCount==0){
					    							 promotionPercent=promoDetails.getPromotionPercentage();
						    						 promotionFlag=true;
								    				 blnPromoFlag=true;
								    				 promotionType=promotions.getPromotionType();
								    				 arlPromoType.add(promotionType);
					    						 }
					    						 promoCount++;
					    					 }
						    			 }
					    			}else{
					    				List<PmsPromotions> listPmsPromotions= promotionController.listDatePromotions(property.getPropertyId(),available.getAccommodationId(),d.toDate());
						    			 if(listPmsPromotions.size()>0 &&!listPmsPromotions.isEmpty()){
						    				 for(PmsPromotions pmsPromotions:listPmsPromotions){
							    				 promotionId=pmsPromotions.getPromotionId();
							    				 //promotionType=pmsPromotions.getPromotionType();
							    				 
							    				 promoAccommId=pmsPromotions.getPropertyAccommodation().getAccommodationId();
								    			 listPromotionDetailCheck=promotionDetailController.listPromotionDetails(pmsPromotions.getPromotionId(),f.format(d.toDate()).toLowerCase());

							    				 if(listPromotionDetailCheck.size()>0 && !listPromotionDetailCheck.isEmpty()){
								        			 if(listPromotionDetailCheck.size()>1){
									    				 promotionFlag=false;
									    				 promotionType=null;
									    				 blnPromoFlag=false;
									    				 basePromoFlag=true;
									    			 }else if(listPromotionDetailCheck.size()==1){
									    				 if(!pmsPromotions.getPromotionType().equalsIgnoreCase("F")){
										    				 if(promoAccommId==available.getAccommodationId()){
										    					 promotionAccommId=promoAccommId;
										    					 lastMinStartDate=pmsPromotions.getStartDate();
											    				 lastMinEndDate=pmsPromotions.getEndDate();
											    				 Double nightsGet=0.0,nightsBook=0.0,totalGetNights;
											    				 if(pmsPromotions.getPromotionType().equalsIgnoreCase("N")){
											    					 for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
											    						 nightsBook=promoDetails.getBookNights();
											    						 nightsGet=promoDetails.getGetNights();
											    						 totalGetNights=nightsBook+nightsGet;
										    			    		     Integer intTotalGetNights=totalGetNights.intValue()-1;
										    			    			 Calendar calCheckOut=Calendar.getInstance();
										    			    			 calCheckOut.setTime(getDepartureDate());
										    			    			 calCheckOut.add(Calendar.DATE,intTotalGetNights);
										    			    			 java.util.Date checkOutDte = calCheckOut.getTime();             
										    			    			 String departureDate = format1.format(checkOutDte);
										    							 
										    			    			 java.util.Date fromDate = format1.parse(departureDate);
										    			    			 Timestamp tsDepartureDate=new Timestamp(fromDate.getTime());
										    			    			 String strStartDate=format1.format(getArrivalDate());
										    			    			 String strEndDate=format1.format(tsDepartureDate);
										    			    			 DateTime Startdate = DateTime.parse(strStartDate);
										    			    			 DateTime Enddate = DateTime.parse(strEndDate);
										    			    			 java.util.Date StartDate = Startdate.toDate();
									    			    		 		 java.util.Date EndDate = Enddate.toDate();
									    			    		 		 int diffDays = (int) ((EndDate.getTime() - StartDate.getTime()) / (1000 * 60 * 60 * 24));
										    			    			 
										    			    			 long minimumCount = getAvailableCount(getPropertyId(),getArrivalDate(),tsDepartureDate,available.getRoomAvailable(),promoAccommId);
										    			    			 
												    					 if((int)minimumCount>0){
												    						 if(nightsBook.intValue()==diffInDays){
													    						 promotionFlag=true;
													    						 blnPromoFlag=true;
													    						 promotionType=pmsPromotions.getPromotionType();
													    						 arlPromoType.add(promotionType);
													    					 }else{
													    						 promotionFlag=false;
													    						 promotionType=null;
													    						 blnPromoFlag=false;
													    						 basePromoFlag=true;
													    					 }
												    					 }else{
												    						 promotionFlag=false;
												    						 promotionType=null;
												    						 blnPromoFlag=false;
												    						 basePromoFlag=true;
												    					 }
												    				 }
											    				 }else if(pmsPromotions.getPromotionType().equalsIgnoreCase("R")){
												    				 for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
											    						 dblGetRooms=promoDetails.getGetRooms();
											    						 dblBookRooms=promoDetails.getBookRooms();
											    					 }
									    							 
									    							 long minimum = getAvailableCount(getPropertyId(),getArrivalDate(),getDepartureDate(),available.getRoomAvailable(),promoAccommId); 
									    							 
									    							 blnBookedGetRooms=bookGetRooms((int)minimum,dblGetRooms,dblBookRooms);
								    						    	 if(!blnBookedGetRooms){
								    						    		 promotionFlag=false;
								    						    		 promotionType=null;
								    						    		 blnPromoFlag=false;
								    						    		 basePromoFlag=true;
								    						    	 }else{
								    						    		 promotionFlag=true;
								    						    		 blnPromoFlag=true;
								    						    		 promotionType=pmsPromotions.getPromotionType();
								    						    		 arlPromoType.add(promotionType);
								    						    	 }
											    				 }else if(pmsPromotions.getPromotionType().equalsIgnoreCase("L")){
											    					 for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
											    						 lastMinuteHours=promoDetails.getPromotionHours();
											    						 promotionPercent=promoDetails.getPromotionPercentage();
											    					 }
											    					
											    					 long minimumCount = getAvailableCount(getPropertyId(),getArrivalDate(),getDepartureDate(),available.getRoomAvailable(),promoAccommId);
									    			    			 if((int)minimumCount>0){
									    			    				 blnLastMinutePromotions=getCurrentAvailable(getPropertyId(),getArrivalDate(),getDepartureDate(),lastMinuteHours,(long)available.getRoomAvailable(),promoAccommId,lastMinStartDate,lastMinEndDate);
									    			    				 if(blnLastMinutePromotions){
									    			    					 promotionFlag=true; 
									    			    					 blnPromoFlag=true;
									    			    					 promotionType=pmsPromotions.getPromotionType();
									    			    					 arlPromoType.add(promotionType);
									    			    				 }
									    			    			 }else{
									    			    				 promotionFlag=false;
									    			    				 promotionType=null;
									    			    				 blnPromoFlag=false;
									    			    				 basePromoFlag=true;
									    			    			 }
											    				 }
										    				 }
									    				 }
									    			 }
								        		 }
							    			 }
						    			 }
					    			}
					    		}
					    	}
					    	if(arlPromoType.contains("F")){
			    			    promoFlag=true;
			    			    int rateCount=0,rateIdCount=0;
							List<PropertyRate> propertyRateList =  rateController.listAllDates(getPropertyId(),available.getAccommodationId(),pmsSource.getSourceId(),d.toDate());
							List<PropertyRate> currentDateSmartPriceActive=rateController.listCurrentSmartPriceActive(getPropertyId(),available.getAccommodationId(), d.toDate());
					    	if(propertyRateList.size()>0)
					    	{
					    		 for (PropertyRate pr : propertyRateList)
				    			 {
					    				 int propertyRateId = pr.getPropertyRateId();
					    				 Boolean SmartPriceCheck=pr.getSmartPriceIsActive();
							    		 if(SmartPriceCheck){
							    			 List<PropertyRateDetail> rateDetailList =   detailController.list(propertyRateId,f.format(d.toDate()).toLowerCase());
								    		 if(rateDetailList.size()>0){
								    			 for (PropertyRateDetail rate : rateDetailList){
								    				 if(rateCount<1){
								    					 SmartPriceEnableCheck=true;
								    					 baseAmount =  rate.getBaseAmount();
									    				 extraAdultAmount+=rate.getExtraAdult();
									    				 extraChildAmount+=rate.getExtraChild();
									    				 rateCount++;
								    				 }
								    			 }
								    		 }
							    		 }else{
							    			 List<PropertyRateDetail> rateDetailList =   detailController.list(propertyRateId,f.format(d.toDate()).toLowerCase());
		        				    		 if(rateDetailList.size()>0){
		        				    			 if(rateCount<1){
		        				    				 for (PropertyRateDetail rate : rateDetailList){
			        				    				 if(currentDateSmartPriceActive.size()>0){
			        				    					 SmartPriceEnableCheck=true;
			        				    					 baseAmount =  rate.getBaseAmount();
		    	        				    				 extraAdultAmount+=rate.getExtraAdult();
		    	        				    				 extraChildAmount+=rate.getExtraChild();
			        				    				 }else{
			        				    					 baseAmount =  rate.getBaseAmount();
		    	        				    				 extraAdultAmount+=rate.getExtraAdult();
		    	        				    				 extraChildAmount+=rate.getExtraChild();
			        				    				 }
			        				    			 }
		        				    				 rateCount++;
		        				    			 }
		        				    		 }else{
		        				    			 if(propertyRateList.size()==rateIdCount){
		        				    				 baseAmount = available.getBaseAmount();
			            				    		 extraAdultAmount+=available.getExtraAdult();
			        			    				 extraChildAmount+=available.getExtraChild();
		        				    			 }
		            				    	 }
							    		 }
				    			 }	    		 
					    	 }
					    	 else{
					    		 baseAmount = available.getBaseAmount();
					    		 extraAdultAmount+=available.getExtraAdult();
				    			 extraChildAmount+=available.getExtraChild();
					    	 }
					    	actualBaseAmount+=baseAmount;
					    	String strTypePercent=String.valueOf(promotionPercent);
					    		if(!strTypePercent.equalsIgnoreCase("null")){
					    		   flatAmount = baseAmount-(baseAmount*promotionPercent/100);
					    		}
					    		else{
					    		   flatAmount = baseAmount;
					    		} 
					    		
					    		flatAmount=Math.round(flatAmount);
					    		actualTotal=flatAmount;
			    		  }else if(arlPromoType.contains("L") || arlPromoType.contains("N") || arlPromoType.contains("R")){
					    		promoFlag=true;
					    		List<PmsPromotions> listPromotions= promotionController.listDatePromotions(getPropertyId(), promoAccommId, d.toDate());
								if(listPromotions.size()>0 && !listPromotions.isEmpty()){
						    		 for(PmsPromotions pmspromotions:listPromotions){
						    			 listPromotionDetailCheck.clear();
						    			 listPromotionDetailCheck=promotionDetailController.listPromotionDetails(pmspromotions.getPromotionId(),f.format(d.toDate()).toLowerCase());
						    			 if(listPromotionDetailCheck.size()>0 && !listPromotionDetailCheck.isEmpty()){
						    				 	promotionId=pmspromotions.getPromotionId();
						    				 	promotionType=pmspromotions.getPromotionType();
						    				 	if(promotionType.equalsIgnoreCase("L")){
							    					 listPromotionDetailCheck=promotionDetailController.listPromotionDetails(promotionId,f.format(d.toDate()).toLowerCase());
										    		  if(listPromotionDetailCheck.size()>0){
							    						 for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
							    							 promotionPercent=promoDetails.getPromotionPercentage();
							    							 promotionHours=promoDetails.getPromotionHours();
							    						 }
										    		} 
							    					int rateCount=0,rateIdCount=0;
						    						List<PropertyRate> propertyRateList =  rateController.listAllDates(getPropertyId(),available.getAccommodationId(),pmsSource.getSourceId(),d.toDate());
						    						List<PropertyRate> currentDateSmartPriceActive=rateController.listCurrentSmartPriceActive(getPropertyId(),available.getAccommodationId(), d.toDate());
						    				    	if(propertyRateList.size()>0)
						    				    	{
						    				    		 for (PropertyRate pr : propertyRateList)
						    			    			 {
										    				 int propertyRateId = pr.getPropertyRateId();
						        				    		 Boolean SmartPriceCheck=pr.getSmartPriceIsActive();
												    		 if(SmartPriceCheck){
												    			 List<PropertyRateDetail> rateDetailList =   detailController.list(propertyRateId,f.format(d.toDate()).toLowerCase());
													    		 if(rateDetailList.size()>0){
													    			 for (PropertyRateDetail rate : rateDetailList){
													    				 if(rateCount<1){
													    					 SmartPriceEnableCheck=true;
													    					 baseAmount =  rate.getBaseAmount();
														    				 extraAdultAmount+=rate.getExtraAdult();
														    				 extraChildAmount+=rate.getExtraChild();
														    				 rateCount++;
													    				 }
													    			 }
													    		 }
												    		 }else{
												    			 List<PropertyRateDetail> rateDetailList =   detailController.list(propertyRateId,f.format(d.toDate()).toLowerCase());
							        				    		 if(rateDetailList.size()>0){
							        				    			 if(rateCount<1){
							        				    				 for (PropertyRateDetail rate : rateDetailList){
								        				    				 if(currentDateSmartPriceActive.size()>0){
								        				    					 SmartPriceEnableCheck=true;
								        				    					 baseAmount =  rate.getBaseAmount();
							    	        				    				 extraAdultAmount+=rate.getExtraAdult();
							    	        				    				 extraChildAmount+=rate.getExtraChild();
								        				    				 }else{
								        				    					 baseAmount =  rate.getBaseAmount();
							    	        				    				 extraAdultAmount+=rate.getExtraAdult();
							    	        				    				 extraChildAmount+=rate.getExtraChild();
								        				    				 }
								        				    			 }
							        				    				 rateCount++;
							        				    			 }
							        				    		 }else{
							        				    			 if(propertyRateList.size()==rateIdCount){
							        				    				 baseAmount = available.getBaseAmount();
								            				    		 extraAdultAmount+=available.getExtraAdult();
								        			    				 extraChildAmount+=available.getExtraChild();
							        				    			 }
							            				    	 }
												    		 }
						    			    			 }	    		 
						    				    	 }
						    				    	 else{
						    				    		 baseAmount = available.getBaseAmount();
						    				    		 extraAdultAmount+=available.getExtraAdult();
									    				 extraChildAmount+=available.getExtraChild();
						    				    	 }
						    				    	actualBaseAmount+=baseAmount;
						    				    	String strTypePercent=String.valueOf(promotionPercent);
											    	 if(!strTypePercent.equalsIgnoreCase("null")){
											    		 lastAmount = baseAmount-(baseAmount*promotionPercent/100);
											    	 }
											    	 else{
											    		 lastAmount = baseAmount;
											    	 } 
											    	 lastAmount=Math.round(lastAmount);
											    	 actualTotal=lastAmount;
						    				 }else if(promotionType.equalsIgnoreCase("R")){
						    					 listPromotionDetailCheck=promotionDetailController.listPromotionDetails(promotionId,f.format(d.toDate()).toLowerCase());
						    					 if(listPromotionDetailCheck.size()>0){
						    						 for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
						    							 dblBookRooms=promoDetails.getBookRooms();
						    							 dblGetRooms=promoDetails.getGetRooms();
						    							 promoBaseAmount =promoDetails.getBaseAmount();
						    							 promoAdultAmount =promoDetails.getExtraAdult();
						    							 promoChildAmount =promoDetails.getExtraChild(); 
						    						 }
						    					 }
						    				 }else if(promotionType.equalsIgnoreCase("N")){
						    					 listPromotionDetailCheck=promotionDetailController.listPromotionDetails(promotionId,f.format(d.toDate()).toLowerCase());
						    					 if(listPromotionDetailCheck.size()>0){
						    						 for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
						    							 dblBookNights=promoDetails.getBookNights();
						    							 dblGetNights=promoDetails.getGetNights();
						    							 promoBaseAmount =promoDetails.getBaseAmount();
						    							 promoAdultAmount =promoDetails.getExtraAdult();
						    							 promoChildAmount =promoDetails.getExtraChild(); 
						    						 }
						    					 }
						    				 }
						    			 }
						    		 }
								}
					    	}else{
					    		if(basePromoFlag){
							    	baseAmount = available.getBaseAmount();
							    	extraAdultAmount+=available.getExtraAdult();
						    		extraChildAmount+=available.getExtraChild();
							    	actualBaseAmount+=baseAmount;
							    	actualTotal=baseAmount;
								}else{
									int rateCount=0,rateIdCount=0;
									List<PropertyRate> propertyRateList =  rateController.listAllDates(getPropertyId(),available.getAccommodationId(),pmsSource.getSourceId(),d.toDate());
									List<PropertyRate> currentDateSmartPriceActive=rateController.listCurrentSmartPriceActive(getPropertyId(),available.getAccommodationId(), d.toDate());
							    	if(propertyRateList.size()>0)
							    	{
							    		 for (PropertyRate pr : propertyRateList)
						    			 {
							    				 int propertyRateId = pr.getPropertyRateId();
							    				 Boolean SmartPriceCheck=pr.getSmartPriceIsActive();
									    		 if(SmartPriceCheck){
									    			
									    			 List<PropertyRateDetail> rateDetailList =   detailController.list(propertyRateId,f.format(d.toDate()).toLowerCase());
										    		 if(rateDetailList.size()>0){
										    			 for (PropertyRateDetail rate : rateDetailList){
										    				 if(rateCount<1){
										    					 SmartPriceEnableCheck=true;
										    					 baseAmount =  rate.getBaseAmount();
											    				 extraAdultAmount+=rate.getExtraAdult();
											    				 extraChildAmount+=rate.getExtraChild();
											    				 rateCount++;
										    				 }
										    			 }
										    		 }
									    		 }else{
									    			 List<PropertyRateDetail> rateDetailList =   detailController.list(propertyRateId,f.format(d.toDate()).toLowerCase());
				        				    		 if(rateDetailList.size()>0){
				        				    			 if(rateCount<1){
				        				    				 for (PropertyRateDetail rate : rateDetailList){
					        				    				 if(currentDateSmartPriceActive.size()>0){
					        				    					 SmartPriceEnableCheck=true;
					        				    					 baseAmount =  rate.getBaseAmount();
				    	        				    				 extraAdultAmount+=rate.getExtraAdult();
				    	        				    				 extraChildAmount+=rate.getExtraChild();
					        				    				 }else{
					        				    					 baseAmount =  rate.getBaseAmount();
				    	        				    				 extraAdultAmount+=rate.getExtraAdult();
				    	        				    				 extraChildAmount+=rate.getExtraChild();
					        				    				 }
					        				    			 }
				        				    				 rateCount++;
				        				    			 }
				        				    		 }else{
				        				    			 if(propertyRateList.size()==rateIdCount){
				        				    				 baseAmount = available.getBaseAmount();
					            				    		 extraAdultAmount+=available.getExtraAdult();
					        			    				 extraChildAmount+=available.getExtraChild();
				        				    			 }
				            				    	 }
									    		 }
						    			 }	    		 
							    	 }
							    	 else{
							    		 baseAmount = available.getBaseAmount();
							    		 extraAdultAmount+=available.getExtraAdult();
						    			 extraChildAmount+=available.getExtraChild();
							    	 }
							    	actualBaseAmount+=baseAmount;
							    	actualTotal=baseAmount;
								}
					    		
					    	}
						}else{
							if(basePromoFlag){
						    	baseAmount = available.getBaseAmount();
						    	extraAdultAmount+=available.getExtraAdult();
					    		extraChildAmount+=available.getExtraChild();
						    	actualBaseAmount+=baseAmount;
						    	actualTotal=baseAmount;
							}else{
								int rateCount=0,rateIdCount=0;
								List<PropertyRate> propertyRateList =  rateController.listAllDates(getPropertyId(),available.getAccommodationId(),pmsSource.getSourceId(),d.toDate());
								List<PropertyRate> currentDateSmartPriceActive=rateController.listCurrentSmartPriceActive(getPropertyId(),available.getAccommodationId(), d.toDate());
						    	if(propertyRateList.size()>0)
						    	{
						    		 for (PropertyRate pr : propertyRateList)
					    			 {
						    				 int propertyRateId = pr.getPropertyRateId();
						    				 Boolean SmartPriceCheck=pr.getSmartPriceIsActive();
								    		 if(SmartPriceCheck){
								    			
								    			 List<PropertyRateDetail> rateDetailList =   detailController.list(propertyRateId,f.format(d.toDate()).toLowerCase());
									    		 if(rateDetailList.size()>0){
									    			 for (PropertyRateDetail rate : rateDetailList){
									    				 if(rateCount<1){
									    					 SmartPriceEnableCheck=true;
									    					 baseAmount =  rate.getBaseAmount();
										    				 extraAdultAmount+=rate.getExtraAdult();
										    				 extraChildAmount+=rate.getExtraChild();
										    				 rateCount++;
									    				 }
									    			 }
									    		 }
								    		 }else{
								    			 List<PropertyRateDetail> rateDetailList =   detailController.list(propertyRateId,f.format(d.toDate()).toLowerCase());
			        				    		 if(rateDetailList.size()>0){
			        				    			 if(rateCount<1){
			        				    				 for (PropertyRateDetail rate : rateDetailList){
				        				    				 if(currentDateSmartPriceActive.size()>0){
				        				    					 SmartPriceEnableCheck=true;
				        				    					 baseAmount =  rate.getBaseAmount();
			    	        				    				 extraAdultAmount+=rate.getExtraAdult();
			    	        				    				 extraChildAmount+=rate.getExtraChild();
				        				    				 }else{
				        				    					 baseAmount =  rate.getBaseAmount();
			    	        				    				 extraAdultAmount+=rate.getExtraAdult();
			    	        				    				 extraChildAmount+=rate.getExtraChild();
				        				    				 }
				        				    			 }
			        				    				 rateCount++;
			        				    			 }
			        				    		 }else{
			        				    			 if(propertyRateList.size()==rateIdCount){
			        				    				 baseAmount = available.getBaseAmount();
				            				    		 extraAdultAmount+=available.getExtraAdult();
				        			    				 extraChildAmount+=available.getExtraChild();
			        				    			 }
			            				    	 }
								    		 }
					    			 }	    		 
						    	 }
						    	 else{
						    		 baseAmount = available.getBaseAmount();
						    		 extraAdultAmount+=available.getExtraAdult();
					    			 extraChildAmount+=available.getExtraChild();
						    	 }
						    	actualBaseAmount+=baseAmount;
						    	actualTotal=baseAmount;
							}
				    	}
				    	double smartPriceAmount=0.0;
				    	if(SmartPriceEnableCheck){
				    		 listAccommodationRoom=accommodationController.listPropertyTotalRooms(getPropertyId(), available.getAccommodationId());
		      				 if(listAccommodationRoom.size()>0 && !listAccommodationRoom.isEmpty()){
		      					 for(AccommodationRoom roomsList:listAccommodationRoom){
		      						 roomCount=(int) roomsList.getRoomCount();
		      						 long minimum = getAvailableCount(getPropertyId(),getArrivalDate(),getDepartureDate(),available.getRoomAvailable(),available.getAccommodationId()); 
		      						 availableCount=(int)minimum;
		      						 dblPercentCount=(double)((roomCount-availableCount)*100)/roomCount;
		      						 totalCount=(int) Math.round(dblPercentCount);
		      						 listSmartPriceDetail=detailController.listSmartPriceDetail(totalCount);
		      						 if(listSmartPriceDetail.size()>0 && !listSmartPriceDetail.isEmpty()){
		      							 for(PmsSmartPrice smartPrice: listSmartPriceDetail){
		      								 dblSmartPricePercent=smartPrice.getAmountPercent();
		      								 dblPercentFrom=smartPrice.getPercentFrom();
		      								 dblPercentTo=smartPrice.getPercentTo();
		      								 sessionMap.put("baseTotalAmount", actualTotal);
				    				    	 sessionMap.put("smartPriceId", smartPrice.getSmartPriceId());
				    				    	 sessionMap.put("smartPricePercent", dblSmartPricePercent);
		      								 if(dblPercentFrom>=0 && dblPercentTo<=59){
		      									 isNegative=true;
		      								 }else if(dblPercentFrom>=60 && dblPercentTo<=99){
		      									 isPositive=true;
		      								 }
		      								 if(isNegative){
		      									smartPriceAmount=actualTotal-(actualTotal*dblSmartPricePercent/100);
		      								 }
		      								 if(isPositive){
		      									smartPriceAmount=actualTotal+(actualTotal*dblSmartPricePercent/100);
		      								 }
		      							 }
		      						 }
		      					 }
		      				 }
		      				smartPriceAmount=Math.round(smartPriceAmount);
		      				 blnSmartPriceFlag=true;
				    	 }
						 if(smartPriceAmount>0){
							 actualAmount+=smartPriceAmount;
						 }else if(lastAmount>0){
							 actualAmount+=lastAmount;
						 }else if(flatAmount>0){
							 actualAmount+=flatAmount;
						 }else{
							 actualAmount+=baseAmount;
						 }
				    	 dateCount++;
			     
			     }
			     
			     Iterator<String> iterPromoType=arlPromoType.iterator();
					while(iterPromoType.hasNext()){
						String strPromoType=iterPromoType.next();
						if(strPromoType.equalsIgnoreCase("F")){
							promotionName=String.valueOf(Math.round(promotionPercent))+" % OFF";
						}else if(strPromoType.equalsIgnoreCase("R")){
							promotionName="Book "+Math.round(dblBookRooms)+" and Get "+Math.round(dblGetRooms)+" Rooms FREE";
						}else if(strPromoType.equalsIgnoreCase("N")){
							promotionName="Book "+Math.round(dblBookNights)+" and Get "+Math.round(dblGetNights)+" Nights FREE";
						}else if(strPromoType.equalsIgnoreCase("L")){
							promotionName=String.valueOf(Math.round(promotionPercent))+" % OFF";
						}
					}
					arlPromoType.clear();
					
					if(promoFlag){
						if(promotionType.equalsIgnoreCase("R") || promotionType.equalsIgnoreCase("N")){
							totalAmount=promoBaseAmount;
							actualAdultAmount=promoAdultAmount;
							actualChildAmount=promoChildAmount; 
						}else if(promotionType.equalsIgnoreCase("F")){
							totalAmount=actualAmount;
							if(dateCount>1){
								//modified by gopi
								//actualAdultAmount=extraAdultAmount/dateCount;
								//actualChildAmount=extraChildAmount/dateCount;
								actualAdultAmount=extraAdultAmount;
	  							actualChildAmount=extraChildAmount;
							}else{
								actualAdultAmount=extraAdultAmount;
								actualChildAmount=extraChildAmount;
							}
						}
						else if(promotionType.equalsIgnoreCase("L")){
							totalAmount=actualAmount;
							if(dateCount>1){
							//modified bby gopi
								//actualAdultAmount=extraAdultAmount/dateCount;
								//actualChildAmount=extraChildAmount/dateCount;
								actualAdultAmount=extraAdultAmount;
	  							actualChildAmount=extraChildAmount;
							}else{
								actualAdultAmount=extraAdultAmount;
								actualChildAmount=extraChildAmount;
							}
						}
					}else{
						/*if(blnSmartPriceFlag){
							totalAmount=totalSmartPriceAmount;
						}else{
							totalAmount=baseAmount;
						}*/
						totalAmount=actualAmount;
						if(dateCount>1){
							//modified by gopi
							//actualAdultAmount=extraAdultAmount/dateCount;
							//actualChildAmount=extraChildAmount/dateCount;
							actualAdultAmount=extraAdultAmount;
  							actualChildAmount=extraChildAmount;
						}else{
							actualAdultAmount=extraAdultAmount;
							actualChildAmount=extraChildAmount;
						}
					}
					
					
					totalAmount=Math.round(totalAmount);
					actualTotalAdultAmount=Math.round(actualAdultAmount);
					actualTotalChildAmount=Math.round(actualChildAmount);
			    
			     
				
				
				jsonOutput += ",\"baseAmount\":\"" + (totalAmount==0 ? available.getBaseAmount() : totalAmount )+ "\"";
				jsonOutput += ",\"totalAmount\":\"" + (totalAmount==0 ? available.getBaseAmount() : totalAmount ) + "\"";
				jsonOutput += ",\"arrivalDate\":\"" + format2.format(getArrivalDate())+ "\"";
				jsonOutput += ",\"departureDate\":\"" + format2.format(getDepartureDate())+ "\"";
				jsonOutput += ",\"baseTotalAmount\":\"" + (actualBaseAmount==0 ? available.getBaseAmount() : actualBaseAmount )+ "\"";
				jsonOutput += ",\"size\":\"" +  available.getAreaSqft() + "\"";
				jsonOutput += ",\"rooms\":\"" + 1 + "\"";
				
				jsonOutput += ",\"accommodationId\":\"" + available.getAccommodationId() + "\"";
				jsonOutput += ",\"accommodationType\":\"" + available.getAccommodationType().toUpperCase() + "\"";
				jsonOutput += ",\"minOccupancy\":\"" + available.getMinOccupancy() + "\"";
				jsonOutput += ",\"maxOccupancy\":\"" + available.getMaxOccupancy() + "\"";
				jsonOutput += ",\"maxAdult\":\"" + available.getMaxOccupancy() + "\"";
				jsonOutput += ",\"maxChild\":\"" + 0 + "\"";
				jsonOutput += ",\"adultsCount\":\"" + 1 + "\"";
				jsonOutput += ",\"childCount\":\"" + 0 + "\"";
				jsonOutput += ",\"noOfAdults\":\"" + available.getNoOfAdults() + "\"";
				jsonOutput += ",\"noOfChild\":\"" + available.getNoOfChild() + "\"";
				jsonOutput += ",\"extraAdultAmount\":\"" + (actualTotalAdultAmount==0 ? available.getExtraAdult() : actualTotalAdultAmount ) + "\"";
				jsonOutput += ",\"extraChildAmount\":\"" + (actualTotalChildAmount==0 ? available.getExtraChild() : actualTotalChildAmount )+ "\"";
				jsonOutput += ",\"totalActualAmount\":\"" + + (totalActualAmount==0 ? available.getBaseAmount() : totalActualAmount )+  "\"";
	    		jsonOutput += ",\"totalActualExtraAdultAmount\":\"" + + (actualTotalAdultAmount==0 ? available.getExtraAdult() : actualTotalAdultAmount )+  "\"";
	    		jsonOutput += ",\"totalActualExtraChildAmount\":\"" + + (actualTotalChildAmount==0 ? available.getExtraChild() : actualTotalChildAmount )+  "\"";
	    		jsonOutput += ",\"bookRooms\":\"" + (dblBookRooms==0.0? 0:dblBookRooms.intValue())+ "\"";
	    		jsonOutput += ",\"getRooms\":\"" + (dblGetRooms==0? 0:dblGetRooms.intValue()) + "\"";
	    		jsonOutput += ",\"getNights\":\"" + (dblGetNights==0.0? 0 :dblGetNights.intValue())+ "\"";
	    		jsonOutput += ",\"bookNights\":\"" + (dblBookNights==0? 0:dblBookNights.intValue()) + "\"";
	    		jsonOutput += ",\"promotionId\":\"" + (promotionId==0? "0" :promotionId) + "\"";
	    		jsonOutput += ",\"promotions\":\"" + promotionName + "\"";
				if(blnSmartPriceFlag){
	    			smartPricePercent=(Double)sessionMap.get("smartPricePercent");
	    			smartPriceId=(Integer)sessionMap.get("smartPriceId");
					
					jsonOutput += ",\"smartPricePercent\":\"" + (smartPricePercent==0.0? "0" :smartPricePercent)+ "\"";
		    		jsonOutput += ",\"smartPriceId\":\"" + (smartPriceId==0? "0" :smartPriceId) + "\"";
	    		}else{
	    			jsonOutput += ",\"smartPricePercent\":\"" + (smartPricePercent==0.0? "0":smartPricePercent)+ "\"";
		    		jsonOutput += ",\"smartPriceId\":\"" + (smartPriceId==0? "0" :smartPriceId) + "\"";
	    		}
		    	
				discountId=35;
				PropertyDiscountManager discountController=new PropertyDiscountManager();
				PropertyDiscount propertyDiscount=discountController.find(discountId);
	    		jsonOutput += ",\"discountPercent\":\"" + (propertyDiscount.getDiscountPercentage()==0.0? "0":propertyDiscount.getDiscountPercentage())+ "\"";
	    		jsonOutput += ",\"discountId\":\"" + (discountId==0? "0" :discountId) + "\"";
	    		
				jsonOutput += ",\"tax\":\"" + 0 + "\"";
				String jsonAmenities ="";
				
				List<AccommodationAmenity> propertyAmenityList =  amenityController.list(available.getAccommodationId());
				if(propertyAmenityList.size()>0)
				{
					for (AccommodationAmenity amenity : propertyAmenityList) {
						if (!jsonAmenities.equalsIgnoreCase(""))
							
							jsonAmenities += ",{";
						else
							jsonAmenities += "{";
						
						PmsAmenity pmsAmenity1 =ameController.find(amenity.getPmsAmenity().getAmenityId());
						jsonAmenities += "\"DT_RowId\":\"" + pmsAmenity1.getAmenityId()+ "\"";					
						jsonAmenities += ",\"amenityName\":\"" + pmsAmenity1.getAmenityName()+ "\"";
						jsonAmenities += ",\"amenityId\":\"" + pmsAmenity1.getAmenityId()+ "\"";
						jsonAmenities += ",\"icon\":\"" + iconPath + pmsAmenity1.getAmenityIcon()+ "\"";
						
						jsonAmenities += "}";
					}
					
					jsonOutput += ",\"amenities\":[" + jsonAmenities+ "]";
				}
				
				
				 long minimum = getAvailableCount(getPropertyId(),fromDateST,toDateST,available.getRoomAvailable(),available.getAccommodationId()); 
				
				jsonOutput += ",\"available\":\"" + minimum + "\"";
				//jsonOutput += ",\"available\":\"" + (roomCount == null ? available.getAvailable() : (available.getAvailable()-roomCount.getRoomCount() )) + "\"";
				jsonOutput += "}";

			}
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
   
   public String getNewOfflineRoomAvailability() throws IOException{

	   String output=null;
	   try{
		   Algorithm algorithm = Algorithm.HMAC256("secret");
			long nowMillis = System.currentTimeMillis();
		    Date now = new Date(nowMillis+15*60*1000);
		    java.util.Date dateStart = null;
		    java.util.Date dateEnd =null;
		    java.util.Date date=new java.util.Date();
		   int count=0;
		   DateFormat format = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
		    dateStart = format.parse(getStrStartDate());
		    dateEnd = format.parse(getStrEndDate());
		   
		    DecimalFormat df = new DecimalFormat("###.##");
		    DateFormat f = new SimpleDateFormat("EEEE");
		    
		    Calendar calCheckStart=Calendar.getInstance();
		    calCheckStart.setTime(dateStart);
		    java.util.Date checkInDate = calCheckStart.getTime();
		    String strCheckInDate=new SimpleDateFormat("yyyy-MM-dd").format(checkInDate.getTime());
		    
		    this.arrivalDate=new Timestamp(checkInDate.getTime());
		   if(getSourceTypeId()==null){
			   this.sourceTypeId=1;
		   }
		    
		    Calendar calCheckEnd=Calendar.getInstance();
		    calCheckEnd.setTime(dateEnd);
		    java.util.Date checkOutDate = calCheckEnd.getTime();
		    this.departureDate=new Timestamp(checkOutDate.getTime());
			
			Map mapDate=new HashMap();  
			mapDate.put("arrivalDate", arrivalDate);
			mapDate.put("departureDate", departureDate); 
		      
			this.propertyId = getPropertyId();
			
//			String checkIn = new SimpleDateFormat("MMM d,yyyy").format(getArrivalDate());
//		    String checkOut = new SimpleDateFormat("MMM d,yyyy").format(getDepartureDate());
			 
			
		    Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		   
			Calendar cal = Calendar.getInstance();
			cal.setTime(this.departureDate);
	        cal.add(Calendar.DATE, -1);
			String startDate = new SimpleDateFormat("yyyy-MM-dd").format(this.arrivalDate);
			String endDate = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
			DateTime start = DateTime.parse(startDate);
	        DateTime end = DateTime.parse(endDate);
	        java.util.Date arrival = start.toDate();
			java.util.Date departure = end.toDate();
			PropertyAccommodationInventoryManager inventoryController= new PropertyAccommodationInventoryManager();
			String iconPath  = "ulowebsite/images/icons/";
			
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("application/json");
			java.util.Date currentdate=new java.util.Date();
        	Calendar curDate=Calendar.getInstance();
        	curDate.setTime(currentdate);
        	curDate.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
        	int bookedHours=curDate.get(Calendar.HOUR);
        	int bookedMinute=curDate.get(Calendar.MINUTE);
        	int bookedSecond=curDate.get(Calendar.SECOND);
        	int AMPM=curDate.get(Calendar.AM_PM);
        	java.util.Date cudate=curDate.getTime();
        	String strBookingDate=new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH).format(curDate.getTime());
        	String hours="",minutes="",seconds="",timeHours="";
        	if(bookedHours<10){
        		hours=String.valueOf(bookedHours);
        		hours="0"+hours;
        	}else{
        		hours=String.valueOf(bookedHours);
        	}
        	if(bookedMinute<10){
        		minutes=String.valueOf(bookedMinute);
        		minutes="0"+minutes;
        	}else{
        		minutes=String.valueOf(bookedMinute);
        	}
        	
        	if(bookedMinute<10){
        		seconds=String.valueOf(bookedSecond);
        		seconds="0"+seconds;
        	}else{
        		seconds=String.valueOf(bookedSecond);
        	}
        	
        	if(AMPM==0){//If the current time is AM
        		timeHours=strBookingDate+" "+hours+" : "+minutes+" : "+seconds+" AM ";
        	}else if(AMPM==1){//If the current time is PM
        		timeHours=strBookingDate+" "+hours+" : "+minutes+" : "+seconds+" PM ";
        	}
        	
        	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        	
        	String checkStartDate = new SimpleDateFormat("yyyy-MM-dd").format(currentdate);
	    	String strStartTime=checkStartDate+" 12:00:00"; 
	   	    java.util.Date dteStartDate = sdf.parse(strStartTime);
	   	    Calendar calStart=Calendar.getInstance();
	   	    calStart.setTime(dteStartDate);
	   	    calStart.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
	   	    String StartDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calStart.getTime());
	    	java.util.Date lastFirst = sdf.parse(StartDate);
	    	String[] weekdays={"Monday","Tuesday","Wednesday","Thursday","Sunday"};
		    String[] weekend={"Friday","Saturday"};
			AccommodationAmenityManager accommodationAmenityController = new AccommodationAmenityManager();
			PmsPropertyManager propertyController = new PmsPropertyManager();		
			PropertyAccommodationManager accommodationManager = new PropertyAccommodationManager();
			PropertyAmenityManager amenityController = new PropertyAmenityManager();
			List<PropertyAmenity> propertyAmenityList;
			List<PropertyAmenity> tagAmenityList;
			PropertyTypeManager typeController =  new PropertyTypeManager();
			PropertyRateManager rateController = new PropertyRateManager();
			PropertyRateDetailManager detailController = new PropertyRateDetailManager();
			PmsAmenityManager ameController = new PmsAmenityManager();
			PromotionManager promotionController=new PromotionManager();
			PromotionDetailManager promotionDetailController=new PromotionDetailManager();
			PmsBookingManager bookingController=new PmsBookingManager();
			PropertyLandmarkManager propertyLandmarkController=new PropertyLandmarkManager();
			
			HashMap<Double,Integer > mapAmountAccommId=new HashMap<Double, Integer>();
			HashMap<Double,Integer > mapSoldOutAmountAccommId=new HashMap<Double, Integer>();
			
			ArrayList<Integer> arlSortPropertyId=new ArrayList<Integer>();
			HashMap<Integer, Integer> mapAvailablePropertyId=new HashMap<Integer, Integer>();
			
	    	List<AccommodationRoom> listAccommodationRoom=null;
			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
			PropertyReviewsManager reviewController=new PropertyReviewsManager();
		    int availRoomCount=0,soldRoomCount=0;
		    double commissionrange1=150,commissionrange2=225,commissionrange3=300,lastdiscountpercent1=10,lastdiscountpercent2=15,lastdiscountpercent3=20;
		    
		    this.propertyId = (Integer) sessionMap.get("propertyId");
		    this.propertyList=propertyController.list(this.propertyId);
		    if(this.propertyList.size()>0 && !this.propertyList.isEmpty()){
		    	for(PmsProperty property:this.propertyList){
		  			
		  			
					
		  			SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
		  			Timestamp FromDate=(Timestamp)mapDate.get("arrivalDate");
		  			Timestamp ToDate=(Timestamp)mapDate.get("departureDate");
		  			
		  			String strFromDate=format2.format(FromDate);
		  			String strToDate=format2.format(ToDate);
		  	    	DateTime startdate = DateTime.parse(strFromDate);
		  	        DateTime enddate = DateTime.parse(strToDate);
		  	        java.util.Date fromdate = startdate.toDate();
		  	 		java.util.Date todate = enddate.toDate();
		  	 		 
		  			int diffInDays = (int) ((todate.getTime() - fromdate.getTime()) / (1000 * 60 * 60 * 24));
		  			
		  			List arllist=new ArrayList();
		  			List arllistAvl=new ArrayList();
		  			List arllistSold=new ArrayList();
		  			
		  				
		  			String jsonAccommodations ="";
		  			List<PropertyAccommodation> accommodationsList =  accommodationManager.list(property.getPropertyId());
		  			int soldOutCount=0,availCount=0,promotionAccommId=0;
		  			if(accommodationsList.size()>0 && !accommodationsList.isEmpty())
		  			{
		  				for (PropertyAccommodation accommodation : accommodationsList) {
		  					int roomCount=0;
		  					listAccommodationRoom=accommodationController.listPropertyTotalRooms(property.getPropertyId(), accommodation.getAccommodationId());
		  					if(listAccommodationRoom.size()>0 && !listAccommodationRoom.isEmpty()){
		  						for(AccommodationRoom roomsList:listAccommodationRoom){
		  							 roomCount=(int) roomsList.getRoomCount();
		  							 long minimum = getAvailableCount(property.getPropertyId(),getArrivalDate(),getDepartureDate(),roomCount,accommodation.getAccommodationId());
		  							 if((int)minimum>0){
		  								 availCount++;
		  							 }else if((int)minimum==0){
		  								 soldOutCount++;
		  							 }
		  							 break;
		  						 }
		  					 }
		  					
		  					
		  					
		  				}
		  				
		  			}

		  			
		  			List<PmsPromotionDetails> listPromotionDetailCheck=null;
		  			Boolean blnAvailable=false,blnSoldout=false,firstPromotionFlag=false,secondPromotionFlag=false,promotionFlag=false,blnPromoFlag=false;
		  			int arlListTotalAccomm=0,soldOutTotalAccomm=0,accommCount=0;
		  			ArrayList<String> arlListPromoType=new ArrayList<String>();
		  			
		  			String promotionType="NA",discountType="NA",promotionFirstName=null,promotionSecondName=null,
		  					promotionFirstType=null,promotionSecondType=null;
					double dblDiscountINR=0,dblDiscountPercent=0;
					boolean promoStatus=false,isFlat=false,isEarly=false,isLast=false,isSoldout=false;
					List<PmsPromotions> listPromotions=promotionController.listDatePromotions(property.getPropertyId(), dateStart);
					 if(listPromotions.size()>0 && !listPromotions.isEmpty()){
					   	for(PmsPromotions promotion:listPromotions){
					   		if(promotion.getPromotionType().equals("F")){
					   			promotionType=promotion.getPromotionType();
					   			promoStatus=true;
					   			isFlat=true;
					   		}else if(promotion.getPromotionType().equals("E")){
					   			promotionType=promotion.getPromotionType();
					   			Timestamp tsStartPromo=promotion.getFirstRangeStartDate();
					   			Timestamp tsEndPromo=promotion.getFirstRangeEndDate();
					   			String startStayDate = new SimpleDateFormat("yyyy-MM-dd").format(tsStartPromo);
								String endStayDate = new SimpleDateFormat("yyyy-MM-dd").format(tsEndPromo);
								DateTime dtstart = DateTime.parse(startStayDate);
							    DateTime dtend = DateTime.parse(endStayDate);
					   			List<DateTime> between = DateUtil.getDateRange(dtstart, dtend);
					   			if(between.size()>0 && !between.isEmpty()){
					   				for(DateTime d:between){
					   					if(cudate.equals(d.toDate())){
					   						promoStatus=true;
					   						isEarly=true;
					   					}
					   				}
					   			}else{
					   				promoStatus=false;
					   			}
					   		}
					   		
							List<PmsPromotionDetails> listPromoDetails=promotionDetailController.listPromotionDetails(promotion.getPromotionId(),f.format(dateStart).toLowerCase());
							if(listPromoDetails.size()>0 && !listPromoDetails.isEmpty()){
								for(PmsPromotionDetails promodetails:listPromoDetails){
									dblDiscountINR=promodetails.getPromotionInr();
									if(dblDiscountINR>0){
										discountType="INR";
									}
									dblDiscountPercent=promodetails.getPromotionPercentage();
									if(dblDiscountPercent>0){
										discountType="PERCENT";
									}
								}
							}
						}
					 }
					String jsonAccommodation=""; 
					String sellingType="NA",sellingDays="NA";
					sellingDays=f.format(dateStart).toLowerCase();
					boolean blnWeekdays=false,blnWeekend=false;
					for(int a=0;a<weekdays.length;a++){
						if(sellingDays.equalsIgnoreCase(weekdays[a])){
							blnWeekdays=true;
						}
					}
					
					for(int b=0;b<weekend.length;b++){
						if(sellingDays.equalsIgnoreCase(weekend[b])){
							blnWeekend=true;
						}
					}
					if(property.getLocationType()!=null){
						if(property.getLocationType().getLocationTypeName().equalsIgnoreCase("Leisure")){
							if(blnWeekdays){
								sellingType="weekdays";
							}
							
							if(blnWeekend){
								sellingType="weekend";
							}	
						}
						
						if(property.getLocationType().getLocationTypeName().equalsIgnoreCase("Metro")){
							sellingType="metro";
						}	
					}
    				
		  			List<PropertyAccommodation> accommodationList =  accommodationManager.listSortByPrice(property.getPropertyId(), sellingType);
		  			if(accommodationList.size()>0)
		  			{
		  				for (PropertyAccommodation accommodation : accommodationList) {
		  					
		  					if (!jsonAccommodation.equalsIgnoreCase(""))
		  						
		  						jsonAccommodation += ",{";
		  					else
		  						jsonAccommodation += "{";

		  					int dateCount=0;
		  					double amount=0.0,extraAdultAmount=0.0,extraChildAmount=0.0,totalMaximumAmount=0.0,totalMinimumAmount=0.0,diffAmount=0;
		  					int roomCount=0,availableCount=0,totalCount=0;
		  				    double dblPercentCount=0.0;
		  	    			listAccommodationRoom=accommodationController.listPropertyTotalRooms(property.getPropertyId(), accommodation.getAccommodationId());
		  					 
		  					if(listAccommodationRoom.size()>0 && !listAccommodationRoom.isEmpty()){
		  						
		  						for(AccommodationRoom roomsList:listAccommodationRoom){
		  							roomCount=(int) roomsList.getRoomCount();
		  							if(roomCount>0){
		  								long minimum = getAvailableCount(property.getPropertyId(),getArrivalDate(),getDepartureDate(),roomCount,accommodation.getAccommodationId());
		  								if((int)minimum>0){
		  									blnAvailable=true;
		  									blnSoldout=false;
		  									arlListTotalAccomm++;
		  								}else if((int)minimum==0){
		  									blnSoldout=true;
		  									blnAvailable=false;
		  									soldOutTotalAccomm++;
		  								}
		  							}
		  							 break;
		  						 }
		  					 }
		  					 List<DateTime> between = DateUtil.getDateRange(start, end);
		  					 
		  					double sellrate=0,minAmount=0,maxAmount=0,promoAmount=0,firstPromoAmount=0,secondPromoAmount=0,firstInr=0,secondInr=0;
		  					
		  				     for (DateTime d : between)
		  				     {

						    	 long rmc = 0;
					   			 this.roomCnt = rmc;
					        	 int sold=0;
					        	 java.util.Date availDate = d.toDate();
					        	 String days=f.format(d.toDate()).toLowerCase();
								 PropertyAccommodationInventory inventoryCount = inventoryController.findInventoryCount(availDate,accommodation.getAccommodationId());
								 if(inventoryCount != null){
								 PmsAvailableRooms roomCount1 = bookingController.findBookingCount(availDate, accommodation.getAccommodationId(),inventoryCount.getInventoryId());
								 if(roomCount1.getRoomCount() == null) {
								 	String num1 = inventoryCount.getInventoryCount();
									PmsAvailableRooms bookingRoomCount=bookingController.findCount(availDate, accommodation.getAccommodationId());
									PropertyAccommodation accomm=accommodationController.find(accommodation.getAccommodationId());
									Integer totavailable=0;
									if(accommodation!=null){
										totavailable=accommodation.getNoOfUnits();
									}
									if(bookingRoomCount.getRoomCount()!=null){
										long lngRooms=bookingRoomCount.getRoomCount();
										long num2 = Long.parseLong(num1);
										this.roomCnt = num2;
										sold=(int) (lngRooms);
										this.soldCnt=lngRooms;
										int availRooms=0;
										availRooms=totavailable-sold;
										if(Integer.parseInt(num1)>availRooms){
											this.roomCnt = availRooms;	
										}else{
											this.roomCnt = num2;
										}
												
									}else{
										 long num2 = Long.parseLong(num1);
										 this.roomCnt = num2;
										 sold=(int) (rmc);
										 this.soldCnt=rmc;
									}
								}else{		
									PmsAvailableRooms bookingRoomCount=bookingController.findCount(availDate, accommodation.getAccommodationId());
									int intRooms=0;
									Integer totavailable=0,availableRooms=0;
									if(accommodation!=null){
										totavailable=accommodation.getNoOfUnits();
									}
									if(bookingRoomCount.getRoomCount()!=null){
										long lngRooms=bookingRoomCount.getRoomCount();
										long num = roomCount1.getRoomCount();	
										intRooms=(int)lngRooms;
										String num1 = inventoryCount.getInventoryCount();
					 					long num2 = Long.parseLong(num1);
					 					availableRooms=totavailable-intRooms;
					 					if(num2>availableRooms){
					 						this.roomCnt = availableRooms;	 
					 					}else{
					 						this.roomCnt = num2-num;
					 					}
					 					long lngSold=bookingRoomCount.getRoomCount();
					   					sold=(int)lngSold;
					   					this.soldCnt=lngSold;
									}else{
										long num = roomCount1.getRoomCount();	
										String num1 = inventoryCount.getInventoryCount();
										long num2 = Long.parseLong(num1);
					  					this.roomCnt = (num2-num);
					  					long lngSold=roomCount1.getRoomCount();
					   					sold=(int)lngSold;
					   					this.soldCnt=lngSold;
									}
				
								}
												
							}else{
								  PmsAvailableRooms inventoryCounts = bookingController.findCount(availDate, accommodation.getAccommodationId());							
									if(inventoryCounts.getRoomCount() == null){								
										this.roomCnt = (accommodation.getNoOfUnits()-rmc);
										sold=(int)rmc;
										this.soldCnt=rmc;
									}
									else{
										this.roomCnt = (accommodation.getNoOfUnits()-inventoryCounts.getRoomCount());	
										long lngSold=inventoryCounts.getRoomCount();
			   							sold=(int)lngSold;
			   							this.soldCnt=lngSold;
									}
							}
												
							double totalRooms=0,intSold=0;
							totalRooms=(double)accommodation.getNoOfUnits();
							intSold=(double)sold;
							double occupancy=0.0;
							occupancy=intSold/totalRooms;
							
							
							Integer occupancyRating=(int) Math.round(occupancy * 100);
							boolean isWeekdays=false,isWeekend=false;
							for(int a=0;a<weekdays.length;a++){
								if(days.equalsIgnoreCase(weekdays[a])){
									isWeekdays=true;
								}
							}
							
							for(int b=0;b<weekend.length;b++){
								if(days.equalsIgnoreCase(weekend[b])){
									isWeekend=true;
								}
							}
							if(property.getLocationType()!=null){
								if(property.getLocationType().getLocationTypeName().equalsIgnoreCase("Leisure")){
									if(isWeekdays){
										if(accommodation.getWeekdaySellingRate()==null){
											maxAmount=0;
										}else{
											maxAmount=accommodation.getWeekdaySellingRate();	
										}
											
									}
									
									if(isWeekend){
										if(accommodation.getWeekendSellingRate()==null){
											maxAmount=0;
										}else{
											maxAmount=accommodation.getWeekendSellingRate();	
										}
										
									}	
								}
								
								if(property.getLocationType().getLocationTypeName().equalsIgnoreCase("Metro")){
									if(accommodation.getSellingRate()==null){
										maxAmount=0;
									}else{
										maxAmount=accommodation.getSellingRate();	
									}
										
								}
								if(occupancyRating>=0 && occupancyRating<=30){
									sellrate=maxAmount;
								}else if(occupancyRating>30 && occupancyRating<=60){
									sellrate=maxAmount+(maxAmount*10/100);
								}else if(occupancyRating>=60 && occupancyRating<=80){
									sellrate=maxAmount+(maxAmount*15/100);
								}else if(occupancyRating>80){
									sellrate=maxAmount+(maxAmount*20/100);
								}
							}
		    				
							sellrate=maxAmount;
								
								if(discountType.equals("INR")){
									firstPromoAmount=dblDiscountINR;
									firstInr+=dblDiscountINR;
								}else if(discountType.equals("PERCENT")){
									firstPromoAmount=(sellrate*dblDiscountPercent/100);
								}else{
									firstPromoAmount=0;
								}
								
								if(checkStartDate.equals(strCheckInDate)){
									if(occupancyRating>=0 && occupancyRating<=30){
										if(this.roomCnt>0){
											isLast=true;
										}else{
											isLast=false;
										}
									}
								}
								
			    				if(isLast){
			    					if(occupancyRating>=0 && occupancyRating<=30){
			    						if(AMPM==0){//If the current time is AM
			    		            		if(bookedHours>=0 && bookedHours<=11){
			    		            			if(maxAmount<1000){
			    									if(occupancyRating>=0 && occupancyRating<=30){
			    										secondPromoAmount=commissionrange1;
			    										secondInr+=commissionrange1;
			    									}
			    							    }else if(maxAmount>=1000 && maxAmount<=1500){
			    							    	if(occupancyRating>=0 && occupancyRating<=30){
			    							    		secondPromoAmount=commissionrange2;
			    							    		secondInr+=commissionrange2;
			    									}
			    							    }else if(maxAmount>1500){
			    							    	if(occupancyRating>=0 && occupancyRating<=30){
			    							    		secondPromoAmount=commissionrange3;
			    							    		secondInr+=commissionrange3;
			    									}
			    							    }
			    		            		}
			    		            	}
			    						if(AMPM==1){
			    							if(bookedHours>=0 && bookedHours<=3){
			    								secondPromoAmount=(sellrate*lastdiscountpercent1/100);
			    		            		}else if(bookedHours>=4 && bookedHours<=7){
			    		            			secondPromoAmount=(sellrate*lastdiscountpercent2/100);
			    		            		}else if(bookedHours>=8 && bookedHours<=11){
			    		            			secondPromoAmount=(sellrate*lastdiscountpercent3/100);
			    		            		}
			    						}
			    					}
			    				}
			    				promoAmount=firstPromoAmount+secondPromoAmount;
			    				totalMinimumAmount+=sellrate-promoAmount;
			  				    totalMaximumAmount+=sellrate;
						    	 dateCount++;
						    	extraAdultAmount+=accommodation.getExtraAdult(); 
						    	extraChildAmount+=accommodation.getExtraChild();
		  				     }
		  				     
		  				    
		  				    diffAmount=totalMaximumAmount-totalMinimumAmount;
		  				   if(isFlat){
								if(discountType.equalsIgnoreCase("INR")){
									promotionFirstName=String.valueOf(Math.round(firstInr))+"";
									promotionFirstType="inr";
								}else if(discountType.equalsIgnoreCase("PERCENT")){
									promotionFirstName=String.valueOf(Math.round(dblDiscountPercent))+"";
									promotionFirstType="percent";
								}
							}
							if(isEarly){
								if(discountType.equalsIgnoreCase("INR")){
									promotionFirstName=String.valueOf(Math.round(firstInr))+"";
									promotionFirstType="inr";
								}else if(discountType.equalsIgnoreCase("PERCENT")){
									promotionFirstName=String.valueOf(Math.round(dblDiscountPercent))+"";
									promotionFirstType="percent";
								}
							}
							if(isLast){

								if(AMPM==0){//If the current time is AM
	    		            		if(bookedHours>=0 && bookedHours<=11){
	    		            
	    		            			String displayDate = new SimpleDateFormat("MMM dd,yyyy HH:mm:ss").format(lastFirst.getTime());
	    								this.displayHoursForTimer=displayDate;
	    								setDisplayHoursForTimer(this.displayHoursForTimer);
	    								promotionSecondType="inr";
	    								promotionSecondName=String.valueOf(Math.round(secondInr))+"";
	    		            		}
	    		            	}
	    						if(AMPM==1){
	    							if(bookedHours>=0 && bookedHours<=3){
	    								String firstTime=checkStartDate+" 16:00:00"; 
	    						   	    java.util.Date dteFirstDate = sdf.parse(strStartTime);
	    						   	    Calendar calFirst=Calendar.getInstance();
	    						   	    calFirst.setTime(dteFirstDate);
	    						   	    calFirst.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
	    						   	    String firstDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calFirst.getTime());
	    						    	java.util.Date dteFirst = sdf.parse(firstTime);
	    								String displayDate = new SimpleDateFormat("MMM dd,yyyy HH:mm:ss").format(dteFirst.getTime());
	    								this.displayHoursForTimer=displayDate;
	    								setDisplayHoursForTimer(this.displayHoursForTimer);
	    								promotionSecondType="percent";
	    								promotionSecondName=String.valueOf(Math.round(lastdiscountpercent1))+"";
	    		            		}else if(bookedHours>=4 && bookedHours<=7){
	    		            			String firstTime=checkStartDate+" 20:00:00"; 
	    						   	    java.util.Date dteFirstDate = sdf.parse(strStartTime);
	    						   	    Calendar calFirst=Calendar.getInstance();
	    						   	    calFirst.setTime(dteFirstDate);
	    						   	    calFirst.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
	    						   	    String firstDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calFirst.getTime());
	    						    	java.util.Date dteFirst = sdf.parse(firstTime);
	    								String displayDate = new SimpleDateFormat("MMM dd,yyyy HH:mm:ss").format(dteFirst.getTime());
	    								this.displayHoursForTimer=displayDate;
	    								setDisplayHoursForTimer(this.displayHoursForTimer);
	    								promotionSecondType="percent";
	    								promotionSecondName=String.valueOf(Math.round(lastdiscountpercent2))+"";
	    		            		}else if(bookedHours>=8 && bookedHours<=11){
	    		            			String firstTime=checkStartDate+" 23:00:00"; 
	    						   	    java.util.Date dteFirstDate = sdf.parse(strStartTime);
	    						   	    Calendar calFirst=Calendar.getInstance();
	    						   	    calFirst.setTime(dteFirstDate);
	    						   	    calFirst.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
	    						   	    String firstDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calFirst.getTime());
	    						    	java.util.Date dteFirst = sdf.parse(firstTime);
	    								String displayDate = new SimpleDateFormat("MMM dd,yyyy HH:mm:ss").format(dteFirst.getTime());
	    								this.displayHoursForTimer=displayDate;
	    								setDisplayHoursForTimer(this.displayHoursForTimer);
	    								promotionSecondType="percent";
	    								promotionSecondName=String.valueOf(Math.round(lastdiscountpercent3))+"";
	    		            		}
	    						}

							
							}
		  				     
							
							PropertyTaxeManager propertytaxController = new PropertyTaxeManager();
	  	  					 double taxe =  totalMinimumAmount/diffInDays;
	  	  					
	  						 PropertyTaxe tax = propertytaxController.find(taxe);
	  						 double taxes = Math.round(totalMinimumAmount * tax.getTaxPercentage() / 100  ) ;
	  						
	  						if (!jsonOutput.equalsIgnoreCase(""))
	  			  				jsonOutput += ",{";
	  			  			else
	  			  				jsonOutput += "{";
	  						  
		  					
		  					long minimum = getAvailableCount(property.getPropertyId(),getArrivalDate(),getDepartureDate(),roomCount,accommodation.getAccommodationId());
		  			    	
		  					jsonOutput += "\"available\":\"" + minimum+ "\""; 
	  				    	jsonOutput += ",\"accommodationType\":\"" + accommodation.getAccommodationType().toUpperCase()+ "\"";
	  				    	jsonOutput += ",\"accommodationId\":\"" + accommodation.getAccommodationId()+ "\"";
	  				    	jsonOutput += ",\"minimumAmount\":\"" + (totalMinimumAmount==0 ? 0 : Math.round(totalMinimumAmount) )+  "\"";
	  				    	jsonOutput += ",\"maximumAmount\":\"" + (totalMaximumAmount==0 ? 0 : Math.round(totalMaximumAmount) )+  "\"";
	  				    	jsonOutput += ",\"availCount\":\"" + this.roomCnt+ "\"";
	  				    	jsonOutput += ",\"soldCount\":\"" + this.soldCnt + "\"";
		  			    	 
		  			    	jsonOutput += "}";
		  				}
		  			}
		  			
		  			arllist.clear();
				
		    	}
		    	
		    }

		    response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
		    
	   }catch(Exception ex){
		   logger.error(ex);
		   ex.printStackTrace();
	   }
	   return output;
   
   
	

   }
   
    public String getOfflineRoomAvailability() throws IOException{

		
	   
	   try {
			DateFormat format = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
		    java.util.Date dateStart = format.parse(getStrStartDate());
		    java.util.Date dateEnd = format.parse(getStrEndDate());
		   
		    
		    Calendar calCheckStart=Calendar.getInstance();
		    calCheckStart.setTime(dateStart);
		    java.util.Date checkInDate = calCheckStart.getTime();
		    this.arrivalDate=new Timestamp(checkInDate.getTime());
		    sessionMap.put("arrivalDate",arrivalDate);  
		    
		    Calendar calCheckEnd=Calendar.getInstance();
		    calCheckEnd.setTime(dateEnd);
		    java.util.Date checkOutDate = calCheckEnd.getTime();
		    this.departureDate=new Timestamp(checkOutDate.getTime());
		    sessionMap.put("departureDate",departureDate);
	
		    Map mapDate=new HashMap();  
			mapDate.put("arrivalDate", arrivalDate);
			mapDate.put("departureDate", departureDate);
			String startDate = new SimpleDateFormat("yyyy-MM-dd").format(this.arrivalDate);
			String endDate = new SimpleDateFormat("yyyy-MM-dd").format(this.departureDate);
			
			DateTime dtstart = DateTime.parse(startDate);
			DateTime dtend = DateTime.parse(endDate);
			java.util.Date dtfromDate = dtstart.toDate();
			java.util.Date dttoDate = dtend.toDate();
		
			int diffInDays = (int) ((dttoDate.getTime() - dtfromDate.getTime()) / (1000 * 60 * 60 * 24));
			
			this.propertyId = (Integer) sessionMap.get("propertyId");
			Timestamp timestamp = new Timestamp(System.currentTimeMillis());
	
			if(getSourceTypeId()==null){
			   this.sourceTypeId=1;
		   }
			Calendar cal = Calendar.getInstance();
			cal.setTime(this.departureDate);
			cal.add(Calendar.DATE, -1);
			String calStartDate = new SimpleDateFormat("yyyy-MM-dd").format(this.arrivalDate);
			String calEndDate = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
			DateTime start = DateTime.parse(calStartDate);
			DateTime end = DateTime.parse(calEndDate);
			java.util.Date arrival = start.toDate();
			java.util.Date departure = end.toDate();
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
			PmsBookingManager bookingController = new PmsBookingManager();
			PropertyTaxeManager taxController = new PropertyTaxeManager();
			PromotionManager promotionController=new PromotionManager();
			PromotionDetailManager promotionDetailController=new PromotionDetailManager();
			PropertyRateManager rateController = new PropertyRateManager();
			PropertyRateDetailManager detailController = new PropertyRateDetailManager();
			PmsPropertyManager propertyController=new PmsPropertyManager();
			response.setContentType("application/json");
			SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
			DateFormat f = new SimpleDateFormat("EEEE");
			List<PmsPromotionDetails> listPromotionDetailCheck=null;
			ArrayList<String> arlListPromoType=new ArrayList<String>();
			List<PropertyAccommodation> accommodationsList =  accommodationController.list(this.propertyId);
			if(accommodationsList.size()>0)
			{

  				for (PropertyAccommodation accommodation : accommodationsList) {
  					Integer promoAccommId=0,enablePromoId=0;
  					ArrayList<Integer> arlEnablePromo=new ArrayList<Integer>();
  					Boolean blnPromotionIsNotFlag=false;
  					Boolean blnLastMinuteIsActive=false,blnLastMinuteFirstActive=false,promotionFlag=false,blnPromoFlag=false;
  					int dateCount=0,promoCount=0;
  					String promotionFirstName=null,promotionSecondName=null,promotionHoursRange=null,promotionType=null,
  	  			    		promotionSecondDiscountType=null,promotionFirstDiscountType=null,strCheckPromotionType=null;
  					Integer promoFirstPercent=null,promoFirstInr=null,promoSecondPercent=null,promoSecondInr=null;
  					double baseAmount=0.0,extraAdultAmount=0.0,extraChildAmount=0.0,totalBaseAmount=0.0,totalFlatAmount=0.0,totalLastAmount=0.0,
  							actualPromoBaseAmount=0.0,actualBaseAmount=0.0,actualTotalAmount=0.0,actualTotalAdultAmount=0.0,actualTotalChildAmount=0.0,
  	  		    			actualAdultAmount=0.0,actualChildAmount=0.0;
  					int roomCount=0,availableCount=0,totalCount=0,discountId=0;
  				    double dblPercentCount=0.0,dblSmartPricePercent=0.0,
  				    		promotionFirstPercent=0.0,promotionFirstInr=0.0,
  				    		promotionSecondPercent=0.0,promotionSecondInr=0.0,totalFlatLastAmount=0.0,flatLastAmount=0.0;
  				    Timestamp lastMinStartDate=null,lastMinEndDate=null;
  				    Boolean isPositive=false,isNegative=false,blnLastMinutePromotions=false;
  					 sourceId = 1;
  					 Timestamp tsPromoBookedDate=null,tsPromoStartDate=null,tsPromoEndDate=null;
  					 Timestamp tsFirstStartRange=null,tsSecondStartRange=null,tsFirstEndRange=null,tsSecondEndRange=null;
 					 Timestamp tsLastStartDate=null,tsLastEndDate=null;
  					 boolean blnFirstActivePromo=false,blnFirstNotActivePromo=false,baseFirstPromoFlag=false,blnEarlyBirdPromo=false;
  					 boolean blnSecondActivePromo=false,blnSecondNotActivePromo=false,baseSecondPromoFlag=false,blnLastMinutePromo=false;
  					 java.util.Date currentdate=new java.util.Date();
  					 String strCurrentDate = new SimpleDateFormat("yyyy-MM-dd").format(currentdate);
  				   	 java.util.Date curdate = format1.parse(strCurrentDate);
  				   	 this.displayHoursForTimer=null;
  				   	 setDisplayHoursForTimer(this.displayHoursForTimer);
  					 List<PmsPromotions> listDatePromotions= promotionController.listBookedDatePromotions(getPropertyId(),curdate);
  					 if(listDatePromotions.size()>0 && !listDatePromotions.isEmpty()){
  						for(PmsPromotions promotions:listDatePromotions){
  							tsPromoBookedDate=promotions.getPromotionBookedDate();
  							tsPromoStartDate=promotions.getStartDate();
  							tsPromoEndDate=promotions.getEndDate();
  							blnEarlyBirdPromo=checkEarlyBirdPromos(tsPromoBookedDate,tsPromoStartDate,tsPromoEndDate,getArrivalDate(),getDepartureDate());
  						}
  					 }else{
  						 blnEarlyBirdPromo=false;
  					 }
  					 
  					List<PmsPromotions> listLastPromotions= promotionController.listLastDatePromotions(getPropertyId(),accommodation.getAccommodationId(),curdate);
					 if(listLastPromotions.size()>0 && !listLastPromotions.isEmpty()){
						for(PmsPromotions promotions:listLastPromotions){
							tsFirstStartRange=promotions.getFirstRangeStartDate();
							tsSecondStartRange=promotions.getSecondRangeStartDate();
							tsFirstEndRange=promotions.getFirstRangeEndDate();
							tsSecondEndRange=promotions.getSecondRangeEndDate();
							tsLastStartDate=promotions.getStartDate();
							tsLastEndDate=promotions.getEndDate();
							promotionHoursRange=promotions.getPromotionHoursRange();
							blnLastMinuteFirstActive=checkLastMinutePromos(promotionHoursRange,getArrivalDate(),getDepartureDate(),tsSecondStartRange,tsFirstEndRange,tsFirstEndRange,tsSecondStartRange);
//							blnLastMinuteIsActive=getCheckHoursRange(promotionHoursRange,getArrivalDate(),getDepartureDate());
							if(blnLastMinuteFirstActive){
								blnLastMinutePromo=true;
								enablePromoId=promotions.getPromotionId();
								arlEnablePromo.add(enablePromoId);
							}
						}
					 }else{
						 blnLastMinutePromo=false;
						 this.displayHoursForTimer=null;
				    	 setDisplayHoursForTimer(this.displayHoursForTimer);
					 }
					 
  					 ArrayList<Integer> arlFirstActivePromo=new ArrayList<Integer>(); 
  					 ArrayList<Integer> arlSecondActivePromo=new ArrayList<Integer>(); 
  					 List<DateTime> between = DateUtil.getDateRange(start, end);
  					 for (DateTime d : between)
  				     {
  						 if(blnEarlyBirdPromo){
  							 List<PmsPromotions> listEarlyPromotions= promotionController.listBookedDatePromotions(getPropertyId(),curdate);
  							 if(listEarlyPromotions.size()>0 && !listEarlyPromotions.isEmpty()){
  								 for(PmsPromotions promos:listEarlyPromotions){
  									 if(promos.getPromotionType().equalsIgnoreCase("E")){
  	 									 if(arlFirstActivePromo.isEmpty()){
  	 										arlFirstActivePromo.add(promos.getPromotionId());
  	 									 }else if(!arlFirstActivePromo.contains(promos.getPromotionId())){
  	 										arlFirstActivePromo.add(promos.getPromotionId());
  	 									 }
  	 								 }else{
  	 									blnFirstNotActivePromo=true;
  	 								 }
  								 }
  							 }
  						 }else if(!blnEarlyBirdPromo){
  							 List<PmsPromotions> listAllPromotions= promotionController.listFlatDatePromotions(getPropertyId(),d.toDate());
  							 if(!listAllPromotions.isEmpty() && listAllPromotions.size()>0){
  								 for(PmsPromotions promos:listAllPromotions){
  									 if(promos.getPromotionType().equalsIgnoreCase("F")){
  	 									 if(arlFirstActivePromo.isEmpty()){
  	 										arlFirstActivePromo.add(promos.getPromotionId());
  	 									 }else if(!arlFirstActivePromo.contains(promos.getPromotionId())){
  	 										arlFirstActivePromo.add(promos.getPromotionId());
  	 									 }
  	 								 }else{
  	 									blnFirstNotActivePromo=true;
  	 								 }
  								 }
  							 } 
  						 }else{
  							 blnFirstNotActivePromo=true;
  						 }
  						 
  						if(blnLastMinutePromo){
  							List<PmsPromotions> listLastPromos= promotionController.listLastBookedPromotions(getPropertyId(),curdate);
  							 if(listLastPromos.size()>0 && !listLastPromos.isEmpty()){
  								for(PmsPromotions promos:listLastPromos){
  									if(promos.getPromotionType().equalsIgnoreCase("L")){
  										promoAccommId=promos.getPropertyAccommodation().getAccommodationId();
  										if(arlEnablePromo.contains(promos.getPromotionId())){
											if(arlSecondActivePromo.isEmpty()){
  		  										arlSecondActivePromo.add(promos.getPromotionId());
  		  									 }else if(!arlSecondActivePromo.contains(promos.getPromotionId())){
  		  										arlSecondActivePromo.add(promos.getPromotionId());
  		  									 }
										}
  									 }
  								}
  							 }else{
  								blnSecondNotActivePromo=true;
  							 }
						}else{
							blnSecondNotActivePromo=true;
						}
  				     
  				     }
  					 int intPromoSize=arlFirstActivePromo.size();
  					 if((arlFirstActivePromo.isEmpty() || blnFirstNotActivePromo)){
  						 blnFirstActivePromo=false;
  					 }else{
  						 blnFirstActivePromo=true;
  					 }
  					 
  					 if(arlSecondActivePromo.isEmpty() || blnSecondNotActivePromo){
  						blnSecondActivePromo=false;
  					 }else{
  						blnSecondActivePromo=true;
  					 }
  					List<AccommodationRoom> listAccommodationRoom=null;
  					
  					listAccommodationRoom=accommodationController.listPropertyTotalRooms(this.propertyId, accommodation.getAccommodationId());
					 
					 
					if(listAccommodationRoom.size()>0 && !listAccommodationRoom.isEmpty()){
						for(AccommodationRoom roomsList:listAccommodationRoom){
							 roomCount=(int) roomsList.getRoomCount();
							 break;
						 }
					 }
  				     for (DateTime d : between)
  				     {
  				    	 Integer promotionId=0;
  				    	 if(blnFirstActivePromo){
  				    		if(blnEarlyBirdPromo){
  				    			List<PmsPromotions> listEarlyPromotions= promotionController.listBookedDatePromotions(getPropertyId(),curdate);
  	  							 if(listEarlyPromotions.size()>0 && !listEarlyPromotions.isEmpty()){
  	  				    			for(PmsPromotions promotions:listEarlyPromotions){
  	  				    				if(promotions.getPromotionType().equalsIgnoreCase("E")){
  	 					    				promotionId=promotions.getPromotionId();
  	 					    				promotionFirstDiscountType=promotions.getPromotionDiscountType();
  	 					    				listPromotionDetailCheck=promotionDetailController.listPromotionDetails(promotions.getPromotionId(),f.format(d.toDate()).toLowerCase());
  	 					    				 for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
  	 					    					if(promoCount==0){
  	 					    						promotionFirstPercent=promoDetails.getPromotionPercentage();
  	 	 					    					promotionFirstInr=promoDetails.getPromotionInr();
  	 	 					    					promotionType=promotions.getPromotionType();
  	 	 					    					if(promotionType.equalsIgnoreCase("E")){
  	 	 					    						strCheckPromotionType="E";
  	 	 					    					}
  	 	 					    					
  	 		  						    			arlListPromoType.add(promotionType);
  	 					    					}
  	 					    					promoCount++;
  	 					    				 }
  	  				    				}
  	  				    			}
  	  				    			promotionFlag=true; 
  			    					blnPromoFlag=true;
  	  				    		}else{
  	  				    			promotionFlag=false;
  	  	  		    				promotionType=null;
  	  	  		    				blnPromoFlag=false;
  	  	  		    				promotionId=null;
  	  				    		}
  				    		}else if(!blnEarlyBirdPromo){
  				    			List<PmsPromotions> listPromotions= promotionController.listFlatDatePromotions(getPropertyId(),d.toDate());
  	  				    		if(listPromotions.size()>0 && !listPromotions.isEmpty()){
  	  				    			for(PmsPromotions promotions:listPromotions){
  	  				    				if(promotions.getPromotionType().equalsIgnoreCase("F")){
  	 					    				promotionId=promotions.getPromotionId();
  	 					    				promotionFirstDiscountType=promotions.getPromotionDiscountType();
  	 					    				listPromotionDetailCheck=promotionDetailController.listPromotionDetails(promotions.getPromotionId(),f.format(d.toDate()).toLowerCase());
  	 					    				 for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
  	 					    					if(promoCount==0){
  	 					    						promotionFirstPercent=promoDetails.getPromotionPercentage();
  	 	 					    					promotionFirstInr=promoDetails.getPromotionInr();
  	 	 					    					promotionType=promotions.getPromotionType();
  	 	 					    					if(promotionType.equalsIgnoreCase("F")){
  	 	 					    						strCheckPromotionType="F";
  	 	 					    					}
  	 	 					    					
  	 		  						    			arlListPromoType.add(promotionType);
  	 					    					}
  	 					    					promoCount++;
  	 					    				 }
  	  				    				}
  	  				    			}
  	  				    			promotionFlag=true; 
  			    					blnPromoFlag=true;
  	  				    		}
  				    		}
  				    	 }
  				    	 if(blnSecondActivePromo){
  				    		List<PmsPromotions> listPmsPromotions= promotionController.listLastDatePromotions(getPropertyId(),accommodation.getAccommodationId(),curdate);
				    			 if(listPmsPromotions.size()>0 &&!listPmsPromotions.isEmpty()){
				    				 for(PmsPromotions pmsPromotions:listPmsPromotions){
				    					 if(pmsPromotions.getPromotionType().equalsIgnoreCase("L")){
				    						 promotionId=pmsPromotions.getPromotionId();
							    			 promoAccommId=pmsPromotions.getPropertyAccommodation().getAccommodationId();
							    			 Timestamp tsStartDate=null,tsEndDate=null;
							    			 tsStartDate=pmsPromotions.getStartDate();
							    			 tsEndDate=pmsPromotions.getEndDate();
							    			 
							    			 promotionHoursRange=pmsPromotions.getPromotionHoursRange();
							    			 if(arlEnablePromo.contains(pmsPromotions.getPromotionId())){
							    				 long minimumCount = getAvailableCount(getPropertyId(),getArrivalDate(),getDepartureDate(),roomCount,accommodation.getAccommodationId());
				    			    			 if((int)minimumCount>0){
				    			    				 promotionFlag=true; 
			    			    					 blnPromoFlag=true;
			    			    					 strCheckPromotionType="L";
			    			    					 promotionType=pmsPromotions.getPromotionType();
    	 		  						    		 arlListPromoType.add(promotionType);
			    			    					 promotionSecondDiscountType=pmsPromotions.getPromotionDiscountType();
    	 					    					 listPromotionDetailCheck=promotionDetailController.listPromotionDetails(pmsPromotions.getPromotionId(),f.format(curdate).toLowerCase());
    	 					    					 if(listPromotionDetailCheck.size()>0 && !listPromotionDetailCheck.isEmpty()){
    	 					    						for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
		    	 					    					promotionSecondPercent=promoDetails.getPromotionPercentage();
	    	 	 					    					promotionSecondInr=promoDetails.getPromotionInr();
		    	 					    				 }
    	 					    					 }
				    			    			 }else{
				    			    				 promotionFlag=false;
				    			    				 promotionType=null;
				    			    				 blnPromoFlag=false;
				    			    				 baseSecondPromoFlag=true;
				    			    			 }
							    			 }
				    					 }
				    					 
				    				 }
				    			 }
  				    	 }
  				    	 if(promotionFlag){
  				    		int rateCount=0,rateIdCount=0;
    						List<PropertyRate> propertyRateList =  rateController.listAllDates(getPropertyId(),accommodation.getAccommodationId(),getSourceTypeId(),d.toDate());
    				    	if(propertyRateList.size()>0)
    				    	{
    				    		 for (PropertyRate pr : propertyRateList)
    			    			 {
				    				 int propertyRateId = pr.getPropertyRateId();
					    			 List<PropertyRateDetail> rateDetailList =   detailController.list(propertyRateId,f.format(d.toDate()).toLowerCase());
        				    		 if(rateDetailList.size()>0){
        				    			 if(rateCount<1){
        				    				 for (PropertyRateDetail rate : rateDetailList){
        				    					 baseAmount+=  rate.getBaseAmount();
        				    					 extraAdultAmount+=rate.getExtraAdult();
	    	  				    				 extraChildAmount+=rate.getExtraChild();
	        				    			 }
        				    				 rateCount++;
        				    			 }
        				    		 }else{
        				    			 if(propertyRateList.size()==rateIdCount){
        				    				 baseAmount+= accommodation.getBaseAmount();
        				    				 extraAdultAmount+=accommodation.getExtraAdult();
    	  				    				 extraChildAmount+=accommodation.getExtraChild();
        				    			 }
            				    	 }
    			    			 }	    		 
    				    	 }
    				    	 else{
    				    		 baseAmount+= accommodation.getBaseAmount();
    				    		 extraAdultAmount+=accommodation.getExtraAdult();
				    			 extraChildAmount+=accommodation.getExtraChild();
    				    	 }
    				    	 double discountAmount=0;
  				    		 if(blnFirstActivePromo){
   	    				    	String strTypePercent=String.valueOf(promotionFirstPercent);
   	    				    	String strTypeInr=String.valueOf(promotionFirstInr);
					    		  if(!strTypePercent.equalsIgnoreCase("null") && promotionFirstPercent!=0){
					    			  flatLastAmount = baseAmount-(baseAmount*promotionFirstPercent/100);
					    			  discountAmount+=baseAmount*promotionFirstPercent/100;
					    		  }
					    		  else if(!strTypeInr.equalsIgnoreCase("null") && promotionFirstInr!=0){
 					    			  flatLastAmount = baseAmount-promotionFirstInr;
 					    			  discountAmount+=promotionFirstInr;
 					    		  }
 					    		  else{
 					    			  flatLastAmount = baseAmount;
 					    		  } 
  				    		 }else{
				    			  flatLastAmount = baseAmount;
				    		  } 
  				    		 if(blnSecondActivePromo){
  				    			String strTypePercent=String.valueOf(promotionSecondPercent);
   	    				    	String strTypeInr=String.valueOf(promotionSecondInr);
					    		  if(!strTypePercent.equalsIgnoreCase("null") && promotionSecondPercent!=0){
					    			  flatLastAmount = flatLastAmount-(flatLastAmount*promotionSecondPercent/100);
					    			  discountAmount+=baseAmount*promotionSecondPercent/100;
					    		  }
					    		  else if(!strTypeInr.equalsIgnoreCase("null") && promotionSecondInr!=0){
 					    			  flatLastAmount = flatLastAmount-promotionSecondInr;
 					    			 discountAmount+=promotionSecondInr;
 					    		  }
 					    		  else{
 					    			  flatLastAmount = flatLastAmount;
 					    		  }
  				    		 }else{
				    			  flatLastAmount = flatLastAmount;
				    		  }
  				    		 
  				    		totalFlatLastAmount=flatLastAmount;
  				    		totalFlatLastAmount=baseAmount-discountAmount; 
  				    	 }else{
  				    		int rateCount=0,rateIdCount=0;
    						List<PropertyRate> propertyRateList =  rateController.listAllDates(getPropertyId(),accommodation.getAccommodationId(),getSourceTypeId(),d.toDate());
    				    	if(propertyRateList.size()>0)
    				    	{
    				    		 for (PropertyRate pr : propertyRateList)
    			    			 {
				    				 int propertyRateId = pr.getPropertyRateId();
				    			 List<PropertyRateDetail> rateDetailList =   detailController.list(propertyRateId,f.format(d.toDate()).toLowerCase());
    				    		 if(rateDetailList.size()>0){
    				    			 if(rateCount<1){
    				    				 for (PropertyRateDetail rate : rateDetailList){
    				    					 baseAmount +=  rate.getBaseAmount();
    				    					 extraAdultAmount+=rate.getExtraAdult();
    	  				    				 extraChildAmount+=rate.getExtraChild();
        				    			 }
    				    				 rateCount++;
    				    			 }
    				    		 }else{
    				    			 if(propertyRateList.size()==rateIdCount){
    				    				 baseAmount += accommodation.getBaseAmount();
    				    				 extraAdultAmount+=accommodation.getExtraAdult();
	  				    				 extraChildAmount+=accommodation.getExtraChild();
    				    			 }
        				    	 }
    			    			 }	    		 
    				    	 }
    				    	 else{
    				    		baseAmount += accommodation.getBaseAmount();
    				    		extraAdultAmount+=accommodation.getExtraAdult();
			    				extraChildAmount+=accommodation.getExtraChild();
    				    	 }
				    		
  				    	 }
  				    	 dateCount++;
  				     }
  				   Iterator<String> iterPromoType=arlListPromoType.iterator();
 					while(iterPromoType.hasNext()){
 						String strPromoType=iterPromoType.next();
 						if(strPromoType.equalsIgnoreCase("F") || strPromoType.equalsIgnoreCase("E")){
 							String strTypePercent=String.valueOf(promotionFirstPercent);
   				    	String strTypeInr=String.valueOf(promotionFirstInr);
				    		if(!strTypePercent.equalsIgnoreCase("null") && promotionFirstPercent!=0){
				    			promotionFirstName=String.valueOf(Math.round(promotionFirstPercent))+" % OFF";
	  							promoFirstPercent=(int) Math.round(promotionFirstPercent);
				    		}
				    		else if(!strTypeInr.equalsIgnoreCase("null") && promotionFirstInr!=0){
				    			promotionFirstName="Flat "+String.valueOf(Math.round(promotionFirstInr))+" OFF";
	  							promoFirstInr=(int) Math.round(promotionFirstInr);
				    		}
 							
 						}
 						if(strPromoType.equalsIgnoreCase("L")){
 							  String strTypePercent=String.valueOf(promotionSecondPercent);
 							  String strTypeInr=String.valueOf(promotionSecondInr);
				    		  if(!strTypePercent.equalsIgnoreCase("null") && promotionSecondPercent!=0){
				    			  promotionSecondName=String.valueOf(Math.round(promotionSecondPercent))+" % OFF";
		  						  promoSecondPercent=(int) Math.round(promotionSecondPercent);
				    		  }
				    		  else if(!strTypeInr.equalsIgnoreCase("null") && promotionSecondInr!=0){
				    			  promotionSecondName="Flat "+String.valueOf(Math.round(promotionSecondInr))+" OFF";
		  						  promoSecondInr=(int) Math.round(promotionSecondInr);
				    		  }
 							
 						}
 					}
 					arlListPromoType.clear();
 					if(blnPromoFlag){
  						if(promotionType.equalsIgnoreCase("L") || promotionType.equalsIgnoreCase("E") || promotionType.equalsIgnoreCase("F")){
  							actualTotalAmount=totalFlatLastAmount;
  							
  							if(dateCount>1){
  								//modified by gopi
  								//actualAdultAmount=extraAdultAmount/dateCount;
  								//actualChildAmount=extraChildAmount/dateCount;
  								actualAdultAmount=extraAdultAmount;
  								actualAdultAmount=extraAdultAmount;
  							}else{
  								actualAdultAmount=extraAdultAmount;
  								actualChildAmount=extraChildAmount;
  							}
  						}
  					}else{
  						actualTotalAmount=baseAmount;
  						
  						if(dateCount>1){
  							//modified by gopi
  							//actualAdultAmount=extraAdultAmount/dateCount;
  							//actualChildAmount=extraChildAmount/dateCount;
  						     actualAdultAmount=extraAdultAmount;
  							  actualChildAmount=extraChildAmount;
  							
  						}else{
  							actualAdultAmount=extraAdultAmount;
  							actualChildAmount=extraChildAmount;
  						}
  					}
  					
  					
  					actualBaseAmount=Math.round(actualTotalAmount);
  					actualTotalAdultAmount=Math.round(actualAdultAmount);
  					actualTotalChildAmount=Math.round(actualChildAmount);
  					
  					
  					if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += ",{";
					
					else
						jsonOutput += "{";
				 
					 jsonOutput += "\"propertyId\":\"" + getPropertyId() + "\"";
					 SimpleDateFormat sdfformat=new SimpleDateFormat("yyyy-MM-dd");
					 String strArrivalDate=sdfformat.format(getArrivalDate());
					 String strDepartureDate=sdfformat.format(getDepartureDate());
					
					 jsonOutput += ",\"arrivalDate\":\"" + strArrivalDate+ "\"";
					 //jsonOutput += ",\"departureDate\":\"" + strDepartureDate+ "\"";
					 PmsProperty property = propertyController.find(getPropertyId());	
					 if(property.getTaxIsActive() == true ){
				            double taxe =  (actualBaseAmount==0 ? accommodation.getBaseAmount() : actualBaseAmount );
				            double taxs = taxe/diffInDays;
				            PropertyTaxe tax = taxController.find(taxs);
						    double taxes = Math.round(tax.getTaxPercentage() / 100 * taxs) ;
						    jsonOutput += ",\"tax\":\"" + taxes + "\"";
			 			    jsonOutput += ",\"taxPercentage\":\"" + tax.getTaxPercentage() + "\"";
			 		    } else{
		 		 		    jsonOutput += ",\"tax\":\"" + 0 + "\"";
			 		 		jsonOutput += ",\"taxPercentage\":\"" + 0 + "\"";
			 		    }
					 
					 long minimum = getAvailableCount(this.propertyId,getArrivalDate(),getDepartureDate(),roomCount,accommodation.getAccommodationId()); 
						 availableCount=(int)minimum;
					 
				 
					
				 	jsonOutput += ",\"baseAmount\":\"" + (actualTotalAmount==0 ? accommodation.getBaseAmount() : actualTotalAmount ) + "\"";
					jsonOutput += ",\"totalAmount\":\"" + (actualTotalAmount==0 ? accommodation.getBaseAmount() : actualTotalAmount ) + "\"";
					jsonOutput += ",\"promoLastMinuteTimer\":\"" + (getDisplayHoursForTimer()==null ?"None" :getDisplayHoursForTimer()  )+ "\"";
					jsonOutput += ",\"promotionFirstName\":\"" + (promotionFirstName==null?"":promotionFirstName)+ "\"";
					jsonOutput += ",\"promotionSecondName\":\"" + (promotionSecondName==null?"None":promotionSecondName)+ "\"";
					jsonOutput += ",\"rooms\":\"" + 1 + "\"";
					jsonOutput += ",\"departureDate\":\"" + strDepartureDate + "\"";
	 				jsonOutput += ",\"diffDays\":\"" + diffInDays + "\""; 
					jsonOutput += ",\"accommodationId\":\"" + accommodation.getAccommodationId() + "\"";
					jsonOutput += ",\"accommodationType\":\"" + accommodation.getAccommodationType().toUpperCase() + "\"";
					jsonOutput += ",\"minOccupancy\":\"" + accommodation.getMinOccupancy() + "\"";
					jsonOutput += ",\"maxOccupancy\":\"" + accommodation.getMaxOccupancy() + "\"";
					jsonOutput += ",\"noOfAdults\":\"" + accommodation.getNoOfAdults() + "\"";
					jsonOutput += ",\"noOfChild\":\"" + accommodation.getNoOfChild() + "\"";
					
					
					jsonOutput += ",\"extraAdult\":\"" + (actualTotalAdultAmount==0 ? accommodation.getExtraAdult() : actualTotalAdultAmount ) + "\"";
					jsonOutput += ",\"extraChild\":\"" + (actualTotalChildAmount==0 ? accommodation.getExtraChild() : actualTotalChildAmount )+ "\"";
					//jsonOutput += ",\"diffDays\":\"" + diffInDays + "\"";
		    		
					discountId=35;
					PropertyDiscountManager discountController=new PropertyDiscountManager();
					PropertyDiscount propertyDiscount=discountController.find(discountId);
		    		jsonOutput += ",\"discountPercent\":\"" + (propertyDiscount.getDiscountPercentage()==0.0? "0":propertyDiscount.getDiscountPercentage())+ "\"";
		    		jsonOutput += ",\"discountId\":\"" + (discountId==0? "0" :discountId) + "\"";
			    		
					
					long rmc = 0;
					this.roomCnt = rmc;
					
					List<Long> myList = new ArrayList<Long>();
					for (DateTime d : between)
				     {
						java.util.Date availDate = d.toDate();
						PropertyAccommodationInventoryManager accommodationInventoryController = new PropertyAccommodationInventoryManager();  
						PropertyAccommodationInventory inventoryCount = accommodationInventoryController.findInventoryCount(availDate,accommodation.getAccommodationId());
	 					if(inventoryCount != null){
	 						//jsonOutput += ",\"inventoryId\":\"" + inventoryCount.getInventoryId() + "\"";
	 						PmsAvailableRooms roomCount1 = bookingController.findBookingCount(availDate, accommodation.getAccommodationId(),inventoryCount.getInventoryId());
	 						if(roomCount1.getRoomCount() == null) {
	 							String num1 = inventoryCount.getInventoryCount();
	 							long num2 = Long.parseLong(num1);
	 							PmsAvailableRooms bookingRoomCount=bookingController.findCount(availDate, accommodation.getAccommodationId());
	 							int sold=0;
	 							PropertyAccommodation accommodations=accommodationController.find(accommodation.getAccommodationId());
	   							Integer totavailable=0;
	   							 if(accommodations!=null){
	   								totavailable=accommodations.getNoOfUnits();
	   							 }
	 							if(bookingRoomCount.getRoomCount()!=null){
	 								long lngRooms=bookingRoomCount.getRoomCount();
	 								sold=(int) (lngRooms);
	 								int availRooms=0;
	 								availRooms=totavailable-sold;
	 								if(Integer.parseInt(num1)>availRooms){
	 									this.roomCnt = availRooms;	
	 								}else{
	 									this.roomCnt = num2;
	 								}
	 								jsonOutput += ",\"soldOutCount\":\"" + sold + "\"";
	 							}else{
	 								this.roomCnt = num2; 
	 								jsonOutput += ",\"soldOutCount\":\"" + 0 + "\"";
	 							}
	 							/*jsonOutput += ",\"soldOutCount\":\"" + inventoryCount.getInventoryCount() + "\"";
	 							 String num1 = inventoryCount.getInventoryCount();
	 							 long num2 = Long.parseLong(num1);
	 							this.roomCnt = num2;*/ 
	 						}
	 						else {
	 							PmsAvailableRooms bookingRoomCount=bookingController.findCount(availDate, accommodation.getAccommodationId());
	 							int intRooms=0;
	 							PropertyAccommodation accommodations=accommodationController.find(accommodation.getAccommodationId());
	   							Integer totavailable=0;
	   							 if(accommodations!=null){
	   								totavailable=accommodations.getNoOfUnits();
	   							 }
	 							if(bookingRoomCount.getRoomCount()!=null){
	 								long lngRooms=bookingRoomCount.getRoomCount();
	 								long num = roomCount1.getRoomCount();	
	 								intRooms=(int)lngRooms;
	 								String num1 = inventoryCount.getInventoryCount();
	 	 							long num2 = Long.parseLong(num1);
	 	 							int totRooms=0;
	 	 							totRooms=totavailable-intRooms;
	 	 							if(num2>totRooms){
	 	 								this.roomCnt = totRooms;	 
	 	 							}else{
	 	 								this.roomCnt = num2-num;
	 	 							}		
	 	 							jsonOutput += ",\"soldOutCount\":\"" + intRooms + "\"";
	 							}else{
	 								long num = roomCount1.getRoomCount();
	 								String num1 = inventoryCount.getInventoryCount();
	 								long num2 = Long.parseLong(num1);
	 								this.roomCnt = (num2-num);	
	 								jsonOutput += ",\"soldOutCount\":\"" + num + "\"";
	 							}
	 							/*jsonOutput += ",\"soldOutCount\":\"" + roomCount1.getRoomCount() + "\"";
	 							 long num = roomCount1.getRoomCount();
	 							 String num1 = inventoryCount.getInventoryCount();
	 							 long num2 = Long.parseLong(num1);
	 							this.roomCnt = (num2-num);*/	
	 						}
	 							
	 					}
	 					else{
	 						//jsonOutput += ",\"inventoryId\":\"" + 0 + "\""; 
							  PmsAvailableRooms pmsRoomCount = bookingController.findCount(availDate, accommodation.getAccommodationId());							
								if(pmsRoomCount.getRoomCount() == null){	
									jsonOutput += ",\"soldOutCount\":\"" + rmc + "\"";
									this.roomCnt = (accommodation.getNoOfUnits()-rmc);		
					               
								}
								else{
									jsonOutput += ",\"soldOutCount\":\"" + pmsRoomCount.getRoomCount() + "\"";
									this.roomCnt = (accommodation.getNoOfUnits()-pmsRoomCount.getRoomCount());						
								}					
								
							
				        //long availableCount = Collections.min(myList);
				        

				        //return availableCount;
				     }
	 					myList.add(this.roomCnt);
					
				     }
					/*PmsAvailableRooms roomCount = bookingController.findCount(arrival, departure,available.getAccommodationId());
					*/
					
					
					//jsonOutput += ",\"available\":\"" + available.getAvailable() + "\"";
					jsonOutput += ",\"available\":\"" + Collections.min(myList) + "\"";
					jsonOutput += "}";
  				}
			}
		
			

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	
   }
	public String getOldOfflineRoomAvailability() throws IOException {
		
	   
	   try {
			DateFormat format = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
		    java.util.Date dateStart = format.parse(getStrStartDate());
		    java.util.Date dateEnd = format.parse(getStrEndDate());
		   
		    
		    Calendar calCheckStart=Calendar.getInstance();
		    calCheckStart.setTime(dateStart);
		    java.util.Date checkInDate = calCheckStart.getTime();
		    this.arrivalDate=new Timestamp(checkInDate.getTime());
		    sessionMap.put("arrivalDate",arrivalDate);  
		    
		    Calendar calCheckEnd=Calendar.getInstance();
		    calCheckEnd.setTime(dateEnd);
		    java.util.Date checkOutDate = calCheckEnd.getTime();
		    this.departureDate=new Timestamp(checkOutDate.getTime());
		    sessionMap.put("departureDate",departureDate);
	
		    Map mapDate=new HashMap();  
			mapDate.put("arrivalDate", arrivalDate);
			mapDate.put("departureDate", departureDate);
			String startDate = new SimpleDateFormat("yyyy-MM-dd").format(this.arrivalDate);
			String endDate = new SimpleDateFormat("yyyy-MM-dd").format(this.departureDate);
			
			DateTime dtstart = DateTime.parse(startDate);
			DateTime dtend = DateTime.parse(endDate);
			java.util.Date dtfromDate = dtstart.toDate();
			java.util.Date dttoDate = dtend.toDate();
		
		 int diffInDays = (int) ((dttoDate.getTime() - dtfromDate.getTime()) / (1000 * 60 * 60 * 24));
			
			/*this.arrivalDate = getArrivalDate();
			sessionMap.put("arrivalDate",arrivalDate);
	
			this.departureDate = getDepartureDate();
			sessionMap.put("departureDate",departureDate);
			*/
			Timestamp timestamp = new Timestamp(System.currentTimeMillis());
	
			Calendar cal = Calendar.getInstance();
			cal.setTime(this.departureDate);
			cal.add(Calendar.DATE, -1);
			String calStartDate = new SimpleDateFormat("yyyy-MM-dd").format(this.arrivalDate);
			String calEndDate = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
			DateTime start = DateTime.parse(calStartDate);
			DateTime end = DateTime.parse(calEndDate);
			java.util.Date arrival = start.toDate();
			java.util.Date departure = end.toDate();
		
		
		this.propertyId = (Integer) sessionMap.get("propertyId");

		
		String jsonOutput = "";
		HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
			PmsBookingManager bookingController = new PmsBookingManager();
			PropertyTaxeManager taxController = new PropertyTaxeManager();
			PromotionManager promotionController=new PromotionManager();
			PromotionDetailManager promotionDetailController=new PromotionDetailManager();
			PropertyRateManager rateController = new PropertyRateManager();
			PropertyRateDetailManager detailController = new PropertyRateDetailManager();
			PmsPropertyManager propertyController=new PmsPropertyManager();
			response.setContentType("application/json");
			SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
			DateFormat f = new SimpleDateFormat("EEEE");
			List<PropertyAccommodation> accommodationsList =  accommodationController.list(this.propertyId);
			if(accommodationsList.size()>0)
			{
				for (PropertyAccommodation accommodations : accommodationsList) {
					Integer promoId=0;
					//Double dblBookRooms,dblGetRooms,dblBookNights,dblGetNights;
					int promotionId=0,promotionAccommId=0,promoCount=0;
					Boolean blnPromoFlag=false,promotionFlag=false,blnFirst=false;    
					String promotionType=null,promotionName=null;
					double promotionPercent=0.0;
					Boolean blnLastMinutePromotions=false,blnBookedGetRooms=false,blnStatus=false,promoFlag=false,blnSmartPriceFlag=false;
					int dateCount=0;
					double totalAmount=0.0,extraAdultAmount=0.0,extraChildAmount=0.0;
					int roomCount=0,availableCount=0,totalCount=0,countDate=0,promotionHours=0,promoAccommId=0;
				    double dblPercentCount=0.0,dblSmartPricePercent=0.0,dblPercentFrom=0.0,dblPercentTo=0.0,lastAmount=0.0, flatAmount=0.0,
				    		smartPricePercent=0.0,discountPercent=0.0,actualAmount=0.0, totalActualAmount=0.0,totalActualExtraAdultAmount=0.0,totalActualExtraChildAmount=0.0;
				    Double totalGetNights=0.0;
				    Boolean isPositive=false,isNegative=false;
				    int smartPriceId=0,discountId=0;
	    			Double dblGetNights=0.0,dblBookNights=0.0,dblGetRooms=0.0,dblBookRooms=0.0;
	    			Integer totalRoom=0,totalNight=0;
	    			Timestamp lastMinStartDate=null,lastMinEndDate=null;
					sourceId = 1;
					
					ArrayList<String> arlPromoType=new ArrayList<String>();
			    	double actualBaseAmount=0.0,actualTotalAdultAmount=0.0,actualTotalChildAmount=0.0,actualTotal=0.0,
			    			actualAdultAmount=0.0,actualChildAmount=0.0, baseAmount=0.0,promoBaseAmount=0.0,promoAdultAmount=0.0,promoChildAmount=0.0,lastMinuteHours=0.0;
				   
			    	List<PmsSmartPrice> listSmartPriceDetail=null; 
			    	List<AccommodationRoom> listAccommodationRoom=null;
			    	List<PmsPromotionDetails> listPromotionDetailCheck=null;
			    	
					List<DateTime> between = DateUtil.getDateRange(start, end);
					 boolean blnActivePromo=false,blnNotActivePromo=false,basePromoFlag=false;
					 ArrayList<Integer> arlActivePromo=new ArrayList<Integer>(); 
					for (DateTime d : between)
				     {
						 List<PmsPromotions> listAllPromotions= promotionController.listDatePromotions(this.propertyId,d.toDate());
						 if(!listAllPromotions.isEmpty() && listAllPromotions.size()>0){
							 for(PmsPromotions promos:listAllPromotions){
								 if(promos.getPromotionType().equalsIgnoreCase("F")){
									 if(arlActivePromo.isEmpty()){
										 arlActivePromo.add(promos.getPromotionId());
									 }else if(!arlActivePromo.contains(promos.getPromotionId())){
										 arlActivePromo.add(promos.getPromotionId());
									 }
								 }
							 }
						 }else{
							 blnNotActivePromo=true;
						 }
				     }
					 int intPromoSize=arlActivePromo.size();
					 if(arlActivePromo.isEmpty() || blnNotActivePromo){
						 blnActivePromo=false;
						 basePromoFlag=false;
					 }else{
						 blnActivePromo=true;
					 }
					 
					 listAccommodationRoom=accommodationController.listPropertyTotalRooms(this.propertyId, accommodations.getAccommodationId());
					 
					 
						if(listAccommodationRoom.size()>0 && !listAccommodationRoom.isEmpty()){
							for(AccommodationRoom roomsList:listAccommodationRoom){
								 roomCount=(int) roomsList.getRoomCount();
								 break;
							 }
						 }
						
					 for (DateTime d : between)
				     {

				    	 boolean SmartPriceEnableCheck=false;
				    	 if(blnActivePromo){
							List<PmsPromotions> listFlatPromotions= promotionController.listDatePromotions(this.propertyId,d.toDate());
					    	if(listFlatPromotions.size()>0 && !listFlatPromotions.isEmpty()){
					    		for(PmsPromotions promotions:listFlatPromotions){
					    			 if(promotions.getPromotionType().equalsIgnoreCase("F")){
					    				 promotionId=promotions.getPromotionId();
					    				 listPromotionDetailCheck=promotionDetailController.listPromotionDetails(promotions.getPromotionId(),f.format(d.toDate()).toLowerCase());
					    				 for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
				    						 if(promoCount==0){
				    							 promotionPercent=promoDetails.getPromotionPercentage();
					    						 promotionFlag=true;
							    				 blnPromoFlag=true;
							    				 promotionType=promotions.getPromotionType();
							    				 arlPromoType.add(promotionType);
				    						 }
				    						 promoCount++;
				    					 }
					    				 promoId=promotions.getPromotionId();
					    			 }else{
					    				 List<PmsPromotions> listPmsPromotions= promotionController.listDatePromotions(this.propertyId,accommodations.getAccommodationId(),d.toDate());
						    			 if(listPmsPromotions.size()>0 &&!listPmsPromotions.isEmpty()){
						    				 for(PmsPromotions pmsPromotions:listPmsPromotions){
						    					 promotionId=pmsPromotions.getPromotionId();
						    					 promoAccommId=pmsPromotions.getPropertyAccommodation().getAccommodationId();
						    					 listPromotionDetailCheck=promotionDetailController.listPromotionDetails(pmsPromotions.getPromotionId(),f.format(d.toDate()).toLowerCase());
							    				 if(listPromotionDetailCheck.size()>0 && !listPromotionDetailCheck.isEmpty()){
								        			 if(listPromotionDetailCheck.size()>1){
									    				 promotionFlag=false;
									    				 promotionType=null;
									    				 blnPromoFlag=false;
									    				 basePromoFlag=true;
									    			 }else if(listPromotionDetailCheck.size()==1){
									    				 if(!pmsPromotions.getPromotionType().equalsIgnoreCase("F")){
										    				 if(promoAccommId==accommodations.getAccommodationId()){
										    					 promotionAccommId=promoAccommId;
										    					 lastMinStartDate=pmsPromotions.getStartDate();
											    				 lastMinEndDate=pmsPromotions.getEndDate();
											    				 Double nightsGet=0.0,nightsBook=0.0;
											    				 if(pmsPromotions.getPromotionType().equalsIgnoreCase("N")){
											    					 for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
											    						 nightsBook=promoDetails.getBookNights();
											    						 nightsGet=promoDetails.getGetNights();
											    						
											    						 totalGetNights=nightsBook+nightsGet;
										    			    		     Integer intTotalGetNights=totalGetNights.intValue()-1;
										    			    			 Calendar calCheckOut=Calendar.getInstance();
										    			    			 calCheckOut.setTime(getDepartureDate());
										    			    			 calCheckOut.add(Calendar.DATE,intTotalGetNights);
										    			    			 java.util.Date checkOutDte = calCheckOut.getTime();             
										    			    			 String departureDate = format1.format(checkOutDte);
										    							 
										    			    			 java.util.Date fromDate = format1.parse(departureDate);
										    			    			 Timestamp tsDepartureDate=new Timestamp(fromDate.getTime());
										    			    			 String strStartDate=format1.format(getArrivalDate());
										    			    			 String strEndDate=format1.format(tsDepartureDate);
										    			    			 DateTime Startdate = DateTime.parse(strStartDate);
										    			    			 DateTime Enddate = DateTime.parse(strEndDate);
										    			    			 java.util.Date StartDate = Startdate.toDate();
									    			    		 		 java.util.Date EndDate = Enddate.toDate();
									    			    		 		 int diffDays = (int) ((EndDate.getTime() - StartDate.getTime()) / (1000 * 60 * 60 * 24));
										    			    			 
										    			    			 long minimumCount = getAvailableCount(getPropertyId(),getArrivalDate(),tsDepartureDate,roomCount,promoAccommId);
										    			    			 
												    					 if((int)minimumCount>0){
												    						 if(nightsBook.intValue()==diffInDays){
													    						 promotionFlag=true;
													    						 blnPromoFlag=true;
													    						 promotionType=pmsPromotions.getPromotionType();
													    						 arlPromoType.add(promotionType);
													    						 promoId=pmsPromotions.getPromotionId();
													    					 }else{
													    						 promotionFlag=false;
													    						 promotionType=null;
													    						 blnPromoFlag=false;
													    						 basePromoFlag=true;
													    					 }
												    					 }else{
												    						 promotionFlag=false;
												    						 promotionType=null;
												    						 blnPromoFlag=false;
												    						 basePromoFlag=true;
												    					 }
												    				 }
											    				 }else if(pmsPromotions.getPromotionType().equalsIgnoreCase("R")){
											    					 
												    				 for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
												    					 dblBookRooms=promoDetails.getGetRooms();
												    					 dblGetRooms=promoDetails.getBookRooms();
											    					 }
									    							 
									    							 long minimum = getAvailableCount(getPropertyId(),getArrivalDate(),getDepartureDate(),roomCount,promoAccommId); 
									    							 
									    							 blnBookedGetRooms=bookGetRooms((int)minimum,dblGetRooms,dblBookRooms);
								    						    	 if(!blnBookedGetRooms){
								    						    		 promotionFlag=false;
								    						    		 promotionType=null;
								    						    		 blnPromoFlag=false;
								    						    		 basePromoFlag=true;
								    						    	 }else{
								    						    		 promotionFlag=true;
								    						    		 blnPromoFlag=true;
								    						    		 promotionType=pmsPromotions.getPromotionType();
								    						    		 arlPromoType.add(promotionType);
								    						    		 promoId=pmsPromotions.getPromotionId();
								    						    	 }
											    				 }else if(pmsPromotions.getPromotionType().equalsIgnoreCase("L")){
											    					 for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
											    						 lastMinuteHours=promoDetails.getPromotionHours();
											    						 promotionPercent=promoDetails.getPromotionPercentage();
											    					 }
											    					
											    					 long minimumCount = getAvailableCount(getPropertyId(),getArrivalDate(),getDepartureDate(),roomCount,promoAccommId);
									    			    			 if((int)minimumCount>0){
									    			    				 blnLastMinutePromotions=getCurrentAvailable(getPropertyId(),getArrivalDate(),getDepartureDate(),(int)lastMinuteHours,(long)roomCount,promoAccommId,lastMinStartDate,lastMinEndDate);
									    			    				 if(blnLastMinutePromotions){
									    			    					 promotionFlag=true; 
									    			    					 blnPromoFlag=true;
									    			    					 promotionType=pmsPromotions.getPromotionType();
									    			    					 arlPromoType.add(promotionType);
									    			    					 promoId=pmsPromotions.getPromotionId();
									    			    				 }
									    			    			 }else{
									    			    				 promotionFlag=false;
									    			    				 promotionType=null;
									    			    				 blnPromoFlag=false;
									    			    				 basePromoFlag=true;
									    			    			 }
											    				 }
										    				 }
									    				 }
									    			 }
								        		 }
						    				 }
						    			 }
					    			 }
					    		}
					    	}
					    	if(arlPromoType.contains("F")){
			    			    promoFlag=true;
			    			    int rateCount=0,rateIdCount=0;
	    						List<PropertyRate> propertyRateList =  rateController.listAllDates(this.propertyId,accommodations.getAccommodationId(),getSourceId(),d.toDate());
	    						List<PropertyRate> currentDateSmartPriceActive=rateController.listCurrentSmartPriceActive(this.propertyId,accommodations.getAccommodationId(), d.toDate());
	    				    	if(propertyRateList.size()>0)
	    				    	{
	    				    		 for (PropertyRate pr : propertyRateList)
	    			    			 {
					    				 int propertyRateId = pr.getPropertyRateId();
	        				    		 Boolean SmartPriceCheck=pr.getSmartPriceIsActive();
							    		 if(SmartPriceCheck){
							    			 List<PropertyRateDetail> rateDetailList =   detailController.list(propertyRateId,f.format(d.toDate()).toLowerCase());
								    		 if(rateDetailList.size()>0){
								    			 for (PropertyRateDetail rate : rateDetailList){
								    				 if(rateCount<1){
								    					 SmartPriceEnableCheck=true;
								    					 baseAmount =  rate.getBaseAmount();
									    				 extraAdultAmount+=rate.getExtraAdult();
									    				 extraChildAmount+=rate.getExtraChild();
									    				 rateCount++;
								    				 }
								    			 }
								    		 }
							    		 }else{
							    			 List<PropertyRateDetail> rateDetailList =   detailController.list(propertyRateId,f.format(d.toDate()).toLowerCase());
		        				    		 if(rateDetailList.size()>0){
		        				    			 if(rateCount<1){
		        				    				 for (PropertyRateDetail rate : rateDetailList){
			        				    				 if(currentDateSmartPriceActive.size()>0){
			        				    					 SmartPriceEnableCheck=true;
			        				    					 baseAmount =  rate.getBaseAmount();
		    	        				    				 extraAdultAmount+=rate.getExtraAdult();
		    	        				    				 extraChildAmount+=rate.getExtraChild();
			        				    				 }else{
			        				    					 baseAmount =  rate.getBaseAmount();
		    	        				    				 extraAdultAmount+=rate.getExtraAdult();
		    	        				    				 extraChildAmount+=rate.getExtraChild();
			        				    				 }
			        				    			 }
		        				    				 rateCount++;
		        				    			 }
		        				    		 }else{
		        				    			 if(propertyRateList.size()==rateIdCount){
		        				    				 baseAmount = accommodations.getBaseAmount();
			            				    		 extraAdultAmount+=accommodations.getExtraAdult();
			        			    				 extraChildAmount+=accommodations.getExtraChild();
		        				    			 }
		            				    	 }
							    		 }
	    			    			 }	    		 
	    				    	 }
	    				    	 else{
	    				    		 baseAmount = accommodations.getBaseAmount();
	    				    		 extraAdultAmount+=accommodations.getExtraAdult();
				    				 extraChildAmount+=accommodations.getExtraChild();
	    				    	 }
	    				    	actualBaseAmount+=baseAmount;
	    				    	String strTypePercent=String.valueOf(promotionPercent);
					    		if(!strTypePercent.equalsIgnoreCase("null")){
					    		   flatAmount = baseAmount-(baseAmount*promotionPercent/100);
					    		}
					    		else{
					    		   flatAmount = baseAmount;
					    		} 
					    		
					    		flatAmount=Math.round(flatAmount);
					    		actualTotal=flatAmount;
			    		  }else if(arlPromoType.contains("L") || arlPromoType.contains("N") || arlPromoType.contains("R")){
					    		promoFlag=true;
					    		List<PmsPromotions> listPromotions= promotionController.listDatePromotions(this.propertyId, promoAccommId, arrivalDate);
								if(listPromotions.size()>0 && !listPromotions.isEmpty()){
						    		 for(PmsPromotions promotions:listPromotions){
						    			 listPromotionDetailCheck.clear();
						    			 listPromotionDetailCheck=promotionDetailController.listPromotionDetails(promotions.getPromotionId(),f.format(d.toDate()).toLowerCase());
						    			 if(listPromotionDetailCheck.size()>0 && !listPromotionDetailCheck.isEmpty()){
						    				 	promotionId=promotions.getPromotionId();
						    				 	promotionType=promotions.getPromotionType();
						    				 	if(promotionType.equalsIgnoreCase("L")){
							    					 listPromotionDetailCheck=promotionDetailController.listPromotionDetails(promotionId,f.format(d.toDate()).toLowerCase());
										    		  if(listPromotionDetailCheck.size()>0){
							    						 for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
							    							 promotionPercent=promoDetails.getPromotionPercentage();
							    							 promotionHours=promoDetails.getPromotionHours();
							    						 }
										    		} 
							    					int rateCount=0,rateIdCount=0;
						    						List<PropertyRate> propertyRateList =  rateController.listAllDates(this.propertyId,accommodations.getAccommodationId(),getSourceId(),d.toDate());
						    						List<PropertyRate> currentDateSmartPriceActive=rateController.listCurrentSmartPriceActive(this.propertyId,accommodations.getAccommodationId(), d.toDate());
						    				    	if(propertyRateList.size()>0)
						    				    	{
						    				    		 for (PropertyRate pr : propertyRateList)
						    			    			 {
										    				 int propertyRateId = pr.getPropertyRateId();
						        				    		 Boolean SmartPriceCheck=pr.getSmartPriceIsActive();
												    		 if(SmartPriceCheck){
												    			 List<PropertyRateDetail> rateDetailList =   detailController.list(propertyRateId,f.format(d.toDate()).toLowerCase());
													    		 if(rateDetailList.size()>0){
													    			 for (PropertyRateDetail rate : rateDetailList){
													    				 if(rateCount<1){
													    					 SmartPriceEnableCheck=true;
													    					 baseAmount =  rate.getBaseAmount();
														    				 extraAdultAmount+=rate.getExtraAdult();
														    				 extraChildAmount+=rate.getExtraChild();
														    				 rateCount++;
													    				 }
													    			 }
													    		 }
												    		 }else{
												    			 List<PropertyRateDetail> rateDetailList =   detailController.list(propertyRateId,f.format(d.toDate()).toLowerCase());
							        				    		 if(rateDetailList.size()>0){
							        				    			 if(rateCount<1){
							        				    				 for (PropertyRateDetail rate : rateDetailList){
								        				    				 if(currentDateSmartPriceActive.size()>0){
								        				    					 SmartPriceEnableCheck=true;
								        				    					 baseAmount =  rate.getBaseAmount();
							    	        				    				 extraAdultAmount+=rate.getExtraAdult();
							    	        				    				 extraChildAmount+=rate.getExtraChild();
								        				    				 }else{
								        				    					 baseAmount =  rate.getBaseAmount();
							    	        				    				 extraAdultAmount+=rate.getExtraAdult();
							    	        				    				 extraChildAmount+=rate.getExtraChild();
								        				    				 }
								        				    			 }
							        				    				 rateCount++;
							        				    			 }
							        				    		 }else{
							        				    			 if(propertyRateList.size()==rateIdCount){
							        				    				 baseAmount = accommodations.getBaseAmount();
								            				    		 extraAdultAmount+=accommodations.getExtraAdult();
								        			    				 extraChildAmount+=accommodations.getExtraChild();
							        				    			 }
							            				    	 }
												    		 }
						    			    			 }	    		 
						    				    	 }
						    				    	 else{
						    				    		 baseAmount = accommodations.getBaseAmount();
						    				    		 extraAdultAmount+=accommodations.getExtraAdult();
									    				 extraChildAmount+=accommodations.getExtraChild();
						    				    	 }
						    				    	actualBaseAmount+=baseAmount;
						    				    	String strTypePercent=String.valueOf(promotionPercent);
											    	 if(!strTypePercent.equalsIgnoreCase("null")){
											    		 lastAmount = baseAmount-(baseAmount*promotionPercent/100);
											    	 }
											    	 else{
											    		 lastAmount = baseAmount;
											    	 } 
											    	 lastAmount=Math.round(lastAmount);
											    	 actualTotal=lastAmount;
						    				 }else if(promotionType.equalsIgnoreCase("R")){
						    					 listPromotionDetailCheck=promotionDetailController.listPromotionDetails(promotionId,f.format(d.toDate()).toLowerCase());
						    					 if(listPromotionDetailCheck.size()>0){
						    						 for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
						    							 dblBookRooms=promoDetails.getBookRooms();
						    							 dblGetRooms=promoDetails.getGetRooms();
						    							 promoBaseAmount =promoDetails.getBaseAmount();
						    							 promoAdultAmount =promoDetails.getExtraAdult();
						    							 promoChildAmount =promoDetails.getExtraChild(); 
						    						 }
						    					 }
						    				 }else if(promotionType.equalsIgnoreCase("N")){
						    					 listPromotionDetailCheck=promotionDetailController.listPromotionDetails(promotionId,f.format(d.toDate()).toLowerCase());
						    					 if(listPromotionDetailCheck.size()>0){
						    						 for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
						    							 dblBookNights=promoDetails.getBookNights();
						    							 dblGetNights=promoDetails.getGetNights();
						    							 promoBaseAmount =promoDetails.getBaseAmount();
						    							 promoAdultAmount =promoDetails.getExtraAdult();
						    							 promoChildAmount =promoDetails.getExtraChild(); 
						    						 }
						    					 }
						    				 }
						    			 }
						    		 }
								}
					    	}else{
					    		if(basePromoFlag){
	    				    		 baseAmount = accommodations.getBaseAmount();
	    				    		 extraAdultAmount+=accommodations.getExtraAdult();
				    				 extraChildAmount+=accommodations.getExtraChild();
				    				 actualBaseAmount+=baseAmount;
				    				 actualTotal=baseAmount;
								}else{
									int rateCount=0,rateIdCount=0;
		    						List<PropertyRate> propertyRateList =  rateController.listAllDates(this.propertyId,accommodations.getAccommodationId(),getSourceId(),d.toDate());
		    						List<PropertyRate> currentDateSmartPriceActive=rateController.listCurrentSmartPriceActive(this.propertyId,accommodations.getAccommodationId(), d.toDate());
		    				    	if(propertyRateList.size()>0)
		    				    	{
		    				    		 for (PropertyRate pr : propertyRateList)
		    			    			 {
						    				 int propertyRateId = pr.getPropertyRateId();
		        				    		 Boolean SmartPriceCheck=pr.getSmartPriceIsActive();
								    		 if(SmartPriceCheck){
								    			 List<PropertyRateDetail> rateDetailList =   detailController.list(propertyRateId,f.format(d.toDate()).toLowerCase());
									    		 if(rateDetailList.size()>0){
									    			 for (PropertyRateDetail rate : rateDetailList){
									    				 if(rateCount<1){
									    					 SmartPriceEnableCheck=true;
									    					 baseAmount =  rate.getBaseAmount();
										    				 extraAdultAmount+=rate.getExtraAdult();
										    				 extraChildAmount+=rate.getExtraChild();
										    				 rateCount++;
									    				 }
									    			 }
									    		 }
								    		 }else{
								    			 List<PropertyRateDetail> rateDetailList =   detailController.list(propertyRateId,f.format(d.toDate()).toLowerCase());
			        				    		 if(rateDetailList.size()>0){
			        				    			 if(rateCount<1){
			        				    				 for (PropertyRateDetail rate : rateDetailList){
				        				    				 if(currentDateSmartPriceActive.size()>0){
				        				    					 SmartPriceEnableCheck=true;
				        				    					 baseAmount =  rate.getBaseAmount();
			    	        				    				 extraAdultAmount+=rate.getExtraAdult();
			    	        				    				 extraChildAmount+=rate.getExtraChild();
				        				    				 }else{
				        				    					 baseAmount =  rate.getBaseAmount();
			    	        				    				 extraAdultAmount+=rate.getExtraAdult();
			    	        				    				 extraChildAmount+=rate.getExtraChild();
				        				    				 }
				        				    			 }
			        				    				 rateCount++;
			        				    			 }
			        				    		 }else{
			        				    			 if(propertyRateList.size()==rateIdCount){
			        				    				 baseAmount = accommodations.getBaseAmount();
				            				    		 extraAdultAmount+=accommodations.getExtraAdult();
				        			    				 extraChildAmount+=accommodations.getExtraChild();
			        				    			 }
			            				    	 }
								    		 }
		    			    			 }	    		 
		    				    	 }
		    				    	 else{
		    				    		 baseAmount = accommodations.getBaseAmount();
		    				    		 extraAdultAmount+=accommodations.getExtraAdult();
					    				 extraChildAmount+=accommodations.getExtraChild();
		    				    	 }
		    				    	actualBaseAmount+=baseAmount;
		    				    	actualTotal=baseAmount;
								}
					    	}
						} else{
								if(basePromoFlag){
	    				    		 baseAmount = accommodations.getBaseAmount();
	    				    		 extraAdultAmount+=accommodations.getExtraAdult();
				    				 extraChildAmount+=accommodations.getExtraChild();
				    				 actualBaseAmount+=baseAmount;
				    				 actualTotal=baseAmount;
								}else{
									int rateCount=0,rateIdCount=0;
		    						List<PropertyRate> propertyRateList =  rateController.listAllDates(this.propertyId,accommodations.getAccommodationId(),getSourceId(),d.toDate());
		    						List<PropertyRate> currentDateSmartPriceActive=rateController.listCurrentSmartPriceActive(this.propertyId,accommodations.getAccommodationId(), d.toDate());
		    				    	if(propertyRateList.size()>0)
		    				    	{
		    				    		 for (PropertyRate pr : propertyRateList)
		    			    			 {
						    				 int propertyRateId = pr.getPropertyRateId();
		        				    		 Boolean SmartPriceCheck=pr.getSmartPriceIsActive();
								    		 if(SmartPriceCheck){
								    			 List<PropertyRateDetail> rateDetailList =   detailController.list(propertyRateId,f.format(d.toDate()).toLowerCase());
									    		 if(rateDetailList.size()>0){
									    			 for (PropertyRateDetail rate : rateDetailList){
									    				 if(rateCount<1){
									    					 SmartPriceEnableCheck=true;
									    					 baseAmount =  rate.getBaseAmount();
										    				 extraAdultAmount+=rate.getExtraAdult();
										    				 extraChildAmount+=rate.getExtraChild();
										    				 rateCount++;
									    				 }
									    			 }
									    		 }
								    		 }else{
								    			 List<PropertyRateDetail> rateDetailList =   detailController.list(propertyRateId,f.format(d.toDate()).toLowerCase());
			        				    		 if(rateDetailList.size()>0){
			        				    			 if(rateCount<1){
			        				    				 for (PropertyRateDetail rate : rateDetailList){
				        				    				 if(currentDateSmartPriceActive.size()>0){
				        				    					 SmartPriceEnableCheck=true;
				        				    					 baseAmount =  rate.getBaseAmount();
			    	        				    				 extraAdultAmount+=rate.getExtraAdult();
			    	        				    				 extraChildAmount+=rate.getExtraChild();
				        				    				 }else{
				        				    					 baseAmount =  rate.getBaseAmount();
			    	        				    				 extraAdultAmount+=rate.getExtraAdult();
			    	        				    				 extraChildAmount+=rate.getExtraChild();
				        				    				 }
				        				    			 }
			        				    				 rateCount++;
			        				    			 }
			        				    		 }else{
			        				    			 if(propertyRateList.size()==rateIdCount){
			        				    				 baseAmount = accommodations.getBaseAmount();
				            				    		 extraAdultAmount+=accommodations.getExtraAdult();
				        			    				 extraChildAmount+=accommodations.getExtraChild();
			        				    			 }
			            				    	 }
								    		 }
		    			    			 }	    		 
		    				    	 }
		    				    	 else{
		    				    		 baseAmount = accommodations.getBaseAmount();
		    				    		 extraAdultAmount+=accommodations.getExtraAdult();
					    				 extraChildAmount+=accommodations.getExtraChild();
		    				    	 }
		    				    	actualBaseAmount+=baseAmount;
		    				    	actualTotal=baseAmount;
								}
					    	}
					    	double smartPriceAmount=0.0;
					    	if(SmartPriceEnableCheck){
					    		 listAccommodationRoom=accommodationController.listPropertyTotalRooms(this.propertyId, accommodations.getAccommodationId());
			      				 if(listAccommodationRoom.size()>0 && !listAccommodationRoom.isEmpty()){
			      					 for(AccommodationRoom roomsList:listAccommodationRoom){
			      						 roomCount=(int) roomsList.getRoomCount();
			      						 long minimum = getAvailableCount(this.propertyId,getArrivalDate(),getDepartureDate(),roomCount,accommodations.getAccommodationId()); 
			      						 availableCount=(int)minimum;
			      						 dblPercentCount=(double)((roomCount-availableCount)*100)/roomCount;
			      						 totalCount=(int) Math.round(dblPercentCount);
			      						 listSmartPriceDetail=detailController.listSmartPriceDetail(totalCount);
			      						 if(listSmartPriceDetail.size()>0 && !listSmartPriceDetail.isEmpty()){
			      							 for(PmsSmartPrice smartPrice: listSmartPriceDetail){
			      								 dblSmartPricePercent=smartPrice.getAmountPercent();
			      								 dblPercentFrom=smartPrice.getPercentFrom();
			      								 dblPercentTo=smartPrice.getPercentTo();
			      								 sessionMap.put("baseTotalAmount", actualTotal);
					    				    	 sessionMap.put("smartPriceId", smartPrice.getSmartPriceId());
					    				    	 sessionMap.put("smartPricePercent", dblSmartPricePercent);
			      								 if(dblPercentFrom>=0 && dblPercentTo<=59){
			      									 isNegative=true;
			      								 }else if(dblPercentFrom>=60 && dblPercentTo<=99){
			      									 isPositive=true;
			      								 }
			      								 if(isNegative){
			      									smartPriceAmount=actualTotal-(actualTotal*dblSmartPricePercent/100);
			      								 }
			      								 if(isPositive){
			      									smartPriceAmount=actualTotal+(actualTotal*dblSmartPricePercent/100);
			      								 }
			      							 }
			      						 }
			      					 }
			      				 }
			      				smartPriceAmount=Math.round(smartPriceAmount);
			      				 blnSmartPriceFlag=true;
					    	 }
							 if(smartPriceAmount>0){
								 actualAmount+=smartPriceAmount;
							 }else if(lastAmount>0){
								 actualAmount+=lastAmount;
							 }else if(flatAmount>0){
								 actualAmount+=flatAmount;
							 }else{
								 actualAmount+=baseAmount;
							 }
					    	 dateCount++;
				     }
					 Iterator<String> iterPromoType=arlPromoType.iterator();
						while(iterPromoType.hasNext()){
							String strPromoType=iterPromoType.next();
							if(strPromoType.equalsIgnoreCase("F")){
								promotionName=String.valueOf(Math.round(promotionPercent))+" % OFF";
							}else if(strPromoType.equalsIgnoreCase("R")){
								promotionName="Book "+Math.round(dblBookRooms)+" and Get "+Math.round(dblGetRooms)+" Rooms FREE";
							}else if(strPromoType.equalsIgnoreCase("N")){
								promotionName="Book "+Math.round(dblBookNights)+" and Get "+Math.round(dblGetNights)+" Nights FREE";
							}else if(strPromoType.equalsIgnoreCase("L")){
								promotionName=String.valueOf(Math.round(promotionPercent))+" % OFF";
							}
						}
						arlPromoType.clear();
						
					 if(promoFlag){
							if(promotionType.equalsIgnoreCase("R") || promotionType.equalsIgnoreCase("N")){
								totalAmount=promoBaseAmount;
								actualAdultAmount=promoAdultAmount;
								actualChildAmount=promoChildAmount; 
							}else if(promotionType.equalsIgnoreCase("F")){
								totalAmount=actualAmount;
								if(dateCount>1){
									//modified by gopi
									//actualAdultAmount=extraAdultAmount/dateCount;
									//actualChildAmount=extraChildAmount/dateCount;
									actualAdultAmount=extraAdultAmount;
		  							actualChildAmount=extraChildAmount;
								}else{
									actualAdultAmount=extraAdultAmount;
									actualChildAmount=extraChildAmount;
								}
							}
							else if(promotionType.equalsIgnoreCase("L")){
								totalAmount=actualAmount;
								if(dateCount>1){
									//modified by gopi
									//actualAdultAmount=extraAdultAmount/dateCount;
									//actualChildAmount=extraChildAmount/dateCount;
									actualAdultAmount=extraAdultAmount;
		  							actualChildAmount=extraChildAmount;
								}else{
									actualAdultAmount=extraAdultAmount;
									actualChildAmount=extraChildAmount;
								}
							}
						}else{
							/*if(blnSmartPriceFlag){
								totalAmount=totalSmartPriceAmount;
							}else{
								totalAmount=baseAmount;
							}*/
							totalAmount=actualAmount;
							if(dateCount>1){
								//modified by gopi
								//actualAdultAmount=extraAdultAmount/dateCount;
								//actualChildAmount=extraChildAmount/dateCount;
								actualAdultAmount=extraAdultAmount;
	  							actualChildAmount=extraChildAmount;
							}else{
								actualAdultAmount=extraAdultAmount;
								actualChildAmount=extraChildAmount;
							}
						}
						
						if(promotionName==null){
							promotionName=" ";
						}
						
						
						totalAmount=Math.round(totalAmount);
						actualTotalAdultAmount=Math.round(actualAdultAmount);
						actualTotalChildAmount=Math.round(actualChildAmount);
						
						if (!jsonOutput.equalsIgnoreCase(""))
							jsonOutput += ",{";
						
						else
							jsonOutput += "{";
					 
						 jsonOutput += "\"propertyId\":\"" + getPropertyId() + "\"";
						 SimpleDateFormat sdfformat=new SimpleDateFormat("yyyy-MM-dd");
						 String strArrivalDate=sdfformat.format(getArrivalDate());
						 String strDepartureDate=sdfformat.format(getDepartureDate());
						
						 jsonOutput += ",\"arrivalDate\":\"" + strArrivalDate+ "\"";
						 //jsonOutput += ",\"departureDate\":\"" + strDepartureDate+ "\"";
						 PmsProperty property = propertyController.find(getPropertyId());	
						 if(property.getTaxIsActive() == true ){
					            double taxe =  (actualAmount==0 ? accommodations.getBaseAmount() : actualAmount );
					            double taxs = taxe/diffInDays;
					            PropertyTaxe tax = taxController.find(taxs);
							    double taxes = Math.round(tax.getTaxPercentage() / 100 * taxs) ;
							    jsonOutput += ",\"tax\":\"" + taxes + "\"";
				 			    jsonOutput += ",\"taxPercentage\":\"" + tax.getTaxPercentage() + "\"";
				 		    } else{
			 		 		    jsonOutput += ",\"tax\":\"" + 0 + "\"";
				 		 		jsonOutput += ",\"taxPercentage\":\"" + 0 + "\"";
				 		    }
						 
						 long minimum = getAvailableCount(this.propertyId,getArrivalDate(),getDepartureDate(),roomCount,accommodations.getAccommodationId()); 
							 availableCount=(int)minimum;
						 
					 
						
					 	jsonOutput += ",\"baseAmount\":\"" + (totalAmount==0 ? accommodations.getBaseAmount() : totalAmount ) + "\"";
						jsonOutput += ",\"totalAmount\":\"" + (totalAmount==0 ? accommodations.getBaseAmount() : totalAmount ) + "\"";
						
						rooms=1;
						if(promoId!=null && promoId!=0){
							if(rooms==dblGetRooms.intValue()){
		 	 	 				totalRoom=rooms+dblBookRooms.intValue();
		 	 	 				jsonOutput += ",\"rooms\":\"" + totalRoom + "\"";
		 	 	 			}else{
		 	 	 				jsonOutput += ",\"rooms\":\"" + 1 + "\"";
		 	 	 			}
							
							if(dblBookNights.intValue()==diffInDays){
								Calendar calNights = Calendar.getInstance();
								calNights.setTime(getDepartureDate());
								totalGetNights=dblBookNights+dblGetNights;
   			    		     	Integer intTotalGetNights=totalGetNights.intValue()-1;
								calNights.add(Calendar.DATE, intTotalGetNights);
		 	 	 			    totalNight=dblGetNights.intValue()+diffInDays;
		 	 	 			    java.util.Date checkOut = calNights.getTime();
		 	 	 			    departure=new Timestamp(checkOut.getTime());
		 	 			    	jsonOutput += ",\"departureDate\":\"" + sdfformat.format(departure) + "\"";
		 	 			    	jsonOutput += ",\"diffDays\":\"" + totalNight + "\""; 
							}else{
								jsonOutput += ",\"departureDate\":\"" + strDepartureDate + "\"";
		 	 					jsonOutput += ",\"diffDays\":\"" + diffInDays + "\""; 
							}
						}else{
							jsonOutput += ",\"rooms\":\"" + 1 + "\"";
							jsonOutput += ",\"departureDate\":\"" + strDepartureDate + "\"";
	 	 					jsonOutput += ",\"diffDays\":\"" + diffInDays + "\""; 
						}
						
						jsonOutput += ",\"accommodationId\":\"" + accommodations.getAccommodationId() + "\"";
						jsonOutput += ",\"promotionName\":\"" + promotionName+ "\"";
						jsonOutput += ",\"accommodationType\":\"" + accommodations.getAccommodationType().toUpperCase() + "\"";
						jsonOutput += ",\"minOccupancy\":\"" + accommodations.getMinOccupancy() + "\"";
						jsonOutput += ",\"maxOccupancy\":\"" + accommodations.getMaxOccupancy() + "\"";
						jsonOutput += ",\"noOfAdults\":\"" + accommodations.getNoOfAdults() + "\"";
						jsonOutput += ",\"noOfChild\":\"" + accommodations.getNoOfChild() + "\"";
						
						jsonOutput += ",\"bookRooms\":\"" + dblBookRooms.intValue() + "\"";
						jsonOutput += ",\"getRooms\":\"" + dblGetRooms.intValue() + "\"";
						jsonOutput += ",\"bookNights\":\"" + dblBookNights.intValue() + "\"";
						jsonOutput += ",\"getNights\":\"" + dblBookNights.intValue() + "\"";
						
						jsonOutput += ",\"extraAdult\":\"" + (actualTotalAdultAmount==0 ? accommodations.getExtraAdult() : actualTotalAdultAmount ) + "\"";
						jsonOutput += ",\"extraChild\":\"" + (actualTotalChildAmount==0 ? accommodations.getExtraChild() : actualTotalChildAmount )+ "\"";
						//jsonOutput += ",\"diffDays\":\"" + diffInDays + "\"";
						jsonOutput += ",\"discountPercent\":\"" + (discountPercent==0.0? "0":discountPercent)+ "\"";
			    		jsonOutput += ",\"discountId\":\"" + (discountId==0?"0":discountId) + "\"";
			    		
						
						long rmc = 0;
						this.roomCnt = rmc;
						
						List<Long> myList = new ArrayList<Long>();
						for (DateTime d : between)
					     {
							java.util.Date availDate = d.toDate();
							PropertyAccommodationInventoryManager accommodationInventoryController = new PropertyAccommodationInventoryManager();  
							PropertyAccommodationInventory inventoryCount = accommodationInventoryController.findInventoryCount(availDate,accommodations.getAccommodationId());
		 					if(inventoryCount != null){
		 						//jsonOutput += ",\"inventoryId\":\"" + inventoryCount.getInventoryId() + "\"";
		 						PmsAvailableRooms roomCount1 = bookingController.findBookingCount(availDate, accommodations.getAccommodationId(),inventoryCount.getInventoryId());
		 						if(roomCount1.getRoomCount() == null) {
		 							jsonOutput += ",\"soldOutCount\":\"" + inventoryCount.getInventoryCount() + "\"";
		 							 String num1 = inventoryCount.getInventoryCount();
		 							 long num2 = Long.parseLong(num1);
		 							this.roomCnt = num2; 
		 						}
		 						else {
		 							jsonOutput += ",\"soldOutCount\":\"" + roomCount1.getRoomCount() + "\"";
		 							 long num = roomCount1.getRoomCount();
		 							 String num1 = inventoryCount.getInventoryCount();
		 							 long num2 = Long.parseLong(num1);
		 							this.roomCnt = (num2-num);	
		 						}
		 							
		 					}
		 					else{
		 						//jsonOutput += ",\"inventoryId\":\"" + 0 + "\""; 
								  PmsAvailableRooms pmsRoomCount = bookingController.findCount(availDate, accommodations.getAccommodationId());							
									if(pmsRoomCount.getRoomCount() == null){	
										jsonOutput += ",\"soldOutCount\":\"" + rmc + "\"";
										this.roomCnt = (accommodations.getNoOfUnits()-rmc);		
						               
									}
									else{
										jsonOutput += ",\"soldOutCount\":\"" + pmsRoomCount.getRoomCount() + "\"";
										this.roomCnt = (accommodations.getNoOfUnits()-pmsRoomCount.getRoomCount());						
									}					
									
								
					        //long availableCount = Collections.min(myList);
					        

					        //return availableCount;
					     }
		 					myList.add(this.roomCnt);
						
					     }
						/*PmsAvailableRooms roomCount = bookingController.findCount(arrival, departure,available.getAccommodationId());
						*/
						
						
						//jsonOutput += ",\"available\":\"" + available.getAvailable() + "\"";
						jsonOutput += ",\"available\":\"" + Collections.min(myList) + "\"";
						jsonOutput += "}";
						
						
				}
			}
		
			

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}


public Long getAvailableCount(int propertyId,Timestamp arrivalDate,Timestamp departureDate,long available,int accommodationId) throws IOException {
	Calendar cal = Calendar.getInstance();
	cal.setTime(departureDate);
    cal.add(Calendar.DATE, -1);
	String startDate = new SimpleDateFormat("yyyy-MM-dd").format(arrivalDate);
	String endDate = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
	DateTime start = DateTime.parse(startDate);
    DateTime end = DateTime.parse(endDate);
    

    PropertyAccommodationManager accommodationController=new PropertyAccommodationManager();
		PmsBookingManager bookingController = new PmsBookingManager();
		
		
		List<DateTime> between = DateUtil.getDateRange(start, end);
		List<Long> myList = new ArrayList<Long>();
		
		long rmc = 0;
		this.roomCnt = rmc;
		
		
		
		for (DateTime d : between)
	     {
			java.util.Date availDate = d.toDate();
			PropertyAccommodationInventoryManager accommodationInventoryController = new PropertyAccommodationInventoryManager();  
			PropertyAccommodationInventory inventoryCount = accommodationInventoryController.findInventoryCount(availDate,accommodationId);
				if(inventoryCount != null){
					PmsAvailableRooms roomCount1 = bookingController.findBookingCount(availDate, accommodationId,inventoryCount.getInventoryId());
					if(roomCount1.getRoomCount() == null) {
						String num1 = inventoryCount.getInventoryCount();
						long num2 = Long.parseLong(num1);
						PmsAvailableRooms bookingRoomCount=bookingController.findCount(availDate, accommodationId);
						
						PropertyAccommodation accommodations=accommodationController.find(accommodationId);
						Integer totavailable=0;
						 if(accommodations!=null){
							totavailable=accommodations.getNoOfUnits();
						 }
						int sold=0;
						if(bookingRoomCount.getRoomCount()!=null){
							long lngRooms=bookingRoomCount.getRoomCount();
							sold=(int) (lngRooms);
							int availRooms=0;
							availRooms=Integer.parseInt(num1)-sold;
							Integer totRooms=0;
							totRooms=totavailable-sold;
//							if(Integer.parseInt(num1)>totRooms){
//								this.roomCnt = totRooms;	
//							}else{
								this.roomCnt = num2;
//							}
						}else{
							this.roomCnt = num2; 
						} 
						 /*String num1 = inventoryCount.getInventoryCount();
						 long num2 = Long.parseLong(num1);
						this.roomCnt = num2;*/ 
					}
					else {
						PmsAvailableRooms bookingRoomCount=bookingController.findCount(availDate, accommodationId);
						
						int intRooms=0;
						PropertyAccommodation accommodations=accommodationController.find(accommodationId);
						Integer totavailable=0;
						 if(accommodations!=null){
							totavailable=accommodations.getNoOfUnits();
						 }
						if(bookingRoomCount.getRoomCount()!=null){
							long num = roomCount1.getRoomCount();
							long lngRooms=bookingRoomCount.getRoomCount();
							intRooms=(int)lngRooms;
							String num1 = inventoryCount.getInventoryCount();
 							 long num2 = Long.parseLong(num1);
 							 Integer totRooms=totavailable-intRooms;
// 							 if(num2>totRooms){
// 								this.roomCnt = totRooms;	 
// 							 }else{
 								this.roomCnt = num2-num;
// 							 }							
						}else{
							long num = roomCount1.getRoomCount();
							String num1 = inventoryCount.getInventoryCount();
							long num2 = Long.parseLong(num1);
							this.roomCnt = (num2-num);	
						}
						 /*long num = roomCount1.getRoomCount();
						 String num1 = inventoryCount.getInventoryCount();
						 long num2 = Long.parseLong(num1);
						this.roomCnt = (num2-num);*/	
					}
						
				}
				else{
				  PmsAvailableRooms roomCount = bookingController.findCount(availDate, accommodationId);
					if(roomCount.getRoomCount() == null){
						this.roomCnt = (available-rmc);
					}
					else{
						this.roomCnt = (available-roomCount.getRoomCount());
					}
				}
					myList.add(this.roomCnt);
	     }
        long availableCount = Collections.min(myList);

        return availableCount;
    
}
   
   
   public String getTypeSelected() throws IOException {
    	
	   try {
		   
		   String jsonOutput = "";
		   HttpServletResponse response = ServletActionContext.getResponse();
		
			
		   PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
	       response.setContentType("application/json");

	       sessionMap.put("types",types);
	       
    		for (int i = 0; i < types.size(); i++) 
  	      {
  			
  		   PropertyAccommodation accommodation = accommodationController.find(types.get(i).getAccommodationId());
  		   
  		   if (!jsonOutput.equalsIgnoreCase(""))
				jsonOutput += ",{";
			
			else
				jsonOutput += "{";
  		    
  		    jsonOutput += "\"accommodationType\":\"" + accommodation.getAccommodationType().toUpperCase() + "\"";
	        jsonOutput += ",\"baseAmount\":\"" + accommodation.getBaseAmount() + "\"";
			jsonOutput += ",\"accommodationId\":\"" + accommodation.getAccommodationId() + "\"";
			jsonOutput += ",\"minOccupancy\":\"" + accommodation.getMinOccupancy() + "\"";
			jsonOutput += ",\"maxOccupancy\":\"" + accommodation.getMaxOccupancy() + "\"";
			jsonOutput += ",\"noOfAdults\":\"" + accommodation.getNoOfAdults() + "\"";
			jsonOutput += ",\"noOfChild\":\"" + accommodation.getNoOfChild() + "\"";
			jsonOutput += ",\"extraChild\":\"" + accommodation.getExtraChild() + "\"";
			jsonOutput += ",\"extraAdult\":\"" + accommodation.getExtraAdult() + "\"";
			
			jsonOutput += "}";
  		   
  		   
  	      }
			
    		response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
    		
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return SUCCESS ;

    	
    }
   
   
	
	public String getBookings() throws IOException {
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PmsBookingManager bookingController = new PmsBookingManager();
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");

			PmsBooking booking = bookingController.find(getBookingId());
			
				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"DT_RowId\":\"" + booking.getBookingId() + "\"";
				jsonOutput += ",\"arrivalDate\":\"" + booking.getArrivalDate()+ "\"";
				jsonOutput += ",\"departureDate\":\"" + booking.getDepartureDate()+ "\"";
				jsonOutput += ",\"rooms\":\"" +booking.getRooms()+ "\"";
				jsonOutput += ",\"securityDeposit\":\"" + booking.getSecurityDeposit() + "\"";
				jsonOutput += ",\"totalAmount\":\"" + booking.getTotalAmount() + "\"";
				jsonOutput += ",\"totalRefund\":\"" + booking.getTotalRefund() + "\"";
				jsonOutput += ",\"totalTax\":\"" + booking.getTotalTax() + "\"";
				
				jsonOutput += "}";

			

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			sessionMap.clear();
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}

	public String getBookingSplit(){
		try{
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			PmsPropertyManager pmsPropertyController=new PmsPropertyManager();
			DecimalFormat df = new DecimalFormat("###.##");
			Double dblBaseAmount=0.0,dblTaxAmount=0.0,dblTotalAmount=0.0,dblAdvanceAmount=0.0,dblDueAmount=0.0;
			Double dblOtaCommission=0.0,dblOtaTax=0.0,dblTotalOtaAmout=0.0,dblRevenueAmount=0.0,dblTotalRevenueTaxAmount=0.0;
			Double dblCommission=0.0,dblTotalUloCommission=0.0,dblUloCommission=0.0,dblUloTaxAmount=0.0,dblHotelPayable=0.0,dblHotelTaxPayable=0.0;
			this.propertyId = (Integer) sessionMap.get("propertyId");
			if (!jsonOutput.equalsIgnoreCase(""))
				jsonOutput += ",{";
			else
				jsonOutput += "{";
			for (int i = 0; i < array.size(); i++) 
		    {
				dblBaseAmount+=array.get(i).getTotal();
				dblTaxAmount+=array.get(i).getTax();
				dblOtaCommission+=array.get(i).getOtaCommission();
				dblOtaTax+=array.get(i).getOtaTax();
				dblAdvanceAmount+=array.get(i).getAdvanceAmount();
		    }
			jsonOutput += "\"propertyId\":\" "+this.propertyId+"\"";
			jsonOutput += ",\"tariffAmount\":\" "+df.format(dblBaseAmount)+"\"";
			jsonOutput += ",\"taxAmount\":\" "+df.format(dblTaxAmount)+"\"";
			
			dblTotalAmount=dblBaseAmount+dblTaxAmount;
			String strTotal=df.format(dblTotalAmount);
			dblDueAmount=Double.valueOf(strTotal)-dblAdvanceAmount;
			
			jsonOutput += ",\"advanceAmount\":\" "+df.format(dblAdvanceAmount)+"\"";
			jsonOutput += ",\"dueAmount\":\" "+df.format(dblDueAmount)+"\"";
			
			dblTotalOtaAmout=dblOtaCommission+dblOtaTax;
			
			jsonOutput += ",\"otaCommission\":\" "+df.format(dblOtaCommission)+"\"";
			jsonOutput += ",\"otaTax\":\" "+df.format(dblOtaTax)+"\"";
			jsonOutput += ",\"otaRevenueAmount\":\" "+df.format(dblTotalOtaAmout)+"\"";
			
			dblRevenueAmount=dblBaseAmount-dblTotalOtaAmout;
			dblTotalRevenueTaxAmount=dblRevenueAmount+dblTaxAmount;
			jsonOutput += ",\"revenueAmount\":\" "+df.format(dblRevenueAmount)+"\"";
			jsonOutput += ",\"revenueTaxAmount\":\" "+df.format(dblTotalRevenueTaxAmount)+"\"";
			
			PmsProperty pmsPropertyDetails=pmsPropertyController.find((Integer) sessionMap.get("propertyId"));
			dblCommission=pmsPropertyDetails.getPropertyCommission();
			if(dblCommission!=null){
				dblUloCommission=dblRevenueAmount*dblCommission/100;
			}
			jsonOutput += ",\"uloCommission\":\" "+df.format(dblUloCommission)+"\"";
			
			dblUloTaxAmount=dblUloCommission*18/100;
			jsonOutput += ",\"uloTaxAmount\":\"" + df.format(dblUloTaxAmount)+ "\"";
			
			dblTotalUloCommission=dblUloCommission+dblUloTaxAmount;
			jsonOutput += ",\"uloPayable\":\"" + df.format(dblTotalUloCommission)+ "\"";
			
			dblHotelPayable=dblRevenueAmount-dblTotalUloCommission;
			dblHotelTaxPayable=dblHotelPayable+dblTaxAmount;
			jsonOutput += ",\"hotelPayable\":\"" + df.format(dblHotelTaxPayable)+ "\"";
			
			jsonOutput += "}";
			
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
		}catch(Exception e){
			logger.error(e);
			e.printStackTrace();
		}
		return null;
	}
	
public String editOtaBookingId(){ 
	try{
		PmsBookingManager bookingController = new PmsBookingManager();
		
		PmsBooking pmsBooking = bookingController.find(getBookingId());
		
		
		long time = System.currentTimeMillis();
		java.sql.Date date = new java.sql.Date(time);
		pmsBooking.setOtaBookingId(getOtaBookingId());
		 for (int i = 0; i < array.size(); i++) 
	      {
			if(array.get(i).getAddonAmount() != 0) {
				pmsBooking.setAddonAmount(array.get(i).getAddonAmount());
				pmsBooking.setAddonDescription(array.get(i).getAddonDescription());
			}
	      }
		
		bookingController.edit(pmsBooking);
	}catch(Exception e){
		logger.error(e);
		e.printStackTrace();
	}finally{
		
	}
	return null;
}
	public String getBookingAPI(Booking booking){
		String output="";
		 try {
			int count=0; 
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			DecimalFormat df = new DecimalFormat("###.##");
			DateFormat f = new SimpleDateFormat("EEEE");
		    SimpleDateFormat sdfformat=new SimpleDateFormat("MMM dd");
		    SimpleDateFormat sdfformat1=new SimpleDateFormat("MMM dd, yyyy");
			PmsPropertyManager propertyController = new PmsPropertyManager();
			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
			BookingDetailManager bookingDetailController = new BookingDetailManager();
			BookingGuestDetailManager bookingGuestDetailController = new BookingGuestDetailManager();
			PmsGuestManager guestController = new PmsGuestManager();
			PmsStatusManager statusController = new PmsStatusManager();
			PropertyDiscountManager propertyDiscountController = new PropertyDiscountManager();  
			PmsBookingManager bookingController = new PmsBookingManager();
			PmsTagManager tagController=new PmsTagManager();
			this.bookedList = bookingDetailController.getBookingList(booking.getBookingId());
			
           for (PmsBookedDetails booked : bookedList) {
        	   if (!jsonOutput.equalsIgnoreCase(""))
   				jsonOutput += ",{";
   			else
   				jsonOutput += "{";
        	   
        	   jsonOutput += "\"bookingId\":\" "+booked.getBookingId()+"\"";
        	   Integer propertyId=booked.getPropertyId();
        	   Integer accommodationId=booked.getAccommodationId();
        	   PmsProperty property=propertyController.find(propertyId);
        	   jsonOutput += ",\"propertyId\":\""+propertyId+"\"";
        	   jsonOutput += ",\"propertyName\":\""+property.getPropertyName()+"\"";
        	   
        	   PropertyAccommodation accommodation=accommodationController.find(accommodationId);
        	   jsonOutput += ",\"accommodationId\":\""+accommodationId+"\"";
        	   jsonOutput += ",\"accommodationName\":\""+accommodation.getAccommodationType()+"\"";
        	   
        	   SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
	  			Timestamp FromDate=booked.getArrivalDate();
	  			Timestamp ToDate=booked.getDepartureDate();
	  			Timestamp bookingDate=booked.getCreatedDate();
	  			String strFromDate=format2.format(FromDate);
	  			String strToDate=format2.format(ToDate);
	  	    	DateTime startdate = DateTime.parse(strFromDate);
	  	        DateTime enddate = DateTime.parse(strToDate);
	  	        
	  	        java.util.Date fromdate = startdate.toDate();
	  	 		java.util.Date todate = enddate.toDate();
	  	 		 
	  			int diffInDays = (int) ((todate.getTime() - fromdate.getTime()) / (1000 * 60 * 60 * 24));
	  			
            	Calendar calDate=Calendar.getInstance();
            	calDate.setTime(booked.getCreatedDate());
            	calDate.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
            	int bookedHours=calDate.get(Calendar.HOUR);
            	int bookedMinute=calDate.get(Calendar.MINUTE);
            	int bookedSecond=calDate.get(Calendar.SECOND);
            	int AMPM=calDate.get(Calendar.AM_PM);
            	String strBookingDate=new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH).format(calDate.getTime());
            	String hours="",minutes="",seconds="";
            	if(bookedHours<10){
            		hours=String.valueOf(bookedHours);
            		hours="0"+hours;
            	}else{
            		hours=String.valueOf(bookedHours);
            	}
            	if(bookedMinute<10){
            		minutes=String.valueOf(bookedMinute);
            		minutes="0"+minutes;
            	}else{
            		minutes=String.valueOf(bookedMinute);
            	}
            	
            	if(bookedMinute<10){
            		seconds=String.valueOf(bookedSecond);
            		seconds="0"+seconds;
            	}else{
            		seconds=String.valueOf(bookedSecond);
            	}
            	
            	if(AMPM==0){//If the current time is AM
            		this.timeHours=hours+":"+minutes+"AM";
            	}else if(AMPM==1){//If the current time is PM
            		this.timeHours=hours+":"+minutes+"PM";
            	}
	  			if(booked.getTagId()!=null){
	  				PmsTags tags=tagController.find(booked.getTagId());
	  				jsonOutput += ",\"targetAudience\":\""+tags.getTagName()+"\"";
	  			}else{
	  				jsonOutput += ",\"targetAudience\":\""+"NA"+"\"";
	  			}
        	   jsonOutput += ",\"checkIn\":\""+sdfformat.format(FromDate)+"\"";
        	   jsonOutput += ",\"checkInDays\":\""+f.format(fromdate)+"\"";
        	   jsonOutput += ",\"checkOut\":\""+sdfformat.format(ToDate)+"\"";
        	   jsonOutput += ",\"checkOutDays\":\""+f.format(todate)+"\"";
        	   jsonOutput += ",\"bookingDate\":\""+sdfformat1.format(booked.getCreatedDate())+"\"";
        	   jsonOutput += ",\"bookingDateDays\":\""+f.format(booked.getCreatedDate())+"\"";
        	   jsonOutput += ",\"bookingTiming\":\""+this.timeHours+"\"";
        	   jsonOutput += ",\"diffDays\":\""+diffInDays+"\"";
        	   PmsGuest pmsGuest=guestController.find(booked.getGuestId());
        	   jsonOutput += ",\"guestName\":\""+pmsGuest.getFirstName()+"\"";
        	   jsonOutput += ",\"emailId\":\""+pmsGuest.getEmailId()+"\"";
        	   jsonOutput += ",\"mobileNumber\":\""+pmsGuest.getPhone()+"\"";
        	   jsonOutput += ",\"guestGST\":\""+(pmsGuest.getGstNo()==null?"NA":pmsGuest.getGstNo().trim())+"\"";
        	   jsonOutput += ",\"noOfGuest\":\""+((int)booked.getAdults()+(int)booked.getChild())+"\"";
        	   jsonOutput += ",\"adult\":\""+booked.getAdults()+"\"";
        	   jsonOutput += ",\"child\":\""+booked.getChild()+"\"";
        	   jsonOutput += ",\"nights\":\""+diffInDays+"\"";
   			   jsonOutput += ",\"rooms\":\""+booked.getRooms()+"\"";
   			   jsonOutput += ",\"tariff\":\""+df.format(booked.getAmount())+"\"";
   			   jsonOutput += ",\"tax\":\""+df.format(booked.getTax())+"\"";
   			   double totalAmount=booked.getAmount()+booked.getTax();
   			   double discountAmount=booked.getPromotionAmount();
   			   double advanceAmount=booked.getAdvanceAmount();
   			   double couponAmount=booked.getCouponAmount();
   			   double actualAmount=booked.getActualAmount();
   			   if(advanceAmount==0){
   				   jsonOutput += ",\"paidBy\":\""+"Amount will paid during hotel check-in"+"\"";
   			   }else if(advanceAmount==totalAmount){
   				   jsonOutput += ",\"paidBy\":\""+"Amount paid by online"+"\"";
   			   }
   			   jsonOutput += ",\"totalPay\":\""+df.format(totalAmount-couponAmount)+"\"";
   			   jsonOutput += ",\"maximumAmount\":\""+df.format(actualAmount)+"\"";
   			   jsonOutput += ",\"discountAmount\":\""+df.format(booked.getPromotionAmount())+"\"";
   			   jsonOutput += ",\"couponAmount\":\""+df.format(booked.getCouponAmount())+"\"";
   			   jsonOutput += ",\"promotionFirstName\":\""+(booked.getPromotionFirstName()==null?"NA":booked.getPromotionFirstName().trim())+"\"";
			   jsonOutput += ",\"promotionSecondName\":\""+(booked.getPromotionSecondName()==null?"NA":booked.getPromotionSecondName().trim())+"\"";
			   jsonOutput += ",\"couponName\":\""+(booked.getCouponName()==null?"NA":booked.getCouponName().trim())+"\"";
   			   jsonOutput += ",\"specialRequest\":\""+(booked.getSpecialRequest()==null?"NA":booked.getSpecialRequest().trim())+"\"";
   			   if(booked.getExtraAdultRate()==null){
   				   this.extraadult=0;   
   			   }else{
   				   this.extraadult=booked.getExtraAdultRate();
   			   }
   			   if(booked.getExtraChildRate()==null){
   				   this.extrachild=0;
   			   }else{
   				   this.extrachild=booked.getExtraChildRate();
   			   }
   			   
   			   this.extracharges=this.extraadult+this.extrachild;
   			   
   			   jsonOutput += ",\"extraAdult\":\""+df.format(this.extraadult)+"\"";
   			   jsonOutput += ",\"extraChild\":\""+df.format(this.extrachild)+"\"";
   			   jsonOutput += "}";
   			   
   			   count++;
           }
           String data="\"data\":[" + jsonOutput + "]";
			String strData=data.replaceAll("\"", "\"");
			
			
			if(count == 0){
				output="{\"error\":\"1\",\"message\":\"No records found\"}";
			}else{
				output="{\"error\":\"0\"," + strData + "}";
			}
           
		} catch (Exception e) {
			output="{\"error\":\"2\",\"message\":\"Some technical error on booking confirmation api\"}";
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return output;
	
	}
	public String addBookingAPI(Booking booking){
		String output="",strStatus="";
		
		try {
			String jsonOutput="";
			int count=0;
			boolean blnBooked=false;
			Integer locationTypeId=0,contractTypeId=0;
			String locationType=null,contractType=null;
			Double purchaseRate=null;
			PmsBookingManager bookingController = new PmsBookingManager();
			PmsStatusManager statusController = new PmsStatusManager();
			PmsSourceManager sourceController = new PmsSourceManager();
			PmsPropertyManager propertyController = new PmsPropertyManager();
			PropertyTaxeManager taxController = new PropertyTaxeManager();
			PropertyAccommodationManager propertyAccommodationController = new PropertyAccommodationManager();
			BookingDetailManager bookingDetailController = new BookingDetailManager();
            BookingGuestDetailManager bookingGuestDetailController = new BookingGuestDetailManager();
            PropertyAccommodationInventoryManager inventoryController=new PropertyAccommodationInventoryManager();
            
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		    java.util.Date dateStart = format.parse(booking.getCheckIn());
		    java.util.Date dateEnd = format.parse(booking.getCheckOut());
		   
		    DateTime dtstart = DateTime.parse(booking.getCheckIn());
            DateTime dtend = DateTime.parse(booking.getCheckOut());
            java.util.Date dtarrival = dtstart.toDate();
            java.util.Date dtdeparture = dtend.toDate();

	 		
			int diffInDays = (int) ((dtdeparture.getTime() - dtarrival.getTime()) / (1000 * 60 * 60 * 24));
			
		    DecimalFormat df = new DecimalFormat("###.##");
		    DateFormat f = new SimpleDateFormat("EEEE");
		    
		    Calendar calCheckStart=Calendar.getInstance();
		    calCheckStart.setTime(dateStart);
		    java.util.Date checkInDate = calCheckStart.getTime();
		    this.arrivalDate=new Timestamp(checkInDate.getTime());
		    
		    Calendar calCheckEnd=Calendar.getInstance();
		    calCheckEnd.setTime(dateEnd);
		    java.util.Date checkOutDate = calCheckEnd.getTime();
		    this.departureDate=new Timestamp(checkOutDate.getTime());
			
		    PmsTagManager tagController=new PmsTagManager();
			PmsBooking pmsbooking = new PmsBooking();
			PmsGuestManager guestController = new PmsGuestManager();
			SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			java.util.Date date=new java.util.Date();
			Calendar today=Calendar.getInstance();
			today.setTime(date);
			today.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
		    String strCurrentDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(today.getTime());
		    java.util.Date currentDate = format1.parse(strCurrentDate);
		    Timestamp currentTS = new Timestamp(currentDate.getTime());
		    pmsbooking.setCreatedDate(currentTS);
		    
		    pmsbooking.setArrivalDate(this.arrivalDate);
		    pmsbooking.setDepartureDate(this.departureDate);
			
		    pmsbooking.setTotalAmount(booking.getTariff());
		    pmsbooking.setAdvanceAmount(booking.getTotalAmount());
		    pmsbooking.setTotalTax(booking.getTax());
		    
		    pmsbooking.setIsActive(true);
		    pmsbooking.setIsDeleted(false);
			
		    pmsbooking.setRooms(booking.getRooms());
		    pmsbooking.setTotalActualAmount(booking.getMaximumAmount());
			PmsSource source = sourceController.find(1);
			pmsbooking.setPmsSource(source);
			PmsProperty property = propertyController.find(booking.getPropertyId());
			this.propertyId=property.getPropertyId();
			if(property.getLocationType()!=null){
				locationType=property.getLocationType().getLocationTypeName();
				locationTypeId=property.getLocationType().getLocationTypeId();
			}
			pmsbooking.setPmsProperty(property);
			PmsStatus status = statusController.find(2);
			pmsbooking.setPmsStatus(status);
			pmsbooking.setPromotionAmount(booking.getPromotionAmount());
			pmsbooking.setCouponAmount(booking.getCouponAmount());
			pmsbooking.setPromotionFirstName(booking.getPromotionFirstName());
			pmsbooking.setPromotionSecondName(booking.getPromotionSecondName());
			pmsbooking.setCouponName(booking.getCouponName());
			PmsTags tags=tagController.find(booking.getTagId());
			pmsbooking.setPmsTags(tags);
			
			PmsBooking book = bookingController.add(pmsbooking);
			this.bookingId=book.getBookingId();
			PmsGuest guest = new PmsGuest();
			
			guest.setEmailId(booking.getEmailId());
			guest.setFirstName(booking.getGuestName());
			guest.setIsActive(true);
			guest.setIsDeleted(false);
			guest.setPhone(booking.getMobileNumber());
		    guest.setCreatedDate(currentTS);
		    guest.setGstNo(booking.getGstNo());
			PmsGuest guests = guestController.add(guest);
			
			BookingGuestDetail bookingGuestDetail = new BookingGuestDetail();
            PmsBooking bookings = bookingController.find(book.getBookingId());
            bookingGuestDetail.setPmsBooking(bookings);
            PmsGuest guest1 = guestController.find(guests.getGuestId());
            bookingGuestDetail.setPmsGuest(guest1);
            bookingGuestDetail.setIsPrimary(true);
            bookingGuestDetail.setIsActive(true);
            bookingGuestDetail.setIsDeleted(false);
            bookingGuestDetail.setSpecialRequest(booking.getSpecialRequest());
            bookingGuestDetailController.add(bookingGuestDetail);

            BookingDetail bookingDetail=new BookingDetail();
            
			this.arrivalDate =new Timestamp(checkInDate.getTime());
			this.departureDate =new Timestamp(checkOutDate.getTime());
             
             
			calCheckEnd.setTime(this.departureDate);
			calCheckEnd.add(Calendar.DATE, -1);
            String startDate = new SimpleDateFormat("yyyy-MM-dd").format(this.arrivalDate);
            String endDate = new SimpleDateFormat("yyyy-MM-dd").format(calCheckEnd.getTime());
             
            DateTime start = DateTime.parse(startDate);
            DateTime end = DateTime.parse(endDate);
            java.util.Date arrival = start.toDate();
            java.util.Date departure = end.toDate();

	 		int bookingcount=0;
            List<DateTime> between = getDateRange(start, end);
            for (DateTime d : between)
          	{
            	java.util.Date bookingDate=d.toDate();
            	if(bookingcount==0){
            		bookingDetail.setAdultCount(booking.getAdult());
                    bookingDetail.setChildCount(booking.getChild());
                    	
            	}
            	bookingcount++;
                bookingDetail.setAdvanceAmount(booking.getTotalAmount()/diffInDays);
                bookingDetail.setAmount(booking.getTariff()/diffInDays);
                bookingDetail.setTax(booking.getTax()/diffInDays);  
                bookingDetail.setCreatedDate(currentTS);
                bookingDetail.setBookingDate(bookingDate);
                bookingDetail.setIsActive(true);
                bookingDetail.setIsDeleted(false);
                bookingDetail.setActualAmount(booking.getMaximumAmount()/diffInDays);
                bookingDetail.setRoomCount(booking.getRooms());
                if(booking.getExtraAdult()==0){
                	bookingDetail.setExtraAdultRate(0.0);
                }else{
                	bookingDetail.setExtraAdultRate(booking.getExtraAdult()/diffInDays);	
                }
                if(booking.getExtraChild()==0){
                	bookingDetail.setExtraChildRate(0.0);	
                }else{
                	bookingDetail.setExtraChildRate(booking.getExtraChild()/diffInDays);
                }
                
                PmsStatus detailstatus = statusController.find(2);
                bookingDetail.setPmsStatus(detailstatus);
                PropertyAccommodation propertyAccommodation = propertyAccommodationController.find(booking.getAccommodationId());
                
                
                if(propertyAccommodation.getPropertyContractType()!=null){
                	contractType=propertyAccommodation.getPropertyContractType().getContractTypeName();
                	contractTypeId=propertyAccommodation.getPropertyContractType().getContractTypeId();
                }
                
                if(locationType.equalsIgnoreCase("Metro")){
                	if(contractTypeId==7){//B-join
                		purchaseRate= propertyAccommodation.getNetRate()==null?0:propertyAccommodation.getNetRate();		
                	}else if(contractTypeId==6){//B-sure
                		purchaseRate= propertyAccommodation.getPurchaseRate()==null?0:propertyAccommodation.getPurchaseRate();
                	}else if(contractTypeId==8){//B-stay
                		purchaseRate= propertyAccommodation.getPurchaseRate()==null?0:propertyAccommodation.getPurchaseRate();
                	}else if(contractTypeId==9){//B-join flat
                		purchaseRate= propertyAccommodation.getBaseAmount()==null?0:propertyAccommodation.getBaseAmount();
                	}
                	
                	
                }else if(locationType.equalsIgnoreCase("Leisure")){
                	String days=f.format(this.arrivalDate);
                    String[] weekdays={"Monday","Tuesday","Wednesday","Thursday","Sunday"};
        		    String[] weekend={"Friday","Saturday"};
        		    boolean isWeekdays=false,isWeekend=false;
    				for(int a=0;a<weekdays.length;a++){
    					if(days.equalsIgnoreCase(weekdays[a])){
    						isWeekdays=true;
    					}
    				}
    				
    				for(int b=0;b<weekend.length;b++){
    					if(days.equalsIgnoreCase(weekend[b])){
    						isWeekend=true;
    					}
    				}
    				
    				if(isWeekdays){
    					if(contractTypeId==7){//B-join
                    		purchaseRate= propertyAccommodation.getWeekdayNetRate()==null?0:propertyAccommodation.getWeekdayNetRate();		
                    	}else if(contractTypeId==6){//B-sure
                    		purchaseRate= propertyAccommodation.getWeekdayPurchaseRate()==null?0:propertyAccommodation.getWeekdayPurchaseRate();
                    	}else if(contractTypeId==8){//B-stay
                    		purchaseRate= propertyAccommodation.getWeekdayPurchaseRate()==null?0:propertyAccommodation.getWeekdayPurchaseRate();
                    	}else if(contractTypeId==9){//B-join flat
                    		purchaseRate= propertyAccommodation.getWeekdayBaseAmount()==null?0:propertyAccommodation.getWeekdayBaseAmount();
                    	}
    				}
    				
    				if(isWeekend){
    					if(contractTypeId==7){//B-join
                    		purchaseRate= propertyAccommodation.getWeekendNetRate()==null?0:propertyAccommodation.getWeekendNetRate();		
                    	}else if(contractTypeId==6){//B-sure
                    		purchaseRate= propertyAccommodation.getWeekendPurchaseRate()==null?0:propertyAccommodation.getWeekendPurchaseRate();
                    	}else if(contractTypeId==8){//B-stay
                    		purchaseRate= propertyAccommodation.getWeekendPurchaseRate()==null?0:propertyAccommodation.getWeekendPurchaseRate();
                    	}else if(contractTypeId==9){//B-join flat
                    		purchaseRate= propertyAccommodation.getWeekendBaseAmount()==null?0:propertyAccommodation.getWeekendBaseAmount();
                    	}
    				}
                }
                double purchaseNet=purchaseRate*booking.getRooms(); 
 				bookingDetail.setPurchaseRate(purchaseNet);
                bookingDetail.setPropertyAccommodation(propertyAccommodation);
                PmsBooking booking1 = bookingController.find(book.getBookingId());
                bookingDetail.setPmsBooking(booking1);
                
                PropertyAccommodationInventory inventoryCount = inventoryController.findInventoryCount(bookingDate,booking.getAccommodationId());
           	 	if(inventoryCount != null){
           	 		PropertyAccommodationInventory inventory = inventoryController.find(inventoryCount.getInventoryId());
           	 		bookingDetail.setPropertyAccommodationInventory(inventory);
	           	
	           	}
                bookingDetailController.add(bookingDetail);
                count++;
                blnBooked=true;
                
                
                OtaHotelsManager otaHotelsController = new OtaHotelsManager();
                
                
                
                
                PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
                	
		    	 long rmc = 0;
	   			 this.roomCnt = rmc;
	        	 int sold=0;
	        	 java.util.Date availDate = d.toDate();
	        	 PropertyAccommodation accommodation1 = accommodationController.find(booking.getAccommodationId());
				 PropertyAccommodationInventory inventoryDetails = inventoryController.findInventoryCount(availDate,booking.getAccommodationId());
				 if(inventoryDetails != null){
				 PmsAvailableRooms roomCount1 = bookingController.findBookingCount(availDate, booking.getAccommodationId(),inventoryDetails.getInventoryId());
				 if(roomCount1.getRoomCount() == null) {
				 	String num1 = inventoryDetails.getInventoryCount();
					PmsAvailableRooms bookingRoomCount=bookingController.findCount(availDate, booking.getAccommodationId());
					Integer totavailable=0;
					if(accommodation1!=null){
						totavailable=accommodation1.getNoOfUnits();
					}
					if(bookingRoomCount.getRoomCount()!=null){
						long lngRooms=bookingRoomCount.getRoomCount();
						long num2 = Long.parseLong(num1);
						this.roomCnt = num2;
						sold=(int) (lngRooms);
						int availRooms=0;
						availRooms=totavailable-sold;
//						if(Integer.parseInt(num1)>availRooms){
//							this.roomCnt = availRooms;	
//						}else{
							this.roomCnt = num2;
//						}
								
					}else{
						 long num2 = Long.parseLong(num1);
						 this.roomCnt = num2;
						 sold=(int) (rmc);
					}
				}else{		
					PmsAvailableRooms bookingRoomCount=bookingController.findCount(availDate, booking.getAccommodationId());
					int intRooms=0;
					
					Integer totavailable=0,availableRooms=0;
					if(accommodation1!=null){
						totavailable=accommodation1.getNoOfUnits();
					}
					if(bookingRoomCount.getRoomCount()!=null){
						long lngRooms=bookingRoomCount.getRoomCount();
						long num = roomCount1.getRoomCount();	
						intRooms=(int)lngRooms;
						String num1 = inventoryDetails.getInventoryCount();
	 					long num2 = Long.parseLong(num1);
	 					availableRooms=totavailable-intRooms;
//	 					if(num2>availableRooms){
//	 						this.roomCnt = availableRooms;	 
//	 					}else{
	 						this.roomCnt = num2-num;
//	 					}
	 					long lngSold=bookingRoomCount.getRoomCount();
	   					sold=(int)lngSold;
					}else{
						long num = roomCount1.getRoomCount();	
						String num1 = inventoryDetails.getInventoryCount();
						long num2 = Long.parseLong(num1);
	  					this.roomCnt = (num2-num);
	  					long lngSold=roomCount1.getRoomCount();
	   					sold=(int)lngSold;
					}

				}
								
			}else{
				PmsAvailableRooms inventoryCounts = bookingController.findCount(availDate, booking.getAccommodationId());							
				if(inventoryCounts.getRoomCount() == null){								
					this.roomCnt = (accommodation1.getNoOfUnits()-rmc);
					sold=(int)rmc;
				}
				else{
					this.roomCnt = (accommodation1.getNoOfUnits()-inventoryCounts.getRoomCount());	
					long lngSold=inventoryCounts.getRoomCount();
						sold=(int)lngSold;
				}
			}
				 
				double totalRooms=0,intSold=0;
					
				totalRooms=(double)accommodation1.getNoOfUnits();
				intSold=(double)sold;
				double occupancy=0.0;
				occupancy=intSold/totalRooms;
				double sellrate=0.0,otarate=0.0;
				Integer occupancyRating=(int) Math.round(occupancy * 100);
					
				boolean otaEnable=true;
				int limit = 1;
				this.propertyId=booking.getPropertyId();
				Integer[] arr={63,64,65};
				for(int i=0;i<arr.length;i++){
					if((int)this.propertyId==(int)arr[i]){
						otaEnable=false;
					}
				}
				

				OtaHotelDetails otaHotelDetails = otaHotelsController.getOtaHotelDetails(booking.getAccommodationId(),limit);
	  			if(otaHotelDetails!=null){

	  				int propertyId = otaHotelDetails.getPmsProperty().getPropertyId();
		  			int sourceId = 3;
		  			OtaHotels otaHotel = otaHotelsController.getOtaHotels(propertyId,sourceId);
		  			DefaultHttpClient httpClient = new DefaultHttpClient();
  					HttpPost postRequest = new HttpPost("https://in.goibibo.com/api/chmv2/updateroominventory/?bearer_token="+otaHotel.getOtaBearerToken()+"&channel_token="+otaHotel.getOtaChannelToken()+"");
  					
  					String xmlBody = "<?xml version='1.0' encoding='UTF-8' ?>"+
  						    "<Website Name='ingoibibo' HotelCode='"+otaHotelDetails.getOtaHotelCode()+"' Version='1'>"+
  						            "<Room>"+
  						            "<RoomTypeCode>"+ otaHotelDetails.getOtaRoomTypeId() +"</RoomTypeCode>"+
  						            "<StartDate Format='yyyy-mm-dd'>"+startDate+"</StartDate>"+
  						            "<EndDate Format='yyyy-mm-dd'>"+endDate+"</EndDate>"+						            
  						            "<DaysOfWeek Mon='"+"true"+"' Tue='"+"true"+"' Wed='"+"true"+"' Thu='"+"true"+"' Fri='"+"true"+"' Sat='"+"true"+"' Sun='"+"true"+"'></DaysOfWeek>"+
//  						            "<MinLOS>"+"1"+"</MinLOS>"+
//  						            "<CutOff>"+"1h"+"</CutOff>"+
  						            "<Available> "+ this.roomCnt +"</Available>"+
  						            "<StopSell>"+"False"+"</StopSell>"+
  						            "</Room>"+
  						     
  						            "</Website>";
  					
  				    
  					StringEntity input = new StringEntity(xmlBody);
  					input.setContentType("text/xml");
  					postRequest.setEntity(input);
  					HttpResponse httpResponse = (HttpResponse) httpClient.execute(postRequest);
  					HttpEntity entity = ((org.apache.http.HttpResponse) httpResponse).getEntity();
  	                // Read the contents of an entity and return it as a String.
  		            String content = EntityUtils.toString(entity);
	  			
	  			}					
			
	  		
          	}
            
            
            if (!jsonOutput.equalsIgnoreCase(""))
				jsonOutput += ",{";
			else
				jsonOutput += "{";
			
			
				jsonOutput += "\"bookingId\":\"" + this.bookingId+ "\"";
				jsonOutput += ",\"status\":\"" + blnBooked+ "\"";
				
			jsonOutput += "}";
			
			String data="\"data\":[" + jsonOutput + "]";
 			String strData=data.replaceAll("\"", "\"");
 			
 			strStatus="No records found";
 			if(count == 0){
 				output="{\"error\":\"1\",\"status\":\""+strStatus+"\"}";
			}else{
				output="{\"error\":\"0\"," + strData + "}";
			}
 			
 			BookingDetailAction bookingDetailAction=new BookingDetailAction();
            bookingDetailAction.getBookingVoucher("Online",this.bookingId);
            
		 } catch (Exception e) { 
			 output="{\"error\":\"2\",\"message\":\"Some technical error on add booking api\"}";
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
	
		return output;
	}
	

	public String addBooking() {

		PmsBookingManager bookingController = new PmsBookingManager();
		PmsStatusManager statusController = new PmsStatusManager();
		PmsSourceManager sourceController = new PmsSourceManager();
		PmsPropertyManager propertyController = new PmsPropertyManager();
		PropertyTaxeManager taxController = new PropertyTaxeManager();
		PropertyAccommodationManager propertyAccommodationController = new PropertyAccommodationManager();
		//UserLoginManager  userController = new UserLoginManager();
		String strReturn = "";
		double addonCost = 0;
		String addonName = null;
		double couponAmount = 0;
		double promotionAmount = 0;
		//this.propertyId = (Integer) sessionMap.get("propertyId");
		Boolean blnBookingCheck;
		long minimumCount = 0;
		this.userId=(Integer)sessionMap.get("userId");
		
		try {
			this.blockFlag=isBlockFlag();
			this.invoiceId=getInvoiceId();
			if(this.invoiceId!=null){
				if(this.invoiceId.equals("valid") || this.invoiceId.equals("undefined") || this.invoiceId=="undefined"){
					this.invoiceId=null;
				}
			}
			
			  for (int i = 0; i < array.size(); i++) 
		      {
			   String dateArrival = array.get(i).getArrival();
			   String dateDeparture = array.get(i).getDeparture();
			   long available = array.get(i).getAvailable();
			   this.propertyId=array.get(i).getPropertyId();
			   int accommmodationId = array.get(i).getAccommodationId();
			   
//			   DateFormat format = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
			   SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd");
			   java.util.Date dateStart = format.parse(dateArrival);
			   java.util.Date dateEnd = format.parse(dateDeparture);
			   
			    
			   Calendar calCheckStart=Calendar.getInstance();
			   calCheckStart.setTime(dateStart);
			   java.util.Date checkInDate = calCheckStart.getTime();
			   this.arrivalDate =new Timestamp(checkInDate.getTime());
			   sessionMap.put("arrivalDate", arrivalDate); 
			   Calendar calCheckEnd=Calendar.getInstance();
			   calCheckEnd.setTime(dateEnd);
			   java.util.Date checkOutDate = calCheckEnd.getTime();
			   this.departureDate =new Timestamp(checkOutDate.getTime());
			   sessionMap.put("departureDate", departureDate);
			   
			   
			   /*SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			   java.util.Date dateStart = format.parse(dateArrival);
			   java.util.Date dateEnd = format.parse(dateDeparture);
			   //java.util.Date dateEnd = format.parse(getStrEndDate());


			   Calendar calCheckStart=Calendar.getInstance();
			   Calendar calCheckEnd=Calendar.getInstance();
			   calCheckStart.setTime(dateStart);
			   calCheckEnd.setTime(dateEnd);
			   java.util.Date checkInDate = calCheckStart.getTime();
			   java.util.Date checkInEnd = calCheckEnd.getTime();
			   this.arrivalDate =new Timestamp(checkInDate.getTime());
			   this.departureDate =new Timestamp(checkInEnd.getTime());*/
			 
			   PropertyAccommodation accommodation = propertyAccommodationController.find(array.get(i).getAccommodationId());
			   //PropertyTaxe propertyTaxe = taxController.findAccommodationTax(array.get(i).getAccommodationId());
			   
			   totalRoomCount +=  array.get(i).getRooms();
			   promotionAmount += array.get(i).getPromotionAmount();
			   totalAdultCount +=  array.get(i).getAdultsCount(); 
			   totalChildCount +=  array.get(i).getChildCount();
			   totalBaseAmount +=   array.get(i).getTotal();
			   totalAdvanceAmount +=   array.get(i).getAdvanceAmount();
			   totalTaxAmount +=   array.get(i).getTax();
			   statusId  = array.get(i).getStatusId();
			   totalOtaCommissionAmount+=array.get(i).getOtaCommission();
			   totalOtaTaxAmount+=array.get(i).getOtaTax();
			   addonAmount += array.get(i).getAddonAmount();
			   if(array.get(i).getAddonDescription() != "NA") {
			   addonDescription = array.get(i).getAddonDescription();
			   }
			   totalActualBaseAmount += array.get(i).getMaximumAmount();
			   
			  // sourceId  =  array.get(i).getSourceId();.
			   sourceId  =  getSourceId() ;
			   if(array.get(i).getPaynowDiscount() == null){
				   paynowDiscount+= 0;
			   }
			   else{
				   paynowDiscount+=array.get(i).getPaynowDiscount();
			   }
			   /* minimumCount = getAvailableCount(getPropertyId(),arrivalDate,departureDate,available,accommmodationId);
			   */
			  
		      }	
			 /* long minimumCount1 = 0;
			  if(minimumCount != 0){
				    	blnBookingCheck=true;
				    	
				    }
			  else{
				  blnBookingCheck=false;  
			  }*/
				   
			  sessionMap.put("totalAdultCount",this.totalAdultCount);
			  sessionMap.put("totalChildCount",this.totalChildCount);			  
			 
			/*  
			  this.arrivalDate = getArrivalDate();
			  sessionMap.put("arrivalDate",arrivalDate); 
		     this.departureDate = getDepartureDate();
  		     sessionMap.put("departureDate",departureDate); */
	// if(blnBookingCheck == true){
			PmsBooking booking = new PmsBooking();
			SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			java.util.Date date=new java.util.Date();
			Calendar today=Calendar.getInstance();
			today.setTime(date);
			today.setTimeZone(TimeZone.getTimeZone("IST"));
		    String strCurrentDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(today.getTime());
		    java.util.Date currentDate = format1.parse(strCurrentDate);
		    Timestamp currentTS = new Timestamp(currentDate.getTime());
		    booking.setCreatedDate(currentTS);
		    if(this.userId!=null){
				booking.setCreatedBy(this.userId);   
			}
			booking.setArrivalDate(this.arrivalDate);
			booking.setDepartureDate(this.departureDate);
			booking.setSecurityDeposit(getSecurityDeposit());
			
			booking.setTotalAmount(totalBaseAmount);
			booking.setAdvanceAmount(totalAdvanceAmount);
			booking.setTotalTax(totalTaxAmount);
			booking.setCouponAmount(couponAmount);
			booking.setPromotionAmount(promotionAmount);
			booking.setOtaCommission(totalOtaCommissionAmount);
			booking.setOtaTax(totalOtaTaxAmount);
			booking.setPaynowDiscount(paynowDiscount);
			if(addonAmount!=0){
				booking.setAddonAmount(addonAmount);
				booking.setAddonDescription(addonDescription);
				}
//			booking.setIsActive(true);
//			booking.setIsDeleted(false);
			
			if(blockFlag){	
				booking.setIsActive(false);	
				booking.setIsDeleted(true);		
			}else if(!blockFlag){		
				booking.setIsActive(true);		
				booking.setIsDeleted(false);		
			}
			
			booking.setRooms(totalRoomCount);
			booking.setTotalActualAmount(totalActualBaseAmount);
			booking.setInvoiceId(this.invoiceId);
			PmsSource source = sourceController.find(sourceId);
			booking.setPmsSource(source);
			PmsProperty property = propertyController.find(this.propertyId);
			booking.setPmsProperty(property);
			PmsStatus status = statusController.find(statusId);
			booking.setPmsStatus(status);
			if(getOtaBookingId()!=null){
				booking.setOtaBookingId(getOtaBookingId());
			}
			PmsBooking book = bookingController.add(booking);
			
			
			//sessionMap.remove("bookingId");
			this.bookingId = book.getBookingId();
			
			if(this.invoiceId==null){
				PmsBooking bookings = bookingController.find(this.bookingId);
				bookings.setInvoiceId(String.valueOf(this.bookingId));
				bookingController.edit(bookings);
			}
			
			if(getOtaBookingId()==null){
				PmsBooking bookings = bookingController.find(this.bookingId);
				bookings.setOtaBookingId(String.valueOf(this.bookingId));
				bookingController.edit(bookings);
			}
			
			sessionMap.put("bookingId",bookingId);
			
			this.bookingId = (Integer) sessionMap.get("bookingId");
			
			strReturn = null;
	/* }else{
			strReturn="error";
	 }*/		
		} catch (Exception e) { 
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		
		
		return null;
	}
	
	public String editBooking() {
		PmsBookingManager bookingController = new PmsBookingManager();
		UserLoginManager  userController = new UserLoginManager();
		try {
			
						
			PmsBooking guest = bookingController.find(getBookingId());
			
			
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			booking.setArrivalDate(getArrivalDate());
			booking.setDepartureDate(getDepartureDate());
			booking.setIsActive(true);
			booking.setIsDeleted(false);
			booking.setRooms(rooms);
			
			bookingController.edit(booking);
		
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return SUCCESS;
	}
	
	public String editBookingStatus() {
		PmsBookingManager bookingController = new PmsBookingManager();
		BookingDetailManager detailController = new BookingDetailManager();
		PmsStatusManager statusController = new PmsStatusManager();
		UserLoginManager  userController = new UserLoginManager();
		try {
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			if(getStatusId() == 3){
				PmsBooking booking = bookingController.find(getBookingId());
				
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
				String formattedDate = sdf.format(date);
				java.util.Date dailyDate = sdf.parse(formattedDate);
				Timestamp tsDate=new Timestamp(dailyDate.getTime());
				
				booking.setArrivedDate(tsDate);
				PmsStatus status = statusController.find(getStatusId());
				booking.setPmsStatus(status);
				
				bookingController.edit(booking);
			}else if(getStatusId() == 5) {
				PmsBooking booking = bookingController.find(getBookingId());
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
				String formattedDate = sdf.format(date);
				java.util.Date dailyDate = sdf.parse(formattedDate);
				Timestamp tsDate=new Timestamp(dailyDate.getTime());
				booking.setDeparturedDate(tsDate);
				PmsStatus status = statusController.find(getStatusId());
				booking.setPmsStatus(status);
				bookingController.edit(booking);
			}else if(getStatusId() == 1) {
				PmsBooking booking = bookingController.find(getBookingId());
				PmsStatus status = statusController.find(getStatusId());
				booking.setPmsStatus(status);
				bookingController.edit(booking);
			}else if(getStatusId() == 2) {
				PmsBooking booking = bookingController.find(getBookingId());
				PmsStatus status = statusController.find(getStatusId());
				booking.setPmsStatus(status);
				bookingController.edit(booking);
			}
			/*else if(getStatusId() == 4) {
				PmsBooking booking = bookingController.find(getBookingId());
				PmsStatus status = statusController.find(getStatusId());
				booking.setPmsStatus(status);
				bookingController.edit(booking);
			}*/
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}
	
	
	public String deleteBooking() {
		PmsBookingManager bookingController = new PmsBookingManager();
		UserLoginManager  userController = new UserLoginManager();
		try {
			PmsBooking booking = bookingController.find(getBookingId());
						
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			booking.setIsActive(false);
			booking.setIsDeleted(true);
			booking.setModifiedDate(new java.sql.Timestamp(date.getTime()));
			bookingController.edit(booking);
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}
	
	public String cancelBooking() {
		
		PmsBookingManager bookingController = new PmsBookingManager();
		PmsStatusManager statusController = new PmsStatusManager();
		UserLoginManager  userController = new UserLoginManager();
		 
		try {
			PmsBooking booking = bookingController.find(getBookingId());
						
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			booking.setIsActive(false);
			booking.setIsDeleted(true);
			booking.setModifiedDate(new java.sql.Timestamp(date.getTime()));
			PmsStatus status = statusController.find(4);
			booking.setPmsStatus(status);
			bookingController.edit(booking);
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}
	
	public String getPartnerCheckInCheckOut() throws IOException {
		
		try {
			this.partnerPropertyId=(Integer)sessionMap.get("partnerPropertyId");
			this.selectPartnerPropertyId=(Integer)sessionMap.get("selectedPropertyId");
			if(this.selectPartnerPropertyId!=null){
				this.partnerPropertyId=this.selectPartnerPropertyId;	
			}else{
				this.partnerPropertyId=this.partnerPropertyId;
			}
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date date = new Date();// Now use today date.
			String todayDate = sdf.format(date.getTime());
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			PmsBookingManager bookingController = new PmsBookingManager();		
			response.setContentType("application/json");
			 DashBoard Arrival = bookingController.findPartnerCheckIn(this.partnerPropertyId,todayDate);
			 DashBoard Departure = bookingController.findPartnerCheckOut(this.partnerPropertyId,todayDate);
			 DashBoard occupancy = bookingController.findOccupancyCount(this.partnerPropertyId,todayDate);
			 DashBoard revenue = bookingController.findTodayRevenue(this.partnerPropertyId,todayDate);
			//for (PmsBooking arrival : bookingList) 
			 double otaCommission = revenue.getOtaCommission() + revenue.getOtaTax();
				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += "{";
				else
					jsonOutput += "{";
				//jsonOutput += "\"DT_RowId\":\"" + arrival.getBookingId() + "\"";
				//jsonOutput += ",\"arrivalDate\":\"" + arrival.getArrivalDate()+ "\"";
				
				jsonOutput += "\"arrivals\":\"" + (Arrival == null ? 0 : (Arrival.getTodayArrival() )) + "\"";
				jsonOutput += ",\"departures\":\"" + (Departure == null ? 0 : (Departure.getTodayDeparture() )) + "\"";
				jsonOutput += ",\"occupancy\":\"" + (occupancy == null ? 0 : (occupancy.getTodayOccupancy() )) + "\"";
				jsonOutput += ",\"revenue\":\"" + (revenue == null ? 0 : (revenue.getTodayRevenue() - otaCommission )) + "\"";
				jsonOutput += "}";

			
			
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	public String getArrival() throws IOException {
		this.propertyId = (Integer) sessionMap.get("propertyId");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();// Now use today date.
		String todayDate = sdf.format(date.getTime());
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			PmsBookingManager bookingController = new PmsBookingManager();		
			response.setContentType("application/json");
			 DashBoard Arrival = bookingController.findArrivalCount(getPropertyId(),todayDate);
			//for (PmsBooking arrival : bookingList) 

				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += "{";
				else
					jsonOutput += "{";
				//jsonOutput += "\"DT_RowId\":\"" + arrival.getBookingId() + "\"";
				//jsonOutput += ",\"arrivalDate\":\"" + arrival.getArrivalDate()+ "\"";
				
				jsonOutput += "\"arrivals\":\"" + (Arrival == null ? 0 : (Arrival.getTodayArrival() )) + "\"";
				jsonOutput += "}";

			
			
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	@SuppressWarnings("unchecked")
	public String getDeparture() throws IOException {
		this.propertyId = (Integer) sessionMap.get("propertyId");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();// Now use today date.
		String todayDate = sdf.format(date.getTime());
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			PmsBookingManager bookingController = new PmsBookingManager();		
			response.setContentType("application/json");
			 DashBoard departure =  bookingController.findDepartureCount(getPropertyId(),todayDate);
			//for (DashBoard departure : dashBoardList) 

				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += "{";
				else
					jsonOutput += "{";
				//jsonOutput += "\"DT_RowId\":\"" + arrival.getBookingId() + "\"";
				//jsonOutput += ",\"arrivalDate\":\"" + arrival.getArrivalDate()+ "\"";
				
				
				jsonOutput += "\"departures\":\"" + (departure == null ? 0 : (departure.getTodayDeparture() )) + "\"";
				jsonOutput += "}";

			
			
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	public String getTodayOccupancy() throws IOException {
		this.propertyId = (Integer) sessionMap.get("propertyId");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();// Now use today date.
		String todayDate = sdf.format(date.getTime());
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			PmsBookingManager bookingController = new PmsBookingManager();		
			response.setContentType("application/json");
			 DashBoard occupancy =  bookingController.findOccupancyCount(getPropertyId(),todayDate);
		
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += "{";
				else
					jsonOutput += "{";
				
				jsonOutput += "\"occupancy\":\"" + (occupancy.getTodayOccupancy() ) + "\"";

			
				jsonOutput += "}";

			
			
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	public String getPartnerTodayBookings(){

		try{ 
			//shiva
			this.partnerPropertyId=(Integer)sessionMap.get("partnerPropertyId");
 			this.selectPartnerPropertyId=(Integer)sessionMap.get("selectedPropertyId");
 			if(this.selectPartnerPropertyId!=null){
 				this.partnerPropertyId=this.selectPartnerPropertyId;	
 			}else{
 				this.partnerPropertyId=this.partnerPropertyId;
 			}
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date date = new Date();// Now use today date.
			String todayDate = sdf.format(date.getTime());
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("application/json");
			ReportManager reportController = new ReportManager();
			PmsStatusManager statusController = new PmsStatusManager();
			PmsSourceManager sourceController = new PmsSourceManager();
			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
			SimpleDateFormat format1 = new SimpleDateFormat("dd-MMM-yyyy");
			SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
			DecimalFormat df = new DecimalFormat("###.##");
			
			List<BookingDetailReport> listTodayBookings=reportController.listTodayBookingDetail(this.partnerPropertyId, todayDate);
			if(listTodayBookings.size()>0 && !listTodayBookings.isEmpty()){
				for(BookingDetailReport bookingDetail:listTodayBookings){
					if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += ",{";
					else
						
						jsonOutput += "{";
					
					
					jsonOutput += "\"DT_RowId\":\"" + bookingDetail.getBookingId()+ "\"";
					
					Timestamp FromDate=bookingDetail.getArrivalDate();
					Timestamp ToDate=bookingDetail.getDepartureDate();
					
					String strFromDate=format2.format(FromDate);
					String strToDate=format2.format(ToDate);
					
			    	DateTime startdate = DateTime.parse(strFromDate);
			        DateTime enddate = DateTime.parse(strToDate);
			        java.util.Date fromdate = startdate.toDate();
			 		java.util.Date todate = enddate.toDate();
			 		
					int diffInDays = (int) ((todate.getTime() - fromdate.getTime()) / (1000 * 60 * 60 * 24));
					
					long lngrooms=bookingDetail.getRoomCount();
					int roomCount=(int)lngrooms;
					PmsStatus pmsStatus = statusController.find(bookingDetail.getStatusId());
					
					jsonOutput += ",\"rooms\":\"" + roomCount/diffInDays + "\"";
					jsonOutput += ",\"statusId\":\"" +bookingDetail.getStatusId()+ "\"";
					jsonOutput += ",\"statusName\":\"" +pmsStatus.getStatus()+ "\"";
					
					String strFirstName="",firstNameLetterUpperCase="";
					String strLastName="",lastNameLetterUpperCase="";
					strFirstName=bookingDetail.getFirstName();
					strLastName=bookingDetail.getLastName();
					if(strFirstName!=null && strFirstName!="" && !strFirstName.equalsIgnoreCase("undefined")){
						strFirstName=strFirstName.trim();
						firstNameLetterUpperCase=strFirstName.substring(0, 1).toUpperCase()+strFirstName.substring(1);
						strFirstName=firstNameLetterUpperCase;
					}else{
						strFirstName="-";
					}
					if(strLastName!=null && strLastName!=""  && !strLastName.equalsIgnoreCase("undefined")){
						strLastName=strLastName.trim();
						lastNameLetterUpperCase=strLastName.substring(0, 1).toUpperCase() +  strLastName.substring(1);
						strLastName=lastNameLetterUpperCase;
					}else{
						strLastName="";
					}
					
					jsonOutput += ",\"guestName\":\"" + strFirstName+" "+strLastName + "\"";
					
					Integer sourceId=0;
					sourceId=bookingDetail.getSourceId();
					
					long arrivalDate =bookingDetail.getArrivalDate().getTime();
					java.util.Date dateArrival = new java.util.Date(arrivalDate);
					String strArrivalDate=format1.format(dateArrival); 
					
					jsonOutput += ",\"arrivalDate\":\"" + strArrivalDate+ "\"";
					
					long departureDate =bookingDetail.getDepartureDate().getTime();
					java.util.Date dateDeparture = new java.util.Date(departureDate);
					String strDepartureDate=format1.format(dateDeparture); 
					
					jsonOutput += ",\"departureDate\":\"" + strDepartureDate+ "\"";
					
					String strAccommodationType=null;
					int accommId=bookingDetail.getAccommodationId();
					jsonOutput += ",\"accommodationId\":\""+bookingDetail.getAccommodationId()+"\"";
					if(accommId>0)
					{
						PropertyAccommodation accommodations=accommodationController.find(accommId);
						strAccommodationType=accommodations.getAccommodationType();
						jsonOutput += ",\"accommodationType\":\" "+strAccommodationType.toUpperCase()+"\"";
					}
					
					Double dblBaseAmount=0.0,dblTaxAmount=0.0,dblTotalAmount=0.0,dblAdvanceAmount=0.0,dblDueAmount=0.0;
					dblBaseAmount=bookingDetail.getAmount();
					dblTaxAmount=bookingDetail.getTaxAmount();
					dblAdvanceAmount=bookingDetail.getAdvanceAmount();
					
					jsonOutput += ",\"tariffAmount\":\" "+df.format(dblBaseAmount)+"\"";
					jsonOutput += ",\"taxAmount\":\" "+df.format(dblTaxAmount)+"\"";
					
					dblTotalAmount=dblBaseAmount+dblTaxAmount;
					
					if(dblAdvanceAmount!=null  || dblAdvanceAmount>=0.0){
						dblAdvanceAmount=bookingDetail.getAdvanceAmount();
					}else{
						if(sourceId==1){
							dblAdvanceAmount=dblTotalAmount;
						}else{
							dblAdvanceAmount=0.0;
						}
					}
					
					boolean compareValues=dblTotalAmount.equals(dblAdvanceAmount);
					
					if(dblAdvanceAmount!=null || dblAdvanceAmount>=0.0){
						if(compareValues){
							jsonOutput += ",\"paymentRemarks\":\" CASH PAID\"";
						}else{
							jsonOutput += ",\"paymentRemarks\":\" PAY AT HOTEL\"";
						}
					}else{
						if(sourceId==1){
							jsonOutput += ",\"paymentRemarks\":\" CASH PAID \"";
						}else{
							if(compareValues){
								jsonOutput += ",\"paymentRemarks\":\" CASH PAID\"";
							}else{
								jsonOutput += ",\"paymentRemarks\":\" PAY AT HOTEL\"";
							}
						}
					}
					
					
					
					jsonOutput += ",\"totalAmount\":\" "+df.format(dblTotalAmount)+"\"";
					jsonOutput += ",\"advanceAmount\":\" "+df.format(dblAdvanceAmount)+"\"";
					
					dblDueAmount=dblTotalAmount-dblAdvanceAmount;
					jsonOutput += ",\"dueAmount\":\" "+df.format(dblDueAmount)+"\"";
					
					
					
					
					jsonOutput += "}";
				}
			}
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			
		}catch(Exception e){
			logger.error(e);
			e.printStackTrace();
		}finally{
			
		}
		
		return null;
	
	
	}
	
	public String getTodayBookings(){
		try{
			this.propertyId = (Integer) sessionMap.get("propertyId");
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date date = new Date();// Now use today date.
			String todayDate = sdf.format(date.getTime());
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("application/json");
			ReportManager reportController = new ReportManager();
			PmsStatusManager statusController = new PmsStatusManager();
			PmsSourceManager sourceController = new PmsSourceManager();
			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
			SimpleDateFormat format1 = new SimpleDateFormat("dd-MMM-yyyy");
			SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
			DecimalFormat df = new DecimalFormat("###.##");
			
			List<BookingDetailReport> listTodayBookings=reportController.listTodayBookingDetail(this.propertyId, todayDate);
			if(listTodayBookings.size()>0 && !listTodayBookings.isEmpty()){
				for(BookingDetailReport bookingDetail:listTodayBookings){
					if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += ",{";
					else
						
						jsonOutput += "{";
					
					
					jsonOutput += "\"DT_RowId\":\"" + bookingDetail.getBookingId()+ "\"";
					
					Timestamp FromDate=bookingDetail.getArrivalDate();
					Timestamp ToDate=bookingDetail.getDepartureDate();
					
					String strFromDate=format2.format(FromDate);
					String strToDate=format2.format(ToDate);
					
			    	DateTime startdate = DateTime.parse(strFromDate);
			        DateTime enddate = DateTime.parse(strToDate);
			        java.util.Date fromdate = startdate.toDate();
			 		java.util.Date todate = enddate.toDate();
			 		
					int diffInDays = (int) ((todate.getTime() - fromdate.getTime()) / (1000 * 60 * 60 * 24));
					
					long lngrooms=bookingDetail.getRoomCount();
					int roomCount=(int)lngrooms;
					PmsStatus pmsStatus = statusController.find(bookingDetail.getStatusId());
					
					jsonOutput += ",\"rooms\":\"" + roomCount/diffInDays + "\"";
					jsonOutput += ",\"statusId\":\"" +bookingDetail.getStatusId()+ "\"";
					jsonOutput += ",\"statusName\":\"" +pmsStatus.getStatus()+ "\"";
					
					String strFirstName="",firstNameLetterUpperCase="";
					String strLastName="",lastNameLetterUpperCase="";
					strFirstName=bookingDetail.getFirstName();
					strLastName=bookingDetail.getLastName();
					if(strFirstName!=null && strFirstName!="" && !strFirstName.equalsIgnoreCase("undefined")){
						strFirstName=strFirstName.trim();
						firstNameLetterUpperCase=strFirstName.substring(0, 1).toUpperCase()+strFirstName.substring(1);
						strFirstName=firstNameLetterUpperCase;
					}else{
						strFirstName="-";
					}
					if(strLastName!=null && strLastName!=""  && !strLastName.equalsIgnoreCase("undefined")){
						strLastName=strLastName.trim();
						lastNameLetterUpperCase=strLastName.substring(0, 1).toUpperCase() +  strLastName.substring(1);
						strLastName=lastNameLetterUpperCase;
					}else{
						strLastName="";
					}
					
					jsonOutput += ",\"guestName\":\"" + strFirstName+" "+strLastName + "\"";
					
					Integer sourceId=0;
					sourceId=bookingDetail.getSourceId();
					
					long arrivalDate =bookingDetail.getArrivalDate().getTime();
					java.util.Date dateArrival = new java.util.Date(arrivalDate);
					String strArrivalDate=format1.format(dateArrival); 
					
					jsonOutput += ",\"arrivalDate\":\"" + strArrivalDate+ "\"";
					
					long departureDate =bookingDetail.getDepartureDate().getTime();
					java.util.Date dateDeparture = new java.util.Date(departureDate);
					String strDepartureDate=format1.format(dateDeparture); 
					
					jsonOutput += ",\"departureDate\":\"" + strDepartureDate+ "\"";
					
					String strAccommodationType=null;
					int accommId=bookingDetail.getAccommodationId();
					jsonOutput += ",\"accommodationId\":\""+bookingDetail.getAccommodationId()+"\"";
					if(accommId>0)
					{
						PropertyAccommodation accommodations=accommodationController.find(accommId);
						strAccommodationType=accommodations.getAccommodationType();
						jsonOutput += ",\"accommodationType\":\" "+strAccommodationType.toUpperCase()+"\"";
					}
					
					Double dblBaseAmount=0.0,dblTaxAmount=0.0,dblTotalAmount=0.0,dblAdvanceAmount=0.0,dblDueAmount=0.0;
					dblBaseAmount=bookingDetail.getAmount();
					dblTaxAmount=bookingDetail.getTaxAmount();
					dblAdvanceAmount=bookingDetail.getAdvanceAmount();
					
					jsonOutput += ",\"tariffAmount\":\" "+df.format(dblBaseAmount)+"\"";
					jsonOutput += ",\"taxAmount\":\" "+df.format(dblTaxAmount)+"\"";
					
					dblTotalAmount=dblBaseAmount+dblTaxAmount;
					
					if(dblAdvanceAmount!=null  || dblAdvanceAmount>=0.0){
						dblAdvanceAmount=bookingDetail.getAdvanceAmount();
					}else{
						if(sourceId==1){
							dblAdvanceAmount=dblTotalAmount;
						}else{
							dblAdvanceAmount=0.0;
						}
					}
					
					boolean compareValues=dblTotalAmount.equals(dblAdvanceAmount);
					
					if(dblAdvanceAmount!=null || dblAdvanceAmount>=0.0){
						if(compareValues){
							jsonOutput += ",\"paymentRemarks\":\" CASH PAID\"";
						}else{
							jsonOutput += ",\"paymentRemarks\":\" PAY AT HOTEL\"";
						}
					}else{
						if(sourceId==1){
							jsonOutput += ",\"paymentRemarks\":\" CASH PAID \"";
						}else{
							if(compareValues){
								jsonOutput += ",\"paymentRemarks\":\" CASH PAID\"";
							}else{
								jsonOutput += ",\"paymentRemarks\":\" PAY AT HOTEL\"";
							}
						}
					}
					
					
					
					jsonOutput += ",\"totalAmount\":\" "+df.format(dblTotalAmount)+"\"";
					jsonOutput += ",\"advanceAmount\":\" "+df.format(dblAdvanceAmount)+"\"";
					
					dblDueAmount=dblTotalAmount-dblAdvanceAmount;
					jsonOutput += ",\"dueAmount\":\" "+df.format(dblDueAmount)+"\"";
					
					
					
					
					jsonOutput += "}";
				}
			}
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			
		}catch(Exception e){
			logger.error(e);
			e.printStackTrace();
		}finally{
			
		}
		
		return null;
	}
	
	public String getAllBookings() throws IOException {
		this.propertyId = (Integer) sessionMap.get("propertyId");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();// Now use today date.
		String todayDate = sdf.format(date.getTime());
		 
		/* SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd h:mm:ss a");
			String formattedDate = sdf.format(date);
			 java.util.Date dailyDate = sdf.parse(formattedDate);
			 Timestamp tsDate=new Timestamp(dailyDate.getTime());*/

		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PmsBookingManager bookingController = new PmsBookingManager();
			ReportManager reportController = new ReportManager();
			PmsSourceManager sourceController = new PmsSourceManager();
			PmsStatusManager statusController = new PmsStatusManager();
			BookingDetailManager detailController = new BookingDetailManager();
			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
			BookingGuestDetailManager bookingGuestController = new BookingGuestDetailManager();
			PmsGuestManager guestController = new PmsGuestManager();


		
			response.setContentType("application/json");

			/*this.availableList = bookingController.list(getPropertyId(),getArrivalDate(),getDepartureDate());
			
            for (PmsAvailableRooms available : availableList) {
            
				*/

			List<BookingDetail> bookingDetailList = detailController.listAllBooking(getPropertyId(),todayDate);
			for (BookingDetail bookingDetail : bookingDetailList)  {
				
				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					
					
					jsonOutput += "{";
				
				BookingDetail booking = detailController.find(bookingDetail.getBookingDetailsId());
				PmsBooking pmsBooking = bookingController.find(booking.getPmsBooking().getBookingId());
				
				List<BookingGuestDetail>  bookingGuestDetailList = reportController.BookingGuestDetails(pmsBooking.getBookingId());
				PmsBooking pmsBooking1 = bookingController.find(pmsBooking.getBookingId());
				BookingDetail booking1 = detailController.find(booking.getBookingDetailsId());
				PmsSource pmsSource = sourceController.find(pmsBooking1.getPmsSource().getSourceId());
				PmsStatus pmsStatus = statusController.find(pmsBooking1.getPmsStatus().getStatusId());
				PropertyAccommodation accommodation =  accommodationController.find(booking1.getPropertyAccommodation().getAccommodationId());
				SimpleDateFormat format=new SimpleDateFormat("dd-MMM-yyyy");
				
				String GuestName="";
				for(BookingGuestDetail bookingGuestDetail : bookingGuestDetailList)
				{
					BookingGuestDetail guestDetail = bookingGuestController.find(bookingGuestDetail.getBookingGuestId());
					PmsGuest pmsGuest = guestController.find(guestDetail.getPmsGuest().getGuestId());
					String strFirstName="",firstNameLetterUpperCase="";
					String strLastName="",lastNameLetterUpperCase="";
					strFirstName=pmsGuest.getFirstName();
					strLastName=pmsGuest.getLastName();
					if(strFirstName!=null && strFirstName!="" && !strFirstName.equalsIgnoreCase("undefined")){
						strFirstName=strFirstName.trim();
						firstNameLetterUpperCase=strFirstName.substring(0, 1).toUpperCase()+strFirstName.substring(1);
						strFirstName=firstNameLetterUpperCase;
					}else{
						strFirstName="-";
					}
					if(strLastName!=null && strLastName!="" && !strLastName.equalsIgnoreCase("undefined")){
						strLastName=strLastName.trim();
						lastNameLetterUpperCase=strLastName.substring(0, 1).toUpperCase() +  strLastName.substring(1);
						strLastName=lastNameLetterUpperCase;
					}else{
						strLastName="";
					}
					GuestName = strFirstName + " " + strLastName;
				}

				
				jsonOutput += "\"DT_RowId\":\"" + pmsBooking.getBookingId()+ "\"";
				jsonOutput += ",\"rooms\":\"" + bookingDetail.getRoomCount()+ "\"";
				jsonOutput += ",\"statusId\":\"" +booking1.getPmsStatus().getStatusId()+ "\"";
				String strBookingDate=format.format(bookingDetail.getBookingDate());
				jsonOutput += ",\"boookingDate\":\"" + strBookingDate+ "\"";
				jsonOutput += ",\"guestName\":\"" + GuestName + "\"";
				String strArrivalDate=format.format(pmsBooking.getArrivalDate());
				String strDepartureDate=format.format(pmsBooking.getDepartureDate());
				jsonOutput += ",\"arrivalDate\":\"" + strArrivalDate+ "\"";
				jsonOutput += ",\"departureDate\":\"" + strDepartureDate+ "\"";
				
				jsonOutput += ",\"guestName\":\"" + GuestName + "\"";
				jsonOutput += ",\"accommodationType\":\"" + accommodation.getAccommodationType().toUpperCase()+ "\"";
				//jsonOutput += ",\"amount\":\"" + bookingDetail.getAmount()+ "\"";
				//jsonOutput += ",\"source\":\"" + pmsSource.getSourceName()+ "\"";
				

				
				jsonOutput += "}";

			}
			
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
 public String getBookingchart() throws IOException, ParseException {
		
		this.propertyId = (Integer) sessionMap.get("propertyId");
		String jsonOutput = "";
		HttpServletResponse response = ServletActionContext.getResponse();
	
		
		BookingDetailManager bookingDetailController = new BookingDetailManager();
		response.setContentType("application/json");
	
		for (int i=0; i<chartDate.split(",").length; i++)
		{
			/*String strChartDate=chartDate.split(",")[i];
			DateFormat format = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
		    Date dteChart = format.parse(strChartDate);
		   
		    
		    Calendar calDateChart=Calendar.getInstance();
		    calDateChart.setTime(dteChart);
		    java.util.Date checkDate = calDateChart.getTime();
		    this.dateChart=new Timestamp(checkDate.getTime());
		    */
		   

			this.bookingDetailList = bookingDetailController.list(getPropertyId(),chartDate.split(",")[i]);
			
			
	     
			if(!jsonOutput.equalsIgnoreCase(""))
				jsonOutput += ",{";
			
			else
				jsonOutput = "{";
			
			    jsonOutput += "\"size\":\"" +bookingDetailList.size()+ "\"";
			    
		        jsonOutput += "}";
			
		        
		}
		
		response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
		
		return null;
		
	}
 
 	public String getBookingPieChart() throws IOException, ParseException {
		
 		try{
// 			this.propertyId = (Integer) sessionMap.get("propertyId");
 		 	this.partnerPropertyId=(Integer)sessionMap.get("partnerPropertyId");
 			this.selectPartnerPropertyId=(Integer)sessionMap.get("selectedPropertyId");
 			if(this.selectPartnerPropertyId!=null){
 				this.partnerPropertyId=this.selectPartnerPropertyId;	
 			}else{
 				this.partnerPropertyId=this.partnerPropertyId;
 			}
 			String jsonOutput = "";
 			HttpServletResponse response = ServletActionContext.getResponse();
 		
 			
 			BookingDetailManager bookingDetailController = new BookingDetailManager();
 			PmsBookingManager bookingController = new PmsBookingManager();
 			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
 			response.setContentType("application/json");
 			DateFormat format = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
 		    java.util.Date dateStart = format.parse(getStartDate());
 		    java.util.Date dateEnd = format.parse(getEndDate());
 			String startDate = new SimpleDateFormat("yyyy-MM-dd").format(dateStart);
 			String endDate = new SimpleDateFormat("yyyy-MM-dd").format(dateEnd);
 			  this.dashBoardList =  accommodationController.listPropertyAccommodation(this.partnerPropertyId);
 				
 				
 				for (DashBoard dashboard : dashBoardList) {
 	  
 					PmsAvailableRooms booking = bookingController.findChartCount(startDate,endDate,dashboard.getAccommodationId());
 					if (!jsonOutput.equalsIgnoreCase(""))
 						jsonOutput += ",{";
 					else
 						jsonOutput += "{";
 					jsonOutput += "\"accommodationId\":\"" + dashboard.getAccommodationId() + "\"";
 					jsonOutput += ",\"total\":\"" + (booking.getRoomCount() == null ? 0 : (booking.getRoomCount()))+ "\"";
 					jsonOutput += ",\"accommodationType\":\"" +dashboard. getAccommodationType().toUpperCase()+ "\"";
 					
 					jsonOutput += "}";

 				}
 			
 			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
 		}catch(Exception e){
 			logger.error(e);
 			e.printStackTrace();
 		}
		
		return null;
		
	}
	
public String getBookingCalendar() throws IOException, ParseException {
	
	this.propertyId = (Integer) sessionMap.get("propertyId");
	String jsonOutput = "";
	HttpServletResponse response = ServletActionContext.getResponse();

	PropertyAccommodationRoomManager  roomController  = new PropertyAccommodationRoomManager();
    AccommodationRoom accommodationRoom =  roomController.findCount(getPropertyId(),getAccommodationId());
	BookingDetailManager bookingDetailController = new BookingDetailManager();
	response.setContentType("application/json");

	for (int i=0; i<chartDate.split(",").length; i++)
	{
		this.bookingDetailList = bookingDetailController.list(getPropertyId(),chartDate.split(",")[i]);
		if (!jsonOutput.equalsIgnoreCase(""))
			jsonOutput += ",{";
		
		else
			jsonOutput = "{";
		
		   // jsonOutput += "\"size\":\"" +bookingDetailList.size()+ "\"";
		    //jsonOutput += ",\"roomcount\":\"" +accommodationRoom.getRoomCount()+ "\"";
		    jsonOutput += "\"title\":\"" +" Available: "+(accommodationRoom.getRoomCount()-bookingDetailList.size())+" / Sold: "+bookingDetailList.size()+ "\"";
		    //jsonOutput += "\"title\":\"" +" Available: "+(accommodationRoom.getRoomCount()-bookingDetailList.size())+"\\n"+"  Sold: "+bookingDetailList.size()+ "\"";
		    //jsonOutput += ",\"description\":\"" +(accommodationRoom.getRoomCount()-bookingDetailList.size())+ "\"";
		    jsonOutput += ",\"start\":\"o\"";
		    
	        jsonOutput += "}";
		
	}
	response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
	
	return null;
	
}
  
  public String updateInventory() {
		
		try {
			
		  
		// String checkIn = new SimpleDateFormat("MM/dd/yyyy").format(getStrDate());
		   sessionMap.put("reserveDate",getStrDate());
		  //DateTime checkIn = DateTime.parse(getStrDate());
	      //this.arrivalDate = getArrivalDate();
		 // sessionMap.put("arrivalDate",arrivalDate);				
			
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return SUCCESS;
	}
  
	 public String getPropertyAccommodation( ) throws IOException {
		this.propertyId = (Integer) sessionMap.get("propertyId");
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
		Date date = new Date();// Now use today date.
		String startDate = sdf.format(date.getTime());
		cal.add(Calendar.DATE, 30); // Adding 30 days
		String endDate = sdf.format(cal.getTime());
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			BookingDetailManager bookingDetailController = new BookingDetailManager();
			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
			
			response.setContentType("application/json");

	        this.dashBoardList =  accommodationController.listPropertyAccommodation(getPropertyId());
			
			
			for (DashBoard dashboard : dashBoardList) {
       
				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"accommodationId\":\"" + dashboard.getAccommodationId() + "\"";
				jsonOutput += ",\"total\":\"" + dashboard.getTotals()+ "\"";
				jsonOutput += ",\"accommodationType\":\"" +dashboard. getAccommodationType().toUpperCase()+ "\"";
				
				DashBoard roomCount  = bookingDetailController.findCountAccommodation(startDate,endDate,dashboard.getAccommodationId());
		  	 // jsonOutput += ",\"roomCounts\":\"" + roomCount+ "\"";
		  	   jsonOutput += ",\"roomCounts\":\"" + (roomCount == null ? 0 : (roomCount.getRoomCount() )) + "\"";

				jsonOutput += "}";

			}
		
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}    
	 
	 public String getPropertyRevenue() throws IOException {
			
			try {

				this.propertyId = (Integer) sessionMap.get("propertyId");
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				Calendar cal = Calendar.getInstance();
				Date date = new Date();// Now use today date.
				cal.add(Calendar.DATE, 30); // Adding 30 days
				
				DecimalFormat df = new DecimalFormat("###.##");
				
				DateFormat format = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
			    java.util.Date dateStart = format.parse(getStrStartDate());
			    java.util.Date dateEnd = format.parse(getStrEndDate());
			   
			    String startDate = sdf.format(dateStart.getTime());
			    String endDate = sdf.format(dateEnd.getTime());
			    
			    
				String jsonOutput = "";
				HttpServletResponse response = ServletActionContext.getResponse();
				
				PmsBookingManager bookingController = new PmsBookingManager();
				response.setContentType("application/json");
				this.dashBoardList = bookingController.listRevenue(this.propertyId,startDate,endDate);
				double totalRevenue=0,totalAmount=0,totalTax=0;
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				if(!dashBoardList.isEmpty() && dashBoardList.size()>0){
					for (DashBoard booking : dashBoardList) {
						totalAmount=booking.getTotalRevenue();
						totalTax=booking.getTotalTax();
						totalRevenue=totalAmount+totalTax;
						jsonOutput += "\"totalRooms\":\"" +booking.getTotalRooms()+ "\"";
						jsonOutput += ",\"totalAmount\":\"" + df.format(totalRevenue) + "\"";
					}
					
				}
				else{
					jsonOutput += "\"totalRooms\":\"" + 0 + "\"";					
					jsonOutput += ",\"totalAmount\":\"" + 0 + "\"";
				}
					
				jsonOutput += "}";
				
				response.getWriter().write("{\"data\":[" + jsonOutput + "]}");

			} catch (Exception e) {
				logger.error(e);
				e.printStackTrace();
			} finally {

			}

			return null;

		}
	 
	 public String getPartnerPropertyRevenue() throws IOException {
			
			try {
				this.partnerPropertyId=(Integer)sessionMap.get("partnerPropertyId");
	 			this.selectPartnerPropertyId=(Integer)sessionMap.get("selectedPropertyId");
	 			if(this.selectPartnerPropertyId!=null){
	 				this.partnerPropertyId=this.selectPartnerPropertyId;	
	 			}else{
	 				this.partnerPropertyId=this.partnerPropertyId;
	 			}
				Timestamp timestamp = new Timestamp(System.currentTimeMillis());
				
				DateFormat format = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
			    java.util.Date dateStart = format.parse(getStrStartDate());
			    java.util.Date dateEnd = format.parse(getStrEndDate());
				String startDate = new SimpleDateFormat("yyyy-MM-dd").format(dateStart);
				String endDate = new SimpleDateFormat("yyyy-MM-dd").format(dateEnd);
				DateTime start = DateTime.parse(startDate);
				DateTime end = DateTime.parse(endDate);
				List<DateTime> between = DateUtil.getDateRange(start, end);
				
				this.propertyId = (Integer) sessionMap.get("propertyId");
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				
				
				DecimalFormat df = new DecimalFormat("###.##");
				
				
				double totalRevenue=0,totalAmount=0,totalTax=0,totalOtaCommission=0,totalOtaTax=0;
				long totalRooms=0;
			    
				String jsonOutput = "";
				HttpServletResponse response = ServletActionContext.getResponse();
				
				PmsBookingManager bookingController = new PmsBookingManager();
				response.setContentType("application/json");
			/*	for (DateTime d : between)
				{*/
				

				this.dashBoardList = bookingController.listPartnerRevenue(this.partnerPropertyId,startDate,endDate);
				//shiva
				
				if(!dashBoardList.isEmpty() && dashBoardList.size()>0){
					for (DashBoard booking : dashBoardList) {
						totalAmount=booking.getTotalRevenue();
						totalTax=booking.getTotalTax();
						totalRooms = booking.getTotalRooms();
						totalOtaCommission=booking.getOtaCommission();
						totalOtaTax=booking.getOtaTax();
						double otaAmount = totalOtaCommission + totalOtaTax;
						totalRevenue=totalAmount+totalTax;
						totalRevenue = totalRevenue - otaAmount;
						
					}
					
				}
					
		//	}	
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"totalRooms\":\"" + totalRooms+ "\"";
				jsonOutput += ",\"totalAmount\":\"" + totalRevenue + "\"";
			
				
			jsonOutput += "}";
				response.getWriter().write("{\"data\":[" + jsonOutput + "]}");

			} catch (Exception e) {
				logger.error(e);
				e.printStackTrace();
			} finally {

			}

			return null;

		}
	 
	 
	 public String getPropertyBookingList() throws IOException {
		    this.propertyId = (Integer) sessionMap.get("propertyId");
			try {

				String jsonOutput = "";
				HttpServletResponse response = ServletActionContext.getResponse();
			
				
				PmsBookingManager bookingController = new PmsBookingManager();
				//this.familyRegisterList = familyController.list(getUser().getUserid());
				//model = familyRegisterList;
				response.setContentType("application/json");

				this.bookingList = bookingController.listBookingreport(getPropertyId(),getArrivalDate(),getDepartureDate());
				for (PmsBooking booking : bookingList) {

					
					if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += ",{";
					else
						jsonOutput += "{";
					jsonOutput += "\"DT_RowId\":\"" + booking.getBookingId() + "\"";
					jsonOutput += ",\"arrivalDate\":\"" + booking.getArrivalDate()+ "\"";
					jsonOutput += ",\"departureDate\":\"" + booking.getDepartureDate()+ "\"";
					jsonOutput += ",\"rooms\":\"" +booking.getRooms()+ "\"";
					//jsonOutput += ",\"securityDeposit\":\"" + booking.getSecurityDeposit() + "\"";
					jsonOutput += ",\"totalAmount\":\"" + booking.getTotalAmount() + "\"";
					//jsonOutput += ",\"totalRefund\":\"" + booking.getTotalRefund() + "\"";
					//jsonOutput += ",\"totalTax\":\"" + booking.getTotalTax() + "\"";
					
					jsonOutput += "}";

				}

				response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
				

			} catch (Exception e) {
				logger.error(e);
				e.printStackTrace();
			} finally {

			}
			return null;

		}   
	 public String getProperties(){
			try{
					Map mapDate=new HashMap();
					
					DateFormat format = new SimpleDateFormat("MMM d,yyyy", Locale.ENGLISH);
				    java.util.Date dateStart = format.parse(getStartDate());
				    java.util.Date dateEnd = format.parse(getEndDate());
				   
				    
				    Calendar calCheckStart=Calendar.getInstance();
				    calCheckStart.setTime(dateStart);
				    java.util.Date checkInDate = calCheckStart.getTime();
				    this.arrivalDate=new Timestamp(checkInDate.getTime());
				   
				    
				    Calendar calCheckEnd=Calendar.getInstance();
				    calCheckEnd.setTime(dateEnd);
				    java.util.Date checkOutDate = calCheckEnd.getTime();
				    this.departureDate=new Timestamp(checkOutDate.getTime());
				    
				     
					/*this.arrivalDate = getArrivalDate();
				    sessionMap.put("arrivalDate",arrivalDate); 
					
					this.departureDate = getDepartureDate();
					sessionMap.put("departureDate",departureDate); */
					mapDate.put("arrivalDate", arrivalDate);
					mapDate.put("departureDate", departureDate);
					sessionMap.put("uloPropertyId", getPropertyId());
					
					PmsBookingManager bookingController = new PmsBookingManager();
					PmsPropertyManager propertyController = new PmsPropertyManager();		
					PropertyAccommodationManager accommodationManager = new PropertyAccommodationManager();
					PropertyRateManager rateController = new PropertyRateManager();
					PropertyRateDetailManager detailController = new PropertyRateDetailManager();
					PromotionManager promotionController=new PromotionManager();
					PromotionDetailManager promotionDetailController=new PromotionDetailManager();
					List arllist=new ArrayList();
					List arllistAvl=new ArrayList();
					List arllistSold=new ArrayList();
					
					HashMap<Double,Integer > mapAmountAccommId=new HashMap<Double, Integer>();
					HashMap<Double,Integer > mapSoldOutAmountAccommId=new HashMap<Double, Integer>();
					
					
					HttpServletResponse response = ServletActionContext.getResponse();
					
					response.setContentType("application/json");
					DecimalFormat df = new DecimalFormat("###.##");
					this.arrivalDate = getArrivalDate();
				    sessionMap.put("arrivalDate",arrivalDate); 
						
					this.departureDate = getDepartureDate();
					sessionMap.put("departureDate",departureDate); 
					 
					Calendar cal = Calendar.getInstance();
					cal.setTime(this.departureDate);
				    cal.add(Calendar.DATE, -1);
				    
				    java.util.Date dteCheckOut = cal.getTime();
				    this.departureDate=new Timestamp(dteCheckOut.getTime());
				    sessionMap.put("endDate",departureDate); 
				    
					String startDate = new SimpleDateFormat("yyyy-MM-dd").format(this.arrivalDate);
					String endDate = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
					DateTime start = DateTime.parse(startDate);
				    DateTime end = DateTime.parse(endDate);
				    String jsonOutput = "";
				    DateFormat f = new SimpleDateFormat("EEEE");
				    
			    	List<PmsSmartPrice> listSmartPriceDetail=null; 
			    	List<AccommodationRoom> listAccommodationRoom=null;
			    	
					PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
					
					
			        this.propertyList = propertyController.list();
						for (PmsProperty property : propertyList) {
							int totalAvailableCount=0,totalBookedCount=0;
							long lngRoomCount;
							listAccommodationRoom=accommodationController.listPropertyTotalRooms(property.getPropertyId());
		      				 if(listAccommodationRoom.size()>0 && !listAccommodationRoom.isEmpty()){
		      					 for(AccommodationRoom roomsList:listAccommodationRoom){
		      						totalAvailableCount+=(int) roomsList.getRoomCount();
		      					 }
		      				 }
		      				 
							PmsAvailableRooms propertyAvailableCount = bookingController.findAvailable(property.getPropertyId(), getArrivalDate());
							lngRoomCount=propertyAvailableCount.getRoomCount();
							totalBookedCount=(int)lngRoomCount;
							if(totalAvailableCount==totalBookedCount){
							}else{
							
							SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
							Timestamp FromDate=(Timestamp)mapDate.get("arrivalDate");
							Timestamp ToDate=(Timestamp)mapDate.get("departureDate");
							
							String strFromDate=format2.format(FromDate);
							String strToDate=format2.format(ToDate);
					    	DateTime startdate = DateTime.parse(strFromDate);
					        DateTime enddate = DateTime.parse(strToDate);
					        java.util.Date fromdate = startdate.toDate();
					 		java.util.Date todate = enddate.toDate();
					 		 
							int diffInDays = (int) ((todate.getTime() - fromdate.getTime()) / (1000 * 60 * 60 * 24));
							
							List<PropertyAccommodation> accommodationsList =  accommodationManager.list(property.getPropertyId());
							int soldOutCount=0,availCount=0,promotionAccommId=0;
							if(accommodationsList.size()>0 && !accommodationsList.isEmpty())
							{
								for (PropertyAccommodation accommodation : accommodationsList) {
									int roomCount=0;
									listAccommodationRoom=accommodationController.listPropertyTotalRooms(property.getPropertyId(), accommodation.getAccommodationId());
									if(listAccommodationRoom.size()>0 && !listAccommodationRoom.isEmpty()){
										for(AccommodationRoom roomsList:listAccommodationRoom){
											 roomCount=(int) roomsList.getRoomCount();
											 long minimum = getAvailableCount(property.getPropertyId(),(Timestamp)mapDate.get("arrivalDate"),(Timestamp)mapDate.get("departureDate"),roomCount,accommodation.getAccommodationId());
											 if((int)minimum>0){
												 availCount++;
											 }else if((int)minimum==0){
												 soldOutCount++;
											 }
											 break;
										 }
									 }
								}
							}
	
							
							List<PmsPromotionDetails> listPromotionDetailCheck=null;
							Boolean blnAvailable=false,blnSoldout=false,promotionFlag=false,blnPromoFlag=false;
							int arlListTotalAccomm=0,soldOutTotalAccomm=0,accommCount=0;
							String promotionType=null;
							 ArrayList<String> arlListPromoType=new ArrayList<String>();
							List<PropertyAccommodation> accommodationList =  accommodationManager.list(property.getPropertyId());
							if(accommodationList.size()>0)
							{
								for (PropertyAccommodation accommodation : accommodationList) {
									Integer promoAccommId=0;
									Boolean SmartPriceEnableCheck=false,blnPromotionswithRoomsNights=false,blnBookedGetRooms=false,blnPromotionIsNotFlag=false,blnBookedGetNights=false;
									int dateCount=0;
									String strCheckPromotionType=null;
									double amount=0.0,extraAdultAmount=0.0,extraChildAmount=0.0,totalSmartPriceAmount=0.0;
									int roomCount=0,availableCount=0,totalCount=0,lastMinuteHours=0;
								    double dblPercentCount=0.0,dblSmartPricePercent=0.0,promotionPercent=0.0,totalFlatLastAmount=0.0,flatLastAmount=0.0;
								    double dblPercentFrom=0.0,dblPercentTo=0.0;
								    Double totalGetNights=0.0,promotionAmount=0.0,getRooms=0.0,bookRooms=0.0,getNights=0.0,bookNights=0.0;
								    Timestamp lastMinStartDate=null,lastMinEndDate=null;
								    Boolean isPositive=false,isNegative=false,blnLastMinutePromotions=false;
								  
					    			
					    			listAccommodationRoom=accommodationController.listPropertyTotalRooms(property.getPropertyId(), accommodation.getAccommodationId());
									 
									if(listAccommodationRoom.size()>0 && !listAccommodationRoom.isEmpty()){
										
										for(AccommodationRoom roomsList:listAccommodationRoom){
											roomCount=(int) roomsList.getRoomCount();
											if(roomCount>0){
												long minimum = getAvailableCount(property.getPropertyId(),(Timestamp)mapDate.get("arrivalDate"),(Timestamp)mapDate.get("departureDate"),roomCount,accommodation.getAccommodationId());
												if((int)minimum>0){
													blnAvailable=true;
													arlListTotalAccomm++;
												}else if((int)minimum==0){
													blnSoldout=true;
													soldOutTotalAccomm++;
												}
											}
											 break;
										 }
									 }
									 sourceId = 1;
									 List<DateTime> between = DateUtil.getDateRange(start, end);
									 
									 boolean blnActivePromo=false,blnNotActivePromo=false,basePromoFlag=false;
									 ArrayList<Integer> arlActivePromo=new ArrayList<Integer>(); 
									 for (DateTime d : between)
								     {
										 List<PmsPromotions> listAllPromotions= promotionController.listDatePromotions(property.getPropertyId(),d.toDate());
										 if(!listAllPromotions.isEmpty() && listAllPromotions.size()>0){
											 for(PmsPromotions promos:listAllPromotions){
												 if(promos.getPromotionType().equalsIgnoreCase("F")){
													 if(arlActivePromo.isEmpty()){
														 arlActivePromo.add(promos.getPromotionId());
													 }else if(!arlActivePromo.contains(promos.getPromotionId())){
														 arlActivePromo.add(promos.getPromotionId());
													 }
												 }
											 }
										 }else{
											 blnNotActivePromo=true;
										 }
								     }
									 int intPromoSize=arlActivePromo.size();
									 if(intPromoSize>1 || blnNotActivePromo){
										 blnActivePromo=false;
									 }else{
										 blnActivePromo=true;
									 }
									 
								     for (DateTime d : between)
								     {
								    	 
								    	 Integer promotionId=0;
								    	 
								    	 if(blnActivePromo){
								    		 List<PmsPromotions> listPromotions= promotionController.listDatePromotions(property.getPropertyId(),d.toDate());
									    	 if(listPromotions.size()>0 && !listPromotions.isEmpty()){
									    		 for(PmsPromotions promotions:listPromotions){
									    			
									    			 if(!arlListPromoType.isEmpty()){
								    					 if(!arlListPromoType.contains(promotions.getPromotionType())){
								    						 promotionFlag=false;
										    				 promotionType=null;
										    				 blnPromoFlag=false;
										    				 basePromoFlag=true;
										    				 break;
								    					 }
								    				 }
									    			 if(promotions.getPromotionType().equalsIgnoreCase("F")){
									    				 promotionId=promotions.getPromotionId();
									    				 listPromotionDetailCheck=promotionDetailController.listPromotionDetails(promotions.getPromotionId(),f.format(d.toDate()).toLowerCase());
										    			 if(listPromotionDetailCheck.size()>0 && !listPromotionDetailCheck.isEmpty()){
										    				 for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
										    					 promotionPercent=promoDetails.getPromotionPercentage();
									    						 promotionFlag=true;
											    				 blnPromoFlag=true;
											    				 strCheckPromotionType="F";
											    				 promotionType=promotions.getPromotionType();
											    				 arlListPromoType.add(promotionType);
											    				 this.promotionId=promotions.getPromotionId();
									    					 }
										    			 }
									    			 }else{
									    				 List<PmsPromotions> listPmsPromotions= promotionController.listDatePromotions(property.getPropertyId(),accommodation.getAccommodationId(),d.toDate());
										    			 if(listPmsPromotions.size()>0 &&!listPmsPromotions.isEmpty()){
										    				 for(PmsPromotions pmsPromotions:listPmsPromotions){
										    					 promotionId=pmsPromotions.getPromotionId();
											    				 //promotionType=pmsPromotions.getPromotionType();
											    				 promoAccommId=pmsPromotions.getPropertyAccommodation().getAccommodationId();
											    				 listPromotionDetailCheck=promotionDetailController.listPromotionDetails(pmsPromotions.getPromotionId(),f.format(d.toDate()).toLowerCase());
											    				 if(listPromotionDetailCheck.size()>0 && !listPromotionDetailCheck.isEmpty()){
												        			 if(listPromotionDetailCheck.size()>1){
													    				 promotionFlag=false;
													    				 promotionType=null;
													    				 blnPromoFlag=false;
													    				 basePromoFlag=true;
													    			 }else if(listPromotionDetailCheck.size()==1){
													    				 if(!pmsPromotions.getPromotionType().equalsIgnoreCase("F")){
														    				 if(promoAccommId.intValue()==accommodation.getAccommodationId().intValue()){
														    					 promotionAccommId=promoAccommId.intValue();
														    					 lastMinStartDate=pmsPromotions.getStartDate();
															    				 lastMinEndDate=pmsPromotions.getEndDate();
															    				 Double nightsGet=0.0,nightsBook=0.0;
															    				 if(pmsPromotions.getPromotionType().equalsIgnoreCase("N")){
															    					 for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
															    						 nightsBook=promoDetails.getBookNights();
															    						 nightsGet=promoDetails.getGetNights();
															    						 promotionAmount=promoDetails.getBaseAmount();
															    						 totalGetNights=nightsBook+nightsGet;
														    			    		     Integer intTotalGetNights=totalGetNights.intValue()-1;
														    			    		     SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
														    			    			 Calendar calCheckOut=Calendar.getInstance();
														    			    			 calCheckOut.setTime(getDepartureDate());
														    			    			 calCheckOut.add(Calendar.DATE,intTotalGetNights);
														    			    			 java.util.Date checkOutDte = calCheckOut.getTime();             
														    			    			 String departureDate = format1.format(checkOutDte);
														    							 
														    			    			 java.util.Date fromDate = format1.parse(departureDate);
														    			    			 Timestamp tsDepartureDate=new Timestamp(fromDate.getTime());
														    			    			 String strStartDate=format2.format(getArrivalDate());
														    			    			 String strEndDate=format2.format(tsDepartureDate);
														    			    			 DateTime Startdate = DateTime.parse(strStartDate);
														    			    			 DateTime Enddate = DateTime.parse(strEndDate);
														    			    			 java.util.Date StartDate = Startdate.toDate();
													    			    		 		 java.util.Date EndDate = Enddate.toDate();
													    			    		 		 int diffDays = (int) ((EndDate.getTime() - StartDate.getTime()) / (1000 * 60 * 60 * 24));
														    			    			 
														    			    			 long minimumCount = getAvailableCount(getPropertyId(),getArrivalDate(),tsDepartureDate,roomCount,accommodation.getAccommodationId());
														    			    			 
																    					 if((int)minimumCount>0){
																    						 if(nightsBook.intValue()==diffInDays){
																	    						 promotionFlag=true;
																	    						 blnPromoFlag=true;
																	    						 strCheckPromotionType="N";
																	    						 promotionType=promotions.getPromotionType();
																	    						 arlListPromoType.add(promotionType);
																	    						 this.promotionId=promotionId;
																	    					 }else{
																	    						 promotionFlag=false;
																	    						 promotionType=null;
																	    						 blnPromoFlag=false;
																	    						 this.promotionId=null;
																	    						 basePromoFlag=true;
																	    					 }
																    					 }else{
																    						 promotionFlag=false;
																    						 promotionType=null;
																    						 blnPromoFlag=false;
																    						 this.promotionId=null;
																    						 basePromoFlag=true;
																    					 }
																    				 }
															    				 }else if(pmsPromotions.getPromotionType().equalsIgnoreCase("R")){
																    				 for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
															    						 getRooms=promoDetails.getGetRooms();
															    						 bookRooms=promoDetails.getBookRooms();
															    					 }
													    							 
													    							 long minimum = getAvailableCount(getPropertyId(),(Timestamp)mapDate.get("arrivalDate"),(Timestamp)mapDate.get("departureDate"),roomCount,accommodation.getAccommodationId()); 
													    							 
													    							 blnBookedGetRooms=bookGetRooms((int)minimum,getRooms,bookRooms);
												    						    	 if(!blnBookedGetRooms){
												    						    		 promotionFlag=false;
												    						    		 promotionType=null;
												    						    		 blnPromoFlag=false;
												    						    		 this.promotionId=null;
												    						    		 basePromoFlag=true;
												    						    	 }else{
												    						    		 promotionFlag=true;
												    						    		 blnPromoFlag=true;
												    						    		 strCheckPromotionType="R";
												    						    		 promotionType=promotions.getPromotionType();
												    						    		 arlListPromoType.add(promotionType);
												    						    		 this.promotionId=promotionId;
												    						    	 }
															    				 }else if(pmsPromotions.getPromotionType().equalsIgnoreCase("L")){
															    					 for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
															    						 lastMinuteHours=promoDetails.getPromotionHours();
															    						 promotionPercent=promoDetails.getPromotionPercentage();
															    					 }
															    					
															    					 long minimumCount = getAvailableCount(getPropertyId(),(Timestamp)mapDate.get("arrivalDate"),(Timestamp)mapDate.get("departureDate"),roomCount,accommodation.getAccommodationId());
													    			    			 if((int)minimumCount>0){
													    			    				 blnLastMinutePromotions=getCurrentAvailable(getPropertyId(),getArrivalDate(),getDepartureDate(),lastMinuteHours,(long)roomCount,accommodation.getAccommodationId(),lastMinStartDate,lastMinEndDate);
													    			    				 if(blnLastMinutePromotions){
													    			    					 promotionFlag=true; 
													    			    					 blnPromoFlag=true;
													    			    					 strCheckPromotionType="L";
													    			    					 promotionType=promotions.getPromotionType();
													    			    					 arlListPromoType.add(promotionType);
													    			    					 this.promotionId=promotionId;
													    			    				 }
													    			    			 }else{
													    			    				 promotionFlag=false;
													    			    				 promotionType=null;
													    			    				 blnPromoFlag=false;
													    			    				 this.promotionId=null;
													    			    				 basePromoFlag=true;
													    			    			 }
															    				 }
														    				 }else{
														    					 promotionFlag=false;
														    					 promotionType=null;
														    					 blnPromoFlag=false;
														    					 this.promotionId=null;
														    					 basePromoFlag=true;
														    				 }
													    				 }
													    			 }
												        		 }
										    				 }
										    			 }
									    			 }
									        		 
									    		 }
									    	 }
								    	 }else{
								    		 promotionFlag=false;
					    					 promotionType=null;
					    					 blnPromoFlag=false;
					    					 this.promotionId=null;
					    					 basePromoFlag=true;
								    	 }
								    	 if(promotionFlag){
								    		 if(promotionType.equalsIgnoreCase("F") || promotionType.equalsIgnoreCase("L")){
								    			int rateCount=0,rateIdCount=0;
					    						List<PropertyRate> propertyRateList =  rateController.listAllDates(property.getPropertyId(),accommodation.getAccommodationId(),getSourceId(),d.toDate());
					    						List<PropertyRate> currentDateSmartPriceActive=rateController.listCurrentSmartPriceActive(property.getPropertyId(),accommodation.getAccommodationId(), d.toDate());
					    				    	if(propertyRateList.size()>0)
					    				    	{
					    				    		 for (PropertyRate pr : propertyRateList)
					    			    			 {
									    				 int propertyRateId = pr.getPropertyRateId();
					        				    		 Boolean SmartPriceCheck=pr.getSmartPriceIsActive();
											    		 if(SmartPriceCheck){
											    			 List<PropertyRateDetail> rateDetailList =   detailController.list(propertyRateId,f.format(d.toDate()).toLowerCase());
												    		 if(rateDetailList.size()>0){
												    			 for (PropertyRateDetail rate : rateDetailList){
												    				 if(rateCount<1){
												    					 SmartPriceEnableCheck=true;
													    				 amount =  rate.getBaseAmount();
													    				 rateCount++;
												    				 }
												    			 }
												    		 }
											    		 }else{
											    			 List<PropertyRateDetail> rateDetailList =   detailController.list(propertyRateId,f.format(d.toDate()).toLowerCase());
						        				    		 if(rateDetailList.size()>0){
						        				    			 if(rateCount<1){
						        				    				 for (PropertyRateDetail rate : rateDetailList){
							        				    				 if(currentDateSmartPriceActive.size()>0){
							        				    					 SmartPriceEnableCheck=true;
							        						    			 amount =  rate.getBaseAmount();
							        				    				 }else{
							        				    					 amount =  rate.getBaseAmount();
							        				    				 }
							        				    			 }
						        				    				 rateCount++;
						        				    			 }
						        				    		 }else{
						        				    			 if(propertyRateList.size()==rateIdCount){
						        				    				 amount = accommodation.getBaseAmount();
						        				    			 }
						            				    	 }
											    		 }
					    			    			 }	    		 
					    				    	 }
					    				    	 else{
					    				    		 amount = accommodation.getBaseAmount();
					    				    	 }
					    				    	String strTypePercent=String.valueOf(promotionPercent);
									    		  if(!strTypePercent.equalsIgnoreCase("null")){
									    			  flatLastAmount = amount-(amount*promotionPercent/100);
									    		  }
									    		  else{
									    			  flatLastAmount = amount;
									    		  } 
									    		  totalFlatLastAmount+=flatLastAmount;
						    				 }else if(promotionType.equalsIgnoreCase("R")){
						    					 listPromotionDetailCheck=promotionDetailController.listPromotionDetails(promotionId,f.format(d.toDate()).toLowerCase());
						    					 if(listPromotionDetailCheck.size()>0){
						    						 for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
						    							 amount =promoDetails.getBaseAmount();
						    						 }
						    					 }
						    				 }else if(promotionType.equalsIgnoreCase("N")){
						    					 listPromotionDetailCheck=promotionDetailController.listPromotionDetails(promotionId,f.format(d.toDate()).toLowerCase());
						    					 if(listPromotionDetailCheck.size()>0){
						    						 for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
						    							 amount =promoDetails.getBaseAmount();
						    						 }
						    					 }
						    				 }
								    	 }else{
								    		 if(basePromoFlag){
									    		amount += accommodation.getBaseAmount();
									    	}else{
									    		int rateCount=0,rateIdCount=0;
					    						List<PropertyRate> propertyRateList =  rateController.listAllDates(property.getPropertyId(),accommodation.getAccommodationId(),getSourceId(),d.toDate());
					    						List<PropertyRate> currentDateSmartPriceActive=rateController.listCurrentSmartPriceActive(property.getPropertyId(),accommodation.getAccommodationId(), d.toDate());
					    				    	if(propertyRateList.size()>0)
					    				    	{
					    				    		 for (PropertyRate pr : propertyRateList)
					    			    			 {
									    				 int propertyRateId = pr.getPropertyRateId();
					        				    		 Boolean SmartPriceCheck=pr.getSmartPriceIsActive();
											    		 if(SmartPriceCheck){
											    			 List<PropertyRateDetail> rateDetailList =   detailController.list(propertyRateId,f.format(d.toDate()).toLowerCase());
												    		 if(rateDetailList.size()>0){
												    			 for (PropertyRateDetail rate : rateDetailList){
												    				 if(rateCount<1){
												    					 SmartPriceEnableCheck=true;
													    				 amount =  rate.getBaseAmount();
													    				 rateCount++;
												    				 }
												    			 }
												    		 }
											    		 }else{
											    			 List<PropertyRateDetail> rateDetailList =   detailController.list(propertyRateId,f.format(d.toDate()).toLowerCase());
						        				    		 if(rateDetailList.size()>0){
						        				    			 if(rateCount<1){
						        				    				 for (PropertyRateDetail rate : rateDetailList){
							        				    				 if(currentDateSmartPriceActive.size()>0){
							        				    					 SmartPriceEnableCheck=true;
							        						    			 amount =  rate.getBaseAmount();
							        				    				 }else{
							        				    					 amount +=  rate.getBaseAmount();
							        				    				 }
							        				    			 }
						        				    				 rateCount++;
						        				    			 }
						        				    		 }else{
						        				    			 if(propertyRateList.size()==rateIdCount){
						        				    				 amount += accommodation.getBaseAmount();
						        				    			 }
						            				    	 }
											    		 }
					    			    			 }	    		 
					    				    	 }
					    				    	 else{
					    				    		 amount += accommodation.getBaseAmount();
					    				    	 }
									    	}
								    		 
								    	 }
								    	 if(SmartPriceEnableCheck){
								    		double smartPriceAmount=0.0;
								    		 listAccommodationRoom=accommodationController.listPropertyTotalRooms(property.getPropertyId(), accommodation.getAccommodationId());
						      				 if(listAccommodationRoom.size()>0 && !listAccommodationRoom.isEmpty()){
						      					 for(AccommodationRoom roomsList:listAccommodationRoom){
						      						 roomCount=(int) roomsList.getRoomCount();
						      						 long minimum = getAvailableCount(property.getPropertyId(),(Timestamp)mapDate.get("arrivalDate"),(Timestamp)mapDate.get("departureDate"),roomCount,accommodation.getAccommodationId()); 
						      						 availableCount=(int)minimum;
						      						 dblPercentCount=(double)((roomCount-availableCount)*100)/roomCount;
						      						 totalCount=(int) Math.round(dblPercentCount);
						      						 listSmartPriceDetail=detailController.listSmartPriceDetail(totalCount);
						      						 if(listSmartPriceDetail.size()>0 && !listSmartPriceDetail.isEmpty()){
						      							 for(PmsSmartPrice smartPrice: listSmartPriceDetail){
						      								 dblSmartPricePercent=smartPrice.getAmountPercent();
						      								 dblPercentFrom=smartPrice.getPercentFrom();
						      								 dblPercentTo=smartPrice.getPercentTo();
						      								 
						      								 if(dblPercentFrom>=0 && dblPercentTo<=59){
						      									 isNegative=true;
						      								 }else if(dblPercentFrom>=60 && dblPercentTo<=99){
						      									 isPositive=true;
						      								 }
						      								 if(isNegative){
						      									smartPriceAmount=amount-(amount*dblSmartPricePercent/100);
						      								 }
						      								 if(isPositive){
						      									smartPriceAmount=amount+(amount*dblSmartPricePercent/100);
						      								 }
						      							 }
						      						 }
						      					 }
						      				 }
						      				totalSmartPriceAmount+=smartPriceAmount;
								    	 }
								    	 dateCount++;
								    	 if(dateCount==diffInDays){
								    		 if(blnAvailable){
								    			if(promotionType!=null && strCheckPromotionType!=null) {
								    				if(strCheckPromotionType.equalsIgnoreCase("F") || strCheckPromotionType.equalsIgnoreCase("L")){
									    				mapAmountAccommId.put(totalFlatLastAmount,accommodation.getAccommodationId());
										      			arllist.add(totalFlatLastAmount);
									    			}else if(SmartPriceEnableCheck){
									    				mapAmountAccommId.put(totalSmartPriceAmount,accommodation.getAccommodationId());
										      			arllist.add(totalSmartPriceAmount);
									    			}
								    			}else{
								    				mapAmountAccommId.put(amount,accommodation.getAccommodationId());
									      			arllist.add(amount);
								    			}
						      				 }else if(blnSoldout){
						      					if(promotionType!=null && strCheckPromotionType!=null) {
						      						if(strCheckPromotionType.equalsIgnoreCase("F") || strCheckPromotionType.equalsIgnoreCase("L")){
							      						mapSoldOutAmountAccommId.put(totalFlatLastAmount,accommodation.getAccommodationId());
										      			arllist.add(totalFlatLastAmount);
									    			}else if(SmartPriceEnableCheck){
									    				mapSoldOutAmountAccommId.put(totalSmartPriceAmount,accommodation.getAccommodationId());
										      			arllist.add(totalSmartPriceAmount);
									    			}
						      					}else{
								    				mapSoldOutAmountAccommId.put(amount,accommodation.getAccommodationId());
									      			arllist.add(amount);
								    			}
									    	 }
								    	 }
								     }
								     if(blnAvailable){
								    	 //arllistAvl.add(amount);
								    	 if(promotionType!=null && strCheckPromotionType!=null) {
								    		 if(strCheckPromotionType.equalsIgnoreCase("F") || strCheckPromotionType.equalsIgnoreCase("L")){
									    		 arllistAvl.add(totalFlatLastAmount);
							    			}
								    	 }else if(SmartPriceEnableCheck){
							    		     arllistAvl.add(totalSmartPriceAmount);
								    	 }
								    	 else{
						    				arllistAvl.add(amount);
						    			}
								     }else if(!blnAvailable){
								    	// arllistSold.add(amount);
								    	 if(promotionType!=null && strCheckPromotionType!=null) {
								    		 if(strCheckPromotionType.equalsIgnoreCase("F") || strCheckPromotionType.equalsIgnoreCase("L")){
									    		 arllistSold.add(totalFlatLastAmount);
							    			}
								    	 }else if(SmartPriceEnableCheck){
							    				arllistSold.add(totalSmartPriceAmount);
							    			}else{
						    				arllistSold.add(amount);
						    			}
								     }
								     accommCount++;
								}
							}
							//end of accommodation
							double totalAmount=0;
							boolean blnJSONContent=false;
							Collections.sort(arllist);
							
							
							int arlListSize=0,accommId=0,intTotalAccomm=0;
						     intTotalAccomm=accommodationList.size();
						     if(intTotalAccomm==accommCount){
						    	 if(intTotalAccomm==soldOutCount){
							    	 if(blnPromoFlag){
								    	 if(arlListPromoType.contains("F")){
								    		 arlListSize=arllist.size();
										     if(soldOutCount==arlListSize){
										    	 Collections.sort(arllist);
										    	 for(int i=0;i<arlListSize;i++){
											    	 totalAmount=(Double) arllist.get(i);
											    	 if((Double) arllist.get(0)==(Double) arllist.get(i)){
											    		 if(blnAvailable){
											    			 accommId=mapAmountAccommId.get(totalAmount);
												    		 blnJSONContent=true;
											    		 }else{
											    			 accommId=mapSoldOutAmountAccommId.get(totalAmount);
												    		 blnJSONContent=true;
											    		 }
											    		 
											    		 break;
											    	 }else{
											    		 
											    	 }
											     }
										     }
								    	 }else if(arlListPromoType.contains("N") || arlListPromoType.contains("R") || arlListPromoType.contains("L")){
								    		 if(promotionAccommId>0){
									    		 accommId=promotionAccommId;
									    		 blnJSONContent=true;
									    	 } 
								    	 }
								     }else{
									     arlListSize=arllist.size();
									     if(soldOutCount==arlListSize){
									    	 Collections.sort(arllist);
									    	 for(int i=0;i<arlListSize;i++){
										    	 totalAmount=(Double) arllist.get(i);
										    	 if((Double) arllist.get(0)==(Double) arllist.get(i)){
										    		 if(blnAvailable){
										    			 accommId=mapAmountAccommId.get(totalAmount);
											    		 blnJSONContent=true;
										    		 }else{
										    			 accommId=mapSoldOutAmountAccommId.get(totalAmount);
											    		 blnJSONContent=true;
										    		 }
										    		 break;
										    	 }else{
										    		 
										    	 }
										     }
									     }
									     
								     }
							     }else{
							    	 if(arlListPromoType.size()>0){
							    		 if(arlListPromoType.contains("F")){
	//							    	 if(promotionType.equalsIgnoreCase("F") || promotionType.equalsIgnoreCase("L")){
								    		 arlListSize=arllist.size();
								    		 Collections.sort(arllist);
									    	 for(int i=0;i<arlListSize;i++){
									    		 if(arllistAvl.contains((Double) arllist.get(i))){
									    			 if((Double) arllist.get(0)==(Double) arllist.get(i)){
										    			 totalAmount=(Double) arllist.get(i);
										    			 accommId=mapAmountAccommId.get(totalAmount);
											    		 blnJSONContent=true;
									    			 }else{
									    				 totalAmount=(Double) arllist.get(i);
										    			 accommId=mapAmountAccommId.get(totalAmount);
											    		 blnJSONContent=true;
									    			 }
									    			 break;
									    		 }
										     }
								    	 }else if(arlListPromoType.contains("N") || arlListPromoType.contains("R") || arlListPromoType.contains("L")){
								    		 if(promotionAccommId>0){
									    		 accommId=promotionAccommId;
									    		 blnJSONContent=true;
									    	 } 
								    	 }
								     }else{
									     arlListSize=arllist.size();
									     Collections.sort(arllist);
								    	 for(int i=0;i<arlListSize;i++){
								    		 if(arllistAvl.contains((Double) arllist.get(i))){
								    			 if((Double) arllist.get(0)==(Double) arllist.get(i)){
									    			 totalAmount=(Double) arllist.get(i);
									    			 accommId=mapAmountAccommId.get(totalAmount);
										    		 blnJSONContent=true;
								    			 }else{
								    				 totalAmount=(Double) arllist.get(i);
									    			 accommId=mapAmountAccommId.get(totalAmount);
										    		 blnJSONContent=true;
								    			 }
								    			 break;
								    		 }
									     }
								     }
							     }
						     }
							
							if(blnJSONContent){
	
								 if (!jsonOutput.equalsIgnoreCase(""))
										jsonOutput += ",{";
									else
										jsonOutput += "{";
								 
								jsonOutput += "\"propertyId\":\"" + property.getPropertyId()+ "\"";
								 jsonOutput += ",\"propertyName\":\"" + property.getPropertyName().toUpperCase()+ "\"";
								 jsonOutput += ",\"propertyImage\":\"" + property.getPropertyThumbPath()+ "\"";
								 jsonOutput += ",\"propertyLocation\":\"" + property.getLocation().getLocationName()+ "\"";
								 jsonOutput += ",\"propertyUrl\":\"" + (property.getPropertyUrl().toLowerCase()==null? "None":property.getPropertyUrl().toLowerCase())+ "\"";
								 

							    listAccommodationRoom.clear(); 
							    ArrayList<String> arlPromoType=new ArrayList<String>();
						    	listAccommodationRoom=accommodationController.listPropertyTotalRooms(property.getPropertyId(), accommId);
						    	List<DateTime> betweenAmount = DateUtil.getDateRange(start, end);
						    	double actualBaseAmount=0.0,actualTotalAmount=0.0,actualTotalAdultAmount=0.0,actualTotalChildAmount=0.0,
						    			actualAdultAmount=0.0,actualChildAmount=0.0,promotionPercent=0.0;
							    int countDate=0,promotionId=0,promotionHours=0,promoAccommId=0; 
							    boolean blnStatus=false,isPositive=false,isNegative=false,promoFlag=false,blnSmartPriceFlag=false,blnBookedGetRooms=false,blnLastMinutePromotions=false;
							    String promotionName=null;
							    double promoBaseAmount=0.0,promoAdultAmount=0.0,promoChildAmount=0.0;
							    double baseAmount=0.0,extraAdultAmount=0.0,extraChildAmount=0.0,totalBaseAmount=0.0,totalSmartPriceAmount=0.0,totalFlatAmount=0.0,totalLastAmount=0.0;
							    int roomCount=0,availableCount=0,totalCount=0,lastMinuteHours=0;
							    Double dblGetNights=0.0,dblBookNights=0.0,dblGetRooms=0.0,dblBookRooms=0.0,totalGetNights=0.0;
							    double dblPercentCount=0.0,dblSmartPricePercent=0.0;
							    double dblPercentFrom=0.0,dblPercentTo=0.0;
							    Timestamp lastMinStartDate=null,lastMinEndDate=null;
								if(listAccommodationRoom.size()>0 && !listAccommodationRoom.isEmpty()){
									for(AccommodationRoom roomsList:listAccommodationRoom){
										 roomCount=(int) roomsList.getRoomCount();
										 break;
									 }
								 }
								
								List<DateTime> between = DateUtil.getDateRange(start, end);
								 
								 boolean blnActivePromo=false,blnNotActivePromo=false,basePromoFlag=false;
								 ArrayList<Integer> arlActivePromo=new ArrayList<Integer>(); 
								 for (DateTime d : between)
							     {
									 List<PmsPromotions> listAllPromotions= promotionController.listDatePromotions(property.getPropertyId(),d.toDate());
									 if(!listAllPromotions.isEmpty() && listAllPromotions.size()>0){
										 for(PmsPromotions promos:listAllPromotions){
											 if(promos.getPromotionType().equalsIgnoreCase("F")){
												 if(arlActivePromo.isEmpty()){
													 arlActivePromo.add(promos.getPromotionId());
												 }else if(!arlActivePromo.contains(promos.getPromotionId())){
													 arlActivePromo.add(promos.getPromotionId());
												 }
											 }
										 }
									 }else{
										 blnNotActivePromo=true;
									 }
							     }
								 int intPromoSize=arlActivePromo.size();
								 if(intPromoSize>1 || blnNotActivePromo){
									 blnActivePromo=false;
									 basePromoFlag=true;
								 }else{
									 blnActivePromo=true;
								 }
								 
								
								List<PropertyAccommodation> listAccommodation =  accommodationManager.list(property.getPropertyId(),accommId);
								
								for (PropertyAccommodation accommodations : listAccommodation) {
									for (DateTime d : betweenAmount)
								    {
										boolean blnFlatPromotions=false,SmartPriceEnableCheck=false;
										if(blnActivePromo){
											listPromotionDetailCheck=null;
											List<PmsPromotions> listFlatPromotions= promotionController.listDatePromotions(property.getPropertyId(),d.toDate());
									    	if(listFlatPromotions.size()>0 && !listFlatPromotions.isEmpty()){
									    		for(PmsPromotions promotions:listFlatPromotions){
									    			if(promotions.getPromotionType().equalsIgnoreCase("F")){
									    				promotionId=promotions.getPromotionId();
									    				listPromotionDetailCheck=promotionDetailController.listPromotionDetails(promotions.getPromotionId(),f.format(d.toDate()).toLowerCase());
										    			 if(listPromotionDetailCheck.size()>0 && !listPromotionDetailCheck.isEmpty()){
										    				 for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
										    					 promotionPercent=promoDetails.getPromotionPercentage();
									    						 promotionFlag=true;
											    				 blnPromoFlag=true;
											    				 promotionType=promotions.getPromotionType();
											    				 arlPromoType.add(promotionType);
										    				 }
										    			 }
									    			}else{
									    				 List<PmsPromotions> listPmsPromotions= promotionController.listDatePromotions(property.getPropertyId(),accommodations.getAccommodationId(),d.toDate());
										    			 if(listPmsPromotions.size()>0 &&!listPmsPromotions.isEmpty()){
										    				 for(PmsPromotions pmsPromotions:listPmsPromotions){
										    					 promotionId=pmsPromotions.getPromotionId();
										    					 promoAccommId=pmsPromotions.getPropertyAccommodation().getAccommodationId();
										    					 listPromotionDetailCheck=promotionDetailController.listPromotionDetails(pmsPromotions.getPromotionId(),f.format(d.toDate()).toLowerCase());
										    					 if(listPromotionDetailCheck.size()>0 && !listPromotionDetailCheck.isEmpty()){
												        			 if(listPromotionDetailCheck.size()>1){
													    				 promotionFlag=false;
													    				 promotionType=null;
													    				 blnPromoFlag=false;
													    				 basePromoFlag=true;
													    			 }else if(listPromotionDetailCheck.size()==1){
													    				 if(!pmsPromotions.getPromotionType().equalsIgnoreCase("F")){
														    				 if(promoAccommId==accommId){
														    					 promotionAccommId=promoAccommId;
														    					 lastMinStartDate=pmsPromotions.getStartDate();
															    				 lastMinEndDate=pmsPromotions.getEndDate();
															    				 Double nightsGet=0.0,nightsBook=0.0;
															    				 if(pmsPromotions.getPromotionType().equalsIgnoreCase("N")){
															    					 for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
															    						 nightsBook=promoDetails.getBookNights();
															    						 nightsGet=promoDetails.getGetNights();
															    						 totalGetNights=nightsBook+nightsGet;
														    			    		     Integer intTotalGetNights=totalGetNights.intValue()-1;
														    			    		     SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
														    			    			 Calendar calCheckOut=Calendar.getInstance();
														    			    			 calCheckOut.setTime(getDepartureDate());
														    			    			 calCheckOut.add(Calendar.DATE,intTotalGetNights);
														    			    			 java.util.Date checkOutDte = calCheckOut.getTime();             
														    			    			 String departureDate = format1.format(checkOutDte);
														    							 
														    			    			 java.util.Date fromDate = format1.parse(departureDate);
														    			    			 Timestamp tsDepartureDate=new Timestamp(fromDate.getTime());
														    			    			 String strStartDate=format2.format(getArrivalDate());
														    			    			 String strEndDate=format2.format(tsDepartureDate);
														    			    			 DateTime Startdate = DateTime.parse(strStartDate);
														    			    			 DateTime Enddate = DateTime.parse(strEndDate);
														    			    			 java.util.Date StartDate = Startdate.toDate();
													    			    		 		 java.util.Date EndDate = Enddate.toDate();
													    			    		 		 int diffDays = (int) ((EndDate.getTime() - StartDate.getTime()) / (1000 * 60 * 60 * 24));
														    			    			 
														    			    			 long minimumCount = getAvailableCount(getPropertyId(),(Timestamp)mapDate.get("arrivalDate"),(Timestamp)mapDate.get("departureDate"),roomCount,accommId);
														    			    			 
																    					 if((int)minimumCount>0){
																    						 if(nightsBook.intValue()==diffInDays){
																	    						 promotionFlag=true;
																	    						 blnPromoFlag=true;
																	    						 promotionType=pmsPromotions.getPromotionType();
																	    						 arlPromoType.add(promotionType);
																	    					 }else{
																	    						 promotionFlag=false;
																	    						 promotionType=null;
																	    						 blnPromoFlag=false;
																	    						 basePromoFlag=true;
																	    					 }
																    					 }else{
																    						 promotionFlag=false;
																    						 promotionType=null;
																    						 blnPromoFlag=false;
																    						 basePromoFlag=true;
																    					 }
																    				 }
															    				 }else if(pmsPromotions.getPromotionType().equalsIgnoreCase("R")){
																    				 for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
															    						 dblGetRooms=promoDetails.getGetRooms();
															    						 dblBookRooms=promoDetails.getBookRooms();
															    					 }
													    							 
													    							 long minimum = getAvailableCount(getPropertyId(),(Timestamp)mapDate.get("arrivalDate"),(Timestamp)mapDate.get("departureDate"),roomCount,accommId); 
													    							 
													    							 blnBookedGetRooms=bookGetRooms((int)minimum,dblGetRooms,dblBookRooms);
												    						    	 if(!blnBookedGetRooms){
												    						    		 promotionFlag=false;
												    						    		 promotionType=null;
												    						    		 blnPromoFlag=false;
												    						    		 basePromoFlag=true;
												    						    	 }else{
												    						    		 promotionFlag=true;
												    						    		 blnPromoFlag=true;
												    						    		 promotionType=pmsPromotions.getPromotionType();
												    						    		 arlPromoType.add(promotionType);
												    						    	 }
															    				 }else if(pmsPromotions.getPromotionType().equalsIgnoreCase("L")){
															    					 for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
															    						 lastMinuteHours=promoDetails.getPromotionHours();
															    						 promotionPercent=promoDetails.getPromotionPercentage();
															    					 }
															    					
															    					 long minimumCount = getAvailableCount(getPropertyId(),(Timestamp)mapDate.get("arrivalDate"),(Timestamp)mapDate.get("departureDate"),roomCount,accommId);
													    			    			 if((int)minimumCount>0){
													    			    				 blnLastMinutePromotions=getCurrentAvailable(getPropertyId(),getArrivalDate(),getDepartureDate(),lastMinuteHours,(long)roomCount,accommId,lastMinStartDate,lastMinEndDate);
													    			    				 if(blnLastMinutePromotions){
													    			    					 promotionFlag=true; 
													    			    					 blnPromoFlag=true;
													    			    					 promotionType=pmsPromotions.getPromotionType();
													    			    					 arlPromoType.add(promotionType);
													    			    				 }
													    			    			 }else{
													    			    				 promotionFlag=false;
													    			    				 promotionType=null;
													    			    				 blnPromoFlag=false;
													    			    				 basePromoFlag=true;
													    			    			 }
															    				 }
														    				 }
													    				 }
													    			 }
												        		 }
										    				 }
										    			 }
									    			 }
									    		}
									    	}
									    	if(arlPromoType.contains("F")){
							    			    promoFlag=true;
							    			    int rateCount=0,rateIdCount=0;
							    			    double flatAmount=0.0;
					    						List<PropertyRate> propertyRateList =  rateController.listAllDates(property.getPropertyId(),accommodations.getAccommodationId(),getSourceId(),d.toDate());
					    						List<PropertyRate> currentDateSmartPriceActive=rateController.listCurrentSmartPriceActive(property.getPropertyId(),accommodations.getAccommodationId(), d.toDate());
					    				    	if(propertyRateList.size()>0)
					    				    	{
					    				    		 for (PropertyRate pr : propertyRateList)
					    			    			 {
									    				 int propertyRateId = pr.getPropertyRateId();
					        				    		 Boolean SmartPriceCheck=pr.getSmartPriceIsActive();
											    		 if(SmartPriceCheck){
											    			 List<PropertyRateDetail> rateDetailList =   detailController.list(propertyRateId,f.format(d.toDate()).toLowerCase());
												    		 if(rateDetailList.size()>0){
												    			 for (PropertyRateDetail rate : rateDetailList){
												    				 if(rateCount<1){
												    					 SmartPriceEnableCheck=true;
												    					 baseAmount =  rate.getBaseAmount();
													    				 extraAdultAmount+=rate.getExtraAdult();
													    				 extraChildAmount+=rate.getExtraChild();
													    				 rateCount++;
												    				 }
												    			 }
												    		 }
											    		 }else{
											    			 List<PropertyRateDetail> rateDetailList =   detailController.list(propertyRateId,f.format(d.toDate()).toLowerCase());
						        				    		 if(rateDetailList.size()>0){
						        				    			 if(rateCount<1){
						        				    				 for (PropertyRateDetail rate : rateDetailList){
							        				    				 if(currentDateSmartPriceActive.size()>0){
							        				    					 SmartPriceEnableCheck=true;
							        				    					 baseAmount =  rate.getBaseAmount();
						    	        				    				 extraAdultAmount+=rate.getExtraAdult();
						    	        				    				 extraChildAmount+=rate.getExtraChild();
							        				    				 }else{
							        				    					 baseAmount =  rate.getBaseAmount();
						    	        				    				 extraAdultAmount+=rate.getExtraAdult();
						    	        				    				 extraChildAmount+=rate.getExtraChild();
							        				    				 }
							        				    			 }
						        				    				 rateCount++;
						        				    			 }
						        				    		 }else{
						        				    			 if(propertyRateList.size()==rateIdCount){
						        				    				 baseAmount = accommodations.getBaseAmount();
							            				    		 extraAdultAmount+=accommodations.getExtraAdult();
							        			    				 extraChildAmount+=accommodations.getExtraChild();
						        				    			 }
						            				    	 }
											    		 }
					    			    			 }	    		 
					    				    	 }
					    				    	 else{
					    				    		 baseAmount = accommodations.getBaseAmount();
					    				    		 extraAdultAmount+=accommodations.getExtraAdult();
								    				 extraChildAmount+=accommodations.getExtraChild();
					    				    	 }
					    				    	actualBaseAmount+=baseAmount;
					    				    	String strTypePercent=String.valueOf(promotionPercent);
									    		if(!strTypePercent.equalsIgnoreCase("null")){
									    		   flatAmount = baseAmount-(baseAmount*promotionPercent/100);
									    		}
									    		else{
									    		   flatAmount = baseAmount;
									    		} 
									    		totalFlatAmount+=flatAmount;
							    		  }else if(arlPromoType.contains("L") || arlPromoType.contains("N") || arlPromoType.contains("R")){
									    		promoFlag=true;
									    		List<PmsPromotions> listPromotions= promotionController.listDatePromotions(property.getPropertyId(), accommId, d.toDate());
												if(listPromotions.size()>0 && !listPromotions.isEmpty()){
										    		 for(PmsPromotions promotions:listPromotions){
										    			 listPromotionDetailCheck.clear();
										    			 listPromotionDetailCheck=promotionDetailController.listPromotionDetails(promotions.getPromotionId(),f.format(d.toDate()).toLowerCase());
										    			 if(listPromotionDetailCheck.size()>0 && !listPromotionDetailCheck.isEmpty()){
										    				 	promotionId=promotions.getPromotionId();
										    				 	promotionType=promotions.getPromotionType();
										    				 	if(promotionType.equalsIgnoreCase("L")){
											    					 listPromotionDetailCheck=promotionDetailController.listPromotionDetails(promotionId,f.format(d.toDate()).toLowerCase());
														    		  if(listPromotionDetailCheck.size()>0){
											    						 for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
											    							 promotionPercent=promoDetails.getPromotionPercentage();
											    							 promotionHours=promoDetails.getPromotionHours();
											    						 }
														    		} 
														    		  
														    		  
											    					int rateCount=0,rateIdCount=0;
											    					double lastAmount=0.0;
										    						List<PropertyRate> propertyRateList =  rateController.listAllDates(property.getPropertyId(),accommodations.getAccommodationId(),getSourceId(),d.toDate());
										    						List<PropertyRate> currentDateSmartPriceActive=rateController.listCurrentSmartPriceActive(property.getPropertyId(),accommodations.getAccommodationId(), d.toDate());
										    				    	if(propertyRateList.size()>0)
										    				    	{
										    				    		 for (PropertyRate pr : propertyRateList)
										    			    			 {
														    				 int propertyRateId = pr.getPropertyRateId();
										        				    		 Boolean SmartPriceCheck=pr.getSmartPriceIsActive();
																    		 if(SmartPriceCheck){
																    			 List<PropertyRateDetail> rateDetailList =   detailController.list(propertyRateId,f.format(d.toDate()).toLowerCase());
																	    		 if(rateDetailList.size()>0){
																	    			 for (PropertyRateDetail rate : rateDetailList){
																	    				 if(rateCount<1){
																	    					 SmartPriceEnableCheck=true;
																	    					 baseAmount =  rate.getBaseAmount();
																		    				 extraAdultAmount+=rate.getExtraAdult();
																		    				 extraChildAmount+=rate.getExtraChild();
																		    				 rateCount++;
																	    				 }
																	    			 }
																	    		 }
																    		 }else{
																    			 List<PropertyRateDetail> rateDetailList =   detailController.list(propertyRateId,f.format(d.toDate()).toLowerCase());
											        				    		 if(rateDetailList.size()>0){
											        				    			 if(rateCount<1){
											        				    				 for (PropertyRateDetail rate : rateDetailList){
												        				    				 if(currentDateSmartPriceActive.size()>0){
												        				    					 SmartPriceEnableCheck=true;
												        				    					 baseAmount =  rate.getBaseAmount();
											    	        				    				 extraAdultAmount+=rate.getExtraAdult();
											    	        				    				 extraChildAmount+=rate.getExtraChild();
												        				    				 }else{
												        				    					 baseAmount =  rate.getBaseAmount();
											    	        				    				 extraAdultAmount+=rate.getExtraAdult();
											    	        				    				 extraChildAmount+=rate.getExtraChild();
												        				    				 }
												        				    			 }
											        				    				 rateCount++;
											        				    			 }
											        				    		 }else{
											        				    			 if(propertyRateList.size()==rateIdCount){
											        				    				 baseAmount = accommodations.getBaseAmount();
												            				    		 extraAdultAmount+=accommodations.getExtraAdult();
												        			    				 extraChildAmount+=accommodations.getExtraChild();
											        				    			 }
											            				    	 }
																    		 }
										    			    			 }	    		 
										    				    	 }
										    				    	 else{
										    				    		 baseAmount = accommodations.getBaseAmount();
										    				    		 extraAdultAmount+=accommodations.getExtraAdult();
													    				 extraChildAmount+=accommodations.getExtraChild();
										    				    	 }
										    				    	actualBaseAmount+=baseAmount;
										    				    	String strTypePercent=String.valueOf(promotionPercent);
															    	 if(!strTypePercent.equalsIgnoreCase("null")){
															    		 lastAmount = baseAmount-(baseAmount*promotionPercent/100);
															    	 }
															    	 else{
															    		 lastAmount = baseAmount;
															    	 } 
															    	 totalLastAmount+=lastAmount;
															    	 
										    				 }else if(promotionType.equalsIgnoreCase("R")){
										    					 listPromotionDetailCheck=promotionDetailController.listPromotionDetails(promotionId,f.format(d.toDate()).toLowerCase());
										    					 if(listPromotionDetailCheck.size()>0){
										    						 for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
										    							 dblBookRooms=promoDetails.getBookRooms();
										    							 dblGetRooms=promoDetails.getGetRooms();
										    							 promoBaseAmount =promoDetails.getBaseAmount();
										    							 promoAdultAmount =promoDetails.getExtraAdult();
										    							 promoChildAmount =promoDetails.getExtraChild(); 
										    						 }
										    					 }
										    				 }else if(promotionType.equalsIgnoreCase("N")){
										    					 listPromotionDetailCheck=promotionDetailController.listPromotionDetails(promotionId,f.format(d.toDate()).toLowerCase());
										    					 if(listPromotionDetailCheck.size()>0){
										    						 for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
										    							 dblBookNights=promoDetails.getBookNights();
										    							 dblGetNights=promoDetails.getGetNights();
										    							 promoBaseAmount =promoDetails.getBaseAmount();
										    							 promoAdultAmount =promoDetails.getExtraAdult();
										    							 promoChildAmount =promoDetails.getExtraChild(); 
										    						 }
										    					 }
										    				 }
										    			 }
										    		 }
												}
									    	}else{
									    		if(basePromoFlag){
						    				    	baseAmount += accommodations.getBaseAmount();
						    				    	extraAdultAmount+=accommodations.getExtraAdult();
									    			extraChildAmount+=accommodations.getExtraChild();
						    				    	actualBaseAmount=baseAmount;
									    		}else{
									    			int rateCount=0,rateIdCount=0;
						    						List<PropertyRate> propertyRateList =  rateController.listAllDates(property.getPropertyId(),accommodations.getAccommodationId(),getSourceId(),d.toDate());
						    						List<PropertyRate> currentDateSmartPriceActive=rateController.listCurrentSmartPriceActive(property.getPropertyId(),accommodations.getAccommodationId(), d.toDate());
						    				    	if(propertyRateList.size()>0)
						    				    	{
						    				    		 for (PropertyRate pr : propertyRateList)
						    			    			 {
										    				 int propertyRateId = pr.getPropertyRateId();
						        				    		 Boolean SmartPriceCheck=pr.getSmartPriceIsActive();
												    		 if(SmartPriceCheck){
												    			 List<PropertyRateDetail> rateDetailList =   detailController.list(propertyRateId,f.format(d.toDate()).toLowerCase());
													    		 if(rateDetailList.size()>0){
													    			 for (PropertyRateDetail rate : rateDetailList){
													    				 if(rateCount<1){
													    					 SmartPriceEnableCheck=true;
													    					 baseAmount =  rate.getBaseAmount();
														    				 extraAdultAmount+=rate.getExtraAdult();
														    				 extraChildAmount+=rate.getExtraChild();
														    				 rateCount++;
													    				 }
													    			 }
													    		 }
												    		 }else{
												    			 List<PropertyRateDetail> rateDetailList =   detailController.list(propertyRateId,f.format(d.toDate()).toLowerCase());
							        				    		 if(rateDetailList.size()>0){
							        				    			 if(rateCount<1){
							        				    				 for (PropertyRateDetail rate : rateDetailList){
								        				    				 if(currentDateSmartPriceActive.size()>0){
								        				    					 SmartPriceEnableCheck=true;
								        				    					 baseAmount =  rate.getBaseAmount();
							    	        				    				 extraAdultAmount+=rate.getExtraAdult();
							    	        				    				 extraChildAmount+=rate.getExtraChild();
								        				    				 }else{
								        				    					 baseAmount +=  rate.getBaseAmount();
							    	        				    				 extraAdultAmount+=rate.getExtraAdult();
							    	        				    				 extraChildAmount+=rate.getExtraChild();
								        				    				 }
								        				    			 }
							        				    				 rateCount++;
							        				    			 }
							        				    		 }else{
							        				    			 if(propertyRateList.size()==rateIdCount){
							        				    				 baseAmount += accommodations.getBaseAmount();
								            				    		 extraAdultAmount+=accommodations.getExtraAdult();
								        			    				 extraChildAmount+=accommodations.getExtraChild();
							        				    			 }
							            				    	 }
												    		 }
						    			    			 }	    		 
						    				    	 }
						    				    	 else{
						    				    		 baseAmount += accommodations.getBaseAmount();
						    				    		 extraAdultAmount+=accommodations.getExtraAdult();
									    				 extraChildAmount+=accommodations.getExtraChild();
						    				    	 }
						    				    	actualBaseAmount=baseAmount;
									    		}
									    	}
										}else{
											if(basePromoFlag){
					    				    	baseAmount += accommodations.getBaseAmount();
					    				    	extraAdultAmount+=accommodations.getExtraAdult();
								    			extraChildAmount+=accommodations.getExtraChild();
					    				    	actualBaseAmount=baseAmount;
											}else{
												int rateCount=0,rateIdCount=0;
					    						List<PropertyRate> propertyRateList =  rateController.listAllDates(property.getPropertyId(),accommodations.getAccommodationId(),getSourceId(),d.toDate());
					    						List<PropertyRate> currentDateSmartPriceActive=rateController.listCurrentSmartPriceActive(property.getPropertyId(),accommodations.getAccommodationId(), d.toDate());
					    				    	if(propertyRateList.size()>0)
					    				    	{
					    				    		 for (PropertyRate pr : propertyRateList)
					    			    			 {
									    				 int propertyRateId = pr.getPropertyRateId();
					        				    		 Boolean SmartPriceCheck=pr.getSmartPriceIsActive();
											    		 if(SmartPriceCheck){
											    			 List<PropertyRateDetail> rateDetailList =   detailController.list(propertyRateId,f.format(d.toDate()).toLowerCase());
												    		 if(rateDetailList.size()>0){
												    			 for (PropertyRateDetail rate : rateDetailList){
												    				 if(rateCount<1){
												    					 SmartPriceEnableCheck=true;
												    					 baseAmount =  rate.getBaseAmount();
													    				 extraAdultAmount+=rate.getExtraAdult();
													    				 extraChildAmount+=rate.getExtraChild();
													    				 rateCount++;
												    				 }
												    			 }
												    		 }
											    		 }else{
											    			 List<PropertyRateDetail> rateDetailList =   detailController.list(propertyRateId,f.format(d.toDate()).toLowerCase());
						        				    		 if(rateDetailList.size()>0){
						        				    			 if(rateCount<1){
						        				    				 for (PropertyRateDetail rate : rateDetailList){
							        				    				 if(currentDateSmartPriceActive.size()>0){
							        				    					 SmartPriceEnableCheck=true;
							        				    					 baseAmount =  rate.getBaseAmount();
						    	        				    				 extraAdultAmount+=rate.getExtraAdult();
						    	        				    				 extraChildAmount+=rate.getExtraChild();
							        				    				 }else{
							        				    					 baseAmount +=  rate.getBaseAmount();
						    	        				    				 extraAdultAmount+=rate.getExtraAdult();
						    	        				    				 extraChildAmount+=rate.getExtraChild();
							        				    				 }
							        				    			 }
						        				    				 rateCount++;
						        				    			 }
						        				    		 }else{
						        				    			 if(propertyRateList.size()==rateIdCount){
						        				    				 baseAmount += accommodations.getBaseAmount();
							            				    		 extraAdultAmount+=accommodations.getExtraAdult();
							        			    				 extraChildAmount+=accommodations.getExtraChild();
						        				    			 }
						            				    	 }
											    		 }
					    			    			 }	    		 
					    				    	 }
					    				    	 else{
					    				    		 baseAmount += accommodations.getBaseAmount();
					    				    		 extraAdultAmount+=accommodations.getExtraAdult();
								    				 extraChildAmount+=accommodations.getExtraChild();
					    				    	 }
					    				    	actualBaseAmount=baseAmount;
											}
								    	}
								    	if(SmartPriceEnableCheck){
								    		 Double smartPriceAmount=0.0;
								    		 listAccommodationRoom=accommodationController.listPropertyTotalRooms(property.getPropertyId(), accommodations.getAccommodationId());
						      				 if(listAccommodationRoom.size()>0 && !listAccommodationRoom.isEmpty()){
						      					 for(AccommodationRoom roomsList:listAccommodationRoom){
						      						 roomCount=(int) roomsList.getRoomCount();
						      						 long minimum = getAvailableCount(property.getPropertyId(),(Timestamp)mapDate.get("arrivalDate"),(Timestamp)mapDate.get("departureDate"),roomCount,accommodations.getAccommodationId()); 
						      						 availableCount=(int)minimum;
						      						 dblPercentCount=(double)((roomCount-availableCount)*100)/roomCount;
						      						 totalCount=(int) Math.round(dblPercentCount);
						      						 listSmartPriceDetail=detailController.listSmartPriceDetail(totalCount);
						      						 if(listSmartPriceDetail.size()>0 && !listSmartPriceDetail.isEmpty()){
						      							 for(PmsSmartPrice smartPrice: listSmartPriceDetail){
						      								 dblSmartPricePercent=smartPrice.getAmountPercent();
						      								 dblPercentFrom=smartPrice.getPercentFrom();
						      								 dblPercentTo=smartPrice.getPercentTo();
						      								 
						      								 if(dblPercentFrom>=0 && dblPercentTo<=59){
						      									 isNegative=true;
						      								 }else if(dblPercentFrom>=60 && dblPercentTo<=99){
						      									 isPositive=true;
						      								 }
						      								 if(isNegative){
						      									smartPriceAmount=baseAmount-(baseAmount*dblSmartPricePercent/100);
						      								 }
						      								 if(isPositive){
						      									smartPriceAmount=baseAmount+(baseAmount*dblSmartPricePercent/100);
						      								 }
						      							 }
						      						 }
						      					 }
						      				 }
						      				 blnSmartPriceFlag=true;
						      				totalSmartPriceAmount+=smartPriceAmount;
								    	 }
										
								    	 countDate++;
								    }
									Iterator<String> iterPromoType=arlPromoType.iterator();
									while(iterPromoType.hasNext()){
										String strPromoType=iterPromoType.next();
										if(strPromoType.equalsIgnoreCase("F")){
											promotionName=String.valueOf(Math.round(promotionPercent))+" % OFF";
											jsonOutput += ",\"promotionId\":\"" + this.promotionId + "\"";
										}else if(strPromoType.equalsIgnoreCase("R")){
											promotionName="Book "+Math.round(dblBookRooms)+" and Get "+Math.round(dblGetRooms)+" Rooms FREE";
											jsonOutput += ",\"promotionId\":\"" + this.promotionId + "\"";
										}else if(strPromoType.equalsIgnoreCase("N")){
											promotionName="Book "+Math.round(dblBookNights)+" and Get "+Math.round(dblGetNights)+" Nights FREE";
											jsonOutput += ",\"promotionId\":\"" + this.promotionId + "\"";
										}else if(strPromoType.equalsIgnoreCase("L")){
											promotionName=String.valueOf(Math.round(promotionPercent))+" % OFF";
											jsonOutput += ",\"promotionId\":\"" + this.promotionId + "\"";
										}
									}
									arlPromoType.clear();
									
									if(promoFlag){
										if(promotionType.equalsIgnoreCase("R") || promotionType.equalsIgnoreCase("N")){
											totalAmount=promoBaseAmount;
											actualAdultAmount=promoAdultAmount;
											actualChildAmount=promoChildAmount; 
										}else if(promotionType.equalsIgnoreCase("F")){
											totalAmount=totalFlatAmount;
											if(countDate>1){
												actualAdultAmount=extraAdultAmount/countDate;
												actualChildAmount=extraChildAmount/countDate;
											}else{
												actualAdultAmount=extraAdultAmount;
												actualChildAmount=extraChildAmount;
											}
										}
										else if(promotionType.equalsIgnoreCase("L")){
											totalAmount=totalLastAmount;
											if(countDate>1){
												actualAdultAmount=extraAdultAmount/countDate;
												actualChildAmount=extraChildAmount/countDate;
											}else{
												actualAdultAmount=extraAdultAmount;
												actualChildAmount=extraChildAmount;
											}
										}
									}else{
										if(blnSmartPriceFlag){
											totalAmount=totalSmartPriceAmount;
										}else{
											totalAmount=baseAmount;
										}
										
										if(countDate>1){
											actualAdultAmount=extraAdultAmount/countDate;
											actualChildAmount=extraChildAmount/countDate;
										}else{
											actualAdultAmount=extraAdultAmount;
											actualChildAmount=extraChildAmount;
										}
									}
									
									
									totalAmount=Math.round(totalAmount);
									actualTotalAdultAmount=Math.round(actualAdultAmount);
									actualTotalChildAmount=Math.round(actualChildAmount);
									jsonOutput += ",\"accommodationName\":\"" + accommodations.getAccommodationType().toUpperCase()+ "\"";
									jsonOutput += ",\"accommodationId\":\"" + accommodations.getAccommodationId()+ "\"";
									jsonOutput += ",\"baseAmount\":\"" + + (totalAmount==0 ? accommodations.getBaseAmount() : totalAmount )+  "\"";
									jsonOutput += ",\"promotionName\":\"" + (promotionName==null? "None":promotionName)+ "\"";
									
									 String jsonReviews="";
										PropertyReviewsManager reviewController=new PropertyReviewsManager();
										List<PropertyReviews> listReviews=reviewController.listReview(property.getPropertyId());
										if(listReviews.size()>0 && !listReviews.isEmpty()){
											for(PropertyReviews reviews:listReviews){
												
												if (!jsonReviews.equalsIgnoreCase(""))
													
													jsonReviews += ",{";
												else
													jsonReviews += "{";
												
												/*jsonReviews += "\"starCount\":\"" + reviews.getStarCount().intValue()+ "\"";	
												jsonReviews += ",\"reviewCount\":\"" + reviews.getReviewCount().intValue()+ "\"";	
												Double averageReviews=reviews.getAverageReview();
												jsonReviews += ",\"averageReview\":\"" + df.format(averageReviews)+ "\"";*/
												
												jsonReviews += "\"starCount\":\"" + (reviews.getStarCount()==null?0:reviews.getStarCount().intValue())+ "\"";	
												jsonReviews += ",\"reviewCount\":\"" + (reviews.getReviewCount()==null?0:reviews.getReviewCount().intValue())+ "\"";	
												if(reviews.getAverageReview()!=null){
													Double averageReviews=reviews.getAverageReview();
													jsonReviews += ",\"averageReview\":\"" + df.format(averageReviews)+ "\"";
												}else{
													jsonReviews += ",\"averageReview\":\"" + df.format(0)+ "\"";
												}
												
												jsonReviews += ",\"tripadvisorStarCount\":\"" + (reviews.getTripadvisorStarCount()==null?0:reviews.getTripadvisorStarCount().intValue())+ "\"";	
												jsonReviews += ",\"tripadvisorReviewCount\":\"" + (reviews.getTripadvisorReviewCount()==null?0:reviews.getTripadvisorReviewCount().intValue())+ "\"";
												if(reviews.getTripadvisorAverageReview()!=null){
													Double taAverageReviews=reviews.getTripadvisorAverageReview();
													jsonReviews += ",\"tripadvisorAverageReview\":\"" + df.format(taAverageReviews)+ "\"";
												}else{
													jsonReviews += ",\"tripadvisorAverageReview\":\"" + df.format(0)+ "\"";
												}
												jsonReviews += ",\"googleReviewUrl\":\"" + (reviews.getGoogleReviewUrl()==null?"":reviews.getGoogleReviewUrl())+ "\"";	
												jsonReviews += ",\"tripadvisorReviewUrl\":\"" + (reviews.getTripadvisorReviewUrl()==null?"":reviews.getTripadvisorReviewUrl())+ "\"";	
												jsonReviews += ",\"propertyRating\":\"" + (reviews.getPropertyRating()==null?"None":reviews.getPropertyRating())+ "\"";
												jsonReviews += "}";
											}
										}else{
											if (!jsonReviews.equalsIgnoreCase(""))
												
												jsonReviews += ",{";
											else
												jsonReviews += "{";
											
											jsonReviews += "\"starCount\":\"" + 0+ "\"";	
											jsonReviews += ",\"reviewCount\":\"" + 0+ "\"";	
											jsonReviews += ",\"averageReview\":\"" + 0+ "\"";
											
											jsonReviews += ",\"tripadvisorStarCount\":\"" + 0 + "\"";	
											jsonReviews += ",\"tripadvisorReviewCount\":\"" + 0 + "\"";	
											jsonReviews += ",\"tripadvisorAverageReview\":\"" + 0+ "\"";
											jsonReviews += ",\"googleReviewUrl\":\"" + "None"+ "\"";	
											jsonReviews += ",\"tripadvisorReviewUrl\":\"" + "None"+ "\"";	
											jsonReviews += ",\"propertyRating\":\"" + "None"+ "\"";
											
											jsonReviews += "}";
										}
										
										jsonOutput += ",\"reviews\":[" + jsonReviews+ "]";
									
								}
						     
							}
							jsonOutput += "}";
							
							arllist.clear();
						}
					}
				 
			        
					response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
//					sessionMap.clear();
					

				
				
			}catch(Exception e){
				logger.error(e);
				e.printStackTrace();
			}
			return null;
		}
	 
	 public String getStatusBookingDetails() throws IOException {
			
			try {
				
				this.propertyId = (Integer) sessionMap.get("propertyId");
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				Date date = new Date();// Now use today date.
				String todayDate = sdf.format(date.getTime());
				List<Integer> arrlist=new ArrayList<Integer>();
				this.statusFlag=isStatusFlag();
				
				
				String jsonOutput = "";
				HttpServletResponse response = ServletActionContext.getResponse();
				PmsBookingManager bookingController = new PmsBookingManager();		
				response.setContentType("application/json");
				ReportManager reportController=new ReportManager();
				
				 
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += "{";
				else
					jsonOutput += "{";
				
				if(this.statusFlag){
					List<BookingDetailReport> bookingDetails=reportController.listBookingDetail(getPropertyId(), getBookingId());
					if(bookingDetails.size()>0 && !bookingDetails.isEmpty()){
						if(bookingDetails.size()>1){
							for(BookingDetailReport bookingDetailReport:bookingDetails){
								int accommId=bookingDetailReport.getAccommodationId();
								arrlist.add(accommId);
								if(arrlist.size()==bookingDetails.size()){
									DashBoard Arrival = bookingController.findArrivalCount(getPropertyId(),todayDate);
							    	jsonOutput += "\"arrival\":\"" + (Arrival == null ? 0 : (int)Arrival.getTodayArrival() ) + "\"";
							    	
							    	DashBoard departure =  bookingController.findDepartureCount(getPropertyId(),todayDate);
							    	jsonOutput += ",\"departure\":\"" + (departure == null ? 0 : (int)departure.getTodayDeparture() ) + "\"";
								}
							}
						}else{
							
							DashBoard Arrival = bookingController.findArrivalCount(getPropertyId(),todayDate);
					    	jsonOutput += "\"arrival\":\"" + (Arrival == null ? 0 : (int)Arrival.getTodayArrival() ) + "\"";
					    	
					    	DashBoard departure =  bookingController.findDepartureCount(getPropertyId(),todayDate);
					    	jsonOutput += ",\"departure\":\"" + (departure == null ? 0 : (int)departure.getTodayDeparture() ) + "\"";
					    	
						}
						
						
					}
				}else if(!this.statusFlag){
					DashBoard Arrival = bookingController.findArrivalCount(getPropertyId(),todayDate);
			    	jsonOutput += "\"arrival\":\"" + (Arrival == null ? 0 : (int)Arrival.getTodayArrival() ) + "\"";
			    	
			    	DashBoard departure =  bookingController.findDepartureCount(getPropertyId(),todayDate);
			    	jsonOutput += ",\"departure\":\"" + (departure == null ? 0 : (int)departure.getTodayDeparture() ) + "\"";
				}
				
					
					
					
				jsonOutput += "}";
				
				
				
				response.getWriter().write("{\"data\":[" + jsonOutput + "]}");

			} catch (Exception e) {
				logger.error(e);
				e.printStackTrace();
			} finally {

			}

			return null;

		}
		
		public String getViewBookedDetails(){
			try{
				
				this.propertyId = (Integer) sessionMap.get("propertyId");
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				String jsonOutput = "";
				HttpServletResponse response = ServletActionContext.getResponse();
				response.setContentType("application/json");
				
				ReportManager reportController=new ReportManager();
				SimpleDateFormat format1 = new SimpleDateFormat("dd-MMM-yyyy");
				SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
				DecimalFormat df = new DecimalFormat("###.##");
				PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += "{";
				else
					jsonOutput += "{";
				
				List<BookingDetailReport> bookingDetails=reportController.listBookingDetail(getPropertyId(), getBookingId(),getAccommodationId());
				if(bookingDetails.size()>0 && !bookingDetails.isEmpty()){
					for(BookingDetailReport bookedDetails:bookingDetails){
						
						jsonOutput += "\"bookingId\":\"" + bookedDetails.getBookingId()+ "\"";
						
						Timestamp FromDate=bookedDetails.getArrivalDate();
						Timestamp ToDate=bookedDetails.getDepartureDate();
						
						String strFromDate=format2.format(FromDate);
						String strToDate=format2.format(ToDate);
						
				    	DateTime startdate = DateTime.parse(strFromDate);
				        DateTime enddate = DateTime.parse(strToDate);
				        java.util.Date fromdate = startdate.toDate();
				 		java.util.Date todate = enddate.toDate();
				 		
						int diffInDays = (int) ((todate.getTime() - fromdate.getTime()) / (1000 * 60 * 60 * 24));
						
						long lngrooms=bookedDetails.getRoomCount();
						int roomCount=(int)lngrooms;
						
						jsonOutput += ",\"rooms\":\"" + roomCount/diffInDays + "\"";
						jsonOutput += ",\"statusId\":\"" +bookedDetails.getStatusId()+ "\"";
						
						String strFirstName="",firstNameLetterUpperCase="";
						String strLastName="",lastNameLetterUpperCase="";
						strFirstName=bookedDetails.getFirstName();
						strLastName=bookedDetails.getLastName();
						if(strFirstName!=null && strFirstName!="" && !strFirstName.equalsIgnoreCase("undefined")){
							strFirstName=strFirstName.trim();
							firstNameLetterUpperCase=strFirstName.substring(0, 1).toUpperCase()+strFirstName.substring(1);
							strFirstName=firstNameLetterUpperCase;
						}else{
							strFirstName="-";
						}
						if(strLastName!=null && strLastName!=""  && !strLastName.equalsIgnoreCase("undefined")){
							strLastName=strLastName.trim();
							lastNameLetterUpperCase=strLastName.substring(0, 1).toUpperCase() +  strLastName.substring(1);
							strLastName=lastNameLetterUpperCase;
						}else{
							strLastName="";
						}
						
						jsonOutput += ",\"guestName\":\"" + strFirstName+" "+strLastName + "\"";
						
						Integer sourceId=0;
						sourceId=bookedDetails.getSourceId();
						
						long arrivalDate =bookedDetails.getArrivalDate().getTime();
						java.util.Date dateArrival = new java.util.Date(arrivalDate);
						String strArrivalDate=format1.format(dateArrival); 
						
						jsonOutput += ",\"arrivalDate\":\"" + strArrivalDate+ "\"";
						
						long departureDate =bookedDetails.getDepartureDate().getTime();
						java.util.Date dateDeparture = new java.util.Date(departureDate);
						String strDepartureDate=format1.format(dateDeparture); 
						
						jsonOutput += ",\"departureDate\":\"" + strDepartureDate+ "\"";
						
						String strAccommodationType=null;
						int accommId=bookedDetails.getAccommodationId();
						jsonOutput += ",\"accommodationId\":\" "+bookedDetails.getAccommodationId()+"\"";
						if(accommId>0)
						{
							PropertyAccommodation accommodations=accommodationController.find(accommId);
							strAccommodationType=accommodations.getAccommodationType();
							jsonOutput += ",\"accommodationType\":\" "+strAccommodationType.toUpperCase()+"\"";
						}
						
						Double dblBaseAmount=0.0,dblTaxAmount=0.0,dblTotalAmount=0.0,dblAdvanceAmount=0.0,dblDueAmount=0.0;
						dblBaseAmount=bookedDetails.getAmount();
						dblTaxAmount=bookedDetails.getTaxAmount();
						dblAdvanceAmount=bookedDetails.getAdvanceAmount();
						
						jsonOutput += ",\"tariffAmount\":\" "+df.format(dblBaseAmount)+"\"";
						jsonOutput += ",\"taxAmount\":\" "+df.format(dblTaxAmount)+"\"";
						
						dblTotalAmount=dblBaseAmount+dblTaxAmount;
						
						if(dblAdvanceAmount!=null  || dblAdvanceAmount>=0.0){
							dblAdvanceAmount=bookedDetails.getAdvanceAmount();
						}else{
							if(sourceId==1){
								dblAdvanceAmount=dblTotalAmount;
							}else{
								dblAdvanceAmount=0.0;
							}
						}
						
						boolean compareValues=dblTotalAmount.equals(dblAdvanceAmount);
						
						if(dblAdvanceAmount!=null || dblAdvanceAmount>=0.0){
							if(compareValues){
								jsonOutput += ",\"paymentRemarks\":\" CASH PAID\"";
							}else{
								jsonOutput += ",\"paymentRemarks\":\" PAY AT HOTEL\"";
							}
						}else{
							if(sourceId==1){
								jsonOutput += ",\"paymentRemarks\":\" CASH PAID \"";
							}else{
								if(compareValues){
									jsonOutput += ",\"paymentRemarks\":\" CASH PAID\"";
								}else{
									jsonOutput += ",\"paymentRemarks\":\" PAY AT HOTEL\"";
								}
							}
						}
						
						
						
						jsonOutput += ",\"totalAmount\":\" "+df.format(dblTotalAmount)+"\"";
						jsonOutput += ",\"advanceAmount\":\" "+df.format(dblAdvanceAmount)+"\"";
						
						dblDueAmount=dblTotalAmount-dblAdvanceAmount;
						jsonOutput += ",\"dueAmount\":\" "+df.format(dblDueAmount)+"\"";
						String strSplRequest=bookedDetails.getSpecialRequest();
						
						if(strSplRequest!=null){
							jsonOutput += ",\"specialRequest\":\" "+strSplRequest.trim()+"\"";
						}else{
							strSplRequest="None";
							jsonOutput += ",\"specialRequest\":\" "+strSplRequest.trim()+"\"";
						}
						
						
						jsonOutput += "}";
					}
				}
				
//				jsonOutput += "}";
				
				
				
				response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
				
				
				
			}catch(Exception e){
				logger.error(e);
				e.printStackTrace();
			} finally {

			}
			return null;
		}
	
		
		public String getSelectedPropertyApi(){
			   String output=null;
			   try{
				   Algorithm algorithm = Algorithm.HMAC256("secret");
					long nowMillis = System.currentTimeMillis();
				    Date now = new Date(nowMillis+15*60*1000);
				   int count=0;
				   	DateFormat format = new SimpleDateFormat("MMM d,yyyy", Locale.ENGLISH);
				    java.util.Date dateStart = format.parse(getStartDate());
				    java.util.Date dateEnd = format.parse(getEndDate());
				   
				    DecimalFormat df = new DecimalFormat("###.##");
				    DateFormat f = new SimpleDateFormat("EEEE");
				    
				    Calendar calCheckStart=Calendar.getInstance();
				    calCheckStart.setTime(dateStart);
				    java.util.Date checkInDate = calCheckStart.getTime();
				    String strCheckInDate=new SimpleDateFormat("yyyy-MM-dd").format(checkInDate.getTime());
				    
				    this.arrivalDate=new Timestamp(checkInDate.getTime());
				   if(getSourceTypeId()==null){
					   this.sourceTypeId=1;
				   }
				    
				    Calendar calCheckEnd=Calendar.getInstance();
				    calCheckEnd.setTime(dateEnd);
				    java.util.Date checkOutDate = calCheckEnd.getTime();
				    this.departureDate=new Timestamp(checkOutDate.getTime());
					
					Map mapDate=new HashMap();  
					mapDate.put("arrivalDate", arrivalDate);
					mapDate.put("departureDate", departureDate); 
				      
					this.propertyId = getPropertyId();
					
//					String checkIn = new SimpleDateFormat("MMM d,yyyy").format(getArrivalDate());
//				    String checkOut = new SimpleDateFormat("MMM d,yyyy").format(getDepartureDate());
					 
					
				    Timestamp timestamp = new Timestamp(System.currentTimeMillis());
				   
					Calendar cal = Calendar.getInstance();
					cal.setTime(this.departureDate);
			        cal.add(Calendar.DATE, -1);
					String startDate = new SimpleDateFormat("yyyy-MM-dd").format(this.arrivalDate);
					String endDate = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
					DateTime start = DateTime.parse(startDate);
			        DateTime end = DateTime.parse(endDate);
			        java.util.Date arrival = start.toDate();
					java.util.Date departure = end.toDate();
					PropertyAccommodationInventoryManager inventoryController= new PropertyAccommodationInventoryManager();
					String iconPath  = "ulowebsite/images/icons/";
					
					String jsonOutput = "";
					HttpServletResponse response = ServletActionContext.getResponse();
					response.setContentType("application/json");
					java.util.Date currentdate=new java.util.Date();
	            	Calendar curDate=Calendar.getInstance();
	            	curDate.setTime(currentdate);
	            	curDate.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
	            	int bookedHours=curDate.get(Calendar.HOUR);
	            	int bookedMinute=curDate.get(Calendar.MINUTE);
	            	int bookedSecond=curDate.get(Calendar.SECOND);
	            	int AMPM=curDate.get(Calendar.AM_PM);
	            	java.util.Date cudate=curDate.getTime();
	            	String strBookingDate=new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH).format(curDate.getTime());
	            	String hours="",minutes="",seconds="",timeHours="";
	            	if(bookedHours<10){
	            		hours=String.valueOf(bookedHours);
	            		hours="0"+hours;
	            	}else{
	            		hours=String.valueOf(bookedHours);
	            	}
	            	if(bookedMinute<10){
	            		minutes=String.valueOf(bookedMinute);
	            		minutes="0"+minutes;
	            	}else{
	            		minutes=String.valueOf(bookedMinute);
	            	}
	            	
	            	if(bookedMinute<10){
	            		seconds=String.valueOf(bookedSecond);
	            		seconds="0"+seconds;
	            	}else{
	            		seconds=String.valueOf(bookedSecond);
	            	}
	            	
	            	if(AMPM==0){//If the current time is AM
	            		timeHours=strBookingDate+" "+hours+" : "+minutes+" : "+seconds+" AM ";
	            	}else if(AMPM==1){//If the current time is PM
	            		timeHours=strBookingDate+" "+hours+" : "+minutes+" : "+seconds+" PM ";
	            	}
	            	
	            	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	            	
	            	String checkStartDate = new SimpleDateFormat("yyyy-MM-dd").format(currentdate);
	    	    	String strStartTime=checkStartDate+" 12:00:00"; 
	    	   	    java.util.Date dteStartDate = sdf.parse(strStartTime);
	    	   	    Calendar calStart=Calendar.getInstance();
	    	   	    calStart.setTime(dteStartDate);
	    	   	    calStart.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
	    	   	    String StartDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calStart.getTime());
	    	    	java.util.Date lastFirst = sdf.parse(StartDate);
	    	    	String[] weekdays={"Monday","Tuesday","Wednesday","Thursday","Sunday"};
	    		    String[] weekend={"Friday","Saturday"};
					AccommodationAmenityManager accommodationAmenityController = new AccommodationAmenityManager();
					PmsPropertyManager propertyController = new PmsPropertyManager();		
					PropertyAccommodationManager accommodationManager = new PropertyAccommodationManager();
					PropertyAmenityManager amenityController = new PropertyAmenityManager();
					List<PropertyAmenity> propertyAmenityList;
					PropertyTypeManager typeController =  new PropertyTypeManager();
					PropertyRateManager rateController = new PropertyRateManager();
					PropertyRateDetailManager detailController = new PropertyRateDetailManager();
					PmsAmenityManager ameController = new PmsAmenityManager();
					PromotionManager promotionController=new PromotionManager();
					PromotionDetailManager promotionDetailController=new PromotionDetailManager();
					PmsBookingManager bookingController=new PmsBookingManager();
					PropertyLandmarkManager propertyLandmarkController=new PropertyLandmarkManager();
					
					HashMap<Double,Integer > mapAmountAccommId=new HashMap<Double, Integer>();
					HashMap<Double,Integer > mapSoldOutAmountAccommId=new HashMap<Double, Integer>();
					
					ArrayList<Integer> arlSortPropertyId=new ArrayList<Integer>();
					HashMap<Integer, Integer> mapAvailablePropertyId=new HashMap<Integer, Integer>();
					
			    	List<AccommodationRoom> listAccommodationRoom=null;
					PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
					PropertyReviewsManager reviewController=new PropertyReviewsManager();
				    int availRoomCount=0,soldRoomCount=0;
				    double commissionrange1=150,commissionrange2=225,commissionrange3=300,lastdiscountpercent1=10,lastdiscountpercent2=15,lastdiscountpercent3=20;
				    SeoUrlAction urlcheck=new SeoUrlAction();
				    /*String id=urlcheck.getSearchPropertyIdByUrl(getLocationUrl());
				    if(id.equals("NA")){
				    	return null;
				    }*/
				    GooglePlaceManager areaController = new GooglePlaceManager();
				    
				    this.propertyList=propertyController.list(getPropertyId());
				    if(this.propertyList.size()>0 && !this.propertyList.isEmpty()){
				    	for(PmsProperty property:this.propertyList){

							
							
				  			
				  			if (!jsonOutput.equalsIgnoreCase(""))
				  				jsonOutput += ",{";
				  			else
				  				jsonOutput += "{";
				  			jsonOutput += "\"propertyId\":\"" + property.getPropertyId()+ "\"";
				  			jsonOutput += ",\"city\":\"" + property.getCity() + "\"";
				  			GoogleArea area = areaController.findArea(this.areaId);
				  			jsonOutput += ",\"areaName\":\"" + (area.getGoogleAreaDisplayName() == null ? "": area.getGoogleAreaDisplayName())+ "\"";
				  			jsonOutput += ",\"address1\":\"" + property.getAddress1() + "\"";
				  			jsonOutput += ",\"address2\":\"" + property.getAddress2() + "\"";
				  			jsonOutput += ",\"propertyName\":\"" + (property.getPropertyName() == null ? "": property.getPropertyName().toUpperCase().trim())+ "\"";
				  			jsonOutput += ",\"paynowDiscount\":\"" + (property.getPaynowDiscount() == null ? "": property.getPaynowDiscount())+ "\"";
				  			jsonOutput += ",\"propertyUrl\":\"" + (property.getPropertyUrl() == null ? "": property.getPropertyUrl().toLowerCase().trim())+ "\"";
				  			if(property.getLocation()!=null){
								jsonOutput += ",\"locationName\":\"" + (property.getGoogleLocation().getGoogleLocationDisplayName() == null ? "": property.getGoogleLocation().getGoogleLocationDisplayName().toUpperCase().trim())+ "\"";	
							}else{
								jsonOutput += ",\"locationName\":\"" + "" + "\"";
							}
				  			jsonOutput += ",\"payAtHotelStatus\":\"" + (property.getPayAtHotelStatus() == null ? false: property.getPayAtHotelStatus())+ "\"";
							jsonOutput += ",\"coupleStatus\":\"" + ( property.getCoupleIsActive() == null? false :  property.getCoupleIsActive() )+ "\"";
							jsonOutput += ",\"breakfastStatus\":\"" + ( property.getBreakfastIsActive() == null? false:  property.getBreakfastIsActive() )+ "\"";
							jsonOutput += ",\"hotelStatus\":\"" + ( property.getHotelIsActive() == null? false:  property.getHotelIsActive() )+ "\"";
							jsonOutput += ",\"latitude\":\"" + ( property.getLatitude() == null? "":  property.getLatitude() )+ "\"";
							jsonOutput += ",\"longitude\":\"" + ( property.getLongitude() == null? false:  property.getLongitude() )+ "\"";
							
				  			SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
				  			Timestamp FromDate=(Timestamp)mapDate.get("arrivalDate");
				  			Timestamp ToDate=(Timestamp)mapDate.get("departureDate");
				  			jsonOutput += ",\"arrivalDate\":\"" + format2.format(FromDate) + "\"";
				  			jsonOutput += ",\"departureDate\":\"" + format2.format(ToDate)+ "\"";
				  			
				  			jsonOutput += ",\"startDate\":\"" + format.format(FromDate) + "\"";
				  			jsonOutput += ",\"endDate\":\"" + format.format(ToDate)+ "\"";
				  			
				  			String strFromDate=format2.format(FromDate);
				  			String strToDate=format2.format(ToDate);
				  	    	DateTime startdate = DateTime.parse(strFromDate);
				  	        DateTime enddate = DateTime.parse(strToDate);
				  	        java.util.Date fromdate = startdate.toDate();
				  	 		java.util.Date todate = enddate.toDate();
				  	 		 
				  			int diffInDays = (int) ((todate.getTime() - fromdate.getTime()) / (1000 * 60 * 60 * 24));
				  			
				  			List arllist=new ArrayList();
				  			List arllistAvl=new ArrayList();
				  			List arllistSold=new ArrayList();
				  			jsonOutput += ",\"diffDays\":\"" + diffInDays+ "\"";
				  			
				          
				  			String jsonAmenities ="";
				  			propertyAmenityList =  amenityController.list(property.getPropertyId());
				  			if(propertyAmenityList.size()>0)
				  			{
				  				for (PropertyAmenity amenity : propertyAmenityList) {
				  					if (!jsonAmenities.equalsIgnoreCase(""))
				  						
				  						jsonAmenities += ",{";
				  					else
				  						jsonAmenities += "{";
				  					PropertyAmenity amentity1 = amenityController.find(amenity.getPropertyAmenityId());
				  					String path = "contents/images/icons/";
				  					//String path = "ulowebsite/images/icons/";
				  					PmsAmenity pmsAmenity1 =ameController.find(amentity1.getPmsAmenity().getAmenityId());
				  					jsonAmenities += "\"DT_RowId\":\"" + amenity.getPropertyAmenityId()+ "\"";					
				  					jsonAmenities += ",\"amenityName\":\"" + pmsAmenity1.getAmenityName()+ "\"";
				  					jsonAmenities += ",\"amenityId\":\"" + pmsAmenity1.getAmenityId()+ "\"";
				                    jsonAmenities += ",\"icon\":\"" + pmsAmenity1.getAmenityIcon()+ "\"";
				               
				  					
				  					jsonAmenities += "}";
				  				}
				  				
				  				jsonOutput += ",\"amenities\":[" + jsonAmenities+ "]";
				  			}
				  			
							
									
							String jsonLandmark="";
							
							List<PropertyLandmark> listPropertyLandmark=propertyLandmarkController.list(property.getPropertyId());
							if(listPropertyLandmark.size()>0 && !listPropertyLandmark.isEmpty()){
								for(PropertyLandmark landmark:listPropertyLandmark){
									if(!jsonLandmark.equalsIgnoreCase(""))
										jsonLandmark+=",{";
									else
										jsonLandmark+="{";
									
										jsonLandmark += "\"propertyLandmarkId\":\"" + (landmark.getPropertyLandmarkId() == 0 ? "":landmark.getPropertyLandmarkId()) + "\"";
									/*	jsonLandmark += ",\"landmarkKilometers1\":\"" + (landmark.getKilometers1() == 0? "" :landmark.getKilometers1())+ "\"";
										jsonLandmark += ",\"landmarkKilometers2\":\"" + (landmark.getKilometers2() == 0? "":landmark.getKilometers2())+ "\"";
										jsonLandmark += ",\"landmarkKilometers3\":\"" + (landmark.getKilometers3() == 0? "":landmark.getKilometers3())+ "\"";
										jsonLandmark += ",\"landmarkKilometers4\":\"" + (landmark.getKilometers4() == 0? "":landmark.getKilometers4())+ "\"";
										jsonLandmark += ",\"landmarkKilometers5\":\"" + (landmark.getKilometers5() == 0? "":landmark.getKilometers5())+ "\"";
												
										jsonLandmark += ",\"attractionKilometers1\":\"" + (landmark.getDistance1() == null?"":landmark.getDistance1())+ "\"";
										jsonLandmark += ",\"attractionKilometers2\":\"" + (landmark.getDistance2() == null?"":landmark.getDistance2())+ "\"";
										jsonLandmark += ",\"attractionKilometers3\":\"" + (landmark.getDistance3() == null?"":landmark.getDistance3())+ "\"";
										jsonLandmark += ",\"attractionKilometers4\":\"" + (landmark.getDistance4() == null?"":landmark.getDistance4())+ "\"";
										jsonLandmark += ",\"attractionKilometers5\":\"" + (landmark.getDistance5() == null?"":landmark.getDistance5())+ "\"";
										jsonLandmark += ",\"landmarkLine1\":\"" + (landmark.getPropertyLandmark1() == null?"":landmark.getPropertyLandmark1())+ "\"";
										jsonLandmark += ",\"landmarkLine2\":\"" + (landmark.getPropertyLandmark2() == null?"":landmark.getPropertyLandmark2())+ "\"";
										jsonLandmark += ",\"landmarkLine3\":\"" + (landmark.getPropertyLandmark3() == null?"":landmark.getPropertyLandmark3())+ "\"";
										jsonLandmark += ",\"landmarkLine4\":\"" + (landmark.getPropertyLandmark4() == null?"":landmark.getPropertyLandmark4())+ "\"";
										jsonLandmark += ",\"landmarkLine5\":\"" + (landmark.getPropertyLandmark5() == null?"":landmark.getPropertyLandmark5())+ "\"";
										*/		
										jsonLandmark += ",\"attraction1\":\"" + (landmark.getPropertyAttraction1() == null?"":landmark.getPropertyAttraction1())+ "\"";
										jsonLandmark += ",\"attraction2\":\"" + (landmark.getPropertyAttraction2() == null?"":landmark.getPropertyAttraction2())+ "\"";
										jsonLandmark += ",\"attraction3\":\"" + (landmark.getPropertyAttraction3() == null?"":landmark.getPropertyAttraction3())+ "\"";
										jsonLandmark += ",\"attraction4\":\"" + (landmark.getPropertyAttraction4() == null?"":landmark.getPropertyAttraction4())+ "\"";
										jsonLandmark += ",\"attraction5\":\"" + (landmark.getPropertyAttraction5() == null?"":landmark.getPropertyAttraction5())+ "\"";
									
									jsonLandmark+="}";
								}
							}
							jsonOutput += ",\"attractions\":[" + jsonLandmark+ "]";
				  			String jsonAccommodations ="";
				  			List<PropertyAccommodation> accommodationsList =  accommodationManager.list(property.getPropertyId());
				  			int soldOutCount=0,availCount=0,promotionAccommId=0;
				  			if(accommodationsList.size()>0 && !accommodationsList.isEmpty())
				  			{
				  				for (PropertyAccommodation accommodation : accommodationsList) {
				  					int roomCount=0;
				  					listAccommodationRoom=accommodationController.listPropertyTotalRooms(property.getPropertyId(), accommodation.getAccommodationId());
				  					if(listAccommodationRoom.size()>0 && !listAccommodationRoom.isEmpty()){
				  						for(AccommodationRoom roomsList:listAccommodationRoom){
				  							 roomCount=(int) roomsList.getRoomCount();
				  							 long minimum = getAvailableCount(property.getPropertyId(),getArrivalDate(),getDepartureDate(),roomCount,accommodation.getAccommodationId());
				  							 if((int)minimum>0){
				  								 availCount++;
				  							 }else if((int)minimum==0){
				  								 soldOutCount++;
				  							 }
				  							 break;
				  						 }
				  					 }
				  					
				  					
				  					if (!jsonAccommodations.equalsIgnoreCase(""))
				  						
				  						jsonAccommodations += ",{";
				  					else
				  						jsonAccommodations += "{";
				  						jsonAccommodations += "\"accommodationId\":\"" + accommodation.getAccommodationId()+ "\"";
				  						jsonAccommodations += ",\"accommodationName\":\"" + accommodation.getAccommodationType().toUpperCase()+ "\"";
				  					
				  					jsonAccommodations += "}";
				  				}
				  				
				  				jsonOutput += ",\"jsonAccommodations\":[" + jsonAccommodations+ "]";
				  			}

				  			
				  			List<PmsPromotionDetails> listPromotionDetailCheck=null;
				  			Boolean blnAvailable=false,blnSoldout=false,firstPromotionFlag=false,secondPromotionFlag=false,promotionFlag=false,blnPromoFlag=false;
				  			int arlListTotalAccomm=0,soldOutTotalAccomm=0,accommCount=0;
				  			ArrayList<String> arlListPromoType=new ArrayList<String>();
				  			
				  			String promotionType="NA",discountType="NA",promotionFirstName="NA",promotionSecondName="NA";
							double dblDiscountINR=0,dblDiscountPercent=0;
							boolean promoStatus=false,isFlat=false,isEarly=false,isLast=false,isSoldout=false;
							List<PmsPromotions> listPromotions=promotionController.listDatePromotions(property.getPropertyId(), dateStart);
							 if(listPromotions.size()>0 && !listPromotions.isEmpty()){
							   	for(PmsPromotions promotion:listPromotions){
							   		if(promotion.getPromotionType().equals("F")){
							   			promotionType=promotion.getPromotionType();
							   			promoStatus=true;
							   			isFlat=true;
							   		}else if(promotion.getPromotionType().equals("E")){
							   			promotionType=promotion.getPromotionType();
							   			Timestamp tsStartPromo=promotion.getFirstRangeStartDate();
							   			Timestamp tsEndPromo=promotion.getFirstRangeEndDate();
							   			String startStayDate = new SimpleDateFormat("yyyy-MM-dd").format(tsStartPromo);
										String endStayDate = new SimpleDateFormat("yyyy-MM-dd").format(tsEndPromo);
										DateTime dtstart = DateTime.parse(startStayDate);
									    DateTime dtend = DateTime.parse(endStayDate);
							   			List<DateTime> between = DateUtil.getDateRange(dtstart, dtend);
							   			if(between.size()>0 && !between.isEmpty()){
							   				for(DateTime d:between){
							   					if(cudate.equals(d.toDate())){
							   						promoStatus=true;
							   						isEarly=true;
							   					}
							   				}
							   			}else{
							   				promoStatus=false;
							   			}
							   		}
							   		
									List<PmsPromotionDetails> listPromoDetails=promotionDetailController.listPromotionDetails(promotion.getPromotionId(),f.format(dateStart).toLowerCase());
									if(listPromoDetails.size()>0 && !listPromoDetails.isEmpty()){
										for(PmsPromotionDetails promodetails:listPromoDetails){
											dblDiscountINR=promodetails.getPromotionInr();
											if(dblDiscountINR>0){
												discountType="INR";
											}
											dblDiscountPercent=promodetails.getPromotionPercentage();
											if(dblDiscountPercent>0){
												discountType="PERCENT";
											}
										}
									}
								}
							 }
							String jsonAccommodation=""; 
				  			List<PropertyAccommodation> accommodationList =  accommodationManager.list(property.getPropertyId());
				  			if(accommodationList.size()>0)
				  			{
				  				for (PropertyAccommodation accommodation : accommodationList) {
				  					
				  					if (!jsonAccommodation.equalsIgnoreCase(""))
				  						
				  						jsonAccommodation += ",{";
				  					else
				  						jsonAccommodation += "{";

				  					int dateCount=0;
				  					double amount=0.0,extraAdultAmount=0.0,extraChildAmount=0.0,totalMaximumAmount=0.0,totalMinimumAmount=0.0,diffAmount=0;
				  					int roomCount=0,availableCount=0,totalCount=0;
				  				    double dblPercentCount=0.0;
				  	    			listAccommodationRoom=accommodationController.listPropertyTotalRooms(property.getPropertyId(), accommodation.getAccommodationId());
				  					 
				  					if(listAccommodationRoom.size()>0 && !listAccommodationRoom.isEmpty()){
				  						
				  						for(AccommodationRoom roomsList:listAccommodationRoom){
				  							roomCount=(int) roomsList.getRoomCount();
				  							if(roomCount>0){
				  								long minimum = getAvailableCount(property.getPropertyId(),getArrivalDate(),getDepartureDate(),roomCount,accommodation.getAccommodationId());
				  								if((int)minimum>0){
				  									blnAvailable=true;
				  									blnSoldout=false;
				  									arlListTotalAccomm++;
				  								}else if((int)minimum==0){
				  									blnSoldout=true;
				  									blnAvailable=false;
				  									soldOutTotalAccomm++;
				  								}
				  							}
				  							 break;
				  						 }
				  					 }
				  					 List<DateTime> between = DateUtil.getDateRange(start, end);
				  					 
				  					double sellrate=0,minAmount=0,maxAmount=0,promoAmount=0,firstPromoAmount=0,secondPromoAmount=0,firstInr=0,secondInr=0;
				  					
				  				     for (DateTime d : between)
				  				     {

								    	 long rmc = 0;
							   			 this.roomCnt = rmc;
							        	 int sold=0;
							        	 java.util.Date availDate = d.toDate();
							        	 String days=f.format(d.toDate()).toLowerCase();
										 PropertyAccommodationInventory inventoryCount = inventoryController.findInventoryCount(availDate,accommodation.getAccommodationId());
										 if(inventoryCount != null){
										 PmsAvailableRooms roomCount1 = bookingController.findBookingCount(availDate, accommodation.getAccommodationId(),inventoryCount.getInventoryId());
										 if(roomCount1.getRoomCount() == null) {
										 	String num1 = inventoryCount.getInventoryCount();
											PmsAvailableRooms bookingRoomCount=bookingController.findCount(availDate, accommodation.getAccommodationId());
											PropertyAccommodation accomm=accommodationController.find(accommodation.getAccommodationId());
											Integer totavailable=0;
											if(accommodation!=null){
												totavailable=accommodation.getNoOfUnits();
											}
											if(bookingRoomCount.getRoomCount()!=null){
												long lngRooms=bookingRoomCount.getRoomCount();
												long num2 = Long.parseLong(num1);
												this.roomCnt = num2;
												sold=(int) (lngRooms);
												int availRooms=0;
												availRooms=totavailable-sold;
//												if(Integer.parseInt(num1)>availRooms){
//													this.roomCnt = availRooms;	
//												}else{
													this.roomCnt = num2;
//												}
														
											}else{
												 long num2 = Long.parseLong(num1);
												 this.roomCnt = num2;
												 sold=(int) (rmc);
											}
										}else{		
											PmsAvailableRooms bookingRoomCount=bookingController.findCount(availDate, accommodation.getAccommodationId());
											int intRooms=0;
											Integer totavailable=0,availableRooms=0;
											if(accommodation!=null){
												totavailable=accommodation.getNoOfUnits();
											}
											if(bookingRoomCount.getRoomCount()!=null){
												long lngRooms=bookingRoomCount.getRoomCount();
												long num = roomCount1.getRoomCount();	
												intRooms=(int)lngRooms;
												String num1 = inventoryCount.getInventoryCount();
							 					long num2 = Long.parseLong(num1);
							 					availableRooms=totavailable-intRooms;
//							 					if(num2>availableRooms){
//							 						this.roomCnt = availableRooms;	 
//							 					}else{
							 						this.roomCnt = num2-num;
//							 					}
							 					long lngSold=bookingRoomCount.getRoomCount();
							   					sold=(int)lngSold;
											}else{
												long num = roomCount1.getRoomCount();	
												String num1 = inventoryCount.getInventoryCount();
												long num2 = Long.parseLong(num1);
							  					this.roomCnt = (num2-num);
							  					long lngSold=roomCount1.getRoomCount();
							   					sold=(int)lngSold;
											}
						
										}
														
									}else{
										  PmsAvailableRooms inventoryCounts = bookingController.findCount(availDate, accommodation.getAccommodationId());							
											if(inventoryCounts.getRoomCount() == null){								
												this.roomCnt = (accommodation.getNoOfUnits()-rmc);
												sold=(int)rmc;
											}
											else{
												this.roomCnt = (accommodation.getNoOfUnits()-inventoryCounts.getRoomCount());	
												long lngSold=inventoryCounts.getRoomCount();
					   							sold=(int)lngSold;
											}
									}
														
									double totalRooms=0,intSold=0;
									totalRooms=(double)accommodation.getNoOfUnits();
									intSold=(double)sold;
									double occupancy=0.0;
									occupancy=intSold/totalRooms;
									Integer occupancyRating=(int) Math.round(occupancy * 100);
									boolean isWeekdays=false,isWeekend=false;
									for(int a=0;a<weekdays.length;a++){
										if(days.equalsIgnoreCase(weekdays[a])){
											isWeekdays=true;
										}
									}
									
									for(int b=0;b<weekend.length;b++){
										if(days.equalsIgnoreCase(weekend[b])){
											isWeekend=true;
										}
									}
									if(property.getLocationType()!=null){
										if(property.getLocationType().getLocationTypeName().equalsIgnoreCase("Leisure")){
											if(isWeekdays){
												if(accommodation.getWeekdaySellingRate()==null){
													maxAmount=0;	
												}else{
													maxAmount=accommodation.getWeekdaySellingRate();
												}
													
											}
											
											if(isWeekend){
												if(accommodation.getWeekendSellingRate()==null){
													maxAmount=0;
												}else{
													maxAmount=accommodation.getWeekendSellingRate();	
												}
												
											}	
										}
										
										if(property.getLocationType().getLocationTypeName().equalsIgnoreCase("Metro")){
											if(accommodation.getSellingRate()==null){
												maxAmount=0;
											}else{
												maxAmount=accommodation.getSellingRate();	
											}
												
										}	
										
										if(occupancyRating>=0 && occupancyRating<=30){
											sellrate=maxAmount;
										}else if(occupancyRating>30 && occupancyRating<=60){
											sellrate=maxAmount+(maxAmount*10/100);
										}else if(occupancyRating>=60 && occupancyRating<=80){
											sellrate=maxAmount+(maxAmount*15/100);
										}else if(occupancyRating>80){
											sellrate=maxAmount+(maxAmount*20/100);
										}
									}
				    				
										
										if(discountType.equals("INR")){
											firstPromoAmount=dblDiscountINR;
											firstInr+=dblDiscountINR;
										}else if(discountType.equals("PERCENT")){
											firstPromoAmount=(sellrate*dblDiscountPercent/100);
										}else{
											firstPromoAmount=0;
										}
										
										if(checkStartDate.equals(strCheckInDate)){ 
											if(occupancyRating>=0 && occupancyRating<=30){
												if(this.roomCnt>0){
													isLast=true;
												}else{
													isLast=false;
												}
											}
										}
										
					    				if(isLast){
					    					if(occupancyRating>=0 && occupancyRating<=30){
					    						if(AMPM==0){//If the current time is AM
					    		            		if(bookedHours>=0 && bookedHours<=11){
					    		            			if(maxAmount<1000){
					    									if(occupancyRating>=0 && occupancyRating<=30){
					    										secondPromoAmount=commissionrange1;
					    										secondInr+=commissionrange1;
					    									}
					    							    }else if(maxAmount>=1000 && maxAmount<=1500){
					    							    	if(occupancyRating>=0 && occupancyRating<=30){
					    							    		secondPromoAmount=commissionrange2;
					    							    		secondInr+=commissionrange2;
					    									}
					    							    }else if(maxAmount>1500){
					    							    	if(occupancyRating>=0 && occupancyRating<=30){
					    							    		secondPromoAmount=commissionrange3;
					    							    		secondInr+=commissionrange3;
					    									}
					    							    }
					    		            		}
					    		            	}
					    						if(AMPM==1){
					    							if(bookedHours>=0 && bookedHours<=3){
					    								if(occupancyRating>=0 &&occupancyRating<30){
					    									secondPromoAmount=(sellrate*lastdiscountpercent1/100);	
					    								}
					    								
					    		            		}else if(bookedHours>=4 && bookedHours<=7){
					    		            			if(occupancyRating>=0 &&occupancyRating<30){
					    		            				secondPromoAmount=(sellrate*lastdiscountpercent2/100);	
					    								}
					    		            			
					    		            		}else if(bookedHours>=8 && bookedHours<=11){
					    		            			if(occupancyRating>=0 &&occupancyRating<30){
					    		            				secondPromoAmount=(sellrate*lastdiscountpercent3/100);	
					    								}
					    		            			
					    		            		}
					    						}
					    					}
					    				}
					    				promoAmount=firstPromoAmount+secondPromoAmount;
					    				totalMinimumAmount+=sellrate-promoAmount;
					  				    totalMaximumAmount+=sellrate;
								    	 dateCount++;
								    	extraAdultAmount+=accommodation.getExtraAdult(); 
								    	extraChildAmount+=accommodation.getExtraChild();
				  				     }
				  				     
				  				    
				  				    diffAmount=totalMaximumAmount-totalMinimumAmount;
				  				   if(isFlat){
										if(discountType.equalsIgnoreCase("INR")){
											promotionFirstName=String.valueOf(Math.round(firstInr))+" OFF";
										}else if(discountType.equalsIgnoreCase("PERCENT")){
											promotionFirstName=String.valueOf(Math.round(dblDiscountPercent))+" % OFF";
										}
									}
									if(isEarly){
										if(discountType.equalsIgnoreCase("INR")){
											promotionFirstName=String.valueOf(Math.round(firstInr))+" OFF";
										}else if(discountType.equalsIgnoreCase("PERCENT")){
											promotionFirstName=String.valueOf(Math.round(dblDiscountPercent))+" % OFF";
										}
									}
									if(isLast){

										if(AMPM==0){//If the current time is AM
			    		            		if(bookedHours>=0 && bookedHours<=11){
			    		            
			    		            			String displayDate = new SimpleDateFormat("MMM dd,yyyy HH:mm:ss").format(lastFirst.getTime());
			    								this.displayHoursForTimer=displayDate;
			    								setDisplayHoursForTimer(this.displayHoursForTimer);

			    								promotionSecondName=String.valueOf(Math.round(secondInr))+" % OFF";
			    		            		}
			    		            	}
			    						if(AMPM==1){
			    							if(bookedHours>=0 && bookedHours<=3){
			    								String firstTime=checkStartDate+" 16:00:00"; 
			    						   	    java.util.Date dteFirstDate = sdf.parse(strStartTime);
			    						   	    Calendar calFirst=Calendar.getInstance();
			    						   	    calFirst.setTime(dteFirstDate);
			    						   	    calFirst.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
			    						   	    String firstDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calFirst.getTime());
			    						    	java.util.Date dteFirst = sdf.parse(firstTime);
			    								String displayDate = new SimpleDateFormat("MMM dd,yyyy HH:mm:ss").format(dteFirst.getTime());
			    								this.displayHoursForTimer=displayDate;
			    								setDisplayHoursForTimer(this.displayHoursForTimer);
			    								
			    								promotionSecondName=String.valueOf(Math.round(lastdiscountpercent1))+" % OFF";
			    		            		}else if(bookedHours>=4 && bookedHours<=7){
			    		            			String firstTime=checkStartDate+" 20:00:00"; 
			    						   	    java.util.Date dteFirstDate = sdf.parse(strStartTime);
			    						   	    Calendar calFirst=Calendar.getInstance();
			    						   	    calFirst.setTime(dteFirstDate);
			    						   	    calFirst.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
			    						   	    String firstDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calFirst.getTime());
			    						    	java.util.Date dteFirst = sdf.parse(firstTime);
			    								String displayDate = new SimpleDateFormat("MMM dd,yyyy HH:mm:ss").format(dteFirst.getTime());
			    								this.displayHoursForTimer=displayDate;
			    								setDisplayHoursForTimer(this.displayHoursForTimer);
			    								
			    								promotionSecondName=String.valueOf(Math.round(lastdiscountpercent2))+" % OFF";
			    		            		}else if(bookedHours>=8 && bookedHours<=11){
			    		            			String firstTime=checkStartDate+" 23:00:00"; 
			    						   	    java.util.Date dteFirstDate = sdf.parse(strStartTime);
			    						   	    Calendar calFirst=Calendar.getInstance();
			    						   	    calFirst.setTime(dteFirstDate);
			    						   	    calFirst.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
			    						   	    String firstDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calFirst.getTime());
			    						    	java.util.Date dteFirst = sdf.parse(firstTime);
			    								String displayDate = new SimpleDateFormat("MMM dd,yyyy HH:mm:ss").format(dteFirst.getTime());
			    								this.displayHoursForTimer=displayDate;
			    								setDisplayHoursForTimer(this.displayHoursForTimer);
			    								
			    								promotionSecondName=String.valueOf(Math.round(lastdiscountpercent3))+" % OFF";
			    		            		}
			    						}

									
									}
				  				     
									
				  					
				  					
				  					long minimum = getAvailableCount(property.getPropertyId(),getArrivalDate(),getDepartureDate(),roomCount,accommodation.getAccommodationId());
				  			    	
				  			    	 if(minimum==0){
				  			    		jsonAccommodation += "\"available\":\"" + minimum+ "\"";
				  			    		jsonAccommodation += ",\"minimumAmount\":\"" + (totalMinimumAmount==0 ? 0 : Math.round(totalMinimumAmount) )+  "\"";
				  			    		jsonAccommodation += ",\"maximumAmount\":\""  + (totalMaximumAmount==0 ? 0 : Math.round(totalMaximumAmount) )+  "\"";
				  			    		jsonAccommodation += ",\"baseAmount\":\""  + (totalMaximumAmount==0 ? 0 : Math.round(totalMaximumAmount) )+  "\"";
				  			    		jsonAccommodation += ",\"offerAmount\":\"" +(Math.round(totalMaximumAmount-totalMinimumAmount))+  "\"";
				  			    		jsonAccommodation += ",\"difference\":\"" + (diffAmount==0 ? diffAmount  : Math.round(diffAmount) )+  "\"";
				  			    		jsonAccommodation += ",\"promotionFirstName\":\"" + (promotionFirstName==null? "None":promotionFirstName)+ "\"";
				  			    		jsonAccommodation += ",\"promotionSecondName\":\"" + (promotionSecondName==null? "None":promotionSecondName)+ "\"";
				  			    		jsonAccommodation += ",\"accommodationName\":\"" + accommodation.getAccommodationType().toUpperCase()+ "\"";
				  			    		jsonAccommodation += ",\"accommodationId\":\"" + accommodation.getAccommodationId()+ "\"";
				  			    		jsonAccommodation += ",\"noOfAdults\":\"" + accommodation.getNoOfAdults()+ "\"";
				  			    		jsonAccommodation += ",\"noOfChild\":\"" + accommodation.getNoOfChild()+ "\"";
				  			    		jsonAccommodation += ",\"minOccupancy\":\"" + accommodation.getMinOccupancy()+ "\"";
				  			    		jsonAccommodation += ",\"maxOccupancy\":\"" + accommodation.getMaxOccupancy()+ "\"";
				  			    		Double extraAdult,extraChild;
				  						double dblextraadult,dblextrachild;
				  						extraAdult=extraAdultAmount;
				  						extraChild=extraChildAmount;
				  						
				  						dblextraadult=extraAdult+extraAdult*15/100;
				  						dblextrachild=extraChild+extraChild*15/100;
				  						
				  			    		jsonAccommodation += ",\"extraAdult\":\"" + dblextraadult+ "\"";
				  			    		jsonAccommodation += ",\"extraChild\":\"" + dblextrachild+ "\"";
				  			    		jsonAccommodation += ",\"diffDays\":\"" + diffInDays + "\"";
				  			    		jsonAccommodation += ",\"rooms\":\"" + 0 + "\"";
				  			    		//PmsProperty property = propertyController.find(getPropertyId());	
				  						 if(property.getTaxIsActive() == true ){
				  							PropertyTaxeManager taxController = new PropertyTaxeManager();
				  					            double taxe =  (totalMinimumAmount==0 ? 0 : totalMinimumAmount );
				  					            double taxs = taxe/diffInDays;
				  					            PropertyTaxe tax = taxController.find(taxs);
				  							    double taxes = Math.round(tax.getTaxPercentage() / 100 * taxs) ;
				  							    jsonOutput += ",\"tax\":\"" + taxes + "\"";
				  				 			    jsonOutput += ",\"taxPercentage\":\"" + tax.getTaxPercentage() + "\"";
				  				 		    } else{
				  			 		 		    jsonOutput += ",\"tax\":\"" + 0 + "\"";
				  				 		 		jsonOutput += ",\"taxPercentage\":\"" + 0 + "\"";
				  				 		    }
				  			    		isSoldout=true;
				  			    		jsonAccommodation += ",\"isSoldout\":\"" + isSoldout+ "\"";
				  			    		jsonAccommodation += ",\"isFlat\":\"" + isFlat+ "\"";
				  			    		jsonAccommodation += ",\"isEarly\":\"" + isEarly+ "\"";
				  			    		jsonAccommodation += ",\"isLast\":\"" + isLast+ "\"";
				  				     }else{
				  				    	 isSoldout=false;
				  				    	jsonAccommodation += "\"available\":\"" + minimum+ "\""; 
				  				    	jsonAccommodation += ",\"accommodationName\":\"" + accommodation.getAccommodationType().toUpperCase()+ "\"";
				  				    	jsonAccommodation += ",\"accommodationId\":\"" + accommodation.getAccommodationId()+ "\"";
				  				    	jsonAccommodation += ",\"minimumAmount\":\"" + (totalMinimumAmount==0 ? 0 : Math.round(totalMinimumAmount) )+  "\"";
				  				    	jsonAccommodation += ",\"maximumAmount\":\"" + (totalMaximumAmount==0 ? 0 : Math.round(totalMaximumAmount) )+  "\"";
				  				    	jsonAccommodation += ",\"baseAmount\":\""  + (totalMaximumAmount==0 ? 0 : Math.round(totalMaximumAmount) )+  "\"";
				  				    	jsonAccommodation += ",\"offerAmount\":\"" +(Math.round(totalMaximumAmount-totalMinimumAmount))+  "\"";
				  				    	jsonAccommodation += ",\"difference\":\"" + (diffAmount==0 ? 0  : Math.round(diffAmount) )+  "\"";
				  				    	//jsonAccommodation += ",\"minOccupancy\":\"" + 1 + "\"";
				  				    	//jsonAccommodation += ",\"maxOccupancy\":\"" + accommodation.getMaxOccupancy() + "\"";
				  				    	jsonAccommodation += ",\"promotionFirstName\":\"" + (promotionFirstName=="NA"? "":promotionFirstName)+ "\"";
				  				    	jsonAccommodation += ",\"promotionSecondName\":\"" + (promotionSecondName=="NA"? "":promotionSecondName)+ "\"";
				  				    	jsonAccommodation += ",\"extraAdultAmount\":\"" + + (extraAdultAmount==0 ? 0 : Math.round(extraAdultAmount))+  "\"";
				  				    	jsonAccommodation += ",\"extraChildAmount\":\"" + + (extraChildAmount==0 ? 0 : Math.round(extraChildAmount))+  "\"";
				  				    	jsonAccommodation += ",\"isSoldout\":\"" + isSoldout+ "\"";
				  				    	jsonAccommodation += ",\"isFlat\":\"" + isFlat+ "\"";
				  			    		jsonAccommodation += ",\"isEarly\":\"" + isEarly+ "\"";
				  			    		jsonAccommodation += ",\"isLast\":\"" + isLast+ "\"";
				  			    		jsonAccommodation += ",\"noOfAdults\":\"" + accommodation.getNoOfAdults()+ "\"";
				  			    		jsonAccommodation += ",\"noOfChild\":\"" + accommodation.getNoOfChild()+ "\"";
				  			    		jsonAccommodation += ",\"minOccupancy\":\"" + accommodation.getMinOccupancy()+ "\"";
				  			    		jsonAccommodation += ",\"maxOccupancy\":\"" + accommodation.getMaxOccupancy()+ "\"";
				  			    		Double extraAdult,extraChild;
				  						double dblextraadult,dblextrachild;
				  						extraAdult=extraAdultAmount;
				  						extraChild=extraChildAmount;
				  						
				  						dblextraadult=extraAdult+extraAdult*15/100;
				  						dblextrachild=extraChild+extraChild*15/100;
				  			    		jsonAccommodation += ",\"extraAdult\":\"" + dblextraadult+ "\"";
				  			    		jsonAccommodation += ",\"extraChild\":\"" + dblextrachild+ "\"";
				  			    		jsonAccommodation += ",\"diffDays\":\"" + diffInDays + "\"";
				  			    		jsonAccommodation += ",\"rooms\":\"" + 1 + "\"";
				  			    		//PmsProperty property = propertyController.find(getPropertyId());	
				  						 if(property.getTaxIsActive() == true ){
				  							PropertyTaxeManager taxController = new PropertyTaxeManager();
				  					            double taxe =  (totalMinimumAmount==0 ? 0 : totalMinimumAmount );
				  					            double taxs = taxe/diffInDays;
				  					            PropertyTaxe tax = taxController.find(taxs);
				  							    double taxes = Math.round(tax.getTaxPercentage() / 100 * taxs) ;
				  							  jsonAccommodation += ",\"tax\":\"" + taxes + "\"";
				  							jsonAccommodation += ",\"taxPercentage\":\"" + tax.getTaxPercentage() + "\"";
				  				 		    } else{
				  				 		    	jsonAccommodation += ",\"tax\":\"" + 0 + "\"";
				  				 		    	jsonAccommodation += ",\"taxPercentage\":\"" + 0 + "\"";
				  				 		    }
				  			    		if(isLast){
				  			    			jsonAccommodation += ",\"promoLastMinuteTimer\":\"" + this.displayHoursForTimer+ "\"";	
										}else{
											jsonAccommodation += ",\"promoLastMinuteTimer\":\"" + "None"+ "\"";
										}
				  			    		count++;
				  			    		
				  				     }
				  			    	 
				  			    	jsonAccommodation += "}";
				  				   
				  				}
				  			}
				  			jsonOutput += ",\"accommodations\":[" + jsonAccommodation+ "]";
				  			
				  			jsonOutput += "}";
				  			
				  			arllist.clear();
				  		
						
				    	}
				    	
				    }

		 			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
		 			
			   }catch(Exception ex){
				   logger.error(ex);
				   ex.printStackTrace();
			   }
			   return null;
			
		}
		
		public String getSelectedPropertyAPI(Property properties){
			   String output=null;
			   try{
				   Algorithm algorithm = Algorithm.HMAC256("secret");
					long nowMillis = System.currentTimeMillis();
				    Date now = new Date(nowMillis+15*60*1000);
				    java.util.Date dateStart = null;
				    java.util.Date dateEnd =null;
				    java.util.Date date=new java.util.Date();
				   int count=0;
				   	DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
				   	if(properties.getCheckIn()!=null && properties.getCheckOut()!=null){
						dateStart = format.parse(properties.getCheckIn());
					    dateEnd = format.parse(properties.getCheckOut());	
					}else{
						Calendar calStart=Calendar.getInstance();
					    calStart.setTime(date);
					    dateStart=calStart.getTime();
					    
					    Calendar calEnd=Calendar.getInstance();
					    calEnd.setTime(date);
					    calEnd.add(Calendar.DATE, 1);
					    dateEnd=calEnd.getTime();
					}
				   
				    DecimalFormat df = new DecimalFormat("###.##");
				    DateFormat f = new SimpleDateFormat("EEEE");
				    
				    Calendar calCheckStart=Calendar.getInstance();
				    calCheckStart.setTime(dateStart);
				    java.util.Date checkInDate = calCheckStart.getTime();
				    String strCheckInDate=new SimpleDateFormat("yyyy-MM-dd").format(checkInDate.getTime());
				    
				    this.arrivalDate=new Timestamp(checkInDate.getTime());
				   if(getSourceTypeId()==null){
					   this.sourceTypeId=1;
				   }
				    
				    Calendar calCheckEnd=Calendar.getInstance();
				    calCheckEnd.setTime(dateEnd);
				    java.util.Date checkOutDate = calCheckEnd.getTime();
				    this.departureDate=new Timestamp(checkOutDate.getTime());
					
					Map mapDate=new HashMap();  
					mapDate.put("arrivalDate", arrivalDate);
					mapDate.put("departureDate", departureDate); 
				      
					this.propertyId = getPropertyId();
					
//					String checkIn = new SimpleDateFormat("MMM d,yyyy").format(getArrivalDate());
//				    String checkOut = new SimpleDateFormat("MMM d,yyyy").format(getDepartureDate());
					 
					
				    Timestamp timestamp = new Timestamp(System.currentTimeMillis());
				   
					Calendar cal = Calendar.getInstance();
					cal.setTime(this.departureDate);
			        cal.add(Calendar.DATE, -1);
					String startDate = new SimpleDateFormat("yyyy-MM-dd").format(this.arrivalDate);
					String endDate = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
					DateTime start = DateTime.parse(startDate);
			        DateTime end = DateTime.parse(endDate);
			        java.util.Date arrival = start.toDate();
					java.util.Date departure = end.toDate();
					PropertyAccommodationInventoryManager inventoryController= new PropertyAccommodationInventoryManager();
					String iconPath  = "ulowebsite/images/icons/";
					
					String jsonOutput = "";
					HttpServletResponse response = ServletActionContext.getResponse();
					response.setContentType("application/json");
					java.util.Date currentdate=new java.util.Date();
	            	Calendar curDate=Calendar.getInstance();
	            	curDate.setTime(currentdate);
	            	curDate.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
	            	int bookedHours=curDate.get(Calendar.HOUR);
	            	int bookedMinute=curDate.get(Calendar.MINUTE);
	            	int bookedSecond=curDate.get(Calendar.SECOND);
	            	int AMPM=curDate.get(Calendar.AM_PM);
	            	java.util.Date cudate=curDate.getTime();
	            	String strBookingDate=new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH).format(curDate.getTime());
	            	String hours="",minutes="",seconds="",timeHours="";
	            	if(bookedHours<10){
	            		hours=String.valueOf(bookedHours);
	            		hours="0"+hours;
	            	}else{
	            		hours=String.valueOf(bookedHours);
	            	}
	            	if(bookedMinute<10){
	            		minutes=String.valueOf(bookedMinute);
	            		minutes="0"+minutes;
	            	}else{
	            		minutes=String.valueOf(bookedMinute);
	            	}
	            	
	            	if(bookedMinute<10){
	            		seconds=String.valueOf(bookedSecond);
	            		seconds="0"+seconds;
	            	}else{
	            		seconds=String.valueOf(bookedSecond);
	            	}
	            	
	            	if(AMPM==0){//If the current time is AM
	            		timeHours=strBookingDate+" "+hours+" : "+minutes+" : "+seconds+" AM ";
	            	}else if(AMPM==1){//If the current time is PM
	            		timeHours=strBookingDate+" "+hours+" : "+minutes+" : "+seconds+" PM ";
	            	}
	            	
	            	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	            	
	            	String checkStartDate = new SimpleDateFormat("yyyy-MM-dd").format(currentdate);
	    	    	String strStartTime=checkStartDate+" 12:00:00"; 
	    	   	    java.util.Date dteStartDate = sdf.parse(strStartTime);
	    	   	    Calendar calStart=Calendar.getInstance();
	    	   	    calStart.setTime(dteStartDate);
	    	   	    calStart.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
	    	   	    String StartDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calStart.getTime());
	    	    	java.util.Date lastFirst = sdf.parse(StartDate);
	    	    	String[] weekdays={"Monday","Tuesday","Wednesday","Thursday","Sunday"};
	    		    String[] weekend={"Friday","Saturday"};
					AccommodationAmenityManager accommodationAmenityController = new AccommodationAmenityManager();
					PmsPropertyManager propertyController = new PmsPropertyManager();		
					PropertyAccommodationManager accommodationManager = new PropertyAccommodationManager();
					PropertyAmenityManager amenityController = new PropertyAmenityManager();
					List<PropertyAmenity> propertyAmenityList;
					List<PropertyAmenity> tagAmenityList;
					PropertyTypeManager typeController =  new PropertyTypeManager();
					PropertyRateManager rateController = new PropertyRateManager();
					PropertyRateDetailManager detailController = new PropertyRateDetailManager();
					PmsAmenityManager ameController = new PmsAmenityManager();
					PromotionManager promotionController=new PromotionManager();
					PromotionDetailManager promotionDetailController=new PromotionDetailManager();
					PmsBookingManager bookingController=new PmsBookingManager();
					PropertyLandmarkManager propertyLandmarkController=new PropertyLandmarkManager();
					
					HashMap<Double,Integer > mapAmountAccommId=new HashMap<Double, Integer>();
					HashMap<Double,Integer > mapSoldOutAmountAccommId=new HashMap<Double, Integer>();
					
					ArrayList<Integer> arlSortPropertyId=new ArrayList<Integer>();
					HashMap<Integer, Integer> mapAvailablePropertyId=new HashMap<Integer, Integer>();
					
			    	List<AccommodationRoom> listAccommodationRoom=null;
					PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
					PropertyReviewsManager reviewController=new PropertyReviewsManager();
				    int availRoomCount=0,soldRoomCount=0;
				    double commissionrange1=150,commissionrange2=225,commissionrange3=300,lastdiscountpercent1=10,lastdiscountpercent2=15,lastdiscountpercent3=20;
				    SeoUrlAction urlcheck=new SeoUrlAction();
				    String id=urlcheck.getSearchPropertyIdByUrl(properties.getPropertyUrl());
				    if(id.equals("NA")){
				    	return null;
				    }
				    
				    this.propertyList=propertyController.list(Integer.parseInt(id));
				    if(this.propertyList.size()>0 && !this.propertyList.isEmpty()){
				    	for(PmsProperty property:this.propertyList){

							
							
				  			
				  			if (!jsonOutput.equalsIgnoreCase(""))
				  				jsonOutput += ",{";
				  			else
				  				jsonOutput += "{";
				  			jsonOutput += "\"propertyId\":\"" + property.getPropertyId()+ "\"";
				  			jsonOutput += ",\"city\":\"" + property.getCity() + "\"";
				  			jsonOutput += ",\"address1\":\"" + property.getAddress1() + "\"";
				  			jsonOutput += ",\"address2\":\"" + property.getAddress2() + "\"";
				  			jsonOutput += ",\"propertyName\":\"" + (property.getPropertyName() == null ? "": property.getPropertyName().toUpperCase().trim())+ "\"";
				  			jsonOutput += ",\"paynowDiscount\":\"" + (property.getPaynowDiscount() == null ? "": property.getPaynowDiscount())+ "\"";
				  			jsonOutput += ",\"propertyUrl\":\"" + (property.getPropertyUrl() == null ? "": property.getPropertyUrl().toLowerCase().trim())+ "\"";

				  			jsonOutput += ",\"payAtHotelStatus\":\"" + (property.getPayAtHotelStatus() == null ? false: property.getPayAtHotelStatus())+ "\"";
							jsonOutput += ",\"coupleStatus\":\"" + ( property.getCoupleIsActive() == null? false :  property.getCoupleIsActive() )+ "\"";
							jsonOutput += ",\"breakfastStatus\":\"" + ( property.getBreakfastIsActive() == null? false:  property.getBreakfastIsActive() )+ "\"";
							jsonOutput += ",\"hotelStatus\":\"" + ( property.getHotelIsActive() == null? false:  property.getHotelIsActive() )+ "\"";
							jsonOutput += ",\"soldoutStatus\":\"" + (property.getStopSellIsActive() == null ? false: property.getStopSellIsActive())+ "\"";
							jsonOutput += ",\"delistStatus\":\"" + ( property.getDelistIsActive() == null? false :  property.getDelistIsActive() )+ "\"";
							jsonOutput += ",\"latitude\":\"" + ( property.getLatitude() == null? "":  property.getLatitude().trim())+ "\"";
							jsonOutput += ",\"longitude\":\"" + ( property.getLongitude() == null? false:  property.getLongitude().trim())+ "\"";
							String strHotelPolicy=null,strStandardPolicy=null,strCancellationPolicy=null,strGigiPolicy=null;
							
							strHotelPolicy=property.getPropertyHotelPolicy();
							strStandardPolicy=property.getPropertyStandardPolicy();
							strCancellationPolicy=property.getPropertyCancellationPolicy();
							strGigiPolicy=property.getPropertyGigiPolicy();
							String refundPolicy,changedRefundPolicy,cancelledPolicy,changedCancelledPolicy,
							gigiPolicy,changedGigiPolicy,hotelPolicy,changedHotelPolicy;
							
							if(strHotelPolicy!=null){
								hotelPolicy = strHotelPolicy.replace("\"", "\\\"");
								changedHotelPolicy = hotelPolicy.replaceAll("\\s+", " ");
							}else{
								changedHotelPolicy="";
							}
							
							if(strStandardPolicy!=null){
								refundPolicy = strStandardPolicy.replace("\"", "\\\"");
								changedRefundPolicy = refundPolicy.replaceAll("\\s+", " ");
							}else{
								changedRefundPolicy ="";
								
							}
							
							if(strCancellationPolicy!=null){
								cancelledPolicy = strCancellationPolicy.replace("\"", "\\\"");
								changedCancelledPolicy = cancelledPolicy.replaceAll("\\s+", " ");
							}else{
								changedCancelledPolicy="";
							}
							
							
							if(strGigiPolicy!=null){
								gigiPolicy = strGigiPolicy.replace("\"", "\\\"");
								changedGigiPolicy = gigiPolicy.replaceAll("\\s+", " ");
							}else{
								changedGigiPolicy="";
							}
							
				  			jsonOutput += ",\"hotelPolicy\":\"" + (changedHotelPolicy == null ? "None": changedHotelPolicy.trim())+ "\"";
				  			jsonOutput += ",\"refundPolicy\":\"" + (changedRefundPolicy == null ? "None": changedRefundPolicy.trim())+ "\"";
				  			jsonOutput += ",\"cancellationPolicy\":\"" + (changedCancelledPolicy == null ? "None": changedCancelledPolicy.trim())+ "\"";
				  			jsonOutput += ",\"gigiPolicy\":\"" + (changedGigiPolicy == null ? "None": changedGigiPolicy.trim())+ "\"";
				  			String imagePath="";
							if ( property.getPropertyThumbPath() != null) {
								imagePath = getText("storage.aws.property.photo") + "/uloimg/property/"
										+ property.getPropertyId()+"/"+property.getPropertyThumbPath();
							} 
							
							jsonOutput += ",\"propertyImage\":\"" + ( imagePath )+ "\"";
				  			SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
				  			Timestamp FromDate=(Timestamp)mapDate.get("arrivalDate");
				  			Timestamp ToDate=(Timestamp)mapDate.get("departureDate");
				  			jsonOutput += ",\"arrivalDate\":\"" + format2.format(FromDate) + "\"";
				  			jsonOutput += ",\"departureDate\":\"" + format2.format(ToDate)+ "\"";
				  			
				  			jsonOutput += ",\"startDate\":\"" + format.format(FromDate) + "\"";
				  			jsonOutput += ",\"endDate\":\"" + format.format(ToDate)+ "\"";
				  			
				  			String strFromDate=format2.format(FromDate);
				  			String strToDate=format2.format(ToDate);
				  	    	DateTime startdate = DateTime.parse(strFromDate);
				  	        DateTime enddate = DateTime.parse(strToDate);
				  	        java.util.Date fromdate = startdate.toDate();
				  	 		java.util.Date todate = enddate.toDate();
				  	 		 
				  			int diffInDays = (int) ((todate.getTime() - fromdate.getTime()) / (1000 * 60 * 60 * 24));
				  			
				  			List arllist=new ArrayList();
				  			List arllistAvl=new ArrayList();
				  			List arllistSold=new ArrayList();
				  			ArrayList<Integer> arlLastImage = new ArrayList<Integer>();
				  			StringBuilder imagebuilder=new StringBuilder();
				  			jsonOutput += ",\"diffDays\":\"" + diffInDays+ "\"";
				  			boolean blnImgExterior=false,blnImgReception=false,blnImgRestaurant=false,
				  					blnImgActivityArea=false,blnImgBedRoom=false,blnImgRestRoom=false;
				  			String jsonPhotos="",strPhotos="",data="",strData="",photoPath="";
							String jsonExteriour = "",exteriorphoto="";
							String jsonReception = "",receptionphoto="";
							String jsonRestaurant = "",restaurantphoto="";
							String jsonActivityArea = "",activityareaphotos="";
							String jsonPhotoAccommodation = "",roomphotos="";
							String jsonBedRooms = "",bedroomphotos="";
							String jsonRestRooms = "",restroomphotos="";
							
							PropertyPhotoManager photoController = new PropertyPhotoManager();
							
						  List<PropertyPhoto> listPhotos =photoController.list(property.getPropertyId());
						  if(listPhotos.size()>0){
				        	  for (PropertyPhoto photo : listPhotos) {
					          		if(photo.getPmsPhotoCategory()!=null && photo.getPmsPhotoCategory().getPhotoCategoryId()==5){
					          			if(!arlLastImage.contains(photo.getPmsPhotoCategory().getPhotoCategoryId())){
					          				arlLastImage.add(photo.getPmsPhotoCategory().getPhotoCategoryId());	
					          			}
					          			
					          		}
				        	  }
				        	  for (PropertyPhoto photo : listPhotos) {
					          		if(photo.getPmsPhotoCategory()!=null && photo.getPmsPhotoCategory().getPhotoCategoryId()==6){
					          			if(!arlLastImage.contains(photo.getPmsPhotoCategory().getPhotoCategoryId())){
					          				arlLastImage.add(photo.getPmsPhotoCategory().getPhotoCategoryId());	
					          			}
					          		}
				        	  }
				        	  for (PropertyPhoto photo : listPhotos) {
					          		if(photo.getPmsPhotoCategory()!=null && photo.getPmsPhotoCategory().getPhotoCategoryId()==1){
					          			if(!arlLastImage.contains(photo.getPmsPhotoCategory().getPhotoCategoryId())){
					          				arlLastImage.add(photo.getPmsPhotoCategory().getPhotoCategoryId());	
					          			}
					          		}
				        	  }
				        	  for (PropertyPhoto photo : listPhotos) {
					          		if(photo.getPmsPhotoCategory()!=null && photo.getPmsPhotoCategory().getPhotoCategoryId()==2){
					          			if(!arlLastImage.contains(photo.getPmsPhotoCategory().getPhotoCategoryId())){
					          				arlLastImage.add(photo.getPmsPhotoCategory().getPhotoCategoryId());	
					          			}
					          		}
				        	  }
				        	  for (PropertyPhoto photo : listPhotos) {
					          		if(photo.getPmsPhotoCategory()!=null && photo.getPmsPhotoCategory().getPhotoCategoryId()==3){
					          			if(!arlLastImage.contains(photo.getPmsPhotoCategory().getPhotoCategoryId())){
					          				arlLastImage.add(photo.getPmsPhotoCategory().getPhotoCategoryId());	
					          			}
					          		}
				        	  }
				        	  for (PropertyPhoto photo : listPhotos) {
					          		if(photo.getPmsPhotoCategory()!=null && photo.getPmsPhotoCategory().getPhotoCategoryId()==4){
					          			if(!arlLastImage.contains(photo.getPmsPhotoCategory().getPhotoCategoryId())){
					          				arlLastImage.add(photo.getPmsPhotoCategory().getPhotoCategoryId());	
					          			}
					          		}
				        	  }
				          }
				          int lastelement=0,nthelement=0;
				          if(arlLastImage.size()>0){
				        	  lastelement=arlLastImage.get(arlLastImage.size()-1);  
				          }				          
				          
				          if(listPhotos.size()>0)
						  {
				        	  
					          	/*for (PropertyPhoto photo : listPhotos) {
					          		
					          		if(photo.getPropertyAccommodation()!=null){
					          			if (!jsonPhotoAccommodation.equalsIgnoreCase(""))
						          			jsonPhotoAccommodation += ",{";
										else
											jsonPhotoAccommodation += "{";
						          		photoPath = getText("storage.aws.property.photo") + "/uloimg/"
												+ photo.getPmsProperty().getPropertyId()+"/accommodation/"+photo.getPropertyAccommodation().getAccommodationId()+"/"+photo.getPhotoPath();
									
						          		jsonPhotoAccommodation += "\"original\":\"" + photoPath + "\"";
						          		jsonPhotoAccommodation += ",\"thumbnail\":\"" + photoPath+ "\"";

						          		jsonPhotoAccommodation += "}";
					          		}
					
					  			 }
					          	
					          	data="\"images\":[" + jsonPhotoAccommodation + "]";
					 			strData=data.replaceAll("\"", "\"");
					 			
					 			roomphotos="{\"name\":\"bedroom\"," + strData+ "}";*/
					 			
				        	  for (PropertyPhoto photo : listPhotos) {
					    			
					          		if(photo.getPmsPhotoCategory()!=null && photo.getPmsPhotoCategory().getPhotoCategoryId() == 5){
					          			blnImgBedRoom=true;
					          			nthelement=photo.getPmsPhotoCategory().getPhotoCategoryId();
										if (!jsonBedRooms.equalsIgnoreCase(""))
											jsonBedRooms += ",{";
										else
											jsonBedRooms += "{";
								
										photoPath = getText("storage.aws.property.photo") + "/uloimg/"
												+ photo.getPmsProperty().getPropertyId()+"/accommodation/"+photo.getPropertyAccommodation().getAccommodationId()+
												"/"+photo.getPmsPhotoCategory().getPhotoCategoryId()+"/"+photo.getPhotoPath();
										
										jsonBedRooms += "\"original\":\"" + photoPath + "\"";
										jsonBedRooms += ",\"thumbnail\":\"" + photoPath+ "\"";
										this.altName=photo.getImageAltName()==null?"NA":photo.getImageAltName();
										jsonBedRooms += ",\"altName\":\"" +this.altName + "\"";
										jsonBedRooms += "}";
					          		}
					
					          		
					
					  			 }
					          	data="";strData="";
					          	data="\"images\":[" + jsonBedRooms+ "]";
					 			strData=data.replaceAll("\"", "\"");
					 			
					 			bedroomphotos ="{\"name\":\"bedroom\"," + strData+ "}";
					 			if(blnImgBedRoom){
					 				if(nthelement==lastelement){
					 					imagebuilder.append(bedroomphotos);
					 				}else{
					 					imagebuilder.append(bedroomphotos+",");
					 				}
					 				
					 			}
					 			for (PropertyPhoto photo : listPhotos) {
					    			
					          		if(photo.getPmsPhotoCategory()!=null && photo.getPmsPhotoCategory().getPhotoCategoryId() == 6){
					          			blnImgRestRoom=true;
					          			nthelement=photo.getPmsPhotoCategory().getPhotoCategoryId();
										if (!jsonRestRooms.equalsIgnoreCase(""))
											jsonRestRooms += ",{";
										else
											jsonRestRooms += "{";
								
										photoPath = getText("storage.aws.property.photo") + "/uloimg/"
												+ photo.getPmsProperty().getPropertyId()+"/accommodation/"+photo.getPropertyAccommodation().getAccommodationId()+
												"/"+photo.getPmsPhotoCategory().getPhotoCategoryId()+"/"+photo.getPhotoPath();
										
										jsonRestRooms += "\"original\":\"" + photoPath + "\"";
										jsonRestRooms += ",\"thumbnail\":\"" + photoPath+ "\"";
										this.altName=photo.getImageAltName()==null?"NA":photo.getImageAltName();
										jsonRestRooms += ",\"altName\":\"" +this.altName + "\"";
										
										jsonRestRooms += "}";
					          		}
					
					          		
					
					  			 }
					          	data="";strData="";
					          	data="\"images\":[" + jsonRestRooms + "]";
					 			strData=data.replaceAll("\"", "\"");
					 			
					 			restroomphotos="{\"name\":\"bathroom\"," + strData+ "}";
					 			if(blnImgRestRoom){
					 				if(nthelement==lastelement){
					 					imagebuilder.append(restroomphotos);
					 				}else{
					 					imagebuilder.append(restroomphotos+",");
					 				}
					 				
					 			}
					          	for (PropertyPhoto photo : listPhotos) {
					    			
					          		if(photo.getPmsPhotoCategory()!=null && photo.getPmsPhotoCategory().getPhotoCategoryId() == 1){
					          			blnImgExterior=true;
					          			nthelement=photo.getPmsPhotoCategory().getPhotoCategoryId();
										if (!jsonExteriour.equalsIgnoreCase(""))
											jsonExteriour += ",{";
										else
											jsonExteriour += "{";
								
										photoPath = getText("storage.aws.property.photo") + "/uloimg/"
												+ photo.getPmsProperty().getPropertyId()+"/category/"+photo.getPmsPhotoCategory().getPhotoCategoryId()+"/"+photo.getPhotoPath();
										
										jsonExteriour += "\"original\":\"" + photoPath + "\"";
										jsonExteriour += ",\"thumbnail\":\"" + photoPath+ "\"";
										this.altName=photo.getImageAltName()==null?"NA":photo.getImageAltName();
										jsonExteriour += ",\"altName\":\"" +this.altName + "\"";
										jsonExteriour += "}";
					          		}
					
					          		
					
					  			 }
					          	data="";strData="";
					          	data="\"images\":[" + jsonExteriour + "]";
					 			strData=data.replaceAll("\"", "\"");
					 			
					 			exteriorphoto="{\"name\":\"exterior\"," + strData+ "}";
					 			if(blnImgExterior){
					 				if(nthelement==lastelement){
					 					imagebuilder.append(exteriorphoto);
					 				}else{
					 					imagebuilder.append(exteriorphoto+",");
					 				}
					 				
					 			}
					          	for (PropertyPhoto photo : listPhotos) {
					    			
					          		if(photo.getPmsPhotoCategory()!=null && photo.getPmsPhotoCategory().getPhotoCategoryId() == 2){
					          			blnImgReception=true;
					          			nthelement=photo.getPmsPhotoCategory().getPhotoCategoryId();
										if (!jsonReception.equalsIgnoreCase(""))
											jsonReception += ",{";
										else
											jsonReception += "{";
										photoPath = getText("storage.aws.property.photo") + "/uloimg/"
												+ photo.getPmsProperty().getPropertyId()+"/category/"+photo.getPmsPhotoCategory().getPhotoCategoryId()+"/"+photo.getPhotoPath();
										jsonReception += "\"original\":\"" + photoPath + "\"";
										jsonReception += ",\"thumbnail\":\"" + photoPath+ "\"";
										this.altName=photo.getImageAltName()==null?"NA":photo.getImageAltName();
										jsonReception += ",\"altName\":\"" +this.altName + "\"";
										jsonReception += "}";
					          		}
					  			 }
					          	data="";strData="";
					          	data="\"images\":[" + jsonReception + "]";
					 			strData=data.replaceAll("\"", "\"");
					 			
					 			receptionphoto="{\"name\":\"reception\"," + strData+ "}";
					 			if(blnImgReception){
					 				if(nthelement==lastelement){
					 					imagebuilder.append(receptionphoto);
					 				}else{
					 					imagebuilder.append(receptionphoto+",");
					 				}
					 				
					 			}
					          	for (PropertyPhoto photo : listPhotos) {
					    			
					          		if(photo.getPmsPhotoCategory()!=null && photo.getPmsPhotoCategory().getPhotoCategoryId() == 3){
					          			blnImgRestaurant=true;
					          			nthelement=photo.getPmsPhotoCategory().getPhotoCategoryId();
										if (!jsonRestaurant.equalsIgnoreCase(""))
											jsonRestaurant += ",{";
										else
											jsonRestaurant += "{";
										photoPath = getText("storage.aws.property.photo") + "/uloimg/"
												+ photo.getPmsProperty().getPropertyId()+"/category/"+photo.getPmsPhotoCategory().getPhotoCategoryId()+"/"+photo.getPhotoPath();
										
										jsonRestaurant += "\"original\":\"" + photoPath + "\"";
										jsonRestaurant += ",\"thumbnail\":\"" + photoPath+ "\"";
										this.altName=photo.getImageAltName()==null?"NA":photo.getImageAltName();
										jsonRestaurant += ",\"altName\":\"" +this.altName + "\"";
										jsonRestaurant += "}";
					          		}
					  			 }
					          	data="";strData="";
					          	data="\"images\":[" + jsonRestaurant + "]";
					 			strData=data.replaceAll("\"", "\"");
					 			
					 			restaurantphoto="{\"name\":\"restaurant\"," + strData+ "}";
					 			if(blnImgRestaurant){
					 				if(nthelement==lastelement){
					 					imagebuilder.append(restaurantphoto);
					 				}else{
					 					imagebuilder.append(restaurantphoto+",");
					 				}
					 				
					 			}
								for (PropertyPhoto photo : listPhotos) {
									
										if(photo.getPmsPhotoCategory()!=null && photo.getPmsPhotoCategory().getPhotoCategoryId() == 4){
											blnImgActivityArea=true;
											nthelement=photo.getPmsPhotoCategory().getPhotoCategoryId();
											if (!jsonActivityArea.equalsIgnoreCase(""))
												jsonActivityArea += ",{";
											else
												jsonActivityArea += "{";
											photoPath = getText("storage.aws.property.photo") + "/uloimg/"
													+ photo.getPmsProperty().getPropertyId()+"/category/"+photo.getPmsPhotoCategory().getPhotoCategoryId()+"/"+photo.getPhotoPath();
											jsonActivityArea += "\"original\":\"" + photoPath + "\"";
											jsonActivityArea += ",\"thumbnail\":\"" + photoPath+ "\"";
											this.altName=photo.getImageAltName()==null?"NA":photo.getImageAltName();
											jsonActivityArea += ",\"altName\":\"" +this.altName + "\"";
											jsonActivityArea += "}";
										}
								 }
								data="";strData="";
								data="\"images\":[" + jsonActivityArea + "]";
					 			strData=data.replaceAll("\"", "\"");
					 			
					 			activityareaphotos="{\"name\":\"others\"," + strData+ "}";
					 			if(blnImgActivityArea){
					 				if(nthelement==lastelement){
					 					imagebuilder.append(activityareaphotos);
					 				}else{
					 					imagebuilder.append(activityareaphotos);
					 				}
					 				
					 			}
					 			
					 			jsonOutput += ",\"gallery\":[" + imagebuilder+ "]";
//					 			jsonOutput += ",\"gallery\":[" + bedroomphotos+","+restroomphotos+","+exteriorphoto+","+receptionphoto+","+restaurantphoto+","+activityareaphotos+ "]";
							
							}
				          
				  			String jsonAmenities ="";
				  			propertyAmenityList =  amenityController.list(property.getPropertyId());
				  			if(propertyAmenityList.size()>0)
				  			{
				  				for (PropertyAmenity amenity : propertyAmenityList) {
				  					if (!jsonAmenities.equalsIgnoreCase(""))
				  						
				  						jsonAmenities += ",{";
				  					else
				  						jsonAmenities += "{";
				  					PropertyAmenity amentity1 = amenityController.find(amenity.getPropertyAmenityId());
				  					String path = "contents/images/grey-color-icons/";
				  					//String path = "ulowebsite/images/icons/";
				  					PmsAmenity pmsAmenity1 =ameController.find(amentity1.getPmsAmenity().getAmenityId());
				  					jsonAmenities += "\"DT_RowId\":\"" + amenity.getPropertyAmenityId()+ "\"";					
				  					jsonAmenities += ",\"amenityName\":\"" + pmsAmenity1.getAmenityName()+ "\"";
				  					jsonAmenities += ",\"amenityId\":\"" + pmsAmenity1.getAmenityId()+ "\"";
				                    jsonAmenities += ",\"icon\":\"" + pmsAmenity1.getAmenityIcon()+ "\"";
				               
				  					
				  					jsonAmenities += "}";
				  				}
				  				
				  				jsonOutput += ",\"amenities\":[" + jsonAmenities+ "]";
				  			}
				  			
				  			Integer tripTypeId=properties.getTripTypeId();
				  			if(tripTypeId==null){
				  				tripTypeId=1;
				  			}
				  			String jsonTagAmenities ="";
				  			Integer businessTag[]={140,141,149,146,147,148};
				  			Integer coupleTag[]={140,141,142,143,144,145};
				  			Integer familyTag[]={140,141,144,149,146,148};
				  			if(tripTypeId==1){
				  				for(int i=0;i<businessTag.length;i++){
				  					if (!jsonTagAmenities.equalsIgnoreCase(""))
				  						
				  						jsonTagAmenities += ",{";
				  					else
				  						jsonTagAmenities += "{";
				  					PmsAmenity pmsAmenity1 =ameController.find(businessTag[i]);
				  					jsonTagAmenities += "\"amenityName\":\"" + pmsAmenity1.getAmenityName()+ "\"";
				  					jsonTagAmenities += ",\"amenityId\":\"" + pmsAmenity1.getAmenityId()+ "\"";
				  					jsonTagAmenities += ",\"icon\":\"" + pmsAmenity1.getAmenityIcon()+ "\"";
				               
				  					
				  					jsonTagAmenities += "}";
					  			}	
				  			}
				  			if(tripTypeId==2){
				  				for(int i=0;i<coupleTag.length;i++){
				  					if (!jsonTagAmenities.equalsIgnoreCase(""))
				  						
				  						jsonTagAmenities += ",{";
				  					else
				  						jsonTagAmenities += "{";
				  					PmsAmenity pmsAmenity1 =ameController.find(coupleTag[i]);
				  					jsonTagAmenities += "\"amenityName\":\"" + pmsAmenity1.getAmenityName()+ "\"";
				  					jsonTagAmenities += ",\"amenityId\":\"" + pmsAmenity1.getAmenityId()+ "\"";
				  					jsonTagAmenities += ",\"icon\":\"" + pmsAmenity1.getAmenityIcon()+ "\"";
				               
				  					
				  					jsonTagAmenities += "}";
					  			}	
				  			}
				  			if(tripTypeId==3){
				  				for(int i=0;i<familyTag.length;i++){
				  					if (!jsonTagAmenities.equalsIgnoreCase(""))
				  						
				  						jsonTagAmenities += ",{";
				  					else
				  						jsonTagAmenities += "{";
				  					PmsAmenity pmsAmenity1 =ameController.find(familyTag[i]);
				  					jsonTagAmenities += "\"amenityName\":\"" + pmsAmenity1.getAmenityName()+ "\"";
				  					jsonTagAmenities += ",\"amenityId\":\"" + pmsAmenity1.getAmenityId()+ "\"";
				  					jsonTagAmenities += ",\"icon\":\"" + pmsAmenity1.getAmenityIcon()+ "\"";
				               
				  					
				  					jsonTagAmenities += "}";
					  			}	
				  			}
				  			jsonOutput += ",\"tagamenities\":[" + jsonTagAmenities+ "]";

				  			String jsonGuestReview="";
				  			String jsonReviews="";
							List<PropertyReviews> listReviews=reviewController.listReview(property.getPropertyId());
							if(listReviews.size()>0 && !listReviews.isEmpty()){
								for(PropertyReviews reviews:listReviews){
									
									if (!jsonReviews.equalsIgnoreCase(""))
										
										jsonReviews += ",{";
									else
										jsonReviews += "{";

									/*jsonReviews += "\"starCount\":\"" + reviews.getStarCount().intValue()+ "\"";	
									jsonReviews += ",\"reviewCount\":\"" + reviews.getReviewCount().intValue()+ "\"";
									Double averageReviews=reviews.getAverageReview();
									jsonReviews += ",\"averageReview\":\"" + df.format(averageReviews)+ "\"";*/
									
									jsonReviews += "\"starCount\":\"" + (reviews.getStarCount()==null?0:reviews.getStarCount().intValue())+ "\"";	
									jsonReviews += ",\"reviewCount\":\"" + (reviews.getReviewCount()==null?0:reviews.getReviewCount().intValue())+ "\"";	
									if(reviews.getAverageReview()!=null){
										Double averageReviews=reviews.getAverageReview();
										jsonReviews += ",\"averageReview\":\"" + df.format(averageReviews)+ "\"";
									}else{
										jsonReviews += ",\"averageReview\":\"" + df.format(0)+ "\"";
									}
									
									jsonReviews += ",\"tripadvisorStarCount\":\"" + (reviews.getTripadvisorStarCount()==null?0:reviews.getTripadvisorStarCount().intValue())+ "\"";	
									jsonReviews += ",\"tripadvisorReviewCount\":\"" + (reviews.getTripadvisorReviewCount()==null?0:reviews.getTripadvisorReviewCount().intValue())+ "\"";
									if(reviews.getTripadvisorAverageReview()!=null){
										Double taAverageReviews=reviews.getTripadvisorAverageReview();
										jsonReviews += ",\"tripadvisorAverageReview\":\"" + df.format(taAverageReviews)+ "\"";
									}else{
										jsonReviews += ",\"tripadvisorAverageReview\":\"" + df.format(0)+ "\"";
									}
									jsonReviews += ",\"googleReviewUrl\":\"" + (reviews.getGoogleReviewUrl()==null?"":reviews.getGoogleReviewUrl())+ "\"";	
									jsonReviews += ",\"tripadvisorReviewUrl\":\"" + (reviews.getTripadvisorReviewUrl()==null?"":reviews.getTripadvisorReviewUrl())+ "\"";	
									jsonReviews += ",\"propertyRating\":\"" + (reviews.getPropertyRating()==null?"None":reviews.getPropertyRating())+ "\"";
									jsonReviews += "}";
								}
								
							}else{
								if (!jsonReviews.equalsIgnoreCase(""))
									
									jsonReviews += ",{";
								else
									jsonReviews += "{";
								
								jsonReviews += "\"starCount\":\"" + 0+ "\"";	
								jsonReviews += ",\"reviewCount\":\"" + 0+ "\"";	
								jsonReviews += ",\"averageReview\":\"" + 0+ "\"";
								
								jsonReviews += ",\"tripadvisorStarCount\":\"" + 0 + "\"";	
								jsonReviews += ",\"tripadvisorReviewCount\":\"" + 0 + "\"";	
								jsonReviews += ",\"tripadvisorAverageReview\":\"" + 0+ "\"";
								
								jsonReviews += ",\"googleReviewUrl\":\"" + "None"+ "\"";	
								jsonReviews += ",\"tripadvisorReviewUrl\":\"" + "None"+ "\"";	
								jsonReviews += ",\"propertyRating\":\"" + "None"+ "\"";
								
								jsonReviews += "}";
							}
							
							jsonOutput += ",\"reviews\":[" + jsonReviews+ "]";
							
							
							PropertyGuestReviewManager guestReviewController=new PropertyGuestReviewManager();
							List<PropertyGuestReview> listGuestReviews=guestReviewController.listReview(property.getPropertyId());
							if(listGuestReviews.size()>0 && !listGuestReviews.isEmpty()){
								for(PropertyGuestReview reviews:listGuestReviews){
									if (!jsonGuestReview.equalsIgnoreCase(""))
										
										jsonGuestReview += ",{";
									else
										jsonGuestReview += "{";
									
									jsonGuestReview += "\"reviewerGuestName\":\"" + (reviews.getReviewGuestName()==null?"":reviews.getReviewGuestName())+ "\"";	
									jsonGuestReview += ",\"reviewerDescription\":\"" + (reviews.getReviewDescription()==null?"":reviews.getReviewDescription())+ "\"";	
									jsonGuestReview += ",\"reviewerCount\":\"" + (reviews.getReviewCount()==null?0:reviews.getReviewCount())+ "\"";
									jsonGuestReview += ",\"reviewerName\":\"" + (reviews.getReviews().getReviewerName()==null?"":reviews.getReviews().getReviewerName())+ "\"";
									jsonGuestReview += ",\"reviewerId\":\"" + (reviews.getReviews().getReviewerId()==null?"":reviews.getReviews().getReviewerId())+ "\"";
									jsonGuestReview += "}";
								}
							}else{
								if (!jsonGuestReview.equalsIgnoreCase(""))
									
									jsonGuestReview += ",{";
								else
									jsonGuestReview += "{";
								
								jsonGuestReview += "\"reviewerGuestName\":\"" + ""+ "\"";	
								jsonGuestReview += ",\"reviewerDescription\":\"" + ""+ "\"";	
								jsonGuestReview += ",\"reviewerCount\":\"" + 0+ "\"";
								jsonGuestReview += ",\"reviewerName\":\"" + ""+ "\"";
								jsonGuestReview += ",\"reviewerId\":\"" + "" + "\"";
								
								jsonGuestReview += "}";
							}
						
							jsonOutput += ",\"guestReviews\":[" + jsonGuestReview+ "]";
				  			
							String jsonLandmark="";
							
							List<PropertyLandmark> listPropertyLandmark=propertyLandmarkController.list(property.getPropertyId());
							if(listPropertyLandmark.size()>0 && !listPropertyLandmark.isEmpty()){
								for(PropertyLandmark landmark:listPropertyLandmark){
									if(!jsonLandmark.equalsIgnoreCase(""))
										jsonLandmark+=",{";
									else
										jsonLandmark+="{";
									
										jsonLandmark += "\"landmarkLine1\":\"" + (landmark.getPropertyLandmark1() == null?"":landmark.getPropertyLandmark1())+ "\"";
										jsonLandmark += ",\"landmarkLine2\":\"" + (landmark.getPropertyLandmark2() == null?"":landmark.getPropertyLandmark2())+ "\"";
										jsonLandmark += ",\"landmarkLine3\":\"" + (landmark.getPropertyLandmark3() == null?"":landmark.getPropertyLandmark3())+ "\"";
										jsonLandmark += ",\"landmarkLine4\":\"" + (landmark.getPropertyLandmark4() == null?"":landmark.getPropertyLandmark4())+ "\"";
										jsonLandmark += ",\"landmarkLine5\":\"" + (landmark.getPropertyLandmark5() == null?"":landmark.getPropertyLandmark5())+ "\"";
									
										jsonLandmark += ",\"landmarkKilometers1\":\"" + (landmark.getKilometers1() == 0? "" :landmark.getKilometers1())+ "\"";
										jsonLandmark += ",\"landmarkKilometers2\":\"" + (landmark.getKilometers2() == 0? "":landmark.getKilometers2())+ "\"";
										jsonLandmark += ",\"landmarkKilometers3\":\"" + (landmark.getKilometers3() == 0? "":landmark.getKilometers3())+ "\"";
										jsonLandmark += ",\"landmarkKilometers4\":\"" + (landmark.getKilometers4() == 0? "":landmark.getKilometers4())+ "\"";
										jsonLandmark += ",\"landmarkKilometers5\":\"" + (landmark.getKilometers5() == 0? "":landmark.getKilometers5())+ "\"";
												
										
									jsonLandmark+="}";
								}
							}
							jsonOutput += ",\"landmark\":[" + jsonLandmark+ "]";
							
							String jsonAttraction="";
							
							List<PropertyLandmark> listPropertyAttraction=propertyLandmarkController.list(property.getPropertyId());
							if(listPropertyAttraction.size()>0 && !listPropertyAttraction.isEmpty()){
								for(PropertyLandmark attraction:listPropertyAttraction){
									if(!jsonAttraction.equalsIgnoreCase(""))
										jsonAttraction+=",{";
									else
										jsonAttraction+="{";
									
										jsonAttraction += "\"attractionLine1\":\"" + (attraction.getPropertyAttraction1() == null?"":attraction.getPropertyAttraction1())+ "\"";
										jsonAttraction += ",\"attractionLine2\":\"" + (attraction.getPropertyAttraction2() == null?"":attraction.getPropertyAttraction2())+ "\"";
										jsonAttraction += ",\"attractionLine3\":\"" + (attraction.getPropertyAttraction3() == null?"":attraction.getPropertyAttraction3())+ "\"";
										jsonAttraction += ",\"attractionLine4\":\"" + (attraction.getPropertyAttraction4() == null?"":attraction.getPropertyAttraction4())+ "\"";
										jsonAttraction += ",\"attractionLine5\":\"" + (attraction.getPropertyAttraction5() == null?"":attraction.getPropertyAttraction5())+ "\"";
										
										jsonAttraction += ",\"attractionKilometers1\":\"" + (attraction.getDistance1() == null?"":attraction.getDistance1())+ "\"";
										jsonAttraction += ",\"attractionKilometers2\":\"" + (attraction.getDistance2() == null?"":attraction.getDistance2())+ "\"";
										jsonAttraction += ",\"attractionKilometers3\":\"" + (attraction.getDistance3() == null?"":attraction.getDistance3())+ "\"";
										jsonAttraction += ",\"attractionKilometers4\":\"" + (attraction.getDistance4() == null?"":attraction.getDistance4())+ "\"";
										jsonAttraction += ",\"attractionKilometers5\":\"" + (attraction.getDistance5() == null?"":attraction.getDistance5())+ "\"";
												
										
									
									jsonAttraction+="}";
								}
							}
							jsonOutput += ",\"attraction\":[" + jsonAttraction+ "]";
							
				  			String jsonAccommodations ="";
				  			List<PropertyAccommodation> accommodationsList =  accommodationManager.list(property.getPropertyId());
				  			int soldOutCount=0,availCount=0,promotionAccommId=0;
				  			if(accommodationsList.size()>0 && !accommodationsList.isEmpty())
				  			{
				  				for (PropertyAccommodation accommodation : accommodationsList) {
				  					int roomCount=0;
				  					listAccommodationRoom=accommodationController.listPropertyTotalRooms(property.getPropertyId(), accommodation.getAccommodationId());
				  					if(listAccommodationRoom.size()>0 && !listAccommodationRoom.isEmpty()){
				  						for(AccommodationRoom roomsList:listAccommodationRoom){
				  							 roomCount=(int) roomsList.getRoomCount();
				  							 long minimum = getAvailableCount(property.getPropertyId(),getArrivalDate(),getDepartureDate(),roomCount,accommodation.getAccommodationId());
				  							 if((int)minimum>0){
				  								 availCount++;
				  							 }else if((int)minimum==0){
				  								 soldOutCount++;
				  							 }
				  							 break;
				  						 }
				  					 }
				  					
				  					
				  					if (!jsonAccommodations.equalsIgnoreCase(""))
				  						
				  						jsonAccommodations += ",{";
				  					else
				  						jsonAccommodations += "{";
				  						jsonAccommodations += "\"accommodationId\":\"" + accommodation.getAccommodationId()+ "\"";
				  						jsonAccommodations += ",\"accommodationName\":\"" + accommodation.getAccommodationType().toUpperCase()+ "\"";
				  					
				  					jsonAccommodations += "}";
				  				}
				  				
				  				jsonOutput += ",\"jsonAccommodations\":[" + jsonAccommodations+ "]";
				  			}

				  			
				  			List<PmsPromotionDetails> listPromotionDetailCheck=null;
				  			Boolean blnAvailable=false,blnSoldout=false,firstPromotionFlag=false,secondPromotionFlag=false,promotionFlag=false,blnPromoFlag=false;
				  			int arlListTotalAccomm=0,soldOutTotalAccomm=0,accommCount=0;
				  			ArrayList<String> arlListPromoType=new ArrayList<String>();
				  			
				  			String promotionType="NA",discountType="NA",promotionFirstName=null,promotionSecondName=null,
				  					promotionFirstType=null,promotionSecondType=null;
							double dblDiscountINR=0,dblDiscountPercent=0;
							boolean promoStatus=false,isFlat=false,isEarly=false,isLast=false,isSoldout=false;
							List<PmsPromotions> listPromotions=promotionController.listDatePromotions(property.getPropertyId(), dateStart);
							 if(listPromotions.size()>0 && !listPromotions.isEmpty()){
							   	for(PmsPromotions promotion:listPromotions){
							   		if(promotion.getPromotionType().equals("F")){
							   			promotionType=promotion.getPromotionType();
							   			promoStatus=true;
							   			isFlat=true;
							   		}else if(promotion.getPromotionType().equals("E")){
							   			promotionType=promotion.getPromotionType();
							   			Timestamp tsStartPromo=promotion.getFirstRangeStartDate();
							   			Timestamp tsEndPromo=promotion.getFirstRangeEndDate();
							   			String startStayDate = new SimpleDateFormat("yyyy-MM-dd").format(tsStartPromo);
										String endStayDate = new SimpleDateFormat("yyyy-MM-dd").format(tsEndPromo);
										DateTime dtstart = DateTime.parse(startStayDate);
									    DateTime dtend = DateTime.parse(endStayDate);
							   			List<DateTime> between = DateUtil.getDateRange(dtstart, dtend);
							   			if(between.size()>0 && !between.isEmpty()){
							   				for(DateTime d:between){
							   					if(cudate.equals(d.toDate())){
							   						promoStatus=true;
							   						isEarly=true;
							   					}
							   				}
							   			}else{
							   				promoStatus=false;
							   			}
							   		}
							   		
									List<PmsPromotionDetails> listPromoDetails=promotionDetailController.listPromotionDetails(promotion.getPromotionId(),f.format(dateStart).toLowerCase());
									if(listPromoDetails.size()>0 && !listPromoDetails.isEmpty()){
										for(PmsPromotionDetails promodetails:listPromoDetails){
											dblDiscountINR=promodetails.getPromotionInr();
											if(dblDiscountINR>0){
												discountType="INR";
											}
											dblDiscountPercent=promodetails.getPromotionPercentage();
											if(dblDiscountPercent>0){
												discountType="PERCENT";
											}
										}
									}
								}
							 }
							String jsonAccommodation=""; 
							String sellingType="NA",sellingDays="NA";
							sellingDays=f.format(dateStart).toLowerCase();
							boolean blnWeekdays=false,blnWeekend=false;
							for(int a=0;a<weekdays.length;a++){
								if(sellingDays.equalsIgnoreCase(weekdays[a])){
									blnWeekdays=true;
								}
							}
							
							for(int b=0;b<weekend.length;b++){
								if(sellingDays.equalsIgnoreCase(weekend[b])){
									blnWeekend=true;
								}
							}
							
		    				if(property.getLocationType()!=null){
		    					if(property.getLocationType().getLocationTypeName().equalsIgnoreCase("Leisure")){
									if(blnWeekdays){
										sellingType="weekdays";
									}
									
									if(blnWeekend){
										sellingType="weekend";
									}	
								}
								
								if(property.getLocationType().getLocationTypeName().equalsIgnoreCase("Metro")){
									sellingType="metro";
								}
		    				}
				  			List<PropertyAccommodation> accommodationList =  accommodationManager.listSortByPrice(property.getPropertyId(), sellingType);
				  			if(accommodationList.size()>0)
				  			{
				  				for (PropertyAccommodation accommodation : accommodationList) {
				  					
				  					if (!jsonAccommodation.equalsIgnoreCase(""))
				  						
				  						jsonAccommodation += ",{";
				  					else
				  						jsonAccommodation += "{";

				  					int dateCount=0;
				  					double amount=0.0,extraAdultAmount=0.0,extraChildAmount=0.0,totalMaximumAmount=0.0,totalMinimumAmount=0.0,diffAmount=0;
				  					int roomCount=0,availableCount=0,totalCount=0;
				  				    double dblPercentCount=0.0;
				  	    			listAccommodationRoom=accommodationController.listPropertyTotalRooms(property.getPropertyId(), accommodation.getAccommodationId());
				  					 
				  					if(listAccommodationRoom.size()>0 && !listAccommodationRoom.isEmpty()){
				  						
				  						for(AccommodationRoom roomsList:listAccommodationRoom){
				  							roomCount=(int) roomsList.getRoomCount();
				  							if(roomCount>0){
				  								long minimum = getAvailableCount(property.getPropertyId(),getArrivalDate(),getDepartureDate(),roomCount,accommodation.getAccommodationId());
				  								if((int)minimum>0){
				  									blnAvailable=true;
				  									blnSoldout=false;
				  									arlListTotalAccomm++;
				  								}else if((int)minimum==0){
				  									blnSoldout=true;
				  									blnAvailable=false;
				  									soldOutTotalAccomm++;
				  								}
				  							}
				  							 break;
				  						 }
				  					 }
				  					 List<DateTime> between = DateUtil.getDateRange(start, end);
				  					 
				  					double sellrate=0,minAmount=0,maxAmount=0,promoAmount=0,firstPromoAmount=0,secondPromoAmount=0,firstInr=0,secondInr=0;
				  					
				  				     for (DateTime d : between)
				  				     {

								    	 long rmc = 0;
							   			 this.roomCnt = rmc;
							        	 int sold=0;
							        	 java.util.Date availDate = d.toDate();
							        	 String days=f.format(d.toDate()).toLowerCase();
										 PropertyAccommodationInventory inventoryCount = inventoryController.findInventoryCount(availDate,accommodation.getAccommodationId());
										 if(inventoryCount != null){
										 PmsAvailableRooms roomCount1 = bookingController.findBookingCount(availDate, accommodation.getAccommodationId(),inventoryCount.getInventoryId());
										 if(roomCount1.getRoomCount() == null) {
										 	String num1 = inventoryCount.getInventoryCount();
											PmsAvailableRooms bookingRoomCount=bookingController.findCount(availDate, accommodation.getAccommodationId());
											PropertyAccommodation accomm=accommodationController.find(accommodation.getAccommodationId());
											Integer totavailable=0;
											if(accommodation!=null){
												totavailable=accommodation.getNoOfUnits();
											}
											if(bookingRoomCount.getRoomCount()!=null){
												long lngRooms=bookingRoomCount.getRoomCount();
												long num2 = Long.parseLong(num1);
												this.roomCnt = num2;
												sold=(int) (lngRooms);
												int availRooms=0;
												availRooms=totavailable-sold;
												/*if(Integer.parseInt(num1)>availRooms){
													this.roomCnt = availRooms;	
												}else{
													this.roomCnt = num2;
												}*/
												this.roomCnt = num2;
														
											}else{
												 long num2 = Long.parseLong(num1);
												 this.roomCnt = num2;
												 sold=(int) (rmc);
											}
										}else{		
											PmsAvailableRooms bookingRoomCount=bookingController.findCount(availDate, accommodation.getAccommodationId());
											int intRooms=0;
											Integer totavailable=0,availableRooms=0;
											if(accommodation!=null){
												totavailable=accommodation.getNoOfUnits();
											}
											if(bookingRoomCount.getRoomCount()!=null){
												long lngRooms=bookingRoomCount.getRoomCount();
												long num = roomCount1.getRoomCount();	
												intRooms=(int)lngRooms;
												String num1 = inventoryCount.getInventoryCount();
							 					long num2 = Long.parseLong(num1);
							 					availableRooms=totavailable-intRooms;
							 					/*if(num2>availableRooms){
							 						this.roomCnt = availableRooms;	 
							 					}else{
							 						this.roomCnt = num2-num;
							 					}*/
							 					this.roomCnt = num2-num;
							 					long lngSold=bookingRoomCount.getRoomCount();
							   					sold=(int)lngSold;
											}else{
												long num = roomCount1.getRoomCount();	
												String num1 = inventoryCount.getInventoryCount();
												long num2 = Long.parseLong(num1);
							  					this.roomCnt = (num2-num);
							  					long lngSold=roomCount1.getRoomCount();
							   					sold=(int)lngSold;
											}
						
										}
														
									}else{
										  PmsAvailableRooms inventoryCounts = bookingController.findCount(availDate, accommodation.getAccommodationId());							
											if(inventoryCounts.getRoomCount() == null){								
												this.roomCnt = (accommodation.getNoOfUnits()-rmc);
												sold=(int)rmc;
											}
											else{
												this.roomCnt = (accommodation.getNoOfUnits()-inventoryCounts.getRoomCount());	
												long lngSold=inventoryCounts.getRoomCount();
					   							sold=(int)lngSold;
											}
									}
									
									/*PmsAvailableRooms canceledCounts = bookingController.findCancelledCount(availDate, accommodation.getAccommodationId());							
									if(canceledCounts.getRoomCount() == null){								
										this.canceledCnt = 0;
									}
									else{
										this.canceledCnt = (accommodation.getNoOfUnits()-canceledCounts.getRoomCount());	
									}
									
									this.roomCnt=this.roomCnt+this.canceledCnt;	*/ 
										 
									double totalRooms=0,intSold=0;
									totalRooms=(double)accommodation.getNoOfUnits();
									intSold=(double)sold;
									double occupancy=0.0;
									occupancy=intSold/totalRooms;
									Integer occupancyRating=(int) Math.round(occupancy * 100);
									boolean isWeekdays=false,isWeekend=false;
									for(int a=0;a<weekdays.length;a++){
										if(days.equalsIgnoreCase(weekdays[a])){
											isWeekdays=true;
										}
									}
									
									for(int b=0;b<weekend.length;b++){
										if(days.equalsIgnoreCase(weekend[b])){
											isWeekend=true;
										}
									}
									if(property.getLocationType()!=null){
										if(property.getLocationType().getLocationTypeName().equalsIgnoreCase("Leisure")){
											if(isWeekdays){
												if(accommodation.getWeekdaySellingRate()==null){
													maxAmount=0;
												}else{
													maxAmount=accommodation.getWeekdaySellingRate();	
												}
													
											}
											
											if(isWeekend){
												if(accommodation.getWeekendSellingRate()==null){
													maxAmount=0;	
												}else{
													maxAmount=accommodation.getWeekendSellingRate();
												}
												
											}	
										}
										
										if(property.getLocationType().getLocationTypeName().equalsIgnoreCase("Metro")){
											if(accommodation.getSellingRate()==null){
												maxAmount=0;
											}else{
												maxAmount=accommodation.getSellingRate();	
											}
												
										}
										
										if(occupancyRating>=0 && occupancyRating<=30){
											sellrate=maxAmount;
										}else if(occupancyRating>30 && occupancyRating<=60){
											sellrate=maxAmount+(maxAmount*10/100);
										}else if(occupancyRating>=60 && occupancyRating<=80){
											sellrate=maxAmount+(maxAmount*15/100);
										}else if(occupancyRating>80){
											sellrate=maxAmount+(maxAmount*20/100);
										}
									}
				    				
									
										
										if(discountType.equals("INR")){
											firstPromoAmount=dblDiscountINR;
											firstInr+=dblDiscountINR;
										}else if(discountType.equals("PERCENT")){
											firstPromoAmount=(sellrate*dblDiscountPercent/100);
										}else{
											firstPromoAmount=0;
										}
										
																				
										if(checkStartDate.equals(strCheckInDate)){ 
											if(occupancyRating>=0 && occupancyRating<=30){
												if(this.roomCnt>0){
													isLast=true;
												}else{
													isLast=false;
												}
											}
										}
										
					    				if(isLast){
					    					if(occupancyRating>=0 && occupancyRating<=30){
					    						if(AMPM==0){//If the current time is AM
					    		            		if(bookedHours>=0 && bookedHours<=11){
					    		            			if(maxAmount<1000){
					    									if(occupancyRating>=0 && occupancyRating<=30){
					    										secondPromoAmount=commissionrange1;
					    										secondInr+=commissionrange1;
					    									}
					    							    }else if(maxAmount>=1000 && maxAmount<=1500){
					    							    	if(occupancyRating>=0 && occupancyRating<=30){
					    							    		secondPromoAmount=commissionrange2;
					    							    		secondInr+=commissionrange2;
					    									}
					    							    }else if(maxAmount>1500){
					    							    	if(occupancyRating>=0 && occupancyRating<=30){
					    							    		secondPromoAmount=commissionrange3;
					    							    		secondInr+=commissionrange3;
					    									}
					    							    }
					    		            		}
					    		            	}
					    						if(AMPM==1){
					    							if(bookedHours>=0 && bookedHours<=3){
					    								if(occupancyRating>=0 && occupancyRating<=30){
					    									secondPromoAmount=(sellrate*lastdiscountpercent1/100);	
					    								}
					    		            		}else if(bookedHours>=4 && bookedHours<=7){
					    		            			if(occupancyRating>=0 && occupancyRating<=30){
					    		            				secondPromoAmount=(sellrate*lastdiscountpercent2/100);	
					    		            			}
					    		            		}else if(bookedHours>=8 && bookedHours<=11){
					    		            			if(occupancyRating>=0 && occupancyRating<=30){
					    		            				secondPromoAmount=(sellrate*lastdiscountpercent3/100);	
					    		            			}
					    		            		}
					    						}
					    					}
					    				}
					    				promoAmount=firstPromoAmount+secondPromoAmount;
					    				totalMinimumAmount+=sellrate-promoAmount;
					  				    totalMaximumAmount+=sellrate;
								    	 dateCount++;
								    	extraAdultAmount+=accommodation.getExtraAdult(); 
								    	extraChildAmount+=accommodation.getExtraChild();
				  				     }
				  				     
				  				    
				  				    diffAmount=totalMaximumAmount-totalMinimumAmount;
				  				   if(isFlat){
										if(discountType.equalsIgnoreCase("INR")){
											promotionFirstName=String.valueOf(Math.round(firstInr))+"";
											promotionFirstType="inr";
										}else if(discountType.equalsIgnoreCase("PERCENT")){
											promotionFirstName=String.valueOf(Math.round(dblDiscountPercent))+"";
											promotionFirstType="percent";
										}
									}
									if(isEarly){
										if(discountType.equalsIgnoreCase("INR")){
											promotionFirstName=String.valueOf(Math.round(firstInr))+"";
											promotionFirstType="inr";
										}else if(discountType.equalsIgnoreCase("PERCENT")){
											promotionFirstName=String.valueOf(Math.round(dblDiscountPercent))+"";
											promotionFirstType="percent";
										}
									}
									if(isLast){

										if(AMPM==0){//If the current time is AM
			    		            		if(bookedHours>=0 && bookedHours<=11){
			    		            
			    		            			String displayDate = new SimpleDateFormat("MMM dd,yyyy HH:mm:ss").format(lastFirst.getTime());
			    								this.displayHoursForTimer=displayDate;
			    								setDisplayHoursForTimer(this.displayHoursForTimer);
			    								promotionSecondType="inr";
			    								promotionSecondName=String.valueOf(Math.round(secondInr))+"";
			    		            		}
			    		            	}
			    						if(AMPM==1){
			    							if(bookedHours>=0 && bookedHours<=3){
			    								String firstTime=checkStartDate+" 16:00:00"; 
			    						   	    java.util.Date dteFirstDate = sdf.parse(strStartTime);
			    						   	    Calendar calFirst=Calendar.getInstance();
			    						   	    calFirst.setTime(dteFirstDate);
			    						   	    calFirst.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
			    						   	    String firstDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calFirst.getTime());
			    						    	java.util.Date dteFirst = sdf.parse(firstTime);
			    								String displayDate = new SimpleDateFormat("MMM dd,yyyy HH:mm:ss").format(dteFirst.getTime());
			    								this.displayHoursForTimer=displayDate;
			    								setDisplayHoursForTimer(this.displayHoursForTimer);
			    								promotionSecondType="percent";
			    								promotionSecondName=String.valueOf(Math.round(lastdiscountpercent1))+"";
			    		            		}else if(bookedHours>=4 && bookedHours<=7){
			    		            			String firstTime=checkStartDate+" 20:00:00"; 
			    						   	    java.util.Date dteFirstDate = sdf.parse(strStartTime);
			    						   	    Calendar calFirst=Calendar.getInstance();
			    						   	    calFirst.setTime(dteFirstDate);
			    						   	    calFirst.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
			    						   	    String firstDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calFirst.getTime());
			    						    	java.util.Date dteFirst = sdf.parse(firstTime);
			    								String displayDate = new SimpleDateFormat("MMM dd,yyyy HH:mm:ss").format(dteFirst.getTime());
			    								this.displayHoursForTimer=displayDate;
			    								setDisplayHoursForTimer(this.displayHoursForTimer);
			    								promotionSecondType="percent";
			    								promotionSecondName=String.valueOf(Math.round(lastdiscountpercent2))+"";
			    		            		}else if(bookedHours>=8 && bookedHours<=11){
			    		            			String firstTime=checkStartDate+" 23:00:00"; 
			    						   	    java.util.Date dteFirstDate = sdf.parse(strStartTime);
			    						   	    Calendar calFirst=Calendar.getInstance();
			    						   	    calFirst.setTime(dteFirstDate);
			    						   	    calFirst.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
			    						   	    String firstDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calFirst.getTime());
			    						    	java.util.Date dteFirst = sdf.parse(firstTime);
			    								String displayDate = new SimpleDateFormat("MMM dd,yyyy HH:mm:ss").format(dteFirst.getTime());
			    								this.displayHoursForTimer=displayDate;
			    								setDisplayHoursForTimer(this.displayHoursForTimer);
			    								promotionSecondType="percent";
			    								promotionSecondName=String.valueOf(Math.round(lastdiscountpercent3))+"";
			    		            		}
			    						}

									
									}
									photoPath="";
									List<PropertyPhoto> listAccommodationPhotos =photoController.listAccommodation(accommodation.getPmsProperty().getPropertyId(), 5,accommodation.getAccommodationId());
							          int intCount=0;
							          if(listAccommodationPhotos.size()>0)
									  {
							        	  for (PropertyPhoto photo : listAccommodationPhotos) {
								    			
								          		if(photo.getPmsPhotoCategory()!=null && photo.getPmsPhotoCategory().getPhotoCategoryId() == 5){
													
													if(intCount==0){
														photoPath = getText("storage.aws.property.photo") + "/uloimg/"
																+ photo.getPmsProperty().getPropertyId()+"/accommodation/"+photo.getPropertyAccommodation().getAccommodationId()+
																"/"+photo.getPmsPhotoCategory().getPhotoCategoryId()+"/"+photo.getPhotoPath();	
													}
													intCount++;
													

								          		}
								
								          		
								
								  			 }
									  }else{
										  photoPath="NA";
									  }
									
									
									PropertyTaxeManager propertytaxController = new PropertyTaxeManager();
			  	  					 double taxe =  totalMinimumAmount/diffInDays;
			  	  					
			  						 PropertyTaxe tax = propertytaxController.find(taxe);
			  						 double taxes = Math.round(totalMinimumAmount * tax.getTaxPercentage() / 100  ) ;
			  	 			         
				  					
				  					long minimum = getAvailableCount(property.getPropertyId(),getArrivalDate(),getDepartureDate(),roomCount,accommodation.getAccommodationId());
				  			    	if(minimum<0){
				  			    		jsonAccommodation += "\"available\":\"" + minimum+ "\"";
				  			    		jsonAccommodation += ",\"minimumAmount\":\"" + (totalMinimumAmount==0 ? 0 : Math.round(totalMinimumAmount) )+  "\"";
				  			    		jsonAccommodation += ",\"maximumAmount\":\""  + (totalMaximumAmount==0 ? 0 : Math.round(totalMaximumAmount) )+  "\"";
				  			    		jsonAccommodation += ",\"taxAmount\":\"" + (taxes==0?0:Math.round(taxes)) + "\"";
				  			    		jsonAccommodation += ",\"taxPercentage\":\"" + (tax.getTaxPercentage()==0?0:Math.round(tax.getTaxPercentage())) + "\"";
				  			    		jsonAccommodation += ",\"difference\":\"" + (diffAmount==0 ? diffAmount  : Math.round(diffAmount) )+  "\"";
				  			    		jsonAccommodation += ",\"promotionFirstName\":\"" + (promotionFirstName==null? "NA":promotionFirstName)+ "\"";
				  			    		jsonAccommodation += ",\"promotionSecondName\":\"" + (promotionSecondName==null? "NA":promotionSecondName)+ "\"";
				  			    		jsonAccommodation += ",\"promotionFirstType\":\"" + (promotionFirstType==null? "NA":promotionFirstType)+ "\"";
				  				    	jsonAccommodation += ",\"promotionSecondType\":\"" + (promotionSecondType==null? "NA":promotionSecondType )+ "\"";
				  			    		jsonAccommodation += ",\"accommodationName\":\"" + accommodation.getAccommodationType().toUpperCase()+ "\"";
				  			    		jsonAccommodation += ",\"accommodationId\":\"" + accommodation.getAccommodationId()+ "\"";
				  			    		jsonAccommodation += ",\"accommodationImage\":\"" + photoPath+ "\"";
				  			    		isSoldout=true;
				  			    		jsonAccommodation += ",\"isSoldout\":\"" + isSoldout+ "\"";
				  			    		jsonAccommodation += ",\"isFlat\":\"" + isFlat+ "\"";
				  			    		jsonAccommodation += ",\"isEarly\":\"" + isEarly+ "\"";
				  			    		jsonAccommodation += ",\"isLast\":\"" + isLast+ "\"";
				  			    		if(isLast){
				  			    			jsonAccommodation += ",\"promoLastMinuteTimer\":\"" + this.displayHoursForTimer+ "\"";	
										}else{
											jsonAccommodation += ",\"promoLastMinuteTimer\":\"" + "NA"+ "\"";
										}
				  			    		count++;
				  			    	}else if(minimum==0){
				  			    		jsonAccommodation += "\"available\":\"" + minimum+ "\"";
				  			    		jsonAccommodation += ",\"minimumAmount\":\"" + (totalMinimumAmount==0 ? 0 : Math.round(totalMinimumAmount) )+  "\"";
				  			    		jsonAccommodation += ",\"maximumAmount\":\""  + (totalMaximumAmount==0 ? 0 : Math.round(totalMaximumAmount) )+  "\"";
				  			    		jsonAccommodation += ",\"taxAmount\":\"" + (taxes==0?0:Math.round(taxes)) + "\"";
				  			    		jsonAccommodation += ",\"taxPercentage\":\"" + (tax.getTaxPercentage()==0?0:Math.round(tax.getTaxPercentage())) + "\"";
				  			    		jsonAccommodation += ",\"difference\":\"" + (diffAmount==0 ? diffAmount  : Math.round(diffAmount) )+  "\"";
				  			    		jsonAccommodation += ",\"promotionFirstName\":\"" + (promotionFirstName==null? "NA":promotionFirstName)+ "\"";
				  			    		jsonAccommodation += ",\"promotionSecondName\":\"" + (promotionSecondName==null? "NA":promotionSecondName)+ "\"";
				  			    		jsonAccommodation += ",\"promotionFirstType\":\"" + (promotionFirstType==null? "NA":promotionFirstType)+ "\"";
				  				    	jsonAccommodation += ",\"promotionSecondType\":\"" + (promotionSecondType==null? "NA":promotionSecondType )+ "\"";
				  			    		jsonAccommodation += ",\"accommodationName\":\"" + accommodation.getAccommodationType().toUpperCase()+ "\"";
				  			    		jsonAccommodation += ",\"accommodationId\":\"" + accommodation.getAccommodationId()+ "\"";
				  			    		jsonAccommodation += ",\"accommodationImage\":\"" + photoPath+ "\"";
				  			    		isSoldout=true;
				  			    		jsonAccommodation += ",\"isSoldout\":\"" + isSoldout+ "\"";
				  			    		jsonAccommodation += ",\"isFlat\":\"" + isFlat+ "\"";
				  			    		jsonAccommodation += ",\"isEarly\":\"" + isEarly+ "\"";
				  			    		jsonAccommodation += ",\"isLast\":\"" + isLast+ "\"";
				  			    		if(isLast){
				  			    			jsonAccommodation += ",\"promoLastMinuteTimer\":\"" + this.displayHoursForTimer+ "\"";	
										}else{
											jsonAccommodation += ",\"promoLastMinuteTimer\":\"" + "NA"+ "\"";
										}
				  			    		count++;
				  				     }else{
				  				    	 isSoldout=false;
				  				    	jsonAccommodation += "\"available\":\"" + minimum+ "\""; 
				  				    	jsonAccommodation += ",\"accommodationName\":\"" + accommodation.getAccommodationType().toUpperCase()+ "\"";
				  				    	jsonAccommodation += ",\"accommodationId\":\"" + accommodation.getAccommodationId()+ "\"";
				  				    	jsonAccommodation += ",\"minimumAmount\":\"" + (totalMinimumAmount==0 ? 0 : Math.round(totalMinimumAmount) )+  "\"";
				  				    	jsonAccommodation += ",\"maximumAmount\":\"" + (totalMaximumAmount==0 ? 0 : Math.round(totalMaximumAmount) )+  "\"";
				  				    	jsonAccommodation += ",\"taxAmount\":\"" + (taxes==0?0:Math.round(taxes)) + "\"";
				  			    		jsonAccommodation += ",\"taxPercentage\":\"" + (tax.getTaxPercentage()==0?0:Math.round(tax.getTaxPercentage())) + "\"";
				  				    	jsonAccommodation += ",\"difference\":\"" + (diffAmount==0 ? 0  : Math.round(diffAmount) )+  "\"";
				  				    	jsonAccommodation += ",\"minOccupancy\":\"" + accommodation.getMinOccupancy() + "\"";
				  				    	jsonAccommodation += ",\"maxOccupancy\":\"" + accommodation.getMaxOccupancy() + "\"";
				  				    	jsonAccommodation += ",\"promotionFirstName\":\"" + (promotionFirstName==null? "NA":promotionFirstName)+ "\"";
				  				    	jsonAccommodation += ",\"promotionSecondName\":\"" + (promotionSecondName==null? "NA":promotionSecondName)+ "\"";
				  				    	jsonAccommodation += ",\"promotionFirstType\":\"" + (promotionFirstType==null? "NA":promotionFirstType)+ "\"";
				  				    	jsonAccommodation += ",\"promotionSecondType\":\"" + (promotionSecondType==null? "NA":promotionSecondType )+ "\"";
				  				    	Double extraAdult,extraChild;
				  						double dblextraadult,dblextrachild;
				  						
				  						dblextraadult=extraAdultAmount+extraAdultAmount*15/100;
				  						dblextrachild=extraChildAmount+extraChildAmount*15/100;
				  						
				  				    	jsonAccommodation += ",\"extraAdultAmount\":\"" + + (extraAdultAmount==0 ? 0 : Math.round(extraAdultAmount))+  "\"";
				  				    	jsonAccommodation += ",\"extraChildAmount\":\"" + + (extraChildAmount==0 ? 0 : Math.round(extraChildAmount))+  "\"";
				  				    	jsonAccommodation += ",\"accommodationImage\":\"" + photoPath+ "\"";
				  				    	jsonAccommodation += ",\"isSoldout\":\"" + isSoldout+ "\"";
				  				    	jsonAccommodation += ",\"isFlat\":\"" + isFlat+ "\"";
				  			    		jsonAccommodation += ",\"isEarly\":\"" + isEarly+ "\"";
				  			    		jsonAccommodation += ",\"isLast\":\"" + isLast+ "\"";
				  			    		
				  			    		if(isLast){
				  			    			jsonAccommodation += ",\"promoLastMinuteTimer\":\"" + this.displayHoursForTimer+ "\"";	
										}else{
											jsonAccommodation += ",\"promoLastMinuteTimer\":\"" + "NA"+ "\"";
										}
				  			    		count++;
				  			    		
				  				     }
				  			    	 
				  			    	String jsonAccommAmenities ="";
				  					List<AccommodationAmenity> accommodationAmenityList =  accommodationAmenityController.list(accommodation.getAccommodationId());
				  					if(propertyAmenityList.size()>0)
				  					{
				  						for (AccommodationAmenity amenity : accommodationAmenityList) {
				  							if (!jsonAccommAmenities.equalsIgnoreCase(""))
				  								
				  								jsonAccommAmenities += ",{";
				  							else
				  								jsonAccommAmenities += "{";
				  							
				  							PmsAmenity pmsAmenity1 =ameController.find(amenity.getPmsAmenity().getAmenityId());
				  							jsonAccommAmenities += "\"DT_RowId\":\"" + pmsAmenity1.getAmenityId()+ "\"";					
				  							jsonAccommAmenities += ",\"amenityName\":\"" + pmsAmenity1.getAmenityName()+ "\"";
				  							jsonAccommAmenities += ",\"amenityId\":\"" + pmsAmenity1.getAmenityId()+ "\"";
				  							jsonAccommAmenities += ",\"icon\":\"" + pmsAmenity1.getAmenityIcon()+ "\"";
				  							
				  							jsonAccommAmenities += "}";
				  						}
				  						
				  						jsonAccommodation += ",\"accommodationamenities\":[" + jsonAccommAmenities+ "]";
				  					}
				  			    	 
				  			    	jsonAccommodation += "}";
				  				   
				  				}
				  			}
				  			jsonOutput += ",\"accommodations\":[" + jsonAccommodation+ "]";
				  			
				  			jsonOutput += "}";
				  			
				  			arllist.clear();
				  		
						
				    	}
				    	
				    }

				    String data="\"data\":[" + jsonOutput + "]";
		 			String strData=data.replaceAll("\"", "\"");
		 			
		 			/*String token = JWT.create()
		 					.withClaim("data", strData)
		 					.withExpiresAt(now)
		 					.sign(algorithm);
		 			output = "\"token\":\"" + token+ "\"";
		 			String strOutput=output.replaceAll("\"", "\"");*/
		 			
		 			
		 			if(count == 0){
						//response.getWriter().write("{\"error\":\"1\",\"message\":\"Sorry No Results Found\"}");
		 				output="{\"error\":\"1\",\"message\":\"No Records Found\"}";
					}else{
						//response.getWriter().write("{\"error\":\"0\",\"propertyTag\":[" + jsonOutput + "]}");
//						output="{\"error\":\"0\",\"data\":{" + strData+ "}}";
						output="{\"error\":\"0\"," + strData + "}";
						
					}
			   }catch(Exception ex){
				   output="{\"error\":\"2\",\"message\":\"Some technical error on property api\"}";
				   logger.error(ex);
				   ex.printStackTrace();
			   }
			   return output;
		   
		   
			
		}
		
	public Integer getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(Integer propertyId) {
		this.propertyId = propertyId;
	}
	
	public Integer getAccommodationId() {
		return accommodationId;
	}

	public void setAccommodationId(Integer accommodationId) {
		this.accommodationId = accommodationId;
	}

	public File getMyFile() {
		return myFile;
	}

	public void setMyFile(File myFile) {
		this.myFile = myFile;
	}

	public String getMyFileContentType() {
		return myFileContentType;
	}

	public void setMyFileContentType(String myFileContentType) {
		this.myFileContentType = myFileContentType;
	}

	public String getMyFileFileName() {
		return myFileFileName;
	}

	public void setMyFileFileName(String myFileFileName) {
		this.myFileFileName = myFileFileName;
	}
	
	public Integer getBookingId() {
		return bookingId;
	}

	public void setBookingId(Integer bookingId) {
		this.bookingId = bookingId;
	}
	
	public Integer getStatusId() {
		return statusId;
	}

	public void setStatusId(Integer statusId) {
		this.statusId = statusId;
	}

	public Timestamp getArrivalDate() {
		return arrivalDate;
	}

	public void setArrivalDate(Timestamp arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	public Timestamp getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Timestamp departureDate) {
		this.departureDate = departureDate;
	}
	
	public Long getRoomCount() {
		return roomCount;
	}

	public void setRoomCount(Long roomCount) {
		this.roomCount = roomCount;
	}
	
	public Integer getRooms() {
		return rooms;
	}

	public void setRooms(Integer rooms) {
		this.rooms = rooms;
	}
	
	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}
	public Integer getSourceId() {
		return sourceId;
	}

	public void setSourceId(Integer sourceId) {
		this.sourceId = sourceId;
	}
	
	public Integer getTax() {
		return tax;
	}

	public void setTax(Integer tax) {
		this.tax = tax;
	}
	
	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}
    
	public double getTotalTax() {
		return totalTax;
	}

	public void setTotalTax(double totalTax) {
		this.totalTax = totalTax;
	}
	
	public double getSecurityDeposit() {
		return securityDeposit;
	}

	public void setSecurityDeposit(double securityDeposit) {
		this.securityDeposit = securityDeposit;
	}
	
	public  String getChartDate() {
		return chartDate;
	}

	public void setChartDate(String chartDate) {
		this.chartDate = chartDate;
	}
	
	public PmsBooking getBooking() {
		return booking;
	}

	public void setBooking(PmsBooking booking) {
		this.booking = booking;
	}

	public List<PmsBooking> getBookingList() {
		return bookingList;
	}

	public void setBookingList(List<PmsBooking> bookingList) {
		this.bookingList = bookingList;
	}
	
	public List<PropertyAccommodation> getAccommodationList() {
		return accommodationList;
	}

	public void setAccomadationList(List<PropertyAccommodation> accommodationList) {
		this.accommodationList = accommodationList;
	}
	
	public List<PmsAvailableRooms> getAvailableList() {
		return availableList;
	}

	public void setAvailableList(List<PmsAvailableRooms> availableList) {
		this.availableList = availableList;
	}
	
    public List<BookingListAction> getArray() {
	        return array;
	 }
	    
	 public void setArray(List<BookingListAction> array) {
	        this.array = array;
	  }
		
	 public List<BookingListAction> getTypes() {
	        return types;
	 }
	    
	 public void setTypes(List<BookingListAction> types) {
	        this.types = types;
	  }
	 
	 public String getStrDate() {
			return strDate;
	  }

	 public void setStrDate(String strDate) {
			this.strDate = strDate;
	  }
	 
	 public String getAccommodationTypeId() {
			return accommodationTypeId;
		}

		public void setAccommodationTypeId(String accommodationTypeId) {
			this.accommodationTypeId = accommodationTypeId;
		}

		public Timestamp getDateChart() {
			return dateChart;
		}

		public void setDateChart(Timestamp dateChart) {
			this.dateChart = dateChart;
		}
		
		
		public String getStrStartDate() {
			return strStartDate;
		}

		public void setStrStartDate(String strStartDate) {
			this.strStartDate = strStartDate;
		}

		public String getStrEndDate() {
			return strEndDate;
		}

		public void setStrEndDate(String strEndDate) {
			this.strEndDate = strEndDate;
		}
		
		public String getStrArrivalDate() {
			return strArrivalDate;
		}

		public void setStrArrivalDate(String strArrivalDate) {
			this.strArrivalDate = strArrivalDate;
		}

		public String getStrDepartureDate() {
			return strDepartureDate;
		}

		public void setStrDepartureDate(String strDepartureDate) {
			this.strDepartureDate = strDepartureDate;
		}
		
		public String getStartDate() {
			return startDate;
		}

		public void setStartDate(String startDate) {
			this.startDate = startDate;
		}

		public String getEndDate() {
			return endDate;
		}

		public void setEndDate(String endDate) {
			this.endDate = endDate;
		}
		
		public String getInvoiceId() {
			return invoiceId;
		}

		public void setInvoiceId(String invoiceId) {
			this.invoiceId = invoiceId;
		}	
		
		
	    public Integer getPropertyAccommodationId() {
			return propertyAccommodationId;
		}

		public void setPropertyAccommodationId(Integer propertyAccommodationId) {
			this.propertyAccommodationId = propertyAccommodationId;
		}

		public String getStartBlockDate() {
			return startBlockDate;
		}

		public void setStartBlockDate(String startBlockDate) {
			this.startBlockDate = startBlockDate;
		}

		public String getEndBlockDate() {
			return endBlockDate;
		}

		public void setEndBlockDate(String endBlockDate) {
			this.endBlockDate = endBlockDate;
		}

		public boolean isBlockFlag() {
			return blockFlag;
		}
		
		public void setBlockFlag(boolean blockFlag) {
			this.blockFlag = blockFlag;
		}
		
		public Integer getAreaId() {
			return areaId;
		}

		public void setAreaId(Integer areaId) { 
			this.areaId = areaId;
		}

		public String getOtaBookingId() {
			return otaBookingId;
		}

		public void setOtaBookingId(String otaBookingId) {
			this.otaBookingId = otaBookingId;
		}
		
		public boolean isStatusFlag() {
			return statusFlag;
		}

		public void setStatusFlag(boolean statusFlag) {
			this.statusFlag = statusFlag;
		}
		
		public String getDisplayHoursForTimer() {
			return displayHoursForTimer;
		}

		public void setDisplayHoursForTimer(String displayHoursForTimer) {
			this.displayHoursForTimer = displayHoursForTimer;
		}
		
	public Integer getCurrentHours(Date date,int lastMinuteHours){
    	 Calendar calendar=Calendar.getInstance();
    	 LocalTime currentTime = LocalTime.now();
    	 int currentHours = currentTime.getHour();
    	 
    	
 		String startDate = new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime());
 		String endDate = new SimpleDateFormat("yyyy-MM-dd").format(date);
 		
    	 DateTime start = DateTime.parse(startDate);
         DateTime end = DateTime.parse(endDate);
         java.util.Date loginDate = start.toDate();
 		 java.util.Date fromDate = end.toDate();
 		
         long diff = fromDate.getTime() - loginDate.getTime();
         
         long diffHours = diff / (60 * 60 * 1000);
         int diffInDays = (int) ((fromDate.getTime() - loginDate.getTime()) / (1000 * 60 * 60 * 24));
    	 int diffInHours=(int)diffHours;
    	 int gethours=lastMinuteHours-currentHours;
    	 int totalHours=0;
    	 if(diffInDays>1){
    		 totalHours= diffInDays*diffInHours;
    		 gethours=lastMinuteHours-currentHours;
    	 }
    	 else if(diffInDays==1){
    		 gethours=lastMinuteHours-currentHours;
    	 }
		 return gethours;
	 }
	 
	 public Boolean bookGetRooms(int availableCount,Double getRoom,Double bookRoom){
		 Double totalGetRooms=0.0;
	     totalGetRooms=bookRoom+getRoom;
	     Integer intTotalGetRooms=totalGetRooms.intValue();
	    	if(intTotalGetRooms<=availableCount){
	    		sessionMap.put("promotionsRoomCount", intTotalGetRooms);
	   	     	sessionMap.put("promotionsRoomNight", "Rooms");
	    		 return true;
	    	}
	    	else{
	    		 return false;
	    	}

		
	 }
	 
	 public Boolean bookGetNights(int availableCount, Double getNight,Double bookNight,int diffDays){
		 Double totalGetNights=0.0;
		 totalGetNights=getNight+bookNight;
	     Integer intTotalGetNights=totalGetNights.intValue();
	     int roomCountDays=0;
	     roomCountDays=availableCount*diffDays;
	     if(intTotalGetNights<=roomCountDays){
    		 return true;
    	}
    	else{
    		 return false;
    	}
	 }
	 
	 public Boolean getCurrentAvailable(int PropertyId,Timestamp start,Timestamp end,int lastMinutesHours,long available,int accommmodationId,Timestamp lastMinuteStart,Timestamp lastMinuteEnd)throws Exception{
		 int currentHours=0,AMPM=0,startHours=0,endHours=0,lastMinStartHours=0;
		 Timestamp lastStartDate=null,lastEndDate=null; 
		 Timestamp tsFromdate=null,tsTodate=null;
		 SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		 SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
		 String checkFromDate = new SimpleDateFormat("yyyy-MM-dd").format(lastMinuteStart);
     	 String strCheckInTime=checkFromDate+" 10:00:00"; 
    	 java.util.Date StartDate = sdf.parse(strCheckInTime);
		 lastStartDate=new Timestamp(StartDate.getTime());
		 
		 String checkToDate = new SimpleDateFormat("yyyy-MM-dd").format(lastMinuteEnd);
     	 String strCheckOutTime=checkToDate+" 08:00:00"; 
    	 java.util.Date EndDate = sdf.parse(strCheckOutTime);
		 lastEndDate=new Timestamp(EndDate.getTime());
		 
		 Calendar calStart=Calendar.getInstance();
		 calStart.setTime(lastStartDate);
		 startHours=calStart.get(Calendar.HOUR);
     	 calStart.add(Calendar.HOUR,-lastMinutesHours);
     	 lastMinStartHours=calStart.get(Calendar.HOUR);
	
     	 String strFromDate = new SimpleDateFormat("yyyy-MM-dd").format(calStart.getTime());
     	 java.util.Date fromDate = format1.parse(strFromDate);
     	 if(lastMinutesHours>0){
     		tsFromdate=new Timestamp(fromDate.getTime());
     	 }else{
     		tsFromdate=start;
     	 }
     	 
     	 
		 Calendar calEnd=Calendar.getInstance();
		 calEnd.setTime(lastEndDate);
		 endHours=calEnd.get(Calendar.HOUR);
		 String strToDate = new SimpleDateFormat("yyyy-MM-dd").format(calEnd.getTime());
     	 java.util.Date toDate = format1.parse(strToDate);
     	 if(lastMinutesHours>0){
     		tsTodate=new Timestamp(toDate.getTime());
     	 }else{
     		tsTodate=end;
     	 }
     	
		 Calendar calendar=Calendar.getInstance();
		 currentHours = calendar.get(Calendar.HOUR);
		 AMPM=calendar.get(Calendar.AM_PM);
		 
		 Calendar today=Calendar.getInstance();
     	 String strCurrentDate = new SimpleDateFormat("yyyy-MM-dd").format(today.getTime());
     	 java.util.Date currentDate = format1.parse(strCurrentDate);
     	 
     	 Calendar calArrivalDate=Calendar.getInstance();
     	 calArrivalDate.setTime(start);
     	 String strArrivalDate = new SimpleDateFormat("yyyy-MM-dd").format(calArrivalDate.getTime());
     	 java.util.Date ArrivalDate = format1.parse(strArrivalDate);
		 
		 /*long diff = toDate.getTime() - fromDate.getTime();
 		 long diffHours = diff / (60 * 60 * 1000);
 		 int diffInDays = (int) ((toDate.getTime() - fromDate.getTime()) / (1000 * 60 * 60 * 24));
 		 int diffInHours=(int)diffHours;
 		 boolean lastMinuteActive=false;*/
 		 
 		 
 		if(currentDate.equals(ArrivalDate)){
 			if(fromDate.equals(currentDate)){
 	 			 if(AMPM==0){// If current time is AM 
 		   			if(currentHours==lastMinStartHours){
 		   				 return true;
 		   			}else if(currentHours>=lastMinStartHours){
 		   				long minimumCount = getAvailableCount(getPropertyId(),tsFromdate,tsTodate,available,accommmodationId);
 		   				if((int)minimumCount>0){
 		   					return true;
 		   				}else{
 		   					return false;
 		   				}
 		   			}
 		   		 }
 		   		 else if(AMPM==1){// if current time is PM
 		   			 if(currentHours<=12){
 		   				 long minimumCount = getAvailableCount(getPropertyId(),tsFromdate,tsTodate,available,accommmodationId);
 		   				 if((int)minimumCount>0){
 		   					 return true;
 		   				 }else{
 		   					return false; 
 		   				 }
 		   			}
 		   		}
 	 		 } else if (currentDate.after(fromDate) && currentDate.before(toDate)) {
 	 			 if(AMPM==0){// If current time is AM 
 		   			 if(currentHours<=lastMinStartHours){
 		   				 return true;
 		   			}else if(currentHours>=lastMinStartHours){
 		   				long minimumCount = getAvailableCount(getPropertyId(),tsFromdate,tsTodate,available,accommmodationId);
 		   				if((int)minimumCount>0){
 		   					return true;
 		   				}else{
 		   					return false;
 		   				}
 		   			}
 		   		 }
 		   		 else if(AMPM==1){// if current time is PM
 		   			 if(currentHours<=12){
 		   				 long minimumCount = getAvailableCount(getPropertyId(),tsFromdate,tsTodate,available,accommmodationId);
 		   				 if((int)minimumCount>0){
 		   					 return true;
 		   				 }else{
 		   					return false; 
 		   				 }
 		   			}
 		   		 }
 	 			
 	 		 }else if (toDate.equals(currentDate)) {
 	 			 if(AMPM==0){// If current time is AM 
 		   			 if(currentHours<=lastMinStartHours){
 		   				 return true;
 		   			}else if(currentHours>=lastMinStartHours){
 		   				long minimumCount = getAvailableCount(getPropertyId(),tsFromdate,tsTodate,available,accommmodationId);
 		   				if((int)minimumCount>0){
 		   					return true;
 		   				}else{
 		   					return false;
 		   				}
 		   			}
 		   		 }
 		   		 else if(AMPM==1){// if current time is PM
 		   			 if(currentHours<=endHours){
 		   				 long minimumCount = getAvailableCount(getPropertyId(),tsFromdate,tsTodate,available,accommmodationId);
 		   				 if((int)minimumCount>0){
 		   					 return true;
 		   				 }else{
 		   					return false; 
 		   				 }
 		   			}
 		   		 }
 	  		 }
 		}
 		 
		 return false;
	 }

	 public Boolean checkLastMinutePromos(String promotionHoursRange,Timestamp tsArrivalDate,Timestamp tsDepartureDate,Timestamp tsStartRange,Timestamp tsEndRange,Timestamp tsFirstRangeDate,Timestamp tsSecondRangeDate){

			boolean blnCheck=false;
			try{
				ArrayList<String> arlDate=new ArrayList<String>();
				int intCount=0,lastCurrentHours=0,currentAMPM=0,firstRangeAMPM=0,secondRangeAMPM=0;
				int firstRange=0,secondRange=0,lastFirstRangeHours=0,lastSecondRangeHours=0;
				String[] arrRange=promotionHoursRange.split("-");
				ArrayList<String> arlRange=new ArrayList<String>();
				for(String str:arrRange){
					arlRange.add(str);
				}
		        Iterator<String> rangeIter=arlRange.iterator();
			    while(rangeIter.hasNext()) {
		            firstRange=Integer.parseInt(rangeIter.next());
		            secondRange=Integer.parseInt(rangeIter.next());
			    }
			    
				Integer startRangeHours=0,startRangeAMPM=0,endRangeHours=0,endRangeAMPM=0;
//				java.util.Date curdate=new java.util.Date();
				java.util.Date currentdate=new java.util.Date();
				Calendar calCurrentDate=Calendar.getInstance();
				calCurrentDate.setTime(currentdate);
				calCurrentDate.setTimeZone(TimeZone.getTimeZone("IST"));
				java.util.Date curdate=calCurrentDate.getTime();
				
				Timestamp tsStartDate=null,tsEndDate=null; 
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
		   	    
		   	    String checkArrivalDate = new SimpleDateFormat("yyyy-MM-dd").format(tsArrivalDate);
		   	    java.util.Date arrivalDate = format1.parse(checkArrivalDate);
				String checkDepartureDate = new SimpleDateFormat("yyyy-MM-dd").format(tsDepartureDate);
		   	    java.util.Date departureDate = format1.parse(checkDepartureDate);
		   	    
		   	    String strStartRangeDate = new SimpleDateFormat("yyyy-MM-dd").format(tsFirstRangeDate);
		   	    String strStartRange= sdf.format(tsStartRange);
		   	    java.util.Date dteStartRangeDate = sdf.parse(strStartRange);
		   	    Calendar calRangeStart=Calendar.getInstance();
		   	    calRangeStart.setTime(dteStartRangeDate);
		   	    calRangeStart.setTimeZone(TimeZone.getTimeZone("IST"));
		    	startRangeHours=calRangeStart.get(Calendar.HOUR_OF_DAY);
		    	startRangeAMPM=calRangeStart.get(Calendar.AM_PM);
		    	
				String strEndRangeDate = new SimpleDateFormat("yyyy-MM-dd").format(tsSecondRangeDate);
				String strEndRange = sdf.format(tsEndRange);
		   	    java.util.Date dteEndRangeDate = sdf.parse(strEndRange);
		   	    Calendar calRangeEnd=Calendar.getInstance();
		   	    calRangeEnd.setTimeZone(TimeZone.getTimeZone("IST"));
		   	    calRangeEnd.setTime(dteEndRangeDate);
		    	endRangeHours=calRangeEnd.get(Calendar.HOUR_OF_DAY);
		    	endRangeAMPM=calRangeEnd.get(Calendar.AM_PM);

		    	String checkArrival = new SimpleDateFormat("yyyy-MM-dd").format(arrivalDate);
		    	String strArrivalTime=checkArrival+" "+startRangeHours+":00:00"; 
		   	    java.util.Date dteArrivalDate = sdf.parse(strArrivalTime);
				Calendar calArrivalRange=Calendar.getInstance();
				calArrivalRange.setTime(dteArrivalDate);
				calArrivalRange.setTimeZone(TimeZone.getTimeZone("IST"));
		    	String strArrivalDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calArrivalRange.getTime());
		    	java.util.Date dteRangeArrival = sdf.parse(strArrivalDate);
		    	arlDate.add(checkArrival);
		    	
		    	Calendar today=Calendar.getInstance();
		    	String strTodayDate = new SimpleDateFormat("yyyy-MM-dd").format(today.getTime());
		    	java.util.Date todayDate = format1.parse(strTodayDate);
		    	
		    	DateTime start = DateTime.parse(strStartRangeDate);
		 	    DateTime end = DateTime.parse(strEndRangeDate);
		    	List<DateTime> between = DateUtil.getDateRange(start, end);
				for(DateTime d : between)
			    {
					java.util.Date availDate = d.toDate();
					String strCurrentDate = new SimpleDateFormat("yyyy-MM-dd").format(availDate);
			   	    java.util.Date currentDate = format1.parse(strCurrentDate);
					if(currentDate.equals(todayDate)){
						
						if((int)startRangeAMPM==1 && (int)endRangeAMPM==0){
							
							String checkStartDate = new SimpleDateFormat("yyyy-MM-dd").format(arrivalDate);
					    	String strStartTime=checkStartDate+" 12:00:00"; 
					   	    java.util.Date dteStartDate = sdf.parse(strStartTime);
					   	    Calendar calStart=Calendar.getInstance();
					   	    calStart.setTime(dteStartDate);
					   	    calStart.setTimeZone(TimeZone.getTimeZone("IST"));
					   	    String StartDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calStart.getTime());
					    	java.util.Date dteRangeStart = sdf.parse(StartDate);

					    	
				    		Calendar calFirst=Calendar.getInstance();
							calFirst.setTime(dteRangeStart);
							calFirst.setTimeZone(TimeZone.getTimeZone("IST"));
							calFirst.add(Calendar.HOUR, -firstRange);
							String FirstDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calFirst.getTime());
					    	java.util.Date dteRangeFirst = sdf.parse(FirstDate);
					    	Calendar calFirstRange=Calendar.getInstance();
					    	calFirstRange.setTime(dteRangeFirst);
					    	
					    	Calendar calSecond=Calendar.getInstance();
					    	calSecond.setTime(dteRangeStart);
					    	calSecond.setTimeZone(TimeZone.getTimeZone("IST"));
					    	calSecond.add(Calendar.HOUR, -secondRange);
							String SecondDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calSecond.getTime());
					    	java.util.Date dteRangeSecond = sdf.parse(SecondDate);
					    	Calendar calSecondRange=Calendar.getInstance();
					    	calSecondRange.setTime(dteRangeSecond);
					    	
							/*Calendar calFirst=Calendar.getInstance();
							calFirst.setTime(arrivalDate);
							calFirst.add(Calendar.HOUR, -firstRange);
							String checkStartDate = new SimpleDateFormat("yyyy-MM-dd").format(arrivalDate);
					    	String strStartTime=checkStartDate+" "+startRangeHours+":00:00"; 
					   	    java.util.Date dteStartDate = sdf.parse(strStartTime);
							Calendar calStartRange=Calendar.getInstance();
							calStartRange.setTime(dteStartDate);
							calStartRange.add(Calendar.DAY_OF_MONTH, -1);
					    	String FromDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calStartRange.getTime());
					    	java.util.Date dteRangeFrom = sdf.parse(FromDate);
					    	
					    	Calendar calSecond=Calendar.getInstance();
					    	calSecond.setTime(arrivalDate);
					    	calSecond.add(Calendar.HOUR, -secondRange);
							
					    	String checkEndDate = new SimpleDateFormat("yyyy-MM-dd").format(arrivalDate);
					    	String strEndTime=checkStartDate+" "+endRangeHours+":00:00"; 
					   	    java.util.Date dteEndDate = sdf.parse(strEndTime);
					    	Calendar calEndRange=Calendar.getInstance();
					    	calEndRange.setTime(dteEndDate);
					    	String ToDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calEndRange.getTime());
					    	java.util.Date dteRangeTo = sdf.parse(ToDate);*/
					    	
					    	if(curdate.after(calSecond.getTime()) && curdate.before(calFirst.getTime())) {
					    		blnCheck=true;
					    		
					    		String displayDate = new SimpleDateFormat("MMM dd,yyyy HH:mm:ss").format(calFirst.getTime());
					    		long diff =  dteRangeFirst.getTime()-curdate.getTime();

								long diffSeconds = diff / 1000 % 60;
								long diffMinutes = diff / (60 * 1000) % 60;
								long diffHours = diff / (60 * 60 * 1000) % 24;
								long diffDays = diff / (24 * 60 * 60 * 1000);
								
								this.displayHoursForTimer=displayDate;
								setDisplayHoursForTimer(this.displayHoursForTimer);
					    	}
				    	}else{
				    		
				    		String checkStartDate = new SimpleDateFormat("yyyy-MM-dd").format(arrivalDate);
					    	String strStartTime=checkStartDate+" 12:00:00"; 
					   	    java.util.Date dteStartDate = sdf.parse(strStartTime);
					   	    Calendar calStart=Calendar.getInstance();
					   	    calStart.setTime(dteStartDate);
					   	    calStart.setTimeZone(TimeZone.getTimeZone("IST"));
					   	    String StartDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calStart.getTime());
					    	java.util.Date dteRangeStart = sdf.parse(StartDate);

					    	
				    		Calendar calFirst=Calendar.getInstance();
							calFirst.setTime(dteRangeStart);
							calFirst.setTimeZone(TimeZone.getTimeZone("IST"));
							calFirst.add(Calendar.HOUR, -firstRange);
							String FirstDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calFirst.getTime());
					    	java.util.Date dteRangeFirst = sdf.parse(FirstDate);
					    	Calendar calFirstRange=Calendar.getInstance();
					    	calFirstRange.setTime(dteRangeFirst);
					    	
				    		/*String checkStartDate = new SimpleDateFormat("yyyy-MM-dd").format(arrivalDate);
					    	String strStartTime=checkStartDate+" "+startRangeHours+":00:00"; 
					   	    java.util.Date dteStartDate = sdf.parse(strStartTime);
							Calendar calStartRange=Calendar.getInstance();
							calStartRange.setTime(dteStartDate);
					    	String FromDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calStartRange.getTime());
					    	java.util.Date dteRangeFrom = sdf.parse(FromDate);*/
					    	
					    	Calendar calSecond=Calendar.getInstance();
					    	calSecond.setTime(dteRangeStart);
					    	calSecond.setTimeZone(TimeZone.getTimeZone("IST"));
					    	calSecond.add(Calendar.HOUR, -secondRange);
							String SecondDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calSecond.getTime());
					    	java.util.Date dteRangeSecond = sdf.parse(SecondDate);
					    	Calendar calSecondRange=Calendar.getInstance();
					    	calSecondRange.setTime(dteRangeSecond);
					    	
					    	/*String checkEndDate = new SimpleDateFormat("yyyy-MM-dd").format(arrivalDate);
					    	String strEndTime=checkStartDate+" "+endRangeHours+":00:00"; 
					   	    java.util.Date dteEndDate = sdf.parse(strEndTime);
					    	Calendar calEndRange=Calendar.getInstance();
					    	calEndRange.setTime(dteEndDate);
					    	String ToDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calEndRange.getTime());
					    	java.util.Date dteRangeTo = sdf.parse(ToDate);*/
					    	
					    	if(curdate.after(calSecond.getTime()) && curdate.before(calFirst.getTime())) {
				    			blnCheck=true;
					    		
					    		String displayDate = new SimpleDateFormat("MMM dd,yyyy HH:mm:ss").format(calFirst.getTime());
					    		long diff =  dteRangeFirst.getTime()-curdate.getTime();

								long diffSeconds = diff / 1000 % 60;
								long diffMinutes = diff / (60 * 1000) % 60;
								long diffHours = diff / (60 * 60 * 1000) % 24;
								long diffDays = diff / (24 * 60 * 60 * 1000);
								
								this.displayHoursForTimer=displayDate;
								setDisplayHoursForTimer(this.displayHoursForTimer);
					    	}
				    	}
					}
			    }
			}catch(Exception e){
				logger.equals(e);
				e.printStackTrace();
			}
			return blnCheck;

	 }
		
		public Boolean checkEarlyBirdPromos(Timestamp bookedDate,Timestamp earlyStartDate,Timestamp earlyEndDate,Timestamp arrival,Timestamp departure){
			boolean blnCheck=false;
			try{
				java.util.Date curdate=new java.util.Date();
				Timestamp tsStartDate=null,tsEndDate=null; 
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
				String checkFromDate = new SimpleDateFormat("yyyy-MM-dd").format(earlyStartDate);
		   	    java.util.Date StartDate = format1.parse(checkFromDate);
				String checkToDate = new SimpleDateFormat("yyyy-MM-dd").format(earlyEndDate);
		   	    java.util.Date EndDate = format1.parse(checkToDate);
				
		   	    String checkArrivalDate = new SimpleDateFormat("yyyy-MM-dd").format(arrival);
		   	    java.util.Date arrivalDate = format1.parse(checkArrivalDate);
				String checkDepartureDate = new SimpleDateFormat("yyyy-MM-dd").format(departure);
		   	    java.util.Date departureDate = format1.parse(checkDepartureDate);
		   	    
		   	    String checkBookedDate = new SimpleDateFormat("yyyy-MM-dd").format(departure);
		   	    java.util.Date dteBookedDate = format1.parse(checkBookedDate);
		   	    
		   	    DateTime start = DateTime.parse(checkFromDate);
		 	    DateTime end = DateTime.parse(checkToDate);
		    	List<DateTime> between = DateUtil.getDateRange(start, end);
				for(DateTime d : between)
			    {
					java.util.Date availDate = d.toDate();
					String strCurrentDate = new SimpleDateFormat("yyyy-MM-dd").format(curdate);
			   	    java.util.Date currentDate = format1.parse(strCurrentDate);
					if(currentDate.equals(dteBookedDate) && arrivalDate.equals(availDate)){
						
						blnCheck=true;
					}
			    }
				
			}catch(Exception e){
				logger.error(e);
				e.printStackTrace();
			}
			return blnCheck;
		}
		public Integer getSourceTypeId() {
			return sourceTypeId;
		}

		public void setSourceTypeId(Integer sourceTypeId) {
			this.sourceTypeId = sourceTypeId;
		}
		
		public Integer getPartnerPropertyId() {
			return partnerPropertyId;
		}

		public void setPartnerPropertyId(Integer partnerPropertyId) {
			this.partnerPropertyId = partnerPropertyId;
		}
		
		public Integer getSelectPartnerPropertyId() {
			return selectPartnerPropertyId;
		}

		public void setSelectPartnerPropertyId(Integer selectPartnerPropertyId) {
			this.selectPartnerPropertyId = selectPartnerPropertyId;
		}
		
		public static List<DateTime> getDateRange(DateTime start, DateTime end) {

	        List<DateTime> ret = new ArrayList<DateTime>();
	        DateTime tmp = start;
	        while(tmp.isBefore(end) || tmp.equals(end)) {
	            ret.add(tmp);
	            tmp = tmp.plusDays(1);
	        }
	        return ret;
	    }
		
		public String getRouteMapSms(String routeMap, String phone)throws Exception{
			
			String number = phone;
			String message = "&sms_text=" + routeMap ;


			//String sender = "&sender=" + "TXTLCL";
			String numbers = "&sms_to=" + "+91"+number ;
			String from = "&sms_from=" + "ULOHTL";
			String type = "&sms_type=" + "trans"; 
			// Send data
			HttpURLConnection conn = (HttpURLConnection) new URL("https://secureets.knowlarity.com/ets/dom/clients/ulohotels/api/ulohotels_sms_api.py").openConnection();
			//"http://etsrds.kapps.in/webapi/ulohotels/api/ulohotels_sms_api.py?sms_text=hi%20this%20gopinath%20from%20ulo&sms_to=%20918220211068&sms_from=ULOHTL&sms_type=promo"
			String data = numbers + message +  from + type;
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.addRequestProperty("auth_key", "Basic dWxvaG90ZWxzX1Q6VUxPSFRMQDIwMTg=");
			conn.setRequestProperty("Content-Length", Integer.toString(data.length()));
			conn.getOutputStream().write(data.getBytes("UTF-8"));
			
			final BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			final StringBuffer stringBuffer = new StringBuffer();
			/*String line;
			
			while ((line = rd.readLine()) != null) {
				stringBuffer.append(line);
				
			}*/
			rd.close();
			
			return null;
			
		
		}
}
