package com.ulopms.view;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;




//import com.medwecare.controller.AwsObjectManager;
//import com.medwecare.controller.CommentManager;
//import com.medwecare.controller.CommentUserManager;
////import com.medwecare.controller.NotificationManager;
//import com.medwecare.controller.ProjectManager;

//import com.medwecare.model.AwsObject;
//import com.medwecare.model.Notification;
//import com.medwecare.model.ObjectComment;
//import com.medwecare.model.ObjectCommentUser;
//import com.medwecare.model.Project;








































import com.opensymphony.xwork2.ActionSupport;

import freemarker.template.Configuration;
import freemarker.template.Template;

import javax.servlet.http.HttpServletRequest;

import com.opensymphony.xwork2.ModelDriven;
import com.ulopms.api.Property;
import com.ulopms.controller.BookingDetailManager;
import com.ulopms.controller.PmsGuestManager;
import com.ulopms.controller.PmsPropertyManager;
import com.ulopms.controller.PropertyAccommodationManager;
import com.ulopms.controller.PropertyDiscountManager;
import com.ulopms.controller.PropertyItemCategoryManager;
import com.ulopms.controller.PropertyItemManager;
import com.ulopms.controller.PropertyTaxeManager;
import com.ulopms.controller.UserLoginManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.BookingDetail;
import com.ulopms.model.DashBoard;
import com.ulopms.model.GooglePropertyAreas;
import com.ulopms.model.PmsGuest;
import com.ulopms.model.PmsProperty;
import com.ulopms.model.PropertyAccommodation;
import com.ulopms.model.PropertyDiscount;
import com.ulopms.model.PropertyItemCategory;
import com.ulopms.model.PropertyItem;
import com.ulopms.model.PropertyTags;
import com.ulopms.model.PropertyTaxe;
import com.ulopms.model.User;
import com.ulopms.util.Email;
import com.ulopms.util.Image;

public class PropertyDiscountAction extends ActionSupport implements
ServletRequestAware,SessionAware, UserAware{
     
	

	private static final long serialVersionUID = 914982665758390091L;
    
	private Integer propertyDiscountId;
	private Integer propertyId;
	private String  discountName;
	private String  discountType;
	private Timestamp startDate;
	private Timestamp endDate;
	private Integer discountUnits;
	private Boolean isSignup;
	private double discountPercentage;
    private String strStartDate;
    private String strEndDate;
    private String couponCode;
    private String locationUrl;
	private double discountInr;
	private Double couponDiscount;
	private Double maximumDiscount; 
	private String couponType;
	
	private PropertyDiscount discount;
	
	private List<PropertyDiscount> DiscountList;
	
	private List<PropertyDiscount> SignupActiveList;
	

	

	private static final Logger logger = Logger.getLogger(PropertyDiscountAction.class);

	private HttpSession session;

	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}
	
	private SessionMap<String,Object> sessionMap;  
	  
	//getters and setters  
	  
	@Override  
	public void setSession(Map<String, Object> map) {  
	    sessionMap=(SessionMap)map;  
	}  
	
	
	private User user;

	
	// @Override
	// public User getModel() {
	// return user;
	// }

	@Override
	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}
	

	
	private HttpServletRequest request;

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	public PropertyDiscountAction() {

	}

	public String execute() {

		//this.patientId = getPatientId();
		return SUCCESS;
	}

	
	
/*	public String getTaxes() throws IOException {
		
		this.propertyId = (Integer) sessionMap.get("propertyId");
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PropertyTaxeManager taxController = new PropertyTaxeManager();
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");

			this.taxeList = taxController.list(getPropertyId());
			for (PropertyTaxe taxe : taxeList) {

				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"taxId\":\"" + taxe.getPropertyTaxId() + "\"";
				jsonOutput += ",\"taxName\":\"" + taxe.getTaxName()+ "\"";
				jsonOutput += "}";


			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}  
	
	public String getDiscount() throws IOException {
		
		
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PropertyItemManager itemController = new PropertyItemManager();
			PropertyItemCategoryManager categoryController = new PropertyItemCategoryManager();
			PropertyTaxeManager taxController =  new PropertyTaxeManager();
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");

			PropertyItem item = itemController.find(getPropertyItemId());
			
				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"itemId\":\"" + item.getPropertyItemId() + "\"";
				jsonOutput += ",\"itemCode\":\"" + item.getItemCode()+ "\"";
				jsonOutput += ",\"itemName\":\"" + item.getItemName()+ "\"";
				jsonOutput += ",\"itemDescription\":\"" + item.getItemDescription()+ "\"";
				jsonOutput += ",\"itemSku\":\"" + item.getSku()+ "\"";
				jsonOutput += ",\"itemAmount\":\"" + item.getAmount()+ "\"";
				jsonOutput += ",\"propertyItemCategoryId\":\"" + item.getPropertyItemCategory().getPropertyItemCategoryId()+ "\"";
				PropertyItemCategory category =  categoryController.find(item.getPropertyItemCategory().getPropertyItemCategoryId());
				jsonOutput += ",\"propertyItemCategoryName\":\"" + category.getCategoryName()+ "\"";
					
				jsonOutput += ",\"propertyTaxeId\":\"" + item.getPropertyTaxe().getPropertyTaxId()+ "\"";
				PropertyTaxe tax = taxController.find(item.getPropertyTaxe().getPropertyTaxId());
				jsonOutput += ",\"propertyTaxeName\":\"" + tax.getTaxName()+ "\"";
				
				jsonOutput += "}";

			

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}      */
	
	public String getDiscount() throws IOException {
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PropertyAccommodationManager propertyAccommodationController = new PropertyAccommodationManager();
			PropertyDiscountManager propertyDiscountController = new PropertyDiscountManager();

			PmsPropertyManager propertyController = new PmsPropertyManager();
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");

			PropertyDiscount discount = propertyDiscountController.find(getPropertyDiscountId());
			
				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"discountId\":\"" + discount.getPropertyDiscountId() + "\"";
				jsonOutput += ",\"discountName\":\"" + discount.getDiscountName()+ "\"";
				String checkIn = new SimpleDateFormat("MMMM d, yyyy",Locale.ENGLISH).format(discount.getStartDate());
				String checkOut = new SimpleDateFormat("MMMM d, yyyy",Locale.ENGLISH).format(discount.getEndDate());
				//jsonOutput += ",\"arrivalDate\":\"" + checkIn + "\"";
				//jsonOutput += ",\"departureDate\":\"" + checkOut+ "\"";
				jsonOutput += ",\"startDate\":\"" + checkIn + "\"";
				jsonOutput += ",\"endDate\":\"" + checkOut + "\"";
				jsonOutput += ",\"discountPercentage\":\"" + discount.getDiscountPercentage()+ "\"";
				jsonOutput += ",\"discountUnits\":\"" + discount.getDiscountUnits()+ "\"";
				jsonOutput += ",\"isSignup\":\"" + discount.getIsSignup()+ "\"";
				jsonOutput += ",\"discountType\":\"" + discount.getDiscountType()+ "\"";
				//jsonOutput += ",\"accommodationId\":\"" + discount.getPropertyAccommodation().getAccommodationId()+ "\"";
				
				jsonOutput += "}";


			

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	public String getCoupon(){

		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PropertyAccommodationManager propertyAccommodationController = new PropertyAccommodationManager();
			PropertyDiscountManager propertyDiscountController = new PropertyDiscountManager();

			PmsPropertyManager propertyController = new PmsPropertyManager();
			response.setContentType("application/json");

			PropertyDiscount discount = propertyDiscountController.find(getPropertyDiscountId());
			
			if(discount!=null){
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"discountId\":\"" + discount.getPropertyDiscountId() + "\"";
				jsonOutput += ",\"discountName\":\"" + (discount.getDiscountName()==null?"":discount.getDiscountName())+ "\"";
				jsonOutput += ",\"discountCode\":\"" + (discount.getDiscountCode()==null?"":discount.getDiscountCode())+ "\"";
				if(discount.getPmsProperty().getGoogleLocation()==null){
					jsonOutput += ",\"locationUrl\":\"" + ("NA")+ "\"";
				}else{
					jsonOutput += ",\"locationUrl\":\"" + (discount.getPmsProperty().getGoogleLocation().getGoogleLocationUrl()==null?"NA":discount.getPmsProperty().getGoogleLocation().getGoogleLocationUrl().trim())+ "\"";	
				}
				
				String checkIn = new SimpleDateFormat("MMMM d, yyyy",Locale.ENGLISH).format(discount.getStartDate());
				String checkOut = new SimpleDateFormat("MMMM d, yyyy",Locale.ENGLISH).format(discount.getEndDate());
				jsonOutput += ",\"startDate\":\"" + checkIn + "\"";
				jsonOutput += ",\"endDate\":\"" + checkOut + "\"";
				jsonOutput += ",\"discountUnits\":\"" + (discount.getDiscountUnits()==null?0:discount.getDiscountUnits().intValue())+ "\"";
				if(discount.getDiscountType().equalsIgnoreCase("inr")){
					if(discount.getDiscountPercentage()==0){
						Double dblInr=discount.getDiscountInr();
						jsonOutput += ",\"discountAmount\":\"" + (dblInr==null?0:dblInr.intValue())+ "\"";
						jsonOutput += ",\"discountType\":\"" + "INR"+ "\"";	
					}
				}
				
				if(discount.getDiscountType().equalsIgnoreCase("percent")){
					if(discount.getDiscountInr()==0){
						Double dblPercent=discount.getDiscountPercentage();
						jsonOutput += ",\"discountAmount\":\"" + (dblPercent==null?0:dblPercent.intValue())+ "\"";
						
						jsonOutput += ",\"discountType\":\"" + "Percent"+ "\"";
					}
				}
				
				jsonOutput += ",\"discountMaximumAmount\":\"" + (discount.getMaximumDiscountAmount()==null?0:discount.getMaximumDiscountAmount().intValue())+ "\"";
				jsonOutput += ",\"isSignup\":\"" + discount.getIsSignup()+ "\"";
				jsonOutput += ",\"couponType\":\"" + discount.getCouponType()+ "\"";
				
				jsonOutput += "}";
			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	
	}
	public String getCoupons(){
		
		try {
			PropertyDiscountManager propertyDiscountController = new PropertyDiscountManager();
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			java.util.Date date=new java.util.Date();
			
			response.setContentType("application/json");
			int count=0;
			this.DiscountList = propertyDiscountController.listCoupons(date);
			if(this.DiscountList.size()>0 && !this.DiscountList.isEmpty()){
				for (PropertyDiscount discounts : DiscountList) {

					count++;
					if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += ",{";
					else
						jsonOutput += "{";
					
					jsonOutput += "\"discountId\":\"" + discounts.getPropertyDiscountId() + "\"";
					jsonOutput += ",\"propertyId\":\"" + discounts.getPmsProperty().getPropertyId() + "\"";
					jsonOutput += ",\"propertyName\":\"" + discounts.getPmsProperty().getPropertyName() + "\"";
					jsonOutput += ",\"discountName\":\"" + (discounts.getDiscountName()==null?discounts.getDiscountCode():discounts.getDiscountName())+ "\"";
					jsonOutput += ",\"discountCode\":\"" + (discounts.getDiscountCode()==null?discounts.getDiscountName():discounts.getDiscountCode())+ "\"";
					String startDate=new SimpleDateFormat("dd-MMM-yyyy").format(discounts.getStartDate());
					String endDate=new SimpleDateFormat("dd-MMM-yyyy").format(discounts.getEndDate());
					jsonOutput += ",\"startDate\":\"" +startDate + "\"";
					jsonOutput += ",\"endDate\":\"" + endDate+ "\"";
					jsonOutput += ",\"discountUnits\":\"" + discounts.getDiscountUnits()+ "\"";
					if(discounts.getDiscountPercentage()==0){
						jsonOutput += ",\"discountAmount\":\"" + discounts.getDiscountPercentage()+ "\"";
						jsonOutput += ",\"discountType\":\"" + "INR"+ "\"";	
					}
					
					if(discounts.getDiscountInr()==0){
						jsonOutput += ",\"discountAmount\":\"" + discounts.getDiscountPercentage()+ "\"";
						jsonOutput += ",\"discountType\":\"" + "Percent"+ "\"";
					}
					
					jsonOutput += ",\"discountMaximumAmount\":\"" + discounts.getMaximumDiscountAmount()+ "\"";
					jsonOutput += ",\"isSignup\":\"" + discounts.getIsSignup()+ "\"";
					jsonOutput += ",\"isAllUsers\":\"" + discounts.getIsAllUsers()+ "\"";
					jsonOutput += ",\"serialNo\":\"" + count+ "\"";
					jsonOutput += "}";


				}	
			}
			

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	
	}
	
	public String getCouponsAPI(Property property){
		String output=null;
		try {
			PropertyDiscountManager propertyDiscountController = new PropertyDiscountManager();
			String jsonOutput = "";
			java.util.Date date=new java.util.Date();
			Calendar calDate=Calendar.getInstance();
        	calDate.setTime(date);
        	calDate.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
        	
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			String strBookingDate=new SimpleDateFormat("yyyy-MM-dd").format(calDate.getTime());
		    
			java.util.Date bookingdate=null;
			
			if(property.getBookingDate()==null){
				bookingdate = format.parse(property.getBookingDate());	
			}else{
				bookingdate = format.parse(strBookingDate);
			}
		    
			int count=0;
			this.DiscountList = propertyDiscountController.listPropertyCoupons(property.getPropertyId(),bookingdate);
			for (PropertyDiscount discounts : DiscountList) {

				count++;
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				
				jsonOutput += "\"discountId\":\"" + discounts.getPropertyDiscountId() + "\"";
				jsonOutput += ",\"discountName\":\"" + (discounts.getDiscountName()==null?"NA":discounts.getDiscountName())+ "\"";
				jsonOutput += ",\"discountCode\":\"" + (discounts.getDiscountCode()==null?"NA":discounts.getDiscountCode())+ "\"";
				String startDate=new SimpleDateFormat("dd-MMM-yyyy").format(discounts.getStartDate());
				String endDate=new SimpleDateFormat("dd-MMM-yyyy").format(discounts.getEndDate());
				jsonOutput += ",\"startDate\":\"" +startDate + "\"";
				jsonOutput += ",\"endDate\":\"" + endDate+ "\"";
				jsonOutput += ",\"discountUnits\":\"" + (discounts.getDiscountUnits()==null?0:discounts.getDiscountUnits())+ "\"";
				if(discounts.getDiscountPercentage()==0){
					jsonOutput += ",\"discountAmount\":\"" + discounts.getDiscountInr()+ "\"";
					jsonOutput += ",\"discountPercent\":\"" + 0 + "\"";
					jsonOutput += ",\"discountType\":\"" + "INR"+ "\"";	
				}
				
				if(discounts.getDiscountInr()==0){
					jsonOutput += ",\"discountAmount\":\"" + 0 + "\"";
					jsonOutput += ",\"discountPercent\":\"" + discounts.getDiscountPercentage()+ "\"";
					jsonOutput += ",\"discountType\":\"" + "Percent"+ "\"";
				}
				
				jsonOutput += ",\"discountMaximumAmount\":\"" + (discounts.getMaximumDiscountAmount()==null?0:discounts.getMaximumDiscountAmount())+ "\"";
				jsonOutput += ",\"isSignup\":\"" + discounts.getIsSignup()+ "\"";
				jsonOutput += ",\"isAllUsers\":\"" + discounts.getIsAllUsers()+ "\"";
				jsonOutput += ",\"serialNo\":\"" + count+ "\"";
				
				jsonOutput += "}";


			}

			String data="\"data\":[" + jsonOutput + "]";
 			String strData=data.replaceAll("\"", "\"");
 			
 			if(count == 0){
 				output="{\"error\":\"1\",\"message\":\"No Records Found\"}";
			}else{
				output="{\"error\":\"0\"," + strData + "}";			
			}
			

		} catch (Exception e) {
			output="{\"error\":\"2\",\"message\":\"Some technical error on property coupons api\"}";
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return output;

	
	}
	
	public String getVerifyCouponsAPI(Property property){
		String output=null;
		try {
			PropertyDiscountManager propertyDiscountController = new PropertyDiscountManager();
			String jsonOutput = "";
			java.util.Date date=new java.util.Date();
			Calendar calDate=Calendar.getInstance();
        	calDate.setTime(date);
        	calDate.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
        	
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			String strBookingDate=new SimpleDateFormat("yyyy-MM-dd").format(calDate.getTime());
		    java.util.Date bookingdate = format.parse(strBookingDate);
		    ArrayList<String> arlList=new ArrayList<String>();
			int count=0;
			this.DiscountList = propertyDiscountController.verifyCoupon(property.getCouponCode(), bookingdate,property.getPropertyId());
			for (PropertyDiscount discounts : DiscountList) {
				if(discounts.getCouponType()!=null){
					if(discounts.getCouponType().equalsIgnoreCase("UW") || discounts.getCouponType().equalsIgnoreCase("RD") || discounts.getCouponType().equalsIgnoreCase("OW")){
						count++;
						if(!arlList.contains(discounts.getDiscountCode())){
							arlList.add(discounts.getDiscountCode());
							
							if (!jsonOutput.equalsIgnoreCase(""))
								jsonOutput += ",{";
							else
								jsonOutput += "{";
							
							jsonOutput += "\"discountId\":\"" + discounts.getPropertyDiscountId() + "\"";
							jsonOutput += ",\"discountName\":\"" + (discounts.getDiscountName()==null?"NA":discounts.getDiscountName())+ "\"";
							jsonOutput += ",\"discountCode\":\"" + (discounts.getDiscountCode()==null?"NA":discounts.getDiscountCode())+ "\"";
							if(discounts.getDiscountPercentage()==0){
								jsonOutput += ",\"discountAmount\":\"" + discounts.getDiscountInr()+ "\"";
								jsonOutput += ",\"discountPercent\":\"" + 0 + "\"";
								jsonOutput += ",\"discountType\":\"" + "INR"+ "\"";	
							}
							
							if(discounts.getDiscountInr()==0){
								jsonOutput += ",\"discountAmount\":\"" + 0 + "\"";
								jsonOutput += ",\"discountPercent\":\"" + discounts.getDiscountPercentage()+ "\"";
								jsonOutput += ",\"discountType\":\"" + "Percent"+ "\"";
							}
							
							jsonOutput += ",\"discountMaximumAmount\":\"" + (discounts.getMaximumDiscountAmount()==null?0:discounts.getMaximumDiscountAmount())+ "\"";
							
							jsonOutput += "}";		
						}
					}
				}
				
			}

			String data="\"data\":[" + jsonOutput + "]";
 			String strData=data.replaceAll("\"", "\"");
 			
 			if(count == 0){
 				output="{\"error\":\"1\",\"message\":\"No Records Found\"}";
			}else{
				output="{\"error\":\"0\"," + strData + "}";			
			}
			

		} catch (Exception e) {
			output="{\"error\":\"2\",\"message\":\"Some technical error on verify coupon api\"}";
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return output;
	
	}
	
	public String getAllCouponsAPI(Property property){

		String output=null;
		try {
			PropertyDiscountManager propertyDiscountController = new PropertyDiscountManager();
			String jsonOutput = "";
			java.util.Date date=new java.util.Date();
			Calendar calDate=Calendar.getInstance();
        	calDate.setTime(date);
        	calDate.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
        	
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			String strBookingDate=new SimpleDateFormat("yyyy-MM-dd").format(calDate.getTime());
		    java.util.Date bookingdate = format.parse(strBookingDate);
		    ArrayList<String> arlList=new ArrayList<String>();
			int count=0;
			this.DiscountList = propertyDiscountController.listPropertyCoupons(bookingdate);
			for (PropertyDiscount discounts : DiscountList) {
				if(discounts.getCouponType()!=null){
					if(discounts.getCouponType().equalsIgnoreCase("RD") || discounts.getCouponType().equalsIgnoreCase("UW")){
						count++;
						if(!arlList.contains(discounts.getDiscountCode())){
							arlList.add(discounts.getDiscountCode());
							
							if (!jsonOutput.equalsIgnoreCase(""))
								jsonOutput += ",{";
							else
								jsonOutput += "{";
							
							jsonOutput += "\"discountId\":\"" + discounts.getPropertyDiscountId() + "\"";
							jsonOutput += ",\"discountName\":\"" + (discounts.getDiscountName()==null?"NA":discounts.getDiscountName())+ "\"";
							jsonOutput += ",\"discountCode\":\"" + (discounts.getDiscountCode()==null?"NA":discounts.getDiscountCode())+ "\"";
							if(discounts.getDiscountPercentage()==0){
								jsonOutput += ",\"discountAmount\":\"" + discounts.getDiscountInr()+ "\"";
								jsonOutput += ",\"discountPercent\":\"" + 0 + "\"";
								jsonOutput += ",\"discountType\":\"" + "INR"+ "\"";	
							}
							
							if(discounts.getDiscountInr()==0){
								jsonOutput += ",\"discountAmount\":\"" + 0 + "\"";
								jsonOutput += ",\"discountPercent\":\"" + discounts.getDiscountPercentage()+ "\"";
								jsonOutput += ",\"discountType\":\"" + "Percent"+ "\"";
							}
							
							jsonOutput += ",\"discountMaximumAmount\":\"" + (discounts.getMaximumDiscountAmount()==null?0:discounts.getMaximumDiscountAmount())+ "\"";
							jsonOutput += ",\"isSignup\":\"" + discounts.getIsSignup()+ "\"";
							jsonOutput += ",\"isAllUsers\":\"" + discounts.getIsAllUsers()+ "\"";
							jsonOutput += ",\"serialNo\":\"" + count+ "\"";
							
							jsonOutput += "}";		
						}
					}
				}
				
			}

			String data="\"data\":[" + jsonOutput + "]";
 			String strData=data.replaceAll("\"", "\"");
 			
 			if(count == 0){
 				output="{\"error\":\"1\",\"message\":\"No Records Found\"}";
			}else{
				output="{\"error\":\"0\"," + strData + "}";			
			}
			

		} catch (Exception e) {
			output="{\"error\":\"2\",\"message\":\"Some technical error on home page coupon api\"}";
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return output;
	
	}
	
	public String getDiscounts() throws IOException {
		
		this.propertyId = (Integer) sessionMap.get("propertyId");
		
		PropertyAccommodationManager propertyAccommodationController = new PropertyAccommodationManager();
		PropertyDiscountManager propertyDiscountController = new PropertyDiscountManager();

		PmsPropertyManager propertyController = new PmsPropertyManager();
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			
			response.setContentType("application/json");

			this.DiscountList = propertyDiscountController.list(getPropertyId());
			for (PropertyDiscount discounts : DiscountList) {

				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"discountId\":\"" + discounts.getPropertyDiscountId() + "\"";
				jsonOutput += ",\"discountName\":\"" + discounts.getDiscountName()+ "\"";
				jsonOutput += ",\"startDate\":\"" + discounts.getStartDate()+ "\"";
				jsonOutput += ",\"endDate\":\"" + discounts.getEndDate()+ "\"";
				jsonOutput += ",\"discountUnits\":\"" + discounts.getDiscountUnits()+ "\"";
				jsonOutput += ",\"discountPercentage\":\"" + discounts.getDiscountPercentage()+ "\"";
				jsonOutput += ",\"isSignup\":\"" + discounts.getIsSignup()+ "\"";
				//jsonOutput += ",\"accommodationId\":\"" + discounts.getPropertyAccommodation().getAccommodationId()+ "\"";
				
				jsonOutput += "}";


			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}   
	
	
public String getResortDiscount() throws IOException {
		
		this.propertyId = (Integer) sessionMap.get("propertyId");
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();// Now use today date.
		String todayDate = sdf.format(date.getTime());
		
		PropertyAccommodationManager propertyAccommodationController = new PropertyAccommodationManager();
		PropertyDiscountManager propertyDiscountController = new PropertyDiscountManager();

		PmsPropertyManager propertyController = new PmsPropertyManager();
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			
			response.setContentType("application/json");

			this.DiscountList = propertyDiscountController.listresort(getPropertyId(),todayDate);
			for (PropertyDiscount discounts : DiscountList) {

				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"discountId\":\"" + discounts.getPropertyDiscountId() + "\"";
				jsonOutput += ",\"discountName\":\"" + discounts.getDiscountName()+ "\"";
				jsonOutput += ",\"startDate\":\"" + discounts.getStartDate()+ "\"";
				jsonOutput += ",\"endDate\":\"" + discounts.getEndDate()+ "\"";
				//jsonOutput += ",\"discountUnits\":\"" + discounts.getDiscountUnits()+ "\"";
				jsonOutput += ",\"discountPercentage\":\"" + discounts.getDiscountPercentage()+ "\"";
				jsonOutput += ",\"isSignup\":\"" + discounts.getIsSignup()+ "\"";
				jsonOutput += ",\"isResort\":\"" + discounts.getIsResort()+ "\"";
				//jsonOutput += ",\"accommodationId\":\"" + discounts.getPropertyAccommodation().getAccommodationId()+ "\"";
				
				jsonOutput += "}";


			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}   
	
	
public String getCurrentDiscounts() throws IOException {
	
	
	
		PropertyAccommodationManager propertyAccommodationController = new PropertyAccommodationManager();
		PropertyDiscountManager propertyDiscountController = new PropertyDiscountManager();
		BookingDetailManager bookingDetailController = new BookingDetailManager();
		PmsPropertyManager propertyController = new PmsPropertyManager();
		
		
		//SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		//Date currentDate = sdf.parse(currDate);
		
		try {
			
			Timestamp todayDate = new Timestamp(System.currentTimeMillis());
			String currDate = new SimpleDateFormat("yyyy-MM-dd").format(todayDate);
			Date currentDate = new SimpleDateFormat("yyyy-MM-dd").parse(currDate);

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		    
			response.setContentType("application/json");
          
			this.DiscountList = propertyDiscountController.currentDiscounts(currentDate);
			
			
			for (PropertyDiscount discounts : DiscountList) {
				
				/*
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				
				//jsonOutput += "\"discountCount\":\"" + dashboard.getDiscountCount() + "\"";
				jsonOutput += "\"discountId\":\"" + discounts.getPropertyDiscountId() + "\"";
				jsonOutput += ",\"discountName\":\"" + discounts.getDiscountName()+ "\"";
				jsonOutput += ",\"startDate\":\"" + discounts.getStartDate()+ "\"";
				jsonOutput += ",\"endDate\":\"" + discounts.getEndDate()+ "\"";
				jsonOutput += ",\"discountUnits\":\"" + discounts.getDiscountUnits()+ "\"";
				jsonOutput += ",\"discountPercentage\":\"" + discounts.getDiscountPercentage()+ "\"";
				//jsonOutput += ",\"available\":\"" + ((discounts.getDiscountUnits()) - (dashboard.getDiscountCount())) + "\"";
				jsonOutput += ",\"isSignup\":\"" + discounts.getIsSignup()+ "\"";
				//jsonOutput += ",\"accommodationId\":\"" + discounts.getPropertyAccommodation().getAccommodationId()+ "\"";
				
				jsonOutput += "}";*/
		
		  DashBoard dashboard = bookingDetailController.findDiscountCount(discounts.getPropertyDiscountId());
		  long available = 0; 
		  if(dashboard == null){
			available = 0; 
		  }
		  else{
			  
			   available = (discounts.getDiscountUnits()) - (dashboard.getDiscountCount());
		  }
          
          if(available>0){
				 
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				
				//jsonOutput += "\"discountCount\":\"" + dashboard.getDiscountCount() + "\"";
				jsonOutput += "\"discountId\":\"" + discounts.getPropertyDiscountId() + "\"";
				jsonOutput += ",\"discountName\":\"" + discounts.getDiscountName()+ "\"";
				jsonOutput += ",\"startDate\":\"" + discounts.getStartDate()+ "\"";
				jsonOutput += ",\"endDate\":\"" + discounts.getEndDate()+ "\"";
				jsonOutput += ",\"discountUnits\":\"" + discounts.getDiscountUnits()+ "\"";
				jsonOutput += ",\"discountPercentage\":\"" + discounts.getDiscountPercentage()+ "\"";
				jsonOutput += ",\"available\":\"" + (dashboard == null ? 0 : (discounts.getDiscountUnits()) - (dashboard.getDiscountCount())) + "\"";
				jsonOutput += ",\"isSignup\":\"" + discounts.getIsSignup()+ "\"";
				//jsonOutput += ",\"accommodationId\":\"" + discounts.getPropertyAccommodation().getAccommodationId()+ "\"";
				//jsonOutput += "\"available\":\"" + (dashboard == null ? 0 : (dashboard.getDiscountCount() )) + "\"";
				jsonOutput += "}";
           
          }

			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	} 


public String getAllCurrentDiscounts() throws IOException {
	
	
	
	PropertyAccommodationManager propertyAccommodationController = new PropertyAccommodationManager();
	PropertyDiscountManager propertyDiscountController = new PropertyDiscountManager();
	BookingDetailManager bookingDetailController = new BookingDetailManager();
	PmsPropertyManager propertyController = new PmsPropertyManager();
    
	try {
		
		Timestamp todayDate = new Timestamp(System.currentTimeMillis());
		String currDate = new SimpleDateFormat("yyyy-MM-dd").format(todayDate);
		Date currentDate = new SimpleDateFormat("yyyy-MM-dd").parse(currDate);
		String jsonOutput = "";
		HttpServletResponse response = ServletActionContext.getResponse();
	    response.setContentType("application/json");
        
		this.DiscountList = propertyDiscountController.allCurrentDiscounts(currentDate);
		for (PropertyDiscount discounts : DiscountList) {
		DashBoard dashboard = bookingDetailController.findDiscountCount(discounts.getPropertyDiscountId());
	    long available = 0; 
	    if(dashboard == null){
		available = 0; 
	    }
	    else{
		available = (discounts.getDiscountUnits()) - (dashboard.getDiscountCount());
	    }
      
      if(available>0){
			 
			if (!jsonOutput.equalsIgnoreCase(""))
				jsonOutput += ",{";
			else
				jsonOutput += "{";
			
			//jsonOutput += "\"discountCount\":\"" + dashboard.getDiscountCount() + "\"";
			jsonOutput += "\"discountId\":\"" + discounts.getPropertyDiscountId() + "\"";
			jsonOutput += ",\"discountName\":\"" + discounts.getDiscountName()+ "\"";
			jsonOutput += ",\"startDate\":\"" + discounts.getStartDate()+ "\"";
			jsonOutput += ",\"endDate\":\"" + discounts.getEndDate()+ "\"";
			jsonOutput += ",\"discountUnits\":\"" + discounts.getDiscountUnits()+ "\"";
			jsonOutput += ",\"discountPercentage\":\"" + discounts.getDiscountPercentage()+ "\"";
			jsonOutput += ",\"available\":\"" + (dashboard == null ? 0 : (discounts.getDiscountUnits()) - (dashboard.getDiscountCount())) + "\"";
			jsonOutput += ",\"isSignup\":\"" + discounts.getIsSignup()+ "\"";
			//jsonOutput += ",\"accommodationId\":\"" + discounts.getPropertyAccommodation().getAccommodationId()+ "\"";
			//jsonOutput += "\"available\":\"" + (dashboard == null ? 0 : (dashboard.getDiscountCount() )) + "\"";
			jsonOutput += "}";
       
      }

		}

		response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
		

	} catch (Exception e) {
		logger.error(e);
		e.printStackTrace();
	} finally {

	}

	return null;

} 


public String getAmpCurrentDiscounts() throws IOException {
	
	
	
	PropertyAccommodationManager propertyAccommodationController = new PropertyAccommodationManager();
	PropertyDiscountManager propertyDiscountController = new PropertyDiscountManager();
	BookingDetailManager bookingDetailController = new BookingDetailManager();
	PmsPropertyManager propertyController = new PmsPropertyManager();
	
	
	//SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	//Date currentDate = sdf.parse(currDate);
	
	try {
		
		Timestamp todayDate = new Timestamp(System.currentTimeMillis());
		String currDate = new SimpleDateFormat("yyyy-MM-dd").format(todayDate);
		Date currentDate = new SimpleDateFormat("yyyy-MM-dd").parse(currDate);

		String jsonOutput = "";
		HttpServletResponse response = ServletActionContext.getResponse();
	    
		response.setContentType("application/json");
      
		this.DiscountList = propertyDiscountController.currentDiscounts(currentDate);
		
		
		for (PropertyDiscount discounts : DiscountList) {
			
			/*
			if (!jsonOutput.equalsIgnoreCase(""))
				jsonOutput += ",{";
			else
				jsonOutput += "{";
			
			//jsonOutput += "\"discountCount\":\"" + dashboard.getDiscountCount() + "\"";
			jsonOutput += "\"discountId\":\"" + discounts.getPropertyDiscountId() + "\"";
			jsonOutput += ",\"discountName\":\"" + discounts.getDiscountName()+ "\"";
			jsonOutput += ",\"startDate\":\"" + discounts.getStartDate()+ "\"";
			jsonOutput += ",\"endDate\":\"" + discounts.getEndDate()+ "\"";
			jsonOutput += ",\"discountUnits\":\"" + discounts.getDiscountUnits()+ "\"";
			jsonOutput += ",\"discountPercentage\":\"" + discounts.getDiscountPercentage()+ "\"";
			//jsonOutput += ",\"available\":\"" + ((discounts.getDiscountUnits()) - (dashboard.getDiscountCount())) + "\"";
			jsonOutput += ",\"isSignup\":\"" + discounts.getIsSignup()+ "\"";
			//jsonOutput += ",\"accommodationId\":\"" + discounts.getPropertyAccommodation().getAccommodationId()+ "\"";
			
			jsonOutput += "}";*/
	
	  DashBoard dashboard = bookingDetailController.findDiscountCount(discounts.getPropertyDiscountId());
	  long available = 0; 
	  if(dashboard == null){
		available = 0; 
	  }
	  else{
		  
		   available = (discounts.getDiscountUnits()) - (dashboard.getDiscountCount());
	  }
      
      if(available>0){
			 
			if (!jsonOutput.equalsIgnoreCase(""))
				jsonOutput += ",{";
			else
				jsonOutput += "{";
			
			//jsonOutput += "\"discountCount\":\"" + dashboard.getDiscountCount() + "\"";
			jsonOutput += "\"discountId\":\"" + discounts.getPropertyDiscountId() + "\"";
			jsonOutput += ",\"discountName\":\"" + discounts.getDiscountName()+ "\"";
			jsonOutput += ",\"startDate\":\"" + discounts.getStartDate()+ "\"";
			jsonOutput += ",\"endDate\":\"" + discounts.getEndDate()+ "\"";
			jsonOutput += ",\"discountUnits\":\"" + discounts.getDiscountUnits()+ "\"";
			jsonOutput += ",\"discountPercentage\":\"" + discounts.getDiscountPercentage()+ "\"";
			jsonOutput += ",\"available\":\"" + (dashboard == null ? 0 : (discounts.getDiscountUnits()) - (dashboard.getDiscountCount())) + "\"";
			jsonOutput += ",\"isSignup\":\"" + discounts.getIsSignup()+ "\"";
			//jsonOutput += ",\"accommodationId\":\"" + discounts.getPropertyAccommodation().getAccommodationId()+ "\"";
			//jsonOutput += "\"available\":\"" + (dashboard == null ? 0 : (dashboard.getDiscountCount() )) + "\"";
			jsonOutput += "}";
       
      }

		}

		response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
		

	} catch (Exception e) {
		logger.error(e);
		e.printStackTrace();
	} finally {

	}

	return null;

} 

	public String addDiscount() {
		
		try {
			this.propertyId = (Integer) sessionMap.get("propertyId");
			
			PropertyAccommodationManager propertyAccommodationController = new PropertyAccommodationManager();
			PropertyDiscountManager propertyDiscountController = new PropertyDiscountManager();

			PmsPropertyManager propertyController = new PmsPropertyManager();			
			
			this.DiscountList = propertyDiscountController.findCount(getDiscountName());
			this.SignupActiveList = propertyDiscountController.findActiveCount();
			
			//this.DiscountList = propertyDiscountController.findCount(getDiscountName());
			
			DateFormat format = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
		    java.util.Date dateStart = format.parse(getStrStartDate());
		    java.util.Date dateEnd = format.parse(getStrEndDate());
		   
		    
		    Calendar calCheckStart=Calendar.getInstance();
		    calCheckStart.setTime(dateStart);
		    java.util.Date checkInDate = calCheckStart.getTime();
		    this.startDate=new Timestamp(checkInDate.getTime());
		    sessionMap.put("startDate",startDate); 
		    
		    Calendar calCheckEnd=Calendar.getInstance();
		    calCheckEnd.setTime(dateEnd);
		    java.util.Date checkOutDate = calCheckEnd.getTime();
		    this.endDate=new Timestamp(checkOutDate.getTime());
		    sessionMap.put("endDate",endDate); 
		    
			
			
			if(this.DiscountList.size() >= 1){
				
			   
				return null;
			}
			
			else{
			
		    
		    if((getIsSignup() == true) && (this.SignupActiveList.size()>=1)){
		    
		    
		    return null; 
		    
		    }		
				
		    else{
		    	
		   
			PropertyDiscount discount = new PropertyDiscount();
			
			
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			
			
			discount.setDiscountName(getDiscountName());
			discount.setIsActive(true);
			discount.setIsDeleted(false);
			discount.setIsSignup(getIsSignup());
			discount.setStartDate(startDate);
			discount.setEndDate(endDate);			
			discount.setIsResort(false);
			discount.setDiscountPercentage(getPercentage());
			discount.setDiscountUnits(getDiscountUnits());
			discount.setDiscountType(getDiscountType());
			PmsProperty property = propertyController.find(getPropertyId());
			discount.setPmsProperty(property);
			//PropertyAccommodation propertyAccommodation = propertyAccommodationController.find(getAccommodationId());
			//discount.setPropertyAccommodation(propertyAccommodation);
			
			
			
			propertyDiscountController.add(discount);
			
			return SUCCESS;
			
		    }
			
			}
			
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return SUCCESS;
		
	}
	
	
	public String addCoupons(){
		try{
			PropertyDiscountManager propertyDiscountController = new PropertyDiscountManager();
			PmsPropertyManager propertyController = new PmsPropertyManager();			
			SeoUrlAction urlaction=new SeoUrlAction();
			List<GooglePropertyAreas> listPropertyAreas=null;
		    List<PmsProperty> listPmsProperty=null;
		    List<PmsProperty> listProperties=new ArrayList<PmsProperty>();
		    String id=null,type="NA";
		    if(getLocationUrl()!=null){
		    	String stroutput=urlaction.getSearchLocationIdByUrl(getLocationUrl());
		    	if(stroutput!=null && stroutput!="NA"){
		    		StringTokenizer stoken=new StringTokenizer(stroutput,",");
				    while(stoken.hasMoreElements()){
				    	id=stoken.nextToken();
				    	type=stoken.nextToken();
					}		
		    	}
			    
		    }
			
		    
		    
 	    	if(type.equals("location")){
		    	listPmsProperty = propertyController.listPropertyByLocation(Integer.parseInt(id));
		    	if(listPmsProperty.size()>0 && !listPmsProperty.isEmpty()){
		    		for(PmsProperty property:listPmsProperty){
		    			List<PmsProperty> listNoTagProperty=propertyController.list(property.getPropertyId());
						if(listNoTagProperty.size()>0 && !listNoTagProperty.isEmpty()){
							for(PmsProperty tagproperty:listNoTagProperty){
								listProperties.add(tagproperty);			
							}
						}
		    		}
		    	}
		    }else if(type.equals("area")){
		    	
		    	listPropertyAreas = propertyController.listPropertyByGoogleArea(Integer.parseInt(id));
		 	    if(listPropertyAreas.size()>0 && !listPropertyAreas.isEmpty()){
		 	    	for(GooglePropertyAreas propertyareas: listPropertyAreas){
		 	    		int areaPropertyId=propertyareas.getPmsProperty().getPropertyId();
		 	    		List<PmsProperty> listProperty=propertyController.list(areaPropertyId);
		 	    		if(listProperty.size()>0 && !listProperty.isEmpty()){
		 	    			for(PmsProperty property:listProperty){

	 	    					List<PmsProperty> listNoTagProperty=propertyController.list(property.getPropertyId());
 	    						if(listNoTagProperty.size()>0 && !listNoTagProperty.isEmpty()){
 	    							for(PmsProperty tagproperty:listNoTagProperty){
 	    								listProperties.add(tagproperty);			
 	    							}
 	    						}
		 	    			}
		 	    		}
		 	    		
		 	    	}
		 	    }
		    }
 	    	
 	    	if(getLocationUrl().equalsIgnoreCase("ulo-all-hotels") || getLocationUrl().equalsIgnoreCase("null")){
 	    		List<PmsProperty> listProperty=propertyController.listProperty();
 	    		if(listProperty.size()>0 && !listProperty.isEmpty()){
 	    			for(PmsProperty property:listProperty){

	    				List<PmsProperty> listAllProperty=propertyController.list(property.getPropertyId());
 						if(listAllProperty.size()>0 && !listAllProperty.isEmpty()){
 							for(PmsProperty tagproperty:listAllProperty){
 								listProperties.add(tagproperty);			
 							}
 						}
 	    			}
 	    		}
 	    	}
			DateFormat format = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
		    java.util.Date dateStart = format.parse(getStrStartDate());
		    java.util.Date dateEnd = format.parse(getStrEndDate());
		   
		    
		    Calendar calCheckStart=Calendar.getInstance();
		    calCheckStart.setTime(dateStart);
		    java.util.Date checkInDate = calCheckStart.getTime();
		    this.startDate=new Timestamp(checkInDate.getTime());
		    sessionMap.put("startDate",startDate); 
		    
		    Calendar calCheckEnd=Calendar.getInstance();
		    calCheckEnd.setTime(dateEnd);
		    java.util.Date checkOutDate = calCheckEnd.getTime();
		    this.endDate=new Timestamp(checkOutDate.getTime());
		    sessionMap.put("endDate",endDate);
		    
		    PropertyDiscount discount = new PropertyDiscount();
			if(!listProperties.isEmpty() && listProperties.size()>0){
				if(getCouponType().equalsIgnoreCase("RD")){
			    	for (PmsProperty property : listProperties) {
			    		discount.setDiscountName(getDiscountName());
			    		discount.setDiscountCode(getCouponCode());
		    			discount.setIsActive(true);
		    			discount.setIsDeleted(false);
		    			discount.setStartDate(startDate);
		    			discount.setEndDate(endDate);			
		    			discount.setIsResort(false);
		    			if(getDiscountType().equalsIgnoreCase("Percent")){
		    				discount.setDiscountPercentage(getCouponDiscount());
		    				discount.setDiscountInr(0.0);
		    		    }else if(getDiscountType().equalsIgnoreCase("Inr")){
		    		    	discount.setDiscountInr(getCouponDiscount());
		    		    	discount.setDiscountPercentage(0.0);
		    		    }
		    			if(getIsSignup()){
		    				discount.setIsSignup(true);
		    				discount.setIsAllUsers(false);
		    			}else{
		    				discount.setIsSignup(false);
		    				discount.setIsAllUsers(true);
		    			}
		    			discount.setDiscountUnits(getDiscountUnits());
		    			discount.setDiscountType(getDiscountType());
		    			discount.setCouponType(getCouponType());
		    			discount.setMaximumDiscountAmount(getMaximumDiscount());
		    			PmsProperty pmsproperty = propertyController.find(property.getPropertyId());
		    			discount.setPmsProperty(pmsproperty);
		    			propertyDiscountController.add(discount);
				    }	
			    }else if(getCouponType().equalsIgnoreCase("UW")){
			    	for(PmsProperty property:listProperties){

		    			discount.setDiscountName(getDiscountName());
		    			discount.setDiscountCode(getCouponCode());
		    			discount.setIsActive(true);
		    			discount.setIsDeleted(false);
		    			if(getIsSignup()){
		    				discount.setIsSignup(true);
		    				discount.setIsAllUsers(false);
		    			}else{
		    				discount.setIsSignup(false);
		    				discount.setIsAllUsers(true);
		    			}
		    			
		    			
		    			
		    			discount.setStartDate(startDate);
		    			discount.setEndDate(endDate);			
		    			discount.setIsResort(false);
		    			discount.setDiscountUnits(getDiscountUnits());
		    			discount.setDiscountType(getDiscountType());
		    			discount.setCouponType(getCouponType());
		    			if(getDiscountType().equalsIgnoreCase("Percent")){
		    				discount.setDiscountPercentage(getCouponDiscount());
		    				discount.setDiscountInr(0.0);
		    		    }else if(getDiscountType().equalsIgnoreCase("Inr")){
		    		    	discount.setDiscountInr(getCouponDiscount());
		    		    	discount.setDiscountPercentage(0.0);
		    		    }
		    			discount.setMaximumDiscountAmount(getMaximumDiscount());
		    			PmsProperty pmsproperty = propertyController.find(property.getPropertyId());
		    			discount.setPmsProperty(pmsproperty);
		    			propertyDiscountController.add(discount);
		    		
			    	}
			    }else if(getCouponType().equalsIgnoreCase("OW")){
			    	for(PmsProperty property:listProperties){

		    			discount.setDiscountName(getDiscountName());
		    			discount.setDiscountCode(getCouponCode());
		    			discount.setIsActive(true);
		    			discount.setIsDeleted(false);
		    			if(getIsSignup()){
		    				discount.setIsSignup(true);
		    				discount.setIsAllUsers(false);
		    			}else{
		    				discount.setIsSignup(false);
		    				discount.setIsAllUsers(true);
		    			}
		    			discount.setStartDate(startDate);
		    			discount.setEndDate(endDate);			
		    			discount.setIsResort(false);
		    			discount.setDiscountUnits(getDiscountUnits());
		    			discount.setDiscountType(getDiscountType());
		    			discount.setCouponType(getCouponType());
		    			if(getDiscountType().equalsIgnoreCase("Percent")){
		    				discount.setDiscountPercentage(getCouponDiscount());
		    				discount.setDiscountInr(0.0);
		    		    }else if(getDiscountType().equalsIgnoreCase("Inr")){
		    		    	discount.setDiscountInr(getCouponDiscount());
		    		    	discount.setDiscountPercentage(0.0);
		    		    }
		    			discount.setMaximumDiscountAmount(getMaximumDiscount());
		    			PmsProperty pmsproperty = propertyController.find(property.getPropertyId());
		    			discount.setPmsProperty(pmsproperty);
		    			propertyDiscountController.add(discount);
		    		
			    	}
			    }
			}
		    
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return null;
	}

	public String editCoupons(){
		try{
			PropertyDiscountManager propertyDiscountController = new PropertyDiscountManager();
			
 	    	
			DateFormat format = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
		    java.util.Date dateStart = format.parse(getStrStartDate());
		    java.util.Date dateEnd = format.parse(getStrEndDate());
		   
		    
		    Calendar calCheckStart=Calendar.getInstance();
		    calCheckStart.setTime(dateStart);
		    java.util.Date checkInDate = calCheckStart.getTime();
		    this.startDate=new Timestamp(checkInDate.getTime());
		    sessionMap.put("startDate",startDate); 
		    
		    Calendar calCheckEnd=Calendar.getInstance();
		    calCheckEnd.setTime(dateEnd);
		    java.util.Date checkOutDate = calCheckEnd.getTime();
		    this.endDate=new Timestamp(checkOutDate.getTime());
		    sessionMap.put("endDate",endDate);
		    
		    PropertyDiscount discount = propertyDiscountController.find(getPropertyDiscountId());
			if(discount!=null){
				
				discount.setDiscountName(getDiscountName());
	    		discount.setDiscountCode(getCouponCode());
    			discount.setIsActive(true);
    			discount.setIsDeleted(false);
    			discount.setStartDate(startDate);
    			discount.setEndDate(endDate);			
    			discount.setIsResort(false);
    			if(getDiscountType().equalsIgnoreCase("Percent")){
    				discount.setDiscountPercentage(getCouponDiscount());
    				discount.setDiscountInr(0.0);
    		    }else if(getDiscountType().equalsIgnoreCase("Inr")){
    		    	discount.setDiscountInr(getCouponDiscount());
    		    	discount.setDiscountPercentage(0.0);
    		    }
    			if(getIsSignup()){
    				discount.setIsSignup(true);
    				discount.setIsAllUsers(false);
    			}else{
    				discount.setIsSignup(false);
    				discount.setIsAllUsers(true);
    			}
    			discount.setDiscountUnits(getDiscountUnits());
    			discount.setDiscountType(getDiscountType());
    			discount.setCouponType(getCouponType());
    			discount.setMaximumDiscountAmount(getMaximumDiscount());
    			propertyDiscountController.edit(discount);	
			}
		    
		    
		    
		    
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return null;
	}
	
	public String deleteCoupons(){

		try{
			PropertyDiscountManager propertyDiscountController = new PropertyDiscountManager();
		    
		    PropertyDiscount discount = propertyDiscountController.find(getPropertyDiscountId());
			if(discount!=null){
    			discount.setIsActive(false);
    			discount.setIsDeleted(true);
    			propertyDiscountController.edit(discount);	
			}
		    
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return null;
	
	}
	
	public String editDiscount() {
		
		
		try {
			this.propertyId = (Integer) sessionMap.get("propertyId");
			PropertyAccommodationManager propertyAccommodationController = new PropertyAccommodationManager();
			PropertyDiscountManager propertyDiscountController = new PropertyDiscountManager();
			PmsPropertyManager propertyController = new PmsPropertyManager();
			
			DateFormat format = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
		    java.util.Date dateStart = format.parse(getStrStartDate());
		    java.util.Date dateEnd = format.parse(getStrEndDate());
		   
		    
		    Calendar calCheckStart=Calendar.getInstance();
		    calCheckStart.setTime(dateStart);
		    java.util.Date checkInDate = calCheckStart.getTime();
		    this.startDate=new Timestamp(checkInDate.getTime());
		    sessionMap.put("startDate",startDate); 
		    
		    Calendar calCheckEnd=Calendar.getInstance();
		    calCheckEnd.setTime(dateEnd);
		    java.util.Date checkOutDate = calCheckEnd.getTime();
		    this.endDate=new Timestamp(checkOutDate.getTime());
		    sessionMap.put("endDate",endDate); 
		    
			
			//PropertyDiscount nameCount = propertyDiscountController.findCount(getDiscountName());
		    
			this.DiscountList = propertyDiscountController.findCount(getDiscountName());
			this.SignupActiveList = propertyDiscountController.findActiveCount();
			/*if(nameCount.getNameCount() >= 1){
			
			}*/
			PropertyDiscount editdis = propertyDiscountController.find(getPropertyDiscountId());
			
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			if(this.DiscountList.size() < 1){
			editdis.setDiscountName(getDiscountName().trim());
			}
			editdis.setIsActive(true);
			editdis.setIsDeleted(false);
			if((getIsSignup() == true) && (this.SignupActiveList.size()< 1)){
			editdis.setIsSignup(getIsSignup());
			}
			if((getIsSignup() == false)){
			editdis.setIsSignup(getIsSignup());
			}
			editdis.setStartDate(startDate);
			editdis.setEndDate(endDate);
			editdis.setDiscountPercentage(getDiscountPercentage());	
			editdis.setDiscountUnits(getDiscountUnits());	
			
			editdis.setDiscountType(getDiscountType());
			PmsProperty property = propertyController.find(getPropertyId());
			editdis.setPmsProperty(property);
			//PropertyAccommodation propertyAccommodation = propertyAccommodationController.find(getAccommodationId());
			//editdis.setPropertyAccommodation(propertyAccommodation);
			propertyDiscountController.edit(editdis);
			
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		
		return SUCCESS;
	}
	
	
   public String deleteDiscount() {
		
	   PropertyAccommodationManager propertyAccommodationController = new PropertyAccommodationManager();
		PropertyDiscountManager propertyDiscountController = new PropertyDiscountManager();

		PmsPropertyManager propertyController = new PmsPropertyManager();
		try {
			
						
			
			
			PropertyDiscount deletedis = propertyDiscountController.find(getPropertyDiscountId());
						
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			deletedis.setIsActive(false);
			deletedis.setIsDeleted(true);
			deletedis.setModifiedDate(new java.sql.Timestamp(date.getTime()));
			propertyDiscountController.edit(deletedis);
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}  

	
   public String applyDiscount() throws IOException {
	   
	   Timestamp todayDate = new Timestamp(System.currentTimeMillis());
	   
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PropertyAccommodationManager propertyAccommodationController = new PropertyAccommodationManager();
			PropertyDiscountManager propertyDiscountController = new PropertyDiscountManager();

			PmsPropertyManager propertyController = new PmsPropertyManager();
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");

		     //PropertyDiscount discount = propertyDiscountController.findName(getDiscountName(),todayDate);
			
			
			this.DiscountList = propertyDiscountController.verifyDiscount(getDiscountName(),todayDate);
			for (PropertyDiscount discounts : DiscountList) {

				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
			
				else
					jsonOutput += "{";
				jsonOutput += "\"discountId\":\"" + discounts.getPropertyDiscountId() + "\"";
				jsonOutput += ",\"discountName\":\"" + discounts.getDiscountName()+ "\"";
				jsonOutput += ",\"startDate\":\"" + discounts.getStartDate()+ "\"";
				jsonOutput += ",\"endDate\":\"" + discounts.getEndDate()+ "\"";
				jsonOutput += ",\"discountPercentage\":\"" + discounts.getDiscountPercentage()+ "\"";
				//jsonOutput += ",\"accommodationId\":\"" + discounts.getPropertyAccommodation().getAccommodationId()+ "\"";
				
				jsonOutput += "}";


			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	

	public Integer getPropertyDiscountId() {
		return propertyDiscountId;
	}

	public void setPropertyDiscountId(Integer propertyDiscountId) {
		this.propertyDiscountId = propertyDiscountId;
	}

	/*public Integer getAccommodationId() {
		return accommodationId;
	}

	public void setAccommodationId(Integer accommodationId) {
		this.accommodationId = accommodationId;
	}*/

	public Integer getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(Integer propertyId) {
		this.propertyId = propertyId;
	}
	
	
	public String getDiscountName() {
		return discountName;
	}

	public void setDiscountName(String discountName) {
		this.discountName = discountName;
	}
	
	public Timestamp getStartDate() {
		return startDate;
	}

	public void setStartDate(Timestamp startDate) {
		this.startDate = startDate;
	}

	public Timestamp getEndDate() {
		return endDate;
	}
	
	public void setEndDate(Timestamp endDate) {
		this.endDate = endDate;
	}


	public double getPercentage() {
		return discountPercentage;
	}

	public void setPercentage(double discountPercentage) {
		this.discountPercentage = discountPercentage;
	}
	
	public String getDiscountType() {
		return discountType;
	}

	public void setDiscountType(String discountType) {
		this.discountType = discountType;
	}

	public double getDiscountInr() {
		return discountInr;
	}

	public void setDiscountInr(double discountInr) {
		this.discountInr = discountInr;
	}
	
	public Integer getDiscountUnits() {
			return discountUnits;
		}

	public void setDiscountUnits(Integer discountUnits) {
			this.discountUnits = discountUnits;
		}
	
	public Boolean getIsSignup() {
		return isSignup;
	}

	public void setIsSignup(Boolean isSignup) {
		this.isSignup = isSignup;
	}
	
	
	public double getDiscountPercentage() {
		return discountPercentage;
	}

	public void setDiscountPercentage(double discountPercentage) {
		this.discountPercentage = discountPercentage;
	}
	
	public List<PropertyDiscount> getDiscountList() {
		return DiscountList;
	}

	public void setBookingDetailList(List<PropertyDiscount> DiscountList) {
		this.DiscountList = DiscountList;
	}
	
	public List<PropertyDiscount> getSignupActiveList() {
		return SignupActiveList;
	}

	public void setSignupActiveList(List<PropertyDiscount> signupActiveList) {
		SignupActiveList = signupActiveList;
	}
	
	public String getStrStartDate() {
		return strStartDate;
	}

	public void setStrStartDate(String strStartDate) {
		this.strStartDate = strStartDate;
	}

	public String getStrEndDate() {
		return strEndDate;
	}

	public void setStrEndDate(String strEndDate) {
		this.strEndDate = strEndDate;
	}

	public String getCouponCode() {
		return couponCode;
	}

	public void setCouponCode(String couponCode) {
		this.couponCode = couponCode;
	}

	public String getLocationUrl() {
		return locationUrl;
	}

	public void setLocationUrl(String locationUrl) {
		this.locationUrl = locationUrl;
	}

	public Double getCouponDiscount() {
		return couponDiscount;
	}

	public void setCouponDiscount(Double couponDiscount) {
		this.couponDiscount = couponDiscount;
	}

	public Double getMaximumDiscount() {
		return maximumDiscount;
	}

	public void setMaximumDiscount(Double maximumDiscount) {
		this.maximumDiscount = maximumDiscount;
	}

	public void setDiscount(PropertyDiscount discount) {
		this.discount = discount;
	}

	public String getCouponType() {
		return couponType;
	}

	public void setCouponType(String couponType) {
		this.couponType = couponType;
	}
	

}
