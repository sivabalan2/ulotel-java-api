package com.ulopms.view;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.ServletRequestAware;




//import com.medwecare.controller.AwsObjectManager;
//import com.medwecare.controller.CommentManager;
//import com.medwecare.controller.CommentUserManager;
////import com.medwecare.controller.NotificationManager;
//import com.medwecare.controller.ProjectManager;

//import com.medwecare.model.AwsObject;
//import com.medwecare.model.Notification;
//import com.medwecare.model.ObjectComment;
//import com.medwecare.model.ObjectCommentUser;
//import com.medwecare.model.Project;


















import com.opensymphony.xwork2.ActionSupport;

import freemarker.template.Configuration;
import freemarker.template.Template;

import javax.servlet.http.HttpServletRequest;

import com.opensymphony.xwork2.ModelDriven;
import com.ulopms.controller.PmsGuestManager;
import com.ulopms.controller.PmsPropertyManager;
import com.ulopms.controller.PropertyItemCategoryManager;
import com.ulopms.controller.PropertyAccommodationManager;
import com.ulopms.controller.PropertyTaxeManager;
import com.ulopms.controller.UserLoginManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.PmsGuest;
import com.ulopms.model.PmsProperty;
import com.ulopms.model.PropertyItem;
import com.ulopms.model.PropertyItemCategory;
import com.ulopms.model.PropertyAccommodation;
import com.ulopms.model.PropertyTaxe;
import com.ulopms.model.User;
import com.ulopms.util.Email;
import com.ulopms.util.Image;

public class PropertyTaxAction extends ActionSupport implements
ServletRequestAware,SessionAware, UserAware{
     
	

	private static final long serialVersionUID = 914982665758390091L;
    
	
	
	private Integer propertyTaxId;
	//private Integer propertyId;
	private Integer propertyAccommodationId;
	private String  taxName;
	private String  taxType;
	private String  taxCode;
	private double  taxAmountFrom;
	private double  taxAmountTo;
	private double  taxPercentage;
	
	
	
	private PropertyTaxe tax;
	
	
	private List<PropertyTaxe> taxeList;

	private static final Logger logger = Logger.getLogger(PropertyTaxAction.class);

	private HttpSession session;

	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}
	
	private SessionMap<String,Object> sessionMap;  
	  
	//getters and setters  
	  
	@Override  
	public void setSession(Map<String, Object> map) {  
	    sessionMap=(SessionMap)map;  
	}  
	
	private User user;

	
	// @Override
	// public User getModel() {
	// return user;
	// }

	@Override
	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	
	private HttpServletRequest request;

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	public PropertyTaxAction() {

	}

	public String execute() {

		//this.patientId = getPatientId();
		return SUCCESS;
	}
	
	

	public String getTax() throws IOException {
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PropertyTaxeManager taxController = new PropertyTaxeManager();
			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");

			PropertyTaxe taxe = taxController.find(getPropertyTaxId());
			
				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"propertyTaxId\":\"" + taxe.getPropertyTaxId() + "\"";
				
				jsonOutput += ",\"taxName\":\"" + taxe.getTaxName()+ "\"";
				jsonOutput += ",\"taxType\":\"" + taxe.getTaxType()+ "\"";
				jsonOutput += ",\"taxCode\":\"" + taxe.getTaxCode()+ "\"";
				jsonOutput += ",\"taxAmountFrom\":\"" + taxe.getTaxAmountFrom()+ "\"";
				jsonOutput += ",\"taxAmountTo\":\"" + taxe.getTaxAmountTo()+ "\"";
				jsonOutput += ",\"taxPercentage\":\"" + taxe.getTaxPercentage()+ "\"";
				jsonOutput += "}";

			

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	public String getTaxes() throws IOException {
		
		//this.propertyId = (Integer) sessionMap.get("propertyId");
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PropertyTaxeManager taxController = new PropertyTaxeManager();
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");

			this.taxeList = taxController.list();
			for (PropertyTaxe taxe : taxeList) {

				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"taxId\":\"" + taxe.getPropertyTaxId() + "\"";
				jsonOutput += ",\"taxName\":\"" + taxe.getTaxName().trim()+ "\"";
				jsonOutput += ",\"taxType\":\"" + taxe.getTaxType().trim()+ "\"";
				jsonOutput += ",\"taxCode\":\"" + taxe.getTaxCode().trim()+ "\"";
				jsonOutput += ",\"taxAmountFrom\":\"" + taxe.getTaxAmountFrom()+ "\"";
				jsonOutput += ",\"taxAmountTo\":\"" + taxe.getTaxAmountTo()+ "\"";
				jsonOutput += ",\"taxPercantage\":\"" + taxe.getTaxPercentage()+ "\"";
				jsonOutput += "}";


			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	

	public String addTax() {
		
		
		//this.propertyId = (Integer) sessionMap.get("propertyId");
		PropertyTaxeManager taxController = new PropertyTaxeManager();
		PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
		//UserLoginManager  userController = new UserLoginManager();
		PmsPropertyManager propertyController = new PmsPropertyManager();
		
		try {
			
						
			PropertyTaxe taxe = new PropertyTaxe();
			
			
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			
			
			taxe.setIsActive(true);
			taxe.setIsDeleted(false);
			taxe.setIsInclusiveRate(true);
			taxe.setTaxAmountFrom(getTaxAmountFrom());
			taxe.setTaxAmountTo(getTaxAmountTo());
			taxe.setTaxPercentage(getTaxPercentage());
			taxe.setTaxCode(getTaxCode());
			taxe.setTaxName(getTaxName());
			taxe.setTaxType(getTaxType());
			//PropertyTaxe propertyTaxe = taxController.find(getPropertyTaxId());
			//taxe.setPropertyTaxe(propertyTaxe);
			taxController.add(taxe);
			
			
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return SUCCESS;
	}
	
	
	public String editTax() {
		
		
		PropertyTaxeManager taxController = new PropertyTaxeManager();
		
		PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
		//UserLoginManager  userController = new UserLoginManager();
		PmsPropertyManager propertyController = new PmsPropertyManager();
	    //PmsPropertyManager propertyController = new PmsPropertyManager();
		//UserLoginManager  userController = new UserLoginManager();
		try {
			
			PropertyTaxe taxe = taxController.find(getPropertyTaxId());
			
			
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			taxe.setIsActive(true);
			taxe.setIsDeleted(false);
			taxe.setIsInclusiveRate(true);
			taxe.setTaxAmountFrom(getTaxAmountFrom());
			taxe.setTaxAmountTo(getTaxAmountTo());
			taxe.setTaxPercentage(getTaxPercentage());
			taxe.setTaxCode(getTaxCode());
			taxe.setTaxName(getTaxName());
			taxe.setTaxType(getTaxType());
			taxController.edit(taxe);
			
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return SUCCESS;
	}
	
	
public String deleteTax() {
		
	    PropertyTaxeManager taxController = new PropertyTaxeManager();
		//UserLoginManager  userController = new UserLoginManager();
		try {
			
						
			
			
			PropertyTaxe taxe = taxController.find(getPropertyTaxId());
						
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			taxe.setIsActive(false);
			taxe.setIsDeleted(true);
			taxe.setIsInclusiveRate(true);
			taxe.setModifiedDate(new java.sql.Timestamp(date.getTime()));
			taxController.edit(taxe);
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}

	
/*  public Integer getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(Integer propertyId) {
		this.propertyId = propertyId;
	}
	
	public Integer getPropertyAccommodationId() {
		return propertyAccommodationId;
	}

	public void setPropertyAccommodationId(Integer propertyAccommodationId) {
		this.propertyAccommodationId = propertyAccommodationId;
	}
	*/

	public Integer getPropertyTaxId() {
		return propertyTaxId;
	}

	public void setPropertyTaxId(Integer propertyTaxId) {
		this.propertyTaxId = propertyTaxId;
	}
	
	public String getTaxName() {
		return taxName;
	}

	public void setTaxName(String taxName) {
		this.taxName = taxName;
	}
	
	public String getTaxType() {
		return taxType;
	}

	public void setTaxType(String taxType) {
		this.taxType = taxType;
		
	}

	public String getTaxCode() {
		return taxCode;
	}

	public void setTaxCode(String taxCode) {
		this.taxCode = taxCode;
	}

	public double getTaxAmountFrom() {
		return taxAmountFrom;
	}

	public void setTaxAmountFrom(double taxAmountFrom) {
		this.taxAmountFrom = taxAmountFrom;
	}

	public double getTaxAmountTo() {
		return taxAmountTo;
	}

	public void setTaxAmountTo(double taxAmountTo) {
		this.taxAmountTo = taxAmountTo;
	}

	public double getTaxPercentage() {
		return taxPercentage;
	}

	public void setTaxPercentage(double taxPercentage) {
		this.taxPercentage = taxPercentage;
	}


	
    public List<PropertyTaxe> getTaxList() {
		return taxeList;
	}

	public void setTaxList(List<PropertyTaxe> taxeList) {
		this.taxeList = taxeList;
	}

	
	public void setTax(PropertyTaxe tax) {
		this.tax = tax;
	}
	

	


}
