package com.ulopms.view;

import java.sql.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;

import com.opensymphony.xwork2.ActionSupport;
import com.ulopms.controller.MenuManager;
import com.ulopms.controller.ScreenManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.Menu;
import com.ulopms.model.Screen;
import com.ulopms.model.User;

import javax.servlet.http.HttpServletRequest;

public class ScreenAction extends ActionSupport implements ServletRequestAware,
		UserAware {

	private static final long serialVersionUID = 9149826260758390091L;

	private Screen screen;

	private int screenId;
	private String screenName;
	private User user;
	private List<Screen> screenList;
	private List<Menu> menuList;

	private Long menuId;
	private String screenPath;
	private String screenPurpose;
	private static final Logger logger = Logger.getLogger(ScreenAction.class);

	private ScreenManager screenController = new ScreenManager();
	private MenuManager menuController = new MenuManager();

	// @Override
	// public User getModel() {
	// return user;
	// }

	@Override
	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	private HttpServletRequest request;

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	public ScreenAction() {

	}

	public String execute() {

		try {
			// this.screenList = screenController.list();
			this.menuList = menuController.list();
			if (request.getParameter("id") != null) {
				Screen s = new Screen();
				s = screenController.find(Short.parseShort(request
						.getParameter("id")));
				this.setScreenId(s.getScreenId());
				this.setScreenName(s.getScreenName());
				this.setScreenPath(s.getScreenPath());
				this.setScreenPurpose(s.getScreenPurpose());

			}
		} catch (Exception e) {
			logger.error(e);
		} finally {

		}
		return SUCCESS;
	}

	public String ScreenSave() {

		return SUCCESS;
	}

	public String ScreenList() {
		try {
			this.screenList = screenController.list();
		} catch (Exception e) {
			logger.error(e);
		} finally {

		}
		return SUCCESS;
	}

	public String ScreenAdd() {
		try {
			Screen s = new Screen();

			// HttpServletRequest request = ServletActionContext.getRequest();
			if (getScreenId() > 0) {
				// s = screenController.find(getScreenId());
				s.setScreenId(getScreenId());

			}
			s.setScreenName(screenName);
			s.setScreenPath(screenPath);
			s.setScreenPurpose(screenPurpose);

			screenController.edit(s);
		} catch (Exception e1) {
			logger.error(e1);
		} finally {

		}
		return SUCCESS;
	}

	public Screen getScreen() {
		return screen;
	}

	public void setScreen(Screen screen) {
		this.screen = screen;
	}

	public int getScreenId() {
		return screenId;
	}

	public void setScreenId(int screenId) {
		this.screenId = screenId;
	}

	public String getScreenName() {
		return screenName;
	}

	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}

	public List<Screen> getScreenList() {
		return screenList;
	}

	public void setScreenList(List<Screen> screenList) {
		this.screenList = screenList;
	}

	public String getScreenPath() {
		return screenPath;
	}

	public void setScreenPath(String screenPath) {
		this.screenPath = screenPath;
	}

	public String getScreenPurpose() {
		return screenPurpose;
	}

	public void setScreenPurpose(String screenPurpose) {
		this.screenPurpose = screenPurpose;
	}

	public List<Menu> getMenuList() {
		return menuList;
	}

	public void setMenuList(List<Menu> menuList) {
		this.menuList = menuList;
	}

	public Long getMenuId() {
		return menuId;
	}

	public void setMenuId(Long menuId) {
		this.menuId = menuId;
	}

}
