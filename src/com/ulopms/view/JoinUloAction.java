package com.ulopms.view;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.catalina.startup.UserConfig;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;

import com.opensymphony.xwork2.ActionSupport;
import com.ulopms.api.Search;
import com.ulopms.controller.HelpTopicManager;
import com.ulopms.controller.UserLoginManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.HelpTopic;
import com.ulopms.model.User;
import com.ulopms.util.Email;

import freemarker.template.Configuration;
import freemarker.template.Template;

import javax.servlet.http.HttpServletRequest;

public class JoinUloAction extends ActionSupport implements ServletRequestAware,
UserAware {

	private static final long serialVersionUID = 9149826260758390091L;

	private User user;
	
	private String hotelName;
	private String message;
	private String name;
	private String email;
	private String phone;
	private String location;
	
	
	
	



	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}

	private HttpSession session;
	
	

	private static final Logger logger = Logger.getLogger(JoinUloAction.class);

	// @Override
	// public User getModel() {
	// return user;
	// }

	@Override
	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	private HttpServletRequest request;

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	public JoinUloAction() {

	}


	public String Join()
	{
		try
		{
		//email template
		Configuration cfg = new Configuration();
		cfg.setClassForTemplateLoading(JoinUloAction.class, "../../../");
		Template template = cfg.getTemplate(getText("partnerwithus.template"));
		Map<String, String> rootMap = new HashMap<String, String>();
		rootMap.put("name",getName());
		rootMap.put("email", getEmail());
		rootMap.put("hotel", getHotelName());
		rootMap.put("mobile", getPhone());
		rootMap.put("location", getLocation());
		rootMap.put("message", getMessage());
		rootMap.put("from", getText("notification.from"));
		Writer out = new StringWriter();
		template.process(rootMap, out);

		//send email
		Email em = new Email();
		em.set_to(getEmail());
		em.set_cc(getText("partnerwithus.notification.email"));
		em.set_cc2(getText("reservation.notification.email"));
		em.set_from(getText("email.from"));
		//em.set_host(getText("email.host"));
		//em.set_password(getText("email.password"));
		//em.set_port(getText("email.port"));
		em.set_username(getText("email.username"));
		em.set_subject(getText("partnerwithus.subject"));
		em.set_bodyContent(out);
		em.sendPartnerWithUs();		
		}
		catch(Exception e1)
		{
			e1.printStackTrace();
		}
		addActionMessage("Your request has been submitted successfully");
		return SUCCESS;
	}
	
	public String partnerWithusAPI(Search search){
		String data="";
		try
		{
			
			String mailId=search.getPartnerEmail();
			String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."
						+ "[a-zA-Z0-9_+&*-]+)*@" + "(?:[a-zA-Z0-9-]+\\.)+[a-z"
						+ "A-Z]{2,7}$";
			 
			Pattern pat = Pattern.compile(emailRegex);
			Boolean bool = pat.matcher(mailId).matches();
			if(bool){
				Configuration cfg = new Configuration();
				cfg.setClassForTemplateLoading(JoinUloAction.class, "../../../");
				Template template = cfg.getTemplate(getText("partnerwithus.template"));
				Map<String, String> rootMap = new HashMap<String, String>();
				rootMap.put("name",search.getPartnerName());
				rootMap.put("email", search.getPartnerEmail());
				rootMap.put("hotel", search.getPartnerHotel());
				rootMap.put("mobile", search.getPartnerNumber());
				rootMap.put("location", search.getPartnerLocation());
				rootMap.put("message", search.getPartnerMessage());
				rootMap.put("from", getText("notification.from"));
				Writer out = new StringWriter();
				template.process(rootMap, out);
		
				//send email
				Email em = new Email();
				em.set_to(search.getPartnerEmail());
				em.set_cc("dwarakesh@ulohotels.com");
				em.set_cc2("sivabalan@ulohotels.com");
				em.set_from(getText("email.from"));
				em.set_username(getText("email.username"));
				em.set_subject(getText("partnerwithus.subject"));
				em.set_bodyContent(out);
				em.sendPartnerWithUs();
				data="success";
			}else{
				data = "error";
			}
			 
					
		}
		catch(Exception e1)
		{
			data="Some technnical issue on partner with us api ";
			logger.error(e1);
			e1.printStackTrace();
		}
		return data;
	
	}


	public String execute() {
		
		return SUCCESS;
	}
	
	public String getHotelName() {
		return hotelName;
	}

	public void setHotelName(String hotelName) {
		this.hotelName = hotelName;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

}
