package com.ulopms.view;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.math.BigDecimal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;




//import com.medwecare.controller.AwsObjectManager;
//import com.medwecare.controller.CommentManager;
//import com.medwecare.controller.CommentUserManager;
////import com.medwecare.controller.NotificationManager;
//import com.medwecare.controller.ProjectManager;

//import com.medwecare.model.AwsObject;
//import com.medwecare.model.Notification;
//import com.medwecare.model.ObjectComment;
//import com.medwecare.model.ObjectCommentUser;
//import com.medwecare.model.Project;














import com.opensymphony.xwork2.ActionSupport;

import freemarker.template.Configuration;
import freemarker.template.Template;

import javax.servlet.http.HttpServletRequest;

import com.opensymphony.xwork2.ModelDriven;
import com.ulopms.controller.PmsGuestManager;
import com.ulopms.controller.PmsPropertyManager;
import com.ulopms.controller.PropertyAccommodationManager;
import com.ulopms.controller.PropertyAccommodationRoomManager;
import com.ulopms.controller.UserLoginManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.AccommodationRoom;
import com.ulopms.model.PmsGuest;
import com.ulopms.model.PmsProperty;
import com.ulopms.model.PropertyAccommodation;
import com.ulopms.model.PropertyAccommodationRoom;
import com.ulopms.model.PropertyItemCategory;
import com.ulopms.model.User;
import com.ulopms.util.Email;
import com.ulopms.util.Image;

public class PropertyAccommodationRoomAction extends ActionSupport implements
ServletRequestAware, SessionAware, UserAware{
     
	

	private static final long serialVersionUID = 914982665758390091L;
 
	private Integer propertyAccommodationRoomId;
	private Integer propertyAccommodationId;
	private Integer propertyId;
	private String  roomAlias;
	private String  sortKey;
	private String  floor;
	private String  areaSqft;
	private String  phoneExtension;
	private String  remark;
	private String  dataExtension;
	
    private Integer limit;
    
	private Integer offset;
	
	private PropertyAccommodationRoom accommodationRoom;
	
	private List<PropertyAccommodationRoom> accommodationRoomList;
	
	private List<AccommodationRoom> accommodationRoomsList;

	

	private static final Logger logger = Logger.getLogger(PropertyAccommodationAction.class);

	private HttpSession session;

	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}
	
	private SessionMap<String,Object> sessionMap;  
	  
	//getters and setters  
	  
	@Override  
	public void setSession(Map<String, Object> map) {  
	    sessionMap=(SessionMap)map;  
	}  
	
	
	private User user;

	
	// @Override
	// public User getModel() {
	// return user;
	// }

	@Override
	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	
	private HttpServletRequest request;

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	public PropertyAccommodationRoomAction() {

	}

	public String execute() {

		//this.patientId = getPatientId();
		return SUCCESS;
	}

	
     public String getAccommodationRoom() throws IOException {
		
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		   
			
			PropertyAccommodationRoomManager accommodationRoomController = new PropertyAccommodationRoomManager();
			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");

			PropertyAccommodationRoom accommodationRoom = accommodationRoomController.find(getPropertyAccommodationRoomId());
			
				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"propertyAccommodationRoomId\":\"" + accommodationRoom.getRoomId() + "\"";
			    jsonOutput += ",\"propertyAccommodationId\":\"" + accommodationRoom.getPropertyAccommodation().getAccommodationId() + "\"";
				PropertyAccommodation propertyAccommodation = accommodationController.find(accommodationRoom.getPropertyAccommodation().getAccommodationId());
				jsonOutput += ",\"propertyAccommodationType\":\"" + propertyAccommodation.getAccommodationType()+ "\"";
				jsonOutput += ",\"roomAlias\":\"" + accommodationRoom.getRoomAlias()+ "\"";
				jsonOutput += ",\"sortKey\":\"" + accommodationRoom.getSortKey()+ "\"";
				jsonOutput += ",\"floor\":\"" + accommodationRoom.getFloor()+ "\"";
				jsonOutput += ",\"areaSqft\":\"" + accommodationRoom.getAreaSqft()+ "\"";
				jsonOutput += ",\"phoneExtension\":\"" + accommodationRoom.getPhoneExtension()+ "\"";
				jsonOutput += ",\"dataExtension\":\"" + accommodationRoom.getDataExtension()+ "\"";
				jsonOutput += ",\"remark\":\"" + accommodationRoom.getRemark()+ "\"";
				
				jsonOutput += "}";

			

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
     public String getAccommodationRooms() throws IOException {
 		try {
 			 this.propertyId = (Integer) sessionMap.get("propertyId");
 			String jsonOutput = "";
 			HttpServletResponse response = ServletActionContext.getResponse();
 		
 			
 			PropertyAccommodationRoomManager accommodationRoomController = new PropertyAccommodationRoomManager();
 			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
 			//this.familyRegisterList = familyController.list(getUser().getUserid());
 			//model = familyRegisterList;
 			response.setContentType("application/json");
 			//this.accommodationRoomsList = accommodationRoomController.pagination(this.propertyId, limit,offset);
 			this.accommodationRoomsList = accommodationRoomController.list(this.propertyId,limit,offset);
 			//this.accommodationRoomList = accommodationRoomController.list();
 			for (AccommodationRoom accommodationRooms : accommodationRoomsList) {

 				
 				if (!jsonOutput.equalsIgnoreCase(""))
 					jsonOutput += ",{";
 				else
 					jsonOutput += "{";
 				
 			    jsonOutput += "\"propertyAccommodationRoomId\":\"" + accommodationRooms.getRoomId() + "\"";
 			    jsonOutput += ",\"propertyAccommodationId\":\"" + accommodationRooms.getAccommodationId() + "\"";
 				PropertyAccommodation propertyAccommodation = accommodationController.find(accommodationRooms.getAccommodationId());
 				jsonOutput += ",\"propertyAccommodationType\":\"" + propertyAccommodation.getAccommodationType()+ "\"";
 				jsonOutput += ",\"roomAlias\":\"" + accommodationRooms.getRoomAlias()+ "\"";
 				jsonOutput += ",\"sortKey\":\"" + (accommodationRooms.getSortKey() == null? "0": accommodationRooms.getSortKey())+ "\"";
 				//jsonOutput += ",\"floor\":\"" + accommodationRoom.getFloor()+ "\"";
 				//jsonOutput += ",\"areaSqft\":\"" + accommodationRoom.getAreaSqft()+ "\"";
 				//jsonOutput += ",\"phoneExtension\":\"" + accommodationRoom.getPhoneExtension()+ "\"";
 				//jsonOutput += ",\"dataExtension\":\"" + accommodationRoom.getDataExtension()+ "\"";
 				//jsonOutput += ",\"remark\":\"" + accommodationRoom.getRemark()+ "\"";
 				jsonOutput += "}";


 			}

 			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
 			

 		} catch (Exception e) {
 			logger.error(e);
 			e.printStackTrace();
 		} finally {

 		}

 		return null;

 	}
 	
 	
 

public String getAccommodationRoomsCount() throws IOException {
 		
	
 		try {
 			this.propertyId = (Integer) sessionMap.get("propertyId");
 			String jsonOutput = "";
 			HttpServletResponse response = ServletActionContext.getResponse();
 		
 			
 			PropertyAccommodationRoomManager accommodationRoomController = new PropertyAccommodationRoomManager();
 			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
 			//this.familyRegisterList = familyController.list(getUser().getUserid());
 			//model = familyRegisterList;
 			response.setContentType("application/json");
 			
 			this.accommodationRoomList = accommodationRoomController.listCount(propertyId);
 			

 				
 				if (!jsonOutput.equalsIgnoreCase(""))
 					jsonOutput += ",{";
 				else
 					jsonOutput += "{";
 				
 			    jsonOutput += "\"count\":\"" + accommodationRoomList.size() + "\"";
 			    
 				jsonOutput += "}";


 		

 			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
 			

 		} catch (Exception e) {
 			logger.error(e);
 			e.printStackTrace();
 		} finally {

 		}

 		return null;

 	}

public String addAccommodationRoom() {
	
	try {
		PropertyAccommodationRoomManager accommodationRoomController = new PropertyAccommodationRoomManager();
		PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
		//UserLoginManager  userController = new UserLoginManager();
		PmsPropertyManager propertyController = new PmsPropertyManager();
					
		PropertyAccommodationRoom accommodationRoom = new PropertyAccommodationRoom();
		
		
		long time = System.currentTimeMillis();
		java.sql.Date date = new java.sql.Date(time);
		
		accommodationRoom.setAreaSqft(getAreaSqft());
		accommodationRoom.setDataExtension(getDataExtension());
		accommodationRoom.setFloor(getFloor());
		accommodationRoom.setIsActive(true);
		accommodationRoom.setIsDeleted(false);
		accommodationRoom.setPhoneExtension(getPhoneExtension());
		accommodationRoom.setRemark(getRemark());
		accommodationRoom.setRoomAlias(getRoomAlias());
		accommodationRoom.setSortKey(getSortKey());
		PropertyAccommodation propertyAccommodation = accommodationController.find(getPropertyAccommodationId());
		accommodationRoom.setPropertyAccommodation(propertyAccommodation);
		accommodationRoomController.add(accommodationRoom);
		AccommodationRoom accommodationRooms = accommodationRoomController.findRoomCount(getPropertyAccommodationId());
		PropertyAccommodation accom = accommodationController.find(getPropertyAccommodationId());
		Integer roomsCount = (int)accommodationRooms.getRoomCount();
		accom.setNoOfUnits(roomsCount);
		accommodationController.edit(accom);
		
		
		
	} catch (Exception e) {
		logger.error(e);
		e.printStackTrace();
	} finally {

	}
	return SUCCESS;
}
	
	
	public String addAccommodationRoom(int accommodationId,String areaSqft, String RoomAlias,Integer userId) {
		try {
			
			PropertyAccommodationRoomManager accommodationRoomController = new PropertyAccommodationRoomManager();
			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
			//UserLoginManager  userController = new UserLoginManager();
			PmsPropertyManager propertyController = new PmsPropertyManager();
			
			//Integer userId=(Integer) sessionMap.get("userId");
			long time = System.currentTimeMillis();
			java.util.Date date = new java.util.Date(time);
			Timestamp tsDate=new Timestamp(date.getTime());  
						
			
			PropertyAccommodationRoom accommodationRoom = new PropertyAccommodationRoom();
			
			
			
			accommodationRoom.setAreaSqft(areaSqft);
			
			accommodationRoom.setIsActive(true);
			accommodationRoom.setIsDeleted(false);
			accommodationRoom.setCreatedBy(userId);
			accommodationRoom.setCreatedDate(tsDate);
			accommodationRoom.setRoomAlias(RoomAlias);
			PropertyAccommodation propertyAccommodation = accommodationController.find(accommodationId);
			accommodationRoom.setPropertyAccommodation(propertyAccommodation);
			accommodationRoomController.add(accommodationRoom);
			
			
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return SUCCESS;
	}
	
	public String editAccommodationRoom() {
		try {
			PropertyAccommodationRoomManager accommodationRoomController = new PropertyAccommodationRoomManager();
			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
			UserLoginManager  userController = new UserLoginManager();
			Integer userId=(Integer) sessionMap.get("userId");
			long time = System.currentTimeMillis();
			java.util.Date date = new java.util.Date(time);
			Timestamp tsDate=new Timestamp(date.getTime());  
						
			
			PropertyAccommodationRoom accommodationRoom = accommodationRoomController.find(getPropertyAccommodationRoomId());
			
			
			
			accommodationRoom.setAreaSqft(getAreaSqft());
			accommodationRoom.setDataExtension(getDataExtension());
			accommodationRoom.setFloor(getFloor());
			accommodationRoom.setIsActive(true);
			accommodationRoom.setIsDeleted(false);
			accommodationRoom.setModifiedBy(userId);
			accommodationRoom.setModifiedDate(tsDate);
			accommodationRoom.setPhoneExtension(getPhoneExtension());
			accommodationRoom.setRemark(getRemark());
			accommodationRoom.setRoomAlias(getRoomAlias());
			accommodationRoom.setSortKey(getSortKey());
			PropertyAccommodation propertyAccommodation = accommodationController.find(getPropertyAccommodationId());
			accommodationRoom.setPropertyAccommodation(propertyAccommodation);
			//PropertyAccommodation accommodation = accommodationController.find(getPropertyId());
			//accommodationRoom.setAccommodation(accommodation);
			
			
			accommodationRoomController.edit(accommodationRoom);
			
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return SUCCESS;
	}
	
public String deleteAccommodationRoom() {
		
		try {
			Integer userId=(Integer) sessionMap.get("userId");
			long time = System.currentTimeMillis();
			java.util.Date date = new java.util.Date(time);
			Timestamp tsDate=new Timestamp(date.getTime());  
			
			PropertyAccommodationRoomManager accommodationRoomController = new PropertyAccommodationRoomManager();
			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
			UserLoginManager  userController = new UserLoginManager();
			
			
			PropertyAccommodationRoom accommodationRoom = accommodationRoomController.find(getPropertyAccommodationRoomId());
			
			accommodationRoom.setIsActive(false);
			accommodationRoom.setIsDeleted(true);
			accommodationRoom.setModifiedBy(userId);
			accommodationRoom.setModifiedDate(tsDate);
			accommodationRoomController.edit(accommodationRoom);
			AccommodationRoom accommodationRooms = accommodationRoomController.findRoomCount(accommodationRoom.getPropertyAccommodation().getAccommodationId());
			PropertyAccommodation accom = accommodationController.find(accommodationRoom.getPropertyAccommodation().getAccommodationId());
			Integer roomsCount = (int)accommodationRooms.getRoomCount();
			accom.setNoOfUnits(roomsCount);
			accommodationController.edit(accom);
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}
	
   
	public Integer getPropertyAccommodationRoomId() {
		return propertyAccommodationRoomId;
	}

	public void setPropertyAccommodationRoomId(Integer propertyAccommodationRoomId) {
		this.propertyAccommodationRoomId = propertyAccommodationRoomId;
	}
	
	public Integer getPropertyAccommodationId() {
		return propertyAccommodationId;
	}

	public void setPropertyAccommodationId(Integer propertyAccommodationId) {
		this.propertyAccommodationId = propertyAccommodationId;
	}

	public Integer getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(Integer propertyId) {
		this.propertyId = propertyId;
	}

	public String getRoomAlias() {
		return roomAlias;
	}

	public void setRoomAlias(String roomAlias) {
		this.roomAlias = roomAlias;
	}

	public String getSortKey() {
		return sortKey;
	}

	public void setSortKey(String sortKey) {
		this.sortKey = sortKey;
	}
	
	public String getFloor() {
		return floor;
	}

	public void setFloor(String floor) {
		this.floor = floor;
	}
	
	public String getAreaSqft() {
		return areaSqft;
	}

	public void setAreaSqft(String areaSqft) {
		this.areaSqft = areaSqft;
	}
	
	public String getPhoneExtension() {
		return phoneExtension;
	}

	public void setPhoneExtension(String phoneExtension) {
		this.phoneExtension = phoneExtension;
	}

	
	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
    
	public String getDataExtension() {
		return dataExtension;
	}

	public void setDataExtension(String dataExtension) {
		this.dataExtension = dataExtension;
	}
	
	
	public List<PropertyAccommodationRoom> getAccommodationRoomList() {
		return accommodationRoomList;
	}

	public void setAccomadationRoomList(List<PropertyAccommodationRoom> accommodationRoomList) {
		this.accommodationRoomList = accommodationRoomList;
	}

	public void setAccommodationRoom(PropertyAccommodationRoom accommodationRoom) {
		this.accommodationRoom = accommodationRoom;
	}
	
	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	public Integer getOffset() {
		return offset;
	}

	public void setOffset(Integer offset) {
		this.offset = offset;
	}

	


}
