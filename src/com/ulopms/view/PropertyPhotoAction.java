package com.ulopms.view;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;




//import com.medwecare.controller.AwsObjectManager;
//import com.medwecare.controller.CommentManager;
//import com.medwecare.controller.CommentUserManager;
////import com.medwecare.controller.NotificationManager;
//import com.medwecare.controller.ProjectManager;

//import com.medwecare.model.AwsObject;
//import com.medwecare.model.Notification;
//import com.medwecare.model.ObjectComment;
//import com.medwecare.model.ObjectCommentUser;
//import com.medwecare.model.Project;

























import org.w3c.tidy.Tidy;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.SdkClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.opensymphony.xwork2.ActionSupport;

import freemarker.template.Configuration;
import freemarker.template.Template;

import javax.servlet.http.HttpServletRequest;

import com.opensymphony.xwork2.ModelDriven;
import com.ulopms.api.Search;
import com.ulopms.controller.AccommodationPhotoManager;
import com.ulopms.controller.BookingDetailManager;
import com.ulopms.controller.BookingGuestDetailManager;
import com.ulopms.controller.PmsBookingManager;
import com.ulopms.controller.PmsGuestManager;
import com.ulopms.controller.PmsPhotoCategoryManager;
import com.ulopms.controller.PmsPropertyManager;
import com.ulopms.controller.PmsStatusManager;
import com.ulopms.controller.PromotionImageManager;
import com.ulopms.controller.PropertyAccommodationManager;
import com.ulopms.controller.PropertyPhotoManager;
import com.ulopms.controller.UserLoginManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.AccommodationPhoto;
import com.ulopms.model.BookingDetail;
import com.ulopms.model.BookingGuestDetail;
import com.ulopms.model.PmsBooking;
import com.ulopms.model.PmsGuest;
import com.ulopms.model.PmsPhotoCategory;
import com.ulopms.model.PmsProperty;
import com.ulopms.model.PmsStatus;
import com.ulopms.model.PromotionImages;
import com.ulopms.model.PropertyAccommodation;
import com.ulopms.model.PropertyPhoto;
import com.ulopms.model.User;
import com.ulopms.util.Email;
import com.ulopms.util.Image;

public class PropertyPhotoAction extends ActionSupport implements
ServletRequestAware,SessionAware, UserAware{

	private static final long serialVersionUID = 914982665758390091L;

	private Integer propertyPhotoId;
	private Integer propertyId;
	private Boolean isPrimary;

	private File myFile;
	private String myFileContentType;
	private String myFileFileName;
	private String promotionImageUrl;
	private Integer photoCategoryId;
	private Integer promotionImageId;
    private Integer propertyAccommodationId;
    private String imageAltName;
	

	private PropertyPhoto propertyPhoto;
	
	private List<PropertyPhoto> propertyPhotoList;
	private List<PropertyPhoto> categoryPhotoList;
	private List<PropertyAccommodation> accommodationList;
	private List<PromotionImages> promotionImageList;
	private String clientRegion = "ap-south-1";
    private String bucketName = "ulohotelsuploads/uloimg";
    private String bucketNameResume = "ulohotelsuploads/uloresume/resume";
    private String keyName = "ulohotelsuploads";

	private static final Logger logger = Logger.getLogger(PropertyPhotoAction.class);
    
	private HttpSession session;

	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}
	
	private SessionMap<String,Object> sessionMap;  
	  
	//getters and setters  
	  
	@Override  
	public void setSession(Map<String, Object> map) {  
	    sessionMap=(SessionMap)map;  
	}  
	
	
	private User user;

	
	// @Override
	// public User getModel() {
	// return user;
	// }

	@Override
	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	
	private HttpServletRequest request;

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	public PropertyPhotoAction() {

	}

	public String execute() {

		//this.patientId = getPatientId();
		return SUCCESS;
	}

	
	
    public String uploadAwsImage() throws IOException{
	
		try {
			this.propertyId = (Integer) sessionMap.get("propertyId");
			PropertyPhotoManager photoController = new PropertyPhotoManager();
			PmsPropertyManager propertyController = new PmsPropertyManager();
			PmsPhotoCategoryManager categoryController = new PmsPhotoCategoryManager();
            PropertyAccommodationManager propertyAccommodationController = new PropertyAccommodationManager();
			this.myFile=getMyFile();			
			PropertyPhoto photo = new PropertyPhoto();
			
			
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			AWSCredentials credentials = new BasicAWSCredentials("AKIA3BSHXMURUP3WEKWH", "BjOPBijiDBysvDDbOLJOTzEK+k7yl/mxxWF28k/Y");
			if(this.myFile!=null){
				AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
	                    .withRegion(clientRegion)
	                    .withCredentials(new AWSStaticCredentialsProvider(credentials))
	                    .build();
				String propertyBucketName=null;
				if(getPropertyAccommodationId()!=null && getPhotoCategoryId()!=null){
					propertyBucketName=bucketName+"/"+getPropertyId()+"/accommodation/"+getPropertyAccommodationId()+"/"+getPhotoCategoryId();	
				}
				
				if(getPhotoCategoryId()!=null && getPhotoCategoryId()!=5 && getPhotoCategoryId()!=6){
					propertyBucketName=bucketName+"/"+getPropertyId()+"/category/"+getPhotoCategoryId();
				}
				
	            s3Client.putObject(propertyBucketName, this.getMyFileFileName(), this.myFile);

				if(this.getMyFileFileName()!=null && !this.getMyFileFileName().equalsIgnoreCase(""))
				{
					PmsProperty property = propertyController.find(getPropertyId());
					photo.setPhotoPath(this.getMyFileFileName());
					photo.setIsActive(true);
					photo.setIsDeleted(false);
					photo.setImageAltName(getImageAltName());
	                if(getPropertyAccommodationId()!=null){
						PropertyAccommodation propertyAccommodation = propertyAccommodationController.find(getPropertyAccommodationId());
					    photo.setPropertyAccommodation(propertyAccommodation);
					    PmsPhotoCategory category = categoryController.find(getPhotoCategoryId());
						photo.setPmsPhotoCategory(category);
					}
	                photo.setPmsProperty(property);
	                if(getPhotoCategoryId() != null && getPhotoCategoryId()!=5 && getPhotoCategoryId()!=6){
						PmsPhotoCategory category = categoryController.find(getPhotoCategoryId());
						photo.setPmsPhotoCategory(category);
	                }
					PropertyPhoto photo1 = photoController.add(photo);
					
				 	
					/*if(photo1.getPropertyPhotoId()>0 && this.getMyFileFileName()!=null && !this.getMyFileFileName().equalsIgnoreCase(""))
					{
						File file = new File(getText("storage.property.photo") + "/"
								+ getPropertyId() + "/" + photo1.getPropertyPhotoId() + "/", this.getMyFileFileName());
						FileUtils.copyFile(this.myFile, file);// copying image in the
					}*/
					
					
				}	
			}
		
        }
        catch(AmazonServiceException e) {
            e.printStackTrace();
        }
        catch(SdkClientException e) {
            e.printStackTrace();
        }
		
		return SUCCESS;
	}


	
	public String getPropertyPhotos() throws IOException {
		
		try {
			this.propertyId = (Integer) sessionMap.get("propertyId");
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			PropertyPhotoManager photoController = new PropertyPhotoManager();
			response.setContentType("application/json");
			if (!jsonOutput.equalsIgnoreCase(""))
				jsonOutput += ",{";
			else
				jsonOutput += "{";

			this.propertyPhotoList = photoController.list(getPropertyId());
			
			jsonOutput += "\"total\":\"" + propertyPhotoList.size() + "\"";

			String jsonExteriour = "";
				String jsonRooms = "";
				String jsonRestaurant = "";
				String jsonActivityArea = "";
				String jsonBedRooms = "";
				String jsonRestRooms = "";
				String photoPath="";
				for (PropertyPhoto photo : propertyPhotoList) {
						if(photo.getPropertyAccommodation()!=null){
							
							if(photo.getPmsPhotoCategory()!=null &&  photo.getPmsPhotoCategory().getPhotoCategoryId()==5 ){
								photoPath = getText("storage.aws.property.photo") + "/uloimg/"
										+ photo.getPmsProperty().getPropertyId()+"/accommodation/"+photo.getPropertyAccommodation().getAccommodationId()
										+"/"+photo.getPmsPhotoCategory().getPhotoCategoryId()+"/"+photo.getPhotoPath();
								
								if (!jsonBedRooms.equalsIgnoreCase(""))
									jsonBedRooms += ",{";
								else
									jsonBedRooms += "{";
								
								jsonBedRooms += "\"DT_RowId\":\"" + photo.getPropertyPhotoId() + "\"";
								jsonBedRooms += ",\"isPrimary\":\"" + photo.getIsPrimary()+ "\"";
								jsonBedRooms += ",\"photoPath\":\"" + photoPath+ "\"";
								jsonBedRooms += ",\"type\":\"" + photo.getPropertyAccommodation().getAccommodationType()+ "\"";
								jsonBedRooms += ",\"categoryName\":\"" + (photo.getPmsPhotoCategory() == null?"NA" : photo.getPmsPhotoCategory().getPhotoCategoryName())+ "\"";
								jsonBedRooms += ",\"categoryId\":\"" + (photo.getPmsPhotoCategory() == null?"" : photo.getPmsPhotoCategory().getPhotoCategoryId())+ "\"";
								jsonBedRooms += ",\"altName\":\"" + (photo.getImageAltName() == null?"NA" : photo.getImageAltName())+ "\"";
								jsonBedRooms += "}";	
							}
							
							
					}
				
				
				}
				jsonOutput += ",\"bedroom\":[" + jsonBedRooms + "]";	

				for (PropertyPhoto photo : propertyPhotoList) {
					if(photo.getPropertyAccommodation()!=null){
						
						if(photo.getPmsPhotoCategory()!=null &&  photo.getPmsPhotoCategory().getPhotoCategoryId()==6 ){
							photoPath = getText("storage.aws.property.photo") + "/uloimg/"
									+ photo.getPmsProperty().getPropertyId()+"/accommodation/"+photo.getPropertyAccommodation().getAccommodationId()
									+"/"+photo.getPmsPhotoCategory().getPhotoCategoryId()+"/"+photo.getPhotoPath();
							
							if (!jsonRestRooms.equalsIgnoreCase(""))
								jsonRestRooms += ",{";
							else
								jsonRestRooms += "{";
							
							jsonRestRooms += "\"DT_RowId\":\"" + photo.getPropertyPhotoId() + "\"";
							jsonRestRooms += ",\"isPrimary\":\"" + photo.getIsPrimary()+ "\"";
							jsonRestRooms += ",\"photoPath\":\"" + photoPath+ "\"";
							jsonRestRooms += ",\"type\":\"" + photo.getPropertyAccommodation().getAccommodationType()+ "\"";
							jsonRestRooms += ",\"categoryName\":\"" + (photo.getPmsPhotoCategory() == null?"NA" : photo.getPmsPhotoCategory().getPhotoCategoryName())+ "\"";
							jsonRestRooms += ",\"categoryId\":\"" + (photo.getPmsPhotoCategory() == null?"" : photo.getPmsPhotoCategory().getPhotoCategoryId())+ "\"";
							jsonRestRooms += ",\"altName\":\"" + (photo.getImageAltName() == null?"NA" : photo.getImageAltName())+ "\"";
							jsonRestRooms += "}";
						}
						
						
				}
			
			
			}
			jsonOutput += ",\"restroom\":[" + jsonRestRooms + "]";	
				for (PropertyPhoto photo : propertyPhotoList) {
					if(photo.getPmsPhotoCategory()!=null && photo.getPmsPhotoCategory().getPhotoCategoryId() == 1){
					if (!jsonExteriour.equalsIgnoreCase(""))
						jsonExteriour += ",{";
					else
						jsonExteriour += "{";
			
					photoPath = getText("storage.aws.property.photo") + "/uloimg/"
							+ photo.getPmsProperty().getPropertyId()+"/category/"+photo.getPmsPhotoCategory().getPhotoCategoryId()+"/"+photo.getPhotoPath();
				
					jsonExteriour += "\"DT_RowId\":\"" + photo.getPropertyPhotoId() + "\"";
				jsonExteriour += ",\"isPrimary\":\"" + photo.getIsPrimary()+ "\"";
				jsonExteriour += ",\"photoPath\":\"" + photoPath+ "\"";
				jsonExteriour += ",\"type\":\"" + photo.getPmsPhotoCategory().getPhotoCategoryName()+ "\"";
				jsonExteriour += ",\"altName\":\"" + (photo.getImageAltName() == null?"NA" : photo.getImageAltName())+ "\"";
				jsonExteriour += "}";
			}
				
				
				}
			jsonOutput += ",\"exterior\":[" + jsonExteriour + "]";
			
			
			for (PropertyPhoto photo : propertyPhotoList) {
				if(photo.getPmsPhotoCategory()!=null && photo.getPmsPhotoCategory().getPhotoCategoryId() == 2){
					
					if (!jsonRooms.equalsIgnoreCase(""))
						jsonRooms += ",{";
					else
						jsonRooms += "{";
						
					photoPath = getText("storage.aws.property.photo") + "/uloimg/"
							+ photo.getPmsProperty().getPropertyId()+"/category/"+photo.getPmsPhotoCategory().getPhotoCategoryId()+"/"+photo.getPhotoPath();
					
					jsonRooms += "\"DT_RowId\":\"" + photo.getPropertyPhotoId() + "\"";
					jsonRooms += ",\"isPrimary\":\"" + photo.getIsPrimary()+ "\"";
					jsonRooms += ",\"photoPath\":\"" + photoPath+ "\"";
					jsonRooms += ",\"type\":\"" + photo.getPmsPhotoCategory().getPhotoCategoryName()+ "\"";
					jsonRooms += ",\"altName\":\"" + (photo.getImageAltName() == null?"NA" : photo.getImageAltName())+ "\"";
					jsonRooms += "}";
				}
			}
			
			jsonOutput += ",\"rooms\":[" + jsonRooms + "]";
			
			
			for (PropertyPhoto photo : propertyPhotoList) {
				if(photo.getPmsPhotoCategory()!=null && photo.getPmsPhotoCategory().getPhotoCategoryId() == 3){
					
					if (!jsonRestaurant.equalsIgnoreCase(""))
						jsonRestaurant += ",{";
					else
						jsonRestaurant += "{";
					
					photoPath = getText("storage.aws.property.photo") + "/uloimg/"
							+ photo.getPmsProperty().getPropertyId()+"/category/"+photo.getPmsPhotoCategory().getPhotoCategoryId()+"/"+photo.getPhotoPath();
						
						
					jsonRestaurant += "\"DT_RowId\":\"" + photo.getPropertyPhotoId() + "\"";
					jsonRestaurant += ",\"isPrimary\":\"" + photo.getIsPrimary()+ "\"";
					jsonRestaurant += ",\"photoPath\":\"" + photoPath+ "\"";
					jsonRestaurant += ",\"type\":\"" + photo.getPmsPhotoCategory().getPhotoCategoryName()+ "\"";
					jsonRestaurant += ",\"altName\":\"" + (photo.getImageAltName() == null?"NA" : photo.getImageAltName())+ "\"";
					jsonRestaurant += "}";
				}
			}
			
			jsonOutput += ",\"restaurant\":[" + jsonRestaurant + "]";
			
			for (PropertyPhoto photo : propertyPhotoList) {
			if(photo.getPmsPhotoCategory()!=null && photo.getPmsPhotoCategory().getPhotoCategoryId() == 4){
					if (!jsonActivityArea.equalsIgnoreCase(""))
						jsonActivityArea += ",{";
					else
						jsonActivityArea += "{";	
					
					photoPath = getText("storage.aws.property.photo") + "/uloimg/"
							+ photo.getPmsProperty().getPropertyId()+"/category/"+photo.getPmsPhotoCategory().getPhotoCategoryId()+"/"+photo.getPhotoPath();
					
					jsonActivityArea += "\"DT_RowId\":\"" + photo.getPropertyPhotoId() + "\"";
					jsonActivityArea += ",\"isPrimary\":\"" + photo.getIsPrimary()+ "\"";
					jsonActivityArea += ",\"photoPath\":\"" + photoPath+ "\"";
					jsonActivityArea += ",\"type\":\"" + photo.getPmsPhotoCategory().getPhotoCategoryName()+ "\"";
					jsonActivityArea += ",\"altName\":\"" + (photo.getImageAltName() == null?"NA" : photo.getImageAltName())+ "\"";
					jsonActivityArea += "}";
				}
			}
			jsonOutput += ",\"others\":[" + jsonActivityArea + "]";
			
			
			
			jsonOutput += "}";
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
     
      public String getCommonPropertyPhotos() throws IOException {
		
		try {
			this.propertyId = (Integer) sessionMap.get("propertyId");
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PropertyPhotoManager photoController = new PropertyPhotoManager();
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");

			this.propertyPhotoList = photoController.list(getPropertyId());
			for (PropertyPhoto photo : propertyPhotoList) {
                
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"DT_RowId\":\"" + photo.getPropertyPhotoId() + "\"";
				jsonOutput += ",\"isPrimary\":\"" + photo.getIsPrimary()+ "\"";
				jsonOutput += ",\"photoPath\":\"" + photo.getPhotoPath()+ "\"";
				
				
				jsonOutput += "}";


			}
			

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}

    
	public String getUloPropertyPhotos() throws IOException {
		
		try {
//			this.propertyId = (Integer) sessionMap.get("uloPropertyId");
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PropertyPhotoManager photoController = new PropertyPhotoManager();
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");

			this.propertyPhotoList = photoController.list(getPropertyId());
			for (PropertyPhoto photo : propertyPhotoList) {

				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"DT_RowId\":\"" + photo.getPropertyPhotoId() + "\"";
				jsonOutput += ",\"isPrimary\":\"" + photo.getIsPrimary()+ "\"";
				jsonOutput += ",\"photoPath\":\"" + photo.getPhotoPath()+ "\"";
				
				 if(photo.getPmsPhotoCategory().getPhotoCategoryId() == 2){
					if(photo.getPropertyAccommodation()!=null){
						jsonOutput += ",\"accommodationType\":\"" + photo.getPropertyAccommodation().getAccommodationType()+ "\"";	
					}else{
						jsonOutput += ",\"accommodationType\":\"" + "Common Places"+ "\"";
					}
					 
					
				 }else{ 	
						if(photo.getPropertyAccommodation()==null){
							
						jsonOutput += ",\"accommodationType\":\"" + "Common Places"+ "\"";
						
						}
						else{
						
						jsonOutput += ",\"accommodationType\":\"" + photo.getPropertyAccommodation().getAccommodationType()+ "\"";
						}
						
							
						}
				
				
				jsonOutput += "}";


			}
			

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}

     public String getUloCommonPropertyPhotos() throws IOException {
		
		try {
//			this.propertyId = (Integer) sessionMap.get("uloPropertyId");
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PropertyPhotoManager photoController = new PropertyPhotoManager();
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");

			
			if (!jsonOutput.equalsIgnoreCase(""))
				jsonOutput += ",{";
			else
				jsonOutput += "{";
			jsonOutput += "\"Name\":\"" + "Common places" + "\"";
			jsonOutput += ",\"id\":\"" + "cp01" + "\"";
			String jsonPhotos ="";
			
			this.propertyPhotoList = photoController.commonPhotosList(getPropertyId());
			for (PropertyPhoto photo : propertyPhotoList) {

				if (!jsonPhotos.equalsIgnoreCase(""))
					jsonPhotos += ",{";
				else
					jsonPhotos += "{";
				jsonPhotos += "\"photos\":\"" + photo.getPropertyPhotoId() + "\"";
				
								
				jsonPhotos += "}";
				
				
				


			}
			
			jsonOutput += ",\"photos\":[" + jsonPhotos+ "]";
			jsonOutput += "}";

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}

	
public String getUloAccommodationPropertyPhotos() throws IOException {
		
		try {
	
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PropertyPhotoManager photoController = new PropertyPhotoManager();
			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
			
			
			response.setContentType("application/json");
            
			this.accommodationList = accommodationController.list(getPropertyId());
			for (PropertyAccommodation accommodation : accommodationList) {
				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"accommodationId\":\"" + accommodation.getAccommodationId() + "\"";
				jsonOutput += ",\"abbreviation\":\"" + accommodation.getAbbreviation().trim()+ "\"";
				jsonOutput += ",\"accommodationType\":\"" + accommodation.getAccommodationType().trim()+ "\"";
				
				
				
                String jsonPhotos ="";
				
                this.categoryPhotoList = photoController.accommodationPhotosList(accommodation.getAccommodationId());
    			for (PropertyPhoto photo : categoryPhotoList) {

    				if (!jsonPhotos.equalsIgnoreCase(""))
    					jsonPhotos += ",{";
    				else
    					jsonPhotos += "{";
    				jsonPhotos += "\"photoPath\":\"" + photo.getPropertyPhotoId() + "\"";
    				
    								
    				jsonPhotos += "}";


    				
    			}
                
    			jsonOutput += ",\"photos\":[" + jsonPhotos+ "]";
				
    			jsonOutput += "}";
				
			}
			
			
			

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
public String getCategoryPhotos() throws IOException {
		this.propertyId = (Integer) sessionMap.get("propertyId");
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PropertyPhotoManager photoController = new PropertyPhotoManager();
			PropertyAccommodationManager propertyAccommodationController = new PropertyAccommodationManager();
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");

			this.propertyPhotoList = photoController.list1(getPropertyId(),getPhotoCategoryId());
			if(getPhotoCategoryId() == 2){
				
				for (PropertyPhoto photo : propertyPhotoList) {

					if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += ",{";
					else
						jsonOutput += "{";
					jsonOutput += "\"DT_RowId\":\"" + photo.getPropertyPhotoId() + "\"";
					jsonOutput += ",\"isPrimary\":\"" + photo.getIsPrimary()+ "\"";
					jsonOutput += ",\"photoPath\":\"" + photo.getPhotoPath()+ "\"";
					jsonOutput += ",\"photoCategoryId\":\"" + getPhotoCategoryId()+ "\"";
					//PropertyAccommodation propertyAccommodation = propertyAccommodationController.find(getPropertyAccommodationId());
					//jsonOutput += ",\"photoAccommodationType\":\"" + photo.getPropertyAccommodation().getAccommodationType()+ "\"";
					if(photo.getPropertyAccommodation()!=null){
						jsonOutput += ",\"photoAccommodationType\":\"" + (photo.getPropertyAccommodation().getAccommodationType() == null ? "": photo.getPropertyAccommodation().getAccommodationType().toUpperCase().trim())+ "\"";	
					}else{
						jsonOutput += ",\"photoAccommodationType\":\"" + (" ")+ "\"";
					}
					
					
					jsonOutput += "}";
	               
				}
			}
			
			else{
			
				for (PropertyPhoto photo : propertyPhotoList) {

					if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += ",{";
					else
						jsonOutput += "{";
					jsonOutput += "\"DT_RowId\":\"" + photo.getPropertyPhotoId() + "\"";
					jsonOutput += ",\"isPrimary\":\"" + photo.getIsPrimary()+ "\"";
					jsonOutput += ",\"photoPath\":\"" + photo.getPhotoPath()+ "\"";
					jsonOutput += ",\"photoCategoryId\":\"" + getPhotoCategoryId()+ "\"";
					//PropertyAccommodation propertyAccommodation = propertyAccommodationController.find(getPropertyAccommodationId());
					//jsonOutput += ",\"photoAccommodationType\":\"" + photo.getPropertyAccommodation().getAccommodationType()+ "\"";
					
					//jsonOutput += ",\"photoAccommodationType\":\"" + (photo.getPropertyAccommodation().getAccommodationType() == null ? "": photo.getPropertyAccommodation().getAccommodationType().toUpperCase().trim())+ "\"";
					
					jsonOutput += "}";
	               
				}
				
			}
			
			
			
			

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	public String uploadResume(Search search){
		String output="error";
		try{
			boolean enableMail=false;
			String strEmailId="",resumePath=""; 
			File oldFileName=null;
			File newFileName=null;
			this.myFile=search.getMyFile();
			
//			String fileName = FileUtils.readFileToString(newFileName);
			/*AWSCredentials credentials = new BasicAWSCredentials("AKIA3BSHXMURUP3WEKWH", "BjOPBijiDBysvDDbOLJOTzEK+k7yl/mxxWF28k/Y");
			if(this.myFile!=null){
				AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
	                    .withRegion(clientRegion)
	                    .withCredentials(new AWSStaticCredentialsProvider(credentials))
	                    .build();
				
	            s3Client.putObject(bucketNameResume, "resume.doc", this.myFile);
	            enableMail=true;
			}*/
			
			/*String extension=getFileExtension(this.myFile);
			
			if(extension.equalsIgnoreCase("docx") || extension.equalsIgnoreCase("doc")){
				oldFileName=search.getResume();
				newFileName=new File("resume.doc");
				oldFileName.renameTo(newFileName);
				enableMail=true;
			}else if(extension.equalsIgnoreCase("pdf")){
				oldFileName=search.getResume();
				newFileName=new File("resume.pdf");
				oldFileName.renameTo(newFileName);
				enableMail=true;
			}else if(extension.equalsIgnoreCase("txt")){
				oldFileName=search.getResume();
				newFileName=new File("resume.txt");
				oldFileName.renameTo(newFileName);
				enableMail=true;
			}*/
			
			if( this.myFile!=null){
				File file = new File(getText("storage.resume.path") + "/", "resume.doc");
				FileUtils.copyFile(this.myFile, file);// copying resume
				enableMail=true;
			}
			
			resumePath=getText("storage.resume.output.path");
			if(enableMail){
				String strSupportMailId=null;
				
				java.util.Date date=new java.util.Date();
	        	Calendar calDate=Calendar.getInstance();
	        	calDate.setTime(date);
	        	calDate.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
	        	SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
			    String strCurrentDate = new SimpleDateFormat("yyyy-MM-dd").format(calDate.getTime());
			    java.util.Date currentDate = format1.parse(strCurrentDate);
			    Timestamp currentTS = new Timestamp(currentDate.getTime());
	        	int bookedHours=calDate.get(Calendar.HOUR);
	        	int bookedMinute=calDate.get(Calendar.MINUTE);
	        	int AMPM=calDate.get(Calendar.AM_PM);
	        	String strBookingDate=new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH).format(date);
	        	String hours="",minutes="",timeHours="";
	        	if(bookedHours<10){
	        		hours=String.valueOf(bookedHours);
	        		hours="0"+hours;
	        	}else{
	        		hours=String.valueOf(bookedHours);
	        	}
	        	if(bookedMinute<10){
	        		minutes=String.valueOf(bookedMinute);
	        		minutes="0"+minutes;
	        	}else{
	        		minutes=String.valueOf(bookedMinute);
	        	}
	        	if(AMPM==0){//If the current time is AM
	        		timeHours=strBookingDate+" "+hours+" : "+minutes+" AM ";
	        	}else if(AMPM==1){//If the current time is PM
	        		timeHours=strBookingDate+" "+hours+" : "+minutes+" PM ";
	        	}
				
				strSupportMailId="hr@ulohotels.com";
				

				strEmailId=search.getEmailId();
				//email template
				Configuration cfg = new Configuration();
				cfg.setClassForTemplateLoading(BookingCancelAction.class, "../../../");
				Template template = cfg.getTemplate(getText("resume.career.voucher.template"));
				Map<String, Object> rootMap = new HashMap<String, Object>();
				rootMap.put("voucherDate",timeHours);
				rootMap.put("applicantMailId",search.getEmailId());
				rootMap.put("applicantName",search.getEmployeeName());
				rootMap.put("applicantMessage",search.getMessage());
				rootMap.put("from", getText("notification.from"));
				Writer out = new StringWriter();
				template.process(rootMap, out);
				//out.flush();
				
				String strSubject="Resume - "+search.getEmployeeName();

				Email em = new Email();
				em.set_to(strEmailId);
				em.set_cc(strSupportMailId);
				em.set_from(getText("email.from"));
				em.set_username(getText("email.username"));
				em.set_subject(strSubject);
				em.set_bodyContent(out);
	            
//				out.flush();
	            em.set_fileName(resumePath);
	            em.sendResumes();
	            output="success";
			}else{
				output="error";
			}

			
		}catch(Exception e){
			output="Some technical issue on upload resume api";
			e.printStackTrace();
			output="error";
		}
		return output;
	}
	
	public String getFileExtension(File file) {
        String extension = "";
 
        try {
            if (file != null && !file.exists()) {
                String name = file.getName();
                extension = name.substring(name.lastIndexOf("."));
            }
        } catch (Exception e) {
            extension = "";
        }
 
        return extension;
 
    }

	public String addPropertyPhoto() {
		
		try {
			this.propertyId = (Integer) sessionMap.get("propertyId");
			PropertyPhotoManager photoController = new PropertyPhotoManager();
			PmsPropertyManager propertyController = new PmsPropertyManager();
			PmsPhotoCategoryManager categoryController = new PmsPhotoCategoryManager();
            PropertyAccommodationManager propertyAccommodationController = new PropertyAccommodationManager();
			//UserLoginManager  userController = new UserLoginManager();
						
			PropertyPhoto photo = new PropertyPhoto();
			
			
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);

			if(this.getMyFileFileName()!=null && !this.getMyFileFileName().equalsIgnoreCase(""))
			{
				PmsProperty property = propertyController.find(getPropertyId());
				photo.setPhotoPath(this.getMyFileFileName());
				photo.setIsActive(true);
				photo.setIsDeleted(false);
                if(getPropertyAccommodationId()!=null){
				PropertyAccommodation propertyAccommodation = propertyAccommodationController.find(getPropertyAccommodationId());
			    photo.setPropertyAccommodation(propertyAccommodation);
				}
                photo.setPmsProperty(property);
                if(getPhotoCategoryId() != null){
				PmsPhotoCategory category = categoryController.find(getPhotoCategoryId());
				photo.setPmsPhotoCategory(category);
                }
				PropertyPhoto photo1 = photoController.add(photo);
				
			 	
				if(photo1.getPropertyPhotoId()>0 && this.getMyFileFileName()!=null && !this.getMyFileFileName().equalsIgnoreCase(""))
				{
					File file = new File(getText("storage.property.photo") + "/"
							+ getPropertyId() + "/" + photo1.getPropertyPhotoId() + "/", this.getMyFileFileName());
					FileUtils.copyFile(this.myFile, file);// copying image in the
				}
				
				
			}
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}
	
	
	public String deletePropertyPhoto() {
		this.propertyId = (Integer) sessionMap.get("propertyId");
		PropertyPhotoManager photoController = new PropertyPhotoManager();
		UserLoginManager  userController = new UserLoginManager();
		try {
			
						
			PropertyPhoto photo = photoController.find(getPropertyPhotoId());
						
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			photo.setIsActive(false);
			photo.setIsDeleted(true);
			photo.setModifiedDate(new java.sql.Timestamp(date.getTime()));
			photoController.edit(photo);
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}
	
	public String updatePropertyPhoto() {

		try {
			
			PropertyPhotoManager photoController = new PropertyPhotoManager();						
			PropertyPhoto photo = photoController.find(getPropertyPhotoId());
						
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			photo.setImageAltName(getImageAltName());
			photo.setModifiedDate(new java.sql.Timestamp(date.getTime()));
			photoController.edit(photo);
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}
	
	public String getPropertyPicture() {
		
		try {
			this.propertyId = (Integer) sessionMap.get("propertyId");
			HttpServletResponse response = ServletActionContext.getResponse();

			PropertyPhotoManager photoController = new PropertyPhotoManager();
			PropertyPhoto photo = photoController.find(getPropertyPhotoId());
			String imagePath = "";
			// response.setContentType("");
			if (getPropertyPhotoId() >0 ) {
				imagePath = getText("storage.property.photo") + "/"
						+ photo.getPmsProperty().getPropertyId()+ "/"
						+ photo.getPropertyPhotoId() + "/"
						+ photo.getPhotoPath();
				Image im = new Image();
				response.getOutputStream().write(
						im.getCustomImageInBytes(imagePath));
				response.getOutputStream().flush();
			} else {
				imagePath = getText("storage.path") + "/emptyprofilepic.png";
				Image im = new Image();
				response.getOutputStream().write(
						im.getCustomImageInBytes(imagePath));
				response.getOutputStream().flush();
			}
		} catch (Exception e1) {
			logger.error(e1);
		} finally {

		}
		return null;

	}
	
	
	public String addPromotionImage() throws IOException {
		this.propertyId = (Integer) sessionMap.get("propertyId");
		PromotionImageManager promotionimageController = new PromotionImageManager();
		
		
		try {
			this.promotionImageList = promotionimageController.list();
			if(promotionImageList.size() < 2 ){
				PromotionImages photo = new PromotionImages();
				
				
				long time = System.currentTimeMillis();
				java.sql.Date date = new java.sql.Date(time);
				BufferedImage image = ImageIO.read(this.myFile);
				int height = image.getHeight();
				int width = image.getWidth();
				
				if(height <= 5000 && width <= 5000){
					if(this.getMyFileFileName()!=null && !this.getMyFileFileName().equalsIgnoreCase(""))
					{
						photo.setPromotionImagePath(this.getMyFileFileName());
						photo.setIsActive(true);
						photo.setIsDeleted(false);
						//photo.setPmsProperty(property);
						//PmsPhotoCategory category = categoryController.find(getPhotoCategoryId());
						//photo.setPmsPhotoCategory(category);
						PromotionImages promotionimage = promotionimageController.add(photo);
						if( this.getMyFileFileName()!=null && !this.getMyFileFileName().equalsIgnoreCase("")){
							File file = new File(getText("storage.promotion.photo") + "/", this.getMyFileFileName());
							FileUtils.copyFile(this.myFile, file);// copying image in the
						}
					}else{
						
					}
				}
			}
			else{
				
			}
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}
	
	
	public String getPromotionImages() throws IOException {
		this.propertyId = (Integer) sessionMap.get("propertyId");
		
		try {

			String jsonOutput = "";
			String imagePath= "";

			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PromotionImageManager promotionimageController = new PromotionImageManager();
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");
			Integer val = 1;
			this.promotionImageList = promotionimageController.list();
			for (PromotionImages promotion : promotionImageList) {

				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				imagePath = getText("storage.promotion.photo") + "/"

						+ "uloimg"+"/"+val+"/"+val+".png";
				jsonOutput += "\"DT_RowId\":\"" + promotion.getPromotionImageId() + "\"";
				jsonOutput += ",\"imagePath\":\"" + imagePath + "\"";
				jsonOutput += ",\"photoPath\":\"" + promotion.getPromotionImagePath()+ "\"";
				jsonOutput += ",\"photoPathUrl\":\"" + (promotion.getPromotionImageUrl() == null?"" : promotion.getPromotionImageUrl()) + "\"";
				//jsonOutput += "\"size\":\"" +promotionImageList.size()+ "\"";
				val++;
				
				jsonOutput += "}";

			}
			

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}

	public String getPromotionImagesAPI() throws IOException {
		String output="";
		try {
			int count=0;
			String jsonOutput = "";
			String imagePath= "";
			PromotionImageManager promotionimageController = new PromotionImageManager();
			Integer val = 1;
			this.promotionImageList = promotionimageController.list();
			if(this.promotionImageList.size()>0 && !this.promotionImageList.isEmpty()){
				for (PromotionImages promotion : promotionImageList) {

					if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += ",{";
					else
						jsonOutput += "{";
					imagePath = getText("storage.promotion.photo") + "/"

							+ "uloimg"+"/"+val+"/"+val+".png";
					jsonOutput += "\"DT_RowId\":\"" + promotion.getPromotionImageId() + "\"";
					jsonOutput += ",\"imagePath\":\"" + imagePath + "\"";
					jsonOutput += ",\"photoPath\":\"" + promotion.getPromotionImagePath()+ "\"";
					jsonOutput += ",\"photoPathUrl\":\"" + (promotion.getPromotionImageUrl() == null?"NA" : promotion.getPromotionImageUrl()) + "\"";
					val++;
					count++;
					jsonOutput += "}";

				}	
			}
			
			String data="\"data\":[" + jsonOutput + "]";
 			String strData=data.replaceAll("\"", "\"");
 			
 			if(count == 0){
 				output="{\"error\":\"1\",\"message\":\"No Records Found\"}";
			}else{
				output="{\"error\":\"0\"," + strData + "}";
			}

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return output;

	}
	
	public String savePromotionImageUrl() throws IOException {
		this.propertyId = (Integer) sessionMap.get("propertyId");
		
		try {
			
			PromotionImageManager promotionimageController = new PromotionImageManager();
			PromotionImages promotion = promotionimageController.find(getPromotionImageId());
			
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			promotion.setPromotionImageUrl(getPromotionImageUrl());
			
			promotionimageController.edit(promotion);
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	public String deletePromotionImage() {
		
		PropertyPhotoManager photoController = new PropertyPhotoManager();
		UserLoginManager  userController = new UserLoginManager();
		try {
			
						
			PromotionImageManager promotionimageController = new PromotionImageManager();
			PromotionImages promotion = promotionimageController.find(getPromotionImageId());
						
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			promotion.setIsActive(false);
			promotion.setIsDeleted(true);
			promotion.setModifiedDate(new java.sql.Timestamp(date.getTime()));
			promotionimageController.edit(promotion);
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}
	
	
	public String getPromotionImageStream() {
		
		this.propertyId = (Integer) sessionMap.get("propertyId");
		HttpServletResponse response = ServletActionContext.getResponse();
		try {
		PromotionImageManager promotionimageController = new PromotionImageManager();
		this.promotionImageList = promotionimageController.findId(getPromotionImageId());
		for (PromotionImages promotion : promotionImageList) {
		String imagePath = "";
		// response.setContentType("");
		
			if (promotion.getPromotionImageId() >0 ) {
				imagePath = getText("storage.promotion.photo") + "/"
						
						+ promotion.getPromotionImagePath();
				Image im = new Image();
				response.getOutputStream().write(
						im.getCustomImageInBytes(imagePath));
				response.getOutputStream().flush();
			}
		else {
				imagePath = getText("storage.path") + "/emptyprofilepic.png";
				Image im = new Image();
				response.getOutputStream().write(
						im.getCustomImageInBytes(imagePath));
				response.getOutputStream().flush();
			}
		}
		}
		
		catch (Exception e1) {
			logger.error(e1);
			e1.printStackTrace();
		} finally {

		}
		return null;

	}

	public String getAllCategoryPhotos(){
		 try{
			 this.propertyId = (Integer) sessionMap.get("propertyId");
			 String jsonOutput = "";
				HttpServletResponse response = ServletActionContext.getResponse();
				PropertyPhotoManager photoController = new PropertyPhotoManager();
				PmsPhotoCategoryManager categoryController=new PmsPhotoCategoryManager();
				response.setContentType("application/json");
	
				List<PmsPhotoCategory> photoCategoryList = categoryController.list();
				for (PmsPhotoCategory photoCategory : photoCategoryList) {
		              			
					if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += ",{";
					else
						jsonOutput += "{";
					
						jsonOutput += "\"DT_RowId\":\"" + photoCategory.getPhotoCategoryId() + "\"";
						jsonOutput += ",\"type\":\"" + photoCategory.getPhotoCategoryName()+ "\"";
						
						String jsonImages="";
						this.propertyPhotoList = photoController.list1(getPropertyId(),photoCategory.getPhotoCategoryId());
						if(this.propertyPhotoList.size()>0 && !this.propertyPhotoList.isEmpty()){
							for(PropertyPhoto photo:propertyPhotoList){
								if (!jsonImages.equalsIgnoreCase(""))
									
									jsonImages += ",{";
								else
									jsonImages += "{";
								
								jsonImages += "\"DT_RowId\":\"" + photo.getPropertyPhotoId() + "\"";
								jsonImages += ",\"isPrimary\":\"" + photo.getIsPrimary()+ "\"";
								jsonImages += ",\"photoPath\":\"" + photo.getPhotoPath()+ "\"";
								jsonImages += ",\"photoCategoryId\":\"" + photo.getPmsPhotoCategory().getPhotoCategoryId()+ "\"";
							
								jsonImages += "}";
							}
						}
						
						jsonOutput += ",\"images\":[" + jsonImages + "]";
						
						
						jsonOutput += "}";
	
	
				}
				
	
				response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
	
		 }catch(Exception ex){
			 ex.printStackTrace();
		 }
		 return null;
	}
	
	

	public File getMyFile() {
		return myFile;
	}

	public void setMyFile(File myFile) {
		this.myFile = myFile;
	}

	public String getMyFileContentType() {
		return myFileContentType;
	}

	public void setMyFileContentType(String myFileContentType) {
		this.myFileContentType = myFileContentType;
	}

	public String getMyFileFileName() {
		return myFileFileName;
	}

	public void setMyFileFileName(String myFileFileName) {
		this.myFileFileName = myFileFileName;
	}

	public String getPromotionImageUrl() {
		return promotionImageUrl;
	}

	public void setPromotionImageUrl(String promotionImageUrl) {
		this.promotionImageUrl = promotionImageUrl;
	}

	public Integer getPropertyPhotoId() {
		return propertyPhotoId;
	}

	public void setPropertyPhotoId(Integer propertyPhotoId) {
		this.propertyPhotoId = propertyPhotoId;
	}

	public Integer getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(Integer propertyId) {
		this.propertyId = propertyId;
	}

	public Boolean getIsPrimary() {
		return isPrimary;
	}

	public void setIsPrimary(Boolean isPrimary) {
		this.isPrimary = isPrimary;
	}
	
	public Integer getPromotionImageId() {
		return promotionImageId;
	}

	public void setPromotionImageId(Integer promotionImageId) {
		this.promotionImageId = promotionImageId;
	}

	public PropertyPhoto getPropertyPhoto() {
		return propertyPhoto;
	}

	public void setPropertyPhoto(PropertyPhoto propertyPhoto) {
		this.propertyPhoto = propertyPhoto;
	}
	
	public Integer getPhotoCategoryId() {
		return photoCategoryId;
	}

	public void setPhotoCategoryId(Integer photoCategoryId) {
		this.photoCategoryId = photoCategoryId;
	}

	public List<PropertyPhoto> getPropertyPhotoList() {
		return propertyPhotoList;
	}

	public void setPropertyPhotoList(List<PropertyPhoto> propertyPhotoList) {
		this.propertyPhotoList = propertyPhotoList;
	}
	
    	public Integer getPropertyAccommodationId() {
		return propertyAccommodationId;
	}

	public void setPropertyAccommodationId(Integer propertyAccommodationId) {
		this.propertyAccommodationId = propertyAccommodationId;
	}
    
	public List<PropertyAccommodation> getAccommodationList() {
		return accommodationList;
	}

	public void setAccommodationList(List<PropertyAccommodation> accommodationList) {
		this.accommodationList = accommodationList;
	}
	
	public List<PropertyPhoto> getCategoryPhotoList() {
		return categoryPhotoList;
	}

	public void setCategoryPhotoList(List<PropertyPhoto> categoryPhotoList) {
		this.categoryPhotoList = categoryPhotoList;
	}

	public String getImageAltName() {
		return imageAltName;
	}

	public void setImageAltName(String imageAltName) {
		this.imageAltName = imageAltName;
	}
	



}
