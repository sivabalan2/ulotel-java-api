package com.ulopms.view;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.net.URL;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.ServletRequestAware;

//import com.medwecare.controller.AwsObjectManager;
//import com.medwecare.controller.CommentManager;
//import com.medwecare.controller.CommentUserManager;

//import com.medwecare.controller.NotificationManager;
//import com.medwecare.controller.ProjectManager;

//import com.medwecare.model.AwsObject;
//import com.medwecare.model.Notification;
//import com.medwecare.model.ObjectComment;
//import com.medwecare.model.ObjectCommentUser;
//import com.medwecare.model.Project;






import com.opensymphony.xwork2.ActionSupport;

import freemarker.template.Configuration;
import freemarker.template.Template;

import javax.servlet.http.HttpServletRequest;

import com.opensymphony.xwork2.ModelDriven;
import com.ulopms.controller.SeoContentManager;
import com.ulopms.controller.UserLoginManager;
import com.ulopms.controller.PmsPropertyManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.SeoContent;
import com.ulopms.model.User;
import com.ulopms.model.PmsProperty;
import com.ulopms.util.Email;

public class LandingAction extends ActionSupport implements
ServletRequestAware, UserAware{

	private static final long serialVersionUID = 914982665758390091L;

	
	
	private Object model;

	

	private static final Logger logger = Logger.getLogger(LandingAction.class);

	
	private User user;

	private String metaTag;
	private String metaDescription;
	private String webContent;
	private int locationId;
	private int areaId;
	
	public int getAreaId() {
		return areaId;
	}

	public void setAreaId(int areaId) {
		this.areaId = areaId;
	}

	public int getLocationId() {
		return locationId;
	}

	public void setLocationId(int locationId) {
		this.locationId = locationId;
	}
	
	public String getMetaTag() {
		return metaTag;
	}

	public void setMetaTag(String metaTag){
		this.metaTag = metaTag;
	}
	
	public String getMetaDescription() {
		return metaDescription;
	}

	public void setMetaDescription(String memtaDescription){
		this.metaDescription = metaDescription;
	}
	
	public String getWebContent() {
		return webContent;
	}

	public void setWebContent(String webContent){
		this.webContent = webContent;
	}

	@Override
	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	List<PmsProperty> listPmsProperty;

	private HttpServletRequest request;

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}
	
	
	private SessionMap<String,Object> sessionMap;  
	  
	//getters and setters  
	  
	public void setSession(Map<String, Object> map) {  
	    sessionMap=(SessionMap)map;  
	}  
	
	private HttpSession session;
	
	
	
	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}

	public LandingAction() {

	}
	
	
	
	public String landing() {
		
		try{
	
		SeoContentManager seoContentController = new SeoContentManager();
		 
			 URL url = new URL(request.getRequestURL().toString());  
		 
	     SeoContent seoContent = seoContentController.findSeoContent(this.locationId);	
			
	        request.setAttribute("metaTag" ,seoContent.getTitle()==null?"":seoContent.getTitle());
			request.setAttribute("description" ,seoContent.getDescription()==null?"":seoContent.getDescription());
			request.setAttribute("content" ,seoContent.getContent()==null?"":seoContent.getContent());
		
		/*if(this.locationId== 1){
		//request.setAttribute("metaTag", " coorg entered");
		//this.metaTag = "Have a comfy stay at the best hotels in Coorg within your budget";
		this.setMetaTag("Have a comfy stay at the best hotels in Coorg within your budget");
		this.setMetaDescription("Explore the ambience of Coorg by staying at the best resorts in Coorg. Ulo Riviera The Spring Arc &amp; Ulo Alipinia Estate resort are budget friendly hotels that gives you the feel of homestay.");
		this.setWebContent("Coorg, also called as Kodagu is a hill station in Karnataka. It is known as the Scotland of India. The note worthy places in Coorg are Madikeri fort, Omkareshwara Temple, Raja�s tomb, Abbi Falls, Tala Cauvery, Gothic-style church and museum. The best time to visit Coorg is between October and March. It is a evergreen place with pleasant climate throughout the year. The Storm festival takes place every February. It is recommended to visit Coorg during summer and winter. It is well known for honey, handmade chocolates, coffee and spice plantations. The scenic charms allure many thrill seekers and nature lovers to this wonderful place. The beauty of this place mesmerizes many tourists to stay in Coorg for a long period of time. Coorg is best suited for family vacations. It is the perfect place for people who love trekking in this breathtaking valleys and mountains. It is a wonderful and safe adventure destination. You must have your travel documents all the time to avoid any inconvenience. You can visit Kushal Nagar Market and Friday Market to buy best quality coffee, spices, nuts and chocolates. You can spot the budget hotels in Coorg with tariff if you search it using the right website. The hotel booking in Coorg, Karnataka is made easier by online booking feature. The bugging question which comes to everyone is where to stay in Coorg?. Coorg has hotels or resorts to suit every traveller�s budget. Search for Coorg places to stay before planning your trip. It can be a boutique resort, budget friendly hotels or home stays and camps.If you plan a trip, the first step is to search for the good resorts in Coorg with all basic amenities. You can locate your ideal hotels and resorts in Coorg by browsing the internet. There are many hotels in Coorg for your stay. Check criteria like cost, travel, food, location, room service, etc., before you book your room. You can search the hotels in Coorg with price on any travel website. There are plenty of lodges in Coorg which are available at moderate prices. Coorg hotels and resorts rates depends upon the location and services provided. You can get the best resorts in Coorg if you are willing to spend some money. Before booking a hotel or resort, check whether it is near to the tourist spots. Nowadays, most of the resorts provide internet connections as it became a major part of our life.You can stay at any 3 star hotels in Coorg based on your budget. You can get many cottages in Coorg at reasonable price. There are many best hotels in Coorg which provides a escape for people from the stressful world. You can check the Coorg hotels price list any time by checking the websites. You have a handful of options when it comes to booking a best place to stay in Coorg. If you are booking online, then you can view all the resorts in Coorg with price. You can book it according to your budget by scrutinizing all available.When it comes to Coorg homestay, you have ample of choices. You should look harder to find a cheap homestay in Coorg. The homestay in Coorg for family is enjoyable if they have special amenities for kids as well as elders. Find the perfect homestay in Coorg with price, location and other features in any best website. You can get valuable deals for cheap hotels in Coorg if you book utilizing any apps. View the resorts in Coorg with tariff before paying for any rooms.Check the hotel reviews prior booking the room on trusted websites. Compares rates of different hotels to find the convenient one for you. There are many best resorts in Coorg for honeymoon couples.Book any resort on booking websites like www.ulohotels.com which provides you with multiple options. You can find the resorts in Coorg with price in this website.The budget hotels in Coorg are ideal for solo travellers and group of friends who wants to spend less on rooms. The best resorts in Coorg, Madikeri are Ulo Riviera The Spring arc and Ulo Alpinia Estate resort.They are located on the hills surrounded by the spice and copy plantation making our stay at Coorg blissful. It provides services like television, complimentary breakfast, personal assistance, bonfire, and extra bed. You have many options for booking rooms like Deluxe rooms, Family Deluxe room and Economy suite.If you search for top resorts in Coorg, Ulo Hotels Group will be one among them. The location and the services provided here makes them the best honeymoon resorts in Coorg. These two are the best budget resorts in Coorg for family. They comes under the top 10 resorts in Coorg. It is comfortable and easy to book the rooms online.The price of the room mentioned in the website is inclusive of all the taxes. They have both budget and luxury rooms. You can book any budget resorts in Coorg with a single touch by utilizing the online booking feature in your mobile phones. You need not to pay any booking fees and they also provide 24/7 Customer service. The resort check-in time is 10 A.M and check-out time is 9 A.M. It is recommended to call and confirm your booking to avoid any issues. Visit www.ulohotels.com to clear any doubts related to booking. There are also many deals available if you book the rooms online.The rooms are equipped with air conditioner, television, laundry,etc,. It have 24/7 hot and cold water supply. The bonfire is also arranged if requested by the tourists. The charges will be based only on the number of rooms you are booking. The cancellation and prepayment policies differs according to the room type. Verify the Fare policy related to your room. You can pay online or directly at the hotel if the room you have chosen supports this option.The price is reasonable when compared to other resorts in Coorg.You can also book rooms with terrace or balcony. You can arrange for a local tour from the travel agency available at the travel desk.They provide car for rentals and you can also hire a guide. You can keep your belongings in your personal lockers. It is the only resort in Coorg which provides Rain Dance. They give complimentary gala dinner on New year and Christmas. Free private parking is available for all customers. The pets are allowed inside the resort. You should display your identification proofs upon check-in. Every room is equipped with a personal bathroom fitted with bath. It serves yummy Breakfast and Dinner daily. If you desire, you can even have your breakfast in your room.It is situated near the major tourist spots and helps the tourists to travel easily. You can find many hotels in Madikeri near bus stand and the best one among them is Ulo Hotels. It is located in 11 kms from the Madikeri Bus Stand on Mangalore Highway. The aroma from the spice and coffee plantation helps to refresh yourselves. It will be definitely be a memorable experience to stay in Coorg.");
		
		}
		else if(this.locationId== 3){
			
			this.setMetaTag("Book Kodaikanal Resorts and Hotels @ Great Rates");
			this.setMetaDescription("Make your trip wonderful by staying at the best luxury and budget resorts in Kodaikanal. ULO SMS Residency, ULO Kodai Green &amp; ULO Muthu Residency are ideal resorts for perfect vacation.");
			this.setWebContent("Kodaikanal is popularly known as the Princess of Hills. It is famous for its green,fresh and scenic environment. It is located @ 2,000 metres above sea level. It is covered with forest, lakes, waterfalls, granite and grassy hills. The points of interest are, Kodaikanal Lake, Kodaikanal Solar Observatory, Silver Cascade Falls, Pine Tree Forest, Dolphin Nose, etc,.It has chill climate for the whole year with seasonal rainfall. Choose a hotel which is present on the top of the hill to get a better view of this place. The famous foods in Kodaikanal are, chocolates, brownies, hot chai or tea and cheeses. Visiting Kodaikanal using travel package is best option if you want to see all the major tourist places.The best time to visit this place is from March to June or December to February. You can go for trekking, horse riding, boating etc,. Only few ATM�s are available in Kodaikanal, so try to carry sufficient amount of money with you. It is the most visited honeymoon destinations in South India. This is a dream destination for family vacation, adventure seekers and honeymoon couples.There are handful of choices for accommodation in Kodaikanal. Look for the list of Kodaikanal hotels and resorts prior booking your room. You can check the best resorts in Kodaikanal with rates from any valid booking websites. Find your ideal Kodaikanal resorts package by logging into the website www.ulohotels.com. Check Kodaikanal resorts list before planning your holiday.If your budget is high, you can reserve any luxury hotels in Kodaikanal. The Kodaikanal resorts price list may slightly differ in some websites.The best hotels in Kodaikanal are the ones which satisfies all your requirements. You can find many best resorts in Kodaikanal at affordable price. The best place to stay in Kodaikanal is the one which is easily accessible.If you travel with a large group, you can get many best cottages in Kodaikanal. ULO SMS Residency and ULO Muthu Residency are the two best hotels in Kodaikanal for family. Log into www.ulohotels.com to locate all hotels in Kodaikanal with rates. You can even book top hotels in Kodaikanal with great deals offered by booking sites. The luxury resorts in Kodaikanal comes with additional features like swimming pools, indoor games, refrigerator, etc,.You can find the hotels in Kodaikanal with phone numbers in ULO Hotels Group official page. Our Kodaikanal hotels rates are moderate when compared to other hotels in the locality. You will have multiple options when it comes to places to stay in Kodaikanal. If you want to limit your travel expenses, view the budget hotels in Kodaikanal with rates in any authorized websites.The cottages in Kodaikanal are generally situated near hills or forest areas. Book independent cottages in Kodaikanal to have a private vacation with your close ones. Make sure that you reserve only family cottages in Kodaikanal if you stay with your family. You can find budget cottages in Kodaikanal with rates in our website. The lodges in Kodaikanal are best suited if you travel alone.ULO Kodai Green is one of the few hotels in Kodaikanal near lake. It is the best lake view resort in Kodaikanal surrounded by the stunning beauty of nature. It is a boutique style hotel which you can rent for few days. It is located at the centre of the city which makes it easily approachable. Many famous tourist spots are located around 4km from the hotel.It has Standard and family rooms with the amenities like TV, AC, Laundry, and 24 hours room service. You can also avail discount of 15%. Even if you cancel the room 24 hours prior the trip, you can receive full refund. The hotel booking in Kodaikanal can be done either by online or directly at the hotels. One can get best offers in cheap hotels in Kodaikanal if you book online with the promo code.If there is a budget cut, rest in any cheap cottages in Kodaikanal. There are plenty of good hotels in Kodaikanal to stay alone or with your family. If you travel as a couple, you can find many good resorts in Kodaikanal. On the basis of your budget, you can choose any 3 star or 5 star hotels in Kodaikanal. The list of hotels in Kodaikanal with tariff can be downloaded from the internet. The cost is calculated with respect to the number of rooms in all Kodaikanal ULO hotels.If you desire to cut short your expenses, try to stay in the dormitory in Kodaikanal. The 3 bedroom cottages in kodaikanal are a wise option if you stay with a large group or family. You can self-cook or arrange a maid for cooking if you stay in a cottage.If you long for your mother�s care, try homestay in Kodaikanal. The homestay in Kodaikanal is cheaper than booking cottages.ULO Muthu Residency provides best accommodation to all its customers. Most of the popular tourist places are located within 7 kms from this hotel. The Bus Stand is also 4 km from the hotel. Each room is equipped with Air conditioners, TV, tables, chairs and personal bathroom. There is also 24*7 hot and cold waeter supply. They also have facilities like, Wi-Fi, laundry, private parking, room service, rental transport and even doctors. ULO SMS Residency is one of the great Kodaikanal hotels near bus stand. They offer services like, laundry, room service, personal parking, free internet, restaurant and also arranging travels. They provide 24/7 room service and hot or cold water supply. You can get extra bed or mattress on request. Make use of meditation room to do yoga or any other exercises. As per the customer�s request, they arrange campfire also. If required, same your valuables in personal lockers. You can book the room online and pay for it directly at the hotel. If you want to make your vacation comfortable, rest in the best villas in Kodaikanal. Many corporates or banks have their guest houses in Kodaikanalfor their employees. Reserve any mount view Kodaikanal hotels on www.ulohotels.com @ starting price of Rs.999. If you want to have a best vacation ever, don�t worry about the money spent.");
			
		}
		else if(this.locationId== 4){
			
			this.setMetaTag("Best Budget Resorts in Yelagiri with Great Ambience @ LowestPrices");
			this.setMetaDescription("Reserve the best low budget luxury hotels and resorts in Yelagiri. Get great deals, offers and discounts @ ULO Hill Breeze, one of the fine resorts on Yelagiri.");
			this.setWebContent("Yelagiri is one of the famous hill stations in Tamil Nadu. It is located at the height of 1,110.6 metres above sea level. It is fully covered with valleys, rose- garden and orchads. The main tourist places to visit are, Jalagamparai Waterfalls, Murugan Temple, and Yelagiri Forest Hill. The best months to visit Yelagiri are November to February. You can buy local honey and jackfruit from the local markets. It is an amazing place for people who love exploring the nature. It is an excellent place for trekking and meditation. Stay in the best hotels in Yelagiri to enjoy the greenery and mesmerizing beauty of Yelagiri. To book your hotel rooms, search as Yelagiri hotels booking in any authenticated booking sites. You will get the list of hotels in Yelagiri with tariff. Select the hotel or resort which is suitable for you. If you want to stay in a homely surrounding, opt for homestay in Yelagiri Hills. If you plan a vacation, you won�t just pack your suitcases and rush to the trip. The crucial step in your trip is to find a good accommodation in Yelagiri. If your family has small kids, they mostly prefer cottages in Yelagiri Hills with swimming pools. Each cottage has a separate swimming pool. You can hire separate cook and maid while you stay at the cottages. If you want to spend your vacation with your family free from other disturbances, book 3 star cottages in Yellagiri. If you are a bachelor or bunch of friends travelling together, book cheap hotels in Yelagiri to save money. If you can enjoy the complete beauty of Yelagiri from your balcony, then that can be called as the best resort in Yelagiri. Ulo Hill Breeze is one of the best hotels in Yelagiri which provide great service for reasonable price. You can say any resorts as the best resorts in Yelagiri if it matches all your expectations.If you search the internet, you will find Ulo Hotel Groups as one of the best places to stay in Yelagiri. If you reserve the cottages online, you can get discounts in Yelagiri cottages prices. One can say that Ulo hill Breeze is one of the best rated Yelagiri Hills resorts in any booking websites. Nowadays, most of the Yelagiri resorts comes with common swimming pool. If you book it online, they also give you upto 50% discounts on total price. Pick any resorts in Yelagiri Hills which is easily accessible via any transport facility. You can reserve even cheap resorts in Yelagiri with swimming pool @ low rates in www.ulohotels.com. See the Yelagiri resorts list in websites and then choose the perfect resort for you. Book any luxury Yelagiri hotels and resorts online @ special price. If you want hotels which is value for money, reserve 3 star budget hotels in Yelagiri like Ulo Hill Breeze. You can find budget hotels in Yelagiri with tariff at their official website. Once the rooms are booked, you will receive either confirmation message or mail within few minutes. They also have 24/7 Customer Support to answer all your queries. They also provide quick refunds if you cancel your booking. The rooms are spacious and are cleaned daily. You can park your cars in private car parking. They have separate desk for local travel agency using which you can book any travel packages. All the rooms have separate bathroom and TV. You can order food directly to your rooms from the restaurant. They have special cottages for honeymoon couples as well.It is also located near the main tourist spots in Yelagiri Hills. If you want to book best budget hotels, then Ulo Hotel Groups is a wonderful choice.");
			
		}
		
		else if(this.locationId== 5){
			
			this.setMetaTag("Experience the hospitality of the amazing Kolli Hills Resorts and Hotels");
			this.setMetaDescription("Great savings on Kolli Hills Resorts and Hotels booking online. ULO Rejoice Villa Resorts in Kolli hills offers great rooms in great prices with best facilities.");
			this.setWebContent("Kolli Hills, also called as Kollimalai is located in Namakkal district in Tamil Nadu. It is located at 1000 to 1300 metres above sea level. It is famous for jackfruit and wild honey which is harvested from the mountains. It is frequently visited by hikers, nature lovers and trekking associations. Kolli Hills is known for its rich and diverse flora and fauna. There are mainly 9 tourist places in Kollimalai which is visited by all tourists. Stay in the best Kolli Hills resorts to enjoy your trip to the fullest. Most of the Kollimalai hotels are situated amidst of hills with breathtaking view. You can get your ideal resorts in Kolli Hills by checking in www.ulohotels.com. The Kolli Hills resorts booking is made easier by booking sites or apps. If you are a family or a group, many Kolli Hills hotels provide you with special packages.  Kolli Hills trip is mostly preferred by couples and group. The best time to visit it is February to December. You can download Kolli Hills Travel Guide to know about places to visit in Kollimalai. Staying in kolli Hills rejuvenate your mind and body. Kolli hills provides the best accommodation  options for all the travellers making your trip pleasant. You can choose among luxurious resorts, budget hotels, lodges and cottages. If you are a honeymoon couple, booking a villa will be the best option. The first step in planning a vacation to Kollimalai is to explore the entire list of hotels in Kollimalai Hills.  To find the best suited room for you, type as Kolli Hills accommodation in www.ulohotels.com. You should assess all the criteria like travel expenses, location, cost, amenities etc, if you plan for Kolli Hills stay. When you look for places to stay in Kollimalai, choose a place which provides easy transportation. If you opt for Kolli Hills homestay, rent homes from people in Kollimalai. You can directly find the perfect homestay for you by searching in any booking websites. You have multiple options when it comes to places to stay in Kolli Hills which you can decide according to your budget. When you select Kollimalai cottages for your stay, you can book either 2 or 3 bedroom cottages based on head count. If you book your resorts online, you will get rooms at discount prices. There is no reservation cost if you book online. You can compare rates of different resorts and then select thee one which satisfies your needs and budget. You can make use of promo codes or coupons to get a discount on your rooms. Check for accommodation details in ULO Hotels official website. You can also get best offers and deals on online booking in Kollimalai. ULO Rejoice Villa resorts is one of the best Kollimalai resorts located near the Arapalliswarar Temple. It is best suited for family, couples and friends. sIt is situated at 6 km from the Kolli Hills. The room categories are, Standard room, Suite room, Family room and Economy suite. The check-in time is 10:00 A.M and check-out time is 12:00 P.M. If you want to change or cancel your booking, do it one week before your arrival date. Most of the tourist places are within 4 km from the resort. They have an on-site restaurant with free Wi-Fi. Each room is fitted with a private bathroom and TV. You can get any help from the 24*7 front desk. No reservation is required for public parking. One of the perks is free maid service. They also have Banquet or Meeting facilities. You can get 2 additional beds in a room if requested. You can enjoy the sunrise and sunset from your balcony. They also arrange campfire with music on customer�s request. ULO Rejoice Villa Resorts is a safe place for your family and friends. You can get best homemade foods when you stay there. The nearest airport, Tiruchirapalli Airport is only 90 minutes from the resort. Make the best of your time by staying @ one of the best resorts in Kollimalai.");
			
		}*/
    
		
			//PmsPropertyManager propertyController = new PmsPropertyManager();
			//listPmsProperty = propertyController.listPropertyByUser(getUser().getUserId());
		if(request.getParameter("locationId") != null && request.getParameter("locationId").toString() !="") this.setLocationId(Integer.parseInt(request.getParameter("locationId").toString()));
		else
			this.setLocationId(getLocationId());
		
		}catch(Exception e){
			logger.error(e);
			e.printStackTrace();
		}
		
			return SUCCESS;
	}
	
public String landingArea() {		
		
		
		if(this.areaId== 1){
		
		  String title = " <title>ULO: Book Hotels in Chennai, Free Breakfast | Free WiFi</title> ";
		  String description = "<h1><center>HOTELS IN OMR</center></h1> Popularly known as Rajiv Gandhi Salai, Old Mahabalipuram Road (OMR) is situated in Chennai of Tamil Nadu. It is a prime road linking Chennai with Mahabalipuram. It houses the famous SIPCOT (State Industries Promotion Corporation of Tamil Nadu Limited) and TIDEL Park, which is the base for many IT companies. OMR is also known as IT Corridor because of the multitude of corporate present here. Other than IT Parks and Automobile companies, there are many good attraction spots to visit near OMR like temples, theatre, theme parks, lake, museum, beaches, and much more. You should definitely head to these tourist spots during your leisure time. If you are looking for accommodations, there are many <a href=https://www.ulohotels.com/hotels-in-omr-chennai>rooms in OMR </a>where you can stay at a decent price.</br> Since OMR is located in Chennai, it also experiences the same tropical wet and dry climate like the rest of the part of Chennai. But, it is not true that the weather is hot throughout the year. The three seasons you can find in OMR, Chennai are the Summer, Monsoon, and Winter. The best time to visit OMR is from November to February (Winter Season) when the weather is generally chill and tolerable. It is advisable to wear light cotton clothes while staying in Chennai.</br> Just like the other parts of Chennai, Pongal and Tamil New Year are auspicious festivals in OMR, Chennai. The Pongal is celebrated every year in January for 4 days as a tribute to the farmers and working animals. The people will prepare sweet Pongal and present it to Sun God. According to Tamilians, April 14 (Chithirai 1) is regarded as the beginning day of the year. If you want to experience the best of Chennai�s culture and hospitality, there are many good <a href=https://www.ulohotels.com/hotels-in-omr-chennai>budget hotels in OMR</a>, Chennai which is available at affordable rates.</br> <b>ULO HOTELS IN OMR, CHENNAI:</b> If you are searching for your ideal Ulo Hotels, there are many good hotels near OMR, Chennai which is available at affordable prices. If you are looking for a budget-friendly stay, we also have 3-star hotels in OMR, Chennai, where you can stay even with a minimal budget. We offer pocket- friendly accommodations which are ideal for all types of travelers like couple, family, group travelers, female vacationers, and groups. With the modern day technology, it�s very simple to book hotels through online. Why worry when Ulo is here? Select Old Mahabalipuram Road hotels from Ulo Hotels that are the perfect accommodation option for a cozy stay within your budget.</br> <b>WHY CHOOSE A ULO IN OMR, CHENNAI:</b> The first thing that is considered while planning a trip is finding good quality accommodations in a prime location. When looking for hotels in OMR for a stay, do pick an Ulo Hotel. Ulo Hotels provides top-class amenities and services like Complimentary Breakfast, Free Wi-Fi, Welcome Drink, AC Rooms, an LCD TV with cable connection, Clean Bathrooms, Comfortable Beds, 24*7, Help Desk, and Car Parking. All our hotels are situated in main locations which can be reached easily. We are frequented by our guests because of our unique 5 Bs services like Best In Guest Service and Care, Bed Clean and Comfortable, Budget Friendly and Value, Breakfast Healthy and Regional and Bathroom Hygienic and Functional. You can easily book hotels in OMR, Chennai through Ulo Hotels� official website. It is quick with easy and secure transactions. <h2><center>THINGS TO DO IN OMR, CHENNAI</center></h2> <b>Places to Stay in OMR, Chennai:</b> When searching for hotels in OMR Road, Chennai, take its location, cost, facilities, and services into consideration before selecting your hotel. Ulo Hotels provides pocket-friendly stays that are affordable for every type of vacationers from budget travelers to luxury trippers. If you are looking for accommodations near major IT parks, book our hotels in Karapakkam. If you want rooms near IT Expressway, you can choose our hotels in Thoraipakkam. If you want budget accommodations, there are many good hotels in OMR, Chennai where you can stay at reasonable rates. We offer flexible check- in and check out for all our guests.</br> <b>Attractions in OMR, Chennai:</b> The trip to OMR in Chennai is not complete without visiting some of the best attraction spots which display the true face of Chennai�s culture. Right from exotic beaches to picturesque temples, there a lot to explore in OMR in Chennai. The Marina Beach is the world�s second largest beach in which charms thousands of trippers every year. Do visit holy shrines like Sai Baba Temple, ISKCON Temple, Marundeeswarar Temple, Sri Subramaniya Swami Temple and Ashtalakshmi Temple. If you like amusement parks for thrill seekers like VGP Universal Kingdom, MGM Dizzee World, and Kishkinta Theme Park. There are also many good shopping malls like Phoenix Market City, Citi Center, Brigade vantage Mall and Express Avenue, which are a haven for Shopaholics and movie freaks. Other attractions are Madras Crocodile Bank, Besant Nagar Beach, Elliot�s Beach and Cholamandal Artists� Village. OMR is well connected by bus and train routes.</br> <b>Food and Shopping:</b> If you are a shopping freak, OMR has the right tricks for you. It houses many malls and shopping centers where you can buy clothes, accessories, and souvenirs at a decent price. You can also head to the beach to buy seashell souvenirs and taste lip-smacking fresh seafood. When it comes to food, OMR is the king of street food. It is popular for its tasty and exotic kebabs, grills and rolls. The local eateries serve regional as well as international food at reasonable prices. The must-try items in OMR are Sushi, Atho, Shawarma Rolls, Biryani, Thattu Idli, Appam, Idli, Jumbo Dosai, Momos, Chats, Grill Chicken, and Chicken Kebabs. You will surely enjoy your trip to OMR in Chennai and do visit these attractions and eateries while you are in Chennai. Booking best hotels in OMR are made easier using Ulo Hotels� website. Save time, money, and energy by booking rooms with a single touch. If you want to explore the true culture of Chennai, there is no better place to stay in Chennai than OMR.";
		
		  request.setAttribute("metaTag" ,title);
		  request.setAttribute("description" ,description);
		
		}
		
		else if(this.areaId== 2){
		
		  String title = " <title>ULO: Book Hotels in Chennai, Free Breakfast | Free WiFi</title> ";
		  String description = "<h1><center>ABOUT VADAPALANI</center></h1> <b>HOTELS IN VADAPALANI, CHENNAI:</b> Set in the western part of Chennai, Vadapalani is one of the popular neighbourhood in Chennai. It is famous for the Vadapalani Murugan Temple and its film studios. Located in the heart of the city, Vadapalani is one of the busiest localities in Chennai. When it comes to places of interest, there are many theatres, shopping malls, and temples to explore in Vadapalani. The Vadapalani Bus Terminus is one of the prime bus terminals in Chennai. It has well-connected train and bus routes to remaining parts o the city. If you are looking for good accommodations, there are many good<a href=https://www.ulohotels.com/ulo-panasia-residency>rooms near Vadapalani </a>where you can stay at affordable prices.</br> <b>ULO HOTELS IN VADAPALANI, CHENNAI</b> If you are looking for your ideal Ulo Hotels in Chennai, there are many decent hotels in Vadapalaniwhich you can book at a decent price. If you are searchingbudget accommodations, we have Vadapalani hotels which you can book even with a minimal budget. We offerpocket-friendlyhotelswhich are best accommodation options for allkindsof travelers like couple, family, solo, female, and group vacationers. With the evolving trends in technology, making room reservations through online is made easier.Book your accommodations with Ulo Hotels and be relaxed. If you want book an accommodation in a prime location, book our hotels near Vadapalani. </br> <b>WHY CHOOSE AN ULO IN VADAPALANI, CHENNAI:</b> Once you are done with trip planning, the next thing you need to work on is finding a good hotel for your stay. When searching for Vadapalani hotel rooms, do choose an Ulo Hotel. Ulo Hotels offers you top-grade amenities and services like Complimentary Breakfast, Free Wi-Fi, Welcome Drink, AC Rooms, an LCD TV with cable connection, Hygienic Bathrooms, Comfortable Beds, 24*7 Service Desk, and Private Parking Space. All our hotels are situated in strategic locations so that you can travel lessto reach the hotel. The factor that differentiates us from others is our unique and quality 5 Bs services like Best In Guest Service and Care, Bed Clean and Comfortable, Budget Friendly and Value, Breakfast Healthy and Regional and Bathroom Hygienic and Functional. You can quicklyreserve ourhotel rooms in Vadapalani through Ulo Hotels�official website. Uloassures quick and safe transactions to all the guests.If you are looking for accommodation in Chennai, book our <a href=https://www.ulohotels.com/chennai-hotels-and-resorts>hotels in Chennai</a>with best amenities and services. If you want to stay near the major IT companies in Chennai, reserve our <a href=https://www.ulohotels.com/hotels-in-omr-chennai>hotels in OMR</a>. If you are looking for budget stays near the IT Expressway, choose <a href=https://www.ulohotels.com/ulo-hotel-hoilday-stay>hotels in Sholinganallur</a>. If want hotel rooms in the center of Chennai city, you can opt for <a href=https://www.ulohotels.com/ulo-hotel-chennai-deluxe-koyambedu>hotels in Koyambedu</a>. If you are searching for service apartments in Chennai, select our hotels in T Nagar.";
			
		  request.setAttribute("metaTag" ,title);
		  request.setAttribute("description" ,description);
			
			}
		
		else if(this.areaId== 3){
			
			String title = " <title>ULO: Book Hotels in Chennai, Free Breakfast | Free WiFi</title> ";
			String description = "<h1><center>ABOUT KOYAMBEDU</center></h1>Koyambedu is one of the main localities in Chennai. The Chennai Mofussil Bus Terminus (CMBT) is the largest bus Terminus in Asia. Koyambedu is one of the busiest and most frequented spots by trippersin Chennai. It boasts of its scenic attraction spots and friendly locals. From cultural attractions, bustling markets, to historical sites, there are many wonderful things to do in Koyambedu. You can explore many new places around Koyambedu that are worth your time. Your trip to Chennai is incomplete without seeing Koyambedu at least once. If you are looking for good stays, there are many rooms near Koyambedu where you can stay at a decent price.</br>Like other parts of Chennai, Koyambedu also experiences the same tropical wet and dry climate. There is a saying among vacationers that the climate in Chennai is hot throughout the year. But, it is purely a myth. You can find three seasons in Koyambedu, Chennai namely the Summer, Monsoon, and Winter. The ideal time to visit Koyambedu is from November to February (Winter Season) when the climate is usually pleasant and tolerable. It is recommended to wear light cotton clothes while staying in Koyambedu, Chennai.</br>Similar to the remaining parts of Chennai, festivals like Tamil New Year and Pongal is grandly celebrated in Koyambedu, Chennai. The Pongal is celebrated in January every year for 4 daysas a tribute to the farmers and farm animals. The Chennaites will preparea dish of sweet Pongal and present it to Sun God. According to Tamilians culture, April 14 (Chithirai 1) is regarded as the first day of the year. If you want to know more about the culture and cuisines of Chennai, you should definitely head to Koyambedu. There are many decent budget hotels in Koyambedu, Chennai where you can stay at reasonable prices.</br><b>ULO HOTELS IN KOYAMBEDU, CHENNAI:</b> If you are searching for your ideal Ulo Hotels, there are many good <a href=https://www.ulohotels.com/ulo-hotel-chennai-deluxe-koyambedu>Hotels In Koyambedu </a>which you can book at the best prices. If you are looking pocket-friendly accommodations, we also have 3-star hotels in Koyambedu, Chennai, which you can book even with a shoe-string budget. We also offerbudgetaccommodations that are perfect for allkindsof vacationers like couple, family, solo, female, and group trippers. With the present day evolutions in technology, it�s very easy to reserve hotel rooms through online.There is no need to worry when Ulo is here. If you want to stay in an accommodation that is easily accessible, book our hotels near Koyambedu bus stand.</br><b>WHY CHOOSE A ULO IN KOYAMBEDU, CHENNAI:</b> Once you are done with planning your trip, the next thing you need to focus on is finding a good accommodation. When looking for hotels near Koyambedu, Chennai,do choose an Ulo Hotel. Ulo Hotelsgives youfirst-grade amenities and services like Complimentary Breakfast, Free Wi-Fi, Welcome Drink, AC Rooms, an LCD TV with DTH connection, Neat Bathrooms, Comfortable Beds, 24*7 Service Desk, and Parking Space. All our hotels are set in key locations which make the traveling easier and reduce the travel time. The thing that differentiates us from othersis our unique and quality 5 Bs services like Best In Guest Service and Care, Bed Clean and Comfortable, Budget Friendly and Value, Breakfast Healthy and Regional and Bathroom Hygienic and Functional. You can quicklyreserve ourhotels in Koyambedu bus standthrough our official website. We guarantee quick and secure transactions to all our guests.</br><h1><center>THINGS TO DO IN KOYAMBEDU, CHENNAI</center></h1><b>Places to Stay in Koyambedu, Chennai:</b> When searching for hotels in Koyambedu, Chennai, take its location, room price, amenities, and services into consideration beforepicking your hotel. Ulo Hotels offerspocket-friendly accommodationsthat are suitable for all types of trippers from budget vacationers to luxury travelers. If you are looking for accommodations in prime spots, book our hotels near Koyambedu bus stand. If you want rooms near outstation bus terminus, you can choose ourhotels near Koyambedu Omni bus stand. If you are looking for budget rooms, there are many good hotels near Koyambedu, Chennai where you can stay at best prices. If you want rooms near the IT Expreessway, book <a href=https://www.ulohotels.com/hotels-in-omr-chennai>Hotels In OMR</a>. We also offer early check-in and check out for all our guests which arepurely subject to room availability and are chargeable.</br><b>Attractions in Koyambedu, Chennai:</b> The trip to Koyambedu in <a href=https://www.ulohotels.com/chennai-hotels-and-resorts>Chennai</a>is incomplete without seeing some of thepicturesque attraction spots which show the true tradition and values of Chennaites. Withbeautiful beaches to crowdedbazaars, there is a lot to see in Koyambedu in Chennai. Don�t forget to visit the world famous Marina Beach in Chennai. It is the world�s second largest beach which allures thousands of travelers every year. Do visit temples like Kapaleeshwarar Temple, Thirumala Thirupathi Devasthanam, Sri Parthasarathy Temple, and <a href=https://www.ulohotels.com/ulo-panasia-residency>Vadapalani</a>Murugan Temple. If you want to explore and know about the tribal of Rajasthan in Chennai, you should head to Chokhidhani. If you are a Shopaholic, headto amazing shopping malls like Forum Vijaya Mall, Chandra Metro Mall, and Sky Walk, which are a paradisefor movie lovers and shopping enthusiasts. If you are a history geek, head to the Chennai Rail Museum. Other attractions nearby Koyambedu are Pondy Bazaar, <a href=https://www.ulohotels.com/ulo-green-tree-service-apartment>T.Nagar</a>, St.Andrew�s Church, Bala Murali Theatre, and M.A.Chidambaram Stadium. Koyambedu haswell-connected bus and train routes. </br><b>Food and Shopping:</b> If you are a shopping enthusiast, there a lot of options to explore near Koyambedu. There are many malls, shopping centers, and restaurants in Koyambedu where you can buy clothes, accessories, and souvenirs at a reasonable price.Do head to the Marina Beach to buy seashell handicrafts and trytasty fish fry. When it comes to food, there are many good vegetarian and non vegetarian eateries around Koyambedu. It is famousfor its lip-smacking and exoticregional and international cuisines which you can taste at a decent price. The must-try items in Koyambedu are Biryanis, Dosa, Idli, Vadai, Poori, Grills, Barbeque, Chicken-65, and Fried Rice. You will definitely enjoy your trip to Koyambedu in Chennai and do explore these attraction spots and restaurants while you are in Koyambedu. Booking hotels in Koyambedu are made quick and safe using Ulo Hotels� official website.Conserve yourtime, energy, and money by booking hotel rooms with a single touch. If you want to knowmore aboutChennai, there is no better place to travel in Chennai than Koyambedu.";

			request.setAttribute("metaTag" ,title);
			request.setAttribute("description" ,description);
			
			}
		
		else if(this.areaId== 4){
		
			String title = " <title>ULO: Book Hotels in Chennai, Free Breakfast | Free WiFi</title> ";
			String description = "<h1><center>ABOUT T NAGAR</center></h1> Thyagaraya Nagar (T Nagar) is one of the most visited places in Chennai. It is the biggest shopping locality in India in terms of revenue. T Nagar is mainly frequented by shoppers. It is famous for its different varieties of Saree, gold, accessories, and furniture. Off the record, T Nagar is the Commercial Capital of Chennai. It has well-connected bus and train routes to other parts of the city. It is situated at just 8 km from the Chennai Central railway station and 10 km from Chennai Airport. If you are searching for good accommodations, there is a plethora of rooms in T Nagar where you can stay at a decent price.</br> <b>HOTELS IN T NAGAR:</b> If you are looking for your ideal Ulo Hotels, there are many good hotels in Chennai T Nagar which you can book at the reasonable prices. If you are looking for a pocket-friendly stays, we also have rooms near T Nagar where you can stay even with a minimum budget. If you want to find cheap stays, search hotels to stay in T Nagar Chennai. We provide budget stays that are apt for all kinds of trippers like the couple, family, solo, female, and group travelers. If you want to stay in a hotel that is set in a prime location, book our hotels rooms in T Nagar Chennai.</br> <b>SERVICE APARTMENTS IN T NAGAR:</b> If you are looking for accommodations to stay with your friends, book service apartments in Pondy Bazaar Chennai. When looking for service apartments near T Nagar, do choose an Ulo Hotel. We offer you first-class amenities and services like Complimentary Breakfast, Free Wi-Fi, Welcome Drink, AC Rooms, an LCD TV with cable, Hygienic Bathrooms, Comfortable Beds, Locker, 24*7 Service Desk, and Parking Space. All our service apartments are set in main locations which reduce your travel time and cost. The thing that makes us stand different from others is our unique and quality 5 Bs services like Best In Guest Service and Care, Bed Clean and Comfortable, Budget Friendly and Value, Breakfast Healthy and Regional and Bathroom Hygienic and Functional. You can easily reserve our service apartments in T Nagar on our official website. We assure you easy and secure bookings for all our guests.</br> <b>HOTELS IN T NAGAR USMAN ROAD:</b> When searching for hotels in Usman Road T Nagar, Chennai, consider its location, room rates, amenities, and services into account before choosing your hotel. Ulo Hotels provides a pocket-friendly stays that are ideal for all types of vacationers from budget travelers to luxury vacationers. If you are looking for stays in key locations, book our hotels in T Nagar Usman Road. If you want rooms near CMBT, you can choose our hotels in Koyambedu. If you are looking for budget rooms, there are many good hotels in T Nagar, which you can book at best prices. We also provide early check-in and check out for all guests which are strictly subject to hotel room availability and are chargeable.</br> <b>HOTELS IN PONDY BAZAAR:</b> If you are looking for stays near the second biggest shopping hub in Chennai, book hotels in T Nagar Pondy Bazaar. If you are on a budget cut, there are many decent hotels near Pondy Bazaar, which you can book with best offers from Ulo Hotels. If you are staying at the hotel as a couple, book our <a href=https://www.ulohotels.com/chennai-hotels-and-resorts>hotels in Chennai</a> at a special price. If you want to stay near the Giant automobile companies in Chennai, book our <a href=https://www.ulohotels.com/hotels-in-omr-chennai> hotels in OMR</a>. If you are searching for accommodations near the IT Corridor, choose <a href=https://www.ulohotels.com/ulo-hotel-hoilday-stay>hotels in Sholinganallur</a>. If you are looking for rooms in the heart of Chennai city, you can opt for hotels in <a href=https://www.ulohotels.com/ulo-panasia-residency>Vadapalani</a>.";
			  
			request.setAttribute("metaTag" ,title);
		    request.setAttribute("description" ,description);
			
			}    
		
		if(request.getParameter("areaId") != null && request.getParameter("areaId").toString() !="") 
			this.setLocationId(Integer.parseInt(request.getParameter("areaId").toString()));
		else
			this.setAreaId(getAreaId());
	
			return SUCCESS;
	}
	
	
	public String execute() {
		
		PmsPropertyManager propertyController = new PmsPropertyManager();
		listPmsProperty = propertyController.listPropertyByUser(getUser().getUserId());
		
		if(listPmsProperty.size() == 0){
		if(sessionMap!=null) sessionMap.invalidate();
		if(session!=null) session.invalidate();
		HttpServletResponse response = ServletActionContext.getResponse();
		response.setHeader("Pragma","no-cache");
		response.setHeader("Cache-Control","no-store");
		response.setHeader("Cache-Control","no-cache ");
		response.setHeader("Expires","0");
		response.setDateHeader("Expires",0);
		return "error";
		}
		else{
			
		}
		

		return SUCCESS;
	}
	
	

	public List<PmsProperty> getListPmsProperty() {
		return listPmsProperty;
	}

	public void setListPmsProperty(List<PmsProperty> listPmsProperty) {
		this.listPmsProperty = listPmsProperty;
	}
	

}
