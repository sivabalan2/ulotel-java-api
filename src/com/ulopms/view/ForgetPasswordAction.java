package com.ulopms.view;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.sql.Blob;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;  

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.catalina.startup.UserConfig;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.hibernate.Hibernate;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.ulopms.controller.ForgetPasswordManager;
import com.ulopms.controller.UserLoginManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.ForgetPassword;
import com.ulopms.model.User;
import com.ulopms.security.passwordDecrypt;
import com.ulopms.util.Email;
import com.ulopms.util.Image;

import freemarker.template.Configuration;
import freemarker.template.Template;


public class ForgetPasswordAction  extends ActionSupport implements ServletRequestAware {

	//private static final long serialVersionUID = 9149826260758390091L;
	private String oper;
	private User user = new User();
	private List<User> userList;
	private Integer userId;
	private String userName;
	private String loginName;
	private String password;
	private String emailId;
	private String phone;
	private String oldPassword;
	private String newPassword;
	private String confirmPassword;
	private String hashPassword;
	private String saltKey;
	

	private static final Logger logger = Logger.getLogger(ForgetPasswordAction.class);

	private HttpServletRequest request  ;

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	private UserLoginManager userController;
	
	private UserLoginManager linkController;
	

	private ForgetPasswordManager passwordController =  new ForgetPasswordManager();

	public ForgetPasswordAction() {
		userController = new UserLoginManager();
	}

	public String execute() throws Exception {

		return SUCCESS;
	}

	public String verifyURL() throws Exception
	{
		String ret="error";
		
		try
		{
			if(request.getParameter("link")!=null && request.getParameter("link")!="")
			{

				List<ForgetPassword> listFp =  passwordController.listByPasswordLink(request.getParameter("link"));
				//Iterator<ForgetPassword> fp1 = listFp.iterator();
				if(listFp.size()==1)
				{
					ret = "success";
				}

			}
			else
			{
				addActionError(getText("forgetpassword.invalidurl"));
				ret= "ERROR";
			}
		}
		catch(Exception e)
		{
			logger.error(e);
		}
		finally
		{

		}
		return ret;
	}

	public  String  changeUserpassword() throws Exception {
		 
		UserLoginManager  userController = new UserLoginManager();
		try{
		User u = userController.findUser(getUserId());
		String PUBLIC_SALT = passwordDecrypt.secrandom();
		String newhashpassword=passwordDecrypt.getDigestvalid(getNewPassword(),PUBLIC_SALT);
		u.setHashpassword(newhashpassword);
		u.setSaltkey(PUBLIC_SALT);
		//userController.edit(u);
		//fp.setIsActive(false);
		//u.setForgetPassword(fp);
		//User u1 = u;
		
		userController.edit(u);
		addActionMessage(getText("forgetpassword.changesuccess"));
		}
		catch(Exception e)
		{
			logger.error(e);
		}
		finally
		{

		}
		return SUCCESS;
	}
	
	public String changePassword() throws Exception
    {
	     
        String ret= ERROR;
      
        
        try
        {
            if(request.getParameter("link")!=null && request.getParameter("link")!="")
            {
            	
            	
                List<ForgetPassword> listFp =  passwordController.listByPasswordLink(request.getParameter("link"));
                //Iterator<ForgetPassword> fp1 = listFp.iterator();
               
                if(listFp.size()==1)
                {
                    for(ForgetPassword fp:listFp)
                    {
                    	
                    	
                        ForgetPassword fp1 =passwordController.find(fp.getForgetPasswordId());
                        User u = userController.findUser(fp1.getUser().getUserId());
                        String PUBLIC_SALT = passwordDecrypt.secrandom();
                        String newhashpassword=passwordDecrypt.getDigestvalid(getPassword(),PUBLIC_SALT);
                        u.setHashpassword(newhashpassword);
                        u.setSaltkey(PUBLIC_SALT);
                        //userController.edit(u);
                        fp1.setIsActive(false);
                        //u.setForgetPassword(fp);
                        //User u1 = u;
                        passwordController.edit(fp1);
                        userController.edit(u);
                        addActionMessage(getText("forgetpassword.changesuccess"));
                        ret = SUCCESS;
                    }
                }
                else
                {
                    addActionError(getText("forgetpassword.invalidurl"));
                    ret = ERROR;
                }
            }
            else
            {
                addActionError(getText("forgetpassword.invalidurl"));
                ret = ERROR;
            }
        }
        catch(Exception e)
        {
            logger.error(e);
            e.printStackTrace();
        }
        finally
        {

        }

        //verify 
        return ret;
    }
	
	public String verifyEmail() throws Exception
	{
		try
		{
			List<User> u = userController.findUserByEmail(getEmailId());
			if(u.size()==1)
			{
				for(User u1:u)
				{

					List<ForgetPassword> all = passwordController.listByUser(u1.getUserId());
					for(ForgetPassword fpa:all)
					{
						fpa.setIsActive(false);
						passwordController.edit(fpa);
					}

					long time = System.currentTimeMillis();
					java.sql.Date date = new java.sql.Date(time);
					java.sql.Timestamp createdstamp =  new java.sql.Timestamp(time);
					java.sql.Timestamp expirystamp =  new java.sql.Timestamp(time+3600000);

					ForgetPassword fp = new ForgetPassword();
					fp.setUser(u1);
					fp.setCreatedOn(createdstamp);
					fp.setExpiryOn(expirystamp);
					UUID uniqueKey = UUID.randomUUID();   
					fp.setPasswordLink(uniqueKey.toString());
					fp.setIsActive(true);
					//List<ForgetPassword> set1 = new ArrayList<ForgetPassword>();
					//set1.add(fp);
					//u1.setForgetPasswords(set1);
					//userController.edit(u1);
					
					passwordController.add(fp);

					//email template
					Configuration cfg = new Configuration();
					cfg.setClassForTemplateLoading(ForgetPasswordAction.class, "../../../");
					Template template = cfg.getTemplate(getText("forgetpassword.template"));
					Map<String, String> rootMap = new HashMap<String, String>();
					rootMap.put("to", u1.getUserName());
					rootMap.put("passworkLink", getText("url.forgetpassword")+uniqueKey.toString());
					rootMap.put("from", getText("notification.from"));
					Writer out = new StringWriter();
					template.process(rootMap, out);

					//send email
					Email em = new Email();
					em.set_to(u1.getEmailId());
					em.set_from(getText("email.from"));
					//em.set_cc(getText("reservation.notification.email"));
					//em.set_host(getText("email.host"));
					//em.set_password(getText("email.password"));
					//em.set_port(getText("email.port"));
					em.set_username(getText("email.username"));
					em.set_subject(getText("forgetpassword.subject"));
					em.set_bodyContent(out);
					em.send();		

					addActionMessage(getText("forgetpassword.success"));
				}
			}
			else
			{
				addActionError(getText("forgetpassword.invalidemail"));
			}
		}
		catch(Exception e)
		{
			logger.error(e);
			e.printStackTrace();
		}
		finally
		{

		}
		return SUCCESS;
	}
	
	
	public String verifyCustomerEmail() throws Exception
	{
		try
		{
			List<User> u = userController.findUserByEmail(getEmailId());
			if(u.size()==1)
			{
				for(User u1:u)
				{

					List<ForgetPassword> all = passwordController.listByUser(u1.getUserId());
					for(ForgetPassword fpa:all)
					{
						fpa.setIsActive(false);
						passwordController.edit(fpa);
					}

					long time = System.currentTimeMillis();
					java.sql.Date date = new java.sql.Date(time);
					java.sql.Timestamp createdstamp =  new java.sql.Timestamp(time);
					java.sql.Timestamp expirystamp =  new java.sql.Timestamp(time+3600000);

					ForgetPassword fp = new ForgetPassword();
					fp.setUser(u1);
					fp.setCreatedOn(createdstamp);
					fp.setExpiryOn(expirystamp);
					UUID uniqueKey = UUID.randomUUID();   
					fp.setPasswordLink(uniqueKey.toString());
					fp.setIsActive(true);
					//List<ForgetPassword> set1 = new ArrayList<ForgetPassword>();
					//set1.add(fp);
					//u1.setForgetPasswords(set1);
					//userController.edit(u1);
					
					passwordController.add(fp);

					//email template
					Configuration cfg = new Configuration();
					cfg.setClassForTemplateLoading(ForgetPasswordAction.class, "../../../");
					Template template = cfg.getTemplate(getText("uloforgetpassword.template"));
					Map<String, String> rootMap = new HashMap<String, String>();
					rootMap.put("to", u1.getUserName());
					rootMap.put("passworkLink", getText("url.uloforgetpassword")+uniqueKey.toString());
					rootMap.put("from", getText("notification.from"));
					Writer out = new StringWriter();
					template.process(rootMap, out);

					//send email
					Email em = new Email();
					em.set_to(u1.getEmailId());
					em.set_from(getText("email.from"));
					//em.set_host(getText("email.host"));
					//em.set_password(getText("email.password"));
					//em.set_port(getText("email.port"));
					em.set_username(getText("email.username"));
					em.set_subject(getText("forgetpassword.subject"));
					em.set_bodyContent(out);
					em.send();		

					addActionMessage(getText("forgetpassword.success"));
				}
			}
			else
			{
				addActionError(getText("forgetpassword.invalidemail"));
			}
		}
		catch(Exception e)
		{
			logger.error(e);
			e.printStackTrace();
		}
		finally
		{

		}
		return SUCCESS;
	}
	
	public String sendPassword() {
		
		String ret = "error";
		UserLoginManager usrlgn = new UserLoginManager();
		try {

		//HttpServletResponse response = ServletActionContext.getResponse();
		//response.reset();
		//response.flushBuffer();
		//response.setContentType("application/json");

		List<User> searchUser = userController.findUserByEmail(getEmailId());
		
		if(searchUser.size()>0)
		{
		User u = new User();
		HttpServletRequest request = ServletActionContext.getRequest();
		long time = System.currentTimeMillis();
		java.sql.Date date = new java.sql.Date(time);
		
		RandomStringUtils rsu = new RandomStringUtils();
		String PUBLIC_SALT = passwordDecrypt.secrandom();
		String tpassword = rsu.random(8,true,true).toString();
		String hashpassword=passwordDecrypt.getDigestvalid(tpassword,PUBLIC_SALT);
		String tloginName = rsu.random(10,true,true).toString();
        
	  
		for(User su:searchUser)
		{   
			this.userName = su.getUserName();
			su.setHashpassword(hashpassword);
			su.setHashpassword(hashpassword);
			su.setSaltkey(PUBLIC_SALT);
			su.setIsActive(true);
			su.setIsDeleted(false);
			usrlgn.edit(su);
		}

		


		Configuration cfg = new Configuration();
		cfg.setClassForTemplateLoading(UserAddAction.class, "../../../");
		Template template = cfg.getTemplate(getText("uloforgetpassword.template"));
		Map<String, String> rootMap = new HashMap<String, String>();
		rootMap.put("password", tpassword);
		
		//rootMap.put("", getText("url.forgetpassword")+uniqueKey.toString());
		rootMap.put("to", this.userName);
		//rootMap.put("createdBy", getUser().getUserName());
		//rootMap.put("logoFileName", c1.getLogoPath());
		rootMap.put("url", getText("app.api.url"));
		rootMap.put("from", getText("notification.from"));
		Writer out = new StringWriter();
		template.process(rootMap, out);

		//send email
		Email em = new Email();
		em.set_to(getEmailId());
		em.set_from(getText("email.from"));
		//em.set_host(getText("email.host"));
		//em.set_password(getText("email.password"));
		//em.set_port(getText("email.port"));
		em.set_username(getText("email.username"));
		em.set_subject(getText("forgetpassword.subject"));
		em.set_bodyContent(out);
		em.send();

		
		ret= null; 
		
		//end notification
		//response.getWriter().write("{\"data\":[{" + "\"result\":\"Created Successfully\"" + "}]}");

		//response.setStatus(200);
		}
		
		else
		{
			
			ret= "error";
			
	
		}

		} catch (Exception e) {
			
			
		
		logger.error(e);
		
		e.printStackTrace();
		
		} 
		
		
		return ret;
		
		
		}
	

	public List<User> getUserList() {
		return userList;
	}

	public void setUserList(List<User> usersList) {
		this.userList = usersList;
	}

	public void setPassword(String password)
	{
		this.password = password;
	}
	public String getPassword()
	{
		return password;
	}

	public void setEmailId(String emailId)
	{
		this.emailId = emailId;
	}
	public String getEmailId()
	{
		return emailId;
	}
	
	public void setUserId(Integer userId)
	{
		this.userId = userId;
	}
	public Integer getUserId()
	{
		return userId;
	}
	public void setNewPassword(String newPassword)
	{
		this.newPassword = newPassword;
	}
	public String getNewPassword()
	{
		return newPassword;
	}
	public void setConfirmPassword(String confirmPassword)
	{
		this.confirmPassword = confirmPassword;
	}
	public String getConfirmPassword()
	{
		return confirmPassword;
	}
	public void setHashPassword(String hashPassword) {
		this.hashPassword = hashPassword;
	}

	public String getHashPassword() {
		return hashPassword;
	}
	
	public void setSaltKey(String saltKey) {
		this.saltKey = saltKey;
	}

	public String getSaltKey() {
		return saltKey;
	}



}
