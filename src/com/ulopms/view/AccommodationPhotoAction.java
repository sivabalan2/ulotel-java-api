package com.ulopms.view;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;




//import com.medwecare.controller.AwsObjectManager;
//import com.medwecare.controller.CommentManager;
//import com.medwecare.controller.CommentUserManager;
////import com.medwecare.controller.NotificationManager;
//import com.medwecare.controller.ProjectManager;

//import com.medwecare.model.AwsObject;
//import com.medwecare.model.Notification;
//import com.medwecare.model.ObjectComment;
//import com.medwecare.model.ObjectCommentUser;
//import com.medwecare.model.Project;


import com.opensymphony.xwork2.ActionSupport;

import freemarker.template.Configuration;
import freemarker.template.Template;

import javax.servlet.http.HttpServletRequest;

import com.opensymphony.xwork2.ModelDriven;
import com.ulopms.controller.AccommodationPhotoManager;

import com.ulopms.controller.UserLoginManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.AccommodationPhoto;
import com.ulopms.model.User;
import com.ulopms.util.Email;
import com.ulopms.util.Image;

public class AccommodationPhotoAction extends ActionSupport implements
ServletRequestAware, UserAware{

	private static final long serialVersionUID = 914982665758390091L;

	private Integer accommodationPhotoId;
	

	private Boolean isPrimary;

	private File myFile;
	private String myFileContentType;
	private String myFileFileName;
	
	private AccommodationPhoto accommodationPhoto;
	
	private List<AccommodationPhoto> accommodationPhotoList;
	

	private static final Logger logger = Logger.getLogger(AccommodationPhotoAction.class);

	
	private User user;

	
	// @Override
	// public User getModel() {
	// return user;
	// }

	@Override
	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	
	private HttpServletRequest request;

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	public AccommodationPhotoAction() {

	}

	public String execute() {

		//this.patientId = getPatientId();
		return SUCCESS;
	}

	
	public String getAccommodationPhotos() throws IOException {
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			AccommodationPhotoManager photoController = new AccommodationPhotoManager();
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");

			AccommodationPhoto photo = photoController.find(getAccommodationPhotoId());
			
				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"DT_RowId\":\"" + photo.getAccommodationPhotoId() + "\"";
				jsonOutput += ",\"isPrimary\":\"" + photo.getIsPrimary()+ "\"";
				jsonOutput += ",\"photoPath\":\"" + photo.getPhotoPath()+ "\"";
				
				
				jsonOutput += "}";

			

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	public String getAllCustomer() throws IOException {
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			AccommodationPhotoManager photoController = new AccommodationPhotoManager();
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");

			this.accommodationPhotoList = photoController.list();
			for (AccommodationPhoto photo : accommodationPhotoList) {

				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"DT_RowId\":\"" + photo.getAccommodationPhotoId() + "\"";
				jsonOutput += ",\"isPrimary\":\"" + photo.getIsPrimary()+ "\"";
				jsonOutput += ",\"photoPath\":\"" + photo.getPhotoPath()+ "\"";
				
				
				jsonOutput += "}";


			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}

	public String addAccommodationPhoto() {
		AccommodationPhotoManager photoController = new AccommodationPhotoManager();
		//UserLoginManager  userController = new UserLoginManager();
		try {
			
						
			AccommodationPhoto photo = new AccommodationPhoto();
			
			
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			
			
			
			//customer.setIcecontactno(getIcecontactno());
			if(this.getMyFileFileName()!=null && !this.getMyFileFileName().equalsIgnoreCase(""))
				photo.setPhotoPath(this.getMyFileFileName());
				photo.setIsActive(true);
				photo.setIsDeleted(false);
			
			AccommodationPhoto photo1 = photoController.add(photo);
			
			if(photo1.getAccommodationPhotoId()>0 && this.getMyFileFileName()!=null && !this.getMyFileFileName().equalsIgnoreCase(""))
			{
				File file = new File(getText("storage.user.logopath") + "/"
					+ photo1.getAccommodationPhotoId() + "/", this.getMyFileFileName());
				FileUtils.copyFile(this.myFile, file);// copying image in the
			}
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return SUCCESS;
	}
	
	public String editAccommodationPhoto() {
		AccommodationPhotoManager photoController = new AccommodationPhotoManager();
		UserLoginManager  userController = new UserLoginManager();
		try {
			
						
			AccommodationPhoto photo = photoController.find(getAccommodationPhotoId());
			
			
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			
			//customer.setIcecontactno(getIcecontactno());
			if(this.getMyFileFileName()!=null && !this.getMyFileFileName().equalsIgnoreCase(""))
				photo.setPhotoPath(this.getMyFileFileName());
				photo.setIsActive(true);
				photo.setIsDeleted(false);
			
			AccommodationPhoto photo1 = photoController.add(photo);
			
			if(photo1.getAccommodationPhotoId()>0 && this.getMyFileFileName()!=null && !this.getMyFileFileName().equalsIgnoreCase(""))
			{
				File file = new File(getText("storage.user.logopath") + "/"
					+ photo1.getAccommodationPhotoId() + "/", this.getMyFileFileName());
				FileUtils.copyFile(this.myFile, file);// copying image in the
			}
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return SUCCESS;
	}
	
	public String deleteAccommodationPhoto() {
		AccommodationPhotoManager photoController = new AccommodationPhotoManager();
		UserLoginManager  userController = new UserLoginManager();
		try {
			
						
			AccommodationPhoto photo = photoController.find(getAccommodationPhotoId());
						
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			photo.setIsActive(false);
			photo.setIsDeleted(true);
			photo.setModifiedDate(new java.sql.Timestamp(date.getTime()));
			photoController.edit(photo);
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}
	public String getAccommodationPicture() {
		HttpServletResponse response = ServletActionContext.getResponse();

		AccommodationPhotoManager photoController = new AccommodationPhotoManager();
		AccommodationPhoto photo = photoController.find(getAccommodationPhotoId());
		String imagePath = "";
		// response.setContentType("");
		try {
			
			if (getAccommodationPhotoId() >0 ) {
				imagePath = getText("storage.user.logopath") + "/"
						+ getAccommodationPhotoId() + "/"
						+ photo.getPhotoPath();
				Image im = new Image();
				response.getOutputStream().write(
						im.getCustomImageInBytes(imagePath));
				response.getOutputStream().flush();
			} else {
				imagePath = getText("storage.path") + "/emptyprofilepic.png";
				Image im = new Image();
				response.getOutputStream().write(
						im.getCustomImageInBytes(imagePath));
				response.getOutputStream().flush();
			}
		} catch (Exception e1) {
			logger.error(e1);
		} finally {

		}
		return null;

	}
	public String dconverttime(java.util.Date input)
	{
		String disdatetime="";
		try { 
			DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		DateFormat outputFormat1 = new SimpleDateFormat("KK:mm a dd-MMM-yyyy");
		disdatetime=outputFormat1.format(input);
		}catch(Exception e)
        {
                        e.printStackTrace();
        }
		return disdatetime.replace("-", " ");
	}

	public  String  dconvert(java.util.Date dformat){
		 String disdate="";
	    try { 
	    SimpleDateFormat  dateFormat = new SimpleDateFormat("dd-MMM-YYYY");
	               
	               
	                 disdate=dateFormat.format(dformat).replace("-"," ");
	                
	                }catch(Exception e)
	                {
	                                e.printStackTrace();
	                }
	    return disdate;
	}

	public  String datediffer(java.util.Date dformat){	
		
		String disstatus="";
	    try { 
	    SimpleDateFormat  dateFormat = new SimpleDateFormat("yyyy/MM/dd");
	               
	                java.util.Date date = new java.util.Date();
	                String date1=dateFormat.format(dformat);
	                String date2=dateFormat.format(date);
	                java.util.Date d1=dateFormat.parse(date1);
	                java.util.Date d2=dateFormat.parse(date2);
	                
	                long diff=d2.getTime() - d1.getTime();
	                if(diff>0)
	                {
	                disstatus="old";
	                }
	                else
	                {
	                  disstatus="New";
	                }
	                
	                }catch(Exception e)
	                {
	                                e.printStackTrace();
	                }
	    
	    
	    return disstatus;
	}
	
	

	public File getMyFile() {
		return myFile;
	}

	public void setMyFile(File myFile) {
		this.myFile = myFile;
	}

	public String getMyFileContentType() {
		return myFileContentType;
	}

	public void setMyFileContentType(String myFileContentType) {
		this.myFileContentType = myFileContentType;
	}

	public String getMyFileFileName() {
		return myFileFileName;
	}

	public void setMyFileFileName(String myFileFileName) {
		this.myFileFileName = myFileFileName;
	}
	
	public Integer getAccommodationPhotoId() {
		return accommodationPhotoId;
	}

	public void setAccommodationPhotoId(Integer accommodationPhotoId) {
		this.accommodationPhotoId = accommodationPhotoId;
	}

	public Boolean getIsPrimary() {
		return isPrimary;
	}

	public void setIsPrimary(Boolean isPrimary) {
		this.isPrimary = isPrimary;
	}

	public AccommodationPhoto getAccommodationPhoto() {
		return accommodationPhoto;
	}

	public void setAccommodationPhoto(AccommodationPhoto accommodationPhoto) {
		this.accommodationPhoto = accommodationPhoto;
	}

	public List<AccommodationPhoto> getAccommodationPhotoList() {
		return accommodationPhotoList;
	}

	public void setAccommodationPhotoList(
			List<AccommodationPhoto> accommodationPhotoList) {
		this.accommodationPhotoList = accommodationPhotoList;
	}



}
