package com.ulopms.view;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;














import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;




//import com.medwecare.controller.AwsObjectManager;
//import com.medwecare.controller.CommentManager;
//import com.medwecare.controller.CommentUserManager;
////import com.medwecare.controller.NotificationManager;
//import com.medwecare.controller.ProjectManager;

//import com.medwecare.model.AwsObject;
//import com.medwecare.model.Notification;
//import com.medwecare.model.ObjectComment;
//import com.medwecare.model.ObjectCommentUser;
//import com.medwecare.model.Project;
























import com.opensymphony.xwork2.ActionSupport;

import freemarker.template.Configuration;
import freemarker.template.Template;

import javax.servlet.http.HttpServletRequest;

import com.opensymphony.xwork2.ModelDriven;
import com.ulopms.controller.AccommodationAmenityManager;
import com.ulopms.controller.PmsAmenityManager;
import com.ulopms.controller.PmsPropertyManager;
import com.ulopms.controller.PmsTagManager;
import com.ulopms.controller.PropertyAccommodationManager;
import com.ulopms.controller.PropertyAmenityManager;
import com.ulopms.controller.PropertyItemCategoryManager;
import com.ulopms.controller.PropertyUserManager;
import com.ulopms.controller.RoleManager;
import com.ulopms.controller.UserLoginManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.AccommodationAmenity;
import com.ulopms.model.PmsAmenity;
import com.ulopms.model.PmsProperty;
import com.ulopms.model.PmsTags;
import com.ulopms.model.PropertyAccommodation;
import com.ulopms.model.PropertyAmenity;
import com.ulopms.model.PropertyItem;
import com.ulopms.model.PropertyItemCategory;
import com.ulopms.model.PropertyUser;
import com.ulopms.model.User;
import com.ulopms.util.Email;
import com.ulopms.util.Image;

public class AmenityAction extends ActionSupport implements
ServletRequestAware,SessionAware, UserAware{

	private static final long serialVersionUID = 914982665758390091L;

	private Integer amenityId;
	
	private Integer propertyId;
	
	private String checkedAmenity;
	
	private String checkedAmenity1;
	
	private Integer accommodationId;

	private String amenityDescription;
	private String amenityName;
	
	private PmsAmenity amenity;
	
	private List<PmsAmenity> amenityList;
	
	private List<PropertyAmenity> propertyAmenityList;
	
	private List<AccommodationAmenity> accommodationAmenityList;
  
	private Integer tagId;
	
	private static final Logger logger = Logger.getLogger(AmenityAction.class);

	
	

	private HttpSession session;

	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}
	
	private SessionMap<String,Object> sessionMap;  
	  
	//getters and setters  
	  
	@Override  
	public void setSession(Map<String, Object> map) {  
	    sessionMap=(SessionMap)map;  
	}  
	// @Override
	// public User getModel() {
	// return user;
	// }
	private User user;
	@Override
	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	
	private HttpServletRequest request;

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	public AmenityAction() {
		
	//this.propertyId = (Integer) sessionMap.get("propertyId");
		
		
	}

	public String execute() {
        
		
		//this.patientId = getPatientId();
		return SUCCESS;
	}
    
	//this.propertyId = (Integer) sessionMap.get("propertyId");
	
	public String getSingleAmenity() throws IOException {
		
		try {
			this.propertyId = (Integer) sessionMap.get("propertyId");
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PmsAmenityManager amenityController = new PmsAmenityManager();
			
			response.setContentType("application/json");

			PmsAmenity amenity = amenityController.find(getAmenityId());
			
				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"DT_RowId\":\"" + amenity.getAmenityId() + "\"";
				jsonOutput += ",\"amenityName\":\"" + amenity.getAmenityName()+ "\"";
				jsonOutput += ",\"amenityDescription\":\"" + amenity.getAmenityDescription()+ "\"";
				
				jsonOutput += "}";

			

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	public String getAmenityPic() {
		
		HttpServletResponse response = ServletActionContext.getResponse();
		PmsAmenityManager amenityController = new PmsAmenityManager();

		PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();

		String imagePath = "";
		// response.setContentType("");
		try {
			PmsAmenity pmsam = amenityController.find(getAmenityId()); 
			if ( pmsam.getAmenityIcon() != null) {
				imagePath = getText("storage.amenityicon.photo") + "/"
						+pmsam.getAmenityId() + "/"
						+ pmsam.getAmenityIcon();
				Image im = new Image();
				response.getOutputStream().write(
						im.getCustomImageInBytes(imagePath));
				response.getOutputStream().flush();
			} else {
				imagePath = getText("storage.amenityicon.photo") + "/emptyprofilepic.png";
				Image im = new Image();
				response.getOutputStream().write(
						im.getCustomImageInBytes(imagePath));
				response.getOutputStream().flush();
			}
		}catch (Exception e1) {
			logger.error(e1);
		} finally {

		}
		return null;

	}
	
	public String getAmenities() throws IOException {
		
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PmsAmenityManager amenityController = new PmsAmenityManager();
			
			response.setContentType("application/json");

			this.amenityList = amenityController.list();
			
			for (PmsAmenity amenity : amenityList) {

				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"DT_RowId\":\"" + amenity.getAmenityId() + "\"";
				jsonOutput += ",\"amenityName\":\"" + amenity.getAmenityName()+ "\"";
				jsonOutput += ",\"amenityDescription\":\"" + amenity.getAmenityDescription()+ "\"";

				jsonOutput += "}";

			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	public String getAllAmenitySize() throws IOException {
		try {
			this.propertyId = (Integer) sessionMap.get("propertyId");
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			PmsAmenityManager amenityController = new PmsAmenityManager();
			PropertyAmenityManager propertyAmenityConroller = new PropertyAmenityManager();
			response.setContentType("application/json");
			this.amenityList = amenityController.listProperty();
		    List<PropertyAmenity> propertyAmenityList = propertyAmenityConroller.list(getPropertyId());
		    if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				
				
				jsonOutput += "\"propertySize\":\"" + propertyAmenityList.size() + "\"";
			
				
			
			String jsonAccommodation ="";
			PropertyAccommodationManager accommodationManager = new PropertyAccommodationManager();
			AccommodationAmenityManager accAmenityController = new AccommodationAmenityManager();
			this.amenityList = amenityController.listAccommodation();
			List<PropertyAccommodation> accommodationsList =  accommodationManager.list(this.propertyId);
			for (PropertyAccommodation accommodation : accommodationsList) {
				
				this.accommodationAmenityList = accAmenityController.list(accommodation.getAccommodationId());
				
				if (!jsonAccommodation.equalsIgnoreCase(""))
					
					jsonAccommodation += ",{";
				else
					jsonAccommodation += "{";
				
					jsonAccommodation += "\"accommodationName\":\"" + accommodation.getAccommodationType() + "\"";
					jsonAccommodation += ",\"accommodationSize\":\"" + accommodationAmenityList.size() + "\"";
					jsonAccommodation += "}";	
			}
					
					
			
			jsonOutput += ",\"accommodation\":[" + jsonAccommodation+ "]";
			
			String jsonTag ="";
			PmsTagManager tagController = new PmsTagManager();
			List<PmsTags> tagList = tagController.list();
			for(PmsTags tags : tagList){
			
				List<PropertyAmenity> tagAmenityList =  propertyAmenityConroller.list(this.propertyId,tags.getTagId());
					
					
					if (!jsonTag.equalsIgnoreCase(""))
						
						jsonTag += ",{";
					else
						jsonTag += "{";
					
					jsonTag += "\"tagName\":\"" + tags.getTagName() + "\"";
					jsonTag += ",\"tagSize\":\"" + tagAmenityList.size() + "\"";
					jsonTag += "}";	
			}
			
					
					
			
			jsonOutput += ",\"propertyTags\":[" + jsonTag+ "]";
			jsonOutput += "}";	
			
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	public String getPropertyAmenities() throws IOException {
		try {
			this.propertyId = (Integer) sessionMap.get("propertyId");
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			PmsAmenityManager amenityController = new PmsAmenityManager();
			PropertyAmenityManager amenityConroller = new PropertyAmenityManager();
			response.setContentType("application/json");
			this.amenityList = amenityController.listProperty();
			for (PmsAmenity amenity : amenityList) {
		    List<PropertyAmenity> propertyAmenityList = amenityConroller.listTwo(getPropertyId(), amenity.getAmenityId());
		    if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				
				
				jsonOutput += "\"DT_RowId\":\"" + amenity.getAmenityId() + "\"";
				jsonOutput += ",\"amenityName\":\"" + amenity.getAmenityName()+ "\"";
               // jsonOutput += ",\"icon\":\"" + path + amenity.getAmenityIcon()+ "\"";
				jsonOutput += ",\"amenityDescription\":\"" + amenity.getAmenityDescription()+ "\"";
				//jsonOutput += ",\"propertyIsDeleted\":\"" + amenity.getgetIsDeleted()+ "\"";
				//jsonOutput += ",\"isDeleted\":\"" + amenity.getIsDeleted()+ "\"";
				if(propertyAmenityList.size()>0) jsonOutput += ",\"status\":"+true+""; else jsonOutput += ",\"status\":"+false+"";

				jsonOutput += "}";
			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	
public String getUloPropertyAmenities() throws IOException {
		try {
			String path = "ulowebsite/images/icons/";
			
			/*this.propertyId = (Integer) sessionMap.get("uloPropertyId");
			
			if(this.propertyId==null && this.propertyId.equals("")){
				this.propertyId=getPropertyId();
			}*/
			this.propertyId=getPropertyId();
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PmsAmenityManager amenityController = new PmsAmenityManager();
			PropertyAmenityManager pAmenityConroller = new PropertyAmenityManager();
			
			
			
			response.setContentType("application/json");

			this.amenityList = amenityController.list();
			
			for (PmsAmenity amenity : amenityList) {
            
		    List<PropertyAmenity> propertyAmenityList = pAmenityConroller.listTwo(getPropertyId(), amenity.getAmenityId());
		   
		 //  for(PropertyAmenity pAmenity :propertyAmenityList)
		   //{
			   
		   //}
		   
		    if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				
				
				jsonOutput += "\"DT_RowId\":\"" + amenity.getAmenityId() + "\"";
				jsonOutput += ",\"amenityName\":\"" + amenity.getAmenityName()+ "\"";
                jsonOutput += ",\"icon\":\"" + path + amenity.getAmenityIcon()+ "\"";
				jsonOutput += ",\"amenityDescription\":\"" + amenity.getAmenityDescription()+ "\"";
				//jsonOutput += ",\"propertyIsDeleted\":\"" + amenity.getgetIsDeleted()+ "\"";
				//jsonOutput += ",\"isDeleted\":\"" + amenity.getIsDeleted()+ "\"";
				if(propertyAmenityList.size()>0) jsonOutput += ",\"status\":\"true\""; else jsonOutput += ",\"status\":\"false\"";

				jsonOutput += "}";

			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	
    public String getAccommodationAmenities() throws IOException {
		
		
		
		//this.propertyId = (Integer) sessionMap.get("propertyId");
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			this.propertyId = (Integer) sessionMap.get("propertyId");
			PmsAmenityManager amenityController = new PmsAmenityManager();
			//PropertyAmenityManager pAmenityConroller = new PropertyAmenityManager();
			AccommodationAmenityManager accAmenityController = new AccommodationAmenityManager();

			
			response.setContentType("application/json");

			 
			//List<PropertyAmenity> propertyAmenityList = pAmenityConroller.listTwo(getPropertyId(), amenity.getAmenityId());
            
			this.amenityList = amenityController.listAccommodation();
			
			for (PmsAmenity amenity : amenityList) {
            
            
				
		    this.accommodationAmenityList = accAmenityController.listTwo(getAccommodationId(), amenity.getAmenityId());
		    //   List<PropertyAmenity> propertyAmenityList = pAmenityConroller.listTwo(getPropertyId(), amenity.getAmenityId());
		   
		    //  for(PropertyAmenity pAmenity :propertyAmenityList)
		   //{
			   
		   //}
		   
		    if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				
				
				jsonOutput += "\"DT_RowId\":\"" + amenity.getAmenityId() + "\"";
				jsonOutput += ",\"amenityName\":\"" + amenity.getAmenityName()+ "\"";
                jsonOutput += ",\"icon\":\"" + amenity.getAmenityIcon()+ "\"";
				jsonOutput += ",\"amenityDescription\":\"" + amenity.getAmenityDescription()+ "\"";
				//jsonOutput += ",\"propertyIsDeleted\":\"" + amenity.getgetIsDeleted()+ "\"";
				//jsonOutput += ",\"isDeleted\":\"" + amenity.getIsDeleted()+ "\"";
				if(accommodationAmenityList.size()>0) jsonOutput += ",\"status\":"+true+""; else jsonOutput += ",\"status\":"+false+"";

				jsonOutput += "}";

			}

			

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}

    public String getAccommodationAmenities1() throws IOException {
	
	//this.propertyId = (Integer) sessionMap.get("propertyId");
	try {

		String jsonOutput = "";
		HttpServletResponse response = ServletActionContext.getResponse();
	
		
		PmsAmenityManager amenityController = new PmsAmenityManager();
		PropertyAmenityManager pAmenityConroller = new PropertyAmenityManager();
		AccommodationAmenityManager accAmenityController = new AccommodationAmenityManager();

		
		
		response.setContentType("application/json");

		this.amenityList = amenityController.list();
		
		for (PmsAmenity amenity : amenityList) {
        
	    List<AccommodationAmenity> accommodationAmenityList = accAmenityController.list(getAccommodationId(), amenity.getAmenityId());
	   
	 //  for(PropertyAmenity pAmenity :propertyAmenityList)
	   //{
		   
	   //}
	   
	    if (!jsonOutput.equalsIgnoreCase(""))
				jsonOutput += ",{";
			else
				jsonOutput += "{";
			
			
			jsonOutput += "\"DT_RowId\":\"" + amenity.getAmenityId() + "\"";
			jsonOutput += ",\"amenityName\":\"" + amenity.getAmenityName()+ "\"";
			jsonOutput += ",\"amenityDescription\":\"" + amenity.getAmenityDescription()+ "\"";
			//jsonOutput += ",\"propertyIsDeleted\":\"" + amenity.getgetIsDeleted()+ "\"";
			//jsonOutput += ",\"isDeleted\":\"" + amenity.getIsDeleted()+ "\"";
			if(accommodationAmenityList.size()>0) jsonOutput += ",\"status\":\"true\""; else jsonOutput += ",\"status\":\"false\"";

			jsonOutput += "}";

		}

		response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
		

	} catch (Exception e) {
		logger.error(e);
		e.printStackTrace();
	} finally {

	}

	return null;

}



    
	public String savePropertyAmenities() {
		PmsAmenityManager pmsAmenityController = new PmsAmenityManager();
		PropertyAmenityManager propertyAmenityController = new PropertyAmenityManager();
		PmsPropertyManager propertyController = new PmsPropertyManager();
		//UserLoginManager  userController = new UserLoginManager();
		
		try {
			this.propertyId = (Integer) sessionMap.get("propertyId");
			this.propertyAmenityList = propertyAmenityController.list(getPropertyId());
			for (PropertyAmenity amen : propertyAmenityList) {
			    
				amen.setIsActive(false);
				amen.setIsDeleted(true);
				amen.setIsTagAmenity(false);
				propertyAmenityController.edit(amen);	
			}			
			PropertyAmenity propertyAmenity = new PropertyAmenity();
			//PmsAmenity pmsAmenity = new PmsAmenity();
			//List<String> items = Arrays.asList(getCheckedAmenity().split("\\s*,\\s*"));
			String[] stringArray = getCheckedAmenity().split("\\s*,\\s*");
			String strArrays=Arrays.toString(stringArray);
		    if(!strArrays.contains("NA")){
		    	int[] intArray = new int[stringArray.length];
			    for (int i = 0; i < stringArray.length; i++) {
			    	String numberAsString = stringArray[i];
			    	intArray[i] = Integer.parseInt(numberAsString);
			    }
			      
			      for (int number : intArray) {
			        
			    	long time = System.currentTimeMillis();
				    java.sql.Date date = new java.sql.Date(time);
				    propertyAmenity.setIsActive(true);
				    propertyAmenity.setIsDeleted(false);
				    propertyAmenity.setIsTagAmenity(false);
				    PmsProperty property = propertyController.find(getPropertyId());
				    propertyAmenity.setPmsProperty(property);
				    PmsAmenity pms = pmsAmenityController.find(number);
				    propertyAmenity.setPmsAmenity(pms);
				    propertyAmenityController.add(propertyAmenity);
				    //propertyAmenityController.add(propertyAmenity);  
				    //List<PropertyAmenity> propertyAmenityList = propertyAmenityController.listTwo(getPropertyId(), number);
				    //if(propertyAmenityList.size() >0 ){
				    //propertyAmenityController.edit(propertyAmenity);	
				    //}
				    //else{
				   
				   // }
				    
			      }	
		    }
		    
		      
			
			
		    /*for(int i = 0; i < items.size(); i++) {
		    propertyAmenity.setIsActive(true);
		    propertyAmenity.setIsDeleted(false);
		    PmsProperty property = propertyController.find(getPropertyId());
		    propertyAmenity.setPmsProperty(property);
		    PmsAmenity pms = pmsAmenityController.find(items.get(i));
		    propertyAmenity.setPmsAmenity(pms);
		    List<PropertyAmenity> propertyAmenityList = propertyAmenityController.list(getPropertyId(), items.get(i));
		    if(propertyAmenityList.size()>0){
		    propertyAmenityController.edit(propertyAmenity);	
		    }
		    else{
		    propertyAmenityController.add(propertyAmenity);
		    }
            }*/
		    
			//long time = System.currentTimeMillis();
			//java.sql.Date date = new java.sql.Date(time);
			
			//amenity.setAmenityName(getAmenityName());
			//amenity.setAmenityDescription(getAmenityDescription());
			//amenity.setIsActive(true);
			//amenity.setIsDeleted(false);
			
			//amenityController.add(amenity);
			
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return SUCCESS;
	}
	public String getTagAmenities(){

		try {

			this.propertyId = (Integer) sessionMap.get("propertyId");
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			PmsAmenityManager amenityController = new PmsAmenityManager();
			PropertyAmenityManager amenityConroller = new PropertyAmenityManager();
			response.setContentType("application/json");
			this.amenityList = amenityController.listTags();
			if(this.amenityList.size()>0 && !this.amenityList.isEmpty()){
				for (PmsAmenity amenity : amenityList) {
				    List<PropertyAmenity> propertyAmenityList = amenityConroller.list(getPropertyId(), amenity.getAmenityId(),getTagId());
				    if (!jsonOutput.equalsIgnoreCase(""))
							jsonOutput += ",{";
						else
							jsonOutput += "{";
						
						
						jsonOutput += "\"DT_RowId\":\"" + amenity.getAmenityId() + "\"";
						jsonOutput += ",\"amenityName\":\"" + amenity.getAmenityName()+ "\"";
						jsonOutput += ",\"amenityDescription\":\"" + amenity.getAmenityDescription()+ "\"";
						if(propertyAmenityList.size()>0) jsonOutput += ",\"status\":"+true+""; else jsonOutput += ",\"status\":"+false+"";

						jsonOutput += "}";
				}	
			}
			

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	
	}
	public String saveTagAmenities(){

		try {
			PmsAmenityManager pmsAmenityController = new PmsAmenityManager();
			PropertyAmenityManager propertyAmenityController = new PropertyAmenityManager();
			PmsPropertyManager propertyController = new PmsPropertyManager();
			PmsTagManager tagController=new PmsTagManager();
			
			this.propertyId = (Integer) sessionMap.get("propertyId");
			this.propertyAmenityList = propertyAmenityController.list(getPropertyId(),getTagId());
			for (PropertyAmenity amen : propertyAmenityList) {
			    
				amen.setIsActive(false);
				amen.setIsDeleted(true);
				amen.setIsTagAmenity(true);
				propertyAmenityController.edit(amen);	
			}			
			PropertyAmenity propertyAmenity = new PropertyAmenity();
			String[] stringArray = getCheckedAmenity().split("\\s*,\\s*");
			String strArrays=Arrays.toString(stringArray);
		    if(!strArrays.contains("NA")){
		    	int[] intArray = new int[stringArray.length];
			    for (int i = 0; i < stringArray.length; i++) {
			    	String numberAsString = stringArray[i];
			    	intArray[i] = Integer.parseInt(numberAsString);
			    }
			      
			      for (int number : intArray) {
			        
			    	long time = System.currentTimeMillis();
				    java.sql.Date date = new java.sql.Date(time);
				    propertyAmenity.setIsActive(true);
				    propertyAmenity.setIsDeleted(false);
				    propertyAmenity.setIsTagAmenity(true);
				    PmsProperty property = propertyController.find(getPropertyId());
				    propertyAmenity.setPmsProperty(property);
				    PmsAmenity pms = pmsAmenityController.find(number);
				    propertyAmenity.setPmsAmenity(pms);
				    PmsTags tags=tagController.find(getTagId());
				    propertyAmenity.setPmsTags(tags);
				    
				    propertyAmenityController.add(propertyAmenity);
	
				    
			      }	
		    }
		    
		      
			
			
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return SUCCESS;
	
	}
	public String saveAccommodationAmenities() {
		PmsAmenityManager pmsAmenityController = new PmsAmenityManager();
		PropertyAmenityManager propertyAmenityController = new PropertyAmenityManager();
		PropertyAccommodationManager propertyAccommodationController = new PropertyAccommodationManager();
		AccommodationAmenityManager accAmenityConroller = new AccommodationAmenityManager();
		PmsPropertyManager propertyController = new PmsPropertyManager();
		//UserLoginManager  userController = new UserLoginManager();
		
		try {
			this.propertyId = (Integer) sessionMap.get("propertyId");
			this.accommodationAmenityList = accAmenityConroller.list(getAccommodationId());
			for (AccommodationAmenity accamen : accommodationAmenityList) {
			    
				accamen.setIsActive(false);
				accamen.setIsDeleted(true);
				accAmenityConroller.edit(accamen);	
			}			
			AccommodationAmenity accommodationAmenity = new AccommodationAmenity();
			
			String[] stringArray = getCheckedAmenity1().split("\\s*,\\s*");
			
			String strArrays=Arrays.toString(stringArray);
		    if(!strArrays.contains("NA")){
		    	int[] intArray = new int[stringArray.length];
			    for (int i = 0; i < stringArray.length; i++) {
			     String numberAsString = stringArray[i];
			     intArray[i] = Integer.parseInt(numberAsString);
			    }
			      
			      for (int number : intArray) {
			        
			    	long time = System.currentTimeMillis();
				    java.sql.Date date = new java.sql.Date(time);
				    accommodationAmenity.setIsActive(true);
				    accommodationAmenity.setIsDeleted(false);
				    PropertyAccommodation property = propertyAccommodationController.find(getAccommodationId());
				    accommodationAmenity.setPropertyAccommodation(property);
				    PmsAmenity pms = pmsAmenityController.find(number);
				    accommodationAmenity.setPmsAmenity(pms);
				    accAmenityConroller.add(accommodationAmenity);
				   
			      }
		    }
		    
		  
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}
	
	

	public String addAmenity() {
		PmsAmenityManager amenityController = new PmsAmenityManager();
		//UserLoginManager  userController = new UserLoginManager();
		try {
			
						
			PmsAmenity amenity = new PmsAmenity();
			
			
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			amenity.setAmenityName(getAmenityName());
			amenity.setAmenityDescription(getAmenityDescription());
			amenity.setIsActive(true);
			amenity.setIsDeleted(false);
			
			amenityController.add(amenity);
			
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return SUCCESS;
	}
	
	public String editAmenity() {
		PmsAmenityManager amenityController = new PmsAmenityManager();
		UserLoginManager  userController = new UserLoginManager();
		try {
			
						
			PmsAmenity amenity = amenityController.find(getAmenityId());
			
			
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			
			amenity.setAmenityName(getAmenityName());
			amenity.setAmenityDescription(getAmenityDescription());
			amenity.setIsActive(true);
			amenity.setIsDeleted(false);
			
			amenityController.edit(amenity);
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return SUCCESS;
	}
	
	public String deleteAmenity() {
		PmsAmenityManager amenityController = new PmsAmenityManager();
		UserLoginManager  userController = new UserLoginManager();
		try {
			
						
			PmsAmenity amenity = amenityController.find(getAmenityId());
						
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			amenity.setIsActive(false);
			amenity.setIsDeleted(true);
			amenity.setModifiedDate(new java.sql.Timestamp(date.getTime()));
			amenityController.edit(amenity);
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}
	
	
	public Integer getAmenityId() {
		return amenityId;
	}

	public void setAmenityId(Integer amenityId) {
		this.amenityId = amenityId;
	}

	public Integer getAccommodationId() {
		return accommodationId;
	}
	
	public void setAccommodationId(Integer accommodationId) {
		this.accommodationId = accommodationId;
	}

	public String getAmenityDescription() {
		return amenityDescription;
	}

	public void setAmenityDescription(String amenityDescription) {
		this.amenityDescription = amenityDescription;
	}

	public String getAmenityName() {
		return amenityName;
	}

	public void setAmenityName(String amenityName) {
		this.amenityName = amenityName;
	}
	
	public String getCheckedAmenity() {
		return checkedAmenity;
	}

	public void setCheckedAmenity(String checkedAmenity) {
		this.checkedAmenity = checkedAmenity;
	}
	
	 public String getCheckedAmenity1() {
		return checkedAmenity1;
	}

	public void setCheckedAmenity1(String checkedAmenity1) {
		this.checkedAmenity1 = checkedAmenity1;
	}     

	public PmsAmenity getAmenity() {
		return amenity;
	}

	public void setAmenity(PmsAmenity amenity) {
		this.amenity = amenity;
	}
	
	public Integer getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(Integer propertyId) {
		this.propertyId = propertyId;
	}
	

	public List<PmsAmenity> getAmenityList() {
		return amenityList;
	}

	public void setAmenityList(List<PmsAmenity> amenityList) {
		this.amenityList = amenityList;
	}
	
	public List<PropertyAmenity> getPropertyAmenityList() {
		return propertyAmenityList;
	}

	public void setPropertyAmenityList(List<PropertyAmenity> propertyAmenityList) {
		this.propertyAmenityList = propertyAmenityList;
	}

	public Integer getTagId() {
		return tagId;
	}

	public void setTagId(Integer tagId) {
		this.tagId = tagId;
	}

	
}
