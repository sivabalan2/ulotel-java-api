package com.ulopms.view;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.catalina.startup.UserConfig;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;

import com.opensymphony.xwork2.ActionSupport;
import com.ulopms.controller.HelpTopicManager;
import com.ulopms.controller.UserLoginManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.HelpTopic;
import com.ulopms.model.User;
import com.ulopms.util.Email;



import java.util.List;
import org.json.JSONObject;
import com.razorpay.Payment;
import com.razorpay.Invoice;
import com.razorpay.RazorpayClient;
import com.razorpay.RazorpayException;



import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import javax.servlet.http.HttpServletRequest;

public class PaymentLinkAction extends ActionSupport implements ServletRequestAware,
UserAware {

	private static final long serialVersionUID = 9149826260758390091L;

	private User user;
	


	private String name;
	private String email;
	private String phone;
	private String subject;
	private String link;
	private String type;
	private int view_less;
	private double amount;
	private String currency;
	private String description ;
	private Integer expire_by;
	private String invoiceId;
	/*
	  "customer": {
	    "name": "Test",
	    "email": "test@test.com",
	    "contact": "9999999999"
	  },
	  "type": "link",
	  "view_less": 1,
	  "amount": 2000,
	  "currency": "INR",
	  "description": "Payment link for this purpose - xyz.",
	  "expire_by": 1493630556*/
	
		
	public String getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(String invoiceId) {
		this.invoiceId = invoiceId;
	}

	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}

	

	private HttpSession session;

	private static final Logger logger = Logger.getLogger(PaymentLinkAction.class);

	// @Override
	// public User getModel() {
	// return user;
	// }

	@Override
	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	private HttpServletRequest request;

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	public PaymentLinkAction() {

	}

	public String sendPaymentLink()
	{
		try
		{
			ActionSupport actionSupport = new ActionSupport();
			actionSupport.getText("paymentlink.template");
			
		
		this.subject="ulotel payment link";
	    //this.link = "hi ulo";
		//email template
		Configuration cfg = new Configuration();
		cfg.setClassForTemplateLoading(PaymentLinkAction.class, "../../../");
		Template template = cfg.getTemplate(getText("paymentlink.template"));
		Map<String, String> rootMap = new HashMap<String, String>();
		rootMap.put("name",getName());
		rootMap.put("email", getEmail());
		rootMap.put("subject", getSubject());
		rootMap.put("link", getLink());
		rootMap.put("from", getText("notification.from"));
		Writer out = new StringWriter();
		template.process(rootMap, out);

		//send email
		Email em = new Email();
		em.set_to(getEmail());
		em.set_from(getText("email.from"));
		//em.set_host(getText("email.host"));
		//em.set_password(getText("email.password"));
		//em.set_port(getText("email.port"));
		em.set_username(getText("email.username"));
		em.set_subject(getText("paymentlink.subject"));
		em.set_bodyContent(out);
		em.send();		
		}
		catch(Exception e1)
		{
			e1.printStackTrace();
		}
		addActionMessage("Your request has been submitted successfully");
		return SUCCESS;
	}
	
	
	public String createPaymentLink()
	{
		try
		{
			RazorpayClient razorpay = new RazorpayClient("rzp_live_vuYF9peaDXMoQF", "mEmSEzreAQqw8iH9YGHTTwBI");
			
				
			  JSONObject invoiceRequest = new JSONObject();
			  JSONObject customer = new JSONObject();
			  
			  HttpServletResponse response = ServletActionContext.getResponse();
			  response.setContentType("application/json");
			  
			/*  
			 *   
			  "customer": {
				    "name": "Test",
				    "email": "test@test.com",
				    "contact": "9999999999"
				  },
			 * "type": "link",
			  "view_less": 1,
			  "amount": 2000,
			  "currency": "INR",
			  "description": "Payment link for this purpose - xyz.",
			  "expire_by": 1493630556 */
			  
			    //supported option filters (from, to, count, skip)
			
				  
			  customer.put("name", getName());
			  customer.put("email", getEmail());
			  customer.put("contact", getPhone());
			  invoiceRequest.put("customer", customer);
			  invoiceRequest.put("type", getType());
			  invoiceRequest.put("view_less", getView_less());
			  invoiceRequest.put("amount", getAmount());
			  invoiceRequest.put("currency", getCurrency());
			  invoiceRequest.put("description", getDescription());
			  invoiceRequest.put("sms_notify", 1);
			  invoiceRequest.put("email_notify", 1);
			  invoiceRequest.put("expire_by", getExpire_by());
			  //1508061370
			  Invoice invoice = razorpay.Invoices.create(invoiceRequest);
			  
			  response.getWriter().write("{\"data\":[" + invoice + "]}");
			 
			  
			  
			
		}
		catch(Exception e1)
		{
			e1.printStackTrace();
		}
		//addActionMessage("Your request has been submitted successfully");
		finally{
			
		}
		
		return null;
	}
	
	public String getInvoiceStatus()
	{
		try
		{
			RazorpayClient razorpay = new RazorpayClient("rzp_live_vuYF9peaDXMoQF", "mEmSEzreAQqw8iH9YGHTTwBI");
			
				
			  JSONObject invoiceRequest = new JSONObject();
			  
			  
			  HttpServletResponse response = ServletActionContext.getResponse();
			  response.setContentType("application/json");
			  
			/*  
			 *   
			  "customer": {
				    "name": "Test",
				    "email": "test@test.com",
				    "contact": "9999999999"
				  },
			 * "type": "link",
			  "view_less": 1,
			  "amount": 2000,
			  "currency": "INR",
			  "description": "Payment link for this purpose - xyz.",
			  "expire_by": 1493630556 */
			  
			    //supported option filters (from, to, count, skip)
			
				  
			 
			  //1508061370
			  Invoice invoice = razorpay.Invoices.fetch(getInvoiceId());
			  
			  response.getWriter().write("{\"data\":[" + invoice + "]}");
			 
			  
			  
			
		}
		catch(Exception e1)
		{
			e1.printStackTrace();
		}
		//addActionMessage("Your request has been submitted successfully");
		finally{
			
		}
		
		return null;
	}



	public String execute() {
		
		return SUCCESS;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getView_less() {
		return view_less;
	}

	public void setView_less(int view_less) {
		this.view_less = view_less;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getExpire_by() {
		return expire_by;
	}

	public void setExpire_by(Integer expire_by) {
		this.expire_by = expire_by;
	}
}
