package com.ulopms.view;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.net.URL;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.ServletRequestAware;

//import com.medwecare.controller.AwsObjectManager;
//import com.medwecare.controller.CommentManager;
//import com.medwecare.controller.CommentUserManager;

//import com.medwecare.controller.NotificationManager;
//import com.medwecare.controller.ProjectManager;

//import com.medwecare.model.AwsObject;
//import com.medwecare.model.Notification;
//import com.medwecare.model.ObjectComment;
//import com.medwecare.model.ObjectCommentUser;
//import com.medwecare.model.Project;





















import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;

import freemarker.template.Configuration;
import freemarker.template.Template;

import javax.servlet.http.HttpServletRequest;

import com.opensymphony.xwork2.ModelDriven;
import com.ulopms.controller.PmsPhotoCategoryManager;
import com.ulopms.controller.PmsPropertyManager;
import com.ulopms.controller.PropertyAccommodationManager;
import com.ulopms.controller.PropertyPaymentDetailManager;
import com.ulopms.controller.PropertyPhotoManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.PmsAvailableRooms;
import com.ulopms.model.PmsBookedDetails;
import com.ulopms.model.PmsPhotoCategory;
import com.ulopms.model.PropertyAccommodation;
import com.ulopms.model.PropertyPaymentDetails;
import com.ulopms.model.PropertyPhoto;
import com.ulopms.model.User;
import com.ulopms.model.PmsProperty;
import com.ulopms.util.Email;

public class PropertyPaymentAction extends ActionSupport implements SessionAware,
ServletRequestAware, UserAware{

	private static final long serialVersionUID = 914982665758390091L;

	private static final Logger logger = Logger.getLogger(PropertyPaymentAction.class);

	
	private User user;
	private int propertyId;
	private int userId;
	private File myFile;
	private String myFileContentType;
	private String myFileFileName;
	private String paymentBookingId;
	private String startDate;
	private String endDate;
	private Double paymentAmount;
	private Timestamp fromDate;
	private Timestamp toDate;
	private Integer limit;
	private Integer offset;
	private String propertyEmail;
	private String resortEmail1;
	private String resortEmail2;
	private String resortEmail3;
	private String resortEmail4;
	@Override
	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}
	
	List<PropertyPaymentDetails> paymentList;

	List<PmsProperty> listPmsProperty;

	private HttpServletRequest request;

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}
	
	
	private SessionMap<String,Object> sessionMap;  
	  
	//getters and setters  
	  
	public void setSession(Map<String, Object> map) {  
	    sessionMap=(SessionMap)map;  
	}  
	
	private HttpSession session;
	
	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}

	public PropertyPaymentAction() {

	}
	
	
    public String addPropertyPayment() {
		
		try {
			this.propertyId = (Integer) sessionMap.get("propertyId");
			this.userId = (Integer) sessionMap.get("userId");
			PmsPropertyManager propertyController = new PmsPropertyManager();
			PropertyPaymentDetailManager paymentController = new PropertyPaymentDetailManager();
			PropertyPaymentDetails paymentDetail = new PropertyPaymentDetails();
			
			java.util.Date date=new java.util.Date();
			Calendar today=Calendar.getInstance();
			today.setTime(date);
			today.setTimeZone(TimeZone.getTimeZone("IST"));
			SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		    String strCurrentDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(today.getTime());
		    String strDisplayDate = new SimpleDateFormat("dd-MM-yyyy").format(today.getTime());
		    java.util.Date currentDate = format1.parse(strCurrentDate);
		    Timestamp currentTS = new Timestamp(currentDate.getTime());
		    PmsProperty property = propertyController.find(this.propertyId);
		    this.resortEmail1=property.getResortManagerEmail();
		    this.resortEmail2=property.getResortManagerEmail1();
		    this.resortEmail3=property.getResortManagerEmail2();
		    this.resortEmail4=property.getResortManagerEmail3();
		    
		    
		    
			String strSubject="Payment receipt upload on "+strDisplayDate;
			DateFormat format = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
			java.util.Date dateStart = format.parse(getStartDate());
			java.util.Date dateEnd = format.parse(getEndDate());
			   Calendar calCheckStart=Calendar.getInstance();
			   calCheckStart.setTime(dateStart);
			   java.util.Date checkInDate = calCheckStart.getTime();
			   this.fromDate =new Timestamp(checkInDate.getTime());

			   Calendar calCheckEnd=Calendar.getInstance();
			   calCheckEnd.setTime(dateEnd);
			   java.util.Date checkOutDate = calCheckEnd.getTime();
			   this.toDate =new Timestamp(checkOutDate.getTime());
			   
			if(this.getMyFileFileName()!=null && !this.getMyFileFileName().equalsIgnoreCase(""))
			{
				
				paymentDetail.setFromDate(this.fromDate);
				paymentDetail.setToDate(this.toDate);
				paymentDetail.setCreatedDate(currentTS);
				paymentDetail.setCreatedBy(this.userId);
				paymentDetail.setUploadFileName(this.getMyFileFileName());
				paymentDetail.setIsActive(true);
				paymentDetail.setIsDeleted(false);
				paymentDetail.setPmsProperty(property);
				paymentDetail.setPaymentBookingId(getPaymentBookingId());
				paymentDetail.setPaymentAmount(getPaymentAmount());
				paymentController.add(paymentDetail);
				File file = new File(getText("storage.payment.photo") + "/"
						+ getPropertyId() + "/",this.getMyFileFileName());
				String filePath = file.getAbsolutePath();
				FileUtils.copyFile(this.myFile, file);// copying image in the
				
				
				
				ArrayList<String> arlEmail=new ArrayList<String>();
				if(this.propertyEmail!=null){
					StringTokenizer stoken2=new StringTokenizer(this.propertyEmail,",");
					while(stoken2.hasMoreElements()){
						arlEmail.add(stoken2.nextToken());
					}	
				}
				if(this.resortEmail1!=null && !this.resortEmail1.isEmpty() ){
					arlEmail.add(this.resortEmail1);
				}
				if(this.resortEmail2!=null && !this.resortEmail2.isEmpty() ){
					arlEmail.add(this.resortEmail2);
				}
				if(this.resortEmail3!=null && !this.resortEmail3.isEmpty() ){
					arlEmail.add(this.resortEmail3);
				}
				if(this.resortEmail4!=null && !this.resortEmail4.isEmpty() ){
					arlEmail.add(this.resortEmail4);
				}
				
				arlEmail.add(getText("finance.notification.email"));
				arlEmail.add(getText("payment.notification.email"));
				arlEmail.add(getText("operations.notification.email"));
				arlEmail.add(getText("superadmin.notification.email"));
				Integer emailSize=arlEmail.size();
				
				Configuration cfg = new Configuration();
				cfg.setClassForTemplateLoading(PropertyPaymentAction.class, "../../../");
 				Template template = cfg.getTemplate(getText("payment.receipt.upload.voucher"));
 				int i=0;
 				String emailArray[]=new String[emailSize];
 				//send email
 				Email em = new Email();
 				Iterator<String> iterEmail=arlEmail.iterator();
 				while(iterEmail.hasNext()){
 					emailArray[i]=iterEmail.next().trim();
 					i++;
 				}
 				
 				Writer out = new StringWriter();
 				template.process("", out);
 				em.set_from(getText("email.from"));
 				em.set_username(getText("email.username"));
 				em.set_subject(strSubject);
 				em.set_bodyContent(out);
 				em.set_fileName(filePath);
 				em.setEmailArray(emailArray);
 				em.sendPayment();	
			
				
			}
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}

	public String getPaymentHistory(){
		
		try{
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			
			response.setContentType("application/json");
			this.propertyId = (Integer) sessionMap.get("propertyId");
			PropertyPaymentDetailManager paymentController = new PropertyPaymentDetailManager();
			if(limit !=null && offset !=null){
				this.paymentList = paymentController.list(this.propertyId,offset,limit);	
			}else{
			this.paymentList = paymentController.list(this.propertyId);
			}
			if(this.paymentList.size() != 0){
				int serialNo = 1;
			for(PropertyPaymentDetails payment : paymentList ){
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				
				jsonOutput += "\"DT_RowId\":\"" + payment.getPropertyPaymentId()+ "\"";
				jsonOutput += ",\"serialNo\":\"" + serialNo + "\"";
				String pattern = "dd-MM-yyyy";
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
				String date = simpleDateFormat.format(payment.getCreatedDate());
				jsonOutput += ",\"paymentDate\":\"" + date + "\"";
				jsonOutput += ",\"paymentBookingId\":\"" + payment.getPaymentBookingId() + "\"";
				jsonOutput += ",\"paidAmount\":\"" + payment.getPaymentAmount() + "\"";
				jsonOutput += ",\"uploadReceipt\":\"" + payment.getUploadFileName() + "\"";
				String path = "/payment/receipt/";
			    jsonOutput += ",\"uploadPath\":\"" + path + payment.getPmsProperty().getPropertyId()+ "/" + payment.getUploadFileName() + "\"";
				jsonOutput += "}";
				serialNo++;
				

					
			}
			}
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			
			
		}catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		}finally {

		}
		
		return null;
		
	}

	public String execute() {
		
		PmsPropertyManager propertyController = new PmsPropertyManager();
		listPmsProperty = propertyController.listPropertyByUser(getUser().getUserId());
		
		if(listPmsProperty.size() == 0){
		if(sessionMap!=null) sessionMap.invalidate();
		if(session!=null) session.invalidate();
		HttpServletResponse response = ServletActionContext.getResponse();
		response.setHeader("Pragma","no-cache");
		response.setHeader("Cache-Control","no-store");
		response.setHeader("Cache-Control","no-cache ");
		response.setHeader("Expires","0");
		response.setDateHeader("Expires",0);
		return "error";
		}
		else{
			
		}
		

		return SUCCESS;
	}
	
	

	public List<PmsProperty> getListPmsProperty() {
		return listPmsProperty;
	}

	public void setListPmsProperty(List<PmsProperty> listPmsProperty) {
		this.listPmsProperty = listPmsProperty;
	}

	public int getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(int propertyId) {
		this.propertyId = propertyId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public File getMyFile() {
		return myFile;
	}

	public void setMyFile(File myFile) {
		this.myFile = myFile;
	}

	public String getMyFileContentType() {
		return myFileContentType;
	}

	public void setMyFileContentType(String myFileContentType) {
		this.myFileContentType = myFileContentType;
	}

	public String getMyFileFileName() {
		return myFileFileName;
	}

	public void setMyFileFileName(String myFileFileName) {
		this.myFileFileName = myFileFileName;
	}

	public String getPaymentBookingId() {
		return paymentBookingId;
	}

	public void setPaymentBookingId(String paymentBookingId) {
		this.paymentBookingId = paymentBookingId;
	}

	public Double getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(Double paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public Timestamp getFromDate() {
		return fromDate;
	}

	public void setFromDate(Timestamp fromDate) {
		this.fromDate = fromDate;
	}

	public Timestamp getToDate() {
		return toDate;
	}

	public void setToDate(Timestamp toDate) {
		this.toDate = toDate;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	public Integer getOffset() {
		return offset;
	}

	public void setOffset(Integer offset) {
		this.offset = offset;
	}
	

}
