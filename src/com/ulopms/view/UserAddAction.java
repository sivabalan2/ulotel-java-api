package com.ulopms.view;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.regex.Pattern;

import org.apache.commons.lang.RandomStringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.dispatcher.SessionMap; 
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;

//import com.whl4.controller.DepartmentManager;
//import com.whl4.controller.CompanyManager;













import org.apache.struts2.interceptor.SessionAware;





















//import com.whl4.model.Company;
//import com.whl4.model.Department;
import com.opensymphony.xwork2.ActionSupport;
import com.ulopms.api.Login;
import com.ulopms.api.Users;
import com.ulopms.controller.AccessRightManager;
import com.ulopms.controller.PmsTagManager;
import com.ulopms.controller.PropertyAccommodationManager;
import com.ulopms.controller.RewardsManager;
import com.ulopms.controller.RoleManager;
import com.ulopms.controller.UserAccessRightManager;
import com.ulopms.controller.UserLoginManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.AccessRight;
import com.ulopms.model.PmsTags;
import com.ulopms.model.PropertyAccommodation;
import com.ulopms.model.RewardDetails;
import com.ulopms.model.RewardUserDetails;
import com.ulopms.model.Role;
import com.ulopms.model.User;
import com.ulopms.model.UserAccessRight;
import com.ulopms.security.passwordDecrypt;
import com.ulopms.util.Email;

import freemarker.template.Configuration;
import freemarker.template.Template;

import javax.servlet.http.HttpServletRequest;

public class UserAddAction extends ActionSupport implements
SessionAware,ServletRequestAware, UserAware {
	
	private static final long serialVersionUID = 9149826260758390091L;
	private User user;

	private List<Role> roleList;
	// private List<Department> departList;
	private String loginName;
	private String userName;
	private String password;
	private String emailId;
	private String phone;
	private short roleId;
	private Long departmentId;
	private int userId;
	private int rewardDetailId;
	

	private int companyId;
	private String accessRightsId;
	
	private HttpServletRequest request;
	
    private HttpSession session;
	
	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}
	
	private SessionMap<String,Object> sessionMap;  
	  
	//getters and setters  
	  
	@Override  
	public void setSession(Map<String, Object> map) {  
	    sessionMap=(SessionMap)map;  
	}  

	private static final Logger logger = Logger.getLogger(UserAddAction.class);

	private UserLoginManager linkController;
	private RoleManager roleController = new RoleManager();

	// private DepartmentManager departController = new DepartmentManager();
	// private CompanyManager companyController = new CompanyManager();

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	public UserAddAction() {
		linkController = new UserLoginManager();
	}

	public String execute() {

		this.roleList = roleController.list();
		// this.departList = departController.list();
		return SUCCESS;
	}
	
	
public String getSingleUser() throws IOException {
		
		
		
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		   
			
			
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");

			
			   User user = linkController.find(getUserId());
				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"userId\":\"" + user.getUserId() + "\"";
				jsonOutput += ",\"userName\":\"" + user.getUserName()+ "\"";
				jsonOutput += ",\"email\":\"" + user.getEmailId()+ "\"";
				jsonOutput += ",\"phone\":\"" + user.getPhone()+ "\"";
				jsonOutput += ",\"address1\":\"" + user.getAddress1()+ "\"";
				jsonOutput += ",\"address2\":\"" + user.getAddress2()+ "\"";
				jsonOutput += "}";

			

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}

	public String UserSave() {
		try {

			HttpServletRequest request = ServletActionContext.getRequest();
			User u = new User();
			u = linkController.findUser(Integer.parseInt(request
					.getParameter("userid")));

			//this.setLoginName(u.getLoginName());
			this.setEmailId(u.getEmailId());
			//
			//this.setPassword(u.getPassword());
			this.setUserName(u.getUserName());
			this.setPhone(u.getPhone());
			this.setUserId(u.getUserId());

			this.roleList = roleController.list();
			// this.departList = departController.list();
			this.user = u;
		} catch (Exception e) {
			logger.error(e);
		} finally {

		}
		return SUCCESS;
	}

	public Integer UserAdd(String userName , String emailId , Integer roleId) {
		
		try {
			AccessRightManager rightsController = new AccessRightManager();
			UserAccessRightManager userRightsController = new UserAccessRightManager();	
			HttpServletResponse response = ServletActionContext.getResponse();
			//response.reset();
			response.flushBuffer();
			response.setContentType("application/json");
			
			List<User> searchUser = linkController.findUserByEmail(emailId);
			if(searchUser.size()==0 && searchUser.isEmpty())
			{
				User user = new User();
				HttpServletRequest request = ServletActionContext.getRequest();
				long time = System.currentTimeMillis();
				java.sql.Date date = new java.sql.Date(time);
	
				Role role = roleController.find(roleId);
	
				
				RandomStringUtils  rsu = new RandomStringUtils();
				String PUBLIC_SALT = passwordDecrypt.secrandom();
				String tpassword = rsu.random(8,true,true).toString();
				String hashpassword=passwordDecrypt.getDigestvalid(tpassword,PUBLIC_SALT);
				String tloginName = rsu.random(10,true,true).toString();
				
				//u.setLoginName(tloginName);
				//u.setPassword(tpassword);
				//u.setPassword(hashpassword);
				user.setHashpassword(hashpassword);
				user.setSaltkey(PUBLIC_SALT);
				// u.setPhone(phone);
				user.setUserName(userName);
				user.setEmailId(emailId);
				user.setRole(role);
				user.setIsActive(true);
				user.setIsDeleted(false);
				
				
			//	u.setCompany(c);
				//u.setCreatedOn(date);
				//u.setCreatedUser(getUser());
						
				
				User newUser = linkController.add(user);
				this.userId=newUser.getUserId();
				
				/*if(!getAccessRightsId().equalsIgnoreCase(""))
				{
					for (int i = 0; i < getAccessRightsId().split(",").length; i++) {
						String f = getAccessRightsId().split(",")[i].trim();
						if(!f.equalsIgnoreCase(""))
						{
							UserAccessRight s = new UserAccessRight();
							AccessRight ar =  rightsController.find(Integer.parseInt(f));
							s.setAccessRight(ar);
							s.setAccessRightsPermission(true);
							s.setUser(newUser);
							userRightsController.add(s);
						}
					}
				}
				else
				{
					List<AccessRight> accessRightList = rightsController.list("PROJECT");
					for(AccessRight rights : accessRightList)
					{
						UserAccessRight s = new UserAccessRight();
						//AccessRights ar =  rightsController.find(Integer.parseInt(f));
						s.setAccessRight(rights);
						s.setAccessRightsPermission(false);
						s.setUser(newUser);
						userRightsController.add(s);
					}
				}
				
				response.getWriter().write("{" + "\"result\":\"Created Successfully\""  + ",\"tempid\":\""+ request.getParameter("tempid") +"\"}");
				*/
				//send email notification 
				//email template
				
				
				
				Configuration cfg = new Configuration();
				cfg.setClassForTemplateLoading(UserAddAction.class, "../../../");
//				Template template = cfg.getTemplate(getText("ulosendpassword.template"));
				Template template = null;
				if(role.getRoleId() == 5 || role.getRoleId() == 3){
					 template = cfg.getTemplate(getText("ulopartnersendpassword.template"));
				}else{
					template = cfg.getTemplate(getText("ulosendpassword.template"));
				}
				Map<String, String> rootMap = new HashMap<String, String>();
				rootMap.put("password", tpassword);
				//rootMap.put("", getText("url.forgetpassword")+uniqueKey.toString());
				rootMap.put("userName", userName);
				rootMap.put("createdBy", userName);
				rootMap.put("from", getText("notification.from"));
				//rootMap.put("logoFileName", c1.getLogoPath());
				rootMap.put("url", getText("app.api.url"));
				Writer out = new StringWriter();
				template.process(rootMap, out);

				//send email
				Email em = new Email();
				em.set_to(emailId);
				em.set_from(getText("email.from"));
				em.set_host(getText("email.host"));
				em.set_password(getText("email.password"));
				em.set_port(getText("email.port"));
				em.set_username(getText("email.username"));
				em.set_subject(getText("forgetpassword.subject"));
				em.set_bodyContent(out);
				em.send();
				
				//end notification
				//response.getWriter().write("{\"data\":[{" + "\"result\":\"Created Successfully\""  + "}]}");
				
				//response.setStatus(200);
			}
			else
			{
				//response.getWriter().write("{\"data\":[{" + "\"result\":\"Already Exists\""  + "}]}");
				//response.getWriter().write("{" + "\"result\":\"Already Exists\""  + ",\"tempid\":\""+ request.getParameter("tempid") +"\"}");
				response.setStatus(412);
			}
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return userId;
	}
	
	
	public String customerAdd() {
		String ret = "error";
		
		try {
			AccessRightManager rightsController = new AccessRightManager();
			UserAccessRightManager userRightsController = new UserAccessRightManager();
			if(getEmailId()!=null && getEmailId()!=""){
				this.emailId=getEmailId().trim();
			}
			if(getPhone()!=null && getPhone()!=""){
				this.phone=getPhone().trim();
			}
			HttpServletResponse response = ServletActionContext.getResponse();
			//response.reset();
			response.flushBuffer();
			response.setContentType("application/json");
			User u = new User();
			List<User> listUserCheck=linkController.findUserByEmail(emailId);
			if(listUserCheck.size()>0 && !listUserCheck.isEmpty()){
				for(User user:listUserCheck){
					
					User us=linkController.find(user.getUserId());
					us.setPhone(phone);
					linkController.edit(us);
				}
			}
			
			User searchUser = linkController.findUserByEmail(emailId,phone);
			if(searchUser == null)
			{
				HttpServletRequest request = ServletActionContext.getRequest();
				SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
				Calendar today=Calendar.getInstance();
		     	String strCurrentDate = new SimpleDateFormat("yyyy-MM-dd").format(today.getTime());
		     	java.util.Date currentDate = format1.parse(strCurrentDate);
				
				long time = System.currentTimeMillis();
				java.sql.Date date = new java.sql.Date(time);
				if (getUserId() > 0) {
					
					// u = linkController.findUser(getUserId());
					u.setUserId(getUserId());
					// u.setModifiedOn(date);
					// u.setModifiedUser(getUser());
					// u.setModificationCounter(u.getModificationCounter()+1);
				}
				
				
	
				Role r = roleController.find(roleId);
 				
				//RandomStringUtils  rsu = new RandomStringUtils();
				//String PUBLIC_SALT = passwordDecrypt.secrandom();
				//String tpassword = rsu.random(8,true,true).toString();
				//String hashpassword=passwordDecrypt.getDigestvalid(password,PUBLIC_SALT);
				//String tloginName = rsu.random(10,true,true).toString();
				
				//u.setLoginName(tloginName);
				//u.setPassword(tpassword);
				//u.setPassword(hashpassword);
				Timestamp currentTS = new Timestamp(currentDate.getTime());
				u.setCreatedDate(currentTS);
				//u.setHashpassword(hashpassword);
				//u.setSaltkey(PUBLIC_SALT);
				u.setPhone(phone);
				u.setUserName(userName);
				u.setEmailId(emailId);
				u.setRole(r);
				u.setIsActive(true);
				u.setIsDeleted(false);
				
				
			//	u.setCompany(c);
				//u.setCreatedOn(date);
				//u.setCreatedUser(getUser());
						
				
				User newUser = linkController.add(u);
				if(getRewardDetailId() != 0){
				RewardsManager rewardsController = new RewardsManager();
				RewardUserDetails rewardUsers = new RewardUserDetails();
				User user = linkController.findUserEmail(emailId);
				rewardUsers.setUser(user);
				RewardDetails rewardDetails = rewardsController.findDetailId(getRewardDetailId());
				rewardUsers.setRewardDetails(rewardDetails);
				rewardUsers.setIsActive(true);
				rewardUsers.setUsedRewardPoints(rewardDetails.getRewardPoints());
				rewardUsers.setIsDeleted(false);
				rewardsController.add(rewardUsers);
				sessionMap.put("uloRewardPoints",rewardDetails.getRewardPoints());
				}
				// creating session
				
				sessionMap.put("uloUserName",newUser.getUserName());
				sessionMap.put("uloUserId",newUser.getUserId());
				sessionMap.put("uloEmailId",newUser.getEmailId());
			    sessionMap.put("uloUserStatus","1");
				sessionMap.put("uloUSER", user);
				sessionMap.put("uloSignup","true");
				sessionMap.put("uloPhone",newUser.getPhone());
			
				
				
			   if(!getAccessRightsId().equalsIgnoreCase(""))
				{
					for (int i = 0; i < getAccessRightsId().split(",").length; i++) {
						String f = getAccessRightsId().split(",")[i].trim();
						if(!f.equalsIgnoreCase(""))
						{
							UserAccessRight s = new UserAccessRight();
							AccessRight ar =  rightsController.find(Integer.parseInt(f));
							s.setAccessRight(ar);
							s.setAccessRightsPermission(true);
							s.setUser(newUser);
							userRightsController.add(s);
						}
					}
				}
				else
				{
					List<AccessRight> accessRightList = rightsController.list("PROJECT");
					for(AccessRight rights : accessRightList)
					{
						UserAccessRight s = new UserAccessRight();
						//AccessRights ar =  rightsController.find(Integer.parseInt(f));
						s.setAccessRight(rights);
						s.setAccessRightsPermission(false);
						s.setUser(newUser);
						userRightsController.add(s);
					}
				}
				
				//response.getWriter().write("{" + "\"result\":\"Created Successfully\""  + ",\"tempid\":\""+ request.getParameter("tempid") +"\"}");
				
				//send email notification 
				//email template
				
				
				
				Configuration cfg = new Configuration();
				cfg.setClassForTemplateLoading(UserAddAction.class, "../../../");
				Template template = cfg.getTemplate(getText("userinvitation.template"));
				Map<String, String> rootMap = new HashMap<String, String>();
				//rootMap.put("password", password);
				//rootMap.put("", getText("url.forgetpassword")+uniqueKey.toString());
				rootMap.put("userName", getUserName());
				rootMap.put("from", getText("notification.from"));
				//rootMap.put("createdBy", getUser().getUserName());
				//rootMap.put("logoFileName", c1.getLogoPath());
				rootMap.put("url", getText("app.api.url"));
				Writer out = new StringWriter();
				template.process(rootMap, out);

				//send email
				Email em = new Email();
				em.set_to(getEmailId());
				em.set_from(getText("email.from"));
				em.set_cc(getText("reservation.notification.email"));
				em.set_cc2(getText("bookings.notification.email"));
 				//ArrayList<String> x = new ArrayList<>(Arrays.asList("rajalakshmi.ar@ulohotels.com", "support@ulohotels.com"));
 				//em.set_bcc(x);
 				//em.set_cc3(getText("bookings2.notification.email"));
 				//em.set_cc4(getText("support.notification.email"));
				//em.set_host(getText("email.host"));
				//em.set_password(getText("email.password"));
				//em.set_port(getText("email.port"));
				em.set_username(getText("email.username"));
				em.set_subject(getText("userinvitation.subject"));
				em.set_bodyContent(out);
				em.send();
				
				
				String jsonOutput = "";
				
				//response.setContentType("application/json");
				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				
				jsonOutput += "\"userName\":\"" + newUser.getUserName() + "\"";
				jsonOutput += ",\"userId\":\"" + newUser.getUserId() + "\"";
				jsonOutput += ",\"emailId\":\"" + newUser.getEmailId()+ "\"";
				jsonOutput += ",\"phone\":\"" + newUser.getPhone()+ "\"";
			    jsonOutput += "}";
                
			    response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
				
				 ret= null;
				 
					String message = "&sms_text=" + "Thank you for signing up with Ulo Hotels. Our goal is offer best guest experience in budget stays. Want to know more about us? Check this link:https://www.ulohotels.com/";
					String numbers = "&sms_to=" + "+91"+phone ;
					String from = "&sms_from=" + "ULOHTL";
					String type = "&sms_type=" + "trans"; 
					
					// Send data
					HttpURLConnection conn = (HttpURLConnection) new URL("https://secureets.knowlarity.com/ets/dom/clients/ulohotels/api/ulohotels_sms_api.py").openConnection();
					//"http://etsrds.kapps.in/webapi/ulohotels/api/ulohotels_sms_api.py?sms_text=hi%20this%20gopinath%20from%20ulo&sms_to=%20918220211068&sms_from=ULOHTL&sms_type=promo"
					String data = numbers + message +  from + type;
					conn.setDoOutput(true);
					conn.setRequestMethod("POST");
					conn.addRequestProperty("auth_key", "Basic dWxvaG90ZWxzX1Q6VUxPSFRMQDIwMTg=");
					conn.setRequestProperty("Content-Length", Integer.toString(data.length()));
					conn.getOutputStream().write(data.getBytes("UTF-8"));
					final BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
					final StringBuffer stringBuffer = new StringBuffer();
					String line;
					
					while ((line = rd.readLine()) != null) {
						stringBuffer.append(line);
						
					}
					rd.close();
				 
				 
				 
				//end notification
				//response.getWriter().write("{\"data\":[{" + "\"result\":\"Created Successfully\""  + "}]}");
				
				//response.setStatus(200);
			}
			else
			{
				String jsonOutput = "",strRemarks=null;
				boolean blnCheck=false;
				 if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += ",{";
					else
						jsonOutput += "{";
				 
				 List<User> listEmailUser = linkController.findUserByEmail(emailId);
				 if(listEmailUser.size()>0 && !listEmailUser.isEmpty()){
					 strRemarks="Email Id already exists";
					 blnCheck=true;
					 jsonOutput += "\"checkMail\":\"" + strRemarks + "\""; 
				 }
				 
				 if(!blnCheck){
					 List<User> listPhoneUser=linkController.findUserByPhone(phone);
					 if(listPhoneUser.size()>0 && !listPhoneUser.isEmpty()){
						 strRemarks="Mobile number already exists";
						 jsonOutput += ",\"checkPhone\":\"" + strRemarks + "\"";
					 }	 
				 }
				 
				 
				 jsonOutput += "}";
	                
				 response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
				 
				 
				/* List<User> listUser = linkController.findUserByEmail(emailId);
			     if(listUser.size()>0 && !listUser.isEmpty()){
			    	 for(User userlist:listUser){
			    		 Integer userId=userlist.getUserId();
			    		 User user=linkController.find(userId);
			    		 SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
						 Calendar today=Calendar.getInstance();
					     String strCurrentDate = new SimpleDateFormat("yyyy-MM-dd").format(today.getTime());
					     java.util.Date currentDate = format1.parse(strCurrentDate);
					     Timestamp currentTS = new Timestamp(currentDate.getTime());
					     user.setModifiedDate(currentTS);
					     linkController.edit(user);
			    	 }
			     }*/
				 
				 ret= "error";
				 
				//response.getWriter().write("{\"data\":[{" + "\"result\":\"Already Exists\""  + "}]}");
				
				//response.getWriter().write("{" + "\"result\":\"Already Exists\""  + ",\"tempid\":\""+ request.getParameter("tempid") +"\"}");
				//response.setStatus(412);
			}
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return ret;
	}

	public String updateProfile(Users users){

		String output = "";
		
		try {
			int count=0;
			this.emailId=users.getEmailId();
			 String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."
						+ "[a-zA-Z0-9_+&*-]+)*@" + "(?:[a-zA-Z0-9-]+\\.)+[a-z"
						+ "A-Z]{2,7}$";
			 
			 Pattern pat = Pattern.compile(emailRegex);
			 Boolean bool = pat.matcher(this.emailId).matches();
			 
			String jsonOutput = "";
			SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
			User user=new User();
			PmsTagManager tagController=new PmsTagManager();
			UserLoginManager userController=new UserLoginManager();
			RoleManager roleController=new RoleManager();
			Calendar today=Calendar.getInstance();
			today.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
	     	String strCurrentDate = new SimpleDateFormat("yyyy-MM-dd").format(today.getTime());
	     	java.util.Date currentDate = format1.parse(strCurrentDate);
	     	Timestamp tsDate=new Timestamp(currentDate.getTime());
			User searchUser = linkController.find(Integer.parseInt(users.getUserId()));
			if(searchUser==null){
				user.setEmailId(users.getEmailId());
				user.setAddress1(users.getAddress());
				user.setCreatedDate(tsDate);
				user.setPhone(users.getMobileNumber());
				user.setPhone1(users.getAlternateNumber());
				user.setUserName(users.getFullName());
				Role role=roleController.find(4);
				user.setRole(role);
				PmsTags tag=tagController.find(users.getTripTypeId());
				user.setPmsTags(tag);
				User loginuser=userController.add(user);
				
				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				
				jsonOutput += "\"userName\":\"" + loginuser.getUserName() + "\"";
				jsonOutput += ",\"userId\":\"" + loginuser.getUserId() + "\"";
				jsonOutput += ",\"emailId\":\"" + loginuser.getEmailId()+ "\"";
				jsonOutput += ",\"mobileNumber\":\"" + loginuser.getPhone()+ "\"";
				jsonOutput += ",\"address\":\"" + (loginuser.getAddress1()==null?"NA":loginuser.getAddress1())+ "\"";
				if(loginuser.getPmsTags()==null){
					jsonOutput += ",\"tripTypeId\":\"" + 0 + "\"";
				}else{
					jsonOutput += ",\"tripTypeId\":\"" + loginuser.getPmsTags().getTagId()+ "\"";	
				}
				
			    jsonOutput += "}";
			    count++;
			}else{
				
				User verifyuser=userController.find(searchUser.getUserId());
				if(users.getAddress()!=null){
					verifyuser.setAddress1(users.getAddress());	
				}
				if(users.getEmailId()!=null){
					verifyuser.setEmailId(users.getEmailId());					
				}
				if(users.getMobileNumber()!=null){
					verifyuser.setPhone(users.getMobileNumber());	
				}
				
				User usersprofile=userController.edit(verifyuser);
				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				
				jsonOutput += "\"userName\":\"" + usersprofile.getUserName() + "\"";
				jsonOutput += ",\"userId\":\"" + usersprofile.getUserId() + "\"";
				jsonOutput += ",\"emailId\":\"" + (usersprofile.getEmailId()==null?"NA":usersprofile.getEmailId())+ "\"";
				jsonOutput += ",\"mobileNumber\":\"" + (usersprofile.getPhone()==null?"NA":usersprofile.getPhone())+ "\"";
				jsonOutput += ",\"address\":\"" + (usersprofile.getAddress1()==null?"NA":usersprofile.getAddress1())+ "\"";
				if(usersprofile.getPmsTags()==null){
					jsonOutput += ",\"tripTypeId\":\"" + 0 + "\"";
				}else{
					jsonOutput += ",\"tripTypeId\":\"" + usersprofile.getPmsTags().getTagId()+ "\"";	
				}
			    jsonOutput += "}";
			    count++;
			}
			
			String data="\"data\":[" + jsonOutput + "]";
 			String strData=data.replaceAll("\"", "\"");
 			
 			if(count == 0){
 				output="{\"error\":\"1\",\"message\":\"No Records Found\"}";
			}else{
				output="{\"error\":\"0\"," + strData + "}";
			}
		    
		} catch (Exception e) {
			output="{\"error\":\"2\",\"message\":\"Some technical error on update user profile api\"}";
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return output;
	
	}

	public String getUserProfile(Users user){
		String output=null;
		try{
			String jsonOutput = "";
			//HttpServletResponse response=ServletActionContext.getResponse();
			//response.setContentType("application/json");
			UserLoginManager userController=new UserLoginManager();
			int userid=Integer.parseInt(user.getUserId());
			User searchUser = userController.find(userid);
			if(searchUser!=null){
				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				
				jsonOutput += "\"userName\":\"" + (searchUser.getUserName()==null?"NA":searchUser.getUserName())+ "\"";
				jsonOutput += ",\"userId\":\"" + searchUser.getUserId() + "\"";
				jsonOutput += ",\"emailId\":\"" + (searchUser.getEmailId()==null?"NA":searchUser.getEmailId())+ "\"";
				jsonOutput += ",\"mobileNumber\":\"" + (searchUser.getPhone()==null?"NA":searchUser.getPhone())+ "\"";
				jsonOutput += ",\"alternateNumber\":\"" + (searchUser.getPhone1()==null?"NA":searchUser.getPhone1())+ "\"";
				jsonOutput += ",\"address\":\"" + (searchUser.getAddress1()==null?"NA":searchUser.getAddress1())+ "\"";
				if(searchUser.getPmsTags()==null){
					jsonOutput += ",\"tripTypeId\":\"" + 0 + "\"";
				}else{
					jsonOutput += ",\"tripTypeId\":\"" + searchUser.getPmsTags().getTagId()+ "\"";	
				}
			    jsonOutput += "}";
			    
			}
			
			String data="\"data\":[" + jsonOutput + "]";
 			String strData=data.replaceAll("\"", "\"");
 			
 			if(searchUser == null){
 				output="{\"error\":\"1\",\"message\":\"Sorry no results found\"}";
			}else{
				output="{\"error\":\"0\"," + strData + "}";
				
			}
 			
		}catch(Exception e){
			output="{\"error\":\"2\",\"message\":\"Some technical error on user profile api\"}";
			e.printStackTrace();
		}
		return output;
	}
	
	public List<Role> getRoleList() {
		return roleList;
	}

	public void setRoleList(List<Role> roleList) {
		this.roleList = roleList;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public short getRoleId() {
		return roleId;
	}

	public void setRoleId(short roleId) {
		this.roleId = roleId;
	}

	public Long getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(Long departmentId) {
		this.departmentId = departmentId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getRewardDetailId() {
		return rewardDetailId;
	}

	public void setRewardDetailId(int rewardDetailId) {
		this.rewardDetailId = rewardDetailId;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public String getAccessRightsId() {
		return accessRightsId;
	}

	public void setAccessRightsId(String accessRightsId) {
		this.accessRightsId = accessRightsId;
	}


	

}
