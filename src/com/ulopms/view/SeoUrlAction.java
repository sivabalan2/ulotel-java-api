package com.ulopms.view;

import java.net.InetAddress;
import java.net.URL;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;
import com.ulopms.api.Hotels;
import com.ulopms.api.Locations;
import com.ulopms.api.Property;
import com.ulopms.api.Search;
import com.ulopms.controller.AreaManager;
import com.ulopms.controller.BlogArticlesManager;
import com.ulopms.controller.BlogCategoriesManager;
import com.ulopms.controller.BlogLocationsManager;
import com.ulopms.controller.GooglePlaceManager;
import com.ulopms.controller.LocationManager;
import com.ulopms.controller.PmsPropertyManager;
import com.ulopms.controller.SeoContentManager;
import com.ulopms.controller.UserCookiesManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.Area;
import com.ulopms.model.BlogArticles;
import com.ulopms.model.BlogCategories;
import com.ulopms.model.BlogLocations;
import com.ulopms.model.GoogleArea;
import com.ulopms.model.GoogleLocation;
import com.ulopms.model.Location;
import com.ulopms.model.PmsProperty;
import com.ulopms.model.SeoContent;
import com.ulopms.model.User;
import com.ulopms.model.UserCookies;

import javax.servlet.http.HttpServletRequest;

public class SeoUrlAction extends ActionSupport implements SessionAware,ServletRequestAware,
UserAware {

	private static final long serialVersionUID = 9149826260758390091L;

	private User user;
	
	private String metaTag;
	private String metaDescription;
	private String webContent;
	private String locationUrl;
	private Integer locationUrlId;
	private String urlType;
	private String returnType;
	public Integer propertyId;
	public Integer statusCode;
	





	private String longitude;
	private String latitude;
	
	private String StartDate;
	private String EndDate;
	
	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	
	
	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getMetaTag() {
		return metaTag;
	}

	public void setMetaTag(String metaTag) {
		this.metaTag = metaTag;
	}

	public String getMetaDescription() {
		return metaDescription;
	}

	public void setMetaDescription(String metaDescription) {
		this.metaDescription = metaDescription;
	}

	public String getWebContent() {
		return webContent;
	}

	public void setWebContent(String webContent) {
		this.webContent = webContent;
	}

	

	

	private HttpSession session;

	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}
	
	private SessionMap<String,Object> sessionMap;  
	  
	//getters and setters  
	  
	
	public void setSession(Map<String, Object> map) {  
	    sessionMap=(SessionMap)map;  
	}  
	
	private static final Logger logger = Logger.getLogger(SeoUrlAction.class);

	// @Override
	// public User getModel() {
	// return user;
	// }
	private Integer locationId;
	private Integer areaId;
	
	public Integer getAreaId() {
		return areaId;
	}

	public void setAreaId(Integer areaId) {
		this.areaId = areaId;
	}

	public Integer getLocationId() {
		return locationId;
	}

	public void setLocationId(Integer locationId) {
		this.locationId = locationId;
	}

	@Override
	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	private HttpServletRequest request;

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	
	
	public SeoUrlAction() {

	}
	
	private String url;

	public String getUrl()
	{
		return url;
	}
	
	
	public void setUrl(String url){
		this.url=url;
	}
	
	public String execute() {
		String strReturn=null;
		try{
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("text/html");
			
			if(getLocationId()!=null){
				locationUrlLink();
				urlType="/properties.tiles";
				strReturn=SUCCESS;
				
			}else if(getPropertyId()!=null){
				propertyUrlLink();
				urlType="/details.tiles";
				strReturn=SUCCESS;
				
			}else{
				String returnValue=checkUrlLink();
				if(returnValue.equalsIgnoreCase("property")){
					urlType="/details.tiles";
					strReturn=SUCCESS;
				}else if(returnValue.equalsIgnoreCase("location")){
					urlType="/properties.tiles";
					strReturn=SUCCESS;
				}else if(returnValue.equalsIgnoreCase("area")){
					urlType="/properties.tiles";
					strReturn=SUCCESS;
				}else if(returnValue.equalsIgnoreCase("blogarticles")){
					urlType="/uloblogdetail.tiles";
					strReturn=SUCCESS;
				}else if(returnValue.equalsIgnoreCase("blogcategories")){
					urlType="/bloglistulo.tiles";
//					/uloblogdetail.tiles
					strReturn=SUCCESS;
				}else if(returnValue.equalsIgnoreCase("bloglocations")){
					urlType="/bloglistulo.tiles";
					strReturn=SUCCESS;
				}else if(returnValue.equalsIgnoreCase("selectlocation")){
					urlType="/home.tiles";
					strReturn=SUCCESS;
				}
				else if(returnValue.equalsIgnoreCase("inActiveArea")){
					
					urlType = (String) request.getAttribute("redirectUrl");
					strReturn="success_redirect";
					
				}
				else{
					strReturn=ERROR;
				}
				
				
				
			}
		}catch(Exception e){
			logger.error(e);
			e.printStackTrace();
		}
		return strReturn;
	}


	
	public void locationUrlLink() {
		
		try{
			
			HttpSession session=request.getSession(false);
		
			SeoContentManager seoContentController = new SeoContentManager();
			
		    session.getAttribute("locationId");  
		    //int locationsId = (Integer) session.getAttribute("locationId");
		    request.setAttribute("locationId" ,session.getAttribute("locationId"));
		    
		    SeoContent seoContent = seoContentController.findSeoContent(getLocationId());
		    request.setAttribute("metaTag" ,seoContent.getTitle());
			request.setAttribute("description" ,seoContent.getDescription());
			request.setAttribute("content" ,seoContent.getContent());
		    
		}catch(Exception e){
			logger.error(e);
			e.printStackTrace();
		}
		
		//return SUCCESS;
	}

	public void propertyUrlLink() {
		try{
			SeoContentManager seoContentController = new SeoContentManager();
			HttpSession session=request.getSession(false); 
			
		    session.setAttribute("uloPropertyId", getPropertyId());
		    int propertyId = (Integer) session.getAttribute("uloPropertyId");
		    //request.setAttribute("locationId" ,session.getAttribute("locationId"));
		    
		    SeoContent seoContent = seoContentController.findId1(propertyId);
		    request.setAttribute("metaTag" ,seoContent.getTitle());
			request.setAttribute("description" ,seoContent.getDescription());
			request.setAttribute("content" ,seoContent.getContent());
			PmsPropertyManager propertyController=new PmsPropertyManager();
			
			if(getPropertyId()!=null && getPropertyId()>0)
	        {
		        PmsProperty pmsproperty = propertyController.find(getPropertyId());
		        this.setLatitude(pmsproperty.getLatitude().trim());
		        this.setLongitude(pmsproperty.getLongitude().trim());
		        request.setAttribute("placeId" ,pmsproperty.getPlaceId());
		        String strHotelPolicy=null,strStandardPolicy=null,strCancellationPolicy=null,strChildPolicy=null;
		        
		        strHotelPolicy=pmsproperty.getPropertyHotelPolicy();
				strStandardPolicy=pmsproperty.getPropertyStandardPolicy();
				strCancellationPolicy=pmsproperty.getPropertyCancellationPolicy();
				strChildPolicy=pmsproperty.getPropertyChildPolicy();
				String refundPolicy,changedRefundPolicy,cancelledPolicy,changedCancelledPolicy,hotelPolicy,changedHotelPolicy;
				
				if(strHotelPolicy!=null){
					hotelPolicy = strHotelPolicy.replace("\"", "\\\"");
					changedHotelPolicy = hotelPolicy.replaceAll("\\s+", " ");
				}else{
					changedHotelPolicy="";
				}
				
				if(strStandardPolicy!=null){
					refundPolicy = strStandardPolicy.replace("\"", "\\\"");
					changedRefundPolicy = refundPolicy.replaceAll("\\s+", " ");
				}else{
					changedRefundPolicy ="";
					
				}
				
				if(strCancellationPolicy!=null){
					cancelledPolicy = strCancellationPolicy.replace("\"", "\\\"");
					changedCancelledPolicy = cancelledPolicy.replaceAll("\\s+", " ");
				}else{
					changedCancelledPolicy="";
				}
		        request.setAttribute("hotelPolicy" ,changedHotelPolicy);
			    request.setAttribute("cancellationPolicy" ,changedCancelledPolicy);
			    request.setAttribute("refundPolicy" ,changedRefundPolicy);
	        }
			
		}catch(Exception e){
			logger.error(e);
			e.printStackTrace();
		}
		//return SUCCESS;
	}	
	
	public String checkUrlLink(){
		String strReturn="";
		try{
//			Integer uloUserId=(Integer)sessionMap.get("uloUserId");
			BlogArticlesManager articlesController=new BlogArticlesManager();
			BlogCategoriesManager blogCategoryController=new BlogCategoriesManager();
			BlogLocationsManager blogLocationController=new BlogLocationsManager();
			AreaManager areaController=new AreaManager();
			UserCookiesManager cookiesController=new UserCookiesManager();
			LocationManager locationController=new LocationManager();
			PmsPropertyManager propertyController=new PmsPropertyManager();
			//HttpSession session=request.getSession(false);
			String strUrlName=null;
			String strStartDate=request.getParameter("checkIn");
			String strEndDate=request.getParameter("checkOut");
			HttpServletResponse response = ServletActionContext.getResponse();
			URL url = new URL(request.getRequestURL().toString());  
			SeoContentManager seoContentController = new SeoContentManager();  
			strUrlName=url.getFile();
			strUrlName=strUrlName.replace("/", "");
			strUrlName=strUrlName.replace("ulopms", "");
			strUrlName=strUrlName.replace("blog", "");
			String strRequestUrl=request.getRequestURI().toString();
			String strSelectLocation="please-select-location";
			InetAddress IP=InetAddress.getLocalHost();
			
			List<Location> listLocationUrl=locationController.listLocationUrl(strUrlName.trim());
			if(listLocationUrl.size()>0 && !listLocationUrl.isEmpty()){
				
				for(Location location:listLocationUrl){
					 request.setAttribute("locationId" ,location.getLocationId());
					 request.setAttribute("areaLocationId" ,location.getLocationId());
//					 sessionMap.put("locationId", location.getLocationId());
					 if(request.getParameter("locationId") != null && request.getParameter("locationId").toString() !=""){
						 this.setLocationId(Integer.parseInt(request.getParameter("locationId").toString()));
					 }else{
						 this.setLocationId(location.getLocationId());
					 }
					 
					/* SeoContent seoContent = seoContentController.findSeoContent(location.getLocationId());	
					 request.setAttribute("metaTag" ,seoContent.getTitle());
					 request.setAttribute("description" ,seoContent.getDescription());
					 request.setAttribute("content" ,seoContent.getContent());	*/
					 
					 List<SeoContent> listSeoLocation=seoContentController.list(location.getLocationId());
					 if(listSeoLocation.size()>0 && !listSeoLocation.isEmpty()){
						 SeoContent seoContent = seoContentController.findSeoContent(location.getLocationId());	
						 request.setAttribute("metaTag" ,seoContent.getTitle());
						 request.setAttribute("description" ,seoContent.getDescription());
						 request.setAttribute("content" ,seoContent.getContent());	
					 }else{
						 request.setAttribute("metaTag" ,"<title>Ulohotels</title>");
						 request.setAttribute("description" ,"<p></p>");
						 request.setAttribute("content" ,"<p></p>");	
					 }
					 
					 request.setAttribute("locationName" ,location.getLocationDescription()==null?"":location.getLocationDescription().toUpperCase().trim());	
					 sessionMap.put("breadCrumAreaName", location.getLocationName());
					 if(strStartDate!=null && strStartDate!="" && strEndDate!=null && strEndDate!=""){
						 DateFormat format = new SimpleDateFormat("MMM d,yyyy", Locale.ENGLISH);
					     java.util.Date dateStart = format.parse(strStartDate);
					     java.util.Date dateEnd = format.parse(strEndDate);

					     Calendar calCheckStart=Calendar.getInstance();
					     calCheckStart.setTime(dateStart);
					     java.util.Date checkInDate = calCheckStart.getTime();
					    
					     Calendar calCheckEnd=Calendar.getInstance();
					     calCheckEnd.setTime(dateEnd);
					     java.util.Date checkOutDate = calCheckEnd.getTime();
						
					     String checkIn = new SimpleDateFormat("MMM d,yyyy").format(checkInDate);
		                 String checkOut = new SimpleDateFormat("MMM d,yyyy").format(checkOutDate);
		                 String checkIn1 = new SimpleDateFormat("MM/dd/yyyy").format(checkInDate); //dd MMM
		                 String checkOut1 = new SimpleDateFormat("MM/dd/yyyy").format(checkOutDate);  //dd MMM
		                 String checkIn2 = new SimpleDateFormat("MMM dd EEEEE").format(checkInDate);
		                 String checkOut2 = new SimpleDateFormat("MMM dd EEEEE").format(checkOutDate);
		                 String mbcheckIn = new SimpleDateFormat("MMM d,yyyy").format(checkInDate);
		                 String mbcheckOut = new SimpleDateFormat("MMM d,yyyy").format(checkOutDate);
		                 String mbcheckIn1 = new SimpleDateFormat("MM/dd/yyyy").format(checkInDate); //dd MMM
		                 String mbcheckOut1 = new SimpleDateFormat("MM/dd/yyyy").format(checkOutDate);  //dd MMM
		                 String mbcheckIn2 = new SimpleDateFormat("MMM dd E").format(checkInDate);
		                 String mbcheckOut2 = new SimpleDateFormat("MMM dd E").format(checkOutDate);
		                 request.setAttribute("checkIn",checkIn);
		                 request.setAttribute("checkOut",checkOut);
		                 request.setAttribute("checkIn1",checkIn1);
		                 request.setAttribute("checkOut1",checkOut1);
		                 request.setAttribute("checkIn2",checkIn2);
		                 request.setAttribute("checkOut2",checkOut2);
		                 request.setAttribute("mbcheckIn",mbcheckIn);
		                 request.setAttribute("mbcheckOut",mbcheckOut);
		                 request.setAttribute("mbcheckIn1",mbcheckIn1);
		                 request.setAttribute("mbcheckOut1",mbcheckOut1);
		                 request.setAttribute("mbcheckIn2",mbcheckIn2);
		                 request.setAttribute("mbcheckOut2",mbcheckOut2);

		                 UserCookies searchValue=new UserCookies();
		                 searchValue.setIsActive(true);
		                 searchValue.setIsDeleted(false);
		                 SimpleDateFormat formatdate=new SimpleDateFormat("yyyy-MM-dd");
		                 String strSearchStartDate=new SimpleDateFormat("yyyy-MM-dd").format(checkInDate.getTime());
		                 String strSearchEndDate=new SimpleDateFormat("yyyy-MM-dd").format(checkOutDate.getTime());
		                 java.util.Date dteSearchStart = formatdate.parse(strSearchStartDate);
					     java.util.Date dteSearchEnd = formatdate.parse(strSearchEndDate);
		                 searchValue.setUserStartDate(new Timestamp(dteSearchStart.getTime()));
		                 searchValue.setUserEndDate(new Timestamp(dteSearchEnd.getTime()));
		                 
		                 java.util.Date date1=new java.util.Date();
			             Calendar calDate=Calendar.getInstance();
			             calDate.setTime(date1);
			             calDate.setTimeZone(TimeZone.getTimeZone("IST"));
			             
		                 java.util.Date currentDate=calDate.getTime();
		                 searchValue.setUserSearchDate(new Timestamp(currentDate.getTime()));
		                 String strCurrentDate=formatdate.format(currentDate);
		                 java.util.Date curdate=formatdate.parse(strCurrentDate);
		                 searchValue.setSearchableDate(new Timestamp(curdate.getTime()));
		                 
		                 searchValue.setIsCookiesEnable(false);
		                 searchValue.setIsSearchEnable(true);
		                 searchValue.setSearchUserId(null);
		                 searchValue.setUserSearchId(location.getLocationId());
		                 searchValue.setUserSearchValue(location.getLocationName());
		                 searchValue.setUserIpAddress(IP.getHostAddress());
		                 cookiesController.add(searchValue);
		                 
					 }else{
						 java.util.Date date=new java.util.Date();
						 Calendar today=Calendar.getInstance();
						 today.setTime(date);
						 today.setTimeZone(TimeZone.getTimeZone("IST"));
						   
							
						 Calendar calCheckStart=Calendar.getInstance();
						 calCheckStart.setTime(date);
					     calCheckStart.setTimeZone(TimeZone.getTimeZone("IST"));
					     java.util.Date checkInDate = calCheckStart.getTime();
					    
					     Calendar calCheckEnd=Calendar.getInstance();
					     calCheckEnd.setTime(date);
					     calCheckEnd.add(Calendar.DATE, 1); 
					     calCheckEnd.setTimeZone(TimeZone.getTimeZone("IST"));
					     java.util.Date checkOutDate = calCheckEnd.getTime();
						
					     String checkIn = new SimpleDateFormat("MMM d,yyyy").format(checkInDate);
		                 String checkOut = new SimpleDateFormat("MMM d,yyyy").format(checkOutDate);
		                 String checkIn1 = new SimpleDateFormat("MM/dd/yyyy").format(checkInDate); //dd MMM
		                 String checkOut1 = new SimpleDateFormat("MM/dd/yyyy").format(checkOutDate);  //dd MMM
		                 String checkIn2 = new SimpleDateFormat("MMM dd EEEEE").format(checkInDate);
		                 String checkOut2 = new SimpleDateFormat("MMM dd EEEEE").format(checkOutDate);
		                 String mbcheckIn = new SimpleDateFormat("MMM d,yyyy").format(checkInDate);
		                 String mbcheckOut = new SimpleDateFormat("MMM d,yyyy").format(checkOutDate);
		                 String mbcheckIn1 = new SimpleDateFormat("MM/dd/yyyy").format(checkInDate); //dd MMM
		                 String mbcheckOut1 = new SimpleDateFormat("MM/dd/yyyy").format(checkOutDate);  //dd MMM
		                 String mbcheckIn2 = new SimpleDateFormat("MMM dd E").format(checkInDate);
		                 String mbcheckOut2 = new SimpleDateFormat("MMM dd E").format(checkOutDate);
		                 request.setAttribute("checkIn",checkIn);
		                 request.setAttribute("checkOut",checkOut);
		                 request.setAttribute("checkIn1",checkIn1);
		                 request.setAttribute("checkOut1",checkOut1);
		                 request.setAttribute("checkIn2",checkIn2);
		                 request.setAttribute("checkOut2",checkOut2);
		                 request.setAttribute("mbcheckIn",mbcheckIn);
		                 request.setAttribute("mbcheckOut",mbcheckOut);
		                 request.setAttribute("mbcheckIn1",mbcheckIn1);
		                 request.setAttribute("mbcheckOut1",mbcheckOut1);
		                 request.setAttribute("mbcheckIn2",mbcheckIn2);
		                 request.setAttribute("mbcheckOut2",mbcheckOut2);
		                 
		                 UserCookies searchValue=new UserCookies();
		                 searchValue.setIsActive(true);
		                 searchValue.setIsDeleted(false);
		                 SimpleDateFormat formatdate=new SimpleDateFormat("yyyy-MM-dd");
		                 String strSearchStartDate=new SimpleDateFormat("yyyy-MM-dd").format(checkInDate.getTime());
		                 String strSearchEndDate=new SimpleDateFormat("yyyy-MM-dd").format(checkOutDate.getTime());
		                 java.util.Date dteSearchStart = formatdate.parse(strSearchStartDate);
					     java.util.Date dteSearchEnd = formatdate.parse(strSearchEndDate);
		                 searchValue.setUserStartDate(new Timestamp(dteSearchStart.getTime()));
		                 searchValue.setUserEndDate(new Timestamp(dteSearchEnd.getTime()));
		                 
		                 java.util.Date date1=new java.util.Date();
			             Calendar calDate=Calendar.getInstance();
			             calDate.setTime(date1);
			             calDate.setTimeZone(TimeZone.getTimeZone("IST"));
			             
		                 java.util.Date currentDate=calDate.getTime();
		                 searchValue.setUserSearchDate(new Timestamp(currentDate.getTime()));
		                 String strCurrentDate=formatdate.format(currentDate);
		                 java.util.Date curdate=formatdate.parse(strCurrentDate);
		                 searchValue.setSearchableDate(new Timestamp(curdate.getTime()));
		                 
		                 searchValue.setIsCookiesEnable(false);
		                 searchValue.setIsSearchEnable(true);
		                 searchValue.setSearchUserId(null);
		                 searchValue.setUserSearchId(location.getLocationId());
		                 searchValue.setUserSearchValue(location.getLocationName());
		                 searchValue.setUserIpAddress(IP.getHostAddress());
		                 cookiesController.add(searchValue);
					 }
					
					 strReturn="location";
				}
			}
			
			/*List<PmsProperty> listInActivePropertyUrl=propertyController.listInActivePropertyUrl(strUrlName.trim());
			if(listInActivePropertyUrl.size()>0 && !listInActivePropertyUrl.isEmpty()){
				for(PmsProperty pmsproperty:listInActivePropertyUrl){
					Integer locationId=pmsproperty.getLocation().getLocationId();
					 request.setAttribute("locationId" ,locationId);

					 if(request.getParameter("locationId") != null && request.getParameter("locationId").toString() !=""){
						 this.setLocationId(Integer.parseInt(request.getParameter("locationId").toString()));
					 }else{
						 this.setLocationId(locationId);
					 }
					 
					 SeoContent seoContent = seoContentController.findSeoContent(locationId);	
					 request.setAttribute("metaTag" ,seoContent.getTitle());
					 request.setAttribute("description" ,seoContent.getDescription());
					 request.setAttribute("content" ,seoContent.getContent());	
					 Location locations=locationController.find(locationId);
					 String strLocationUrl=null; 
					 strLocationUrl=locations.getLocationUrl();
					 request.setAttribute("locationName" ,locations.getLocationDescription()==null?"":locations.getLocationDescription().toUpperCase().trim());	
					 
					 if(strStartDate!=null && strStartDate!="" && strEndDate!=null && strEndDate!=""){
						 DateFormat format = new SimpleDateFormat("MMM d,yyyy", Locale.ENGLISH);
					     java.util.Date dateStart = format.parse(strStartDate);
					     java.util.Date dateEnd = format.parse(strEndDate);

					     Calendar calCheckStart=Calendar.getInstance();
					     calCheckStart.setTime(dateStart);
					     java.util.Date checkInDate = calCheckStart.getTime();
					    
					     Calendar calCheckEnd=Calendar.getInstance();
					     calCheckEnd.setTime(dateEnd);
					     java.util.Date checkOutDate = calCheckEnd.getTime();
						
						 String checkIn = new SimpleDateFormat("MMM d,yyyy").format(checkInDate);
		                 String checkOut = new SimpleDateFormat("MMM d,yyyy").format(checkOutDate);
		                 String checkIn1 = new SimpleDateFormat("dd MMM").format(checkInDate);
		                 String checkOut1 = new SimpleDateFormat("dd MMM").format(checkOutDate);
		                 
		                 request.setAttribute("checkIn",checkIn);
		                 request.setAttribute("checkOut",checkOut);
		                 request.setAttribute("checkIn1",checkIn1);
		                 request.setAttribute("checkOut1",checkOut1);
		                 
		                 
					 }else{
						 java.util.Date date=new java.util.Date();
						 Calendar today=Calendar.getInstance();
						 today.setTime(date);
						 today.setTimeZone(TimeZone.getTimeZone("IST"));
						   
							
						 Calendar calCheckStart=Calendar.getInstance();
						 calCheckStart.setTime(date);
					     calCheckStart.setTimeZone(TimeZone.getTimeZone("IST"));
					     java.util.Date checkInDate = calCheckStart.getTime();
					    
					     Calendar calCheckEnd=Calendar.getInstance();
					     calCheckEnd.setTime(date);
					     calCheckEnd.add(Calendar.DATE, 1); 
					     calCheckEnd.setTimeZone(TimeZone.getTimeZone("IST"));
					     java.util.Date checkOutDate = calCheckEnd.getTime();
						
						 String checkIn = new SimpleDateFormat("MMM d,yyyy").format(checkInDate);
			             String checkOut = new SimpleDateFormat("MMM d,yyyy").format(checkOutDate);
			             String checkIn1 = new SimpleDateFormat("dd MMM").format(checkInDate);
			             String checkOut1 = new SimpleDateFormat("dd MMM").format(checkOutDate);
			                 
		                 request.setAttribute("checkIn",checkIn);
		                 request.setAttribute("checkOut",checkOut);
		                 request.setAttribute("checkIn1",checkIn1);
		                 request.setAttribute("checkOut1",checkOut1);
					 }
					 response.sendRedirect(strLocationUrl);
					 strReturn="location";
					 
				}
			}*/
			
			
			List<PmsProperty> listPropertyUrl=propertyController.listPropertyUrl(strUrlName.trim());
			if(listPropertyUrl.size()>0 && !listPropertyUrl.isEmpty()){
				for(PmsProperty property:listPropertyUrl){
	                 
					request.setAttribute("urlPropertyId" ,property.getPropertyId());
					sessionMap.put("breadCrumPropertyName", property.getPropertyName());
					
					if(property.getArea()!=null){	
					       sessionMap.put("breadCrumAreaName", property.getArea().getAreaName().toUpperCase());
					       sessionMap.put("breadCrumAreaUrl", property.getArea().getAreaUrl());
					}else if(property.getLocation()!=null){		
							sessionMap.put("breadCrumAreaName", property.getLocation().getLocationName().toUpperCase());
							sessionMap.put("breadCrumAreaUrl", property.getLocation().getLocationUrl());
					}
					
					//sessionMap.put("breadCrumAreaName", property.getArea().getAreaName().toUpperCase());
//					sessionMap.put("uloPropertyId", property.getPropertyId()); 
					/*SeoContent seoContent = seoContentController.findId1(property.getPropertyId());
				    request.setAttribute("metaTag" ,seoContent.getTitle());
					request.setAttribute("description" ,seoContent.getDescription());
					request.setAttribute("content" ,seoContent.getContent());*/
					List<SeoContent> listSeoProperty=seoContentController.list1(property.getPropertyId());
					if(listSeoProperty.size()>0 && !listSeoProperty.isEmpty()){
						SeoContent seoContent = seoContentController.findId1(property.getPropertyId());
					    request.setAttribute("metaTag" ,seoContent.getTitle());
						request.setAttribute("description" ,seoContent.getDescription());
						request.setAttribute("content" ,seoContent.getContent());
					}else{
						request.setAttribute("metaTag" ,"<title>Ulohotels</title>");
						request.setAttribute("description" ,"<p></p>");
						request.setAttribute("content" ,"<p></p>");	
					}
					
					if(property.getPropertyId()!=null && property.getPropertyId()>0)
			        {
				        PmsProperty pmsproperty = propertyController.find(property.getPropertyId());
				        this.setLatitude(pmsproperty.getLatitude().trim());
				        this.setLongitude(pmsproperty.getLongitude().trim());
				        request.setAttribute("placeId" ,pmsproperty.getPlaceId());
//					    request.setAttribute("hotelPolicy" ,pmsproperty.getPropertyHotelPolicy());
				        String strHotelPolicy=null,strStandardPolicy=null,strCancellationPolicy=null,strChildPolicy=null;
				        
				        strHotelPolicy=pmsproperty.getPropertyHotelPolicy();
						strStandardPolicy=pmsproperty.getPropertyStandardPolicy();
						strCancellationPolicy=pmsproperty.getPropertyCancellationPolicy();
						strChildPolicy=pmsproperty.getPropertyChildPolicy();
						String refundPolicy,changedRefundPolicy,cancelledPolicy,changedCancelledPolicy,hotelPolicy,changedHotelPolicy;
						
						if(strHotelPolicy!=null){
							hotelPolicy = strHotelPolicy.replace("\"", "\\\"");
							changedHotelPolicy = hotelPolicy.replaceAll("\\s+", " ");
						}else{
							changedHotelPolicy="";
						}
						
						if(strStandardPolicy!=null){
							refundPolicy = strStandardPolicy.replace("\"", "\\\"");
							changedRefundPolicy = refundPolicy.replaceAll("\\s+", " ");
						}else{
							changedRefundPolicy ="";
							
						}
						
						if(strCancellationPolicy!=null){
							cancelledPolicy = strCancellationPolicy.replace("\"", "\\\"");
							changedCancelledPolicy = cancelledPolicy.replaceAll("\\s+", " ");
						}else{
							changedCancelledPolicy="";
						}
						

				        request.setAttribute("hotelPolicy" ,changedHotelPolicy);
					    request.setAttribute("cancellationPolicy" ,changedCancelledPolicy);
					    request.setAttribute("refundPolicy" ,changedRefundPolicy);
			        }
					
					if(strStartDate!=null && strStartDate!="" && strEndDate!=null && strEndDate!=""){
						 DateFormat format = new SimpleDateFormat("MMM d,yyyy", Locale.ENGLISH);
					     java.util.Date dateStart = format.parse(strStartDate);
					     java.util.Date dateEnd = format.parse(strEndDate);

					     Calendar calCheckStart=Calendar.getInstance();
					     calCheckStart.setTime(dateStart);
					     java.util.Date checkInDate = calCheckStart.getTime();
					    
					     Calendar calCheckEnd=Calendar.getInstance();
					     calCheckEnd.setTime(dateEnd);
					     java.util.Date checkOutDate = calCheckEnd.getTime();
						
					     String checkIn = new SimpleDateFormat("MMM d,yyyy").format(checkInDate);
		                 String checkOut = new SimpleDateFormat("MMM d,yyyy").format(checkOutDate);
		                 String checkIn1 = new SimpleDateFormat("MM/dd/yyyy").format(checkInDate); //dd MMM
		                 String checkOut1 = new SimpleDateFormat("MM/dd/yyyy").format(checkOutDate);  //dd MMM
		                 String checkIn2 = new SimpleDateFormat("MMM dd EEEEE").format(checkInDate);
		                 String checkOut2 = new SimpleDateFormat("MMM dd EEEEE").format(checkOutDate);
		                 String mbcheckIn = new SimpleDateFormat("MMM d,yyyy").format(checkInDate);
		                 String mbcheckOut = new SimpleDateFormat("MMM d,yyyy").format(checkOutDate);
		                 String mbcheckIn1 = new SimpleDateFormat("MM/dd/yyyy").format(checkInDate); //dd MMM
		                 String mbcheckOut1 = new SimpleDateFormat("MM/dd/yyyy").format(checkOutDate);  //dd MMM
		                 String mbcheckIn2 = new SimpleDateFormat("MMM dd E").format(checkInDate);
		                 String mbcheckOut2 = new SimpleDateFormat("MMM dd E").format(checkOutDate);
		                 request.setAttribute("checkIn",checkIn);
		                 request.setAttribute("checkOut",checkOut);
		                 request.setAttribute("checkIn1",checkIn1);
		                 request.setAttribute("checkOut1",checkOut1);
		                 request.setAttribute("checkIn2",checkIn2);
		                 request.setAttribute("checkOut2",checkOut2);
		                 request.setAttribute("mbcheckIn",mbcheckIn);
		                 request.setAttribute("mbcheckOut",mbcheckOut);
		                 request.setAttribute("mbcheckIn1",mbcheckIn1);
		                 request.setAttribute("mbcheckOut1",mbcheckOut1);
		                 request.setAttribute("mbcheckIn2",mbcheckIn2);
		                 request.setAttribute("mbcheckOut2",mbcheckOut2);

		                 	UserCookies searchValue=new UserCookies();
			                searchValue.setIsActive(true);
			                searchValue.setIsDeleted(false);
			                SimpleDateFormat formatdate=new SimpleDateFormat("yyyy-MM-dd");
			                String strSearchStartDate=new SimpleDateFormat("yyyy-MM-dd").format(checkInDate.getTime());
			                String strSearchEndDate=new SimpleDateFormat("yyyy-MM-dd").format(checkOutDate.getTime());
			                java.util.Date dteSearchStart = formatdate.parse(strSearchStartDate);
						    java.util.Date dteSearchEnd = formatdate.parse(strSearchEndDate);
			                searchValue.setUserStartDate(new Timestamp(dteSearchStart.getTime()));
			                searchValue.setUserEndDate(new Timestamp(dteSearchEnd.getTime()));
			                 
			                java.util.Date date1=new java.util.Date();
				            Calendar calDate=Calendar.getInstance();
				            calDate.setTime(date1);
				            calDate.setTimeZone(TimeZone.getTimeZone("IST"));
				             
			                java.util.Date currentDate=calDate.getTime();
			                searchValue.setUserSearchDate(new Timestamp(currentDate.getTime()));
			                String strCurrentDate=formatdate.format(currentDate);
			                java.util.Date curdate=formatdate.parse(strCurrentDate);
			                searchValue.setSearchableDate(new Timestamp(curdate.getTime()));
			                
			                searchValue.setIsCookiesEnable(false);
			                searchValue.setIsSearchEnable(true);
			                searchValue.setSearchUserId(null);
			                searchValue.setUserSearchId(property.getPropertyId());
			                searchValue.setUserSearchValue(property.getPropertyName());
			                searchValue.setUserIpAddress(IP.getHostAddress());
			                cookiesController.add(searchValue);
			                
					 }else{
						 java.util.Date date=new java.util.Date();
						 Calendar today=Calendar.getInstance();
						 today.setTime(date);
						 today.setTimeZone(TimeZone.getTimeZone("IST"));
						   
							
						 Calendar calCheckStart=Calendar.getInstance();
						 calCheckStart.setTime(date);
					     calCheckStart.setTimeZone(TimeZone.getTimeZone("IST"));
					     java.util.Date checkInDate = calCheckStart.getTime();
					    
					     Calendar calCheckEnd=Calendar.getInstance();
					     calCheckEnd.setTime(date);
					     calCheckEnd.add(Calendar.DATE, 1); 
					     calCheckEnd.setTimeZone(TimeZone.getTimeZone("IST"));
					     java.util.Date checkOutDate = calCheckEnd.getTime();
						
					     String checkIn = new SimpleDateFormat("MMM d,yyyy").format(checkInDate);
		                 String checkOut = new SimpleDateFormat("MMM d,yyyy").format(checkOutDate);
		                 String checkIn1 = new SimpleDateFormat("MM/dd/yyyy").format(checkInDate); //dd MMM
		                 String checkOut1 = new SimpleDateFormat("MM/dd/yyyy").format(checkOutDate);  //dd MMM
		                 String checkIn2 = new SimpleDateFormat("MMM dd EEEEE").format(checkInDate);
		                 String checkOut2 = new SimpleDateFormat("MMM dd EEEEE").format(checkOutDate);
		                 String mbcheckIn = new SimpleDateFormat("MMM d,yyyy").format(checkInDate);
		                 String mbcheckOut = new SimpleDateFormat("MMM d,yyyy").format(checkOutDate);
		                 String mbcheckIn1 = new SimpleDateFormat("MM/dd/yyyy").format(checkInDate); //dd MMM
		                 String mbcheckOut1 = new SimpleDateFormat("MM/dd/yyyy").format(checkOutDate);  //dd MMM
		                 String mbcheckIn2 = new SimpleDateFormat("MMM dd E").format(checkInDate);
		                 String mbcheckOut2 = new SimpleDateFormat("MMM dd E").format(checkOutDate);
		                 request.setAttribute("checkIn",checkIn);
		                 request.setAttribute("checkOut",checkOut);
		                 request.setAttribute("checkIn1",checkIn1);
		                 request.setAttribute("checkOut1",checkOut1);
		                 request.setAttribute("checkIn2",checkIn2);
		                 request.setAttribute("checkOut2",checkOut2);
		                 request.setAttribute("mbcheckIn",mbcheckIn);
		                 request.setAttribute("mbcheckOut",mbcheckOut);
		                 request.setAttribute("mbcheckIn1",mbcheckIn1);
		                 request.setAttribute("mbcheckOut1",mbcheckOut1);
		                 request.setAttribute("mbcheckIn2",mbcheckIn2);
		                 request.setAttribute("mbcheckOut2",mbcheckOut2);
		                 
		                 UserCookies searchValue=new UserCookies();
		                 searchValue.setIsActive(true);
		                 searchValue.setIsDeleted(false);
		                 SimpleDateFormat formatdate=new SimpleDateFormat("yyyy-MM-dd");
		                 String strSearchStartDate=new SimpleDateFormat("yyyy-MM-dd").format(checkInDate.getTime());
		                 String strSearchEndDate=new SimpleDateFormat("yyyy-MM-dd").format(checkOutDate.getTime());
		                 java.util.Date dteSearchStart = formatdate.parse(strSearchStartDate);
					     java.util.Date dteSearchEnd = formatdate.parse(strSearchEndDate);
		                 searchValue.setUserStartDate(new Timestamp(dteSearchStart.getTime()));
		                 searchValue.setUserEndDate(new Timestamp(dteSearchEnd.getTime()));
		                 
		                 java.util.Date date1=new java.util.Date();
			             Calendar calDate=Calendar.getInstance();
			             calDate.setTime(date1);
			             calDate.setTimeZone(TimeZone.getTimeZone("IST"));
			             
		                 java.util.Date currentDate=calDate.getTime();
		                 searchValue.setUserSearchDate(new Timestamp(currentDate.getTime()));
		                 String strCurrentDate=formatdate.format(currentDate);
		                 java.util.Date curdate=formatdate.parse(strCurrentDate);
		                 searchValue.setSearchableDate(new Timestamp(curdate.getTime()));
		                 searchValue.setIsCookiesEnable(false);
		                 searchValue.setIsSearchEnable(true);
		                 searchValue.setSearchUserId(null);
		                 searchValue.setUserSearchId(property.getPropertyId());
		                 searchValue.setUserSearchValue(property.getPropertyName());
		                 searchValue.setUserIpAddress(IP.getHostAddress());
		                 cookiesController.add(searchValue);
					 }
					
					strReturn="property";
				}
			}
			
			List<Area> listAreaUrl=areaController.listAreaUrl(strUrlName.trim());
			if(listAreaUrl.size()>0 && !listAreaUrl.isEmpty()){
				
				  for(Area area:listAreaUrl){
					 request.setAttribute("areaId" ,area.getAreaId());
					 request.setAttribute("areaLocationId" ,area.getLocation().getLocationId());
//					sessionMap.put("areaLocationId", area.getLocation().getLocationId());
					request.setAttribute("uloAreaId" ,area.getAreaId());
					sessionMap.put("breadCrumAreaName", area.getAreaName());
					request.setAttribute("locationName" ,area.getAreaDescription()==null?"":area.getAreaDescription().toUpperCase().trim());
//					sessionMap.put("uloAreaId", area.getAreaId());
					this.setAreaId(area.getAreaId());
					
		  	  	  	SeoContent seoContent = seoContentController.findId(area.getAreaId());	
		  	  	  	request.setAttribute("metaTag" ,seoContent.getTitle() == null ? "": seoContent.getTitle());
		  	  	  	request.setAttribute("description" ,seoContent.getDescription()  == null ? "": seoContent.getDescription());
		  	  	  	request.setAttribute("content" ,seoContent.getContent() == null ? "": seoContent.getContent());
		  	  	  	
		  	  	 if(strStartDate!=null && strStartDate!="" && strEndDate!=null && strEndDate!=""){
					 DateFormat format = new SimpleDateFormat("MMM d,yyyy", Locale.ENGLISH);
				     java.util.Date dateStart = format.parse(strStartDate);
				     java.util.Date dateEnd = format.parse(strEndDate);

				     Calendar calCheckStart=Calendar.getInstance();
				     calCheckStart.setTime(dateStart);
				     java.util.Date checkInDate = calCheckStart.getTime();
				    
				     Calendar calCheckEnd=Calendar.getInstance();
				     calCheckEnd.setTime(dateEnd);
				     java.util.Date checkOutDate = calCheckEnd.getTime();
					
					 String checkIn = new SimpleDateFormat("MMM d,yyyy").format(checkInDate);
	                 String checkOut = new SimpleDateFormat("MMM d,yyyy").format(checkOutDate);
	                 String checkIn1 = new SimpleDateFormat("MM/dd/yyyy").format(checkInDate); //dd MMM
	                 String checkOut1 = new SimpleDateFormat("MM/dd/yyyy").format(checkOutDate);  //dd MMM
	                 String checkIn2 = new SimpleDateFormat("MMM dd EEEEE").format(checkInDate);
	                 String checkOut2 = new SimpleDateFormat("MMM dd EEEEE").format(checkOutDate);
	                 String mbcheckIn = new SimpleDateFormat("MMM d,yyyy").format(checkInDate);
	                 String mbcheckOut = new SimpleDateFormat("MMM d,yyyy").format(checkOutDate);
	                 String mbcheckIn1 = new SimpleDateFormat("MM/dd/yyyy").format(checkInDate); //dd MMM
	                 String mbcheckOut1 = new SimpleDateFormat("MM/dd/yyyy").format(checkOutDate);  //dd MMM
	                 String mbcheckIn2 = new SimpleDateFormat("MMM dd E").format(checkInDate);
	                 String mbcheckOut2 = new SimpleDateFormat("MMM dd E").format(checkOutDate);
	                 request.setAttribute("checkIn",checkIn);
	                 request.setAttribute("checkOut",checkOut);
	                 request.setAttribute("checkIn1",checkIn1);
	                 request.setAttribute("checkOut1",checkOut1);
	                 request.setAttribute("checkIn2",checkIn2);
	                 request.setAttribute("checkOut2",checkOut2);
	                 request.setAttribute("mbcheckIn",mbcheckIn);
	                 request.setAttribute("mbcheckOut",mbcheckOut);
	                 request.setAttribute("mbcheckIn1",mbcheckIn1);
	                 request.setAttribute("mbcheckOut1",mbcheckOut1);
	                 request.setAttribute("mbcheckIn2",mbcheckIn2);
	                 request.setAttribute("mbcheckOut2",mbcheckOut2);
	                 
	                 UserCookies searchValue=new UserCookies();
	                 searchValue.setIsActive(true);
	                 searchValue.setIsDeleted(false);
	                 SimpleDateFormat formatdate=new SimpleDateFormat("yyyy-MM-dd");
	                 String strSearchStartDate=new SimpleDateFormat("yyyy-MM-dd").format(checkInDate.getTime());
	                 String strSearchEndDate=new SimpleDateFormat("yyyy-MM-dd").format(checkOutDate.getTime());
	                 java.util.Date dteSearchStart = formatdate.parse(strSearchStartDate);
				     java.util.Date dteSearchEnd = formatdate.parse(strSearchEndDate);
	                 searchValue.setUserStartDate(new Timestamp(dteSearchStart.getTime()));
	                 searchValue.setUserEndDate(new Timestamp(dteSearchEnd.getTime()));
	                 java.util.Date date1=new java.util.Date();
		             Calendar calDate=Calendar.getInstance();
		             calDate.setTime(date1);
		             calDate.setTimeZone(TimeZone.getTimeZone("IST"));
		             
	                 java.util.Date currentDate=calDate.getTime();
	                 searchValue.setUserSearchDate(new Timestamp(currentDate.getTime()));
	                 String strCurrentDate=formatdate.format(currentDate);
	                 java.util.Date curdate=formatdate.parse(strCurrentDate);
	                 searchValue.setSearchableDate(new Timestamp(curdate.getTime()));
	                 searchValue.setIsCookiesEnable(false);
	                 searchValue.setIsSearchEnable(true);
	                 searchValue.setSearchUserId(null);
	                 searchValue.setUserSearchId(area.getAreaId());
	                 searchValue.setUserSearchValue(area.getAreaName());
	                 searchValue.setUserIpAddress(IP.getHostAddress());
	                 cookiesController.add(searchValue);
	                 
				 }else{
					 java.util.Date date=new java.util.Date();
					 Calendar today=Calendar.getInstance();
					 today.setTime(date);
					 today.setTimeZone(TimeZone.getTimeZone("IST"));
					   
						
					 Calendar calCheckStart=Calendar.getInstance();
					 calCheckStart.setTime(date);
				     calCheckStart.setTimeZone(TimeZone.getTimeZone("IST"));
				     java.util.Date checkInDate = calCheckStart.getTime();
				    
				     Calendar calCheckEnd=Calendar.getInstance();
				     calCheckEnd.setTime(date);
				     calCheckEnd.add(Calendar.DATE, 1); 
				     calCheckEnd.setTimeZone(TimeZone.getTimeZone("IST"));
				     java.util.Date checkOutDate = calCheckEnd.getTime();
					
				     String checkIn = new SimpleDateFormat("MMM d,yyyy").format(checkInDate);
	                 String checkOut = new SimpleDateFormat("MMM d,yyyy").format(checkOutDate);
	                 String checkIn1 = new SimpleDateFormat("MM/dd/yyyy").format(checkInDate); //dd MMM
	                 String checkOut1 = new SimpleDateFormat("MM/dd/yyyy").format(checkOutDate);  //dd MMM
	                 String checkIn2 = new SimpleDateFormat("MMM dd EEEEE").format(checkInDate);
	                 String checkOut2 = new SimpleDateFormat("MMM dd EEEEE").format(checkOutDate);
	                 String mbcheckIn = new SimpleDateFormat("MMM d,yyyy").format(checkInDate);
	                 String mbcheckOut = new SimpleDateFormat("MMM d,yyyy").format(checkOutDate);
	                 String mbcheckIn1 = new SimpleDateFormat("MM/dd/yyyy").format(checkInDate); //dd MMM
	                 String mbcheckOut1 = new SimpleDateFormat("MM/dd/yyyy").format(checkOutDate);  //dd MMM
	                 String mbcheckIn2 = new SimpleDateFormat("MMM dd E").format(checkInDate);
	                 String mbcheckOut2 = new SimpleDateFormat("MMM dd E").format(checkOutDate);
	                 request.setAttribute("checkIn",checkIn);
	                 request.setAttribute("checkOut",checkOut);
	                 request.setAttribute("checkIn1",checkIn1);
	                 request.setAttribute("checkOut1",checkOut1);
	                 request.setAttribute("checkIn2",checkIn2);
	                 request.setAttribute("checkOut2",checkOut2);
	                 request.setAttribute("mbcheckIn",mbcheckIn);
	                 request.setAttribute("mbcheckOut",mbcheckOut);
	                 request.setAttribute("mbcheckIn1",mbcheckIn1);
	                 request.setAttribute("mbcheckOut1",mbcheckOut1);
	                 request.setAttribute("mbcheckIn2",mbcheckIn2);
	                 request.setAttribute("mbcheckOut2",mbcheckOut2);

	                 UserCookies searchValue=new UserCookies();
	                 searchValue.setIsActive(true);
	                 searchValue.setIsDeleted(false);
	                 SimpleDateFormat formatdate=new SimpleDateFormat("yyyy-MM-dd");
	                 String strSearchStartDate=new SimpleDateFormat("yyyy-MM-dd").format(checkInDate.getTime());
	                 String strSearchEndDate=new SimpleDateFormat("yyyy-MM-dd").format(checkOutDate.getTime());
	                 java.util.Date dteSearchStart = formatdate.parse(strSearchStartDate);
				     java.util.Date dteSearchEnd = formatdate.parse(strSearchEndDate);
	                 searchValue.setUserStartDate(new Timestamp(dteSearchStart.getTime()));
	                 searchValue.setUserEndDate(new Timestamp(dteSearchEnd.getTime()));
	                 
	                 java.util.Date date1=new java.util.Date();
		             Calendar calDate=Calendar.getInstance();
		             calDate.setTime(date1);
		             calDate.setTimeZone(TimeZone.getTimeZone("IST"));
		             
	                 java.util.Date currentDate=calDate.getTime();
	                 searchValue.setUserSearchDate(new Timestamp(currentDate.getTime()));
	                 String strCurrentDate=formatdate.format(currentDate);
	                 java.util.Date curdate=formatdate.parse(strCurrentDate);
	                 searchValue.setSearchableDate(new Timestamp(curdate.getTime()));
	                 searchValue.setIsCookiesEnable(false);
	                 searchValue.setIsSearchEnable(true);
	                 searchValue.setSearchUserId(null);
	                 searchValue.setUserSearchId(area.getAreaId());
	                 searchValue.setUserSearchValue(area.getAreaName());
	                 searchValue.setUserIpAddress(IP.getHostAddress());
	                 cookiesController.add(searchValue);
				 }
		  	  	 
					strReturn="area";
				}
			}
			else if(listAreaUrl.size() == 0 && listAreaUrl.isEmpty()){
				
				
				List<Area> listInactiveAreaUrl=areaController.listInactiveAreaUrl(strUrlName.trim());
				for(Area area:listInactiveAreaUrl){
					request.setAttribute("redirectUrl" ,area.getRedirectUrl());
							  	  	 
					strReturn="inActiveArea";
				}
			}
			
			List<Area> listInactiveAreaUrl=areaController.listInactiveAreaUrl(strUrlName.trim());
			if(listInactiveAreaUrl.size()>0 && !listInactiveAreaUrl.isEmpty()){
				  for(Area area:listInactiveAreaUrl){
					request.setAttribute("redirectUrl" ,area.getRedirectUrl());
							  	  	 
					strReturn="inActiveArea";
				}
			}

			
			List<BlogArticles> listBlogArticles=articlesController.listBlogArticleUrl(strUrlName.trim());
			if(listBlogArticles.size()>0 && !listBlogArticles.isEmpty()){
				for(BlogArticles blogArticles: listBlogArticles){
					request.setAttribute("blogArticleId" ,blogArticles.getBlogArticleId());
					request.setAttribute("blogArticleTitle" ,blogArticles.getBlogArticleTitle());
					request.setAttribute("blogArticleDescription" ,blogArticles.getBlogArticleDescription());
					request.setAttribute("DummyId" ,"true");
					
					SeoContent seoContent = seoContentController.findSeoBlogArticleContent(blogArticles.getBlogArticleId());
					 if(seoContent!=null){
						 request.setAttribute("metaTag" ,seoContent.getTitle());
						 request.setAttribute("description" ,seoContent.getDescription());
						 request.setAttribute("content" ,seoContent.getContent());	
					 }else{
						 request.setAttribute("metaTag" ,"<title>Ulo Hotels Travel Blog</title>");
						 request.setAttribute("description" ,"");
						 request.setAttribute("content" ,"");	
					 }
					 
					strReturn="blogarticles";
				}
			}
			
			List<BlogCategories> listBlogCategories=blogCategoryController.listBlogCategoryUrl(strUrlName.trim());
			if(listBlogCategories.size()>0 && !listBlogCategories.isEmpty()){
				for(BlogCategories blogCategories: listBlogCategories){
					request.setAttribute("blogCategoryId" ,blogCategories.getBlogCategoryId());
					strReturn="blogcategories";
				}
			}
			
			List<BlogLocations> listBlogLocations=blogLocationController.listBlogLocationUrl(strUrlName.trim());
			if(listBlogLocations.size()>0 && !listBlogLocations.isEmpty()){
				for(BlogLocations blogLocations:listBlogLocations){
					request.setAttribute("blogLocationId" ,blogLocations.getBlogLocationId());
					strReturn="bloglocations";
				}
			}
			if(strUrlName.trim().equalsIgnoreCase(strSelectLocation)){
				strReturn="selectlocation";
			}
			
		}catch(Exception e){
			logger.error(e);
			e.printStackTrace();
		}
		
		return strReturn;
	}

	public String getSeoContentApi(Search search){
		String output=null;
		try{
			String url=null;
			GooglePlaceManager googleController=new GooglePlaceManager();
			SeoContentManager seoContentController=new SeoContentManager();
			PmsPropertyManager propertyController=new PmsPropertyManager();
			String jsonOutput = "";
			url=search.getUrl();
			if(url==null){
				url="NA";
			}
			int count=0;
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("application/json");
			List<GoogleLocation> listGoogleLocation=googleController.listLocationUrl(url);
			if(listGoogleLocation.size()>0 && !listGoogleLocation.isEmpty()){
				for(GoogleLocation location:listGoogleLocation){
					SeoContent seoContent = seoContentController.findSeoContent(location.getGoogleLocationId());	
		  	  	  	if(seoContent!=null){
		  	  	  	if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += ",{";
					else
						jsonOutput += "{";
					
				  	  	String strTrimContent="NA",strTrimTitle="NA",strTrimDescription="NA";
		  	  	  		if(seoContent.getTitle()!=null){
			  	  	  		String title =  seoContent.getTitle().trim();
							String strTitle = title.replace("\"", "\\\"" );
							strTrimTitle = strTitle.replaceAll("\\s+"," ");	
		  	  	  		}
		  	  	  		if(seoContent.getDescription()!=null){
			  	  	  		String description = seoContent.getDescription().trim();
							String strDescription = description.replace("\"", "\\\"" );
							strTrimDescription = strDescription.replaceAll("\\s+"," ");	
		  	  	  		}
		  	  	  		
		  	  	  		if(seoContent.getContent()!=null){
			  	  	  		String content = seoContent.getContent().trim();
							String strContent = content.replace("\"", "\\\"" );
							strTrimContent = strContent.replaceAll("\\s+"," ");
		  	  	  		}
					  	  	/*String title =  seoContent.getTitle().trim();
							String strTitle = title.replace("\"", "\\\"" );
							String strTrimTitle = strTitle.replaceAll("\\s+"," ");
							String description = seoContent.getDescription().trim();
							String strDescription = description.replace("\"", "\\\"" );
							String strTrimDescription = strDescription.replaceAll("\\s+"," ");
							String content = seoContent.getContent().trim();
							String strContent = content.replace("\"", "\\\"" );
							String strTrimContent = strContent.replaceAll("\\s+"," ");*/
					
						jsonOutput += "\"metaTag\":\"" + (strTrimTitle.trim())+ "\"";
						jsonOutput += ",\"description\":\"" + (strTrimDescription.trim())+ "\"";
						jsonOutput += ",\"content\":\"" + (strTrimContent.trim())+ "\"";
						
//							jsonOutput += "\"seoContent\":\"" + (strTrimTitle.trim()+ strTrimDescription.trim()+ strTrimContent.trim())+ "\"";
						jsonOutput += "}";
		  	  	  	count++;	
		  	  	  	}
			  	  	
				}
			}
			
			List<GoogleArea> listGoogleArea=googleController.listAreaUrl(url);
			if(listGoogleArea.size()>0 && !listGoogleArea.isEmpty()){
				for(GoogleArea area:listGoogleArea){
					SeoContent seoContent = seoContentController.findId(area.getGoogleAreaId());	
		  	  	  	if(seoContent!=null){
		  	  	  	if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += ",{";
					else
						jsonOutput += "{";
				  	  	String strTrimContent="NA",strTrimTitle="NA",strTrimDescription="NA";
		  	  	  		if(seoContent.getTitle()!=null){
			  	  	  		String title =  seoContent.getTitle().trim();
							String strTitle = title.replace("\"", "\\\"" );
							strTrimTitle = strTitle.replaceAll("\\s+"," ");	
		  	  	  		}
		  	  	  		if(seoContent.getDescription()!=null){
			  	  	  		String description = seoContent.getDescription().trim();
							String strDescription = description.replace("\"", "\\\"" );
							strTrimDescription = strDescription.replaceAll("\\s+"," ");	
		  	  	  		}
		  	  	  		
		  	  	  		if(seoContent.getContent()!=null){
			  	  	  		String content = seoContent.getContent().trim();
							String strContent = content.replace("\"", "\\\"" );
							strTrimContent = strContent.replaceAll("\\s+"," ");
		  	  	  		}
					  	  	/*String title =  seoContent.getTitle().trim();
							String strTitle = title.replace("\"", "\\\"" );
							String strTrimTitle = strTitle.replaceAll("\\s+"," ");
							String description = seoContent.getDescription().trim();
							String strDescription = description.replace("\"", "\\\"" );
							String strTrimDescription = strDescription.replaceAll("\\s+"," ");
							String content = seoContent.getContent().trim();
							String strContent = content.replace("\"", "\\\"" );
							String strTrimContent = strContent.replaceAll("\\s+"," ");*/
					
							jsonOutput += "\"metaTag\":\"" + (strTrimTitle.trim())+ "\"";
							jsonOutput += ",\"description\":\"" + (strTrimDescription.trim())+ "\"";
							jsonOutput += ",\"content\":\"" + (strTrimContent.trim())+ "\"";
						
							//jsonOutput += "\"seoContent\":\"" + (strTrimTitle.trim()+ strTrimDescription.trim()+ strTrimContent.trim())+ "\"";
				
						jsonOutput += "}";
		  	  	  	count++;	
		  	  	  	}
			  	  	
				}
			}
			
			List<PmsProperty> listProperty=propertyController.listPropertyUrl(url);
			if(listProperty.size()>0 && !listProperty.isEmpty()){
				for(PmsProperty property:listProperty){
					SeoContent seoContent = seoContentController.findId1(property.getPropertyId());	
		  	  	  	if(seoContent!=null){
		  	  	  	if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += ",{";
					else
						jsonOutput += "{";
					
		  	  	  		String strTrimContent="NA",strTrimTitle="NA",strTrimDescription="NA";
		  	  	  		if(seoContent.getTitle()!=null){
			  	  	  		String title =  seoContent.getTitle().trim();
							String strTitle = title.replace("\"", "\\\"" );
							strTrimTitle = strTitle.replaceAll("\\s+"," ");	
		  	  	  		}
		  	  	  		if(seoContent.getDescription()!=null){
			  	  	  		String description = seoContent.getDescription().trim();
							String strDescription = description.replace("\"", "\\\"" );
							strTrimDescription = strDescription.replaceAll("\\s+"," ");	
		  	  	  		}
		  	  	  		
		  	  	  		if(seoContent.getContent()!=null){
			  	  	  		String content = seoContent.getContent().trim();
							String strContent = content.replace("\"", "\\\"" );
							strTrimContent = strContent.replaceAll("\\s+"," ");
		  	  	  		}
				  	  	
						
						
			
						jsonOutput += "\"metaTag\":\"" + (strTrimTitle.trim())+ "\"";
						jsonOutput += ",\"description\":\"" + (strTrimDescription.trim())+ "\"";
						jsonOutput += ",\"content\":\"" + (strTrimContent.trim())+ "\"";
						
						//jsonOutput += "\"seoContent\":\"" + (strTrimTitle.trim()+ strTrimDescription.trim()+ strTrimContent.trim())+ "\"";
						jsonOutput += "}";
					
					
		  	  	  	count++;	
		  	  	  	}
			  	  	
				}
			}
			
			String data="\"data\":[" + jsonOutput + "]";
 			String strData=data.replaceAll("\"", "\"");
 			
 			if(count == 0){
 				output="{\"error\":\"1\",\"message\":\"No Records Found\"}";
			}else{
				output="{\"error\":\"0\"," + strData + "}";			
			}
			
 			
		}catch(Exception e){
			output="{\"error\":\"2\",\"message\":\"Some technical error on seo content api\"}";
			logger.error(e);	
			e.printStackTrace();
		}
		return output;
	}
	
	
	public String getSearchLocationIdByUrl(String url){

		String output="NA,NA";
		try{
			GooglePlaceManager googleController=new GooglePlaceManager();
			List<GoogleLocation> listGoogleLocation=googleController.listLocationUrl(url);
			if(listGoogleLocation.size()>0 && !listGoogleLocation.isEmpty()){
				for(GoogleLocation location:listGoogleLocation){
					output=String.valueOf(location.getGoogleLocationId())+",location";
				}
			}
			
			List<GoogleArea> listGoogleArea=googleController.listAreaUrl(url);
			if(listGoogleArea.size()>0 && !listGoogleArea.isEmpty()){
				for(GoogleArea area:listGoogleArea){
					output=String.valueOf(area.getGoogleAreaId())+",area";
				}
			}
			
		}catch(Exception e){
			logger.error(e);	
			e.printStackTrace();
		}
		return output;
	
	}
	
	public String getSearchPropertyIdByUrl(String url){
		String output=null;
		try{
			PmsPropertyManager propertyController=new PmsPropertyManager();
			List<PmsProperty> listProperty=propertyController.listPropertyUrl(url);
			if(listProperty.size()>0 && !listProperty.isEmpty()){
				for(PmsProperty pmsproperty:listProperty){
					output=String.valueOf(pmsproperty.getPropertyId());
				}
			}else{
				output="NA";
			}
		}catch(Exception e){
			logger.error(e);	
			e.printStackTrace();
		}
		return output;
	}
	
	public String getLocationUrl() {
		return locationUrl;
	}

	public void setLocationUrl(String locationUrl) {
		this.locationUrl = locationUrl;
	}

	public Integer getLocationUrlId() {
		return locationUrlId;
	}

	public void setLocationUrlId(Integer locationUrlId) {
		this.locationUrlId = locationUrlId;
	}
	
	public String getUrlType() {
		return urlType;
	}

	public void setUrlType(String urlType) {
		this.urlType = urlType;
	}
	
	

	public String getReturnType() {
		return returnType;
	}

	public void setReturnType(String returnType) {
		this.returnType = returnType;
	}
	
	public Integer getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}

	public Integer getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(Integer propertyId) {
		this.propertyId = propertyId;
	}
	
	public String getStartDate() {
		return StartDate;
	}

	public void setStartDate(String StartDate) {
		this.StartDate = StartDate;
	}

	public String getEndDate() {
		return EndDate;
	}

	public void setEndDate(String EndDate) {
		this.EndDate = EndDate;
	}
}
