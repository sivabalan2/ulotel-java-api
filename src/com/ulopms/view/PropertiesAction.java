package com.ulopms.view;

import java.io.IOException;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.catalina.startup.UserConfig;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;

import com.opensymphony.xwork2.ActionSupport;
import com.ulopms.controller.HelpTopicManager;
import com.ulopms.controller.SeoContentManager;
import com.ulopms.controller.UserLoginManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.HelpTopic;
import com.ulopms.model.SeoContent;
import com.ulopms.model.User;

import javax.servlet.http.HttpServletRequest;

public class PropertiesAction extends ActionSupport implements ServletRequestAware,
UserAware {

	private static final long serialVersionUID = 9149826260758390091L;

	private User user;
	
	private String metaTag;
	private String metaDescription;
	private String webContent;
	

	public String getMetaTag() {
		return metaTag;
	}

	public void setMetaTag(String metaTag) {
		this.metaTag = metaTag;
	}

	public String getMetaDescription() {
		return metaDescription;
	}

	public void setMetaDescription(String metaDescription) {
		this.metaDescription = metaDescription;
	}

	public String getWebContent() {
		return webContent;
	}

	public void setWebContent(String webContent) {
		this.webContent = webContent;
	}

	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}

	private HttpSession session;

	private static final Logger logger = Logger.getLogger(PropertiesAction.class);

	// @Override
	// public User getModel() {
	// return user;
	// }
	private int locationId;
	
	public int getLocationId() {
		return locationId;
	}

	public void setLocationId(int locationId) {
		this.locationId = locationId;
	}

	@Override
	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	private HttpServletRequest request;

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	public PropertiesAction() {

	}



	public String execute() {
		
		
		SeoContentManager seoContentController = new SeoContentManager();
	    SeoContent seoContent = seoContentController.findSeoContent(this.locationId);	
			
	        request.setAttribute("metaTag" ,seoContent.getTitle());
			request.setAttribute("description" ,seoContent.getDescription());
			request.setAttribute("content" ,seoContent.getContent());
		
		/* if(this.locationId== 1){
				//request.setAttribute("metaTag", " coorg entered");
				//this.metaTag = "Have a comfy stay at the best hotels in Coorg within your budget";
				this.setMetaTag("Have a comfy stay at the best hotels in Coorg within your budget");
				this.setMetaDescription("Explore the ambience of Coorg by staying at the best resorts in Coorg. Ulo Riviera The Spring Arc &amp; Ulo Alipinia Estate resort are budget friendly hotels that gives you the feel of homestay.");
				this.setWebContent("Coorg, also called as Kodagu is a hill station in Karnataka. It is known as the Scotland of India. The note worthy places in Coorg are Madikeri fort, Omkareshwara Temple, Raja�s tomb, Abbi Falls, Tala Cauvery, Gothic-style church and museum. The best time to visit Coorg is between October and March. It is a evergreen place with pleasant climate throughout the year. The Storm festival takes place every February. It is recommended to visit Coorg during summer and winter. It is well known for honey, handmade chocolates, coffee and spice plantations. The scenic charms allure many thrill seekers and nature lovers to this wonderful place. The beauty of this place mesmerizes many tourists to stay in Coorg for a long period of time. Coorg is best suited for family vacations. It is the perfect place for people who love trekking in this breathtaking valleys and mountains. It is a wonderful and safe adventure destination. You must have your travel documents all the time to avoid any inconvenience. You can visit Kushal Nagar Market and Friday Market to buy best quality coffee, spices, nuts and chocolates. You can spot the budget hotels in Coorg with tariff if you search it using the right website. The hotel booking in Coorg, Karnataka is made easier by online booking feature. The bugging question which comes to everyone is where to stay in Coorg?. Coorg has hotels or resorts to suit every traveller�s budget. Search for Coorg places to stay before planning your trip. It can be a boutique resort, budget friendly hotels or home stays and camps.If you plan a trip, the first step is to search for the good resorts in Coorg with all basic amenities. You can locate your ideal hotels and resorts in Coorg by browsing the internet. There are many hotels in Coorg for your stay. Check criteria like cost, travel, food, location, room service, etc., before you book your room. You can search the hotels in Coorg with price on any travel website. There are plenty of lodges in Coorg which are available at moderate prices. Coorg hotels and resorts rates depends upon the location and services provided. You can get the best resorts in Coorg if you are willing to spend some money. Before booking a hotel or resort, check whether it is near to the tourist spots. Nowadays, most of the resorts provide internet connections as it became a major part of our life.You can stay at any 3 star hotels in Coorg based on your budget. You can get many cottages in Coorg at reasonable price. There are many best hotels in Coorg which provides a escape for people from the stressful world. You can check the Coorg hotels price list any time by checking the websites. You have a handful of options when it comes to booking a best place to stay in Coorg. If you are booking online, then you can view all the resorts in Coorg with price. You can book it according to your budget by scrutinizing all available.When it comes to Coorg homestay, you have ample of choices. You should look harder to find a cheap homestay in Coorg. The homestay in Coorg for family is enjoyable if they have special amenities for kids as well as elders. Find the perfect homestay in Coorg with price, location and other features in any best website. You can get valuable deals for cheap hotels in Coorg if you book utilizing any apps. View the resorts in Coorg with tariff before paying for any rooms.Check the hotel reviews prior booking the room on trusted websites. Compares rates of different hotels to find the convenient one for you. There are many best resorts in Coorg for honeymoon couples.Book any resort on booking websites like www.ulohotels.com which provides you with multiple options. You can find the resorts in Coorg with price in this website.The budget hotels in Coorg are ideal for solo travellers and group of friends who wants to spend less on rooms. The best resorts in Coorg, Madikeri are Ulo Riviera The Spring arc and Ulo Alpinia Estate resort.They are located on the hills surrounded by the spice and copy plantation making our stay at Coorg blissful. It provides services like television, complimentary breakfast, personal assistance, bonfire, and extra bed. You have many options for booking rooms like Deluxe rooms, Family Deluxe room and Economy suite.If you search for top resorts in Coorg, Ulo Hotels Group will be one among them. The location and the services provided here makes them the best honeymoon resorts in Coorg. These two are the best budget resorts in Coorg for family. They comes under the top 10 resorts in Coorg. It is comfortable and easy to book the rooms online.The price of the room mentioned in the website is inclusive of all the taxes. They have both budget and luxury rooms. You can book any budget resorts in Coorg with a single touch by utilizing the online booking feature in your mobile phones. You need not to pay any booking fees and they also provide 24/7 Customer service. The resort check-in time is 10 A.M and check-out time is 9 A.M. It is recommended to call and confirm your booking to avoid any issues. Visit www.ulohotels.com to clear any doubts related to booking. There are also many deals available if you book the rooms online.The rooms are equipped with air conditioner, television, laundry,etc,. It have 24/7 hot and cold water supply. The bonfire is also arranged if requested by the tourists. The charges will be based only on the number of rooms you are booking. The cancellation and prepayment policies differs according to the room type. Verify the Fare policy related to your room. You can pay online or directly at the hotel if the room you have chosen supports this option.The price is reasonable when compared to other resorts in Coorg.You can also book rooms with terrace or balcony. You can arrange for a local tour from the travel agency available at the travel desk.They provide car for rentals and you can also hire a guide. You can keep your belongings in your personal lockers. It is the only resort in Coorg which provides Rain Dance. They give complimentary gala dinner on New year and Christmas. Free private parking is available for all customers. The pets are allowed inside the resort. You should display your identification proofs upon check-in. Every room is equipped with a personal bathroom fitted with bath. It serves yummy Breakfast and Dinner daily. If you desire, you can even have your breakfast in your room.It is situated near the major tourist spots and helps the tourists to travel easily. You can find many hotels in Madikeri near bus stand and the best one among them is Ulo Hotels. It is located in 11 kms from the Madikeri Bus Stand on Mangalore Highway. The aroma from the spice and coffee plantation helps to refresh yourselves. It will be definitely be a memorable experience to stay in Coorg.");
				
				}
				else if(this.locationId== 3){
					
					this.setMetaTag("Book Kodaikanal Resorts and Hotels @ Great Rates");
					this.setMetaDescription("Make your trip wonderful by staying at the best luxury and budget resorts in Kodaikanal. ULO SMS Residency, ULO Kodai Green &amp; ULO Muthu Residency are ideal resorts for perfect vacation.");
					this.setWebContent("Kodaikanal is popularly known as the Princess of Hills. It is famous for its green,fresh and scenic environment. It is located @ 2,000 metres above sea level. It is covered with forest, lakes, waterfalls, granite and grassy hills. The points of interest are, Kodaikanal Lake, Kodaikanal Solar Observatory, Silver Cascade Falls, Pine Tree Forest, Dolphin Nose, etc,.It has chill climate for the whole year with seasonal rainfall. Choose a hotel which is present on the top of the hill to get a better view of this place. The famous foods in Kodaikanal are, chocolates, brownies, hot chai or tea and cheeses. Visiting Kodaikanal using travel package is best option if you want to see all the major tourist places.The best time to visit this place is from March to June or December to February. You can go for trekking, horse riding, boating etc,. Only few ATM�s are available in Kodaikanal, so try to carry sufficient amount of money with you. It is the most visited honeymoon destinations in South India. This is a dream destination for family vacation, adventure seekers and honeymoon couples.There are handful of choices for accommodation in Kodaikanal. Look for the list of Kodaikanal hotels and resorts prior booking your room. You can check the best resorts in Kodaikanal with rates from any valid booking websites. Find your ideal Kodaikanal resorts package by logging into the website www.ulohotels.com. Check Kodaikanal resorts list before planning your holiday.If your budget is high, you can reserve any luxury hotels in Kodaikanal. The Kodaikanal resorts price list may slightly differ in some websites.The best hotels in Kodaikanal are the ones which satisfies all your requirements. You can find many best resorts in Kodaikanal at affordable price. The best place to stay in Kodaikanal is the one which is easily accessible.If you travel with a large group, you can get many best cottages in Kodaikanal. ULO SMS Residency and ULO Muthu Residency are the two best hotels in Kodaikanal for family. Log into www.ulohotels.com to locate all hotels in Kodaikanal with rates. You can even book top hotels in Kodaikanal with great deals offered by booking sites. The luxury resorts in Kodaikanal comes with additional features like swimming pools, indoor games, refrigerator, etc,.You can find the hotels in Kodaikanal with phone numbers in ULO Hotels Group official page. Our Kodaikanal hotels rates are moderate when compared to other hotels in the locality. You will have multiple options when it comes to places to stay in Kodaikanal. If you want to limit your travel expenses, view the budget hotels in Kodaikanal with rates in any authorized websites.The cottages in Kodaikanal are generally situated near hills or forest areas. Book independent cottages in Kodaikanal to have a private vacation with your close ones. Make sure that you reserve only family cottages in Kodaikanal if you stay with your family. You can find budget cottages in Kodaikanal with rates in our website. The lodges in Kodaikanal are best suited if you travel alone.ULO Kodai Green is one of the few hotels in Kodaikanal near lake. It is the best lake view resort in Kodaikanal surrounded by the stunning beauty of nature. It is a boutique style hotel which you can rent for few days. It is located at the centre of the city which makes it easily approachable. Many famous tourist spots are located around 4km from the hotel.It has Standard and family rooms with the amenities like TV, AC, Laundry, and 24 hours room service. You can also avail discount of 15%. Even if you cancel the room 24 hours prior the trip, you can receive full refund. The hotel booking in Kodaikanal can be done either by online or directly at the hotels. One can get best offers in cheap hotels in Kodaikanal if you book online with the promo code.If there is a budget cut, rest in any cheap cottages in Kodaikanal. There are plenty of good hotels in Kodaikanal to stay alone or with your family. If you travel as a couple, you can find many good resorts in Kodaikanal. On the basis of your budget, you can choose any 3 star or 5 star hotels in Kodaikanal. The list of hotels in Kodaikanal with tariff can be downloaded from the internet. The cost is calculated with respect to the number of rooms in all Kodaikanal ULO hotels.If you desire to cut short your expenses, try to stay in the dormitory in Kodaikanal. The 3 bedroom cottages in kodaikanal are a wise option if you stay with a large group or family. You can self-cook or arrange a maid for cooking if you stay in a cottage.If you long for your mother�s care, try homestay in Kodaikanal. The homestay in Kodaikanal is cheaper than booking cottages.ULO Muthu Residency provides best accommodation to all its customers. Most of the popular tourist places are located within 7 kms from this hotel. The Bus Stand is also 4 km from the hotel. Each room is equipped with Air conditioners, TV, tables, chairs and personal bathroom. There is also 24*7 hot and cold waeter supply. They also have facilities like, Wi-Fi, laundry, private parking, room service, rental transport and even doctors. ULO SMS Residency is one of the great Kodaikanal hotels near bus stand. They offer services like, laundry, room service, personal parking, free internet, restaurant and also arranging travels. They provide 24/7 room service and hot or cold water supply. You can get extra bed or mattress on request. Make use of meditation room to do yoga or any other exercises. As per the customer�s request, they arrange campfire also. If required, same your valuables in personal lockers. You can book the room online and pay for it directly at the hotel. If you want to make your vacation comfortable, rest in the best villas in Kodaikanal. Many corporates or banks have their guest houses in Kodaikanalfor their employees. Reserve any mount view Kodaikanal hotels on www.ulohotels.com @ starting price of Rs.999. If you want to have a best vacation ever, don�t worry about the money spent.");
					
				}
				else if(this.locationId== 4){
					
					this.setMetaTag("Best Budget Resorts in Yelagiri with Great Ambience @ LowestPrices");
					this.setMetaDescription("Reserve the best low budget luxury hotels and resorts in Yelagiri. Get great deals, offers and discounts @ ULO Hill Breeze, one of the fine resorts on Yelagiri.");
					this.setWebContent("Yelagiri is one of the famous hill stations in Tamil Nadu. It is located at the height of 1,110.6 metres above sea level. It is fully covered with valleys, rose- garden and orchads. The main tourist places to visit are, Jalagamparai Waterfalls, Murugan Temple, and Yelagiri Forest Hill. The best months to visit Yelagiri are November to February. You can buy local honey and jackfruit from the local markets. It is an amazing place for people who love exploring the nature. It is an excellent place for trekking and meditation. Stay in the best hotels in Yelagiri to enjoy the greenery and mesmerizing beauty of Yelagiri. To book your hotel rooms, search as Yelagiri hotels booking in any authenticated booking sites. You will get the list of hotels in Yelagiri with tariff. Select the hotel or resort which is suitable for you. If you want to stay in a homely surrounding, opt for homestay in Yelagiri Hills. If you plan a vacation, you won�t just pack your suitcases and rush to the trip. The crucial step in your trip is to find a good accommodation in Yelagiri. If your family has small kids, they mostly prefer cottages in Yelagiri Hills with swimming pools. Each cottage has a separate swimming pool. You can hire separate cook and maid while you stay at the cottages. If you want to spend your vacation with your family free from other disturbances, book 3 star cottages in Yellagiri. If you are a bachelor or bunch of friends travelling together, book cheap hotels in Yelagiri to save money. If you can enjoy the complete beauty of Yelagiri from your balcony, then that can be called as the best resort in Yelagiri. Ulo Hill Breeze is one of the best hotels in Yelagiri which provide great service for reasonable price. You can say any resorts as the best resorts in Yelagiri if it matches all your expectations.If you search the internet, you will find Ulo Hotel Groups as one of the best places to stay in Yelagiri. If you reserve the cottages online, you can get discounts in Yelagiri cottages prices. One can say that Ulo hill Breeze is one of the best rated Yelagiri Hills resorts in any booking websites. Nowadays, most of the Yelagiri resorts comes with common swimming pool. If you book it online, they also give you upto 50% discounts on total price. Pick any resorts in Yelagiri Hills which is easily accessible via any transport facility. You can reserve even cheap resorts in Yelagiri with swimming pool @ low rates in www.ulohotels.com. See the Yelagiri resorts list in websites and then choose the perfect resort for you. Book any luxury Yelagiri hotels and resorts online @ special price. If you want hotels which is value for money, reserve 3 star budget hotels in Yelagiri like Ulo Hill Breeze. You can find budget hotels in Yelagiri with tariff at their official website. Once the rooms are booked, you will receive either confirmation message or mail within few minutes. They also have 24/7 Customer Support to answer all your queries. They also provide quick refunds if you cancel your booking. The rooms are spacious and are cleaned daily. You can park your cars in private car parking. They have separate desk for local travel agency using which you can book any travel packages. All the rooms have separate bathroom and TV. You can order food directly to your rooms from the restaurant. They have special cottages for honeymoon couples as well.It is also located near the main tourist spots in Yelagiri Hills. If you want to book best budget hotels, then Ulo Hotel Groups is a wonderful choice.");
					
				}
				
				else if(this.locationId== 5){
					
					this.setMetaTag("Experience the hospitality of the amazing Kolli Hills Resorts and Hotels");
					this.setMetaDescription("Great savings on Kolli Hills Resorts and Hotels booking online. ULO Rejoice Villa Resorts in Kolli hills offers great rooms in great prices with best facilities.");
					this.setWebContent("Kolli Hills, also called as Kollimalai is located in Namakkal district in Tamil Nadu. It is located at 1000 to 1300 metres above sea level. It is famous for jackfruit and wild honey which is harvested from the mountains. It is frequently visited by hikers, nature lovers and trekking associations. Kolli Hills is known for its rich and diverse flora and fauna. There are mainly 9 tourist places in Kollimalai which is visited by all tourists. Stay in the best Kolli Hills resorts to enjoy your trip to the fullest. Most of the Kollimalai hotels are situated amidst of hills with breathtaking view. You can get your ideal resorts in Kolli Hills by checking in www.ulohotels.com. The Kolli Hills resorts booking is made easier by booking sites or apps. If you are a family or a group, many Kolli Hills hotels provide you with special packages.  Kolli Hills trip is mostly preferred by couples and group. The best time to visit it is February to December. You can download Kolli Hills Travel Guide to know about places to visit in Kollimalai. Staying in kolli Hills rejuvenate your mind and body. Kolli hills provides the best accommodation  options for all the travellers making your trip pleasant. You can choose among luxurious resorts, budget hotels, lodges and cottages. If you are a honeymoon couple, booking a villa will be the best option. The first step in planning a vacation to Kollimalai is to explore the entire list of hotels in Kollimalai Hills.  To find the best suited room for you, type as Kolli Hills accommodation in www.ulohotels.com. You should assess all the criteria like travel expenses, location, cost, amenities etc, if you plan for Kolli Hills stay. When you look for places to stay in Kollimalai, choose a place which provides easy transportation. If you opt for Kolli Hills homestay, rent homes from people in Kollimalai. You can directly find the perfect homestay for you by searching in any booking websites. You have multiple options when it comes to places to stay in Kolli Hills which you can decide according to your budget. When you select Kollimalai cottages for your stay, you can book either 2 or 3 bedroom cottages based on head count. If you book your resorts online, you will get rooms at discount prices. There is no reservation cost if you book online. You can compare rates of different resorts and then select thee one which satisfies your needs and budget. You can make use of promo codes or coupons to get a discount on your rooms. Check for accommodation details in ULO Hotels official website. You can also get best offers and deals on online booking in Kollimalai. ULO Rejoice Villa resorts is one of the best Kollimalai resorts located near the Arapalliswarar Temple. It is best suited for family, couples and friends. sIt is situated at 6 km from the Kolli Hills. The room categories are, Standard room, Suite room, Family room and Economy suite. The check-in time is 10:00 A.M and check-out time is 12:00 P.M. If you want to change or cancel your booking, do it one week before your arrival date. Most of the tourist places are within 4 km from the resort. They have an on-site restaurant with free Wi-Fi. Each room is fitted with a private bathroom and TV. You can get any help from the 24*7 front desk. No reservation is required for public parking. One of the perks is free maid service. They also have Banquet or Meeting facilities. You can get 2 additional beds in a room if requested. You can enjoy the sunrise and sunset from your balcony. They also arrange campfire with music on customer�s request. ULO Rejoice Villa Resorts is a safe place for your family and friends. You can get best homemade foods when you stay there. The nearest airport, Tiruchirapalli Airport is only 90 minutes from the resort. Make the best of your time by staying @ one of the best resorts in Kollimalai.");
					
				}*/
		
		if(request.getParameter("locationId") != null && request.getParameter("locationId").toString() !="") this.setLocationId(Integer.parseInt(request.getParameter("locationId").toString()));
		else
			this.setLocationId(getLocationId());
		
		
		return SUCCESS;
	}

}
