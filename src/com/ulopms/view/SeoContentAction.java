package com.ulopms.view;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;




//import com.medwecare.controller.AwsObjectManager;
//import com.medwecare.controller.CommentManager;
//import com.medwecare.controller.CommentUserManager;
////import com.medwecare.controller.NotificationManager;
//import com.medwecare.controller.ProjectManager;

//import com.medwecare.model.AwsObject;
//import com.medwecare.model.Notification;
//import com.medwecare.model.ObjectComment;
//import com.medwecare.model.ObjectCommentUser;
//import com.medwecare.model.Project;

































import com.opensymphony.xwork2.ActionSupport;

import freemarker.template.Configuration;
import freemarker.template.Template;

import javax.servlet.http.HttpServletRequest;

import com.opensymphony.xwork2.ModelDriven;
import com.ulopms.controller.AreaManager;
import com.ulopms.controller.BlogArticlesManager;
import com.ulopms.controller.GooglePlaceManager;
import com.ulopms.controller.LocationContentManager;
import com.ulopms.controller.LocationManager;
import com.ulopms.controller.PmsGuestManager;
import com.ulopms.controller.PmsPropertyManager;
import com.ulopms.controller.PropertyItemCategoryManager;
import com.ulopms.controller.PropertyItemManager;
import com.ulopms.controller.PropertyTaxeManager;
import com.ulopms.controller.SeoContentManager;
import com.ulopms.controller.UserLoginManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.Area;
import com.ulopms.model.BlogArticles;
import com.ulopms.model.GoogleArea;		
import com.ulopms.model.GoogleLocation;
import com.ulopms.model.Location;
import com.ulopms.model.LocationContent;
import com.ulopms.model.PmsGuest;
import com.ulopms.model.PmsProperty;
import com.ulopms.model.PropertyItemCategory;
import com.ulopms.model.PropertyItem;
import com.ulopms.model.PropertyTaxe;
import com.ulopms.model.SeoContent;
import com.ulopms.model.User;
import com.ulopms.util.Email;
import com.ulopms.util.Image;

public class SeoContentAction extends ActionSupport implements
ServletRequestAware,SessionAware, UserAware{
     
	

	private static final long serialVersionUID = 914982665758390091L;
    
	private Integer seoContentId;
	private Integer locationId;
	private Integer googleAreaId;		
	private Integer googleLocationId;
	private Integer propertyId;
	private Integer areaId;
	private String  title;
	private String  description;
	private String  content;
	private String  keywords;
	private String  socialMetaTag;
	private String  h1;
	private String  h2;
	private String  h3;
	private String  h4;
	private String  shortDescription;
	private Integer blogArticleId;
	
	private SeoContent seoContent;
	
	private List<SeoContent> seoContentList;
	

	private static final Logger logger = Logger.getLogger(SeoContentAction.class);

	private HttpSession session;

	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}
	
	private SessionMap<String,Object> sessionMap;  
	  
	//getters and setters  
	  
	@Override  
	public void setSession(Map<String, Object> map) {  
	    sessionMap=(SessionMap)map;  
	}  
	
	
	private User user;

	
	// @Override
	// public User getModel() {
	// return user;
	// }

	@Override
	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	
	private HttpServletRequest request;

	

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	public SeoContentAction() {

	}

	public String execute() {

		//this.patientId = getPatientId();
		return SUCCESS;
	}

	
	
	
	
	
	
	public String getSeoLocationContent() throws IOException {
		//this.propertyId = (Integer) sessionMap.get("propertyId");
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			SeoContentManager seoContentController = new SeoContentManager();
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");

			SeoContent seoContent = seoContentController.findSeoContent(getGoogleLocationId());	
			List<SeoContent> listSeoContent=seoContentController.list(getGoogleLocationId());
			if(listSeoContent.size()>0 && !listSeoContent.isEmpty())
			{
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"seoContentId\":\"" + seoContent.getSeoContentId()+ "\"";
				String title =  seoContent.getTitle().trim();
				String strTitle = title.replace("\"", "\\\"" );
				String strTrimTitle = strTitle.replaceAll("\\s+"," ");
				jsonOutput += ",\"title\":\"" + strTrimTitle + "\"";
				String description = seoContent.getDescription().trim();
				String strDescription = description.replace("\"", "\\\"" );
				String strTrimDescription = strDescription.replaceAll("\\s+"," ");
				jsonOutput += ",\"description\":\"" + strTrimDescription + "\"";
				String content = seoContent.getContent().trim();
				String strContent = content.replace("\"", "\\\"" );
				String strTrimContent = strContent.replaceAll("\\s+"," ");
				jsonOutput += ",\"content\":\"" + strTrimContent + "\"";
				jsonOutput += ",\"keywords\":\"" + seoContent.getKeywords().trim()+ "\"";
				jsonOutput += ",\"shortDescription\":\"" + seoContent.getShortDescription().trim()+ "\"";
				jsonOutput += ",\"socialMetaTag\":\"" + seoContent.getSocialMetaTag().trim()+ "\"";
				jsonOutput += ",\"h1\":\"" + seoContent.getH1().trim()+ "\"";
				jsonOutput += ",\"h2\":\"" + seoContent.getH2().trim()+ "\"";
				jsonOutput += ",\"h3\":\"" + seoContent.getH3().trim()+ "\"";
				jsonOutput += ",\"h4\":\"" + seoContent.getH4().trim()+ "\"";
				jsonOutput += "}";

			}else{
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"seoContentId\":\" " + ""+ "\"";
				jsonOutput += ",\"title\":\"" + " "+ "\"";
				jsonOutput += ",\"description\":\" " + ""+ "\"";
				jsonOutput += ",\"content\":\"" + " "+ "\"";
				jsonOutput += ",\"keywords\":\"" + " "+ "\"";
				jsonOutput += ",\"shortDescription\":\"" + " "+ "\"";
				jsonOutput += ",\"socialMetaTag\":\"" + " "+ "\"";
				jsonOutput += ",\"h1\":\"" + " "+ "\"";
				jsonOutput += ",\"h2\":\"" + " "+ "\"";
				jsonOutput += ",\"h3\":\"" + " "+ "\"";
				jsonOutput += ",\"h4\":\"" + " "+ "\"";
				jsonOutput += "}";
			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}	

	public String editSeoLocationContent() {
		
		//this.propertyId = (Integer) sessionMap.get("propertyId");
		SeoContentManager seoContentController = new SeoContentManager();
		GooglePlaceManager placeController = new GooglePlaceManager();	
		
		
		try {
			
						
			//SeoContent seoContent = new SeoContent();
			
			
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			//SeoContent seoContent = seoContentController.findId(getLocationId());	
			String modTitle = getTitle().replace("percent", "%").replaceAll("\\sand"," &");
			String modDescription = getDescription().replace("percent", "%").replaceAll("\\sand"," &");
			String modContent = getContent().replace("plus", "+");
			
			this.seoContentList = seoContentController.list(getGoogleLocationId());
			if(seoContentList.size()>0 && !seoContentList.isEmpty()){
				SeoContent seoContent = seoContentController.findSeoContent(getGoogleLocationId());
				seoContent.setTitle(modTitle);
				seoContent.setDescription(modDescription);
				seoContent.setContent(modContent);
				seoContent.setKeywords(getKeywords());
				seoContent.setShortDescription(getShortDescription());
				seoContent.setSocialMetaTag(getSocialMetaTag());
				seoContent.setH1(getH1());
				seoContent.setH2(getH2());
				seoContent.setH3(getH3());
				seoContent.setH4(getH4());
				seoContent.setIsActive(true);
				seoContent.setIsDeleted(false);
				GoogleLocation location = placeController.find(getGoogleLocationId());
				seoContent.setGoogleLocation(location);
				seoContentController.edit(seoContent);	
			
			
			}
			else{
				SeoContent content1 = new SeoContent();
				
				content1.setTitle(modTitle);
				content1.setDescription(modDescription);
				content1.setContent(getContent());
				content1.setKeywords(getKeywords());
				content1.setShortDescription(getShortDescription());
				content1.setSocialMetaTag(getSocialMetaTag());
				content1.setH1(getH1());
				content1.setH2(getH2());
				content1.setH3(getH3());
				content1.setH4(getH4());
				content1.setIsActive(true);
				content1.setIsDeleted(false);
				GoogleLocation location = placeController.find(getGoogleLocationId());
				content1.setGoogleLocation(location);
				
			
			
			seoContentController.edit(content1);
			}
			/*for (SeoContent content : seoContentList) {
				content.setIsActive(false);
				content.setIsDeleted(true);
				
				seoContentController.edit(content);	
			}	*/
			
			
			
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return SUCCESS;
	}
	
	
	public String editSeoPropertyContent() {
		
		//this.propertyId = (Integer) sessionMap.get("propertyId");
		SeoContentManager seoContentController = new SeoContentManager();
		PmsPropertyManager pmsPropertyController = new PmsPropertyManager();
		
		
		try {
			
						
			//SeoContent seoContent = new SeoContent();
			
			
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			String modTitle = getTitle().replace("percent", "%").replaceAll("\\sand"," &");
			String modDescription = getDescription().replace("percent", "%").replaceAll("\\sand"," &");	
			String modContent = getContent().replace("plus", "+");
			
			this.seoContentList = seoContentController.list1(getPropertyId());
			if(seoContentList.size()>0 && !seoContentList.isEmpty()){
				
				SeoContent seoContent = seoContentController.findId1(getPropertyId());
				seoContent.setTitle(modTitle);
				seoContent.setDescription(modDescription);
				seoContent.setContent(modContent);
				seoContent.setKeywords(getKeywords());
				seoContent.setShortDescription(getShortDescription());
				seoContent.setSocialMetaTag(getSocialMetaTag());
				seoContent.setH1(getH1());
				seoContent.setH2(getH2());
				seoContent.setH3(getH3());
				seoContent.setH4(getH4());
				seoContent.setIsActive(true);
				seoContent.setIsDeleted(false);
				PmsProperty pmsProperty = pmsPropertyController.find(getPropertyId());
				seoContent.setPmsProperty(pmsProperty);
				seoContentController.edit(seoContent);
			}
			else{
				SeoContent content1 = new SeoContent();
				
				content1.setTitle(modTitle);
				content1.setDescription(modDescription);
				content1.setContent(getContent());
				content1.setKeywords(getKeywords());
				content1.setShortDescription(getShortDescription());
				content1.setSocialMetaTag(getSocialMetaTag());
				content1.setH1(getH1());
				content1.setH2(getH2());
				content1.setH3(getH3());
				content1.setH4(getH4());
				content1.setIsActive(true);
				content1.setIsDeleted(false);
				PmsProperty pmsProperty = pmsPropertyController.find(getPropertyId());
				content1.setPmsProperty(pmsProperty);
				seoContentController.edit(content1);

			}
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return SUCCESS;
	}
	
	
	public String getSeoPropertyContent() throws IOException {
	//this.propertyId = (Integer) sessionMap.get("propertyId");
	try {

		String jsonOutput = "";
		HttpServletResponse response = ServletActionContext.getResponse();
	
		
		SeoContentManager seoContentController = new SeoContentManager();
		//this.familyRegisterList = familyController.list(getUser().getUserid());
		//model = familyRegisterList;
		response.setContentType("application/json");

		SeoContent seoContent = seoContentController.findId1(getPropertyId());	
		List<SeoContent> listSeoContent=seoContentController.list1(getPropertyId());
		if(listSeoContent.size()>0 && !listSeoContent.isEmpty())
		

		{
			if (!jsonOutput.equalsIgnoreCase(""))
				jsonOutput += ",{";
			else
				jsonOutput += "{";
			jsonOutput += "\"seoContentId\":\"" + seoContent.getSeoContentId()+ "\"";
			String title =  seoContent.getTitle().trim();
			String strTitle = title.replace("\"", "\\\"" );
			String strTrimTitle = strTitle.replaceAll("\\s+"," ");
			jsonOutput += ",\"title\":\"" + strTrimTitle + "\"";
			String description = seoContent.getDescription().trim();
			String strDescription = description.replace("\"", "\\\"" );
			String strTrimDescription = strDescription.replaceAll("\\s+"," ");
			jsonOutput += ",\"description\":\"" + strTrimDescription + "\"";
			String content = seoContent.getContent().trim();
			String strContent = content.replace("\"", "\\\"" );
			String strTrimContent = strContent.replaceAll("\\s+"," ");
			jsonOutput += ",\"content\":\"" + strTrimContent + "\"";
			jsonOutput += ",\"keywords\":\"" + seoContent.getKeywords().trim()+ "\"";
			jsonOutput += ",\"shortDescription\":\"" + seoContent.getShortDescription().trim()+ "\"";
			jsonOutput += ",\"socialMetaTag\":\"" + seoContent.getSocialMetaTag().trim()+ "\"";
			jsonOutput += ",\"h1\":\"" + seoContent.getH1().trim()+ "\"";
			jsonOutput += ",\"h2\":\"" + seoContent.getH2().trim()+ "\"";
			jsonOutput += ",\"h3\":\"" + seoContent.getH3().trim()+ "\"";
			jsonOutput += ",\"h4\":\"" + seoContent.getH4().trim()+ "\"";
			
			jsonOutput += "}";
			
		}else{
			
			if (!jsonOutput.equalsIgnoreCase(""))
				jsonOutput += ",{";
			else
				jsonOutput += "{";
			jsonOutput += "\"seoContentId\":\" " + ""+ "\"";
			jsonOutput += ",\"title\":\"" + " "+ "\"";
			jsonOutput += ",\"description\":\" " + ""+ "\"";
			jsonOutput += ",\"content\":\"" + " "+ "\"";
			jsonOutput += ",\"keywords\":\"" + " "+ "\"";
			jsonOutput += ",\"shortDescription\":\"" + " "+ "\"";
			jsonOutput += ",\"socialMetaTag\":\"" + " "+ "\"";
			jsonOutput += ",\"h1\":\"" + " "+ "\"";
			jsonOutput += ",\"h2\":\"" + " "+ "\"";
			jsonOutput += ",\"h3\":\"" + " "+ "\"";
			jsonOutput += ",\"h4\":\"" + " "+ "\"";
			jsonOutput += "}";
		


		}

		response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
		

	} catch (Exception e) {
		logger.error(e);
		e.printStackTrace();
	} finally {

	}

	return null;

}	

	public String getSeoAreaContent() throws IOException {
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			SeoContentManager seoContentController = new SeoContentManager();
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");

			SeoContent seoContent = seoContentController.findId(getGoogleAreaId());	
			List<SeoContent> listSeoContent=seoContentController.listArea(getGoogleAreaId());
			if(listSeoContent.size()>0 && !listSeoContent.isEmpty())
			{
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"seoContentId\":\"" + seoContent.getSeoContentId()+ "\"";
				String title =  seoContent.getTitle().trim();
				String strTitle = title.replace("\"", "\\\"" );
				String strTrimTitle = strTitle.replaceAll("\\s+"," ");
				jsonOutput += ",\"title\":\"" + strTrimTitle + "\"";
				String description = seoContent.getDescription().trim();
				String strDescription = description.replace("\"", "\\\"" );
				String strTrimDescription = strDescription.replaceAll("\\s+"," ");
				jsonOutput += ",\"description\":\"" + strTrimDescription + "\"";
				String content = seoContent.getContent().trim();
				String strContent = content.replace("\"", "\\\"" );
				String strTrimContent = strContent.replaceAll("\\s+"," ");
				jsonOutput += ",\"content\":\"" + strTrimContent + "\"";
				jsonOutput += ",\"keywords\":\"" + seoContent.getKeywords().trim()+ "\"";
				jsonOutput += ",\"shortDescription\":\"" + seoContent.getShortDescription().trim()+ "\"";
				jsonOutput += ",\"socialMetaTag\":\"" + seoContent.getSocialMetaTag().trim()+ "\"";
				jsonOutput += ",\"h1\":\"" + seoContent.getH1().trim()+ "\"";
				jsonOutput += ",\"h2\":\"" + seoContent.getH2().trim()+ "\"";
				jsonOutput += ",\"h3\":\"" + seoContent.getH3().trim()+ "\"";
				jsonOutput += ",\"h4\":\"" + seoContent.getH4().trim()+ "\"";
				jsonOutput += "}";

			}else{
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"seoContentId\":\" " + ""+ "\"";
				jsonOutput += ",\"title\":\"" + " "+ "\"";
				jsonOutput += ",\"description\":\" " + ""+ "\"";
				jsonOutput += ",\"content\":\"" + " "+ "\"";
				jsonOutput += ",\"keywords\":\"" + " "+ "\"";
				jsonOutput += ",\"shortDescription\":\"" + " "+ "\"";
				jsonOutput += ",\"socialMetaTag\":\"" + " "+ "\"";
				jsonOutput += ",\"h1\":\"" + " "+ "\"";
				jsonOutput += ",\"h2\":\"" + " "+ "\"";
				jsonOutput += ",\"h3\":\"" + " "+ "\"";
				jsonOutput += ",\"h4\":\"" + " "+ "\"";
				jsonOutput += "}";
			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}	
	
	public String editSeoAreaContent() {
		
		//this.propertyId = (Integer) sessionMap.get("propertyId");
		SeoContentManager seoContentController = new SeoContentManager();
		GooglePlaceManager placeController = new GooglePlaceManager();
		
		try {
			
						
			//SeoContent seoContent = new SeoContent();
			
			
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			//SeoContent seoContent = seoContentController.findId(getLocationId());	
			String modTitle = getTitle().replace("percent", "%").replaceAll("\\sand"," &");
			String modDescription = getDescription().replace("percent", "%").replaceAll("\\sand"," &");
			String modContent = getContent().replace("plus", "+");
			this.seoContentList = seoContentController.listArea(getGoogleAreaId());
			if(seoContentList.size()>0 && !seoContentList.isEmpty()){
				SeoContent seoContent = seoContentController.findId(getGoogleAreaId());
				seoContent.setTitle(modTitle);
				seoContent.setDescription(modDescription);
				seoContent.setContent(modContent);
				seoContent.setKeywords(getKeywords());
				seoContent.setShortDescription(getShortDescription());
				seoContent.setSocialMetaTag(getSocialMetaTag());
				seoContent.setH1(getH1());
				seoContent.setH2(getH2());
				seoContent.setH3(getH3());
				seoContent.setH4(getH4());
				seoContent.setIsActive(true);
				seoContent.setIsDeleted(false);
				
				GoogleArea area = placeController.findArea(getGoogleAreaId());
				seoContent.setGoogleArea(area);
				seoContentController.edit(seoContent);	
			
			
			}
			else{
				SeoContent content1 = new SeoContent();
				
				content1.setTitle(modTitle);
				content1.setDescription(modDescription);
				content1.setContent(getContent());
				content1.setKeywords(getKeywords());
				content1.setShortDescription(getShortDescription());
				content1.setSocialMetaTag(getSocialMetaTag());
				content1.setH1(getH1());
				content1.setH2(getH2());
				content1.setH3(getH3());
				content1.setH4(getH4());
				content1.setIsActive(true);
				content1.setIsDeleted(false);
				GoogleArea area = placeController.findArea(getGoogleAreaId());
				content1.setGoogleArea(area);
				seoContentController.edit(content1);
			}
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return SUCCESS;
	}

	public String editSeoBlogArticleContent() {
		
		SeoContentManager seoContentController = new SeoContentManager();
		BlogArticlesManager blogArticleController=new BlogArticlesManager();
		try {
			
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			String editDescription = getDescription().replace("percent", "%").replaceAll("\\sand"," &");
			String editTitle = getTitle().replace("percent", "%").replaceAll("\\sand"," &");
			String modContent = getContent().replace("plus", "+");
			
			this.seoContentList = seoContentController.listBlogArticles(getBlogArticleId());
			if(seoContentList.size()>0 && !seoContentList.isEmpty()){
				SeoContent seoContent = seoContentController.findSeoBlogArticleContent(getBlogArticleId());
				seoContent.setTitle(editTitle);
				seoContent.setDescription(editDescription);
				seoContent.setContent(modContent);
				seoContent.setKeywords(getKeywords());
				seoContent.setShortDescription(getShortDescription());
				seoContent.setSocialMetaTag(getSocialMetaTag());
				seoContent.setH1(getH1());
				seoContent.setH2(getH2());
				seoContent.setH3(getH3());
				seoContent.setH4(getH4());
				seoContent.setIsActive(true);
				seoContent.setIsDeleted(false);
				BlogArticles blogArticles=blogArticleController.find(getBlogArticleId());
				seoContent.setBlogArticles(blogArticles);
				seoContentController.edit(seoContent);	
			
			
			}
			else{
				SeoContent content1 = new SeoContent();
				
				content1.setTitle(editTitle);
				content1.setDescription(editDescription);
				content1.setContent(getContent());
				content1.setKeywords(getKeywords());
				content1.setShortDescription(getShortDescription());
				content1.setSocialMetaTag(getSocialMetaTag());
				content1.setH1(getH1());
				content1.setH2(getH2());
				content1.setH3(getH3());
				content1.setH4(getH4());
				content1.setIsActive(true);
				content1.setIsDeleted(false);
				BlogArticles blogArticles=blogArticleController.find(getBlogArticleId());
				content1.setBlogArticles(blogArticles);
				seoContentController.edit(content1);
			}
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return SUCCESS;
	}


	public String getSeoBlogArticleContent() throws IOException {
		try {
	
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			SeoContentManager seoContentController = new SeoContentManager();
			response.setContentType("application/json");
	
			SeoContent seoContent = seoContentController.findSeoBlogArticleContent(getBlogArticleId());
			List<SeoContent> listSeoContent=seoContentController.listBlogArticles(getBlogArticleId());
			if(listSeoContent.size()>0 && !listSeoContent.isEmpty())
			{
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"seoContentId\":\"" + seoContent.getSeoContentId()+ "\"";
				String title =  seoContent.getTitle().trim();
				String strTitle = title.replace("\"", "\\\"" );
				String strTrimTitle = strTitle.replaceAll("\\s+"," ");
				jsonOutput += ",\"title\":\"" + strTrimTitle + "\"";
				String description = seoContent.getDescription().trim();
				String strDescription = description.replace("\"", "\\\"" );
				String strTrimDescription = strDescription.replaceAll("\\s+"," ");
				jsonOutput += ",\"description\":\"" + strTrimDescription + "\"";
				String content = seoContent.getContent().trim();
				String strContent = content.replace("\"", "\\\"" );
				String strTrimContent = strContent.replaceAll("\\s+"," ");
				jsonOutput += ",\"content\":\"" + strTrimContent + "\"";
				jsonOutput += ",\"keywords\":\"" + seoContent.getKeywords().trim()+ "\"";
				jsonOutput += ",\"shortDescription\":\"" + seoContent.getShortDescription().trim()+ "\"";
				jsonOutput += ",\"socialMetaTag\":\"" + seoContent.getSocialMetaTag().trim()+ "\"";
				jsonOutput += ",\"h1\":\"" + seoContent.getH1().trim()+ "\"";
				jsonOutput += ",\"h2\":\"" + seoContent.getH2().trim()+ "\"";
				jsonOutput += ",\"h3\":\"" + seoContent.getH3().trim()+ "\"";
				jsonOutput += ",\"h4\":\"" + seoContent.getH4().trim()+ "\"";
				jsonOutput += "}";
	
			}else{
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"seoContentId\":\" " + ""+ "\"";
				jsonOutput += ",\"title\":\"" + " "+ "\"";
				jsonOutput += ",\"description\":\" " + ""+ "\"";
				jsonOutput += ",\"content\":\"" + " "+ "\"";
				jsonOutput += ",\"keywords\":\"" + " "+ "\"";
				jsonOutput += ",\"shortDescription\":\"" + " "+ "\"";
				jsonOutput += ",\"socialMetaTag\":\"" + " "+ "\"";
				jsonOutput += ",\"h1\":\"" + " "+ "\"";
				jsonOutput += ",\"h2\":\"" + " "+ "\"";
				jsonOutput += ",\"h3\":\"" + " "+ "\"";
				jsonOutput += ",\"h4\":\"" + " "+ "\"";
				jsonOutput += "}";
			}
	
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
	
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {
	
		}
	
		return null;
	
	}	
	
	public Integer getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(Integer propertyId) {
		this.propertyId = propertyId;
	}
	
	public Integer getSeoContentId() {
		return seoContentId;
	}

	public void setSeoContentId(Integer seoContentId) {
		this.seoContentId = seoContentId;
	}

	public Integer getLocationId() {
		return locationId;
	}

	public void setLocationId(Integer locationId) {
		this.locationId = locationId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getKeywords() {
		return keywords;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

	public String getH1() {
		return h1;
	}

	public void setH1(String h1) {
		this.h1 = h1;
	}

	public String getH2() {
		return h2;
	}

	public void setH2(String h2) {
		this.h2 = h2;
	}

	public String getH3() {
		return h3;
	}

	public void setH3(String h3) {
		this.h3 = h3;
	}

	public String getH4() {
		return h4;
	}

	public void setH4(String h4) {
		this.h4 = h4;
	}

	public String getShortDescription() {
		return shortDescription;
	}

	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}
	
	public String getSocialMetaTag() {
		return socialMetaTag;
	}

	public void setSocialMetaTag(String socialMetaTag) {
		this.socialMetaTag = socialMetaTag;
	}

	

	public List<SeoContent> getSeoContentList() {
		return seoContentList;
	}

	public void setSeoContentList(List<SeoContent> seoContentList) {
		this.seoContentList = seoContentList;
	}
	
	public Integer getGoogleAreaId() {
		return googleAreaId;
	}

	public void setGoogleAreaId(Integer googleAreaId) {
		this.googleAreaId = googleAreaId;
	}

	public Integer getGoogleLocationId() {
		return googleLocationId;
	}

	public void setGoogleLocationId(Integer googleLocationId) {
		this.googleLocationId = googleLocationId;
	}

	public Integer getAreaId() {
		return areaId;
	}

	public void setAreaId(Integer areaId) {
		this.areaId = areaId;
	}
	
	public Integer getBlogArticleId() {
		return blogArticleId;
	}

	public void setBlogArticleId(Integer blogArticleId) {
		this.blogArticleId = blogArticleId;
	}

}
