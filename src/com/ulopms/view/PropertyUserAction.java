package com.ulopms.view;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;




//import com.medwecare.controller.AwsObjectManager;
//import com.medwecare.controller.CommentManager;
//import com.medwecare.controller.CommentUserManager;
////import com.medwecare.controller.NotificationManager;
//import com.medwecare.controller.ProjectManager;

//import com.medwecare.model.AwsObject;
//import com.medwecare.model.Notification;
//import com.medwecare.model.ObjectComment;
//import com.medwecare.model.ObjectCommentUser;
//import com.medwecare.model.Project;


















import com.opensymphony.xwork2.ActionSupport;

import freemarker.template.Configuration;
import freemarker.template.Template;

import javax.servlet.http.HttpServletRequest;

import com.opensymphony.xwork2.ModelDriven;
import com.ulopms.view.UserAddAction;
import com.ulopms.controller.PmsGuestManager;
import com.ulopms.controller.PmsPropertyManager;
import com.ulopms.controller.PropertyItemCategoryManager;
import com.ulopms.controller.PropertyItemManager;
import com.ulopms.controller.PropertyTaxeManager;
import com.ulopms.controller.PropertyUserManager;
import com.ulopms.controller.RoleManager;
import com.ulopms.controller.UserLoginManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.PmsGuest;
import com.ulopms.model.PmsProperty;
import com.ulopms.model.PropertyAmenity;
import com.ulopms.model.PropertyItem;
import com.ulopms.model.PropertyItemCategory;
import com.ulopms.model.PropertyTaxe;
import com.ulopms.model.PropertyUser;
import com.ulopms.model.Role;
import com.ulopms.model.User;
import com.ulopms.util.Email;
import com.ulopms.util.Image;





public class PropertyUserAction extends ActionSupport implements 
ServletRequestAware, SessionAware, UserAware{

	private static final long serialVersionUID = 914982665758390091L;

	private Integer userId;
	private Integer propertyUserId;
	private Integer roleId;
	private String emailId;
	private String userName;
	public Integer propertyId;
	
	
	private PropertyUser propertyUser;
	
	private PropertyUser userProperty;
	
	private List<PropertyUser> propertyUserList;
	
	private List<PropertyUser> userPropertyList;
	
	private List<User> userList;
   
	private List<PmsProperty> propertyList;
	
	private static final Logger logger = Logger.getLogger(PropertyUserAction.class);

    private User user;
    
	private HttpSession session;

	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}
	
	private SessionMap<String,Object> sessionMap;  
	  
	//getters and setters  
	  
	@Override  
	public void setSession(Map<String, Object> map) {  
	    sessionMap=(SessionMap)map;  
	}  
	
	
	// @Override
	// public User getModel() {
	// return user;
	// }

	@Override
	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}
    
	
	
	private HttpServletRequest request;

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	public PropertyUserAction() {

	}
	

	public String execute() {

		this.propertyId = (Integer) sessionMap.get("propertyId");
		//this.patientId = getPatientId();
		return SUCCESS;
	}
	
	
    
	public String getSingleUser() throws IOException {
		
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PropertyUserManager propertyUserController = new PropertyUserManager();
			UserLoginManager userController = new UserLoginManager();
			RoleManager roleController = new RoleManager();
			response.setContentType("application/json");
             
			PropertyUser propertyUser =  propertyUserController.find(getPropertyUserId());
			if(propertyUser!=null){

				User user = userController.find(propertyUser.getUser().getUserId());
			
				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				
				jsonOutput += "\"userName\":\"" + user.getUserName() + "\"";
				jsonOutput += ",\"userId\":\"" + user.getUserId() + "\"";
				jsonOutput += ",\"propertyUserId\":\"" + propertyUser.getPropertyUserId() + "\"";
				jsonOutput += ",\"emailId\":\"" + user.getEmailId()+ "\"";
				jsonOutput += ",\"roleId\":\"" + user.getRole().getRoleId()+ "\"";
				Role rl = roleController.find(user.getRole().getRoleId());
				jsonOutput += ",\"roleName\":\"" + rl.getRoleName()+ "\"";
				jsonOutput += "}";
				
			}
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	
    public String getProperties() throws IOException {
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PropertyUserManager propertyUserController = new PropertyUserManager();
			UserLoginManager userController = new UserLoginManager();
			RoleManager roleController = new RoleManager();
			
			response.setContentType("application/json");

			this.userPropertyList = propertyUserController.listProperty(getUserId());
			for (PropertyUser userProperty : userPropertyList) {
               
				 // User user1 = userController.find(userProperty.getUser().getUserId());
				 // Role role1 = roleController.find(userProperty.getRole().getRoleId());
				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				 jsonOutput += "\"propUserId\":\"" + userProperty.getPropertyUserId() + "\"";
				 jsonOutput += ",\"propertyId\":\"" + userProperty.getPmsProperty().getPropertyId() + "\"";
				 jsonOutput += ",\"propertyName\":\"" + userProperty.getPmsProperty().getPropertyName() + "\"";
				 
				 
				
				
				jsonOutput += "}";

			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}

    public String changeProperty() throws IOException {
    	try{
    		sessionMap.remove("propertyId");
        	sessionMap.remove("propertyName");
        	sessionMap.put("propertyId",getPropertyId());
        	
        	PmsPropertyManager propertyController = new PmsPropertyManager();
        	PmsProperty  property = propertyController.find(getPropertyId());
        	
        	
        	sessionMap.put("propertyName", property.getPropertyName());
        	
        	
        	
        	//session.getValue("pu)
            //sessionMap.get("propertyId");
        	
         
    	}catch(Exception e){
    		logger.error(e);
    		e.printStackTrace();
    	}
        
    	return SUCCESS;
    	
    }
	
	public String getUsers() throws IOException {
		
		try {
			this.propertyId = (Integer) sessionMap.get("propertyId");
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PropertyUserManager propertyUserController = new PropertyUserManager();
			UserLoginManager userController = new UserLoginManager();
			RoleManager roleController = new RoleManager();
			response.setContentType("application/json");

			this.propertyUserList = propertyUserController.list(getPropertyId());
			for (PropertyUser propertyUser : propertyUserList) {
               
				
				  User user1 = userController.findId(propertyUser.getUser().getUserId());
//				  Role role1 = roleController.find(propertyUser.getRole().getRoleId());
				if(user1!=null){
					if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += ",{";
					else
						jsonOutput += "{";
					 jsonOutput += "\"propUserId\":\"" + propertyUser.getPropertyUserId() + "\"";
					 jsonOutput += ",\"userId\":\"" + user1.getUserId()+ "\"";
					 jsonOutput += ",\"userName\":\"" + user1.getUserName()+ "\"";
					 jsonOutput += ",\"emailId\":\"" + user1.getEmailId()+ "\"";
					 jsonOutput += ",\"roleId\":\"" + propertyUser.getRole().getRoleId()+ "\"";
					 jsonOutput += ",\"roleName\":\"" + propertyUser.getRole().getRoleName()+ "\"";
					//PropertyUser pu = propertyUserController.find(propertyUser.getRole().getRoleId());
					//jsonOutput += ",\"roleName\":\"" + role1.getRoleName()+ "\"";
					
					jsonOutput += "}";
	
				}
				
			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}

	public String addUser() {
		try {

			//PmsGuestManager guestController = new PmsGuestManager();
			this.propertyId = (Integer) sessionMap.get("propertyId");

			PropertyUserManager propertyUserController = new PropertyUserManager();
			UserLoginManager userController = new UserLoginManager();
			PmsPropertyManager propertyController = new PmsPropertyManager();

			String jsonOutput="";
			HttpServletResponse response=ServletActionContext.getResponse();
			response.setContentType("application/json");
			
			UserAddAction addUser = new UserAddAction();
			
			if(!jsonOutput.equalsIgnoreCase(""))
				jsonOutput+=",{";
			else
				jsonOutput+= "{";
			
			this.userList = userController.findUserByEmail(getEmailId());
			PropertyUser propertyUser = new PropertyUser();
			
			
			if(this.userList.size()==0 && this.userList.isEmpty()){
				
				Integer userId = addUser.UserAdd(getUserName(),getEmailId(),getRoleId());
				
				User user=userController.find(userId);
				List<PropertyUser> propertyUserList = propertyUserController.list(getPropertyId(),user.getUserId());
				if(propertyUserList.size()==0 && propertyUserList.isEmpty()){
					long time = System.currentTimeMillis();
					java.sql.Date date = new java.sql.Date(time);
			
					propertyUser.setUser(user);
					propertyUser.setRole(user.getRole());
					propertyUser.setIsActive(true);
					propertyUser.setIsDeleted(false);
					PmsProperty property = propertyController.find(getPropertyId());
					propertyUser.setPmsProperty(property);
					propertyUserController.add(propertyUser);
		
				}
				jsonOutput+="\"status\":\""+true+"\"";
				
			}else if(this.userList.size()>0 && !this.userList.isEmpty()){
				for (User us : userList) {
					
					List<PropertyUser> propertyUserList = propertyUserController.list(getPropertyId(),us.getUserId());
			
					if(propertyUserList.size()>0){
						jsonOutput+="\"status\":\""+false+"\"";
			
					}
					else{
						Integer  intRoleId=us.getRole().getRoleId();
						this.roleId=getRoleId();
						if(intRoleId==this.roleId){
							long time = System.currentTimeMillis();
							java.sql.Date date = new java.sql.Date(time);
					
							propertyUser.setUser(us);
							propertyUser.setRole(us.getRole());
							propertyUser.setIsActive(true);
							propertyUser.setIsDeleted(false);
							PmsProperty property = propertyController.find(getPropertyId());
							propertyUser.setPmsProperty(property);
							propertyUserController.add(propertyUser);
							
							jsonOutput+="\"status\":\""+true+"\"";	
						}else{
							String strNA="NA";
							jsonOutput+="\"status\":\""+strNA+"\"";
						}
						
					}
			
				}
			}
			jsonOutput+="}";
			response.getWriter().write("{\"data\":["+jsonOutput+"]}");

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
		}
	
	public String editUser() {
		
		try {
			this.roleId=getRoleId();
			if(getEmailId()!=null && getEmailId()!=""){
				this.emailId=getEmailId().trim();
			}
			
			this.propertyId = (Integer) sessionMap.get("propertyId");
			PropertyUserManager propertyUserController = new PropertyUserManager();
		    UserLoginManager  userController = new UserLoginManager();
		    RoleManager roleController = new RoleManager();
			List<PropertyUser> userlist = propertyUserController.list(this.propertyId,getUserId());
			String jsonOutput="";
			HttpServletResponse response=ServletActionContext.getResponse();
			response.setContentType("application/json");
			boolean blnCheck=false;
			if(!jsonOutput.equalsIgnoreCase(""))
				jsonOutput+=",{";
			else
				jsonOutput+= "{";
			Integer intRoleId=0;
			String emailIds=null;
			if(userlist.size()>0 && !userlist.isEmpty()){
				for(PropertyUser users: userlist ){
					intRoleId = users.getRole().getRoleId();
					User us=userController.find(users.getUser().getUserId());
					emailIds=us.getEmailId();
					if(intRoleId==roleId){
						Role role = roleController.find(users.getRole().getRoleId());
		            	users.setRole(role);
		            	propertyUserController.edit(users);	
		            	blnCheck=true;
					}else{
						blnCheck=false;
						jsonOutput+="\"status\":\""+false+"\"";	
					}
					if(!this.emailId.equalsIgnoreCase(emailIds)){
						blnCheck=false;
						String NA="NA";
						jsonOutput+="\"status\":\""+NA+"\"";
					}
					
					
	            }	
			}
			if(blnCheck){
				if(intRoleId==roleId){
					User user =  userController.find(getUserId());
					long time = System.currentTimeMillis();
					java.sql.Date date = new java.sql.Date(time);
					user.setEmailId(getEmailId());
					user.setUserName(getUserName());
					Role role = roleController.find(getRoleId());
					user.setRole(role);
					userController.edit(user);	
				}	
				jsonOutput+="\"status\":\""+true+"\"";	
			}
			
			

			jsonOutput+="}";
			response.getWriter().write("{\"data\":["+jsonOutput+"]}");
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}
	
	public String deleteUser() {
		
		try {
			PropertyUserManager propertyUserController = new PropertyUserManager();
			UserLoginManager  userController = new UserLoginManager();
			
			
			PropertyUser propertyUser = propertyUserController.find(getPropertyUserId());
						
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			propertyUser.setIsActive(false);
			propertyUser.setIsDeleted(true);
			propertyUser.setModifiedDate(new java.sql.Timestamp(date.getTime()));
			propertyUserController.edit(propertyUser);
			
			
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getPropertyUserId() {
		return propertyUserId;
	}

	public void setPropertyUserId(Integer propertyUserId) {
		this.propertyUserId = propertyUserId;
	}

	public Integer getRoleId() {
		return roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Integer getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(Integer propertyId) {
		this.propertyId = propertyId;
	}

	public PropertyUser getPropertyUser() {
		return propertyUser;
	}

	public void setPropertyUser(PropertyUser propertyUser) {
		this.propertyUser = propertyUser;
	}

	public List<PropertyUser> getPropertyUserList() {
		return propertyUserList;
	}

	public void setPropertyUserList(List<PropertyUser> propertyUserList) {
		this.propertyUserList = propertyUserList;
	}
	
	
	
}
