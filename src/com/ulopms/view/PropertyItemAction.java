package com.ulopms.view;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;




//import com.medwecare.controller.AwsObjectManager;
//import com.medwecare.controller.CommentManager;
//import com.medwecare.controller.CommentUserManager;
////import com.medwecare.controller.NotificationManager;
//import com.medwecare.controller.ProjectManager;

//import com.medwecare.model.AwsObject;
//import com.medwecare.model.Notification;
//import com.medwecare.model.ObjectComment;
//import com.medwecare.model.ObjectCommentUser;
//import com.medwecare.model.Project;



















import com.opensymphony.xwork2.ActionSupport;

import freemarker.template.Configuration;
import freemarker.template.Template;

import javax.servlet.http.HttpServletRequest;

import com.opensymphony.xwork2.ModelDriven;
import com.ulopms.controller.PmsGuestManager;
import com.ulopms.controller.PmsPropertyManager;
import com.ulopms.controller.PropertyItemCategoryManager;
import com.ulopms.controller.PropertyItemManager;
import com.ulopms.controller.PropertyTaxeManager;
import com.ulopms.controller.UserLoginManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.PmsGuest;
import com.ulopms.model.PmsProperty;
import com.ulopms.model.PropertyItemCategory;
import com.ulopms.model.PropertyItem;
import com.ulopms.model.PropertyTaxe;
import com.ulopms.model.User;
import com.ulopms.util.Email;
import com.ulopms.util.Image;

public class PropertyItemAction extends ActionSupport implements
ServletRequestAware,SessionAware, UserAware{
     
	

	private static final long serialVersionUID = 914982665758390091L;
    
	private Integer propertyItemCategoryId;
	private Integer propertyItemId;
	private Integer propertyId;
	private String  itemName;
	private String  itemSku;
	private String  itemCode;
	private String  itemDescription;
	private double  itemAmount;
	private Integer  propertyTaxId;
	
	private PropertyItem item;
	
	private List<PropertyItem> itemList;
	private List<PropertyTaxe> taxeList;

	private static final Logger logger = Logger.getLogger(PropertyItemAction.class);

	private HttpSession session;

	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}
	
	private SessionMap<String,Object> sessionMap;  
	  
	//getters and setters  
	  
	@Override  
	public void setSession(Map<String, Object> map) {  
	    sessionMap=(SessionMap)map;  
	}  
	
	
	private User user;

	
	// @Override
	// public User getModel() {
	// return user;
	// }

	@Override
	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	
	private HttpServletRequest request;

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	public PropertyItemAction() {

	}

	public String execute() {

		//this.patientId = getPatientId();
		return SUCCESS;
	}

	
	
	public String getTaxes() throws IOException {
		
		
		try {
			this.propertyId = (Integer) sessionMap.get("propertyId");
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PropertyTaxeManager taxController = new PropertyTaxeManager();
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");

			this.taxeList = taxController.list();
			for (PropertyTaxe taxe : taxeList) {

				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"taxId\":\"" + taxe.getPropertyTaxId() + "\"";
				jsonOutput += ",\"taxName\":\"" + taxe.getTaxName()+ "\"";
				jsonOutput += "}";


			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	public String getItem() throws IOException {
		
		
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PropertyItemManager itemController = new PropertyItemManager();
			PropertyItemCategoryManager categoryController = new PropertyItemCategoryManager();
			PropertyTaxeManager taxController =  new PropertyTaxeManager();
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");

			PropertyItem item = itemController.find(getPropertyItemId());
			
				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"itemId\":\"" + item.getPropertyItemId() + "\"";
				jsonOutput += ",\"itemCode\":\"" + item.getItemCode()+ "\"";
				jsonOutput += ",\"itemName\":\"" + item.getItemName()+ "\"";
				jsonOutput += ",\"itemDescription\":\"" + item.getItemDescription()+ "\"";
				jsonOutput += ",\"itemSku\":\"" + item.getSku()+ "\"";
				jsonOutput += ",\"itemAmount\":\"" + item.getAmount()+ "\"";
				jsonOutput += ",\"propertyItemCategoryId\":\"" + item.getPropertyItemCategory().getPropertyItemCategoryId()+ "\"";
				PropertyItemCategory category =  categoryController.find(item.getPropertyItemCategory().getPropertyItemCategoryId());
				jsonOutput += ",\"propertyItemCategoryName\":\"" + category.getCategoryName()+ "\"";
					
				jsonOutput += ",\"propertyTaxeId\":\"" + item.getPropertyTaxe().getPropertyTaxId()+ "\"";
				PropertyTaxe tax = taxController.find(item.getPropertyTaxe().getPropertyTaxId());
				jsonOutput += ",\"propertyTaxeName\":\"" + tax.getTaxName()+ "\"";
				
				jsonOutput += "}";

			

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	public String getItems() throws IOException {
		

		try {
			this.propertyId = (Integer) sessionMap.get("propertyId");
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PropertyItemManager itemController = new PropertyItemManager();
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");

			this.itemList = itemController.list(getPropertyId());
			for (PropertyItem item : itemList) {

				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"itemId\":\"" + item.getPropertyItemId() + "\"";
				jsonOutput += ",\"itemCode\":\"" + item.getItemCode()+ "\"";
				jsonOutput += ",\"itemName\":\"" + item.getItemName()+ "\"";
				jsonOutput += ",\"itemDescription\":\"" + item.getItemDescription()+ "\"";
				jsonOutput += ",\"itemSku\":\"" + item.getSku()+ "\"";
				jsonOutput += ",\"itemAmount\":\"" + item.getAmount()+ "\"";
				jsonOutput += ",\"propertyId\":\"" + item.getPmsProperty()+ "\"";
				jsonOutput += ",\"propertyTaxe\":\"" + item.getPropertyTaxe()+ "\"";
				jsonOutput += "}";


			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	

	public String addItem() {
		
		this.propertyId = (Integer) sessionMap.get("propertyId");
		PropertyItemManager itemController = new PropertyItemManager();
		PropertyItemCategoryManager categoryController = new PropertyItemCategoryManager();
		PropertyTaxeManager taxController = new PropertyTaxeManager();
		//UserLoginManager  userController = new UserLoginManager();
		PmsPropertyManager propertyController = new PmsPropertyManager();
		
		try {
			
						
			PropertyItem item = new PropertyItem();
			
			
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			
			item.setAmount(getItemAmount());
			item.setIsActive(true);
			item.setIsDeleted(false);
			item.setItemCode(getItemCode());
			item.setItemDescription(getItemDescription());
			item.setItemName(getItemName());
			item.setSku(getItemSku());
			PmsProperty property = propertyController.find(getPropertyId());
			item.setPmsProperty(property);
			PropertyItemCategory propertyItemCategory = categoryController.find(getPropertyItemCategoryId());
			item.setPropertyItemCategory(propertyItemCategory);
			PropertyTaxe propertyTaxe = taxController.find(getPropertyTaxId());
			item.setPropertyTaxe(propertyTaxe);
			
			
			
			itemController.add(item);
			
			
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return SUCCESS;
	}
	
	
	public String editItem() {
		
		this.propertyId = (Integer) sessionMap.get("propertyId");
		PropertyItemManager itemController = new PropertyItemManager();
		PropertyItemCategoryManager categoryController = new PropertyItemCategoryManager();
		PropertyTaxeManager taxController = new PropertyTaxeManager();
	    PmsPropertyManager propertyController = new PmsPropertyManager();
		UserLoginManager  userController = new UserLoginManager();
		try {
			
						
			PropertyItem item = itemController.find(getPropertyItemId());
			
			
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			item.setAmount(getItemAmount());
			item.setIsActive(true);
			item.setIsDeleted(false);
			item.setItemCode(getItemCode());
			item.setItemDescription(getItemDescription());
			item.setItemName(getItemName());
			item.setSku(getItemSku());
			PmsProperty property = propertyController.find(getPropertyId());
			item.setPmsProperty(property);
			PropertyItemCategory propertyItemCategory = categoryController.find(getPropertyItemCategoryId());
			item.setPropertyItemCategory(propertyItemCategory);
			PropertyTaxe propertyTaxe = taxController.find(getPropertyTaxId());
			item.setPropertyTaxe(propertyTaxe);
			itemController.edit(item);
			
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return SUCCESS;
	}
	
	
public String deleteItem() {
		
		PropertyItemManager itemController = new PropertyItemManager();
		UserLoginManager  userController = new UserLoginManager();
		try {
			
						
			
			
			PropertyItem item = itemController.find(getPropertyItemId());
						
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			item.setIsActive(false);
			item.setIsDeleted(true);
			item.setModifiedDate(new java.sql.Timestamp(date.getTime()));
			itemController.edit(item);
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}

	public Integer getPropertyItemCategoryId() {
		return propertyItemCategoryId;
	}

	public void setPropertyItemCategoryId(Integer propertyItemCategoryId) {
		this.propertyItemCategoryId = propertyItemCategoryId;
	}

	public Integer getPropertyItemId() {
		return propertyItemId;
	}

	public void setPropertyItemId(Integer propertyItemId) {
		this.propertyItemId = propertyItemId;
	}

	public Integer getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(Integer propertyId) {
		this.propertyId = propertyId;
	}
	
	
	
	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	
	public String getItemSku() {
		return itemSku;
	}

	public void setItemSku(String itemSku) {
		this.itemSku = itemSku;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getItemDescription() {
		return itemDescription;
	}

	public void setItemDescription(String itemDescription) {
		this.itemDescription = itemDescription;
	}

	public double getItemAmount() {
		return itemAmount;
	}

	public void setItemAmount(double itemAmount) {
		this.itemAmount = itemAmount;
	}

	public Integer getPropertyTaxId() {
		return propertyTaxId;
	}

	public void setPropertyTaxId(Integer propertyTaxId) {
		this.propertyTaxId = propertyTaxId;
	}
	
	public List<PropertyItem> getItemList() {
		return itemList;
	}

	public void setItemList(List<PropertyItem> itemList) {
		this.itemList = itemList;
	}

	public void setItem(PropertyItem item) {
		this.item = item;
	}
	
	

	


}
