
package com.ulopms.view;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
//import org.json.JSONArray;
//import org.json.JSONObject;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;

//import com.medwecare.controller.AwsObjectManager;
//import com.medwecare.controller.CommentManager;
//import com.medwecare.controller.CommentUserManager;
////import com.medwecare.controller.NotificationManager;
//import com.medwecare.controller.ProjectManager;

//import com.medwecare.model.AwsObject;
//import com.medwecare.model.Notification;
//import com.medwecare.model.ObjectComment;
//import com.medwecare.model.ObjectCommentUser;
//import com.medwecare.model.Project;







import com.opensymphony.xwork2.ActionSupport;

import freemarker.template.Configuration;
import freemarker.template.Template;

import javax.servlet.http.HttpServletRequest;

import jdk.nashorn.internal.parser.JSONParser;

import com.opensymphony.xwork2.ModelDriven;
import com.ulopms.controller.PmsBookingManager;
import com.ulopms.controller.PmsGuestManager;
import com.ulopms.controller.PmsStatusManager;
import com.ulopms.controller.PropertyAccommodationManager;
import com.ulopms.controller.BookingDetailManager;
import com.ulopms.controller.BookingGuestDetailManager;
import com.ulopms.controller.UserLoginManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.PmsBooking;
import com.ulopms.model.BookingDetail;
import com.ulopms.model.BookingGuestDetail;
import com.ulopms.model.PmsAvailableRooms;
import com.ulopms.model.PmsGuest;
import com.ulopms.model.PmsProperty;
import com.ulopms.model.PmsStatus;
import com.ulopms.model.PropertyAccommodation;
import com.ulopms.model.User;
import com.ulopms.util.Email;
import com.ulopms.util.Image;

public class BookingListAction extends ActionSupport implements
		ServletRequestAware, UserAware {

	private static final long serialVersionUID = 914982665758390091L;

	private Integer accommodationId;
	private String accommodationType;
	private String arrival;
	private String departure;
	private Integer adultsAllow;
	private Integer adultsCount;
	private double baseAmount;
	private double baseOriginAmount;
	private double totalAmount;
	private double advanceAmount;
	private double promotionAmount;
	private double extraAdultChildAmount;
	

	private double otaCommission;
	private double otaTax;
	private Integer usedRewardPoints;
	private Integer paynowDiscount;
    private String rates;

	private Integer childAllow;
	private Integer childCount;
	private Integer infantCount;
	private Integer extraInfant;
    private Integer extraChild;
	private Integer extraAdult;
	private double rateExtraChild;
	private double rateExtraAdult;
	private String mblExtraInfant;
    private String mblExtraChild;
	private String mblExtraAdult;
	private Integer minOccu;
	private double refund;
	private double addonAmount;
	private String addonDescription;
	private Integer rooms;
	public Integer statusId;
	public Integer sourceId;
	public double tax;
	private double total;
	private double randomNo;
	public Integer available;
	public Integer roomId;
	public Integer promotionId;
	public Integer offerId;
	public Integer discountId;
    public Integer diffDays;
    private Integer bookRooms;
    private Integer getRooms;
    private Integer bookNights;
    private Integer getNights;
    private Integer smartPriceId;
	private String promotionName;
	private Integer propertyId;
	private Integer inventoryId;
	private Integer propertyAccommodationId;
	private String inventoryCount;
	private String startBlockDate;
	
	private String ratePlanType;
	private String ratePlanName;
	private double ratePlanTariff;
	private Boolean planIsActive;
	private Integer ratePlanId;
	private String strStartDate;
	private String strEndDate;
	private Integer serialNo;
	private Timestamp blockedDate;
	private Timestamp createdDate;
	private Integer createdBy;
	private String ratePlanSymbolType;

	private String sourceName;
	private String sourceType;
	private Integer sourceTypeId;
	private double minimumAmount;
	private boolean otaEnable;
	private Integer inventoryRating;
	private double maximumAmount;
	private double sellRateAmount;
	private double otaRateAmount;
	private double cpAmount;
	

	private double epAmount;

	public Integer getSourceTypeId() {
		return sourceTypeId;
	}

	public void setSourceTypeId(Integer sourceTypeId) {
		this.sourceTypeId = sourceTypeId;
	}
	
	public String getMblExtraInfant() {
		return mblExtraInfant;
	}

	public void setMblExtraInfant(String mblExtraInfant) {
		this.mblExtraInfant = mblExtraInfant;
	}

	public String getMblExtraChild() {
		return mblExtraChild;
	}

	public void setMblExtraChild(String mblExtraChild) {
		this.mblExtraChild = mblExtraChild;
	}

	public String getMblExtraAdult() {
		return mblExtraAdult;
	}

	public void setMblExtraAdult(String mblExtraAdult) {
		this.mblExtraAdult = mblExtraAdult;
	}

	private BookingDetail bookingDetail;
    
	
	//private List<BookingListAction> data;
	
	private String jsonData;
	
	// TimeZone timeZone = TimeZone.getDefault();

	// private String data;

	// JSONObject jsonObj = new JSONObject(data);

	// JSONObject json = (JSONObject) new JSONParser().parse("json_data");

	private static final Logger logger = Logger
			.getLogger(BookingListAction.class);

	private User user;

	// @Override
	// public User getModel() {
	// return user;
	// }

	@Override
	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	private HttpServletRequest request;

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	public BookingListAction() {

	}

	public String execute() {
		return SUCCESS;
	}
	
	
public String addBookingDetails() {
		
		
		//UserLoginManager  userController = new UserLoginManager();
		try {
			
			
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
		    String a = getJsonData().toString();
			
		    
		    if(a!=null){
			//JSONObject jsonObj = new JSONObject(a);
		    }
			//List<String> list = new ArrayList<String>();
			//JSONArray array = jsonObj.getJSONArray("roomsbooked");
			//for(int i = 0 ; i < array.length() ; i++){
			  //  list.add(array.getJSONObject(i).getString("accommodationType"));
			//}
				
			
			//JSONObject json = (JSONObject)new JSONParser().parse(jsonObj);
			/*
	        for (int i = 0; i < data.size(); i++) {
	         }
	          
	         */  
			
			
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return SUCCESS;
	}

	public Integer getAccommodationId() {
		return accommodationId;
	}

	public void setAccommodationId(Integer accommodationId) {
		this.accommodationId = accommodationId;
	}

	public Integer getChildCount() {
		return childCount;
	}

	public void setChildCount(Integer childCount) {
		this.childCount = childCount;
	}

	public double getTax() {
		return tax;
	}

	public void setTax(double tax) {
		this.tax = tax;
	}

	public double getRefund() {
		return refund;
	}

	public void setRefund(double refund) {
		this.refund = refund;
	}

	public Integer getStatusId() {
		return statusId;
	}

	public void setStatusId(Integer statusId) {
		this.statusId = statusId;
	}

	public String getAccommodationType() {
		return accommodationType;
	}

	public void setAccommodationType(String accommodationType) {
		this.accommodationType = accommodationType;
	}

	public Integer getAdultsAllow() {
		return adultsAllow;
	}

	public void setAdultsAllow(Integer adultsAllow) {
		this.adultsAllow = adultsAllow;
	}

	public Integer getAdultsCount() {
		return adultsCount;
	}

	public void setAdultsCount(Integer adultsCount) {
		this.adultsCount = adultsCount;
	}

	public double getBaseAmount() {
		return baseAmount;
	}

	public void setBaseAmount(double baseAmount) {
		this.baseAmount = baseAmount;
	}

	public Integer getChildAllow() {
		return childAllow;
	}

	public void setChildAllow(Integer childAllow) {
		this.childAllow = childAllow;
	}

	public Integer getExtraChild() {
		return extraChild;
	}

	public void setExtraChild(Integer extraChild) {
		this.extraChild = extraChild;
	}

	public Integer getExtraAdult() {
		return extraAdult;
	}

	public void setExtraAdult(Integer extraAdult) {
		this.extraAdult = extraAdult;
	}

	public Integer getMinOccu() {
		return minOccu;
	}

	public void setMinOccu(Integer minOccu) {
		this.minOccu = minOccu;
	}

	public Integer getRooms() {
		return rooms;
	}

	public void setRooms(Integer rooms) {
		this.rooms = rooms;
	}
	
	public Integer getSourceId() {
		return sourceId;
	}

	public void setSourceId(Integer sourceId) {
		this.sourceId = sourceId;
	}
	
	public Integer getRoomId() {
		return roomId;
	}

	public void setRoomId(Integer roomId) {
		this.roomId = roomId;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public BookingDetail getBookingDetail() {
		return bookingDetail;
	}

	public void setBookingDetail(BookingDetail bookingDetail) {
		this.bookingDetail = bookingDetail;
	}

	
	public HttpServletRequest getRequest() {
		return request;
	}

	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getArrival() {
		return arrival;
	}

	public void setArrival(String arrival) {
		
		this.arrival = arrival;
	}

	public String getDeparture() {
		return departure;
	}

	public void setDeparture(String departure) {
		this.departure = departure;
	}
    
	
	 public double getRandomNo() {
			return this.randomNo;
	   }

     public void setRandomNo(double randomNo) {
			this.randomNo = randomNo;
	   }
	
     public Integer getAvailable() {
 		return available;
 	}

 	public void setAvailable(Integer available) {
 		this.available = available;
 	}
 	
 	
	public Integer getInfantCount() {
		return infantCount;
	}

	public void setInfantCount(Integer infantCount) {
		this.infantCount = infantCount;
	}

	public Integer getExtraInfant() {
		return extraInfant;
	}

	public void setExtraInfant(Integer extraInfant) {
		this.extraInfant = extraInfant;
	}
	
	
	public Integer getPromotionId() {
		return promotionId;
	}

	public void setPromotionId(Integer promotionId) {
		this.promotionId = promotionId;
	}

	public Integer getOfferId() {
		return offerId;
	}

	public void setOfferId(Integer offerId) {
		this.offerId = offerId;
	}

	public Integer getDiscountId() {
		return discountId;
	}

	public void setDiscountId(Integer discountId) {
		this.discountId = discountId;
	}
 	
	/*public List < BookingListAction > getData() {
	 * 
	 * 
	 * 
	        return data;
	}
	 
	  public void setData(List < BookingListAction > data) {
	        this.data = data;
	  } 
	  */
	
	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	
	  public String getJsonData() {
			return jsonData;
		}

		public void setJsonData(String jsonData) {
			this.jsonData = jsonData;
		}
		
		public Integer getDiffDays() {
			return diffDays;
		}

		public void setDiffDays(Integer diffDays) {
			this.diffDays = diffDays;
		}	
		
		public Integer getBookRooms() {
			return bookRooms;
		}

		public void setBookRooms(Integer bookRooms) {
			this.bookRooms = bookRooms;
		}

		public Integer getGetRooms() {
			return getRooms;
		}

		public void setGetRooms(Integer getRooms) {
			this.getRooms = getRooms;
		}

		public Integer getBookNights() {
			return bookNights;
		}

		public void setBookNights(Integer bookNights) {
			this.bookNights = bookNights;
		}

		public Integer getGetNights() {
			return getNights;
		}

		public void setGetNights(Integer getNights) {
			this.getNights = getNights;
		}
		
		public Integer getSmartPriceId() {
			return smartPriceId;
		}

		public void setSmartPriceId(Integer smartPriceId) {
			this.smartPriceId = smartPriceId;
		}

		public String getPromotionName() {
			return promotionName;
		}

		public void setPromotionName(String promotionName) {
			this.promotionName = promotionName;
		}
		
		public double getAdvanceAmount() {
			return advanceAmount;
		}

		public void setAdvanceAmount(double advanceAmount) {
			this.advanceAmount = advanceAmount;
		}
		
		public double getPromotionAmount() {
			return promotionAmount;
		}

		public void setPromotionAmount(double promotionAmount) {
			this.promotionAmount = promotionAmount;
		}

		public double getExtraAdultChildAmount() {
			return extraAdultChildAmount;
		}

		public void setExtraAdultChildAmount(double extraAdultChildAmount) {
			this.extraAdultChildAmount = extraAdultChildAmount;
		}

		public double getOtaCommission() {
			return otaCommission;
		}

		public void setOtaCommission(double otaCommission) {
			this.otaCommission = otaCommission;
		}
		
		public double getOtaTax() {
			return otaTax;
		}

		public void setOtaTax(double otaTax) {
			this.otaTax = otaTax;
		}
		
		public Integer getUsedRewardPoints() {
			return usedRewardPoints;
		}

		public void setUsedRewardPoints(Integer usedRewardPoints) {
			this.usedRewardPoints = usedRewardPoints;
		}

		public Integer getPaynowDiscount() {
			return paynowDiscount;
		}

		public void setPaynowDiscount(Integer paynowDiscount) {
			this.paynowDiscount = paynowDiscount;
		}

		public double getBaseOriginAmount() {
			return baseOriginAmount;
		}

		public void setBaseOriginAmount(double baseOriginAmount) {
			this.baseOriginAmount = baseOriginAmount;
		}
		
		public Integer getPropertyId() {
			return propertyId;
		}

		public void setPropertyId(Integer propertyId) {
			this.propertyId = propertyId;
		}

		public Integer getInventoryId() {
			return inventoryId;
		}

		public void setInventoryId(Integer inventoryId) {
			this.inventoryId = inventoryId;
		}

		public Integer getPropertyAccommodationId() {
			return propertyAccommodationId;
		}

		public void setPropertyAccommodationId(Integer propertyAccommodationId) {
			this.propertyAccommodationId = propertyAccommodationId;
		}

		public String getInventoryCount() {
			return inventoryCount;
		}

		public void setInventoryCount(String inventoryCount) {
			this.inventoryCount = inventoryCount;
		}

		public String getStartBlockDate() {
			return startBlockDate;
		}

		public void setStartBlockDate(String startBlockDate) {
			this.startBlockDate = startBlockDate;
		}
		
		public String getRatePlanType() {
			return ratePlanType;
		}

		public void setRatePlanType(String ratePlanType) {
			this.ratePlanType = ratePlanType;
		}

		public String getRatePlanName() {
			return ratePlanName;
		}

		public void setRatePlanName(String ratePlanName) {
			this.ratePlanName = ratePlanName;
		}

		public double getRatePlanTariff() {
			return ratePlanTariff;
		}

		public void setRatePlanTariff(double ratePlanTariff) {
			this.ratePlanTariff = ratePlanTariff;
		}

		public Boolean isPlanIsActive() {
			return planIsActive;
		}

		public void setPlanIsActive(Boolean planIsActive) {
			this.planIsActive = planIsActive;
		}

		public Integer getRatePlanId() {
			return ratePlanId;
		}

		public void setRatePlanId(Integer ratePlanId) {
			this.ratePlanId = ratePlanId;
		}
		
		public String getStrStartDate() {
			return strStartDate;
		}

		public void setStrStartDate(String strStartDate) {
			this.strStartDate = strStartDate;
		}

		public String getStrEndDate() {
			return strEndDate;
		}

		public void setStrEndDate(String strEndDate) {
			this.strEndDate = strEndDate;
		}

		public Integer getSerialNo() {
			return serialNo;
		}

		public void setSerialNo(Integer serialNo) {
			this.serialNo = serialNo;
		}
		
		public String getRatePlanSymbolType() {
			return ratePlanSymbolType;
		}

		public void setRatePlanSymbolType(String ratePlanSymbolType) {
			this.ratePlanSymbolType = ratePlanSymbolType;
		}
		
		public Timestamp getBlockedDate() {
			return blockedDate;
		}

		public void setBlockedDate(Timestamp blockedDate) {
			this.blockedDate = blockedDate;
		}
		
		public Timestamp getCreatedDate() {
			return createdDate;
		}

		public void setCreatedDate(Timestamp createdDate) {
			this.createdDate = createdDate;
		}

		public Integer getCreatedBy() {
			return createdBy;
		}

		public void setCreatedBy(Integer createdBy) {
			this.createdBy = createdBy;
		}
		public String getSourceName() {
			return sourceName;
		}

		public void setSourceName(String sourceName) {
			this.sourceName = sourceName;
		}

		public String getSourceType() {
			return sourceType;
		}

		public void setSourceType(String sourceType) {
			this.sourceType = sourceType;
		}
		public double getRateExtraChild() {
			return rateExtraChild;
		}

		public void setRateExtraChild(double rateExtraChild) {
			this.rateExtraChild = rateExtraChild;
		}

		public double getRateExtraAdult() {
			return rateExtraAdult;
		}

		public void setRateExtraAdult(double rateExtraAdult) {
			this.rateExtraAdult = rateExtraAdult;
		}
		
		public double getMinimumAmount() {
			return minimumAmount;
		}

		public void setMinimumAmount(double minimumAmount) {
			this.minimumAmount = minimumAmount;
		}

		public double getMaximumAmount() {
			return maximumAmount;
		}

		public void setMaximumAmount(double maximumAmount) {
			this.maximumAmount = maximumAmount;
		}
		
		public double getSellRateAmount() {
			return sellRateAmount;
		}

		public void setSellRateAmount(double sellRateAmount) {
			this.sellRateAmount = sellRateAmount;
		}

		public double getOtaRateAmount() {
			return otaRateAmount;
		}

		public void setOtaRateAmount(double otaRateAmount) {
			this.otaRateAmount = otaRateAmount;
		}

		public double getCpAmount() {
			return cpAmount;
		}

		public void setCpAmount(double cpAmount) {
			this.cpAmount = cpAmount;
		}

		public double getEpAmount() {
			return epAmount;
		}

		public void setEpAmount(double epAmount) {
			this.epAmount = epAmount;
		}
		
		public boolean isOtaEnable() {
			return otaEnable;
		}

		public void setOtaEnable(boolean otaEnable) {
			this.otaEnable = otaEnable;
		}
		
		public String getRates() {
			return rates;
		}

		public void setRates(String rates) {
			this.rates = rates;
		}
		
		public Integer getInventoryRating() {
			return inventoryRating;
		}

		public void setInventoryRating(Integer inventoryRating) {
			this.inventoryRating = inventoryRating;
		}

		public double getAddonAmount() {
			return addonAmount;
		}

		public void setAddonAmount(double addonAmount) {
			this.addonAmount = addonAmount;
		}

		public String getAddonDescription() {
			return addonDescription;
		}

		public void setAddonDescription(String addonDescription) {
			this.addonDescription = addonDescription;
		}
		
}
