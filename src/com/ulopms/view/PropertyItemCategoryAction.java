package com.ulopms.view;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.ServletRequestAware;




//import com.medwecare.controller.AwsObjectManager;
//import com.medwecare.controller.CommentManager;
//import com.medwecare.controller.CommentUserManager;
////import com.medwecare.controller.NotificationManager;
//import com.medwecare.controller.ProjectManager;

//import com.medwecare.model.AwsObject;
//import com.medwecare.model.Notification;
//import com.medwecare.model.ObjectComment;
//import com.medwecare.model.ObjectCommentUser;
//import com.medwecare.model.Project;









import com.opensymphony.xwork2.ActionSupport;

import freemarker.template.Configuration;
import freemarker.template.Template;

import javax.servlet.http.HttpServletRequest;

import com.opensymphony.xwork2.ModelDriven;
import com.ulopms.controller.PmsGuestManager;
import com.ulopms.controller.PmsPropertyManager;
import com.ulopms.controller.PropertyItemCategoryManager;
import com.ulopms.controller.UserLoginManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.PmsGuest;
import com.ulopms.model.PmsProperty;
import com.ulopms.model.PropertyItemCategory;
import com.ulopms.model.User;
import com.ulopms.util.Email;
import com.ulopms.util.Image;

public class PropertyItemCategoryAction extends ActionSupport implements
ServletRequestAware,SessionAware, UserAware{
     
	

	private static final long serialVersionUID = 914982665758390091L;

	private Integer propertyItemCategoryId;
	private Integer propertyId;
	private String categoryCode;
	private String categoryName;
	
	private PropertyItemCategory category;
	
	private List<PropertyItemCategory> categoryList;
	

	private static final Logger logger = Logger.getLogger(PropertyItemCategoryAction.class);

	private HttpSession session;

	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}
	
	private SessionMap<String,Object> sessionMap;  
	  
	//getters and setters  
	  
	@Override  
	public void setSession(Map<String, Object> map) {  
	    sessionMap=(SessionMap)map;  
	}  
	
	
	private User user;

	
	// @Override
	// public User getModel() {
	// return user;
	// }

	@Override
	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	
	private HttpServletRequest request;

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	public PropertyItemCategoryAction() {

	}

	public String execute() {

		//this.patientId = getPatientId();
		return SUCCESS;
	}

	
	public String getCategory() throws IOException {
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PropertyItemCategoryManager categoryController = new PropertyItemCategoryManager();
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");

			PropertyItemCategory category = categoryController.find(getPropertyItemCategoryId());
			
				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"DT_RowId\":\"" + category.getPropertyItemCategoryId() + "\"";
				jsonOutput += ",\"categoryCode\":\"" + category.getCategoryCode()+ "\"";
				jsonOutput += ",\"categoryName\":\"" + category.getCategoryName()+ "\"";
				
				jsonOutput += "}";

			

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	public String getCategories() throws IOException {
		

		try {
			this.propertyId = (Integer) sessionMap.get("propertyId");
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PropertyItemCategoryManager categoryController = new PropertyItemCategoryManager();
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");

			this.categoryList = categoryController.list(getPropertyId());
			for (PropertyItemCategory category : categoryList) {

				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"DT_RowId\":\"" + category.getPropertyItemCategoryId() + "\"";
				jsonOutput += ",\"categoryCode\":\"" + category.getCategoryCode()+ "\"";
				jsonOutput += ",\"categoryName\":\"" + category.getCategoryName()+ "\"";
				
				jsonOutput += "}";


			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}

	public String addCategory() {
		
		this.propertyId = (Integer) sessionMap.get("propertyId");
		PropertyItemCategoryManager categoryController = new PropertyItemCategoryManager();
		//UserLoginManager  userController = new UserLoginManager();
		PmsPropertyManager propertyController = new PmsPropertyManager();
		try {
			
						
			PropertyItemCategory category = new PropertyItemCategory();
			
			
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			category.setCategoryCode(getCategoryCode());
			category.setCategoryName(getCategoryName());
			category.setIsActive(true);
			category.setIsDeleted(false);
			PmsProperty property = propertyController.find(getPropertyId());
			category.setPmsProperty(property);
			categoryController.add(category);
			
			
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return SUCCESS;
	}
	
	public String editCategory() {
		PropertyItemCategoryManager categoryController = new PropertyItemCategoryManager();
		UserLoginManager  userController = new UserLoginManager();
		try {
			
						
			PropertyItemCategory category = categoryController.find(getPropertyItemCategoryId());
			
			
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			category.setCategoryCode(getCategoryCode());
			category.setCategoryName(getCategoryName());
			category.setIsActive(true);
			category.setIsDeleted(false);
			
			categoryController.edit(category);
			
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return SUCCESS;
	}
	
	public String deleteCategory() {
		
		PropertyItemCategoryManager categoryController = new PropertyItemCategoryManager();
		UserLoginManager  userController = new UserLoginManager();
		try {
			
						
			
			
			PropertyItemCategory category = categoryController.find(getPropertyItemCategoryId());
						
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			category.setIsActive(false);
			category.setIsDeleted(true);
			category.setModifiedDate(new java.sql.Timestamp(date.getTime()));
			categoryController.edit(category);
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}
	

	public Integer getPropertyItemCategoryId() {
		return propertyItemCategoryId;
	}

	public void setPropertyItemCategoryId(Integer propertyItemCategoryId) {
		this.propertyItemCategoryId = propertyItemCategoryId;
	}

	public Integer getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(Integer propertyId) {
		this.propertyId = propertyId;
	}

	public String getCategoryCode() {
		return categoryCode;
	}

	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public List<PropertyItemCategory> getCategoryList() {
		return categoryList;
	}

	public void setCategoryList(List<PropertyItemCategory> categoryList) {
		this.categoryList = categoryList;
	}

	public void setCategory(PropertyItemCategory category) {
		this.category = category;
	}
	
	

	


}
