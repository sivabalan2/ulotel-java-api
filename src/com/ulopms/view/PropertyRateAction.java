package com.ulopms.view;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Calendar;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TimeZone;
import java.util.TreeSet;

import com.ulopms.util.DateUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.commons.io.FileUtils;
import org.apache.derby.tools.sysinfo;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.ServletRequestAware;




//import com.medwecare.controller.AwsObjectManager;
//import com.medwecare.controller.CommentManager;
//import com.medwecare.controller.CommentUserManager;
////import com.medwecare.controller.NotificationManager;
//import com.medwecare.controller.ProjectManager;

//import com.medwecare.model.AwsObject;
//import com.medwecare.model.Notification;
//import com.medwecare.model.ObjectComment;
//import com.medwecare.model.ObjectCommentUser;
//import com.medwecare.model.Project;

























import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.jsoup.select.Evaluator.IsEmpty;

import sun.security.action.GetIntegerAction;

import com.opensymphony.xwork2.ActionSupport;

import freemarker.template.Configuration;
import freemarker.template.Template;

import javax.servlet.http.HttpServletRequest;

import com.opensymphony.xwork2.ModelDriven;
import com.ulopms.controller.BookingDetailManager;
import com.ulopms.controller.OtaHotelsManager;
import com.ulopms.controller.PmsBookingManager;
import com.ulopms.controller.PmsDayManager;
import com.ulopms.controller.PmsGuestManager;
import com.ulopms.controller.PmsPropertyManager;
import com.ulopms.controller.PmsSourceManager;
import com.ulopms.controller.PmsSourceTypeManager;
import com.ulopms.controller.PromotionDetailManager;
import com.ulopms.controller.PromotionManager;
import com.ulopms.controller.PropertyAccommodationInventoryManager;
import com.ulopms.controller.PropertyAccommodationRoomManager;
import com.ulopms.controller.PropertyDiscountManager;
import com.ulopms.controller.PropertyItemCategoryManager;
import com.ulopms.controller.PropertyAccommodationManager;
import com.ulopms.controller.PropertyRateDetailManager;
import com.ulopms.controller.PropertyRateManager;
import com.ulopms.controller.PropertyRatePlanDetailManager;
import com.ulopms.controller.PropertyRatePlanManager;
import com.ulopms.controller.PropertyTaxeManager;
import com.ulopms.controller.UserLoginManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.AccommodationAmenity;
import com.ulopms.model.AccommodationRoom;
import com.ulopms.model.OtaHotelDetails;
import com.ulopms.model.OtaHotels;
import com.ulopms.model.PmsAvailableRooms;
import com.ulopms.model.PmsDays;
import com.ulopms.model.PmsGuest;
import com.ulopms.model.PmsPromotionDetails;
import com.ulopms.model.PmsPromotions;
import com.ulopms.model.PmsProperty;
import com.ulopms.model.PmsSmartPrice;
import com.ulopms.model.PmsSource;
import com.ulopms.model.PmsSourceType;
import com.ulopms.model.PropertyAccommodationInventory;
import com.ulopms.model.PropertyDiscount;
import com.ulopms.model.PropertyItem;
import com.ulopms.model.PropertyItemCategory;
import com.ulopms.model.PropertyAccommodation;
import com.ulopms.model.PropertyRate;
import com.ulopms.model.PropertyRateDetail;
import com.ulopms.model.PropertyRateOrder;
import com.ulopms.model.PropertyRatePlan;
import com.ulopms.model.PropertyRatePlanDetail;
import com.ulopms.model.PropertyTaxe;
import com.ulopms.model.User;
import com.ulopms.util.Email;
import com.ulopms.util.Image;

public class PropertyRateAction extends ActionSupport implements
ServletRequestAware,SessionAware, UserAware{
     
	

	private static final long serialVersionUID = 914982665758390091L;
    
	
	private Integer  propertyRateId;
	private Integer propertyId;
	private Integer sourceId;
	private Integer propertyAccommodationId;
	private String  rateName;
	private String  checkedDays;
	private Integer  propertyRateDetailsId;
	private Timestamp  startDate;
	private Timestamp  endDate;
	private Integer  orderBy;
	private Timestamp arrivalDate;
    private Timestamp departureDate;
    private String chartDate;
	public Integer accommodationId;
	private Timestamp modStartDate;
    private Timestamp modEndDate;
    private String modSources;
    private String modSourceTypes;
    private Integer sourceTypeId;
    private Integer smartPricePropertyAccommodationId;
    private Timestamp smartPriceStartDate;
    private Timestamp smartPriceEndDate;
    private Boolean smartPriceIsActiveOrNot;
    private String resultDate;
    private String updatePropertyRateId;
    private Integer userId;
	private String strStartDate;
	private String strEndDate;
	private String strModStartDate;
	private String strModEndDate;
	private String strSmartPriceStartDate;
	private String strSmartPriceEndDate;
	private Integer baseAmount;
	private Integer extraAdult;
	private Integer extraChild;
	private Integer extraInfant;
	private double minimumAmount;
	private double maximumAmount;
	private Integer contractTypeId;
	private String contractTypeName;

	List<BookingListAction> array = new ArrayList<BookingListAction>();

    public List<BookingListAction> getArray() {
        return array;
    }
    
    public void setArray(List<BookingListAction> array) {
        this.array = array;
    }
	

	private PropertyRate rate;
	
	
	private List<PropertyRate> rateList;
	
	private List<PropertyRateDetail> rateDetailList;
	
	private List<PmsSource> sourceList;
	
	private List<PmsDays> daylist;
	
	private List<PropertyRate> listSmartPrices;
	
	private List<PropertyRate> listPropertyRates;
	
	List<PropertyRateOrder> orders = new ArrayList<PropertyRateOrder>();

	private static final Logger logger = Logger.getLogger(PropertyRateAction.class);
	
	private long roomCnt;
	
	public long getRoomCnt() {
		return roomCnt;
	}

	public void setRoomCnt(long roomCnt) {
		this.roomCnt = roomCnt;
	}

	private HttpSession session;

	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}
	
	private SessionMap<String,Object> sessionMap;  
	  
	//getters and setters  
	  
	@Override  
	public void setSession(Map<String, Object> map) {  
	    sessionMap=(SessionMap)map;  
	}  
	
	private User user;

	
	// @Override
	// public User getModel() {
	// return user;
	// }

	@Override
	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	
	private HttpServletRequest request;

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	public PropertyRateAction() {

	}

	public String execute() {

		//this.patientId = getPatientId();
		return SUCCESS;
	}
	
public String getDay() throws IOException {
		
		this.propertyId = (Integer) sessionMap.get("propertyId");
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PmsDayManager dayController = new PmsDayManager();
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");

			this.daylist = dayController.list();
			for (PmsDays days : daylist) {

				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"dayId\":\"" + days.getDayId() + "\"";
				String strDays = days.getDaysOfWeek();
				String firstLetterUpperCase=strDays.substring(0, 1).toUpperCase() +  strDays.substring(1);
				jsonOutput += ",\"daysOfWeek\":\"" + firstLetterUpperCase+ "\"";
				jsonOutput += ",\"dayPart\":\"" + days.getDayPart()+ "\"";
				jsonOutput += ",\"selected\":\"" + false+ "\"";
				//jsonOutput += ",\"taxCode\":\"" + taxe.getTaxCode()+ "\"";
				//jsonOutput += ",\"taxAmount\":\"" + taxe.getTaxAmount()+ "\"";
				jsonOutput += "}";


			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}

public String getRoomRate() throws IOException {
    
    this.propertyId = (Integer) sessionMap.get("propertyId");
    
    this.arrivalDate = getArrivalDate();
   sessionMap.put("arrivalDate",arrivalDate);
        
    this.departureDate = getDepartureDate();
    sessionMap.put("departureDate",departureDate);
 
    try {        String jsonOutput = "";
        HttpServletResponse response = ServletActionContext.getResponse();
    
        
        PropertyRateManager rateController = new PropertyRateManager();
        PropertyRateDetailManager rateDetailController = new PropertyRateDetailManager();        
        response.setContentType("application/json");
        Calendar cal = Calendar.getInstance();
        cal.setTime(this.departureDate);
       cal.add(Calendar.DATE, -1);
        String startDate = new SimpleDateFormat("yyyy-MM-dd").format(this.arrivalDate);
        String endDate = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
        DateTime arrival = DateTime.parse(startDate);
        DateTime departure = DateTime.parse(endDate);
        java.util.Date arrival1 = arrival.toDate();
        java.util.Date departure1 = departure.toDate();        
                
                int dateCount=0;
                 double amount=0;
                 double totalAmount=0;
                List<DateTime> between = DateUtil.getDateRange(arrival, departure);
                for (DateTime d : between){
                                        if (!jsonOutput.equalsIgnoreCase(""))
                        jsonOutput += ",{";
                    
                    else
                        jsonOutput += "{";
                
                List<PropertyRate> propertyRateList = rateController.list1(getPropertyAccommodationId(),getSourceId(),d.toDate());            
            
             if(propertyRateList.size()>0)
             {
                
                 for (PropertyRate rates : propertyRateList)
                    
                    
                {        
                    
                     jsonOutput += "\"rateId\":\"" + rates.getPropertyRateId() + "\"";
                 jsonOutput += ",\"rateName\":\"" + rates.getRateName()+ "\"";
                 jsonOutput += ",\"accommodationType\":\"" + rates.getPropertyAccommodation().getAccommodationType()+ "\"";
                 jsonOutput += ",\"orderBy\":\"" + rates.getOrderBy()+ "\"";
                 String start = new SimpleDateFormat("MM/dd/yyyy").format(rates.getStartDate());
                 String end = new SimpleDateFormat("MM/dd/yyyy").format(rates.getEndDate());
                 jsonOutput += ",\"startDate\":\"" + start + "\"";
                 jsonOutput += ",\"endDate\":\"" + end + "\"";
                 
                 DateFormat f = new SimpleDateFormat("EEEE");
                 List<PropertyRateDetail> rateDetailList =   rateDetailController.list(rates.getPropertyRateId(),f.format(d.toDate()).toLowerCase());
                 if(rateDetailList.size()>0)
                 {
                     for (PropertyRateDetail rate : rateDetailList)
                     {
                         amount +=  rate.getBaseAmount();
                     }
                 }
                 else
                 {
                    // amount += available.getBaseAmount();
                 }                
                
                }
                // totalAmount = amount;
                
                 totalAmount = amount;
                 jsonOutput += ",\"baseAmount\":\"" + totalAmount + "\"";
                    
            
             } 
            
            //PropertyRateDetail rateDetail = rateDetailController.find1(rates.getPropertyRateId());        
            //jsonOutput += ",\"baseAmount\":\"" + rateDetail.getBaseAmount()+ "\"";
            //jsonOutput += ",\"taxCode\":\"" + taxe.getTaxCode()+ "\"";
            //jsonOutput += ",\"taxAmount\":\"" + taxe.getTaxAmount()+ "\"";
            jsonOutput += "}";            
                }    
            
                
                
                
                
                       response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
            } catch (Exception e) {
        logger.error(e);
        e.printStackTrace();
    } finally {  
    	
    }   
    return null;
    }

public String getRates() throws IOException {
		
		this.propertyId = (Integer) sessionMap.get("propertyId");
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PropertyRateManager rateController = new PropertyRateManager();
			PropertyRateDetailManager rateDetailController = new PropertyRateDetailManager();
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");

			this.rateList = rateController.list(getPropertyId());
			for (PropertyRate rates : rateList) {

				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"rateId\":\"" + rates.getPropertyRateId() + "\"";
				jsonOutput += ",\"rateName\":\"" + rates.getRateName()+ "\"";
				jsonOutput += ",\"accommodationType\":\"" + rates.getPropertyAccommodation().getAccommodationType()+ "\"";
				jsonOutput += ",\"orderBy\":\"" + rates.getOrderBy()+ "\"";
				String start = new SimpleDateFormat("MM/dd/yyyy").format(rates.getStartDate());
				String end = new SimpleDateFormat("MM/dd/yyyy").format(rates.getEndDate());
				jsonOutput += ",\"startDate\":\"" + start + "\"";
				jsonOutput += ",\"endDate\":\"" + end + "\"";				
				PropertyRateDetail rateDetail = rateDetailController.find1(rates.getPropertyRateId());        
				jsonOutput += ",\"baseAmount\":\"" + rateDetail.getBaseAmount()+ "\"";
				//jsonOutput += ",\"taxCode\":\"" + taxe.getTaxCode()+ "\"";
				//jsonOutput += ",\"taxAmount\":\"" + taxe.getTaxAmount()+ "\"";
				jsonOutput += "}";


			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}

public String getRate() throws IOException {
	
	this.propertyId = (Integer) sessionMap.get("propertyId");
	try {

		String jsonOutput = "";
		HttpServletResponse response = ServletActionContext.getResponse();
	
		
		PropertyRateManager rateController = new PropertyRateManager();
		PropertyRateDetailManager rateDetailController = new PropertyRateDetailManager();
		PmsDayManager dayController = new PmsDayManager();
		//this.familyRegisterList = familyController.list(getUser().getUserid());
		//model = familyRegisterList;
		response.setContentType("application/json");

		List<PropertyRate> rateList =  rateController.list1(getPropertyRateId());
		for (PropertyRate rate : rateList) {

			
			if (!jsonOutput.equalsIgnoreCase(""))
				jsonOutput += ",{";
			else
				jsonOutput += "{";
			jsonOutput += "\"rateId\":\"" + rate.getPropertyRateId() + "\"";
			jsonOutput += ",\"rateName\":\"" + rate.getRateName()+ "\"";
			String checkIn = new SimpleDateFormat("MM/dd/yyyy").format(rate.getStartDate());
			String checkOut = new SimpleDateFormat("MM/dd/yyyy").format(rate.getEndDate());
			jsonOutput += ",\"startDate\":\"" + checkIn + "\"";
			jsonOutput += ",\"endDate\":\"" + checkOut + "\"";
			jsonOutput += ",\"sourceId\":\"" + rate.getPmsSource().getSourceId()+ "\"";		
			jsonOutput += ",\"accommodationType\":\"" + rate.getPropertyAccommodation().getAccommodationType()+ "\"";
			jsonOutput += ",\"accommodationId\":\"" + rate.getPropertyAccommodation().getAccommodationId()+ "\"";
			PropertyRateDetail rateDetail = rateDetailController.find1(rate.getPropertyRateId());        
			jsonOutput += ",\"baseAmount\":\"" + rateDetail.getBaseAmount()+ "\"";
			
			
			String jsonDays ="";
			
	//		if(propertyRateDetail.size()>0)
	//		{
			
			this.daylist = dayController.list(); 
				for(PmsDays days : daylist){
					
					List<PropertyRateDetail> rateDetailList = rateDetailController.list(rate.getPropertyRateId(),days.getDayPart());
					if (!jsonDays.equalsIgnoreCase(""))
						
						jsonDays += ",{";
					else
						jsonDays += "{";
					
					//PropertyRateDetail propertyRateDetail1 = rateDetailController.find(ratedetail.getPropertyRateDetailsId());
					
					
					jsonDays += "\"daysOfWeek\":\"" + days.getDaysOfWeek()+ "\"";
					jsonDays += ",\"dayPart\":\"" + days.getDayPart()+ "\"";
					jsonDays += ",\"isActive\":\"" + days.getIsActive()+ "\"";					
					if(rateDetailList.size()>0)
						jsonDays += ",\"status\":\"true\""; else jsonDays += ",\"status\":\"false\"";
					jsonDays += "}";
				
				}
				jsonOutput += ",\"days\":[" + jsonDays+ "]";
//			}
			
			//jsonOutput += ",\"taxCode\":\"" + taxe.getTaxCode()+ "\"";
			//jsonOutput += ",\"taxAmount\":\"" + taxe.getTaxAmount()+ "\"";
			jsonOutput += "}";


		}


		response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
		

	} catch (Exception e) {
		logger.error(e);
		e.printStackTrace();
	} finally {

	}

	return null;

}

	public String addSmartPriceRate(){
		this.propertyId = (Integer) sessionMap.get("propertyId");
		
		/*this.smartPriceStartDate = getSmartPriceStartDate();
		sessionMap.put("smartPriceStartDate",smartPriceStartDate); 
	      
		this.smartPriceEndDate = getSmartPriceEndDate();
	    sessionMap.put("smartPriceStartDate",smartPriceEndDate); */
			
		this.smartPricePropertyAccommodationId = getSmartPricePropertyAccommodationId();
		sessionMap.put("smartPricePropertyAccommodationId",smartPricePropertyAccommodationId);
		
		this.smartPriceIsActiveOrNot = getSmartPriceIsActiveOrNot();
	    sessionMap.put("smartPriceIsActive",smartPriceIsActiveOrNot); 
	    
	    
	    
	    try{
	    	DateFormat format = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
		    java.util.Date dateStart = format.parse(getStrSmartPriceStartDate());
		    java.util.Date dateEnd = format.parse(getStrSmartPriceEndDate());
		    SimpleDateFormat sdfformat = new SimpleDateFormat("yyyy-MM-dd");
		    this.userId=(Integer)sessionMap.get("userId");
			java.util.Date date=new java.util.Date();
        	Calendar calDate=Calendar.getInstance();
        	calDate.setTime(date);
        	calDate.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
        	String strCurrentDate = new SimpleDateFormat("yyyy-MM-dd").format(calDate.getTime());
		    java.util.Date currentDate = sdfformat.parse(strCurrentDate);
		    Timestamp tsCurrentDate = new Timestamp(currentDate.getTime());
	    	
    	    
		    Calendar calCheckStart=Calendar.getInstance();
		    calCheckStart.setTime(dateStart);
		    java.util.Date checkInDate = calCheckStart.getTime();
		    this.smartPriceStartDate=new Timestamp(checkInDate.getTime());
		    sessionMap.put("smartPriceStartDate",smartPriceStartDate); 
		    
		    Calendar calCheckEnd=Calendar.getInstance();
		    calCheckEnd.setTime(dateEnd);
		    java.util.Date checkOutDate = calCheckEnd.getTime();
		    this.smartPriceEndDate=new Timestamp(checkOutDate.getTime());
		    sessionMap.put("smartPriceStartDate",smartPriceEndDate); 
	    	
		    
	    	if(this.smartPriceStartDate==null || this.smartPriceEndDate==null || this.smartPricePropertyAccommodationId==null || this.smartPriceIsActiveOrNot==null){
	    		return null;
	    	}
	    	
	    	
		    
		    
	    	String jsonOutput="";
	    	HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("application/json");
	    	PropertyRate rate = new PropertyRate();
	    	PmsSourceManager sourceController = new PmsSourceManager();
			PropertyRateManager rateController = new PropertyRateManager();
			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
			PmsPropertyManager propertyController = new PmsPropertyManager();
			Boolean rateChk=false;
			String strStartDate=null,strEndDate=null;
			List<PropertyRate> listRateCheckAll=null;
			SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
			
	    	strStartDate=format1.format(getSmartPriceStartDate());
    		strEndDate=format1.format(getSmartPriceEndDate());
    		DateTime FromDate = DateTime.parse(strStartDate);
	        DateTime ToDate = DateTime.parse(strEndDate);
    		
	        List<DateTime> dateBetween = DateUtil.getDateRange(FromDate, ToDate);
	        Iterator<DateTime> iterDateValues=dateBetween.iterator();
	        while(iterDateValues.hasNext()){
	        	DateTime dateValues=iterDateValues.next();
	        	java.util.Date DateValues = dateValues.toDate();
	        	listRateCheckAll=rateController.listSmartPriceCheckDetails(propertyId, smartPricePropertyAccommodationId,DateValues);
	        	if(listRateCheckAll.size()>0 && !listRateCheckAll.isEmpty()){
	        		rateChk=true;
	        	}
	        }
			
			if (!jsonOutput.equalsIgnoreCase(""))
				jsonOutput += ",{";
			else
				jsonOutput += "{";
			if(rateChk){
				jsonOutput += "\"check\":\"" + rateChk+ "\"";
			}
			else if(!rateChk){
				
				List<PropertyRate> rateList =  rateController.list(propertyId,smartPricePropertyAccommodationId);            
	            Object orderBy = rateList.size();            
	            String converted = orderBy .toString();            
	            int orderBy1 = 0;
	            orderBy1 = Integer.parseInt(converted) + 1;
	            
	            rate.setIsActive(true);
				rate.setIsDeleted(false);
				rate.setStartDate(smartPriceStartDate);
				rate.setEndDate(smartPriceEndDate);
				rate.setOrderBy(orderBy1);
				rate.setRateName("Smart Price");
				rate.setCreatedBy(this.userId);
				rate.setCreatedDate(tsCurrentDate);
				rate.setSmartPriceIsActive(getSmartPriceIsActiveOrNot());
				PmsProperty property = propertyController.find(getPropertyId());
				rate.setPmsProperty(property);
				PropertyAccommodation propertyAccommodation = accommodationController.find(smartPricePropertyAccommodationId);
				rate.setPropertyAccommodation(propertyAccommodation);
				PmsSource source = sourceController.find(1);
				rate.setPmsSource(source);
				
				rateController.add(rate);
				this.propertyRateId = rate.getPropertyRateId();
				sessionMap.put("propertyRateId",propertyRateId);
				this.propertyRateId = (Integer) sessionMap.get("propertyRateId");
				
				jsonOutput += "\"check\":\"" + rateChk+ "\"";
				
			}
			jsonOutput += "}";
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
	    }
	    catch(Exception e)
	    {
	    	logger.error(e);
			e.printStackTrace();
	    }finally{
	    	
	    }
	    
		return null;
	}
	
	public String addMMRate(){

		try {
			
			this.propertyId = (Integer) sessionMap.get("propertyId");
			PmsSourceManager sourceController = new PmsSourceManager();
			PropertyRateManager rateController = new PropertyRateManager();
			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
			OtaHotelsManager otaController = new OtaHotelsManager(); 
			//UserLoginManager  userController = new UserLoginManager();
			PmsPropertyManager propertyController = new PmsPropertyManager();
			
			DateFormat format = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
		    java.util.Date dateStart = format.parse(getStrModStartDate());
		    java.util.Date dateEnd = format.parse(getStrModEndDate());
		   
		    String jsonOutput="";
	    	HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("application/json");
			
		    Calendar calCheckStart=Calendar.getInstance();
		    calCheckStart.setTime(dateStart);
		    java.util.Date checkInDate = calCheckStart.getTime();
		    this.modStartDate=new Timestamp(checkInDate.getTime());
		    
		    Calendar calCheckEnd=Calendar.getInstance();
		    calCheckEnd.setTime(dateEnd);
		    java.util.Date checkOutDate = calCheckEnd.getTime();
		    this.modEndDate=new Timestamp(checkOutDate.getTime());
		    
		    
	        PropertyRateDetailManager rateDetailController = new PropertyRateDetailManager();    
	        PropertyRatePlanManager propertyRateController = new PropertyRatePlanManager();
	        PropertyRatePlanDetailManager ratePlanDetailsController=new PropertyRatePlanDetailManager();
			PropertyRate rate = new PropertyRate();
            
			this.userId=(Integer)sessionMap.get("userId");
			SimpleDateFormat sdfformat = new SimpleDateFormat("yyyy-MM-dd");
		    java.util.Date date=new java.util.Date();
        	Calendar calDate=Calendar.getInstance();
        	calDate.setTime(date);
        	calDate.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
        	String strCurrentDate = new SimpleDateFormat("yyyy-MM-dd").format(calDate.getTime());
		    java.util.Date currentDate = sdfformat.parse(strCurrentDate);
		    Timestamp tsCurrentDate = new Timestamp(currentDate.getTime());
		    PmsSourceTypeManager sourceTypeController = new PmsSourceTypeManager();
			List<PropertyRate> rateList =  rateController.list(getPropertyId(),getPropertyAccommodationId());            
            Object orderBy = rateList.size();            
            String converted = orderBy .toString();            
            int orderBy1 = 0;
            orderBy1 = Integer.parseInt(converted) + 1;
            
			//String[] stringArray = getModSourceTypes().split("\\s*,\\s*");
			int RateId=0;
			String strRateId="";
				
			rate.setIsActive(true);
			rate.setIsDeleted(false);
			rate.setStartDate(modStartDate);
			rate.setEndDate(modEndDate);
			rate.setOrderBy(orderBy1);
			//rate.setRateName(getRateName());
			rate.setSmartPriceIsActive(false);
			rate.setCreatedBy(this.userId);
			rate.setCreatedDate(tsCurrentDate);
			PmsProperty property = propertyController.find(getPropertyId());
			rate.setPmsProperty(property);
			PropertyAccommodation propertyAccommodation = accommodationController.find(getPropertyAccommodationId());
			rate.setPropertyAccommodation(propertyAccommodation);
			PmsSourceType sourcetype = sourceTypeController.find(1);
			rate.setPmsSourceType(sourcetype);
			rateController.add(rate);
			RateId=rate.getPropertyRateId();
			strRateId=String.valueOf(RateId)+","+strRateId;
			this.propertyRateId = rate.getPropertyRateId();
			sessionMap.put("propertyRateId",propertyRateId);
			sessionMap.put("RateId", strRateId);
		
			this.propertyRateId = (Integer) sessionMap.get("propertyRateId");
			
			int sourceId = 3;
			this.minimumAmount=getMinimumAmount();
			double otaAmount=minimumAmount+(minimumAmount*15/100);
		   /* OtaHotels otaHotels =  otaController.getOtaHotels(this.propertyId,sourceId);
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpPost postRequest = new HttpPost("https://in.goibibo.com/api/chmv2/updateroomrates/?bearer_token="+otaHotels.getOtaBearerToken()+"&channel_token="+otaHotels.getOtaChannelToken()+"");
			
			List <OtaHotelDetails> otaDetailList = otaController.listDetails(otaHotels.getOtaHotelCode(),getPropertyAccommodationId());
			
			for(OtaHotelDetails otaDetail : otaDetailList){

				if(otaDetail.getPropertyRatePlan().getRatePlanId() == 1){
					
					int rateid = 1;
					PropertyRatePlanDetail ratePlanDetails=ratePlanDetailsController.listRateDetails(getPropertyId(),getPropertyAccommodationId(),rateid);
					double epTotal = otaAmount - ratePlanDetails.getTariffAmount();
				String xmlBody = "<?xml version='1.0' encoding='UTF-8' ?>"+
			    "<Website Name='ingoibibo' HotelCode='"+otaHotels.getOtaHotelCode()+"' Version='1'>"+
			            "<RatePlan>"+
			            "<RatePlanCode>"+otaDetail.getOtaRateplanId()+"</RatePlanCode>"+
			            "<StartDate Format='yyyy-mm-dd'>"+startDate+"</StartDate>"+
			            "<EndDate Format='yyyy-mm-dd'>"+endDate+"</EndDate>"+
			            "<SingleOccupancyRates>"+				            
			            "<SellRate>"+epTotal+"</SellRate>"+
			            "</SingleOccupancyRates>"+
			            "<DoubleOccupancyRates>"+				            
			            "<SellRate>"+epTotal+"</SellRate>"+
			            "</DoubleOccupancyRates>"+
			            "<TripleOccupancyRates>"+				            
			            "<SellRate>"+epTotal+"</SellRate>"+
			            "</TripleOccupancyRates>"+
			            "<ExtraChildCharge>"+getExtraChild()+"</ExtraChildCharge>"+
			            "<ExtraAdultCharge>"+getExtraAdult()+"</ExtraAdultCharge>"+
			            "<InfantCharge>"+getExtraInfant()+"</InfantCharge>"+
			            "<DaysOfWeek Mon='"+getDaysOfWeek("1")+"' Tue='"+getDaysOfWeek("2")+"' Wed='"+getDaysOfWeek("3")+"' Thu='"+getDaysOfWeek("4")+"' Fri='"+getDaysOfWeek("5")+"' Sat='"+getDaysOfWeek("6")+"' Sun='"+getDaysOfWeek("7")+"'></DaysOfWeek>"+
			            "<ContractType b2b='False' b2c='True' mobile='False' fph='False'></ContractType>"+
			            "<Currency>INR</Currency>"+
			            "</RatePlan>"+
			      "</Website>";
					
					StringEntity input = new StringEntity(xmlBody);
					input.setContentType("text/xml");
					postRequest.setEntity(input);
					HttpResponse httpResponse = httpClient.execute(postRequest);
					HttpEntity entity = httpResponse.getEntity();
		            // Read the contents of an entity and return it as a String.
		            String content = EntityUtils.toString(entity);
				}else if(otaDetail.getPropertyRatePlan().getRatePlanId() == 2){
					
					String xmlBody = "<?xml version='1.0' encoding='UTF-8' ?>"+
						    "<Website Name='ingoibibo' HotelCode='"+otaHotels.getOtaHotelCode()+"' Version='1'>"+
						            "<RatePlan>"+
						            "<RatePlanCode>"+otaDetail.getOtaRateplanId()+"</RatePlanCode>"+
						            "<StartDate Format='yyyy-mm-dd'>"+startDate+"</StartDate>"+
						            "<EndDate Format='yyyy-mm-dd'>"+endDate+"</EndDate>"+
						            "<SingleOccupancyRates>"+				            
						            "<SellRate>"+otaAmount+"</SellRate>"+
						            "</SingleOccupancyRates>"+
						            "<DoubleOccupancyRates>"+				            
						            "<SellRate>"+otaAmount+"</SellRate>"+
						            "</DoubleOccupancyRates>"+
						            "<TripleOccupancyRates>"+				            
						            "<SellRate>"+otaAmount+"</SellRate>"+
						            "</TripleOccupancyRates>"+
						            "<ExtraChildCharge>"+getExtraChild()+"</ExtraChildCharge>"+
						            "<ExtraAdultCharge>"+getExtraAdult()+"</ExtraAdultCharge>"+
						            "<InfantCharge>"+getExtraInfant()+"</InfantCharge>"+
						            "<DaysOfWeek Mon='"+getDaysOfWeek("1")+"' Tue='"+getDaysOfWeek("2")+"' Wed='"+getDaysOfWeek("3")+"' Thu='"+getDaysOfWeek("4")+"' Fri='"+getDaysOfWeek("5")+"' Sat='"+getDaysOfWeek("6")+"' Sun='"+getDaysOfWeek("7")+"'></DaysOfWeek>"+
						            "<ContractType b2b='False' b2c='True' mobile='False' fph='False'></ContractType>"+
						            "<Currency>INR</Currency>"+
						            "</RatePlan>"+
						      "</Website>";
								
								
						StringEntity input = new StringEntity(xmlBody);
						input.setContentType("text/xml");
						postRequest.setEntity(input);
						HttpResponse httpResponse = httpClient.execute(postRequest);
						HttpEntity entity = httpResponse.getEntity();
			            // Read the contents of an entity and return it as a String.
			            String content = EntityUtils.toString(entity);
				}
		    	
			}*/
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	
	}
	
		public String addRate() {
		try {
			
			this.propertyId = (Integer) sessionMap.get("propertyId");
			PmsSourceManager sourceController = new PmsSourceManager();
			PropertyRateManager rateController = new PropertyRateManager();
			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
			OtaHotelsManager otaController = new OtaHotelsManager(); 
			//UserLoginManager  userController = new UserLoginManager();
			PmsPropertyManager propertyController = new PmsPropertyManager();
			
			DateFormat format = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
		    java.util.Date dateStart = format.parse(getStrModStartDate());
		    java.util.Date dateEnd = format.parse(getStrModEndDate());
		   
		    String jsonOutput="";
	    	HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("application/json");
			
		    Calendar calCheckStart=Calendar.getInstance();
		    calCheckStart.setTime(dateStart);
		    java.util.Date checkInDate = calCheckStart.getTime();
		    this.modStartDate=new Timestamp(checkInDate.getTime());
		    
		    Calendar calCheckEnd=Calendar.getInstance();
		    calCheckEnd.setTime(dateEnd);
		    java.util.Date checkOutDate = calCheckEnd.getTime();
		    this.modEndDate=new Timestamp(checkOutDate.getTime());
		    
		    
	        PropertyRateDetailManager rateDetailController = new PropertyRateDetailManager();    
	        PropertyRatePlanManager propertyRateController = new PropertyRatePlanManager();
	        PropertyRatePlanDetailManager ratePlanDetailsController=new PropertyRatePlanDetailManager();
			PropertyRate rate = new PropertyRate();
            
			this.userId=(Integer)sessionMap.get("userId");
			SimpleDateFormat sdfformat = new SimpleDateFormat("yyyy-MM-dd");
		    java.util.Date date=new java.util.Date();
        	Calendar calDate=Calendar.getInstance();
        	calDate.setTime(date);
        	calDate.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
        	String strCurrentDate = new SimpleDateFormat("yyyy-MM-dd").format(calDate.getTime());
		    java.util.Date currentDate = sdfformat.parse(strCurrentDate);
		    Timestamp tsCurrentDate = new Timestamp(currentDate.getTime());
		    PmsSourceTypeManager sourceTypeController = new PmsSourceTypeManager();
			List<PropertyRate> rateList =  rateController.list(getPropertyId(),getPropertyAccommodationId());            
            Object orderBy = rateList.size();            
            String converted = orderBy .toString();            
            int orderBy1 = 0;
            orderBy1 = Integer.parseInt(converted) + 1;
            
			//String[] stringArray = getModSourceTypes().split("\\s*,\\s*");
			int RateId=0;
			String strRateId="";
			if (!jsonOutput.equalsIgnoreCase(""))
				jsonOutput += ",{";
			else
				jsonOutput += "{";

			jsonOutput += "\"check\":\"" + false+ "\"";
	    

    		rate.setIsActive(true);
			rate.setIsDeleted(false);
			rate.setStartDate(modStartDate);
			rate.setEndDate(modEndDate);
			rate.setOrderBy(orderBy1);
			//rate.setRateName(getRateName());
			rate.setSmartPriceIsActive(false);
			rate.setCreatedBy(this.userId);
			rate.setCreatedDate(tsCurrentDate);
			PmsProperty property = propertyController.find(getPropertyId());
			rate.setPmsProperty(property);
			PropertyAccommodation propertyAccommodation = accommodationController.find(getPropertyAccommodationId());
			rate.setPropertyAccommodation(propertyAccommodation);
			PmsSourceType sourcetype = sourceTypeController.find(1);
			rate.setPmsSourceType(sourcetype);
			//PropertyTaxe propertyTaxe = taxController.find(getPropertyTaxId());
			//taxe.setPropertyTaxe(propertyTaxe);
			rateController.add(rate);
			RateId=rate.getPropertyRateId();
			strRateId=String.valueOf(RateId);
    	
			
			this.propertyRateId = rate.getPropertyRateId();
			sessionMap.put("propertyRateId",propertyRateId);
			sessionMap.put("RateId", strRateId);
			
			this.propertyRateId = (Integer) sessionMap.get("propertyRateId");
		
			jsonOutput += "}";
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}
	
	public String getSourceTypes(){

		try {
			this.propertyId = (Integer) sessionMap.get("propertyId");
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			PmsSourceTypeManager sourceTypeController = new PmsSourceTypeManager();
			response.setContentType("application/json");

			List<PmsSourceType> sourceTypeList = sourceTypeController.list();
			for (PmsSourceType sourceType : sourceTypeList) {

				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"sourceTypeId\":\"" + sourceType.getSourceTypeId() + "\"";
				jsonOutput += ",\"sourceType\":\"" + sourceType.getSourceTypeName()+ "\"";
				
				jsonOutput += "}";


			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	
	}
	public String editRate() {
		
		this.propertyId = (Integer) sessionMap.get("propertyId");
		PmsSourceManager sourceController = new PmsSourceManager();
		PropertyRateManager rateController = new PropertyRateManager();
		PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
		PropertyRateDetailManager rateDetailController = new PropertyRateDetailManager();
		//UserLoginManager  userController = new UserLoginManager();
		PmsPropertyManager propertyController = new PmsPropertyManager();
		boolean dateChk=false;
		try {
			
			String alldates=null;	
			TreeSet<String> setValues=new TreeSet<String>();
			Set<String> setDateValues=new HashSet<String>();
			PropertyRate rate = rateController.find(getPropertyRateId());
			Map<String, String> findDateMap = new HashMap<String, String>();
			java.util.Date sysdate=new java.util.Date();
			ArrayList<String> arrListDate=new ArrayList<String>();
			ArrayList<String> arrListDetail=new ArrayList<String>();
			ArrayList<String> arrListDateId=new ArrayList<String>();
			//PropertyDiscount editdis = propertyDiscountController.find(getPropertyDiscountId());
			
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			/*List<PropertyRate> rateList =  rateController.list(getPropertyId(),getPropertyAccommodationId());            
            Object orderBy = rateList.size();            
            String converted = orderBy .toString();            
            int orderBy1 = 0;
            orderBy1 = Integer.parseInt(converted) + 1;*/
			int fromDay=0,fromMonth=0,fromYear=0;
			int toDay=0,toMonth=0,toYear=0;
			SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
			Calendar calStart=Calendar.getInstance();
			calStart.setTime(getStartDate());
			java.util.Date startDate = calStart.getTime();             
	    	String displayFromDate = format1.format(startDate);  
	    	String[] fromDays=displayFromDate.split("-");
	    	fromDay=Integer.valueOf(fromDays[2]);
    		fromMonth=Integer.valueOf(fromDays[1]);
    		fromYear=Integer.valueOf(fromDays[0]);
	    	fromDay=calStart.getActualMinimum(calStart.DAY_OF_MONTH);
	    	String strFromDate=fromYear+"-"+fromMonth+"-"+fromDay;
	    	//String startDay=getDayOfWeek(calStart.get(Calendar.DAY_OF_WEEK));
	    	
	    	Calendar calEnd=Calendar.getInstance();
	    	calEnd.setTime(getEndDate());
	    	java.util.Date endDate = calEnd.getTime();  
	    	String displayToDate = format1.format(endDate); 
	    	String[] toDays=displayToDate.split("-");
	    	toDay=Integer.valueOf(toDays[2]);
    		toMonth=Integer.valueOf(toDays[1]);
    		toYear=Integer.valueOf(toDays[0]);
	    	toDay=calEnd.getActualMaximum(calEnd.DAY_OF_MONTH);
	    	String strToDate=toYear+"-"+toMonth+"-"+toDay;
	    	//String endDay=getDayOfWeek(calEnd.get(Calendar.DAY_OF_WEEK));
	    	/*while (calStart.before(calEnd)) {
	    	    if (i % 7 == 0) {   // last day of the week
	    	        i  = 1;
	    	    } else {
	    	        i++;
	    	    }
	    	    calStart.add(DATE, 1);
	    	}*/
	    	
	    	for ( int i = 0; i < toDay; i++) {
	    		calStart.set(Calendar.DAY_OF_MONTH, i + 1);
	    		alldates=format1.format(calStart.getTime());
	    		setDateValues.add(alldates);
	        }
	    	
			String strStartDate=null,strEndDate=null,strPropertyRateId=null,strAmount=null,strDaysofWeek=null,strDays=null,strDateValues=null;
		    List<PropertyRate> dateList =  rateController.list(getPropertyId(),getPropertyAccommodationId(),getSourceId(),getStartDate(),getEndDate());
		    if(!dateList.isEmpty()){
		    	for (PropertyRate rates : dateList) {
		    		strPropertyRateId=String.valueOf(rates.getPropertyRateId());
		    		strStartDate=format1.format(rates.getStartDate());
		    		strEndDate=format1.format(rates.getEndDate());
		    		DateTime FromDate = DateTime.parse(strStartDate);
			        DateTime ToDate = DateTime.parse(strEndDate);
			        
			        int propertyRatesId=rates.getPropertyRateId();
		    		List<PropertyRateDetail> rateDetailList =  rateDetailController.listRateDetail(propertyRatesId);
		    		
			        List<DateTime> dateBetween = DateUtil.getDateRange(FromDate, ToDate);
			        Iterator<DateTime> iterDateValues=dateBetween.iterator();
			        while(iterDateValues.hasNext()){
			        	DateTime dateValues=iterDateValues.next();
			        	java.util.Date DateValues = dateValues.toDate();
			        	strDateValues=format1.format(DateValues);
			        	if(!arrListDateId.contains(strDateValues)){
			        		arrListDateId.add(strDateValues);
				        	arrListDateId.add(strPropertyRateId);
				        	if(!rateDetailList.isEmpty()){
				    			for(PropertyRateDetail rateDetails: rateDetailList){
				    				strAmount=String.valueOf(rateDetails.getBaseAmount());
				    				strDaysofWeek=rateDetails.getDaysOfWeek().toLowerCase();
				    				List<String> daysBetween = DateUtil.getDayRange(FromDate, ToDate);
				    				Iterator<String> iterDayValues=daysBetween.iterator();
							        while(iterDayValues.hasNext()){
							        	strDays=iterDayValues.next().toLowerCase();
							        	if(strDaysofWeek.contains(strDays)){
							        		arrListDateId.add(strDaysofWeek);
							        		arrListDateId.add(strAmount);
							        	}
							        }
				    			}
				    		}
			        	}
			        	
			        }
		    		dateChk=true;
		    	}
		    }
		    
		   if(!dateChk){
		    	rate.setIsActive(true);
				rate.setIsDeleted(false);
				rate.setStartDate(getStartDate());
				rate.setEndDate(getEndDate());
				//rate.setOrderBy(orderBy1);
				rate.setRateName(getRateName());
				PmsProperty property = propertyController.find(getPropertyId());
				rate.setPmsProperty(property);
				PropertyAccommodation propertyAccommodation = accommodationController.find(getPropertyAccommodationId());
				rate.setPropertyAccommodation(propertyAccommodation);
				PmsSource source = sourceController.find(getSourceId());
				rate.setPmsSource(source);
				//PropertyTaxe propertyTaxe = taxController.find(getPropertyTaxId());
				//taxe.setPropertyTaxe(propertyTaxe);
				rateController.edit(rate);
		    }
			
			this.propertyRateId = (Integer) sessionMap.get("propertyRateId");
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return SUCCESS;
	}
	
	public String deleteRates() {
		
		PmsSourceManager sourceController = new PmsSourceManager();
		PropertyRateManager rateController = new PropertyRateManager();
		PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
			try {
				PropertyRate deleterate = rateController.find(getPropertyRateId());
				long time = System.currentTimeMillis();
				java.sql.Date date = new java.sql.Date(time);
				deleterate.setIsActive(false);
				deleterate.setIsDeleted(true);
				deleterate.setModifiedDate(new java.sql.Timestamp(date.getTime()));
				rateController.edit(deleterate);
				
			} catch (Exception e) {
				logger.error(e);
				e.printStackTrace();
			} finally {

			}
			return null;
		}  

	public String editRateOrders() throws IOException {
		   try {
			   String jsonOutput = "";
			   HttpServletResponse response = ServletActionContext.getResponse();
			   this.propertyId = (Integer) sessionMap.get("propertyId");
			   PmsSourceManager sourceController = new PmsSourceManager();
			   PropertyRateManager rateController = new PropertyRateManager();
			   PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
			   //UserLoginManager  userController = new UserLoginManager();
			   PmsPropertyManager propertyController = new PmsPropertyManager();
			   response.setContentType("application/json");			   
			   /*int orderBy1 = 0;
	            orderBy1 = (orders.size()) + 1;
			   */
	    		for (int i = 0; i < orders.size(); i++) 
	  	      {
	  		    PropertyRate rate = rateController.find(orders.get(i).getRateId());
	  		    rate.setOrderBy(i);
				rateController.edit(rate);
	  	      } 				
	    		
			} catch (Exception e) {
				logger.error(e);
				e.printStackTrace();
			} finally {

			}

			return null ;

	    	
	    }
	
    public String getSources() throws IOException {
		
		this.propertyId = (Integer) sessionMap.get("propertyId");
		try {
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			PmsSourceManager sourceController = new PmsSourceManager();
			response.setContentType("application/json");

			this.sourceList = sourceController.list();
			for (PmsSource source : sourceList) {

				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"sourceId\":\"" + source.getSourceId() + "\"";
				jsonOutput += ",\"sourceName\":\"" + source.getSourceName()+ "\"";
				
				jsonOutput += "}";


			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
    
public String getResortSources() throws IOException {
		
		this.propertyId = (Integer) sessionMap.get("propertyId");
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			PmsSourceManager sourceController = new PmsSourceManager();
			response.setContentType("application/json");

			this.sourceList = sourceController.list1();
			for (PmsSource source : sourceList) {
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"sourceId\":\"" + source.getSourceId() + "\"";
				jsonOutput += ",\"sourceName\":\"" + source.getSourceName()+ "\"";
				
				jsonOutput += "}";
			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}

public String getAllSources() throws IOException {
	
	this.propertyId = (Integer) sessionMap.get("propertyId");
	try {

		String jsonOutput = "";
		HttpServletResponse response = ServletActionContext.getResponse();
		
		PmsSourceManager sourceController = new PmsSourceManager();
		response.setContentType("application/json");

		this.sourceList = sourceController.listAll();
		for (PmsSource source : sourceList) {
			if (!jsonOutput.equalsIgnoreCase(""))
				jsonOutput += ",{";
			else
				jsonOutput += "{";
			jsonOutput += "\"sourceId\":\"" + source.getSourceId() + "\"";
			jsonOutput += ",\"sourceName\":\"" + source.getSourceName()+ "\"";
			
			jsonOutput += "}";


		}

		response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
		

	} catch (Exception e) {
		logger.error(e);
		e.printStackTrace();
	} finally {

	}

	return null;

}
	
    public String getDays() {
		this.propertyId = (Integer) sessionMap.get("propertyId");
		PmsSourceManager sourceController = new PmsSourceManager();
		PropertyRateManager rateController = new PropertyRateManager();
		PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
		//UserLoginManager  userController = new UserLoginManager();
		PmsPropertyManager propertyController = new PmsPropertyManager();
		try {
			PropertyRate rate = new PropertyRate();
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			Calendar c = Calendar.getInstance();
			
			String startDate = new SimpleDateFormat("yyyy-MM-dd").format(getStartDate());
			String endDate = new SimpleDateFormat("yyyy-MM-dd").format(getEndDate());
			DateTime start = DateTime.parse(startDate);
		    DateTime end = DateTime.parse(endDate);
			
		     List<String> items = Arrays.asList(getCheckedDays().split("\\s*,\\s*"));
		     List<String> daysBetween = DateUtil.getDayRange(start, end);
		     for (String days : daysBetween)
		     {
		    	 int dayNo =  DateUtil.getDay(days);
		     }
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return SUCCESS;
	}
	


	

	public Integer getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(Integer propertyId) {
		this.propertyId = propertyId;
	}
	
	public Integer getPropertyAccommodationId() {
		return propertyAccommodationId;
	}

	public void setPropertyAccommodationId(Integer propertyAccommodationId) {
		this.propertyAccommodationId = propertyAccommodationId;
	}
	
	public Integer getSmartPricePropertyAccommodationId() {
		return smartPricePropertyAccommodationId;
	}

	public void setSmartPricePropertyAccommodationId(Integer smartPricePropertyAccommodationId) {
		this.smartPricePropertyAccommodationId = smartPricePropertyAccommodationId;
	}

	public Integer getPropertyRateId() {
		return propertyRateId;
	}

	public void setPropertyRateId(Integer propertyRateId) {
		this.propertyRateId = propertyRateId;
	}
	
	public Integer getPropertyRateDetailsId() {
		return propertyRateDetailsId;
	}

	public void setPropertyRateDetailsId(Integer propertyRateDetailsId) {
		this.propertyRateDetailsId = propertyRateDetailsId;
	}

	public Integer getSourceId() {
		return sourceId;
	}

	public void setSourceId(Integer sourceId) {
		this.sourceId = sourceId;
	}

	public String getRateName() {
		return rateName;
	}

	public void setRateName(String rateName) {
		this.rateName = rateName;
	}
	
	
	
	public Timestamp getSmartPriceStartDate() {
		return smartPriceStartDate;
	}

	public void setSmartPriceStartDate(Timestamp smartPriceStartDate) {
		this.smartPriceStartDate = smartPriceStartDate;
	}

	public Timestamp getSmartPriceEndDate() {
		return smartPriceEndDate;
	}

	public void setSmartPriceEndDate(Timestamp smartPriceEndDate) {
		this.smartPriceEndDate = smartPriceEndDate;
	}
	
	public Timestamp getStartDate() {
		return startDate;
	}

	public void setStartDate(Timestamp startDate) {
		this.startDate = startDate;
	}

	public Timestamp getEndDate() {
		return endDate;
	}

	public void setEndDate(Timestamp endDate) {
		this.endDate = endDate;
	}
	

	public Timestamp getArrivalDate() {
        return arrivalDate;
    }
    public void setArrivalDate(Timestamp arrivalDate) {
        this.arrivalDate = arrivalDate;
    }    
    public Timestamp getDepartureDate() {
        return departureDate;
    }  
    public void setDepartureDate(Timestamp departureDate) {
        this.departureDate = departureDate;
    }

	public Integer getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(Integer orderBy) {
		this.orderBy = orderBy;
	}

	public PropertyRate getPropertyRate() {
		return rate;
	}

	public void setPropertyRate(PropertyRate rate) {
		this.rate = rate;
	}

	public List<PropertyRate> getRateList() {
		return rateList;
	}

	public void setRateList(List<PropertyRate> rateList) {
		this.rateList = rateList;
	}
	
	public String getCheckedDays() {
		return checkedDays;
	}

	public void setCheckedDays(String checkedDays) {
		this.checkedDays = checkedDays;
	}
	 
	public String getModSources(){
		return modSources;
	}
	
	public void setModSources(String modSources){
		this.modSources=modSources;
	}
	 public List<PmsSource> getSourceList() {
			return sourceList;
		}
	
	 public void setSourceList(List<PmsSource> sourceList) {
			this.sourceList = sourceList;
		}
	 
	 public List<PropertyRateOrder> getOrders() {
	        return orders;
	 }
	    
	 public void setOrders(List<PropertyRateOrder> orders) {
	        this.orders = orders;
	  }
	 
	 public String getChartDate(){
		 return chartDate;
	 }
	 
	 public void setChartDate(String chartDate){
		 this.chartDate=chartDate;
	 }
	 
	 public String getResultDate(){
		 return resultDate;
	 }
	 
	 public void setResultDate(String resultDate){
		 this.resultDate=resultDate;
	 }
	 
	 public Integer getAccommodationId(){
		 return accommodationId;
	 }
	 
	 public void setAccommodationId(Integer accommodationId){
		 this.accommodationId=accommodationId;
	 }
	 
	 public Timestamp getModStartDate() {
        return modStartDate;
    }
    public void setModStartDate(Timestamp modStartDate) {
        this.modStartDate = modStartDate;
    }    
    public Timestamp getModEndDate() {
        return modEndDate;
    }  
    public void setModEndDate(Timestamp modEndDate) {
        this.modEndDate = modEndDate;
    }
    public Boolean getSmartPriceIsActiveOrNot() {
        return smartPriceIsActiveOrNot;
    }  
    public void setSmartPriceIsActiveOrNot(Boolean smartPriceIsActiveOrNot) {
        this.smartPriceIsActiveOrNot = smartPriceIsActiveOrNot;
    }
    public String getUpdatePropertyRateId() {
		return updatePropertyRateId;
	}

	public void setUpdatePropertyRateId(String updatePropertyRateId) {
		this.updatePropertyRateId = updatePropertyRateId;
	}
	
	public String getStrStartDate() {
		return strStartDate;
	}

	public void setStrStartDate(String strStartDate) {
		this.strStartDate = strStartDate;
	}

	public String getStrEndDate() {
		return strEndDate;
	}

	public void setStrEndDate(String strEndDate) {
		this.strEndDate = strEndDate;
	}

	public String getStrModStartDate() {
		return strModStartDate;
	}

	public void setStrModStartDate(String strModStartDate) {
		this.strModStartDate = strModStartDate;
	}

	public String getStrModEndDate() {
		return strModEndDate;
	}

	public void setStrModEndDate(String strModEndDate) {
		this.strModEndDate = strModEndDate;
	}

	public String getStrSmartPriceStartDate() {
		return strSmartPriceStartDate;
	}

	public void setStrSmartPriceStartDate(String strSmartPriceStartDate) {
		this.strSmartPriceStartDate = strSmartPriceStartDate;
	}

	public String getStrSmartPriceEndDate() {
		return strSmartPriceEndDate;
	}

	public void setStrSmartPriceEndDate(String strSmartPriceEndDate) {
		this.strSmartPriceEndDate = strSmartPriceEndDate;
	}
	
	 public String getBookingRates() throws IOException, ParseException {
			this.propertyId = (Integer) sessionMap.get("propertyId");
		    sessionMap.put("chartDate",chartDate); 
			if(getAccommodationId()==null){
				accommodationId=0;
			}
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			PropertyAccommodationRoomManager  roomController  = new PropertyAccommodationRoomManager();
//		    AccommodationRoom accommodationRoom =  roomController.findCount(getPropertyId(),accommodationId);
//		    PropertyRateManager rateController = new PropertyRateManager();
			response.setContentType("application/json");

			for (int i=0; i<chartDate.split(",").length; i++)
			{
				List<PropertyAccommodation> accommodationList= roomController.list(accommodationId,getPropertyId());
				for (PropertyAccommodation accommodationRate : accommodationList) {
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				
				else
					jsonOutput = "{";
				
				    jsonOutput += "\"title\":\"" +" Rent: "+(accommodationRate.getBaseAmount())+" \"";
				    jsonOutput += ",\"start\":\"o\"";
				    
			        jsonOutput += "}";
				
				}
			        
			}
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			return null;
			
		}
	
	 public String getRateDetails() throws IOException, ParseException{
		 try {
			 /*   
		 	this.arrivalDate = getArrivalDate();
		    sessionMap.put("startDate",arrivalDate); 
			
			this.departureDate = getDepartureDate();
			sessionMap.put("endDate",departureDate); */
		    
			this.sourceId = getSourceId();
		    sessionMap.put("sourceId",sourceId); 
				
			this.accommodationId = getAccommodationId();
			sessionMap.put("accommodationId",accommodationId);
			
			if(this.strStartDate==null || this.strEndDate==null || this.strStartDate=="" || this.strEndDate==""){
				return null;
			}
			
			DateFormat format = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
		    java.util.Date dateStart = format.parse(getStrStartDate());
		    java.util.Date dateEnd = format.parse(getStrEndDate());
		   
		    
		    Calendar calCheckStart=Calendar.getInstance();
		    calCheckStart.setTime(dateStart);
		    java.util.Date checkInDate = calCheckStart.getTime();
		    this.arrivalDate=new Timestamp(checkInDate.getTime());
		    sessionMap.put("startDate",arrivalDate); 
		    
		    Calendar calCheckEnd=Calendar.getInstance();
		    calCheckEnd.setTime(dateEnd);
		    java.util.Date checkOutDate = calCheckEnd.getTime();
		    this.departureDate=new Timestamp(checkOutDate.getTime());
		    sessionMap.put("endDate",departureDate); 
			
		    if(this.arrivalDate==null || this.departureDate==null || this.accommodationId==null || this.sourceId==null){
				return null;
			}
		    
			this.propertyId = (Integer) sessionMap.get("propertyId");
		    
			sessionMap.put("chartDate",chartDate); 
			this.chartDate = (String) sessionMap.get("chartDate");
			
			
			
			String checkIn = new SimpleDateFormat("MM/dd/yyyy").format(getArrivalDate());
		    String checkOut = new SimpleDateFormat("MM/dd/yyyy").format(getDepartureDate());
		    sessionMap.put("checkIn",checkIn);
			sessionMap.put("checkOut",checkOut);
			HttpServletResponse response = ServletActionContext.getResponse();
			PropertyAccommodationRoomManager  roomController  = new PropertyAccommodationRoomManager();
			
			PmsSourceManager sourceController = new PmsSourceManager();
			PropertyRateManager rateController = new PropertyRateManager();
			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
			PropertyRateDetailManager rateDetailController = new PropertyRateDetailManager();
			PmsPropertyManager propertyController = new PmsPropertyManager();
			boolean dateChk=false;
			
			int roomCount=0,availableCount=0,totalCount=0,totalDate=0,dateCount=0;
	    	Double dblTotalBaseAmount=0.0,dblBaseAmount=0.0,dblPercentCount=0.0,dblSmartPricePercent=0.0;
	    	Double dblPercentFrom,dblPercentTo;
	    	Boolean isPositive=false,isNegative=false;
	    	
			int fromDay=0,fromMonth=0,fromYear=0;
			int toDay=0,toMonth=0,toYear=0;
			Boolean isSmartPriceActive=false;
			SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
			Calendar calStart=Calendar.getInstance();
			calStart.setTime(getArrivalDate());
			java.util.Date startDate = calStart.getTime();             
	    	String displayFromDate = format1.format(startDate);  
	    	String[] fromDays=displayFromDate.split("-");
	    	fromDay=Integer.valueOf(fromDays[2]);
    		fromMonth=Integer.valueOf(fromDays[1]);
    		fromYear=Integer.valueOf(fromDays[0]);
	    	fromDay=calStart.getActualMinimum(calStart.DAY_OF_MONTH);
	    	String strFromDate=fromYear+"-"+fromMonth+"-"+fromDay;
	    	//String startDay=getDayOfWeek(calStart.get(Calendar.DAY_OF_WEEK));
	    	
	    	Calendar calEnd=Calendar.getInstance();
	    	calEnd.setTime(getDepartureDate());
	    	java.util.Date endDate = calEnd.getTime();  
	    	String displayToDate = format1.format(endDate); 
	    	String[] toDays=displayToDate.split("-");
	    	toDay=Integer.valueOf(toDays[2]);
    		toMonth=Integer.valueOf(toDays[1]);
    		toYear=Integer.valueOf(toDays[0]);
	    	toDay=calEnd.getActualMaximum(calEnd.DAY_OF_MONTH);
	    	String strToDate=toYear+"-"+toMonth+"-"+toDay;
	    	
	    	java.util.Date fromDate = format1.parse(strFromDate);
			Timestamp tsFromDate=new Timestamp(fromDate.getTime());

			java.util.Date toDate = format1.parse(strToDate);
			Timestamp tsToDate=new Timestamp(toDate.getTime());
			
			String jsonOutput = "";
			String jsonSmart ="";
			
			response.setContentType("application/json");
			   
			HashMap<String,String> dateMap=new HashMap<String,String>();
			HashMap<String,String> dayMap=new HashMap<String,String>();
			ArrayList<Integer> listSmartPriceChecked=new ArrayList<Integer>();
			
			ArrayList<String> arrListDateId=new ArrayList<String>();
			SimpleDateFormat formatEE = new SimpleDateFormat("EE", Locale.ENGLISH);
			SimpleDateFormat formatEEEE = new SimpleDateFormat("EEEE", Locale.ENGLISH);
			ArrayList<String> arrListRateDetail=new ArrayList<String>();
			List<PropertyAccommodation> accommodationList= roomController.list(accommodationId,getPropertyId());
			DateFormat f = new SimpleDateFormat("EEEE");
			List<PmsSmartPrice> listSmartPriceDetail=null; 
	    	List<AccommodationRoom> listAccommodationRoom=null;
	    	
			String strStartDate=null,strEndDate=null,strPropertyRateId=null,strAmount=null,strDaysofWeek=null,strDays=null,strDateValues=null;
			DateTime dtFromDate = DateTime.parse(strFromDate);
	        DateTime dtToDate = DateTime.parse(strToDate);
			List<DateTime> listAllDates=DateUtil.getDateRange(dtFromDate, dtToDate);
			for(DateTime d:listAllDates){
				List<PropertyRate> dateList =  rateController.listAllDates(getPropertyId(),getAccommodationId(),getSourceId(),d.toDate());
				//List<PropertyRate> dateList =  rateController.list(getPropertyId(),getAccommodationId(),getSourceId(),tsFromDate,tsToDate);
			    if(!dateList.isEmpty()){
			    	for (PropertyRate rates : dateList) {
			    		strPropertyRateId=String.valueOf(rates.getPropertyRateId());
			    		strStartDate=format1.format(rates.getStartDate());
			    		strEndDate=format1.format(rates.getEndDate());
			    		DateTime FromDate = DateTime.parse(strStartDate);
				        DateTime ToDate = DateTime.parse(strEndDate);
				        isSmartPriceActive=rates.getSmartPriceIsActive();
				        
				        int propertyRatesId=rates.getPropertyRateId(); 
				        /*if(isSmartPriceActive){
				        	listSmartPriceChecked.add(propertyRatesId);
				        }*/
				        java.util.Date DateValues = d.toDate();
			        	strDateValues=format1.format(DateValues);
				        arrListDateId.add(strDateValues);
			        	arrListDateId.add(strPropertyRateId);
			        	
			    		List<PropertyRateDetail> rateDetailList =  rateDetailController.listRateDetail(propertyRatesId,f.format(d.toDate()).toLowerCase());
			    		
			    		if(!rateDetailList.isEmpty()){
			    			List<PropertyRate> currentDateSmartPriceActive=rateController.listCurrentSmartPriceActive(propertyId, getAccommodationId(),d.toDate());
			    			if(currentDateSmartPriceActive.size()>0){
			    				listSmartPriceChecked.add(propertyRatesId);
			    			}
			    			/*if(isSmartPriceActive){
					        	listSmartPriceChecked.add(propertyRatesId);
					        }*/
			    			for(PropertyRateDetail rateDetails: rateDetailList){
			    				strAmount=String.valueOf(rateDetails.getBaseAmount());
			    				strDaysofWeek=rateDetails.getDaysOfWeek().toLowerCase();
			    				arrListDateId.add(strDaysofWeek);
				        		arrListDateId.add(strAmount);
			    				List<String> daysBetween = DateUtil.getDayRange(FromDate, ToDate);
			    				Iterator<String> iterDayValues=daysBetween.iterator();
						        while(iterDayValues.hasNext()){
						        	strDays=iterDayValues.next().toLowerCase();
						        	if(strDaysofWeek.contains(strDays)){
						        		arrListDateId.add(strDaysofWeek);
						        		arrListDateId.add(strAmount);
						        	}
						        }
			    			}
			    			break;
			    		}
			    		dateChk=true;
			    	}
			    }else{
			    	if(accommodationList.size()>0 && !accommodationList.isEmpty()){
			    		for (PropertyAccommodation accommodationRate : accommodationList) {
			    			strAmount=String.valueOf(accommodationRate.getBaseAmount());
			    			 java.util.Date DateValues = d.toDate();
					         strDateValues=format1.format(DateValues);
						     arrListDateId.add(strDateValues);
					         arrListDateId.add(String.valueOf(0));
					         arrListDateId.add(f.format(d.toDate()).toLowerCase());
				        	 arrListDateId.add(strAmount);
			    		}
			    		//break;
			    	}
			    }
			}
			
		    /*List<PropertyRate> dateList =  rateController.list(getPropertyId(),getAccommodationId(),getSourceId(),tsFromDate,tsToDate);
		    if(!dateList.isEmpty()){
		    	for (PropertyRate rates : dateList) {
		    		strPropertyRateId=String.valueOf(rates.getPropertyRateId());
		    		strStartDate=format1.format(rates.getStartDate());
		    		strEndDate=format1.format(rates.getEndDate());
		    		DateTime FromDate = DateTime.parse(strStartDate);
			        DateTime ToDate = DateTime.parse(strEndDate);
			        isSmartPriceActive=rates.getSmartPriceIsActive();
			        
			        int propertyRatesId=rates.getPropertyRateId();
			        if(isSmartPriceActive){
			        	listSmartPriceChecked.add(propertyRatesId);
			        }
		    		List<PropertyRateDetail> rateDetailList =  rateDetailController.listRateDetail(propertyRatesId);
		    		
			        List<DateTime> dateBetween = DateUtil.getDateRange(FromDate, ToDate);
			        Iterator<DateTime> iterDateValues=dateBetween.iterator();
			        while(iterDateValues.hasNext()){
			        	DateTime dateValues=iterDateValues.next();
			        	java.util.Date DateValues = dateValues.toDate();
			        	strDateValues=format1.format(DateValues);
			        	if(!arrListDateId.contains(strDateValues)){
			        		arrListDateId.add(strDateValues);
				        	arrListDateId.add(strPropertyRateId);
				        	if(!rateDetailList.isEmpty()){
				    			for(PropertyRateDetail rateDetails: rateDetailList){
				    				strAmount=String.valueOf(rateDetails.getBaseAmount());
				    				strDaysofWeek=rateDetails.getDaysOfWeek().toLowerCase();
				    				List<String> daysBetween = DateUtil.getDayRange(FromDate, ToDate);
				    				Iterator<String> iterDayValues=daysBetween.iterator();
							        while(iterDayValues.hasNext()){
							        	strDays=iterDayValues.next().toLowerCase();
							        	if(strDaysofWeek.contains(strDays)){
							        		arrListDateId.add(strDaysofWeek);
							        		arrListDateId.add(strAmount);
							        	}
							        }
				    			}
				    		}
			        	}
			        	
			        }
		    		dateChk=true;
		    	}
		    }*/
		    
		   int intCount=0;
		   String strArrays[] = new String[arrListDateId.size()];
		    Iterator<String> iterListValues=arrListDateId.iterator();
		    while(iterListValues.hasNext()){
	    		String strTemp=iterListValues.next();
	    		strArrays[intCount]=strTemp;
	    		intCount++;		
		    }
		    
		    String dateval="",doubleval="",rateId="";
		    for(String s : strArrays) {
		          String[] s2 = s.split(" ");
		          for(String results : s2) {
		              if(results.contains("-")){//Date check
		            	  dateval=results;
		            	  arrListRateDetail.add(dateval);
		            	  
		              }else{// Number
		            	  if(isNumeric(results)){
		            		  rateId=results;
		            	  }
		            	  else{
		            		  int first = results.indexOf(".");
		            		  if ( (first >= 0) && (first - results.lastIndexOf(".")) == 0) {
		            		      // only one decimal point
		            			  doubleval=results;
		            			  arrListRateDetail.add(doubleval);
		            		  }
		            		  else {
		            			
		            			  // no decimal point or more than one decimal point
		            		  }
		            	  }
		              }
		  		      if(!doubleval.isEmpty() && !dateval.isEmpty()){
		  		    	dateMap.put(dateval, doubleval);
		              }
		  		      if(!rateId.isEmpty() && !dateval.isEmpty()){
		  		    	  dayMap.put(dateval, rateId);
		  		      }
		          }
		      }
		    intCount=0;
            String[] listValues=new String[arrListRateDetail.size()];
            Iterator<String> rateDetailsIter=arrListRateDetail.iterator();
		    while (rateDetailsIter.hasNext()) {
                String strTemp=rateDetailsIter.next();
                listValues[intCount]=strTemp;
	    		intCount++;		
		    }
		   
		    String strDay="", strRateId="";
		    int intRateId=0;
            String[] monthDate=chartDate.split(",");
            
            for(String strDate : monthDate){ 
        		if(arrListRateDetail.contains(strDate)){
        			java.util.Date dteReqDate= format1.parse(strDate);
	    			Timestamp tsDate=new Timestamp(dteReqDate.getTime());
	    			
        			strDay=formatEEEE.format(dteReqDate).toLowerCase();
        			strRateId=dayMap.get(strDate);
        			intRateId=Integer.parseInt(strRateId);
        			if(listSmartPriceChecked.contains(intRateId)){
        				List<PropertyRateDetail> rateDetailList1 =   rateDetailController.list(intRateId,strDay);
        				sessionMap.put("SmartPriceRateId", intRateId);
        				PropertyRate rates=rateController.listRates(intRateId);
        				int accommId=rates.getPropertyAccommodation().getAccommodationId();
        				int rateCount=0;
            			if(rateDetailList1.size()>0 && !rateDetailList1.isEmpty()){
            				for (PropertyRateDetail rate : rateDetailList1)
    		    			{
            					if(rateCount==0){

                					 dblBaseAmount=rate.getBaseAmount();
                					 listAccommodationRoom=accommodationController.listPropertyTotalRooms(propertyId, accommId);
    	               				 if(listAccommodationRoom.size()>0 && !listAccommodationRoom.isEmpty()){
    	               					 for(AccommodationRoom roomsList:listAccommodationRoom){
    	               						 roomCount=(int) roomsList.getRoomCount();
    	               						 long minimum = getAvailableCount(propertyId,tsDate,tsDate,roomCount,accommId); 
    	               						 availableCount=(int)minimum;
    	               						 dblPercentCount=(double)((roomCount-availableCount)*100)/roomCount;
    	               						 totalCount=(int) Math.round(dblPercentCount);
    	               						 listSmartPriceDetail=rateDetailController.listSmartPriceDetail(totalCount);
    	               						 if(listSmartPriceDetail.size()>0 && !listSmartPriceDetail.isEmpty()){
    	               							 for(PmsSmartPrice smartPrice: listSmartPriceDetail){
    	               								 dblSmartPricePercent=smartPrice.getAmountPercent();
    	               								 dblPercentFrom=smartPrice.getPercentFrom();
    	               								 dblPercentTo=smartPrice.getPercentTo();
    	               								 if(dblPercentFrom>=0 && dblPercentTo<=59){
    	               									 isNegative=true;
    	               								 }else if(dblPercentFrom>=60 && dblPercentTo<=99){
    	               									 isPositive=true;
    	               								 }
    	               								 if(isNegative){
    	               									 dblTotalBaseAmount=dblBaseAmount-(dblBaseAmount*dblSmartPricePercent/100);
    	               								 }
    	               								 if(isPositive){
    	               									dblTotalBaseAmount=dblBaseAmount+(dblBaseAmount*dblSmartPricePercent/100);
    	               								 }
    	               								rateCount++;
    	               							 }
    	               						 }
    	               					 }
    	               				 }
    	               				isNegative=false;
    	               				isPositive=false;
                					if (!jsonOutput.equalsIgnoreCase(""))
                    					jsonOutput += ",{";
                    				
                    				else
                    					
                    					
                    					jsonOutput = "{";
                    				
    	            				    jsonOutput += "\"title\":\"" +" Tariff: "+dblTotalBaseAmount+" \"";
    	            				    jsonOutput += ",\"color\":\"red\"";
    	            				    jsonOutput += ",\"start\":\""+strDate+"\"";
    	            				    
    	            			        jsonOutput += "}";
    	            			        jsonOutput += ",{";
    	            				    jsonOutput += "\"title\":\"" +" Child: "+(rate.getExtraChild())+" \"";
    	            				    jsonOutput += ",\"color\":\"red\"";
    	            				    jsonOutput += ",\"start\":\""+strDate+"\"";
    	            				    
    	            				    jsonOutput += "}";
    	            				    jsonOutput += ",{";
    	            				    jsonOutput += "\"title\":\"" +" Adult: "+(rate.getExtraAdult())+"\"";
    	            				    jsonOutput += ",\"color\":\"red\"";
    	            				    jsonOutput += ",\"start\":\""+strDate+"\"";
    	            				    
    	            				    jsonOutput += "}";
        		    			 
            					}
            					
    		    			 }
            			}
        			}
        			else{
	        			List<PropertyRateDetail> rateDetailList1 =   rateDetailController.list(intRateId,strDay);
	        			
	        			if(rateDetailList1.size()>0 && !rateDetailList1.isEmpty()){
	        				for (PropertyRateDetail rate : rateDetailList1)
			    			 {
	        					if (!jsonOutput.equalsIgnoreCase(""))
	            					jsonOutput += ",{";
	            				
	            				else
	            					
	            					jsonOutput = "{";
                				
	            				    jsonOutput += "\"title\":\"" +" Tariff: "+(rate.getBaseAmount())+" \"";
	            				    jsonOutput += ",\"start\":\""+strDate+"\"";
	            				    jsonOutput += ",\"color\":\"green\"";
	            				    
	            			        jsonOutput += "}";
	            			        jsonOutput += ",{";
	            				    jsonOutput += "\"title\":\"" +" Child: "+(rate.getExtraChild())+" \"";
	            				    jsonOutput += ",\"start\":\""+strDate+"\"";
	            				    jsonOutput += ",\"color\":\"orange\"";
	            				    
	            				    jsonOutput += "}";
	            				    jsonOutput += ",{";
	            				    jsonOutput += "\"title\":\"" +" Adult: "+(rate.getExtraAdult())+" \"";
	            				    jsonOutput += ",\"start\":\""+strDate+"\"";
	            				    
	            				    jsonOutput += "}";
	            				   
			    			 }
	        			}
	        			else
	        			{
	        				for (PropertyAccommodation accommodationRate : accommodationList) {
	        					if (!jsonOutput.equalsIgnoreCase(""))
	            					jsonOutput += ",{";
	            				
	            				else
	            					
	            					jsonOutput = "{";
	            				
		        				    jsonOutput += "\"title\":\"" +" Tariff: "+(accommodationRate.getBaseAmount())+" \"";
		        				    jsonOutput += ",\"start\":\""+strDate+"\"";
		        				    jsonOutput += ",\"color\":\"green\"";
		        				    
		        			        jsonOutput += "}";
		        			        jsonOutput += ",{";
		        				    jsonOutput += "\"title\":\"" +" Child: "+(accommodationRate.getExtraChild())+" \"";
		        				    jsonOutput += ",\"start\":\""+strDate+"\"";
		        				    jsonOutput += ",\"color\":\"orange\"";
		        				    
		        				    jsonOutput += "}";
		        				    jsonOutput += ",{";
		        				    jsonOutput += "\"title\":\"" +" Adult: "+(accommodationRate.getExtraAdult())+" \"";
		        				    jsonOutput += ",\"start\":\""+strDate+"\"";
		        				    
		        				    jsonOutput += "}";
		        				   
	        				}
	        			}
        			}
        		}
        		else{
    				
    				for (PropertyAccommodation accommodationRate : accommodationList) {
    					if (!jsonOutput.equalsIgnoreCase(""))
        					jsonOutput += ",{";
        				
        				else
        					jsonOutput = "{";
        				
        				    jsonOutput += "\"title\":\"" +" Tariff: "+(accommodationRate.getBaseAmount())+" \"";
        				    jsonOutput += ",\"start\":\""+strDate+"\"";
        				    jsonOutput += ",\"color\":\"green\"";
        				    
        			        jsonOutput += "}";
        			        jsonOutput += ",{";
        				    jsonOutput += "\"title\":\"" +" Child: "+(accommodationRate.getExtraChild())+" \"";
        				    jsonOutput += ",\"start\":\""+strDate+"\"";
        				    jsonOutput += ",\"color\":\"orange\"";
        				    
        				    jsonOutput += "}";
        				    jsonOutput += ",{";
        				    jsonOutput += "\"title\":\"" +" Adult: "+(accommodationRate.getExtraAdult())+" \"";
        				    jsonOutput += ",\"start\":\""+strDate+"\"";
        				    
        				    jsonOutput += "}";
        				   
    				}
        		}
            	
        	}
			
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
		
	 }
	 public String getInactiveRates(){
		 try{
			 sessionMap.put("updatePropertyRateId",updatePropertyRateId); 
			 this.updatePropertyRateId=(String)sessionMap.get("updatePropertyRateId");
			 PropertyRateManager rateController = new PropertyRateManager();
			 PropertyRateDetailManager rateDetailController=new PropertyRateDetailManager();
			 PropertyRate deleterate = rateController.find(Integer.parseInt(updatePropertyRateId));
			 long time = System.currentTimeMillis();
			 java.sql.Date date = new java.sql.Date(time);
			
			 deleterate.setIsActive(false);
			 deleterate.setIsDeleted(true);
			 deleterate.setModifiedDate(new java.sql.Timestamp(date.getTime()));
			 rateController.edit(deleterate);
			 
			 PropertyRateDetail rateDetail=rateDetailController.find(Integer.parseInt(updatePropertyRateId));
			 rateDetail.setIsActive(false);
			 rateDetail.setIsDeleted(true);
			 rateDetailController.edit(rateDetail);
			 
		 }catch(Exception e){
			 logger.equals(e);
			 e.printStackTrace();
		 }finally{
			 
		 }
		 return null;
	 }
	 
	 public String getInactiveSmartPrice(){
		 try{
			 sessionMap.put("updatePropertyRateId",updatePropertyRateId); 
			 this.updatePropertyRateId=(String)sessionMap.get("updatePropertyRateId");
			 PropertyRateManager rateController = new PropertyRateManager();
			 
			 List<PropertyRate> listSmartPriceDetail=rateController.listActiveSmartPricesCheck(Integer.parseInt(updatePropertyRateId));
			 if(listSmartPriceDetail.size()>0 && !listSmartPriceDetail.isEmpty())
			 {
				 for(PropertyRate ratelist:listSmartPriceDetail){
				     
					 PropertyRate rateId = rateController.find(ratelist.getPropertyRateId());
					 rateId.setIsActive(false);
					 rateId.setIsDeleted(true);
					 
					 rateId.setStartDate(ratelist.getStartDate());
					 rateId.setEndDate(ratelist.getEndDate());
					 rateId.setOrderBy(ratelist.getOrderBy());
					 rateId.setRateName(ratelist.getRateName());
					 rateId.setPmsProperty(ratelist.getPmsProperty());
					 rateId.setPropertyAccommodation(ratelist.getPropertyAccommodation());
					 rateId.setPmsSource(ratelist.getPmsSource());
					 rateId.setSmartPriceIsActive(false);	
					 rateController.edit(rateId);
					 //break;
				 }
			 }
			 
		 }catch(Exception e){
			 logger.equals(e);
			 e.printStackTrace();
		 }finally{
			 
		 }
		 return null;
	 }
	 
	 
	 public String getSmartPrices(){
		 
		 	this.propertyId = (Integer) sessionMap.get("propertyId");
			try{
				Calendar calendar=Calendar.getInstance();
				SimpleDateFormat format1 = new SimpleDateFormat("dd-MMM-yyyy");
		 		String currentDate = format1.format(calendar.getTime());
		    	java.util.Date curdate=format1.parse(currentDate);
		    	java.sql.Timestamp tsCurrentDate = new java.sql.Timestamp(curdate.getTime());

				String jsonOutput = "",strRateName=null;
				HttpServletResponse response = ServletActionContext.getResponse();
				PropertyRateManager rateController=new PropertyRateManager();
				PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
				response.setContentType("application/json");
				PropertyRateDetailManager rateDetailController = new PropertyRateDetailManager();
				Double dblBaseAmount=0.0;
				int serialno=1;
				this.listSmartPrices=rateController.listSmartPrices(propertyId,tsCurrentDate);
				if(listSmartPrices.size()>0 && !listSmartPrices.isEmpty()){
					for(PropertyRate rates:listSmartPrices){
						if (!jsonOutput.equalsIgnoreCase(""))
							jsonOutput += ",{";
						else
							jsonOutput += "{";
						jsonOutput += "\"propertyRateId\":\"" + rates.getPropertyRateId() + "\"";
						String start = new SimpleDateFormat("dd-MMM-yyyy").format(rates.getStartDate());
						String end = new SimpleDateFormat("dd-MMM-yyyy").format(rates.getEndDate());
						jsonOutput += ",\"startDate\":\"" + start + "\"";
						jsonOutput += ",\"endDate\":\"" + end + "\"";
						jsonOutput += ",\"serialNo\":\"" + serialno + "\"";
						jsonOutput += ",\"accommodationType\":\"" + rates.getPropertyAccommodation().getAccommodationType().trim()+ "\"";
						jsonOutput += "}";
						serialno++;
					}
				}
				response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			}
			catch(Exception e){
				logger.error(e);
				e.printStackTrace();
			}
			finally{
				
			}
			return null;
	 }
	 
	 public String getPropertyRates(){
		 this.propertyId = (Integer) sessionMap.get("propertyId");
			try{
				Calendar calendar=Calendar.getInstance();
				SimpleDateFormat format1 = new SimpleDateFormat("dd-MMM-yyyy");
		 		String currentDate = format1.format(calendar.getTime());
		    	java.util.Date curdate=format1.parse(currentDate);
		    	java.sql.Timestamp tsCurrentDate = new java.sql.Timestamp(curdate.getTime());
		    	PropertyRateDetailManager rateDetailController = new PropertyRateDetailManager();
		    	Double dblBaseAmount=0.0;
				String jsonOutput = "",strRateName=null;
				HttpServletResponse response = ServletActionContext.getResponse();
				PropertyRateManager rateController=new PropertyRateManager();
				response.setContentType("application/json");
				int serialno=1;
				this.listPropertyRates=rateController.listPropertyRates(propertyId,tsCurrentDate);
				if(listPropertyRates.size()>0 && !listPropertyRates.isEmpty()){
					for(PropertyRate rates:listPropertyRates){
						if (!jsonOutput.equalsIgnoreCase(""))
							jsonOutput += ",{";
						else
							jsonOutput += "{";
						jsonOutput += "\"propertyRateId\":\"" + rates.getPropertyRateId() + "\"";
						String start = new SimpleDateFormat("dd-MMM-yyyy").format(rates.getStartDate());
						String end = new SimpleDateFormat("dd-MMM-yyyy").format(rates.getEndDate());
						jsonOutput += ",\"startDate\":\"" + start + "\"";
						jsonOutput += ",\"endDate\":\"" + end + "\"";
						PropertyRateDetail rateDetails=rateDetailController.listDetails(rates.getPropertyRateId());
						dblBaseAmount=rateDetails.getBaseAmount();
						jsonOutput += ",\"baseAmount\":\""+dblBaseAmount+"\"";
						jsonOutput += ",\"serialNo\":\""+serialno+"\"";
						jsonOutput += ",\"accommodationType\":\"" + rates.getPropertyAccommodation().getAccommodationType().trim()+ "\"";
						jsonOutput += ",\"sourceType\":\"" + rates.getPmsSource().getSourceName().trim()+ "\"";
						jsonOutput += "}";
						serialno++;
					}
				}
				response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			}
			catch(Exception e){
				logger.error(e);
				e.printStackTrace();
			}
			finally{
				
			}
		 return null;
	 }
	 
	 public static boolean isNumeric(String str)
	 {
	     for (char c : str.toCharArray())
	     {
	         if (!Character.isDigit(c)) return false;
	     }
	     return true;
	 }
	 
	 
	 public Long getAvailableCount(int propertyId,Timestamp tsStartDate,Timestamp tsEndDate,long available,int accommodationId) throws IOException {
			String startDate = new SimpleDateFormat("yyyy-MM-dd").format(tsStartDate);
			String endDate = new SimpleDateFormat("yyyy-MM-dd").format(tsEndDate);
			DateTime start = DateTime.parse(startDate);
		    DateTime end = DateTime.parse(endDate);
			PmsBookingManager bookingController = new PmsBookingManager();
			
			List<DateTime> between = DateUtil.getDateRange(start, end);
			List<Long> myList = new ArrayList<Long>();
			
			long rmc = 0;
			this.roomCnt = rmc;
			
			for (DateTime d : between)
		     {
				java.util.Date availDate = d.toDate();
					  PmsAvailableRooms roomCount = bookingController.findCount(availDate, accommodationId);
						if(roomCount.getRoomCount() == null){
							this.roomCnt = (available-rmc);
						}
						else{
							this.roomCnt = (available-roomCount.getRoomCount());
						}
						myList.add(this.roomCnt);
		     
		     }
		        long availableCount = Collections.min(myList);
	
		        return availableCount;
		          
		}
	 
	 public String addSingleRate(){
			try{
				this.propertyId = (Integer) sessionMap.get("propertyId");
				PmsSourceTypeManager sourceTypeController = new PmsSourceTypeManager();
				PmsSourceManager sourceController = new PmsSourceManager();
				PropertyRateManager rateController = new PropertyRateManager();
				PropertyRateDetailManager rateDetailController=new PropertyRateDetailManager();
				PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
				PropertyRatePlanManager propertyRateController = new PropertyRatePlanManager();
		        PropertyRatePlanDetailManager ratePlanDetailsController=new PropertyRatePlanDetailManager();
				PmsPropertyManager propertyController = new PmsPropertyManager();
				OtaHotelsManager otaController = new OtaHotelsManager();
				this.userId=(Integer)sessionMap.get("userId");
				SimpleDateFormat sdfformat = new SimpleDateFormat("yyyy-MM-dd");
			    java.util.Date date=new java.util.Date();
	        	Calendar calDate=Calendar.getInstance();
	        	calDate.setTime(date);
	        	calDate.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
	        	String strCurrentDate = new SimpleDateFormat("yyyy-MM-dd").format(calDate.getTime());
			    java.util.Date currentDate = sdfformat.parse(strCurrentDate);
			    Timestamp tsCurrentDate = new Timestamp(currentDate.getTime());
			    
			    SimpleDateFormat dayformat = new SimpleDateFormat("EEEE"); // the day of the week spelled out completely
			    
				for (int i = 0; i < array.size(); i++) 
			    {
					DateFormat format = new SimpleDateFormat("MMM d,yyyy" , Locale.ENGLISH);
				    java.util.Date dateStart = format.parse(array.get(i).getStartBlockDate());
				    Calendar calBlockStart=Calendar.getInstance();
				    calBlockStart.setTime(dateStart);
				    java.util.Date checkInDate = calBlockStart.getTime();
				    int dayOfWeek=calBlockStart.get(Calendar.DAY_OF_WEEK);
				    String dayId = Integer.toString(dayOfWeek);
				    checkedDays = dayId;
				    String startDate = new SimpleDateFormat("yyyy-MM-dd").format(checkInDate);
		            DateTime start = DateTime.parse(startDate);
		            java.util.Date bookingDate=  start.toDate();
		            
		       /*     if(array.get(i).getSourceTypeId() == 2){

		            	ota start 	
		            		  // int getDaysOfWeek = dayOfWeek;
		            		    //getDaysOfWeek(ids);
		            			//String checkedDays = getDaysOfWeek(getCheckedDays());
		            		    int sourceId = 3;
		            		    OtaHotels otaHotels =  otaController.getOtaHotels(this.propertyId,sourceId);
		            			DefaultHttpClient httpClient = new DefaultHttpClient();
		            			HttpPost postRequest = new HttpPost("https://in.goibibo.com/api/chmv2/updateroomrates/?bearer_token="+otaHotels.getOtaBearerToken()+"&channel_token="+otaHotels.getOtaChannelToken()+"");
		            			
		            			
		            			//List<PropertyRate> rateList =  rateController.list(getPropertyId(),getPropertyAccommodationId());  
		            			List <OtaHotelDetails> otaDetailList = otaController.listDetails(otaHotels.getOtaHotelCode(),array.get(i).getPropertyAccommodationId());
		            			
		            			for(OtaHotelDetails otaDetail : otaDetailList){
		            				PropertyRatePlan rateplan = propertyRateController.find(otaDetail.getPropertyRatePlan().getRatePlanId()); 
		            				if(otaDetail.getPropertyRatePlan().getRatePlanId() == 1){
		            			String xmlBody = "<?xml version='1.0' encoding='UTF-8' ?>"+
		            		    "<Website Name='ingoibibo' HotelCode='"+otaHotels.getOtaHotelCode()+"' Version='1'>"+
		            		            "<RatePlan>"+
		            		            "<RatePlanCode>"+otaDetail.getOtaRateplanId()+"</RatePlanCode>"+
		            		            "<StartDate Format='yyyy-mm-dd'>"+startDate+"</StartDate>"+
		            		            "<EndDate Format='yyyy-mm-dd'>"+startDate+"</EndDate>"+
		            		            "<SingleOccupancyRates>"+				            
		            		            "<SellRate>"+ array.get(i).getBaseAmount()+"</SellRate>"+
		            		            "</SingleOccupancyRates>"+
		            		            "<ExtraChildCharge>"+array.get(i).getRateExtraChild()+"</ExtraChildCharge>"+
		            		            "<ExtraAdultCharge>"+array.get(i).getRateExtraAdult()+"</ExtraAdultCharge>"+
		            		            "<DaysOfWeek Mon='"+getDaysOfWeek("2")+"' Tue='"+getDaysOfWeek("3")+"' Wed='"+getDaysOfWeek("4")+"' Thu='"+getDaysOfWeek("5")+"' Fri='"+getDaysOfWeek("6")+"' Sat='"+getDaysOfWeek("7")+"' Sun='"+getDaysOfWeek("1")+"'></DaysOfWeek>"+
		            		            "<ContractType b2b='False' b2c='True' mobile='False' fph='False'></ContractType>"+
		            		            "<Currency>INR</Currency>"+
		            		            "</RatePlan>"+
		            		      "</Website>";
		            				
		            				
		            			    
		            				
		            			    
		            			StringEntity input = new StringEntity(xmlBody);
		            			input.setContentType("text/xml");
		            			postRequest.setEntity(input);
		            			HttpResponse httpResponse = httpClient.execute(postRequest);
		            			HttpEntity entity = httpResponse.getEntity();
		                        // Read the contents of an entity and return it as a String.
		                        String content = EntityUtils.toString(entity);
		            				}
		            				else if(otaDetail.getPropertyRatePlan().getRatePlanId() == 2){
		            					PmsProperty property = propertyController.find(getPropertyId());
		            					int rateid = 2;
		            					PropertyRatePlanDetail ratePlanDetails=ratePlanDetailsController.listRateDetails(getPropertyId(),array.get(i).getPropertyAccommodationId(),rateid);
		            					double cbTotal = array.get(i).getBaseAmount() + ratePlanDetails.getTariffAmount();
		            					String xmlBody = "<?xml version='1.0' encoding='UTF-8' ?>"+
		            						    "<Website Name='ingoibibo' HotelCode='"+otaHotels.getOtaHotelCode()+"' Version='1'>"+
		            						            "<RatePlan>"+
		            						            "<RatePlanCode>"+otaDetail.getOtaRateplanId()+"</RatePlanCode>"+
		            						            "<StartDate Format='yyyy-mm-dd'>"+startDate+"</StartDate>"+
		            						            "<EndDate Format='yyyy-mm-dd'>"+startDate+"</EndDate>"+
		            						            "<SingleOccupancyRates>"+				            
		            						            "<SellRate>"+cbTotal+"</SellRate>"+
		            						            "</SingleOccupancyRates>"+
		            						            "<ExtraChildCharge>"+array.get(i).getRateExtraChild()+"</ExtraChildCharge>"+
		            						            "<ExtraAdultCharge>"+array.get(i).getRateExtraAdult()+"</ExtraAdultCharge>"+
                                                        "<DaysOfWeek Mon='"+getDaysOfWeek("2")+"' Tue='"+getDaysOfWeek("3")+"' Wed='"+getDaysOfWeek("4")+"' Thu='"+getDaysOfWeek("5")+"' Fri='"+getDaysOfWeek("6")+"' Sat='"+getDaysOfWeek("7")+"' Sun='"+getDaysOfWeek("1")+"'></DaysOfWeek>"+
		            						            "<ContractType b2b='False' b2c='True' mobile='False' fph='False'></ContractType>"+
		            						            "<Currency>INR</Currency>"+
		            						            "</RatePlan>"+
		            						      "</Website>";
		            								
		            								
		            							    
		            								
		            							    
		            							StringEntity input = new StringEntity(xmlBody);
		            							input.setContentType("text/xml");
		            							postRequest.setEntity(input);
		            							HttpResponse httpResponse = httpClient.execute(postRequest);
		            							HttpEntity entity = httpResponse.getEntity();
		            				            // Read the contents of an entity and return it as a String.
		            				            String content = EntityUtils.toString(entity);
		            				}
		            		    	}
		            			
		            		ota end 
		            		    	}*/
		            
		            PropertyRate rates=new PropertyRate();
		            rates.setIsActive(true);
		            rates.setIsDeleted(false);
		            PmsProperty pmsProperty=propertyController.find(this.propertyId);
		            rates.setPmsProperty(pmsProperty);
		            PropertyAccommodation accommodation=accommodationController.find(array.get(i).getPropertyAccommodationId());
		            rates.setPropertyAccommodation(accommodation);
		            rates.setCreatedBy(this.userId);
		            PmsSourceType sourceType=sourceTypeController.find(array.get(i).getSourceTypeId());
		            rates.setPmsSourceType(sourceType);
		            rates.setCreatedDate(tsCurrentDate);
		            rates.setStartDate(new Timestamp(bookingDate.getTime()));
		            rates.setSmartPriceIsActive(false);
		            rates.setEndDate(new Timestamp(bookingDate.getTime()));
		            PropertyRate rate=rateController.add(rates);
		            Integer rateId=rate.getPropertyRateId();
		            
		            
		            PropertyRateDetail rateDetail=new PropertyRateDetail();
		            rateDetail.setBaseAmount(array.get(i).getBaseAmount());
		            rateDetail.setCreatedBy(this.userId);
		            rateDetail.setCreatedDate(tsCurrentDate);
		            rateDetail.setIsActive(true);
		            rateDetail.setIsDeleted(false);
		            PropertyRate propertyRate=rateController.find(rateId);
		            rateDetail.setPropertyRate(propertyRate);
		            rateDetail.setDaysOfWeek(dayformat.format(bookingDate).toLowerCase());
		            rateDetail.setDayPart(dayOfWeek);
		            rateDetail.setExtraAdult(array.get(i).getRateExtraAdult());
		            rateDetail.setExtraChild(array.get(i).getRateExtraChild());
		            rateDetailController.add(rateDetail);
		            
			    }
				
			}catch(Exception e){
				logger.error(e);
				e.printStackTrace();
			}
			return null;
		}
	 
	 public String addSingleMMRate(){

			try{
				this.propertyId = (Integer) sessionMap.get("propertyId");
				PmsSourceTypeManager sourceTypeController = new PmsSourceTypeManager();
				PmsSourceManager sourceController = new PmsSourceManager();
				PropertyRateManager rateController = new PropertyRateManager();
				PropertyRateDetailManager rateDetailController=new PropertyRateDetailManager();
				PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
				PropertyRatePlanManager propertyRateController = new PropertyRatePlanManager();
		        PropertyRatePlanDetailManager ratePlanDetailsController=new PropertyRatePlanDetailManager();
				PmsPropertyManager propertyController = new PmsPropertyManager();
				OtaHotelsManager otaController = new OtaHotelsManager();
				this.userId=(Integer)sessionMap.get("userId");
				SimpleDateFormat sdfformat = new SimpleDateFormat("yyyy-MM-dd");
			    java.util.Date date=new java.util.Date();
	        	Calendar calDate=Calendar.getInstance();
	        	calDate.setTime(date);
	        	calDate.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
	        	String strCurrentDate = new SimpleDateFormat("yyyy-MM-dd").format(calDate.getTime());
			    java.util.Date currentDate = sdfformat.parse(strCurrentDate);
			    Timestamp tsCurrentDate = new Timestamp(currentDate.getTime());
			    
			    SimpleDateFormat dayformat = new SimpleDateFormat("EEEE"); // the day of the week spelled out completely
			    
				for (int i = 0; i < array.size(); i++) 
			    {
					DateFormat format = new SimpleDateFormat("MMM d,yyyy" , Locale.ENGLISH);
				    java.util.Date dateStart = format.parse(array.get(i).getStartBlockDate());
				    Calendar calBlockStart=Calendar.getInstance();
				    calBlockStart.setTime(dateStart);
				    java.util.Date checkInDate = calBlockStart.getTime();
				    int dayOfWeek=calBlockStart.get(Calendar.DAY_OF_WEEK);
				    String dayId = Integer.toString(dayOfWeek);
				    checkedDays = dayId;
				    String startDate = new SimpleDateFormat("yyyy-MM-dd").format(checkInDate);
		            DateTime start = DateTime.parse(startDate);
		            java.util.Date bookingDate=  start.toDate();
		           /* if(array.get(i).isOtaEnable()){


            		    int sourceId = 3;
            		    OtaHotels otaHotels =  otaController.getOtaHotels(this.propertyId,sourceId);
            			DefaultHttpClient httpClient = new DefaultHttpClient();
            			HttpPost postRequest = new HttpPost("https://in.goibibo.com/api/chmv2/updateroomrates/?bearer_token="+otaHotels.getOtaBearerToken()+"&channel_token="+otaHotels.getOtaChannelToken()+"");

            			List <OtaHotelDetails> otaDetailList = otaController.listDetails(otaHotels.getOtaHotelCode(),array.get(i).getPropertyAccommodationId());
            			
            			for(OtaHotelDetails otaDetail : otaDetailList){
            				PropertyRatePlan rateplan = propertyRateController.find(otaDetail.getPropertyRatePlan().getRatePlanId()); 
            				if(otaDetail.getPropertyRatePlan().getRatePlanId() == 1){
            					int rateid = 1;
            					PropertyRatePlanDetail ratePlanDetails=ratePlanDetailsController.listRateDetails(getPropertyId(),getPropertyAccommodationId(),rateid);
            					double epTotal = array.get(i).getOtaRateAmount() - ratePlanDetails.getTariffAmount();
	            			String xmlBody = "<?xml version='1.0' encoding='UTF-8' ?>"+
	            		    "<Website Name='ingoibibo' HotelCode='"+otaHotels.getOtaHotelCode()+"' Version='1'>"+
	            		            "<RatePlan>"+
	            		            "<RatePlanCode>"+otaDetail.getOtaRateplanId()+"</RatePlanCode>"+
	            		            "<StartDate Format='yyyy-mm-dd'>"+startDate+"</StartDate>"+
	            		            "<EndDate Format='yyyy-mm-dd'>"+startDate+"</EndDate>"+
	            		            "<SingleOccupancyRates>"+				            
	            		            "<SellRate>"+ epTotal+"</SellRate>"+
	            		            "</SingleOccupancyRates>"+
	            		            "<DoubleOccupancyRates>"+				            
	            		            "<SellRate>"+ epTotal+"</SellRate>"+
	            		            "</DoubleOccupancyRates>"+
	            		            "<TripleOccupancyRates>"+				            
	            		            "<SellRate>"+ epTotal+"</SellRate>"+
	            		            "</TripleOccupancyRates>"+
	            		            "<ExtraChildCharge>"+array.get(i).getRateExtraChild()+"</ExtraChildCharge>"+
	            		            "<ExtraAdultCharge>"+array.get(i).getRateExtraAdult()+"</ExtraAdultCharge>"+
	            		            "<DaysOfWeek Mon='"+getDaysOfWeek("2")+"' Tue='"+getDaysOfWeek("3")+"' Wed='"+getDaysOfWeek("4")+"' Thu='"+getDaysOfWeek("5")+"' Fri='"+getDaysOfWeek("6")+"' Sat='"+getDaysOfWeek("7")+"' Sun='"+getDaysOfWeek("1")+"'></DaysOfWeek>"+
	            		            "<ContractType b2b='False' b2c='True' mobile='False' fph='False'></ContractType>"+
	            		            "<Currency>INR</Currency>"+
	            		            "</RatePlan>"+
	            		      "</Website>";
            				
            			StringEntity input = new StringEntity(xmlBody);
            			input.setContentType("text/xml");
            			postRequest.setEntity(input);
            			HttpResponse httpResponse = httpClient.execute(postRequest);
            			HttpEntity entity = httpResponse.getEntity();
                        // Read the contents of an entity and return it as a String.
                        String content = EntityUtils.toString(entity);
    				}
    				else if(otaDetail.getPropertyRatePlan().getRatePlanId() == 2){
    					PmsProperty property = propertyController.find(getPropertyId());
    					
    					String xmlBody = "<?xml version='1.0' encoding='UTF-8' ?>"+
    						    "<Website Name='ingoibibo' HotelCode='"+otaHotels.getOtaHotelCode()+"' Version='1'>"+
    						            "<RatePlan>"+
    						            "<RatePlanCode>"+otaDetail.getOtaRateplanId()+"</RatePlanCode>"+
    						            "<StartDate Format='yyyy-mm-dd'>"+startDate+"</StartDate>"+
    						            "<EndDate Format='yyyy-mm-dd'>"+startDate+"</EndDate>"+
    						            "<SingleOccupancyRates>"+				            
    						            "<SellRate>"+array.get(i).getOtaRateAmount()+"</SellRate>"+
    						            "</SingleOccupancyRates>"+
    						            "<DoubleOccupancyRates>"+				            
    						            "<SellRate>"+array.get(i).getOtaRateAmount()+"</SellRate>"+
    						            "</DoubleOccupancyRates>"+
    						            "<TripleOccupancyRates>"+				            
    						            "<SellRate>"+array.get(i).getOtaRateAmount()+"</SellRate>"+
    						            "</TripleOccupancyRates>"+
    						            "<ExtraChildCharge>"+array.get(i).getRateExtraChild()+"</ExtraChildCharge>"+
    						            "<ExtraAdultCharge>"+array.get(i).getRateExtraAdult()+"</ExtraAdultCharge>"+
                                     "<DaysOfWeek Mon='"+getDaysOfWeek("2")+"' Tue='"+getDaysOfWeek("3")+"' Wed='"+getDaysOfWeek("4")+"' Thu='"+getDaysOfWeek("5")+"' Fri='"+getDaysOfWeek("6")+"' Sat='"+getDaysOfWeek("7")+"' Sun='"+getDaysOfWeek("1")+"'></DaysOfWeek>"+
    						            "<ContractType b2b='False' b2c='True' mobile='False' fph='False'></ContractType>"+
    						            "<Currency>INR</Currency>"+
    						            "</RatePlan>"+
    						      "</Website>";
    								
    								
						    
							
						    
						StringEntity input = new StringEntity(xmlBody);
						input.setContentType("text/xml");
						postRequest.setEntity(input);
						HttpResponse httpResponse = httpClient.execute(postRequest);
						HttpEntity entity = httpResponse.getEntity();
			            // Read the contents of an entity and return it as a String.
			            String content = EntityUtils.toString(entity);
    				}
            	}
		    	
		            }*/
		           
		            
		            PropertyRate rates=new PropertyRate();
		            rates.setIsActive(true);
		            rates.setIsDeleted(false);
		            PmsProperty pmsProperty=propertyController.find(this.propertyId);
		            rates.setPmsProperty(pmsProperty);
		            PropertyAccommodation accommodation=accommodationController.find(array.get(i).getPropertyAccommodationId());
		            rates.setPropertyAccommodation(accommodation);
		            rates.setCreatedBy(this.userId);
		            PmsSourceType sourceType=sourceTypeController.find(array.get(i).getSourceTypeId());
		            rates.setPmsSourceType(sourceType);
		            rates.setCreatedDate(tsCurrentDate);
		            rates.setStartDate(new Timestamp(bookingDate.getTime()));
		            rates.setSmartPriceIsActive(false);
		            rates.setEndDate(new Timestamp(bookingDate.getTime()));
		            PropertyRate rate=rateController.add(rates);
		            Integer rateId=rate.getPropertyRateId();
		            
		            
		            PropertyRateDetail rateDetail=new PropertyRateDetail();
		            rateDetail.setBaseAmount(array.get(i).getBaseAmount());
		            rateDetail.setMinimumBaseAmount(array.get(i).getMinimumAmount());
		            rateDetail.setMaximumBaseAmount(array.get(i).getMaximumAmount());
		            rateDetail.setCreatedBy(this.userId);
		            rateDetail.setCreatedDate(tsCurrentDate);
		            rateDetail.setIsActive(true);
		            rateDetail.setIsDeleted(false);
		            PropertyRate propertyRate=rateController.find(rateId);
		            rateDetail.setPropertyRate(propertyRate);
		            rateDetail.setDaysOfWeek(dayformat.format(bookingDate).toLowerCase());
		            rateDetail.setDayPart(dayOfWeek);
		            rateDetail.setExtraAdult(array.get(i).getRateExtraAdult());
		            rateDetail.setExtraChild(array.get(i).getRateExtraChild());
		            
		            rateDetailController.add(rateDetail);
		            
			    }
				
			}catch(Exception e){
				logger.error(e);
				e.printStackTrace();
			}
			return null;
		
	 }
	 public String getOldRevenueRateDetails() throws IOException, ParseException{
		 try {
		    
			 this.sourceTypeId = getSourceTypeId();
			 sessionMap.put("sourceTypeId",sourceTypeId); 
				
			this.accommodationId = getAccommodationId();
			sessionMap.put("accommodationId",accommodationId);
			
			this.strStartDate=getStrStartDate();
			this.strEndDate=getStrEndDate();
			
			if(this.strStartDate==null || this.strEndDate==null ){
				return null;
			}
			
			DateFormat format = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
		    java.util.Date dateStart = format.parse(getStrStartDate());
		    java.util.Date dateEnd = format.parse(getStrEndDate());
		   
		    
		    Calendar calCheckStart=Calendar.getInstance();
		    calCheckStart.setTime(dateStart);
		    java.util.Date checkInDate = calCheckStart.getTime();
		    this.arrivalDate=new Timestamp(checkInDate.getTime());
		    
		    Calendar calCheckEnd=Calendar.getInstance();
		    calCheckEnd.setTime(dateEnd);
		    java.util.Date checkOutDate = calCheckEnd.getTime();
		    this.departureDate=new Timestamp(checkOutDate.getTime());
		    
		    
			this.propertyId = (Integer) sessionMap.get("propertyId");
		    
			
		    
			HttpServletResponse response = ServletActionContext.getResponse();
			PropertyAccommodationRoomManager  roomController  = new PropertyAccommodationRoomManager();
			
			PmsSourceManager sourceController = new PmsSourceManager();
			PropertyRateManager rateController = new PropertyRateManager();
			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
			PropertyRateDetailManager rateDetailController = new PropertyRateDetailManager();
			PmsPropertyManager propertyController = new PmsPropertyManager();
			boolean dateChk=false;
			
			int roomCount=0,availableCount=0,totalCount=0,totalDate=0,dateCount=0;
	    	Double dblTotalBaseAmount=0.0,dblBaseAmount=0.0,dblPercentCount=0.0,dblSmartPricePercent=0.0;
	    	Double dblPercentFrom,dblPercentTo;
	    	Boolean isPositive=false,isNegative=false;
	    	
			int fromDay=0,fromMonth=0,fromYear=0;
			int toDay=0,toMonth=0,toYear=0;
			SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
			Calendar calStart=Calendar.getInstance();
			calStart.setTime(getArrivalDate());
			java.util.Date startDate = calStart.getTime();             
	    	String strFromDate = format1.format(startDate);  
	    	
	    	
	    	Calendar calEnd=Calendar.getInstance();
	    	calEnd.setTime(getDepartureDate());
	    	java.util.Date endDate = calEnd.getTime();  
	    	String strToDate = format1.format(endDate); 
	    	
	    	
	    	java.util.Date fromDate = format1.parse(strFromDate);
			Timestamp tsFromDate=new Timestamp(fromDate.getTime());

			java.util.Date toDate = format1.parse(strToDate);
			Timestamp tsToDate=new Timestamp(toDate.getTime());
			
			String jsonOutput = "";
			String jsonRates ="";
			
			response.setContentType("application/json");
			   
			HashMap<String,String> dateMap=new HashMap<String,String>();
			HashMap<String,String> dayMap=new HashMap<String,String>();
			ArrayList<Integer> listSmartPriceChecked=new ArrayList<Integer>();
			
			ArrayList<String> arrListDateId=new ArrayList<String>();
			SimpleDateFormat formatEE = new SimpleDateFormat("EE", Locale.ENGLISH);
			SimpleDateFormat formatEEEE = new SimpleDateFormat("EEEE", Locale.ENGLISH);
			ArrayList<String> arrListRateDetail=new ArrayList<String>();
			List<PropertyAccommodation> accommodationList= roomController.list(accommodationId,getPropertyId());
			DateFormat f = new SimpleDateFormat("EEEE");
			List<PmsSmartPrice> listSmartPriceDetail=null; 
	    	List<AccommodationRoom> listAccommodationRoom=null;
	    	PropertyAccommodation accommodations=accommodationController.find(accommodationId);
	    	if (!jsonOutput.equalsIgnoreCase(""))
				jsonOutput += ",{";
			
			else
				jsonOutput = "{";
	    	
	    	jsonOutput += "\"accommodationId\":\""+accommodations.getAccommodationId()+"\"";
		 	jsonOutput += ",\"accommodationType\":\""+accommodations.getAccommodationType()+"\"";
		 	jsonOutput += ",\"sourceTypeId\":\""+this.sourceTypeId+"\"";
			String strStartDate=null,strEndDate=null,strPropertyRateId=null,strAmount=null,strDaysofWeek=null,strDays=null,strDateValues=null;
			DateTime dtFromDate = DateTime.parse(strFromDate);
	        DateTime dtToDate = DateTime.parse(strToDate);
			List<DateTime> listAllDates=DateUtil.getDateRange(dtFromDate, dtToDate);
			for(DateTime d:listAllDates){
				List<PropertyRate> dateList =  rateController.listAllDates(getPropertyId(),getAccommodationId(),getSourceTypeId(),d.toDate());
				//List<PropertyRate> dateList =  rateController.list(getPropertyId(),getAccommodationId(),getSourceId(),tsFromDate,tsToDate);
			    if(!dateList.isEmpty()){
			    	for (PropertyRate rates : dateList) {
			    		strPropertyRateId=String.valueOf(rates.getPropertyRateId());
			    		strStartDate=format1.format(rates.getStartDate());
			    		strEndDate=format1.format(rates.getEndDate());
			    		DateTime FromDate = DateTime.parse(strStartDate);
				        DateTime ToDate = DateTime.parse(strEndDate);
				        
				        int propertyRatesId=rates.getPropertyRateId(); 
				        
				        java.util.Date DateValues = d.toDate();
			        	strDateValues=format1.format(DateValues);
			        	
				        arrListDateId.add(strDateValues);
			        	arrListDateId.add(strPropertyRateId);
			        	
			    		List<PropertyRateDetail> rateDetailList =  rateDetailController.listRateDetail(propertyRatesId,f.format(d.toDate()).toLowerCase());
			    		
			    		if(!rateDetailList.isEmpty()){
			    			
			    			
			    			for(PropertyRateDetail rateDetails: rateDetailList){
			    				strAmount=String.valueOf(rateDetails.getBaseAmount());
			    				strDaysofWeek=rateDetails.getDaysOfWeek().toLowerCase();
			    				arrListDateId.add(strDaysofWeek);
				        		arrListDateId.add(strAmount);
			    				List<String> daysBetween = DateUtil.getDayRange(FromDate, ToDate);
			    				Iterator<String> iterDayValues=daysBetween.iterator();
						        while(iterDayValues.hasNext()){
						        	strDays=iterDayValues.next().toLowerCase();
						        	if(strDaysofWeek.contains(strDays)){
						        		arrListDateId.add(strDaysofWeek);
						        		arrListDateId.add(strAmount);
						        	}
						        }
			    			}
			    			break;
			    		}
			    		dateChk=true;
			    	}
			    }else{
			    	if(accommodationList.size()>0 && !accommodationList.isEmpty()){
			    		for (PropertyAccommodation accommodationRate : accommodationList) {
			    			strAmount=String.valueOf(accommodationRate.getBaseAmount());
			    			 java.util.Date DateValues = d.toDate();
					         strDateValues=format1.format(DateValues);
						     arrListDateId.add(strDateValues);
					         arrListDateId.add(String.valueOf(0));
					         arrListDateId.add(f.format(d.toDate()).toLowerCase());
				        	 arrListDateId.add(strAmount);
			    		}
			    		//break;
			    	}
			    }
			}
			
		    
		   int intCount=0;
		   String strArrays[] = new String[arrListDateId.size()];
		    Iterator<String> iterListValues=arrListDateId.iterator();
		    while(iterListValues.hasNext()){
	    		String strTemp=iterListValues.next();
	    		strArrays[intCount]=strTemp;
	    		intCount++;		
		    }
		    
		    String dateval="",doubleval="",rateId="";
		    for(String s : strArrays) {
		          String[] s2 = s.split(" ");
		          for(String results : s2) {
		              if(results.contains("-")){//Date check
		            	  dateval=results;
		            	  arrListRateDetail.add(dateval);
		            	  
		              }else{// Number
		            	  if(isNumeric(results)){
		            		  rateId=results;
		            	  }
		            	  else{
		            		  int first = results.indexOf(".");
		            		  if ( (first >= 0) && (first - results.lastIndexOf(".")) == 0) {
		            		      // only one decimal point
		            			  doubleval=results;
		            			  arrListRateDetail.add(doubleval);
		            		  }
		            		  else {
		            			
		            			  // no decimal point or more than one decimal point
		            		  }
		            	  }
		              }
		  		      if(!doubleval.isEmpty() && !dateval.isEmpty()){
		  		    	dateMap.put(dateval, doubleval);
		              }
		  		      if(!rateId.isEmpty() && !dateval.isEmpty()){
		  		    	  dayMap.put(dateval, rateId);
		  		      }
		          }
		      }
		    intCount=0;
            String[] listValues=new String[arrListRateDetail.size()];
            Iterator<String> rateDetailsIter=arrListRateDetail.iterator();
		    while (rateDetailsIter.hasNext()) {
                String strTemp=rateDetailsIter.next();
                listValues[intCount]=strTemp;
	    		intCount++;		
		    }
		   
		    String strDay="", strRateId="";
		    int intRateId=0;
	        
            for(DateTime d:listAllDates){
            	java.util.Date DateValues = d.toDate();
            	strDateValues=format1.format(DateValues);
        		if(arrListRateDetail.contains(strDateValues)){
        			java.util.Date dteReqDate= format1.parse(strDateValues);
	    			Timestamp tsDate=new Timestamp(dteReqDate.getTime());
	    			
        			strDay=formatEEEE.format(dteReqDate).toLowerCase();
        			strRateId=dayMap.get(strDateValues);
        			intRateId=Integer.parseInt(strRateId);
        			if(listSmartPriceChecked.contains(intRateId)){
        				List<PropertyRateDetail> rateDetailList1 =   rateDetailController.list(intRateId,strDay);
        				PropertyRate rates=rateController.listRates(intRateId);
        				int accommId=rates.getPropertyAccommodation().getAccommodationId();
        				int rateCount=0;
            			if(rateDetailList1.size()>0 && !rateDetailList1.isEmpty()){
            				for (PropertyRateDetail rate : rateDetailList1)
    		    			{
            					if(rateCount==0){

                					 dblBaseAmount=rate.getBaseAmount();
                					 listAccommodationRoom=accommodationController.listPropertyTotalRooms(propertyId, accommId);
    	               				 if(listAccommodationRoom.size()>0 && !listAccommodationRoom.isEmpty()){
    	               					 for(AccommodationRoom roomsList:listAccommodationRoom){
    	               						 roomCount=(int) roomsList.getRoomCount();
    	               						 long minimum = getAvailableCount(propertyId,tsDate,tsDate,roomCount,accommId); 
    	               						 availableCount=(int)minimum;
    	               						 dblPercentCount=(double)((roomCount-availableCount)*100)/roomCount;
    	               						 totalCount=(int) Math.round(dblPercentCount);
    	               						 listSmartPriceDetail=rateDetailController.listSmartPriceDetail(totalCount);
    	               						 if(listSmartPriceDetail.size()>0 && !listSmartPriceDetail.isEmpty()){
    	               							 for(PmsSmartPrice smartPrice: listSmartPriceDetail){
    	               								 dblSmartPricePercent=smartPrice.getAmountPercent();
    	               								 dblPercentFrom=smartPrice.getPercentFrom();
    	               								 dblPercentTo=smartPrice.getPercentTo();
    	               								 if(dblPercentFrom>=0 && dblPercentTo<=59){
    	               									 isNegative=true;
    	               								 }else if(dblPercentFrom>=60 && dblPercentTo<=99){
    	               									 isPositive=true;
    	               								 }
    	               								 if(isNegative){
    	               									 dblTotalBaseAmount=dblBaseAmount-(dblBaseAmount*dblSmartPricePercent/100);
    	               								 }
    	               								 if(isPositive){
    	               									dblTotalBaseAmount=dblBaseAmount+(dblBaseAmount*dblSmartPricePercent/100);
    	               								 }
    	               								rateCount++;
    	               							 }
    	               						 }
    	               					 }
    	               				 }
    	               				isNegative=false;
    	               				isPositive=false;
                					if (!jsonOutput.equalsIgnoreCase(""))
                    					jsonOutput += ",{";
                    				
                    				else
                    					
                    					
                    					jsonOutput = "{";
                					
                					 	jsonOutput += "\"tariff\":\""+dblTotalBaseAmount+" \"";
                					 	jsonOutput += ",\"adult\":\""+(rate.getExtraChild())+"\"";
                					 	jsonOutput += ",\"child\":\""+(rate.getExtraChild())+"\"";
                					 	java.util.Date baseDate=new SimpleDateFormat("yyyy-MM-dd").parse(strDateValues);
                					 	jsonOutput += ",\"date\":\""+new SimpleDateFormat("MMMM d, yyyy").format(baseDate)+"\"";
 	            				    
                					 	jsonOutput += "}";
        		    			 
            					}
            					
    		    			 }
            			}
        			}
        			else{
	        			List<PropertyRateDetail> rateDetailList1 =   rateDetailController.list(intRateId,strDay);
	        			
	        			if(rateDetailList1.size()>0 && !rateDetailList1.isEmpty()){
	        				for (PropertyRateDetail rate : rateDetailList1)
			    			 {
	        					if (!jsonRates.equalsIgnoreCase(""))
	        						jsonRates += ",{";
	            				
	            				else
	            					
	            					jsonRates = "{";
            					
	        					jsonRates += "\"tariff\":\""+rate.getBaseAmount()+" \"";
	        					jsonRates += ",\"adult\":\""+(rate.getExtraAdult())+"\"";
	        					jsonRates += ",\"child\":\""+(rate.getExtraChild())+"\"";
//	        					jsonRates += ",\"date\":\""+strDateValues+"\"";
	        					java.util.Date baseDate=new SimpleDateFormat("yyyy-MM-dd").parse(strDateValues);
        					 	jsonRates += ",\"date\":\""+new SimpleDateFormat("MMMM d, yyyy").format(baseDate)+"\"";
	         				    
	        					jsonRates += "}";
			    			 }
	        			}
	        			else
	        			{
	        				for (PropertyAccommodation accommodationRate : accommodationList) {
	        					if (!jsonRates.equalsIgnoreCase(""))
	        						jsonRates += ",{";
	            				
	            				else
	            					
	            					jsonRates = "{";
            					
	        					jsonRates += "\"tariff\":\""+accommodationRate.getBaseAmount()+" \"";
	        					jsonRates += ",\"adult\":\""+(accommodationRate.getExtraAdult())+"\"";
	        					jsonRates += ",\"child\":\""+(accommodationRate.getExtraChild())+"\"";
//	        					jsonRates += ",\"date\":\""+strDateValues+"\"";
	        					java.util.Date baseDate=new SimpleDateFormat("yyyy-MM-dd").parse(strDateValues);
	        					jsonRates += ",\"date\":\""+new SimpleDateFormat("MMMM d, yyyy").format(baseDate)+"\"";
	         				    
	        					jsonRates += "}";
        					 	
	        				}
	        			}
        			}
        		}
        		else{
    				
    				for (PropertyAccommodation accommodationRate : accommodationList) {
    					if (!jsonRates.equalsIgnoreCase(""))
    						jsonRates += ",{";
        				
        				else
        					
        					jsonRates = "{";
    					
    					jsonRates += "\"tariff\":\""+accommodationRate.getBaseAmount()+" \"";
    					jsonRates += ",\"adult\":\""+(accommodationRate.getExtraAdult())+"\"";
    					jsonRates += ",\"child\":\""+(accommodationRate.getExtraChild())+"\"";
//    					jsonRates += ",\"date\":\""+strDateValues+"\"";
    					java.util.Date baseDate=new SimpleDateFormat("yyyy-MM-dd").parse(strDateValues);
    					jsonRates += ",\"date\":\""+new SimpleDateFormat("MMMM d, yyyy").format(baseDate)+"\"";
	 				    
    					jsonRates += "}";
    				}
        		}
        		
        	
        	
            }
            jsonOutput += ",\"rates\":[" + jsonRates+ "]";
            jsonOutput += "}";
            
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
		
	 }
	 public String getMMRevenueRateDetails(){

		 try {
		    
			 this.sourceTypeId = getSourceTypeId();
			 sessionMap.put("sourceTypeId",sourceTypeId); 
				
			this.accommodationId = getAccommodationId();
			sessionMap.put("accommodationId",accommodationId);
			
			this.strStartDate=getStrStartDate();
			this.strEndDate=getStrEndDate();
			
			if(this.strStartDate==null || this.strEndDate==null ){
				return null;
			}
			
			DateFormat format = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
		    java.util.Date dateStart = format.parse(getStrStartDate());
		    java.util.Date dateEnd = format.parse(getStrEndDate());
		   
		    
		    Calendar calCheckStart=Calendar.getInstance();
		    calCheckStart.setTime(dateStart);
		    java.util.Date checkInDate = calCheckStart.getTime();
		    this.arrivalDate=new Timestamp(checkInDate.getTime());
		    
		    Calendar calCheckEnd=Calendar.getInstance();
		    calCheckEnd.setTime(dateEnd);
		    java.util.Date checkOutDate = calCheckEnd.getTime();
		    this.departureDate=new Timestamp(checkOutDate.getTime());
		    
		    
			this.propertyId = (Integer) sessionMap.get("propertyId");
		    
			
		    
			HttpServletResponse response = ServletActionContext.getResponse();
			PropertyAccommodationRoomManager  roomController  = new PropertyAccommodationRoomManager();
			PmsSourceManager sourceController = new PmsSourceManager();
			PropertyRateManager rateController = new PropertyRateManager();
			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
			PropertyRateDetailManager rateDetailController = new PropertyRateDetailManager();
			PropertyRatePlanDetailManager planDetailController=new PropertyRatePlanDetailManager();
			PropertyAccommodationInventoryManager inventoryController= new PropertyAccommodationInventoryManager();
			PmsBookingManager bookingController=new PmsBookingManager();
			PmsPropertyManager propertyController = new PmsPropertyManager();
			boolean dateChk=false;
			int roomCount=0,availableCount=0,totalCount=0,totalDate=0,dateCount=0;
	    	Double dblTotalBaseAmount=0.0,dblBaseAmount=0.0,dblPercentCount=0.0,dblSmartPricePercent=0.0;
	    	Double dblPercentFrom,dblPercentTo,dblNetRate;
	    	Boolean isPositive=false,isNegative=false;
	    	
			int fromDay=0,fromMonth=0,fromYear=0;
			int toDay=0,toMonth=0,toYear=0;
			SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
			Calendar calStart=Calendar.getInstance();
			calStart.setTime(getArrivalDate());
			java.util.Date startDate = calStart.getTime();             
	    	String strFromDate = format1.format(startDate);  
	    	
	    	
	    	Calendar calEnd=Calendar.getInstance();
	    	calEnd.setTime(getDepartureDate());
	    	java.util.Date endDate = calEnd.getTime();  
	    	String strToDate = format1.format(endDate); 
	    	
	    	
	    	java.util.Date fromDate = format1.parse(strFromDate);
			Timestamp tsFromDate=new Timestamp(fromDate.getTime());

			java.util.Date toDate = format1.parse(strToDate);
			Timestamp tsToDate=new Timestamp(toDate.getTime());
			
			DateTime dtFromDate = DateTime.parse(strFromDate);
	        DateTime dtToDate = DateTime.parse(strToDate);
	        
			String jsonOutput = "";
			String jsonRates ="";
			String jsonRatePlan ="";
			
/*			promotions start
*/
			int promoCount=0;
			List<PropertyDiscount> listDiscountDetails=null;
			PropertyDiscountManager discountController=new PropertyDiscountManager();
			List<PmsPromotionDetails> listPromotionDetailCheck=null;
			
			java.util.Date currentdate=new java.util.Date();
			String strCurrentDate = new SimpleDateFormat("yyyy-MM-dd").format(currentdate);
		   	java.util.Date curdate = format1.parse(strCurrentDate);
			 List<DateTime> between = DateUtil.getDateRange(dtFromDate, dtToDate);
			
			 ArrayList arlRateType=new ArrayList();
			 List<PropertyRatePlanDetail> listPlanDetails=planDetailController.listAccommodation(getPropertyId(), getAccommodationId());
			 if(listPlanDetails.size()>0 && !listPlanDetails.isEmpty()){
	        	for(PropertyRatePlanDetail ratePlanDetails:listPlanDetails){
	        		arlRateType.add(ratePlanDetails.getRatePlan().getRatePlanType());
	        		arlRateType.add(ratePlanDetails.getRatePlan().getRatePlanSymbolType());
	        		arlRateType.add(ratePlanDetails.getTariffAmount());
	        	}
			 }
			
				 /*promotions end here*/
			response.setContentType("application/json");
			   
			HashMap<String,String> dateMap=new HashMap<String,String>();
			HashMap<String,String> dayMap=new HashMap<String,String>();
			
			ArrayList<String> arrListDateId=new ArrayList<String>();
			SimpleDateFormat formatEE = new SimpleDateFormat("EE", Locale.ENGLISH);
			SimpleDateFormat formatEEEE = new SimpleDateFormat("EEEE", Locale.ENGLISH);
			ArrayList<String> arrListRateDetail=new ArrayList<String>();
			List<PropertyAccommodation> accommodationList= roomController.list(accommodationId,getPropertyId());
			DateFormat f = new SimpleDateFormat("EEEE");
			List<PmsSmartPrice> listSmartPriceDetail=null; 
	    	List<AccommodationRoom> listAccommodationRoom=null;
	    	PropertyAccommodation accommodations=accommodationController.find(accommodationId);
	    	if (!jsonOutput.equalsIgnoreCase(""))
				jsonOutput += ",{";
			
			else
				jsonOutput = "{";
	    	
	    	jsonOutput += "\"accommodationId\":\""+accommodations.getAccommodationId()+"\"";
		 	jsonOutput += ",\"accommodationType\":\""+accommodations.getAccommodationType()+"\"";
		 	jsonOutput += ",\"netRateRevenue\":\""+accommodations.getNetRateRevenue()+"\"";
		 	jsonOutput += ",\"sourceTypeId\":\""+this.sourceTypeId+"\"";
		 	dblNetRate=accommodations.getNetRateRevenue();
		 	
	        
			String strStartDate=null,strEndDate=null,strPropertyRateId=null,
					strMinAmount=null,strMaxAmount=null,strDaysofWeek=null,strDays=null,strDateValues=null;
			
			List<DateTime> listAllDates=DateUtil.getDateRange(dtFromDate, dtToDate);
			for(DateTime d:listAllDates){
				
				List<PropertyRate> dateList =  rateController.listAllDates(getPropertyId(),getAccommodationId(),getSourceTypeId(),d.toDate());
				//List<PropertyRate> dateList =  rateController.list(getPropertyId(),getAccommodationId(),getSourceId(),tsFromDate,tsToDate);
			    if(!dateList.isEmpty()){
			    	for (PropertyRate rates : dateList) {
			    		strPropertyRateId=String.valueOf(rates.getPropertyRateId());
			    		strStartDate=format1.format(rates.getStartDate());
			    		strEndDate=format1.format(rates.getEndDate());
			    		DateTime FromDate = DateTime.parse(strStartDate);
				        DateTime ToDate = DateTime.parse(strEndDate);
				        
				        int propertyRatesId=rates.getPropertyRateId(); 
				        
				        java.util.Date DateValues = d.toDate();
			        	strDateValues=format1.format(DateValues);
			        	
				        arrListDateId.add(strDateValues);
			        	arrListDateId.add(strPropertyRateId);
			        	
			    		List<PropertyRateDetail> rateDetailList =  rateDetailController.listMinMaxRateDetail(propertyRatesId,f.format(d.toDate()).toLowerCase());
			    		
			    		if(!rateDetailList.isEmpty() && rateDetailList.size()>0){
			    			for(PropertyRateDetail rateDetails: rateDetailList){
			    				strMinAmount=String.valueOf(rateDetails.getMinimumBaseAmount());
			    				strMaxAmount=String.valueOf(rateDetails.getMaximumBaseAmount());
			    				strDaysofWeek=rateDetails.getDaysOfWeek().toLowerCase();
			    				arrListDateId.add(strDaysofWeek);
				        		arrListDateId.add(strMinAmount);
			    				List<String> daysBetween = DateUtil.getDayRange(FromDate, ToDate);
			    				Iterator<String> iterDayValues=daysBetween.iterator();
						        while(iterDayValues.hasNext()){
						        	strDays=iterDayValues.next().toLowerCase();
						        	if(strDaysofWeek.contains(strDays)){
						        		arrListDateId.add(strDaysofWeek);
						        		arrListDateId.add(strMinAmount);
						        		arrListDateId.add(strMaxAmount);
						        	}
						        }
			    			}
			    			break;
			    		}else{
			    			if(accommodationList.size()>0 && !accommodationList.isEmpty()){
					    		for (PropertyAccommodation accommodationRate : accommodationList) {
					    			 strMinAmount=String.valueOf(accommodationRate.getMinimumBaseAmount());
					    			 strMaxAmount=String.valueOf(accommodationRate.getMaximumBaseAmount());
					    			 java.util.Date dteCheckDate = d.toDate();
							         strDateValues=format1.format(dteCheckDate);
								     arrListDateId.add(strDateValues);
							         arrListDateId.add(String.valueOf(0));
							         arrListDateId.add(f.format(d.toDate()).toLowerCase());
						        	 arrListDateId.add(strMinAmount);
						        	 arrListDateId.add(strMaxAmount);
					    		}
					    		//break;
					    	}
			    		}
			    		dateChk=true;
			    	}
			    }else{
			    	if(accommodationList.size()>0 && !accommodationList.isEmpty()){
			    		for (PropertyAccommodation accommodationRate : accommodationList) {
			    			 strMinAmount=String.valueOf(accommodationRate.getMinimumBaseAmount());
			    			 strMaxAmount=String.valueOf(accommodationRate.getMaximumBaseAmount());
			    			 java.util.Date DateValues = d.toDate();
					         strDateValues=format1.format(DateValues);
						     arrListDateId.add(strDateValues);
					         arrListDateId.add(String.valueOf(0));
					         arrListDateId.add(f.format(d.toDate()).toLowerCase());
				        	 arrListDateId.add(strMinAmount);
				        	 arrListDateId.add(strMaxAmount);
			    		}
			    		//break;
			    	}
			    }
			}
			
		    
		   int intCount=0;
		   String strArrays[] = new String[arrListDateId.size()];
		    Iterator<String> iterListValues=arrListDateId.iterator();
		    while(iterListValues.hasNext()){
	    		String strTemp=iterListValues.next();
	    		strArrays[intCount]=strTemp;
	    		intCount++;		
		    }
		    
		    String dateval="",doubleval="",rateId="";
		    for(String s : strArrays) {
		          String[] s2 = s.split(" ");
		          for(String results : s2) {
		              if(results.contains("-")){//Date check
		            	  dateval=results;
		            	  arrListRateDetail.add(dateval);
		            	  
		              }else{// Number
		            	  if(isNumeric(results)){
		            		  rateId=results;
		            	  }
		            	  else{
		            		  int first = results.indexOf(".");
		            		  if ( (first >= 0) && (first - results.lastIndexOf(".")) == 0) {
		            		      // only one decimal point
		            			  doubleval=results;
		            			  arrListRateDetail.add(doubleval);
		            		  }
		            		  else {
		            			
		            			  // no decimal point or more than one decimal point
		            		  }
		            	  }
		              }
		  		      if(!doubleval.isEmpty() && !dateval.isEmpty()){
		  		    	dateMap.put(dateval, doubleval);
		              }
		  		      if(!rateId.isEmpty() && !dateval.isEmpty()){
		  		    	  dayMap.put(dateval, rateId);
		  		      }
		          }
		      }
		    intCount=0;
            String[] listValues=new String[arrListRateDetail.size()];
            Iterator<String> rateDetailsIter=arrListRateDetail.iterator();
		    while (rateDetailsIter.hasNext()) {
                String strTemp=rateDetailsIter.next();
                listValues[intCount]=strTemp;
	    		intCount++;		
		    }

		    
		    
		    String strDay="", strRateId="";
		    int intRateId=0;
		    
            for(DateTime d:listAllDates){
            	java.util.Date DateValues = d.toDate();
            	strDateValues=format1.format(DateValues);
            	String strType=null;
            	String strSymbol=null;
            	double dblMinimumAmount=0,dblMaximumAmount=0;
            	
            	if (!jsonRates.equalsIgnoreCase(""))
					jsonRates += ",{";
				
				else
					jsonRates = "{";
            	
        		if(arrListRateDetail.contains(strDateValues)){
        			java.util.Date dteReqDate= format1.parse(strDateValues);
	    			Timestamp tsDate=new Timestamp(dteReqDate.getTime());
	    			
        			strDay=formatEEEE.format(dteReqDate).toLowerCase();
        			strRateId=dayMap.get(strDateValues);
        			intRateId=Integer.parseInt(strRateId);

        			List<PropertyRateDetail> rateDetailList1 =   rateDetailController.list(intRateId,strDay);
        			
        			if(rateDetailList1.size()>0 && !rateDetailList1.isEmpty()){
        				for (PropertyRateDetail rate : rateDetailList1)
		    			 {
        					jsonRates += "\"minimumBaseAmount\":\""+(rate.getMinimumBaseAmount()==null?0:rate.getMinimumBaseAmount())+" \"";
        					jsonRates += ",\"maximumBaseAmount\":\""+(rate.getMaximumBaseAmount()==null?0:rate.getMaximumBaseAmount())+" \"";
        					jsonRates += ",\"minimumAmount\":\""+(rate.getMinimumBaseAmount()==null?0:rate.getMinimumBaseAmount())+" \"";
        					jsonRates += ",\"maximumAmount\":\""+(rate.getMaximumBaseAmount()==null?0:rate.getMaximumBaseAmount())+" \"";
        					jsonRates += ",\"adult\":\""+(rate.getExtraAdult())+"\"";
        					jsonRates += ",\"child\":\""+(rate.getExtraChild())+"\"";
//        					jsonRates += ",\"date\":\""+strDateValues+"\"";
        					java.util.Date baseDate=new SimpleDateFormat("yyyy-MM-dd").parse(strDateValues);
    					 	jsonRates += ",\"date\":\""+new SimpleDateFormat("MMMM d, yyyy").format(baseDate)+"\"";
         				    dblMinimumAmount=rate.getMinimumBaseAmount();
         				    dblMaximumAmount=rate.getMinimumBaseAmount();

		    			 }
        			}
        			else
        			{
        				for (PropertyAccommodation accommodationRate : accommodationList) {
        					
        					jsonRates += "\"minimumBaseAmount\":\""+(accommodationRate.getMinimumBaseAmount()==null?0:accommodationRate.getMinimumBaseAmount())+" \"";
        					jsonRates += ",\"maximumBaseAmount\":\""+(accommodationRate.getMaximumBaseAmount()==null?0:accommodationRate.getMaximumBaseAmount())+" \"";
        					jsonRates += ",\"minimumAmount\":\""+(accommodationRate.getMinimumBaseAmount()==null?0:accommodationRate.getMinimumBaseAmount())+" \"";
        					jsonRates += ",\"maximumAmount\":\""+(accommodationRate.getMaximumBaseAmount()==null?0:accommodationRate.getMaximumBaseAmount())+" \"";
        					jsonRates += ",\"adult\":\""+(accommodationRate.getExtraAdult())+"\"";
        					jsonRates += ",\"child\":\""+(accommodationRate.getExtraChild())+"\"";
//        					jsonRates += ",\"date\":\""+strDateValues+"\"";
        					java.util.Date baseDate=new SimpleDateFormat("yyyy-MM-dd").parse(strDateValues);
        					jsonRates += ",\"date\":\""+new SimpleDateFormat("MMMM d, yyyy").format(baseDate)+"\"";
        					dblMinimumAmount=accommodationRate.getMinimumBaseAmount();
         				    dblMaximumAmount=accommodationRate.getMinimumBaseAmount();
    					 	
        				}
        			}
    			
        		}
        		else{
    				
    				for (PropertyAccommodation accommodationRate : accommodationList) {
    					
    					jsonRates += "\"minimumBaseAmount\":\""+(accommodationRate.getMinimumBaseAmount()==null?0:accommodationRate.getMinimumBaseAmount())+" \"";
    					jsonRates += ",\"maximumBaseAmount\":\""+(accommodationRate.getMaximumBaseAmount()==null?0:accommodationRate.getMaximumBaseAmount())+" \"";
    					jsonRates += ",\"minimumAmount\":\""+(accommodationRate.getMinimumBaseAmount()==null?0:accommodationRate.getMinimumBaseAmount())+" \"";
    					jsonRates += ",\"maximumAmount\":\""+(accommodationRate.getMaximumBaseAmount()==null?0:accommodationRate.getMaximumBaseAmount())+" \"";
    					jsonRates += ",\"adult\":\""+(accommodationRate.getExtraAdult())+"\"";
    					jsonRates += ",\"child\":\""+(accommodationRate.getExtraChild())+"\"";
//    					jsonRates += ",\"date\":\""+strDateValues+"\"";
    					java.util.Date baseDate=new SimpleDateFormat("yyyy-MM-dd").parse(strDateValues);
    					jsonRates += ",\"date\":\""+new SimpleDateFormat("MMMM d, yyyy").format(baseDate)+"\"";
    					dblMinimumAmount=accommodationRate.getMinimumBaseAmount();
     				    dblMaximumAmount=accommodationRate.getMinimumBaseAmount();
    					
    				}
        		}
        		
        		
        		long rmc = 0;
   				this.roomCnt = rmc;
        		int sold=0;
        		java.util.Date availDate = d.toDate();
					PropertyAccommodationInventory inventoryCount = inventoryController.findInventoryCount(availDate,accommodations.getAccommodationId());
					if(inventoryCount != null){
						PmsAvailableRooms roomCount1 = bookingController.findBookingCount(availDate, accommodations.getAccommodationId(),inventoryCount.getInventoryId());
						if(roomCount1.getRoomCount() == null) {
							String num1 = inventoryCount.getInventoryCount();
							PmsAvailableRooms bookingRoomCount=bookingController.findCount(availDate, accommodations.getAccommodationId());
							PropertyAccommodation accommodation=accommodationController.find(accommodations.getAccommodationId());
							Integer totavailable=0;
							 if(accommodations!=null){
								totavailable=accommodations.getNoOfUnits();
							 }
						if(bookingRoomCount.getRoomCount()!=null){
							long lngRooms=bookingRoomCount.getRoomCount();
							long num2 = Long.parseLong(num1);
							this.roomCnt = num2;
							sold=(int) (lngRooms);
							int availRooms=0;
							availRooms=totavailable-sold;
							if(Integer.parseInt(num1)>availRooms){
								this.roomCnt = availRooms;	
							}else{
								this.roomCnt = num2;
							}
							
						}else{
							 long num2 = Long.parseLong(num1);
							this.roomCnt = num2;
							sold=(int) (rmc);
						}
						
							 /*String num1 = inventoryCount.getInventoryCount();
							 long num2 = Long.parseLong(num1);
							this.roomCnt = num2; 
							jsonAvailable += "\"sold\":\"" + rmc + "\"";
							sold=(int) (rmc);*/
						}
						else {
							
							PmsAvailableRooms bookingRoomCount=bookingController.findCount(availDate, accommodations.getAccommodationId());
							int intRooms=0;
							PropertyAccommodation accommodation=accommodationController.find(accommodations.getAccommodationId());
							Integer totavailable=0,availableRooms=0;
							 if(accommodations!=null){
								totavailable=accommodations.getNoOfUnits();
							 }
							if(bookingRoomCount.getRoomCount()!=null){
								long lngRooms=bookingRoomCount.getRoomCount();
								long num = roomCount1.getRoomCount();	
								intRooms=(int)lngRooms;
								String num1 = inventoryCount.getInventoryCount();
 							 long num2 = Long.parseLong(num1);
 							availableRooms=totavailable-intRooms;
 							 if(num2>availableRooms){
 								this.roomCnt = availableRooms;	 
 							 }else{
 								this.roomCnt = num2-num;
 							 }
 							long lngSold=bookingRoomCount.getRoomCount();
   							sold=(int)lngSold;
							}else{
								long num = roomCount1.getRoomCount();	
								String num1 = inventoryCount.getInventoryCount();
  							 long num2 = Long.parseLong(num1);
  							this.roomCnt = (num2-num);
  							long lngSold=roomCount1.getRoomCount();
   							sold=(int)lngSold;
							}
							/* long num = roomCount1.getRoomCount();
							 String num1 = inventoryCount.getInventoryCount();
							 long num2 = Long.parseLong(num1);
							this.roomCnt = (num2-num);
							jsonAvailable += "\"sold\":\"" + roomCount1.getRoomCount() + "\"";
							long lngSold=roomCount1.getRoomCount();
							sold=(int)lngSold;*/
						}
							
					}else{
						  PmsAvailableRooms inventoryCounts = bookingController.findCount(availDate, accommodations.getAccommodationId());							
							if(inventoryCounts.getRoomCount() == null){								
								this.roomCnt = (accommodations.getNoOfUnits()-rmc);
								sold=(int)rmc;
							}
							else{
								this.roomCnt = (accommodations.getNoOfUnits()-inventoryCounts.getRoomCount());	
								long lngSold=inventoryCounts.getRoomCount();
	   							sold=(int)lngSold;
							}
					}
							
					double totalRooms=0,intSold=0;
					totalRooms=(double)accommodations.getNoOfUnits();
					intSold=(double)sold;
					double occupancy=0.0;
					occupancy=intSold/totalRooms;
					double sellrate=0.0,otarate=0.0;
					Integer occupancyRating=(int) Math.round(occupancy * 100);
					if(occupancyRating==0){
						sellrate = dblMinimumAmount;
					}else if(occupancyRating>0 && occupancyRating<=30){
			        	sellrate = dblMinimumAmount;
			        }else if(occupancyRating>=31 && occupancyRating<=60){
			        	sellrate = dblMinimumAmount+(dblMinimumAmount*10/100);
			        }else if(occupancyRating>=61 && occupancyRating<=80){
			        	sellrate = dblMinimumAmount+(dblMinimumAmount*15/100);
			        }else if(occupancyRating>=81 && occupancyRating<=100){
			        	sellrate = dblMaximumAmount;
			        }
			        jsonRates +=",\"occupancy\":\""+occupancyRating+"\"";
			        jsonRates += ",\"sellRate\":\""+(sellrate)+"\"";
			        jsonRates += ",\"cpAmount\":\""+(sellrate)+" \"";
			        double dblRatePlanAmount=0;
			        Iterator iterRatePlan=arlRateType.iterator();
	            	while(iterRatePlan.hasNext()){
	            		strType=(String)iterRatePlan.next();
	            		strSymbol=(String)iterRatePlan.next();
	            		dblRatePlanAmount=(Double)iterRatePlan.next();
	            		
	            		if(strType.equalsIgnoreCase("EP")){
	            			
	                		jsonRates += ",\"ratePlanType\":\""+strType+"\"";
	                		if(strSymbol.equalsIgnoreCase("plus")){
	                			jsonRates += ",\"epAmount\":\""+(sellrate+dblRatePlanAmount)+"\"";	
	                		}else{
	                			jsonRates += ",\"epAmount\":\""+(sellrate-dblRatePlanAmount)+"\"";
	                		}
	                		
	                		jsonRates += ",\"ratePlanAmount\":\""+(dblRatePlanAmount)+"\"";
	                		jsonRates += ",\"ratePlanSymbol\":\""+(strSymbol.toLowerCase())+"\"";
	                		
	            		}
	            		
	            	}
			        otarate=sellrate+(sellrate*15/100);
			        jsonRates += ",\"otaRate\":\""+(otarate)+"\"";
			        jsonRates += "}";
            }
            
            
            jsonOutput += ",\"rates\":[" + jsonRates+ "]";
            jsonOutput += "}";
            
            
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
		
	 
	 }
	 public String getRevenueRateDetails() throws IOException, ParseException{
		 try {
		    
			 this.sourceTypeId = getSourceTypeId();
			 sessionMap.put("sourceTypeId",sourceTypeId); 
				
			this.accommodationId = getAccommodationId();
			sessionMap.put("accommodationId",accommodationId);
			
			this.strStartDate=getStrStartDate();
			this.strEndDate=getStrEndDate();
			
			if(this.strStartDate==null || this.strEndDate==null ){
				return null;
			}
			
			DateFormat format = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
		    java.util.Date dateStart = format.parse(getStrStartDate());
		    java.util.Date dateEnd = format.parse(getStrEndDate());
		   
		    
		    Calendar calCheckStart=Calendar.getInstance();
		    calCheckStart.setTime(dateStart);
		    java.util.Date checkInDate = calCheckStart.getTime();
		    this.arrivalDate=new Timestamp(checkInDate.getTime());
		    
		    Calendar calCheckEnd=Calendar.getInstance();
		    calCheckEnd.setTime(dateEnd);
		    java.util.Date checkOutDate = calCheckEnd.getTime();
		    this.departureDate=new Timestamp(checkOutDate.getTime());
		    
		    
			this.propertyId = (Integer) sessionMap.get("propertyId");
		    
			
		    
			HttpServletResponse response = ServletActionContext.getResponse();
			PropertyAccommodationRoomManager  roomController  = new PropertyAccommodationRoomManager();
			PmsSourceManager sourceController = new PmsSourceManager();
			PropertyRateManager rateController = new PropertyRateManager();
			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
			PropertyRateDetailManager rateDetailController = new PropertyRateDetailManager();
			PropertyRatePlanDetailManager planDetailController=new PropertyRatePlanDetailManager();
			
			PmsPropertyManager propertyController = new PmsPropertyManager();
			boolean dateChk=false;
			int roomCount=0,availableCount=0,totalCount=0,totalDate=0,dateCount=0;
	    	Double dblTotalBaseAmount=0.0,dblBaseAmount=0.0,dblPercentCount=0.0,dblSmartPricePercent=0.0;
	    	Double dblPercentFrom,dblPercentTo,dblNetRate;
	    	Boolean isPositive=false,isNegative=false;
	    	
			int fromDay=0,fromMonth=0,fromYear=0;
			int toDay=0,toMonth=0,toYear=0;
			SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
			Calendar calStart=Calendar.getInstance();
			calStart.setTime(getArrivalDate());
			java.util.Date startDate = calStart.getTime();             
	    	String strFromDate = format1.format(startDate);  
	    	
	    	
	    	Calendar calEnd=Calendar.getInstance();
	    	calEnd.setTime(getDepartureDate());
	    	java.util.Date endDate = calEnd.getTime();  
	    	String strToDate = format1.format(endDate); 
	    	
	    	
	    	java.util.Date fromDate = format1.parse(strFromDate);
			Timestamp tsFromDate=new Timestamp(fromDate.getTime());

			java.util.Date toDate = format1.parse(strToDate);
			Timestamp tsToDate=new Timestamp(toDate.getTime());
			
			DateTime dtFromDate = DateTime.parse(strFromDate);
	        DateTime dtToDate = DateTime.parse(strToDate);
	        
			String jsonOutput = "";
			String jsonRates ="";
			String jsonRatePlan ="";
			
/*			promotions start
*/
			int promoCount=0;
			List<PropertyDiscount> listDiscountDetails=null;
			PropertyDiscountManager discountController=new PropertyDiscountManager();
			List<PmsPromotionDetails> listPromotionDetailCheck=null;
			double promotionFirstPercent=0.0,promotionFirstInr=0.0,
			    		promotionSecondPercent=0.0,promotionSecondInr=0.0,totalFlatLastAmount=0.0,flatLastAmount=0.0;
			ArrayList<Integer> arlEnablePromo=new ArrayList<Integer>();
			String promotionHoursRange=null,promotionFirstDiscountType=null,promotionType=null,promotionSecondDiscountType=null;
			PromotionManager promotionController=new PromotionManager();
			PromotionDetailManager promotionDetailController=new PromotionDetailManager();
			Timestamp tsPromoBookedDate=null,tsPromoStartDate=null,tsPromoEndDate=null;
			Timestamp tsFirstStartRange=null,tsSecondStartRange=null,tsFirstEndRange=null,tsSecondEndRange=null;
			Timestamp tsLastStartDate=null,tsLastEndDate=null;
			boolean blnFirstActivePromo=false,blnFirstNotActivePromo=false,baseFirstPromoFlag=false,blnEarlyBirdPromo=false,blnLastMinuteFirstActive=false;
			boolean blnSecondActivePromo=false,blnSecondNotActivePromo=false,baseSecondPromoFlag=false,blnLastMinutePromo=false;
			java.util.Date currentdate=new java.util.Date();
			String strCurrentDate = new SimpleDateFormat("yyyy-MM-dd").format(currentdate);
		   	java.util.Date curdate = format1.parse(strCurrentDate);
			   	 
			 List<PmsPromotions> listDatePromotions= promotionController.listBookedDatePromotions(this.propertyId,curdate);
			 if(listDatePromotions.size()>0 && !listDatePromotions.isEmpty()){
				for(PmsPromotions promotions:listDatePromotions){
					tsPromoBookedDate=promotions.getPromotionBookedDate();
					tsPromoStartDate=promotions.getStartDate();
					tsPromoEndDate=promotions.getEndDate();
					blnEarlyBirdPromo=checkEarlyBirdPromos(tsPromoBookedDate,tsPromoStartDate,tsPromoEndDate,getArrivalDate(),getDepartureDate());
				}
			 }else{
				 blnEarlyBirdPromo=false;
			 }
				 
			 List<PmsPromotions> listLastPromotions= promotionController.listLastBookedPromotions(this.propertyId,curdate);
			 if(listLastPromotions.size()>0 && !listLastPromotions.isEmpty()){
				for(PmsPromotions promotions:listLastPromotions){
					tsFirstStartRange=promotions.getFirstRangeStartDate();
					tsSecondStartRange=promotions.getSecondRangeStartDate();
					tsFirstEndRange=promotions.getFirstRangeEndDate();
					tsSecondEndRange=promotions.getSecondRangeEndDate();
					tsLastStartDate=promotions.getStartDate();
					tsLastEndDate=promotions.getEndDate();
					promotionHoursRange=promotions.getPromotionHoursRange();
					blnLastMinuteFirstActive=checkLastMinutePromos(promotionHoursRange,getArrivalDate(),getDepartureDate(),tsSecondStartRange,tsFirstEndRange,tsFirstEndRange,tsSecondStartRange);
//					blnLastMinuteIsActive=getCheckHoursRange(promotionHoursRange,getArrivalDate(),getDepartureDate());
					if(blnLastMinuteFirstActive){
						blnLastMinutePromo=true;
						arlEnablePromo.add(promotions.getPromotionId());
					}
				}
			 }else{
				 blnLastMinutePromo=false;
				 
			 }
			 
			 ArrayList<Integer> arlFirstActivePromo=new ArrayList<Integer>(); 
			 ArrayList<Integer> arlSecondActivePromo=new ArrayList<Integer>(); 
			 List<DateTime> between = DateUtil.getDateRange(dtFromDate, dtToDate);
			 for (DateTime d : between)
		     {
				 if(blnEarlyBirdPromo){
					 List<PmsPromotions> listEarlyPromotions= promotionController.listBookedDatePromotions(this.propertyId,curdate);
					 if(listEarlyPromotions.size()>0 && !listEarlyPromotions.isEmpty()){
						 for(PmsPromotions promos:listEarlyPromotions){
							 if(promos.getPromotionType().equalsIgnoreCase("E")){
								 if(arlFirstActivePromo.isEmpty()){
									arlFirstActivePromo.add(promos.getPromotionId());
								 }else if(!arlFirstActivePromo.contains(promos.getPromotionId())){
									arlFirstActivePromo.add(promos.getPromotionId());
								 }
							 }else{
								blnFirstNotActivePromo=true;
							 }
						 }
					 }
				 }else if(!blnEarlyBirdPromo){
					 List<PmsPromotions> listAllPromotions= promotionController.listFlatDatePromotions(this.propertyId,d.toDate());
					 if(!listAllPromotions.isEmpty() && listAllPromotions.size()>0){
						 for(PmsPromotions promos:listAllPromotions){
							 if(promos.getPromotionType().equalsIgnoreCase("F")){
								 if(arlFirstActivePromo.isEmpty()){
									arlFirstActivePromo.add(promos.getPromotionId());
								 }else if(!arlFirstActivePromo.contains(promos.getPromotionId())){
									arlFirstActivePromo.add(promos.getPromotionId());
								 }
							 }else{
								blnFirstNotActivePromo=true;
							 }
						 }
					 } 
				 }else{
					 blnFirstNotActivePromo=true;
				 }
				 
				if(blnLastMinutePromo){
					List<PmsPromotions> listLastPromos= promotionController.listLastBookedPromotions(this.propertyId,curdate);
					 if(listLastPromos.size()>0 && !listLastPromos.isEmpty()){
						for(PmsPromotions promos:listLastPromos){
							if(promos.getPromotionType().equalsIgnoreCase("L")){
								if(arlEnablePromo.contains(promos.getPromotionId())){
									if(arlSecondActivePromo.isEmpty()){
	  										arlSecondActivePromo.add(promos.getPromotionId());
	  									 }else if(!arlSecondActivePromo.contains(promos.getPromotionId())){
	  										arlSecondActivePromo.add(promos.getPromotionId());
	  									 }
									}
							 	}
							}
					 }else{
						blnSecondNotActivePromo=true;
					 }
			}else{
				blnSecondNotActivePromo=true;
			}
		     
		     }
			 int intPromoSize=arlFirstActivePromo.size();
			 if((arlFirstActivePromo.isEmpty() || blnFirstNotActivePromo)){
				 blnFirstActivePromo=false;
			 }else{
				 blnFirstActivePromo=true;
			 }
			 
			 if(arlSecondActivePromo.isEmpty() || blnSecondNotActivePromo){
				blnSecondActivePromo=false;
			 }else{
				blnSecondActivePromo=true;
			 }
			
				 /*promotions end here*/
			response.setContentType("application/json");
			   
			HashMap<String,String> dateMap=new HashMap<String,String>();
			HashMap<String,String> dayMap=new HashMap<String,String>();
			ArrayList<Integer> listSmartPriceChecked=new ArrayList<Integer>();
			
			ArrayList<String> arrListDateId=new ArrayList<String>();
			SimpleDateFormat formatEE = new SimpleDateFormat("EE", Locale.ENGLISH);
			SimpleDateFormat formatEEEE = new SimpleDateFormat("EEEE", Locale.ENGLISH);
			ArrayList<String> arrListRateDetail=new ArrayList<String>();
			List<PropertyAccommodation> accommodationList= roomController.list(accommodationId,getPropertyId());
			DateFormat f = new SimpleDateFormat("EEEE");
			List<PmsSmartPrice> listSmartPriceDetail=null; 
	    	List<AccommodationRoom> listAccommodationRoom=null;
	    	PropertyAccommodation accommodations=accommodationController.find(accommodationId);
	    	if (!jsonOutput.equalsIgnoreCase(""))
				jsonOutput += ",{";
			
			else
				jsonOutput = "{";
	    	
	    	jsonOutput += "\"accommodationId\":\""+accommodations.getAccommodationId()+"\"";
		 	jsonOutput += ",\"accommodationType\":\""+accommodations.getAccommodationType()+"\"";
		 	jsonOutput += ",\"netRateRevenue\":\""+accommodations.getNetRateRevenue()+"\"";
		 	jsonOutput += ",\"sourceTypeId\":\""+this.sourceTypeId+"\"";
		 	dblNetRate=accommodations.getNetRateRevenue();
		 	ArrayList arlRateType=new ArrayList();
	        List<PropertyRatePlanDetail> listPlanDetails=planDetailController.listAccommodation(getPropertyId(), getAccommodationId());
	        if(listPlanDetails.size()>0 && !listPlanDetails.isEmpty()){
	        	for(PropertyRatePlanDetail ratePlanDetails:listPlanDetails){
	        		arlRateType.add(ratePlanDetails.getRatePlan().getRatePlanType());
	        		arlRateType.add(ratePlanDetails.getRatePlan().getRatePlanSymbolType());
	        		arlRateType.add(ratePlanDetails.getTariffAmount());
	        	}
	        }
	        
			String strStartDate=null,strEndDate=null,strPropertyRateId=null,
					strAmount=null,strDaysofWeek=null,strDays=null,strDateValues=null;
			
			List<DateTime> listAllDates=DateUtil.getDateRange(dtFromDate, dtToDate);
			for(DateTime d:listAllDates){
				
				if(blnFirstActivePromo){
			    		if(blnEarlyBirdPromo){
			    			List<PmsPromotions> listEarlyPromotions= promotionController.listBookedDatePromotions(this.propertyId,curdate);
							 if(listEarlyPromotions.size()>0 && !listEarlyPromotions.isEmpty()){
				    			for(PmsPromotions promotions:listEarlyPromotions){
				    				if(promotions.getPromotionType().equalsIgnoreCase("E")){
					    				promotionFirstDiscountType=promotions.getPromotionDiscountType();
					    				listPromotionDetailCheck=promotionDetailController.listPromotionDetails(promotions.getPromotionId(),f.format(d.toDate()).toLowerCase());
					    				 for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
					    					if(promoCount==0){
					    						promotionFirstPercent=promoDetails.getPromotionPercentage();
	 					    					promotionFirstInr=promoDetails.getPromotionInr();
	 					    					promotionType=promotions.getPromotionType();
	 					    					
					    					}
					    					promoCount++;
					    				 }
				    				}
				    			}
				    		}
			    		}else if(!blnEarlyBirdPromo){
			    			List<PmsPromotions> listPromotions= promotionController.listFlatDatePromotions(this.propertyId,d.toDate());
				    		if(listPromotions.size()>0 && !listPromotions.isEmpty()){
				    			for(PmsPromotions promotions:listPromotions){
				    				if(promotions.getPromotionType().equalsIgnoreCase("F")){
					    				promotionFirstDiscountType=promotions.getPromotionDiscountType();
					    				listPromotionDetailCheck=promotionDetailController.listPromotionDetails(promotions.getPromotionId(),f.format(d.toDate()).toLowerCase());
					    				 for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
					    					if(promoCount==0){
					    						promotionFirstPercent=promoDetails.getPromotionPercentage();
	 					    					promotionFirstInr=promoDetails.getPromotionInr();
	 					    					promotionType=promotions.getPromotionType();
	 					    					
					    					}
					    					promoCount++;
					    				 }
				    				}
				    			}
				    		}
			    		}
			    	 }
			    	 if(blnSecondActivePromo){
			    		List<PmsPromotions> listPmsPromotions= promotionController.listLastDatePromotions(this.propertyId,accommodationId,curdate);
		    			 if(listPmsPromotions.size()>0 &&!listPmsPromotions.isEmpty()){
		    				 for(PmsPromotions pmsPromotions:listPmsPromotions){
		    					 if(pmsPromotions.getPromotionType().equalsIgnoreCase("L")){
					    			 Timestamp tsStartDate=null,tsEndDate=null;
					    			 tsStartDate=pmsPromotions.getStartDate();
					    			 tsEndDate=pmsPromotions.getEndDate();
					    			 
					    			 promotionHoursRange=pmsPromotions.getPromotionHoursRange();
					    			 if(arlEnablePromo.contains(pmsPromotions.getPromotionId())){
					    				 long minimumCount = getAvailableCount(this.propertyId,getArrivalDate(),getDepartureDate(),roomCount,accommodationId);
		    			    			 if((int)minimumCount>0){
	    			    					 promotionType=pmsPromotions.getPromotionType();
	    			    					 promotionSecondDiscountType=pmsPromotions.getPromotionDiscountType();
					    					 listPromotionDetailCheck=promotionDetailController.listPromotionDetails(pmsPromotions.getPromotionId(),f.format(curdate).toLowerCase());
					    					 if(listPromotionDetailCheck.size()>0 && !listPromotionDetailCheck.isEmpty()){
					    						for(PmsPromotionDetails promoDetails:listPromotionDetailCheck){
    	 					    					promotionSecondPercent=promoDetails.getPromotionPercentage();
	 	 					    					promotionSecondInr=promoDetails.getPromotionInr();
    	 					    				 }
					    					 }
		    			    			 }
					    			 }
		    					 }
		    					 
		    				 }
		    			 }
			    	 }
			    	 
				List<PropertyRate> dateList =  rateController.listAllDates(getPropertyId(),getAccommodationId(),getSourceTypeId(),d.toDate());
				//List<PropertyRate> dateList =  rateController.list(getPropertyId(),getAccommodationId(),getSourceId(),tsFromDate,tsToDate);
			    if(!dateList.isEmpty()){
			    	for (PropertyRate rates : dateList) {
			    		strPropertyRateId=String.valueOf(rates.getPropertyRateId());
			    		strStartDate=format1.format(rates.getStartDate());
			    		strEndDate=format1.format(rates.getEndDate());
			    		DateTime FromDate = DateTime.parse(strStartDate);
				        DateTime ToDate = DateTime.parse(strEndDate);
				        
				        int propertyRatesId=rates.getPropertyRateId(); 
				        
				        java.util.Date DateValues = d.toDate();
			        	strDateValues=format1.format(DateValues);
			        	
				        arrListDateId.add(strDateValues);
			        	arrListDateId.add(strPropertyRateId);
			        	
			    		List<PropertyRateDetail> rateDetailList =  rateDetailController.listRateDetail(propertyRatesId,f.format(d.toDate()).toLowerCase());
			    		
			    		if(!rateDetailList.isEmpty()){
			    			
			    			
			    			for(PropertyRateDetail rateDetails: rateDetailList){
			    				strAmount=String.valueOf(rateDetails.getBaseAmount());
			    				strDaysofWeek=rateDetails.getDaysOfWeek().toLowerCase();
			    				arrListDateId.add(strDaysofWeek);
				        		arrListDateId.add(strAmount);
			    				List<String> daysBetween = DateUtil.getDayRange(FromDate, ToDate);
			    				Iterator<String> iterDayValues=daysBetween.iterator();
						        while(iterDayValues.hasNext()){
						        	strDays=iterDayValues.next().toLowerCase();
						        	if(strDaysofWeek.contains(strDays)){
						        		arrListDateId.add(strDaysofWeek);
						        		arrListDateId.add(strAmount);
						        	}
						        }
			    			}
			    			break;
			    		}
			    		dateChk=true;
			    	}
			    }else{
			    	if(accommodationList.size()>0 && !accommodationList.isEmpty()){
			    		for (PropertyAccommodation accommodationRate : accommodationList) {
			    			strAmount=String.valueOf(accommodationRate.getBaseAmount());
			    			 java.util.Date DateValues = d.toDate();
					         strDateValues=format1.format(DateValues);
						     arrListDateId.add(strDateValues);
					         arrListDateId.add(String.valueOf(0));
					         arrListDateId.add(f.format(d.toDate()).toLowerCase());
				        	 arrListDateId.add(strAmount);
			    		}
			    		//break;
			    	}
			    }
			}
			
		    
		   int intCount=0;
		   String strArrays[] = new String[arrListDateId.size()];
		    Iterator<String> iterListValues=arrListDateId.iterator();
		    while(iterListValues.hasNext()){
	    		String strTemp=iterListValues.next();
	    		strArrays[intCount]=strTemp;
	    		intCount++;		
		    }
		    
		    String dateval="",doubleval="",rateId="";
		    for(String s : strArrays) {
		          String[] s2 = s.split(" ");
		          for(String results : s2) {
		              if(results.contains("-")){//Date check
		            	  dateval=results;
		            	  arrListRateDetail.add(dateval);
		            	  
		              }else{// Number
		            	  if(isNumeric(results)){
		            		  rateId=results;
		            	  }
		            	  else{
		            		  int first = results.indexOf(".");
		            		  if ( (first >= 0) && (first - results.lastIndexOf(".")) == 0) {
		            		      // only one decimal point
		            			  doubleval=results;
		            			  arrListRateDetail.add(doubleval);
		            		  }
		            		  else {
		            			
		            			  // no decimal point or more than one decimal point
		            		  }
		            	  }
		              }
		  		      if(!doubleval.isEmpty() && !dateval.isEmpty()){
		  		    	dateMap.put(dateval, doubleval);
		              }
		  		      if(!rateId.isEmpty() && !dateval.isEmpty()){
		  		    	  dayMap.put(dateval, rateId);
		  		      }
		          }
		      }
		    intCount=0;
            String[] listValues=new String[arrListRateDetail.size()];
            Iterator<String> rateDetailsIter=arrListRateDetail.iterator();
		    while (rateDetailsIter.hasNext()) {
                String strTemp=rateDetailsIter.next();
                listValues[intCount]=strTemp;
	    		intCount++;		
		    }

		    
		    
		    String strDay="", strRateId="";
		    int intRateId=0;
		    
            for(DateTime d:listAllDates){
            	java.util.Date DateValues = d.toDate();
            	strDateValues=format1.format(DateValues);
            	String strType=null;
            	String strSymbol=null;
            	double dblRatePlanAmount=0,dblRate=0;
            	
            	if (!jsonRates.equalsIgnoreCase(""))
					jsonRates += ",{";
				
				else
					jsonRates = "{";
            	
        		if(arrListRateDetail.contains(strDateValues)){
        			java.util.Date dteReqDate= format1.parse(strDateValues);
	    			Timestamp tsDate=new Timestamp(dteReqDate.getTime());
	    			
        			strDay=formatEEEE.format(dteReqDate).toLowerCase();
        			strRateId=dayMap.get(strDateValues);
        			intRateId=Integer.parseInt(strRateId);
        			if(listSmartPriceChecked.contains(intRateId)){
        				List<PropertyRateDetail> rateDetailList1 =   rateDetailController.list(intRateId,strDay);
        				PropertyRate rates=rateController.listRates(intRateId);
        				int accommId=rates.getPropertyAccommodation().getAccommodationId();
        				int rateCount=0;
            			if(rateDetailList1.size()>0 && !rateDetailList1.isEmpty()){
            				for (PropertyRateDetail rate : rateDetailList1)
    		    			{
            					if(rateCount==0){

                					 dblBaseAmount=rate.getBaseAmount();
                					 listAccommodationRoom=accommodationController.listPropertyTotalRooms(propertyId, accommId);
    	               				 if(listAccommodationRoom.size()>0 && !listAccommodationRoom.isEmpty()){
    	               					 for(AccommodationRoom roomsList:listAccommodationRoom){
    	               						 roomCount=(int) roomsList.getRoomCount();
    	               						 long minimum = getAvailableCount(propertyId,tsDate,tsDate,roomCount,accommId); 
    	               						 availableCount=(int)minimum;
    	               						 dblPercentCount=(double)((roomCount-availableCount)*100)/roomCount;
    	               						 totalCount=(int) Math.round(dblPercentCount);
    	               						 listSmartPriceDetail=rateDetailController.listSmartPriceDetail(totalCount);
    	               						 if(listSmartPriceDetail.size()>0 && !listSmartPriceDetail.isEmpty()){
    	               							 for(PmsSmartPrice smartPrice: listSmartPriceDetail){
    	               								 dblSmartPricePercent=smartPrice.getAmountPercent();
    	               								 dblPercentFrom=smartPrice.getPercentFrom();
    	               								 dblPercentTo=smartPrice.getPercentTo();
    	               								 if(dblPercentFrom>=0 && dblPercentTo<=59){
    	               									 isNegative=true;
    	               								 }else if(dblPercentFrom>=60 && dblPercentTo<=99){
    	               									 isPositive=true;
    	               								 }
    	               								 if(isNegative){
    	               									 dblTotalBaseAmount=dblBaseAmount-(dblBaseAmount*dblSmartPricePercent/100);
    	               								 }
    	               								 if(isPositive){
    	               									dblTotalBaseAmount=dblBaseAmount+(dblBaseAmount*dblSmartPricePercent/100);
    	               								 }
    	               								rateCount++;
    	               							 }
    	               						 }
    	               					 }
    	               				 }
    	               				isNegative=false;
    	               				isPositive=false;
                					
                					
                						jsonRates += "\"tariff\":\""+dblTotalBaseAmount+" \"";
	                					jsonRates += ",\"tariffAmount\":\""+dblTotalBaseAmount+" \"";
	                					jsonRates += ",\"adult\":\""+(rate.getExtraChild())+"\"";
	                					jsonRates += ",\"child\":\""+(rate.getExtraChild())+"\"";
                					 	java.util.Date baseDate=new SimpleDateFormat("yyyy-MM-dd").parse(strDateValues);
                					 	jsonRates += ",\"date\":\""+new SimpleDateFormat("MMMM d, yyyy").format(baseDate)+"\"";
 	            				    
                					 	dblRate=dblTotalBaseAmount;
                
        		    			 
            					}
            					
    		    			 }
            			}
        			}
        			else{
	        			List<PropertyRateDetail> rateDetailList1 =   rateDetailController.list(intRateId,strDay);
	        			
	        			if(rateDetailList1.size()>0 && !rateDetailList1.isEmpty()){
	        				for (PropertyRateDetail rate : rateDetailList1)
			    			 {
            					
	        					jsonRates += "\"tariff\":\""+rate.getBaseAmount()+" \"";
	        					jsonRates += ",\"tariffAmount\":\""+rate.getBaseAmount()+" \"";
	        					jsonRates += ",\"adult\":\""+(rate.getExtraAdult())+"\"";
	        					jsonRates += ",\"child\":\""+(rate.getExtraChild())+"\"";
//	        					jsonRates += ",\"date\":\""+strDateValues+"\"";
	        					java.util.Date baseDate=new SimpleDateFormat("yyyy-MM-dd").parse(strDateValues);
        					 	jsonRates += ",\"date\":\""+new SimpleDateFormat("MMMM d, yyyy").format(baseDate)+"\"";
	         				    dblRate=rate.getBaseAmount();

			    			 }
	        			}
	        			else
	        			{
	        				for (PropertyAccommodation accommodationRate : accommodationList) {
            					
	        					jsonRates += "\"tariff\":\""+accommodationRate.getBaseAmount()+" \"";
	        					jsonRates += ",\"tariffAmount\":\""+accommodationRate.getBaseAmount()+" \"";
	        					jsonRates += ",\"adult\":\""+(accommodationRate.getExtraAdult())+"\"";
	        					jsonRates += ",\"child\":\""+(accommodationRate.getExtraChild())+"\"";
//	        					jsonRates += ",\"date\":\""+strDateValues+"\"";
	        					java.util.Date baseDate=new SimpleDateFormat("yyyy-MM-dd").parse(strDateValues);
	        					jsonRates += ",\"date\":\""+new SimpleDateFormat("MMMM d, yyyy").format(baseDate)+"\"";
	        					dblRate=accommodationRate.getBaseAmount();
        					 	
	        				}
	        			}
        			}
        		}
        		else{
    				
    				for (PropertyAccommodation accommodationRate : accommodationList) {
    					
    					jsonRates += "\"tariff\":\""+accommodationRate.getBaseAmount()+" \"";
    					jsonRates += ",\"tariffAmount\":\""+accommodationRate.getBaseAmount()+" \"";
    					jsonRates += ",\"adult\":\""+(accommodationRate.getExtraAdult())+"\"";
    					jsonRates += ",\"child\":\""+(accommodationRate.getExtraChild())+"\"";
//    					jsonRates += ",\"date\":\""+strDateValues+"\"";
    					java.util.Date baseDate=new SimpleDateFormat("yyyy-MM-dd").parse(strDateValues);
    					jsonRates += ",\"date\":\""+new SimpleDateFormat("MMMM d, yyyy").format(baseDate)+"\"";
    					dblRate=accommodationRate.getBaseAmount();
    					
    				}
        		}
        		
        		
        		if(blnFirstActivePromo){
				    String strTypePercent=String.valueOf(promotionFirstPercent);
				    String strTypeInr=String.valueOf(promotionFirstInr);
		    		if(!strTypePercent.equalsIgnoreCase("null") && promotionFirstPercent!=0){
		    			flatLastAmount = dblRate-(dblRate*promotionFirstPercent/100);
		    			jsonRates += ",\"firstPromoPercent\":\""+promotionFirstPercent+" \"";
		    			jsonRates += ",\"firstPromoInr\":\""+0+" \"";
		    		}else if(!strTypeInr.equalsIgnoreCase("null") && promotionFirstInr!=0){
		    			flatLastAmount = dblRate-promotionFirstInr;
		    			jsonRates += ",\"firstPromoPercent\":\""+0+" \"";
		    			jsonRates += ",\"firstPromoInr\":\""+promotionFirstInr+" \"";
		    		}else{
		    			flatLastAmount = dblRate;
	    			  	jsonRates += ",\"firstPromoPercent\":\""+0+" \"";
		    			jsonRates += ",\"firstPromoInr\":\""+0+" \"";
	    		 	} 
	    		}else{
    			    flatLastAmount = dblRate;
    			    jsonRates += ",\"firstPromoPercent\":\""+0+" \"";
	    			jsonRates += ",\"firstPromoInr\":\""+0+" \"";
	    		} 
	    		 if(blnSecondActivePromo){
	    			 String strTypePercent=String.valueOf(promotionSecondPercent);
	    			 String strTypeInr=String.valueOf(promotionSecondInr);
		    		  if(!strTypePercent.equalsIgnoreCase("null") && promotionSecondPercent!=0){
		    			  flatLastAmount = flatLastAmount-(flatLastAmount*promotionSecondPercent/100);
		    			  jsonRates += ",\"secondPromoPercent\":\""+promotionSecondPercent+" \"";
		    			  jsonRates += ",\"secondPromoInr\":\""+0+" \"";
		    		  }else if(!strTypeInr.equalsIgnoreCase("null") && promotionSecondInr!=0){
		    			  flatLastAmount = flatLastAmount-promotionSecondInr;
		    			  jsonRates += ",\"secondPromoInr\":\""+promotionSecondInr+" \"";
		    			  jsonRates += ",\"secondPromoPercent\":\""+0+" \"";
		    		  }else{
		    			  flatLastAmount = flatLastAmount;
		    			  jsonRates += ",\"secondPromoPercent\":\""+0+" \"";
		    			  jsonRates += ",\"secondPromoInr\":\""+0+" \"";
		    		  }
	    		 }else{
	    			 flatLastAmount = flatLastAmount;
	    			 jsonRates += ",\"secondPromoPercent\":\""+0+" \"";
	    			 jsonRates += ",\"secondPromoInr\":\""+0+" \"";
	    		 }
	    		 
	    		ArrayList<Double> arlDiscounts=new ArrayList<Double>();
	    		listDiscountDetails = discountController.listDiscounts(d.toDate()); 
	    		if(listDiscountDetails.size()>0 && !listDiscountDetails.isEmpty()){
	    			for(PropertyDiscount discounts:listDiscountDetails){
	    				arlDiscounts.add(discounts.getDiscountPercentage());
	    			}
	    			Collections.sort(arlDiscounts);
	    		}
	    		
	    		Iterator iterDiscounts=arlDiscounts.iterator();
	    		int discountCount=0,arlSize=0;
	    		double discountPercent=0;
	    		arlSize=arlDiscounts.size();
	    		while(iterDiscounts.hasNext()){
	    			discountCount++;
	    			double percent=(double)iterDiscounts.next();
	    			if(discountCount==arlSize){
	    				discountPercent=percent;
	    				jsonRates += ",\"couponPercent\":\""+discountPercent+" \"";
	    			}
	    		}
	    		if(arlSize==0){
	    			jsonRates += ",\"couponPercent\":\""+0+" \"";
	    		}
	    		
	    		flatLastAmount=flatLastAmount-(flatLastAmount*discountPercent/100);
	    		
	    		jsonRates += ",\"sellRateTariff\":\""+flatLastAmount+" \"";
	    		 
	    		if(dblNetRate>flatLastAmount){
	    			jsonRates += ",\"netRateEnable\":\""+0+" \"";	
	    		}else{
	    			jsonRates += ",\"netRateEnable\":\""+1+" \"";
	    		}
        		
        		jsonRates += "}";
        		
        		
        		
        		Iterator iterRatePlan=arlRateType.iterator();
            	while(iterRatePlan.hasNext()){
            		strType=(String)iterRatePlan.next();
            		strSymbol=(String)iterRatePlan.next();
            		dblRatePlanAmount=(Double)iterRatePlan.next();
            		
            		if(strType.equalsIgnoreCase("CP")){
            			if (!jsonRatePlan.equalsIgnoreCase(""))
                			jsonRatePlan += ",{";
        				
        				else
        					jsonRatePlan = "{";
                		
                		jsonRatePlan += "\"ratePlanType\":\""+strType+" \"";
                		if(strSymbol.equalsIgnoreCase("plus")){
                			jsonRatePlan += ",\"tariffRatePlanAmount\":\""+(dblRate+dblRatePlanAmount)+" \"";	
                		}else{
                			jsonRatePlan += ",\"tariffRatePlanAmount\":\""+(dblRate-dblRatePlanAmount)+" \"";
                		}
                		
                		jsonRatePlan += ",\"ratePlanAmount\":\""+(dblRatePlanAmount)+" \"";
                		jsonRatePlan += ",\"ratePlanSymbol\":\""+(strSymbol.toLowerCase())+" \"";
                		
                		jsonRatePlan += "}";	
            		}
            		
            	}
        		
            }
            jsonOutput += ",\"rates\":[" + jsonRates+ "]";
            jsonOutput += ",\"rateplans\":[" + jsonRatePlan+ "]";
            jsonOutput += "}";
            
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
		
	 }
	 
	 public String getSellingRateDetails(){
		 try{

			    
			 this.sourceTypeId = getSourceTypeId();
			 sessionMap.put("sourceTypeId",sourceTypeId); 
				
			this.accommodationId = getAccommodationId();
			sessionMap.put("accommodationId",accommodationId);
			
			this.strStartDate=getStrStartDate();
			this.strEndDate=getStrEndDate();
			
			if(this.strStartDate==null || this.strEndDate==null ){
				return null;
			}
			
			DateFormat format = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
		    java.util.Date dateStart = format.parse(getStrStartDate());
		    java.util.Date dateEnd = format.parse(getStrEndDate());
		   
		    
		    Calendar calCheckStart=Calendar.getInstance();
		    calCheckStart.setTime(dateStart);
		    java.util.Date checkInDate = calCheckStart.getTime();
		    this.arrivalDate=new Timestamp(checkInDate.getTime());
		    
		    Calendar calCheckEnd=Calendar.getInstance();
		    calCheckEnd.setTime(dateEnd);
		    java.util.Date checkOutDate = calCheckEnd.getTime();
		    this.departureDate=new Timestamp(checkOutDate.getTime());
		    
		    
			this.propertyId = (Integer) sessionMap.get("propertyId");
		    
			
		    
			HttpServletResponse response = ServletActionContext.getResponse();
			PropertyAccommodationRoomManager  roomController  = new PropertyAccommodationRoomManager();
			PmsSourceManager sourceController = new PmsSourceManager();
			PropertyRateManager rateController = new PropertyRateManager();
			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
			PropertyRateDetailManager rateDetailController = new PropertyRateDetailManager();
			PropertyRatePlanDetailManager planDetailController=new PropertyRatePlanDetailManager();
			PropertyAccommodationInventoryManager inventoryController=new PropertyAccommodationInventoryManager();
			PmsBookingManager bookingController=new PmsBookingManager();
			PmsPropertyManager propertyController = new PmsPropertyManager();
			boolean dateChk=false;
			String[] weekdays={"Monday","Tuesday","Wednesday","Thursday","Sunday"};
		    String[] weekend={"Friday","Saturday"};
			int roomCount=0,availableCount=0,totalCount=0,totalDate=0,dateCount=0;
	    	Double dblTotalBaseAmount=0.0,dblBaseAmount=0.0,dblPercentCount=0.0,dblSmartPricePercent=0.0;
	    	Double dblPercentFrom,dblPercentTo,dblNetRate;
	    	Boolean isPositive=false,isNegative=false;
	    	
			int fromDay=0,fromMonth=0,fromYear=0;
			int toDay=0,toMonth=0,toYear=0;
			SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
			Calendar calStart=Calendar.getInstance();
			calStart.setTime(getArrivalDate());
			java.util.Date startDate = calStart.getTime();             
	    	String strFromDate = format1.format(startDate);  
	    	
	    	
	    	Calendar calEnd=Calendar.getInstance();
	    	calEnd.setTime(getDepartureDate());
	    	java.util.Date endDate = calEnd.getTime();  
	    	String strToDate = format1.format(endDate); 
	    	
	    	
	    	java.util.Date fromDate = format1.parse(strFromDate);
			Timestamp tsFromDate=new Timestamp(fromDate.getTime());

			java.util.Date toDate = format1.parse(strToDate);
			Timestamp tsToDate=new Timestamp(toDate.getTime());
			
			DateTime dtFromDate = DateTime.parse(strFromDate);
	        DateTime dtToDate = DateTime.parse(strToDate);
	        
			String jsonOutput = "",locationType="NA";
			String jsonRates ="";
			
			java.util.Date currentdate=new java.util.Date();
			String strCurrentDate = new SimpleDateFormat("yyyy-MM-dd").format(currentdate);
		   	java.util.Date curdate = format1.parse(strCurrentDate);
			PmsProperty property=propertyController.find(getPropertyId());
			if(property!=null){
				if(property.getLocationType()!=null){
					locationType=property.getLocationType().getLocationTypeName();	
				}
				
			}
			response.setContentType("application/json");
			   
			SimpleDateFormat formatEE = new SimpleDateFormat("EE", Locale.ENGLISH);
			SimpleDateFormat formatEEEE = new SimpleDateFormat("EEEE", Locale.ENGLISH);
			List<PropertyAccommodation> accommodationList= roomController.list(accommodationId,getPropertyId());
			DateFormat f = new SimpleDateFormat("EEEE");
	    	HashMap<String,String> dateMap=new HashMap<String,String>();
			HashMap<String,String> dayMap=new HashMap<String,String>();
			ArrayList<Integer> listSmartPriceChecked=new ArrayList<Integer>();
			
			ArrayList<String> arrListDateId=new ArrayList<String>();
			ArrayList<String> arrListRateDetail=new ArrayList<String>();
			String strStartDate=null,strEndDate=null,strPropertyRateId=null,
					strAmount=null,strAdult=null,strChild=null,strDaysofWeek=null,strDays=null,strDateValues=null;
						
	    	PropertyAccommodation accommodations=accommodationController.find(accommodationId);
	    	if(accommodations.getPropertyContractType()==null){
	    		this.contractTypeId=0;	
	    		this.contractTypeName=null;
	    	}else{
	    		this.contractTypeId=accommodations.getPropertyContractType().getContractTypeId();
	    		this.contractTypeName=accommodations.getPropertyContractType().getContractTypeName();
	    	}
	    	
	    	if (!jsonOutput.equalsIgnoreCase(""))
				jsonOutput += ",{";
			
			else
				jsonOutput = "{";
	    	
	    	jsonOutput += "\"accommodationId\":\""+accommodations.getAccommodationId()+"\"";
		 	jsonOutput += ",\"accommodationType\":\""+accommodations.getAccommodationType()+"\"";
	        
			
			List<DateTime> listAllDates=DateUtil.getDateRange(dtFromDate, dtToDate);
			for(DateTime d:listAllDates){
				
				long rmc = 0;
	   			 this.roomCnt = rmc;
	        	 int sold=0;
				java.util.Date availDate = d.toDate();
	        	String days=f.format(d.toDate()).toLowerCase();
	        	strDateValues=format1.format(availDate);
				boolean isWeekdays=false,isWeekend=false;
				double maxAmount=0,netrate=0,sellrate=0,sellextraadult=0,sellextrachild=0,
						sellotaextrachild=0,sellotaextraadult=0,extraadult=0,extrachild=0;
				 PropertyAccommodationInventory inventoryCount = inventoryController.findInventoryCount(availDate,accommodations.getAccommodationId());
				 if(inventoryCount != null){
				 PmsAvailableRooms roomCount1 = bookingController.findBookingCount(availDate, accommodations.getAccommodationId(),inventoryCount.getInventoryId());
				 if(roomCount1.getRoomCount() == null) {
				 	String num1 = inventoryCount.getInventoryCount();
					PmsAvailableRooms bookingRoomCount=bookingController.findCount(availDate, accommodations.getAccommodationId());
					PropertyAccommodation accomm=accommodationController.find(accommodations.getAccommodationId());
					Integer totavailable=0;
					if(accommodations!=null){
						totavailable=accommodations.getNoOfUnits();
					}
					if(bookingRoomCount.getRoomCount()!=null){
						long lngRooms=bookingRoomCount.getRoomCount();
						long num2 = Long.parseLong(num1);
						this.roomCnt = num2;
						sold=(int) (lngRooms);
						int availRooms=0;
						availRooms=totavailable-sold;
//						if(Integer.parseInt(num1)>availRooms){
//							this.roomCnt = availRooms;	
//						}else{
							this.roomCnt = num2;
//						}
								
						}else{
							 long num2 = Long.parseLong(num1);
							 this.roomCnt = num2;
							 sold=(int) (rmc);
						}
					}else{		
						PmsAvailableRooms bookingRoomCount=bookingController.findCount(availDate, accommodations.getAccommodationId());
						int intRooms=0;
						Integer totavailable=0,availableRooms=0;
						if(accommodations!=null){
							totavailable=accommodations.getNoOfUnits();
						}
						if(bookingRoomCount.getRoomCount()!=null){
							long lngRooms=bookingRoomCount.getRoomCount();
							long num = roomCount1.getRoomCount();	
							intRooms=(int)lngRooms;
							String num1 = inventoryCount.getInventoryCount();
		 					long num2 = Long.parseLong(num1);
		 					availableRooms=totavailable-intRooms;
	//	 					if(num2>availableRooms){
	//	 						this.roomCnt = availableRooms;	 
	//	 					}else{
		 						this.roomCnt = num2-num;
	//	 					}
		 					long lngSold=bookingRoomCount.getRoomCount();
		   					sold=(int)lngSold;
						}else{
							long num = roomCount1.getRoomCount();	
							String num1 = inventoryCount.getInventoryCount();
							long num2 = Long.parseLong(num1);
		  					this.roomCnt = (num2-num);
		  					long lngSold=roomCount1.getRoomCount();
		   					sold=(int)lngSold;
						}
	
					}
								
				}else{
					  PmsAvailableRooms inventoryCounts = bookingController.findCount(availDate, accommodations.getAccommodationId());							
						if(inventoryCounts.getRoomCount() == null){								
							this.roomCnt = (accommodations.getNoOfUnits()-rmc);
							sold=(int)rmc;
						}
						else{
							this.roomCnt = (accommodations.getNoOfUnits()-inventoryCounts.getRoomCount());	
							long lngSold=inventoryCounts.getRoomCount();
								sold=(int)lngSold;
						}
				}
								
				double totalRooms=0,intSold=0;
				totalRooms=(double)accommodations.getNoOfUnits();
				intSold=(double)sold;
				double occupancy=0.0;
				occupancy=intSold/totalRooms;
				Integer occupancyRating=(int) Math.round(occupancy * 100);
				List<PropertyRate> dateList =  rateController.listAllDates(getPropertyId(),getAccommodationId(),1,d.toDate());
				//List<PropertyRate> dateList =  rateController.list(getPropertyId(),getAccommodationId(),getSourceId(),tsFromDate,tsToDate);
			    if(!dateList.isEmpty()){
			    	for (PropertyRate rates : dateList) {
			    		strPropertyRateId=String.valueOf(rates.getPropertyRateId());
			    		strStartDate=format1.format(rates.getStartDate());
			    		strEndDate=format1.format(rates.getEndDate());
			    		DateTime FromDate = DateTime.parse(strStartDate);
				        DateTime ToDate = DateTime.parse(strEndDate);
				        
				        int propertyRatesId=rates.getPropertyRateId(); 
				        
				        java.util.Date DateValues = d.toDate();
			        	strDateValues=format1.format(DateValues);
			        	
				        arrListDateId.add(strDateValues);
			        	arrListDateId.add(strPropertyRateId);
			        	
			    		List<PropertyRateDetail> rateDetailList =  rateDetailController.listRateDetail(propertyRatesId,f.format(d.toDate()).toLowerCase());
			    		
			    		if(!rateDetailList.isEmpty()){
			    			
			    			
			    			for(PropertyRateDetail rateDetails: rateDetailList){
			    				strAmount=String.valueOf(rateDetails.getBaseAmount());
			    				strAdult=String.valueOf(rateDetails.getExtraAdult());
			    				strChild=String.valueOf(rateDetails.getExtraChild());
			    				
			    				strDaysofWeek=rateDetails.getDaysOfWeek().toLowerCase();
			    				arrListDateId.add(strDaysofWeek);
				        		arrListDateId.add(strAmount);
				        		arrListDateId.add(strAdult);
				        		arrListDateId.add(strChild);
				        		
			    				List<String> daysBetween = DateUtil.getDayRange(FromDate, ToDate);
			    				Iterator<String> iterDayValues=daysBetween.iterator();
						        while(iterDayValues.hasNext()){
						        	strDays=iterDayValues.next().toLowerCase();
						        	if(strDaysofWeek.contains(strDays)){
						        		arrListDateId.add(strDaysofWeek);
						        		arrListDateId.add(strAmount);
						        		arrListDateId.add(strAdult);
						        		arrListDateId.add(strChild);
						        	}
						        }
			    			}
			    			break;
			    		}
			    		dateChk=true;
			    	}
			    }else{
			    	if(accommodationList.size()>0 && !accommodationList.isEmpty()){
			    		for (PropertyAccommodation accommodationRate : accommodationList) {
			    			for(int a=0;a<weekdays.length;a++){
								if(days.equalsIgnoreCase(weekdays[a])){
									isWeekdays=true;
								}
							}
							
							for(int b=0;b<weekend.length;b++){
								if(days.equalsIgnoreCase(weekend[b])){
									isWeekend=true;
								}
							}
							
							if(locationType.equalsIgnoreCase("Leisure")){

								if(isWeekdays){
									if(accommodations.getWeekdaySellingRate()==null){
										maxAmount=0;	
									}else{
										maxAmount=accommodations.getWeekdaySellingRate();
									}
									
									if(this.contractTypeId==7){//B-join
			                    		netrate= accommodations.getWeekdayNetRate()==null?0:accommodations.getWeekdayNetRate();		
			                    	}else if(this.contractTypeId==6){//B-sure
			                    		netrate= accommodations.getWeekdayPurchaseRate()==null?0:accommodations.getWeekdayPurchaseRate();
			                    	}else if(this.contractTypeId==8){//B-stay
			                    		netrate= accommodations.getWeekdayPurchaseRate()==null?0:accommodations.getWeekdayPurchaseRate();
			                    	}else if(this.contractTypeId==9){//B-join flat
			                    		netrate= accommodations.getWeekdayBaseAmount()==null?0:accommodations.getWeekdayBaseAmount();
			                    	}
								}
								
								if(isWeekend){
									if(accommodations.getWeekendSellingRate()==null){
										maxAmount=0;	
									}else{
										maxAmount=accommodations.getWeekendSellingRate();
									}
									
									if(this.contractTypeId==7){//B-join
			                    		netrate= accommodations.getWeekendNetRate()==null?0:accommodations.getWeekendNetRate();		
			                    	}else if(this.contractTypeId==6){//B-sure
			                    		netrate= accommodations.getWeekendPurchaseRate()==null?0:accommodations.getWeekendPurchaseRate();
			                    	}else if(this.contractTypeId==8){//B-stay
			                    		netrate= accommodations.getWeekendPurchaseRate()==null?0:accommodations.getWeekendPurchaseRate();
			                    	}else if(this.contractTypeId==9){//B-join flat
			                    		netrate= accommodations.getWeekendBaseAmount()==null?0:accommodations.getWeekendBaseAmount();
			                    	}
									
								}	
							
							}
							
							if(locationType.equalsIgnoreCase("Metro")){
								if(this.contractTypeId==7){//B-join
			                		netrate= accommodations.getNetRate()==null?0:accommodations.getNetRate();		
			                	}else if(this.contractTypeId==6){//B-sure
			                		netrate= accommodations.getPurchaseRate()==null?0:accommodations.getPurchaseRate();
			                	}else if(this.contractTypeId==8){//B-stay
			                		netrate= accommodations.getPurchaseRate()==null?0:accommodations.getPurchaseRate();
			                	}else if(this.contractTypeId==9){//B-join flat
			                		netrate= accommodations.getBaseAmount()==null?0:accommodations.getBaseAmount();
			                	}
								if(accommodations.getSellingRate()==null){
									maxAmount=0;	
								}else{
									maxAmount=accommodations.getSellingRate();
								}
							}
							
							if(occupancyRating>=0 && occupancyRating<=30){
								sellrate=maxAmount;
							}else if(occupancyRating>30 && occupancyRating<=60){
								sellrate=maxAmount+(maxAmount*10/100);
							}else if(occupancyRating>=60 && occupancyRating<=80){
								sellrate=maxAmount+(maxAmount*15/100);
							}else if(occupancyRating>80){
								sellrate=maxAmount+(maxAmount*20/100);
							}
							if(accommodations.getExtraAdult()==null){
								extraadult=0;
							}else{
								extraadult=accommodations.getExtraAdult();
							}
							
							if(accommodations.getExtraChild()==null){
								extrachild=accommodations.getExtraChild();
							}else{
								extrachild=accommodations.getExtraChild();
							}
							sellextraadult=extraadult+extraadult*15/100;
							sellextrachild=extrachild+extrachild*15/100;
							
							sellotaextraadult=sellextraadult+sellextraadult*20/100;
							sellotaextrachild=sellextrachild+sellextrachild*20/100;
			    			strAmount=String.valueOf(sellrate);
			    			strAdult=String.valueOf(sellextraadult);
			    			strChild=String.valueOf(sellextrachild);
			    			 java.util.Date DateValues = d.toDate();
					         strDateValues=format1.format(DateValues);
						     arrListDateId.add(strDateValues);
					         arrListDateId.add(String.valueOf(0));
					         arrListDateId.add(f.format(d.toDate()).toLowerCase());
				        	 arrListDateId.add(strAmount);
				        	 arrListDateId.add(strAdult);
				        	 arrListDateId.add(strChild);
			    		}
			    		//break;
			    	}
			    }
			    
			    int intCount=0;
				   String strArrays[] = new String[arrListDateId.size()];
				    Iterator<String> iterListValues=arrListDateId.iterator();
				    while(iterListValues.hasNext()){
			    		String strTemp=iterListValues.next();
			    		strArrays[intCount]=strTemp;
			    		intCount++;		
				    }
				    
				    String dateval="",doubleval="",rateId="";
				    for(String s : strArrays) {
				          String[] s2 = s.split(" ");
				          for(String results : s2) {
				              if(results.contains("-")){//Date check
				            	  dateval=results;
				            	  arrListRateDetail.add(dateval);
				            	  
				              }else{// Number
				            	  if(isNumeric(results)){
				            		  rateId=results;
				            	  }
				            	  else{
				            		  int first = results.indexOf(".");
				            		  if ( (first >= 0) && (first - results.lastIndexOf(".")) == 0) {
				            		      // only one decimal point
				            			  doubleval=results;
				            			  arrListRateDetail.add(doubleval);
				            		  }
				            		  else {
				            			
				            			  // no decimal point or more than one decimal point
				            		  }
				            	  }
				              }
				  		      if(!doubleval.isEmpty() && !dateval.isEmpty()){
				  		    	dateMap.put(dateval, doubleval);
				              }
				  		      if(!rateId.isEmpty() && !dateval.isEmpty()){
				  		    	  dayMap.put(dateval, rateId);
				  		      }
				          }
				      }
				    intCount=0;
		            String[] listValues=new String[arrListRateDetail.size()];
		            Iterator<String> rateDetailsIter=arrListRateDetail.iterator();
				    while (rateDetailsIter.hasNext()) {
		                String strTemp=rateDetailsIter.next();
		                listValues[intCount]=strTemp;
			    		intCount++;		
				    }

			}

			for(DateTime d:listAllDates){
            	java.util.Date DateValues = d.toDate();
            	long rmc = 0;
	   			 this.roomCnt = rmc;
	        	 int sold=0;
				java.util.Date availDate = d.toDate();
				String days=f.format(d.toDate()).toLowerCase();
            	strDateValues=format1.format(DateValues);
            	String strType=null;
            	String strSymbol=null;
            	double dblRatePlanAmount=0,dblRate=0;
            	
    		    boolean isWeekdays=false,isWeekend=false;
				double maxAmount=0,netrate=0,sellrate=0,sellextraadult=0,sellextrachild=0,
						sellotaextrachild=0,sellotaextraadult=0,extraadult=0,extrachild=0;
				 PropertyAccommodationInventory inventoryCount = inventoryController.findInventoryCount(availDate,accommodations.getAccommodationId());
				 if(inventoryCount != null){
				 PmsAvailableRooms roomCount1 = bookingController.findBookingCount(availDate, accommodations.getAccommodationId(),inventoryCount.getInventoryId());
				 if(roomCount1.getRoomCount() == null) {
				 	String num1 = inventoryCount.getInventoryCount();
					PmsAvailableRooms bookingRoomCount=bookingController.findCount(availDate, accommodations.getAccommodationId());
					PropertyAccommodation accomm=accommodationController.find(accommodations.getAccommodationId());
					Integer totavailable=0;
					if(accommodations!=null){
						totavailable=accommodations.getNoOfUnits();
					}
					if(bookingRoomCount.getRoomCount()!=null){
						long lngRooms=bookingRoomCount.getRoomCount();
						long num2 = Long.parseLong(num1);
						this.roomCnt = num2;
						sold=(int) (lngRooms);
						int availRooms=0;
						availRooms=totavailable-sold;
//						if(Integer.parseInt(num1)>availRooms){
//							this.roomCnt = availRooms;	
//						}else{
							this.roomCnt = num2;
//						}
								
						}else{
							 long num2 = Long.parseLong(num1);
							 this.roomCnt = num2;
							 sold=(int) (rmc);
						}
					}else{		
						PmsAvailableRooms bookingRoomCount=bookingController.findCount(availDate, accommodations.getAccommodationId());
						int intRooms=0;
						Integer totavailable=0,availableRooms=0;
						if(accommodations!=null){
							totavailable=accommodations.getNoOfUnits();
						}
						if(bookingRoomCount.getRoomCount()!=null){
							long lngRooms=bookingRoomCount.getRoomCount();
							long num = roomCount1.getRoomCount();	
							intRooms=(int)lngRooms;
							String num1 = inventoryCount.getInventoryCount();
		 					long num2 = Long.parseLong(num1);
		 					availableRooms=totavailable-intRooms;
	//	 					if(num2>availableRooms){
	//	 						this.roomCnt = availableRooms;	 
	//	 					}else{
		 						this.roomCnt = num2-num;
	//	 					}
		 					long lngSold=bookingRoomCount.getRoomCount();
		   					sold=(int)lngSold;
						}else{
							long num = roomCount1.getRoomCount();	
							String num1 = inventoryCount.getInventoryCount();
							long num2 = Long.parseLong(num1);
		  					this.roomCnt = (num2-num);
		  					long lngSold=roomCount1.getRoomCount();
		   					sold=(int)lngSold;
						}
	
					}
								
				}else{
					  PmsAvailableRooms inventoryCounts = bookingController.findCount(availDate, accommodations.getAccommodationId());							
						if(inventoryCounts.getRoomCount() == null){								
							this.roomCnt = (accommodations.getNoOfUnits()-rmc);
							sold=(int)rmc;
						}
						else{
							this.roomCnt = (accommodations.getNoOfUnits()-inventoryCounts.getRoomCount());	
							long lngSold=inventoryCounts.getRoomCount();
								sold=(int)lngSold;
						}
				}
								
				double totalRooms=0,intSold=0;
				totalRooms=(double)accommodations.getNoOfUnits();
				intSold=(double)sold;
				double occupancy=0.0;
				occupancy=intSold/totalRooms;
				Integer occupancyRating=(int) Math.round(occupancy * 100);
				
				for(int a=0;a<weekdays.length;a++){
					if(days.equalsIgnoreCase(weekdays[a])){
						isWeekdays=true;
					}
				}
				
				for(int b=0;b<weekend.length;b++){
					if(days.equalsIgnoreCase(weekend[b])){
						isWeekend=true;
					}
				}
				
				if(locationType.equalsIgnoreCase("Leisure")){

					if(isWeekdays){
						if(accommodations.getWeekdaySellingRate()==null){
							maxAmount=0;	
						}else{
							maxAmount=accommodations.getWeekdaySellingRate();
						}
						
						if(this.contractTypeId==7){//B-join
                    		netrate= accommodations.getWeekdayNetRate()==null?0:accommodations.getWeekdayNetRate();		
                    	}else if(this.contractTypeId==6){//B-sure
                    		netrate= accommodations.getWeekdayPurchaseRate()==null?0:accommodations.getWeekdayPurchaseRate();
                    	}else if(this.contractTypeId==8){//B-stay
                    		netrate= accommodations.getWeekdayPurchaseRate()==null?0:accommodations.getWeekdayPurchaseRate();
                    	}else if(this.contractTypeId==9){//B-join flat
                    		netrate= accommodations.getWeekdayBaseAmount()==null?0:accommodations.getWeekdayBaseAmount();
                    	}
					}
					
					if(isWeekend){
						if(accommodations.getWeekendSellingRate()==null){
							maxAmount=0;	
						}else{
							maxAmount=accommodations.getWeekendSellingRate();
						}
						
						if(this.contractTypeId==7){//B-join
                    		netrate= accommodations.getWeekendNetRate()==null?0:accommodations.getWeekendNetRate();		
                    	}else if(this.contractTypeId==6){//B-sure
                    		netrate= accommodations.getWeekendPurchaseRate()==null?0:accommodations.getWeekendPurchaseRate();
                    	}else if(this.contractTypeId==8){//B-stay
                    		netrate= accommodations.getWeekendPurchaseRate()==null?0:accommodations.getWeekendPurchaseRate();
                    	}else if(this.contractTypeId==9){//B-join flat
                    		netrate= accommodations.getWeekendBaseAmount()==null?0:accommodations.getWeekendBaseAmount();
                    	}
						
					}	
				
				}
				
				if(locationType.equalsIgnoreCase("Metro")){
					if(this.contractTypeId==7){//B-join
                		netrate= accommodations.getNetRate()==null?0:accommodations.getNetRate();		
                	}else if(this.contractTypeId==6){//B-sure
                		netrate= accommodations.getPurchaseRate()==null?0:accommodations.getPurchaseRate();
                	}else if(this.contractTypeId==8){//B-stay
                		netrate= accommodations.getPurchaseRate()==null?0:accommodations.getPurchaseRate();
                	}else if(this.contractTypeId==9){//B-join flat
                		netrate= accommodations.getBaseAmount()==null?0:accommodations.getBaseAmount();
                	}
					if(accommodations.getSellingRate()==null){
						maxAmount=0;	
					}else{
						maxAmount=accommodations.getSellingRate();
					}
				}
				
				if(occupancyRating>=0 && occupancyRating<=30){
					sellrate=maxAmount;
				}else if(occupancyRating>30 && occupancyRating<=60){
					sellrate=maxAmount+(maxAmount*10/100);
				}else if(occupancyRating>=60 && occupancyRating<=80){
					sellrate=maxAmount+(maxAmount*15/100);
				}else if(occupancyRating>80){
					sellrate=maxAmount+(maxAmount*20/100);
				}

				if(accommodations.getExtraAdult()==null){
					extraadult=0;
				}else{
					extraadult=accommodations.getExtraAdult();
				}
				
				if(accommodations.getExtraChild()==null){
					extrachild=accommodations.getExtraChild();
				}else{
					extrachild=accommodations.getExtraChild();
				}
				sellextraadult=extraadult+extraadult*15/100;
				sellextrachild=extrachild+extrachild*15/100;
				
				sellotaextraadult=sellextraadult+sellextraadult*20/100;
				sellotaextrachild=sellextrachild+sellextrachild*20/100;
				
				if (!jsonRates.equalsIgnoreCase(""))
					jsonRates += ",{";
				
				else
					jsonRates = "{";
            	String strDay="", strRateId="";
    		    int intRateId=0;
    		    
    		    
        		if(arrListRateDetail.contains(strDateValues)){
        			java.util.Date dteReqDate= format1.parse(strDateValues);
	    			Timestamp tsDate=new Timestamp(dteReqDate.getTime());
	    			
        			strDay=formatEEEE.format(dteReqDate).toLowerCase();
        			strRateId=dayMap.get(strDateValues);
        			intRateId=Integer.parseInt(strRateId);

        			List<PropertyRateDetail> rateDetailList1 =   rateDetailController.list(intRateId,strDay);
        			
        			if(rateDetailList1.size()>0 && !rateDetailList1.isEmpty()){
        				for (PropertyRateDetail rate : rateDetailList1)
		    			 {
        					
        					jsonRates += "\"tariff\":\""+Math.round(rate.getBaseAmount())+" \"";
        					jsonRates += ",\"sellrate\":\""+Math.round(rate.getBaseAmount())+" \"";
        					jsonRates += ",\"netrate\":\""+Math.round(netrate)+"\"";
        					jsonRates += ",\"adult\":\""+Math.round(rate.getExtraAdult())+"\"";
        					jsonRates += ",\"child\":\""+Math.round(rate.getExtraChild())+"\"";
        					jsonRates += ",\"netadult\":\""+Math.round(extraadult)+"\"";
        					jsonRates += ",\"netchild\":\""+Math.round(extrachild)+"\"";
        					sellrate=rate.getBaseAmount();
        					sellextraadult=rate.getExtraAdult();
        					sellextrachild=rate.getExtraChild();
        					
        					java.util.Date baseDate=new SimpleDateFormat("yyyy-MM-dd").parse(strDateValues);
        				 	jsonRates += ",\"date\":\""+new SimpleDateFormat("MMMM d, yyyy").format(baseDate)+"\"";
        				 	double otaRate=0,otaCommission=20;
        				 	otaRate=sellrate+(sellrate*otaCommission/100);
        				 	sellotaextraadult=sellextraadult+sellextraadult*20/100;
        					sellotaextrachild=sellextrachild+sellextrachild*20/100;
        				 	jsonRates += ",\"otatariff\":\""+Math.round(otaRate)+"\"";
        				 	jsonRates += ",\"otaadult\":\""+Math.round(sellotaextraadult)+"\"";
        				 	jsonRates += ",\"otachild\":\""+Math.round(sellotaextrachild)+"\"";

         				    
		    			 }
        			}
        			else
        			{
        				for (PropertyAccommodation accommodationRate : accommodationList) {
        					
        					jsonRates += "\"tariff\":\""+Math.round(sellrate)+" \"";
        					jsonRates += ",\"sellrate\":\""+Math.round(sellrate)+" \"";
        					jsonRates += ",\"netrate\":\""+Math.round(netrate)+"\"";
        					jsonRates += ",\"adult\":\""+Math.round(sellextraadult)+"\"";
        					jsonRates += ",\"child\":\""+Math.round(sellextrachild)+"\"";
        					jsonRates += ",\"netadult\":\""+Math.round(extraadult)+"\"";
        					jsonRates += ",\"netchild\":\""+Math.round(extrachild)+"\"";
        					
        					java.util.Date baseDate=new SimpleDateFormat("yyyy-MM-dd").parse(strDateValues);
        				 	jsonRates += ",\"date\":\""+new SimpleDateFormat("MMMM d, yyyy").format(baseDate)+"\"";
        				 	double otaRate=0,otaCommission=20;
        				 	otaRate=sellrate+(sellrate*otaCommission/100);
        				 	jsonRates += ",\"otatariff\":\""+Math.round(otaRate)+"\"";
        				 	jsonRates += ",\"otaadult\":\""+Math.round(sellotaextraadult)+"\"";
        				 	jsonRates += ",\"otachild\":\""+Math.round(sellotaextrachild)+"\"";
        				}
        			}
    			
        		}
        		else{
    				
    				for (PropertyAccommodation accommodationRate : accommodationList) {
    					jsonRates += "\"tariff\":\""+Math.round(sellrate)+" \"";
    					jsonRates += ",\"sellrate\":\""+Math.round(sellrate)+" \"";
    					jsonRates += ",\"netrate\":\""+Math.round(netrate)+"\"";
    					jsonRates += ",\"adult\":\""+Math.round(sellextraadult)+"\"";
    					jsonRates += ",\"child\":\""+Math.round(sellextrachild)+"\"";
    					jsonRates += ",\"netadult\":\""+Math.round(extraadult)+"\"";
    					jsonRates += ",\"netchild\":\""+Math.round(extrachild)+"\"";
    					
    					java.util.Date baseDate=new SimpleDateFormat("yyyy-MM-dd").parse(strDateValues);
    				 	jsonRates += ",\"date\":\""+new SimpleDateFormat("MMMM d, yyyy").format(baseDate)+"\"";
    				 	double otaRate=0,otaCommission=20;
    				 	otaRate=sellrate+(sellrate*otaCommission/100);
    				 	jsonRates += ",\"otatariff\":\""+Math.round(otaRate)+"\"";
    				 	jsonRates += ",\"otaadult\":\""+Math.round(sellotaextraadult)+"\"";
    				 	jsonRates += ",\"otachild\":\""+Math.round(sellotaextrachild)+"\"";
    					
    				}
        		}
        		jsonRates += "}";
        		
                
			}
			jsonOutput += ",\"rates\":[" + jsonRates+ "]";
            jsonOutput += "}";
			/*if (!jsonRates.equalsIgnoreCase(""))
				jsonRates += ",{";
			
			else
				
				jsonRates = "{";
			
			jsonRates += "\"tariff\":\""+Math.round(sellrate)+" \"";
			jsonRates += ",\"sellrate\":\""+Math.round(sellrate)+" \"";
			jsonRates += ",\"netrate\":\""+Math.round(netrate)+"\"";
			jsonRates += ",\"adult\":\""+Math.round(sellextraadult)+"\"";
			jsonRates += ",\"child\":\""+Math.round(sellextrachild)+"\"";
			jsonRates += ",\"netadult\":\""+Math.round(extraadult)+"\"";
			jsonRates += ",\"netchild\":\""+Math.round(extrachild)+"\"";
			
			java.util.Date baseDate=new SimpleDateFormat("yyyy-MM-dd").parse(strDateValues);
		 	jsonRates += ",\"date\":\""+new SimpleDateFormat("MMMM d, yyyy").format(baseDate)+"\"";
		 	double otaRate=0,otaCommission=20;
		 	otaRate=sellrate+(sellrate*otaCommission/100);
		 	jsonRates += ",\"otatariff\":\""+Math.round(otaRate)+"\"";
		 	jsonRates += ",\"otaadult\":\""+Math.round(sellotaextraadult)+"\"";
		 	jsonRates += ",\"otachild\":\""+Math.round(sellotaextrachild)+"\"";
			jsonRates += "}";
            jsonOutput += ",\"rates\":[" + jsonRates+ "]";
            jsonOutput += "}";*/
            
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			
		
		 }catch(Exception e){
			 logger.error(e);
			 e.printStackTrace();
		 }
		 return null;
	 }
	 public String getLogRates(){
		try{
			this.propertyId=getPropertyId();
			if(this.propertyId==null){
				this.propertyId = (Integer) sessionMap.get("propertyId");
			}
			
			UserLoginManager userController=new UserLoginManager();
			Calendar calendar=Calendar.getInstance();
			SimpleDateFormat format1 = new SimpleDateFormat("dd-MMM-yyyy");
	 		String currentDate = format1.format(calendar.getTime());
	    	java.util.Date curdate=format1.parse(currentDate);
	    	java.sql.Timestamp tsCurrentDate = new java.sql.Timestamp(curdate.getTime());
	    	PropertyRateDetailManager rateDetailController = new PropertyRateDetailManager();
	    	Double dblBaseAmount=0.0;
			String jsonOutput = "",strRateName=null;
			HttpServletResponse response = ServletActionContext.getResponse();
			PropertyRateManager rateController=new PropertyRateManager();
			response.setContentType("application/json");
			int serialno=1;
			this.listPropertyRates=rateController.listPropertyRates(propertyId,tsCurrentDate);
			if(listPropertyRates.size()>0 && !listPropertyRates.isEmpty()){
				for(PropertyRate rates:listPropertyRates){
					if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += ",{";
					else
						jsonOutput += "{";
					jsonOutput += "\"propertyRateId\":\"" + rates.getPropertyRateId() + "\"";
					String start = new SimpleDateFormat("dd-MMM-yyyy").format(rates.getStartDate());
					String end = new SimpleDateFormat("dd-MMM-yyyy").format(rates.getEndDate());
					jsonOutput += ",\"startDate\":\"" + start + "\"";
					jsonOutput += ",\"endDate\":\"" + end + "\"";
					PropertyRateDetail rateDetails=rateDetailController.listDetails(rates.getPropertyRateId());
					dblBaseAmount=rateDetails.getBaseAmount();
					jsonOutput += ",\"baseAmount\":\""+dblBaseAmount+"\"";
					jsonOutput += ",\"serialNo\":\""+serialno+"\"";
					jsonOutput += ",\"accommodationType\":\"" + rates.getPropertyAccommodation().getAccommodationType().trim()+ "\"";
					if(rates.getPmsSourceType()!=null){
						jsonOutput += ",\"sourceType\":\"" + rates.getPmsSourceType().getSourceTypeName().trim()+ "\"";	
					}else{
						jsonOutput += ",\"sourceType\":\"" + "-"+ "\"";
					}
					if(rates.getCreatedDate()!=null){
						String strCreatedDate = new SimpleDateFormat("dd-MMM-yyyy").format(rates.getCreatedDate());
						jsonOutput += ",\"createdDate\":\"" + strCreatedDate+ "\"";
					}else{
						jsonOutput += ",\"createdDate\":\"" + "-"+ "\"";
					}
					if(rates.getCreatedBy()!=null){
						User user=userController.find(rates.getCreatedBy());
						jsonOutput += ",\"createdBy\":\"" + user.getUserName() + "\"";	
					}else{
						jsonOutput += ",\"createdBy\":\"" + "-" + "\"";
					}
					
					
					jsonOutput += "}";
					serialno++;
				}
			}
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
		}
		catch(Exception e){
			logger.error(e);
			e.printStackTrace();
		}
		finally{
			
		}
		 return null;
	 
	 }
	 
	 public Boolean checkLastMinutePromos(String promotionHoursRange,Timestamp tsArrivalDate,Timestamp tsDepartureDate,Timestamp tsStartRange,Timestamp tsEndRange,Timestamp tsFirstRangeDate,Timestamp tsSecondRangeDate){
			boolean blnCheck=false;
			try{
				ArrayList<String> arlDate=new ArrayList<String>();
				int intCount=0,lastCurrentHours=0,currentAMPM=0,firstRangeAMPM=0,secondRangeAMPM=0;
				int firstRange=0,secondRange=0,lastFirstRangeHours=0,lastSecondRangeHours=0;
				String[] arrRange=promotionHoursRange.split("-");
				ArrayList<String> arlRange=new ArrayList<String>();
				for(String str:arrRange){
					arlRange.add(str);
				}
		        Iterator<String> rangeIter=arlRange.iterator();
			    while(rangeIter.hasNext()) {
		            firstRange=Integer.parseInt(rangeIter.next());
		            secondRange=Integer.parseInt(rangeIter.next());
			    }
			    
				Integer startRangeHours=0,startRangeAMPM=0,endRangeHours=0,endRangeAMPM=0;
//				java.util.Date curdate=new java.util.Date();
				java.util.Date currentdate=new java.util.Date();
				Calendar calCurrentDate=Calendar.getInstance();
				calCurrentDate.setTime(currentdate);
				calCurrentDate.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
				java.util.Date curdate=calCurrentDate.getTime();
				
				Timestamp tsStartDate=null,tsEndDate=null; 
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
		   	    
		   	    String checkArrivalDate = new SimpleDateFormat("yyyy-MM-dd").format(tsArrivalDate);
		   	    java.util.Date arrivalDate = format1.parse(checkArrivalDate);
				String checkDepartureDate = new SimpleDateFormat("yyyy-MM-dd").format(tsDepartureDate);
		   	    java.util.Date departureDate = format1.parse(checkDepartureDate);
		   	    
		   	    String strStartRangeDate = new SimpleDateFormat("yyyy-MM-dd").format(tsFirstRangeDate);
		   	    String strStartRange= sdf.format(tsStartRange);
		   	    java.util.Date dteStartRangeDate = sdf.parse(strStartRange);
		   	    Calendar calRangeStart=Calendar.getInstance();
		   	    calRangeStart.setTime(dteStartRangeDate);
		   	    calRangeStart.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
		    	startRangeHours=calRangeStart.get(Calendar.HOUR_OF_DAY);
		    	startRangeAMPM=calRangeStart.get(Calendar.AM_PM);
		    	
				String strEndRangeDate = new SimpleDateFormat("yyyy-MM-dd").format(tsSecondRangeDate);
				String strEndRange = sdf.format(tsEndRange);
		   	    java.util.Date dteEndRangeDate = sdf.parse(strEndRange);
		   	    Calendar calRangeEnd=Calendar.getInstance();
		   	    calRangeEnd.setTime(dteEndRangeDate);
		   	    calRangeEnd.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
		    	endRangeHours=calRangeEnd.get(Calendar.HOUR_OF_DAY);
		    	endRangeAMPM=calRangeEnd.get(Calendar.AM_PM);

		    	String checkArrival = new SimpleDateFormat("yyyy-MM-dd").format(arrivalDate);
		    	String strArrivalTime=checkArrival+" "+startRangeHours+":00:00"; 
		   	    java.util.Date dteArrivalDate = sdf.parse(strArrivalTime);
				Calendar calArrivalRange=Calendar.getInstance();
				calArrivalRange.setTime(dteArrivalDate);
				calArrivalRange.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
		    	String strArrivalDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calArrivalRange.getTime());
		    	java.util.Date dteRangeArrival = sdf.parse(strArrivalDate);
		    	arlDate.add(checkArrival);
		    	
		    	Calendar today=Calendar.getInstance();
		    	today.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
		    	String strTodayDate = new SimpleDateFormat("yyyy-MM-dd").format(today.getTime());
		    	java.util.Date todayDate = format1.parse(strTodayDate);
		    	
		    	DateTime start = DateTime.parse(strStartRangeDate);
		 	    DateTime end = DateTime.parse(strEndRangeDate);
		    	List<DateTime> between = DateUtil.getDateRange(start, end);
				for(DateTime d : between)
			    {
					java.util.Date availDate = d.toDate();
					String strCurrentDate = new SimpleDateFormat("yyyy-MM-dd").format(availDate);
			   	    java.util.Date currentDate = format1.parse(strCurrentDate);
					if(currentDate.equals(todayDate)){
						
						if((int)startRangeAMPM==1 && (int)endRangeAMPM==0){
							
							String checkStartDate = new SimpleDateFormat("yyyy-MM-dd").format(arrivalDate);
					    	String strStartTime=checkStartDate+" 12:00:00"; 
					   	    java.util.Date dteStartDate = sdf.parse(strStartTime);
					   	    
				    		Calendar calFirst=Calendar.getInstance();
							calFirst.setTime(dteStartDate);
							calFirst.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
							calFirst.add(Calendar.HOUR, -firstRange);
							String FirstDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calFirst.getTime());
					    	java.util.Date dteRangeFirst = sdf.parse(FirstDate);
					    	Calendar calFirstRange=Calendar.getInstance();
					    	calFirstRange.setTime(dteRangeFirst);
					    	
					    	Calendar calSecond=Calendar.getInstance();
					    	calSecond.setTime(dteStartDate);
					    	calSecond.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
					    	calSecond.add(Calendar.HOUR, -secondRange);
							String SecondDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calSecond.getTime());
					    	java.util.Date dteRangeSecond = sdf.parse(SecondDate);
					    	Calendar calSecondRange=Calendar.getInstance();
					    	calSecondRange.setTime(dteRangeSecond);
					    	
							/*Calendar calFirst=Calendar.getInstance();
							calFirst.setTime(arrivalDate);
							calFirst.add(Calendar.HOUR, -firstRange);
							String checkStartDate = new SimpleDateFormat("yyyy-MM-dd").format(arrivalDate);
					    	String strStartTime=checkStartDate+" "+startRangeHours+":00:00"; 
					   	    java.util.Date dteStartDate = sdf.parse(strStartTime);
							Calendar calStartRange=Calendar.getInstance();
							calStartRange.setTime(dteStartDate);
							calStartRange.add(Calendar.DAY_OF_MONTH, -1);
					    	String FromDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calStartRange.getTime());
					    	java.util.Date dteRangeFrom = sdf.parse(FromDate);
					    	
					    	Calendar calSecond=Calendar.getInstance();
					    	calSecond.setTime(arrivalDate);
					    	calSecond.add(Calendar.HOUR, -secondRange);
							
					    	String checkEndDate = new SimpleDateFormat("yyyy-MM-dd").format(arrivalDate);
					    	String strEndTime=checkStartDate+" "+endRangeHours+":00:00"; 
					   	    java.util.Date dteEndDate = sdf.parse(strEndTime);
					    	Calendar calEndRange=Calendar.getInstance();
					    	calEndRange.setTime(dteEndDate);
					    	String ToDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calEndRange.getTime());
					    	java.util.Date dteRangeTo = sdf.parse(ToDate);*/
					    	
					    	if(curdate.after(calSecond.getTime()) && curdate.before(calFirst.getTime())) {
					    		blnCheck=true;
					    		
					    		String displayDate = new SimpleDateFormat("MMM dd,yyyy HH:mm:ss").format(calFirst.getTime());
					    		long diff =  dteRangeFirst.getTime()-curdate.getTime();

								long diffSeconds = diff / 1000 % 60;
								long diffMinutes = diff / (60 * 1000) % 60;
								long diffHours = diff / (60 * 60 * 1000) % 24;
								long diffDays = diff / (24 * 60 * 60 * 1000);
								
					    	}
				    	}else{
				    		
				    		String checkStartDate = new SimpleDateFormat("yyyy-MM-dd").format(arrivalDate);
					    	String strStartTime=checkStartDate+" 12:00:00"; 
					   	    java.util.Date dteStartDate = sdf.parse(strStartTime);
					   	    
				    		Calendar calFirst=Calendar.getInstance();
							calFirst.setTime(dteStartDate);
							calFirst.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
							calFirst.add(Calendar.HOUR, -firstRange);
							String FirstDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calFirst.getTime());
					    	java.util.Date dteRangeFirst = sdf.parse(FirstDate);
					    	Calendar calFirstRange=Calendar.getInstance();
					    	calFirstRange.setTime(dteRangeFirst);
					    	
				    		/*String checkStartDate = new SimpleDateFormat("yyyy-MM-dd").format(arrivalDate);
					    	String strStartTime=checkStartDate+" "+startRangeHours+":00:00"; 
					   	    java.util.Date dteStartDate = sdf.parse(strStartTime);
							Calendar calStartRange=Calendar.getInstance();
							calStartRange.setTime(dteStartDate);
					    	String FromDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calStartRange.getTime());
					    	java.util.Date dteRangeFrom = sdf.parse(FromDate);*/
					    	
					    	Calendar calSecond=Calendar.getInstance();
					    	calSecond.setTime(dteStartDate);
					    	calSecond.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
					    	calSecond.add(Calendar.HOUR, -secondRange);
							String SecondDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calSecond.getTime());
					    	java.util.Date dteRangeSecond = sdf.parse(SecondDate);
					    	Calendar calSecondRange=Calendar.getInstance();
					    	calSecondRange.setTime(dteRangeSecond);
					    	
					    	/*String checkEndDate = new SimpleDateFormat("yyyy-MM-dd").format(arrivalDate);
					    	String strEndTime=checkStartDate+" "+endRangeHours+":00:00"; 
					   	    java.util.Date dteEndDate = sdf.parse(strEndTime);
					    	Calendar calEndRange=Calendar.getInstance();
					    	calEndRange.setTime(dteEndDate);
					    	String ToDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(calEndRange.getTime());
					    	java.util.Date dteRangeTo = sdf.parse(ToDate);*/
					    	
					    	if(curdate.after(calSecond.getTime()) && curdate.before(calFirst.getTime())) {
					    		blnCheck=true;
					    		
					    		String displayDate = new SimpleDateFormat("MMM dd,yyyy HH:mm:ss").format(calFirst.getTime());
					    		long diff =  dteRangeFirst.getTime()-curdate.getTime();

								long diffSeconds = diff / 1000 % 60;
								long diffMinutes = diff / (60 * 1000) % 60;
								long diffHours = diff / (60 * 60 * 1000) % 24;
								long diffDays = diff / (24 * 60 * 60 * 1000);
								
					    	}
				    	}
					}
			    }
			}catch(Exception e){
				logger.equals(e);
				e.printStackTrace();
			}
			return blnCheck;
		}
		
		public Boolean checkEarlyBirdPromos(Timestamp bookedDate,Timestamp earlyStartDate,Timestamp earlyEndDate,Timestamp arrival,Timestamp departure){
			boolean blnCheck=false;
			try{
				java.util.Date curdate=new java.util.Date();
				Timestamp tsStartDate=null,tsEndDate=null; 
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
				String checkFromDate = new SimpleDateFormat("yyyy-MM-dd").format(earlyStartDate);
		   	    java.util.Date StartDate = format1.parse(checkFromDate);
				String checkToDate = new SimpleDateFormat("yyyy-MM-dd").format(earlyEndDate);
		   	    java.util.Date EndDate = format1.parse(checkToDate);
				
		   	    String checkArrivalDate = new SimpleDateFormat("yyyy-MM-dd").format(arrival);
		   	    java.util.Date arrivalDate = format1.parse(checkArrivalDate);
				String checkDepartureDate = new SimpleDateFormat("yyyy-MM-dd").format(departure);
		   	    java.util.Date departureDate = format1.parse(checkDepartureDate);
		   	    
		   	    String checkBookedDate = new SimpleDateFormat("yyyy-MM-dd").format(bookedDate);
		   	    java.util.Date dteBookedDate = format1.parse(checkBookedDate);
		   	    
		   	    DateTime start = DateTime.parse(checkFromDate);
		 	    DateTime end = DateTime.parse(checkToDate);
		    	List<DateTime> between = DateUtil.getDateRange(start, end);
				for(DateTime d : between)
			    {
					java.util.Date availDate = d.toDate();
					String strCurrentDate = new SimpleDateFormat("yyyy-MM-dd").format(curdate);
			   	    java.util.Date currentDate = format1.parse(strCurrentDate);
					if(currentDate.equals(dteBookedDate) && arrivalDate.equals(availDate)){
						
						blnCheck=true;
					}
			    }
				
			}catch(Exception e){
				logger.error(e);
				e.printStackTrace();
			}
			return blnCheck;
		}
		
		
		public String getDaysOfWeek( String checkedDays) {	    	
	    	
	    	
		   	 if(getCheckedDays().contains(checkedDays)){    		
		   		 return "True";    		 
		   	 }    	 
		   	 else{
		   		 
		   		 return "False"; 
		   		 
		   	 }
		   	
		   }

		
	 public Integer getSourceTypeId() {
			return sourceTypeId;
		}

		public void setSourceTypeId(Integer sourceTypeId) {
			this.sourceTypeId = sourceTypeId;
		}
		
		public String getModSourceTypes() {
			return modSourceTypes;
		}

		public void setModSourceTypes(String modSourceTypes) {
			this.modSourceTypes = modSourceTypes;
		}

		public Integer getBaseAmount() {
			return baseAmount;
		}

		public void setBaseAmount(Integer baseAmount) {
			this.baseAmount = baseAmount;
		}

		public Integer getExtraAdult() {
			return extraAdult;
		}

		public void setExtraAdult(Integer extraAdult) {
			this.extraAdult = extraAdult;
		}

		public Integer getExtraChild() {
			return extraChild;
		}

		public void setExtraChild(Integer extraChild) {
			this.extraChild = extraChild;
		}

		public Integer getExtraInfant() {
			return extraInfant;
		}

		public void setExtraInfant(Integer extraInfant) {
			this.extraInfant = extraInfant;
		}
		
		public double getMinimumAmount() {
			return minimumAmount;
		}

		public void setMinimumAmount(double minimumAmount) {
			this.minimumAmount = minimumAmount;
		}

		public double getMaximumAmount() {
			return maximumAmount;
		}

		public void setMaximumAmount(double maximumAmount) {
			this.maximumAmount = maximumAmount;
		}

}
