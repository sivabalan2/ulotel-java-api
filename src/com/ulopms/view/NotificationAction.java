package com.ulopms.view;

import java.io.IOException;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;
import com.opensymphony.xwork2.ActionSupport;
import com.ulopms.controller.NotificationManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.Notification;
import com.ulopms.model.User;

import javax.servlet.http.HttpServletRequest;

public class NotificationAction extends ActionSupport implements
ServletRequestAware, UserAware {

	private static final long serialVersionUID = 914982665758390091L;

	private static final Logger logger = Logger
			.getLogger(NotificationAction.class);

	private User user;

	// @Override
	// public User getModel() {
	// return user;
	// }

	@Override
	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	private NotificationManager notificationController = new NotificationManager();

	private HttpServletRequest request;

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	public NotificationAction() {

	}

	public String execute() {
		try
		{
			NotificationManager nm= new NotificationManager();
			
			Notification notify= new Notification();
			notify.setIsRead(true);
			nm.edit(notify);
			
		}
		catch(Exception e)
		{
			logger.error(e);
		}
		/*try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();

			response.setContentType("application/json");
			// response.getWriter().write("[{\"start\":\"2015-05-11T00:00:00\",\"title\":\"test\"}]");

			List<Notification> notiList = notificationController
					.unReadlist(getUser().getUserId());
			for (Notification n : notiList) {

				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"DT_RowId\":\"" + n.getNotificationId() + "\"";
				jsonOutput += ",\"o_notificationtype\":\""
						+ n.getNotificationMessage() + "\"";
				jsonOutput += ",\"o_notificationmsg\":\""
						+ n.getNotificationMessage() + "\"";
				jsonOutput += ",\"o_notificationurl\":\"" + n.getNavigateUrl()
						+ "\"";
				jsonOutput += ",\"o_notificationisread\":\"" + n.isIsRead()
						+ "\"";
				jsonOutput += ",\"o_notificationcreated\":\"" + dconverttime(new java.util.Date(n.getCreatedOn().getTime()) )
						+ "\"";
				jsonOutput += ",\"o_notificationstatus\":\"" +datediffer(new java.util.Date(n.getCreatedOn().getTime()) )
						+ "\"";
				jsonOutput += "}";

			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");

		} catch (Exception e) {
			logger.error(e);
		} finally {

		}
		// this.setArea(a);

		//return null;
*/
		return SUCCESS;
	}

	public String getNotifications() throws IOException {
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();

			response.setContentType("application/json");
			// response.getWriter().write("[{\"start\":\"2015-05-11T00:00:00\",\"title\":\"test\"}]");

			List<Notification> notiList = notificationController
					.unReadList(getUser().getUserId());
			for (Notification n : notiList) {
				
				

				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"DT_RowId\":\"" + n.getNotificationId() + "\"";
				jsonOutput += ",\"o_notificationtype\":\""
						+ n.getNotificationType() + "\"";
				jsonOutput += ",\"o_notificationmsg\":\""
						+ n.getNotificationMessage() + "\"";
				jsonOutput += ",\"o_notificationurl\":\"" + n.getNavigateUrl()
						+ "\"";
				jsonOutput += ",\"o_notificationisread\":\"" + n.getIsRead()
						+ "\"";
				jsonOutput += ",\"o_notificationcreated\":\"" + dconvert(new java.util.Date(n.getCreatedOn().getTime()) )
						+ "\"";
				jsonOutput += ",\"o_notificationstatus\":\"" +datediffer(new java.util.Date(n.getCreatedOn().getTime()) )
						+ "\"";
				jsonOutput += "}";

			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");

		} catch (Exception e) {
			logger.error(e);
		} finally {

		}
		// this.setArea(a);

		return null;

	}
	
	
	public String getsetNotifications() throws IOException {
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();

			response.setContentType("application/json");
			// response.getWriter().write("[{\"start\":\"2015-05-11T00:00:00\",\"title\":\"test\"}]");

			List<Notification> notiList = notificationController
					.list(getUser().getUserId());
			for (Notification n : notiList) {
				NotificationManager nm= new NotificationManager();
				n.setIsRead(true);
				nm.edit(n);
				
				

				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"DT_RowId\":\"" + n.getNotificationId() + "\"";
				jsonOutput += ",\"o_notificationtype\":\""
						+ n.getNotificationType() + "\"";
				jsonOutput += ",\"o_notificationmsg\":\""
						+ n.getNotificationMessage() + "\"";
				jsonOutput += ",\"o_notificationurl\":\"" + n.getNavigateUrl()
						+ "\"";
				jsonOutput += ",\"o_notificationisread\":\"" + n.getIsRead()
						+ "\"";
				jsonOutput += ",\"o_notificationcreated\":\"" + dconvert(new java.util.Date(n.getCreatedOn().getTime()) )
						+ "\"";
				jsonOutput += ",\"o_notificationstatus\":\"" +datediffer(new java.util.Date(n.getCreatedOn().getTime()) )
						+ "\"";
				jsonOutput += "}";

			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");

		} catch (Exception e) {
			logger.error(e);
		} finally {

		}
		// this.setArea(a);

		return null;

	}
	
	
	public String dconverttime(java.util.Date input)
	{
		String disdatetime="";
		try { 
			DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		DateFormat outputFormat1 = new SimpleDateFormat("KK:mm a dd-MMM-yyyy");
		disdatetime=outputFormat1.format(input);
		}catch(Exception e)
        {
                        e.printStackTrace();
        }
		return disdatetime.replace("-", " ");
	}


	public  String  dconvert(java.util.Date dformat){
		 String disdate="";
	    try { 
	    SimpleDateFormat  dateFormat = new SimpleDateFormat("dd-MMM-YYYY");
	               
	               
	                 disdate=dateFormat.format(dformat).replace("-"," ");
	                
	                }catch(Exception e)
	                {
	                                e.printStackTrace();
	                }
	    return disdate;
	}

	public  String datediffer(java.util.Date dformat){	
		
		String disstatus="";
	    try { 
	    SimpleDateFormat  dateFormat = new SimpleDateFormat("yyyy/MM/dd");
	               
	                java.util.Date date = new java.util.Date();
	                String date1=dateFormat.format(dformat);
	                String date2=dateFormat.format(date);
	                java.util.Date d1=dateFormat.parse(date1);
	                java.util.Date d2=dateFormat.parse(date2);
	                
	                long diff=d2.getTime() - d1.getTime();
	                if(diff>0)
	                {
	                disstatus="old";
	                }
	                else
	                {
	                  disstatus="New";
	                }
	                
	                }catch(Exception e)
	                {
	                                e.printStackTrace();
	                }
	    
	    
	    return disstatus;
	}

	

}
