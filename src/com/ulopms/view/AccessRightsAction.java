package com.ulopms.view;

import java.io.IOException;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.catalina.startup.UserConfig;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;

import com.opensymphony.xwork2.ActionSupport;
import com.ulopms.controller.AccessRightManager;

import com.ulopms.controller.UserLoginManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.AccessRight;


import com.ulopms.model.User;

import javax.servlet.http.HttpServletRequest;

public class AccessRightsAction extends ActionSupport implements ServletRequestAware,
UserAware {

	private static final long serialVersionUID = 9149826260758390091L;

	private User user;

	private static final Logger logger = Logger.getLogger(AccessRightsAction.class);

	// @Override
	// public User getModel() {
	// return user;
	// }

	@Override
	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	private HttpServletRequest request;

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	public AccessRightsAction() {

	}

	public String GetAccessRights() throws IOException {
		try {
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();

			response.setContentType("application/json");
			// response.getWriter().write("[{\"start\":\"2015-05-11T00:00:00\",\"title\":\"test\"}]");

			AccessRightManager rightsController = new AccessRightManager();
			List<AccessRight> objRightsList = rightsController.list(request.getParameter("rightsType"));
			for (AccessRight objRights : objRightsList) {
				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"DT_RowId\":\""
						+ objRights.getAccessRightsId() + "\"";
				jsonOutput += ",\"accessRights\":\""
						+ objRights.getAccessRights() + "\"";
				jsonOutput += ",\"displayName\":\"" + objRights.getDisplayName()
						+ "\"";
				jsonOutput += ",\"createdDate\":\""
						+ objRights.getCreatedDate() + "\"";

				jsonOutput += "}";

			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");

		} catch (Exception e) {
			logger.error(e);
		}
		return null;
	}

	public String execute() {

		return SUCCESS;
	}

}
