package com.ulopms.view;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;

import com.opensymphony.xwork2.ActionSupport;
import com.ulopms.controller.PmsPropertyManager;
import com.ulopms.controller.PropertyReviewsManager;
import com.ulopms.controller.UserLoginManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.BookingDetailReport;
import com.ulopms.model.PmsProperty;
import com.ulopms.model.PropertyReviews;
import com.ulopms.model.User;
import com.ulopms.util.Image;
import com.ulopms.controller.PropertyGuestReviewManager;
import com.ulopms.controller.ReviewsManager;
import com.ulopms.model.PropertyGuestReview;
import com.ulopms.model.Reviews;

public class ReviewAction extends ActionSupport implements
ServletRequestAware,SessionAware, UserAware{

	private static final long serialVersionUID = 914982665758390091L;
	private Integer propertyId;
	private Integer propertyReviewId;
	private Integer reviewPropertyId;
	private Integer propertyStarCount;
	private Double reviewCount;
	private Double averageReview;
	private Integer userId;
	private File myFile;
	private String myFileContentType;
	private String myFileFileName;
	private Integer limit;
	private Integer offset;
	private Integer guestReviewId;
	private Integer tripadvisorPropertyStarCount;
	private Double tripadvisorReviewCount;
	private Double tripadvisorAverageReview;
	private Integer reviewerId;
	private String reviewGuest;
	private String reviewDescription;
	private Integer guestReviewsCount;
	private String propertyRating;
	private String googleReviewUrl;
	private String tripadvisorReviewUrl;

	

	private static final Logger logger = Logger.getLogger(ReviewAction.class);

	private HttpSession session;

	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}
	
	private SessionMap<String,Object> sessionMap;  
	  
	//getters and setters  
	  
	@Override  
	public void setSession(Map<String, Object> map) {  
	    sessionMap=(SessionMap)map;  
	}  
	
	private User user;

	
	// @Override
	// public User getModel() {
	// return user;
	// }

	@Override
	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	
	private HttpServletRequest request;

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}
	

	public ReviewAction() {
		
	

	}

	public String execute() {//.
//this.propertyId= session("")
		//this.patientId = getPatientId();
		
		
		return SUCCESS;
	}

	

	
	public String addReviewPicture() throws IOException {
		try {
			PropertyReviewsManager reviewController=new PropertyReviewsManager();
			File fileZip1 = new File(getText("storage.propertyreviewthumb.photo") + "/"
					+ getPropertyReviewId() + "/", this.getMyFileFileName());
			FileUtils.copyFile(this.myFile, fileZip1);// copying image in the
			PropertyReviews propertyReviews = reviewController.find(getPropertyReviewId()); 
			propertyReviews.setReviewImagePath(this.getMyFileFileName());
			reviewController.edit(propertyReviews);
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return SUCCESS;
	}
	

	public String addPropertyReviews() {
		try {
			PropertyReviewsManager reviewController=new PropertyReviewsManager();
			PmsPropertyManager propertyController=new PmsPropertyManager();
			SimpleDateFormat sdfformat = new SimpleDateFormat("yyyy-MM-dd");
			java.util.Date date=new java.util.Date();
        	Calendar calDate=Calendar.getInstance();
        	calDate.setTime(date);
        	calDate.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
        	String strCurrentDate = new SimpleDateFormat("yyyy-MM-dd").format(calDate.getTime());
		    java.util.Date currentDate = sdfformat.parse(strCurrentDate);
		    Timestamp tsCurrentDate = new Timestamp(currentDate.getTime());
		    
			this.userId=(Integer)sessionMap.get("userId");
			
			PropertyReviews propertyReviews=new PropertyReviews();
			propertyReviews.setIsActive(true);
			propertyReviews.setIsDeleted(false);
			propertyReviews.setStarCount(propertyStarCount);
			propertyReviews.setReviewCount(reviewCount);
			propertyReviews.setAverageReview(averageReview);
			propertyReviews.setCreatedBy(this.userId);
			propertyReviews.setTripadvisorStarCount(getTripadvisorPropertyStarCount());
			propertyReviews.setTripadvisorReviewCount(getTripadvisorReviewCount());
			propertyReviews.setTripadvisorAverageReview(getTripadvisorAverageReview());
			propertyReviews.setGoogleReviewUrl(getGoogleReviewUrl());
			propertyReviews.setTripadvisorReviewUrl(getTripadvisorReviewUrl());
			propertyReviews.setPropertyRating(getPropertyRating());

			propertyReviews.setCreatedDate(tsCurrentDate);
			PmsProperty property = propertyController.find(getReviewPropertyId());
			propertyReviews.setPmsProperty(property);
			
			PropertyReviews reviews=reviewController.add(propertyReviews);
			
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("application/json");
			
			  if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"propertyReviewId\":\"" + reviews.getPropertyReviewId()  + "\"";
				
				jsonOutput += "}";


			

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}
	
	public String editPropertyReviews(){
		try{
			this.userId=(Integer)sessionMap.get("userId");
			PropertyReviewsManager reviewController=new PropertyReviewsManager();
			PmsPropertyManager propertyController=new PmsPropertyManager();
			SimpleDateFormat sdfformat = new SimpleDateFormat("yyyy-MM-dd");
			java.util.Date date=new java.util.Date();
        	Calendar calDate=Calendar.getInstance();
        	calDate.setTime(date);
        	calDate.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
        	String strCurrentDate = new SimpleDateFormat("yyyy-MM-dd").format(calDate.getTime());
		    java.util.Date currentDate = sdfformat.parse(strCurrentDate);
		    Timestamp tsCurrentDate = new Timestamp(currentDate.getTime());
		    
		    PropertyReviews propertyReviews=reviewController.find(getPropertyReviewId());
		    propertyReviews.setIsActive(true);
		    propertyReviews.setIsDeleted(false);
		    propertyReviews.setStarCount(getPropertyStarCount());
			propertyReviews.setReviewCount(getReviewCount());
			propertyReviews.setAverageReview(getAverageReview());
			propertyReviews.setTripadvisorStarCount(getTripadvisorPropertyStarCount());
			propertyReviews.setTripadvisorReviewCount(getTripadvisorReviewCount());
			propertyReviews.setTripadvisorAverageReview(getTripadvisorAverageReview());
			propertyReviews.setGoogleReviewUrl(getGoogleReviewUrl());
			propertyReviews.setTripadvisorReviewUrl(getTripadvisorReviewUrl());
			propertyReviews.setPropertyRating(getPropertyRating());
			propertyReviews.setCreatedBy(this.userId);
			propertyReviews.setCreatedDate(tsCurrentDate);
			PmsProperty property = propertyController.find(getReviewPropertyId());
			propertyReviews.setPmsProperty(property);
			reviewController.edit(propertyReviews);
		    
		}catch(Exception e){
			logger.error(e);
			e.printStackTrace();
		}
		return null;
	}

	public String getPropertyReviews(){
		try{
			PropertyReviewsManager reviewController=new PropertyReviewsManager();
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("application/json");
			UserLoginManager userController=new UserLoginManager();
			String jsonReviews ="";
			int serialno=1;
			List<PropertyReviews> listReviews=reviewController.list(limit,offset);
			if(listReviews.size()>0 && !listReviews.isEmpty()){
				for(PropertyReviews reviewDetails : listReviews){
					if (!jsonReviews.equalsIgnoreCase(""))
						
						jsonReviews += ",{";
					else
						jsonReviews += "{";
					
					jsonReviews += "\"propertyReviewId\":\"" + reviewDetails.getPropertyReviewId()+ "\"";	
					jsonReviews += ",\"serialNo\":\"" + serialno + "\"";
					/*jsonReviews += ",\"starCount\":\"" + reviewDetails.getStarCount()+ "\"";	
					jsonReviews += ",\"reviewCount\":\"" + reviewDetails.getReviewCount()+ "\"";	
					jsonReviews += ",\"averageReview\":\"" + reviewDetails.getAverageReview()+ "\"";*/
					jsonReviews += ",\"starCount\":\"" + (reviewDetails.getStarCount()==null?0:reviewDetails.getStarCount())+ "\"";	
					jsonReviews += ",\"reviewCount\":\"" + (reviewDetails.getReviewCount()==null?0:reviewDetails.getReviewCount())+ "\"";	
					jsonReviews += ",\"averageReview\":\"" + (reviewDetails.getAverageReview()==null?0:reviewDetails.getAverageReview())+ "\"";
					jsonReviews += ",\"tripadvisorStarCount\":\"" + (reviewDetails.getTripadvisorStarCount()==null?0:reviewDetails.getTripadvisorStarCount())+ "\"";	
					jsonReviews += ",\"tripadvisorReviewCount\":\"" + (reviewDetails.getTripadvisorReviewCount()==null?0:reviewDetails.getTripadvisorReviewCount())+ "\"";	
					jsonReviews += ",\"tripadvisorAverageReview\":\"" + (reviewDetails.getTripadvisorAverageReview()==null?0:reviewDetails.getTripadvisorAverageReview())+ "\"";
					jsonReviews += ",\"reviewPropertyId\":\"" + reviewDetails.getPmsProperty().getPropertyId()+ "\"";
					jsonReviews += ",\"reviewPropertyName\":\"" + reviewDetails.getPmsProperty().getPropertyName()+ "\"";
					
					if(reviewDetails.getCreatedBy()!=null){
						User user=userController.find(reviewDetails.getCreatedBy());
						String strFirstName=user.getUserName().trim();
						String firstNameLetterUpperCase=strFirstName.substring(0, 1).toUpperCase()+strFirstName.substring(1);
						jsonReviews += ",\"createdBy\":\"" + firstNameLetterUpperCase+ "\"";
					}else{
						jsonReviews += ",\"createdBy\":\"" +" "+ "\"";
					}
					
					jsonReviews += "}";
					serialno++;
				}
				
			}
			response.getWriter().write("{\"data\":[" + jsonReviews + "]}");
		}catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}
	
	public String getPropertyReview(){
		try{
			PropertyReviewsManager reviewController=new PropertyReviewsManager();
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("application/json");
			String jsonReviews ="";
			
			List<PropertyReviews> listReviews=reviewController.listReview(getPropertyReviewId(),getReviewPropertyId());
			if(listReviews.size()>0 && !listReviews.isEmpty()){
				for(PropertyReviews reviewDetails : listReviews){
					if (!jsonReviews.equalsIgnoreCase(""))
						
						jsonReviews += ",{";
					else
						jsonReviews += "{";
					
					jsonReviews += "\"propertyReviewId\":\"" + reviewDetails.getPropertyReviewId()+ "\"";	
					/*jsonReviews += ",\"starCount\":\"" + reviewDetails.getStarCount()+ "\"";	
					jsonReviews += ",\"reviewCount\":\"" + reviewDetails.getReviewCount()+ "\"";	
					jsonReviews += ",\"averageReview\":\"" + reviewDetails.getAverageReview()+ "\"";*/
					jsonReviews += ",\"starCount\":\"" + (reviewDetails.getStarCount()==null?0:reviewDetails.getStarCount())+ "\"";	
					jsonReviews += ",\"reviewCount\":\"" + (reviewDetails.getReviewCount()==null?0:reviewDetails.getReviewCount())+ "\"";	
					jsonReviews += ",\"averageReview\":\"" + (reviewDetails.getAverageReview()==null?0:reviewDetails.getAverageReview())+ "\"";
					jsonReviews += ",\"tripadvisorStarCount\":\"" + (reviewDetails.getTripadvisorStarCount()==null?0:reviewDetails.getTripadvisorStarCount())+ "\"";	
					jsonReviews += ",\"tripadvisorReviewCount\":\"" + (reviewDetails.getTripadvisorReviewCount()==null?0:reviewDetails.getTripadvisorReviewCount())+ "\"";	
					jsonReviews += ",\"tripadvisorAverageReview\":\"" + (reviewDetails.getTripadvisorAverageReview()==null?0:reviewDetails.getTripadvisorAverageReview())+ "\"";
					jsonReviews += ",\"googleReviewUrl\":\"" + (reviewDetails.getGoogleReviewUrl()==null?"":reviewDetails.getGoogleReviewUrl())+ "\"";	
					jsonReviews += ",\"tripadvisorReviewUrl\":\"" + (reviewDetails.getTripadvisorReviewUrl()==null?"":reviewDetails.getTripadvisorReviewUrl())+ "\"";	
					jsonReviews += ",\"propertyRating\":\"" + (reviewDetails.getPropertyRating()==null?"None":reviewDetails.getPropertyRating())+ "\"";
					jsonReviews += ",\"reviewPropertyId\":\"" + reviewDetails.getPmsProperty().getPropertyId()+ "\"";
					jsonReviews += ",\"reviewPropertyName\":\"" + reviewDetails.getPmsProperty().getPropertyName()+ "\"";
					
					jsonReviews += "}";
				}
				
			}
			response.getWriter().write("{\"data\":[" + jsonReviews + "]}");
		}catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}
	
	public String getPropertyReviewCount() throws IOException {
 		try {
 			String jsonOutput = "";
 			HttpServletResponse response = ServletActionContext.getResponse();
 			response.setContentType("application/json");
 			PropertyReviewsManager reviewController=new PropertyReviewsManager();
 			List<PropertyReviews> listReviewCount =reviewController.list();
 			if(listReviewCount.size()>0 && !listReviewCount.isEmpty()){
 				if (!jsonOutput.equalsIgnoreCase(""))
 					jsonOutput += ",{";
 				else
 					jsonOutput += "{";
 				
 			    jsonOutput += "\"count\":\"" + listReviewCount.size() + "\"";
 			    
 				jsonOutput += "}";
 			}
 			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
 		} catch (Exception e) {
 			logger.error(e);
 			e.printStackTrace();
 		} finally {

 		}

 		return null;

 	}
	
	
	public String getPropertyReviewThumbPicture() {
		
		//this.propertyId = (Integer) sessionMap.get("propertyId");
		HttpServletResponse response = ServletActionContext.getResponse();

		PropertyReviewsManager reviewController = new PropertyReviewsManager();
		PropertyReviews reviews = reviewController.find(getPropertyReviewId());
		String imagePath = "";
		// response.setContentType("");
		try {
			
			if ( reviews.getReviewImagePath()  != null) {
				imagePath = getText("storage.propertyreviewthumb.photo") + "/"
						+ reviews.getPropertyReviewId()+ "/"
						+ reviews.getReviewImagePath();
				Image img = new Image();
				
				response.getOutputStream().write(img.getCustomImageInBytes(imagePath));
				response.getOutputStream().flush();
			} else {
				imagePath = getText("storage.propertyreviewthumb.photo") + "/emptyprofilepic.png";
				Image img = new Image();
				
				response.getOutputStream().write(img.getCustomImageInBytes(imagePath));
				response.getOutputStream().flush();
			}
		} catch (Exception e1) {
			logger.error(e1);
		} finally {

		}
		return null;

	}

public String getPropertyDetailReviewThumbPicture() {
		
		//this.propertyId = (Integer) sessionMap.get("propertyId");
		HttpServletResponse response = ServletActionContext.getResponse();

		
		String imagePath = "";
		// response.setContentType("");
		try {
			
			PropertyReviewsManager reviewController = new PropertyReviewsManager();
			List<PropertyReviews> listpropertyreviews = reviewController.listReview(getReviewPropertyId());
			if(listpropertyreviews.size()>0 && !listpropertyreviews.isEmpty()){
				for(PropertyReviews reviews:listpropertyreviews){
					if ( reviews.getReviewImagePath()  != null) {
						imagePath = getText("storage.propertyreviewthumb.photo") + "/"
								+ reviews.getPropertyReviewId()+ "/"
								+ reviews.getReviewImagePath();
						Image img = new Image();
						
						response.getOutputStream().write(img.getCustomImageInBytes(imagePath));
						response.getOutputStream().flush();
					} else {
						imagePath = getText("storage.propertyreviewthumb.photo") + "/emptyprofilepic.png";
						Image img = new Image();
						
						response.getOutputStream().write(img.getCustomImageInBytes(imagePath));
						response.getOutputStream().flush();
					}
				}
			}
			
		} catch (Exception e1) {
			logger.error(e1);
		} finally {

		}
		return null;

	}

	public String deletePropertyReview(){
		try{
			
			PropertyReviewsManager reviewController=new PropertyReviewsManager();
			PropertyReviews reviews=reviewController.find(getPropertyReviewId());
			this.userId=(Integer)sessionMap.get("userId");
			
			reviews.setCreatedBy(this.userId);
			reviews.setIsActive(false);
			reviews.setIsDeleted(true);
			reviewController.edit(reviews);
			
		}catch(Exception e){
			logger.error(e);
			e.printStackTrace();
		}finally{
			
		}
		return null;
	}
	
	public String deleteGuestReview(){
		try{
			PropertyGuestReviewManager guestReviewController=new PropertyGuestReviewManager();
			PropertyGuestReview guestReview=guestReviewController.find(getGuestReviewId()); 
			this.userId=(Integer)sessionMap.get("userId");
			
			guestReview.setCreatedBy(this.userId);
			guestReview.setIsActive(false);
			guestReview.setIsDeleted(true);
			guestReviewController.edit(guestReview);
			
		}catch(Exception ex){
			logger.error(ex);
			ex.printStackTrace();
		}
		return null;
	}
	
	public String getGuestReviews(){
		try{
			PropertyGuestReviewManager reviewController=new PropertyGuestReviewManager();
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("application/json");
			UserLoginManager userController=new UserLoginManager();
			PmsPropertyManager propertyController=new PmsPropertyManager();
			String jsonReviews ="";
			int serialno=1;
			List<PropertyGuestReview> listReviews=reviewController.list(limit,offset);
			if(listReviews.size()>0 && !listReviews.isEmpty()){
				for(PropertyGuestReview reviewDetails : listReviews){
					if (!jsonReviews.equalsIgnoreCase(""))
						
						jsonReviews += ",{";
					else
						jsonReviews += "{";
					
					jsonReviews += "\"guestReviewId\":\"" + reviewDetails.getGuestReviewId() + "\"";	
					jsonReviews += ",\"reviewPropertyId\":\"" + reviewDetails.getPmsProperty().getPropertyId() + "\"";
					jsonReviews += ",\"serialNo\":\"" + serialno + "\"";
					jsonReviews += ",\"reviewCount\":\"" + reviewDetails.getReviewCount() + "\"";	
					jsonReviews += ",\"guestName\":\"" + reviewDetails.getReviewGuestName() + "\"";
					PmsProperty property=propertyController.find(reviewDetails.getPmsProperty().getPropertyId());
					jsonReviews += ",\"reviewPropertyName\":\"" + property.getPropertyName() + "\"";
					
					if(reviewDetails.getCreatedBy()!=null){
						User user=userController.find(reviewDetails.getCreatedBy());
						String strFirstName=user.getUserName().trim();
						String firstNameLetterUpperCase=strFirstName.substring(0, 1).toUpperCase()+strFirstName.substring(1);
						jsonReviews += ",\"createdBy\":\"" + firstNameLetterUpperCase+ "\"";
					}else{
						jsonReviews += ",\"createdBy\":\"" +" "+ "\"";
					}
					
					jsonReviews += "}";
					serialno++;
				}
				
			}
			response.getWriter().write("{\"data\":[" + jsonReviews + "]}");
		}catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}
	
	public String getGuestReviewCount() throws IOException {
 		try {
 			String jsonOutput = "";
 			HttpServletResponse response = ServletActionContext.getResponse();
 			response.setContentType("application/json");
 			PropertyGuestReviewManager reviewController=new PropertyGuestReviewManager();
 			List<PropertyGuestReview> listReviewCount =reviewController.list();
 			if(listReviewCount.size()>0 && !listReviewCount.isEmpty()){
 				if (!jsonOutput.equalsIgnoreCase(""))
 					jsonOutput += ",{";
 				else
 					jsonOutput += "{";
 				
 			    jsonOutput += "\"count\":\"" + listReviewCount.size() + "\"";
 			    
 				jsonOutput += "}";
 			}
 			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
 		} catch (Exception e) {
 			logger.error(e);
 			e.printStackTrace();
 		} finally {

 		}

 		return null;

 	}
	
	public String getAllReviewer(){
		try{
			String jsonOutput = "";
 			HttpServletResponse response = ServletActionContext.getResponse();
 			response.setContentType("application/json");
 			ReviewsManager reviewController=new ReviewsManager();
 			List<Reviews> listReviewers =reviewController.list();
 			if(listReviewers.size()>0 && !listReviewers.isEmpty()){
 				for(Reviews review:listReviewers){
 					if (!jsonOutput.equalsIgnoreCase(""))
 	 					jsonOutput += ",{";
 	 				else
 	 					jsonOutput += "{";
 					
 					jsonOutput += "\"reviewerId\":\"" + review.getReviewerId()+ "\"";	
 					jsonOutput += ",\"reviewerName\":\"" + review.getReviewerName() + "\"";
 					
 					jsonOutput += "}";
 				}
 			}
 			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
		}catch(Exception e){
			logger.error(e);
			e.printStackTrace();
		}
		return null;
	}

	public String getGuestReview(){
		try{
			PropertyGuestReviewManager reviewController=new PropertyGuestReviewManager();
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("application/json");
			String jsonReviews ="";
			
			List<PropertyGuestReview> listReview=reviewController.listReview(getGuestReviewId(),getReviewPropertyId());
			if(listReview.size()>0 && !listReview.isEmpty()){
				for(PropertyGuestReview reviewDetails : listReview){
					if (!jsonReviews.equalsIgnoreCase(""))
						
						jsonReviews += ",{";
					else
						jsonReviews += "{";
					
					jsonReviews += "\"guestReviewId\":\"" + reviewDetails.getGuestReviewId()+ "\"";	
					jsonReviews += ",\"reviewCount\":\"" + reviewDetails.getReviewCount()+ "\"";	
					jsonReviews += ",\"guestName\":\"" + reviewDetails.getReviewGuestName()+ "\"";	
					jsonReviews += ",\"reviewDescription\":\"" + reviewDetails.getReviewDescription()+ "\"";
					jsonReviews += ",\"reviewerId\":\"" + reviewDetails.getReviews().getReviewerId()+ "\"";
					jsonReviews += ",\"reviewerName\":\"" + reviewDetails.getReviews().getReviewerName()+ "\"";
					jsonReviews += ",\"reviewPropertyId\":\"" + reviewDetails.getPmsProperty().getPropertyId()+ "\"";
					jsonReviews += ",\"reviewPropertyName\":\"" + reviewDetails.getPmsProperty().getPropertyName()+ "\"";
					
					jsonReviews += "}";
				}
				
			}
			response.getWriter().write("{\"data\":[" + jsonReviews + "]}");
		}catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}
	
	public String addGuestReview() {
		try {
			PropertyGuestReviewManager reviewController=new PropertyGuestReviewManager();
			PmsPropertyManager propertyController=new PmsPropertyManager();
			ReviewsManager reviewerController=new ReviewsManager();
			SimpleDateFormat sdfformat = new SimpleDateFormat("yyyy-MM-dd");
			java.util.Date date=new java.util.Date();
        	Calendar calDate=Calendar.getInstance();
        	calDate.setTime(date);
        	calDate.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
        	String strCurrentDate = new SimpleDateFormat("yyyy-MM-dd").format(calDate.getTime());
		    java.util.Date currentDate = sdfformat.parse(strCurrentDate);
		    Timestamp tsCurrentDate = new Timestamp(currentDate.getTime());
		    
			this.userId=(Integer)sessionMap.get("userId");
			
			PropertyGuestReview guestReviews=new PropertyGuestReview();
			guestReviews.setIsActive(true);
			guestReviews.setIsDeleted(false);
			
			guestReviews.setCreatedBy(this.userId);
			guestReviews.setCreatedOn(tsCurrentDate);
			PmsProperty property = propertyController.find(getReviewPropertyId());
			guestReviews.setPmsProperty(property);
			guestReviews.setReviewGuestName(getReviewGuest());
			guestReviews.setReviewCount(getGuestReviewsCount());
			guestReviews.setReviewDescription(getReviewDescription().trim());
			Reviews reviewers=reviewerController.find(getReviewerId());
			guestReviews.setReviews(reviewers);
			reviewController.add(guestReviews);
			
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}
	
	public String editGuestReview(){
		try{
			PropertyGuestReviewManager reviewController=new PropertyGuestReviewManager();
			PmsPropertyManager propertyController=new PmsPropertyManager();
			ReviewsManager reviewerController=new ReviewsManager();
			SimpleDateFormat sdfformat = new SimpleDateFormat("yyyy-MM-dd");
			java.util.Date date=new java.util.Date();
        	Calendar calDate=Calendar.getInstance();
        	calDate.setTime(date);
        	calDate.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
        	String strCurrentDate = new SimpleDateFormat("yyyy-MM-dd").format(calDate.getTime());
		    java.util.Date currentDate = sdfformat.parse(strCurrentDate);
		    Timestamp tsCurrentDate = new Timestamp(currentDate.getTime());
		    
			this.userId=(Integer)sessionMap.get("userId");
			
			PropertyGuestReview guestReviews=reviewController.find(getGuestReviewId());
			guestReviews.setIsActive(true);
			guestReviews.setIsDeleted(false);
			
			guestReviews.setCreatedBy(this.userId);
			guestReviews.setCreatedOn(tsCurrentDate);
			PmsProperty property = propertyController.find(getReviewPropertyId());
			guestReviews.setPmsProperty(property);
			guestReviews.setReviewCount(getGuestReviewsCount());
			guestReviews.setReviewGuestName(getReviewGuest());
			guestReviews.setReviewDescription(getReviewDescription());
			Reviews reviewers=reviewerController.find(getReviewerId());
			guestReviews.setReviews(reviewers);
			reviewController.edit(guestReviews);
			
		}catch(Exception e){
			logger.error(e);
			e.printStackTrace();
		}
		return null;
	}
	


	public Integer getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(Integer propertyId) {
		this.propertyId = propertyId;
	}
     
	



	public File getMyFile() {
		return myFile;
	}

	public void setMyFile(File myFile) {
		this.myFile = myFile;
	}

	public String getMyFileContentType() {
		return myFileContentType;
	}

	public void setMyFileContentType(String myFileContentType) {
		this.myFileContentType = myFileContentType;
	}

	public String getMyFileFileName() {
		return myFileFileName;
	}

	public void setMyFileFileName(String myFileFileName) {
		this.myFileFileName = myFileFileName;
	}
	
	public Integer getPropertyReviewId() {
		return propertyReviewId;
	}

	public void setPropertyReviewId(Integer propertyReviewId) {
		this.propertyReviewId = propertyReviewId;
	}
	
	public Integer getPropertyStarCount() {
		return propertyStarCount;
	}

	public void setPropertyStarCount(Integer propertyStarCount) {
		this.propertyStarCount = propertyStarCount;
	}

	public Double getReviewCount() {
		return reviewCount;
	}

	public void setReviewCount(Double reviewCount) {
		this.reviewCount = reviewCount;
	}

	public Double getAverageReview() {
		return averageReview;
	}

	public void setAverageReview(Double averageReview) {
		this.averageReview = averageReview;
	}
	
	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	public Integer getOffset() {
		return offset;
	}

	public void setOffset(Integer offset) {
		this.offset = offset;
	}
	
	public Integer getReviewPropertyId() {
		return reviewPropertyId;
	}

	public void setReviewPropertyId(Integer reviewPropertyId) {
		this.reviewPropertyId = reviewPropertyId;
	}

	public Integer getGuestReviewId() {
		return guestReviewId;
	}

	public void setGuestReviewId(Integer guestReviewId) {
		this.guestReviewId = guestReviewId;
	}
	
	public Integer getReviewerId() {
		return reviewerId;
	}

	public void setReviewerId(Integer reviewerId) {
		this.reviewerId = reviewerId;
	}

	public String getReviewGuest() {
		return reviewGuest;
	}

	public void setReviewGuest(String reviewGuest) {
		this.reviewGuest = reviewGuest;
	}

	public String getReviewDescription() {
		return reviewDescription;
	}

	public void setReviewDescription(String reviewDescription) {
		this.reviewDescription = reviewDescription;
	}
	
	public Integer getGuestReviewsCount() {
		return guestReviewsCount;
	}

	public void setGuestReviewsCount(Integer guestReviewsCount) {
		this.guestReviewsCount = guestReviewsCount;
	}
	
	public Integer getTripadvisorPropertyStarCount() {
		return tripadvisorPropertyStarCount;
	}

	public void setTripadvisorPropertyStarCount(Integer tripadvisorPropertyStarCount) {
		this.tripadvisorPropertyStarCount = tripadvisorPropertyStarCount;
	}

	public Double getTripadvisorReviewCount() {
		return tripadvisorReviewCount;
	}

	public void setTripadvisorReviewCount(Double tripadvisorReviewCount) {
		this.tripadvisorReviewCount = tripadvisorReviewCount;
	}

	public Double getTripadvisorAverageReview() {
		return tripadvisorAverageReview;
	}

	public void setTripadvisorAverageReview(Double tripadvisorAverageReview) {
		this.tripadvisorAverageReview = tripadvisorAverageReview;
	}

	public String getPropertyRating() {
		return propertyRating;
	}

	public void setPropertyRating(String propertyRating) {
		this.propertyRating = propertyRating;
	}

	public String getGoogleReviewUrl() {
		return googleReviewUrl;
	}

	public void setGoogleReviewUrl(String googleReviewUrl) {
		this.googleReviewUrl = googleReviewUrl;
	}

	public String getTripadvisorReviewUrl() {
		return tripadvisorReviewUrl;
	}

	public void setTripadvisorReviewUrl(String tripadvisorReviewUrl) {
		this.tripadvisorReviewUrl = tripadvisorReviewUrl;
	}
}
