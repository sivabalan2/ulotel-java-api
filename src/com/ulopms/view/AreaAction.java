package com.ulopms.view;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;

import com.opensymphony.xwork2.ActionSupport;



import javax.servlet.http.HttpServletRequest;

import com.ulopms.controller.AreaManager;
import com.ulopms.controller.LocationManager;
import com.ulopms.controller.UserLoginManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.Area;
import com.ulopms.model.BookingDetailReport;
import com.ulopms.model.Location;
import com.ulopms.model.User;
import com.ulopms.util.Image;

public class AreaAction extends ActionSupport implements
ServletRequestAware,SessionAware, UserAware{

	private static final long serialVersionUID = 914982665758390091L;
	private Integer areaId;
	private String areaName;
	private String displayAreaName;
	private Integer userId;
	private String areaUrl;
	private String areaRedirectUrl;
	private Integer locationId;	
	private File myFile;
	private String myFileContentType;
	private String myFileFileName;
	private Integer areaLocationId;
	private Integer locationAreaId;
	private Integer limit;
	private Integer offset;
	private List<Area> areaList;
	private List<Location> locationList;
	private String areaDescription;
	private boolean areaIsActive;
	

	
	

	private static final Logger logger = Logger.getLogger(AreaAction.class);

	private HttpSession session;

	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}
	
	private SessionMap<String,Object> sessionMap;  
	  
	//getters and setters  
	  
	@Override  
	public void setSession(Map<String, Object> map) {  
	    sessionMap=(SessionMap)map;  
	}  
	
	private User user;

	
	// @Override
	// public User getModel() {
	// return user;
	// }

	@Override
	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	
	private HttpServletRequest request;

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}
	
	

	public AreaAction() {
		
	

	}

	public String execute() {//.
//this.propertyId= session("")
		//this.patientId = getPatientId();
		
		
		return SUCCESS;
	}

	
	public String getArea() throws IOException {
		this.areaId = (Integer) sessionMap.get("areaId");
		
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			AreaManager areaController = new AreaManager();
			
			response.setContentType("application/json");

			Area area = areaController.find(getAreaId());
			
				
			if (!jsonOutput.equalsIgnoreCase(""))
				jsonOutput += ",{";
			else
				jsonOutput += "{";
			jsonOutput += "\"DT_RowId\":\"" + area.getAreaId()+ "\"";
			jsonOutput += ",\"areaName\":\"" + (area.getAreaName() == null ? "": area.getAreaName().toUpperCase().trim())+ "\"";
			
			jsonOutput += "}";

			

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	public String getFilterSearch() throws IOException {
		this.areaId = (Integer) sessionMap.get("areaId");
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			AreaManager areaController = new AreaManager();
			LocationManager locationController = new LocationManager();
			
	//		UserLoginManager userController=new UserLoginManager();
			
			response.setContentType("application/json");
			int serialno=1;
			
			this.locationList = locationController.list();
			for (Location location : locationList) {
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				String locationName="",strLocationName="";
				if(location.getLocationName()!=null && location.getLocationName().trim()!=""){
					jsonOutput += "\"DT_RowId\":\"" + location.getLocationId()+ "\"";
					locationName=location.getLocationName().toLowerCase().trim();	
					strLocationName=locationName.substring(0, 1).toUpperCase()+locationName.substring(1);
					locationName=strLocationName;
					jsonOutput += ",\"locationName\":\"" + locationName+ "\"";
					jsonOutput += ",\"displayLocationName\":\"" + locationName+ "\"";
					jsonOutput += ",\"locationUrl\":\"" + location.getLocationUrl()+ "\"";
					jsonOutput += ",\"type\":\"" + "Location"+ "\"";
				}
				
				
				jsonOutput += "}";
				
			}
			this.areaList = areaController.list();
			for (Area area : areaList) {
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				
				
				jsonOutput += "\"DT_RowId\":\"" + area.getAreaId()+ "\"";
		//		jsonOutput += ",\"serialNo\":\"" + serialno+ "\"";
				jsonOutput += ",\"locationName\":\"" + (area.getAreaName() == null ? "": area.getAreaName().toUpperCase().trim())+ "\"";
				jsonOutput += ",\"displayLocationName\":\"" + (area.getDisplayAreaName() == null ? "": area.getDisplayAreaName())+ "\"";
				jsonOutput += ",\"locationUrl\":\"" + area.getAreaUrl() + "\"";
				jsonOutput += ",\"type\":\"" + "Area" + "\"";
				if(area.getLocation()!=null){
					jsonOutput += ",\"locationId\":\"" + (area.getLocation().getLocationId() == null ? "": area.getLocation().getLocationId())+ "\"";
					jsonOutput += ",\"areaLocationName\":\"" + (area.getLocation().getLocationName() == null ? "": area.getLocation().getLocationName().toUpperCase().trim())+ "\"";
				}else{						
					jsonOutput += ",\"locationId\":\"" + ""+ "\"";
					jsonOutput += ",\"areaLocationName\":\"" + ""+ "\"";
				}
				
				/*if(area.getCreatedBy()!=null){
					User user=userController.find(area.getCreatedBy());
					String strFirstName=user.getUserName().trim();
					String firstNameLetterUpperCase=strFirstName.substring(0, 1).toUpperCase()+strFirstName.substring(1);
					jsonOutput += ",\"createdBy\":\"" + firstNameLetterUpperCase+ "\"";
				}else{
					jsonOutput += ",\"createdBy\":\"" +" "+ "\"";
				}*/
				
				jsonOutput += "}";

				serialno++;
			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	
	public String getAllArea() throws IOException {
		this.areaId = (Integer) sessionMap.get("areaId");
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			AreaManager areaController=new AreaManager();
			
			response.setContentType("application/json");

			this.areaList = areaController.list();
			for (Area area : areaList) {

				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"DT_RowId\":\"" + area.getAreaId()+ "\"";
				
				jsonOutput += ",\"areaName\":\"" + area.getAreaName().toUpperCase().trim() + "\"";
				jsonOutput += ",\"redirectUrl\":\"" + area.getRedirectUrl()==null?"":area.getRedirectUrl().trim() + "\"";
				
				jsonOutput += "}";


			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	public String getLocationAreas() throws IOException {
		
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			AreaManager areaController=new AreaManager();
			
			response.setContentType("application/json");

			this.areaList = areaController.listLocationArea(getAreaLocationId());
			for (Area area : areaList) {

				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"DT_RowId\":\"" + area.getAreaId()+ "\"";
				
				jsonOutput += ",\"areaName\":\"" + area.getAreaName().toUpperCase().trim() + "\"";
				jsonOutput += ",\"areaDescription\":\"" + (area.getAreaDescription()==null?"":area.getAreaDescription().trim().toUpperCase()) + "\"";
				jsonOutput += ",\"areaUrl\":\"" + area.getAreaUrl().trim() + "\"";
				jsonOutput += "}";


			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	
	public String getAllAreas() throws IOException {
		this.areaId = (Integer) sessionMap.get("areaId");
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			AreaManager areaController=new AreaManager();
			UserLoginManager userController=new UserLoginManager();
			
			response.setContentType("application/json");
			int serialno=1;
			this.areaList = areaController.list(limit,offset);
			for (Area area : areaList) {

				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"DT_RowId\":\"" + area.getAreaId()+ "\"";
				jsonOutput += ",\"serialNo\":\"" + serialno+ "\"";
				jsonOutput += ",\"areaName\":\"" + (area.getAreaName() == null ? "": area.getAreaName().toUpperCase().trim())+ "\"";
				jsonOutput += ",\"displayAreaName\":\"" + (area.getDisplayAreaName() == null ? "": area.getDisplayAreaName())+ "\"";
				if(area.getLocation()!=null){
					jsonOutput += ",\"locationId\":\"" + (area.getLocation().getLocationId() == null ? "": area.getLocation().getLocationId())+ "\"";
					jsonOutput += ",\"areaLocationName\":\"" + (area.getLocation().getLocationName() == null ? "": area.getLocation().getLocationName().toUpperCase().trim())+ "\"";
				}else{
					jsonOutput += ",\"locationId\":\"" + ""+ "\"";
					jsonOutput += ",\"areaLocationName\":\"" + ""+ "\"";
				}
				
				if(area.getCreatedBy()!=null){
					User user=userController.find(area.getCreatedBy());
					String strFirstName=user.getUserName().trim();
					String firstNameLetterUpperCase=strFirstName.substring(0, 1).toUpperCase()+strFirstName.substring(1);
					jsonOutput += ",\"createdBy\":\"" + firstNameLetterUpperCase+ "\"";
				}else{
					jsonOutput += ",\"createdBy\":\"" +" "+ "\"";
				}
				
				jsonOutput += "}";

				serialno++;
			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	
	public String getAllInactiveAreas() throws IOException {
		this.areaId = (Integer) sessionMap.get("areaId");
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			AreaManager areaController=new AreaManager();
			UserLoginManager userController=new UserLoginManager();
			
			response.setContentType("application/json");
			int serialno=1;
			this.areaList = areaController.inactiveList(limit,offset);
			for (Area area : areaList) {

				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"DT_RowId\":\"" + area.getAreaId()+ "\"";
				jsonOutput += ",\"serialNo\":\"" + serialno+ "\"";
				jsonOutput += ",\"areaName\":\"" + (area.getAreaName() == null ? "": area.getAreaName().toUpperCase().trim())+ "\"";
				if(area.getLocation()!=null){
					jsonOutput += ",\"locationId\":\"" + (area.getLocation().getLocationId() == null ? "": area.getLocation().getLocationId())+ "\"";
					jsonOutput += ",\"areaLocationName\":\"" + (area.getLocation().getLocationName() == null ? "": area.getLocation().getLocationName().toUpperCase().trim())+ "\"";
				}else{
					jsonOutput += ",\"locationId\":\"" + ""+ "\"";
					jsonOutput += ",\"areaLocationName\":\"" + ""+ "\"";
				}
				
				if(area.getCreatedBy()!=null){
					User user=userController.find(area.getCreatedBy());
					String strFirstName=user.getUserName().trim();
					String firstNameLetterUpperCase=strFirstName.substring(0, 1).toUpperCase()+strFirstName.substring(1);
					jsonOutput += ",\"createdBy\":\"" + firstNameLetterUpperCase+ "\"";
				}else{
					jsonOutput += ",\"createdBy\":\"" +" "+ "\"";
				}
				
				jsonOutput += "}";

				serialno++;
			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	public String getAreas() throws IOException {
		this.areaId = (Integer) sessionMap.get("areaId");
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			AreaManager areaController=new AreaManager();
			LocationManager locationController = new LocationManager();
			UserLoginManager userController=new UserLoginManager();
			
			response.setContentType("application/json");
			int serialno=1;
			this.locationList = locationController.list();
			for (Location location : locationList) {
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				
				jsonOutput += "\"DT_RowId\":\"" + location.getLocationId()+ "\"";
				jsonOutput += ",\"locationName\":\"" + location.getLocationName()+ "\"";
				jsonOutput += ",\"redirectUrl\":\"" + location.getLocationUrl()+ "\"";
				jsonOutput += ",\"type\":\"" + "Location"+ "\"";
				if(location.getCreatedBy()!=null){
					User user=userController.find(location.getCreatedBy());
					String strFirstName=user.getUserName().trim();
					String firstNameLetterUpperCase=strFirstName.substring(0, 1).toUpperCase()+strFirstName.substring(1);
					jsonOutput += ",\"createdBy\":\"" + firstNameLetterUpperCase+ "\"";
				}else{
					jsonOutput += ",\"createdBy\":\"" + " "+ "\"";
				}
				jsonOutput += "}";
				
			}
			
			this.areaList = areaController.list();
			for (Area area : areaList) {

				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"DT_RowId\":\"" + area.getAreaId()+ "\"";
				jsonOutput += ",\"serialNo\":\"" + serialno+ "\"";
				jsonOutput += ",\"areaName\":\"" + (area.getAreaName() == null ? "": area.getAreaName().toUpperCase().trim())+ "\"";
				jsonOutput += ",\"redirectUrl\":\"" + area.getAreaUrl().trim() + "\"";
				jsonOutput += ",\"locationId\":\"" + (area.getLocation().getLocationId() == null ? "": area.getLocation().getLocationId())+ "\"";
				jsonOutput += ",\"areaLocationName\":\"" + (area.getLocation().getLocationName() == null ? "": area.getLocation().getLocationName().toUpperCase().trim())+ "\"";
				if(area.getCreatedBy()!=null){
					User user=userController.find(area.getCreatedBy());
					String strFirstName=user.getUserName().trim();
					String firstNameLetterUpperCase=strFirstName.substring(0, 1).toUpperCase()+strFirstName.substring(1);
					jsonOutput += ",\"createdBy\":\"" + firstNameLetterUpperCase+ "\"";
				}else{
					jsonOutput += ",\"createdBy\":\"" + " "+ "\"";
				}
				
				jsonOutput += "}";

				serialno++;
			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	public String getLocations() throws IOException {
		try {
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			LocationManager locationController = new LocationManager();
			response.setContentType("application/json");

			List<Location> locationList = locationController.list();
			if(locationList.size()>0 && !locationList.isEmpty()){
				for (Location location : locationList) {
					
					if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += ",{";
					else
						jsonOutput += "{";
					
					jsonOutput += "\"locationId\":\"" + location.getLocationId()+ "\"";
					jsonOutput += ",\"locationName\":\"" + location.getLocationName()+ "\"";
					
					jsonOutput += "}";
				}
			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
		public String editNewArea() throws IOException {
			try {
				this.userId=(Integer) sessionMap.get("userId");
				
				long time = System.currentTimeMillis();
				java.util.Date date = new java.util.Date(time);
				Timestamp tsDate=new Timestamp(date.getTime());
				
				AreaManager areaController = new AreaManager();
				LocationManager locationController=new LocationManager();
				Area area = areaController.find(getLocationAreaId());
				area.setIsActive(getAreaIsActive());
				area.setIsDeleted(false);
				area.setAreaName(getAreaName());
				area.setDisplayAreaName(getDisplayAreaName());
				area.setAreaDescription(getAreaDescription());
				area.setAreaUrl(getAreaUrl());
				area.setRedirectUrl(getAreaRedirectUrl());
				area.setCreatedBy(this.userId);
				area.setCreatedOn(tsDate);
				Location location=locationController.find(getAreaLocationId());
				area.setLocation(location);
				areaController.edit(area);
				
			} catch (Exception e) {
				logger.error(e);
				e.printStackTrace();
			} finally {

			}
			return SUCCESS;
		}
	public String getEditArea() throws IOException {
		try {
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			AreaManager areaController = new AreaManager();
			response.setContentType("application/json");
			this.areaList = areaController.list(getAreaId());
			for (Area area : areaList) {
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				
				jsonOutput += "\"areaId\":\"" + area.getAreaId()+ "\"";
				jsonOutput += ",\"areaName\":\"" + area.getAreaName()+ "\"";
				jsonOutput += ",\"displayAreaName\":\"" + area.getDisplayAreaName()+ "\"";
				jsonOutput += ",\"areaDescription\":\"" + area.getAreaDescription()+ "\"";
				jsonOutput += ",\"areaUrl\":\"" + area.getAreaUrl()+ "\"";
				jsonOutput += ",\"areaLocationId\":\"" + area.getLocation().getLocationId()+ "\"";
				jsonOutput += ",\"photoPath\":\"" + area.getAreaThumbPath()+ "\"";
				
				jsonOutput += "}";

			}
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	public String getEditInactiveArea() throws IOException {
		try {
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			AreaManager areaController = new AreaManager();
			response.setContentType("application/json");
			this.areaList = areaController.inactiveList(getAreaId());
			for (Area area : areaList) {
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				
				jsonOutput += "\"areaId\":\"" + area.getAreaId()+ "\"";
				jsonOutput += ",\"areaName\":\"" + area.getAreaName()+ "\"";
				jsonOutput += ",\"areaDescription\":\"" + area.getAreaDescription()+ "\"";
				jsonOutput += ",\"areaUrl\":\"" + area.getAreaUrl()+ "\"";
				jsonOutput += ",\"redirectUrl\":\"" + (area.getRedirectUrl() == null ? "": area.getRedirectUrl().trim())+ "\"";
				jsonOutput += ",\"areaLocationId\":\"" + area.getLocation().getLocationId()+ "\"";
				jsonOutput += ",\"photoPath\":\"" + area.getAreaThumbPath()+ "\"";
				
				jsonOutput += "}";

			}
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}

	
	public String addAreaThumbPicture() throws IOException {
		try {
			AreaManager areaController = new AreaManager();
			File fileZip1 = new File(getText("storage.areathumb.photo") + "/"
					+ getLocationAreaId() + "/", this.getMyFileFileName());
			FileUtils.copyFile(this.myFile, fileZip1);// copying image in the
			Area area = areaController.find(getLocationAreaId()); 
			area.setAreaThumbPath(this.getMyFileFileName());
			areaController.edit(area);
			
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return SUCCESS;
	}
	public String getAreaThumbPicture() {
		
		HttpServletResponse response = ServletActionContext.getResponse();

		AreaManager areaController = new AreaManager();
		Area area= areaController.find(getAreaId());
		String imagePath = "";
		// response.setContentType("");
		try {
			
			if ( area.getAreaThumbPath() != null) {
				imagePath = getText("storage.areathumb.photo") + "/"
						+ area.getAreaId()+ "/"
						+ area.getAreaThumbPath();
				Image img = new Image();
				
				response.getOutputStream().write(img.getCustomImageInBytes(imagePath));
				response.getOutputStream().flush();
			} else {
				imagePath = getText("storage.areathumb.photo") + "/emptyprofilepic.png";
				Image img = new Image();
				
				response.getOutputStream().write(img.getCustomImageInBytes(imagePath));
				response.getOutputStream().flush();
			}
		} catch (Exception e1) {
			logger.error(e1);
		} finally {

		}
		return null;

	}

	public String addAreaPicture() throws IOException {
		try {
			AreaManager areaController = new AreaManager();
			File fileZip1 = new File(getText("storage.area.photo") + "/"
					+ getAreaId() + "/", this.getMyFileFileName());
			FileUtils.copyFile(this.myFile, fileZip1);// copying image in the
			Area area = areaController.find(getAreaId()); 
			area.setAreaThumbPath(this.getMyFileFileName());
			areaController.edit(area);
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return SUCCESS;
	}
	
	public String getAreaPicture() {
		
		HttpServletResponse response = ServletActionContext.getResponse();
		AreaManager areaController = new AreaManager();
		Area area = areaController.find(getAreaId()); 
		
		
		String imagePath = "";
		// response.setContentType("");
		try {
			
			if (getAreaId() >0 ) {
				imagePath = getText("storage.area.photo") + "/"
						+ area.getAreaId()+ "/"
						+ area.getAreaThumbPath();
				Image im = new Image();
				response.getOutputStream().write(im.getCustomImageInBytes(imagePath));
				response.getOutputStream().flush();
			} else {
				imagePath = getText("storage.path") + "/emptyprofilepic.png";
				Image im = new Image();
				response.getOutputStream().write(
						im.getCustomImageInBytes(imagePath));
				response.getOutputStream().flush();
			}
		} catch (Exception e1) {
			logger.error(e1);
			e1.printStackTrace();
		} finally {

		}
		return null;

	}
	
	
	public String addNewArea() {
		this.areaId = (Integer) sessionMap.get("areaId");
		AreaManager areaController = new AreaManager();
		LocationManager locationController=new LocationManager();
		this.userId=(Integer) sessionMap.get("userId");
		try {
			
			Area area=new Area();
			long time = System.currentTimeMillis();
			java.util.Date date = new java.util.Date(time);
			Timestamp tsDate=new Timestamp(date.getTime());  
			
			area.setIsActive(true);
			area.setIsDeleted(false);
			area.setAreaName(getAreaName());
			area.setDisplayAreaName(getDisplayAreaName());
			area.setAreaDescription(getAreaDescription());
			area.setCreatedBy(this.userId);
			area.setCreatedOn(tsDate);
			area.setAreaUrl(getAreaUrl());
			Location location=locationController.find(getAreaLocationId());
			area.setLocation(location);
			Area areas = areaController.add(area);
			
			this.areaId = areas.getAreaId();
			
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("application/json");
			
			  if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"locationAreaId\":\"" + this.areaId  + "\"";
				
				jsonOutput += "}";


			

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}

	
	
	
	

	public String deleteArea() {
		AreaManager areaController = new AreaManager();
		try {
			Area area= areaController.find(getAreaId());
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			area.setIsActive(false);
			area.setIsDeleted(true);
			area.setModifiedOn(new java.sql.Timestamp(date.getTime()));
			areaController.edit(area);
			
			
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}
	
	public String getAreaCount() throws IOException {
 		try {
 			String jsonOutput = "";
 			HttpServletResponse response = ServletActionContext.getResponse();
 			response.setContentType("application/json");
 			AreaManager areaController = new AreaManager();
 			List<Area> listAreaCount =areaController.list();
 			if(listAreaCount.size()>0 && !listAreaCount.isEmpty()){
 				if (!jsonOutput.equalsIgnoreCase(""))
 					jsonOutput += ",{";
 				else
 					jsonOutput += "{";
 				
 			    jsonOutput += "\"count\":\"" + listAreaCount.size() + "\"";
 			    
 				jsonOutput += "}";
 			}
 			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
 		} catch (Exception e) {
 			logger.error(e);
 			e.printStackTrace();
 		} finally {

 		}

 		return null;

 	}
	
	public String getInactiveAreaCount() throws IOException {
 		try {
 			String jsonOutput = "";
 			HttpServletResponse response = ServletActionContext.getResponse();
 			response.setContentType("application/json");
 			AreaManager areaController = new AreaManager();
 			List<Area> listAreaCount =areaController.inactiveList();
 			if(listAreaCount.size()>0 && !listAreaCount.isEmpty()){
 				if (!jsonOutput.equalsIgnoreCase(""))
 					jsonOutput += ",{";
 				else
 					jsonOutput += "{";
 				
 			    jsonOutput += "\"count\":\"" + listAreaCount.size() + "\"";
 			    
 				jsonOutput += "}";
 			}
 			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
 		} catch (Exception e) {
 			logger.error(e);
 			e.printStackTrace();
 		} finally {

 		}

 		return null;

 	}
	public String getAreaProperty(){
		try{
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			AreaManager areaController=new AreaManager();
			
			response.setContentType("application/json");
			
			List<BookingDetailReport> listPropertyArea=null;
			listPropertyArea=areaController.listAreaProperty(getAreaId());
			if(listPropertyArea.size()>0 && !listPropertyArea.isEmpty()){
				for(BookingDetailReport propertyArea:listPropertyArea){

					if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += ",{";
					else
						jsonOutput += "{";
					jsonOutput += "\"propertyId\":\"" + propertyArea.getPropertyId()+ "\"";
					
					jsonOutput += ",\"propertyName\":\"" + propertyArea.getPropertyName().toUpperCase().trim() + "\"";
					
					jsonOutput += "}";
				}
			}
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
		}catch(Exception e){
			logger.error(e);
			e.printStackTrace();
		}
		return null;
	}

	
	public Integer getAreaId() {
		return areaId;
	}

	public void setAreaId(Integer areaId) {
		this.areaId = areaId;
	}

	public Integer getLocationId() {
		return locationId;
	}

	public void setLocationId(Integer locationId) {
		this.locationId = locationId;
	}

	
	public File getMyFile() {
		return myFile;
	}

	public void setMyFile(File myFile) {
		this.myFile = myFile;
	}

	public String getMyFileContentType() {
		return myFileContentType;
	}

	public void setMyFileContentType(String myFileContentType) {
		this.myFileContentType = myFileContentType;
	}

	public String getMyFileFileName() {
		return myFileFileName;
	}

	public void setMyFileFileName(String myFileFileName) {
		this.myFileFileName = myFileFileName;
	}
	public Integer getAreaLocationId() {
		return areaLocationId;
	}

	public void setAreaLocationId(Integer areaLocationId) {
		this.areaLocationId = areaLocationId;
	}
	public Integer getLocationAreaId() {
		return locationAreaId;
	}

	public void setLocationAreaId(Integer locationAreaId) {
		this.locationAreaId = locationAreaId;
	}
	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	public Integer getOffset() {
		return offset;
	}

	public void setOffset(Integer offset) {
		this.offset = offset;
	}
	
	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}
	
	public String getDisplayAreaName() {
		return displayAreaName;
	}

	public void setDisplayAreaName(String displayAreaName) {
		this.displayAreaName = displayAreaName;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	
	public String getAreaUrl() {
		return areaUrl;
	}

	public void setAreaUrl(String areaUrl) {
		this.areaUrl = areaUrl;
	}
	
	public String getAreaDescription() {
		return areaDescription;
	}

	public void setAreaDescription(String areaDescription) {
		this.areaDescription = areaDescription;
	}
	
	public String getAreaRedirectUrl() {
		return areaRedirectUrl;
	}

	public void setAreaRedirectUrl(String areaRedirectUrl) {
		this.areaRedirectUrl = areaRedirectUrl;
	}
	
	public Boolean getAreaIsActive() {
		return areaIsActive;
	}

	public void setAreaIsActive(Boolean areaIsActive) {
		this.areaIsActive = areaIsActive;
	}

	
}
