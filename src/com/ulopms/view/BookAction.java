package com.ulopms.view;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;




//import com.medwecare.controller.AwsObjectManager;
//import com.medwecare.controller.CommentManager;
//import com.medwecare.controller.CommentUserManager;
////import com.medwecare.controller.NotificationManager;
//import com.medwecare.controller.ProjectManager;

//import com.medwecare.model.AwsObject;
//import com.medwecare.model.Notification;
//import com.medwecare.model.ObjectComment;
//import com.medwecare.model.ObjectCommentUser;
//import com.medwecare.model.Project;
















import com.opensymphony.xwork2.ActionSupport;

import freemarker.template.Configuration;
import freemarker.template.Template;

import javax.servlet.http.HttpServletRequest;

import com.opensymphony.xwork2.ModelDriven;
import com.ulopms.controller.PmsBookingManager;
import com.ulopms.controller.PmsPropertyManager;
import com.ulopms.controller.PropertyAccommodationManager;
import com.ulopms.controller.UserLoginManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.PmsBooking;
import com.ulopms.model.PmsAvailableRooms;
import com.ulopms.model.PmsProperty;
import com.ulopms.model.PropertyAccommodation;
import com.ulopms.model.User;
import com.ulopms.util.Email;
import com.ulopms.util.Image;

public class BookAction extends ActionSupport implements
ServletRequestAware, SessionAware, UserAware{

	private static final long serialVersionUID = 914982665758390091L;

	private Integer bookingId;
	private Timestamp arrivalDate;
	private Timestamp departureDate;
	private Integer rooms;
	private Long roomCount;
	private File myFile;
	private String myFileContentType;
	private String myFileFileName;
	private double totalTax;
	private double securityDeposit;
	public Integer propertyId;
	
	
	private PmsBooking booking;
	
	//TimeZone timeZone = TimeZone.getDefault();
	
	private List<PmsBooking> bookingList;
	
	private List<PmsAvailableRooms> availableList;
	
	private List<PropertyAccommodation> accommodationList;

	private static final Logger logger = Logger.getLogger(BookAction.class);

	private HttpSession session;

	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}
	
	private SessionMap<String,Object> sessionMap;  
	  
	//getters and setters  
	  
	@Override  
	public void setSession(Map<String, Object> map) {  
	    sessionMap=(SessionMap)map;  
	}  
	
	
	private User user;

	
	// @Override
	// public User getModel() {
	// return user;
	// }

	@Override
	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	
	private HttpServletRequest request;

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	public BookAction() {

	}

	public String execute() {

		
		return SUCCESS;
	}

	
	
   public String getAvailability() throws IOException {
		
	   this.bookingId = (Integer) sessionMap.get("bookingId");
		
		
		
	   // String timezone ="+05:30";
	     
	   //String arrival = getArrivalDate().toString();
	   //arrival = arrival.substring(0, arrival.indexOf('.'));
	
	    Timestamp timestamp = new Timestamp(System.currentTimeMillis());
	    
	    
		this.propertyId = (Integer) sessionMap.get("propertyId");
		
		
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
			PmsBookingManager bookingController = new PmsBookingManager();
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");
			this.availableList = bookingController.list(getPropertyId(),getArrivalDate(),getDepartureDate());
			
            for (PmsAvailableRooms available : availableList) {
            
				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				
				else
					jsonOutput += "{";
				//jsonOutput += "\"availableId\":\"" + availableId+ "\"";
				jsonOutput += "\"propertyId\":\"" + available.getPropertyId() + "\"";
				jsonOutput += ",\"baseAmount\":\"" + available.getBaseAmount() + "\"";
				jsonOutput += ",\"arrivalDate\":\"" + getArrivalDate()+ "\"";
				jsonOutput += ",\"departureDate\":\"" + getDepartureDate()+ "\"";
				jsonOutput += ",\"roomCount\":\"" + available.getRoomCount() + "\"";
				jsonOutput += ",\"accommodationId\":\"" + available.getAccommodationId() + "\"";
				jsonOutput += ",\"accommodationType\":\"" + available.getAccommodationType() + "\"";
				jsonOutput += ",\"maxOccupancy\":\"" + available.getMaxOccupancy() + "\"";
				//jsonOutput += ",\"adultsIncludedRate\":\"" + available.getAdultsIncludedRate() + "\"";
				//jsonOutput += ",\"childIncludedRate\":\"" + available.getChildIncludedRate() + "\"";
				jsonOutput += ",\"extraChild\":\"" + available.getExtraChild() + "\"";
				jsonOutput += ",\"extraAdult\":\"" + available.getExtraAdult() + "\"";
				jsonOutput += ",\"available\":\"" + (available.getAvailable() == null ? available.getNoOfUnits() : available.getAvailable()) + "\"";
				jsonOutput += "}";
				
				

			}
            
			

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	public String getBookings() throws IOException {
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PmsBookingManager bookingController = new PmsBookingManager();
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");

			PmsBooking booking = bookingController.find(getBookingId());
			
				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"DT_RowId\":\"" + booking.getBookingId() + "\"";
				jsonOutput += ",\"arrivalDate\":\"" + booking.getArrivalDate()+ "\"";
				jsonOutput += ",\"departureDate\":\"" + booking.getDepartureDate()+ "\"";
				jsonOutput += ",\"rooms\":\"" +booking.getRooms()+ "\"";
				jsonOutput += ",\"securityDeposit\":\"" + booking.getSecurityDeposit() + "\"";
				jsonOutput += ",\"totalAmount\":\"" + booking.getTotalAmount() + "\"";
				jsonOutput += ",\"totalRefund\":\"" + booking.getTotalRefund() + "\"";
				jsonOutput += ",\"totalTax\":\"" + booking.getTotalTax() + "\"";
				
				jsonOutput += "}";

			

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	public String getAllBookings() throws IOException {
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PmsBookingManager bookingController = new PmsBookingManager();
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");

			 this.bookingList = bookingController.list(getPropertyId());
			for (PmsBooking booking : bookingList) {

				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"DT_RowId\":\"" + booking.getBookingId() + "\"";
				jsonOutput += ",\"arrivalDate\":\"" + booking.getArrivalDate()+ "\"";
				jsonOutput += ",\"departureDate\":\"" + booking.getDepartureDate()+ "\"";
				jsonOutput += ",\"rooms\":\"" +booking.getRooms()+ "\"";
				jsonOutput += ",\"securityDeposit\":\"" + booking.getSecurityDeposit() + "\"";
				jsonOutput += ",\"totalAmount\":\"" + booking.getTotalAmount() + "\"";
				jsonOutput += ",\"totalRefund\":\"" + booking.getTotalRefund() + "\"";
				jsonOutput += ",\"totalTax\":\"" + booking.getTotalTax() + "\"";
				
				jsonOutput += "}";

			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}

	public String addBooking() {
		PmsBookingManager bookingController = new PmsBookingManager();
		PmsPropertyManager propertyController = new PmsPropertyManager();
		//UserLoginManager  userController = new UserLoginManager();
		
		this.propertyId = (Integer) sessionMap.get("propertyId");
		
		try {
			
						
			PmsBooking booking = new PmsBooking();
			
			
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			booking.setArrivalDate(getArrivalDate());
			booking.setDepartureDate(getDepartureDate());
			booking.setTotalTax(getTotalTax());
			booking.setSecurityDeposit(getSecurityDeposit());
			booking.setIsActive(true);
			booking.setIsDeleted(false);
			booking.setRooms(rooms);
			PmsProperty property = propertyController.find(getPropertyId());
			booking.setPmsProperty(property);
			
			PmsBooking book = bookingController.add(booking);
			
			this.bookingId = book.getBookingId();
			sessionMap.put("bookingId",bookingId);
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return SUCCESS;
	}
	
	public String editBooking() {
		PmsBookingManager bookingController = new PmsBookingManager();
		UserLoginManager  userController = new UserLoginManager();
		try {
			
						
			PmsBooking guest = bookingController.find(getBookingId());
			
			
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			booking.setArrivalDate(getArrivalDate());
			booking.setDepartureDate(getDepartureDate());
			booking.setIsActive(true);
			booking.setIsDeleted(false);
			booking.setRooms(rooms);
			
			bookingController.edit(booking);
		
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return SUCCESS;
	}
	
	public String deleteBooking() {
		PmsBookingManager bookingController = new PmsBookingManager();
		UserLoginManager  userController = new UserLoginManager();
		try {
			
						
			PmsBooking booking = bookingController.find(getBookingId());
						
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			booking.setIsActive(false);
			booking.setIsDeleted(true);
			booking.setModifiedDate(new java.sql.Timestamp(date.getTime()));
			bookingController.edit(booking);
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}
	
	public Integer getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(Integer propertyId) {
		this.propertyId = propertyId;
	}
	
	public File getMyFile() {
		return myFile;
	}

	public void setMyFile(File myFile) {
		this.myFile = myFile;
	}

	public String getMyFileContentType() {
		return myFileContentType;
	}

	public void setMyFileContentType(String myFileContentType) {
		this.myFileContentType = myFileContentType;
	}

	public String getMyFileFileName() {
		return myFileFileName;
	}

	public void setMyFileFileName(String myFileFileName) {
		this.myFileFileName = myFileFileName;
	}
	
	public Integer getBookingId() {
		return bookingId;
	}

	public void setBookingId(Integer bookingId) {
		this.bookingId = bookingId;
	}

	public Timestamp getArrivalDate() {
		return arrivalDate;
	}

	public void setArrivalDate(Timestamp arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	public Timestamp getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Timestamp departureDate) {
		this.departureDate = departureDate;
	}

	public Long getRoomCount() {
		return roomCount;
	}

	public void setRoomCount(Long roomCount) {
		this.roomCount = roomCount;
	}
	
	public Integer getRooms() {
		return rooms;
	}

	public void setRooms(Integer rooms) {
		this.rooms = rooms;
	}
    
	public double getTotalTax() {
		return totalTax;
	}

	public void setTotalTax(double totalTax) {
		this.totalTax = totalTax;
	}
	
	public double getSecurityDeposit() {
		return securityDeposit;
	}

	public void setSecurityDeposit(double securityDeposit) {
		this.securityDeposit = securityDeposit;
	}
	
	public PmsBooking getBooking() {
		return booking;
	}

	public void setBooking(PmsBooking booking) {
		this.booking = booking;
	}

	public List<PmsBooking> getBookingList() {
		return bookingList;
	}

	public void setBookingList(List<PmsBooking> bookingList) {
		this.bookingList = bookingList;
	}
	
	public List<PropertyAccommodation> getAccommodationList() {
		return accommodationList;
	}

	public void setAccomadationList(List<PropertyAccommodation> accommodationList) {
		this.accommodationList = accommodationList;
	}
	
	public List<PmsAvailableRooms> getAvailableList() {
		return availableList;
	}

	public void setAvailableList(List<PmsAvailableRooms> availableList) {
		this.availableList = availableList;
	}


}
