package com.ulopms.view;

import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;

//import com.whl4.controller.CompanyManager;

//import com.medwecare.controller.CompanyManager;
//import com.medwecare.controller.RoleManager;

//import com.medwecare.controller.UserRightsManager;

//import com.medwecare.model.AccessRights;
//import com.medwecare.model.Company;
//import com.medwecare.model.ObjectComment;
//import com.medwecare.model.ObjectTags;
//import com.medwecare.model.Role;





import org.jsoup.select.Evaluator.IsEmpty;

//import com.medwecare.model.UserAccessRights;
//import com.whl4.model.Company;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.ulopms.controller.BookingGuestDetailManager;
import com.ulopms.controller.RewardsManager;
import com.ulopms.controller.UserLoginManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.BookingGuestDetail;
import com.ulopms.model.RewardUserDetails;
import com.ulopms.model.User;

public class UserAdminAction extends ActionSupport implements ServletRequestAware, UserAware,
ModelDriven<User> {

	// private static final long serialVersionUID = 9149826260758390091L;
	private String oper;
	private User user = new User();
	private List<User> userList;
	private List<BookingGuestDetail> bookingGuestDetailList;
	//private List<Role> roleList;
	// private List<Company> companyList;

	private String titleHeader;
	
	private int auserId;
	private String auserName;
	private String aloginName;
	private String apassword;
	private String aemailId;
	private String aphone;
	private Long acompanyId;
	private short aroleId;
	private HttpSession session;
	
	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}

	private String logoFileName;

	public String getLogoFileName() {
		return logoFileName;
	}

	public void setLogoFileName(String logoFileName) {
		this.logoFileName = logoFileName;
	}
	public String getTitleHeader() {
		return titleHeader;
	}

	public void setTitleHeader(String titleHeader) {
		this.titleHeader = titleHeader;
	}

	private static final Logger logger = Logger
			.getLogger(UserAdminAction.class);

	@Override
	public User getModel() {
		return user;
	}

	private HttpServletRequest request;

	private UserLoginManager linkController;
	// private CompanyManager companyController = new CompanyManager();
	//private RoleManager roleController = new RoleManager();

	public UserAdminAction() {
		linkController = new UserLoginManager();
	}

	public String getSingleUser() {
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();

			response.setContentType("application/json");
			// response.getWriter().write("[{\"start\":\"2015-05-11T00:00:00\",\"title\":\"test\"}]");
            
			
			User usr = linkController.findUser(getAuserId());

			String accessRights ="";
			String displayname="";
			
			//Set<UserAccessRights> uaHset = usr.getUserAccessRightses();
			//Iterator<UserAccessRights> t = uaHset.iterator();
			//while (t.hasNext()) {
				//UserAccessRights uar = t.next();
				//AccessRights ar =  uar.getAccessRights();
			//	if (!accessRights.equalsIgnoreCase(""))
			//							accessRights = accessRights + ",\"" + ar.getDisplayName() + "\":\"" +uar.getAccessRightsPermission()	+ "\"";
			//	else
			//		accessRights = "\"" + ar.getDisplayName() + "\":\"" +uar.getAccessRightsPermission()	+ "\"";
				
				/*if (!accessRights.equalsIgnoreCase(""))
					displayname = displayname + ",\"" + ar.getDisplayName() + "\"";
				else
					displayname = "\"" + ar.getDisplayName() + "\"";
*/
			//}
			
			/*for (UserAccessRights objRights : uaHset) {
				if (!accessRights.equalsIgnoreCase(""))
					accessRights += ",{";
				else
					accessRights += "{";
				accessRights += "\"displayName\":\"" + objRights.getAccessRights().getDisplayName()
						+ "\"";
				accessRights += "}";

			
			}*/
			
			if(usr!=null){
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"DT_RowId\":\"" + (usr.getUserId()==null ? "": usr.getUserId()) + "\"";
				jsonOutput += ",\"o_username\":\"" + usr.getUserName() + "\"";
				jsonOutput += ",\"o_email\":\"" + usr.getEmailId() + "\"";
				jsonOutput += ",\"o_email1\":\"" + (usr.getEmailId1()==null ? "": usr.getEmailId1()) + "\"";
				jsonOutput += ",\"o_phone\":\"" + usr.getPhone() + "\"";
				jsonOutput += ",\"o_phone1\":\"" + (usr.getPhone1()==null ? "": usr.getPhone1()) + "\"";
				jsonOutput += ",\"o_address1\":\"" + (usr.getAddress1()==null ? "": usr.getAddress1()) + "\"";
				jsonOutput += ",\"o_address2\":\"" + (usr.getAddress2()==null ? "": usr.getAddress2()) + "\"";
				jsonOutput += ",\"o_address3\":\"" + (usr.getAddress3()==null ? "": usr.getAddress3()) + "\"";
				jsonOutput += ",\"travelerType\":\"" + (usr.getTravelerType()==null ? "": usr.getTravelerType()) + "\"";
				//jsonOutput += ",\"o_jobtitle\":\"" + usr.getj= + "\"";
				jsonOutput += ",\"o_profilepic\":\"" + usr.getImgPath()+ "\"";
				jsonOutput += ",\"o_active\":\"" + usr.getIsActive()+ "\"";
				if (!accessRights.equalsIgnoreCase(""))
								jsonOutput += ",\"accessRights\":[" + accessRights + "]";
				Integer usedPoints = 0; 
				BookingGuestDetailManager bookingGuestController = new BookingGuestDetailManager();
				this.bookingGuestDetailList = bookingGuestController.bookingRewardList(usr.getEmailId());
				 
				response.setContentType("application/json");
				 for(BookingGuestDetail detail : bookingGuestDetailList){
					 if(detail.getUsedRewardPoints() == null){
						 usedPoints = 0;
					break;
					 }
					 else{
						 usedPoints += (detail.getUsedRewardPoints());
					 }
				 }
				 /*for (int i = 0; i<bookingIds.split(",").length; i++) 
			      {
					
				BookingGuestDetail detail =  bookingGuestController.find(Integer.parseInt(bookingIds.split(",")[i].trim()));	
			      }*/
				
				RewardsManager rewardsController = new RewardsManager();
				RewardUserDetails rewardUserDetail = rewardsController.findUserId(usr.getUserId());
				Integer rewardPoint = 0;
				if(rewardUserDetail == null){
					rewardPoint = 0;
				}
				else{
					rewardPoint = rewardUserDetail.getUsedRewardPoints();
					jsonOutput += ",\"rewardUserDetailsId\":\"" + rewardUserDetail.getRewardUserDetailId()+ "\"";
				}
				jsonOutput += ",\"rewardPoints\":\"" + (rewardPoint - usedPoints)+ "\"";
				
				
				jsonOutput += "}";
			}
			

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}

	public String getUsers() {
		//UserRightsManager userRightsController = new UserRightsManager();
		//CompanyManager companyController = new CompanyManager();
		//RoleManager roleController = new RoleManager();
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();

			response.setContentType("application/json");
			// response.getWriter().write("[{\"start\":\"2015-05-11T00:00:00\",\"title\":\"test\"}]");

			// this.setArea(a);
		} catch (Exception e) {
			logger.error(e);
		} finally {

		}
		return null;
	}

	public String getUsersByRole() {
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();

			response.setContentType("application/json");
			// response.getWriter().write("[{\"start\":\"2015-05-11T00:00:00\",\"title\":\"test\"}]");

			
			List<User> userList = linkController.list(request.getParameter("roleName"));

			for (User u : userList) {

				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"DT_RowId\":\"" + u.getUserId() + "\"";
				jsonOutput += ",\"o_username\":\"" + u.getUserName() + "\"";
				jsonOutput += ",\"o_email\":\"" + u.getEmailId()+ "\"";
			//	jsonOutput += ",\"o_jobtitle\":\"" + u.get + "\"";
				jsonOutput += ",\"o_profilepic\":\"" + u.getImgPath()
						+ "\"";
				jsonOutput += "}";

			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");

			// this.setArea(a);
		} catch (Exception e) {
			//logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}

	public String execute() throws Exception {

	
		this.session = request.getSession();
		
		this.titleHeader = "USER MASTER";
		return SUCCESS;
	}

	public String UserAdminList() {
		try {
			this.userList = linkController.listAll();
		} catch (Exception e) {
			logger.error(e);
		} finally {

		}
		return SUCCESS;
	}

	public String UserAdd() {
		try {
			
		} catch (Exception e) {
			logger.error(e);
		} finally {

		}
		return SUCCESS;
	}

	@Override
	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	public List<User> getUserList() {
		return userList;
	}

	public void setUserList(List<User> usersList) {
		this.userList = usersList;
	}

	public int getAuserId() {
		return auserId;
	}

	public void setAuserId(int auserId) {
		this.auserId = auserId;
	}

	public String getAuserName() {
		return auserName;
	}

	public void setAuserName(String auserName) {
		this.auserName = auserName;
	}

	public String getAloginName() {
		return aloginName;
	}

	public void setAloginName(String aloginName) {
		this.aloginName = aloginName;
	}

	public String getApassword() {
		return apassword;
	}

	public void setApassword(String apassword) {
		this.apassword = apassword;
	}

	public String getAemailId() {
		return aemailId;
	}

	public void setAemailId(String aemailId) {
		this.aemailId = aemailId;
	}

	public String getAphone() {
		return aphone;
	}

	public void setAphone(String aphone) {
		this.aphone = aphone;
	}

	public Long getAcompanyId() {
		return acompanyId;
	}

	public void setAcompanyId(Long acompanyId) {
		this.acompanyId = acompanyId;
	}

	public short getAroleId() {
		return aroleId;
	}

	public void setAroleId(short aroleId) {
		this.aroleId = aroleId;
	}

	public void setOper(String oper) {
		this.oper = oper;
	}

	public String getOper() {
		return oper;
	}
/*
	public List<Role> getRoleList() {
		return roleList;
	}

	public void setRoleList(List<Role> roleList) {
		this.roleList = roleList;
	}
	*/
	/*
	 * public List<Company> getCompanyList() { return companyList; }
	 * 
	 * 
	 * public void setCompanyList(List<Company> companyList) { this.companyList
	 * = companyList; }
	 */

	@Override
	public void setServletRequest(HttpServletRequest request) {
		// TODO Auto-generated method stub
		this.request = request;
	}
	
	public  String  dconvert(java.util.Date dformat){
		 String disdate="";
	    try { 
	    SimpleDateFormat  dateFormat = new SimpleDateFormat("dd-MMM-YYYY");
	               
	               
	                 disdate=dateFormat.format(dformat).replace("-"," ");
	                
	                }catch(Exception e)
	                {
	                                e.printStackTrace();
	                }
	    return disdate;
	}



public String dconverttime(java.util.Date input)
	{
		String disdatetime="";
		try { 
			DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		DateFormat outputFormat1 = new SimpleDateFormat("KK:mm a dd-MMM-yyyy");
		disdatetime=outputFormat1.format(input);
		}catch(Exception e)
       {
                       e.printStackTrace();
       }
		return disdatetime.replace("-", " ");
	}

}
