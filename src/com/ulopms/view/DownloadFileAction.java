package com.ulopms.view;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.activation.MimetypesFileTypeMap;
import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.ServletRequestAware;




//import com.medwecare.controller.AwsObjectManager;
//import com.medwecare.controller.CommentManager;
//import com.medwecare.controller.CommentUserManager;
////import com.medwecare.controller.NotificationManager;
//import com.medwecare.controller.ProjectManager;

//import com.medwecare.model.AwsObject;
//import com.medwecare.model.Notification;
//import com.medwecare.model.ObjectComment;
//import com.medwecare.model.ObjectCommentUser;
//import com.medwecare.model.Project;
















import org.apache.struts2.interceptor.SessionAware;

import com.lowagie.text.pdf.codec.Base64.InputStream;
import com.opensymphony.xwork2.ActionSupport;

import freemarker.template.Configuration;
import freemarker.template.Template;

import javax.servlet.http.HttpServletRequest;

import com.opensymphony.xwork2.ModelDriven;
import com.sun.glass.ui.Application;
import com.ulopms.controller.BookingDetailManager;
import com.ulopms.controller.BookingGuestDetailManager;
import com.ulopms.controller.PmsBookingManager;
import com.ulopms.controller.PmsGuestManager;
import com.ulopms.controller.PmsPropertyManager;
import com.ulopms.controller.PmsSourceManager;
import com.ulopms.controller.PromotionDetailManager;
import com.ulopms.controller.PropertyAccommodationManager;
import com.ulopms.controller.ReportManager;
import com.ulopms.controller.StateManager;
import com.ulopms.controller.UserLoginManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.BookingDetail;
import com.ulopms.model.BookingDetailReport;
import com.ulopms.model.BookingGuestDetail;
import com.ulopms.model.DashBoard;
import com.ulopms.model.PmsBooking;
import com.ulopms.model.PmsGuest;
import com.ulopms.model.PmsPromotionDetails;
import com.ulopms.model.PmsProperty;
import com.ulopms.model.PmsSource;
import com.ulopms.model.PropertyAccommodation;
import com.ulopms.model.Revenue;
import com.ulopms.model.State;
import com.ulopms.model.User;
import com.ulopms.model.UserGuestDetailReport;
import com.ulopms.util.Email;
import com.ulopms.util.Image;

public class DownloadFileAction extends ActionSupport implements
ServletRequestAware, SessionAware, UserAware{

	private static final long serialVersionUID = 914982665758390091L;

	
	private InputStream inputStream;
	private String fileName;
	private long contentLength;
	private String fileExcelName;
	
	

	private HttpSession session;

	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}

	private static final Logger logger = Logger.getLogger(DownloadFileAction.class);

	
	private User user;

	
	// @Override
	// public User getModel() {
	// return user;
	// }

	@Override
	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}
	private SessionMap<String,Object> sessionMap;  
	  
	//getters and setters  
	  
	@SuppressWarnings("unchecked")
	@Override
	public void setSession(Map<String, Object> map) {
		// TODO Auto-generated method stub
		 sessionMap=(SessionMap)map;  
	}
	

	
	private HttpServletRequest request;

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	public DownloadFileAction() {

	}

	public String execute() {
		try{
			String myHomePath= System.getProperty("user.home");
			String strFilePath=myHomePath+"\\Downloads\\"+fileExcelName;
			File fileToDownload = new File(strFilePath);
		    inputStream = new InputStream(new FileInputStream(fileToDownload));
		    fileName = fileToDownload.getName();
		    contentLength = fileToDownload.length();
		    HttpServletResponse response = ServletActionContext.getResponse();
		    MimetypesFileTypeMap mimeTypesMap = new MimetypesFileTypeMap();
		    String mimeType = mimeTypesMap.getContentType(strFilePath);
	        if (mimeType == null) {        
	            // set to binary type if MIME mapping not found
	            mimeType = "application/octet-stream";
	        }
	         
	        // modifies response
	        response.setContentType(mimeType);
	        response.setContentLength((int)contentLength);
	         
	        // forces download
	        String headerKey = "Content-Disposition";
	        String headerValue = String.format("attachment; filename=\"%s\"", fileName);
	        response.setHeader(headerKey, headerValue);
		}
		catch(Exception e){
			logger.error(e);
			e.printStackTrace();
		}
		finally{
			/*try{
				if(inputStream!=null){
					inputStream.close();
				}
			}
			catch(Exception e){
				logger.error(e);
				e.printStackTrace();
			}*/
		}
		return SUCCESS;
	}

	
	
	
	public InputStream getInputStream() {
		return inputStream;
	}

	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public long getContentLength() {
		return contentLength;
	}

	public void setContentLength(long contentLength) {
		this.contentLength = contentLength;
	}
	
	public String getFileExcelName() {
		return fileExcelName;
	}

	public void setFileExcelName(String fileExcelName) {
		this.fileExcelName = fileExcelName;
	}

}
