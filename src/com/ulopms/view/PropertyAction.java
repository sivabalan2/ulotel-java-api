package com.ulopms.view;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.nio.channels.GatheringByteChannel;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TimeZone;

import javax.imageio.ImageIO;
import javax.persistence.Column;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;









//import com.medwecare.controller.AwsObjectManager;
//import com.medwecare.controller.CommentManager;
//import com.medwecare.controller.CommentUserManager;
////import com.medwecare.controller.NotificationManager;

//import com.medwecare.model.AwsObject;
//import com.medwecare.model.Notification;
//import com.medwecare.model.ObjectComment;
//import com.medwecare.model.ObjectCommentUser;
//import com.medwecare.model.Project;






















































import org.json.JSONObject;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.SdkClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.opensymphony.xwork2.ActionSupport;

import freemarker.template.Configuration;
import freemarker.template.Template;

import javax.servlet.http.HttpServletRequest;

import com.opensymphony.xwork2.ModelDriven;
import com.sun.org.apache.xerces.internal.util.URI;
import com.ulopms.api.Hotels;
import com.ulopms.controller.AreaManager;
import com.ulopms.controller.CountryManager;
import com.ulopms.controller.GooglePlaceManager;
import com.ulopms.controller.LocationContentManager;
import com.ulopms.controller.LocationManager;
import com.ulopms.controller.PmsPhotoCategoryManager;
import com.ulopms.controller.PmsPropertyManager;
import com.ulopms.controller.PmsTagManager;
import com.ulopms.controller.PropertyAddonDetailManager;
import com.ulopms.controller.PropertyAddonManager;
import com.ulopms.controller.PropertyAreasManager;
import com.ulopms.controller.PropertyContentManager;
import com.ulopms.controller.PropertyContractTypeManager;
import com.ulopms.controller.PropertyGuestReviewManager;
import com.ulopms.controller.PropertyLandmarkManager;
import com.ulopms.controller.PropertyReviewsManager;
import com.ulopms.controller.PropertyRoleDetailManager;
import com.ulopms.controller.PropertyRolesManager;
import com.ulopms.controller.PropertyTypeManager;
import com.ulopms.controller.PropertyUserManager;
import com.ulopms.controller.StateManager;
import com.ulopms.controller.UserLoginManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.Area;
import com.ulopms.model.Country;
import com.ulopms.model.GoogleArea;
import com.ulopms.model.GooglePropertyAreas;
import com.ulopms.model.Location;
import com.ulopms.model.LocationContent;
import com.ulopms.model.LocationType;
import com.ulopms.model.PmsAmenity;
import com.ulopms.model.PmsBookedDetails;
import com.ulopms.model.PmsPhotoCategory;
import com.ulopms.model.PmsProperty;
import com.ulopms.model.PmsTags;
import com.ulopms.model.PropertyAddon;
import com.ulopms.model.PropertyAddonDetails;
import com.ulopms.model.PropertyAreas;
import com.ulopms.model.PropertyContent;
import com.ulopms.model.PropertyContractType;
import com.ulopms.model.PropertyGuestReview;
import com.ulopms.model.PropertyLandmark;
import com.ulopms.model.PropertyReviews;
import com.ulopms.model.PropertyRoleDetails;
import com.ulopms.model.PropertyRoles;
import com.ulopms.model.PropertyTags;
import com.ulopms.model.PropertyType;
import com.ulopms.model.PropertyUser;
import com.ulopms.model.State;
import com.ulopms.model.UlotelHotelDetailTags;
import com.ulopms.model.User;
import com.ulopms.util.Email;
import com.ulopms.util.Image;

public class PropertyAction extends ActionSupport implements
ServletRequestAware,SessionAware, UserAware{

	private static final long serialVersionUID = 914982665758390091L;
	
	



	private Integer propertyId;
	private Integer propertyTypeId;
	private String propertyUrl;
	private String timeHours;

	private String address1;
	private String address2;
	private String city;
	private String latitude;
	private String longitude;
	private String placeId;
	private String propertyContact;
	private String propertyDescription;
	private String propertyEmail;
	private String propertyLogo;
	private String propertyRole;
	private String propertyName;
	private String resortManagerName;
	private String resortManagerContact;
	private String resortManagerEmail;
	private String resortManagerDesignation;
	private String resortManagerName1;
	private String resortManagerContact1;
	private String resortManagerEmail1;
	private String resortManagerDesignation1;
	private String resortManagerName2;
	private String resortManagerContact2;
	private String resortManagerEmail2;
	private String resortManagerDesignation2;
	private String resortManagerName3;
	private String resortManagerContact3;
	private String resortManagerEmail3;
	private String resortManagerDesignation3;
	private Integer paynowDiscount;
	private String reservationManagerName;
	private String reservationManagerContact;
	private String reservationManagerEmail;
	private String contractManagerName;
	private String contractManagerEmail;
	private String contractManagerContact;
	private String revenueManagerName;
	private String revenueManagerEmail;
	private String revenueManagerContact;
	private String propertyPhone;
	private String zipCode;
	private String stateCode;
	private String countryCode;
	private Timestamp taxValidFrom;
	private Timestamp taxValidTo;
	private Boolean taxIsActive;
	private Boolean payAtHotelStatus;
	private String propertyCommission;
	private String strTaxValidFrom;
	private String strTaxValidTo;
	private String propertyGstNo;
	private String propertyFinanceEmail;
	private String routeMap;
	private String propertyReviewLink;
	private String tripadvisorReviewLink;
	private String propertyAddonPolicy;
	private String propertyHotelPolicy;
	private String propertyRefundPolicy;
	private String propertyCancellationPolicy;
	private String propertyChildPolicy; 
	private String propertyGigiPolicy;
	private Boolean travelDiscountIsActive;
	private Boolean flexibleStayIsActive;
	private String metaDescription;
	private String schemaContent;
	private String locationDescription;
	private String scriptContent;
	private Integer locationId;	
	private String propertyContent;
	private String cancellationCommission;
	private PmsProperty property;
	
	private String propertyLandmark1;
	private String propertyLandmark2;
	private String propertyLandmark3;
	private String propertyLandmark4;
	private String propertyLandmark5;
	private String propertyLandmark6;
	private String propertyLandmark7;
	private String propertyLandmark8;
	private String propertyLandmark9;
	private String propertyLandmark10;
	private String propertyAttraction1;
	private String propertyAttraction2;
	private String propertyAttraction3;
	private String propertyAttraction4;
	private String propertyAttraction5;
	private double  distance1;
	private double  distance2;
	private double  distance3;
	private double  distance4;
	private double  distance5;
	private double  kilometers1;
	private double  kilometers2;
	private double  kilometers3;
	private double  kilometers4;
	private double  kilometers5;
	private double  kilometers6;
	private double  kilometers7;
	private double  kilometers8;
	private double  kilometers9;
	private double  kilometers10;
	private String checkedAreas;
	private String checkedPropertyTag;
	private File myFile;
	private String myFileContentType;
	private String myFileFileName;
	private Integer propertyLocationId;
	private Integer locationTypeId;
	private Integer locationPropertyId;
	private Integer limit;
	private Integer offset;
	private Boolean coupleStatus;
	private Boolean breakfastStatus;
	private Boolean hotelStatus; 
	private Boolean hotelIsActive;
	private Boolean stopSellIsActive;
	private Boolean delistIsActive;

	private String walkinCommission;
	private String onlineCommission;
	private Integer contractTypeId;
	private String netRateAmount;
	private String addonName;
	private Double addonRate;
	private Boolean addonIsActive;
	private Integer addonId;
	

	private Integer propertyAddonDetailId;
	private Integer addonPropertyId;
	private Integer propertyAddonId;
	private Integer firstPartnerPropertyId;
	private Integer selectedPartnerPropertyId;
	
	

	private List<PmsProperty> propertyList;
	
	private List<PmsPhotoCategory> categoryList;
	
	private List<LocationContent> locationContentList;
	
	private List<PropertyContent> propertyContentList;
	
	private List<PropertyLandmark> propertyLandmarkList;

	private static final Logger logger = Logger.getLogger(PropertyAction.class);

	private HttpSession session;

	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}
	
	private SessionMap<String,Object> sessionMap;  
	  
	//getters and setters  
	  
	@Override  
	public void setSession(Map<String, Object> map) {  
	    sessionMap=(SessionMap)map;  
	}  
	
	private User user;

	
	// @Override
	// public User getModel() {
	// return user;
	// }

	@Override
	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	
	private HttpServletRequest request;

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}
	
	public String getStrTaxValidFrom() {
		return strTaxValidFrom;
	}

	public void setStrTaxValidFrom(String strTaxValidFrom) {
		this.strTaxValidFrom = strTaxValidFrom;
	}

	public String getStrTaxValidTo() {
		return strTaxValidTo;
	}

	public void setStrTaxValidTo(String strTaxValidTo) {
		this.strTaxValidTo = strTaxValidTo;
	}

	public PropertyAction() {
		
	

	}

	public String execute() {//.
//this.propertyId= session("")
		//this.patientId = getPatientId();
		
		
		return SUCCESS;
	}
	
	String clientRegion = "ap-south-1";
	String bucketName = "ulohotelsuploads/uloimg";    
    String keyName = "ulohotelsuploads";
	

	public String uploadAwsImageProperty(){
			    
	        try {
	        	PmsPropertyManager propertyController=new PmsPropertyManager();
	        	
	        	AWSCredentials credentials = new BasicAWSCredentials("AKIA3BSHXMURUP3WEKWH", "BjOPBijiDBysvDDbOLJOTzEK+k7yl/mxxWF28k/Y");
		        String bucketName1 = "ulohotelsuploads/uloimg/property/"+getLocationPropertyId()+"";
	            AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
	                    .withRegion(clientRegion)
	                    .withCredentials(new AWSStaticCredentialsProvider(credentials))
	                    .build();
	        
	            // Upload a text string as a new object.
	            s3Client.putObject(bucketName1, this.getMyFileFileName(), this.myFile);
	            
	            
				PmsProperty property = propertyController.find(getLocationPropertyId()); 
				property.setPropertyThumbPath(this.getMyFileFileName());
				propertyController.edit(property);
	        }
	        catch(AmazonServiceException e) {
	            e.printStackTrace();
	        }
	        catch(SdkClientException e) {
	            e.printStackTrace();
	        }
			
			return SUCCESS; 
		}

	
	public String getProperties() throws IOException {
		this.propertyId = (Integer) sessionMap.get("propertyId");
		
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PmsPropertyManager propertyController = new PmsPropertyManager();
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");

			PmsProperty property = propertyController.find(getPropertyId());
			
				
			if (!jsonOutput.equalsIgnoreCase(""))
				jsonOutput += ",{";
			else
				jsonOutput += "{";
			jsonOutput += "\"DT_RowId\":\"" + property.getPropertyId()+ "\"";
			jsonOutput += ",\"propertyTypeId\":\"" + property.getPropertyType().getPropertyTypeId()+ "\"";
			jsonOutput += ",\"address1\":\"" + property.getAddress1().trim()+ "\"";
			jsonOutput += ",\"address2\":\"" + property.getAddress2().trim()+ "\"";
			jsonOutput += ",\"city\":\"" + property.getCity().trim() + "\"";
			jsonOutput += ",\"latitude\":\"" +property.getLatitude().trim()+ "\"";
			jsonOutput += ",\"longitude\":\"" + property.getLongitude().trim()+ "\"";
			jsonOutput += ",\"placeId\":\"" + (property.getPlaceId() == null ? "": property.getPlaceId().trim()) + "\"";
			jsonOutput += ",\"propertyContact\":\"" + property.getPropertyContact().trim() + "\"";
			//jsonOutput += ",\"propertyDescription\":\"" + property.getPropertyDescription() + "\"";
			String strPropertyDesc=null,propertyDesc=null;
			strPropertyDesc=property.getPropertyDescription();
			if(strPropertyDesc!=null){
				propertyDesc = strPropertyDesc.replace("\"", "\\\"").trim();
				propertyDesc = strPropertyDesc.replaceAll("\\s+", " ");
			}else{
				propertyDesc="";
			}
			
			jsonOutput += ",\"propertyDescription\":\"" + (propertyDesc) + "\"";
			
			jsonOutput += ",\"propertyEmail\":\"" + (property.getPropertyEmail() == null ? "": property.getPropertyEmail().trim())+ "\"";
			jsonOutput += ",\"propertyName\":\"" + (property.getPropertyName() == null ? "": property.getPropertyName().trim())+ "\"";
			jsonOutput += ",\"propertyPhone\":\"" + (property.getPropertyPhone() == null ? "": property.getPropertyPhone().trim())+ "\"";
			String strHotelPolicy=null,strStandardPolicy=null,strCancellationPolicy=null,strChildPolicy=null,strGigiPolicy=null;
			strHotelPolicy=property.getPropertyHotelPolicy();
			strStandardPolicy=property.getPropertyStandardPolicy();
			strCancellationPolicy=property.getPropertyCancellationPolicy();
			strChildPolicy=property.getPropertyChildPolicy();
			strGigiPolicy=property.getPropertyGigiPolicy();
			String refundPolicy,changedRefundPolicy,cancelledPolicy,gigiPolicy,changedCancelledPolicy,hotelPolicy,changedHotelPolicy,changedGigiPolicy;
			
			if(strHotelPolicy!=null){
				hotelPolicy = strHotelPolicy.replace("\"", "\\\"");
				changedHotelPolicy = hotelPolicy.replaceAll("\\s+", " ");
			}else{
				changedHotelPolicy="";
			}
			
			if(strStandardPolicy!=null){
				refundPolicy = strStandardPolicy.replace("\"", "\\\"");
				changedRefundPolicy = refundPolicy.replaceAll("\\s+", " ");
			}else{
				changedRefundPolicy ="";
				
			}
			
			if(strCancellationPolicy!=null){
				cancelledPolicy = strCancellationPolicy.replace("\"", "\\\"");
				changedCancelledPolicy = cancelledPolicy.replaceAll("\\s+", " ");
			}else{
				changedCancelledPolicy="";
			}
			
			if(strGigiPolicy!=null){
				gigiPolicy = strGigiPolicy.replace("\"", "\\\"");
				changedGigiPolicy = gigiPolicy.replaceAll("\\s+", " ");
			}else{
				changedGigiPolicy="";
			}
			
			
			jsonOutput += ",\"propertyHotelPolicy\":\"" + changedHotelPolicy + "\"";
			jsonOutput += ",\"propertyLocationTypeId\":\"" + (property.getLocationType() == null ? "": property.getLocationType().getLocationTypeId()) + "\"";
			jsonOutput += ",\"propertyLocationTypeName\":\"" + (property.getLocationType() == null ? "": property.getLocationType().getLocationTypeName()) + "\"";
			jsonOutput += ",\"propertyRefundPolicy\":\"" + (changedRefundPolicy)+ "\"";
			jsonOutput += ",\"propertyGigiPolicy\":\"" + (changedGigiPolicy)+ "\"";
			jsonOutput += ",\"propertyCancellationPolicy\":\"" + (changedCancelledPolicy)+ "\"";
			jsonOutput += ",\"reservationManagerName\":\"" + (property.getReservationManagerName() == null ?"": property.getReservationManagerName().trim())+ "\"";
			jsonOutput += ",\"reservationManagerEmail\":\"" + (property.getReservationManagerEmail() == null ?"": property.getReservationManagerEmail().trim())+ "\"";
			jsonOutput += ",\"reservationManagerContact\":\"" + (property.getReservationManagerContact() == null ?"": property.getReservationManagerContact().trim()) + "\"";
			jsonOutput += ",\"resortManagerName\":\"" + (property.getResortManagerName() == null ?"": property.getResortManagerName().trim())+ "\"";
			jsonOutput += ",\"resortManagerEmail\":\"" + (property.getResortManagerEmail() == null ?"": property.getResortManagerEmail().trim())+ "\"";
			jsonOutput += ",\"resortManagerContact\":\"" + (property.getResortManagerContact() == null?"":property.getResortManagerContact().trim()) + "\"";
			jsonOutput += ",\"resortManagerDesignation\":\"" + (property.getResortManagerDesignation() == null?"":property.getResortManagerDesignation().trim()) + "\"";
			jsonOutput += ",\"resortManagerName1\":\"" + (property.getResortManagerName1() == null ?"": property.getResortManagerName1().trim())+ "\"";
			jsonOutput += ",\"resortManagerEmail1\":\"" + (property.getResortManagerEmail1() == null ?"": property.getResortManagerEmail1().trim())+ "\"";
			jsonOutput += ",\"resortManagerContact1\":\"" + (property.getResortManagerContact1() == null?"":property.getResortManagerContact1().trim()) + "\"";
			jsonOutput += ",\"resortManagerDesignation1\":\"" + (property.getResortManagerDesignation1() == null?"":property.getResortManagerDesignation1().trim()) + "\"";
			jsonOutput += ",\"resortManagerName2\":\"" + (property.getResortManagerName2() == null ?"": property.getResortManagerName2().trim())+ "\"";
			jsonOutput += ",\"resortManagerEmail2\":\"" + (property.getResortManagerEmail2() == null ?"": property.getResortManagerEmail2().trim())+ "\"";
			jsonOutput += ",\"resortManagerContact2\":\"" + (property.getResortManagerContact2() == null?"":property.getResortManagerContact2().trim()) + "\"";
			jsonOutput += ",\"resortManagerDesignation2\":\"" + (property.getResortManagerDesignation2() == null?"":property.getResortManagerDesignation2().trim()) + "\"";
			/*jsonOutput += ",\"resortManagerName3\":\"" + (property.getResortManagerName3() == null ?"": property.getResortManagerName3().trim())+ "\"";
			jsonOutput += ",\"resortManagerEmail3\":\"" + (property.getResortManagerEmail3() == null ?"": property.getResortManagerEmail3().trim())+ "\"";
			jsonOutput += ",\"resortManagerContact3\":\"" + (property.getResortManagerContact3() == null?"":property.getResortManagerContact3().trim()) + "\"";
			jsonOutput += ",\"resortManagerDesignation3\":\"" + (property.getResortManagerDesignation3() == null?"":property.getResortManagerDesignation3().trim()) + "\"";*/
			jsonOutput += ",\"contractManagerName\":\"" + (property.getContractManagerName() == null ?"": property.getContractManagerName().trim())+ "\"";
			jsonOutput += ",\"contractManagerEmail\":\"" + (property.getContractManagerEmail() == null ?"": property.getContractManagerEmail().trim())+ "\"";
			jsonOutput += ",\"contractManagerContact\":\"" + (property.getContractManagerContact() == null ?"": property.getContractManagerContact().trim()) + "\"";
			jsonOutput += ",\"revenueManagerName\":\"" + (property.getRevenueManagerName() == null ?"": property.getRevenueManagerName().trim())+ "\"";
			jsonOutput += ",\"revenueManagerEmail\":\"" + (property.getRevenueManagerEmail() == null ?"": property.getRevenueManagerEmail().trim())+ "\"";
			jsonOutput += ",\"revenueManagerContact\":\"" + (property.getRevenueManagerContact() == null?"":property.getRevenueManagerContact().trim()) + "\"";
			jsonOutput += ",\"coupleStatus\":\"" + ( property.getCoupleIsActive() == null?false :  property.getCoupleIsActive() )+ "\"";
			jsonOutput += ",\"breakfastStatus\":\"" + ( property.getBreakfastIsActive() == null?false:  property.getBreakfastIsActive() )+ "\"";
			jsonOutput += ",\"hotelStatus\":\"" + ( property.getHotelIsActive() == null?false:  property.getHotelIsActive() )+ "\"";
			jsonOutput += ",\"zipCode\":\"" + (property.getZipCode() == null ? "": property.getZipCode().trim())+ "\"";
			String from = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH).format(property.getTaxValidFrom());
			String to = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH).format(property.getTaxValidTo());
			jsonOutput += ",\"taxValidFrom\":\"" + from + "\"";
			jsonOutput += ",\"taxValidTo\":\"" + to + "\"";
			jsonOutput += ",\"taxIsActive\":\"" + ( property.getTaxIsActive() == true ? "true": "false" )+ "\"";
			jsonOutput += ",\"payAtHotelStatus\":\"" + ( property.getPayAtHotelStatus() == null? "false":  property.getPayAtHotelStatus() )+ "\"";
			jsonOutput += ",\"travelDiscountIsActive\":\"" + ( property.getTravelDiscountStatus() == null? false:  property.getTravelDiscountStatus() )+ "\"";
			jsonOutput += ",\"flexibleStayIsActive\":\"" + ( property.getFlexibleStayStatus() == null? false:  property.getFlexibleStayStatus())+ "\"";
			jsonOutput += ",\"propertyCommission\":\"" + ( property.getPropertyCommission() == null? " ":  property.getPropertyCommission() )+ "\"";
			jsonOutput += ",\"walkinCommission\":\"" + ( property.getWalkinCommission() == null? " ":  property.getWalkinCommission())+ "\"";
			jsonOutput += ",\"onlineCommission\":\"" + ( property.getOnlineCommission() == null? " ":  property.getOnlineCommission() )+ "\"";
			jsonOutput += ",\"paynowDiscount\":\"" + ( property.getPaynowDiscount() == null? 0:   property.getPaynowDiscount() )+ "\"";
			jsonOutput += ",\"cancellationCommission\":\"" + ( property.getNetRateRevenue()== null? 0:  property.getNetRateRevenue())+ "\"";
			jsonOutput += ",\"stateCode\":\"" + (property.getState() == null ? "": property.getState().getStateCode().trim())+ "\"";
			jsonOutput += ",\"countryCode\":\"" + (property.getCountry() == null ? "": property.getCountry().getCountryCode().trim())+ "\"";
			jsonOutput += ",\"propertyGstNo\":\"" + (property.getPropertyGstNo() == null ? "": property.getPropertyGstNo().trim())+ "\"";
			jsonOutput += ",\"propertyFinanceEmail\":\"" + (property.getPropertyFinanceEmail() == null ? "": property.getPropertyFinanceEmail().trim())+ "\"";
			jsonOutput += ",\"routeMap\":\"" + (property.getRouteMap() == null ? "": property.getRouteMap().trim())+ "\"";
			jsonOutput += ",\"propertyReviewLink\":\"" + (property.getPropertyReviewLink() == null ? "": property.getPropertyReviewLink().trim())+ "\"";
			jsonOutput += ",\"tripadvisorReviewLink\":\"" + (property.getTripadvisorReviewLink() == null ? "": property.getTripadvisorReviewLink().trim())+ "\"";
			jsonOutput += ",\"stopSellIsActive\":\"" +  property.getStopSellIsActive() + "\"";
			jsonOutput += ",\"delistIsActive\":\"" + property.getDelistIsActive() + "\"";
			jsonOutput += ",\"hotelIsActive\":\"" + property.getHotelIsActive() + "\"";
			if(property.getContractType()==null){
				jsonOutput += ",\"contractTypeId\":\"" + 0 + "\"";	
				jsonOutput += ",\"contractTypeName\":\"" + ("Nil")+ "\"";
			}else{
				jsonOutput += ",\"contractTypeId\":\"" + (property.getContractType().getContractTypeId() == null ? "": property.getContractType().getContractTypeId())+ "\"";
				jsonOutput += ",\"contractTypeName\":\"" + (property.getContractType().getContractTypeName())+ "\"";
			}
			
			/*String jsonPorpertyTag = "";
			List<PmsTags> tag = propertyController.listTag();
			for (PmsTags tagList : tag) {
				List<PropertyTags>  propertyTag = propertyController.findTag(property.getPropertyId(),tagList.getTagId());	
			if (!jsonPorpertyTag.equalsIgnoreCase(""))
				jsonPorpertyTag += ",{";
			else
					
			jsonPorpertyTag += "{";
			jsonPorpertyTag += "\"tagId\":\"" + tagList.getTagId() + "\"";	
			jsonPorpertyTag += ",\"tagName\":\"" + tagList.getTagName() + "\"";	
			jsonPorpertyTag += ",\"status\":\"" + (propertyTag.size() == 0 ? false: true) + "\"";
			
			jsonPorpertyTag += "}";
			}
			jsonOutput += ",\"propertyTags\":[" + jsonPorpertyTag+ "]";*/
			
			jsonOutput += "}";

			

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	public String getPropertyTags() throws IOException {
		this.propertyId = (Integer) sessionMap.get("propertyId");
		
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PmsPropertyManager propertyController = new PmsPropertyManager();
			response.setContentType("application/json");

			
			List<PmsTags> tag = propertyController.listTag();
			for (PmsTags tagList : tag) {
				
				List<PropertyTags>  propertyTag = propertyController.findTag(this.propertyId,tagList.getTagId());	
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				
				jsonOutput += "\"tagId\":\"" + tagList.getTagId() + "\"";	
				jsonOutput += ",\"tagName\":\"" + tagList.getTagName() + "\"";	
				if(propertyTag.size() == 0){
				jsonOutput += ",\"status\":" +  false    +"" ;
				}else if(propertyTag.size() != 0){
					jsonOutput += ",\"status\":" +  true  + "";
				}
				jsonOutput += "}";
			}

			

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	public String getUlotelHotelTags() throws IOException {
		this.propertyId = (Integer) sessionMap.get("propertyId");
		
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PmsPropertyManager propertyController = new PmsPropertyManager();
 			response.setContentType("application/json");

			
			UlotelHotelDetailTags tag = propertyController.findHotelTag(this.propertyId);
			if(tag!=null){
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				
				jsonOutput += "\"hotelTagId\":\"" + tag.getHotelTagId() + "\"";	
				jsonOutput += ",\"hotelProfile\":\"" + tag.getHotelProfile() + "\"";
				jsonOutput += ",\"roomCategories\":\"" + tag.getRoomCategories() + "\"";
				jsonOutput += ",\"ratePlan\":\"" + tag.getRatePlan() + "\"";
				jsonOutput += ",\"hotelAmenities\":\"" + tag.getHotelAmenities() + "\"";
				jsonOutput += ",\"hotelPolicy\":\"" + tag.getHotelPolicy() + "\"";
				jsonOutput += ",\"addOn\":\"" + tag.getAddOn() + "\"";
				jsonOutput += ",\"hotelLandmark\":\"" + tag.getHotelLandmark() + "\"";
				jsonOutput += ",\"hotelImages\":\"" + tag.getHotelImages() + "\"";
				jsonOutput += ",\"hotelStatus\":\"" + tag.getHotelStatus() + "\"";
			
				jsonOutput += "}";
			}
				

			

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	public String getUloProperties() throws IOException {
		try {
			/*this.propertyId = (Integer) sessionMap.get("uloPropertyId");
			if(this.propertyId==null){
				this.propertyId = (Integer) sessionMap.get("urlPropertyId");
			}*/
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			DecimalFormat df = new DecimalFormat("###.##");
			
			PmsPropertyManager propertyController = new PmsPropertyManager();
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");

			PmsProperty property = propertyController.find(getPropertyId());
			
				
			if (!jsonOutput.equalsIgnoreCase(""))
				jsonOutput += ",{";
			else
				jsonOutput += "{";
			jsonOutput += "\"DT_RowId\":\"" + property.getPropertyId()+ "\"";
			jsonOutput += ",\"propertyTypeId\":\"" + property.getPropertyType().getPropertyTypeId()+ "\"";
			jsonOutput += ",\"address1\":\"" + property.getAddress1().trim()+ "\"";
			jsonOutput += ",\"address2\":\"" + property.getAddress2().trim()+ "\"";
			jsonOutput += ",\"city\":\"" + property.getCity().trim() + "\"";
			jsonOutput += ",\"latitude\":\"" +property.getLatitude().trim()+ "\"";
			jsonOutput += ",\"longitude\":\"" + property.getLongitude().trim()+ "\"";
			jsonOutput += ",\"placeId\":\"" + property.getPlaceId().trim()+ "\"";
			jsonOutput += ",\"propertyContact\":\"" + property.getPropertyContact().trim() + "\"";
			//jsonOutput += ",\"propertyDescription\":\"" + property.getPropertyDescription() + "\"";
			jsonOutput += ",\"propertyDescription\":\"" + (property.getPropertyDescription() == null ? "": property.getPropertyDescription().trim()) + "\"";
			jsonOutput += ",\"propertyEmail\":\"" + (property.getPropertyEmail() == null ? "": property.getPropertyEmail().trim())+ "\"";
			jsonOutput += ",\"propertyName\":\"" + (property.getPropertyName() == null ? "": property.getPropertyName().trim())+ "\"";
			jsonOutput += ",\"propertyPhone\":\"" + (property.getPropertyPhone() == null ? "": property.getPropertyPhone().trim())+ "\"";
			/*String strHotelPolicy=null,strStandardPolicy=null,strCancellationPolicy=null,strChildPolicy=null;
			strHotelPolicy=property.getPropertyHotelPolicy();
			strStandardPolicy=property.getPropertyStandardPolicy();
			strCancellationPolicy=property.getPropertyCancellationPolicy();
			strChildPolicy=property.getPropertyChildPolicy();
			jsonOutput += ",\"propertyHotelPolicy\":\"" + (strHotelPolicy == null ? "": strHotelPolicy.trim())+ "\"";
			jsonOutput += ",\"propertyStandardPolicy\":\"" + (strStandardPolicy== null ? "": strStandardPolicy.trim())+ "\"";
			jsonOutput += ",\"propertyCancellationPolicy\":\"" + (strCancellationPolicy == null ? "": strCancellationPolicy.trim())+ "\"";
			jsonOutput += ",\"propertyChildPolicy\":\"" + (strChildPolicy == null ? "": strChildPolicy.trim())+ "\"";*/
			jsonOutput += ",\"reservationManagerName\":\"" + (property.getReservationManagerName() == null ? "": property.getReservationManagerName().trim())+ "\"";
			jsonOutput += ",\"reservationManagerEmail\":\"" + (property.getReservationManagerEmail() == null ? "": property.getReservationManagerEmail().trim())+ "\"";
			jsonOutput += ",\"reservationManagerContact\":\"" + (property.getReservationManagerContact() == null ? "": property.getReservationManagerContact().trim()) + "\"";
			jsonOutput += ",\"resortManagerName\":\"" + (property.getResortManagerName() == null ? "": property.getResortManagerName().trim())+ "\"";
			jsonOutput += ",\"resortManagerEmail\":\"" + (property.getResortManagerEmail() == null ? "": property.getResortManagerEmail().trim())+ "\"";
			jsonOutput += ",\"resortManagerContact\":\"" + (property.getResortManagerContact() == null?"":property.getResortManagerContact().trim()) + "\"";
			jsonOutput += ",\"contractManagerName\":\"" + (property.getContractManagerName() == null ? "": property.getContractManagerName().trim())+ "\"";
			jsonOutput += ",\"contractManagerEmail\":\"" + (property.getContractManagerEmail() == null ? "": property.getContractManagerEmail().trim())+ "\"";
			jsonOutput += ",\"contractManagerContact\":\"" + (property.getContractManagerContact() == null ? "": property.getContractManagerContact().trim()) + "\"";
			jsonOutput += ",\"revenueManagerName\":\"" + (property.getRevenueManagerName() == null ? "": property.getRevenueManagerName().trim())+ "\"";
			jsonOutput += ",\"revenueManagerEmail\":\"" + (property.getRevenueManagerEmail() == null ? "": property.getRevenueManagerEmail().trim())+ "\"";
			jsonOutput += ",\"revenueManagerContact\":\"" + (property.getRevenueManagerContact() == null?"":property.getRevenueManagerContact().trim()) + "\"";
			jsonOutput += ",\"zipCode\":\"" + (property.getZipCode() == null ? "": property.getZipCode().trim())+ "\"";
			String from = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH).format(property.getTaxValidFrom());
			String to = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH).format(property.getTaxValidTo());
			jsonOutput += ",\"taxValidFrom\":\"" + from + "\"";
			jsonOutput += ",\"taxValidTo\":\"" + to + "\"";
			jsonOutput += ",\"taxIsActive\":\"" + ( property.getTaxIsActive() == true ? "true": "false" )+ "\"";
			jsonOutput += ",\"propertyCommission\":\"" + ( property.getPropertyCommission() == null? " ":  property.getPropertyCommission() )+ "\"";
			jsonOutput += ",\"walkinCommission\":\"" + ( property.getWalkinCommission() == null? " ":  property.getWalkinCommission() )+ "\"";
			jsonOutput += ",\"onlineCommission\":\"" + ( property.getOnlineCommission() == null? " ":  property.getOnlineCommission() )+ "\"";
			jsonOutput += ",\"stateCode\":\"" + (property.getState() == null ? "": property.getState().getStateCode().trim())+ "\"";
			jsonOutput += ",\"countryCode\":\"" + (property.getState() == null ? "": property.getState().getCountry().getCountryCode().trim())+ "\"";
			jsonOutput += ",\"propertyGstNo\":\"" + (property.getPropertyGstNo() == null ? "": property.getPropertyGstNo().trim())+ "\"";
			jsonOutput += ",\"propertyFinanceEmail\":\"" + (property.getPropertyFinanceEmail() == null ? "": property.getPropertyFinanceEmail().trim())+ "\"";
			if(property.getContractType()==null){
				jsonOutput += ",\"contractTypeId\":\"" + (0)+ "\"";	
				jsonOutput += ",\"contractTypeName\":\"" + ("Nil")+ "\"";
			}else{
				jsonOutput += ",\"contractTypeId\":\"" + (property.getContractType().getContractTypeId() == null ? "": property.getContractType().getContractTypeId())+ "\"";
				jsonOutput += ",\"contractTypeName\":\"" + (property.getContractType().getContractTypeName())+ "\"";
			}			
			String jsonReviews="";
			String jsonGuestReview="";
			PropertyReviewsManager reviewController=new PropertyReviewsManager();
			List<PropertyReviews> listReviews=reviewController.listReview(property.getPropertyId());
			if(listReviews.size()>0 && !listReviews.isEmpty()){
				for(PropertyReviews reviews:listReviews){
					
					if (!jsonReviews.equalsIgnoreCase(""))
						
						jsonReviews += ",{";
					else
						jsonReviews += "{";
					
					/*jsonReviews += "\"starCount\":\"" + reviews.getStarCount().intValue()+ "\"";	
					jsonReviews += ",\"reviewCount\":\"" + reviews.getReviewCount().intValue()+ "\"";	
					Double averageReviews=reviews.getAverageReview();
					jsonReviews += ",\"averageReview\":\"" + df.format(averageReviews)+ "\"";*/
					
					jsonReviews += "\"starCount\":\"" + (reviews.getStarCount()==null?0:reviews.getStarCount().intValue())+ "\"";	
					jsonReviews += ",\"reviewCount\":\"" + (reviews.getReviewCount()==null?0:reviews.getReviewCount().intValue())+ "\"";	
					if(reviews.getAverageReview()!=null){
						Double averageReviews=reviews.getAverageReview();
						jsonReviews += ",\"averageReview\":\"" + df.format(averageReviews)+ "\"";
					}else{
						jsonReviews += ",\"averageReview\":\"" + df.format(0)+ "\"";
					}
					
					jsonReviews += ",\"tripadvisorStarCount\":\"" + (reviews.getTripadvisorStarCount()==null?0:reviews.getTripadvisorStarCount().intValue())+ "\"";	
					jsonReviews += ",\"tripadvisorReviewCount\":\"" + (reviews.getTripadvisorReviewCount()==null?0:reviews.getTripadvisorReviewCount().intValue())+ "\"";
					if(reviews.getTripadvisorAverageReview()!=null){
						Double taAverageReviews=reviews.getTripadvisorAverageReview();
						jsonReviews += ",\"tripadvisorAverageReview\":\"" + df.format(taAverageReviews)+ "\"";
					}else{
						jsonReviews += ",\"tripadvisorAverageReview\":\"" + df.format(0)+ "\"";
					}
					
					
					jsonReviews += "}";
				}
			}else{
				if (!jsonReviews.equalsIgnoreCase(""))
					
					jsonReviews += ",{";
				else
					jsonReviews += "{";
				
				jsonReviews += "\"starCount\":\"" + 0+ "\"";	
				jsonReviews += ",\"reviewCount\":\"" + 0+ "\"";	
				jsonReviews += ",\"averageReview\":\"" + 0+ "\"";
				
				jsonReviews += ",\"tripadvisorStarCount\":\"" + 0 + "\"";	
				jsonReviews += ",\"tripadvisorReviewCount\":\"" + 0 + "\"";	
				jsonReviews += ",\"tripadvisorAverageReview\":\"" + 0+ "\"";
				
				jsonReviews += "}";
			}
			
			jsonOutput += ",\"reviews\":[" + jsonReviews+ "]";
			PropertyGuestReviewManager guestReviewController=new PropertyGuestReviewManager();
			List<PropertyGuestReview> listGuestReviews=guestReviewController.listReview(property.getPropertyId());
			if(listGuestReviews.size()>0 && !listGuestReviews.isEmpty()){
				for(PropertyGuestReview reviews:listGuestReviews){
					if (!jsonGuestReview.equalsIgnoreCase(""))
						
						jsonGuestReview += ",{";
					else
						jsonGuestReview += "{";
					
					jsonGuestReview += "\"reviewerGuestName\":\"" + (reviews.getReviewGuestName()==null?"":reviews.getReviewGuestName())+ "\"";	
					jsonGuestReview += ",\"reviewerDescription\":\"" + (reviews.getReviewDescription()==null?"":reviews.getReviewDescription())+ "\"";	
					jsonGuestReview += ",\"reviewerCount\":\"" + (reviews.getReviewCount()==null?0:reviews.getReviewCount())+ "\"";
					jsonGuestReview += ",\"reviewerName\":\"" + (reviews.getReviews().getReviewerName()==null?"":reviews.getReviews().getReviewerName())+ "\"";
					jsonGuestReview += ",\"reviewerId\":\"" + (reviews.getReviews().getReviewerId()==null?"":reviews.getReviews().getReviewerId())+ "\"";
					jsonGuestReview += "}";
				}
			}else{
				if (!jsonGuestReview.equalsIgnoreCase(""))
					
					jsonGuestReview += ",{";
				else
					jsonGuestReview += "{";
				
				jsonGuestReview += "\"reviewerGuestName\":\"" + ""+ "\"";	
				jsonGuestReview += ",\"reviewerDescription\":\"" + ""+ "\"";	
				jsonGuestReview += ",\"reviewerCount\":\"" + 0+ "\"";
				jsonGuestReview += ",\"reviewerName\":\"" + ""+ "\"";
				jsonGuestReview += ",\"reviewerId\":\"" + "" + "\"";
				
				jsonGuestReview += "}";
			}
		
			jsonOutput += ",\"guestReviews\":[" + jsonGuestReview+ "]";
		
			
			jsonOutput += "}";

			

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	public String getContractTypesCheck(){
		try{
			Integer contractTypeId=0;	
			this.propertyId = (Integer) sessionMap.get("propertyId");
			PmsPropertyManager propertyController=new PmsPropertyManager();
			
			PmsProperty property=propertyController.find(this.propertyId);
			contractTypeId=property.getContractType().getContractTypeId();
			
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("application/json");
			if (!jsonOutput.equalsIgnoreCase(""))
				jsonOutput += ",{";
			else
				jsonOutput += "{";
			
				if(contractTypeId==1 || contractTypeId==2 || contractTypeId==3){
					jsonOutput += "\"netRateContractTypeId\":\"" + contractTypeId+ "\"";
					jsonOutput += ",\"netRateEnable\":\"" + (1)+ "\"";
				}else{
					jsonOutput += "\"netRateContractTypeId\":\"" + contractTypeId+ "\"";
					jsonOutput += ",\"netRateEnable\":\"" + (0)+ "\"";
				}
			
			jsonOutput += "}";
			
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
		}catch(Exception ex){
			logger.error(ex);
			ex.printStackTrace();
		}
		return null;
	}
	
	public String getContractTypes(){
		try{
			this.propertyId = (Integer) sessionMap.get("propertyId");
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("application/json");
			
			PropertyContractTypeManager contractTypeController=new PropertyContractTypeManager();
			
			List<PropertyContractType> listContractType=null;
			listContractType=contractTypeController.list();
			
			if(!listContractType.isEmpty() && listContractType.size()>0){
				for(PropertyContractType contractType: listContractType){
					if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += ",{";
					else
						jsonOutput += "{";
					
					jsonOutput += "\"contractTypeId\":\"" + contractType.getContractTypeId()+ "\"";
					
					jsonOutput += ",\"contractTypeName\":\"" + contractType.getContractTypeName() + "\"";
				
					jsonOutput += "}";
				}
			}
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
		}catch(Exception ex){
			logger.error(ex);
			ex.printStackTrace();
		}
		return null;
	}
	
	public String getNewContractTypes(){
		try{
			this.propertyId = (Integer) sessionMap.get("propertyId");
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("application/json");
			
			PropertyContractTypeManager contractTypeController=new PropertyContractTypeManager();
			
			List<PropertyContractType> listContractType=null;
			listContractType=contractTypeController.listContract();
			
			if(!listContractType.isEmpty() && listContractType.size()>0){
				for(PropertyContractType contractType: listContractType){
					if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += ",{";
					else
						jsonOutput += "{";
					
					jsonOutput += "\"contractTypeId\":\"" + contractType.getContractTypeId()+ "\"";
					
					jsonOutput += ",\"contractTypeName\":\"" + contractType.getContractTypeName() + "\"";
				
					jsonOutput += "}";
				}
			}
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
		}catch(Exception ex){
			logger.error(ex);
			ex.printStackTrace();
		}
		return null;
	}
	
	
	
	public String getPropertyRoles(){
		try{
			this.propertyId = (Integer) sessionMap.get("propertyId");
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("application/json");
			
			PropertyRolesManager rolesController=new PropertyRolesManager();
			
			List<PropertyRoles> listPropertyRoles=null;
			listPropertyRoles=rolesController.list();
			
			if(!listPropertyRoles.isEmpty() && listPropertyRoles.size()>0){
				for(PropertyRoles propertyRoles: listPropertyRoles){
					if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += ",{";
					else
						jsonOutput += "{";
					
					jsonOutput += "\"propertyRoleId\":\"" + propertyRoles.getPropertyRoleId()+ "\"";
					
					jsonOutput += ",\"propertyRoleName\":\"" + propertyRoles.getPropertyRoleName() + "\"";
				
					jsonOutput += "}";
				}
			}
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
		}catch(Exception ex){
			logger.error(ex);
			ex.printStackTrace();
		}
		return null;
	}
	
	public String getAllProperty() throws IOException {
		this.propertyId = (Integer) sessionMap.get("propertyId");
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PmsPropertyManager propertyController = new PmsPropertyManager();
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");

			this.propertyList = propertyController.list();
			for (PmsProperty property : propertyList) {

				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"DT_RowId\":\"" + property.getPropertyId()+ "\"";
				
				jsonOutput += ",\"propertyName\":\"" + property.getPropertyName() + "\"";
			
				jsonOutput += "}";


			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	
	public String getAllProperties() throws IOException {
		this.propertyId = (Integer) sessionMap.get("propertyId");
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PmsPropertyManager propertyController = new PmsPropertyManager();
			UserLoginManager userController=new UserLoginManager();
			response.setContentType("application/json");
			int serialno=1;
			this.propertyList = propertyController.list(limit,offset);
			for (PmsProperty property : propertyList) {

				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"DT_RowId\":\"" + property.getPropertyId()+ "\"";
				jsonOutput += ",\"serialNo\":\"" + serialno+ "\"";
				jsonOutput += ",\"propertyName\":\"" + (property.getPropertyName() == null ? "": property.getPropertyName().trim())+ "\"";
				jsonOutput += ",\"propertyUrl\":\"" + (property.getPropertyUrl() == null ? "": property.getPropertyUrl().trim())+ "\"";
				jsonOutput += ",\"locationId\":\"" + (property.getLocation().getLocationId() == null ? "": property.getLocation().getLocationId())+ "\"";
				jsonOutput += ",\"propertyLocationName\":\"" + (property.getLocation().getLocationName() == null ? "": property.getLocation().getLocationName())+ "\"";
				if(property.getCreatedBy()!=null){
					User user=userController.find(property.getCreatedBy());
					String strFirstName=user.getUserName().trim();
					String firstNameLetterUpperCase=strFirstName.substring(0, 1).toUpperCase()+strFirstName.substring(1);
					jsonOutput += ",\"createdBy\":\"" + firstNameLetterUpperCase+ "\"";
				}else{
					jsonOutput += ",\"createdBy\":\"" +" "+ "\"";
				}
				
				
				jsonOutput += "}";

				serialno++;
			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	public String getLocations() throws IOException {
		try {
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			LocationManager locationController = new LocationManager();
			response.setContentType("application/json");

			List<Location> locationList = locationController.list();
			if(locationList.size()>0 && !locationList.isEmpty()){
				for (Location location : locationList) {
					
					if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += ",{";
					else
						jsonOutput += "{";
					
					jsonOutput += "\"locationId\":\"" + location.getLocationId()+ "\"";
					jsonOutput += ",\"locationName\":\"" + location.getLocationName()+ "\"";
					
					jsonOutput += "}";
				}
			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	//editNewProperty
		public String editNewProperty() throws IOException {
			try {
				Integer userId=(Integer) sessionMap.get("userId");
				PropertyAreasManager propertyAreaController=new PropertyAreasManager();
				AreaManager areaController=new AreaManager();
				String[] stringArray = getCheckedAreas().split("\\s*,\\s*");
				
			    int[] intArray = new int[stringArray.length];
				if(getCheckedAreas()!=null && !getCheckedAreas().trim().isEmpty()){
				    
				    for (int i = 0; i < stringArray.length; i++) {
				     String numberAsString = stringArray[i];
				     intArray[i] = Integer.parseInt(numberAsString);
				    }
				}
				
			    long time = System.currentTimeMillis();
				java.util.Date date = new java.util.Date(time);
				Timestamp tsDate=new Timestamp(date.getTime());  
				
				PropertyAreas propertyArea=new PropertyAreas();
				
				
				PmsPropertyManager propertyController = new PmsPropertyManager();
				LocationManager locationController=new LocationManager();
				PmsProperty property = propertyController.find(getLocationPropertyId());
				property.setIsActive(true);
				property.setIsDeleted(false);
				property.setPropertyUrl(getPropertyUrl());
				property.setPropertyIsControl(true);
				property.setModifiedBy(userId);
				property.setModifiedDate(tsDate);
				Location location=locationController.find(getPropertyLocationId());
				property.setLocation(location);
				LocationType locationType = locationController.findType(getLocationTypeId());
				property.setLocationType(locationType);
				propertyController.edit(property);
				
				UlotelHotelDetailTags tag = propertyController.findHotelTag(getLocationPropertyId()) ;
				if(tag == null){
				UlotelHotelDetailTags tags = new UlotelHotelDetailTags();	
				tags.setHotelProfile("active");
				tags.setRoomCategories("disable");
				tags.setRatePlan("disable");
				tags.setHotelAmenities("disable");
				tags.setHotelPolicy("disable");
				tags.setAddOn("disable");
				tags.setHotelLandmark("disable");
				tags.setHotelStatus("disable");
				tags.setHotelImages("disable");
				tags.setPmsProperty(property);
				propertyController.edit(tags);
				}
				
				if( getCheckedAreas()!=null && !getCheckedAreas().trim().isEmpty()){
					
					
					List<PropertyAreas> listPropertyAreas=propertyAreaController.list(getLocationPropertyId());
					if(listPropertyAreas.size()>0 && !listPropertyAreas.isEmpty()){
						for(PropertyAreas propertyareas:listPropertyAreas){
							PropertyAreas propertyAreas=propertyAreaController.find(propertyareas.getPropertyAreaId());
							propertyAreas.setIsActive(false);
							propertyAreas.setIsDeleted(true);
							propertyArea.setPropertyIsControl(true);
							propertyAreas.setModifiedBy(userId);
							propertyAreas.setModifiedDate(tsDate);
							propertyAreaController.edit(propertyAreas);
						}
					}
					
					
					for (int areaId : intArray) {
						 propertyArea.setIsActive(true);
						 propertyArea.setIsDeleted(false);
						 propertyArea.setModifiedBy(userId);
						 propertyArea.setModifiedDate(tsDate);
						 propertyArea.setPropertyIsControl(true);
						 Area area=areaController.find(areaId);
						 propertyArea.setArea(area);
						 
						 PmsProperty pmspropery=propertyController.find(getLocationPropertyId());
						 propertyArea.setPmsProperty(pmspropery);
						 propertyAreaController.add(propertyArea);
					 }
				}
				 
				
			} catch (Exception e) {
				logger.error(e);
				e.printStackTrace();
			} finally {

			}
			return SUCCESS;
		}
	public String getEditProperty() throws IOException {
		try {
			String jsonOutput = "",jsonAreas="";
			
			HttpServletResponse response = ServletActionContext.getResponse();
			PmsPropertyManager pmsPropertyController = new PmsPropertyManager();
			AreaManager areaController=new AreaManager();
			PropertyAreasManager propertyAreasController=new PropertyAreasManager();
			PropertyAreas propertyAreas=new PropertyAreas();
			
			response.setContentType("application/json");
			List<Area> listAreas=null;
			List<PropertyAreas> listPropertyAreas=null;
			this.propertyList = pmsPropertyController.listEditProperty(getPropertyId());
			for (PmsProperty property : propertyList) {
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				
				jsonOutput += "\"propertyId\":\"" + property.getPropertyId()+ "\"";
				jsonOutput += ",\"propertyName\":\"" + property.getPropertyName()+ "\"";
				jsonOutput += ",\"locationTypeId\":\"" + (property.getLocationType() == null ? "": property.getLocationType().getLocationTypeId()) + "\"";
				jsonOutput += ",\"locationTypeName\":\"" + (property.getLocationType() == null ? "": property.getLocationType().getLocationTypeName())+ "\"";
				jsonOutput += ",\"propertyLocationId\":\"" + property.getLocation().getLocationId()+ "\"";
				jsonOutput += ",\"propertyUrl\":\"" + (property.getPropertyUrl() == null ? "": property.getPropertyUrl().trim())+ "\"";
				jsonOutput += ",\"photoPath\":\"" + property.getPropertyThumbPath()+ "\"";
				
				listAreas=areaController.listAreaByLocation(getPropertyLocationId());
				if(listAreas.size()>0 && !listAreas.isEmpty()){
					for(Area areas:listAreas){
						if (!jsonAreas.equalsIgnoreCase(""))
							
							jsonAreas += ",{";
						else
							jsonAreas += "{";
						
						Area area=areaController.find(areas.getAreaId());
						jsonAreas += "\"areaId\":\"" + area.getAreaId()+ "\"";
						jsonAreas += ",\"areaName\":\"" + area.getAreaName().toUpperCase().trim()+ "\"";
						jsonAreas += ",\"areaDescription\":\"" +(area.getAreaDescription()==null?" ":area.getAreaDescription().toUpperCase().trim())+ "\"";
						listPropertyAreas=propertyAreasController.list(property.getPropertyId(),areas.getAreaId());
						if(listPropertyAreas.size()>0 && !listPropertyAreas.isEmpty()){
							for(PropertyAreas propertyareas:listPropertyAreas){
								int areaId=propertyareas.getArea().getAreaId();
								if(areaId==(int)area.getAreaId()){
									jsonAreas += ",\"status\":\"" + true + "\"";
								}
							}
						}else{
							jsonAreas += ",\"status\":\"" + false + "\"";
						}
						
						
			                jsonAreas += "}";
					}
					  
	                jsonOutput += ",\"areas\":[" + jsonAreas+ "]";
				}
				
				
				jsonOutput += "}";

			}
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}

	public String getPhotoCategory() throws IOException {
		//this.propertyId = (Integer) sessionMap.get("propertyId");
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PmsPhotoCategoryManager photoCategoryController = new PmsPhotoCategoryManager();
			response.setContentType("application/json");
			
			this.categoryList = photoCategoryController.list();
			for (PmsPhotoCategory category : categoryList) {
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				
				
				jsonOutput += "\"photoCategoryId\":\"" + category.getPhotoCategoryId()+ "\"";
				jsonOutput += ",\"photoCategoryName\":\"" + category.getPhotoCategoryName()+ "\"";
				
				
				jsonOutput += "}";


			}
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	public String getRoomPhotoCategory() throws IOException {
		//this.propertyId = (Integer) sessionMap.get("propertyId");
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PmsPhotoCategoryManager photoCategoryController = new PmsPhotoCategoryManager();
			response.setContentType("application/json");
			this.categoryList = photoCategoryController.listRoomTag();
			for (PmsPhotoCategory category : categoryList) {
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				
				
				jsonOutput += "\"photoCategoryId\":\"" + category.getPhotoCategoryId()+ "\"";
				jsonOutput += ",\"photoCategoryName\":\"" + category.getPhotoCategoryName()+ "\"";
				
				
				jsonOutput += "}";

			}
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	public String addPropertyThumbPicture() throws IOException {
		try {
			PmsPropertyManager propertyController=new PmsPropertyManager();
			//Location locations=new Location();
			File fileZip1 = new File(getText("storage.propertythumb.photo") + "/"
					+ getLocationPropertyId() + "/", this.getMyFileFileName());
			FileUtils.copyFile(this.myFile, fileZip1);// copying image in the
			PmsProperty property = propertyController.find(getLocationPropertyId()); 
			property.setPropertyThumbPath(this.getMyFileFileName());
			propertyController.edit(property);
			/*BufferedImage image = ImageIO.read(this.myFile);
			int height = image.getHeight();
			int width = image.getWidth();
			if(height == 221 && width == 214){
				if(this.getMyFileFileName()!=null && !this.getMyFileFileName().equalsIgnoreCase(""))
				{
					File fileZip1 = new File(getText("storage.propertythumb.photo") + "/"
							+ getLocationPropertyId() + "/", this.getMyFileFileName());
					FileUtils.copyFile(this.myFile, fileZip1);// copying image in the
					PmsProperty property = propertyController.find(getLocationPropertyId()); 
					property.setPropertyThumbPath(this.getMyFileFileName());
					propertyController.edit(property);
				}
			}*/
			
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return SUCCESS;
	}
	public String getPropertyThumbPicture() {
		
		//this.propertyId = (Integer) sessionMap.get("propertyId");
		HttpServletResponse response = ServletActionContext.getResponse();

		PmsPropertyManager propertyController = new PmsPropertyManager();
		PmsProperty property = propertyController.find(getPropertyId());
		String imagePath = "";
		// response.setContentType("");
		try {
			
			if ( property.getPropertyThumbPath() != null) {
				imagePath = getText("storage.propertythumb.photo") + "/"
						+ property.getPropertyId()+ "/"
						+ property.getPropertyThumbPath();
				Image img = new Image();
				
				response.getOutputStream().write(img.getCustomImageInBytes(imagePath));
				response.getOutputStream().flush();
			} else {
				imagePath = getText("storage.propertythumb.photo") + "/emptyprofilepic.png";
				Image img = new Image();
				
				response.getOutputStream().write(img.getCustomImageInBytes(imagePath));
				response.getOutputStream().flush();
			}
		} catch (Exception e1) {
			logger.error(e1);
		} finally {

		}
		return null;

	}

	public String addPropertyPicture() throws IOException {
		try {
			PmsPropertyManager propertyController=new PmsPropertyManager();
			//Location locations=new Location();
			File fileZip1 = new File(getText("storage.location.photo") + "/"
					+ getLocationId() + "/", this.getMyFileFileName());
			FileUtils.copyFile(this.myFile, fileZip1);// copying image in the
			PmsProperty property = propertyController.find(getPropertyId()); 
			property.setPropertyThumbPath(this.getMyFileFileName());
			propertyController.edit(property);
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return SUCCESS;
	}
	
	public String getPropertyPicture() {
		
		//this.propertyId = (Integer) sessionMap.get("propertyId");
		HttpServletResponse response = ServletActionContext.getResponse();

		PmsPropertyManager propertyController = new PmsPropertyManager();
		PmsProperty property = propertyController.find(getPropertyId());
		
		String imagePath = "";
		// response.setContentType("");
		try {
			
			if (getPropertyId() >0 ) {
				imagePath = getText("storage.property.photo") + "/"
						+ property.getPropertyId()+ "/"
						+ property.getPropertyThumbPath();
				Image im = new Image();
				response.getOutputStream().write(im.getCustomImageInBytes(imagePath));
				response.getOutputStream().flush();
			} else {
				imagePath = getText("storage.path") + "/emptyprofilepic.png";
				Image im = new Image();
				response.getOutputStream().write(
						im.getCustomImageInBytes(imagePath));
				response.getOutputStream().flush();
			}
		} catch (Exception e1) {
			logger.error(e1);
			e1.printStackTrace();
		} finally {

		}
		return null;

	}
	
	public String getUserProperty(){
		
		try {
			this.propertyId = (Integer) sessionMap.get("propertyId");
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PmsPropertyManager propertyController = new PmsPropertyManager();
			
			response.setContentType("application/json");

			this.propertyList =propertyController.listPropertyByUser(getUser().getUserId());
			for (PmsProperty property : propertyList) {

				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				
				jsonOutput += "\"DT_RowId\":\"" + property.getPropertyId()+ "\"";
				
				jsonOutput += ",\"propertyName\":\"" + property.getPropertyName() + "\"";
			
				jsonOutput += "}";


			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}


	
		return null;
	}
	
	public String getPartnerUserProperty(){
		
		try {
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PmsPropertyManager propertyController = new PmsPropertyManager();
			
			response.setContentType("application/json");
			int count=0;
			this.propertyList =propertyController.listPropertyByUser(getUser().getUserId());
			for (PmsProperty property : propertyList) {

				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				
				jsonOutput += "\"propertyId\":\"" + property.getPropertyId()+ "\"";
				
				jsonOutput += ",\"propertyName\":\"" + property.getPropertyName() + "\"";
				
				//Integer selectedId=(Integer)sessionMap.get("selectedPropertyId");
				/*if(selectedId==null){
					
					sessionMap.put("partnerPropertyId", property.getPropertyId());
				}else{
					jsonOutput += ",\"selectedPropertyId\":\"" + selectedId + "\"";
					sessionMap.put("selectedPropertyId", selectedId);
				}*/
				
				jsonOutput += "}";
				/*count++;
				if(count==1){
					sessionMap.put("partnerPropertyId", property.getPropertyId());
					
				}*/

			}
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}


	
		return null;
	}
	
	public String getSelectedPropertyId(){
		try{
			this.selectedPartnerPropertyId=getSelectedPartnerPropertyId();
			sessionMap.put("selectedPropertyId", this.selectedPartnerPropertyId);
			sessionMap.put("partnerPropertyId",this.selectedPartnerPropertyId);
		}catch(Exception e){
			logger.error(e);
			e.printStackTrace();
		}
		
		return null;
	}
	
	public String addNewPropertyUser() {
		try {
			//PmsGuestManager guestController = new PmsGuestManager();
			this.locationPropertyId = getLocationPropertyId();
			String strUserName=(String) sessionMap.get("userName");
			String strEmailId=(String) sessionMap.get("emailId");
			Integer userId=(Integer) sessionMap.get("userId");
			long time = System.currentTimeMillis();
			java.util.Date date = new java.util.Date(time);
			Timestamp tsDate=new Timestamp(date.getTime());  
			
			PropertyUserManager propertyUserController = new PropertyUserManager();
			UserLoginManager userController = new UserLoginManager();
			PmsPropertyManager propertyController = new PmsPropertyManager();
			PropertyUser propertyUser = new PropertyUser();
			List<User> userList = userController.findUserByRole(2);
			
			for (User us : userList) {
				List<PropertyUser> propertyUserList = propertyUserController.list(this.locationPropertyId,us.getUserId() );
				if(propertyUserList.size()>0){
				}
				else{
					
					
					propertyUser.setUser(us);
					propertyUser.setRole(us.getRole());
					propertyUser.setIsActive(true);
					propertyUser.setIsDeleted(false);
					propertyUser.setCreatedBy(userId);
					propertyUser.setCreatedDate(tsDate);
					PmsProperty property = propertyController.find(this.locationPropertyId);
					propertyUser.setPmsProperty(property);
					propertyUserController.add(propertyUser);
				}
			}
			
			List<User> userRevenueList = userController.findUserByRole(9);// Revenue Role
			
			if(userRevenueList.size()>0 && !userRevenueList.isEmpty()){
				for(User user:userRevenueList){
					
					List<PropertyUser> propertyUserList = propertyUserController.list(this.locationPropertyId,user.getUserId() );
					if(propertyUserList.size()>0){
					}
					else{
						
						propertyUser.setUser(user);
						propertyUser.setRole(user.getRole());
						propertyUser.setIsActive(true);
						propertyUser.setIsDeleted(false);
						propertyUser.setCreatedBy(userId);
						propertyUser.setCreatedDate(tsDate);
						PmsProperty property = propertyController.find(this.locationPropertyId);
						propertyUser.setPmsProperty(property);
						propertyUserController.add(propertyUser);
					}
				}
			}
				
			List<User> userSeoList = userController.findUserByRole(7);// Seo Role
			
			if(userSeoList.size()>0 && !userSeoList.isEmpty()){
				for(User user:userSeoList){
					
					List<PropertyUser> propertyUserList = propertyUserController.list(this.locationPropertyId,user.getUserId() );
					if(propertyUserList.size()>0){
					}
					else{
						
						propertyUser.setUser(user);
						propertyUser.setRole(user.getRole());
						propertyUser.setIsActive(true);
						propertyUser.setIsDeleted(false);
						propertyUser.setCreatedBy(userId);
						propertyUser.setCreatedDate(tsDate);
						PmsProperty property = propertyController.find(this.locationPropertyId);
						propertyUser.setPmsProperty(property);
						propertyUserController.add(propertyUser);
					}
				}
			}
			
			List<User> userFinanceList = userController.findUserByRole(8);// Finance Role
			
			if(userFinanceList.size()>0 && !userFinanceList.isEmpty()){
				for(User user:userFinanceList){
					
					List<PropertyUser> propertyUserList = propertyUserController.list(this.locationPropertyId,user.getUserId() );
					if(propertyUserList.size()>0){
					}
					else{
						
						propertyUser.setUser(user);
						propertyUser.setRole(user.getRole());
						propertyUser.setIsActive(true);
						propertyUser.setIsDeleted(false);
						propertyUser.setCreatedBy(userId);
						propertyUser.setCreatedDate(tsDate);
						PmsProperty property = propertyController.find(this.locationPropertyId);
						propertyUser.setPmsProperty(property);
						propertyUserController.add(propertyUser);
					}
				}
			}
			
			List<User> userReservationList = userController.findUserByRole(6);// Reservation Role
			
			if(userReservationList.size()>0 && !userReservationList.isEmpty()){
				for(User user:userReservationList){
					
					List<PropertyUser> propertyUserList = propertyUserController.list(this.locationPropertyId,user.getUserId() );
					if(propertyUserList.size()>0){
					}
					else{
						
						propertyUser.setUser(user);
						propertyUser.setRole(user.getRole());
						propertyUser.setIsActive(true);
						propertyUser.setIsDeleted(false);
						propertyUser.setCreatedBy(userId);
						propertyUser.setCreatedDate(tsDate);
						PmsProperty property = propertyController.find(this.locationPropertyId);
						propertyUser.setPmsProperty(property);
						propertyUserController.add(propertyUser);
					}
				}
			}

		} catch (Exception e) {
		logger.error(e);
		e.printStackTrace();
		} finally {

		}
		return SUCCESS;
	}
	
	public String updateProperty(){
		try{
			this.propertyId = (Integer) sessionMap.get("propertyId");
			PmsPropertyManager propertyController = new PmsPropertyManager();
			PropertyAreasManager propertyAreaController=new PropertyAreasManager();
			
			PmsProperty property=propertyController.find(this.propertyId);
			property.setPropertyIsControl(true);
			propertyController.edit(property);
			
			
			List<PropertyAreas> listPropertyAreas=propertyAreaController.list(this.propertyId);
			if(listPropertyAreas.size()>0 && !listPropertyAreas.isEmpty()){
				for(PropertyAreas areas:listPropertyAreas){
					int propertyAreaId=areas.getPropertyAreaId();
					PropertyAreas area=propertyAreaController.find(propertyAreaId);
					area.setPropertyIsControl(true);
					propertyAreaController.edit(area);
					
				}
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return null;
	}
	public String addNewProperty() {
		try {
			PmsPropertyManager propertyController = new PmsPropertyManager();
			LocationManager locationController=new LocationManager();
			PropertyAreasManager propertyAreaController=new PropertyAreasManager();
			AreaManager areaController=new AreaManager();
			String[] stringArray = getCheckedAreas().split("\\s*,\\s*");
		    int[] intArray = new int[stringArray.length];
		    if(getCheckedAreas()!=null && !getCheckedAreas().trim().isEmpty()){
		    	for (int i = 0; i < stringArray.length; i++) {
				     String numberAsString = stringArray[i];
				     intArray[i] = Integer.parseInt(numberAsString);
			    }
			}
		    
		    PropertyAreas propertyArea=new PropertyAreas();
		    
			Integer userId=(Integer) sessionMap.get("userId");
			PmsProperty property = new PmsProperty();
			long time = System.currentTimeMillis();
			java.util.Date date = new java.util.Date(time);
			Timestamp tsDate=new Timestamp(date.getTime());  
			
			property.setIsActive(true);
			property.setIsDeleted(false);
			property.setTaxValidFrom(tsDate);
			property.setTaxValidTo(tsDate);
			property.setTaxIsActive(true);
			property.setPropertyIsControl(false);
			property.setPropertyName(getPropertyName());
			property.setPropertyUrl(getPropertyUrl());
			property.setCreatedBy(userId);
			property.setCreatedDate(tsDate);
			Location location=locationController.find(getPropertyLocationId());
			property.setLocation(location);
			LocationType locationType = locationController.findType(getLocationTypeId());
			property.setLocationType(locationType);
			PmsProperty pmsProperty = propertyController.add(property);
			
			this.propertyId = pmsProperty.getPropertyId();
			UlotelHotelDetailTags tags = new UlotelHotelDetailTags();
			
			tags.setHotelProfile("active");
			tags.setRoomCategories("disable");
			tags.setRatePlan("disable");
			tags.setHotelAmenities("disable");
			tags.setHotelPolicy("disable");
			tags.setAddOn("disable");
			tags.setHotelLandmark("disable");
			tags.setHotelStatus("disable");
			tags.setHotelImages("disable");
			tags.setPmsProperty(pmsProperty);
			propertyController.edit(tags);
			
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("application/json");
			
			  if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"locationPropertyId\":\"" + this.propertyId  + "\"";
				
				jsonOutput += "}";


				
				if(getCheckedAreas()!=null && !getCheckedAreas().trim().isEmpty()){
					
					List<PropertyAreas> listPropertyAreas=propertyAreaController.list(getPropertyId());
					if(listPropertyAreas.size()>0 && !listPropertyAreas.isEmpty()){
						for(PropertyAreas propertyareas:listPropertyAreas){
							PropertyAreas propertyAreas=propertyAreaController.find(propertyareas.getPropertyAreaId());
							propertyAreas.setIsActive(false);
							propertyAreas.setIsDeleted(true);
							propertyArea.setPropertyIsControl(true);
							propertyAreas.setCreatedBy(userId);
							propertyAreas.setCreatedDate(tsDate);
							propertyAreaController.edit(propertyAreas);
						}
					}
					
					for (int areaId : intArray) {
						 propertyArea.setIsActive(true);
						 propertyArea.setIsDeleted(false);
						 propertyArea.setCreatedBy(userId);
						 propertyArea.setCreatedDate(tsDate);
						 Area area=areaController.find(areaId);
						 propertyArea.setArea(area);
						 propertyArea.setPropertyIsControl(true);
						 PmsProperty pmspropery=propertyController.find(getPropertyId());
						 propertyArea.setPmsProperty(pmspropery);
						 propertyAreaController.add(propertyArea);
					 }	
				}
				 
			

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}

	public String addPropertyDetails(){
		try{
			this.propertyId = (Integer) sessionMap.get("propertyId");
			
		}catch(Exception ex){
			
		}
		return null;
	}
	
	public String getEnableAccommodation(){
		try{
			this.propertyId = (Integer) sessionMap.get("propertyId");
			PmsPropertyManager propertyController=new PmsPropertyManager();
			UlotelHotelDetailTags tags = propertyController.findHotelTag(getPropertyId());
			if(tags.getRoomCategories().equals("enable")){
			}else{
			tags.setRoomCategories("enable");
			tags.setRatePlan("active");
			propertyController.edit(tags);
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return null;
	}
	
	public String getEnableRate(){
		try{
			this.propertyId = (Integer) sessionMap.get("propertyId");
			PmsPropertyManager propertyController=new PmsPropertyManager();
			UlotelHotelDetailTags tags = propertyController.findHotelTag(getPropertyId());

			if(tags.getRatePlan().equals("enable")){
			}else{
				tags.setRatePlan("enable");
				tags.setHotelAmenities("active");
				propertyController.edit(tags);
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return null;
	}
	
	public String getEnableAmenity(){
		try{
			this.propertyId = (Integer) sessionMap.get("propertyId");
			PmsPropertyManager propertyController=new PmsPropertyManager();
			UlotelHotelDetailTags tags = propertyController.findHotelTag(getPropertyId());
			if(tags.getHotelAmenities().equals("enable")){
			}else{
			tags.setHotelAmenities("enable");
			tags.setHotelPolicy("active");
			propertyController.edit(tags);
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return null;
	}
	
	public String getEnableAddon(){
		try{
			this.propertyId = (Integer) sessionMap.get("propertyId");
			PmsPropertyManager propertyController=new PmsPropertyManager();
			UlotelHotelDetailTags tags = propertyController.findHotelTag(getPropertyId());
			if(tags.getAddOn().equals("enable")){
			}else{
			tags.setAddOn("enable");
			tags.setHotelLandmark("active");
			propertyController.edit(tags);
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return null;
	}
	
	public String getEnableImage(){
		try{
			this.propertyId = (Integer) sessionMap.get("propertyId");
			PmsPropertyManager propertyController=new PmsPropertyManager();
			UlotelHotelDetailTags tags = propertyController.findHotelTag(getPropertyId());
			if(tags.getHotelImages().equals("enable")){
			}else{
			tags.setHotelImages("enable");
			tags.setHotelStatus("active");
			propertyController.edit(tags);
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return null;
	}

	
	
	public String getPropertyAddonDetails(){
		try{
			this.propertyId = (Integer) sessionMap.get("propertyId");
			PmsPropertyManager propertyController=new PmsPropertyManager();
			
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("application/json");
			PropertyAddonDetailManager addonDetailController=new PropertyAddonDetailManager();
			
			List<PropertyAddonDetails> listAddonDetails=null;
			listAddonDetails=addonDetailController.list(this.propertyId);
			if(listAddonDetails.size()>0 && !listAddonDetails.isEmpty()){
				for(PropertyAddonDetails addonDetails:listAddonDetails){
					if (!jsonOutput.equalsIgnoreCase(""))
						
						jsonOutput += ",{";
					else
						jsonOutput += "{";
					
					jsonOutput += "\"propertyAddonDetailId\":\"" + addonDetails.getPropertyAddonDetailId()+ "\"";
					jsonOutput += ",\"propertyAddonId\":\"" + addonDetails.getPropertyAddon().getPropertyAddonId()+ "\"";
					jsonOutput += ",\"addonName\":\"" +  addonDetails.getPropertyAddon().getPropertyAddonName()+ "\"";
					jsonOutput += ",\"addonRate\":\"" +  addonDetails.getAddonRate()+ "\"";
					jsonOutput += ",\"addonIsActive\":\"" +  addonDetails.getAddonIsActive()+ "\"";
					jsonOutput += ",\"addonPropertyId\":\"" +  addonDetails.getPmsProperty().getPropertyId()+ "\"";
					
					jsonOutput += "}";
				}
			}
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");	
		}catch(Exception ex){
			
		}
		return null;
	}
	
	public String getGuestPropertyAddonDetails(){
		try{
			this.propertyId = getPropertyId();
			PmsPropertyManager propertyController=new PmsPropertyManager();
			
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("application/json");
			PropertyAddonDetailManager addonDetailController=new PropertyAddonDetailManager();
			PropertyAddonManager addonController = new PropertyAddonManager();
			List<PropertyAddon> addonList = addonController.list();
//			for(PropertyAddon addon : addonList){
				
				
				
			List<PropertyAddonDetails> listAddonDetails=null;
			listAddonDetails=addonDetailController.propertyAddonList(this.propertyId);
			if(listAddonDetails.size()>0 && !listAddonDetails.isEmpty()){
				for(PropertyAddonDetails addonDetails:listAddonDetails){
					
					if (!jsonOutput.equalsIgnoreCase(""))
						
						jsonOutput += ",{";
					else
						jsonOutput += "{";
						
					jsonOutput += "\"propertyAddonId\":\"" + addonDetails.getPropertyAddon().getPropertyAddonId()+ "\"";
					jsonOutput += ",\"addonName\":\"" +  addonDetails.getPropertyAddon().getPropertyAddonName()+ "\"";
					jsonOutput += ",\"propertyAddonDetailId\":\"" + addonDetails.getPropertyAddonDetailId()+ "\"";
					jsonOutput += ",\"addonRate\":\"" +  addonDetails.getAddonRate()+ "\"";
					jsonOutput += ",\"addonIsActive\":\"" +  addonDetails.getAddonIsActive()+ "\"";
					jsonOutput += ",\"addonPropertyId\":\"" +  addonDetails.getPmsProperty().getPropertyId()+ "\"";
					jsonOutput += "}";
					
				}
			}
			
//			}
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");	
		}catch(Exception ex){
			
		}
		return null;
	}
	
	public String deleteAddon(){
		try{
			this.propertyId = (Integer) sessionMap.get("propertyId");
			
			Integer userId=(Integer) sessionMap.get("userId");
			long time = System.currentTimeMillis();
			java.util.Date date = new java.util.Date(time);
			Timestamp tsDate=new Timestamp(date.getTime());  
			
			PropertyAddonManager addonController=new PropertyAddonManager();
			PropertyAddonDetailManager addonDetailController=new PropertyAddonDetailManager();
			
			List<PropertyAddonDetails> listPropertyAddon=addonDetailController.list(getPropertyAddonId(), this.propertyId);
			if(listPropertyAddon.size()>0 && !listPropertyAddon.isEmpty()){
				for(PropertyAddonDetails addonDetails:listPropertyAddon){
					int addonDetailId=addonDetails.getPropertyAddonDetailId();
					PropertyAddonDetails addonDetail=addonDetailController.find(addonDetailId);
					addonDetail.setIsActive(false);
					addonDetail.setIsDeleted(true);
					addonDetail.setModifiedBy(userId);
					addonDetail.setModifiedOn(tsDate);
					addonDetailController.edit(addonDetail);
				}
			}
			
			PropertyAddon addon=addonController.find(getPropertyAddonId());
			addon.setIsActive(false);
			addon.setIsDeleted(true);
			addon.setModifiedBy(userId);
			addon.setModifiedOn(tsDate);
			addonController.edit(addon);
			
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return null;
	}
	
	public String addAddon(){
		try{
			this.propertyId = (Integer) sessionMap.get("propertyId");
			Integer userId=(Integer) sessionMap.get("userId");
			
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("application/json");
			
			PropertyAddonManager addonController=new PropertyAddonManager();
			PropertyAddon addon =new PropertyAddon();
			if(getAddonName()!=null){
				addon.setPropertyAddonName(getAddonName().trim());	
			}else{
				addon.setPropertyAddonName(getAddonName());
			}
			
			addon.setIsActive(true);
			addon.setIsDeleted(false);
			addon.setCreatedBy(userId);
			addon.setCreatedOn(new java.sql.Timestamp(date.getTime()));
			PropertyAddon propertyaddon=addonController.add(addon);
			
			int addonId=propertyaddon.getPropertyAddonId();
			
			sessionMap.put("addonId",addonId);
			
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return null;
	}
	
	public String addAddondetail(){
		try{
			this.propertyId = (Integer) sessionMap.get("propertyId");
			Integer userId=(Integer) sessionMap.get("userId");
			Integer addonId = (Integer) sessionMap.get("addonId");
			PmsPropertyManager propertyController=new PmsPropertyManager();
			
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			PropertyAddonDetailManager addonDetailController=new PropertyAddonDetailManager();
			PropertyAddonManager addonController=new PropertyAddonManager();
			PropertyAddonDetails addonDetails=new PropertyAddonDetails();
			
			addonDetails.setIsActive(true);
			addonDetails.setIsDeleted(false);
			addonDetails.setAddonRate(getAddonRate());
			addonDetails.setCreatedBy(userId);
			addonDetails.setAddonIsActive(getAddonIsActive());
			
			addonDetails.setCreatedOn(new java.sql.Timestamp(date.getTime()));
			PmsProperty pmsProperty=propertyController.find(this.propertyId);
			addonDetails.setPmsProperty(pmsProperty);
			
			PropertyAddon propertyAddon=addonController.find(addonId);
			addonDetails.setPropertyAddon(propertyAddon);
			addonDetailController.add(addonDetails);
			
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return null;
	}
	
	public String editAddon(){
		try{
			this.propertyId = (Integer) sessionMap.get("propertyId");
			Integer userId=(Integer) sessionMap.get("userId");
			
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			
			PropertyAddonManager addonController=new PropertyAddonManager();
			PropertyAddon addon =addonController.find(getPropertyAddonId());
			if(getAddonName()!=null){
				addon.setPropertyAddonName(getAddonName().trim());	
			}else{
				addon.setPropertyAddonName(getAddonName());
			}
			
			addon.setIsActive(true);
			addon.setIsDeleted(false);
			addon.setModifiedBy(userId);
			addon.setModifiedOn(new java.sql.Timestamp(date.getTime()));
			PropertyAddon propertyaddon=addonController.add(addon);
			
			int addonId=propertyaddon.getPropertyAddonId();
			
			sessionMap.put("editAddonId",addonId);
			
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return null;
	}
	
	public String editAddondetail(){
		try{
			this.propertyId = (Integer) sessionMap.get("propertyId");
			Integer userId=(Integer) sessionMap.get("userId");
			Integer addonId = (Integer) sessionMap.get("editAddonId");
			PmsPropertyManager propertyController=new PmsPropertyManager();
			
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			PropertyAddonDetailManager addonDetailController=new PropertyAddonDetailManager();
			PropertyAddonManager addonController=new PropertyAddonManager();
			if(getPropertyAddonDetailId() != null){
				PropertyAddonDetails addonDetails=addonDetailController.find(getPropertyAddonDetailId());
				
				addonDetails.setIsActive(true);
				addonDetails.setIsDeleted(false);
				addonDetails.setAddonRate(getAddonRate());
				addonDetails.setCreatedBy(userId);
				addonDetails.setAddonIsActive(getAddonIsActive());
				
				addonDetails.setCreatedOn(new java.sql.Timestamp(date.getTime()));
				
				PmsProperty pmsProperty=propertyController.find(getAddonPropertyId());
				addonDetails.setPmsProperty(pmsProperty);
				
				PropertyAddon propertyAddon=addonController.find(getPropertyAddonId());
				addonDetails.setPropertyAddon(propertyAddon);
				addonDetailController.add(addonDetails);	
			}else{
				PropertyAddonDetails addonDetail = new PropertyAddonDetails();
				PropertyAddon propertyAddon=addonController.find(getPropertyAddonId());
				addonDetail.setPropertyAddon(propertyAddon);
				PmsProperty pmsProperty=propertyController.find(this.propertyId);
				addonDetail.setPmsProperty(pmsProperty);
				addonDetail.setIsActive(true);
				addonDetail.setIsDeleted(false);
				addonDetail.setCreatedBy(userId);
				addonDetail.setCreatedOn(new java.sql.Timestamp(date.getTime()));
				addonDetail.setAddonRate(getAddonRate());
				addonDetail.setAddonIsActive(getAddonIsActive());
				addonDetailController.add(addonDetail);
			}
			
			
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return null;
	}
	
	
	public String getAllPropertyDetails(){
		try{
			this.propertyId = (Integer) sessionMap.get("propertyId");
			PropertyRoleDetailManager roleDetailController=new PropertyRoleDetailManager();
			List<PropertyRoleDetails> listRoleDetails=roleDetailController.list(propertyId);
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("application/json");
			if(listRoleDetails.size()>0 && !listRoleDetails.isEmpty()){
				for(PropertyRoleDetails roleDetails:listRoleDetails){
					
					if (!jsonOutput.equalsIgnoreCase(""))
						
						jsonOutput += ",{";
					else
						jsonOutput += "{";
						
						jsonOutput += "\"propertyRoleDetailId\":\"" + roleDetails.getPropertyRoleDetailId()+ "\"";
						jsonOutput += ",\"propertyRoleName\":\"" + roleDetails.getPropertyRoleName()+ "\"";
						jsonOutput += ",\"propertyRoleEmail\":\"" +  roleDetails.getPropertyRoleEmail()+ "\"";
						jsonOutput += ",\"propertyRoleContact\":\"" +  roleDetails.getPropertyRoleContact()+ "\"";
						jsonOutput += ",\"propertyRoleId\":\"" +  roleDetails.getPropertyRoles().getPropertyRoleId()+ "\"";
						jsonOutput += ",\"rolePropertyId\":\"" +  roleDetails.getPmsProperty().getPropertyId()+ "\"";
						
					jsonOutput += "}";
				}
			}
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");	
			
		}catch(Exception ex){
			
		}
		return null;
	}
	
	public String addProperty() {
		
		try {
			
			this.propertyId = (Integer) sessionMap.get("propertyId");
			PmsPropertyManager propertyController = new PmsPropertyManager();
			UserLoginManager  userController = new UserLoginManager();			
			PmsProperty property = new PmsProperty();
			
			
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			property.setAddress1(getAddress1());
			property.setAddress2(getAddress2());
			property.setCity(getCity());
			property.setIsActive(true);
			property.setIsDeleted(false);
			property.setLatitude(getLatitude());
			property.setLongitude(getLongitude());
			property.setPlaceId(getPlaceId());
			property.setPropertyContact(getPropertyContact());
			property.setPropertyDescription(getPropertyDescription());
			property.setPropertyEmail(getPropertyEmail());
			property.setPropertyName(getPropertyName());
			property.setReservationManagerEmail(getReservationManagerEmail());
			property.setReservationManagerName(getReservationManagerName());
			property.setReservationManagerContact(getReservationManagerContact());
			property.setResortManagerEmail(getResortManagerEmail());
			property.setResortManagerName(getResortManagerName());
			property.setResortManagerContact(getResortManagerContact());
			property.setContractManagerName(getContractManagerName());
			property.setContractManagerEmail(getContractManagerEmail());
			property.setContractManagerContact(getContractManagerContact());
			property.setRevenueManagerName(getRevenueManagerName());
			property.setRevenueManagerEmail(getRevenueManagerEmail());
			property.setRevenueManagerContact(getRevenueManagerContact());
			property.setZipCode(getZipCode());		
			property.setPropertyGstNo(getPropertyGstNo());
			property.setPropertyFinanceEmail(getPropertyFinanceEmail());
			propertyController.add(property);
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return SUCCESS;
	}
	
	public String editProperty() {
		try {
			
			this.propertyId = (Integer) sessionMap.get("propertyId");
			PmsPropertyManager propertyController = new PmsPropertyManager();
			PropertyTypeManager propertyTypeController = new PropertyTypeManager();
			PropertyContractTypeManager contractController=new PropertyContractTypeManager();
			StateManager stateController = new StateManager();
			CountryManager countryController = new CountryManager();
			UserLoginManager  userController = new UserLoginManager();
			DateFormat format = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
		    /*java.util.Date dateStart = format.parse(getStrTaxValidFrom());
		    java.util.Date dateEnd = format.parse(getStrTaxValidTo());
		   */
		    
			
		   // String description = getPropertyDescription().replaceAll("ands", "&");
		    
		    /*Calendar calCheckStart=Calendar.getInstance();
		    calCheckStart.setTime(dateStart);
		    java.util.Date checkInDate = calCheckStart.getTime();
		    this.taxValidFrom=new Timestamp(checkInDate.getTime());
		    sessionMap.put("taxValidFrom",taxValidFrom); 
		    
		    Calendar calCheckEnd=Calendar.getInstance();
		    calCheckEnd.setTime(dateEnd);
		    java.util.Date checkOutDate = calCheckEnd.getTime();
		    this.taxValidTo=new Timestamp(checkOutDate.getTime());
		    sessionMap.put("taxValidTo",taxValidTo); 
			*/
			
			
			PmsProperty property = propertyController.find(getPropertyId());
			PropertyType propertyType = propertyTypeController.find(getPropertyTypeId());
			
			Integer userId=(Integer) sessionMap.get("userId");
			long time = System.currentTimeMillis();
			java.util.Date date = new java.util.Date(time);
			Timestamp tsDate=new Timestamp(date.getTime());  
			
			property.setAddress1(getAddress1());
			property.setAddress2(getAddress2());
			property.setCity(getCity());
			property.setIsActive(true);
			property.setIsDeleted(false);
			property.setLatitude(getLatitude());
			property.setLongitude(getLongitude());
			
			property.setPropertyContact(getPropertyContact());
			State state = stateController.find(getStateCode());
			property.setState(state);
			Country country = countryController.find(getCountryCode());
			property.setCountry(country);
			property.setPropertyEmail(getPropertyEmail().trim());
			property.setPropertyName(getPropertyName().trim());
			property.setZipCode(getZipCode());	
			property.setPropertyType(propertyType);	
			property.setReservationManagerEmail(getReservationManagerEmail().trim());
			property.setResortManagerEmail(getResortManagerEmail().trim());
			property.setResortManagerName(getResortManagerName().trim());
			property.setResortManagerContact(getResortManagerContact().trim());
			property.setResortManagerDesignation(getResortManagerDesignation().trim());
			property.setResortManagerEmail1(getResortManagerEmail1().trim());
			property.setResortManagerName1(getResortManagerName1().trim());
			property.setResortManagerContact1(getResortManagerContact1().trim());
			property.setResortManagerDesignation1(getResortManagerDesignation1().trim());
			property.setResortManagerEmail2(getResortManagerEmail2().trim());
			property.setResortManagerName2(getResortManagerName2().trim());
			property.setResortManagerContact2(getResortManagerContact2().trim());
			property.setResortManagerDesignation2(getResortManagerDesignation2().trim());
			property.setContractManagerName(getContractManagerName().trim());
			property.setContractManagerEmail(getContractManagerEmail().trim());
			property.setContractManagerContact(getContractManagerContact().trim());
			property.setRevenueManagerName(getRevenueManagerName().trim());
			property.setRevenueManagerEmail(getRevenueManagerEmail().trim());
			property.setRevenueManagerContact(getRevenueManagerContact().trim());
			property.setRouteMap(getRouteMap());
			property.setModifiedBy(userId);
			property.setModifiedDate(tsDate);
			property.setPropertyGstNo(getPropertyGstNo());
			property.setPropertyReviewLink(getPropertyReviewLink());
			property.setTripadvisorReviewLink(getTripadvisorReviewLink());
			property.setPayAtHotelStatus(getPayAtHotelStatus());
			property.setBreakfastIsActive(true);
			String[] stringArray = getCheckedPropertyTag().split("\\s*,\\s*");
			String strArrays=Arrays.toString(stringArray);
		    if(!strArrays.contains("NA")){
		    	int[] intArray = new int[stringArray.length];
			    for (int i = 0; i < stringArray.length; i++) {
			    	String numberAsString = stringArray[i];
			    	intArray[i] = Integer.parseInt(numberAsString);
			    }
			    
			    List<PropertyTags>  propertyTag = propertyController.findTag(property.getPropertyId());
		    	  if(propertyTag.size() != 0){
		    		  for(PropertyTags tag : propertyTag){
		    			  tag.setIsActive(false);
		    			  tag.setIsDeleted(true);
		    			  tag.setModifiedBy(userId);
		    			  tag.setModifiedDate(tsDate);
		    			  propertyController.edit(tag);
		    		  }
		    	  }
			      
			      for (int number : intArray) {
			    	  PropertyTags propertyTags = new PropertyTags();
		    		  PmsTags tags = propertyController.findTagId(number);
		    		  propertyTags.setPmsProperty(property); 
		    		  propertyTags.setPmsTags(tags);
		    		  propertyTags.setCreatedBy(userId);
		    		  propertyTags.setCreatedDate(tsDate);
		    		  propertyTags.setIsActive(true);
		    		  propertyTags.setIsDeleted(false);
		    		  propertyController.add(propertyTags);	 
			    		  
			    	  
			      }
			    
		    }
		    propertyController.edit(property);
		    UlotelHotelDetailTags hotelTag = propertyController.findHotelTag(this.propertyId);
		    if(hotelTag.getHotelProfile().equals("enable")){
			}else{
		    hotelTag.setHotelProfile("enable");
		    hotelTag.setRoomCategories("active");
		    propertyController.edit(hotelTag);
		    }
		    
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}
	
	public String savePropertyStatus() {
		
		try{
			this.propertyId = (Integer) sessionMap.get("propertyId");
			PmsPropertyManager propertyController = new PmsPropertyManager();
			PmsProperty property = propertyController.find(getPropertyId());
			java.util.Date date=new java.util.Date();
        	Calendar calDate=Calendar.getInstance();
        	calDate.setTime(date);
        	calDate.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
        	int bookedHours=calDate.get(Calendar.HOUR);
        	int bookedMinute=calDate.get(Calendar.MINUTE);
        	int bookedSecond=calDate.get(Calendar.SECOND);
        	int AMPM=calDate.get(Calendar.AM_PM);
        	DateFormat f = new SimpleDateFormat("EEEE");
        	String strBookingDate=new SimpleDateFormat("MMM d, yyyy", Locale.ENGLISH).format(calDate.getTime());
        	String hours="",minutes="",seconds="";
        	if(bookedHours<10){
        		hours=String.valueOf(bookedHours);
        		hours="0"+hours;
        	}else{
        		hours=String.valueOf(bookedHours);
        	}
        	if(bookedMinute<10){
        		minutes=String.valueOf(bookedMinute);
        		minutes="0"+minutes;
        	}else{
        		minutes=String.valueOf(bookedMinute);
        	}
        	
        	if(bookedMinute<10){
        		seconds=String.valueOf(bookedSecond);
        		seconds="0"+seconds;
        	}else{
        		seconds=String.valueOf(bookedSecond);
        	}
        	if(AMPM==0){//If the current time is AM
        		this.timeHours=f.format(calDate.getTime())+","+strBookingDate+" at "+hours+" : "+minutes+" AM ";
        	}else if(AMPM==1){//If the current time is PM
        		this.timeHours=f.format(calDate.getTime())+","+strBookingDate+" at "+hours+" : "+minutes+" PM ";
        	}
			property.setHotelIsActive(getHotelIsActive());
			if(getHotelIsActive()==true){
				property.setPropertyIsControl(true);
			}else if(getHotelIsActive()==false){
				property.setPropertyIsControl(false);
			}
			property.setStopSellIsActive(getStopSellIsActive());
			property.setDelistIsActive(getDelistIsActive());
			propertyController.edit(property);
			
			String strContractMailId=null,strRevenueMailId=null;
				PmsPropertyManager pmsPropertyController=new PmsPropertyManager();
				PmsProperty pmsProperty=pmsPropertyController.find(this.propertyId);
				
				this.reservationManagerEmail=pmsProperty.getReservationManagerEmail();
				this.propertyEmail=pmsProperty.getPropertyEmail();
				strRevenueMailId=pmsProperty.getRevenueManagerEmail();
				strContractMailId=pmsProperty.getContractManagerEmail();
				this.propertyUrl=pmsProperty.getPropertyUrl();
				this.resortManagerEmail=pmsProperty.getResortManagerEmail();
				this.resortManagerEmail1=pmsProperty.getResortManagerEmail1();
				this.resortManagerEmail2=pmsProperty.getResortManagerEmail2();
				this.resortManagerEmail3=pmsProperty.getResortManagerEmail3();
				
				ArrayList<String> arlEmail=new ArrayList<String>();
				if(this.resortManagerEmail!=null && !this.resortManagerEmail.isEmpty()){
					arlEmail.add(this.resortManagerEmail);
				}
				if(this.resortManagerEmail1!=null && !this.resortManagerEmail1.isEmpty()){
					arlEmail.add(this.resortManagerEmail1);
				}
				if(this.resortManagerEmail2!=null && !this.resortManagerEmail2.isEmpty()){
					arlEmail.add(this.resortManagerEmail2);
				}
				if(this.resortManagerEmail3!=null && !this.resortManagerEmail3.isEmpty()){
					arlEmail.add(this.resortManagerEmail3);
				}
				StringTokenizer stoken=new StringTokenizer(this.reservationManagerEmail,",");
				while(stoken.hasMoreElements()){
					arlEmail.add(stoken.nextToken());
				}
				
				StringTokenizer stoken2=new StringTokenizer(this.propertyEmail,",");
				while(stoken2.hasMoreElements()){
					arlEmail.add(stoken2.nextToken());
				}
			
			
				StringTokenizer stoken3=new StringTokenizer(strRevenueMailId,",");
				while(stoken3.hasMoreElements()){
					arlEmail.add(stoken3.nextToken());
				}
				
				StringTokenizer stoken4=new StringTokenizer(strContractMailId,",");
				while(stoken4.hasMoreElements()){
					arlEmail.add(stoken4.nextToken());
				}
				arlEmail.add(getText("bookings.notification.email"));
				arlEmail.add(getText("operations.notification.email"));
				arlEmail.add(getText("finance.notification.email"));
				
 				Integer emailSize=arlEmail.size();
			//email template
			Configuration cfg = new Configuration();
			cfg.setClassForTemplateLoading(PropertyAction.class, "../../../");
			Template template = cfg.getTemplate(getText("propertystatus.template"));
			Map<String, String> rootMap = new HashMap<String, String>();
			rootMap.put("propertyName",property.getPropertyName());
			rootMap.put("createdDate",this.timeHours);
			
			rootMap.put("from", getText("notification.from"));
			Writer out = new StringWriter();
			template.process(rootMap, out);
//			System.out.println(emailSize+"....."+arlEmail);
			//send email
			int i=0;
			String emailArray[]=new String[emailSize];
			Email em = new Email();
			Iterator<String> iterEmail=arlEmail.iterator();
			while(iterEmail.hasNext()){
				emailArray[i]=iterEmail.next().toLowerCase();
				i++;
			}
				
			em.set_from(getText("email.from"));
			em.setEmailArray(emailArray);
			em.set_username(getText("email.username"));
			em.set_subject(getText("partnerwithus.subject"));
			em.set_bodyContent(out);
			em.sendPartnerVouchers();
			
		}catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}
	
	public String editPropertyPolicy() {
		
		
		try {
			this.propertyId = (Integer) sessionMap.get("propertyId");
			PmsPropertyManager propertyController = new PmsPropertyManager();
			
			Integer userId=(Integer) sessionMap.get("userId");
			long time = System.currentTimeMillis();
			java.util.Date date = new java.util.Date(time);
			Timestamp tsDate=new Timestamp(date.getTime());  
			
			PmsProperty property = propertyController.find(getPropertyId());
			
			String hotelPolicy = getPropertyHotelPolicy().replace("percent","%").replaceAll("\\sand"," &");
			String cancellationPolicy = getPropertyCancellationPolicy().replace("percent","%").replaceAll("\\sand"," &");
			String refundPolicy = getPropertyRefundPolicy().replace("percent","%").replaceAll("\\sand"," &");
			String gigiPolicy = getPropertyGigiPolicy().replace("percent","%").replaceAll("\\sand"," &");
			property.setPropertyHotelPolicy(hotelPolicy.trim());
			property.setPropertyStandardPolicy(cancellationPolicy.trim());
			property.setPropertyCancellationPolicy(refundPolicy.trim());
			property.setPropertyGigiPolicy(gigiPolicy.trim());
			property.setModifiedBy(userId);
			property.setModifiedDate(tsDate);
			/*property.setPropertyChildPolicy(getPropertyChildPolicy().trim());*/
			
			propertyController.edit(property);
			
			UlotelHotelDetailTags tags = propertyController.findHotelTag(getPropertyId());
			 if(tags.getHotelPolicy().equals("enable")){
				}else{
			tags.setHotelPolicy("enable");
			tags.setAddOn("active");
			propertyController.edit(tags);
			}
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}
	
	public String getPromotionProperty(){
		try {
 			String jsonOutput = "";
 			HttpServletResponse response = ServletActionContext.getResponse();
 			response.setContentType("application/json");
 			PmsPropertyManager propertyController=new PmsPropertyManager();
 			List<PmsProperty> listProperty =propertyController.listProperty();
 			if(listProperty.size()>0 && !listProperty.isEmpty()){
 				for(PmsProperty property:listProperty){
 					if (!jsonOutput.equalsIgnoreCase(""))
 	 					jsonOutput += ",{";
 	 				else
 	 					jsonOutput += "{";
 	 				
 					jsonOutput += "\"propertyId\":\"" + property.getPropertyId()+ "\"";
 					jsonOutput += ",\"propertyName\":\"" + property.getPropertyName()+ "\"";
 	 			    
 	 				jsonOutput += "}";
 				}
 			}
 			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
 		} catch (Exception e) {
 			logger.error(e);
 			e.printStackTrace();
 		} finally {

 		}
		return null;
	}
	
	public String getActivePromotionProperty(){
		try {
 			String jsonOutput = "";
 			HttpServletResponse response = ServletActionContext.getResponse();
 			response.setContentType("application/json");
 			PmsPropertyManager propertyController=new PmsPropertyManager();
 			List<PmsProperty> listProperty =propertyController.listProperty();
 			if(listProperty.size()>0 && !listProperty.isEmpty()){
 				for(PmsProperty property:listProperty){
 					if (!jsonOutput.equalsIgnoreCase(""))
 	 					jsonOutput += ",{";
 	 				else
 	 					jsonOutput += "{";
 	 				
 					jsonOutput += "\"propertyId\":\"" + property.getPropertyId()+ "\"";
 					jsonOutput += ",\"propertyName\":\"" + property.getPropertyName()+ "\"";
 	 			    
 	 				jsonOutput += "}";
 				}
 			}
 			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
 		} catch (Exception e) {
 			logger.error(e);
 			e.printStackTrace();
 		} finally {

 		}
		return null;
	}
	
	public String getActiveProperties(){

		try {
 			String jsonOutput = "";
 			HttpServletResponse response = ServletActionContext.getResponse();
 			response.setContentType("application/json");
 			PmsPropertyManager propertyController=new PmsPropertyManager();
 			List<PmsProperty> listProperty =propertyController.listProperty();
 			if(listProperty.size()>0 && !listProperty.isEmpty()){
 				for(PmsProperty property:listProperty){
 					if (!jsonOutput.equalsIgnoreCase(""))
 	 					jsonOutput += ",{";
 	 				else
 	 					jsonOutput += "{";
 	 				
 					jsonOutput += "\"propertyId\":\"" + property.getPropertyId()+ "\"";
 					jsonOutput += ",\"propertyName\":\"" + property.getPropertyName()+ "\"";
 	 			    
 	 				jsonOutput += "}";
 				}
 			}
 			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
 		} catch (Exception e) {
 			logger.error(e);
 			e.printStackTrace();
 		} finally {

 		}
		return null;
	
	}
	public String getPropertyPayAtHotelStatus() {
		try {
			PmsPropertyManager propertyController = new PmsPropertyManager();
			PmsProperty property = propertyController.find(getPropertyId());
					
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("application/json");
			
			  if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"payAtHotelStatus\":\"" + ( property.getPayAtHotelStatus() == null? "false ":  property.getPayAtHotelStatus() )  + "\"";
				
				jsonOutput += "}";
				
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");	
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}
	
	public String editPropertyLandmark() {
		
		this.propertyId = (Integer) sessionMap.get("propertyId");
		PropertyLandmarkManager propertyLandmarkController = new PropertyLandmarkManager();
		PmsPropertyManager propertyController = new PmsPropertyManager();
		try {
			//PropertyLandmark landmark = new PropertyLandmark();
			//this.propertyContentList = propertyContentController.list(getPropertyId());
			
			this.propertyLandmarkList = propertyLandmarkController.list(getPropertyId()); 
			if(propertyLandmarkList.size() > 0 && !propertyLandmarkList.isEmpty()){
				
			for(PropertyLandmark landmark : propertyLandmarkList)
			{
				landmark.setKilometers1(getKilometers1());
				landmark.setKilometers2(getKilometers2());
				landmark.setKilometers3(getKilometers3());
				landmark.setKilometers4(getKilometers4());
				landmark.setKilometers5(getKilometers5());
//				landmark.setKilometers6(getKilometers6());
//				landmark.setKilometers7(getKilometers7());
//				landmark.setKilometers8(getKilometers8());
//				landmark.setKilometers9(getKilometers9());
//				landmark.setKilometers10(getKilometers10());
				landmark.setDistance1(getDistance1());
				landmark.setDistance2(getDistance2());
				landmark.setDistance3(getDistance3());
				landmark.setDistance4(getDistance4());
				landmark.setDistance5(getDistance5());
				landmark.setPropertyLandmark1(getPropertyLandmark1());
				landmark.setPropertyLandmark2(getPropertyLandmark2());
				landmark.setPropertyLandmark3(getPropertyLandmark3());
				landmark.setPropertyLandmark4(getPropertyLandmark4());
				landmark.setPropertyLandmark5(getPropertyLandmark5());
//				landmark.setPropertyLandmark6(getPropertyLandmark6());
//				landmark.setPropertyLandmark7(getPropertyLandmark7());
//				landmark.setPropertyLandmark8(getPropertyLandmark8());
//				landmark.setPropertyLandmark9(getPropertyLandmark9());
//				landmark.setPropertyLandmark10(getPropertyLandmark10());
				landmark.setPropertyAttraction1(getPropertyAttraction1());
				landmark.setPropertyAttraction2(getPropertyAttraction2());
				landmark.setPropertyAttraction3(getPropertyAttraction3());
				landmark.setPropertyAttraction4(getPropertyAttraction4());
				landmark.setPropertyAttraction5(getPropertyAttraction5());
				landmark.setIsActive(true);
				landmark.setIsDeleted(false);
				
				PmsProperty property = propertyController.find(getPropertyId());
				landmark.setPmsProperty(property);
				propertyLandmarkController.edit(landmark);
			}
			}
			else{
				PropertyLandmark landmark = new PropertyLandmark();
				
				landmark.setKilometers1(getKilometers1());
				landmark.setKilometers2(getKilometers2());
				landmark.setKilometers3(getKilometers3());
				landmark.setKilometers4(getKilometers4());
				landmark.setKilometers5(getKilometers5());
				landmark.setKilometers6(getKilometers6());
				landmark.setKilometers7(getKilometers7());
				landmark.setKilometers8(getKilometers8());
				landmark.setKilometers9(getKilometers9());
				landmark.setKilometers10(getKilometers10());
				landmark.setPropertyLandmark1(getPropertyLandmark1());
				landmark.setPropertyLandmark2(getPropertyLandmark2());
				landmark.setPropertyLandmark3(getPropertyLandmark3());
				landmark.setPropertyLandmark4(getPropertyLandmark4());
				landmark.setPropertyLandmark5(getPropertyLandmark5());
				landmark.setPropertyLandmark6(getPropertyLandmark6());
				landmark.setPropertyLandmark7(getPropertyLandmark7());
				landmark.setPropertyLandmark8(getPropertyLandmark8());
				landmark.setPropertyLandmark9(getPropertyLandmark9());
				landmark.setPropertyLandmark10(getPropertyLandmark10());
				landmark.setIsActive(true);
				landmark.setIsDeleted(false);
				
				PmsProperty property = propertyController.find(getPropertyId());
				landmark.setPmsProperty(property);
				propertyLandmarkController.edit(landmark);
				
			}
			
			UlotelHotelDetailTags tags = propertyController.findHotelTag(getPropertyId());
			if(tags.getHotelLandmark().equals("enable")){
			}else{
			tags.setHotelLandmark("enable");
			tags.setHotelImages("active");
			propertyController.edit(tags);
			}
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}
		
		public String getPropertyLandmark() throws IOException {
			
			this.propertyId = (Integer) sessionMap.get("propertyId");
			 
			try {

				String jsonOutput = "";
				HttpServletResponse response = ServletActionContext.getResponse();
			
				PropertyLandmarkManager propertyLandmarkController = new PropertyLandmarkManager();
				PmsPropertyManager propertyController = new PmsPropertyManager();
				response.setContentType("application/json");
				List<PropertyLandmark> listPropertyLandmark=propertyLandmarkController.list(getPropertyId());
				if(listPropertyLandmark.size()>0 && !listPropertyLandmark.isEmpty()){
					
					PropertyLandmark landmark = propertyLandmarkController.find(getPropertyId()); 	
					
					if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += ",{";
					else
						jsonOutput += "{";
					
					jsonOutput += "\"propertyLandmarkId\":\"" + (landmark.getPropertyLandmarkId() == 0 ? "":landmark.getPropertyLandmarkId()) + "\"";
					jsonOutput += ",\"kilometers1\":\"" + (landmark.getKilometers1() == 0? "" :landmark.getKilometers1())+ "\"";
					jsonOutput += ",\"kilometers2\":\"" + (landmark.getKilometers2() == 0? "":landmark.getKilometers2())+ "\"";
					jsonOutput += ",\"kilometers3\":\"" + (landmark.getKilometers3() == 0? "":landmark.getKilometers3())+ "\"";
					jsonOutput += ",\"kilometers4\":\"" + (landmark.getKilometers4() == 0? "":landmark.getKilometers4())+ "\"";
					jsonOutput += ",\"kilometers5\":\"" + (landmark.getKilometers5() == 0? "":landmark.getKilometers5())+ "\"";
					jsonOutput += ",\"kilometers6\":\"" + (landmark.getKilometers6() == 0?"":landmark.getKilometers6())+ "\"";
					jsonOutput += ",\"kilometers7\":\"" + (landmark.getKilometers7() == 0?"":landmark.getKilometers7())+ "\"";
					jsonOutput += ",\"kilometers8\":\"" + (landmark.getKilometers8() == 0?"":landmark.getKilometers8())+ "\"";
					jsonOutput += ",\"kilometers9\":\"" + (landmark.getKilometers9() ==0 ?"":landmark.getKilometers9())+ "\"";
					jsonOutput += ",\"kilometers10\":\"" + (landmark.getKilometers10() == 0?"":landmark.getKilometers10())+ "\"";
					jsonOutput += ",\"distance1\":\"" + (landmark.getDistance1() == null?"":landmark.getDistance1())+ "\"";
					jsonOutput += ",\"distance2\":\"" + (landmark.getDistance2() == null?"":landmark.getDistance2())+ "\"";
					jsonOutput += ",\"distance3\":\"" + (landmark.getDistance3() == null?"":landmark.getDistance3())+ "\"";
					jsonOutput += ",\"distance4\":\"" + (landmark.getDistance4() == null?"":landmark.getDistance4())+ "\"";
					jsonOutput += ",\"distance5\":\"" + (landmark.getDistance5() == null?"":landmark.getDistance5())+ "\"";
					jsonOutput += ",\"propertyLandmark1\":\"" + (landmark.getPropertyLandmark1() == null?"":landmark.getPropertyLandmark1())+ "\"";
					jsonOutput += ",\"propertyLandmark2\":\"" + (landmark.getPropertyLandmark2() == null?"":landmark.getPropertyLandmark2())+ "\"";
					jsonOutput += ",\"propertyLandmark3\":\"" + (landmark.getPropertyLandmark3() == null?"":landmark.getPropertyLandmark3())+ "\"";
					jsonOutput += ",\"propertyLandmark4\":\"" + (landmark.getPropertyLandmark4() == null?"":landmark.getPropertyLandmark4())+ "\"";
					jsonOutput += ",\"propertyLandmark5\":\"" + (landmark.getPropertyLandmark5() == null?"":landmark.getPropertyLandmark5())+ "\"";
					jsonOutput += ",\"propertyLandmark6\":\"" + (landmark.getPropertyLandmark6() == null?"":landmark.getPropertyLandmark6())+ "\"";
					jsonOutput += ",\"propertyLandmark7\":\"" + (landmark.getPropertyLandmark7() == null?"":landmark.getPropertyLandmark7())+ "\"";
					jsonOutput += ",\"propertyLandmark8\":\"" + (landmark.getPropertyLandmark8() == null?"":landmark.getPropertyLandmark8())+ "\"";
					jsonOutput += ",\"propertyLandmark9\":\"" + (landmark.getPropertyLandmark9() == null?"":landmark.getPropertyLandmark9())+ "\"";
					jsonOutput += ",\"propertyLandmark10\":\"" + (landmark.getPropertyLandmark10() == null?"":landmark.getPropertyLandmark10())+ "\"";
					jsonOutput += ",\"propertyAttraction1\":\"" + (landmark.getPropertyAttraction1() == null?"":landmark.getPropertyAttraction1())+ "\"";
					jsonOutput += ",\"propertyAttraction2\":\"" + (landmark.getPropertyAttraction2() == null?"":landmark.getPropertyAttraction2())+ "\"";
					jsonOutput += ",\"propertyAttraction3\":\"" + (landmark.getPropertyAttraction3() == null?"":landmark.getPropertyAttraction3())+ "\"";
					jsonOutput += ",\"propertyAttraction4\":\"" + (landmark.getPropertyAttraction4() == null?"":landmark.getPropertyAttraction4())+ "\"";
					jsonOutput += ",\"propertyAttraction5\":\"" + (landmark.getPropertyAttraction5() == null?"":landmark.getPropertyAttraction5())+ "\"";
					
					jsonOutput += "}";
				}
				



				response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
				

			} catch (Exception e) {
				logger.error(e);
				e.printStackTrace();
			} finally {

			}

			return null;

		}
		
	
	public String getPropertyContent1() throws IOException {
		//this.propertyId = (Integer) sessionMap.get("propertyId");
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PropertyContentManager propertyContentController = new PropertyContentManager();
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");

			PropertyContent propertycontent = propertyContentController.findId(getPropertyId());	

			{
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"locationContentId\":\"" + propertycontent.getLocationContentId()+ "\"";
				jsonOutput += ",\"metaDescription\":\"" + propertycontent.getMetaDescription()+ "\"";
				jsonOutput += ",\"schemaContent\":\"" + propertycontent.getSchemaContent()+ "\"";
				jsonOutput += ",\"locationDescription\":\"" + propertycontent.getPropertyDescription()+ "\"";
				jsonOutput += ",\"scriptContent\":\"" + propertycontent.getScriptContent()+ "\"";
				
				
				jsonOutput += "}";


			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}	
	
	public String getLocationContent() throws IOException {
		//this.propertyId = (Integer) sessionMap.get("propertyId");
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			LocationContentManager locationContentController = new LocationContentManager();
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");

			LocationContent locationcontent = locationContentController.findId(getLocationId());	

			{
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"locationContentId\":\"" + locationcontent.getLocationContentId()+ "\"";
				jsonOutput += ",\"metaDescription\":\"" + locationcontent.getMetaDescription()+ "\"";
				jsonOutput += ",\"schemaContent\":\"" + locationcontent.getSchemaContent()+ "\"";
				jsonOutput += ",\"locationDescription\":\"" + locationcontent.getLocationDescription()+ "\"";
				jsonOutput += ",\"scriptContent\":\"" + locationcontent.getScriptContent()+ "\"";
				
				
				jsonOutput += "}";


			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}	
	
	public String editPropertyContent() {
		
		PropertyContentManager propertyContentController = new PropertyContentManager();
		PmsPropertyManager pmsPropertyController = new PmsPropertyManager();
		try {
			
			//LocationContent locationcontent = new LocationContent();
			
			//LocationContent locationcontent = locationContentController.findId(getLocationId());	
		
			this.propertyContentList = propertyContentController.list(getPropertyId());
			
			for (PropertyContent propertycontent : propertyContentList) {
				propertycontent.setIsActive(false);
				propertycontent.setIsDeleted(true);
				
				propertyContentController.edit(propertycontent);	
			}	

			PropertyContent propertycontent1 = new PropertyContent();
			
			propertycontent1.setMetaDescription(getMetaDescription().trim());
			propertycontent1.setSchemaContent(getSchemaContent().trim());
			propertycontent1.setPropertyDescription(getPropertyContent().trim());
			propertycontent1.setScriptContent(getScriptContent().trim());
			propertycontent1.setIsActive(true);
			propertycontent1.setIsDeleted(false);
			PmsProperty property = pmsPropertyController.find(getPropertyId());
			
			propertycontent1.setPmsProperty(property);
		
		
			propertyContentController.edit(propertycontent1);
			
			/* 
			locationcontent.setIsActive(false);
			locationcontent.setIsDeleted(true);
			locationContentController.edit(locationcontent);	
			
			LocationContent locationcontent1 = new LocationContent();
			
				locationcontent1.setMetaDescription(getMetaDescription().trim());
				locationcontent1.setSchemaContent(getSchemaContent().trim());
				locationcontent1.setLocationDescription(getLocationDescription().trim());
				locationcontent1.setScriptContent(getScriptContent().trim());
				locationcontent1.setIsActive(true);
				locationcontent1.setIsDeleted(false);
				Location location = locationController.find(getLocationId());
				
				locationcontent1.setLocation(location);
			
			
			locationContentController.edit(locationcontent1);*/
			
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}
	
	public String editLocationContent() {
		this.propertyId = (Integer) sessionMap.get("propertyId");
		LocationContentManager locationContentController = new LocationContentManager();
		LocationManager locationController = new LocationManager();
		try {
			
			//LocationContent locationcontent = new LocationContent();
			
			LocationContent locationcontent = locationContentController.findId(getLocationId());	
		
			this.locationContentList = locationContentController.list(getLocationId());
			
			for (LocationContent accamen : locationContentList) {
				accamen.setIsActive(false);
				accamen.setIsDeleted(true);
				
				locationContentController.edit(accamen);	
			}	

			LocationContent locationcontent1 = new LocationContent();
			
			locationcontent1.setMetaDescription(getMetaDescription().trim());
			locationcontent1.setSchemaContent(getSchemaContent().trim());
			locationcontent1.setLocationDescription(getLocationDescription().trim());
			locationcontent1.setScriptContent(getScriptContent().trim());
			locationcontent1.setIsActive(true);
			locationcontent1.setIsDeleted(false);
			Location location = locationController.find(getLocationId());
			
			locationcontent1.setLocation(location);
		
		
		locationContentController.edit(locationcontent1);
			
			/* 
			locationcontent.setIsActive(false);
			locationcontent.setIsDeleted(true);
			locationContentController.edit(locationcontent);	
			
			LocationContent locationcontent1 = new LocationContent();
			
				locationcontent1.setMetaDescription(getMetaDescription().trim());
				locationcontent1.setSchemaContent(getSchemaContent().trim());
				locationcontent1.setLocationDescription(getLocationDescription().trim());
				locationcontent1.setScriptContent(getScriptContent().trim());
				locationcontent1.setIsActive(true);
				locationcontent1.setIsDeleted(false);
				Location location = locationController.find(getLocationId());
				
				locationcontent1.setLocation(location);
			
			
			locationContentController.edit(locationcontent1);*/
			
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}
	
	

	public String deleteProperty() {
		try {
//			this.propertyId = (Integer) sessionMap.get("propertyId");
			PmsPropertyManager propertyController = new PmsPropertyManager();
			PropertyUserManager propertyUserController=new PropertyUserManager();
			Integer userId=(Integer) sessionMap.get("userId");
			long time = System.currentTimeMillis();
			java.util.Date date = new java.util.Date(time);
			Timestamp tsDate=new Timestamp(date.getTime());  
			PmsProperty property = propertyController.find(getPropertyId());
			
			property.setIsActive(false);
			property.setIsDeleted(true);
			property.setModifiedBy(userId);
			property.setModifiedDate(tsDate);
			
			propertyController.edit(property);
			
			List<PropertyUser> listPropertyUser=propertyUserController.list(getPropertyId());
			if(listPropertyUser.size()>0 && !listPropertyUser.isEmpty()){
				for(PropertyUser propertyUser:listPropertyUser){
					PropertyUser propertyuser=propertyUserController.find(propertyUser.getPropertyUserId());
					propertyuser.setIsActive(false);
					propertyuser.setIsDeleted(true);
					propertyuser.setModifiedBy(userId);
					propertyuser.setModifiedDate(tsDate);
					propertyUserController.edit(propertyuser);
				}
			}
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}
	
	public String getPropertyCount() throws IOException {
 		try {
 			String jsonOutput = "";
 			HttpServletResponse response = ServletActionContext.getResponse();
 			response.setContentType("application/json");
 			PmsPropertyManager propertyController=new PmsPropertyManager();
 			List<PmsProperty> listPropertyCount =propertyController.list();
 			if(listPropertyCount.size()>0 && !listPropertyCount.isEmpty()){
 				if (!jsonOutput.equalsIgnoreCase(""))
 					jsonOutput += ",{";
 				else
 					jsonOutput += "{";
 				
 			    jsonOutput += "\"count\":\"" + listPropertyCount.size() + "\"";
 			    
 				jsonOutput += "}";
 			}
 			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
 		} catch (Exception e) {
 			logger.error(e);
 			e.printStackTrace();
 		} finally {

 		}

 		return null;

 	}
	
	public String getPropertyTagsApiOther(){
		try {
			Algorithm algorithm = Algorithm.HMAC256("secret");
			int count=0;
		    Map<String, Object> dataClaims=new HashMap<String, Object>();
 			String jsonOutput = "",output="";
 			JSONObject jsonConfig=new JSONObject();
 			HttpServletResponse response = ServletActionContext.getResponse();
 			response.setContentType("application/json");
 			PmsTagManager tagController=new PmsTagManager();
 			List<PmsTags> listTags =tagController.list();
 			if(listTags.size()>0 && !listTags.isEmpty()){
 				for(PmsTags tags:listTags){
 					count++;
 					if (!jsonOutput.equalsIgnoreCase(""))
 	 					jsonOutput += ",{";
 	 				else
 	 					jsonOutput += "{";
 	 				jsonConfig.put("id:"+tags.getTagId(), tags.getTagId());
 	 				jsonConfig.put("name:"+tags.getTagId(), tags.getTagName());
 	 				
 					jsonOutput += "\"id\":\"" + tags.getTagId()+ "\"";
 					jsonOutput += ",\"name\":\"" + tags.getTagName()+ "\"";
 	 			    
 	 				jsonOutput += "}";
 	 				dataClaims.put("id", String.valueOf(tags.getTagId()));
 	 				dataClaims.put("name", tags.getTagName());
 				}
 			}
 			
 			if(count == 0){
				//response.getWriter().write("{\"error\":\"1\",\"message\":\"Sorry No Results Found\"}");
 				output="{\"error\":\"1\",\"message\":\"Sorry No Results Found\"}";
				dataClaims.put("error", "1");
				jsonConfig.put("error", 1);
			}else{
				//response.getWriter().write("{\"error\":\"0\",\"propertyTag\":[" + jsonOutput + "]}");
				dataClaims.put("error", "0");
				jsonConfig.put("error", 0);
				output="{\"error\":\"0\",\"propertyTag\":[" + jsonOutput + "]}";
			}
 			String token = JWT.create()
 					.withClaim("data", JSONObject.valueToString(output))
 					.withSubject("Hello API")
 					.sign(algorithm);
 			
 			response.getWriter().write(token);
 			/*String token = JWT.create()
 			        .withIssuer("auth0")
 			        .withClaim("name", 123)
 			        .sign(algorithm);*/
 			
		    
 		} catch (Exception e) {
 			logger.error(e);
 			e.printStackTrace();
 		} finally {

 		}
		return null;
	}
	
	public String getPropertyTagsApi(){
		String output=null;
		try {
			Algorithm algorithm = Algorithm.HMAC256("secret");
			int count=0;
			long nowMillis = System.currentTimeMillis();
		    Date now = new Date(nowMillis+15*60*1000);
 			String jsonOutput = "";
 			String path = "contents/images/icons/";
 			String assetbluepath="http://ulotel.ulohotels.com/contents/images/blue-color-icons/";
 			String assetgreypath="http://ulotel.ulohotels.com/contents/images/grey-color-icons/";
 			String assettagpath="http://ulotel.ulohotels.com/contents/images/tag-new-icons/";
 			HttpServletResponse response = ServletActionContext.getResponse();
 			response.setContentType("application/json");
 			PmsTagManager tagController=new PmsTagManager();
 			List<PmsTags> listTags =tagController.list();
 			if(listTags.size()>0 && !listTags.isEmpty()){
 				for(PmsTags tags:listTags){
 					count++;
 					if (!jsonOutput.equalsIgnoreCase(""))
 	 					jsonOutput += ",{";
 	 				else
 	 					jsonOutput += "{";
 	 				
 					jsonOutput += "\"id\":\"" + tags.getTagId()+ "\"";
 					jsonOutput += ",\"name\":\"" + tags.getTagName()+ "\"";
// 					jsonOutput += ",\"amenitiespath\":\"" + path+ "\"";
 					
 	 				jsonOutput += "}";
 				}
 			}
 			
 			String strAssetBluePath="\"assetBluePath\":\""+assetbluepath+"\"";
 			String strAssetGreyPath="\"assetGreyPath\":\""+assetgreypath+"\"";
 			String strAssetTagPath="\"assetTagPath\":\""+assettagpath+"\"";
 			
 			String data="\"trip-type\":[" + jsonOutput + "]";
 			String strData=data.replaceAll("\"", "\"");
 			
 			/*String token = JWT.create()
 					.withClaim("data", strData)
 					.withClaim("email", "techsupport@ulohotels.com")
 					.withExpiresAt(now)
 					.withIssuer("auth0")
 					.sign(algorithm);
 			output = "\"token\":\"" + token+ "\"";
 			String strOutput=output.replaceAll("\"", "\"");*/
 			
 			
 			if(count == 0){
 				output="{\"error\":\"1\",\"message\":\"No Records Found\"}";
			}else{
				output="{\"error\":\"0\",\"data\":{" + strData+","+strAssetBluePath +","+strAssetGreyPath +","+strAssetTagPath+ "}}";
//				output="{\"error\":\"0\"," + strData + "}";
			}
 			
 			/*String token = JWT.create()
 			        .withIssuer("auth0")
 			        .withClaim("name", 123)
 			        .sign(algorithm);*/
 			
		    
 		} catch (Exception e) {
 			output="{\"error\":\"2\",\"message\":\"Some technical error on config api\"}";
 			logger.error(e);
 			e.printStackTrace();
 		} finally {

 		}
		return output;
	}
	
	public String getFilterAPI(Hotels hotels){
		String output=null;
		try{
			PmsPropertyManager propertyController=new PmsPropertyManager();
			PropertyTypeManager propertyTypeController=new PropertyTypeManager();
			PmsTagManager tagController=new PmsTagManager();
			GooglePlaceManager googleController=new GooglePlaceManager();
			String tripdata="",hoteltypedata="",areadata="",offerdata="",pricedata="";
			String trip="",hoteltype="",areas="",offer="",price="";
			boolean[] isactivearray={true,true,true};
			String[] typearray={"Flat Sale","Early Bird","Last Minute"};
			
 			String jsonAreas = "",jsonHotelType="",jsonTripType="",jsonOffer="",jsonPrice="";
 			HttpServletResponse response = ServletActionContext.getResponse();
 			response.setContentType("application/json");
 			String id=null,type=null;
 			SeoUrlAction urlcheck=new SeoUrlAction();
		    String stroutput=urlcheck.getSearchLocationIdByUrl(hotels.getLocationUrl());
		    if(stroutput!=null){
		    	StringTokenizer stoken=new StringTokenizer(stroutput,",");
			    while(stoken.hasMoreElements()){
			    	id=stoken.nextToken();
			    	type=stoken.nextToken();
				}	
		    }else{
		    	return null;
		    }
		    
		    int count=0;
		    String strFirstName="",firstNameLetterUpperCase="";
			String strLastName="",lastNameLetterUpperCase="";
			
 	    	if(type.equals("location")){
 	    		List<GoogleArea> listAreas=googleController.listGoogleLocationArea(Integer.parseInt(id));
    			if(listAreas.size()>0 && !listAreas.isEmpty()){
    				for(GoogleArea area:listAreas){
    					count++;
	 					if (!jsonAreas.equalsIgnoreCase(""))
	 						jsonAreas += ",{";
	 	 				else
	 	 					jsonAreas += "{";
	 	 				
    	 					jsonAreas += "\"id\":\"" + area.getGoogleAreaId()+ "\"";
    	 					
    	 					StringTokenizer token=new StringTokenizer(area.getGoogleAreaDisplayName()," ");
    	 					while(token.hasMoreTokens()){
    	 						if(token.countTokens()==1){
    	 							strFirstName=token.nextToken();
    	 						}
    	 						if(token.countTokens()==2){
    	 							strFirstName=token.nextToken();
        	 						strLastName=token.nextToken();	
    	 						}
    	 					}
    	 					if(strFirstName!=null && strFirstName!="" && !strFirstName.equalsIgnoreCase("undefined")){
    	 						strFirstName=strFirstName.trim();
    	 						firstNameLetterUpperCase=strFirstName.substring(0, 1).toUpperCase()+strFirstName.substring(1);
    	 						strFirstName=firstNameLetterUpperCase;
    	 					}else{
    	 						strFirstName="";
    	 					}
    	 					
    	 					if(strLastName!=null && strLastName!=""  && !strLastName.equalsIgnoreCase("undefined")){
    	 						strLastName=strLastName.trim();
    	 						lastNameLetterUpperCase=strLastName.substring(0, 1).toUpperCase() +  strLastName.substring(1);
    	 						strLastName=lastNameLetterUpperCase;
    	 					}else{
    	 						strLastName="";
    	 					}
    	 					jsonAreas += ",\"name\":\"" + strFirstName+" "+strLastName+ "\"";
    	 					jsonAreas += ",\"url\":\"" + area.getGoogleAreaUrl()+ "\"";
    	 					
    	 					jsonAreas += "}";
    				}
    			}
		    }else if(type.equals("area")){
		    	GoogleArea googleareas=googleController.findArea(Integer.parseInt(id));
		    	if(googleareas!=null){
		    		int locationId=googleareas.getGoogleLocation().getGoogleLocationId();
	    			List<GoogleArea> listAreas=googleController.listGoogleLocationArea(locationId);
	    			if(listAreas.size()>0 && !listAreas.isEmpty()){
	    				for(GoogleArea area:listAreas){
	    					count++;
	    					if((int)Integer.parseInt(id)==(int)area.getGoogleAreaId()){
	    						
	    					}else{
	    						if (!jsonAreas.equalsIgnoreCase(""))
	    	 						jsonAreas += ",{";
	    	 	 				else
	    	 	 					jsonAreas += "{";
	    	 	 				
		    	 					jsonAreas += "\"id\":\"" + area.getGoogleAreaId()+ "\"";
		    	 					StringTokenizer token=new StringTokenizer(area.getGoogleAreaDisplayName()," ");
		    	 					while(token.hasMoreTokens()){
		    	 						if(token.countTokens()==1){
		    	 							strFirstName=token.nextToken();
		    	 						}
		    	 						if(token.countTokens()==2){
		    	 							strFirstName=token.nextToken();
		        	 						strLastName=token.nextToken();	
		    	 						}
		    	 					}
		    	 					if(strFirstName!=null && strFirstName!="" && !strFirstName.equalsIgnoreCase("undefined")){
		    	 						strFirstName=strFirstName.trim();
		    	 						firstNameLetterUpperCase=strFirstName.substring(0, 1).toUpperCase()+strFirstName.substring(1);
		    	 						strFirstName=firstNameLetterUpperCase;
		    	 					}else{
		    	 						strFirstName="";
		    	 					}
		    	 					
		    	 					if(strLastName!=null && strLastName!=""  && !strLastName.equalsIgnoreCase("undefined")){
		    	 						strLastName=strLastName.trim();
		    	 						lastNameLetterUpperCase=strLastName.substring(0, 1).toUpperCase() +  strLastName.substring(1);
		    	 						strLastName=lastNameLetterUpperCase;
		    	 					}else{
		    	 						strLastName="";
		    	 					}
		    	 					jsonAreas += ",\"name\":\"" + strFirstName+" "+strLastName+ "\"";
		    	 					jsonAreas += ",\"url\":\"" + area.getGoogleAreaUrl()+ "\"";
		    	 					
		    	 					jsonAreas += "}";	
	    					}
    	 					
	    				}
	    			}
		    	}
		    }
 	    	
 	    	List<PropertyType> listPropertyType=propertyTypeController.list();
 	    	if(listPropertyType.size()>0 && !listPropertyType.isEmpty() ){
 	    		for(PropertyType propertytype:listPropertyType){
 	    			count++;
 					if (!jsonHotelType.equalsIgnoreCase(""))
 						jsonHotelType += ",{";
 	 				else
 	 					jsonHotelType += "{";
 	 				
 					jsonHotelType += "\"typeId\":\"" + propertytype.getPropertyTypeId()+ "\"";
 					jsonHotelType += ",\"typeName\":\"" + propertytype.getPropertyTypeName()+ "\"";
	 					
 					jsonHotelType += "}";
 	    		}
 	    	}

 	    	List<PmsTags> listTags=tagController.list();
 	    	if(listTags.size()>0 && !listTags.isEmpty()){
 	    		for(PmsTags tags:listTags){
 	    			count++;
 					if (!jsonTripType.equalsIgnoreCase(""))
 						jsonTripType += ",{";
 	 				else
 	 					jsonTripType += "{";
 	 				
 					jsonTripType += "\"tripTypeId\":\"" + tags.getTagId()+ "\"";
 					jsonTripType += ",\"tripTypeName\":\"" + tags.getTagName().toUpperCase()+ "\"";
	 					
 					jsonTripType += "}";
 	    		}
 	    	}
 	    	
			if (!jsonPrice.equalsIgnoreCase(""))
				jsonPrice += ",{";
				else
					jsonPrice += "{";
				
			jsonPrice += "\"minAmount\":\"" + 1000+ "\"";
			jsonPrice += ",\"maxAmount\":\"" + 1500+ "\"";
			
			jsonPrice += "}";
 	    	
 	    	for(int i=0;i<isactivearray.length;i++){
 	    		count++;
					if (!jsonOffer.equalsIgnoreCase(""))
						jsonOffer += ",{";
	 				else
	 					jsonOffer += "{";
	 				
					jsonOffer += "\"offerType\":\"" + typearray[i]+ "\"";
					jsonOffer += ",\"isActive\":\"" + isactivearray[i]+ "\"";
 					
					jsonOffer += "}";
 	    	}
 	    	trip="\"tripType\":[" + jsonTripType+ "]";
 	    	tripdata=trip.replaceAll("\"", "\"");
 	    	price="\"priceRange\":[" + jsonPrice+ "]";
 	    	pricedata=price.replaceAll("\"", "\"");
 	    	offer="\"offerType\":[" + jsonOffer + "]";
 	    	offerdata=offer.replaceAll("\"", "\"");
 	    	areas="\"areaType\":[" + jsonAreas + "]";
 	    	areadata=areas.replaceAll("\"", "\"");
 	    	hoteltype="\"hotelType\":[" + jsonHotelType + "]";
 	    	hoteltypedata=hoteltype.replaceAll("\"", "\"");
 	    	
 	    	if(count == 0){
 				output="{\"error\":\"1\",\"message\":\"No Records Found\"}";
			}else{
				output="{\"error\":\"0\",\"data\":{" + areadata+","+pricedata+","+hoteltypedata+","+tripdata+","+offerdata + "}}";
//				output="{\"error\":\"0\"," + strData + "}";
			}
 	    	
		    
 		
		}catch(Exception e){
			e.printStackTrace();
		}
		return output;
	}
	public String getApiProperties() throws IOException {
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PmsPropertyManager propertyController = new PmsPropertyManager();
			UserLoginManager userController=new UserLoginManager();
			response.setContentType("application/json");
			int serialno=1;
			this.propertyList = propertyController.list();
			for (PmsProperty property : propertyList) {

				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"propertyId\":\"" + property.getPropertyId()+ "\"";
				jsonOutput += ",\"serialNo\":\"" + serialno+ "\"";
				jsonOutput += ",\"propertyName\":\"" + (property.getPropertyName() == null ? "": property.getPropertyName().trim())+ "\"";
				jsonOutput += ",\"propertyUrl\":\"" + (property.getPropertyUrl() == null ? "": property.getPropertyUrl().trim())+ "\"";
				/*jsonOutput += ",\"locationId\":\"" + (property.getLocation().getLocationId() == null ? "": property.getLocation().getLocationId())+ "\"";
				jsonOutput += ",\"propertyLocationName\":\"" + (property.getLocation().getLocationName() == null ? "": property.getLocation().getLocationName())+ "\"";*/
				if(property.getCreatedBy()!=null){
					User user=userController.find(property.getCreatedBy());
					String strFirstName=user.getUserName().trim();
					String firstNameLetterUpperCase=strFirstName.substring(0, 1).toUpperCase()+strFirstName.substring(1);
					jsonOutput += ",\"createdBy\":\"" + firstNameLetterUpperCase+ "\"";
				}else{
					jsonOutput += ",\"createdBy\":\"" +" "+ "\"";
				}
				
				
				jsonOutput += "}";

				serialno++;
			}

			if(jsonOutput.length() == 0){
				response.getWriter().write("{\"error\":\"1\",\"message\":\"No Records Found\"}");
			}else{
				response.getWriter().write("{\"error\":\"0\",\"data\":[" + jsonOutput + "]}");
			}
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	public Integer getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(Integer propertyId) {
		this.propertyId = propertyId;
	}
     
	
	public Integer getPropertyTypeId() {
		return propertyTypeId;
	}

	public void setPropertyTypeId(Integer propertyTypeId) {
		this.propertyTypeId = propertyTypeId;
	}
	
	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	
	public String getPlaceId() {
		return placeId;
	}

	public void setPlaceId(String placeId) {
		this.placeId = placeId;
	}



	public String getPropertyContact() {
		return propertyContact;
	}

	public void setPropertyContact(String propertyContact) {
		this.propertyContact = propertyContact;
	}

	public String getPropertyDescription() {
		return propertyDescription;
	}

	public void setPropertyDescription(String propertyDescription) {
		this.propertyDescription = propertyDescription;
	}

	public String getPropertyEmail() {
		return propertyEmail;
	}

	public void setPropertyEmail(String propertyEmail) {
		this.propertyEmail = propertyEmail;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public String getPropertyPhone() {
		return propertyPhone;
	}

	public void setPropertyPhone(String propertyPhone) {
		this.propertyPhone = propertyPhone;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}
	
	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	
	public String getResortManagerName() {
		return resortManagerName;
	}

	public void setResortManagerName(String resortManagerName) {
		this.resortManagerName = resortManagerName;
	}

	public String getResortManagerContact() {
		return resortManagerContact;
	}

	public void setResortManagerContact(String resortManagerContact) {
		this.resortManagerContact = resortManagerContact;
	}

	public String getResortManagerEmail() {
		return resortManagerEmail;
	}

	public void setResortManagerEmail(String resortManagerEmail) {
		this.resortManagerEmail = resortManagerEmail;
	}

	public String getResortManagerDesignation() {
		return resortManagerDesignation;
	}

	public void setResortManagerDesignation(String resortManagerDesignation) {
		this.resortManagerDesignation = resortManagerDesignation;
	}

	public String getResortManagerName1() {
		return resortManagerName1;
	}

	public void setResortManagerName1(String resortManagerName1) {
		this.resortManagerName1 = resortManagerName1;
	}

	public String getResortManagerContact1() {
		return resortManagerContact1;
	}

	public void setResortManagerContact1(String resortManagerContact1) {
		this.resortManagerContact1 = resortManagerContact1;
	}

	public String getResortManagerEmail1() {
		return resortManagerEmail1;
	}

	public void setResortManagerEmail1(String resortManagerEmail1) {
		this.resortManagerEmail1 = resortManagerEmail1;
	}

	public String getResortManagerDesignation1() {
		return resortManagerDesignation1;
	}

	public void setResortManagerDesignation1(String resortManagerDesignation1) {
		this.resortManagerDesignation1 = resortManagerDesignation1;
	}

	public String getResortManagerName2() {
		return resortManagerName2;
	}

	public void setResortManagerName2(String resortManagerName2) {
		this.resortManagerName2 = resortManagerName2;
	}

	public String getResortManagerContact2() {
		return resortManagerContact2;
	}

	public void setResortManagerContact2(String resortManagerContact2) {
		this.resortManagerContact2 = resortManagerContact2;
	}

	public String getResortManagerEmail2() {
		return resortManagerEmail2;
	}

	public void setResortManagerEmail2(String resortManagerEmail2) {
		this.resortManagerEmail2 = resortManagerEmail2;
	}

	public String getResortManagerDesignation2() {
		return resortManagerDesignation2;
	}

	public void setResortManagerDesignation2(String resortManagerDesignation2) {
		this.resortManagerDesignation2 = resortManagerDesignation2;
	}

	public String getResortManagerName3() {
		return resortManagerName3;
	}

	public void setResortManagerName3(String resortManagerName3) {
		this.resortManagerName3 = resortManagerName3;
	}

	public String getResortManagerContact3() {
		return resortManagerContact3;
	}

	public void setResortManagerContact3(String resortManagerContact3) {
		this.resortManagerContact3 = resortManagerContact3;
	}

	public String getResortManagerEmail3() {
		return resortManagerEmail3;
	}

	public void setResortManagerEmail3(String resortManagerEmail3) {
		this.resortManagerEmail3 = resortManagerEmail3;
	}

	public String getResortManagerDesignation3() {
		return resortManagerDesignation3;
	}

	public void setResortManagerDesignation3(String resortManagerDesignation3) {
		this.resortManagerDesignation3 = resortManagerDesignation3;
	}

	public Integer getPaynowDiscount() {
		return paynowDiscount;
	}

	public void setPaynowDiscount(Integer paynowDiscount) {
		this.paynowDiscount = paynowDiscount;
	}

	public String getReservationManagerName() {
		return reservationManagerName;
	}

	public void setReservationManagerName(String reservationManagerName) {
		this.reservationManagerName = reservationManagerName;
	}

	public String getReservationManagerContact() {
		return reservationManagerContact;
	}

	public void setReservationManagerContact(String reservationManagerContact) {
		this.reservationManagerContact = reservationManagerContact;
	}

	public String getReservationManagerEmail() {
		return reservationManagerEmail;
	}

	public void setReservationManagerEmail(String reservationManagerEmail) {
		this.reservationManagerEmail = reservationManagerEmail;
	}
	

	public PmsProperty getProperty() {
		return property;
	}

	public void setProperty(PmsProperty property) {
		this.property = property;
	}

	public List<PmsProperty> getPropertyList() {
		return propertyList;
	}

	public void setPropertyList(List<PmsProperty> propertyList) {
		this.propertyList = propertyList;
	}
	
	public Timestamp getTaxValidFrom() {
		return taxValidFrom;
	}

	public void setTaxValidFrom(Timestamp taxValidFrom) {
		this.taxValidFrom = taxValidFrom;
	}

	public Timestamp getTaxValidTo() {
		return taxValidTo;
	}

	public void setTaxValidTo(Timestamp taxValidTo) {
		this.taxValidTo = taxValidTo;
	}

	public Boolean getTaxIsActive() {
		return taxIsActive;
	}

	public void setTaxIsActive(Boolean taxIsActive) {
		this.taxIsActive = taxIsActive;
	}
	
	public Boolean getPayAtHotelStatus() {
		return payAtHotelStatus;
	}

	public void setPayAtHotelStatus(Boolean payAtHotelStatus) {
		this.payAtHotelStatus = payAtHotelStatus;
	}


	
	public String getPropertyHotelPolicy()
	{
		return propertyHotelPolicy;
	}
	
	public void setPropertyHotelPolicy(String propertyHotelPolicy){
		this.propertyHotelPolicy=propertyHotelPolicy;
	}
	
	public String getPropertyRefundPolicy()
	{
		return propertyRefundPolicy;
	}
	
	public void setPropertyRefundPolicy(String propertyRefundPolicy){
		this.propertyRefundPolicy=propertyRefundPolicy;
	}
	
	public String getPropertyCancellationPolicy()
	{
		return propertyCancellationPolicy;
	}
	
	public void setPropertyCancellationPolicy(String propertyCancellationPolicy){
		this.propertyCancellationPolicy=propertyCancellationPolicy;
	}
	
	public String getPropertyChildPolicy() {
		return propertyChildPolicy;
	}

	public void setPropertyChildPolicy(String propertyChildPolicy) {
		this.propertyChildPolicy = propertyChildPolicy;
	}
	
	public String getPropertyGigiPolicy() {
		return propertyGigiPolicy;
	}

	public void setPropertyGigiPolicy(String propertyGigiPolicy) {
		this.propertyGigiPolicy = propertyGigiPolicy;
	}

	public String getMetaDescription() {
		return metaDescription;
	}

	public void setMetaDescription(String metaDescription) {
		this.metaDescription = metaDescription;
	}

	public String getSchemaContent() {
		return schemaContent;
	}

	public void setSchemaContent(String schemaContent) {
		this.schemaContent = schemaContent;
	}

	public String getLocationDescription() {
		return locationDescription;
	}

	public void setLocationDescription(String locationDescription) {
		this.locationDescription = locationDescription;
	}

	public String getScriptContent() {
		return scriptContent;
	}

	public void setScriptContent(String scriptContent) {
		this.scriptContent = scriptContent;
	}

	public Integer getLocationId() {
		return locationId;
	}

	public void setLocationId(Integer locationId) {
		this.locationId = locationId;
	}

	public String getPropertyContent() {
		return propertyContent;
	}

	public void setPropertyContent(String propertyContent) {
		this.propertyContent = propertyContent;
	}

	public String getPropertyLandmark1() {
		return propertyLandmark1;
	}

	public void setPropertyLandmark1(String propertyLandmark1) {
		this.propertyLandmark1 = propertyLandmark1;
	}

	public String getPropertyLandmark2() {
		return propertyLandmark2;
	}

	public void setPropertyLandmark2(String propertyLandmark2) {
		this.propertyLandmark2 = propertyLandmark2;
	}

	public String getPropertyLandmark3() {
		return propertyLandmark3;
	}

	public void setPropertyLandmark3(String propertyLandmark3) {
		this.propertyLandmark3 = propertyLandmark3;
	}

	public String getPropertyLandmark4() {
		return propertyLandmark4;
	}

	public void setPropertyLandmark4(String propertyLandmark4) {
		this.propertyLandmark4 = propertyLandmark4;
	}

	public String getPropertyLandmark5() {
		return propertyLandmark5;
	}

	public void setPropertyLandmark5(String propertyLandmark5) {
		this.propertyLandmark5 = propertyLandmark5;
	}

	public String getPropertyLandmark6() {
		return propertyLandmark6;
	}

	public void setPropertyLandmark6(String propertyLandmark6) {
		this.propertyLandmark6 = propertyLandmark6;
	}

	public String getPropertyLandmark7() {
		return propertyLandmark7;
	}

	public void setPropertyLandmark7(String propertyLandmark7) {
		this.propertyLandmark7 = propertyLandmark7;
	}

	public String getPropertyLandmark8() {
		return propertyLandmark8;
	}

	public void setPropertyLandmark8(String propertyLandmark8) {
		this.propertyLandmark8 = propertyLandmark8;
	}

	public String getPropertyLandmark9() {
		return propertyLandmark9;
	}

	public void setPropertyLandmark9(String propertyLandmark9) {
		this.propertyLandmark9 = propertyLandmark9;
	}

	public String getPropertyLandmark10() {
		return propertyLandmark10;
	}

	public void setPropertyLandmark10(String propertyLandmark10) {
		this.propertyLandmark10 = propertyLandmark10;
	}

	public String getPropertyAttraction1() {
		return propertyAttraction1;
	}

	public void setPropertyAttraction1(String propertyAttraction1) {
		this.propertyAttraction1 = propertyAttraction1;
	}

	public String getPropertyAttraction2() {
		return propertyAttraction2;
	}

	public void setPropertyAttraction2(String propertyAttraction2) {
		this.propertyAttraction2 = propertyAttraction2;
	}

	public String getPropertyAttraction3() {
		return propertyAttraction3;
	}

	public void setPropertyAttraction3(String propertyAttraction3) {
		this.propertyAttraction3 = propertyAttraction3;
	}

	public String getPropertyAttraction4() {
		return propertyAttraction4;
	}

	public void setPropertyAttraction4(String propertyAttraction4) {
		this.propertyAttraction4 = propertyAttraction4;
	}

	public String getPropertyAttraction5() {
		return propertyAttraction5;
	}

	public void setPropertyAttraction5(String propertyAttraction5) {
		this.propertyAttraction5 = propertyAttraction5;
	}

	public double getDistance1() {
		return distance1;
	}

	public void setDistance1(double distance1) {
		this.distance1 = distance1;
	}

	public double getDistance2() {
		return distance2;
	}

	public void setDistance2(double distance2) {
		this.distance2 = distance2;
	}

	public double getDistance3() {
		return distance3;
	}

	public void setDistance3(double distance3) {
		this.distance3 = distance3;
	}

	public double getDistance4() {
		return distance4;
	}

	public void setDistance4(double distance4) {
		this.distance4 = distance4;
	}

	public double getDistance5() {
		return distance5;
	}

	public void setDistance5(double distance5) {
		this.distance5 = distance5;
	}

	public double getKilometers1() {
		return kilometers1;
	}

	public void setKilometers1(double kilometers1) {
		this.kilometers1 = kilometers1;
	}

	public double getKilometers2() {
		return kilometers2;
	}

	public void setKilometers2(double kilometers2) {
		this.kilometers2 = kilometers2;
	}

	public double getKilometers3() {
		return kilometers3;
	}

	public void setKilometers3(double kilometers3) {
		this.kilometers3 = kilometers3;
	}

	public double getKilometers4() {
		return kilometers4;
	}

	public void setKilometers4(double kilometers4) {
		this.kilometers4 = kilometers4;
	}

	public double getKilometers5() {
		return kilometers5;
	}

	public void setKilometers5(double kilometers5) {
		this.kilometers5 = kilometers5;
	}

	public double getKilometers6() {
		return kilometers6;
	}

	public void setKilometers6(double kilometers6) {
		this.kilometers6 = kilometers6;
	}

	public double getKilometers7() {
		return kilometers7;
	}

	public void setKilometers7(double kilometers7) {
		this.kilometers7 = kilometers7;
	}

	public double getKilometers8() {
		return kilometers8;
	}

	public void setKilometers8(double kilometers8) {
		this.kilometers8 = kilometers8;
	}

	public double getKilometers9() {
		return kilometers9;
	}

	public void setKilometers9(double kilometers9) {
		this.kilometers9 = kilometers9;
	}

	public double getKilometers10() {
		return kilometers10;
	}

	public void setKilometers10(double kilometers10) {
		this.kilometers10 = kilometers10;
	}
	
	public List<PropertyLandmark> getPropertyLandmarkList() {
		return propertyLandmarkList;
	}

	public void setPropertyLandmarkList(List<PropertyLandmark> propertyLandmarkList) {
		this.propertyLandmarkList = propertyLandmarkList;
	}
	
	public File getMyFile() {
		return myFile;
	}

	public void setMyFile(File myFile) {
		this.myFile = myFile;
	}

	public String getMyFileContentType() {
		return myFileContentType;
	}

	public void setMyFileContentType(String myFileContentType) {
		this.myFileContentType = myFileContentType;
	}

	public String getMyFileFileName() {
		return myFileFileName;
	}

	public void setMyFileFileName(String myFileFileName) {
		this.myFileFileName = myFileFileName;
	}
	public Integer getPropertyLocationId() {
		return propertyLocationId;
	}

	public void setPropertyLocationId(Integer propertyLocationId) {
		this.propertyLocationId = propertyLocationId;
	}
	
	public Integer getLocationTypeId() {
		return locationTypeId;
	}

	public void setLocationTypeId(Integer locationTypeId) {
		this.locationTypeId = locationTypeId;
	}

	public Integer getLocationPropertyId() {
		return locationPropertyId;
	}

	public void setLocationPropertyId(Integer locationPropertyId) {
		this.locationPropertyId = locationPropertyId;
	}
	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	public Integer getOffset() {
		return offset;
	}

	public void setOffset(Integer offset) {
		this.offset = offset;
	}
	
	public String getPropertyCommission() {
		return propertyCommission;
	}

	public void setPropertyCommission(String propertyCommission) {
		this.propertyCommission = propertyCommission;
	}
	
	public String getContractManagerName() {
		return contractManagerName;
	}

	public void setContractManagerName(String contractManagerName) {
		this.contractManagerName = contractManagerName;
	}

	public String getContractManagerEmail() {
		return contractManagerEmail;
	}

	public void setContractManagerEmail(String contractManagerEmail) {
		this.contractManagerEmail = contractManagerEmail;
	}

	public String getContractManagerContact() {
		return contractManagerContact;
	}

	public void setContractManagerContact(String contractManagerContact) {
		this.contractManagerContact = contractManagerContact;
	}

	public String getRevenueManagerName() {
		return revenueManagerName;
	}

	public void setRevenueManagerName(String revenueManagerName) {
		this.revenueManagerName = revenueManagerName;
	}

	public String getRevenueManagerEmail() {
		return revenueManagerEmail;
	}

	public void setRevenueManagerEmail(String revenueManagerEmail) {
		this.revenueManagerEmail = revenueManagerEmail;
	}

	public String getRevenueManagerContact() {
		return revenueManagerContact;
	}

	public void setRevenueManagerContact(String revenueManagerContact) {
		this.revenueManagerContact = revenueManagerContact;
	}
	
	public String getPropertyGstNo() {
		return propertyGstNo;
	}

	public void setPropertyGstNo(String propertyGstNo) {
		this.propertyGstNo = propertyGstNo;
	}
	
	public String getPropertyFinanceEmail() {
		return propertyFinanceEmail;
	}

	public void setPropertyFinanceEmail(String propertyFinanceEmail) {
		this.propertyFinanceEmail = propertyFinanceEmail;
	}
	
	public String getRouteMap() {
		return routeMap;
	}

	public void setRouteMap(String routeMap) {
		this.routeMap = routeMap;
	}
	
	public String getCheckedAreas() {
		return checkedAreas;
	}

	public void setCheckedAreas(String checkedAreas) {
		this.checkedAreas = checkedAreas;
	}
	
	public String getCheckedPropertyTag() {
		return checkedPropertyTag;
	}

	public void setCheckedPropertyTag(String checkedPropertyTag) {
		this.checkedPropertyTag = checkedPropertyTag;
	}

	public String getPropertyUrl() {
		return propertyUrl;
	}

	public void setPropertyUrl(String propertyUrl) {
		this.propertyUrl = propertyUrl;
	}

	public String getPropertyReviewLink() {
		return propertyReviewLink;
	}

	public void setPropertyReviewLink(String propertyReviewLink) {
		this.propertyReviewLink = propertyReviewLink;
	}
	
	public String getTripadvisorReviewLink() {
		return tripadvisorReviewLink;
	}

	public void setTripadvisorReviewLink(String tripadvisorReviewLink) {
		this.tripadvisorReviewLink = tripadvisorReviewLink;
	}

	public Boolean getCoupleStatus() {
		return coupleStatus;
	}

	public void setCoupleStatus(Boolean coupleStatus) {
		this.coupleStatus = coupleStatus;
	}

	public Boolean getBreakfastStatus() {
		return breakfastStatus;
	}

	public void setBreakfastStatus(Boolean breakfastStatus) {
		this.breakfastStatus = breakfastStatus;
	}

	public Boolean getHotelStatus() {
		return hotelStatus;
	}

	public void setHotelStatus(Boolean hotelStatus) {
		this.hotelStatus = hotelStatus;
	}
	
	public Boolean getHotelIsActive() {
		return hotelIsActive;
	}

	public void setHotelIsActive(Boolean hotelIsActive) {
		this.hotelIsActive = hotelIsActive;
	}

	public Boolean getStopSellIsActive() {
		return stopSellIsActive;
	}

	public void setStopSellIsActive(Boolean stopSellIsActive) {
		this.stopSellIsActive = stopSellIsActive;
	}

	public Boolean getDelistIsActive() {
		return delistIsActive;
	}

	public void setDelistIsActive(Boolean delistIsActive) {
		this.delistIsActive = delistIsActive;
	}

	public String getPropertyRole() {
		return propertyRole;
	}

	public void setPropertyRole(String propertyRole) {
		this.propertyRole = propertyRole;
	}
	
	public String getWalkinCommission() {
		return walkinCommission;
	}

	public void setWalkinCommission(String walkinCommission) {
		this.walkinCommission = walkinCommission;
	}

	public String getOnlineCommission() {
		return onlineCommission;
	}

	public void setOnlineCommission(String onlineCommission) {
		this.onlineCommission = onlineCommission;
	}
	
	public String getAddonName() {
		return addonName;
	}

	public void setAddonName(String addonName) {
		this.addonName = addonName;
	}

	public Double getAddonRate() {
		return addonRate;
	}

	public void setAddonRate(Double addonRate) {
		this.addonRate = addonRate;
	}

	public Boolean getAddonIsActive() {
		return addonIsActive;
	}

	public void setAddonIsActive(Boolean addonIsActive) {
		this.addonIsActive = addonIsActive;
	}

	public Integer getAddonId() {
		return addonId;
	}

	public void setAddonId(Integer addonId) {
		this.addonId = addonId;
	}
	
	public Integer getPropertyAddonDetailId() {
		return propertyAddonDetailId;
	}

	public void setPropertyAddonDetailId(Integer propertyAddonDetailId) {
		this.propertyAddonDetailId = propertyAddonDetailId;
	}

	public Integer getAddonPropertyId() {
		return addonPropertyId;
	}

	public void setAddonPropertyId(Integer addonPropertyId) {
		this.addonPropertyId = addonPropertyId;
	}

	public Integer getPropertyAddonId() {
		return propertyAddonId;
	}

	public void setPropertyAddonId(Integer propertyAddonId) {
		this.propertyAddonId = propertyAddonId;
	}
	
	public Integer getContractTypeId() {
		return contractTypeId;
	}

	public void setContractTypeId(Integer contractTypeId) {
		this.contractTypeId = contractTypeId;
	}

	public String getNetRateAmount() {
		return netRateAmount;
	}

	public void setNetRateAmount(String netRateAmount) {
		this.netRateAmount = netRateAmount;
	}
	
	public String getPropertyAddonPolicy() {
		return propertyAddonPolicy;
	}

	public void setPropertyAddonPolicy(String propertyAddonPolicy) {
		this.propertyAddonPolicy = propertyAddonPolicy;
	}
	
	public Boolean getTravelDiscountIsActive() {
		return travelDiscountIsActive;
	}

	public void setTravelDiscountIsActive(Boolean travelDiscountIsActive) {
		this.travelDiscountIsActive = travelDiscountIsActive;
	}

	public Boolean getFlexibleStayIsActive() {
		return flexibleStayIsActive;
	}

	public void setFlexibleStayIsActive(Boolean flexibleStayIsActive) {
		this.flexibleStayIsActive = flexibleStayIsActive;
	}

	public String getCancellationCommission() {
		return cancellationCommission;
	}

	public void setCancellationCommission(String cancellationCommission) {
		this.cancellationCommission = cancellationCommission;
	}
	
	public Integer getFirstPartnerPropertyId() {
		return firstPartnerPropertyId;
	}

	public void setFirstPartnerPropertyId(Integer firstPartnerPropertyId) {
		this.firstPartnerPropertyId = firstPartnerPropertyId;
	}

	public Integer getSelectedPartnerPropertyId() {
		return selectedPartnerPropertyId;
	}

	public void setSelectedPartnerPropertyId(Integer selectedPartnerPropertyId) {
		this.selectedPartnerPropertyId = selectedPartnerPropertyId;
	}
}
