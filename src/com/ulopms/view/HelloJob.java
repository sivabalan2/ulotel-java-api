package com.ulopms.view;



import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.ulopms.controller.BookingDetailManager;
import com.ulopms.controller.BookingGuestDetailManager;
import com.ulopms.controller.PmsBookingManager;
import com.ulopms.controller.PmsGuestManager;
import com.ulopms.controller.PmsPropertyManager;
import com.ulopms.model.BookingGuestDetail;
import com.ulopms.model.DashBoard;
import com.ulopms.model.PmsBooking;
import com.ulopms.model.PmsGuest;
import com.ulopms.model.PmsProperty;
 
/**
 * This class defines a quartz job.
 * @author javawithease
 */
public class HelloJob implements Job{
	
	private List<PmsBooking> bookingList;
	
	public void execute(JobExecutionContext context) throws JobExecutionException {  
		try{	
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date date = new Date();// Now use today date.
			String todayDate = sdf.format(date.getTime());
			PmsBookingManager bookingController = new PmsBookingManager();
			 //this.bookingList =  bookingController.findSmsId(todayDate);
			// getReviewSms();
//			 System.out.println("enter hello job");
			/* for(PmsBooking booking : bookingList ){
			 }*/
		/*	
		String name = "siva";
		String number = "8675273744";
		String apiKey = "apiKey=" + "Basic dWxvaG90ZWxzOlVMT0hUTEAyMDE4";
		String message = "&sms_text=" + "Thank you for signing up with Ulo Hotels. Our goal is offer best guest experience in budget stays. Want to know more about us? Check this link: www.ulohotels.com";


		//String sender = "&sender=" + "TXTLCL";
		String numbers = "&sms_to=" + "919944668918";
		String from = "&sms_from=" + "ULOHTL";
		String type = "&sms_type=" + "promo"; 
		// Send data
		HttpURLConnection conn = (HttpURLConnection) new URL("https://secureets.knowlarity.com/ets/dom/clients/ulohotels/api/ulohotels_sms_api.py").openConnection();
		//"http://etsrds.kapps.in/webapi/ulohotels/api/ulohotels_sms_api.py?sms_text=hi%20this%20gopinath%20from%20ulo&sms_to=%20918220211068&sms_from=ULOHTL&sms_type=promo"
		String data = numbers + message +  from + type;
		conn.setDoOutput(true);
		conn.setRequestMethod("POST");
		conn.addRequestProperty("auth_key", "Basic dWxvaG90ZWxzOlVMT0hUTEAyMDE4");
		conn.setRequestProperty("Content-Length", Integer.toString(data.length()));
		conn.getOutputStream().write(data.getBytes("UTF-8"));
		final BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		final StringBuffer stringBuffer = new StringBuffer();
		String line;
		
		while ((line = rd.readLine()) != null) {
			stringBuffer.append(line);
			
		}
		rd.close();*/
			} catch (Exception e) {
				e.printStackTrace();
			} finally {

			}
	}
	
	 public String getReviewSms()throws Exception{
		
 		 Date todayDate = new Date();
 		SimpleDateFormat localDateFormat = new SimpleDateFormat("yyyy-MM-dd");
 		String date = localDateFormat.format(todayDate);
 		PmsBookingManager bookingController = new PmsBookingManager();
 		BookingGuestDetailManager bookingGuestDetailController = new BookingGuestDetailManager();
 		PmsPropertyManager propertyController = new PmsPropertyManager();
 		PmsGuestManager guestController = new PmsGuestManager();
 		this.bookingList =  bookingController.findSmsId(date); 		
 		if(bookingList.size() > 0){
 		for(PmsBooking booking : bookingList ){
 			
 			List<BookingGuestDetail> bookingGuestDetailList = bookingGuestDetailController.findSmsGuestId(booking.getBookingId());
 			for(BookingGuestDetail bookingGuestDetail : bookingGuestDetailList)
			{
 			
				PmsProperty property = propertyController.find(booking.getBookingId());
 			PmsGuest guest = guestController.find(bookingGuestDetail.getPmsGuest().getGuestId());
 			
 			String number = guest.getPhone();
 			//String apiKey = "apiKey=" + "Basic dWxvaG90ZWxzOlVMT0hUTEAyMDE4";
 			String message = "&sms_text=" + "Thank you for staying in Ulo Hotels. We hope that you had an enjoyable and comfortable stay. Guest feedback is the main driving force for us. Do share your experiences to https://"+"property.getPropertyReviewLink()";

 			//String sender = "&sender=" + "TXTLCL";
 			String numbers = "&sms_to=" + "+91"+number;
 			String from = "&sms_from=" + "ULOHTL";
 			String type = "&sms_type=" + "trans"; 
 			// Send data
 			HttpURLConnection conn = (HttpURLConnection) new URL("https://secureets.knowlarity.com/ets/dom/clients/ulohotels/api/ulohotels_sms_api.py").openConnection();
 			//"http://etsrds.kapps.in/webapi/ulohotels/api/ulohotels_sms_api.py?sms_text=hi%20this%20gopinath%20from%20ulo&sms_to=%20918220211068&sms_from=ULOHTL&sms_type=promo"
 			String data = numbers + message +  from + type;
 			conn.setDoOutput(true);
 			conn.setRequestMethod("POST");
 			conn.addRequestProperty("auth_key", "Basic dWxvaG90ZWxzX1Q6VUxPSFRMQDIwMTg=");
 			conn.setRequestProperty("Content-Length", Integer.toString(data.length()));
 			conn.getOutputStream().write(data.getBytes("UTF-8"));
 			final BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
 			final StringBuffer stringBuffer = new StringBuffer();
 			/*String line;
 			
 			while ((line = rd.readLine()) != null) {
 				stringBuffer.append(line);
 				
 			}*/
 			rd.close();
			}
		 }
 		}
 		/*else{
 		}*/
 		return null;
		 }

	 
}
	
