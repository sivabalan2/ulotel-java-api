package com.ulopms.view;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Calendar;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TimeZone;
import java.util.TreeSet;

import com.ulopms.util.DateUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.ServletRequestAware;

import com.opensymphony.xwork2.ActionSupport;



import com.ulopms.controller.PmsDayManager;
import com.ulopms.controller.PmsPropertyManager;
import com.ulopms.controller.PromotionDetailManager;
import com.ulopms.controller.PromotionManager;
import com.ulopms.controller.PropertyAccommodationManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.PmsDays;
import com.ulopms.model.PmsPromotionDetails;
import com.ulopms.model.PmsPromotions;
import com.ulopms.model.PmsProperty;
import com.ulopms.model.PropertyAccommodation;
import com.ulopms.model.User;

public class PromotionDetailAction extends ActionSupport implements
ServletRequestAware,SessionAware, UserAware{
     
	

	private static final long serialVersionUID = 914982665758390091L;
    
	
	private String  checkedDays;
	private String promotionType;
	private Double percentage;
	private Double bookNights;
	private Double getNights;
	private Double bookRooms;
	private Double getRooms;
	private Double promotionBaseAmount;
	private Double promotionExtraAdult;
	private Double promotionExtraChild;
	private Integer promotionId;
	private String strPromotionId;
	private Integer promotionHours;
	private Integer userId;
	private Double promotionInr;
	
	private static final Logger logger = Logger.getLogger(PromotionDetailAction.class);
	
	

	private HttpSession session;

	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}
	
	private SessionMap<String,Object> sessionMap;  
	  
	//getters and setters  
	  
	@Override  
	public void setSession(Map<String, Object> map) {  
	    sessionMap=(SessionMap)map;  
	}  
	
	private User user;

	
	// @Override
	// public User getModel() {
	// return user;
	// }

	@Override
	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	
	private HttpServletRequest request;

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	public PromotionDetailAction() {

	}

	public String execute() {
		return SUCCESS;
	}
	
	public String getCheckedDays() {
		return checkedDays;
	}

	public void setCheckedDays(String checkedDays) {
		this.checkedDays = checkedDays;
	}
	 
	public String getPromotionType(){
		return promotionType;
	}

	public void setPromotionType(String promotionType){
		this.promotionType=promotionType;
	}
	
	public Double getPercentage() {
		return percentage;
	}

	public void setPercentage(Double percentage) {
		this.percentage = percentage;
	}

	public Double getBookNights() {
		return bookNights;
	}

	public void setBookNights(Double bookNights) {
		this.bookNights = bookNights;
	}

	public Double getGetNights() {
		return getNights;
	}

	public void setGetNights(Double getNights) {
		this.getNights = getNights;
	}

	public Double getBookRooms() {
		return bookRooms;
	}

	public void setBookRooms(Double bookRooms) {
		this.bookRooms = bookRooms;
	}

	public Double getGetRooms() {
		return getRooms;
	}

	public void setGetRooms(Double getRooms) {
		this.getRooms = getRooms;
	}

	public Double getPromotionBaseAmount() {
		return promotionBaseAmount;
	}

	public void setPromotionBaseAmount(Double promotionBaseAmount) {
		this.promotionBaseAmount = promotionBaseAmount;
	}

	public Double getPromotionExtraAdult() {
		return promotionExtraAdult;
	}

	public void setPromotionExtraAdult(Double promotionExtraAdult) {
		this.promotionExtraAdult = promotionExtraAdult;
	}

	public Double getPromotionExtraChild() {
		return promotionExtraChild;
	}

	public void setPromotionExtraChild(Double promotionExtraChild) {
		this.promotionExtraChild = promotionExtraChild;
	}

	public Integer getPromotionId() {
		return promotionId;
	}

	public void setPromotionId(Integer promotionId) {
		this.promotionId = promotionId;
	}
	
	public String getStrPromotionId() {
		return strPromotionId;
	}

	public void setStrPromotionId(String strPromotionId) {
		this.strPromotionId = strPromotionId;
	}
	
	public Integer getPromotionHours(){
		return promotionHours;
	}
	
	public void setPromotionHours(Integer promotionHours){
		this.promotionHours=promotionHours;
	}
	 
	public String addPromotionDetails(){

		this.promotionId = (Integer) sessionMap.get("promotionId");
		this.strPromotionId = (String) sessionMap.get("strPromotionId");
		this.userId=(Integer)sessionMap.get("userId");
		this.promotionType=getPromotionType();
		sessionMap.put("promotionType", promotionType);
		
		this.promotionHours=getPromotionHours();
		sessionMap.put("promotionHours", promotionHours);
		
		this.checkedDays=getCheckedDays();
		sessionMap.put("checkedDays", checkedDays);
		
		this.getRooms=getGetRooms();
		sessionMap.put("getRooms", getRooms);
		
		this.getNights=getGetNights();
		sessionMap.put("getNights", getNights);
		
		this.bookRooms=getBookRooms();
		sessionMap.put("bookRooms", bookRooms);
		
		this.bookNights=getBookNights();
		sessionMap.put("bookNights", bookNights);
		
		this.promotionBaseAmount=getPromotionBaseAmount();
		sessionMap.put("promotionBaseAmount", promotionBaseAmount);
		
		this.promotionExtraAdult=getPromotionExtraAdult();
		sessionMap.put("promotionExtraAdult", promotionExtraAdult);
		
		this.promotionExtraChild=getPromotionExtraChild();
		sessionMap.put("promotionExtraChild", promotionExtraChild);
		
		PmsPromotionDetails promotionDetails=new PmsPromotionDetails();
		PromotionManager promotionController=new PromotionManager();
		PromotionDetailManager promotiondetailController=new PromotionDetailManager();
		PmsDayManager dayController = new PmsDayManager();
		
		try{
			SimpleDateFormat sdfformat = new SimpleDateFormat("yyyy-MM-dd");
			java.util.Date date=new java.util.Date();
        	Calendar calDate=Calendar.getInstance();
        	calDate.setTime(date);
        	calDate.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
        	String strCurrentDate = new SimpleDateFormat("yyyy-MM-dd").format(calDate.getTime());
		    java.util.Date currentDate = sdfformat.parse(strCurrentDate);
		    Timestamp tsCurrentDate = new Timestamp(currentDate.getTime());
		    
			String[] PromotionIdArray = strPromotionId.split("\\s*,\\s*");
			for (int j = 0; j < PromotionIdArray.length; j++){  
				String[] stringArray = getCheckedDays().split("\\s*,\\s*");
			    int[] intArray = new int[stringArray.length];
			    for (int i = 0; i < stringArray.length; i++) {
			    	intArray[i] = Integer.parseInt(stringArray[i]);
			    	if(intArray[i]!=0){
				    	promotionDetails.setIsActive(true);
						promotionDetails.setIsDeleted(false);
						PmsDays day = dayController.find(Integer.parseInt(stringArray[i]));
						promotionDetails.setDayPart(Integer.parseInt(stringArray[i]));
						promotionDetails.setDaysOfWeek(day.getDaysOfWeek());
						PmsPromotions promotions=promotionController.find(Integer.parseInt(PromotionIdArray[j]));
						promotionDetails.setPmsPromotions(promotions);
						promotionDetails.setCreatedBy(this.userId);
						promotionDetails.setCreatedDate(tsCurrentDate);
						
						if(promotionType.equalsIgnoreCase("R"))
						{
							promotionDetails.setBaseAmount(promotionBaseAmount);
							promotionDetails.setExtraAdult(promotionExtraAdult);
							promotionDetails.setExtraChild(promotionExtraChild);
							promotionDetails.setBookRooms(bookRooms);
							promotionDetails.setGetRooms(getRooms);
							
						}
						if(promotionType.equalsIgnoreCase("N")){
							promotionDetails.setBaseAmount(promotionBaseAmount);
							promotionDetails.setExtraAdult(promotionExtraAdult);
							promotionDetails.setExtraChild(promotionExtraChild);
							promotionDetails.setBookNights(bookNights);
							promotionDetails.setGetNights(getNights);
						}
						if(promotionType.equalsIgnoreCase("L")){
							promotionDetails.setPromotionHours(promotionHours);
							promotionDetails.setPromotionPercentage(percentage);
						}
						if(promotionType.equalsIgnoreCase("F")){
							promotionDetails.setPromotionPercentage(percentage);
						}
						
						promotiondetailController.add(promotionDetails);
			    	}
			    }
			}
			
			
			
		}
		catch(Exception e){
			logger.error(e);
			e.printStackTrace();
		}finally{
			
		}
		return null;
	}
	
	public String addNewPromotionDetails(){
		try{
			this.strPromotionId = (String) sessionMap.get("strPromotionId");
			this.userId=(Integer)sessionMap.get("userId");
			this.promotionType=getPromotionType();
			this.checkedDays=getCheckedDays();
			
			
			PmsPromotionDetails promotionDetails=new PmsPromotionDetails();
			PromotionManager promotionController=new PromotionManager();
			PromotionDetailManager promotiondetailController=new PromotionDetailManager();
			PmsDayManager dayController = new PmsDayManager();
			
			SimpleDateFormat sdfformat = new SimpleDateFormat("yyyy-MM-dd");
			java.util.Date date=new java.util.Date();
        	Calendar calDate=Calendar.getInstance();
        	calDate.setTime(date);
        	calDate.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
        	String strCurrentDate = new SimpleDateFormat("yyyy-MM-dd").format(calDate.getTime());
		    java.util.Date currentDate = sdfformat.parse(strCurrentDate);
		    Timestamp tsCurrentDate = new Timestamp(currentDate.getTime());
		    
			String[] PromotionIdArray = strPromotionId.split("\\s*,\\s*");
			for (int j = 0; j < PromotionIdArray.length; j++){  
				String[] stringArray = getCheckedDays().split("\\s*,\\s*");
			    int[] intArray = new int[stringArray.length];
			    for (int i = 0; i < stringArray.length; i++) {
			    	intArray[i] = Integer.parseInt(stringArray[i]);
			    	if(intArray[i]!=0){
				    	promotionDetails.setIsActive(true);
						promotionDetails.setIsDeleted(false);
						PmsDays day = dayController.find(Integer.parseInt(stringArray[i]));
						promotionDetails.setDayPart(Integer.parseInt(stringArray[i]));
						promotionDetails.setDaysOfWeek(day.getDaysOfWeek());
						PmsPromotions promotions=promotionController.find(Integer.parseInt(PromotionIdArray[j]));
						promotionDetails.setPmsPromotions(promotions);
						promotionDetails.setCreatedBy(this.userId);
						promotionDetails.setCreatedDate(tsCurrentDate);
						
						
						if(promotionType.equalsIgnoreCase("L")){
							promotionDetails.setPromotionPercentage(percentage);
							promotionDetails.setPromotionInr(getPromotionInr());
						}
						if(promotionType.equalsIgnoreCase("F")){
							promotionDetails.setPromotionPercentage(percentage);
							promotionDetails.setPromotionInr(getPromotionInr());
						}
						
						if(promotionType.equalsIgnoreCase("E")){
							promotionDetails.setPromotionPercentage(percentage);
							promotionDetails.setPromotionInr(getPromotionInr());
						}
						
						promotiondetailController.add(promotionDetails);
			    	}
			    }
			}
			
			
			
		}
		catch(Exception e){
			logger.error(e);
			e.printStackTrace();
		}finally{
			
		}
		return null;
	}
	
	public Double getPromotionInr() {
		return promotionInr;
	}

	public void setPromotionInr(Double promotionInr) {
		this.promotionInr = promotionInr;
	}

}
