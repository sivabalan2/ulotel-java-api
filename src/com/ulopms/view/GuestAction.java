package com.ulopms.view;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;

import com.ulopms.api.Search;
import com.ulopms.api.Users;
import com.ulopms.controller.PmsGuestManager;
import com.ulopms.controller.UserLoginManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.PmsGuest;
import com.ulopms.model.User;
import com.ulopms.util.Email;
//import com.medwecare.controller.AwsObjectManager;
//import com.medwecare.controller.CommentManager;
//import com.medwecare.controller.CommentUserManager;
////import com.medwecare.controller.NotificationManager;
//import com.medwecare.controller.ProjectManager;
//import com.medwecare.model.AwsObject;
//import com.medwecare.model.Notification;
//import com.medwecare.model.ObjectComment;
//import com.medwecare.model.ObjectCommentUser;
//import com.medwecare.model.Project;
import com.opensymphony.xwork2.ActionSupport;

import freemarker.template.Configuration;
import freemarker.template.Template;

public class GuestAction extends ActionSupport implements
ServletRequestAware,SessionAware,UserAware{

	private static final long serialVersionUID = 914982665758390091L;

	private int guestId;	
	private String address1;
	private String address2;
	private String city;
	private String countryCode;
	private String emailId;
	private String firstName;
	private String landline;
	private String lastName;
	private String phone;
	private String zipCode;
	private String stateCode;
	private String bookingId;
	private String expectReason;
	private String otherReason;
	private String payAtHotelOtp;	
	
	private File myFile;
	private String myFileContentType;
	private String myFileFileName;
	
	private PmsGuest guest;
	
	private List<PmsGuest> guestList;
	

	private static final Logger logger = Logger.getLogger(GuestAction.class);
    
	private HttpSession session;

	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}
	
	private SessionMap<String,Object> sessionMap;  
	  
	//getters and setters  
	  
	@Override  
	public void setSession(Map<String, Object> map) {  
	    sessionMap=(SessionMap)map;  
	}  
	
	private User user;

	
	// @Override
	// public User getModel() {
	// return user;
	// }

	@Override
	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	
	private HttpServletRequest request;

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	public GuestAction() {

	}

	public String execute() {

		//this.patientId = getPatientId();
		return SUCCESS;
	}

	
	public String getGuests() throws IOException {
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PmsGuestManager guestController = new PmsGuestManager();
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");

			PmsGuest guest = guestController.find(getGuestId());
			
				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"DT_RowId\":\"" + guest.getGuestId() + "\"";
				jsonOutput += ",\"address1\":\"" + guest.getAddress1()+ "\"";
				jsonOutput += ",\"address2\":\"" + guest.getAddress2()+ "\"";
				jsonOutput += ",\"city\":\"" + guest.getCity() + "\"";
				jsonOutput += ",\"countryCode\":\"" + guest.getCountryCode() + "\"";
				jsonOutput += ",\"emailId\":\"" + guest.getEmailId() + "\"";
				jsonOutput += ",\"firstName\":\"" + guest.getFirstName() + "\"";
				jsonOutput += ",\"landline\":\"" + (guest.getLandline()== null ? "": guest.getLandline()) + "\"";
				jsonOutput += ",\"lastName\":\"" + (guest.getLastName() == null ? "": guest.getLastName())+ "\"";
				jsonOutput += ",\"phone\":\"" + (guest.getPhone() == null ? "": guest.getPhone()) + "\"";
				jsonOutput += ",\"zipCode\":\"" + (guest.getZipCode() == null ? "": guest.getZipCode()) + "\"";
				jsonOutput += ",\"stateName\":\"" + (guest.getState().getStateName() == null ? "": guest.getState().getStateName() )+ "\"";
				
				jsonOutput += "}";

			

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	public String getAllCustomer() throws IOException {
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PmsGuestManager guestController = new PmsGuestManager();
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");

			this.guestList = guestController.list();
			for (PmsGuest guest : guestList) {

				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"DT_RowId\":\"" + guest.getGuestId() + "\"";
				jsonOutput += ",\"address1\":\"" + guest.getAddress1()+ "\"";
				jsonOutput += ",\"address2\":\"" + guest.getAddress2()+ "\"";
				jsonOutput += ",\"city\":\"" + guest.getCity() + "\"";
				jsonOutput += ",\"countryCode\":\"" + guest.getCountryCode() + "\"";
				jsonOutput += ",\"emailId\":\"" + guest.getEmailId() + "\"";
				jsonOutput += ",\"firstName\":\"" + guest.getFirstName() + "\"";
				jsonOutput += ",\"landline\":\"" + (guest.getLandline()== null ? "": guest.getLandline()) + "\"";
				jsonOutput += ",\"lastName\":\"" + (guest.getLastName() == null ? "": guest.getLastName())+ "\"";
				jsonOutput += ",\"phone\":\"" + (guest.getPhone() == null ? "": guest.getPhone()) + "\"";
				jsonOutput += ",\"zipCode\":\"" + (guest.getZipCode() == null ? "": guest.getZipCode()) + "\"";
				jsonOutput += ",\"stateName\":\"" + (guest.getState().getStateName() == null ? "": guest.getState().getStateName() )+ "\"";
				
				jsonOutput += "}";

			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}

	public String editGuestDetails(){
		try{
			PmsGuestManager guestController = new PmsGuestManager();
			this.guestId=getGuestId();
			PmsGuest guest = guestController.find(this.guestId);
			guest.setFirstName(getFirstName());
			guest.setEmailId(getEmailId());
			guest.setPhone(getPhone());
			guestController.edit(guest);
			
		}catch(Exception e){
			logger.error(e);
			e.printStackTrace();
		}finally{
			
		}
		return null;
	}
	
	public String addGuest() {
		
		PmsGuestManager guestController = new PmsGuestManager();
		//UserLoginManager  userController = new UserLoginManager();
		String strReturn = "";	
		try {
			
				
			PmsGuest guest = new PmsGuest();
			
			
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			
			guest.setAddress1(getAddress1());
			guest.setAddress2(getAddress2());
			guest.setCity(getCity());
			guest.setEmailId(getEmailId());
			guest.setFirstName(getFirstName());
			guest.setLandline(getLandline());
			guest.setLastName(getLastName());
			guest.setIsActive(true);
			guest.setIsDeleted(false);
			guest.setPhone(getPhone());
			guest.setZipCode(getZipCode());
			
			
			SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
			Calendar today=Calendar.getInstance();
		    String strCurrentDate = new SimpleDateFormat("yyyy-MM-dd").format(today.getTime());
		    java.util.Date currentDate = format1.parse(strCurrentDate);
		    Timestamp currentTS = new Timestamp(currentDate.getTime());
		    guest.setCreatedDate(currentTS);
		    
			PmsGuest guestli = guestController.add(guest);
			
			this.guestId = guestli.getGuestId();
			sessionMap.put("guestId",guestId);
			
			
			/*List<PmsGuest> searchGuest = guestController.findGuestByEmail(emailId);
			if(searchGuest.size()==0)
			{
			
				guest.setAddress1(getAddress1());
				guest.setAddress2(getAddress2());
				guest.setCity(getCity());
				guest.setEmailId(getEmailId());
				guest.setFirstName(getFirstName());
				guest.setLandline(getLandline());
				guest.setLastName(getLastName());
				guest.setIsActive(true);
				guest.setIsDeleted(false);
				guest.setPhone(getPhone());
				guest.setZipCode(getZipCode());
				
				
				SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
				Calendar today=Calendar.getInstance();
			    String strCurrentDate = new SimpleDateFormat("yyyy-MM-dd").format(today.getTime());
			    java.util.Date currentDate = format1.parse(strCurrentDate);
			    Timestamp currentTS = new Timestamp(currentDate.getTime());
			    guest.setCreatedDate(currentTS);
			    
				PmsGuest guestli = guestController.add(guest);
				
				this.guestId = guestli.getGuestId();
				sessionMap.put("guestId",guestId);
				
			}else{
				for(PmsGuest pg : searchGuest){
					Integer guestId=pg.getGuestId();
					PmsGuest pmsGuest=guestController.find(guestId);
					SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
					Calendar today=Calendar.getInstance();
				    String strCurrentDate = new SimpleDateFormat("yyyy-MM-dd").format(today.getTime());
				    java.util.Date currentDate = format1.parse(strCurrentDate);
				    Timestamp currentTS = new Timestamp(currentDate.getTime());
				    pmsGuest.setModifiedDate(currentTS);
				    guestController.edit(pmsGuest);
						
					sessionMap.put("guestId",pg.getGuestId());
					
					this.guestId = (Integer) sessionMap.get("guestId");
				
				}
			}*/
			//customer.setIcecontactno(getIcecontactno());
			
			/*if(this.getMyFileFileName()!=null && !this.getMyFileFileName().equalsIgnoreCase(""))
				guest.setImgPath(this.getMyFileFileName());
			
			PmsGuest guest1 = guestController.add(guest);
			
			if(guest1.getGuestId()>0 && this.getMyFileFileName()!=null && !this.getMyFileFileName().equalsIgnoreCase(""))
			{
				File file = new File(getText("storage.user.logopath") + "/"
					+ guest1.getGuestId() + "/", this.getMyFileFileName());
				FileUtils.copyFile(this.myFile, file);// copying image in the
			}*/
			strReturn="success";
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
			strReturn="error";
		} finally {

		}
		return strReturn;
	}
	
	public String editGuest() {
		PmsGuestManager guestController = new PmsGuestManager();
		UserLoginManager  userController = new UserLoginManager();
		try {
			PmsGuest guest = guestController.find(getGuestId());
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			guest.setAddress1(getAddress1());
			guest.setAddress2(getAddress2());
			guest.setCity(getCity());
			guest.setEmailId(getEmailId());
			guest.setFirstName(getFirstName());
			guest.setLandline(getLandline());
			guest.setLastName(getLastName());
			guest.setIsActive(true);
			guest.setIsDeleted(false);
			guest.setPhone(getPhone());
			guest.setZipCode(getZipCode());
			
			/*if(this.getMyFileFileName()!=null && !this.getMyFileFileName().equalsIgnoreCase(""))
				guest.setImgPath(this.getMyFileFileName());
			PmsGuest guest1 = guestController.edit(guest);
			
			if(guest1.getGuestId()>0 && this.getMyFileFileName()!=null && !this.getMyFileFileName().equalsIgnoreCase(""))
			{
				File file = new File(getText("storage.user.logopath") + "/"
					+ guest1.getGuestId() + "/", this.getMyFileFileName());
				FileUtils.copyFile(this.myFile, file);// copying image in the
			}
			*/
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return SUCCESS;
	}
	
	public String deleteGuest() {
		PmsGuestManager guestController = new PmsGuestManager();
		UserLoginManager  userController = new UserLoginManager();
		try {
			
						
			PmsGuest guest = guestController.find(getGuestId());
						
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			guest.setIsActive(false);
			guest.setIsDeleted(true);
			guest.setModifiedDate(new java.sql.Timestamp(date.getTime()));
			guestController.edit(guest);
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}
	
	public String sendPayAtHotelOtpApi(Users users){
		String output = "";	
		try {
			boolean blnMobile=false,blnEmail=false;
			int count=0,mailCount=0;
			PmsGuestManager guestController = new PmsGuestManager();
			int randomPin   =(int)(Math.random()*900000)+100000;
			String otp  =String.valueOf(randomPin);
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			this.emailId=users.getEmailId();
			
			List<PmsGuest> listMobileGuest=guestController.listCheckMobileNumber(users.getMobileNumber());
			if(listMobileGuest.size()>0 && !listMobileGuest.isEmpty()){
				blnMobile=true;
			}
			if(blnMobile){
				if(listMobileGuest.size()>0 && !listMobileGuest.isEmpty()){
					for(PmsGuest guestphone:listMobileGuest){
						mailCount++;
						if((int)listMobileGuest.size()==mailCount){
							PmsGuest phone=guestController.find(guestphone.getGuestId());
							this.emailId=phone.getEmailId();	
						}
						
					}
				}	
			}
			mailCount=0;
			List<PmsGuest> listEmailGuest=guestController.listCheckEmail(this.emailId);
			if(listEmailGuest.size()>0 && !listEmailGuest.isEmpty()){
				blnEmail=true;
			}
			
			
			
			if(blnEmail){
				if(listEmailGuest.size()>0 && !listEmailGuest.isEmpty()){
					for(PmsGuest guestemail:listEmailGuest){
						mailCount++;
						if((int)listEmailGuest.size()==mailCount){
							PmsGuest pmsGuest=guestController.find(guestemail.getGuestId());
							pmsGuest.setPhone(users.getMobileNumber());
							pmsGuest.setPayAtHotelOtp(otp);
							PmsGuest guests=guestController.edit(pmsGuest);
							this.guestId=guests.getGuestId();
							this.payAtHotelOtp=guests.getPayAtHotelOtp();
							this.phone=guests.getPhone();	
						}
					}
				}
			}else{
				PmsGuest guest = new PmsGuest();
				guest.setEmailId(users.getEmailId());
				guest.setFirstName(users.getFullName());
				guest.setIsActive(true);
				guest.setIsDeleted(false);
				guest.setPhone(users.getMobileNumber());
				guest.setPayAtHotelOtp(otp);
				
				
				SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
				Calendar today=Calendar.getInstance();
			    String strCurrentDate = new SimpleDateFormat("yyyy-MM-dd").format(today.getTime());
			    java.util.Date currentDate = format1.parse(strCurrentDate);
			    Timestamp currentTS = new Timestamp(currentDate.getTime());
			    guest.setCreatedDate(currentTS);
			    
			    PmsGuest guests = guestController.add(guest);
				this.guestId=guests.getGuestId();
				this.payAtHotelOtp=guests.getPayAtHotelOtp();
				this.phone=guests.getPhone();
					
			}
			
			
			
			String message = "&sms_text="+"Dear Guest, Your OTP code for Ulohotels is "+ otp;
			String numbers = "&sms_to=" + "+91"+this.phone;
			String from = "&sms_from=" + "ULOHTL";
			String type = "&sms_type=" + "trans"; 
			// Send data
			HttpURLConnection conn = (HttpURLConnection) new URL("https://secureets.knowlarity.com/ets/dom/clients/ulohotels/api/ulohotels_sms_api.py").openConnection();
			//"http://etsrds.kapps.in/webapi/ulohotels/api/ulohotels_sms_api.py?sms_text=hi%20this%20gopinath%20from%20ulo&sms_to=%20918220211068&sms_from=ULOHTL&sms_type=promo"
			String data = numbers + message +  from + type;
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.addRequestProperty("auth_key", "Basic dWxvaG90ZWxzX1Q6VUxPSFRMQDIwMTg=");
			conn.setRequestProperty("Content-Length", Integer.toString(data.length()));
			conn.getOutputStream().write(data.getBytes("UTF-8"));
			final BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			final StringBuffer stringBuffer = new StringBuffer();
			/*String line;
			
			while ((line = rd.readLine()) != null) {
				stringBuffer.append(line);
				
			}*/
			rd.close();
		    
			String jsonOutput = "";
				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				PmsGuest guests = guestController.find(this.guestId);
				if(guests!=null){
					count++;
				}
				jsonOutput += "\"guestId\":\"" + this.guestId + "\"";
				jsonOutput += ",\"payAtHotelOtp\":\"" + this.payAtHotelOtp + "\"";
				jsonOutput += ",\"phone\":\"" + this.phone + "\"";
				
				jsonOutput += "}";

				String strdata="\"data\":[" + jsonOutput + "]";
	 			String strData=strdata.replaceAll("\"", "\"");
	 			
	 			if(count == 0){
	 				output="{\"error\":\"1\",\"message\":\"No Records Found\"}";
				}else{
					output="{\"error\":\"0\"," + strData + "}";
				}
				
			} catch (Exception e) {
				output="{\"error\":\"2\",\"message\":\"Some technical error on send otp for pay at hotel api \"}";
				logger.error(e);
				e.printStackTrace();
			} finally {

			}
			return output;
		
	}
	
	public String resendPayAtHotelApi(Users users){
		String output = "";	
		try {
			PmsGuestManager guestController = new PmsGuestManager();
			int count=0,phoneCount=0;
			boolean blnMobile=false;
			int randomPin   =(int)(Math.random()*900000)+100000;
			String otp  = String.valueOf(randomPin);
			List<PmsGuest> listMobileGuest=guestController.listCheckMobileNumber(users.getMobileNumber());
			if(listMobileGuest.size()>0 && !listMobileGuest.isEmpty()){
				for(PmsGuest mobileguest:listMobileGuest){
					phoneCount++;
					if((int)listMobileGuest.size() == phoneCount){
						PmsGuest pmsGuest=guestController.find(mobileguest.getGuestId());
						pmsGuest.setPhone(users.getMobileNumber());
						pmsGuest.setPayAtHotelOtp(otp);
						PmsGuest guests=guestController.edit(pmsGuest);
						this.guestId=guests.getGuestId();
						this.phone=guests.getPhone();
						this.payAtHotelOtp=guests.getPayAtHotelOtp();
					}
					blnMobile=true;
				}
			}
			if(blnMobile){
				
			}
			if(!blnMobile){
				PmsGuest guest = new PmsGuest();
				guest.setEmailId(users.getEmailId());
				guest.setFirstName(users.getFullName());
				guest.setIsActive(true);
				guest.setIsDeleted(false);
				guest.setPhone(users.getMobileNumber());
				guest.setPayAtHotelOtp(otp);
				
				
				SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
				Calendar today=Calendar.getInstance();
			    String strCurrentDate = new SimpleDateFormat("yyyy-MM-dd").format(today.getTime());
			    java.util.Date currentDate = format1.parse(strCurrentDate);
			    Timestamp currentTS = new Timestamp(currentDate.getTime());
			    guest.setCreatedDate(currentTS);
			    
			    PmsGuest guests = guestController.add(guest);
				this.guestId=guests.getGuestId();
				this.payAtHotelOtp=guests.getPayAtHotelOtp();
				this.phone=guests.getPhone();
			}
			
			
			String message = "&sms_text="+"Dear Guest, Your OTP code for Ulohotels is "+ otp;

			//String sender = "&sender=" + "TXTLCL";
			String numbers = "&sms_to=" + "+91"+this.phone;
			String from = "&sms_from=" + "ULOHTL";
			String type = "&sms_type=" + "trans"; 
			// Send data
			HttpURLConnection conn = (HttpURLConnection) new URL("https://secureets.knowlarity.com/ets/dom/clients/ulohotels/api/ulohotels_sms_api.py").openConnection();
			//"http://etsrds.kapps.in/webapi/ulohotels/api/ulohotels_sms_api.py?sms_text=hi%20this%20gopinath%20from%20ulo&sms_to=%20918220211068&sms_from=ULOHTL&sms_type=promo"
			String data = numbers + message +  from + type;
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.addRequestProperty("auth_key", "Basic dWxvaG90ZWxzX1Q6VUxPSFRMQDIwMTg=");
			conn.setRequestProperty("Content-Length", Integer.toString(data.length()));
			conn.getOutputStream().write(data.getBytes("UTF-8"));
			final BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			final StringBuffer stringBuffer = new StringBuffer();
			String line;
			
			while ((line = rd.readLine()) != null) {
				stringBuffer.append(line);
				
			}
			rd.close();
				
			String jsonOutput = "";
			
			if (!jsonOutput.equalsIgnoreCase(""))
				jsonOutput += ",{";
			else
				jsonOutput += "{";
			PmsGuest guests = guestController.find(this.guestId);
			if(guests!=null){
				count++;
			}
			jsonOutput += "\"guestId\":\"" + this.guestId + "\"";
			jsonOutput += ",\"payAtHotelOtp\":\"" + guests.getPayAtHotelOtp() + "\"";
			jsonOutput += ",\"phone\":\"" + guests.getPhone() + "\"";
			
			
			jsonOutput += "}";

			String strdata="\"data\":[" + jsonOutput + "]";
 			String strData=strdata.replaceAll("\"", "\"");
 			
 			if(count == 0){
 				output="{\"error\":\"1\",\"message\":\"No Records Found\"}";
			}else{
				output="{\"error\":\"0\"," + strData + "}";
			}
			} catch (Exception e) {
				output="{\"error\":\"2\",\"message\":\"Some technical error on resend otp for pay at hotel api\"}";
				logger.error(e);
				e.printStackTrace();
			} finally {

			}
			return output;
		
	}
	
	
	public String sendPayAtHotelOtp() {
		
	PmsGuestManager guestController = new PmsGuestManager();
	//UserLoginManager  userController = new UserLoginManager();
	String strReturn = "";	
	try {
		
		int randomPin   =(int)(Math.random()*900000)+100000;
		String otp  =String.valueOf(randomPin);
			
		PmsGuest guest = new PmsGuest();
		
		
		long time = System.currentTimeMillis();
		java.sql.Date date = new java.sql.Date(time);
		
		
		guest.setAddress1(getAddress1());
		guest.setAddress2(getAddress2());
		guest.setCity(getCity());
		guest.setEmailId(getEmailId());
		guest.setFirstName(getFirstName());
		guest.setLandline(getLandline());
		guest.setLastName(getLastName());
		guest.setIsActive(true);
		guest.setIsDeleted(false);
		guest.setPhone(getPhone());
		guest.setZipCode(getZipCode());
		guest.setPayAtHotelOtp(otp);
		
		
		SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
		Calendar today=Calendar.getInstance();
	    String strCurrentDate = new SimpleDateFormat("yyyy-MM-dd").format(today.getTime());
	    java.util.Date currentDate = format1.parse(strCurrentDate);
	    Timestamp currentTS = new Timestamp(currentDate.getTime());
	    guest.setCreatedDate(currentTS);
	    
	    PmsGuest guestli = guestController.add(guest);
		
		this.guestId = guestli.getGuestId();
		sessionMap.put("guestId",guestId);
		
		String message = "&sms_text="+"Dear Guest, Your OTP code for Ulohotels is "+ otp;
		//String sender = "&sender=" + "TXTLCL";
		String numbers = "&sms_to=" + "+91"+getPhone();
		String from = "&sms_from=" + "ULOHTL";
		String type = "&sms_type=" + "trans"; 
		// Send data
		HttpURLConnection conn = (HttpURLConnection) new URL("https://secureets.knowlarity.com/ets/dom/clients/ulohotels/api/ulohotels_sms_api.py").openConnection();
		//"http://etsrds.kapps.in/webapi/ulohotels/api/ulohotels_sms_api.py?sms_text=hi%20this%20gopinath%20from%20ulo&sms_to=%20918220211068&sms_from=ULOHTL&sms_type=promo"
		String data = numbers + message +  from + type;
		conn.setDoOutput(true);
		conn.setRequestMethod("POST");
		conn.addRequestProperty("auth_key", "Basic dWxvaG90ZWxzX1Q6VUxPSFRMQDIwMTg=");
		conn.setRequestProperty("Content-Length", Integer.toString(data.length()));
		conn.getOutputStream().write(data.getBytes("UTF-8"));
		final BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		final StringBuffer stringBuffer = new StringBuffer();
		/*String line;
		
		while ((line = rd.readLine()) != null) {
			stringBuffer.append(line);
			
		}*/
		rd.close();
	    
		String jsonOutput = "";
		HttpServletResponse response = ServletActionContext.getResponse();
		
		response.setContentType("application/json");
			
			if (!jsonOutput.equalsIgnoreCase(""))
				jsonOutput += ",{";
			else
				jsonOutput += "{";
			PmsGuest guests = guestController.find(getGuestId());
			
			jsonOutput += "\"guestId\":\"" + this.guestId + "\"";
			jsonOutput += ",\"payAtHotelOtp\":\"" + guests.getPayAtHotelOtp() + "\"";
			jsonOutput += ",\"phone\":\"" + guests.getPhone() + "\"";
			
			jsonOutput += "}";

		response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}


public String resendPayAtHotelOtp() {
	
	PmsGuestManager guestController = new PmsGuestManager();
	//UserLoginManager  userController = new UserLoginManager();
	String strReturn = "";	
	try {
		int randomPin   =(int)(Math.random()*900000)+100000;
		String otp  = String.valueOf(randomPin);
		
		PmsGuest guest = guestController.find(getGuestId());
		
		guest.setPayAtHotelOtp(otp);
		
		guestController.edit(guest);
		
		String message = "&sms_text="+"Dear Guest, Your OTP code for Ulohotels is "+ otp;

		//String sender = "&sender=" + "TXTLCL";
		String numbers = "&sms_to=" + "+91"+guest.getPhone();
		String from = "&sms_from=" + "ULOHTL";
		String type = "&sms_type=" + "trans"; 
		// Send data
		HttpURLConnection conn = (HttpURLConnection) new URL("https://secureets.knowlarity.com/ets/dom/clients/ulohotels/api/ulohotels_sms_api.py").openConnection();
		//"http://etsrds.kapps.in/webapi/ulohotels/api/ulohotels_sms_api.py?sms_text=hi%20this%20gopinath%20from%20ulo&sms_to=%20918220211068&sms_from=ULOHTL&sms_type=promo"
		String data = numbers + message +  from + type;
		conn.setDoOutput(true);
		conn.setRequestMethod("POST");
		conn.addRequestProperty("auth_key", "Basic dWxvaG90ZWxzX1Q6VUxPSFRMQDIwMTg=");
		conn.setRequestProperty("Content-Length", Integer.toString(data.length()));
		conn.getOutputStream().write(data.getBytes("UTF-8"));
		final BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		final StringBuffer stringBuffer = new StringBuffer();
		String line;
		
		while ((line = rd.readLine()) != null) {
			stringBuffer.append(line);
			
		}
		rd.close();
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}

		public String verifyPayatHotelOtp(Users users){
			String output = "";	
			try 
			{
				String jsonOutput="";
				boolean status=false;
				int count=0;
				PmsGuestManager guestController = new PmsGuestManager();
				PmsGuest guest = guestController.findSmsId(Integer.parseInt(users.getGuestId()),users.getGuestOtp());
				if(guest != null){
					status=true;
					if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += ",{";
					else
						jsonOutput += "{";
						PmsGuest guests=guestController.find(guest.getGuestId());
						jsonOutput += "\"guestId\":\"" + guests.getGuestId() + "\"";
						jsonOutput += ",\"guestName\":\"" + guests.getFirstName()+ "\"";
						jsonOutput += ",\"mobileNumber\":\"" + guests.getPhone() + "\"";
						jsonOutput += ",\"emailId\":\"" + guests.getEmailId()+ "\"";
						jsonOutput += ",\"status\":\"" + status+ "\"";
					
					jsonOutput += "}";
					count++;
				}
				else{
					status=false;
				}
				
				

				String strdata="\"data\":[" + jsonOutput + "]";
	 			String strData=strdata.replaceAll("\"", "\"");
	 			
	 			if(count == 0){
	 				output="{\"error\":\"1\",\"message\":\"Invalid OTP\"}";
				}else{
					output="{\"error\":\"0\"," + strData + "}";
				}
				
			} catch (Exception e) {
				output="{\"error\":\"2\",\"message\":\"Some technical error on verify otp for pay at hotel api\"}";
				logger.error(e);
				e.printStackTrace();
			}
			
			return output;
		
		}
	
	public String verifySmsOtp() {
		
		PmsGuestManager guestController = new PmsGuestManager();
		//UserLoginManager  userController = new UserLoginManager();
		String strReturn = "";	
		try {
			
			
			PmsGuest guest = guestController.findSmsId(getGuestId(),getPayAtHotelOtp());
			if(guest != null){
				strReturn = null;
			}
			else{
				strReturn = ERROR;
			}
				
			} catch (Exception e) {
				logger.error(e);
				e.printStackTrace();
			} finally {

			}
			return strReturn;
		}
	
public String cancellationRequest() {
		
		PmsGuestManager guestController = new PmsGuestManager();
		//UserLoginManager  userController = new UserLoginManager();
		String strReturn = "";	
		try {
			
			Configuration cfg = new Configuration();
			cfg.setClassForTemplateLoading(UserAddAction.class, "../../../");
			Template template = cfg.getTemplate(getText("cancelrequest.template"));
			Map<String, String> rootMap = new HashMap<String, String>();
			//rootMap.put("password", password);
			//rootMap.put("", getText("url.forgetpassword")+uniqueKey.toString());
			if(getOtherReason() == null){
				String other = getOtherReason();
				other = "None";
			}
			if(getExpectReason() == null){
				String expect = getExpectReason();
				expect = "None";
			}
			rootMap.put("bookingId", getBookingId());
			rootMap.put("otherReason", getOtherReason());
			rootMap.put("expectReason", getExpectReason());
			rootMap.put("from", getText("notification.from"));
			//rootMap.put("createdBy", getUser().getUserName());
			//rootMap.put("logoFileName", c1.getLogoPath());
			rootMap.put("url", getText("app.api.url"));
			Writer out = new StringWriter();
			template.process(rootMap, out);

			//send email
			Email em = new Email();
			em.set_to(getText("operations.notification.email"));
			em.set_from(getText("email.from"));
			em.set_cc(getText("reservation.notification.email"));
			em.set_cc2(getText("bookings.notification.email"));
				//ArrayList<String> x = new ArrayList<>(Arrays.asList("rajalakshmi.ar@ulohotels.com", "support@ulohotels.com"));
				//em.set_bcc(x);
				//em.set_cc3(getText("bookings2.notification.email"));
				//em.set_cc4(getText("support.notification.email"));
			//em.set_host(getText("email.host"));
			//em.set_password(getText("email.password"));
			//em.set_port(getText("email.port"));
			em.set_username(getText("email.username"));
			em.set_subject(getText("userinvitation.subject"));
			em.set_bodyContent(out);
			em.send();
				
			} catch (Exception e) {
				logger.error(e);
				e.printStackTrace();
			} finally {

			}
			return strReturn;
		}

	/*public String getGuestPicture() {
		HttpServletResponse response = ServletActionContext.getResponse();

		PmsGuestManager guestController = new PmsGuestManager();
		PmsGuest guest = guestController.find(getGuestId());
		String imagePath = "";
		// response.setContentType("");
		try {
			
			if (getGuestId() >0 ) {
				imagePath = getText("storage.user.logopath") + "/"
						+ getGuestId() + "/"
						+ guest.getImgPath();
				Image im = new Image();
				response.getOutputStream().write(
						im.getCustomImageInBytes(imagePath));
				response.getOutputStream().flush();
			} else {
				imagePath = getText("storage.path") + "/emptyprofilepic.png";
				Image im = new Image();
				response.getOutputStream().write(
						im.getCustomImageInBytes(imagePath));
				response.getOutputStream().flush();
			}
		} catch (Exception e1) {
			logger.error(e1);
		} finally {

		}
		return null;

	}*/

	public String dconverttime(java.util.Date input)
	{
		String disdatetime="";
		try { 
			DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		DateFormat outputFormat1 = new SimpleDateFormat("KK:mm a dd-MMM-yyyy");
		disdatetime=outputFormat1.format(input);
		}catch(Exception e)
        {
                        e.printStackTrace();
        }
		return disdatetime.replace("-", " ");
	}

	public  String  dconvert(java.util.Date dformat){
		 String disdate="";
	    try { 
	    SimpleDateFormat  dateFormat = new SimpleDateFormat("dd-MMM-YYYY");
	               
	               
	                 disdate=dateFormat.format(dformat).replace("-"," ");
	                
	                }catch(Exception e)
	                {
	                                e.printStackTrace();
	                }
	    return disdate;
	}

	public  String datediffer(java.util.Date dformat){	
		
		String disstatus="";
	    try { 
	    SimpleDateFormat  dateFormat = new SimpleDateFormat("yyyy/MM/dd");
	               
	                java.util.Date date = new java.util.Date();
	                String date1=dateFormat.format(dformat);
	                String date2=dateFormat.format(date);
	                java.util.Date d1=dateFormat.parse(date1);
	                java.util.Date d2=dateFormat.parse(date2);
	                
	                long diff=d2.getTime() - d1.getTime();
	                if(diff>0)
	                {
	                disstatus="old";
	                }
	                else
	                {
	                  disstatus="New";
	                }
	                
	                }catch(Exception e)
	                {
	                                e.printStackTrace();
	                }
	    
	    return disstatus;
	}
	
	

	public File getMyFile() {
		return myFile;
	}

	public void setMyFile(File myFile) {
		this.myFile = myFile;
	}

	public String getMyFileContentType() {
		return myFileContentType;
	}

	public void setMyFileContentType(String myFileContentType) {
		this.myFileContentType = myFileContentType;
	}

	public String getMyFileFileName() {
		return myFileFileName;
	}

	public void setMyFileFileName(String myFileFileName) {
		this.myFileFileName = myFileFileName;
	}
	
	public int getGuestId() {
		return guestId;
	}

	public void setGuestId(int guestId) {
		this.guestId = guestId;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLandline() {
		return landline;
	}

	public void setLandline(String landline) {
		this.landline = landline;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public String getBookingId() {
		return bookingId;
	}

	public void setBookingId(String bookingId) {
		this.bookingId = bookingId;
	}
	
	public String getExpectReason() {
		return expectReason;
	}

	public void setExpectReason(String expectReason) {
		this.expectReason = expectReason;
	}

	public String getOtherReason() {
		return otherReason;
	}

	public void setOtherReason(String otherReason) {
		this.otherReason = otherReason;
	}

	public String getPayAtHotelOtp() {
		return payAtHotelOtp;
	}

	public void setPayAtHotelOtp(String payAtHotelOtp) {
		this.payAtHotelOtp = payAtHotelOtp;
	}


}
