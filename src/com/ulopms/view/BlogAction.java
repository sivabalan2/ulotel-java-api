package com.ulopms.view;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;




//import com.medwecare.controller.AwsObjectManager;
//import com.medwecare.controller.CommentManager;
//import com.medwecare.controller.CommentUserManager;
////import com.medwecare.controller.NotificationManager;
//import com.medwecare.controller.ProjectManager;

//import com.medwecare.model.AwsObject;
//import com.medwecare.model.Notification;
//import com.medwecare.model.ObjectComment;
//import com.medwecare.model.ObjectCommentUser;
//import com.medwecare.model.Project;
































import org.jsoup.select.Evaluator.IsEmpty;

import com.opensymphony.xwork2.ActionSupport;

import freemarker.template.Configuration;
import freemarker.template.Template;

import javax.servlet.http.HttpServletRequest;

import com.opensymphony.xwork2.ModelDriven;
import com.ulopms.controller.AccommodationPhotoManager;
import com.ulopms.controller.BlogArticleImagesManager;
import com.ulopms.controller.BlogArticlesManager;
import com.ulopms.controller.BlogCategoriesManager;
import com.ulopms.controller.BlogLocationsManager;
import com.ulopms.controller.BlogUserCommentsManager;
import com.ulopms.controller.LocationManager;
import com.ulopms.controller.PmsPhotoCategoryManager;
import com.ulopms.controller.PmsPropertyManager;
import com.ulopms.controller.PromotionImageManager;
import com.ulopms.controller.PropertyAccommodationManager;
import com.ulopms.controller.PropertyAccommodationRoomManager;
import com.ulopms.controller.PropertyPhotoManager;
import com.ulopms.controller.SeoContentManager;
import com.ulopms.controller.UserLoginManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.AccommodationPhoto;
import com.ulopms.model.AccommodationRoom;
import com.ulopms.model.BlogArticleImages;
import com.ulopms.model.BlogArticles;
import com.ulopms.model.BlogCategories;
import com.ulopms.model.BlogLocations;
import com.ulopms.model.BlogUserComments;
import com.ulopms.model.Location;
import com.ulopms.model.PmsPhotoCategory;
import com.ulopms.model.PmsProperty;
import com.ulopms.model.PromotionImages;
import com.ulopms.model.PropertyAccommodation;
import com.ulopms.model.PropertyDiscount;
import com.ulopms.model.PropertyPhoto;
import com.ulopms.model.SeoContent;
import com.ulopms.model.User;
import com.ulopms.util.Email;
import com.ulopms.util.Image;

public class BlogAction extends ActionSupport implements
ServletRequestAware,SessionAware, UserAware{

	private static final long serialVersionUID = 914982665758390091L;
	
	private String blogCategoryName;
	private String blogCategoryContent;	
	private String blogLocationName;
	private String blogLocationContent;	
	private String blogArticleTitle;
	private String blogArticleDescription;	
	private Integer locationId;
	private Integer blogArticleImageId;	
	private Integer limit;
	private Integer offset;	
	private String blogLocationUrl;
	private String blogCategoryUrl;
	private String blogArticleUrl;
	

	private String blogUserName;
	private String blogUserEmail;	
	private String blogUserUrl;	
	private String blogUserComment;	
	private Integer blogUserCommentId;

	

	private Integer propertyId;
	private Integer blogArticleId;
	private Integer blogLocationId;
	private Integer blogCategoryId;
	private Boolean isPrimary;
	private Integer propertyPhotoId;
	private File myFile;
	private String myFileContentType;
	private String myFileFileName;
	private Integer photoCategoryId;
	private Integer promotionImageId;
	
	

	
	private List<BlogCategories> blogCategoryList;
	
	private List<BlogLocations> blogLocationList;
	
	private List<BlogArticles> blogArticleList;
	
	private List<BlogArticleImages> blogArticleImageList;
	
	private List<BlogUserComments> blogUserCommentList;
	

	private static final Logger logger = Logger.getLogger(BlogAction.class);
    
	private HttpSession session;

	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}
	
	private SessionMap<String,Object> sessionMap;  
	  
	//getters and setters  
	  
	@Override  
	public void setSession(Map<String, Object> map) {  
	    sessionMap=(SessionMap)map;  
	}  
	
	
	private User user;

	
	// @Override
	// public User getModel() {
	// return user;
	// }

	@Override
	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	
	private HttpServletRequest request;

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	public BlogAction() {

	}

	public String execute() {

		//this.patientId = getPatientId();
		return SUCCESS;
	} 
	
	public String addBlogCategory() {
		BlogCategoriesManager BlogCategoriesController =  new BlogCategoriesManager();
		try {
			
			BlogCategories blogCategory = new BlogCategories();
			long time = System.currentTimeMillis();
			java.util.Date date = new java.util.Date(time);
			Timestamp tsDate=new Timestamp(date.getTime());  
			
			String categoryContent = getBlogCategoryContent().replace("percent", "%").replaceAll("\\sand"," &");
			
			blogCategory.setBlogCategoryName(getBlogCategoryName());
			blogCategory.setBlogCategoryContent(categoryContent);
			blogCategory.setBlogCategoryUrl(getBlogCategoryUrl());
			blogCategory.setIsActive(true);
			blogCategory.setIsDeleted(false);						
			blogCategory.setCreatedDate(tsDate);
			BlogCategories blogCategories = BlogCategoriesController.add(blogCategory);
			
			this.blogCategoryId = blogCategories.getBlogCategoryId();
			
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("application/json");
			
			  if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"blogCategoryId\":\"" + this.blogCategoryId  + "\"";
				
				jsonOutput += "}";


			

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}	
	
	
	public String addBlogCategoryimage() throws IOException {
		
		try {
			BlogCategoriesManager BlogCategoriesController =  new BlogCategoriesManager();
			//Location locations=new Location();
			File fileZip1 = new File(getText("storage.blogcategory.photo") + "/"
					+ getBlogCategoryId() + "/", this.getMyFileFileName());
			FileUtils.copyFile(this.myFile, fileZip1);// copying image in the
			BlogCategories blogCategory = BlogCategoriesController.find(getBlogCategoryId());
			blogCategory.setBlogCategoryImage(this.getMyFileFileName());
			BlogCategoriesController.edit(blogCategory);
			/*BufferedImage image = ImageIO.read(this.myFile);
			int height = image.getHeight();
			int width = image.getWidth();
			if(height == 221 && width == 214){
				if(this.getMyFileFileName()!=null && !this.getMyFileFileName().equalsIgnoreCase(""))
				{
					File fileZip1 = new File(getText("storage.propertythumb.photo") + "/"
							+ getLocationPropertyId() + "/", this.getMyFileFileName());
					FileUtils.copyFile(this.myFile, fileZip1);// copying image in the
					PmsProperty property = propertyController.find(getLocationPropertyId()); 
					property.setPropertyThumbPath(this.getMyFileFileName());
					propertyController.edit(property);
				}
			}*/
			
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return SUCCESS;
	}
	
	public String getBlogCategories() throws IOException {
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			BlogCategoriesManager BlogCategoriesController =  new BlogCategoriesManager();
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");
			int serialno=1;
			this.blogCategoryList = BlogCategoriesController.list(limit,offset);
			for (BlogCategories blogCategory : blogCategoryList) {

				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"blogCategoryId\":\"" + blogCategory.getBlogCategoryId()+ "\"";
				jsonOutput += ",\"serialNo\":\"" + serialno+ "\"";
				jsonOutput += ",\"blogCategoryName\":\"" + (blogCategory.getBlogCategoryName() == null ? "": blogCategory.getBlogCategoryName().trim())+ "\"";

				jsonOutput += "}";

				serialno++;
			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}	
	
	
	public String getAllBlogCategories() throws IOException {
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			BlogCategoriesManager BlogCategoriesController =  new BlogCategoriesManager();
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");
			this.blogCategoryList = BlogCategoriesController.list();
			for (BlogCategories blogCategory : blogCategoryList) {

				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"blogCategoryId\":\"" + blogCategory.getBlogCategoryId()+ "\"";
				jsonOutput += ",\"blogCategoryName\":\"" + (blogCategory.getBlogCategoryName() == null ? "": blogCategory.getBlogCategoryName().trim())+ "\"";
				jsonOutput += ",\"blogCategoryContent\":\"" + (blogCategory.getBlogCategoryContent() == null ? "": blogCategory.getBlogCategoryContent().trim())+ "\"";
				jsonOutput += ",\"blogCategoryUrl\":\"" + (blogCategory.getBlogCategoryUrl())+ "\"";
				jsonOutput += ",\"blogNamespace\":\"" + "blog"+ "\"";
				jsonOutput += "}";

			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	
	public String getBlogCategory() throws IOException {
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		   
			
			BlogCategoriesManager BlogCategoriesController =  new BlogCategoriesManager();

			response.setContentType("application/json");
                                        
			BlogCategories blogCategory = BlogCategoriesController.find(getBlogCategoryId());
			//AccommodationRoom accommodationRooms = accommodationRoomController.findCount(this.propertyId,getPropertyAccommodationId());
			//accommodationRooms.getRoomCount();
			
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"blogCategoryId\":\"" + blogCategory.getBlogCategoryId() + "\"";
				jsonOutput += ",\"blogCategoryName\":\"" + blogCategory.getBlogCategoryName()+ "\"";
				jsonOutput += ",\"blogCategoryContent\":\"" + blogCategory.getBlogCategoryContent()+ "\"";
				jsonOutput += ",\"blogCategoryUrl\":\"" + blogCategory.getBlogCategoryUrl()+ "\"";
				jsonOutput += ",\"createdDate\":\"" + blogCategory.getCreatedDate()+ "\"";				
				jsonOutput += "}";

			

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	public String getBlogCategoryImage() {
		
		HttpServletResponse response = ServletActionContext.getResponse();
		BlogCategoriesManager BlogCategoriesController =  new BlogCategoriesManager();

		String imagePath = "";
		// response.setContentType("");
		try {
			BlogCategories blogCategory = BlogCategoriesController.find(getBlogCategoryId()); 
			if ( blogCategory.getBlogCategoryImage() != null) {
				imagePath = getText("storage.blogcategory.photo") + "/"
						+blogCategory.getBlogCategoryId() + "/"
						+ blogCategory.getBlogCategoryImage();
				Image im = new Image();
				response.getOutputStream().write(im.getCustomImageInBytes(imagePath));
				response.getOutputStream().flush();
			} 
			/*else {
				imagePath = getText("storage.accomodation.photo") + "/emptyprofilepic.png";
				Image im = new Image();
				response.getOutputStream().write(
						im.getCustomImageInBytes(imagePath));
				response.getOutputStream().flush();
			}*/
		}catch (Exception e1) {
			logger.error(e1);
		} finally {

		}
		return null;

	}
	
	public String getBlogCategoryCount() throws IOException {
 		try {
 			String jsonOutput = "";
 			HttpServletResponse response = ServletActionContext.getResponse();
 			response.setContentType("application/json");
 			BlogCategoriesManager BlogCategoriesController =  new BlogCategoriesManager();
 			this.blogCategoryList = BlogCategoriesController.list();
 			if(blogCategoryList.size()>0 && !blogCategoryList.isEmpty()){
 				if (!jsonOutput.equalsIgnoreCase(""))
 					jsonOutput += ",{";
 				else
 					jsonOutput += "{";
 				
 			    jsonOutput += "\"count\":\"" + blogCategoryList.size() + "\"";
 			    
 				jsonOutput += "}";
 			}
 			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
 		} catch (Exception e) {
 			logger.error(e);
 			e.printStackTrace();
 		} finally {

 		}

 		return null;

 	}
	
	public String editBlogCategory() {
		
	BlogCategoriesManager BlogCategoriesController =  new BlogCategoriesManager();
		try {
			
						
			BlogCategories blogCategory = BlogCategoriesController.find(getBlogCategoryId());
			
			String categoryContent = getBlogCategoryContent().replace("percent", "%").replaceAll("\\sand"," &");
			/*long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time); */
			
			blogCategory.setBlogCategoryName(getBlogCategoryName());
			blogCategory.setBlogCategoryContent(categoryContent);
			blogCategory.setBlogCategoryUrl(getBlogCategoryUrl());
			blogCategory.setIsActive(true);
			blogCategory.setIsDeleted(false);						
			//BlogCategories blogCategories = BlogCategoriesController.add(blogCategory);
			
			BlogCategoriesController.edit(blogCategory);
			
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return SUCCESS;
	}
	
	public String editBlogCategoryImage() throws IOException {
		try {

			BlogCategoriesManager BlogCategoriesController =  new BlogCategoriesManager();

			//User u = new User();
			// FileInputStream fis;
			// fis = new FileInputStream(myFile);
			// @SuppressWarnings("deprecation")
			// Blob blob = Hibernate.createBlob(fis);
			// user.setProfilePicture(blob);
			// linkController.edit(getModel());
			// this.user = getModel();
			File fileZip1 = new File(getText("storage.blogcategory.photo") + "/"
					+ getBlogCategoryId() + "/", this.getMyFileFileName());
			FileUtils.copyFile(this.myFile, fileZip1);// copying image in the
			// new file
			BlogCategories blogCategory = BlogCategoriesController.find(getBlogCategoryId()); 
			
			
			blogCategory.setBlogCategoryImage(this.getMyFileFileName());
			//u.setProfilePicName(this.getMyFileFileName());
			BlogCategoriesController.edit(blogCategory);
			//this.propertyaccommodation = getModel();
		} catch (Exception e) {
			logger.error(e);
		} finally {

		}
		return SUCCESS;
	}
	
	public String deleteBlogCategory() {
		BlogCategoriesManager BlogCategoriesController =  new BlogCategoriesManager();
		
		try {
			
						
			BlogCategories blogCategory = BlogCategoriesController.find(getBlogCategoryId());
						
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			blogCategory.setIsActive(false);
			blogCategory.setIsDeleted(true);
			blogCategory.setModifiedDate(new java.sql.Timestamp(date.getTime()));
			BlogCategoriesController.edit(blogCategory);
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null; 
	}
	
	public String addBlogLocation() {
		BlogLocationsManager BlogLocationsController =  new BlogLocationsManager();
		try {
			
			BlogLocations blogLocation = new BlogLocations();
			long time = System.currentTimeMillis();
			java.util.Date date = new java.util.Date(time);
			Timestamp tsDate=new Timestamp(date.getTime());  
			String locationContent = getBlogLocationContent().replace("percent", "%").replaceAll("\\sand"," &");
			
			blogLocation.setBlogLocationName(getBlogLocationName());
			blogLocation.setBlogLocationContent(locationContent);
			blogLocation.setBlogLocationUrl(getBlogLocationUrl());
			blogLocation.setIsActive(true);
			blogLocation.setIsDeleted(false);						
			blogLocation.setCreatedDate(tsDate);
			BlogLocations blogLocations = BlogLocationsController.add(blogLocation);
			
			this.blogLocationId = blogLocations.getBlogLocationId();
			
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("application/json");
			
			  if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"blogLocationId\":\"" + this.blogLocationId  + "\"";
				
				jsonOutput += "}";


			

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}
	
	public String addBlogLocationimage() throws IOException {
		
		try {
			BlogLocationsManager BlogLocationsController =  new BlogLocationsManager();
			//Location locations=new Location();
			File fileZip1 = new File(getText("storage.bloglocation.photo") + "/"
					+ getBlogLocationId() + "/", this.getMyFileFileName());
			FileUtils.copyFile(this.myFile, fileZip1);// copying image in the
			BlogLocations blogLocations = BlogLocationsController.find(getBlogLocationId());
			blogLocations.setBlogLocationImage(this.getMyFileFileName());
			BlogLocationsController.edit(blogLocations);
			/*BufferedImage image = ImageIO.read(this.myFile);
			int height = image.getHeight();
			int width = image.getWidth();
			if(height == 221 && width == 214){
				if(this.getMyFileFileName()!=null && !this.getMyFileFileName().equalsIgnoreCase(""))
				{
					File fileZip1 = new File(getText("storage.propertythumb.photo") + "/"
							+ getLocationPropertyId() + "/", this.getMyFileFileName());
					FileUtils.copyFile(this.myFile, fileZip1);// copying image in the
					PmsProperty property = propertyController.find(getLocationPropertyId()); 
					property.setPropertyThumbPath(this.getMyFileFileName());
					propertyController.edit(property);
				}
			}*/
			
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return SUCCESS;
	}
	
	public String getBlogLocationCount() throws IOException {
 		try {
 			String jsonOutput = "";
 			HttpServletResponse response = ServletActionContext.getResponse();
 			response.setContentType("application/json");
 			BlogLocationsManager BlogLocationsController =  new BlogLocationsManager();
 			this.blogLocationList = BlogLocationsController.list();
 			if(blogLocationList.size()>0 && !blogLocationList.isEmpty()){
 				if (!jsonOutput.equalsIgnoreCase(""))
 					jsonOutput += ",{";
 				else
 					jsonOutput += "{";
 				
 			    jsonOutput += "\"count\":\"" + blogLocationList.size() + "\"";
 			    
 				jsonOutput += "}";
 			}
 			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
 		} catch (Exception e) {
 			logger.error(e);
 			e.printStackTrace();
 		} finally {

 		}

 		return null;

 	}
	
	public String getAllBlogLocations() throws IOException {
		try {

			String jsonOutput = "";
			String imagePath = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			BlogLocationsManager BlogLocationsController =  new BlogLocationsManager();
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");
			this.blogLocationList = BlogLocationsController.list();
			for (BlogLocations blogLocation : blogLocationList) {

				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"blogLocationId\":\"" + blogLocation.getBlogLocationId()+ "\"";
			
				jsonOutput += ",\"blogLocationName\":\"" + (blogLocation.getBlogLocationName() == null ? "": blogLocation.getBlogLocationName().trim())+ "\"";
				jsonOutput += ",\"blogLocationUrl\":\"" + (blogLocation.getBlogLocationUrl())+ "\"";
				jsonOutput += ",\"blogNamespace\":\"" + "blog"+ "\"";
				jsonOutput += "}";

				
			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	public String getBlogLocations() throws IOException {
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			BlogLocationsManager BlogLocationsController =  new BlogLocationsManager();
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");
			int serialno=1;
			this.blogLocationList = BlogLocationsController.list(limit,offset);
			for (BlogLocations blogLocation : blogLocationList) {

				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"blogLocationId\":\"" + blogLocation.getBlogLocationId()+ "\"";
				jsonOutput += ",\"serialNo\":\"" + serialno+ "\"";
				jsonOutput += ",\"blogLocationName\":\"" + (blogLocation.getBlogLocationName() == null ? "": blogLocation.getBlogLocationName().trim())+ "\"";

				jsonOutput += "}";

				serialno++;
			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	public String getBlogLocation() throws IOException {
		
		
		
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		   
			
			BlogLocationsManager BlogLocationsController =  new BlogLocationsManager();

			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");
                                        
			BlogLocations blogLocation = BlogLocationsController.find(getBlogLocationId());
			//AccommodationRoom accommodationRooms = accommodationRoomController.findCount(this.propertyId,getPropertyAccommodationId());
			//accommodationRooms.getRoomCount();
			
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"blogLocationId\":\"" + blogLocation.getBlogLocationId() + "\"";
				jsonOutput += ",\"blogLocationName\":\"" + blogLocation.getBlogLocationName()+ "\"";
				jsonOutput += ",\"blogLocationContent\":\"" + blogLocation.getBlogLocationContent()+ "\"";
				jsonOutput += ",\"blogLocationUrl\":\"" + blogLocation.getBlogLocationUrl()+ "\"";
				jsonOutput += ",\"createdDate\":\"" + blogLocation.getCreatedDate()+ "\"";				
				jsonOutput += "}";

			

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	
	public String getBlogLocationImage() {
		HttpServletResponse response = ServletActionContext.getResponse();
		BlogLocationsManager BlogLocationsController =  new BlogLocationsManager();

		String imagePath = "";
		// response.setContentType("");
		try {
			BlogLocations blogLocation = BlogLocationsController.find(getBlogLocationId()); 
			if ( blogLocation.getBlogLocationImage() != null) {
				imagePath = getText("storage.bloglocation.photo") + "/"
						+blogLocation.getBlogLocationId() + "/"
						+ blogLocation.getBlogLocationImage();
				Image im = new Image();
				response.getOutputStream().write(im.getCustomImageInBytes(imagePath));
				response.getOutputStream().flush();
			} 
			/*else {
				imagePath = getText("storage.accomodation.photo") + "/emptyprofilepic.png";
				Image im = new Image();
				response.getOutputStream().write(
						im.getCustomImageInBytes(imagePath));
				response.getOutputStream().flush();
			}*/
		}catch (Exception e1) {
			logger.error(e1);
			e1.printStackTrace();
		} finally {

		}
		return null;

	}
	
	public String editBlogLocation() {
		
		BlogLocationsManager BlogLocationsController =  new BlogLocationsManager();
			try {
				
							
				BlogLocations blogLocation = BlogLocationsController.find(getBlogLocationId());
				
				String editLocationContent = getBlogLocationContent().replace("percent", "%").replaceAll("\\sand"," &");
				/*long time = System.currentTimeMillis();
				java.sql.Date date = new java.sql.Date(time); */
				
				blogLocation.setBlogLocationName(getBlogLocationName());
				blogLocation.setBlogLocationContent(editLocationContent);
				blogLocation.setBlogLocationUrl(getBlogLocationUrl());
				blogLocation.setIsActive(true);
				blogLocation.setIsDeleted(false);						
				//BlogCategories blogCategories = BlogCategoriesController.add(blogCategory);
				
				BlogLocationsController.edit(blogLocation);
				
				
			} catch (Exception e) {
				logger.error(e);
				e.printStackTrace();
			} finally {

			}
			return SUCCESS;
		}
	

	public String editBlogLocationImage() throws IOException {
	try {

		BlogLocationsManager BlogLocationsController =  new BlogLocationsManager();

		//User u = new User();
		// FileInputStream fis;
		// fis = new FileInputStream(myFile);
		// @SuppressWarnings("deprecation")
		// Blob blob = Hibernate.createBlob(fis);
		// user.setProfilePicture(blob);
		// linkController.edit(getModel());
		// this.user = getModel();
		File fileZip1 = new File(getText("storage.bloglocation.photo") + "/"
				+ getBlogLocationId() + "/", this.getMyFileFileName());
		FileUtils.copyFile(this.myFile, fileZip1);// copying image in the
		// new file
		BlogLocations blogLocation = BlogLocationsController.find(getBlogLocationId()); 
		
		
		blogLocation.setBlogLocationImage(this.getMyFileFileName());
		//u.setProfilePicName(this.getMyFileFileName());
		BlogLocationsController.edit(blogLocation);
		//this.propertyaccommodation = getModel();
	} catch (Exception e) {
		logger.error(e);
	} finally {

	}
	return SUCCESS;
}
	
	public String deleteBlogLocation() {
		BlogLocationsManager BlogLocationsController =  new BlogLocationsManager();
		
		try {
			
						
			BlogLocations blogLocation = BlogLocationsController.find(getBlogLocationId());
						
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			blogLocation.setIsActive(false);
			blogLocation.setIsDeleted(true);
			blogLocation.setModifiedDate(new java.sql.Timestamp(date.getTime()));
			BlogLocationsController.edit(blogLocation);
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null; 
	}
	
	public String addBlogArticleTitle() {
		BlogArticlesManager BlogArticleController =  new BlogArticlesManager();
		BlogLocationsManager blogLocationController = new BlogLocationsManager();
		BlogCategoriesManager blogCategoryController = new BlogCategoriesManager();
		try {
			
			String articleTitleContent  = getBlogArticleTitle().replace("percent", "%").replaceAll("\\sand"," &");
			BlogArticles blogArticle = new BlogArticles();
			long time = System.currentTimeMillis();
			java.util.Date date = new java.util.Date(time);
			Timestamp tsDate=new Timestamp(date.getTime());  
			
			blogArticle.setBlogArticleTitle(articleTitleContent);
			blogArticle.setIsActive(true);
			blogArticle.setIsDeleted(false);	
			blogArticle.setBlogArticleUrl(getBlogArticleUrl());
			BlogCategories blogcategory = blogCategoryController.find(getBlogCategoryId());
			blogArticle.setBlogCategories(blogcategory);
			blogArticle.setCreatedDate(tsDate);
			if(getBlogLocationId() == null){
				
				blogArticle.setBlogLocations(null);
			}
			else{
			BlogLocations bloglocation = blogLocationController.find(getBlogLocationId());
			blogArticle.setBlogLocations(bloglocation);
			}
			BlogArticles blogArticles = BlogArticleController.add(blogArticle);
			
			this.blogArticleId = blogArticles.getBlogArticleId();
			
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("application/json");
			
			  if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"blogArticleId\":\"" + this.blogArticleId  + "\"";
				
				jsonOutput += "}";

			

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;  
	}	
	
	public String addBlogArticleTitleImage() throws IOException {
		try {

			BlogArticlesManager BlogArticleController =  new BlogArticlesManager();

			//User u = new User();
			// FileInputStream fis;
			// fis = new FileInputStream(myFile);
			// @SuppressWarnings("deprecation")
			// Blob blob = Hibernate.createBlob(fis);
			// user.setProfilePicture(blob);
			// linkController.edit(getModel());
			// this.user = getModel();
			File fileZip1 = new File(getText("storage.blogarticletitle.photo") + "/"
					+ getBlogArticleId() + "/", this.getMyFileFileName());
			FileUtils.copyFile(this.myFile, fileZip1);// copying image in the
			// new file
			BlogArticles blogArticle = BlogArticleController.find(getBlogArticleId()); 
			
			
			blogArticle.setBlogArticleTitleImage(this.getMyFileFileName());
			//u.setProfilePicName(this.getMyFileFileName());
			 BlogArticleController.edit(blogArticle);
			//this.propertyaccommodation = getModel();
		} catch (Exception e) {
			logger.error(e);
		} finally {

		}
		return SUCCESS;
	}
	
	
	public String editBlogArticleTitle() {
		BlogArticlesManager BlogArticleController =  new BlogArticlesManager();
		BlogLocationsManager blogLocationController = new BlogLocationsManager();
		BlogCategoriesManager blogCategoryController = new BlogCategoriesManager();
		try {
			BlogArticles blogArticle = BlogArticleController.find(getBlogArticleId());
			String editArticleTitleContent = getBlogArticleTitle().replace("percent", "%").replaceAll("\\sand"," &");
			long time = System.currentTimeMillis();
			java.util.Date date = new java.util.Date(time);
			Timestamp tsDate=new Timestamp(date.getTime());  
			
			blogArticle.setBlogArticleTitle(editArticleTitleContent);
			blogArticle.setBlogArticleUrl(getBlogArticleUrl());			
			BlogCategories blogcategory = blogCategoryController.find(getBlogCategoryId());
			blogArticle.setBlogCategories(blogcategory);
			//blogArticle.setModifiedDate(tsDate);
			if(getBlogLocationId() == null){
				blogArticle.setBlogLocations(null);
			}
			else{
				BlogLocations bloglocation = blogLocationController.find(getBlogLocationId());
				blogArticle.setBlogLocations(bloglocation);
			}
			 BlogArticleController.add(blogArticle);		
		
			
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}	
	
	
	public String addBlogArticleDescription() { 
		BlogArticlesManager BlogArticleController =  new BlogArticlesManager();
		
		try {
			
			BlogArticles blogArticle = BlogArticleController.find(getBlogArticleId());		
			String articleDescription = getBlogArticleDescription().replace("percent", "%").replaceAll("\\sand"," &");
			
			blogArticle.setBlogArticleDescription(articleDescription);			
			BlogArticleController.add(blogArticle);
			
			
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}	
	
	public String editBlogArticleDescription() { 
		BlogArticlesManager BlogArticleController =  new BlogArticlesManager();
		
		try {
			BlogArticles blogArticle = BlogArticleController.find(getBlogArticleId());		
			String editArticleDescription = getBlogArticleDescription().replace("percent", "%").replaceAll("\\sand"," &");
			blogArticle.setBlogArticleDescription(editArticleDescription);			
			BlogArticleController.edit(blogArticle);
			
			
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}	
	
	
	public String addBlogArticleImage() {
		BlogArticleImagesManager BlogArticleImageController =  new BlogArticleImagesManager();
		BlogArticlesManager BlogArticleController =  new BlogArticlesManager();
		try {
			
						
			
			
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);

			if(this.getMyFileFileName()!=null && !this.getMyFileFileName().equalsIgnoreCase(""))
			{
				BlogArticleImages blogArticleimage = new BlogArticleImages() ;	
				blogArticleimage.setBlogArticleImage(this.getMyFileFileName());
				blogArticleimage.setIsActive(true);
				blogArticleimage.setIsDeleted(false);
				BlogArticles blogArticle = BlogArticleController.find(getBlogArticleId());	
				blogArticleimage.setBlogArticles(blogArticle);
				//String path = "D:/final backup/ulolivejun4/WebContent/blog/uloblog/images/blogarticle-photo/";
				
				String path = "/opt/bitnami/apache-tomcat/webapps/ROOT/blog/uloblog/images/blogarticle-photo/";
			
				BlogArticleImageController.add(blogArticleimage);
			
				if(getBlogArticleId()>0 && this.getMyFileFileName()!=null && !this.getMyFileFileName().equalsIgnoreCase(""))
				{
					File file = new File( path  + getBlogArticleId() + "/", this.getMyFileFileName());
					FileUtils.copyFile(this.myFile, file);// copying image in the
				}
			}
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}
	
	public String getBlogArticleImages() throws IOException {
			try {
	 
				String jsonOutput = "";
				HttpServletResponse response = ServletActionContext.getResponse();
			
				
				BlogArticleImagesManager BlogArticleImageController =  new BlogArticleImagesManager();
				
				response.setContentType("application/json");
				String path = "/blog/uloblog/images/blogarticle-photo/";
				//String path = "D:/final backup/ulolivejun4/WebContent/blog/uloblog/images/blogarticle-photo";
				//String path = "/blog/uloblog/images/blogarticle-photo/";
				String imagePath = "";
				this.blogArticleImageList = BlogArticleImageController.list(getBlogArticleId());
				for (BlogArticleImages image : blogArticleImageList) {

					if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += ",{";
					else
						jsonOutput += "{";
					jsonOutput += "\"blogArticleImageId\":\"" + image.getBlogArticleImageId() + "\"";
					jsonOutput += ",\"blogArticlImage\":\"" + image.getBlogArticleImage()+ "\"";
					
						   imagePath = getText("storage.blogarticle.photo") + "/"						
								   		+ getBlogArticleId() + "/"
								   		+ image.getBlogArticleImage();
					jsonOutput += ",\"imagePath\":\"" + path + getBlogArticleId()+ "/" + image.getBlogArticleImage() + "\"";
					jsonOutput += "}";


				}
				

				response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
				

			} catch (Exception e) {
				logger.error(e);
				e.printStackTrace();
			} finally {

			}

			return null;

		}
	
	public String getBlogArticleTitleImage() {
		
		HttpServletResponse response = ServletActionContext.getResponse();
		BlogArticlesManager BlogArticleController =  new BlogArticlesManager();

		String imagePath = "";
		// response.setContentType("");
		try {
			BlogArticles blogArticle = BlogArticleController.find(getBlogArticleId()); 
			
				imagePath = getText("storage.blogarticletitle.photo") + "/"
						+blogArticle.getBlogArticleId() + "/"
						+ blogArticle.getBlogArticleTitleImage();
				Image im = new Image();
				
				response.getOutputStream().write(im.getCustomImageInBytes(imagePath));
				response.getOutputStream().flush();
			
		}catch (Exception e1) {
			logger.error(e1);
		} finally {

		}
		return null;

	}
	
public String getBlogArticleImage() {
		
		
		HttpServletResponse response = ServletActionContext.getResponse();

		BlogArticleImagesManager BlogArticleImageController =  new BlogArticleImagesManager();
		BlogArticlesManager BlogArticleController =  new BlogArticlesManager();
		BlogArticleImages articleimage = BlogArticleImageController.find(getBlogArticleImageId());
		String imagePath = "";
		String path = "/opt/bitnami/apache-tomcat/webapps/ROOT/blog/uloblog/images/blogarticle-photo/";
		//String path = "D:/final backup/ulolivejun4/WebContent/blog/uloblog/images/blogarticle-photo";
		// response.setContentType("");
		try {
			
			if (getBlogArticleImageId() >0 ) {
				imagePath = path + "/"
						
						+ articleimage.getBlogArticles().getBlogArticleId() + "/"
						+ articleimage.getBlogArticleImage();
				Image im = new Image();
				response.getOutputStream().write(
						im.getCustomImageInBytes(imagePath));
				response.getOutputStream().flush();
			} else {
				imagePath = getText("storage.path") + "/emptyprofilepic.png";
				Image im = new Image();
				response.getOutputStream().write(
						im.getCustomImageInBytes(imagePath));
				response.getOutputStream().flush();
			}
		} catch (Exception e1) {
			logger.error(e1);
		} finally {

		}
		return null;

	}
	
	
	public String getBlogArticles() throws IOException {
		
		
		
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		   
			
			BlogArticlesManager BlogArticleController =  new BlogArticlesManager();

			response.setContentType("application/json");
                                        
			
			this.blogArticleList = BlogArticleController.list();
			for (BlogArticles blogArticle : blogArticleList) {

				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"blogArticleId\":\"" + blogArticle.getBlogArticleId() + "\"";
				jsonOutput += ",\"blogArticleTitle\":\"" + blogArticle.getBlogArticleTitle()+ "\""; /*toUpperCase().trim()*/
				
				String title =  (blogArticle.getBlogArticleDescription() == null ? "": blogArticle.getBlogArticleDescription());
				String strTitle = title.replace("\"", "\\\"" );
				String strTrim = strTitle.replaceAll("\\<.*?\\>"," ");
				String strTrimTitle = strTrim.replaceAll("\\s+"," ");
				
				jsonOutput += ",\"blogArticleDescription\":\"" + strTrimTitle + "\"";
				String createDate = new SimpleDateFormat("MMMM dd,yyyy").format(blogArticle.getCreatedDate());
				jsonOutput += ",\"createdDate\":\"" + createDate + "\"";	
				request.getAttribute("blogArticleDescription"+blogArticle.getBlogArticleTitle());
				/*request.setAttribute("description"+serialno ,blogArticle.getBlogArticleDescription());
				*/
				/*jsonOutput += ",\"blogLocationContent\":\"" + blogArticle.getb()+ "\"";
				jsonOutput += ",\"createdDate\":\"" + blogArticle.getCreatedDate()+ "\"";	*/			
				jsonOutput += "}";

			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}	
	
public String getBlogUloArticles() throws IOException {
		
		
		
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		   
			
			BlogArticlesManager BlogArticleController =  new BlogArticlesManager();

			response.setContentType("application/json");
                                        
			
			this.blogArticleList = BlogArticleController.list(limit,offset);
			for (BlogArticles blogArticle : blogArticleList) {

				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				
				this.blogArticleList = BlogArticleController.list();
				jsonOutput += "\"count\":\"" + this.blogArticleList.size() + "\"";
				jsonOutput += ",\"blogArticleId\":\"" + blogArticle.getBlogArticleId() + "\"";
				
//				jsonOutput += "\"blogArticleId\":\"" + blogArticle.getBlogArticleId() + "\"";
				jsonOutput += ",\"blogArticleTitle\":\"" + blogArticle.getBlogArticleTitle()+ "\""; /*toUpperCase().trim()*/
				jsonOutput += ",\"blogArticleUrl\":\"" + blogArticle.getBlogArticleUrl()+ "\""; /*toUpperCase().trim()*/
				String title =  (blogArticle.getBlogArticleDescription() == null ? "": blogArticle.getBlogArticleDescription());
				String strTitle = title.replace("\"", "\\\"" );
				String strTrim = strTitle.replaceAll("\\<.*?\\>"," ");
				String strTrimTitle = strTrim.replaceAll("\\s+"," ");
				jsonOutput += ",\"blogNamespace\":\"" + "blog"+ "\"";
				jsonOutput += ",\"blogArticleDescription\":\"" + strTrimTitle + "\"";
				String createDate = new SimpleDateFormat("MMMM dd,yyyy").format(blogArticle.getCreatedDate());
				jsonOutput += ",\"createdDate\":\"" + createDate + "\"";	
				request.getAttribute("blogArticleDescription"+blogArticle.getBlogArticleTitle());
				/*request.setAttribute("description"+serialno ,blogArticle.getBlogArticleDescription());
				*/
				/*jsonOutput += ",\"blogLocationContent\":\"" + blogArticle.getb()+ "\"";
				jsonOutput += ",\"createdDate\":\"" + blogArticle.getCreatedDate()+ "\"";	*/			
				jsonOutput += "}";

			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}	

	
	public String deleteBlogArticle() {
		BlogArticlesManager BlogArticleController =  new BlogArticlesManager();
		
		try {
			
						
			BlogArticles blogArticle = BlogArticleController.find(getBlogArticleId());
						
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			blogArticle.setIsActive(false);
			blogArticle.setIsDeleted(true);
			blogArticle.setModifiedDate(new java.sql.Timestamp(date.getTime()));
			BlogArticleController.edit(blogArticle);
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null; 
	}
	
	
	public String getBlogArticle() throws IOException {
		
		
		
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		   
			
			BlogArticlesManager BlogArticleController =  new BlogArticlesManager();
			BlogLocationsManager blogLocationController = new BlogLocationsManager();
			BlogCategoriesManager blogCategoryController = new BlogCategoriesManager();
			BlogArticleImagesManager BlogArticleImageController =  new BlogArticleImagesManager();

			response.setContentType("application/json");
			
                                        
			
		/*	this.blogArticleList = BlogArticleController.list();
			for (BlogArticles blogArticle : blogArticleList) {*/
			//BlogArticles blogArticle = BlogArticleController.find(getBlogArticleId());
			AccommodationRoom blogArticle = BlogArticleController.findArticleId(getBlogArticleId());	
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"blogArticleId\":\"" + blogArticle.getBlogArticleId() + "\"";
				jsonOutput += ",\"blogArticleTitle\":\"" + blogArticle.getBlogArticleTitle()+ "\"";
				jsonOutput += ",\"blogArticleUrl\":\"" + blogArticle.getBlogArticleUrl()+ "\"";
				//BlogLocations bloglocation = blogLocationController.find(blogArticle.getBlogLocations().getBlogLocationId());
				/*Integer frontDiscountId = null;
				frontDiscountId = blogArticle.getBlogLocationId();
				if(frontDiscountId == null ){
				jsonOutput += ",\"blogLocationId\":\"" + ( blogArticle.getBlogLocationId()== null ? "": blogArticle.getBlogLocationId() )+ "\"";
				}*/
				jsonOutput += ",\"blogLocationId\":\"" + ( blogArticle.getBlogLocationId()== null ? "": blogArticle.getBlogLocationId() )+ "\"";
				jsonOutput += ",\"blogCategoryId\":\"" + (blogArticle.getBlogCategoryId() == null ? "": blogArticle.getBlogCategoryId() )+ "\"";
				
				//BlogCategories blogcategory = blogCategoryController.find(getBlogCategoryId());
				String title =  (blogArticle.getBlogArticleDescription() == null ? "":  blogArticle.getBlogArticleDescription().trim());
				String strTitle = title.replace("\"", "\\\"" );
				String strTrimTitle = strTitle.replaceAll("\\s+"," ");
				
				jsonOutput += ",\"blogArticleDescription\":\"" + strTrimTitle + "\"";
				
				
				
				
				
				
				
				/*this.blogArticleImageList = BlogArticleImageController.list(getBlogArticleId());
				String imagePath = "";
				String jsonBlogImages ="";
				for (BlogArticleImages image : blogArticleImageList) {
					
					if (!jsonBlogImages.equalsIgnoreCase(""))
						
						jsonBlogImages += ",{";
					else
						jsonBlogImages += "{";


					
					jsonBlogImages += "\"blogArticleImageId\":\"" + image.getBlogArticleImageId() + "\"";
					jsonBlogImages += ",\"blogArticlImage\":\"" + image.getBlogArticleImage()+ "\"";
					
						   imagePath = getText("storage.blogarticle.photo") + "/"						
								   		+ getBlogArticleId() + "/"
								   		+ image.getBlogArticleImage();
						   jsonBlogImages += ",\"imagePath\":\"" + imagePath + "\"";
					
						   jsonBlogImages += "}";


				}
				jsonOutput += ",\"blogImages\":[" + jsonBlogImages+ "]";*/

				
				/*jsonOutput += ",\"blogLocationContent\":\"" + blogArticle.getb()+ "\"";
				jsonOutput += ",\"createdDate\":\"" + blogArticle.getCreatedDate()+ "\"";	*/			
				jsonOutput += "}";

			//}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	} 	
	
	public String getUloBlogArticle() throws IOException {
		
		
		
		try {

			//String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		   
			
			BlogArticlesManager BlogArticleController =  new BlogArticlesManager();
			BlogLocationsManager blogLocationController = new BlogLocationsManager();
			BlogCategoriesManager blogCategoryController = new BlogCategoriesManager();
			BlogArticleImagesManager BlogArticleImageController =  new BlogArticleImagesManager();
			
			
				BlogArticles blogArticles = BlogArticleController.find(getBlogArticleId());
			
			String DummyId = "true";
			sessionMap.put("blogArticleId", blogArticles.getBlogArticleId());
			sessionMap.put("DummyId", DummyId);
			
		String title =  (blogArticles.getBlogArticleDescription() == null ? "":  blogArticles.getBlogArticleDescription().trim());
			String strTitle = title.replace("\"", "\\\"" );
			String strTrimTitle = strTitle.replaceAll("\\s+"," ");
			
			
			request.setAttribute("blogArticleDescription" ,blogArticles.getBlogArticleDescription());
			request.setAttribute("blogArticleTitle" ,blogArticles.getBlogArticleTitle());
			
			

			/*response.setContentType("application/json");
			
                                        
			 
			this.blogArticleList = BlogArticleController.list();
			for (BlogArticles blogArticle : blogArticleList) {
			BlogArticles blogArticle = BlogArticleController.find(getBlogArticleId());
				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"blogArticleId\":\"" + blogArticle.getBlogArticleId() + "\"";
				jsonOutput += ",\"blogArticleTitle\":\"" + blogArticle.getBlogArticleTitle()+ "\"";
				//BlogLocations bloglocation = blogLocationController.find(blogArticle.getBlogLocations().getBlogLocationId());
				jsonOutput += ",\"blogLocationId\":\"" + (blogArticle.getBlogLocations().getBlogLocationId() == null ? "": blogArticle.getBlogLocations().getBlogLocationId() )+ "\"";
				jsonOutput += ",\"blogCategoryId\":\"" + (blogArticle.getBlogCategories().getBlogCategoryId() == null ? "": blogArticle.getBlogCategories().getBlogCategoryId() )+ "\"";
				//BlogCategories blogcategory = blogCategoryController.find(getBlogCategoryId());
				
				String title =  (blogArticle.getBlogArticleDescription() == null ? "":  blogArticle.getBlogArticleDescription().trim());
				String strTitle = title.replace("\"", "\\\"" );
				String strTrimTitle = strTitle.replaceAll("\\s+"," ");
				
				//jsonOutput += ",\"blogArticleDescription\":\"" + strTrimTitle + "\"";
				
				sessionMap.put("blogArticleId", blogArticle.getBlogArticleId());				
				request.setAttribute("blogArticleDescription" ,blogArticle.getBlogArticleTitle());
				
				
				
				this.blogArticleImageList = BlogArticleImageController.list(getBlogArticleId());
				String imagePath = "";
				String jsonBlogImages ="";
				for (BlogArticleImages image : blogArticleImageList) {
					
					if (!jsonBlogImages.equalsIgnoreCase(""))
						
						jsonBlogImages += ",{";
					else
						jsonBlogImages += "{";


					
					jsonBlogImages += "\"blogArticleImageId\":\"" + image.getBlogArticleImageId() + "\"";
					jsonBlogImages += ",\"blogArticlImage\":\"" + image.getBlogArticleImage()+ "\"";
					
						   imagePath = getText("storage.blogarticle.photo") + "/"						
								   		+ getBlogArticleId() + "/"
								   		+ image.getBlogArticleImage();
						   jsonBlogImages += ",\"imagePath\":\"" + imagePath + "\"";
					
						   jsonBlogImages += "}";


				}
				jsonOutput += ",\"blogImages\":[" + jsonBlogImages+ "]";

				
				jsonOutput += ",\"blogLocationContent\":\"" + blogArticle.getb()+ "\"";
				jsonOutput += ",\"createdDate\":\"" + blogArticle.getCreatedDate()+ "\"";				
				jsonOutput += "}";

			//}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			*/

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return SUCCESS;

	} 
	
public String getUloBlogArticles() throws IOException {
		
		
		try {

			//String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		   
			
			BlogArticlesManager BlogArticleController =  new BlogArticlesManager();
			BlogLocationsManager blogLocationController = new BlogLocationsManager();
			BlogCategoriesManager blogCategoryController = new BlogCategoriesManager();
			BlogArticleImagesManager BlogArticleImageController =  new BlogArticleImagesManager();
			
			
				BlogArticles blogArticles = BlogArticleController.find(getBlogArticleId());
			
				String DummyId = "true";
			//sessionMap.put("blogArticleId", blogArticles.getBlogArticleId());
				sessionMap.put("DummyId", DummyId);
			
			String title =  (blogArticles.getBlogArticleDescription() == null ? "":  blogArticles.getBlogArticleDescription().trim());
			String strTitle = title.replace("\"", "\\\"" );
			String strTrimTitle = strTitle.replaceAll("\\s+"," ");
			
			
			request.setAttribute("blogArticleDescription" ,blogArticles.getBlogArticleDescription());
			request.setAttribute("blogArticleTitle" ,blogArticles.getBlogArticleTitle());
			
			

			/*response.setContentType("application/json");
			
                                        
			 
			this.blogArticleList = BlogArticleController.list();
			for (BlogArticles blogArticle : blogArticleList) {
			BlogArticles blogArticle = BlogArticleController.find(getBlogArticleId());
				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"blogArticleId\":\"" + blogArticle.getBlogArticleId() + "\"";
				jsonOutput += ",\"blogArticleTitle\":\"" + blogArticle.getBlogArticleTitle()+ "\"";
				//BlogLocations bloglocation = blogLocationController.find(blogArticle.getBlogLocations().getBlogLocationId());
				jsonOutput += ",\"blogLocationId\":\"" + (blogArticle.getBlogLocations().getBlogLocationId() == null ? "": blogArticle.getBlogLocations().getBlogLocationId() )+ "\"";
				jsonOutput += ",\"blogCategoryId\":\"" + (blogArticle.getBlogCategories().getBlogCategoryId() == null ? "": blogArticle.getBlogCategories().getBlogCategoryId() )+ "\"";
				//BlogCategories blogcategory = blogCategoryController.find(getBlogCategoryId());
				
				String title =  (blogArticle.getBlogArticleDescription() == null ? "":  blogArticle.getBlogArticleDescription().trim());
				String strTitle = title.replace("\"", "\\\"" );
				String strTrimTitle = strTitle.replaceAll("\\s+"," ");
				
				//jsonOutput += ",\"blogArticleDescription\":\"" + strTrimTitle + "\"";
				
				sessionMap.put("blogArticleId", blogArticle.getBlogArticleId());				
				request.setAttribute("blogArticleDescription" ,blogArticle.getBlogArticleTitle());
				
				
				
				this.blogArticleImageList = BlogArticleImageController.list(getBlogArticleId());
				String imagePath = "";
				String jsonBlogImages ="";
				for (BlogArticleImages image : blogArticleImageList) {
					
					if (!jsonBlogImages.equalsIgnoreCase(""))
						
						jsonBlogImages += ",{";
					else
						jsonBlogImages += "{";


					
					jsonBlogImages += "\"blogArticleImageId\":\"" + image.getBlogArticleImageId() + "\"";
					jsonBlogImages += ",\"blogArticlImage\":\"" + image.getBlogArticleImage()+ "\"";
					
						   imagePath = getText("storage.blogarticle.photo") + "/"						
								   		+ getBlogArticleId() + "/"
								   		+ image.getBlogArticleImage();
						   jsonBlogImages += ",\"imagePath\":\"" + imagePath + "\"";
					
						   jsonBlogImages += "}";


				}
				jsonOutput += ",\"blogImages\":[" + jsonBlogImages+ "]";

				
				jsonOutput += ",\"blogLocationContent\":\"" + blogArticle.getb()+ "\"";
				jsonOutput += ",\"createdDate\":\"" + blogArticle.getCreatedDate()+ "\"";				
				jsonOutput += "}";

			//}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
*/

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	} 
	
public String getUloBlogArticleContent() throws IOException {
		
		
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		   
			
			BlogArticlesManager BlogArticleController =  new BlogArticlesManager();
			BlogLocationsManager blogLocationController = new BlogLocationsManager();
			BlogCategoriesManager blogCategoryController = new BlogCategoriesManager();
			BlogArticleImagesManager BlogArticleImageController =  new BlogArticleImagesManager();

			response.setContentType("application/json");
			
                                        
			
		/*	this.blogArticleList = BlogArticleController.list();
			for (BlogArticles blogArticle : blogArticleList) {*/
			BlogArticles blogArticle = BlogArticleController.find(getBlogArticleId());
				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"blogArticleId\":\"" + blogArticle.getBlogArticleId() + "\"";
				jsonOutput += ",\"blogArticleTitle\":\"" + blogArticle.getBlogArticleTitle()+ "\"";
				//BlogLocations bloglocation = blogLocationController.find(blogArticle.getBlogLocations().getBlogLocationId());
				jsonOutput += ",\"blogLocationId\":\"" + (blogArticle.getBlogLocations().getBlogLocationId() == null ? "": blogArticle.getBlogLocations().getBlogLocationId() )+ "\"";
				jsonOutput += ",\"blogCategoryId\":\"" + (blogArticle.getBlogCategories().getBlogCategoryId() == null ? "": blogArticle.getBlogCategories().getBlogCategoryId() )+ "\"";
				//BlogCategories blogcategory = blogCategoryController.find(getBlogCategoryId());
				
				String title =  (blogArticle.getBlogArticleDescription() == null ? "":  blogArticle.getBlogArticleDescription().trim());
				String strTitle = title.replace("\"", "\\\"" );
				String strTrimTitle = strTitle.replaceAll("\\s+"," ");
				
				jsonOutput += ",\"blogArticleDescription\":\"" + strTrimTitle + "\"";
				
				request.setAttribute("blogArticleDescription" ,blogArticle.getBlogArticleTitle());
				
			
				
				//jsonOutput += ",\"createdDate\":\"" + blogArticle.getCreatedDate()+ "\"";	*/			
				jsonOutput += "}";

			//}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	} 	
	
	
	public String deleteBlogArticleImage() {
		BlogArticleImagesManager BlogArticleImageController =  new BlogArticleImagesManager();
		
		try {
			
						
			BlogArticleImages articleimage = BlogArticleImageController.find(getBlogArticleImageId());
						
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			articleimage.setIsActive(false);
			articleimage.setIsDeleted(true);
			articleimage.setModifiedDate(new java.sql.Timestamp(date.getTime()));
			BlogArticleImageController.edit(articleimage);
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null; 
	}
	
	
	public String getBlogCategoryArticle() throws IOException {
		
		
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		   
			 
			BlogArticlesManager BlogArticleController =  new BlogArticlesManager();
			
			/*response.setContentType("application/json");
                                        
			
			this.blogArticleList = BlogArticleController.list(getBlogCategoryId());
			for (BlogArticles blogArticle : blogArticleList) {

				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"blogArticleId\":\"" + blogArticle.getBlogArticleId() + "\"";
				jsonOutput += ",\"blogArticleTitle\":\"" + blogArticle.getBlogArticleTitle()+ "\""; toUpperCase().trim()
				
				String title =  (blogArticle.getBlogArticleDescription() == null ? "": blogArticle.getBlogArticleDescription());
				String strTitle = title.replace("\"", "\\\"" );
				String strTrim = strTitle.replaceAll("\\<.*?\\>"," ");
				String strTrimTitle = strTrim.replaceAll("\\s+"," ");
				
				jsonOutput += ",\"blogArticleDescription\":\"" + strTrimTitle + "\"";
				String createDate = new SimpleDateFormat("MMMM dd,yyyy").format(blogArticle.getCreatedDate());
				jsonOutput += ",\"createdDate\":\"" + createDate + "\"";*/
				//sessionMap.put("blogCategoryId", getBlogCategoryId());
				
				request.setAttribute("blogCategoryId",getBlogCategoryId());
				/*request.setAttribute("description"+serialno ,blogArticle.getBlogArticleDescription());
				*/
				/*jsonOutput += ",\"blogLocationContent\":\"" + blogArticle.getb()+ "\"";
				jsonOutput += ",\"createdDate\":\"" + blogArticle.getCreatedDate()+ "\"";	*/			
			/*	jsonOutput += "}";

			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			*/
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return SUCCESS;

	}
	
public String getBlogCategoryArticles() throws IOException {
		
		
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		   
			 
			BlogArticlesManager BlogArticleController =  new BlogArticlesManager();
			BlogCategoriesManager BlogCategoriesController =  new BlogCategoriesManager();
			
			response.setContentType("application/json");
                                        
			
			this.blogArticleList = BlogArticleController.list(getBlogCategoryId());
			if(blogArticleList.size() == 0 && blogArticleList.isEmpty()){
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				BlogCategories blogCategory = BlogCategoriesController.find(getBlogCategoryId());
				jsonOutput += "\"blogCategoryId\":\"" + blogCategory.getBlogCategoryId() + "\"";
				jsonOutput += ",\"blogCategoryName\":\"" + blogCategory.getBlogCategoryName()+ "\"";
				jsonOutput += ",\"blogCategoryContent\":\"" + blogCategory.getBlogCategoryContent()+ "\"";
				jsonOutput += ",\"blogCategoryUrl\":\"" + blogCategory.getBlogCategoryUrl()+ "\"";
				jsonOutput += "}";
			}
			else{
			for (BlogArticles blogArticle : blogArticleList) { 

				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"blogArticleId\":\"" + blogArticle.getBlogArticleId() + "\"";
				jsonOutput += ",\"blogArticleTitle\":\"" + blogArticle.getBlogArticleTitle()+ "\""; /*toUpperCase().trim()*/
				jsonOutput += ",\"blogArticleUrl\":\"" + blogArticle.getBlogArticleUrl()+ "\"";
				String title =  (blogArticle.getBlogArticleDescription() == null ? "": blogArticle.getBlogArticleDescription());
				String strTitle = title.replace("\"", "\\\"" );
				String strTrim = strTitle.replaceAll("\\<.*?\\>"," ");
				String strTrimTitle = strTrim.replaceAll("\\s+"," ");
				
				jsonOutput += ",\"blogArticleDescription\":\"" + strTrimTitle + "\"";
				String createDate = new SimpleDateFormat("MMMM dd,yyyy").format(blogArticle.getCreatedDate());
				jsonOutput += ",\"createdDate\":\"" + createDate + "\"";
				BlogCategories blogCategory = BlogCategoriesController.find(getBlogCategoryId());
				jsonOutput += ",\"blogCategoryId\":\"" + blogCategory.getBlogCategoryId() + "\"";
				jsonOutput += ",\"blogCategoryName\":\"" + blogCategory.getBlogCategoryName()+ "\"";
				jsonOutput += ",\"blogCategoryContent\":\"" + blogCategory.getBlogCategoryContent()+ "\"";
				jsonOutput += ",\"blogCategoryUrl\":\"" + blogCategory.getBlogCategoryUrl()+ "\"";
				//sessionMap.put("blogCategoryId", getBlogCategoryId());
				//request.setAttribute("blogCategoryId",getBlogCategoryId());
				/*request.setAttribute("description"+serialno ,blogArticle.getBlogArticleDescription());
				*/
				/*jsonOutput += ",\"blogLocationContent\":\"" + blogArticle.getb()+ "\"";
				jsonOutput += ",\"createdDate\":\"" + blogArticle.getCreatedDate()+ "\"";	*/			
				jsonOutput += "}";

			}
			}
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}

	public String getBlogLocationArticle() throws IOException {
	
	
	try {

		String jsonOutput = "";
		HttpServletResponse response = ServletActionContext.getResponse();
	   
		 
		BlogArticlesManager BlogArticleController =  new BlogArticlesManager();

		/*response.setContentType("application/json");
                                    
		
		this.blogArticleList = BlogArticleController.listLocationArticle(getBlogLocationId());
		for (BlogArticles blogArticle : blogArticleList) { 

			
			if (!jsonOutput.equalsIgnoreCase(""))
				jsonOutput += ",{";
			else
				jsonOutput += "{";
			jsonOutput += "\"blogArticleId\":\"" + blogArticle.getBlogArticleId() + "\"";
			jsonOutput += ",\"blogArticleTitle\":\"" + blogArticle.getBlogArticleTitle()+ "\""; toUpperCase().trim()
			
			String title =  (blogArticle.getBlogArticleDescription() == null ? "": blogArticle.getBlogArticleDescription());
			String strTitle = title.replace("\"", "\\\"" );
			String strTrim = strTitle.replaceAll("\\<.*?\\>"," ");
			String strTrimTitle = strTrim.replaceAll("\\s+"," ");
			
			jsonOutput += ",\"blogArticleDescription\":\"" + strTrimTitle + "\"";
			String createDate = new SimpleDateFormat("MMMM dd,yyyy").format(blogArticle.getCreatedDate());
			jsonOutput += ",\"createdDate\":\"" + createDate + "\"";*/
			//sessionMap.put("blogLocationId", getBlogLocationId());
			
			request.setAttribute("blogLocationId",getBlogLocationId());
			/*request.setAttribute("description"+serialno ,blogArticle.getBlogArticleDescription());
			*/
			/*jsonOutput += ",\"blogLocationContent\":\"" + blogArticle.getb()+ "\"";
			jsonOutput += ",\"createdDate\":\"" + blogArticle.getCreatedDate()+ "\"";	*/			
		/*	jsonOutput += "}";

		}

		response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
		*/
		

	} catch (Exception e) {
		logger.error(e);
		e.printStackTrace();
	} finally {

	}

	return SUCCESS;

}
	
	public String getBlogLocationArticles() throws IOException {
	
	
	try {

		String jsonOutput = "";
		HttpServletResponse response = ServletActionContext.getResponse();
	   
		 
		BlogArticlesManager BlogArticleController =  new BlogArticlesManager();
		BlogLocationsManager BlogLocationsController =  new BlogLocationsManager();	
			
			

		response.setContentType("application/json");
                                    
		
		this.blogArticleList = BlogArticleController.listLocationArticle(getBlogLocationId());
		if(blogArticleList.size() == 0 && blogArticleList.isEmpty()){
			if (!jsonOutput.equalsIgnoreCase(""))
				jsonOutput += ",{";
			else
				jsonOutput += "{";
			BlogLocations blogLocation = BlogLocationsController.find(getBlogLocationId());
			jsonOutput += "\"blogLocationId\":\"" + blogLocation.getBlogLocationId() + "\"";
			jsonOutput += ",\"blogLocationName\":\"" + blogLocation.getBlogLocationName()+ "\"";
			jsonOutput += ",\"blogLocationContent\":\"" + blogLocation.getBlogLocationContent()+ "\"";
			jsonOutput += "}";
		}
		else{
		for (BlogArticles blogArticle : blogArticleList) { 

			
			if (!jsonOutput.equalsIgnoreCase(""))
				jsonOutput += ",{";
			else
				jsonOutput += "{";
			jsonOutput += "\"blogArticleId\":\"" + blogArticle.getBlogArticleId() + "\"";
			jsonOutput += ",\"blogArticleTitle\":\"" + blogArticle.getBlogArticleTitle()+ "\""; /*toUpperCase().trim()*/
			jsonOutput += ",\"blogArticleUrl\":\"" + blogArticle.getBlogArticleUrl()+ "\"";
			String title =  (blogArticle.getBlogArticleDescription() == null ? "": blogArticle.getBlogArticleDescription());
			String strTitle = title.replace("\"", "\\\"" );
			String strTrim = strTitle.replaceAll("\\<.*?\\>"," ");
			String strTrimTitle = strTrim.replaceAll("\\s+"," ");
			
			jsonOutput += ",\"blogArticleDescription\":\"" + strTrimTitle + "\"";
			String createDate = new SimpleDateFormat("MMMM dd,yyyy").format(blogArticle.getCreatedDate());
			jsonOutput += ",\"createdDate\":\"" + createDate + "\"";
			BlogLocations blogLocation = BlogLocationsController.find(getBlogLocationId());
			jsonOutput += ",\"blogLocationId\":\"" + blogLocation.getBlogLocationId() + "\"";
			jsonOutput += ",\"blogLocationName\":\"" + blogLocation.getBlogLocationName()+ "\"";
			jsonOutput += ",\"blogLocationContent\":\"" + blogLocation.getBlogLocationContent()+ "\"";
			//sessionMap.put("blogCategoryId", getBlogCategoryId());
			//request.setAttribute("blogCategoryId",getBlogCategoryId());
			/*request.setAttribute("description"+serialno ,blogArticle.getBlogArticleDescription());
			*/
			/*jsonOutput += ",\"blogLocationContent\":\"" + blogArticle.getb()+ "\"";
			jsonOutput += ",\"createdDate\":\"" + blogArticle.getCreatedDate()+ "\"";	*/			
			jsonOutput += "}";

		}
		}
		response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
		

	} catch (Exception e) {
		logger.error(e);
		e.printStackTrace();
	} finally {

	}

	return null;

}
	
public String sendBlogUserComment() {
		
		
		try {
			BlogUserCommentsManager BlogUserCommentController =  new BlogUserCommentsManager();
			BlogArticlesManager BlogArticleController =  new BlogArticlesManager();
			BlogUserComments blogComment = new BlogUserComments();
			long time = System.currentTimeMillis();
			java.util.Date date = new java.util.Date(time);
			Timestamp tsDate=new Timestamp(date.getTime());  
			
			blogComment.setBlogUserName(getBlogUserName());
			blogComment.setBlogUserEmail(getBlogUserEmail());
			blogComment.setBlogUserUrl(getBlogUserUrl());
			blogComment.setBlogUserComment(getBlogUserComment());
			blogComment.setIsActive(true);
			blogComment.setIsDeleted(false);						
			blogComment.setCreatedDate(tsDate);
			blogComment.setIsDisplay(false);
			BlogArticles blogArticle = BlogArticleController.find(getBlogArticleId());	
			blogComment.setBlogArticles(blogArticle);
			BlogUserCommentController.add(blogComment);			
			
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}	

	public String getBlogUserCommentCount() throws IOException {
		try {
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("application/json");
			BlogUserCommentsManager BlogUserCommentController =  new BlogUserCommentsManager();
			this.blogUserCommentList = BlogUserCommentController.list();
			if(blogUserCommentList.size()>0 && !blogUserCommentList.isEmpty()){
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				
			    jsonOutput += "\"count\":\"" + blogUserCommentList.size() + "\"";
			    
				jsonOutput += "}";
			}
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
public String getBlogAllUserComments() throws IOException {
		
		
		
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		   
			
			BlogUserCommentsManager BlogUserCommentController =  new BlogUserCommentsManager();

			response.setContentType("application/json");
                                        
			this.blogUserCommentList = BlogUserCommentController.list(limit,offset);
			for (BlogUserComments blogComments : blogUserCommentList) {

				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"blogUserCommentId\":\"" + blogComments.getBlogUserCommentId() + "\"";
				jsonOutput += ",\"blogUserName\":\"" + blogComments.getBlogUserName()+ "\""; /*toUpperCase().trim()*/
				jsonOutput += ",\"blogUserEmail\":\"" + blogComments.getBlogUserEmail() + "\"";
				jsonOutput += ",\"blogUserUrl\":\"" + blogComments.getBlogUserUrl() + "\"";
				jsonOutput += ",\"blogUserComment\":\"" + blogComments.getBlogUserComment() + "\"";
				String createDate = new SimpleDateFormat("MMMM dd,yyyy").format(blogComments.getCreatedDate());
				jsonOutput += ",\"createdDate\":\"" + createDate + "\"";
				jsonOutput += ",\"status\":\"" + blogComments.getIsDisplay() + "\"";
				/*String title =  (blogArticle.getBlogArticleDescription() == null ? "": blogArticle.getBlogArticleDescription());
				String strTitle = title.replace("\"", "\\\"" );
				String strTrim = strTitle.replaceAll("\\<.*?\\>"," ");
				String strTrimTitle = strTrim.replaceAll("\\s+"," ");
				
				jsonOutput += ",\"blogArticleDescription\":\"" + strTrimTitle + "\"";
				String createDate = new SimpleDateFormat("MMMM dd,yyyy").format(blogArticle.getCreatedDate());
				jsonOutput += ",\"createdDate\":\"" + createDate + "\"";	
				request.getAttribute("blogArticleDescription"+blogArticle.getBlogArticleTitle());
*/				/*request.setAttribute("description"+serialno ,blogArticle.getBlogArticleDescription());
				*/
				/*jsonOutput += ",\"blogLocationContent\":\"" + blogArticle.getb()+ "\"";
				jsonOutput += ",\"createdDate\":\"" + blogArticle.getCreatedDate()+ "\"";	*/			
				jsonOutput += "}";

			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}	

	public String editBlogUserStatus() {
		BlogUserCommentsManager BlogUserCommentController =  new BlogUserCommentsManager();
	
	try {
		
					
		BlogUserComments blogComments = BlogUserCommentController.find(getBlogUserCommentId());
					
		long time = System.currentTimeMillis();
		java.util.Date date = new java.util.Date(time);
		Timestamp tsDate=new Timestamp(date.getTime());  
		
		blogComments.setIsDisplay(true);
		blogComments.setModifiedDate(tsDate);
		BlogUserCommentController.edit(blogComments);		
	} catch (Exception e) {
		logger.error(e);
		e.printStackTrace();
	} finally {

	}
	return null; 
}
	
	public String getUloBlogUserComment() throws IOException {
		
		
		
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		   
			
			BlogUserCommentsManager BlogUserCommentController =  new BlogUserCommentsManager();

			response.setContentType("application/json");
                                        
			this.blogUserCommentList  = BlogUserCommentController.list(getBlogArticleId());
			
			for (BlogUserComments blogComments : blogUserCommentList) {
				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"blogUserCommentId\":\"" + blogComments.getBlogUserCommentId() + "\"";
				jsonOutput += ",\"blogUserName\":\"" + blogComments.getBlogUserName()+ "\""; /*toUpperCase().trim()*/
				jsonOutput += ",\"blogUserEmail\":\"" + blogComments.getBlogUserEmail() + "\"";
				jsonOutput += ",\"blogUserUrl\":\"" + blogComments.getBlogUserUrl() + "\"";
				jsonOutput += ",\"blogUserComment\":\"" + blogComments.getBlogUserComment() + "\"";
				String createDate = new SimpleDateFormat("MMMM dd,yyyy").format(blogComments.getCreatedDate());
				jsonOutput += ",\"createdDate\":\"" + createDate + "\"";
				/*String title =  (blogArticle.getBlogArticleDescription() == null ? "": blogArticle.getBlogArticleDescription());
				String strTitle = title.replace("\"", "\\\"" );
				String strTrim = strTitle.replaceAll("\\<.*?\\>"," ");
				String strTrimTitle = strTrim.replaceAll("\\s+"," ");
				
				jsonOutput += ",\"blogArticleDescription\":\"" + strTrimTitle + "\"";
				String createDate = new SimpleDateFormat("MMMM dd,yyyy").format(blogArticle.getCreatedDate());
				jsonOutput += ",\"createdDate\":\"" + createDate + "\"";	
				request.getAttribute("blogArticleDescription"+blogArticle.getBlogArticleTitle());
*/				/*request.setAttribute("description"+serialno ,blogArticle.getBlogArticleDescription());
				*/
				/*jsonOutput += ",\"blogLocationContent\":\"" + blogArticle.getb()+ "\"";
				jsonOutput += ",\"createdDate\":\"" + blogArticle.getCreatedDate()+ "\"";	*/			
				jsonOutput += "}";

			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	
/*public String searchArticleTitle() throws IOException {
		
		
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		   
			
			BlogArticlesManager BlogArticleController =  new BlogArticlesManager();

			response.setContentType("application/json");
                                        
			
			this.blogArticleList = BlogArticleController.list(getBlogArticleTitle());
			for (BlogArticles blogArticle : blogArticleList) {

				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"blogArticleId\":\"" + blogArticle.getBlogArticleId() + "\"";
				jsonOutput += ",\"blogArticleTitle\":\"" + blogArticle.getBlogArticleTitle()+ "\"";
				jsonOutput += ",\"blogLocationContent\":\"" + blogArticle.getb()+ "\"";
				jsonOutput += ",\"createdDate\":\"" + blogArticle.getCreatedDate()+ "\"";				
				jsonOutput += "}";
			}
				
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}*/

	
/*	public String getPropertyPhotos() throws IOException {
		this.propertyId = (Integer) sessionMap.get("propertyId");
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PropertyPhotoManager photoController = new PropertyPhotoManager();
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");

			this.propertyPhotoList = photoController.list(getPropertyId());
			for (PropertyPhoto photo : propertyPhotoList) {

				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"DT_RowId\":\"" + photo.getPropertyPhotoId() + "\"";
				jsonOutput += ",\"isPrimary\":\"" + photo.getIsPrimary()+ "\"";
				jsonOutput += ",\"photoPath\":\"" + photo.getPhotoPath()+ "\"";
				
				
				jsonOutput += "}";


			}
			

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	public String getUloPropertyPhotos() throws IOException {
		this.propertyId = (Integer) sessionMap.get("uloPropertyId");
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PropertyPhotoManager photoController = new PropertyPhotoManager();
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");

			this.propertyPhotoList = photoController.list(getPropertyId());
			for (PropertyPhoto photo : propertyPhotoList) {

				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"DT_RowId\":\"" + photo.getPropertyPhotoId() + "\"";
				jsonOutput += ",\"isPrimary\":\"" + photo.getIsPrimary()+ "\"";
				jsonOutput += ",\"photoPath\":\"" + photo.getPhotoPath()+ "\"";
				
				
				jsonOutput += "}";


			}
			

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	
	public String getCategoryPhotos() throws IOException {
		this.propertyId = (Integer) sessionMap.get("propertyId");
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PropertyPhotoManager photoController = new PropertyPhotoManager();
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");

			this.propertyPhotoList = photoController.list1(getPropertyId(),getPhotoCategoryId());
			for (PropertyPhoto photo : propertyPhotoList) {

				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"DT_RowId\":\"" + photo.getPropertyPhotoId() + "\"";
				jsonOutput += ",\"isPrimary\":\"" + photo.getIsPrimary()+ "\"";
				jsonOutput += ",\"photoPath\":\"" + photo.getPhotoPath()+ "\"";
				jsonOutput += ",\"photoCategoryId\":\"" + getPhotoCategoryId()+ "\"";
				
				
				jsonOutput += "}";


			}
			

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}*/
	

	public String addPropertyPhoto() {
		this.propertyId = (Integer) sessionMap.get("propertyId");
		PropertyPhotoManager photoController = new PropertyPhotoManager();
		PmsPropertyManager propertyController = new PmsPropertyManager();
		PmsPhotoCategoryManager categoryController = new PmsPhotoCategoryManager();
		//UserLoginManager  userController = new UserLoginManager();
		try {
			
						
			PropertyPhoto photo = new PropertyPhoto();
			
			
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);

			if(this.getMyFileFileName()!=null && !this.getMyFileFileName().equalsIgnoreCase(""))
			{
				PmsProperty property = propertyController.find(getPropertyId());
				photo.setPhotoPath(this.getMyFileFileName());
				photo.setIsActive(true);
				photo.setIsDeleted(false);
				photo.setPmsProperty(property);
				PmsPhotoCategory category = categoryController.find(getPhotoCategoryId());
				photo.setPmsPhotoCategory(category);
			
				PropertyPhoto photo1 = photoController.add(photo);
			
				if(photo1.getPropertyPhotoId()>0 && this.getMyFileFileName()!=null && !this.getMyFileFileName().equalsIgnoreCase(""))
				{
					File file = new File(getText("storage.property.photo") + "/"
							+ getPropertyId() + "/" + photo1.getPropertyPhotoId() + "/", this.getMyFileFileName());
					FileUtils.copyFile(this.myFile, file);// copying image in the
				}
			}
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}
	
	
	
	public String getPropertyPicture() {
		
		this.propertyId = (Integer) sessionMap.get("propertyId");
		HttpServletResponse response = ServletActionContext.getResponse();

		PropertyPhotoManager photoController = new PropertyPhotoManager();
		PropertyPhoto photo = photoController.find(getPropertyPhotoId());
		String imagePath = "";
		// response.setContentType("");
		try {
			
			if (getPropertyPhotoId() >0 ) {
				imagePath = getText("storage.property.photo") + "/"
						+ photo.getPmsProperty().getPropertyId()+ "/"
						+ photo.getPropertyPhotoId() + "/"
						+ photo.getPhotoPath();
				Image im = new Image();
				response.getOutputStream().write(
						im.getCustomImageInBytes(imagePath));
				response.getOutputStream().flush();
			} else {
				imagePath = getText("storage.path") + "/emptyprofilepic.png";
				Image im = new Image();
				response.getOutputStream().write(
						im.getCustomImageInBytes(imagePath));
				response.getOutputStream().flush();
			}
		} catch (Exception e1) {
			logger.error(e1);
		} finally {

		}
		return null;

	}
	
	
	
	
	
	
	public String deletePromotionImage() {
		
		PropertyPhotoManager photoController = new PropertyPhotoManager();
		UserLoginManager  userController = new UserLoginManager();
		try {
			
						
			PromotionImageManager promotionimageController = new PromotionImageManager();
			PromotionImages promotion = promotionimageController.find(getPromotionImageId());
						
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			promotion.setIsActive(false);
			promotion.setIsDeleted(true);
			promotion.setModifiedDate(new java.sql.Timestamp(date.getTime()));
			promotionimageController.edit(promotion);
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}
	
	

	
	

	public File getMyFile() {
		return myFile;
	}

	public void setMyFile(File myFile) {
		this.myFile = myFile;
	}

	public String getMyFileContentType() {
		return myFileContentType;
	}

	public void setMyFileContentType(String myFileContentType) {
		this.myFileContentType = myFileContentType;
	}

	public String getMyFileFileName() {
		return myFileFileName;
	}

	public void setMyFileFileName(String myFileFileName) {
		this.myFileFileName = myFileFileName;
	}

	public Integer getPropertyPhotoId() {
		return propertyPhotoId;
	}

	public void setPropertyPhotoId(Integer propertyPhotoId) {
		this.propertyPhotoId = propertyPhotoId;
	}

	public Integer getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(Integer propertyId) {
		this.propertyId = propertyId;
	}

	public Boolean getIsPrimary() {
		return isPrimary;
	}

	public void setIsPrimary(Boolean isPrimary) {
		this.isPrimary = isPrimary;
	}
	
	public Integer getPromotionImageId() {
		return promotionImageId;
	}

	public void setPromotionImageId(Integer promotionImageId) {
		this.promotionImageId = promotionImageId;
	}

	
	
	public Integer getPhotoCategoryId() {
		return photoCategoryId;
	}

	public void setPhotoCategoryId(Integer photoCategoryId) {
		this.photoCategoryId = photoCategoryId;
	}
	
	
	public List<BlogCategories> getBlogCategoryList() {
		return blogCategoryList;
	}

	public void setBlogCategoryList(List<BlogCategories> blogCategoryList) {
		this.blogCategoryList = blogCategoryList;
	}

	public Integer getBlogArticleId() {
		return blogArticleId;
	}

	public void setBlogArticleId(Integer blogArticleId) {
		this.blogArticleId = blogArticleId;
	}

	public Integer getBlogLocationId() {
		return blogLocationId;
	}

	public void setBlogLocationId(Integer blogLocationId) {
		this.blogLocationId = blogLocationId;
	}

	public Integer getBlogCategoryId() {
		return blogCategoryId;
	}

	public void setBlogCategoryId(Integer blogCategoryId) {
		this.blogCategoryId = blogCategoryId;
	}
	
	public String getBlogCategoryName() {
		return blogCategoryName;
	}

	public void setBlogCategoryName(String blogCategoryName) {
		this.blogCategoryName = blogCategoryName;
	}
	
	public String getBlogCategoryContent() {
		return blogCategoryContent;
	}

	public void setBlogCategoryContent(String blogCategoryContent) {
		this.blogCategoryContent = blogCategoryContent;
	}
	
	public String getBlogLocationName() {
		return blogLocationName;
	}

	public void setBlogLocationName(String blogLocationName) {
		this.blogLocationName = blogLocationName;
	}

	public String getBlogLocationContent() {
		return blogLocationContent;
	}

	public void setBlogLocationContent(String blogLocationContent) {
		this.blogLocationContent = blogLocationContent;
	}
	
	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	public Integer getOffset() {
		return offset;
	}

	public void setOffset(Integer offset) {
		this.offset = offset;
	}
	
	public String getBlogArticleTitle() {
		return blogArticleTitle;
	}

	public void setBlogArticleTitle(String blogArticleTitle) {
		this.blogArticleTitle = blogArticleTitle;
	}
	
	public Integer getLocationId() {
		return locationId;
	}

	public void setLocationId(Integer locationId) {
		this.locationId = locationId;
	}
	
	public String getBlogArticleDescription() {
		return blogArticleDescription;
	}

	public void setBlogArticleDescription(String blogArticleDescription) {
		this.blogArticleDescription = blogArticleDescription;
	}
	
	public Integer getBlogArticleImageId() {
		return blogArticleImageId;
	}

	public void setBlogArticleImageId(Integer blogArticleImageId) {
		this.blogArticleImageId = blogArticleImageId;
	}
	
	public String getBlogUserName() {
		return blogUserName;
	}

	public void setBlogUserName(String blogUserName) {
		this.blogUserName = blogUserName;
	}

	public String getBlogUserEmail() {
		return blogUserEmail;
	}

	public void setBlogUserEmail(String blogUserEmail) {
		this.blogUserEmail = blogUserEmail;
	}

	public String getBlogUserUrl() {
		return blogUserUrl;
	}

	public void setBlogUserUrl(String blogUserUrl) {
		this.blogUserUrl = blogUserUrl;
	}

	public String getBlogUserComment() {
		return blogUserComment;
	}

	public void setBlogUserComment(String blogUserComment) {
		this.blogUserComment = blogUserComment;
	}
	
	public Integer getBlogUserCommentId() {
		return blogUserCommentId;
	}

	public void setBlogUserCommentId(Integer blogUserCommentId) {
		this.blogUserCommentId = blogUserCommentId;
	}

	public String getBlogCategoryUrl() {
		return blogCategoryUrl;
	}

	public void setBlogCategoryUrl(String blogCategoryUrl) {
		this.blogCategoryUrl = blogCategoryUrl;
	}

	public String getBlogLocationUrl() {
		return blogLocationUrl;
	}

	public void setBlogLocationUrl(String blogLocationUrl) {
		this.blogLocationUrl = blogLocationUrl;
	}

	public String getBlogArticleUrl() {
		return blogArticleUrl;
	}

	public void setBlogArticleUrl(String blogArticleUrl) {
		this.blogArticleUrl = blogArticleUrl;
	}

}
