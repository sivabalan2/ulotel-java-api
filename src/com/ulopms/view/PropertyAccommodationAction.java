package com.ulopms.view;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.math.BigDecimal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.commons.io.FileUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;




//import com.medwecare.controller.AwsObjectManager;
//import com.medwecare.controller.CommentManager;
//import com.medwecare.controller.CommentUserManager;
////import com.medwecare.controller.NotificationManager;
//import com.medwecare.controller.ProjectManager;

//import com.medwecare.model.AwsObject;
//import com.medwecare.model.Notification;
//import com.medwecare.model.ObjectComment;
//import com.medwecare.model.ObjectCommentUser;
//import com.medwecare.model.Project;






















import org.hamcrest.core.IsNull;
import org.joda.time.DateTime;

import com.opensymphony.xwork2.ActionSupport;

import freemarker.template.Configuration;
import freemarker.template.Template;

import javax.servlet.http.HttpServletRequest;

import com.opensymphony.xwork2.ModelDriven;
import com.ulopms.controller.OtaHotelsManager;
import com.ulopms.controller.PmsGuestManager;
import com.ulopms.controller.PmsPropertyManager;
import com.ulopms.controller.PropertyAccommodationInventoryManager;
import com.ulopms.controller.PropertyAccommodationManager;
import com.ulopms.controller.PropertyAccommodationRoomManager;
import com.ulopms.controller.PropertyContractTypeManager;
import com.ulopms.controller.UserLoginManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.AccommodationRoom;
import com.ulopms.model.OtaHotelDetails;
import com.ulopms.model.OtaHotels;
import com.ulopms.model.PmsBooking;
import com.ulopms.model.PmsGuest;
import com.ulopms.model.PmsProperty;
import com.ulopms.model.PropertyAccommodation;
import com.ulopms.model.PropertyAccommodationInventory;
import com.ulopms.model.PropertyAccommodationRoom;
import com.ulopms.model.User;
import com.ulopms.util.Email;
import com.ulopms.util.Image;
import com.ulopms.view.PropertyAccommodationRoomAction;










import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.math.BigDecimal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.commons.io.FileUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;




//import com.medwecare.controller.AwsObjectManager;
//import com.medwecare.controller.CommentManager;
//import com.medwecare.controller.CommentUserManager;
////import com.medwecare.controller.NotificationManager;
//import com.medwecare.controller.ProjectManager;

//import com.medwecare.model.AwsObject;
//import com.medwecare.model.Notification;
//import com.medwecare.model.ObjectComment;
//import com.medwecare.model.ObjectCommentUser;
//import com.medwecare.model.Project;






















import org.joda.time.DateTime;
import org.json.JSONObject;
import org.jsoup.select.Evaluator.IsEmpty;

import com.opensymphony.xwork2.ActionSupport;

import freemarker.template.Configuration;
import freemarker.template.Template;

import javax.servlet.http.HttpServletRequest;

import com.opensymphony.xwork2.ModelDriven;
import com.ulopms.controller.OtaHotelsManager;
import com.ulopms.controller.PmsGuestManager;
import com.ulopms.controller.PmsPropertyManager;
import com.ulopms.controller.PropertyAccommodationInventoryManager;
import com.ulopms.controller.PropertyAccommodationManager;
import com.ulopms.controller.PropertyAccommodationRoomManager;
import com.ulopms.controller.UserLoginManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.AccommodationRoom;
import com.ulopms.model.BookedDetail;
import com.ulopms.model.DashBoard;
import com.ulopms.model.OtaHotelDetails;
import com.ulopms.model.OtaHotels;
import com.ulopms.model.PmsBooking;
import com.ulopms.model.PmsGuest;
import com.ulopms.model.PmsProperty;
import com.ulopms.model.PropertyAccommodation;
import com.ulopms.model.PropertyAccommodationInventory;
import com.ulopms.model.PropertyAccommodationRoom;
import com.ulopms.model.PropertyContractType;
import com.ulopms.model.User;
import com.ulopms.util.Email;
import com.ulopms.util.Image;
import com.ulopms.view.PropertyAccommodationRoomAction;
public class PropertyAccommodationAction extends ActionSupport implements
ServletRequestAware, SessionAware, UserAware{
     
	

	private static final long serialVersionUID = 914982665758390091L;

	private Integer propertyAccommodationId;
	private Integer propertyContractTypeId;
	private Integer propertyId;
	private Integer partnerPropertyId;
	private Integer selectPartnerPropertyId;
	private PropertyAccommodation propertyaccommodation = new PropertyAccommodation();
	private String  abbreviation;
	private String  accommodationType;
	private Integer noOfUnits;
	private Integer minOccupancy;
	private Integer maxOccupancy;
	private Integer noOfAdults;
	private Integer noOfChild;
	private double  baseAmount;
	private double  extraChild;
	private double  extraAdult;
	private double  extraInfant;
	private String  description;
	private String accommodationPic;
	private String startBlockDate;
	private String endBlockDate;
	private String inventoryCount;
	private String inventoryRemarks;
	private String timeHours;
	private Integer userId;
	private double netRateRevenue;
	private double minimumBaseAmount;
	private double maximumBaseAmount;
	
	private double netRate;
	private Integer taxType;
	private double tariff;
	private double tax;
	private double sellingRate;
	private double purchaseRate;
	private Integer locationTypeId;
	private double weekdayNetRate;
	private double weekendNetRate;
	private double weekdayPurchaseRate;
	private double weekendPurchaseRate;
	private double weekdayBaseAmount;
	private double weekendBaseAmount;
	private double weekdayTariff;
	private double weekendTariff;
	private double weekdayTax;
	private double weekendTax;
	private double weekendSellingRate;
	private double weekdaySellingRate;
	private double propertyCommission;
	
	private PropertyAccommodation accommodation;
	
	private List<PropertyAccommodation> accommodationList;
	
	private List<PropertyAccommodationRoom> accommodationroomList;
	
	List<BookingListAction> array = new ArrayList<BookingListAction>();

    public List<BookingListAction> getArray() {
        return array;
    }
    
    public void setArray(List<BookingListAction> array) {
        this.array = array;
    }
	

	private static final Logger logger = Logger.getLogger(PropertyAccommodationAction.class);
    
	private HttpSession session;
	
	private File myFile;
	private String myFileContentType;
	private String myFileFileName;

	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}
	
	private SessionMap<String,Object> sessionMap;  
	  
	//getters and setters  
	  
	@Override  
	public void setSession(Map<String, Object> map) {  
	    sessionMap=(SessionMap)map;  
	}  
	
	
	// @Override
	// public User getModel() {
	// return user;
	// }
	
	private User user;

	
	// @Override
	// public User getModel() {
	// return user;
	// }

	@Override
	public void setUser(User user) {
		this.user = user;
	}
	
	 

	public PropertyAccommodation getModel() {
		return propertyaccommodation;
	}
    
	
	private HttpServletRequest request;

	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	public PropertyAccommodationAction() {
		
	//this.propertyId = (Integer) sessionMap.get("propertyId");
	
	}

	public String execute() {

		//this.patientId = getPatientId();
		return SUCCESS;
	}

	
	public String getAccommodation() throws IOException {
		try {
			Integer contractTypeId=0;	
			
			this.propertyId = (Integer) sessionMap.get("propertyId");
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			PmsPropertyManager propertyController=new PmsPropertyManager();
			
			PmsProperty property=propertyController.find(this.propertyId);
			contractTypeId=property.getContractType().getContractTypeId();
			
			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
			PropertyAccommodationRoomManager accommodationRoomController = new PropertyAccommodationRoomManager();

			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");
                                        
			PropertyAccommodation accommodation = accommodationController.find(getPropertyAccommodationId());
			//AccommodationRoom accommodationRooms = accommodationRoomController.findCount(this.propertyId,getPropertyAccommodationId());
			//accommodationRooms.getRoomCount();
			
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"propertyAccommodationId\":\"" + accommodation.getAccommodationId() + "\"";
				jsonOutput += ",\"abbreviation\":\"" + accommodation.getAbbreviation()+ "\"";
				jsonOutput += ",\"accommodationType\":\"" + accommodation.getAccommodationType()+ "\"";
				jsonOutput += ",\"noOfUnits\":\"" + accommodation.getNoOfUnits()+ "\"";
				jsonOutput += ",\"minimumOccupancy\":\"" + accommodation.getMinOccupancy()+ "\"";
				jsonOutput += ",\"maximumOccupancy\":\"" + accommodation.getMaxOccupancy()+ "\"";
				jsonOutput += ",\"noOfAdults\":\"" + accommodation.getNoOfAdults()+ "\"";
				jsonOutput += ",\"noOfChild\":\"" + accommodation.getNoOfChild()+ "\"";
				//jsonOutput += ",\"baseAmount\":\"" + accommodation.getBaseAmount()+ "\"";
				//jsonOutput += ",\"extraChild\":\"" + accommodation.getExtraChild()+ "\"";
				//jsonOutput += ",\"extraAdult\":\"" + accommodation.getExtraAdult()+ "\"";
				//jsonOutput += ",\"extraInfant\":\"" + accommodation.getExtraInfant()+ "\"";
				//jsonOutput += ",\"netRateRevenue\":\"" + (accommodation.getNetRateRevenue()==null?0:accommodation.getNetRateRevenue())+ "\"";
				jsonOutput += ",\"propertyContractType\":\"" + (accommodation.getPropertyContractType()==null?0:accommodation.getPropertyContractType().getContractTypeName())+ "\"";
				jsonOutput += ",\"propertyContractTypeId\":\"" + (accommodation.getPropertyContractType()==null?0:accommodation.getPropertyContractType().getContractTypeId())+ "\"";
				if(contractTypeId==1 || contractTypeId==2 || contractTypeId==3){
					jsonOutput += ",\"netRateContractTypeId\":\"" + contractTypeId+ "\"";
					jsonOutput += ",\"netRateEnable\":\"" + (1)+ "\"";
				}else{
					jsonOutput += ",\"netRateContractTypeId\":\"" + contractTypeId+ "\"";
					jsonOutput += ",\"netRateEnable\":\"" + (0)+ "\"";
				}
				jsonOutput += ",\"minimumBaseAmount\":\"" + (accommodation.getMinimumBaseAmount()==null?0:accommodation.getMinimumBaseAmount())+ "\"";
				jsonOutput += ",\"maximumBaseAmount\":\"" + (accommodation.getMaximumBaseAmount()==null?0:accommodation.getMaximumBaseAmount())+ "\"";
				jsonOutput += ",\"description\":\"" + accommodation.getAccommodationDescription()+ "\"";
				
				List<PropertyAccommodation> accommodations = accommodationController.listId(accommodation.getAccommodationId(),accommodation.getPropertyContractType().getContractTypeId());
				for(PropertyAccommodation accommodationss : accommodations){
				if(property.getLocationType().getLocationTypeId() == 1){
					jsonOutput += ",\"baseAmount\":\"" + (accommodationss.getBaseAmount() == 0?0 : accommodation.getBaseAmount()) + "\"";
					jsonOutput += ",\"netRate\":\"" + accommodationss.getNetRate()+ "\"";
					jsonOutput += ",\"taxType\":\"" + accommodationss.getTaxType()+ "\"";
					jsonOutput += ",\"tariff\":\"" + accommodationss.getTariff()+ "\"";
					jsonOutput += ",\"tax\":\"" + accommodationss.getTax()+ "\"";
					jsonOutput += ",\"sellingRate\":\"" + accommodationss.getSellingRate()+ "\"";
					jsonOutput += ",\"purchaseRate\":\"" + accommodationss.getPurchaseRate()+ "\"";
					jsonOutput += ",\"extraChild\":\"" + accommodationss.getExtraChild()+ "\"";
					jsonOutput += ",\"extraAdult\":\"" + accommodationss.getExtraAdult()+ "\"";
					//jsonOutput += ",\"propertyContractType\":\"" + (accommodationss.getPropertyContractType()==null?0:accommodationss.getPropertyContractType().getContractTypeName())+ "\"";
					//jsonOutput += ",\"propertyContractId\":\"" + (accommodationss.getPropertyContractType()==null?0:accommodationss.getPropertyContractType().getContractTypeId())+ "\"";
				}else if(property.getLocationType().getLocationTypeId() == 2){
					jsonOutput += ",\"taxType\":\"" + accommodationss.getTaxType()+ "\"";
					jsonOutput += ",\"weekdayPurchase\":\"" + (accommodationss.getWeekdayPurchaseRate() == null?0 : accommodationss.getWeekdayPurchaseRate() ) + "\"";
					jsonOutput += ",\"weekendPurchase\":\"" + accommodationss.getWeekendPurchaseRate()+ "\"";
					jsonOutput += ",\"weekdayNetRate\":\"" + accommodationss.getWeekdayNetRate()+ "\"";
					jsonOutput += ",\"weekendNetRate\":\"" + accommodationss.getWeekendNetRate()+ "\"";
					jsonOutput += ",\"weekdayBaseAmount\":\"" + accommodationss.getWeekdayBaseAmount()+ "\"";
					jsonOutput += ",\"weekendBaseAmount\":\"" + accommodationss.getWeekendBaseAmount()+ "\"";
					jsonOutput += ",\"weekdayTariff\":\"" + accommodationss.getWeekdayTariff()+ "\"";
					jsonOutput += ",\"weekendTariff\":\"" + accommodationss.getWeekendTariff()+ "\"";
					jsonOutput += ",\"weekdayTax\":\"" + accommodationss.getWeekdayTax()+ "\"";
					jsonOutput += ",\"weekdendTax\":\"" + accommodationss.getWeekendTax()+ "\"";
					jsonOutput += ",\"weekdaySelling\":\"" + accommodationss.getWeekdaySellingRate()+ "\"";
					jsonOutput += ",\"weekendSelling\":\"" + accommodationss.getWeekendSellingRate()+ "\"";
					jsonOutput += ",\"extraChild\":\"" + accommodationss.getExtraChild()+ "\"";
					jsonOutput += ",\"extraAdult\":\"" + accommodationss.getExtraAdult()+ "\"";
				}

				}
				
				jsonOutput += "}";

			

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	
	public String getAccommodations() throws IOException {
		try {
			this.propertyId = (Integer) sessionMap.get("propertyId");
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
			PropertyContractTypeManager contractTypeController = new PropertyContractTypeManager();
			PropertyAccommodationRoomManager accommodationRoomController = new PropertyAccommodationRoomManager();
			PmsPropertyManager propertyController=new PmsPropertyManager();
			PmsProperty property=propertyController.find(this.propertyId);
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");

			this.accommodationList = accommodationController.list(getPropertyId());
			for (PropertyAccommodation accommodation : accommodationList) {

				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				
				//AccommodationRoom accommodationRooms = accommodationRoomController.findCount(this.propertyId,accommodation.getAccommodationId());
				jsonOutput += "\"accommodationId\":\"" + accommodation.getAccommodationId() + "\"";
				jsonOutput += ",\"abbreviation\":\"" + accommodation.getAbbreviation()+ "\"";
				jsonOutput += ",\"accommodationType\":\"" + accommodation.getAccommodationType()+ "\"";
				jsonOutput += ",\"noOfUnits\":\"" + accommodation.getNoOfUnits()+ "\"";
				jsonOutput += ",\"minOccupancy\":\"" + accommodation.getMinOccupancy()+ "\"";
				jsonOutput += ",\"maxOccupancy\":\"" + accommodation.getMaxOccupancy()+ "\"";
				jsonOutput += ",\"noOfAdults\":\"" + accommodation.getNoOfAdults()+ "\"";
				jsonOutput += ",\"noOfChild\":\"" + accommodation.getNoOfChild()+ "\"";
				/*jsonOutput += ",\"baseAmount\":\"" + accommodation.getBaseAmount()+ "\"";
				jsonOutput += ",\"extraChild\":\"" + accommodation.getExtraChild()+ "\"";
				jsonOutput += ",\"extraAdult\":\"" + accommodation.getExtraAdult()+ "\"";*/
				jsonOutput += ",\"extraInfant\":\"" + accommodation.getExtraInfant()+ "\"";
				jsonOutput += ",\"description\":\"" + accommodation.getAccommodationDescription()+ "\"";
				jsonOutput += ",\"propertyContractType\":\"" + ( accommodation.getPropertyContractType()==null?"Nil": accommodation.getPropertyContractType().getContractTypeName()) + "\"";
				jsonOutput += ",\"propertyContractTypeId\":\"" + ( accommodation.getPropertyContractType()==null?0: accommodation.getPropertyContractType().getContractTypeId()) + "\"";
				
				if(accommodation.getPropertyContractType() != null){
				List<PropertyAccommodation> accommodations = accommodationController.listId(accommodation.getAccommodationId(),accommodation.getPropertyContractType().getContractTypeId());
				for(PropertyAccommodation accommodationss : accommodations){
				if(property.getLocationType().getLocationTypeId() == 1){
					jsonOutput += ",\"baseAmount\":\"" + (accommodationss.getBaseAmount() == null?0 : (int)Math.round(accommodation.getBaseAmount())) + "\"";
					jsonOutput += ",\"netRate\":\"" + (accommodationss.getNetRate() == null?0 : (int)Math.round(accommodationss.getNetRate()))+ "\"";
					jsonOutput += ",\"taxType\":\"" + (accommodationss.getTaxType() == null?"" : accommodationss.getTaxType())+ "\"";
					jsonOutput += ",\"tariff\":\"" + (accommodationss.getTariff() == null?0 : (int)Math.round(accommodationss.getTariff()))+ "\"";
					jsonOutput += ",\"tax\":\"" + (accommodationss.getTax() == null?0 : (int)Math.round(accommodationss.getTax()))+ "\"";
					jsonOutput += ",\"sellingRate\":\"" + (accommodationss.getSellingRate() == null?0 : (int)Math.round(accommodationss.getSellingRate()))+ "\"";
					jsonOutput += ",\"purchaseRate\":\"" + (accommodationss.getPurchaseRate() == null?0 : (int)Math.round(accommodationss.getPurchaseRate()))+ "\"";
					jsonOutput += ",\"extraChild\":\"" + (accommodationss.getExtraChild() == null?0 : (int)Math.round(accommodationss.getExtraChild()))+ "\"";
					jsonOutput += ",\"extraAdult\":\"" + (accommodationss.getExtraAdult() == null?0 : (int)Math.round(accommodationss.getExtraAdult()))+ "\"";
					Double extraAdult,extraChild;
					double dblextraadult,dblextrachild;
					extraAdult=accommodationss.getExtraAdult()==null?0:accommodationss.getExtraAdult();
					extraChild=accommodationss.getExtraChild()==null?0:accommodationss.getExtraChild();
					
					dblextraadult=extraAdult+extraAdult*15/100;
					dblextrachild=extraChild+extraChild*15/100;
					jsonOutput += ",\"sellingChild\":\"" + (Math.round(dblextrachild))+ "\"";
					jsonOutput += ",\"sellingAdult\":\"" + (Math.round(dblextraadult))+ "\"";
					
					jsonOutput += ",\"propertyCommission\":\"" + (accommodationss.getAccommodationCommission() == null?0 : (int)Math.round(accommodationss.getAccommodationCommission()))+ "\"";
					//jsonOutput += ",\"propertyContractType\":\"" + (accommodationss.getPropertyContractType()==null?0:accommodationss.getPropertyContractType().getContractTypeName())+ "\"";
					//jsonOutput += ",\"propertyContractId\":\"" + (accommodationss.getPropertyContractType()==null?0:accommodationss.getPropertyContractType().getContractTypeId())+ "\"";
				}else if(property.getLocationType().getLocationTypeId() == 2){
					jsonOutput += ",\"taxType\":\"" + (accommodationss.getTaxType() == null?"" : accommodationss.getTaxType())+ "\"";
					jsonOutput += ",\"weekdayPurchase\":\"" + (accommodationss.getWeekdayPurchaseRate() == null?0 : (int)Math.round(accommodationss.getWeekdayPurchaseRate())) + "\"";
					jsonOutput += ",\"weekendPurchase\":\"" + (accommodationss.getWeekendPurchaseRate() == null?0 : (int)Math.round(accommodationss.getWeekendPurchaseRate()))+ "\"";
					jsonOutput += ",\"weekdayNetRate\":\"" + ( accommodationss.getWeekdayNetRate() == null?0 :  (int)Math.round(accommodationss.getWeekdayNetRate()))+ "\"";
					jsonOutput += ",\"weekendNetRate\":\"" + (accommodationss.getWeekendNetRate() == null?0 : (int)Math.round(accommodationss.getWeekendNetRate()))+ "\"";
					jsonOutput += ",\"weekdayBaseAmount\":\"" + (accommodationss.getWeekdayBaseAmount() == null?0 : (int)Math.round(accommodationss.getWeekdayBaseAmount()))+ "\"";
					jsonOutput += ",\"weekendBaseAmount\":\"" + (accommodationss.getWeekendBaseAmount() == null?0 : (int)Math.round(accommodationss.getWeekendBaseAmount()))+ "\"";
					jsonOutput += ",\"weekdayTariff\":\"" + (accommodationss.getWeekdayTariff() == null?0 : (int)Math.round(accommodationss.getWeekdayTariff()))+ "\"";
					jsonOutput += ",\"weekendTariff\":\"" +( accommodationss.getWeekendTariff() == null?0 :  (int)Math.round(accommodationss.getWeekendTariff()))+ "\"";
					jsonOutput += ",\"weekdayTax\":\"" + (accommodationss.getWeekdayTax() == null?0 : (int)Math.round(accommodationss.getWeekdayTax()))+ "\"";
					jsonOutput += ",\"weekdendTax\":\"" + (accommodationss.getWeekendTax() == null?0 : (int)Math.round(accommodationss.getWeekendTax()))+ "\"";
					jsonOutput += ",\"weekdaySelling\":\"" + (accommodationss.getWeekdaySellingRate() == null?0 : (int)Math.round(accommodationss.getWeekdaySellingRate()))+ "\"";
					jsonOutput += ",\"weekendSelling\":\"" + (accommodationss.getWeekendSellingRate() == null?0 : (int)Math.round(accommodationss.getWeekendSellingRate()))+ "\"";
					jsonOutput += ",\"extraChild\":\"" + (accommodationss.getExtraChild() == null?0 : (int)Math.round(accommodationss.getExtraChild()))+ "\"";
					jsonOutput += ",\"extraAdult\":\"" + (accommodationss.getExtraAdult() == null?0 : (int)Math.round(accommodationss.getExtraAdult()))+ "\"";
					Double extraAdult,extraChild;
					double dblextraadult,dblextrachild;
					extraAdult=accommodationss.getExtraAdult()==null?0:accommodationss.getExtraAdult();
					extraChild=accommodationss.getExtraChild()==null?0:accommodationss.getExtraChild();
					
					dblextraadult=extraAdult+extraAdult*15/100;
					dblextrachild=extraChild+extraChild*15/100;
					jsonOutput += ",\"sellingChild\":\"" + (Math.round(dblextrachild))+ "\"";
					jsonOutput += ",\"sellingAdult\":\"" + (Math.round(dblextraadult))+ "\"";
					
					jsonOutput += ",\"propertyCommission\":\"" + (accommodationss.getAccommodationCommission() == null?0 : (int)Math.round(accommodationss.getAccommodationCommission()))+ "\"";
				}

				}
				}else{
					jsonOutput += ",\"baseAmount\":\"" + 0 + "\"";
					jsonOutput += ",\"netRate\":\"" + 0 + "\"";
					jsonOutput += ",\"taxType\":\"" + 0 + "\"";
					jsonOutput += ",\"tariff\":\"" + 0+ "\"";
					jsonOutput += ",\"tax\":\"" + 0+ "\"";
					jsonOutput += ",\"sellingRate\":\"" + 0+ "\"";
					jsonOutput += ",\"purchaseRate\":\"" + 0+ "\"";
					jsonOutput += ",\"taxType\":\"" + 0+ "\"";
					jsonOutput += ",\"weekdayPurchase\":\"" + 0 + "\"";
					jsonOutput += ",\"weekendPurchase\":\"" + 0+ "\"";
					jsonOutput += ",\"weekdayNetRate\":\"" + 0+ "\"";
					jsonOutput += ",\"weekendNetRate\":\"" + 0+ "\"";
					jsonOutput += ",\"weekdayBaseAmount\":\"" + 0+ "\"";
					jsonOutput += ",\"weekendBaseAmount\":\"" +0+ "\"";
					jsonOutput += ",\"weekdayTariff\":\"" + 0+ "\"";
					jsonOutput += ",\"weekendTariff\":\"" + 0+ "\"";
					jsonOutput += ",\"weekdayTax\":\"" + 0+ "\"";
					jsonOutput += ",\"weekdendTax\":\"" + 0+ "\"";
					jsonOutput += ",\"weekdaySelling\":\"" + 0+ "\"";
					jsonOutput += ",\"weekendSelling\":\"" + 0+ "\"";
					jsonOutput += ",\"extraChild\":\"" + 0+ "\"";
					jsonOutput += ",\"extraAdult\":\"" + 0+ "\"";
					jsonOutput += ",\"sellingChild\":\"" + 0+ "\"";
					jsonOutput += ",\"sellingAdult\":\"" + 0+ "\"";
				}
				jsonOutput += "}";


			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}

	
	public String getPartnerAccommodations() throws IOException {
		try {
			
			this.partnerPropertyId=(Integer)sessionMap.get("partnerPropertyId");
 			this.selectPartnerPropertyId=(Integer)sessionMap.get("selectedPropertyId");
 			if(this.selectPartnerPropertyId!=null){
 				this.propertyId=this.selectPartnerPropertyId;	
 			}else{
 				this.propertyId=this.partnerPropertyId;
 			}
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
		
			
			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
			PropertyAccommodationRoomManager accommodationRoomController = new PropertyAccommodationRoomManager();
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");

			this.accommodationList = accommodationController.list(this.propertyId);
			for (PropertyAccommodation accommodation : accommodationList) {

				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				
				//AccommodationRoom accommodationRooms = accommodationRoomController.findCount(this.propertyId,accommodation.getAccommodationId());
				jsonOutput += "\"accommodationId\":\"" + accommodation.getAccommodationId() + "\"";
				jsonOutput += ",\"abbreviation\":\"" + accommodation.getAbbreviation()+ "\"";
				jsonOutput += ",\"accommodationType\":\"" + accommodation.getAccommodationType()+ "\"";
				jsonOutput += ",\"noOfUnits\":\"" + accommodation.getNoOfUnits()+ "\"";
				jsonOutput += ",\"minOccupancy\":\"" + accommodation.getMinOccupancy()+ "\"";
				jsonOutput += ",\"maxOccupancy\":\"" + accommodation.getMaxOccupancy()+ "\"";
				jsonOutput += ",\"noOfAdults\":\"" + accommodation.getNoOfAdults()+ "\"";
				jsonOutput += ",\"noOfChild\":\"" + accommodation.getNoOfChild()+ "\"";
				jsonOutput += ",\"baseAmount\":\"" + accommodation.getBaseAmount()+ "\"";
				jsonOutput += ",\"extraChild\":\"" + accommodation.getExtraChild()+ "\"";
				jsonOutput += ",\"extraAdult\":\"" + accommodation.getExtraAdult()+ "\"";
				jsonOutput += ",\"extraInfant\":\"" + accommodation.getExtraInfant()+ "\"";
				jsonOutput += ",\"description\":\"" + accommodation.getAccommodationDescription()+ "\"";
				jsonOutput += "}";


			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	public String getPropertyThumb() {
		
		try {
			
			//this.propertyId = (Integer) sessionMap.get("propertyId");
			HttpServletResponse response = ServletActionContext.getResponse();

			PmsPropertyManager propertyController = new PmsPropertyManager();
			PmsProperty property = propertyController.find(getPropertyId());
			String imagePath = "";
			// response.setContentType("");
			
			if ( property.getPropertyThumbPath() != null) {
				imagePath = getText("storage.propertythumb.photo") + "/"
						+ property.getPropertyId()+ "/"
						+ property.getPropertyThumbPath();
				Image img = new Image();
				response.getOutputStream().write(
						img.getCustomImageInBytes(imagePath));
				response.getOutputStream().flush();
			} else {
				imagePath = getText("storage.propertythumb.photo") + "/emptyprofilepic.png";
				Image img = new Image();
				response.getOutputStream().write(
						img.getCustomImageInBytes(imagePath));
				response.getOutputStream().flush();
			}
		} catch (Exception e1) {
			logger.error(e1);
			e1.printStackTrace();
		} finally {

		}
		return null;

	}

	
	public String accommodationUpdatePicture() throws IOException {
		try {

			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();

			File fileZip1 = new File(getText("storage.accomodation.photo") + "/"
					+ getPropertyAccommodationId() + "/", this.getMyFileFileName());
			FileUtils.copyFile(this.myFile, fileZip1);// copying image in the
			// new file
			PropertyAccommodation propaccom = accommodationController.find(getPropertyAccommodationId()); 
			
			
			propaccom.setAccommodationImgPath(this.getMyFileFileName());
			//u.setProfilePicName(this.getMyFileFileName());
			accommodationController.edit(propaccom);
			this.propertyaccommodation = getModel();
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return SUCCESS;
	}

	
	
	
	public String getAccommodationPic() {
		
		
		try {
			HttpServletResponse response = ServletActionContext.getResponse();
			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();

			String imagePath = "";
			// response.setContentType("");
			
			PropertyAccommodation propacco = accommodationController.find(getPropertyAccommodationId()); 
			if ( propacco.getAccommodationImgPath() != null) {
				imagePath = getText("storage.accomodation.photo") + "/"
						+propacco.getAccommodationId() + "/"
						+ propacco.getAccommodationImgPath();
				Image im = new Image();
				response.getOutputStream().write(im.getCustomImageInBytes(imagePath));
				response.getOutputStream().flush();
			} else {
				imagePath = getText("storage.accomodation.photo") + "/emptyprofilepic.png";
				Image im = new Image();
				response.getOutputStream().write(
						im.getCustomImageInBytes(imagePath));
				response.getOutputStream().flush();
			}
		}catch (Exception e1) {
			logger.error(e1);
			e1.printStackTrace();
		} finally {

		}
		return null;

	}
	
	
	public String addAccommodation() {
		try {
			
			this.propertyId = (Integer) sessionMap.get("propertyId");
			
			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
			PropertyContractTypeManager contractController = new PropertyContractTypeManager();
			//UserLoginManager  userController = new UserLoginManager();
			
			PmsPropertyManager propertyController = new PmsPropertyManager();
			
						
			PropertyAccommodation accommodation = new PropertyAccommodation();
			PropertyAccommodationRoomAction accommodationRoom = new PropertyAccommodationRoomAction();
			
			Integer userId=(Integer) sessionMap.get("userId");
			long time = System.currentTimeMillis();
			java.util.Date date = new java.util.Date(time);
			Timestamp tsDate=new Timestamp(date.getTime());  
			
			//accommodation.setAbbreviation(getAbbreviation());
			//accommodation.setAccommodationDescription(getDescription());
			//accommodation.setBaseAmount(getBaseAmount());
//			accommodation.setExtraAdult(getExtraAdult());
//			accommodation.setExtraChild(getExtraChild());
//			accommodation.setExtraInfant(getExtraInfant());
			
			accommodation.setAccommodationType(getAccommodationType());
			accommodation.setNoOfAdults(getNoOfAdults());
			accommodation.setNoOfChild(getNoOfChild());
			accommodation.setIsActive(true);
			accommodation.setIsDeleted(false);
			accommodation.setCreatedBy(userId);
			accommodation.setCreatedDate(tsDate);
			accommodation.setMinOccupancy(getMinOccupancy());
			accommodation.setMaxOccupancy(getMaxOccupancy());
			accommodation.setNoOfUnits(getNoOfUnits());
			PropertyContractType contract = contractController.find(getPropertyContractTypeId());
			accommodation.setPropertyContractType(contract);
			PmsProperty property = propertyController.find(getPropertyId());
			accommodation.setPmsProperty(property);
			//accommodationController.add(accommodation);
			PropertyAccommodation accom = accommodationController.add(accommodation);
	
			this.propertyAccommodationId = accom.getAccommodationId();
			
			for(int i=1; i<=getNoOfUnits(); i++){
			
				accommodationRoom.addAccommodationRoom(this.propertyAccommodationId, "200", getAbbreviation()+i,userId);
				 
			}
		    
			
			//sessionMap.put("propertyAccommodationId",propertyAccommodationId);
			
			//this.propertyAccommodationId = (Integer) sessionMap.get("propertyAccommodationId");
			
			
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("application/json");
			
			  if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"accommodationId\":\"" + this.propertyAccommodationId  + "\"";
				
				jsonOutput += "}";


			

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			 
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}
	
	public String editAccommodation() {
		try {
			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
			PropertyAccommodationRoomManager roomController = new PropertyAccommodationRoomManager();
			UserLoginManager  userController = new UserLoginManager();
			PropertyAccommodationRoomAction accommodationRoom = new PropertyAccommodationRoomAction();			
			PropertyAccommodation accommodation = accommodationController.find(getPropertyAccommodationId());
			PropertyContractTypeManager contractController = new PropertyContractTypeManager();
			
			Integer userId=(Integer) sessionMap.get("userId");
			long time = System.currentTimeMillis();
			java.util.Date date = new java.util.Date(time);
			Timestamp tsDate=new Timestamp(date.getTime());  
			
			accommodation.setAccommodationType(getAccommodationType());
			accommodation.setNoOfAdults(getNoOfAdults());
			accommodation.setNoOfChild(getNoOfChild());
			accommodation.setIsActive(true);
			accommodation.setIsDeleted(false);
			accommodation.setModifiedBy(userId);
			accommodation.setModifiedDate(tsDate);
			accommodation.setMinOccupancy(getMinOccupancy());
			accommodation.setMaxOccupancy(getMaxOccupancy());
			accommodation.setNoOfUnits(getNoOfUnits());
			PropertyContractType contract = contractController.find(getPropertyContractTypeId());
			accommodation.setPropertyContractType(contract);
			accommodationController.edit(accommodation);
			
			this.accommodationroomList = roomController.list1(getPropertyAccommodationId());
			for(PropertyAccommodationRoom room : accommodationroomList){
				room.setIsActive(false);
				room.setIsDeleted(true);
				room.setModifiedBy(userId);
				room.setModifiedDate(tsDate);
				roomController.edit(room);
			}
			
			for(int i=1; i<=getNoOfUnits(); i++){
				
				accommodationRoom.addAccommodationRoom(getPropertyAccommodationId(), "200", getAbbreviation()+i,userId);
				 
			}
		    
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return SUCCESS;
	}
	
	public String deleteAccommodation() {
		
		try {
			
			this.propertyId = (Integer) sessionMap.get("propertyId");
			
			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
			
			PropertyAccommodationRoomManager accommodationRoomController = new PropertyAccommodationRoomManager();
			UserLoginManager  userController = new UserLoginManager();			
			
			
			PropertyAccommodation accommodation = accommodationController.find(getPropertyAccommodationId());
			
			
			Integer userId=(Integer) sessionMap.get("userId");
			long time = System.currentTimeMillis();
			java.util.Date date = new java.util.Date(time);
			Timestamp tsDate=new Timestamp(date.getTime());  
			
			accommodation.setIsActive(false);
			accommodation.setIsDeleted(true);
			accommodation.setModifiedBy(userId);
			accommodation.setModifiedDate(tsDate);
			accommodation.setModifiedDate(new java.sql.Timestamp(date.getTime()));
			accommodationController.edit(accommodation);
			
			
			this.accommodationroomList = accommodationRoomController.list1(getPropertyAccommodationId());
			
			for(PropertyAccommodationRoom accommodationRooms : accommodationroomList){
				
			accommodationRooms.setIsActive(false);
			accommodationRooms.setIsDeleted(true);
			accommodationRooms.setModifiedBy(userId);
			accommodationRooms.setModifiedDate(tsDate);
			accommodationRoomController.edit(accommodationRooms);
			
			}
			
			
			
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}
	
	public String getPropertyAccommodationRate() throws IOException {
		try {
			
			this.propertyId = (Integer) sessionMap.get("propertyId");
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			PmsPropertyManager propertyController=new PmsPropertyManager();
			
			
			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
			PropertyAccommodationRoomManager accommodationRoomController = new PropertyAccommodationRoomManager();

			response.setContentType("application/json");
                                        
			List<PropertyAccommodation> accommodations = accommodationController.listId(getPropertyAccommodationId(),getPropertyContractTypeId());
			for(PropertyAccommodation accommodation : accommodations){
			
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"baseAmount\":\"" + (accommodation.getBaseAmount() == 0?0 : accommodation.getBaseAmount()) + "\"";
				jsonOutput += ",\"netRate\":\"" + accommodation.getNetRate()+ "\"";
				jsonOutput += ",\"taxType\":\"" + accommodation.getTaxType()+ "\"";
				jsonOutput += ",\"tariff\":\"" + accommodation.getTariff()+ "\"";
				jsonOutput += ",\"tax\":\"" + accommodation.getTax()+ "\"";
				jsonOutput += ",\"sellingRate\":\"" + accommodation.getSellingRate()+ "\"";
				jsonOutput += ",\"purchaseRate\":\"" + accommodation.getPurchaseRate()+ "\"";
				jsonOutput += ",\"extraChild\":\"" + accommodation.getExtraChild()+ "\"";
				jsonOutput += ",\"extraAdult\":\"" + accommodation.getExtraAdult()+ "\"";
				jsonOutput += ",\"propertyContractType\":\"" + (accommodation.getPropertyContractType()==null?0:accommodation.getPropertyContractType().getContractTypeName())+ "\"";
				jsonOutput += ",\"propertyContractId\":\"" + (accommodation.getPropertyContractType()==null?0:accommodation.getPropertyContractType().getContractTypeId())+ "\"";
			
				jsonOutput += "}";

			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	}
	
	public String addPropertyAccommodationRate() {
		try {
			
			this.propertyId = (Integer) sessionMap.get("propertyId");
			
			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
			PropertyContractTypeManager contractController = new PropertyContractTypeManager();
			//UserLoginManager  userController = new UserLoginManager();
			
			PmsPropertyManager propertyController = new PmsPropertyManager();
						
			List<PropertyAccommodation> accommodations = accommodationController.listId(getPropertyAccommodationId(),getPropertyContractTypeId());
			Integer userId=(Integer) sessionMap.get("userId");
			long time = System.currentTimeMillis();
			java.util.Date date = new java.util.Date(time);
			Timestamp tsDate=new Timestamp(date.getTime()); 
			for(PropertyAccommodation accommodation : accommodations){
				if(getLocationTypeId() == 1){
			if(propertyContractTypeId == 7){
			
				accommodation.setNetRate(getNetRate());
				accommodation.setTaxType(getTaxType());
				accommodation.setTariff(getTariff());
				accommodation.setTax(getTax());
				accommodation.setSellingRate(getSellingRate());
				accommodation.setExtraAdult(getExtraAdult());
				accommodation.setExtraChild(getExtraChild());
				accommodationController.edit(accommodation);
			}else if(propertyContractTypeId == 6){
				
				accommodation.setPurchaseRate(getPurchaseRate());
				accommodation.setTaxType(getTaxType());
				accommodation.setTax(getTax());
				accommodation.setTariff(getTariff());
				accommodation.setSellingRate(getSellingRate());
				accommodation.setExtraAdult(getExtraAdult());
				accommodation.setExtraChild(getExtraChild());
				accommodationController.edit(accommodation);
			}else if(propertyContractTypeId == 9){
				
				accommodation.setBaseAmount(getBaseAmount());
				accommodation.setTaxType(getTaxType());
				accommodation.setTax(getTax());
				accommodation.setAccommodationCommission(getPropertyCommission());
				accommodation.setTariff(getTariff());
				accommodation.setSellingRate(getSellingRate());
				accommodation.setExtraAdult(getExtraAdult());
				accommodation.setExtraChild(getExtraChild());
				accommodationController.edit(accommodation);
			}
			}
				else if(getLocationTypeId() == 2){
					if(propertyContractTypeId == 7){
						
						accommodation.setWeekdayNetRate(getWeekdayNetRate());
						accommodation.setWeekendNetRate(getWeekendNetRate());
						accommodation.setWeekdayTariff(getWeekdayTariff());
						accommodation.setWeekendTariff(getWeekendTariff());
						accommodation.setWeekdayTax(getWeekdayTax());
						accommodation.setWeekendTax(getWeekendTax());
						accommodation.setWeekdaySellingRate(getWeekdaySellingRate());
						accommodation.setWeekendSellingRate(getWeekendSellingRate());
						accommodation.setTaxType(getTaxType());
						accommodation.setExtraAdult(getExtraAdult());
						accommodation.setExtraChild(getExtraChild());
						accommodationController.edit(accommodation);
					}else if(propertyContractTypeId == 6){
						
						accommodation.setWeekdayPurchaseRate(getWeekdayPurchaseRate());
						accommodation.setWeekendPurchaseRate(getWeekendPurchaseRate());
						accommodation.setWeekdayTariff(getWeekdayTariff());
						accommodation.setWeekendTariff(getWeekendTariff());
						accommodation.setWeekdayTax(getWeekdayTax());
						accommodation.setWeekendTax(getWeekendTax());
						accommodation.setWeekdaySellingRate(getWeekdaySellingRate());
						accommodation.setWeekendSellingRate(getWeekendSellingRate());
						accommodation.setTaxType(getTaxType());
						accommodation.setExtraAdult(getExtraAdult());
						accommodation.setExtraChild(getExtraChild());
						accommodationController.edit(accommodation);
					}else if(propertyContractTypeId == 9){
						
						accommodation.setWeekdayBaseAmount(getWeekdayBaseAmount());
						accommodation.setWeekendBaseAmount(getWeekendBaseAmount());
						accommodation.setWeekdayTariff(getWeekdayTariff());
						accommodation.setWeekendTariff(getWeekendTariff());
						accommodation.setWeekdayTax(getWeekdayTax());
						accommodation.setWeekendTax(getWeekendTax());
						accommodation.setAccommodationCommission(getPropertyCommission());
						accommodation.setWeekdaySellingRate(getWeekdaySellingRate());
						accommodation.setWeekendSellingRate(getWeekendSellingRate());
						accommodation.setTaxType(getTaxType());
						accommodation.setExtraAdult(getExtraAdult());
						accommodation.setExtraChild(getExtraChild());
						accommodationController.edit(accommodation);
					}
				}
			}
			
			
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
		return null;
	}
	
	@SuppressWarnings("unused")
	public String addBlockRoomInventory() {
			
			String strReturn = "";
			
			try {
				Integer userId=(Integer) sessionMap.get("userId");
				long time = System.currentTimeMillis();
				java.util.Date date = new java.util.Date(time);
				Timestamp tsDate=new Timestamp(date.getTime());  
				
				this.propertyId = (Integer) sessionMap.get("propertyId");
				
				PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
				PropertyAccommodationInventoryManager accommodationInventoryController = new PropertyAccommodationInventoryManager();
				OtaHotelsManager otaHotelController = new OtaHotelsManager();
				/*PropertyAccommodationRoomManager accommodationRoomController = new PropertyAccommodationRoomManager();
				UserLoginManager  userController = new UserLoginManager();*/
				
				DateFormat format = new SimpleDateFormat("MMM d,yyyy" , Locale.ENGLISH);
			    java.util.Date dateStart = format.parse(getStartBlockDate());
			    java.util.Date dateEnd = format.parse(getEndBlockDate());
			    
			    Calendar calCheckStart=Calendar.getInstance();
			    calCheckStart.setTime(dateStart);
			    java.util.Date checkInDate = calCheckStart.getTime();
			   
			    
			    Calendar calCheckEnd=Calendar.getInstance();
			    calCheckEnd.setTime(dateEnd);
			    java.util.Date checkOutDate = calCheckEnd.getTime();
			    
			    String startDate = new SimpleDateFormat("yyyy-MM-dd").format(checkInDate);
	            String endDate = new SimpleDateFormat("yyyy-MM-dd").format(checkOutDate);
	            
	            DateTime start = DateTime.parse(startDate);
	            DateTime end = DateTime.parse(endDate);
			    
			
		        
			    PropertyAccommodation propertyAccommodation1 = accommodationController.find(getPropertyAccommodationId());
			    int roomCount = Integer.parseInt(getInventoryCount());
			    if(propertyAccommodation1.getNoOfUnits() >= roomCount){
			    	OtaHotelDetails otaHotelDetails = otaHotelController.getOtaHotelDetails(getPropertyAccommodationId(),1);
				
					  if(otaHotelDetails != null){

					    	int sourceId = 3;
					    	OtaHotels otaHotel = otaHotelController.getOtaHotels(getPropertyId(), sourceId);
					    	DefaultHttpClient httpClient = new DefaultHttpClient();
							HttpPost postRequest = new HttpPost("https://in.goibibo.com/api/chmv2/updateroominventory/?bearer_token="+otaHotel.getOtaBearerToken()+"&channel_token="+otaHotel.getOtaChannelToken()+"");
								String xmlBody = "<?xml version='1.0' encoding='UTF-8'?>"+
									    "<Website Name='ingoibibo' HotelCode='"+otaHotelDetails.getOtaHotelCode()+"' Version='1'>"+
									            "<Room>"+
								            "<RoomTypeCode>"+ otaHotelDetails.getOtaRoomTypeId() +"</RoomTypeCode>"+
									            "<StartDate Format='yyyy-mm-dd'>"+startDate+"</StartDate>"+
									            "<EndDate Format='yyyy-mm-dd'>"+endDate+"</EndDate>"+						            
									            "<DaysOfWeek Mon='"+"true"+"' Tue='"+"true"+"' Wed='"+"true"+"' Thu='"+"true"+"' Fri='"+"true"+"' Sat='"+"true"+"' Sun='"+"true"+"'></DaysOfWeek>"+
//									            "<MinLOS>"+"1"+"</MinLOS>"+
//									            "<CutOff>"+"1h"+"</CutOff>"+
									            "<Available> "+ getInventoryCount() +"</Available>"+
									            "<StopSell>"+"False"+"</StopSell>"+
									            "</Room>"+
									      "</Website>";
								
							    
								
							    
								StringEntity input = new StringEntity(xmlBody);
								input.setContentType("text/xml");
								postRequest.setEntity(input);
								  
								HttpResponse httpResponse = httpClient.execute(postRequest);
								HttpEntity entity = httpResponse.getEntity();
				                // Read the contents of an entity and return it as a String.
					            String content = EntityUtils.toString(entity);
					    	
					  }
				 
	            List<DateTime> between = getDateRange(start,end);
	            
	            for (DateTime d : between)
	            {
	           
	             java.util.Date bookingDate=  d.toDate();
	             PropertyAccommodationInventory inventoryCount = accommodationInventoryController.findId(bookingDate,getPropertyAccommodationId());
	             if(inventoryCount != null){
					 PropertyAccommodationInventory inventory1 = accommodationInventoryController.find(inventoryCount.getInventoryId());	 
		             inventory1.setIsActive(false);
		             inventory1.setIsDeleted(true);
		             inventory1.setCreatedBy(userId);
		             inventory1.setCreatedDate(tsDate);
		             accommodationInventoryController.edit(inventory1);
		             
		             PropertyAccommodationInventory inventory2 = new PropertyAccommodationInventory();
		             inventory2.setIsActive(true);
		             inventory2.setIsDeleted(false);
		             inventory2.setCreatedBy(userId);
		             inventory2.setCreatedDate(tsDate);
		             inventory2.setInventoryRemarks(getInventoryRemarks());
		             inventory2.setBlockedDate(bookingDate);	 
		             inventory2.setInventoryCount(getInventoryCount());	
		             PropertyAccommodation propertyAccommodation = accommodationController.find(getPropertyAccommodationId());
	                 inventory2.setPropertyAccommodation(propertyAccommodation);
		             accommodationInventoryController.add(inventory2);
				 }
	             else{
	                 PropertyAccommodationInventory inventory = new PropertyAccommodationInventory();
	                 inventory.setInventoryCount(getInventoryCount());
	                 inventory.setBlockedDate(bookingDate);	                 
	                 inventory.setIsActive(true);
	                 inventory.setIsDeleted(false);
	                 inventory.setCreatedBy(userId);
	                 inventory.setCreatedDate(tsDate);
	                 inventory.setInventoryRemarks(getInventoryRemarks());
	                 PropertyAccommodation propertyAccommodation = accommodationController.find(getPropertyAccommodationId());
	                 inventory.setPropertyAccommodation(propertyAccommodation);
	                 accommodationInventoryController.add(inventory);
	    			 }
	             strReturn = null;
	            }
	            
			    }			   
			    
			} catch (Exception e) {                  
				logger.error(e);	
				strReturn="error";
				e.printStackTrace();
			} finally {				
				
			}
			return strReturn;
		}	
	
	@SuppressWarnings("unused")
	public String addPartnerBlockRoomInventory() {
			
			String strReturn = "";
			
			try {
				DateFormat f = new SimpleDateFormat("EEEE");
				Integer userId=(Integer) sessionMap.get("partnerUserId");
				long time = System.currentTimeMillis();
				java.util.Date date = new java.util.Date(time);
				
				Calendar calDate=Calendar.getInstance();
            	calDate.setTime(date);
            	calDate.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
            	java.util.Date dtDate=calDate.getTime();
            	Timestamp tsDate=new Timestamp(dtDate.getTime());
            	int bookedHours=calDate.get(Calendar.HOUR);
            	int bookedMinute=calDate.get(Calendar.MINUTE);
            	int bookedSecond=calDate.get(Calendar.SECOND);
            	int AMPM=calDate.get(Calendar.AM_PM);
            	
            	String strBookingDate=new SimpleDateFormat("MMM d, yyyy", Locale.ENGLISH).format(calDate.getTime());
            	String hours="",minutes="",seconds="";
            	if(bookedHours<10){
            		hours=String.valueOf(bookedHours);
            		hours="0"+hours;
            	}else{
            		hours=String.valueOf(bookedHours);
            	}
            	if(bookedMinute<10){
            		minutes=String.valueOf(bookedMinute);
            		minutes="0"+minutes;
            	}else{
            		minutes=String.valueOf(bookedMinute);
            	}
            	
            	if(bookedMinute<10){
            		seconds=String.valueOf(bookedSecond);
            		seconds="0"+seconds;
            	}else{
            		seconds=String.valueOf(bookedSecond);
            	}
            	if(AMPM==0){//If the current time is AM
            		this.timeHours=f.format(calDate.getTime())+","+strBookingDate+" at "+hours+" : "+minutes+" AM ";
            	}else if(AMPM==1){//If the current time is PM
            		this.timeHours=f.format(calDate.getTime())+","+strBookingDate+" at "+hours+" : "+minutes+" PM ";
            	}  
				 this.partnerPropertyId=(Integer)sessionMap.get("partnerPropertyId");
			 		this.selectPartnerPropertyId=(Integer)sessionMap.get("selectedPropertyId");
					if(this.selectPartnerPropertyId!=null){
						this.partnerPropertyId=this.selectPartnerPropertyId;	
					}else{
						this.partnerPropertyId=this.partnerPropertyId;
					}
				
				this.propertyId = this.partnerPropertyId;
				
				PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
				PropertyAccommodationInventoryManager accommodationInventoryController = new PropertyAccommodationInventoryManager();
				OtaHotelsManager otaHotelController = new OtaHotelsManager();
				/*PropertyAccommodationRoomManager accommodationRoomController = new PropertyAccommodationRoomManager();
				UserLoginManager  userController = new UserLoginManager();*/
				
				DateFormat format = new SimpleDateFormat("MMM d,yyyy" , Locale.ENGLISH);
			    java.util.Date dateStart = format.parse(getStartBlockDate());
			    java.util.Date dateEnd = format.parse(getEndBlockDate());
			    
			    Calendar calCheckStart=Calendar.getInstance();
			    calCheckStart.setTime(dateStart);
			    java.util.Date checkInDate = calCheckStart.getTime();
			   
			    
			    Calendar calCheckEnd=Calendar.getInstance();
			    calCheckEnd.setTime(dateEnd);
			    java.util.Date checkOutDate = calCheckEnd.getTime();
			    
			    String startDate = new SimpleDateFormat("yyyy-MM-dd").format(checkInDate);
	            String endDate = new SimpleDateFormat("yyyy-MM-dd").format(checkOutDate);
	            
	            DateTime start = DateTime.parse(startDate);
	            DateTime end = DateTime.parse(endDate);
			    
			
		        
			    PropertyAccommodation propertyAccommodation1 = accommodationController.find(getPropertyAccommodationId());
			    int roomCount = Integer.parseInt(getInventoryCount());
			    if(propertyAccommodation1.getNoOfUnits() >= roomCount){
			    	OtaHotelDetails otaHotelDetails = otaHotelController.getOtaHotelDetails(getPropertyAccommodationId(),1);
			    	if(otaHotelDetails != null){
			    	int sourceId = 3;
			    	OtaHotels otaHotel = otaHotelController.getOtaHotels(getPropertyId(), sourceId);
			    	DefaultHttpClient httpClient = new DefaultHttpClient();
					HttpPost postRequest = new HttpPost("https://in.goibibo.com/api/chmv2/updateroominventory/?bearer_token="+otaHotel.getOtaBearerToken()+"&channel_token="+otaHotel.getOtaChannelToken()+"");
						String xmlBody = "<?xml version='1.0' encoding='UTF-8'?>"+
							    "<Website Name='ingoibibo' HotelCode='"+otaHotelDetails.getOtaHotelCode()+"' Version='1'>"+
							            "<Room>"+
						            "<RoomTypeCode>"+ otaHotelDetails.getOtaRoomTypeId() +"</RoomTypeCode>"+
							            "<StartDate Format='yyyy-mm-dd'>"+startDate+"</StartDate>"+
							            "<EndDate Format='yyyy-mm-dd'>"+endDate+"</EndDate>"+						            
							            "<DaysOfWeek Mon='"+"true"+"' Tue='"+"true"+"' Wed='"+"true"+"' Thu='"+"true"+"' Fri='"+"true"+"' Sat='"+"true"+"' Sun='"+"true"+"'></DaysOfWeek>"+
//							            "<MinLOS>"+"1"+"</MinLOS>"+
//							            "<CutOff>"+"1h"+"</CutOff>"+
							            "<Available> "+ getInventoryCount() +"</Available>"+
							            "<StopSell>"+"False"+"</StopSell>"+
							            "</Room>"+
							      "</Website>";
						
					    
						
					    
						StringEntity input = new StringEntity(xmlBody);
						input.setContentType("text/xml");
						postRequest.setEntity(input);
						  
						HttpResponse httpResponse = httpClient.execute(postRequest);
						HttpEntity entity = httpResponse.getEntity();
		                // Read the contents of an entity and return it as a String.
			            String content = EntityUtils.toString(entity);
			    	}
	            List<DateTime> between = getDateRange(start,end);
	            
	            for (DateTime d : between)
	            {
	           
	             java.util.Date bookingDate=  d.toDate();
	             PropertyAccommodationInventory inventoryCount = accommodationInventoryController.findId(bookingDate,getPropertyAccommodationId());
	             if(inventoryCount != null){
					 PropertyAccommodationInventory inventory1 = accommodationInventoryController.find(inventoryCount.getInventoryId());	 
		             inventory1.setIsActive(false);
		             inventory1.setIsDeleted(true);
		             inventory1.setCreatedBy(userId);
		             inventory1.setCreatedDate(tsDate);
		             accommodationInventoryController.edit(inventory1);
		             
		             PropertyAccommodationInventory inventory2 = new PropertyAccommodationInventory();
		             inventory2.setIsActive(true);
		             inventory2.setIsDeleted(false);
		             inventory2.setCreatedBy(userId);
		             inventory2.setCreatedDate(tsDate);
		             inventory2.setInventoryRemarks(getInventoryRemarks());
		             inventory2.setBlockedDate(bookingDate);	 
		             inventory2.setInventoryCount(getInventoryCount());	
		             PropertyAccommodation propertyAccommodation = accommodationController.find(getPropertyAccommodationId());
	                 inventory2.setPropertyAccommodation(propertyAccommodation);
		             accommodationInventoryController.add(inventory2);
				 }
	             else{
	                 PropertyAccommodationInventory inventory = new PropertyAccommodationInventory();
	                 inventory.setInventoryCount(getInventoryCount());
	                 inventory.setBlockedDate(bookingDate);	                 
	                 inventory.setIsActive(true);
	                 inventory.setIsDeleted(false);
	                 inventory.setCreatedBy(userId);
	                 inventory.setCreatedDate(tsDate);
	                 inventory.setInventoryRemarks(getInventoryRemarks());
	                 PropertyAccommodation propertyAccommodation = accommodationController.find(getPropertyAccommodationId());
	                 inventory.setPropertyAccommodation(propertyAccommodation);
	                 accommodationInventoryController.add(inventory);
	    			 }
	             strReturn = null;
	            }
	            
	            Configuration cfg = new Configuration();
 				cfg.setClassForTemplateLoading(BookingDetailAction.class, "../../../");
 				Template template = cfg.getTemplate(getText("partnerbulkblock.template"));
 				Map<String, Object> rootMap = new HashMap<String, Object>();
 				PmsPropertyManager propertyController=new PmsPropertyManager();
 				UserLoginManager userController=new UserLoginManager();
 				PmsProperty property=propertyController.find(this.propertyId);	
 				User user = userController.find(userId);
 				 PropertyAccommodation propertyAccommodation = accommodationController.find(getPropertyAccommodationId());
 				
 				rootMap.put("propertyName",property.getPropertyName());
 				rootMap.put("createdBy",user.getUserName());
 				rootMap.put("createdDate",tsDate);
 				rootMap.put("blockReason",getInventoryRemarks());
 				//rootMap.put("address",property.getAddress1()); 
 				rootMap.put("startDate",startDate); 
 				rootMap.put("endDate",endDate); 
 				rootMap.put("available",roomCount);
 				rootMap.put("totalRooms",propertyAccommodation.getNoOfUnits());
 				rootMap.put("accommodationType",propertyAccommodation.getAccommodationType());
 				
 				
				rootMap.put("from", getText("notification.from"));
 				Writer out = new StringWriter();
 				template.process(rootMap, out);


 				String strSubject="Blocking Rooms In "+property.getPropertyName();
 				//send email
 				Email em = new Email();
 				em.set_to(user.getEmailId());  /* property.getContractManagerEmail()*/
 				em.set_cc(getText("bookings.notification.email"));
 				em.set_cc2(getText("operations.notification.email"));
 				em.set_cc3(getText("finance.notification.email"));
 				em.set_cc4("revenue@ulohotels.com");
 				em.set_cc5("reservations@ulohotels.com");
 				em.set_cc6("bookings@ulohotels.com");
 				em.set_cc7("payments@ulohotels.com");
 				em.set_cc8(property.getContractManagerEmail());
 				em.set_from(getText("email.from"));
 				em.set_username(getText("email.username"));
 				em.set_subject(strSubject);
 				em.set_bodyContent(out);
 				em.sendPartnerBlock();
	            
			    }		
			    
			    
			} catch (Exception e) {                  
				logger.error(e);	
				strReturn="error";
				e.printStackTrace();
			} finally {				
				
			}
			return strReturn;
		}	
	
	
	
	@SuppressWarnings("unused")
	public String singleBlockInventory() {
		String strReturn = "";
			try {
				this.propertyId = (Integer) sessionMap.get("propertyId");
				Integer userId=(Integer) sessionMap.get("userId");
				long time = System.currentTimeMillis();
				java.util.Date date = new java.util.Date(time);
				Timestamp tsDate=new Timestamp(date.getTime());  
				
				//this.propertyId = (Integer) sessionMap.get("propertyId");
				
				PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
				PropertyAccommodationInventoryManager accommodationInventoryController = new PropertyAccommodationInventoryManager();
				OtaHotelsManager otaHotelController = new OtaHotelsManager();
				/*PropertyAccommodationRoomManager accommodationRoomController = new PropertyAccommodationRoomManager();
				UserLoginManager  userController = new UserLoginManager();*/
				
				
				 for (int i = 0; i < array.size(); i++) 
			     {
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																						
				
				
				DateFormat format = new SimpleDateFormat("MMM d,yyyy" , Locale.ENGLISH);
			    java.util.Date dateStart = format.parse(array.get(i).getStartBlockDate());
			    
			  
			    
			    Calendar calCheckStart=Calendar.getInstance();
			    calCheckStart.setTime(dateStart);
			    java.util.Date checkInDate = calCheckStart.getTime();
			   
			    
			    
			    
			    String startDate = new SimpleDateFormat("yyyy-MM-dd").format(checkInDate);
	            DateTime start = DateTime.parse(startDate);
	            java.util.Date bookingDate=  start.toDate();
			    PropertyAccommodation propertyAccommodation1 = accommodationController.find(array.get(i).getPropertyAccommodationId());
			    int roomCount = Integer.parseInt(array.get(i).getInventoryCount());
			    if(propertyAccommodation1.getNoOfUnits() >= roomCount){
			    	OtaHotelDetails otaHotelDetails = otaHotelController.getOtaHotelDetails(array.get(i).getPropertyAccommodationId(),1);
			    	if(otaHotelDetails != null){
			    	int sourceId = 3;
			    	OtaHotels otaHotel = otaHotelController.getOtaHotels(getPropertyId(), sourceId);
			    	DefaultHttpClient httpClient = new DefaultHttpClient();
					HttpPost postRequest = new HttpPost("https://in.goibibo.com/api/chmv2/updateroominventory/?bearer_token="+otaHotel.getOtaBearerToken()+"&channel_token="+otaHotel.getOtaChannelToken()+"");
					String xmlBody = "<?xml version='1.0' encoding='UTF-8' ?>"+
						    "<Website Name='ingoibibo' HotelCode='"+otaHotelDetails.getOtaHotelCode()+"'>"+
						            "<Room>"+
						            "<RoomTypeCode>"+otaHotelDetails.getOtaRoomTypeId()+"</RoomTypeCode>"+
						            "<StartDate Format='yyyy-mm-dd'>"+startDate+"</StartDate>"+
						            "<EndDate Format='yyyy-mm-dd'>"+startDate+"</EndDate>"+						            
						            "<DaysOfWeek Mon='"+"true"+"' Tue='"+"true"+"' Wed='"+"true"+"' Thu='"+"true"+"' Fri='"+"true"+"' Sat='"+"true"+"' Sun='"+"true"+"'></DaysOfWeek>"+
//						            "<MinLOS>"+"1"+"</MinLOS>"+
//						            "<CutOff>"+"1h"+"</CutOff>"+
						            "<Available> "+ roomCount +"</Available>"+
						            "<StopSell>"+"False"+"</StopSell>"+
						            "</Room>"+
						      "</Website>";
					
				    
					
					StringEntity input = new StringEntity(xmlBody);
					input.setContentType("text/xml");
					postRequest.setEntity(input);
					HttpResponse httpResponse = httpClient.execute(postRequest);
					HttpEntity entity = httpResponse.getEntity();
	                // Read the contents of an entity and return it as a String.
		            String content = EntityUtils.toString(entity);
			    	}
	             PropertyAccommodationInventory inventoryCount = accommodationInventoryController.findId(bookingDate,array.get(i).getPropertyAccommodationId());
	             if(inventoryCount != null){
					 PropertyAccommodationInventory inventory1 = accommodationInventoryController.find(inventoryCount.getInventoryId());	 
		             inventory1.setIsActive(false);
		             inventory1.setIsDeleted(true);
		             inventory1.setCreatedBy(userId);
		             inventory1.setCreatedDate(tsDate);
		             accommodationInventoryController.edit(inventory1);   
		             
		             PropertyAccommodationInventory inventory2 = new PropertyAccommodationInventory();
		             inventory2.setIsActive(true);
		             inventory2.setIsDeleted(false);
		             inventory2.setCreatedBy(userId);
		             inventory2.setCreatedDate(tsDate);
		             inventory2.setBlockedDate(bookingDate);
		             inventory2.setInventoryRemarks(getInventoryRemarks());
		             inventory2.setInventoryCount(array.get(i).getInventoryCount());	
		             PropertyAccommodation propertyAccommodation = accommodationController.find(array.get(i).getPropertyAccommodationId());
	                 inventory2.setPropertyAccommodation(propertyAccommodation);
		             accommodationInventoryController.add(inventory2);
				 }
	             else{
	                 PropertyAccommodationInventory inventory = new PropertyAccommodationInventory();
	                 inventory.setInventoryCount(array.get(i).getInventoryCount());
	                 inventory.setBlockedDate(bookingDate);	                 
	                 inventory.setIsActive(true);
	                 inventory.setIsDeleted(false);
	                 inventory.setCreatedBy(userId);
	                 inventory.setCreatedDate(tsDate);
	                 inventory.setInventoryRemarks(getInventoryRemarks());
	                 PropertyAccommodation propertyAccommodation = accommodationController.find(array.get(i).getPropertyAccommodationId());
	                 inventory.setPropertyAccommodation(propertyAccommodation);
	                 accommodationInventoryController.add(inventory);
	    			 }
	             strReturn = null;
	            }
			    
			     }
			} catch (Exception e) {                    
				logger.error(e);
				strReturn="error";
				e.printStackTrace();
			} finally {

			}
			return strReturn;
		}	
	
	@SuppressWarnings("unused")
	public String partnerSingleBlockInventory() {
		String strReturn = "";
			try {
				DateFormat f = new SimpleDateFormat("EEEE");
				   this.partnerPropertyId=(Integer)sessionMap.get("partnerPropertyId");
			 		this.selectPartnerPropertyId=(Integer)sessionMap.get("selectedPropertyId");
					if(this.selectPartnerPropertyId!=null){
						this.partnerPropertyId=this.selectPartnerPropertyId;	
					}else{
						this.partnerPropertyId=this.partnerPropertyId;
					}
				this.propertyId = this.partnerPropertyId;
				Integer userId=(Integer) sessionMap.get("partnerUserId");
				long time = System.currentTimeMillis();
				java.util.Date date = new java.util.Date(time);
				Calendar calDate=Calendar.getInstance();
            	calDate.setTime(date);
            	calDate.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
            	java.util.Date dtDate=calDate.getTime();
            	Timestamp tsDate=new Timestamp(dtDate.getTime());
            	int bookedHours=calDate.get(Calendar.HOUR);
            	int bookedMinute=calDate.get(Calendar.MINUTE);
            	int bookedSecond=calDate.get(Calendar.SECOND);
            	int AMPM=calDate.get(Calendar.AM_PM);
            	
            	String strBookingDate=new SimpleDateFormat("MMM d, yyyy", Locale.ENGLISH).format(calDate.getTime());
            	String hours="",minutes="",seconds="";
            	if(bookedHours<10){
            		hours=String.valueOf(bookedHours);
            		hours="0"+hours;
            	}else{
            		hours=String.valueOf(bookedHours);
            	}
            	if(bookedMinute<10){
            		minutes=String.valueOf(bookedMinute);
            		minutes="0"+minutes;
            	}else{
            		minutes=String.valueOf(bookedMinute);
            	}
            	
            	if(bookedMinute<10){
            		seconds=String.valueOf(bookedSecond);
            		seconds="0"+seconds;
            	}else{
            		seconds=String.valueOf(bookedSecond);
            	}
            	if(AMPM==0){//If the current time is AM
            		this.timeHours=f.format(calDate.getTime())+","+strBookingDate+" at "+hours+" : "+minutes+" AM ";
            	}else if(AMPM==1){//If the current time is PM
            		this.timeHours=f.format(calDate.getTime())+","+strBookingDate+" at "+hours+" : "+minutes+" PM ";
            	}  
				
				PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
				PropertyAccommodationInventoryManager accommodationInventoryController = new PropertyAccommodationInventoryManager();
				OtaHotelsManager otaHotelController = new OtaHotelsManager();
				/*PropertyAccommodationRoomManager accommodationRoomController = new PropertyAccommodationRoomManager();
				UserLoginManager  userController = new UserLoginManager();*/
				
				 for (int i = 0; i < array.size(); i++) 
			     {
																																																																																																																																																																																																																																																																																																																																																																																																																																																																																						
				
				
				DateFormat format = new SimpleDateFormat("MMM d,yyyy" , Locale.ENGLISH);
			    java.util.Date dateStart = format.parse(array.get(i).getStartBlockDate());
			    
			  
			    
			    Calendar calCheckStart=Calendar.getInstance();
			    calCheckStart.setTime(dateStart);
			    java.util.Date checkInDate = calCheckStart.getTime();
			   
			    
			    
			    
			    String startDate = new SimpleDateFormat("yyyy-MM-dd").format(checkInDate);
	            DateTime start = DateTime.parse(startDate);
	            java.util.Date bookingDate=  start.toDate();
			    PropertyAccommodation propertyAccommodation1 = accommodationController.find(array.get(i).getPropertyAccommodationId());
			    int roomCount = Integer.parseInt(array.get(i).getInventoryCount());
			    if(propertyAccommodation1.getNoOfUnits() >= roomCount){
			    	OtaHotelDetails otaHotelDetails = otaHotelController.getOtaHotelDetails(array.get(i).getPropertyAccommodationId(),1);
			    	if(otaHotelDetails != null){
			    	int sourceId = 3;
			    	OtaHotels otaHotel = otaHotelController.getOtaHotels(getPropertyId(), sourceId);
			    	DefaultHttpClient httpClient = new DefaultHttpClient();
					HttpPost postRequest = new HttpPost("https://in.goibibo.com/api/chmv2/updateroominventory/?bearer_token="+otaHotel.getOtaBearerToken()+"&channel_token="+otaHotel.getOtaChannelToken()+"");
					String xmlBody = "<?xml version='1.0' encoding='UTF-8' ?>"+
						    "<Website Name='ingoibibo' HotelCode='"+otaHotelDetails.getOtaHotelCode()+"'>"+
						            "<Room>"+
						            "<RoomTypeCode>"+otaHotelDetails.getOtaRoomTypeId()+"</RoomTypeCode>"+
						            "<StartDate Format='yyyy-mm-dd'>"+startDate+"</StartDate>"+
						            "<EndDate Format='yyyy-mm-dd'>"+startDate+"</EndDate>"+						            
						            "<DaysOfWeek Mon='"+"true"+"' Tue='"+"true"+"' Wed='"+"true"+"' Thu='"+"true"+"' Fri='"+"true"+"' Sat='"+"true"+"' Sun='"+"true"+"'></DaysOfWeek>"+
//						            "<MinLOS>"+"1"+"</MinLOS>"+
//						            "<CutOff>"+"1h"+"</CutOff>"+
						            "<Available> "+ roomCount +"</Available>"+
						            "<StopSell>"+"False"+"</StopSell>"+
						            "</Room>"+
						      "</Website>";
					
				    
					
					StringEntity input = new StringEntity(xmlBody);
					input.setContentType("text/xml");
					postRequest.setEntity(input);
					HttpResponse httpResponse = httpClient.execute(postRequest);
					HttpEntity entity = httpResponse.getEntity();
	                // Read the contents of an entity and return it as a String.
		            String content = EntityUtils.toString(entity);
			    	}
	             PropertyAccommodationInventory inventoryCount = accommodationInventoryController.findId(bookingDate,array.get(i).getPropertyAccommodationId());
	             if(inventoryCount != null){
					 PropertyAccommodationInventory inventory1 = accommodationInventoryController.find(inventoryCount.getInventoryId());	 
		             inventory1.setIsActive(false);
		             inventory1.setIsDeleted(true);
		             inventory1.setCreatedBy(userId);
		             inventory1.setCreatedDate(tsDate);
		             accommodationInventoryController.edit(inventory1);   
		             
		             PropertyAccommodationInventory inventory2 = new PropertyAccommodationInventory();
		             inventory2.setIsActive(true);
		             inventory2.setIsDeleted(false);
		             inventory2.setCreatedBy(userId);
		             inventory2.setCreatedDate(tsDate);
		             inventory2.setBlockedDate(bookingDate);
		             inventory2.setInventoryRemarks(getInventoryRemarks());
		             inventory2.setInventoryCount(array.get(i).getInventoryCount());	
		             PropertyAccommodation propertyAccommodation = accommodationController.find(array.get(i).getPropertyAccommodationId());
	                 inventory2.setPropertyAccommodation(propertyAccommodation);
		             accommodationInventoryController.add(inventory2);
				 }
	             else{
	                 PropertyAccommodationInventory inventory = new PropertyAccommodationInventory();
	                 inventory.setInventoryCount(array.get(i).getInventoryCount());
	                 inventory.setBlockedDate(bookingDate);	                 
	                 inventory.setIsActive(true);
	                 inventory.setIsDeleted(false);
	                 inventory.setCreatedBy(userId);
	                 inventory.setCreatedDate(tsDate);
	                 inventory.setInventoryRemarks(getInventoryRemarks());
	                 PropertyAccommodation propertyAccommodation = accommodationController.find(array.get(i).getPropertyAccommodationId());
	                 inventory.setPropertyAccommodation(propertyAccommodation);
	                 accommodationInventoryController.add(inventory);
	    			 }
	             strReturn = null;
	            }
			    
			     }
				 
				    Configuration cfg = new Configuration();
	 				cfg.setClassForTemplateLoading(BookingDetailAction.class, "../../../");
	 				Template template = cfg.getTemplate(getText("partnerblock.template"));
	 				Map<String, Object> rootMap = new HashMap<String, Object>();
	 				PmsPropertyManager propertyController=new PmsPropertyManager();
	 				UserLoginManager userController=new UserLoginManager();
	 				PmsProperty property=propertyController.find(this.propertyId);	
	 				User user = userController.find(userId);
	 				
	 				rootMap.put("propertyName",property.getPropertyName());
	 				rootMap.put("createdBy",user.getUserName());
	 				rootMap.put("createdDate",this.timeHours);
	 				rootMap.put("blockReason",getInventoryRemarks());
	 				//srootMap.put("address",property.getAddress1());
	 				
	 				List accommodationBooked = new ArrayList();
	 				for(int j=0;j<array.size();j++){
	 					DateFormat format1 = new SimpleDateFormat("MMM d,yyyy" , Locale.ENGLISH);
	 				    java.util.Date dateStart1 = format1.parse(array.get(j).getStartBlockDate());
	 				    String startDate = new SimpleDateFormat("yyyy-MM-dd").format(dateStart1);
	 				   PropertyAccommodation propertyAccommodation = accommodationController.find(array.get(j).getPropertyAccommodationId());
	 				   int blockedRoom = propertyAccommodation.getNoOfUnits() - Integer.parseInt(array.get(j).getInventoryCount());
	 				   accommodationBooked.add(new DashBoard(propertyAccommodation.getAccommodationType(),array.get(j).getInventoryCount(),startDate,propertyAccommodation.getNoOfUnits(),blockedRoom));
	 					
		 				
		 				rootMap.put("json",accommodationBooked);
		 				
	 				}
	 				
					rootMap.put("from", getText("notification.from"));
	 				Writer out = new StringWriter();
	 				template.process(rootMap, out);



	 				String strSubject="Blocking Rooms In "+property.getPropertyName();
	 				//send email
	 				Email em = new Email();
	 				em.set_to(user.getEmailId());  /* property.getContractManagerEmail()*/
	 				em.set_cc(getText("bookings.notification.email"));
	 				em.set_cc2(getText("operations.notification.email"));
	 				em.set_cc3(getText("finance.notification.email"));
	 				em.set_cc4("revenue@ulohotels.com");
	 				em.set_cc5("reservations@ulohotels.com");
	 				em.set_cc6("bookings@ulohotels.com");
	 				em.set_cc7("payments@ulohotels.com");
	 				em.set_cc8(property.getContractManagerEmail());
	 				em.set_from(getText("email.from"));
	 				em.set_username(getText("email.username"));
	 				em.set_subject(strSubject);
	 				em.set_bodyContent(out);
	 				em.sendPartnerBlock();
				 
					
			} catch (Exception e) {                    
				logger.error(e);
				strReturn="error";
				e.printStackTrace();
			} finally {

			}
			return strReturn;
		}	
	
	public String getPropertyAccommodations(){
		try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			
			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
			PropertyAccommodationRoomManager accommodationRoomController = new PropertyAccommodationRoomManager();
			//this.familyRegisterList = familyController.list(getUser().getUserid());
			//model = familyRegisterList;
			response.setContentType("application/json");

			this.accommodationList = accommodationController.list(getPropertyId());
			for (PropertyAccommodation accommodation : accommodationList) {

				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				
				//AccommodationRoom accommodationRooms = accommodationRoomController.findCount(this.propertyId,accommodation.getAccommodationId());
				jsonOutput += "\"accommodationId\":\"" + accommodation.getAccommodationId() + "\"";
				jsonOutput += ",\"abbreviation\":\"" + accommodation.getAbbreviation()+ "\"";
				jsonOutput += ",\"accommodationType\":\"" + accommodation.getAccommodationType()+ "\"";
				jsonOutput += ",\"noOfUnits\":\"" + accommodation.getNoOfUnits()+ "\"";
				jsonOutput += ",\"minOccupancy\":\"" + accommodation.getMinOccupancy()+ "\"";
				jsonOutput += ",\"maxOccupancy\":\"" + accommodation.getMaxOccupancy()+ "\"";
				jsonOutput += ",\"noOfAdults\":\"" + accommodation.getNoOfAdults()+ "\"";
				jsonOutput += ",\"noOfChild\":\"" + accommodation.getNoOfChild()+ "\"";
				jsonOutput += ",\"baseAmount\":\"" + accommodation.getBaseAmount()+ "\"";
				jsonOutput += ",\"extraChild\":\"" + accommodation.getExtraChild()+ "\"";
				jsonOutput += ",\"extraAdult\":\"" + accommodation.getExtraAdult()+ "\"";
				jsonOutput += ",\"extraInfant\":\"" + accommodation.getExtraInfant()+ "\"";
				jsonOutput += ",\"description\":\"" + accommodation.getAccommodationDescription()+ "\"";
				jsonOutput += "}";


			}

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

		return null;

	
	}
	public String getLogInventory(){
		try{
			this.propertyId=getPropertyId();
			if(this.propertyId==null){
				this.propertyId = (Integer) sessionMap.get("propertyId");
			}
			UserLoginManager userController=new UserLoginManager();
			Calendar calendar=Calendar.getInstance();
			SimpleDateFormat format1 = new SimpleDateFormat("dd-MMM-yyyy");
	 		String currentDate = format1.format(calendar.getTime());
	    	java.util.Date curdate=format1.parse(currentDate);
	    	java.sql.Timestamp tsCurrentDate = new java.sql.Timestamp(curdate.getTime());
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("application/json");
			int serialno=1;
			
			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
			PropertyAccommodationInventoryManager accommodationInventoryController = new PropertyAccommodationInventoryManager();
			
			List<BookingListAction> listPropertyInventory=accommodationInventoryController.listAccommodationInventory(propertyId, curdate);
			if(listPropertyInventory.size()>0 && !listPropertyInventory.isEmpty()){
				for(BookingListAction inventory:listPropertyInventory){
					if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += ",{";
					else
						jsonOutput += "{";
					jsonOutput += "\"inventoryId\":\"" + inventory.getInventoryId() + "\"";
					jsonOutput += ",\"inventoryCount\":\"" + inventory.getInventoryCount()+ "\"";
					String strBlockedDate="";
					if(inventory.getBlockedDate()!=null){
						strBlockedDate=new SimpleDateFormat("dd-MMM-yyyy").format(inventory.getBlockedDate());	
					}else{
						strBlockedDate="";
					}
					
					jsonOutput += ",\"blockedDate\":\"" + strBlockedDate + "\"";
					if(inventory.getAccommodationId()!=null){
						PropertyAccommodation accommodations=accommodationController.find(inventory.getAccommodationId());
						jsonOutput += ",\"accommodationType\":\"" + accommodations.getAccommodationType().toUpperCase().trim()+ "\"";	
					}else{
						jsonOutput += ",\"accommodationType\":\"" + "" + "\"";
					}
					
					String strCreatedDate=null;
					if(inventory.getCreatedDate()!=null){
						strCreatedDate=new SimpleDateFormat("dd-MMM-yyyy").format(inventory.getCreatedDate());
						jsonOutput += ",\"createdDate\":\"" + strCreatedDate+ "\"";
					}else{
						strCreatedDate="";
						jsonOutput += ",\"createdDate\":\"" + strCreatedDate+ "\"";
					}
					Integer createdBy=0;
					if(inventory.getCreatedBy()!=null){
						createdBy=inventory.getCreatedBy();
						User user=userController.find(createdBy);
						jsonOutput += ",\"createdBy\":\"" + user.getUserName() + "\"";
					}else{
						jsonOutput += ",\"createdBy\":\"" + ""+ "\"";
					}
					jsonOutput += ",\"serialNo\":\"" + serialno+ "\"";
					serialno++;
					
					jsonOutput += "}";
				}
				
			}
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
		}catch(Exception e){
			logger.error(e);
			e.printStackTrace();
		}
		return null;
	}
	public static List<DateTime> getDateRange(DateTime start, DateTime end) {

	    List<DateTime> ret = new ArrayList<DateTime>();
	    DateTime tmp = start;
	    while(tmp.isBefore(end) || tmp.equals(end)) {
	        ret.add(tmp);
	        tmp = tmp.plusDays(1);
	    }
	    return ret;
	}
	

	public Integer getPropertyAccommodationId() {
		return propertyAccommodationId;
	}

	public void setPropertyAccommodationId(Integer propertyAccommodationId) {
		this.propertyAccommodationId = propertyAccommodationId;
	}

	public Integer getPropertyContractTypeId() {
		return propertyContractTypeId;
	}

	public void setPropertyContractTypeId(Integer propertyContractTypeId) {
		this.propertyContractTypeId = propertyContractTypeId;
	}

	public Integer getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(Integer propertyId) {
		this.propertyId = propertyId;
	}

	public Integer getPartnerPropertyId() {
		return partnerPropertyId;
	}

	public void setPartnerPropertyId(Integer partnerPropertyId) {
		this.partnerPropertyId = partnerPropertyId;
	}

	public Integer getSelectPartnerPropertyId() {
		return selectPartnerPropertyId;
	}

	public void setSelectPartnerPropertyId(Integer selectPartnerPropertyId) {
		this.selectPartnerPropertyId = selectPartnerPropertyId;
	}

	public String getAbbreviation() {
		return abbreviation;
	}

	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}

	public String getAccommodationType() {
		return accommodationType;
	}

	public void setAccommodationType(String accommodationType) {
		this.accommodationType = accommodationType;
	}
	
	public Integer getNoOfUnits() {
		return noOfUnits;
	}

	public void setNoOfUnits(Integer noOfUnits) {
		this.noOfUnits = noOfUnits;
	}
	
	public Integer getMinOccupancy() {
		return minOccupancy;
	}

	public void setMinOccupancy(Integer minOccupancy) {
		this.minOccupancy = minOccupancy;
	}
	
	public Integer getMaxOccupancy() {
		return maxOccupancy;
	}

	public void setMaxOccupancy(Integer maxOccupancy) {
		this.maxOccupancy = maxOccupancy;
	}
	
	public Integer getNoOfAdults() {
		return noOfAdults;
	}

	public void setNoOfAdults(Integer noOfAdults) {
		this.noOfAdults = noOfAdults;
	}

	
	public Integer getNoOfChild() {
		return noOfChild;
	}

	public void setNoOfChild(Integer noOfChild) {
		this.noOfChild = noOfChild;
	}
    
	public double getBaseAmount() {
		return baseAmount;
	}

	public void setBaseAmount(double baseAmount) {
		this.baseAmount = baseAmount;
	}
	
	public double getExtraChild() {
		return extraChild;
	}

	public void setExtraChild(double extraChild) {
		this.extraChild = extraChild;
	}
	
	public double getExtraAdult() {
		return extraAdult;
	}

	public void setExtraAdult(double extraAdult) {
		this.extraAdult = extraAdult;
	}
	
	public double getExtraInfant() {
		return extraInfant;
	}

	public void setExtraInfant(double extraInfant) {
		this.extraInfant = extraInfant;
	}
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public void setAccommodationImgPath(String accommodationPic) {
		this.accommodationPic = accommodationPic;
	}

	public String getAccommodationImgPath() {
		return accommodationPic;
	}
	
	public File getMyFile() {
		return myFile;
	}

	public void setMyFile(File myFile) {
		this.myFile = myFile;
	}
	public void setImgPath(String accommodationPic) {
		this.accommodationPic = accommodationPic;
	}

	public String getImgPath() {
		return accommodationPic;
	}

	public String getMyFileContentType() {
		return myFileContentType;
	}

	public void setMyFileContentType(String myFileContentType) {
		this.myFileContentType = myFileContentType;
	}

	public String getMyFileFileName() {
		return myFileFileName;
	}

	public void setMyFileFileName(String myFileFileName) {
		this.myFileFileName = myFileFileName;
	}
	
	public List<PropertyAccommodation> getAccommodationList() {
		return accommodationList;
	}

	public void setAccomadationList(List<PropertyAccommodation> accommodationList) {
		this.accommodationList = accommodationList;
	}

	public void setAccommodation(PropertyAccommodation accommodation) {
		this.accommodation = accommodation;
	}
	
	public String getStartBlockDate() {
		return startBlockDate;
	}

	public void setStartBlockDate(String startBlockDate) {
		this.startBlockDate = startBlockDate;
	}

	public String getEndBlockDate() {
		return endBlockDate;
	}

	public void setEndBlockDate(String endBlockDate) {
		this.endBlockDate = endBlockDate;
	}

	public String getInventoryCount() {
		return inventoryCount;
	}

	public void setInventoryCount(String inventoryCount) {
		this.inventoryCount = inventoryCount;
	}	

	public String getInventoryRemarks() {
		return inventoryRemarks;
	}

	public void setInventoryRemarks(String inventoryRemarks) {
		this.inventoryRemarks = inventoryRemarks;
	}

	public double getNetRateRevenue() {
		return netRateRevenue;
	}

	public void setNetRateRevenue(double netRateRevenue) {
		this.netRateRevenue = netRateRevenue;
	}
	
	public double getMinimumBaseAmount() {
		return minimumBaseAmount;
	}

	public void setMinimumBaseAmount(double minimumBaseAmount) {
		this.minimumBaseAmount = minimumBaseAmount;
	}

	public double getMaximumBaseAmount() {
		return maximumBaseAmount;
	}

	public void setMaximumBaseAmount(double maximumBaseAmount) {
		this.maximumBaseAmount = maximumBaseAmount;
	}

	public double getNetRate() {
		return netRate;
	}

	public void setNetRate(double netRate) {
		this.netRate = netRate;
	}

	public Integer getTaxType() {
		return taxType;
	}

	public void setTaxType(Integer taxType) {
		this.taxType = taxType;
	}

	public double getTariff() {
		return tariff;
	}

	public void setTariff(double tariff) {
		this.tariff = tariff;
	}

	public double getTax() {
		return tax;
	}

	public void setTax(double tax) {
		this.tax = tax;
	}

	public double getSellingRate() {
		return sellingRate;
	}

	public void setSellingRate(double sellingRate) {
		this.sellingRate = sellingRate;
	}

	public double getPurchaseRate() {
		return purchaseRate;
	}

	public void setPurchaseRate(double purchaseRate) {
		this.purchaseRate = purchaseRate;
	}

	public Integer getLocationTypeId() {
		return locationTypeId;
	}

	public void setLocationTypeId(Integer locationTypeId) {
		this.locationTypeId = locationTypeId;
	}

	public double getWeekdayNetRate() {
		return weekdayNetRate;
	}

	public void setWeekdayNetRate(double weekdayNetRate) {
		this.weekdayNetRate = weekdayNetRate;
	}

	public double getWeekendNetRate() {
		return weekendNetRate;
	}

	public void setWeekendNetRate(double weekendNetRate) {
		this.weekendNetRate = weekendNetRate;
	}

	public double getWeekdayPurchaseRate() {
		return weekdayPurchaseRate;
	}

	public void setWeekdayPurchaseRate(double weekdayPurchaseRate) {
		this.weekdayPurchaseRate = weekdayPurchaseRate;
	}

	public double getWeekendPurchaseRate() {
		return weekendPurchaseRate;
	}

	public void setWeekendPurchaseRate(double weekendPurchaseRate) {
		this.weekendPurchaseRate = weekendPurchaseRate;
	}

	public double getWeekdayBaseAmount() {
		return weekdayBaseAmount;
	}

	public void setWeekdayBaseAmount(double weekdayBaseAmount) {
		this.weekdayBaseAmount = weekdayBaseAmount;
	}

	public double getWeekendBaseAmount() {
		return weekendBaseAmount;
	}

	public void setWeekendBaseAmount(double weekendBaseAmount) {
		this.weekendBaseAmount = weekendBaseAmount;
	}

	public double getWeekdayTariff() {
		return weekdayTariff;
	}

	public void setWeekdayTariff(double weekdayTariff) {
		this.weekdayTariff = weekdayTariff;
	}

	public double getWeekendTariff() {
		return weekendTariff;
	}

	public void setWeekendTariff(double weekendTariff) {
		this.weekendTariff = weekendTariff;
	}

	public double getWeekdayTax() {
		return weekdayTax;
	}

	public void setWeekdayTax(double weekdayTax) {
		this.weekdayTax = weekdayTax;
	}

	public double getWeekendTax() {
		return weekendTax;
	}

	public void setWeekendTax(double weekendTax) {
		this.weekendTax = weekendTax;
	}

	public double getWeekendSellingRate() {
		return weekendSellingRate;
	}

	public void setWeekendSellingRate(double weekendSellingRate) {
		this.weekendSellingRate = weekendSellingRate;
	}

	public double getWeekdaySellingRate() {
		return weekdaySellingRate;
	}

	public void setWeekdaySellingRate(double weekdaySellingRate) {
		this.weekdaySellingRate = weekdaySellingRate;
	}

	public double getPropertyCommission() {
		return propertyCommission;
	}

	public void setPropertyCommission(double propertyCommission) {
		this.propertyCommission = propertyCommission;
	}
}
