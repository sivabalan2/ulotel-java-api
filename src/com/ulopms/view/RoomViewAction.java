package com.ulopms.view;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.ServletRequestAware;
















import org.joda.time.DateTime;

import com.opensymphony.xwork2.ActionSupport;
import com.ulopms.controller.BookingDetailManager;
import com.ulopms.controller.PmsBookingManager;
import com.ulopms.controller.PropertyAccommodationManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.AccommodationRoom;
import com.ulopms.model.BookingDetail;
import com.ulopms.model.PmsAvailableRooms;
import com.ulopms.model.PropertyAccommodation;
import com.ulopms.model.User;
import com.ulopms.util.DateUtil;


public class RoomViewAction extends ActionSupport implements
ServletRequestAware, SessionAware, UserAware{

	private static final long serialVersionUID = 914982665758390091L;

	private Integer bookingId;
	private Timestamp arrivalDate;
	private Timestamp departureDate;
	private Integer rooms;
	private Long roomCount;
	public Integer propertyId;
	public Integer accommodationId;
	private String chartDate;
	private String checkedAccommodationId;
	public String getChartDate(){
		return chartDate;
	}
	
	public void setChartDate(String chartDate){
		this.chartDate=chartDate;
	}
	
	private long roomCnt;
	
	public long getRoomCnt() {
		return roomCnt;
	}

	public void setRoomCnt(long roomCnt) {
		this.roomCnt = roomCnt;
	}

	public String getCheckedAccommodationId(){
		return checkedAccommodationId;
	}
	
	public void setCheckedAccommodationId(String checkedAccommodationId){
		this.checkedAccommodationId=checkedAccommodationId;
	}

	private static final Logger logger = Logger.getLogger(BookingAction.class);

	private HttpSession session;

	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}
	
	private SessionMap<String,Object> sessionMap;  
	  
	//getters and setters  
	  
	@Override  
	public void setSession(Map<String, Object> map) {  
	    sessionMap=(SessionMap)map;  
	}  
	
	
	private User user;

	
	// @Override
	// public User getModel() {
	// return user;
	// }

	@Override
	public void setUser(User user) {
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	
	private HttpServletRequest request;
	
	
	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}

	public RoomViewAction() {

	}

	public String execute() {

		
		return SUCCESS;
	}

	
	public Integer getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(Integer propertyId) {
		this.propertyId = propertyId;
	}
	
	public Integer getAccommodationId() {
		return accommodationId;
	}

	public void setAccommodationId(Integer accommodationId) {
		this.accommodationId = accommodationId;
	}
	
	
	
	public Integer getBookingId() {
		return bookingId;
	}

	public void setBookingId(Integer bookingId) {
		this.bookingId = bookingId;
	}
	
	

	public Timestamp getArrivalDate() {
		return arrivalDate;
	}

	public void setArrivalDate(Timestamp arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	public Timestamp getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Timestamp departureDate) {
		this.departureDate = departureDate;
	}

	public Long getRoomCount() {
		return roomCount;
	}

	public void setRoomCount(Long roomCount) {
		this.roomCount = roomCount;
	}
	
	public Integer getRooms() {
		return rooms;
	}

	public void setRooms(Integer rooms) {
		this.rooms = rooms;
	}
	
	 public String getRoomAvailable() throws IOException{
		 this.propertyId = (Integer) sessionMap.get("propertyId");
		 this.checkedAccommodationId = getCheckedAccommodationId();
		 sessionMap.put("checkedAccommodationId",checkedAccommodationId);
		 sessionMap.put("chartDate",chartDate); 
		 if(getCheckedAccommodationId()==null || getCheckedAccommodationId().equalsIgnoreCase("undefined")){
			 checkedAccommodationId="0";
			 return null;
		 }
		 ArrayList<String> AccommIdList=new ArrayList<String>();
		 String jsonOutput = "";
		 HttpServletResponse response = ServletActionContext.getResponse();
		 response.setContentType("application/json");
		 int roomCount=0;
		 int totalCount=0;
		 int availableCount=0;
		 int totalAvailableCount=0;
		 int soldCount=0;
		 int totalSoldCount=0;
		 String strAccommodId;
		 try{
			 PropertyAccommodationManager roomController=new PropertyAccommodationManager();
			 String[] strAccommodationId=checkedAccommodationId.split(",");
			 for(String accommodationId:strAccommodationId){
				 AccommIdList.add(accommodationId);
			 }
			 SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
			 List<AccommodationRoom> listAccommodationRoom=null;
			 String[] monthDate=chartDate.split(",");
			 for(String strDate: monthDate){
				 java.util.Date dailyDate = format1.parse(strDate);
				 Timestamp tsDate=new Timestamp(dailyDate.getTime());
				 Iterator<String> iterAccommValues=AccommIdList.iterator();
				 while(iterAccommValues.hasNext()){
					 strAccommodId=iterAccommValues.next();
					 listAccommodationRoom=roomController.listPropertyTotalRooms(propertyId, Integer.parseInt(strAccommodId));
					 if(listAccommodationRoom.size()>0 && !listAccommodationRoom.isEmpty()){
						 for(AccommodationRoom roomsList:listAccommodationRoom){
							 roomCount=(int) roomsList.getRoomCount();
							 totalCount=totalCount+roomCount;
							 long minimum = getAvailableCount(getPropertyId(),dailyDate,roomCount,Integer.parseInt(strAccommodId)); 
							 availableCount=(int)minimum;
							 totalAvailableCount=totalAvailableCount+availableCount;
						 }
					 }
					 
					 
				 }
				/* if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += ",{";
					
					else
						
						jsonOutput = "{";
					
						jsonOutput += "\"title\":\"" +" Total Room: "+(totalCount)+" Available : "+(totalAvailableCount)+" Sold :"+(totalCount-totalAvailableCount)+" \"";
					    jsonOutput += ",\"start\":\"o\"";
					    
				        jsonOutput += "}";*/
				 
				if (!jsonOutput.equalsIgnoreCase(""))
	 					jsonOutput += ",{";
	 			
	 				        else
	 				 					jsonOutput = "{";
	 				 				
	 				 				   jsonOutput += "\"title\":\"" +" Total Room: "+(totalCount)+" \"";
	 				 				    jsonOutput += ",\"start\":\""+strDate+"\"";
	 				 				    
	 				 			        jsonOutput += "}";
	 				 			        
	 				 			        jsonOutput += ",{";
	 				 			        
	 				 				    jsonOutput += "\"title\":\"" +" Available : "+(totalAvailableCount)+" \"";
	 				 				    jsonOutput += ",\"start\":\""+strDate+"\"";
	 				 				   jsonOutput += ",\"color\":\"green\"";
	 				 				   
	 				 				    jsonOutput += "}";
	 				 				    
	 				 				    jsonOutput += ",{";
	 				 				    
	 				 				    jsonOutput += "\"title\":\"" +" Sold :"+(totalCount-totalAvailableCount)+" \"";
	 				 				    jsonOutput += ",\"start\":\""+strDate+"\"";
	 				 				   jsonOutput += ",\"color\":\"red\"";
	 				 				    
	 				 				    jsonOutput += "}";
	 				 				    
	 				 				    jsonOutput += ",{";
	 				 				  
	 				 				   jsonOutput += "\"title\":\"" +" Occup % :"+(totalCount-totalAvailableCount) * 100 / totalCount +" \"";
	 				 				    jsonOutput += ",\"start\":\""+strDate+"\"";
	 				 				    jsonOutput += ",\"color\":\"orange\"";
	 				 				    
	 				 				   jsonOutput += "}";
	 				 				   
	 				 
	 				   // jsonOutput += ",{";
	 				    //jsonOutput += "\"title\":\"" +" Percentage  :"+(totalCount-totalAvailableCount) * 100 / totalCount +" \"";
	 				   // jsonOutput += ",\"start\":\""+strDate+"\"";
	 				    //jsonOutput += ",\"color\":\"skyblue\"";
	 				   // jsonOutput += "}";
	 				   //(Obtained score x 100) / Total Score
					        
					        
					 roomCount=0; 
					 totalCount=0;
					 totalAvailableCount=0;
					 availableCount=0;
				 } 
				 response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			 }
			 catch(Exception e){
				 logger.error(e);
				 e.printStackTrace();
			 }finally{
				 
			 }
			 
			 return null;
		 }


	 private Long getAvailableCount(int propertyId,Date curdate,long available,int accommodationId) throws IOException {
			PmsBookingManager bookingController = new PmsBookingManager();
			
			List<Long> myList = new ArrayList<Long>();
			
			long rmc = 0;
			this.roomCnt = rmc;
			PmsAvailableRooms roomCount = bookingController.findCount(curdate, accommodationId);
			if(roomCount.getRoomCount() == null){
				
				this.roomCnt = (available-rmc);
			}
			else{
				
				this.roomCnt = (available-roomCount.getRoomCount());
			}
			
			myList.add(this.roomCnt);
			
	        long availableCount = Collections.min(myList);

	        return availableCount;
	          
		}
}
