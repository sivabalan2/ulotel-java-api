package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;


/**
 * The persistent class for the booking_guest_details database table.
 * 
 */
@Entity
@Table(name="booking_guest_details")
@NamedQuery(name="BookingGuestDetail.findAll", query="SELECT b FROM BookingGuestDetail b")
public class BookingGuestDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="id",sequenceName="booking_guest_details_booking_guest_id_seq")
	@GeneratedValue(generator="id")
	@Column(name="booking_guest_id", unique=true, nullable=false)
	private Integer bookingGuestId;

	@Column(name="created_by")
	private Integer createdBy;

	@Column(name="created_date")
	private Timestamp createdDate;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_deleted")
	private Boolean isDeleted;

	@Column(name="is_primary")
	private Boolean isPrimary;

	@Column(name="modified_by")
	private Integer modifiedBy;

	@Column(name="modified_date")
	private Timestamp modifiedDate;
	
	@Column(name="special_request")
	private String specialRequest;
	
	@Column(name="used_reward_points")
	private Integer usedRewardPoints;

	

	//bi-directional many-to-one association to PmsBooking
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="booking_id")
	private PmsBooking pmsBooking;

	//bi-directional many-to-one association to PmsGuest
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="guest_id")
	private PmsGuest pmsGuest;

	public BookingGuestDetail() {
	}

	public Integer getBookingGuestId() {
		return this.bookingGuestId;
	}

	public void setBookingGuestId(Integer bookingGuestId) {
		this.bookingGuestId = bookingGuestId;
	}

	public Integer getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Boolean getIsPrimary() {
		return this.isPrimary;
	}

	public void setIsPrimary(Boolean isPrimary) {
		this.isPrimary = isPrimary;
	}

	public Integer getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Timestamp getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public PmsBooking getPmsBooking() {
		return this.pmsBooking;
	}

	public void setPmsBooking(PmsBooking pmsBooking) {
		this.pmsBooking = pmsBooking;
	}

	public PmsGuest getPmsGuest() {
		return this.pmsGuest;
	}

	public void setPmsGuest(PmsGuest pmsGuest) {
		this.pmsGuest = pmsGuest;
	}
	
	public String getSpecialRequest() {
		return specialRequest;
	}

	public void setSpecialRequest(String specialRequest) {
		this.specialRequest = specialRequest;
	}

	public Integer getUsedRewardPoints() {
		return usedRewardPoints;
	}

	public void setUsedRewardPoints(Integer usedRewardPoints) {
		this.usedRewardPoints = usedRewardPoints;
	}
	

}