package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the blog_categories database table.
 * 
 */
@Entity
@Table(name="blog_categories")
@NamedQuery(name="BlogCategories.findAll", query="SELECT p FROM BlogCategories p")
public class BlogCategories implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="id",sequenceName="blog_categories_blog_category_id_seq")
	@GeneratedValue(generator="id")
	@Column(name="blog_category_id", unique=true, nullable=false)
	private Integer blogCategoryId;
	
	@Column(name="blog_category_name", length=250)
	private String blogCategoryName;
	
	@Column(name="blog_category_content", length=10485760)
	private String blogCategoryContent;
	
	@Column(name="created_by")
	private Integer createdBy;

	@Column(name="created_date")
	private Timestamp createdDate;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_deleted")
	private Boolean isDeleted;
	
	@Column(name="modified_by")
	private Integer modifiedBy;

	@Column(name="modified_date")
	private Timestamp modifiedDate;

	@Column(name="blog_category_image", length=250)
	private String blogCategoryImage;
	
	@Column(name="blog_category_url", length=250)
	private String blogCategoryUrl;
	

	//bi-directional many-to-one association to BlogCategories
	@OneToMany(mappedBy="blogCategories")
	private List<BlogArticles> blogArticles;
	

	public BlogCategories() {
	}	

	public Integer getBlogCategoryId() {
		return blogCategoryId;
	}

	public void setBlogCategoryId(Integer blogCategoryId) {
		this.blogCategoryId = blogCategoryId;
	}

	public String getBlogCategoryName() {
		return blogCategoryName;
	}

	public void setBlogCategoryName(String blogCategoryName) {
		this.blogCategoryName = blogCategoryName;
	}

	public String getBlogCategoryContent() {
		return blogCategoryContent;
	}

	public void setBlogCategoryContent(String blogCategoryContent) {
		this.blogCategoryContent = blogCategoryContent;
	}

	public String getBlogCategoryImage() {
		return blogCategoryImage;
	}

	public void setBlogCategoryImage(String blogCategoryImage) {
		this.blogCategoryImage = blogCategoryImage;
	}

	public Integer getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}


	public Integer getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Timestamp getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public List<BlogArticles> getBlogArticles() {
		return blogArticles;
	}

	public void setBlogArticles(List<BlogArticles> blogArticles) {
		this.blogArticles = blogArticles;
	}
	
	public String getBlogCategoryUrl() {
		return blogCategoryUrl;
	}

	public void setBlogCategoryUrl(String blogCategoryUrl) {
		this.blogCategoryUrl = blogCategoryUrl;
	}


}