package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;


/**
 * The persistent class for the property_items database table.
 * 
 */
@Entity
@Table(name="property_items")
@NamedQuery(name="PropertyItem.findAll", query="SELECT p FROM PropertyItem p")
public class PropertyItem implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="id",sequenceName="property_items_property_item_id_seq")
	@GeneratedValue(generator="id")
	@Column(name="property_item_id", unique=true, nullable=false)
	private Integer propertyItemId;

	private double amount;

	@Column(name="created_by")
	private Integer createdBy;

	@Column(name="created_date")
	private Timestamp createdDate;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_deleted")
	private Boolean isDeleted;

	@Column(name="item_code", length=10)
	private String itemCode;

	@Column(name="item_description", length=250)
	private String itemDescription;

	@Column(name="item_name", length=100)
	private String itemName;

	@Column(name="modified_by")
	private Integer modifiedBy;

	@Column(name="modified_date")
	private Timestamp modifiedDate;

	@Column(name="\"SKU\"", length=10)
	private String sku;

	//bi-directional many-to-one association to PmsProperty
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="property_id")
	private PmsProperty pmsProperty;

	//bi-directional many-to-one association to PropertyItemCategory
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="property_item_category_id")
	private PropertyItemCategory propertyItemCategory;

	//bi-directional many-to-one association to PropertyTaxe
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="property_tax_id")
	private PropertyTaxe propertyTaxe;

	public PropertyItem() {
	}

	public Integer getPropertyItemId() {
		return this.propertyItemId;
	}

	public void setPropertyItemId(Integer propertyItemId) {
		this.propertyItemId = propertyItemId;
	}

	public double getAmount() {
		return this.amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public Integer getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getItemCode() {
		return this.itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getItemDescription() {
		return this.itemDescription;
	}

	public void setItemDescription(String itemDescription) {
		this.itemDescription = itemDescription;
	}

	public String getItemName() {
		return this.itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public Integer getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Timestamp getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getSku() {
		return this.sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public PmsProperty getPmsProperty() {
		return this.pmsProperty;
	}

	public void setPmsProperty(PmsProperty pmsProperty) {
		this.pmsProperty = pmsProperty;
	}

	public PropertyItemCategory getPropertyItemCategory() {
		return this.propertyItemCategory;
	}

	public void setPropertyItemCategory(PropertyItemCategory propertyItemCategory) {
		this.propertyItemCategory = propertyItemCategory;
	}

	public PropertyTaxe getPropertyTaxe() {
		return this.propertyTaxe;
	}

	public void setPropertyTaxe(PropertyTaxe propertyTaxe) {
		this.propertyTaxe = propertyTaxe;
	}

}