package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the pms_booking database table.
 * 
 */

public class PmsAvailableRooms implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer propertyId;
	
	private Long roomCount;
	
	private Integer bookingId;
	
	private Integer accommodationId;
    
	private String accommodationType;
	
	private String accommodationTypeId;
	
	private String areaSqft;
	
	private Integer minOccupancy;
	
	private Integer maxOccupancy;
	
	private double noOfAdults;
	
	private double noOfChild;
	
	private double noOfInfant;
	
	private double extraAdult;
	
	private double extraChild;
	
	private double extraInfant;
	
	private double baseAmount;
	
	private Integer noOfUnits;
	
	private Integer roomavailable;
	
	private Long available;
	
	private Timestamp arrivalDate;

	private Timestamp departureDate;

	private Integer rooms;
	
	private Integer roomId;
	
	private double randomNo;
	
	private Boolean isActive;

	private Boolean isDeleted;
	
	private Integer sourceId;
	
	private Integer statusId;


	

	//
	
	public Integer getPropertyId() {
		return this.propertyId;
	}

	public void setPropertyId(Integer propertyId) {
		this.propertyId = propertyId;
	}
	
	public Long getRoomCount() {
		
		
		return this.roomCount;
		
	}
	
	public void setRoomCount(Long roomCount) {
		
			
			this.roomCount = roomCount;
	
		
	}
	
	public Integer getAccommodationId() {
		return this.accommodationId;
	}

	public void setAccommodationId(Integer accommodationId) {
		this.accommodationId = accommodationId;
	}
	
	public String getAccommodationType() {
			return this.accommodationType;
	}

	public void setAccommodationType(String accommodationType) {
			this.accommodationType = accommodationType;
	}
	
	public Integer getMinOccupancy() {
		return this.minOccupancy;
    }

   public void setMinOccupancy(Integer minOccupancy) {
		this.minOccupancy = minOccupancy;
    }
	
	public Integer getMaxOccupancy() {
		return this.maxOccupancy;
    }

   public void setMaxOccupancy(Integer maxOccupancy) {
		this.maxOccupancy = maxOccupancy;
    }
   
   public double getNoOfAdults() {
		return this.noOfAdults;
    }

   public void set(double noOfAdults) {
		this.noOfAdults = noOfAdults;
    }
   
   public double getNoOfChild() {
		return this.noOfChild;
   }

   public void setNoOfChild(double noOfChild) {
		this.noOfChild = noOfChild;
   }
   
   public double getNoOfInfant() {
		return this.noOfInfant;
 }

 public void setNoOfInfant(double noOfInfant) {
		this.noOfInfant = noOfInfant;
 }
 
   public double getExtraChild() {
		return this.extraChild;
   }

   public void setExtraChild(double extraChild) {
		this.extraChild = extraChild;
   }
   
   public double getExtraInfant() {
		return extraInfant;
	}

	public void setExtraInfant(double extraInfant) {
		this.extraInfant = extraInfant;
	}
   
   public double getExtraAdult() {
		return this.extraAdult;
   }

   public void setExtraAdult(double extraAdult) {
		this.extraAdult = extraAdult;
   }
 	
	public double getBaseAmount() {
		return this.baseAmount;
	}

	public void setBaseAmount(double baseAmount) {
		this.baseAmount = baseAmount;
	}
	
	public String getAreaSqft() {
		return this.areaSqft;
	}

	public void setAreaSqft(String areaSqft) {
		this.areaSqft = areaSqft;
	}
	
	public Integer getNoOfUnits() {
		return this.noOfUnits;
	}

	public void setNoOfUnits(Integer noOfUnits) {
		this.noOfUnits = noOfUnits;
	}
	
	public Long getAvailable() {
		return this.available;
	}

	public void setAvailable(Long available) {
		this.available = available;
	}
	
	public Timestamp getArrivalDate() {
		return this.arrivalDate;
	}

	public void setArrivalDate(Timestamp arrivalDate) {
		this.arrivalDate = arrivalDate;
	}
	
	public Timestamp getDepartureDate() {
		return this.departureDate;
	}
	
	public Integer getSourceId() {
		return sourceId;
	}

	public void setSourceId(Integer sourceId) {
		this.sourceId = sourceId;
	}

	public Integer getStatusId() {
		return statusId;
	}

	public void setStatusId(Integer statusId) {
		this.statusId = statusId;
	}


	public void setDepartureDate(Timestamp departureDate) {
		this.departureDate = departureDate;
	}


	public Integer getBookingId() {
		return this.bookingId;
	}

	public void setBookingId(Integer bookingId) {
		this.bookingId = bookingId;
	}

	public Integer getRoomId() {
		return this.roomId;
	}

	public void setRoomId(Integer roomId) {
		this.roomId = roomId;
	}
	
	public double getRandomNo() {
		return this.randomNo;
	}

	public void setRoomId(double randomNo) {
		this.randomNo = randomNo;
	}
	
    public Integer getRooms() {
		return this.rooms;
	}

	public void setRooms(Integer rooms) {
		this.rooms = rooms;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	
	public String getAccommodationTypeId() {
		return accommodationTypeId;
	}

	public void setAccommodationTypeId(String accommodationTypeId) {
		this.accommodationTypeId = accommodationTypeId;
	}
	
	public Integer getRoomAvailable() {
		return this.roomavailable;
	}

	public void setRoomAvailable(Integer roomavailable) {
		this.roomavailable = roomavailable;
	}


}