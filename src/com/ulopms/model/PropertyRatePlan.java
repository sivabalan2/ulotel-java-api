package com.ulopms.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * The persistent class for the pms_smart_price database table.
 * 
 */
@Entity
@Table(name="property_rate_plan")
@NamedQuery(name="PropertyRatePlan.findAll", query="SELECT p FROM PropertyRatePlan p")


public class PropertyRatePlan implements Serializable {
	private static final long serialVersionUID = 1L;

	

	@Id
	@SequenceGenerator(name="id",sequenceName="property_rate_plan_rate_plan_id_seq")
	@GeneratedValue(generator="id")
	
	@Column(name="rate_plan_id")
	private Integer ratePlanId;

	@Column(name="rate_plan_name")
	private String ratePlanName;
	
	@Column(name="rate_plan_type")
	private String ratePlanType;
	
	@Column(name="start_date")
	private Timestamp startDate;
	
	@Column(name="end_date")
	private Timestamp endDate;
	
	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_deleted")
	private Boolean isDeleted;
	
	@Column(name="rate_is_active")
	private Boolean rateIsActive;
	
	@Column(name="tax_is_active")
	private Boolean taxIsActive;
	
	@Column(name="created_by")
	private Integer createdBy;

	@Column(name="created_on")
	private Timestamp createdDate;
	
	@Column(name="modified_by")
	private Integer modifiedBy;

	@Column(name="modified_on")
	private Timestamp modifiedDate;
		
	@Column(name="rate_plan_symbol_type")
	private String ratePlanSymbolType;
	
	//bi-directional many-to-one association to OtaHotelDetails
	@OneToMany(mappedBy="propertyRatePlan")
	private List<OtaHotelDetails> otaHotelDetails;


	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	
	public Integer getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}
	
	public Integer getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Timestamp getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	
	public Integer getRatePlanId() {
		return ratePlanId;
	}

	public void setRatePlanId(Integer ratePlanId) {
		this.ratePlanId = ratePlanId;
	}

	public String getRatePlanName() {
		return ratePlanName;
	}

	public void setRatePlanName(String ratePlanName) {
		this.ratePlanName = ratePlanName;
	}

	public Timestamp getStartDate() {
		return startDate;
	}

	public void setStartDate(Timestamp startDate) {
		this.startDate = startDate;
	}

	public Timestamp getEndDate() {
		return endDate;
	}

	public void setEndDate(Timestamp endDate) {
		this.endDate = endDate;
	}

	public Boolean getRateIsActive() {
		return rateIsActive;
	}

	public void setRateIsActive(Boolean rateIsActive) {
		this.rateIsActive = rateIsActive;
	}
	
	public String getRatePlanType() {
		return ratePlanType;
	}

	public void setRatePlanType(String ratePlanType) {
		this.ratePlanType = ratePlanType;
	}
	
	public Boolean getTaxIsActive() {
		return taxIsActive;
	}

	public void setTaxIsActive(Boolean taxIsActive) {
		this.taxIsActive = taxIsActive;
	}
	
	public String getRatePlanSymbolType() {
		return ratePlanSymbolType;
	}

	public void setRatePlanSymbolType(String ratePlanSymbolType) {
		this.ratePlanSymbolType = ratePlanSymbolType;
	}

	public List<OtaHotelDetails> getOtaHotelDetails() {
		return otaHotelDetails;
	}

	public void setOtaHotelDetails(List<OtaHotelDetails> otaHotelDetails) {
		this.otaHotelDetails = otaHotelDetails;
	}
	
}