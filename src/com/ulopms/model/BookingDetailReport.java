package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;
import java.util.List;


public class BookingDetailReport implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Integer accommodationId;
	
	private String accommodationType;
	
	private Integer propertyId;
	
	private Integer bookingId;
	
	private Integer bookingDetailId;
	
	private Integer rooms;
	
	private Timestamp arrivalDate;
	
	private Timestamp departureDate;
	
	private Timestamp bookingDate;
	
	private Integer promotionId;
	
	private Integer smartPriceId;
	
	private String firstName;

	private String lastName;
	
	private String emailId;
	
	private Integer guestId;
	
	private String mobileNumber;
	
	private String phoneNumber;

	private Long roomCount;
	
	private Double amount;
	
	private Integer propertyDiscountId;
	
	private String city;
	
	private String sourceName;

	private Integer childCount;
	
	private Integer adultCount;

	private String promotionType;
	
	private Double taxAmount;
	
	private Double advanceAmount;

	private Double actualAmount;
	
	private Integer statusId;
	
	private Timestamp createdDate;
	
	private Integer sourceId;
	
	private Double otaCommission;
	
	private Double otaTax;
	
	private String durationPeriod;
	
	private Double revenueAmount;
	
	private Double totalTaxAmount;
	
	private Double revenueTaxAmount;
	
	private Double uloCommission;
	
	private Double uloTaxAmount;
	
	private Double uloRevenue;
	
	private Double hotelDueRevenue;
	
	private Double netHotelPayable;
	
	private Double centralGstTax;
	
	private Double stateGstTax;
	
	private Double totalUloCommission;
	
	private Double uloTotalCommission;
	
	private Integer invoiceNumber;
	
	private Integer createdBy;
	
	private Integer modifiedBy;
	
	private Integer propertyReviewId;

	private Integer starCount;

	private Double reviewCount;

	private Double averageReview;

	private String reviewImagePath;
	
	private Double refundAmount;
	
	private Double reducedAmount; 
	
	private Double baseAmount;

	private String diffDays;
	
	private String diffInDays;
	
	private String guestName;
	
	private String checkIn;
	
	private String checkOut;

	private String taxStatus;
	
	private String filterInvoiceDate;
	
	private String otaBookingId;
	
	private String specialRequest;

	private String propertyName;
	
	private Timestamp modifiedDate;
	
	private Double addonAmount;
	
	private String addonDescription;
	
	private Double purchaseRate;

	private Timestamp checkInTime;
	
	private Timestamp checkOutTime;
	
	private String voucherType;
	
	private Integer hours;
	
	private String strArrivalDate;
	
	private String strDepartureDate;
	
	private String strCheckInTime;
	
	private String strCheckOutTime;
	
	private Double promotionAmount;
	
	private Double couponAmount;

	private Double extraAdultRate;
	
	private Double extraChildRate;
	
	public BookingDetailReport(){
		
	}
	
	public BookingDetailReport(String accommodationType,Timestamp bookingDate,String firstName,String lastName,String emailId,String mobileNumber,String phoneNumber,Double amount){
		this.accommodationType=accommodationType;
		this.bookingDate=bookingDate;
		this.firstName=firstName;
		this.lastName=lastName;
		this.emailId=emailId;
		this.mobileNumber=mobileNumber;
		this.phoneNumber=phoneNumber;
		this.amount=amount;
	}
	
	
	public Integer getPropertyDiscountId() {
		return propertyDiscountId;
	}

	public void setPropertyDiscountId(Integer propertyDiscountId) {
		this.propertyDiscountId = propertyDiscountId;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}
	
	public String getAccommodationType() {
		return accommodationType;
	}

	public void setAccommodationType(String accommodationType) {
		this.accommodationType = accommodationType;
	}
	
	public Timestamp getBookingDate() {
		return bookingDate;
	}

	public void setBookingDate(Timestamp bookingDate) {
		this.bookingDate = bookingDate;
	}

	public Integer getAccommodationId() {
		return this.accommodationId;
	}

	public void setAccommodationId(Integer accommodationId) {
		this.accommodationId = accommodationId;
	}
	
	public Integer getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(Integer propertyId) {
		this.propertyId = propertyId;
	}

	public Integer getBookingId() {
		return bookingId;
	}

	public void setBookingId(Integer bookingId) {
		this.bookingId = bookingId;
	}

	public Integer getBookingDetailId() {
		return bookingDetailId;
	}

	public void setBookingDetailId(Integer bookingDetailId) {
		this.bookingDetailId = bookingDetailId;
	}

	public Integer getRooms() {
		return rooms;
	}

	public void setRooms(Integer rooms) {
		this.rooms = rooms;
	}

	public Timestamp getArrivalDate() {
		return arrivalDate;
	}

	public void setArrivalDate(Timestamp arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	public Timestamp getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Timestamp departureDate) {
		this.departureDate = departureDate;
	}

	public Integer getPromotionId() {
		return promotionId;
	}

	public void setPromotionId(Integer promotionId) {
		this.promotionId = promotionId;
	}

	public Integer getSmartPriceId() {
		return smartPriceId;
	}

	public void setSmartPriceId(Integer smartPriceId) {
		this.smartPriceId = smartPriceId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public Long getRoomCount() {
		return this.roomCount;
	}

	public void setRoomCount(Long roomCount) {
		this.roomCount = roomCount;
	}
	
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getSourceName() {
		return sourceName;
	}

	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}
	
	public String getPromotionType() {
		return promotionType;
	}

	public void setPromotionType(String promotionType) {
		this.promotionType = promotionType;
	}
	
	public Double getTaxAmount() {
		return taxAmount;
	}

	public void setTaxAmount(Double taxAmount) {
		this.taxAmount = taxAmount;
	}
	
	public Integer getStatusId() {
		return statusId;
	}

	public void setStatusId(Integer statusId) {
		this.statusId = statusId;
	}

	public Integer getChildCount() {
		return childCount;
	}

	public void setChildCount(Integer childCount) {
		this.childCount = childCount;
	}

	public Integer getAdultCount() {
		return adultCount;
	}

	public void setAdultCount(Integer adultCount) {
		this.adultCount = adultCount;
	}
	
	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}
	
	public Integer getSourceId() {
		return sourceId;
	}

	public void setSourceId(Integer sourceId) {
		this.sourceId = sourceId;
	}
	
	public Double getAdvanceAmount() {
		return advanceAmount;
	}

	public void setAdvanceAmount(Double advanceAmount) {
		this.advanceAmount = advanceAmount;
	}
	
	public Double getActualAmount() {
		return actualAmount;
	}

	public void setActualAmount(Double actualAmount) {
		this.actualAmount = actualAmount;
	}
	
	public Double getOtaCommission() {
		return otaCommission;
	}

	public void setOtaCommission(Double otaCommission) {
		this.otaCommission = otaCommission;
	}

	public Double getOtaTax() {
		return otaTax;
	}

	public void setOtaTax(Double otaTax) {
		this.otaTax = otaTax;
	}
	
	public Integer getGuestId() {
		return guestId;
	}

	public void setGuestId(Integer guestId) {
		this.guestId = guestId;
	}
	
	public String getDurationPeriod() {
		return durationPeriod;
	}

	public void setDurationPeriod(String durationPeriod) {
		this.durationPeriod = durationPeriod;
	}

	public Double getRevenueAmount() {
		return revenueAmount;
	}

	public void setRevenueAmount(Double revenueAmount) {
		this.revenueAmount = revenueAmount;
	}

	public Double getTotalTaxAmount() {
		return totalTaxAmount;
	}

	public void setTotalTaxAmount(Double totalTaxAmount) {
		this.totalTaxAmount = totalTaxAmount;
	}

	public Double getRevenueTaxAmount() {
		return revenueTaxAmount;
	}

	public void setRevenueTaxAmount(Double revenueTaxAmount) {
		this.revenueTaxAmount = revenueTaxAmount;
	}

	public Double getUloCommission() {
		return uloCommission;
	}

	public void setUloCommission(Double uloCommission) {
		this.uloCommission = uloCommission;
	}

	public Double getUloTaxAmount() {
		return uloTaxAmount;
	}

	public void setUloTaxAmount(Double uloTaxAmount) {
		this.uloTaxAmount = uloTaxAmount;
	}

	public Double getUloRevenue() {
		return uloRevenue;
	}

	public void setUloRevenue(Double uloRevenue) {
		this.uloRevenue = uloRevenue;
	}

	public Double getHotelDueRevenue() {
		return hotelDueRevenue;
	}

	public void setHotelDueRevenue(Double hotelDueRevenue) {
		this.hotelDueRevenue = hotelDueRevenue;
	}

	public Double getNetHotelPayable() {
		return netHotelPayable;
	}

	public void setNetHotelPayable(Double netHotelPayable) {
		this.netHotelPayable = netHotelPayable;
	}
	
	public Double getCentralGstTax() {
		return centralGstTax;
	}

	public void setCentralGstTax(Double centralGstTax) {
		this.centralGstTax = centralGstTax;
	}

	public Double getStateGstTax() {
		return stateGstTax;
	}

	public void setStateGstTax(Double stateGstTax) {
		this.stateGstTax = stateGstTax;
	}
	
	public Double getTotalUloCommission() {
		return totalUloCommission;
	}

	public void setTotalUloCommission(Double totalUloCommission) {
		this.totalUloCommission = totalUloCommission;
	}
	
	public Double getUloTotalCommission() {
		return uloTotalCommission;
	}

	public void setUloTotalCommission(Double uloTotalCommission) {
		this.uloTotalCommission = uloTotalCommission;
	}
	
	public Integer getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(Integer invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	
	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	
	public Integer getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	
	public Integer getPropertyReviewId() {
		return propertyReviewId;
	}

	public void setPropertyReviewId(Integer propertyReviewId) {
		this.propertyReviewId = propertyReviewId;
	}

	public Integer getStarCount() {
		return starCount;
	}

	public void setStarCount(Integer starCount) {
		this.starCount = starCount;
	}

	public Double getReviewCount() {
		return reviewCount;
	}

	public void setReviewCount(Double reviewCount) {
		this.reviewCount = reviewCount;
	}

	public Double getAverageReview() {
		return averageReview;
	}

	public void setAverageReview(Double averageReview) {
		this.averageReview = averageReview;
	}

	public String getReviewImagePath() {
		return reviewImagePath;
	}

	public void setReviewImagePath(String reviewImagePath) {
		this.reviewImagePath = reviewImagePath;
	}
	
	public Double getRefundAmount() {
		return refundAmount;
	}

	public void setRefundAmount(Double refundAmount) {
		this.refundAmount = refundAmount;
	}

	public Double getReducedAmount() {
		return reducedAmount;
	}

	public void setReducedAmount(Double reducedAmount) {
		this.reducedAmount = reducedAmount;
	}
	
	public Double getBaseAmount() {
		return baseAmount;
	}

	public void setBaseAmount(Double baseAmount) {
		this.baseAmount = baseAmount;
	}
	
	public String getDiffDays() {
		return diffDays;
	}

	public void setDiffDays(String diffDays) {
		this.diffDays = diffDays;
	}
	
	public String getGuestName() {
		return guestName;
	}

	public void setGuestName(String guestName) {
		this.guestName = guestName;
	}
	
	public String getCheckIn() {
		return checkIn;
	}

	public void setCheckIn(String checkIn) {
		this.checkIn = checkIn;
	}

	public String getCheckOut() {
		return checkOut;
	}

	public void setCheckOut(String checkOut) {
		this.checkOut = checkOut;
	}
	
	public String getDiffInDays() {
		return diffInDays;
	}

	public void setDiffInDays(String diffInDays) {
		this.diffInDays = diffInDays;
	}
	
	public String getTaxStatus() {
		return taxStatus;
	}

	public void setTaxStatus(String taxStatus) {
		this.taxStatus = taxStatus;
	}
	
	public String getFilterInvoiceDate() {
		return filterInvoiceDate;
	}

	public void setFilterInvoiceDate(String filterInvoiceDate) {
		this.filterInvoiceDate = filterInvoiceDate;
	}
	
	public String getOtaBookingId() {
		return otaBookingId;
	}

	public void setOtaBookingId(String otaBookingId) {
		this.otaBookingId = otaBookingId;
	}
	
	public String getSpecialRequest() {
		return specialRequest;
	}

	public void setSpecialRequest(String specialRequest) {
		this.specialRequest = specialRequest;
	}
	
	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}
	
	public Timestamp getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	
	public Double getAddonAmount() {
		return addonAmount;
	}

	public void setAddonAmount(Double addonAmount) {
		this.addonAmount = addonAmount;
	}

	public String getAddonDescription() {
		return addonDescription;
	}

	public void setAddonDescription(String addonDescription) {
		this.addonDescription = addonDescription;
	}

	public Double getPurchaseRate() {
		return purchaseRate;
	}

	public void setPurchaseRate(Double purchaseRate) {
		this.purchaseRate = purchaseRate;
	}

	public Timestamp getCheckInTime() {
		return checkInTime;
	}

	public void setCheckInTime(Timestamp checkInTime) {
		this.checkInTime = checkInTime;
	}

	public Timestamp getCheckOutTime() {
		return checkOutTime;
	}

	public void setCheckOutTime(Timestamp checkOutTime) {
		this.checkOutTime = checkOutTime;
	}

	public String getVoucherType() {
		return voucherType;
	}

	public void setVoucherType(String voucherType) {
		this.voucherType = voucherType;
	}

	public Integer getHours() {
		return hours;
	}

	public void setHours(Integer hours) {
		this.hours = hours;
	}

	public String getStrArrivalDate() {
		return strArrivalDate;
	}

	public void setStrArrivalDate(String strArrivalDate) {
		this.strArrivalDate = strArrivalDate;
	}

	public String getStrDepartureDate() {
		return strDepartureDate;
	}

	public void setStrDepartureDate(String strDepartureDate) {
		this.strDepartureDate = strDepartureDate;
	}

	public String getStrCheckInTime() {
		return strCheckInTime;
	}

	public void setStrCheckInTime(String strCheckInTime) {
		this.strCheckInTime = strCheckInTime;
	}

	public String getStrCheckOutTime() {
		return strCheckOutTime;
	}

	public void setStrCheckOutTime(String strCheckOutTime) {
		this.strCheckOutTime = strCheckOutTime;
	}

	public Double getPromotionAmount() {
		return promotionAmount;
	}

	public void setPromotionAmount(Double promotionAmount) {
		this.promotionAmount = promotionAmount;
	}

	public Double getCouponAmount() {
		return couponAmount;
	}

	public void setCouponAmount(Double couponAmount) {
		this.couponAmount = couponAmount;
	}

	public Double getExtraAdultRate() {
		return extraAdultRate;
	}

	public void setExtraAdultRate(Double extraAdultRate) {
		this.extraAdultRate = extraAdultRate;
	}

	public Double getExtraChildRate() {
		return extraChildRate;
	}

	public void setExtraChildRate(Double extraChildRate) {
		this.extraChildRate = extraChildRate;
	}
	
}