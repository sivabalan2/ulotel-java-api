package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import java.util.List;


/**
 * The persistent class for the state database table.
 * 
 */
@Entity
@Table(name="ota_hotel_details")
@NamedQuery(name="OtaHotelDetails.findAll", query="SELECT o FROM OtaHotelDetails o")
public class OtaHotelDetails implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="id",sequenceName="ota_hotel_details_ota_hotel_details_id_seq")
	@GeneratedValue(generator="id")
	@Column(name="ota_hotel_details_id")
	private Integer otaHotelDetailsId;	
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ota_hotel_id")
	private OtaHotels otaHotels;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="property_id")
	private PmsProperty pmsProperty;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="rate_plan_id")
	private PropertyRatePlan propertyRatePlan;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="accommodation_id")
	
	
	private PropertyAccommodation propertyAccommodation;	
	

	@Column(name="ota_hotel_code", length=1000)
	private String otaHotelCode;
	
	@Column(name="ota_room_type_id", length=1000)
	private String otaRoomTypeId;
	
	
	@Column(name="ota_rate_plan_id", length=1000)
	private String otaRateplanId;	
	
	@Column(name="is_active")
	private Boolean IsActive;
	
	public OtaHotelDetails() {
	}   
	
	
	public Integer getOtaHotelDetailsId() {
		return otaHotelDetailsId;
	}


	public void setOtaHotelDetailsId(Integer otaHotelDetailsId) {
		this.otaHotelDetailsId = otaHotelDetailsId;
	}
	

	public String getOtaHotelCode() {
		return otaHotelCode;
	}


	public void setOtaHotelCode(String otaHotelCode) {
		this.otaHotelCode = otaHotelCode;
	}


	public String getOtaRoomTypeId() {
		return otaRoomTypeId;
	}

	public void setOtaRoomTypeId(String otaRoomTypeId) {
		this.otaRoomTypeId = otaRoomTypeId;
	}

	public String getOtaRateplanId() {
		return otaRateplanId;
	}

	public void setOtaRateplanId(String otaRateplanId) {
		this.otaRateplanId = otaRateplanId;
	}

	public Boolean getIsActive() {
		return IsActive;
	}

	public void setIsActive(Boolean isActive) {
		IsActive = isActive;
	}	
			
	public PmsProperty getPmsProperty() {
		return pmsProperty;
	}

	public void setPmsProperty(PmsProperty pmsProperty) {
		this.pmsProperty = pmsProperty;
	}

	public OtaHotels getOtaHotels() {
		return otaHotels;
	}

	public void setOtaHotels(OtaHotels otaHotels) {
		this.otaHotels = otaHotels;
	}
	
	public PropertyAccommodation getPropertyAccommodation() {
		return propertyAccommodation;
	}

	public void setPropertyAccommodation(PropertyAccommodation propertyAccommodation) {
		this.propertyAccommodation = propertyAccommodation;
	}


	public PropertyRatePlan getPropertyRatePlan() {
		return propertyRatePlan;
	}


	public void setPropertyRatePlan(PropertyRatePlan propertyRatePlan) {
		this.propertyRatePlan = propertyRatePlan;
	}



	

}