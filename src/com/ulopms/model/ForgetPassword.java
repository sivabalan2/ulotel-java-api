package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;


/**
 * The persistent class for the forget_password database table.
 * 
 */
@Entity
@Table(name="forget_password")
@NamedQuery(name="ForgetPassword.findAll", query="SELECT f FROM ForgetPassword f")
public class ForgetPassword implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="id",sequenceName="forget_password_link_generation_forget_password_id_seq")
	@GeneratedValue(generator="id")
	@Column(name="forget_password_id", unique=true, nullable=false)
	private Integer forgetPasswordId;

	@Column(name="created_on")
	private Timestamp createdOn;

	@Column(name="expiry_on")
	private Timestamp expiryOn;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="password_link", length=250)
	private String passwordLink;

	//bi-directional many-to-one association to User
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="user_id", nullable=false)
	private User user;

	public ForgetPassword() {
	}

	public Integer getForgetPasswordId() {
		return this.forgetPasswordId;
	}

	public void setForgetPasswordId(Integer forgetPasswordId) {
		this.forgetPasswordId = forgetPasswordId;
	}

	public Timestamp getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public Timestamp getExpiryOn() {
		return this.expiryOn;
	}

	public void setExpiryOn(Timestamp expiryOn) {
		this.expiryOn = expiryOn;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getPasswordLink() {
		return this.passwordLink;
	}

	public void setPasswordLink(String passwordLink) {
		this.passwordLink = passwordLink;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}