package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the pms_amenities database table.
 * 
 */
@Entity
@Table(name="location_content")
@NamedQuery(name="LocationContent.findAll", query="SELECT p FROM LocationContent p")
public class LocationContent implements Serializable {
	private static final long serialVersionUID = 1L;
 
	
	@Id  
    @SequenceGenerator(name="id",sequenceName="location_content_location_content_id_seq")
	@GeneratedValue(generator="id")
	@Column(name="location_content_id", unique=true, nullable=false)
	private Integer locationContentId;

	@Column(name="meta_description", length=10485760)
	private String metaDescription;
	
	@Column(name="schema_content", length=10485760)
	private String schemaContent;

	@Column(name="location_description", length=10485760)
	private String locationDescription;

	@Column(name="script_content", length=10485760)
	private String scriptContent;
	
	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_deleted")
	private Boolean isDeleted;	
	

		//bi-directional many-to-one association to Location
		@ManyToOne(fetch=FetchType.EAGER)
		@JoinColumn(name="location_id")		
		private Location location;

	public LocationContent() {
	}

	public Integer getLocationContentId() {
		return locationContentId;
	}

	public void setLocationContentId(Integer locationContentId) {
		this.locationContentId = locationContentId;
	}

	public String getMetaDescription() {
		return metaDescription;
	}

	public void setMetaDescription(String metaDescription) {
		this.metaDescription = metaDescription;
	}

	public String getSchemaContent() {
		return schemaContent;
	}

	public void setSchemaContent(String schemaContent) {
		this.schemaContent = schemaContent;
	}

	public String getLocationDescription() {
		return locationDescription;
	}

	public void setLocationDescription(String locationDescription) {
		this.locationDescription = locationDescription;
	}

	public String getScriptContent() {
		return scriptContent;
	}

	public void setScriptContent(String scriptContent) {
		this.scriptContent = scriptContent;
	}
	
	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	
	public Location getLocation() {
		return this.location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	
}