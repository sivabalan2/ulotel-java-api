package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;


/**
 * The persistent class for the property_rate_details database table.
 * 
 */
@Entity
@Table(name="pms_promotion_details")
@NamedQuery(name="PmsPromotionDetails.findAll", query="SELECT p FROM PmsPromotionDetails p")
public class PmsPromotionDetails implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="id",sequenceName="pms_promotion_details_promotion_detail_id_seq")
	@GeneratedValue(generator="id")
	@Column(name="promotion_detail_id")
	private Integer promotionDetailId;

	@Column(name="base_amount")
	private double baseAmount;

	@Column(name="created_by")
	private Integer createdBy;

	@Column(name="created_date")
	private Timestamp createdDate;
	
	@Column(name="modified_by")
	private Integer modifiedBy;

	@Column(name="modified_date")
	private Timestamp modifiedDate;

	@Column(name="day_part")
	private Integer dayPart;

	@Column(name="days_of_week")
	private String daysOfWeek;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_deleted")
	private Boolean isDeleted;
	
	@Column(name="extra_adult", precision=131089)
	private Double extraAdult;

	@Column(name="extra_child", precision=131089)
	private Double extraChild;
	
	@Column(name="book_rooms", precision=131089)
	private Double bookRooms;
	
	@Column(name="get_rooms", precision=131089)
	private Double getRooms;
	
	@Column(name="book_nights", precision=131089)
	private Double bookNights;
	
	@Column(name="get_nights", precision=131089)
	private Double getNights;
	
	@Column(name="promotion_percentage", precision=131089)
	private Double promotionPercentage;
	
	@Column(name="promotion_hours", precision=131089)
	private Integer promotionHours;
	
	@Column(name="promotion_inr", precision=131089)
	private Double promotionInr;
	
	

	@ManyToOne
	@JoinColumn(name="promotion_id")
	private PmsPromotions pmsPromotions;

	public PmsPromotionDetails() {
	}

	public Integer getPromotionDetailsId() {
		return this.promotionDetailId;
	}

	public void setPromotionDetailsId(Integer promotionDetailId) {
		this.promotionDetailId = promotionDetailId;
	}

	public double getBaseAmount() {
		return this.baseAmount;
	}

	public void setBaseAmount(double baseAmount) {
		this.baseAmount = baseAmount;
	}

	public Integer getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedDate() {
		return this.createdDate;
	}
	
	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}
	
	public Integer getModifiedBy() {
		return this.modifiedBy;
	}
	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Timestamp getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public Integer getDayPart() {
		return this.dayPart;
	}

	public void setDayPart(Integer dayPart) {
		this.dayPart = dayPart;
	}

	public String getDaysOfWeek() {
		return this.daysOfWeek;
	}

	public void setDaysOfWeek(String daysOfWeek) {
		this.daysOfWeek = daysOfWeek;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public PmsPromotions getPmsPromotions() {
		return this.pmsPromotions;
	}

	public void setPmsPromotions(PmsPromotions pmsPromotions) {
		this.pmsPromotions = pmsPromotions;
	}

	public Double getExtraAdult() {
		return this.extraAdult;
	}

	public void setExtraAdult(Double extraAdult) {
		this.extraAdult = extraAdult;
	}
	
	
	public Double getExtraChild() {
		return this.extraChild;
	}

	public void setExtraChild(Double extraChild) {
		this.extraChild = extraChild;
	}
	
	public Double getPromotionPercentage(){
		return this.promotionPercentage;
	}
	
	public void setPromotionPercentage(Double promotionPercentage)
	{
		this.promotionPercentage=promotionPercentage;
	}

	public Integer getPromotionHours(){
		return promotionHours;
	}
	
	public void setPromotionHours(Integer promotionHours){
		this.promotionHours=promotionHours;
	}
	
	public Double getBookRooms() {
		return bookRooms;
	}

	public void setBookRooms(Double bookRooms) {
		this.bookRooms = bookRooms;
	}

	public Double getGetRooms() {
		return getRooms;
	}

	public void setGetRooms(Double getRooms) {
		this.getRooms = getRooms;
	}

	public Double getBookNights() {
		return bookNights;
	}

	public void setBookNights(Double bookNights) {
		this.bookNights = bookNights;
	}

	public Double getGetNights() {
		return getNights;
	}

	public void setGetNights(Double getNights) {
		this.getNights = getNights;
	}
	
	public Double getPromotionInr() {
		return promotionInr;
	}

	public void setPromotionInr(Double promotionInr) {
		this.promotionInr = promotionInr;
	}
}