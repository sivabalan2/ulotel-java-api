package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;


/**
 * The persistent class for the property_commission_invoice database table.
 * 
 */
@Entity
@Table(name="property_reviews")
@NamedQuery(name="PropertyReviews.findAll", query="SELECT p FROM PropertyReviews p")
public class PropertyReviews implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="id",sequenceName="property_reviews_property_review_id_seq")
	@GeneratedValue(generator="id")
	@Column(name="property_review_id", unique=true, nullable=false)
	private Integer propertyReviewId;

	@Column(name="star_count")
	private Integer starCount;

	@Column(name="review_count")
	private Double reviewCount;

	@Column(name="average_review")
	private Double averageReview;
	
	@Column(name="tripadvisor_star_count")
	private Integer tripadvisorStarCount;

	@Column(name="tripadvisor_review_count")
	private Double tripadvisorReviewCount;

	@Column(name="tripadvisor_average_review")
	private Double tripadvisorAverageReview;

	@Column(name="review_image_path", length=250)
	private String reviewImagePath;
	
	@Column(name="created_by")
	private Integer createdBy;

	@Column(name="created_date")
	private Timestamp createdDate;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_deleted")
	private Boolean isDeleted;

	@Column(name="modified_by")
	private Integer modifiedBy;

	@Column(name="modified_date")
	private Timestamp modifiedDate;
	
	@Column(name="google_review_url")
	private String googleReviewUrl;
	
	@Column(name="tripadvisor_review_url")
	private String tripadvisorReviewUrl;
	
	@Column(name="property_rating")
	private String propertyRating;
	
	//bi-directional many-to-one association to PmsProperty
	@ManyToOne
	@JoinColumn(name="property_id")
	private PmsProperty pmsProperty;


	public Integer getPropertyReviewId() {
		return propertyReviewId;
	}

	public void setPropertyReviewId(Integer propertyReviewId) {
		this.propertyReviewId = propertyReviewId;
	}

	public Integer getStarCount() {
		return starCount;
	}

	public void setStarCount(Integer starCount) {
		this.starCount = starCount;
	}

	public Double getReviewCount() {
		return reviewCount;
	}

	public void setReviewCount(Double reviewCount) {
		this.reviewCount = reviewCount;
	}

	public Double getAverageReview() {
		return averageReview;
	}

	public void setAverageReview(Double averageReview) {
		this.averageReview = averageReview;
	}

	public PmsProperty getPmsProperty() {
		return pmsProperty;
	}

	public void setPmsProperty(PmsProperty pmsProperty) {
		this.pmsProperty = pmsProperty;
	}
	

	public String getReviewImagePath() {
		return reviewImagePath;
	}

	public void setReviewImagePath(String reviewImagePath) {
		this.reviewImagePath = reviewImagePath;
	}
	
	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Integer getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Timestamp getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	
	public Integer getTripadvisorStarCount() {
		return tripadvisorStarCount;
	}

	public void setTripadvisorStarCount(Integer tripadvisorStarCount) {
		this.tripadvisorStarCount = tripadvisorStarCount;
	}

	public Double getTripadvisorReviewCount() {
		return tripadvisorReviewCount;
	}

	public void setTripadvisorReviewCount(Double tripadvisorReviewCount) {
		this.tripadvisorReviewCount = tripadvisorReviewCount;
	}

	public Double getTripadvisorAverageReview() {
		return tripadvisorAverageReview;
	}

	public void setTripadvisorAverageReview(Double tripadvisorAverageReview) {
		this.tripadvisorAverageReview = tripadvisorAverageReview;
	}
	
	public String getGoogleReviewUrl() {
		return googleReviewUrl;
	}

	public void setGoogleReviewUrl(String googleReviewUrl) {
		this.googleReviewUrl = googleReviewUrl;
	}

	public String getTripadvisorReviewUrl() {
		return tripadvisorReviewUrl;
	}

	public void setTripadvisorReviewUrl(String tripadvisorReviewUrl) {
		this.tripadvisorReviewUrl = tripadvisorReviewUrl;
	}

	public String getPropertyRating() {
		return propertyRating;
	}

	public void setPropertyRating(String propertyRating) {
		this.propertyRating = propertyRating;
	}

}