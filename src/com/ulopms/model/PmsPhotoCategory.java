package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the pms_photo_category database table.
 * 
 */
@Entity
@Table(name="pms_photo_category")
@NamedQuery(name="PmsPhotoCategory.findAll", query="SELECT p FROM PmsPhotoCategory p")
public class PmsPhotoCategory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="id",sequenceName="pms_photo_category_photo_category_id_seq")
	@GeneratedValue(generator="id")
	@Column(name="photo_category_id")
	private Integer photoCategoryId;	
	

	@Column(name="photo_category_name")
	private String photoCategoryName;	

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_deleted")
	private Boolean isDeleted;
	
	@Column(name="accommodation_tag_is_active")
	private Boolean accommodationTagIsActive;
    
	//bi-directional many-to-one association to PmsBooking
			@OneToMany(mappedBy="pmsPhotoCategory")
			private List<PropertyPhoto> propertyPhoto;

			
			public PmsPhotoCategory() {
			}

		public List<PropertyPhoto> getPropertyPhoto() {
				return propertyPhoto;
			}

			public void setPropertyPhoto(List<PropertyPhoto> propertyPhoto) {
				this.propertyPhoto = propertyPhoto;
			}		
		

			public Integer getPhotoCategoryId() {
				return photoCategoryId;
			}

			public void setPhotoCategoryId(Integer photoCategoryId) {
				this.photoCategoryId = photoCategoryId;
			}
			
			public String getPhotoCategoryName() {
				return photoCategoryName;
			}

			public void setPhotoCategoryName(String photoCategoryName) {
				this.photoCategoryName = photoCategoryName;
			}

			
			public Boolean getIsActive() {
				return isActive;
			}

			public void setIsActive(Boolean isActive) {
				this.isActive = isActive;
			}

			public Boolean getIsDeleted() {
				return isDeleted;
			}

			public void setIsDeleted(Boolean isDeleted) {
				this.isDeleted = isDeleted;
			}

			public Boolean getAccommodationTagIsActive() {
				return accommodationTagIsActive;
			}

			public void setAccommodationTagIsActive(Boolean accommodationTagIsActive) {
				this.accommodationTagIsActive = accommodationTagIsActive;
			}

}