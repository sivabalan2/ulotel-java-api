package com.ulopms.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * The persistent class for the pms_smart_price database table.
 * 
 */
@Entity
@Table(name="property_addon_details")
@NamedQuery(name="PropertyAddonDetails.findAll", query="SELECT p FROM PropertyAddonDetails p")


public class PropertyAddonDetails implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="id",sequenceName="property_addon_details_property_addon_detail_id_seq")
	@GeneratedValue(generator="id")
	
	@Column(name="property_addon_detail_id")
	private Integer propertyAddonDetailId;
	
	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_deleted")
	private Boolean isDeleted;
	
	@Column(name="addon_is_active")
	private Boolean addonIsActive;
	
	@Column(name="addon_rate")
	private double addonRate;

	@Column(name="created_by")
	private Integer createdBy;

	@Column(name="created_on")
	private Timestamp createdOn;
	
	@Column(name="modified_by")
	private Integer modifiedBy;

	@Column(name="modified_on")
	private Timestamp modifiedOn;
		
	@ManyToOne
	@JoinColumn(name="property_id")
	private PmsProperty pmsProperty;
	
	@ManyToOne
	@JoinColumn(name="property_addon_id")
	private PropertyAddon propertyAddon;

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	
	public Integer getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}
	
	public Integer getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Timestamp getModifiedOn() {
		return this.modifiedOn;
	}

	public void setModifiedOn(Timestamp modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
	
	public Integer getPropertyAddonDetailId() {
		return propertyAddonDetailId;
	}

	public void setPropertyAddonDetailId(Integer propertyAddonDetailId) {
		this.propertyAddonDetailId = propertyAddonDetailId;
	}


	public PmsProperty getPmsProperty() {
		return pmsProperty;
	}

	public void setPmsProperty(PmsProperty pmsProperty) {
		this.pmsProperty = pmsProperty;
	}

	public PropertyAddon getPropertyAddon() {
		return propertyAddon;
	}

	public void setPropertyAddon(PropertyAddon propertyAddon) {
		this.propertyAddon = propertyAddon;
	}

	public double getAddonRate() {
		return addonRate;
	}

	public void setAddonRate(double addonRate) {
		this.addonRate = addonRate;
	}
	
	public Boolean getAddonIsActive() {
		return addonIsActive;
	}

	public void setAddonIsActive(Boolean addonIsActive) {
		this.addonIsActive = addonIsActive;
	}


}