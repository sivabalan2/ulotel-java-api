package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the pms_amenities database table.
 * 
 */
@Entity
@Table(name="property_content")
@NamedQuery(name="PropertyContent.findAll", query="SELECT p FROM PropertyContent p")
public class PropertyContent implements Serializable {
	private static final long serialVersionUID = 1L;
 
	
	@Id  
    @SequenceGenerator(name="id",sequenceName="location_content_location_content_id_seq")
	@GeneratedValue(generator="id")
	@Column(name="property_content_id", unique=true, nullable=false)
	private Integer propertyContentId;

	@Column(name="meta_description", length=10485760)
	private String metaDescription;
	
	@Column(name="schema_content", length=10485760)
	private String schemaContent;

	@Column(name="property_description", length=10485760)
	private String propertyDescription;

	@Column(name="script_content", length=10485760)
	private String scriptContent;
	
	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_deleted")
	private Boolean isDeleted;
	
	//bi-directional many-to-one association to Location
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="property_id")		
	private PmsProperty pmsProperty;

	
	
	public PropertyContent() {
	}

	public Integer getLocationContentId() {
		return propertyContentId;
	}

	public void setLocationContentId(Integer propertyContentId) {
		this.propertyContentId = propertyContentId;
	}

	public String getMetaDescription() {
		return metaDescription;
	}

	public void setMetaDescription(String metaDescription) {
		this.metaDescription = metaDescription;
	}

	public String getSchemaContent() {
		return schemaContent;
	}

	public void setSchemaContent(String schemaContent) {
		this.schemaContent = schemaContent;
	}

	public String getPropertyDescription() {
		return propertyDescription;
	}

	public void setPropertyDescription(String propertyDescription) {
		this.propertyDescription = propertyDescription;
	}

	public String getScriptContent() {
		return scriptContent;
	}

	public void setScriptContent(String scriptContent) {
		this.scriptContent = scriptContent;
	}
	
	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	
	public PmsProperty getPmsProperty() {
		return pmsProperty;
	}

	public void setPmsProperty(PmsProperty pmsProperty) {
		this.pmsProperty = pmsProperty;
	}


	
}