package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import java.util.List;


/**
 * The persistent class for the state database table.
 * 
 */
@Entity
@Table(name="ota_hotels")
@NamedQuery(name="OtaHotels.findAll", query="SELECT o FROM OtaHotels o")
public class OtaHotels implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="id",sequenceName="ota_hotels_ota_hotel_id_seq")
	@GeneratedValue(generator="id")	
	@Column(name="ota_hotel_id")
	private Integer otaHotelId;	
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="source_id")
	private PmsSource pmsSource;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="property_id")
	private PmsProperty pmsProperty;
	
	//bi-directional many-to-one association to OtaHotelDetails
	@OneToMany(mappedBy="otaHotels")
	private List<OtaHotelDetails> otaHotelDetails;

	@Column(name="ota_hotel_code", length=1000)
	private String otaHotelCode;
	
	@Column(name="ota_bearer_token", length=1000)
	private String otaBearerToken;	
	
	@Column(name="ota_channel_token", length=1000)
	private String otaChannelToken;
	
	@Column(name="ota_username", length=1000)
	private String otaUserName;
	
	@Column(name="ota_password", length=1000)
	private String otaPassword;
	
	@Column(name="ota_is_active")
	private Boolean otaIsActive;
	
	public OtaHotels() {
	}    
	
	public Integer getOtaHotelId() {
		return this.otaHotelId;
	}

	public void setOtaHotelId(Integer otaHotelId) {
		this.otaHotelId = otaHotelId;
	}
	
	
	public String getOtaHotelCode() {
		return this.otaHotelCode;
	}

	public void setOtaHotelCode(String otaHotelCode) {
		this.otaHotelCode = otaHotelCode;
	}

	public String getOtaBearerToken() {
		return this.otaBearerToken;
	}

	public void setOtaBearerToken(String otaBearerToken) {
		this.otaBearerToken = otaBearerToken;
	}
	
	public String getOtaChannelToken() {
		return this.otaChannelToken;
	}

	public void setOtaChannelToken(String otaChannelToken) {
		this.otaChannelToken = otaChannelToken;
	}
	
	public String getOtaUserName() {
		return this.otaUserName;
	}

	public void setOtaUserName(String otaUserName) {
		this.otaUserName = otaUserName;
	}
	
	public String getOtaPassword() {
		return this.otaPassword;
	}
	
	public void setOtaPassword(String otaPassword) {
		this.otaPassword = otaPassword;
	}
	
	public Boolean getOtaIsActive() {
		return this.otaIsActive;
	}

	public void setOtaIsActive(Boolean otaIsActive) {
		this.otaIsActive = otaIsActive;
	}
	
	public PmsSource getPmsSource() {
		return pmsSource;
	}

	public void setPmsSource(PmsSource pmsSource) {
		this.pmsSource = pmsSource;
	}

	public PmsProperty getPmsProperty() {
		return pmsProperty;
	}

	public void setPmsProperty(PmsProperty pmsProperty) {
		this.pmsProperty = pmsProperty;
	}
	
	public List<OtaHotelDetails> getOtaHotelDetails() {
		return otaHotelDetails;
	}

	public void setOtaHotelDetails(List<OtaHotelDetails> otaHotelDetails) {
		this.otaHotelDetails = otaHotelDetails;
	}
	

}