
package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;
import java.util.List;


public class AccommodationRoom implements Serializable {
	private static final long serialVersionUID = 1L;

	
	private Integer accommodationId;

	private String accommodationType;
	
	private String roomAlias;

	private String sortKey;

	private Integer roomId;
 
	private long roomCount;	
	
	private Boolean isActive;
	
	private Boolean isDeleted;
	
	private Timestamp modifiedDate;
	
	private Integer blogArticleId;
	private Integer blogLocationId;
	private Integer blogCategoryId;
	private String blogArticleTitle;
	private String blogArticleDescription;
	private String blogArticleUrl;
	

	public Integer getBlogArticleId() {
		return blogArticleId;
	}

	public void setBlogArticleId(Integer blogArticleId) {
		this.blogArticleId = blogArticleId;
	}

	public Integer getBlogLocationId() {
		return blogLocationId;
	}

	public void setBlogLocationId(Integer blogLocationId) {
		this.blogLocationId = blogLocationId;
	}

	public Integer getBlogCategoryId() {
		return blogCategoryId;
	}

	public void setBlogCategoryId(Integer blogCategoryId) {
		this.blogCategoryId = blogCategoryId;
	}

	public String getBlogArticleTitle() {
		return blogArticleTitle;
	}

	public void setBlogArticleTitle(String blogArticleTitle) {
		this.blogArticleTitle = blogArticleTitle;
	}

	public String getBlogArticleDescription() {
		return blogArticleDescription;
	}

	public void setBlogArticleDescription(String blogArticleDescription) {
		this.blogArticleDescription = blogArticleDescription;
	}

	
	
	public Integer getRoomId() {
		return this.roomId;
	}

	public void setRoomId(Integer roomId) {
		this.roomId = roomId;
	}
	
	public long getRoomCount() {
		return this.roomCount;
	}

	public void setRoomCount(long roomCount) {
		this.roomCount = roomCount;
	}

	public String getRoomAlias() {
		return this.roomAlias;
	}

	public void setRoomAlias(String roomAlias) {
		this.roomAlias = roomAlias;
	}

	public String getSortKey() {
		return this.sortKey;
	}

	public void setSortKey(String sortKey) {
		this.sortKey = sortKey;
	}
	
	public Integer getAccommodationId() {
		return this.accommodationId;
	}

	public void setAccommodationId(Integer accommodationId) {
		this.accommodationId = accommodationId;
	}
	
	public String getAccommodationType() {
		return this.accommodationType;
	}

	public void setAccommodationType(String accommodationType) {
		this.accommodationType = accommodationType;
	}
	
	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	
	public Timestamp getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getBlogArticleUrl() {
		return blogArticleUrl;
	}

	public void setBlogArticleUrl(String blogArticleUrl) {
		this.blogArticleUrl = blogArticleUrl;
	}
	
}