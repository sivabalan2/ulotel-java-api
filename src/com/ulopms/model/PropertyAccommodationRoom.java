package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the property_accommodation_rooms database table.
 * 
 */
@Entity
@Table(name="property_accommodation_rooms")
@NamedQuery(name="PropertyAccommodationRoom.findAll", query="SELECT p FROM PropertyAccommodationRoom p")
public class PropertyAccommodationRoom implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="room_id", unique=true, nullable=false)
	private Integer roomId;

	@Column(name="area_sqft", length=10)
	private String areaSqft;

	@Column(name="created_by")
	private Integer createdBy;

	@Column(name="created_date")
	private Timestamp createdDate;

	@Column(name="data_extension", length=10)
	private String dataExtension;

	@Column(length=10)
	private String floor;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_deleted")
	private Boolean isDeleted;

	@Column(name="modified_by")
	private Integer modifiedBy;

	@Column(name="modified_date")
	private Timestamp modifiedDate;

	@Column(name="phone_extension", length=10)
	private String phoneExtension;

	@Column(length=500)
	private String remark;

	@Column(name="room_alias", length=20)
	private String roomAlias;

	@Column(name="sort_key", length=10)
	private String sortKey;

	//bi-directional many-to-one association to PropertyAccommodation
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="accommodation_id")
	private PropertyAccommodation propertyAccommodation;

	public PropertyAccommodationRoom() {
	}

	public Integer getRoomId() {
		return this.roomId;
	}

	public void setRoomId(Integer roomId) {
		this.roomId = roomId;
	}

	public String getAreaSqft() {
		return this.areaSqft;
	}

	public void setAreaSqft(String areaSqft) {
		this.areaSqft = areaSqft;
	}

	public Integer getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public String getDataExtension() {
		return this.dataExtension;
	}

	public void setDataExtension(String dataExtension) {
		this.dataExtension = dataExtension;
	}

	public String getFloor() {
		return this.floor;
	}

	public void setFloor(String floor) {
		this.floor = floor;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Integer getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Timestamp getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getPhoneExtension() {
		return this.phoneExtension;
	}

	public void setPhoneExtension(String phoneExtension) {
		this.phoneExtension = phoneExtension;
	}

	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getRoomAlias() {
		return this.roomAlias;
	}

	public void setRoomAlias(String roomAlias) {
		this.roomAlias = roomAlias;
	}

	public String getSortKey() {
		return this.sortKey;
	}

	public void setSortKey(String sortKey) {
		this.sortKey = sortKey;
	}

	public PropertyAccommodation getPropertyAccommodation() {
		return this.propertyAccommodation;
	}

	public void setPropertyAccommodation(PropertyAccommodation propertyAccommodation) {
		this.propertyAccommodation = propertyAccommodation;
	}
	
	

}