package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the seo_content database table.
 * 
 */
@Entity
@Table(name="seo_content")
@NamedQuery(name="SeoContent.findAll", query="SELECT p FROM SeoContent p")
public class SeoContent implements Serializable {
	private static final long serialVersionUID = 1L;
 
	
	@Id  
    @SequenceGenerator(name="id",sequenceName="seo_content_seo_content_id_seq")
	@GeneratedValue(generator="id")
	@Column(name="seo_content_id", unique=true, nullable=false)
	private Integer seoContentId;

	@Column(name="title", length=10485760)
	private String title;
	
	@Column(name="description", length=10485760)
	private String description;

	@Column(name="content", length=10485760)
	private String content;

	@Column(name="keywords", length=10485760)
	private String keywords;
	
	@Column(name="h1", length=10485760)
	private String h1;
	
	@Column(name="h2", length=10485760)
	private String h2;
	
	@Column(name="h3", length=10485760)
	private String h3;
	
	@Column(name="h4", length=10485760)
	private String h4;
	
	@Column(name="short_description", length=10485760)
	private String shortDescription;
	
	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_deleted")
	private Boolean isDeleted;	
	
	@Column(name="social_meta_tag", length=10485760)
	private String socialMetaTag;
	
	
	//bi-directional many-to-one association to Location
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="property_id")		
	private PmsProperty pmsProperty;

	//bi-directional many-to-one association to Location
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="location_id")		
	private Location location;
	
	//bi-directional many-to-one association to area
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="area_id")		
	private Area area;

	//bi-directional many-to-one association to blog article
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="blog_article_id")		
	private BlogArticles blogArticles;
	
	//bi-directional many-to-one association to GoogleLocation
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="google_location_id")		
	private GoogleLocation googleLocation;
		
	//bi-directional many-to-one association to GoogleLocation
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="google_area_id")		
	private GoogleArea googleArea;

	public SeoContent() {
	}

	
	
	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	
	public Integer getSeoContentId() {
		return seoContentId;
	}



	public void setSeoContentId(Integer seoContentId) {
		this.seoContentId = seoContentId;
	}



	public String getTitle() {
		return title;
	}



	public void setTitle(String title) {
		this.title = title;
	}



	public String getDescription() {
		return description;
	}



	public void setDescription(String description) {
		this.description = description;
	}



	public String getContent() {
		return content;
	}



	public void setContent(String content) {
		this.content = content;
	}



	public String getKeywords() {
		return keywords;
	}



	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}



	public String getH1() {
		return h1;
	}



	public void setH1(String h1) {
		this.h1 = h1;
	}



	public String getH2() {
		return h2;
	}



	public void setH2(String h2) {
		this.h2 = h2;
	}



	public String getH3() {
		return h3;
	}



	public void setH3(String h3) {
		this.h3 = h3;
	}



	public String getH4() {
		return h4;
	}



	public void setH4(String h4) {
		this.h4 = h4;
	}



	public String getShortDescription() {
		return shortDescription;
	}



	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}



	public PmsProperty getPmsProperty() {
		return pmsProperty;
	}



	public void setPmsProperty(PmsProperty pmsProperty) {
		this.pmsProperty =pmsProperty;
	}



	public Location getLocation() {
		return this.location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}
	
	public String getSocialMetaTag() {
		return socialMetaTag;
	}

	public void setSocialMetaTag(String socialMetaTag) {
		this.socialMetaTag = socialMetaTag;
	}

	public Area getArea() {
		return area;
	}

	public void setArea(Area area) {
		this.area = area;
	}
	
	public BlogArticles getBlogArticles() {
		return blogArticles;
	}

	public void setBlogArticles(BlogArticles blogArticles) {
		this.blogArticles = blogArticles;
	}

	public GoogleLocation getGoogleLocation() {
		return googleLocation;
	}

	public void setGoogleLocation(GoogleLocation googleLocation) {
		this.googleLocation = googleLocation;
	}

	public GoogleArea getGoogleArea() {
		return googleArea;
	}

	public void setGoogleArea(GoogleArea googleArea) {
		this.googleArea = googleArea;
	}
	
}