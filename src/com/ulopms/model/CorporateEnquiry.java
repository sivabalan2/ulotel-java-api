package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;
import java.util.List;
import java.util.Set;


/**
 * The persistent class for the mst_users database table.
 * 
 */
@Entity
@Table(name="corporate_enquiry",schema="public")
@NamedQuery(name="CorporateEnquiry.findAll", query="SELECT p FROM CorporateEnquiry p")
public class CorporateEnquiry implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="id",sequenceName="corporate_enquiry_corporate_id_seq")
	@GeneratedValue(generator="id")
	@Column(name="corporate_id", unique=true, nullable=false)
	private Integer corporateId;

	@Column(name="employee_name")
	private String employeeName;

	@Column(name="corporate_name")
	private String corporateName;
	
	@Column(name="corporate_number")
	private String corporateNumber;
	
	@Column(name="corporate_email")
	private String corporateEmail;

	@Column(name="created_date")
	private Timestamp createdDate;
	
	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_deleted")
	private Boolean isDeleted;

	

	public CorporateEnquiry() {
	}



	public Integer getCorporateId() {
		return corporateId;
	}

	public void setCorporateId(Integer corporateId) {
		this.corporateId = corporateId;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public String getCorporateName() {
		return corporateName;
	}

	public void setCorporateName(String corporateName) {
		this.corporateName = corporateName;
	}

	public String getCorporateNumber() {
		return corporateNumber;
	}

	public void setCorporateNumber(String corporateNumber) {
		this.corporateNumber = corporateNumber;
	}

	public String getCorporateEmail() {
		return corporateEmail;
	}

	public void setCorporateEmail(String corporateEmail) {
		this.corporateEmail = corporateEmail;
	}

	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	}