package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the property_item_category database table.
 * 
 */
@Entity
@Table(name="property_item_category")
@NamedQuery(name="PropertyItemCategory.findAll", query="SELECT p FROM PropertyItemCategory p")
public class PropertyItemCategory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="id",sequenceName="property_item_category_property_item_category_id_seq")
	@GeneratedValue(generator="id")
	@Column(name="property_item_category_id", unique=true, nullable=false)
	private Integer propertyItemCategoryId;

	@Column(name="category_code", length=10)
	private String categoryCode;

	@Column(name="category_name", length=100)
	private String categoryName;

	@Column(name="created_by")
	private Integer createdBy;

	@Column(name="created_date")
	private Timestamp createdDate;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_deleted")
	private Boolean isDeleted;

	@Column(name="modified_by")
	private Integer modifiedBy;

	@Column(name="modified_date")
	private Timestamp modifiedDate;

	//bi-directional many-to-one association to PmsProperty
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="property_id")
	private PmsProperty pmsProperty;

	//bi-directional many-to-one association to PropertyItem
	@OneToMany(mappedBy="propertyItemCategory")
	private List<PropertyItem> propertyItems;

	public PropertyItemCategory() {
	}

	public Integer getPropertyItemCategoryId() {
		return this.propertyItemCategoryId;
	}

	public void setPropertyItemCategoryId(Integer propertyItemCategoryId) {
		this.propertyItemCategoryId = propertyItemCategoryId;
	}

	public String getCategoryCode() {
		return this.categoryCode;
	}

	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}

	public String getCategoryName() {
		return this.categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public Integer getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Integer getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Timestamp getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public PmsProperty getPmsProperty() {
		return this.pmsProperty;
	}

	public void setPmsProperty(PmsProperty pmsProperty) {
		this.pmsProperty = pmsProperty;
	}

	public List<PropertyItem> getPropertyItems() {
		return this.propertyItems;
	}

	public void setPropertyItems(List<PropertyItem> propertyItems) {
		this.propertyItems = propertyItems;
	}

	public PropertyItem addPropertyItem(PropertyItem propertyItem) {
		getPropertyItems().add(propertyItem);
		propertyItem.setPropertyItemCategory(this);

		return propertyItem;
	}

	public PropertyItem removePropertyItem(PropertyItem propertyItem) {
		getPropertyItems().remove(propertyItem);
		propertyItem.setPropertyItemCategory(null);

		return propertyItem;
	}

}