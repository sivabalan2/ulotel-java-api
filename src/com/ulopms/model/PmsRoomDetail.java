package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the pms_booking database table.
 * 
 */

public class PmsRoomDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	
	
	
	private Integer roomId;
	
	private Integer accommodationId;
    
	private String accommodationType;
	
	private Timestamp arrivalDate;

	private Timestamp departureDate;

	private Boolean isActive;

	private Boolean isDeleted;


	

	//
	public Integer getRoomId() {
		return this.roomId;
	}

	public void setRoomId(Integer roomId) {
		this.roomId = roomId;
	}
	
	
	public Integer getAccommodationId() {
		return this.accommodationId;
	}

	public void setAccommodationId(Integer accommodationId) {
		this.accommodationId = accommodationId;
	}
	
	public String getAccommodationType() {
			return this.accommodationType;
	}

	public void setAccommodationType(String accommodationType) {
			this.accommodationType = accommodationType;
	}
	
	
	
	public Timestamp getArrivalDate() {
		return this.arrivalDate;
	}

	public void setArrivalDate(Timestamp arrivalDate) {
		this.arrivalDate = arrivalDate;
	}
	
	public Timestamp getDepartureDate() {
		return this.departureDate;
	}

	public void setDepartureDate(Timestamp departureDate) {
		this.departureDate = departureDate;
	}
  

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}


}