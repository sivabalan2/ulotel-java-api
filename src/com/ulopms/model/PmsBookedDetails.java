package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the pms_booking database table.
 * 
 */

public class PmsBookedDetails implements Serializable {
	private static final long serialVersionUID = 1L;

	
	
	
	private Integer propertyId;
	
	private Integer propertyDiscountId;	
	
	private Integer promotionId;	

	private Long roomCount;
	
	private Integer guestId;
	
	private Integer bookingId;
	
	private Integer statusId;
	
    private Integer sourceId;
    
	private Integer accommodationId;
    
	private String accommodationType;
	
	private Integer maxOccupancy;
	
	private double adultsIncludedRate;
	
	private double childIncludedRate;
	
	private long adults;

	private double amount;

    private long child;
	
	private double extraAdult;
	
	private double extraChild;
	
	private double baseAmount;
	
	private Integer noOfUnits;
	
	private Long available;
	
	private Timestamp arrivalDate;

	private Timestamp departureDate;

	private Integer rooms;
	
	private double refund;

    private double advanceAmount;
    
    private Double otaTax;
    
    private Double otaCommission;
    
	private double tax;
	
	private Boolean isActive;

	private Boolean isDeleted;

    private String specialRequest;
	
    private Timestamp createdDate;
    
    private String checkIn;
    
    private String checkOut;
    
    private String bookingDate;
    
    private String promotionFirstName;
    
    private String promotionSecondName;
    
    private String couponName;
    
    private Double promotionAmount;
    
    private Double couponAmount;
    
    private String gstNo;
    
    private Integer tagId;
    
    private Timestamp checkInTime;
    
    private Timestamp checkOutTime;
    
    private Double purchaseRate;
    
    private Double actualAmount;
    
    private Double extraAdultRate;
    
    private Double extraChildRate;
    
    private Double addonAmount;
    
    public PmsBookedDetails(){
    	
    }
    
    public PmsBookedDetails(Integer bookingId){
    	this.bookingId=bookingId;
    }
	
	
	public Integer getPropertyId() {
		return this.propertyId;
	}

	public void setPropertyId(Integer propertyId) {
		this.propertyId = propertyId;
	}
	
	public Integer getPropertyDiscountId() {
		return propertyDiscountId;
	}

	public void setPropertyDiscountId(Integer propertyDiscountId) {
		this.propertyDiscountId = propertyDiscountId;
	}
	
	public Integer getPromotionId() {
		return promotionId;
	}

	public void setPromotionId(Integer promotionId) {
		this.promotionId = promotionId;
	}

	public Integer getGuestId() {
		return this.guestId;
	}

	public void setGuestId(Integer guestId) {
		this.guestId = guestId;
	}
	
	public Long getRoomCount() {
		return this.roomCount;
	}

	public void setRoomCount(Long roomCount) {
		this.roomCount = roomCount;
	}
	
	public Integer getAccommodationId() {
		return this.accommodationId;
	}

    public Integer getSourceId() {
		return sourceId;
	}

	public void setSourceId(Integer sourceId) {
		this.sourceId = sourceId;
	}

	public double getAdvanceAmount() {
		return advanceAmount;
	}

	public void setAdvanceAmount(double advanceAmount) {
		this.advanceAmount = advanceAmount;
	}
	public void setAccommodationId(Integer accommodationId) {
		this.accommodationId = accommodationId;
	}
	
	public String getAccommodationType() {
			return this.accommodationType;
	}

	public void setAccommodationType(String accommodationType) {
			this.accommodationType = accommodationType;
	}
	
	public Integer getMaxOccupancy() {
		return this.maxOccupancy;
    }

   public void setMaxOccupancy(Integer maxOccupancy) {
		this.maxOccupancy = maxOccupancy;
    }
   
   public double getAdultsIncludedRate() {
		return this.adultsIncludedRate;
    }

   public void setAdultsIncludedRate(double adultsIncludedRate) {
		this.adultsIncludedRate = adultsIncludedRate;
    }
   
   public double getChildIncludedRate() {
		return this.childIncludedRate;
   }

   public void setChildIncludedRate(double childIncludedRate) {
		this.childIncludedRate = childIncludedRate;
   }
   
   public long getAdults() {
		return this.adults;
	}

	public void setAdults(long adults) {
		this.adults = adults;
	}

	public double getAmount() {
		return this.amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public long getChild() {
		return this.child;
	}

	public void setChild(long child) {
		this.child = child;
	}
	
	
   public double getExtraChild() {
		return this.extraChild;
   }

   public void setExtraChild(double extraChild) {
		this.extraChild = extraChild;
   }
   
   public double getExtraAdult() {
		return this.extraAdult;
   }

   public void setExtraAdult(double extraAdult) {
		this.extraAdult = extraAdult;
   }
 	
	public double getBaseAmount() {
		return this.baseAmount;
	}

	public void setBaseAmount(double baseAmount) {
		this.baseAmount = baseAmount;
	}
	
	public Integer getNoOfUnits() {
		return this.noOfUnits;
	}

	public void setNoOfUnits(Integer noOfUnits) {
		this.noOfUnits = noOfUnits;
	}
	
	public Long getAvailable() {
		return this.available;
	}

	public void setAvailable(Long available) {
		this.available = available;
	}
	
	public Timestamp getArrivalDate() {
		return this.arrivalDate;
	}

	public void setArrivalDate(Timestamp arrivalDate) {
		this.arrivalDate = arrivalDate;
	}
	
	public Timestamp getDepartureDate() {
		return this.departureDate;
	}

	public void setDepartureDate(Timestamp departureDate) {
		this.departureDate = departureDate;
	}
  

	public Integer getBookingId() {
		return this.bookingId;
	}

	public void setBookingId(Integer bookingId) {
		this.bookingId = bookingId;
	}
    
	public Integer getStatusId() {
		return this.statusId;
	}

	public void setStatusId(Integer statusId) {
		this.statusId = statusId;
	}
	
    public Integer getRooms() {
		return this.rooms;
	}

	public void setRooms(Integer rooms) {
		this.rooms = rooms;
	}
	
   	public String getSpecialRequest() {
		return specialRequest;
	}

	public void setSpecialRequest(String specialRequest) {
		this.specialRequest = specialRequest;
	}
	
	public double getRefund() {
		return this.refund;
	}

	public void setRefund(double refund) {
		this.refund = refund;
	}

	public double getTax() {
		return this.tax;
	}

	public void setTax(double tax) {
		this.tax = tax;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Double getOtaTax() {
		return otaTax;
	}

	public void setOtaTax(Double otaTax) {
		this.otaTax = otaTax;
	}

	public Double getOtaCommission() {
		return otaCommission;
	}

	public void setOtaCommission(Double otaCommission) {
		this.otaCommission = otaCommission;
	}
	
	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public String getCheckIn() {
		return checkIn;
	}

	public void setCheckIn(String checkIn) {
		this.checkIn = checkIn;
	}

	public String getCheckOut() {
		return checkOut;
	}

	public void setCheckOut(String checkOut) {
		this.checkOut = checkOut;
	}

	public String getBookingDate() {
		return bookingDate;
	}

	public void setBookingDate(String bookingDate) {
		this.bookingDate = bookingDate;
	}

	public String getPromotionFirstName() {
		return promotionFirstName;
	}

	public void setPromotionFirstName(String promotionFirstName) {
		this.promotionFirstName = promotionFirstName;
	}

	public String getPromotionSecondName() {
		return promotionSecondName;
	}

	public void setPromotionSecondName(String promotionSecondName) {
		this.promotionSecondName = promotionSecondName;
	}

	public String getCouponName() {
		return couponName;
	}

	public void setCouponName(String couponName) {
		this.couponName = couponName;
	}

	public Double getPromotionAmount() {
		return promotionAmount;
	}

	public void setPromotionAmount(Double promotionAmount) {
		this.promotionAmount = promotionAmount;
	}

	public Double getCouponAmount() {
		return couponAmount;
	}

	public void setCouponAmount(Double couponAmount) {
		this.couponAmount = couponAmount;
	}

	public String getGstNo() {
		return gstNo;
	}

	public void setGstNo(String gstNo) {
		this.gstNo = gstNo;
	}

	public Integer getTagId() {
		return tagId;
	}

	public void setTagId(Integer tagId) {
		this.tagId = tagId;
	}

	public Timestamp getCheckInTime() {
		return checkInTime;
	}

	public void setCheckInTime(Timestamp checkInTime) {
		this.checkInTime = checkInTime;
	}

	public Timestamp getCheckOutTime() {
		return checkOutTime;
	}

	public void setCheckOutTime(Timestamp checkOutTime) {
		this.checkOutTime = checkOutTime;
	}

	public Double getPurchaseRate() {
		return purchaseRate;
	}

	public void setPurchaseRate(Double purchaseRate) {
		this.purchaseRate = purchaseRate;
	}

	public Double getActualAmount() {
		return actualAmount;
	}

	public void setActualAmount(Double actualAmount) {
		this.actualAmount = actualAmount;
	}

	public Double getExtraAdultRate() {
		return extraAdultRate;
	}

	public void setExtraAdultRate(Double extraAdultRate) {
		this.extraAdultRate = extraAdultRate;
	}

	public Double getExtraChildRate() {
		return extraChildRate;
	}

	public void setExtraChildRate(Double extraChildRate) {
		this.extraChildRate = extraChildRate;
	}

	public Double getAddonAmount() {
		return addonAmount;
	}

	public void setAddonAmount(Double addonAmount) {
		this.addonAmount = addonAmount;
	}

	
}
