package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;


/**
 * The persistent class for the property_commission_invoice database table.
 * 
 */
@Entity
@Table(name="property_commission_invoice")
@NamedQuery(name="PropertyCommissionInvoice.findAll", query="SELECT p FROM PropertyCommissionInvoice p")
public class PropertyCommissionInvoice implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="id",sequenceName="property_commission_invoice_invoice_id_seq")
	@GeneratedValue(generator="id")
	@Column(name="invoice_id", unique=true, nullable=false)
	private Integer invoiceId;

	@Column(name="from_date")
	private Timestamp fromDate;

	@Column(name="to_date")
	private Timestamp toDate;

	
	public Integer getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(Integer invoiceId) {
		this.invoiceId = invoiceId;
	}

	public Timestamp getFromDate() {
		return fromDate;
	}

	public void setFromDate(Timestamp fromDate) {
		this.fromDate = fromDate;
	}

	public Timestamp getToDate() {
		return toDate;
	}

	public void setToDate(Timestamp toDate) {
		this.toDate = toDate;
	}

	

}