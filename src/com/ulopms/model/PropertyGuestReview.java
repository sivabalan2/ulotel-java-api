package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;


/**
 * The persistent class for the property_guest_review database table.
 * 
 */
@Entity
@Table(name="property_guest_review")
@NamedQuery(name="PropertyGuestReview.findAll", query="SELECT p FROM PropertyGuestReview p")
public class PropertyGuestReview implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="id",sequenceName="property_guest_review_guest_review_id_seq")
	@GeneratedValue(generator="id")
	@Column(name="guest_review_id", unique=true, nullable=false)
	private Integer guestReviewId;

	@Column(name="review_guest_name")
	private String reviewGuestName;
	
	@Column(name="review_description")
	private String reviewDescription;
	
	@Column(name="review_count")
	private Integer reviewCount;
	
	@Column(name="created_by")
	private Integer createdBy;

	@Column(name="created_on")
	private Timestamp createdOn;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_deleted")
	private Boolean isDeleted;

	@Column(name="modified_by")
	private Integer modifiedBy;

	@Column(name="modified_on")
	private Timestamp modifiedOn;
	

	//bi-directional many-to-one association to PmsProperty
	@ManyToOne
	@JoinColumn(name="property_id")
	private PmsProperty pmsProperty;
	
	@ManyToOne
	@JoinColumn(name="reviewer_id")
	private Reviews reviews;
	
	

	public Integer getGuestReviewId() {
		return guestReviewId;
	}

	public void setGuestReviewId(Integer guestReviewId) {
		this.guestReviewId = guestReviewId;
	}

	public String getReviewGuestName() {
		return reviewGuestName;
	}

	public void setReviewGuestName(String reviewGuestName) {
		this.reviewGuestName = reviewGuestName;
	}

	public String getReviewDescription() {
		return reviewDescription;
	}

	public void setReviewDescription(String reviewDescription) {
		this.reviewDescription = reviewDescription;
	}

	public Integer getReviewCount() {
		return reviewCount;
	}

	public void setReviewCount(Integer reviewCount) {
		this.reviewCount = reviewCount;
	}

	public Reviews getReviews() {
		return reviews;
	}

	public void setReviews(Reviews reviews) {
		this.reviews = reviews;
	}

	public PmsProperty getPmsProperty() {
		return pmsProperty;
	}

	public void setPmsProperty(PmsProperty pmsProperty) {
		this.pmsProperty = pmsProperty;
	}
	

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Integer getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Timestamp getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Timestamp modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
}