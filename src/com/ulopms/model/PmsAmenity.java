package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the pms_amenities database table.
 * 
 */
@Entity
@Table(name="pms_amenities")
@NamedQuery(name="PmsAmenity.findAll", query="SELECT p FROM PmsAmenity p")
public class PmsAmenity implements Serializable {
	private static final long serialVersionUID = 1L;
 
	
	@Id  
    @SequenceGenerator(name="id",sequenceName="pms_amenities_amenity_id_seq")
	@GeneratedValue(generator="id")
	@Column(name="amenity_id", unique=true, nullable=false)
	private Integer amenityId;

	@Column(name="amenity_description", length=250)
	private String amenityDescription;

	@Column(name="amenity_name", length=100)
	private String amenityName;
	
	@Column(name="amenity_icon", length=250)
	private String amenityIcon;

	@Column(name="created_by")
	private Integer createdBy;

	@Column(name="created_date")
	private Timestamp createdDate;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_deleted")
	private Boolean isDeleted;

	@Column(name="modified_by")
	private Integer modifiedBy;

	@Column(name="modified_date")
	private Timestamp modifiedDate;
	
	@Column(name="amenity_is_active")
	private Boolean amenityIsActive;
	
	@Column(name="property_amenity_is_active")
	private Boolean propertyAmenityIsActive;

	@Column(name="accommodation_amenity_is_active")
	private Boolean accommodationAmenityIsActive;

	@Column(name="tag_amenity_is_active")
	private Boolean tagAmenityIsActive;
	
	//bi-directional many-to-one association to AccommodationAmenity
	@OneToMany(mappedBy="pmsAmenity")
	private List<AccommodationAmenity> accommodationAmenities;
	
	//bi-directional many-to-one association to PropertyAmenity
		@OneToMany(mappedBy="pmsAmenity")
		private List<PmsAmenity> pmsAmenity;

	public PmsAmenity() {
	}

	public Integer getAmenityId() {
		return this.amenityId;
	}

	public void setAmenityId(Integer amenityId) {
		this.amenityId = amenityId;
	}

	public String getAmenityDescription() {
		return this.amenityDescription;
	}

	public void setAmenityDescription(String amenityDescription) {
		this.amenityDescription = amenityDescription;
	}

	public String getAmenityName() {
		return this.amenityName;
	}

	public void setAmenityName(String amenityName) {
		this.amenityName = amenityName;
	}
	
	public String getAmenityIcon() {
		return this.amenityIcon;
	}

	public void setAmenityIcon(String amenityIcon) {
		this.amenityIcon = amenityIcon;
	}

	public Integer getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Boolean getAmenityIsActive() {
		return amenityIsActive;
	}

	public void setAmenityIsActive(Boolean amenityIsActive) {
		this.amenityIsActive = amenityIsActive;
	}

	public Integer getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Timestamp getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public List<AccommodationAmenity> getAccommodationAmenities() {
		return this.accommodationAmenities;
	}

	public void setAccommodationAmenities(List<AccommodationAmenity> accommodationAmenities) {
		this.accommodationAmenities = accommodationAmenities;
	}

	public AccommodationAmenity addAccommodationAmenity(AccommodationAmenity accommodationAmenity) {
		getAccommodationAmenities().add(accommodationAmenity);
		accommodationAmenity.setPmsAmenity(this);

		return accommodationAmenity;
	}

	public AccommodationAmenity removeAccommodationAmenity(AccommodationAmenity accommodationAmenity) {
		getAccommodationAmenities().remove(accommodationAmenity);
		accommodationAmenity.setPmsAmenity(null);

		return accommodationAmenity;
	}

	public Boolean getPropertyAmenityIsActive() {
		return propertyAmenityIsActive;
	}

	public void setPropertyAmenityIsActive(Boolean propertyAmenityIsActive) {
		this.propertyAmenityIsActive = propertyAmenityIsActive;
	}

	public Boolean getAccommodationAmenityIsActive() {
		return accommodationAmenityIsActive;
	}

	public void setAccommodationAmenityIsActive(Boolean accommodationAmenityIsActive) {
		this.accommodationAmenityIsActive = accommodationAmenityIsActive;
	}

	public Boolean getTagAmenityIsActive() {
		return tagAmenityIsActive;
	}

	public void setTagAmenityIsActive(Boolean tagAmenityIsActive) {
		this.tagAmenityIsActive = tagAmenityIsActive;
	}

	
}