package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the pms_guests database table.
 * 
 */
@Entity
@Table(name="pms_guests")
@NamedQuery(name="PmsGuest.findAll", query="SELECT p FROM PmsGuest p")
public class PmsGuest implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="id",sequenceName="pms_guests_guest_id_seq")
	@GeneratedValue(generator="id")
	@Column(name="guest_id", unique=true, nullable=false)
	private Integer guestId;

	@Column(length=250)
	private String address1;

	@Column(length=250)
	private String address2;

	@Column(length=50)
	private String city;

	@Column(name="country_code", length=10)
	private String countryCode;

	@Column(name="created_by")
	private Integer createdBy;

	@Column(name="created_date")
	private Timestamp createdDate;

	@Column(name="email_id", length=50)
	private String emailId;

	@Column(name="first_name", length=50)
	private String firstName;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_deleted")
	private Boolean isDeleted;

	@Column(name="landline",length=20)
	private String landline;

	@Column(name="last_name", length=50)
	private String lastName;

	@Column(name="modified_date")
	private Timestamp modifiedDate;

	@Column(name="phone", length=20)
	private String phone;

	@Column(name="zip_code", length=10)
	private String zipCode;
	
	@Column(name="pay_at_hotel_otp", length=10)
	private String payAtHotelOtp;

	@Column(name="gst_no", length=10)
	private String gstNo;
	
	//@Column(name="profile_pic_name", length=50)
	//private String imgPath;	

	
	//bi-directional many-to-one association to BookingGuestDetail
	@OneToMany(mappedBy="pmsGuest")
	private List<BookingGuestDetail> bookingGuestDetails;

	//bi-directional many-to-one association to State
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="state_code")
	private State state;

	public PmsGuest() {
	}

	public Integer getGuestId() {
		return this.guestId;
	}

	public void setGuestId(Integer guestId) {
		this.guestId = guestId;
	}

	public String getAddress1() {
		return this.address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return this.address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	
	/*public String getImgPath() {
		return imgPath;
	}

	public void setImgPath(String imgPath) {
		this.imgPath = imgPath;
	}*/

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountryCode() {
		return this.countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public Integer getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public String getEmailId() {
		return this.emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getLandline() {
		return this.landline;
	}

	public void setLandline(String landline) {
		this.landline = landline;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Timestamp getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getZipCode() {
		return this.zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	
	public String getPayAtHotelOtp() {
		return payAtHotelOtp;
	}

	public void setPayAtHotelOtp(String payAtHotelOtp) {
		this.payAtHotelOtp = payAtHotelOtp;
	}

	public List<BookingGuestDetail> getBookingGuestDetails() {
		return this.bookingGuestDetails;
	}

	public void setBookingGuestDetails(List<BookingGuestDetail> bookingGuestDetails) {
		this.bookingGuestDetails = bookingGuestDetails;
	}

	public BookingGuestDetail addBookingGuestDetail(BookingGuestDetail bookingGuestDetail) {
		getBookingGuestDetails().add(bookingGuestDetail);
		bookingGuestDetail.setPmsGuest(this);

		return bookingGuestDetail;
	}

	public BookingGuestDetail removeBookingGuestDetail(BookingGuestDetail bookingGuestDetail) {
		getBookingGuestDetails().remove(bookingGuestDetail);
		bookingGuestDetail.setPmsGuest(null);

		return bookingGuestDetail;
	}

	public State getState() {
		return this.state;
	}

	public void setState(State state) {
		this.state = state;
	}

	public String getGstNo() {
		return gstNo;
	}

	public void setGstNo(String gstNo) {
		this.gstNo = gstNo;
	}

}