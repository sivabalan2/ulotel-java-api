package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;


/**
 * The persistent class for the blog_article_images database table.
 * 
 */
@Entity
@Table(name="blog_article_images")
@NamedQuery(name="BlogArticleImages.findAll", query="SELECT p FROM BlogArticleImages p")
public class BlogArticleImages implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="id",sequenceName="blog_article_images_blog_article_image_id_seq")
	@GeneratedValue(generator="id")
	@Column(name="blog_article_image_id", unique=true, nullable=false)
	private Integer blogArticleImageId;

	@Column(name="created_by")
	private Integer createdBy;

	@Column(name="created_date")
	private Timestamp createdDate;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_deleted")
	private Boolean isDeleted;
	
	@Column(name="modified_by")
	private Integer modifiedBy;

	@Column(name="modified_date")
	private Timestamp modifiedDate;

	@Column(name="blog_article_image", length=250)
	private String blogArticleImage;
	
	
	//bi-directional many-to-one association to BlogArticles
		@ManyToOne(fetch=FetchType.EAGER)
		@JoinColumn(name="blog_article_id")
		private BlogArticles blogArticles;	



	public BlogArticleImages() {
	}

	

	public Integer getBlogArticleImageId() {
		return blogArticleImageId;
	}

	public void setBlogArticleImageId(Integer blogArticleImageId) {
		this.blogArticleImageId = blogArticleImageId;
	}

	public String getBlogArticleImage() {
		return blogArticleImage;
	}

	public void setBlogArticleImage(String blogArticleImage) {
		this.blogArticleImage = blogArticleImage;
	}

	public Integer getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}


	public Integer getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Timestamp getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}	
	
	public BlogArticles getBlogArticles() {
		return blogArticles;
	}

	public void setBlogArticles(BlogArticles blogArticles) {
		this.blogArticles = blogArticles;
	}

}