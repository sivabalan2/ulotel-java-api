package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the location database table.
 * 
 */
@Entity
@Table(name="location")
@NamedQuery(name="Location.findAll", query="SELECT l FROM Location l")
public class Location implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="id",sequenceName="location_location_id_seq")
	@GeneratedValue(generator="id")
	@Column(name="location_id", unique=true, nullable=false)
	private Integer locationId;

	@Column(name="created_by")
	private Integer createdBy;

	@Column(name="created_on")
	private Timestamp createdOn;

	@Column(length=1000)
	private String description;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_deleted")
	private Boolean isDeleted;

	@Column(name="location_name", length=50)
	private String locationName;

	@Column(name="modified_by")
	private Integer modifiedBy;

	@Column(name="modified_on")
	private Timestamp modifiedOn;
	
	@Column(name="photo_path", length=250)
	private String photoPath;

	//bi-directional many-to-one association to LocationPhoto
	@OneToMany(mappedBy="location")
	private List<LocationPhoto> locationPhotos;

	//bi-directional many-to-one association to PmsProperty
	@OneToMany(mappedBy="location")
	private List<PmsProperty> pmsProperties;
	
   //bi-directional many-to-one association to SeoContent
	@OneToMany(mappedBy="location")
	private List<SeoContent> seoContent;
	
	@Column(name="location_url", length=250)
	private String locationUrl;
	
	@Column(name="location_description", length=250)
	private String locationDescription;


	public Location() {
	}

	public Integer getLocationId() {
		return this.locationId;
	}

	public void setLocationId(Integer locationId) {
		this.locationId = locationId;
	}

	public Integer getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getLocationName() {
		return this.locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public Integer getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Timestamp getModifiedOn() {
		return this.modifiedOn;
	}

	public void setModifiedOn(Timestamp modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public List<LocationPhoto> getLocationPhotos() {
		return this.locationPhotos;
	}

	public void setLocationPhotos(List<LocationPhoto> locationPhotos) {
		this.locationPhotos = locationPhotos;
	}

	public LocationPhoto addLocationPhoto(LocationPhoto locationPhoto) {
		getLocationPhotos().add(locationPhoto);
		locationPhoto.setLocation(this);

		return locationPhoto;
	}

	public LocationPhoto removeLocationPhoto(LocationPhoto locationPhoto) {
		getLocationPhotos().remove(locationPhoto);
		locationPhoto.setLocation(null);

		return locationPhoto;
	}

	public List<PmsProperty> getPmsProperties() {
		return this.pmsProperties;
	}

	public void setPmsProperties(List<PmsProperty> pmsProperties) {
		this.pmsProperties = pmsProperties;
	}

	public PmsProperty addPmsProperty(PmsProperty pmsProperty) {
		getPmsProperties().add(pmsProperty);
		pmsProperty.setLocation(this);

		return pmsProperty;
	}

	public PmsProperty removePmsProperty(PmsProperty pmsProperty) {
		getPmsProperties().remove(pmsProperty);
		pmsProperty.setLocation(null);

		return pmsProperty;
	}

	public String getPhotoPath() {
		return photoPath;
	}

	public void setPhotoPath(String photoPath) {
		this.photoPath = photoPath;
	}
	

	public List<SeoContent> getSeoContent() {
		return this.seoContent;
	}

	public void setSeoContent(List<SeoContent> seoContent) {
		this.seoContent = seoContent;
	}

	public String getLocationUrl() {
		return locationUrl;
	}

	public void setLocationUrl(String locationUrl) {
		this.locationUrl = locationUrl;
	}
	
	public String getLocationDescription() {
		return locationDescription;
	}

	public void setLocationDescription(String locationDescription) {
		this.locationDescription = locationDescription;
	}
}