package com.ulopms.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * The persistent class for the pms_smart_price database table.
 * 
 */
@Entity
@Table(name="property_rate_plan_details")
@NamedQuery(name="PropertyRatePlanDetail.findAll", query="SELECT p FROM PropertyRatePlanDetail p")


public class PropertyRatePlanDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	

	@Id
	@SequenceGenerator(name="id",sequenceName="property_rate_plan_details_rate_plan_detail_id_seq")
	@GeneratedValue(generator="id")
	
	@Column(name="rate_plan_detail_id")
	private Integer ratePlanDetailId;

	@Column(name="tariff_amount")
	private Double tariffAmount;
	
	@Column(name="start_date")
	private Timestamp startDate; 
	
	@Column(name="end_date")
	private Timestamp endDate;
	
	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_deleted")
	private Boolean isDeleted;
	
	@Column(name="plan_is_active")
	private Boolean planIsActive;
	
	@Column(name="created_by")
	private Integer createdBy;

	@Column(name="created_on")
	private Timestamp createdOn;
	
	@Column(name="modified_by")
	private Integer modifiedBy;

	@Column(name="modified_on")
	private Timestamp modifiedOn;
		
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="accommodation_id")
	private PropertyAccommodation propertyAccommodation;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="property_id")
	private PmsProperty pmsProperty;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="rate_plan_id")
	private PropertyRatePlan ratePlan;

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	
	public Integer getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}
	
	public Integer getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Timestamp getModifiedOn() {
		return this.modifiedOn;
	}

	public void setModifiedOn(Timestamp modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
	
	public Integer getRatePlanDetailId() {
		return ratePlanDetailId;
	}

	public void setRatePlanDetailId(Integer ratePlanDetailId) {
		this.ratePlanDetailId = ratePlanDetailId;
	}

	public Timestamp getStartDate() {
		return startDate;
	}

	public void setStartDate(Timestamp startDate) {
		this.startDate = startDate;
	}

	public Timestamp getEndDate() {
		return endDate;
	}

	public void setEndDate(Timestamp endDate) {
		this.endDate = endDate;
	}

	public Boolean getPlanIsActive() {
		return planIsActive;
	}

	public void setPlanIsActive(Boolean planIsActive) {
		this.planIsActive = planIsActive;
	}
	
	public Double getTariffAmount() {
		return tariffAmount;
	}

	public void setTariffAmount(Double tariffAmount) {
		this.tariffAmount = tariffAmount;
	}

	public PropertyAccommodation getPropertyAccommodation() {
		return propertyAccommodation;
	}

	public void setPropertyAccommodation(PropertyAccommodation propertyAccommodation) {
		this.propertyAccommodation = propertyAccommodation;
	}

	public PmsProperty getPmsProperty() {
		return pmsProperty;
	}

	public void setPmsProperty(PmsProperty pmsProperty) {
		this.pmsProperty = pmsProperty;
	}

	public PropertyRatePlan getRatePlan() {
		return ratePlan;
	}

	public void setRatePlan(PropertyRatePlan ratePlan) {
		this.ratePlan = ratePlan;
	}
	
}