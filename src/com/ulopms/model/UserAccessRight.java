package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;


/**
 * The persistent class for the user_access_rights database table.
 * 
 */
@Entity
@Table(name="user_access_rights")
@NamedQuery(name="UserAccessRight.findAll", query="SELECT u FROM UserAccessRight u")
public class UserAccessRight implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="id",sequenceName="user_access_rights_user_access_rights_id_seq")
	@GeneratedValue(generator="id")
	@Column(name="user_access_rights_id", unique=true, nullable=false)
	private Integer userAccessRightsId;

	@Column(name="access_rights_permission")
	private Boolean accessRightsPermission;

	@Column(name="created_by")
	private Integer createdBy;

	@Column(name="created_date")
	private Timestamp createdDate;

	//bi-directional many-to-one association to AccessRight
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="access_rights_id")
	private AccessRight accessRight;

	//bi-directional many-to-one association to User
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="user_id")
	private User user;

	public UserAccessRight() {
	}

	public Integer getUserAccessRightsId() {
		return this.userAccessRightsId;
	}

	public void setUserAccessRightsId(Integer userAccessRightsId) {
		this.userAccessRightsId = userAccessRightsId;
	}

	public Boolean getAccessRightsPermission() {
		return this.accessRightsPermission;
	}

	public void setAccessRightsPermission(Boolean accessRightsPermission) {
		this.accessRightsPermission = accessRightsPermission;
	}

	public Integer getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public AccessRight getAccessRight() {
		return this.accessRight;
	}

	public void setAccessRight(AccessRight accessRight) {
		this.accessRight = accessRight;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}