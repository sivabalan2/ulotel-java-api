package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the google_location database table.
 * 
 */
@Entity
@Table(name="google_location")
@NamedQuery(name="GoogleLocation.findAll", query="SELECT l FROM GoogleLocation l")
public class GoogleLocation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="id",sequenceName="google_location_google_location_id_seq")
	@GeneratedValue(generator="id")
	@Column(name="google_location_id", unique=true, nullable=false)
	private Integer googleLocationId;
	
	@Column(name="google_location_name")
	private String googleLocationName;
	
	@Column(name="google_location_display_name")
	private String googleLocationDisplayName;
	
	@Column(name="google_location_url")
	private String googleLocationUrl;
	
	@Column(name="description")
	private String description;
	
	@Column(name="google_location_latitude")
	private Double googleLocationLatitude;
	
	@Column(name="google_location_longitude")
	private Double googleLocationLongitude;

	@Column(name="photo_path")
	private String photoPath;
	
	@Column(name="google_location_place_id")
	private String googleLocationPlaceId;
	
	@Column(name="created_by")
	private Integer createdBy;

	@Column(name="created_on")
	private Timestamp createdOn;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_deleted")
	private Boolean isDeleted;

	@Column(name="modified_by")
	private Integer modifiedBy;

	@Column(name="modified_on")
	private Timestamp modifiedOn;

	@OneToMany(mappedBy="googleLocation")
	private List<GoogleArea> googleArea;

	@OneToMany(mappedBy="googleLocation")
	private List<PmsProperty> pmsProperty;
	
	@OneToMany(mappedBy="googleLocation")
	private List<SeoContent> seoContent;
	

	public GoogleLocation() {
	}

	public Integer getGoogleLocationId() {
		return googleLocationId;
	}

	public void setGoogleLocationId(Integer googleLocationId) {
		this.googleLocationId = googleLocationId;
	}

	public String getGoogleLocationName() {
		return googleLocationName;
	}

	public void setGoogleLocationName(String googleLocationName) {
		this.googleLocationName = googleLocationName;
	}

	public String getGoogleLocationDisplayName() {
		return googleLocationDisplayName;
	}

	public void setGoogleLocationDisplayName(String googleLocationDisplayName) {
		this.googleLocationDisplayName = googleLocationDisplayName;
	}

	public String getGoogleLocationUrl() {
		return googleLocationUrl;
	}

	public void setGoogleLocationUrl(String googleLocationUrl) {
		this.googleLocationUrl = googleLocationUrl;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Double getGoogleLocationLatitude() {
		return googleLocationLatitude;
	}

	public void setGoogleLocationLatitude(Double googleLocationLatitude) {
		this.googleLocationLatitude = googleLocationLatitude;
	}

	public Double getGoogleLocationLongitude() {
		return googleLocationLongitude;
	}

	public void setGoogleLocationLongitude(Double googleLocationLongitude) {
		this.googleLocationLongitude = googleLocationLongitude;
	}

	public String getPhotoPath() {
		return photoPath;
	}

	public void setPhotoPath(String photoPath) {
		this.photoPath = photoPath;
	}

	public String getGoogleLocationPlaceId() {
		return googleLocationPlaceId;
	}

	public void setGoogleLocationPlaceId(String googleLocationPlaceId) {
		this.googleLocationPlaceId = googleLocationPlaceId;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Integer getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Timestamp getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Timestamp modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public List<GoogleArea> getGoogleArea() {
		return googleArea;
	}

	public void setGoogleArea(List<GoogleArea> googleArea) {
		this.googleArea = googleArea;
	}
	
	public List<PmsProperty> getPmsProperty() {
		return pmsProperty;
	}

	public void setPmsProperty(List<PmsProperty> pmsProperty) {
		this.pmsProperty = pmsProperty;
	}

	public List<SeoContent> getSeoContent() {
		return seoContent;
	}

	public void setSeoContent(List<SeoContent> seoContent) {
		this.seoContent = seoContent;
	}

	
}