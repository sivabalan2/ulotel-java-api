package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the google_area database table.
 * 
 */
@Entity
@Table(name="google_area")
@NamedQuery(name="GoogleArea.findAll", query="SELECT l FROM GoogleArea l")
public class GoogleArea implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="id",sequenceName="google_area_google_area_id_seq")
	@GeneratedValue(generator="id")
	@Column(name="google_area_id", unique=true, nullable=false)
	private Integer googleAreaId;
	
	@Column(name="google_area_name")
	private String googleAreaName;
	
	@Column(name="google_area_display_name")
	private String googleAreaDisplayName;
	
	@Column(name="google_area_url")
	private String googleAreaUrl;
	
	@Column(name="description")
	private String description;
	
	@Column(name="google_area_latitude")
	private Double googleAreaLatitude;
	
	@Column(name="google_area_longitude")
	private Double googleAreaLongitude;

	@Column(name="photo_path")
	private String photoPath;
	
	@Column(name="created_by")
	private Integer createdBy;

	@Column(name="created_on")
	private Timestamp createdOn;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_deleted")
	private Boolean isDeleted;

	@Column(name="modified_by")
	private Integer modifiedBy;

	@Column(name="modified_on")
	private Timestamp modifiedOn;
	
	//bi-directional many-to-one association to GoogleLocation
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="google_location_id")
	private GoogleLocation googleLocation;
	
	//bi-directional many-to-one association to googlePropertyAreas
	@OneToMany(mappedBy="googleArea")
	private List<GooglePropertyAreas> googlePropertyAreas;
	
	//bi-directional many-to-one association to googlePropertyAreas
	@OneToMany(mappedBy="googleArea")
	private List<SeoContent> seoContent;
	
	@Column(name="google_area_Place_id")
	private String googleAreaPlaceId;
	
	@Column(name="google_redirect_url")
	private String googleRedirectUrl;

	
	public GoogleArea() {
	}

	
	public Integer getGoogleAreaId() {
		return googleAreaId;
	}

	public void setGoogleAreaId(Integer googleAreaId) {
		this.googleAreaId = googleAreaId;
	}

	public String getGoogleAreaName() {
		return googleAreaName;
	}

	public void setGoogleAreaName(String googleAreaName) {
		this.googleAreaName = googleAreaName;
	}

	public String getGoogleAreaDisplayName() {
		return googleAreaDisplayName;
	}

	public void setGoogleAreaDisplayName(String googleAreaDisplayName) {
		this.googleAreaDisplayName = googleAreaDisplayName;
	}

	public String getGoogleAreaUrl() {
		return googleAreaUrl;
	}

	public void setGoogleAreaUrl(String googleAreaUrl) {
		this.googleAreaUrl = googleAreaUrl;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Double getGoogleAreaLatitude() {
		return googleAreaLatitude;
	}

	public void setGoogleAreaLatitude(Double googleAreaLatitude) {
		this.googleAreaLatitude = googleAreaLatitude;
	}

	public Double getGoogleAreaLongitude() {
		return googleAreaLongitude;
	}

	public void setGoogleAreaLongitude(Double googleAreaLongitude) {
		this.googleAreaLongitude = googleAreaLongitude;
	}

	public String getPhotoPath() {
		return photoPath;
	}

	public void setPhotoPath(String photoPath) {
		this.photoPath = photoPath;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Integer getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Timestamp getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Timestamp modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public String getGoogleAreaPlaceId() {
		return googleAreaPlaceId;
	}

	public void setGoogleAreaPlaceId(String googleAreaPlaceId) {
		this.googleAreaPlaceId = googleAreaPlaceId;
	}

	public String getGoogleRedirectUrl() {
		return googleRedirectUrl;
	}

	public void setGoogleRedirectUrl(String googleRedirectUrl) {
		this.googleRedirectUrl = googleRedirectUrl;
	}

	public GoogleLocation getGoogleLocation() {
		return googleLocation;
	}

	public void setGoogleLocation(GoogleLocation googleLocation) {
		this.googleLocation = googleLocation;
	}

	public List<GooglePropertyAreas> getGooglePropertyAreas() {
		return googlePropertyAreas;
	}

	public void setGooglePropertyAreas(List<GooglePropertyAreas> googlePropertyAreas) {
		this.googlePropertyAreas = googlePropertyAreas;
	}
	
	public List<SeoContent> getSeoContent() {
		return seoContent;
	}

	public void setSeoContent(List<SeoContent> seoContent) {
		this.seoContent = seoContent;
	}


	
}