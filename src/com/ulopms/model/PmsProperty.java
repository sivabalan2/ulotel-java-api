package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the pms_properties database table.
 * 
 */
@Entity
@Table(name="pms_properties")
@NamedQuery(name="PmsProperty.findAll", query="SELECT p FROM PmsProperty p")
public class PmsProperty implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="id",sequenceName="pms_properties_property_id_seq")
	@GeneratedValue(generator="id")
	@Column(name="property_id", unique=true, nullable=false)
	private Integer propertyId;

	@Column(name="\"Address1\"", length=250)
	private String address1;

	@Column(name="\"Address2\"", length=250)
	private String address2;

	@Column(length=100)
	private String city;

	@Column(name="created_by")
	private Integer createdBy;

	@Column(name="created_date")
	private Timestamp createdDate;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_deleted")
	private Boolean isDeleted;

	@Column(length=20)
	private String latitude;

	@Column(length=20)
	private String longitude;

	@Column(name="modified_by")
	private Integer modifiedBy;

	@Column(name="modified_date")
	private Timestamp modifiedDate;

	@Column(name="property_contact", length=1000)
	private String propertyContact;

	@Column(name="property_description", length=1000)
	private String propertyDescription;

	@Column(name="property_email", length=1000)
	private String propertyEmail;

	@Column(name="property_logo", length=1000)
	private String propertyLogo;

	@Column(name="property_name", length=1000)
	private String propertyName;

	@Column(name="property_phone", length=1000)
	private String propertyPhone;

	@Column(name="zip_code", length=1000)
	private String zipCode;
	
	@Column(name="property_thumbimg", length=1000)
	private String propertyThumbPath;
	
	@Column(name="reservation_manager_name", length=1000)
	private String reservationManagerName;
	
	@Column(name="reservation_manager_email", length=1000)
	private String reservationManagerEmail;
	
	@Column(name="reservation_manager_contact", length=1000)
	private String reservationManagerContact;
	
	@Column(name="resort_manager_name", length=1000)
	private String resortManagerName;
	
	@Column(name="resort_manager_email", length=1000)
	private String resortManagerEmail;
	
	@Column(name="resort_manager_contact", length=1000)
	private String resortManagerContact;
	
	@Column(name="resort_manager_designation", length=1000)
	private String resortManagerDesignation;
	
	@Column(name="resort_manager_name1")
	private String resortManagerName1;
	
	@Column(name="resort_manager_email1")
	private String resortManagerEmail1;
	
	@Column(name="resort_manager_contact1")
	private String resortManagerContact1;
	
	@Column(name="resort_manager_designation1")
	private String resortManagerDesignation1;
	
	@Column(name="resort_manager_name2")
	private String resortManagerName2;
	
	@Column(name="resort_manager_email2")
	private String resortManagerEmail2;
	
	@Column(name="resort_manager_contact2")
	private String resortManagerContact2;
	
	@Column(name="resort_manager_designation2")
	private String resortManagerDesignation2;
	
	@Column(name="resort_manager_name3")
	private String resortManagerName3;
	
	@Column(name="resort_manager_email3")
	private String resortManagerEmail3;
	
	@Column(name="resort_manager_contact3")
	private String resortManagerContact3;
	
	@Column(name="resort_manager_designation3")
	private String resortManagerDesignation3;
	
	@Column(name="contract_manager_name", length=1000)
	private String contractManagerName;
	
	@Column(name="contract_manager_email", length=1000)
	private String contractManagerEmail;
	
	@Column(name="contract_manager_contact", length=1000)
	private String contractManagerContact;
	
	@Column(name="revenue_manager_name", length=1000)
	private String revenueManagerName;
	
	@Column(name="revenue_manager_email", length=1000)
	private String revenueManagerEmail;
	
	@Column(name="revenue_manager_contact", length=1000)
	private String revenueManagerContact;
	
	@Column(name="tax_valid_from")
	private Timestamp taxValidFrom;
	
	@Column(name="pay_at_hotel_status")
	private Boolean payAtHotelStatus;	
  
	@Column(name="tax_valid_to")
	private Timestamp taxValidTo;
	
	@Column(name="tax_is_active")
	private Boolean taxIsActive;

	@Column(name="property_hotel_policy", length=1000)
	private String propertyHotelPolicy;
	
	@Column(name="property_standard_policy", length=1000)
	private String propertyStandardPolicy;
	
	@Column(name="property_cancellation_policy", length=1000)
	private String propertyCancellationPolicy;
	
	@Column(name="property_child_policy", length=1000)
	private String propertyChildPolicy;	

	@Column(name="property_commission")
	private Double propertyCommission;
	
	@Column(name="walkin_commission")
	private Double walkinCommission;
	
	@Column(name="online_commission")
	private Double onlineCommission;
	
	@Column(name="net_rate_revenue")
	private Double netRateRevenue;
	
	@Column(name="place_id", length=1000)
	private String placeId;
	
	@Column(name="route_map", length=5000)
	private String routeMap;
	
	@Column(name="property_gstno", length=1000)
	private String propertyGstNo;
	
	@Column(name="property_finance_email", length=1000)
	private String propertyFinanceEmail;
	
	@Column(name="property_url", length=1000)
	private String propertyUrl;
	
	@Column(name="property_review_link", length=1000)
	private String propertyReviewLink;
	
	@Column(name="tripadvisor_review_link")
	private String tripadvisorReviewLink;
	
	@Column(name="couple_is_active")
	private Boolean coupleIsActive;	
	
	@Column(name="breakfast_is_active")
	private Boolean breakfastIsActive;	
	
	@Column(name="hotel_is_active")
	private Boolean hotelIsActive;	
	
	@Column(name="property_is_control")
	private Boolean propertyIsControl;	
	
	@Column(name="paynow_discount")
	private Integer paynowDiscount;	
	
	@Column(name="flexible_stay_status")
	private Boolean flexibleStayStatus;	
	
	@Column(name="travel_discount_status")
	private Boolean travelDiscountStatus;
	
	@Column(name="stop_sell_is_active")
	private Boolean stopSellIsActive;
	
	@Column(name="delist_is_active")
	private Boolean delistIsActive;
	
	@Column(name="property_gigi_policy")
	private String propertyGigiPolicy;
	
	@OneToMany(mappedBy="pmsProperty")
	private List<PmsBooking> pmsBookings;
	
	@OneToMany(mappedBy="pmsProperty")
	private List<OtaHotels> otaHotels;
		
	@OneToMany(mappedBy="pmsProperty")
	private List<OtaHotelDetails> otaHotelDetails;
	
	@OneToMany(mappedBy="pmsProperty")
	private List<PropertyTags> propertyTags;
	
	@OneToMany(mappedBy="pmsProperty")
	private List<PropertyPaymentDetails> propertyPaymentDetails;
		
	/*
	//bi-directional many-to-one association to PropertyLAndmark
		@OneToMany(mappedBy="pmsProperty")
		private List<PropertyLandmark> propertyLandmark;*/

	//bi-directional many-to-one association to PropertyType
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="property_type_id")
	private PropertyType propertyType;
	
	//bi-directional many-to-one association to locationType
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="location_type_id")
	private LocationType locationType;
	
	//bi-directional many-to-one association to PropertyType
	//@ManyToOne(fetch=FetchType.LAZY)
	//@JoinColumn(name="property_id")
	//private PropertyUser propertyUser;

	//bi-directional many-to-one association to State
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="state_code")
	private State state;
	
	//bi-directional many-to-one association to Country
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="country_code")
	private Country country;

	//bi-directional many-to-one association to User
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="property_owner_id")
	private User user;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="contract_type_id")
	private PropertyContractType contractType;

	
	//bi-directional many-to-one association to PropertyAccommodation
	@OneToMany(mappedBy="pmsProperty")
	private List<PropertyAccommodation> propertyAccommodations;
	
	//bi-directional many-to-one association to UlotelHotelDetailTags
	@OneToMany(mappedBy="pmsProperty")
	private List<UlotelHotelDetailTags> ulotelHotelDetailTags;
	
	//bi-directional many-to-one association to GooglePropertyAreas
	@OneToMany(mappedBy="pmsProperty")
	private List<GooglePropertyAreas> googlePropertyAreas;

	//bi-directional many-to-one association to PropertyAccommodation
	@OneToMany(mappedBy="pmsProperty")
	private List<PropertyUser> propertyUsers;
		
	//bi-directional many-to-one association to SeoContent
	@OneToMany(mappedBy="pmsProperty")
	private List<SeoContent> seoContent;

		
	public List<PropertyUser> getPropertyUsers() {
		return propertyUsers;
	}

	public void setPropertyUsers(List<PropertyUser> propertyUsers) {
		this.propertyUsers = propertyUsers;
	}

	//bi-directional many-to-one association to PropertyAmenity
	@OneToMany(mappedBy="pmsProperty")
	private List<PropertyAmenity> propertyAmenities;

	//bi-directional many-to-one association to PropertyItemCategory
	@OneToMany(mappedBy="pmsProperty")
	private List<PropertyItemCategory> propertyItemCategories;

	//bi-directional many-to-one association to PropertyItem
	@OneToMany(mappedBy="pmsProperty")
	private List<PropertyItem> propertyItems;

	//bi-directional many-to-one association to PropertyPhoto
	@OneToMany(mappedBy="pmsProperty")
	private List<PropertyPhoto> propertyPhotos;

	//bi-directional many-to-one association to PropertyTaxe
	
	/*@OneToMany(mappedBy="pmsProperty")
	private List<PropertyTaxe> propertyTaxes;
	*/
	
	//bi-directional many-to-one association to Location
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="location_id")
	private Location location;
	
	//bi-directional many-to-one association to GoogleLocation
    @ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="google_location_id")
	private GoogleLocation googleLocation;

	//bi-directional many-to-one association to Area
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="area_id")
	private Area area;			
	

	public PmsProperty() {
	}

	public Integer getPropertyId() {
		return this.propertyId;
	}

	public void setPropertyId(Integer propertyId) {
		this.propertyId = propertyId;
	}

	public String getAddress1() {
		return this.address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return this.address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Integer getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getLatitude() {
		return this.latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return this.longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public Integer getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Timestamp getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getPropertyContact() {
		return this.propertyContact;
	}

	public void setPropertyContact(String propertyContact) {
		this.propertyContact = propertyContact;
	}

	public String getPropertyDescription() {
		return this.propertyDescription;
	}

	public void setPropertyDescription(String propertyDescription) {
		this.propertyDescription = propertyDescription;
	}

	public String getPropertyEmail() {
		return this.propertyEmail;
	}

	public void setPropertyEmail(String propertyEmail) {
		this.propertyEmail = propertyEmail;
	}

	public String getPropertyLogo() {
		return this.propertyLogo;
	}

	public void setPropertyLogo(String propertyLogo) {
		this.propertyLogo = propertyLogo;
	}

	public String getPropertyName() {
		return this.propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public String getPropertyPhone() {
		return this.propertyPhone;
	}

	public void setPropertyPhone(String propertyPhone) {
		this.propertyPhone = propertyPhone;
	}

	public String getZipCode() {
		return this.zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	
	public String getPropertyThumbPath() {
		return this.propertyThumbPath;
	}

	public void setPropertyThumbPath(String propertyThumbPath) {
		this.propertyThumbPath = propertyThumbPath;
	}
	
	public String getResortManagerName() {
		return this.resortManagerName;
	}

	public void setResortManagerName(String resortManagerName) {
		this.resortManagerName = resortManagerName;
	}
	
	public String getReservationManagerName() {
		return this.reservationManagerName;
	}

	public void setReservationManagerName(String reservationManagerName) {
		this.reservationManagerName = reservationManagerName;
	}
	
	public String getResortManagerEmail() {
		return this.resortManagerEmail;
	}

	public void setResortManagerEmail(String resortManagerEmail) {
		this.resortManagerEmail = resortManagerEmail;
	}
	
	public String getReservationManagerEmail() {
		return this.reservationManagerEmail;
	}

	public void setReservationManagerEmail(String reservationManagerEmail) {
		this.reservationManagerEmail = reservationManagerEmail;
	}
	
	public String getResortManagerContact() {
		return this.resortManagerContact;
	}

	public void setResortManagerContact(String resortManagerContact) {
		this.resortManagerContact = resortManagerContact;
	}
	
	public String getResortManagerDesignation() {
		return resortManagerDesignation;
	}

	public void setResortManagerDesignation(String resortManagerDesignation) {
		this.resortManagerDesignation = resortManagerDesignation;
	}

	public String getResortManagerName1() {
		return resortManagerName1;
	}

	public void setResortManagerName1(String resortManagerName1) {
		this.resortManagerName1 = resortManagerName1;
	}

	public String getResortManagerEmail1() {
		return resortManagerEmail1;
	}

	public void setResortManagerEmail1(String resortManagerEmail1) {
		this.resortManagerEmail1 = resortManagerEmail1;
	}

	public String getResortManagerContact1() {
		return resortManagerContact1;
	}

	public void setResortManagerContact1(String resortManagerContact1) {
		this.resortManagerContact1 = resortManagerContact1;
	}

	public String getResortManagerDesignation1() {
		return resortManagerDesignation1;
	}

	public void setResortManagerDesignation1(String resortManagerDesignation1) {
		this.resortManagerDesignation1 = resortManagerDesignation1;
	}

	public String getResortManagerName2() {
		return resortManagerName2;
	}

	public void setResortManagerName2(String resortManagerName2) {
		this.resortManagerName2 = resortManagerName2;
	}

	public String getResortManagerEmail2() {
		return resortManagerEmail2;
	}

	public void setResortManagerEmail2(String resortManagerEmail2) {
		this.resortManagerEmail2 = resortManagerEmail2;
	}

	public String getResortManagerContact2() {
		return resortManagerContact2;
	}

	public void setResortManagerContact2(String resortManagerContact2) {
		this.resortManagerContact2 = resortManagerContact2;
	}

	public String getResortManagerDesignation2() {
		return resortManagerDesignation2;
	}

	public void setResortManagerDesignation2(String resortManagerDesignation2) {
		this.resortManagerDesignation2 = resortManagerDesignation2;
	}

	public String getResortManagerName3() {
		return resortManagerName3;
	}

	public void setResortManagerName3(String resortManagerName3) {
		this.resortManagerName3 = resortManagerName3;
	}

	public String getResortManagerEmail3() {
		return resortManagerEmail3;
	}

	public void setResortManagerEmail3(String resortManagerEmail3) {
		this.resortManagerEmail3 = resortManagerEmail3;
	}

	public String getResortManagerContact3() {
		return resortManagerContact3;
	}

	public void setResortManagerContact3(String resortManagerContact3) {
		this.resortManagerContact3 = resortManagerContact3;
	}

	public String getResortManagerDesignation3() {
		return resortManagerDesignation3;
	}

	public void setResortManagerDesignation3(String resortManagerDesignation3) {
		this.resortManagerDesignation3 = resortManagerDesignation3;
	}

	public String getReservationManagerContact() {
		return this.reservationManagerContact;
	}

	public void setReservationManagerContact(String reservationManagerContact) {
		this.reservationManagerContact = reservationManagerContact;
	}
	

	public List<PmsBooking> getPmsBookings() {
		return this.pmsBookings;
	}

	public void setPmsBookings(List<PmsBooking> pmsBookings) {
		this.pmsBookings = pmsBookings;
	}
	
	/*public List<PropertyLandmark> getPropertyLandmark() {
		return this.propertyLandmark;
	}

	public void setPropertyLandmark(List<PropertyLandmark> propertyLandmark) {
		this.propertyLandmark = propertyLandmark;
	}*/

	public PmsBooking addPmsBooking(PmsBooking pmsBooking) {
		getPmsBookings().add(pmsBooking);
		pmsBooking.setPmsProperty(this);

		return pmsBooking;
	}

	public PmsBooking removePmsBooking(PmsBooking pmsBooking) {
		getPmsBookings().remove(pmsBooking);
		pmsBooking.setPmsProperty(null);

		return pmsBooking;
	}

	public PropertyType getPropertyType() {
		return this.propertyType;
	}

	public void setPropertyType(PropertyType propertyType) {
		this.propertyType = propertyType;
	}
	
	public LocationType getLocationType() {
		return locationType;
	}

	public void setLocationType(LocationType locationType) {
		this.locationType = locationType;
	}
	
	/*
	public PropertyUser getPropertyUser() {
		return this.propertyUser;
	}

	public void setPropertyUser(PropertyUser propertyUser) {
		this.propertyUser = propertyUser;
	}
	*/

	public State getState() {
		return this.state;
	}

	public void setState(State state) {
		this.state = state;
	}
	
	public Country getCountry() {
		return this.country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}
	
	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<PropertyAccommodation> getPropertyAccommodations() {
		return this.propertyAccommodations;
	}

	public void setPropertyAccommodations(List<PropertyAccommodation> propertyAccommodations) {
		this.propertyAccommodations = propertyAccommodations;
	}

	public List<UlotelHotelDetailTags> getUlotelHotelDetailTags() {
		return ulotelHotelDetailTags;
	}

	public void setUlotelHotelDetailTags(
			List<UlotelHotelDetailTags> ulotelHotelDetailTags) {
		this.ulotelHotelDetailTags = ulotelHotelDetailTags;
	}

	public List<GooglePropertyAreas> getGooglePropertyAreas() {
		return googlePropertyAreas;
	}

	public void setGooglePropertyAreas(List<GooglePropertyAreas> googlePropertyAreas) {
		this.googlePropertyAreas = googlePropertyAreas;
	}

	public PropertyAccommodation addPropertyAccommodation(PropertyAccommodation propertyAccommodation) {
		getPropertyAccommodations().add(propertyAccommodation);
		propertyAccommodation.setPmsProperty(this);

		return propertyAccommodation;
	}

	public PropertyAccommodation removePropertyAccommodation(PropertyAccommodation propertyAccommodation) {
		getPropertyAccommodations().remove(propertyAccommodation);
		propertyAccommodation.setPmsProperty(null);

		return propertyAccommodation;
	}

	public List<PropertyAmenity> getPropertyAmenities() {
		return this.propertyAmenities;
	}

	public void setPropertyAmenities(List<PropertyAmenity> propertyAmenities) {
		this.propertyAmenities = propertyAmenities;
	}

	public PropertyAmenity addPropertyAmenity(PropertyAmenity propertyAmenity) {
		getPropertyAmenities().add(propertyAmenity);
		propertyAmenity.setPmsProperty(this);

		return propertyAmenity;
	}

	public PropertyAmenity removePropertyAmenity(PropertyAmenity propertyAmenity) {
		getPropertyAmenities().remove(propertyAmenity);
		propertyAmenity.setPmsProperty(null);

		return propertyAmenity;
	}

	public List<PropertyItemCategory> getPropertyItemCategories() {
		return this.propertyItemCategories;
	}

	public void setPropertyItemCategories(List<PropertyItemCategory> propertyItemCategories) {
		this.propertyItemCategories = propertyItemCategories;
	}

	public PropertyItemCategory addPropertyItemCategory(PropertyItemCategory propertyItemCategory) {
		getPropertyItemCategories().add(propertyItemCategory);
		propertyItemCategory.setPmsProperty(this);

		return propertyItemCategory;
	}

	public PropertyItemCategory removePropertyItemCategory(PropertyItemCategory propertyItemCategory) {
		getPropertyItemCategories().remove(propertyItemCategory);
		propertyItemCategory.setPmsProperty(null);

		return propertyItemCategory;
	}

	public List<PropertyItem> getPropertyItems() {
		return this.propertyItems;
	}

	public void setPropertyItems(List<PropertyItem> propertyItems) {
		this.propertyItems = propertyItems;
	}

	public PropertyItem addPropertyItem(PropertyItem propertyItem) {
		getPropertyItems().add(propertyItem);
		propertyItem.setPmsProperty(this);

		return propertyItem;
	}

	public PropertyItem removePropertyItem(PropertyItem propertyItem) {
		getPropertyItems().remove(propertyItem);
		propertyItem.setPmsProperty(null);

		return propertyItem;
	}
	
	public List<SeoContent> getSeoContent() {
		return seoContent;
	}

	public void setSeoContent(List<SeoContent> seoContent) {
		this.seoContent = seoContent;
	}

	public List<PropertyPhoto> getPropertyPhotos() {
		return this.propertyPhotos;
	}

	public void setPropertyPhotos(List<PropertyPhoto> propertyPhotos) {
		this.propertyPhotos = propertyPhotos;
	}

	public PropertyPhoto addPropertyPhoto(PropertyPhoto propertyPhoto) {
		getPropertyPhotos().add(propertyPhoto);
		propertyPhoto.setPmsProperty(this);

		return propertyPhoto;
	}

	public PropertyPhoto removePropertyPhoto(PropertyPhoto propertyPhoto) {
		getPropertyPhotos().remove(propertyPhoto);
		propertyPhoto.setPmsProperty(null);

		return propertyPhoto;
	}

	/*public List<PropertyTaxe> getPropertyTaxes() {
		return this.propertyTaxes;
	}

	public void setPropertyTaxes(List<PropertyTaxe> propertyTaxes) {
		this.propertyTaxes = propertyTaxes;
	}*/

	/*public PropertyTaxe addPropertyTaxe(PropertyTaxe propertyTaxe) {
		getPropertyTaxes().add(propertyTaxe);
		propertyTaxe.setPmsProperty(this);

		return propertyTaxe;
	}

	public PropertyTaxe removePropertyTaxe(PropertyTaxe propertyTaxe) {
		getPropertyTaxes().remove(propertyTaxe);
		propertyTaxe.setPmsProperty(null);

		return propertyTaxe;
	}
    */
	
	public Timestamp getTaxValidFrom() {
		return taxValidFrom;
	}

	public void setTaxValidFrom(Timestamp taxValidFrom) {
		this.taxValidFrom = taxValidFrom;
	}
	
	 public Boolean getPayAtHotelStatus() {
		return payAtHotelStatus;
	}

	public void setPayAtHotelStatus(Boolean payAtHotelStatus) {
		this.payAtHotelStatus = payAtHotelStatus;
	}

	public Timestamp getTaxValidTo() {
		return taxValidTo;
	}

	public void setTaxValidTo(Timestamp taxValidTo) {
		this.taxValidTo = taxValidTo;
	}

	public Boolean getTaxIsActive() {
		return taxIsActive;
	}

	public void setTaxIsActive(Boolean taxIsActive) {
		this.taxIsActive = taxIsActive;
	}
	
	public Location getLocation() {
		return this.location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}
	
	public GoogleLocation getGoogleLocation() {
		return googleLocation;
	}

	public void setGoogleLocation(GoogleLocation googleLocation) {
		this.googleLocation = googleLocation;
	}

	public Area getArea() {
		return this.area;
	}

	public void setArea(Area area) {
		this.area = area;
	}
	
	public String getPropertyHotelPolicy()
	{
		return propertyHotelPolicy;
	}
	
	public void setPropertyHotelPolicy(String propertyHotelPolicy){
		this.propertyHotelPolicy=propertyHotelPolicy;
	}
	
	public String getPropertyStandardPolicy()
	{
		return propertyStandardPolicy;
	}
	
	public void setPropertyStandardPolicy(String propertyStandardPolicy){
		this.propertyStandardPolicy=propertyStandardPolicy;
	}
	
	public String getPropertyCancellationPolicy()
	{
		return propertyCancellationPolicy;
	}
	
	public void setPropertyCancellationPolicy(String propertyCancellationPolicy){
		this.propertyCancellationPolicy=propertyCancellationPolicy;
	}
	
	public String getPropertyChildPolicy() {
		return propertyChildPolicy;
	}

	public void setPropertyChildPolicy(String propertyChildPolicy) {
		this.propertyChildPolicy = propertyChildPolicy;
	}
	
	public Double getPropertyCommission() {
		return propertyCommission;
	}

	public void setPropertyCommission(Double propertyCommission) {
		this.propertyCommission = propertyCommission;
	}
	
	public String getPlaceId() {
		return placeId;
	}

	public void setPlaceId(String placeId) {
		this.placeId = placeId;
	}	
	
	public String getRouteMap() {
		return routeMap;
	}

	public void setRouteMap(String routeMap) {
		this.routeMap = routeMap;
	}

	public String getContractManagerName() {
		return contractManagerName;
	}

	public void setContractManagerName(String contractManagerName) {
		this.contractManagerName = contractManagerName;
	}

	public String getContractManagerEmail() {
		return contractManagerEmail;
	}

	public void setContractManagerEmail(String contractManagerEmail) {
		this.contractManagerEmail = contractManagerEmail;
	}

	public String getContractManagerContact() {
		return contractManagerContact;
	}

	public void setContractManagerContact(String contractManagerContact) {
		this.contractManagerContact = contractManagerContact;
	}

	public String getRevenueManagerName() {
		return revenueManagerName;
	}

	public void setRevenueManagerName(String revenueManagerName) {
		this.revenueManagerName = revenueManagerName;
	}

	public String getRevenueManagerEmail() {
		return revenueManagerEmail;
	}

	public void setRevenueManagerEmail(String revenueManagerEmail) {
		this.revenueManagerEmail = revenueManagerEmail;
	}

	public String getRevenueManagerContact() {
		return revenueManagerContact;
	}

	public void setRevenueManagerContact(String revenueManagerContact) {
		this.revenueManagerContact = revenueManagerContact;
	}
	
	public String getPropertyGstNo() {
		return propertyGstNo;
	}

	public void setPropertyGstNo(String propertyGstNo) {
		this.propertyGstNo = propertyGstNo;
	}
	
	public String getPropertyFinanceEmail() {
		return propertyFinanceEmail;
	}

	public void setPropertyFinanceEmail(String propertyFinanceEmail) {
		this.propertyFinanceEmail = propertyFinanceEmail;
	}
	
	public String getPropertyUrl() {
		return propertyUrl;
	}

	public void setPropertyUrl(String propertyUrl) {
		this.propertyUrl = propertyUrl;
	}

	public String getPropertyReviewLink() {
		return propertyReviewLink;
	}

	public void setPropertyReviewLink(String propertyReviewLink) {
		this.propertyReviewLink = propertyReviewLink;
	}
	
	public String getTripadvisorReviewLink() {
		return tripadvisorReviewLink;
	}

	public void setTripadvisorReviewLink(String tripadvisorReviewLink) {
		this.tripadvisorReviewLink = tripadvisorReviewLink;
	}

	public List<OtaHotels> getOtaHotels() {
		return otaHotels;
	}

	public void setOtaHotels(List<OtaHotels> otaHotels) {
		this.otaHotels = otaHotels;
	}

	public List<OtaHotelDetails> getOtaHotelDetails() {
		return otaHotelDetails;
	}

	public void setOtaHotelDetails(List<OtaHotelDetails> otaHotelDetails) {
		this.otaHotelDetails = otaHotelDetails;
	}
	
	public List<PropertyTags> getPropertyTags() {
		return propertyTags;
	}

	public void setPropertyTags(List<PropertyTags> propertyTags) {
		this.propertyTags = propertyTags;
	}

	public List<PropertyPaymentDetails> getPropertyPaymentDetails() {
		return propertyPaymentDetails;
	}

	public void setPropertyPaymentDetails(List<PropertyPaymentDetails> propertyPaymentDetails) {
		this.propertyPaymentDetails = propertyPaymentDetails;
	}

	public Boolean getCoupleIsActive() {
		return coupleIsActive;
	}

	public void setCoupleIsActive(Boolean coupleIsActive) {
		this.coupleIsActive = coupleIsActive;
	}

	public Boolean getBreakfastIsActive() {
		return breakfastIsActive;
	}

	public void setBreakfastIsActive(Boolean breakfastIsActive) {
		this.breakfastIsActive = breakfastIsActive;
	}

	public Boolean getHotelIsActive() {
		return hotelIsActive;
	}

	public void setHotelIsActive(Boolean hotelIsActive) {
		this.hotelIsActive = hotelIsActive;
	}
	
	
	public PropertyContractType getContractType() {
		return contractType;
	}

	public void setContractType(PropertyContractType contractType) {
		this.contractType = contractType;
	}
	
	public Double getWalkinCommission() {
		return walkinCommission;
	}

	public void setWalkinCommission(Double walkinCommission) {
		this.walkinCommission = walkinCommission;
	}

	public Double getOnlineCommission() {
		return onlineCommission;
	}

	public void setOnlineCommission(Double onlineCommission) {
		this.onlineCommission = onlineCommission;
	}

	public Double getNetRateRevenue() {
		return netRateRevenue;
	}

	public void setNetRateRevenue(Double netRateRevenue) {
		this.netRateRevenue = netRateRevenue;
	}
	
	public Boolean getPropertyIsControl() {
		return propertyIsControl;
	}

	public void setPropertyIsControl(Boolean propertyIsControl) {
		this.propertyIsControl = propertyIsControl;
	}

	public Integer getPaynowDiscount() {
		return paynowDiscount;
	}

	public void setPaynowDiscount(Integer paynowDiscount) {
		this.paynowDiscount = paynowDiscount;
	}
	
	public Boolean getFlexibleStayStatus() {
		return flexibleStayStatus;
	}

	public void setFlexibleStayStatus(Boolean flexibleStayStatus) {
		this.flexibleStayStatus = flexibleStayStatus;
	}

	public Boolean getTravelDiscountStatus() {
		return travelDiscountStatus;
	}

	public void setTravelDiscountStatus(Boolean travelDiscountStatus) {
		this.travelDiscountStatus = travelDiscountStatus;
	}

	public Boolean getStopSellIsActive() {
		return stopSellIsActive;
	}

	public void setStopSellIsActive(Boolean stopSellIsActive) {
		this.stopSellIsActive = stopSellIsActive;
	}

	public Boolean getDelistIsActive() {
		return delistIsActive;
	}

	public void setDelistIsActive(Boolean delistIsActive) {
		this.delistIsActive = delistIsActive;
	}

	public String getPropertyGigiPolicy() {
		return propertyGigiPolicy;
	}

	public void setPropertyGigiPolicy(String propertyGigiPolicy) {
		this.propertyGigiPolicy = propertyGigiPolicy;
	}	
	
	
}