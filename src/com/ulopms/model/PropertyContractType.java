package com.ulopms.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * The persistent class for the pms_smart_price database table.
 * 
 */
@Entity
@Table(name="property_contract_type")
@NamedQuery(name="PropertyContractType.findAll", query="SELECT p FROM PropertyContractType p")


public class PropertyContractType implements Serializable {
	private static final long serialVersionUID = 1L;

	

	@Id
	@SequenceGenerator(name="id",sequenceName="property_contract_type_contract_type_id_seq")
	@GeneratedValue(generator="id")
	
	@Column(name="contract_type_id")
	private Integer contractTypeId;

	@Column(name="contract_type_name")
	private String contractTypeName;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_deleted")
	private Boolean isDeleted;
	
	@Column(name="type_is_active")
	private Boolean typeIsActive;
	
	@Column(name="created_by")
	private Integer createdBy;

	@Column(name="created_on")
	private Timestamp createdOn;
	
	@Column(name="modified_by")
	private Integer modifiedBy;

	@Column(name="modified_on")
	private Timestamp modifiedOn;
	
	//bi-directional many-to-one association to PropertyAccommodation
	@OneToMany(mappedBy="propertyContractType")
	private List<PropertyAccommodation> propertyAccommodations;
		
	

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	
	public Boolean getTypeIsActive() {
		return typeIsActive;
	}

	public void setTypeIsActive(Boolean typeIsActive) {
		this.typeIsActive = typeIsActive;
	}

	public Integer getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	
	public Integer getModifiedBy() {
		return this.modifiedBy;
	}

	public Integer getContractTypeId() {
		return contractTypeId;
	}

	public void setContractTypeId(Integer contractTypeId) {
		this.contractTypeId = contractTypeId;
	}

	public String getContractTypeName() {
		return contractTypeName;
	}

	public void setContractTypeName(String contractTypeName) {
		this.contractTypeName = contractTypeName;
	}

	public Timestamp getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public Timestamp getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Timestamp modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public List<PropertyAccommodation> getPropertyAccommodations() {
		return propertyAccommodations;
	}

	public void setPropertyAccommodations(
			List<PropertyAccommodation> propertyAccommodations) {
		this.propertyAccommodations = propertyAccommodations;
	}

	
}