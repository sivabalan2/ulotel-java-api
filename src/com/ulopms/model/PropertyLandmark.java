package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the pms_amenities database table.
 * 
 */
@Entity
@Table(name="property_landmark")
@NamedQuery(name="PropertyLandmark.findAll", query="SELECT p FROM PropertyLandmark p")
public class PropertyLandmark implements Serializable {
	private static final long serialVersionUID = 1L;
 
	
	@Id  
    @SequenceGenerator(name="id",sequenceName="property_landmark_property_landmark_id_seq")
	@GeneratedValue(generator="id")
	@Column(name="property_landmark_id", unique=true, nullable=false)
	private Integer propertyLandmarkId;
	
	@Column(name="property_landmark1", length=100)
	private String propertyLandmark1;
	
	@Column(name="property_landmark2", length=100)
	private String propertyLandmark2;
	
	@Column(name="property_landmark3", length=100)
	private String propertyLandmark3;
	
	@Column(name="property_landmark4", length=100)
	private String propertyLandmark4;
	
	@Column(name="property_landmark5", length=100)
	private String propertyLandmark5;
	
	@Column(name="property_landmark6", length=100)
	private String propertyLandmark6;
	
	@Column(name="property_landmark7", length=100)
	private String propertyLandmark7;
	
	@Column(name="property_landmark8", length=100)
	private String propertyLandmark8;
	
	@Column(name="property_landmark9", length=100)
	private String propertyLandmark9;
	
	@Column(name="property_landmark10", length=100)
	private String propertyLandmark10;
	
	@Column(name="property_attraction1", length=100)
	private String propertyAttraction1;
	
	@Column(name="property_attraction2", length=100)
	private String propertyAttraction2;
	
	@Column(name="property_attraction3", length=100)
	private String propertyAttraction3;
	
	@Column(name="property_attraction4", length=100)
	private String propertyAttraction4;
	
	@Column(name="property_attraction5", length=100)
	private String propertyAttraction5;
	
	
	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_deleted")
	private Boolean isDeleted;
	
	@Column(name="distance1", precision=131089)
	private Double distance1;
	
	@Column(name="distance2", precision=131089)
	private Double distance2;
	
	@Column(name="distance3", precision=131089)
	private Double distance3;
	
	@Column(name="distance4", precision=131089)
	private Double distance4;
	
	@Column(name="distance5", precision=131089)
	private Double distance5;

	@Column(name="kilometers1", precision=131089)
	private double kilometers1;
	
	@Column(name="kilometers2", precision=131089)
	private double kilometers2;
	
	@Column(name="kilometers3", precision=131089)
	private double kilometers3;
	
	@Column(name="kilometers4", precision=131089)
	private double kilometers4;
	
	@Column(name="kilometers5", precision=131089)
	private double kilometers5;
	
	@Column(name="kilometers6", precision=131089)
	private double kilometers6;
	
	@Column(name="kilometers7", precision=131089)
	private double kilometers7;
	
	@Column(name="kilometers8", precision=131089)
	private double kilometers8;
	
	@Column(name="kilometers9", precision=131089)
	private double kilometers9;
	
	@Column(name="kilometers10", precision=131089)
	private double kilometers10;
	


	//bi-directional many-to-one association to PmsProperty
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="property_id")
	private PmsProperty pmsProperty;

	

	public PropertyLandmark() {
	}

	public Integer getPropertyLandmarkId() {
		return propertyLandmarkId;
	}

	public void setPropertyLandmarkId(Integer propertyLandmarkId) {
		this.propertyLandmarkId = propertyLandmarkId;
	}

	public String getPropertyLandmark1() {
		return propertyLandmark1;
	}

	public void setPropertyLandmark1(String propertyLandmark1) {
		this.propertyLandmark1 = propertyLandmark1;
	}

	public String getPropertyLandmark2() {
		return propertyLandmark2;
	}

	public void setPropertyLandmark2(String propertyLandmark2) {
		this.propertyLandmark2 = propertyLandmark2;
	}

	public String getPropertyLandmark3() {
		return propertyLandmark3;
	}

	public void setPropertyLandmark3(String propertyLandmark3) {
		this.propertyLandmark3 = propertyLandmark3;
	}

	public String getPropertyLandmark4() {
		return propertyLandmark4;
	}

	public void setPropertyLandmark4(String propertyLandmark4) {
		this.propertyLandmark4 = propertyLandmark4;
	}

	public String getPropertyLandmark5() {
		return propertyLandmark5;
	}

	public void setPropertyLandmark5(String propertyLandmark5) {
		this.propertyLandmark5 = propertyLandmark5;
	}

	public String getPropertyLandmark6() {
		return propertyLandmark6;
	}

	public void setPropertyLandmark6(String propertyLandmark6) {
		this.propertyLandmark6 = propertyLandmark6;
	}

	public String getPropertyLandmark7() {
		return propertyLandmark7;
	}

	public void setPropertyLandmark7(String propertyLandmark7) {
		this.propertyLandmark7 = propertyLandmark7;
	}

	public String getPropertyLandmark8() {
		return propertyLandmark8;
	}

	public void setPropertyLandmark8(String propertyLandmark8) {
		this.propertyLandmark8 = propertyLandmark8;
	}

	public String getPropertyLandmark9() {
		return propertyLandmark9;
	}

	public void setPropertyLandmark9(String propertyLandmark9) {
		this.propertyLandmark9 = propertyLandmark9;
	}

	public String getPropertyLandmark10() {
		return propertyLandmark10;
	}

	public void setPropertyLandmark10(String propertyLandmark10) {
		this.propertyLandmark10 = propertyLandmark10;
	}
	
	public String getPropertyAttraction1() {
		return propertyAttraction1;
	}

	public void setPropertyAttraction1(String propertyAttraction1) {
		this.propertyAttraction1 = propertyAttraction1;
	}

	public String getPropertyAttraction2() {
		return propertyAttraction2;
	}

	public void setPropertyAttraction2(String propertyAttraction2) {
		this.propertyAttraction2 = propertyAttraction2;
	}

	public String getPropertyAttraction3() {
		return propertyAttraction3;
	}

	public void setPropertyAttraction3(String propertyAttraction3) {
		this.propertyAttraction3 = propertyAttraction3;
	}

	public String getPropertyAttraction4() {
		return propertyAttraction4;
	}

	public void setPropertyAttraction4(String propertyAttraction4) {
		this.propertyAttraction4 = propertyAttraction4;
	}

	public String getPropertyAttraction5() {
		return propertyAttraction5;
	}

	public void setPropertyAttraction5(String propertyAttraction5) {
		this.propertyAttraction5 = propertyAttraction5;
	}

	public Double getDistance1() {
		return distance1;
	}

	public void setDistance1(Double distance1) {
		this.distance1 = distance1;
	}

	public Double getDistance2() {
		return distance2;
	}

	public void setDistance2(Double distance2) {
		this.distance2 = distance2;
	}

	public Double getDistance3() {
		return distance3;
	}

	public void setDistance3(Double distance3) {
		this.distance3 = distance3;
	}

	public Double getDistance4() {
		return distance4;
	}

	public void setDistance4(Double distance4) {
		this.distance4 = distance4;
	}

	public Double getDistance5() {
		return distance5;
	}

	public void setDistance5(Double distance5) {
		this.distance5 = distance5;
	}

	public double getKilometers1() {
		return kilometers1;
	}

	public void setKilometers1(double kilometers1) {
		this.kilometers1 = kilometers1;
	}

	public double getKilometers2() {
		return kilometers2;
	}

	public void setKilometers2(double kilometers2) {
		this.kilometers2 = kilometers2;
	}

	public double getKilometers3() {
		return kilometers3;
	}

	public void setKilometers3(double kilometers3) {
		this.kilometers3 = kilometers3;
	}

	public double getKilometers4() {
		return kilometers4;
	}

	public void setKilometers4(double kilometers4) {
		this.kilometers4 = kilometers4;
	}

	public double getKilometers5() {
		return kilometers5;
	}

	public void setKilometers5(double kilometers5) {
		this.kilometers5 = kilometers5;
	}

	public double getKilometers6() {
		return kilometers6;
	}

	public void setKilometers6(double kilometers6) {
		this.kilometers6 = kilometers6;
	}

	public double getKilometers7() {
		return kilometers7;
	}

	public void setKilometers7(double kilometers7) {
		this.kilometers7 = kilometers7;
	}

	public double getKilometers8() {
		return kilometers8;
	}

	public void setKilometers8(double kilometers8) {
		this.kilometers8 = kilometers8;
	}

	public double getKilometers9() {
		return kilometers9;
	}

	public void setKilometers9(double kilometers9) {
		this.kilometers9 = kilometers9;
	}

	public double getKilometers10() {
		return kilometers10;
	}

	public void setKilometers10(double kilometers10) {
		this.kilometers10 = kilometers10;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	
	public PmsProperty getPmsProperty() {
		return pmsProperty;
	}

	public void setPmsProperty(PmsProperty pmsProperty) {
		this.pmsProperty = pmsProperty;
	}	

	
}