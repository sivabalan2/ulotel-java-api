package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;


/**
 * The persistent class for the user_client_token database table.
 * 
 */
@Entity
@Table(name="user_client_token")
@NamedQuery(name="UserClientToken.findAll", query="SELECT u FROM UserClientToken u")
public class UserClientToken implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="token_code", unique=true, nullable=false, length=50)
	private String tokenCode;

	@Column(name="created_by")
	private Integer createdBy;

	@Column(name="created_on")
	private Timestamp createdOn;

	@Column(name="expiry_on")
	private Timestamp expiryOn;

	@Column(name="is_active", nullable=false)
	private Boolean isActive;

	@Column(name="is_deleted", nullable=false)
	private Boolean isDeleted;

	@Column(name="token_id", nullable=false)
	private Integer tokenId;

	@Column(name="user_ip_address", length=20)
	private String userIpAddress;

	//bi-directional many-to-one association to User
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="user_id", nullable=false)
	private User user;

	public UserClientToken() {
	}

	public String getTokenCode() {
		return this.tokenCode;
	}

	public void setTokenCode(String tokenCode) {
		this.tokenCode = tokenCode;
	}

	public Integer getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public Timestamp getExpiryOn() {
		return this.expiryOn;
	}

	public void setExpiryOn(Timestamp expiryOn) {
		this.expiryOn = expiryOn;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Integer getTokenId() {
		return this.tokenId;
	}

	public void setTokenId(Integer tokenId) {
		this.tokenId = tokenId;
	}

	public String getUserIpAddress() {
		return this.userIpAddress;
	}

	public void setUserIpAddress(String userIpAddress) {
		this.userIpAddress = userIpAddress;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}