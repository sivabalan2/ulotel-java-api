package com.ulopms.model;

public class Revenue {
	
	public Revenue() {
	}
	
	private double amount;

	private long nights;
	
	private Integer accommodationId; 
	
	private double taxAmount;
	
	private Integer propertyDiscountId;
	
	private Integer smartPriceId;
	
	private Integer promotionId;

	public double getTaxAmount() {
		return taxAmount;
	}


	public void setTaxAmount(double taxAmount) {
		this.taxAmount = taxAmount;
	}


	public Integer getPropertyDiscountId() {
		return propertyDiscountId;
	}


	public void setPropertyDiscountId(Integer propertyDiscountId) {
		this.propertyDiscountId = propertyDiscountId;
	}


	public Integer getSmartPriceId() {
		return smartPriceId;
	}


	public void setSmartPriceId(Integer smartPriceId) {
		this.smartPriceId = smartPriceId;
	}


	public Integer getPromotionId() {
		return promotionId;
	}


	public void setPromotionId(Integer promotionId) {
		this.promotionId = promotionId;
	}


	public double getAmount() {
		return amount;
	}


	public void setAmount(double amount) {
		this.amount = amount;
	}


	
	public long getNights() {
		return nights;
	}


	public void setNights(long nights) {
		this.nights = nights;
	}


	public Integer getAccommodationId() {
		return accommodationId;
	}


	public void setAccommodationId(Integer accommodationId) {
		this.accommodationId = accommodationId;
	}
	
	
}
