package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;


/**
 * The persistent class for the property_areas database table.
 * 
 */
@Entity
@Table(name="google_property_areas")
@NamedQuery(name="GooglePropertyAreas.findAll", query="SELECT p FROM GooglePropertyAreas p")
public class GooglePropertyAreas implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="id",sequenceName="google_property_areas_google_property_area_id_seq")
	@GeneratedValue(generator="id")
	@Column(name="google_property_area_id", unique=true, nullable=false)
	private Integer googlePropertyAreaId;

	@Column(name="created_by")
	private Integer createdBy;

	@Column(name="created_date")
	private Timestamp createdDate;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_deleted")
	private Boolean isDeleted;

	@Column(name="modified_by")
	private Integer modifiedBy;

	@Column(name="modified_date")
	private Timestamp modifiedDate;
	
	@Column(name="property_is_control")
	private Boolean propertyIsControl;

	
	//bi-directional many-to-one association to PmsProperty
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="property_id")
	private PmsProperty pmsProperty;
	
	//bi-directional many-to-one association to PmsProperty
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="google_area_id")
	private GoogleArea googleArea;

	public GooglePropertyAreas() {
	}


	public Integer getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Integer getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Timestamp getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public PmsProperty getPmsProperty() {
		return this.pmsProperty;
	}

	public void setPmsProperty(PmsProperty pmsProperty) {
		this.pmsProperty = pmsProperty;
	}

	public Boolean getPropertyIsControl() {
		return propertyIsControl;
	}

	public void setPropertyIsControl(Boolean propertyIsControl) {
		this.propertyIsControl = propertyIsControl;
	}

	public Integer getGooglePropertyAreaId() {
		return googlePropertyAreaId;
	}

	public void setGooglePropertyAreaId(Integer googlePropertyAreaId) {
		this.googlePropertyAreaId = googlePropertyAreaId;
	}

	public GoogleArea getGoogleArea() {
		return googleArea;
	}

	public void setGoogleArea(GoogleArea googleArea) {
		this.googleArea = googleArea;
	}
	

}