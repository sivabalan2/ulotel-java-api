package com.ulopms.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * The persistent class for the pms_smart_price database table.
 * 
 */
@Entity
@Table(name="property_roles")
@NamedQuery(name="PropertyRoles.findAll", query="SELECT p FROM PropertyRoles p")


public class PropertyRoles implements Serializable {
	private static final long serialVersionUID = 1L;

	

	@Id
	@SequenceGenerator(name="id",sequenceName="property_roles_property_role_id_seq")
	@GeneratedValue(generator="id")
	
	@Column(name="property_role_id")
	private Integer propertyRoleId;

	@Column(name="property_role_name")
	private String propertyRoleName;
	

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_deleted")
	private Boolean isDeleted;
	
	@Column(name="created_by")
	private Integer createdBy;

	@Column(name="created_on")
	private Timestamp createdOn;
	
	@Column(name="modified_by")
	private Integer modifiedBy;

	@Column(name="modified_on")
	private Timestamp modifiedOn;
		
	

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	
	public Integer getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}
	
	public Integer getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Timestamp getModifiedOn() {
		return this.modifiedOn;
	}

	public void setModifiedOn(Timestamp modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
	
	public Integer getPropertyRoleId() {
		return propertyRoleId;
	}

	public void setPropertyRoleId(Integer propertyRoleId) {
		this.propertyRoleId = propertyRoleId;
	}

	public String getPropertyRoleName() {
		return propertyRoleName;
	}

	public void setPropertyRoleName(String propertyRoleName) {
		this.propertyRoleName = propertyRoleName;
	}
	
}