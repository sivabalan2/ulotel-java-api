package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;


/**
 * The persistent class for the property_photo database table.
 * 
 */
@Entity
@Table(name="property_photo")
@NamedQuery(name="PropertyPhoto.findAll", query="SELECT p FROM PropertyPhoto p")
public class PropertyPhoto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="id",sequenceName="property_photo_property_photo_id_seq")
	@GeneratedValue(generator="id")
	@Column(name="property_photo_id", unique=true, nullable=false)
	private Integer propertyPhotoId;

	@Column(name="created_by")
	private Integer createdBy;

	@Column(name="created_date")
	private Timestamp createdDate;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_deleted")
	private Boolean isDeleted;

	@Column(name="is_primary")
	private Boolean isPrimary;

	@Column(name="modified_by")
	private Integer modifiedBy;

	@Column(name="modified_date")
	private Timestamp modifiedDate;

	@Column(name="photo_path", length=250)
	private String photoPath;
	
	@Column(name="image_name", length=250)
	private String imageName;
	
	@Column(name="image_alt_name", length=250)
	private String imageAltName;

	//bi-directional many-to-one association to PmsProperty
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="property_id")
	private PmsProperty pmsProperty;
	
	//bi-directional many-to-one association to PmsProperty
	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="photo_category_id")
	private PmsPhotoCategory pmsPhotoCategory;
	
    @ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="accommodation_id")
	private PropertyAccommodation propertyAccommodation;


	public PropertyPhoto() {
	}

	public Integer getPropertyPhotoId() {
		return this.propertyPhotoId;
	}

	public void setPropertyPhotoId(Integer propertyPhotoId) {
		this.propertyPhotoId = propertyPhotoId;
	}

	public Integer getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Boolean getIsPrimary() {
		return this.isPrimary;
	}

	public void setIsPrimary(Boolean isPrimary) {
		this.isPrimary = isPrimary;
	}

	public Integer getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Timestamp getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getPhotoPath() {
		return this.photoPath;
	}

	public void setPhotoPath(String photoPath) {
		this.photoPath = photoPath;
	}

	public PmsProperty getPmsProperty() {
		return this.pmsProperty;
	}

	public void setPmsProperty(PmsProperty pmsProperty) {
		this.pmsProperty = pmsProperty;
	}
	
	public PmsPhotoCategory getPmsPhotoCategory() {
		return this.pmsPhotoCategory;
	}

	public void setPmsPhotoCategory(PmsPhotoCategory pmsPhotoCategory) {
		this.pmsPhotoCategory = pmsPhotoCategory;
	}
	
   public PropertyAccommodation getPropertyAccommodation() {
		return propertyAccommodation;
	}

	public void setPropertyAccommodation(PropertyAccommodation propertyAccommodation) {
		this.propertyAccommodation = propertyAccommodation;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public String getImageAltName() {
		return imageAltName;
	}

	public void setImageAltName(String imageAltName) {
		this.imageAltName = imageAltName;
	}

}