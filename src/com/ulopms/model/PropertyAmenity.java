package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;


/**
 * The persistent class for the property_amenities database table.
 * 
 */
@Entity
@Table(name="property_amenities")
@NamedQuery(name="PropertyAmenity.findAll", query="SELECT p FROM PropertyAmenity p")
public class PropertyAmenity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="id",sequenceName="property_amenities_property_amenity_id_seq")
	@GeneratedValue(generator="id")
	@Column(name="property_amenity_id", unique=true, nullable=false)
	private Integer propertyAmenityId;

	@Column(name="created_by")
	private Integer createdBy;

	@Column(name="created_date")
	private Timestamp createdDate;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_deleted")
	private Boolean isDeleted;

	@Column(name="modified_by")
	private Integer modifiedBy;

	@Column(name="modified_date")
	private Timestamp modifiedDate;

	@Column(name="is_tag_amenity")
	private Boolean isTagAmenity;
	
	//bi-directional many-to-one association to PmsProperty
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="property_id")
	private PmsProperty pmsProperty;
	
	//bi-directional many-to-one association to PmsProperty
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="amenity_id")
	private PmsAmenity pmsAmenity;

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="tag_id")
	private PmsTags pmsTags;
		
	public PropertyAmenity() {
	}

	public Integer getPropertyAmenityId() {
		return this.propertyAmenityId;
	}

	public void setPropertyAmenityId(Integer propertyAmenityId) {
		this.propertyAmenityId = propertyAmenityId;
	}

	public Integer getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Integer getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Timestamp getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public PmsProperty getPmsProperty() {
		return this.pmsProperty;
	}

	public void setPmsProperty(PmsProperty pmsProperty) {
		this.pmsProperty = pmsProperty;
	}

	public PmsAmenity getPmsAmenity() {
		return pmsAmenity;
	}

	public void setPmsAmenity(PmsAmenity pmsAmenity) {
		this.pmsAmenity = pmsAmenity;
	}

	public Boolean getIsTagAmenity() {
		return isTagAmenity;
	}

	public void setIsTagAmenity(Boolean isTagAmenity) {
		this.isTagAmenity = isTagAmenity;
	}

	public PmsTags getPmsTags() {
		return pmsTags;
	}

	public void setPmsTags(PmsTags pmsTags) {
		this.pmsTags = pmsTags;
	}
	
	
}