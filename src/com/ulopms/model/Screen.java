package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the screen database table.
 * 
 */
@Entity
@Table(name="screen")
@NamedQuery(name="Screen.findAll", query="SELECT s FROM Screen s")
public class Screen implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="id",sequenceName="mast_screen_screen_id_seq")
	@GeneratedValue(generator="id")
	@Column(name="screen_id", unique=true, nullable=false)
	private Integer screenId;

	@Column(name="created_by")
	private Integer createdBy;

	@Column(name="created_on")
	private Timestamp createdOn;

	@Column(name="is_active", nullable=false)
	private Boolean isActive;

	@Column(name="is_deleted", nullable=false)
	private Boolean isDeleted;

	@Column(name="menu_id")
	private Integer menuId;

	@Column(name="modification_counter")
	private Integer modificationCounter;

	@Column(name="modified_by")
	private Integer modifiedBy;

	@Column(name="modified_on")
	private Timestamp modifiedOn;

	@Column(name="screen_name", length=50)
	private String screenName;

	@Column(name="screen_path", length=50)
	private String screenPath;

	@Column(name="screen_purpose", length=50)
	private String screenPurpose;

	//bi-directional many-to-one association to ConfRoleScreen
	@OneToMany(mappedBy="screen")
	private List<ConfRoleScreen> confRoleScreens;

	public Screen() {
	}

	public Integer getScreenId() {
		return this.screenId;
	}

	public void setScreenId(Integer screenId) {
		this.screenId = screenId;
	}

	public Integer getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Integer getMenuId() {
		return this.menuId;
	}

	public void setMenuId(Integer menuId) {
		this.menuId = menuId;
	}

	public Integer getModificationCounter() {
		return this.modificationCounter;
	}

	public void setModificationCounter(Integer modificationCounter) {
		this.modificationCounter = modificationCounter;
	}

	public Integer getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Timestamp getModifiedOn() {
		return this.modifiedOn;
	}

	public void setModifiedOn(Timestamp modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public String getScreenName() {
		return this.screenName;
	}

	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}

	public String getScreenPath() {
		return this.screenPath;
	}

	public void setScreenPath(String screenPath) {
		this.screenPath = screenPath;
	}

	public String getScreenPurpose() {
		return this.screenPurpose;
	}

	public void setScreenPurpose(String screenPurpose) {
		this.screenPurpose = screenPurpose;
	}

	public List<ConfRoleScreen> getConfRoleScreens() {
		return this.confRoleScreens;
	}

	public void setConfRoleScreens(List<ConfRoleScreen> confRoleScreens) {
		this.confRoleScreens = confRoleScreens;
	}

	public ConfRoleScreen addConfRoleScreen(ConfRoleScreen confRoleScreen) {
		getConfRoleScreens().add(confRoleScreen);
		confRoleScreen.setScreen(this);

		return confRoleScreen;
	}

	public ConfRoleScreen removeConfRoleScreen(ConfRoleScreen confRoleScreen) {
		getConfRoleScreens().remove(confRoleScreen);
		confRoleScreen.setScreen(null);

		return confRoleScreen;
	}

}