package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the blog_locations database table.
 * 
 */
@Entity
@Table(name="blog_locations")
@NamedQuery(name="BlogLocations.findAll", query="SELECT p FROM BlogLocations p")
public class BlogLocations implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="id",sequenceName="blog_locations_blog_location_id_seq")
	@GeneratedValue(generator="id")
	@Column(name="blog_location_id", unique=true, nullable=false)
	private Integer blogLocationId;
	
	@Column(name="blog_location_name", length=250)
	private String blogLocationName;
	
	@Column(name="blog_location_content", length=10485760)
	private String blogLocationContent;
	
	@Column(name="blog_location_image", length=250)
	private String blogLocationImage; 

	@Column(name="created_by")
	private Integer createdBy;

	@Column(name="created_date")
	private Timestamp createdDate;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_deleted")
	private Boolean isDeleted;
	
	@Column(name="modified_by")
	private Integer modifiedBy;

	@Column(name="modified_date")
	private Timestamp modifiedDate;
	
	@Column(name="blog_location_url", length=250)
	private String blogLocationUrl; 
	
	//bi-directional many-to-one association to BlogCategories
	@OneToMany(mappedBy="blogLocations")
	private List<BlogArticles> blogArticles;
		

	public BlogLocations() {
	}	

	public Integer getBlogLocationId() {
		return blogLocationId;
	}

	public void setBlogLocationId(Integer blogLocationId) {
		this.blogLocationId = blogLocationId;
	}

	public String getBlogLocationName() {
		return blogLocationName;
	}

	public void setBlogLocationName(String blogLocationName) {
		this.blogLocationName = blogLocationName;
	}

	public String getBlogLocationContent() {
		return blogLocationContent;
	}

	public void setBlogLocationContent(String blogLocationContent) {
		this.blogLocationContent = blogLocationContent;
	}

	public String getBlogLocationImage() {
		return blogLocationImage;
	}

	public void setBlogLocationImage(String blogLocationImage) {
		this.blogLocationImage = blogLocationImage;
	}

	public Integer getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}


	public Integer getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Timestamp getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}	
	
	public List<BlogArticles> getBlogArticles() {
		return blogArticles;
	}

	public void setBlogArticles(List<BlogArticles> blogArticles) {
		this.blogArticles = blogArticles;
	}

	public String getBlogLocationUrl() {
		return blogLocationUrl;
	}

	public void setBlogLocationUrl(String blogLocationUrl) {
		this.blogLocationUrl = blogLocationUrl;
	}

}