package com.ulopms.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * The persistent class for the ulotel_hotel_detail_tags database table.
 * 
 */
@Entity
@Table(name="ulotel_hotel_detail_tags")
@NamedQuery(name="UlotelHotelDetailTags.findAll", query="SELECT p FROM UlotelHotelDetailTags p")


public class UlotelHotelDetailTags implements Serializable {
	private static final long serialVersionUID = 1L;

	

	@Id
	@SequenceGenerator(name="id",sequenceName="ulotel_hotel_detail_tags_hotel_tag_id_seq")
	@GeneratedValue(generator="id")
	@Column(name="hotel_tag_id", unique=true, nullable=false)
	private Integer hotelTagId;

	@Column(name="hotel_profile")
	private String hotelProfile;
	
	@Column(name="room_categories")
	private String roomCategories;
	
	@Column(name="rate_plan")
	private String ratePlan;
	
	@Column(name="hotel_amenities")
	private String hotelAmenities;
	
	@Column(name="hotel_policy")
	private String hotelPolicy;
	
	@Column(name="add_on")
	private String addOn;
	
	@Column(name="hotel_landmark")
	private String hotelLandmark;
	
	@Column(name="hotel_images")
	private String hotelImages;
	
	@Column(name="hotel_status")
	private String hotelStatus;
		
	@ManyToOne
	@JoinColumn(name="property_id")
	private PmsProperty pmsProperty;

	

	public Integer getHotelTagId() {
		return hotelTagId;
	}

	public void setHotelTagId(Integer hotelTagId) {
		this.hotelTagId = hotelTagId;
	}

	public String getHotelProfile() {
		return hotelProfile;
	}

	public void setHotelProfile(String hotelProfile) {
		this.hotelProfile = hotelProfile;
	}

	public String getRoomCategories() {
		return roomCategories;
	}

	public void setRoomCategories(String roomCategories) {
		this.roomCategories = roomCategories;
	}

	public String getRatePlan() {
		return ratePlan;
	}

	public void setRatePlan(String ratePlan) {
		this.ratePlan = ratePlan;
	}

	public String getHotelAmenities() {
		return hotelAmenities;
	}

	public void setHotelAmenities(String hotelAmenities) {
		this.hotelAmenities = hotelAmenities;
	}

	public String getHotelPolicy() {
		return hotelPolicy;
	}

	public void setHotelPolicy(String hotelPolicy) {
		this.hotelPolicy = hotelPolicy;
	}

	public String getAddOn() {
		return addOn;
	}

	public void setAddOn(String addOn) {
		this.addOn = addOn;
	}

	public String getHotelLandmark() {
		return hotelLandmark;
	}

	public void setHotelLandmark(String hotelLandmark) {
		this.hotelLandmark = hotelLandmark;
	}

	public String getHotelImages() {
		return hotelImages;
	}

	public void setHotelImages(String hotelImages) {
		this.hotelImages = hotelImages;
	}

	public String getHotelStatus() {
		return hotelStatus;
	}

	public void setHotelStatus(String hotelStatus) {
		this.hotelStatus = hotelStatus;
	}

	public PmsProperty getPmsProperty() {
		return pmsProperty;
	}

	public void setPmsProperty(PmsProperty pmsProperty) {
		this.pmsProperty = pmsProperty;
	}

		
	
}