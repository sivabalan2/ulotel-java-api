package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;
import java.util.Date;


/**
 * The persistent class for the booking_details database table.
 * 
 */

public class BookedDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer accommodationId;
	
	private Double amount;
	
	private Long roomCount;

	private String accommodationType;
	
	private Double tax;
	
    private Integer adultCount;

	private Integer childCount;
	
	private Double otaCommission;
	
	private Double otaTax;
	
	public BookedDetail(){
		
	}

	public BookedDetail(String accommodationType, Double tax, Long roomCount, Integer adultCount,Integer childCount, Double amount ){
		
		this.accommodationType = accommodationType;
		this.adultCount = adultCount;
		this.childCount = childCount;
		this.roomCount = roomCount;
		this.amount = amount;
		this.tax = tax;
		
	}
	
	public Integer getAccommodationId() {
		return this.accommodationId;
	}

	public void setAccommodationId(Integer accommodationId) {
		this.accommodationId = accommodationId;
	}
	
	public Double getAmount() {
		return this.amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}
	
	public Long getRoomCount() {
		return roomCount;
	}

	public void setRoomCount(Long roomCount) {
		this.roomCount = roomCount;
	}

	public String getAccommodationType() {
		return accommodationType;
	}

	public void setAccommodationType(String accommodationType) {
		this.accommodationType = accommodationType;
	}

	public Double getTax() {
		return tax;
	}

	public void setTax(Double tax) {
		
      this.tax = tax;
    
      }

    public Integer getAdultCount() {
		return adultCount;
	}
	public void setAdultCount(Integer adultCount) {
		this.adultCount = adultCount;
	}
	public Integer getChildCount() {
		return childCount;
	}
	public void setChildCount(Integer childCount) {
		this.childCount = childCount;
	}
	
	public Double getOtaCommission() {
		return otaCommission;
	}

	public void setOtaCommission(Double otaCommission) {
		this.otaCommission = otaCommission;
	}

	public Double getOtaTax() {
		return otaTax;
	}

	public void setOtaTax(Double otaTax) {
		this.otaTax = otaTax;
	}

}