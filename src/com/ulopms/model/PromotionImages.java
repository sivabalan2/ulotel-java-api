package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;


/**
 * The persistent class for the property_photo database table.
 * 
 */
@Entity
@Table(name="promotion_images")
@NamedQuery(name="PromotionImages.findAll", query="SELECT p FROM PromotionImages p")
public class PromotionImages implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="id",sequenceName="promotion_images_promotion_image_id_seq")
	@GeneratedValue(generator="id")
	@Column(name="promotion_image_id", unique=true, nullable=false)
	private Integer promotionImageId;

	@Column(name="created_by")
	private Integer createdBy;

	@Column(name="created_date")
	private Timestamp createdDate;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_deleted")
	private Boolean isDeleted;
	
	@Column(name="modified_by")
	private Integer modifiedBy;

	@Column(name="modified_date")
	private Timestamp modifiedDate;

	@Column(name="promotion_image_path", length=250)
	private String promotionImagePath;
	
	@Column(name="promotion_image_url", length=500)
	private String promotionImageUrl;

	

	public PromotionImages() {
	}

	public Integer getPromotionImageId() {
		return promotionImageId;
	}

	public void setPromotionImageId(Integer promotionImageId) {
		this.promotionImageId = promotionImageId;
	}
	
	public String getPromotionImagePath() {
		return promotionImagePath;
	}

	public void setPromotionImagePath(String promotionImagePath) {
		this.promotionImagePath = promotionImagePath;
	}
	

	public String getPromotionImageUrl() {
		return promotionImageUrl;
	}

	public void setPromotionImageUrl(String promotionImageUrl) {
		this.promotionImageUrl = promotionImageUrl;
	}

	public Integer getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}


	public Integer getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Timestamp getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}	


}