package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the property_taxes database table.
 * 
 */
@Entity
@Table(name="property_taxes")
@NamedQuery(name="PropertyTaxe.findAll", query="SELECT p FROM PropertyTaxe p")
public class PropertyTaxe implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="id",sequenceName="property_taxes_property_tax_id_seq")
	@GeneratedValue(generator="id")
	@Column(name="property_tax_id", unique=true, nullable=false)
	private Integer propertyTaxId;

	@Column(name="created_by")
	private Integer createdBy;

	@Column(name="created_date")
	private Timestamp createdDate;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_deleted")
	private Boolean isDeleted;

	@Column(name="is_inclusive_rate")
	private Boolean isInclusiveRate;

	@Column(name="modified_by")
	private Integer modifiedBy;

	@Column(name="modified_date")
	private Timestamp modifiedDate;
	
	@Column(name="tax_amount_from")
	private double taxAmountFrom;
	
	@Column(name="tax_amount_to")
	private double taxAmountTo;
	
	@Column(name="tax_percentage")
	private double taxPercentage;

	@Column(name="tax_code", length=10)
	private String taxCode;

	@Column(name="tax_name", length=50)
	private String taxName;

	@Column(name="tax_type", length=10)
	private String taxType;

	//bi-directional many-to-one association to PropertyItem
	@OneToMany(mappedBy="propertyTaxe")
	private List<PropertyItem> propertyItems;

	//bi-directional many-to-one association to PmsProperty
	
	/*@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="property_id")
	private PmsProperty pmsProperty;
	
	//bi-directional many-to-one association to PropertyAccommodation
	@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="accommodation_id")
    private PropertyAccommodation propertyAccommodation;
    */
	
	public PropertyTaxe() {
	}

	public Integer getPropertyTaxId() {
		return this.propertyTaxId;
	}

	public void setPropertyTaxId(Integer propertyTaxId) {
		this.propertyTaxId = propertyTaxId;
	}

	public Integer getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Boolean getIsInclusiveRate() {
		return this.isInclusiveRate;
	}

	public void setIsInclusiveRate(Boolean isInclusiveRate) {
		this.isInclusiveRate = isInclusiveRate;
	}

	public Integer getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Timestamp getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	
	

	public double getTaxAmountFrom() {
		return this.taxAmountFrom;
	}

	public double getTaxAmountTo() {
		return taxAmountTo;
	}

	public void setTaxAmountTo(double taxAmountTo) {
		this.taxAmountTo = taxAmountTo;
	}

	public double getTaxPercentage() {
		return taxPercentage;
	}

	public void setTaxPercentage(double taxPercentage) {
		this.taxPercentage = taxPercentage;
	}

	public void setTaxAmountFrom(double taxAmountFrom) {
		this.taxAmountFrom = taxAmountFrom;
	}

	public String getTaxCode() {
		return this.taxCode;
	}

	public void setTaxCode(String taxCode) {
		this.taxCode = taxCode;
	}

	public String getTaxName() {
		return this.taxName;
	}

	public void setTaxName(String taxName) {
		this.taxName = taxName;
	}

	public String getTaxType() {
		return this.taxType;
	}

	public void setTaxType(String taxType) {
		this.taxType = taxType;
	}

	public List<PropertyItem> getPropertyItems() {
		return this.propertyItems;
	}

	public void setPropertyItems(List<PropertyItem> propertyItems) {
		this.propertyItems = propertyItems;
	}

	public PropertyItem addPropertyItem(PropertyItem propertyItem) {
		getPropertyItems().add(propertyItem);
		propertyItem.setPropertyTaxe(this);

		return propertyItem;
	}

	public PropertyItem removePropertyItem(PropertyItem propertyItem) {
		getPropertyItems().remove(propertyItem);
		propertyItem.setPropertyTaxe(null);

		return propertyItem;
	}

  /*	public PmsProperty getPmsProperty() {
		return this.pmsProperty;
	}

	public void setPmsProperty(PmsProperty pmsProperty) {
		this.pmsProperty = pmsProperty;
	}
	
	public PropertyAccommodation getPropertyAccommodation() {
		return this.propertyAccommodation;
	}

	public void setPropertyAccommodation(PropertyAccommodation propertyAccommodation) {
		this.propertyAccommodation = propertyAccommodation;
	}
	
	*/

}