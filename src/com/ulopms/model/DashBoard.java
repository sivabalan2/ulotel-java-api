package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;
import java.util.List;


public class DashBoard implements Serializable {
	private static final long serialVersionUID = 1L;
	
	
private long totals;

private Integer accommodationId;

private String accommodationType;

private long todayArrival;

private long todayDeparture;

private long todayOccupancy;

private long totalRooms;

private double todayRevenue;

private double totalRevenue;

private double totalTax;

private double otaCommission;

private double otaTax;

private Long roomCount;

private long discountCount;

private String blockDate;
private String inventory;
private int available;
private int blockedRoom;

public String getBlockDate() {
	return blockDate;
}

public void setBlockDate(String blockDate) {
	this.blockDate = blockDate;
}

public String getInventory() {
	return inventory;
}

public void setInventory(String inventory) {
	this.inventory = inventory;
}

public int getAvailable() {
	return available;
}

public void setAvailable(int available) {
	this.available = available;
}

public int getBlockedRoom() {
	return blockedRoom;
}

public void setBlockedRoom(int blockedRoom) {
	this.blockedRoom = blockedRoom;
}

public long getDiscountCount() {
	return discountCount;
}

public void setDiscountCount(long discountCount) {
	this.discountCount = discountCount;
}

public DashBoard(String accomType,String inventory,String date,int total,int blockedRoom){
	
	this.blockDate = date;
	this.inventory = inventory;
	this.blockedRoom = blockedRoom;
	this.accommodationType = accomType;
	this.available = total;
	
}
	public DashBoard(){
		
	}
	public long getTotals() {
		return this.totals;
	}

	public void setTotals(long totals) {
		this.totals = totals;  
	}      
	
	
	public Integer getAccommodationId() {
		return this.accommodationId;
	}

	public void setAccommodationId(Integer accommodationId) {
		this.accommodationId = accommodationId;
	}
	
	public String getAccommodationType() {
		return this.accommodationType;
	}

	public void setAccommodationType(String accommodationType) {
		this.accommodationType = accommodationType;
	}
	
	public long getTodayArrival() {
		return this.todayArrival;
	}

	public void setTodayArrival(long todayArrival) {
		this.todayArrival = todayArrival;
	}
	
	
	public long getTodayDeparture() {
		return this.todayDeparture;
	}

	public void setTodayDeparture(long todayDeparture) {
		this.todayDeparture = todayDeparture;
	}
	
	public long getTodayOccupancy() {
		return this.todayOccupancy;
	}

	public void setTodayOccupancy(long todayOccupancy) {
		this.todayOccupancy = todayOccupancy;
	}

	public long getTotalRooms() {
		return this.totalRooms;
	}

	public void setTotalRooms(long totalRooms) {
		this.totalRooms = totalRooms;
	}
	
	public double getTotalRevenue() {
		return this.totalRevenue;
	}

	public void setTotalRevenue(double totalRevenue) {
		this.totalRevenue = totalRevenue;
	}
	
	public double getTodayRevenue() {
		return todayRevenue;
	}

	public void setTodayRevenue(double todayRevenue) {
		this.todayRevenue = todayRevenue;
	}

	public Long getRoomCount() {
		return this.roomCount;
	}

	public void setRoomCount(Long roomCount) {
		this.roomCount = roomCount;
	}
	
	public double getTotalTax() {
		return totalTax;
	}

	public void setTotalTax(double totalTax) {
		this.totalTax = totalTax;
	}

	public double getOtaCommission() {
		return otaCommission;
	}

	public void setOtaCommission(double otaCommission) {
		this.otaCommission = otaCommission;
	}

	public double getOtaTax() {
		return otaTax;
	}

	public void setOtaTax(double otaTax) {
		this.otaTax = otaTax;
	}
	
}