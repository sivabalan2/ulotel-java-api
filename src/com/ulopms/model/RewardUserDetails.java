package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the reward_user_details database table.
 * 
 */
@Entity
@Table(name="reward_user_details")
@NamedQuery(name="RewardUserDetails.findAll", query="SELECT p FROM RewardUserDetails p")
public class RewardUserDetails implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="id",sequenceName="reward_user_details_reward_user_detail_id_seq")
	@GeneratedValue(generator="id")
	@Column(name="reward_user_detail_id")
	private Integer rewardUserDetailId;

	@Column(name="created_by")
	private Integer createdBy;

	@Column(name="created_date")
	private Timestamp createdDate;
	
	@Column(name="modified_by")
	private Integer modifiedBy;

	@Column(name="modified_date")
	private Timestamp modifiedDate;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_deleted")
	private Boolean isDeleted;	
	
	@Column(name="used_reward_points")
	private Integer usedRewardPoints;	


	//bi-directional many-to-one association to PmsStatus
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="reward_detail_id")
	private RewardDetails rewardDetails;

	/*//bi-directional many-to-one association to PmsStatus
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="booking_id")
	private PmsBooking pmsBooking;*/
		
	//bi-directional many-to-one association to PmsStatus
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="user_id")
	private User user;
	

	public RewardUserDetails() { 
	}

	public Integer getRewardUserDetailId() {
		return rewardUserDetailId;
	}

	public void setRewardUserDetailId(Integer rewardUserDetailId) {
		this.rewardUserDetailId = rewardUserDetailId;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedDate() {
		return createdDate;
	}
	
	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public Integer getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Timestamp getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Integer getUsedRewardPoints() {
		return usedRewardPoints;
	}

	public void setUsedRewardPoints(Integer usedRewardPoints) {
		this.usedRewardPoints = usedRewardPoints;
	}

	public RewardDetails getRewardDetails() {
		return rewardDetails;
	}

	public void setRewardDetails(RewardDetails rewardDetails) {
		this.rewardDetails = rewardDetails;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	
}