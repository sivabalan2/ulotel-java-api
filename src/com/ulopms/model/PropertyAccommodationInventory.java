package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the property_accommodation_inventory database table.
 * 
 */
@Entity
@Table(name="property_accommodation_inventory")
@NamedQuery(name="PropertyAccommodationInventory.findAll", query="SELECT p FROM PropertyAccommodationInventory p")
public class PropertyAccommodationInventory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="id",sequenceName="property_accommodation_inventory_inventory_id_seq")
	@GeneratedValue(generator="id")
	@Column(name="inventory_id")
	private Integer inventoryId;

	@Column(name="created_by")
	private Integer createdBy;

	@Column(name="created_date")
	private Timestamp createdDate;

	@Column(name="blocked_date")
	private Date blockedDate;

	@Column(name="is_active")
	private Boolean isActive;
	
	@Column(name="is_deleted")
	private Boolean isDeleted;

	@Column(name="modified_by")
	private Integer modifiedBy;

	@Column(name="modified_date")
	private Timestamp modifiedDate;	

	@Column(name="inventory_count")
	private String inventoryCount;
	
	@Column(name="inventory_remarks")
	private String inventoryRemarks;
	
	/*@Column(name="blocked_time")
	private Time blockedTime;*/	
	
	//bi-directional many-to-one association to BookingDetails
		@OneToMany(mappedBy="propertyAccommodationInventory")
		private List<BookingDetail> bookingDetail;	

	

	//bi-directional many-to-one association to PropertyAccommodation
	@ManyToOne
	@JoinColumn(name="accommodation_id")
	private PropertyAccommodation propertyAccommodation;

	

	public PropertyAccommodationInventory() {
	}
	

	public Integer getInventoryId() {
		return inventoryId;
	}

	public void setInventoryId(Integer inventoryId) {
		this.inventoryId = inventoryId;
	}

	public Date getBlockedDate() {
		return blockedDate;
	}

	public void setBlockedDate(Date blockedDate) {
		this.blockedDate = blockedDate;
	}

	public String getInventoryCount() {
		return inventoryCount;
	}

	public void setInventoryCount(String inventoryCount) {
		this.inventoryCount = inventoryCount;
	}

	public Integer getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}	

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	

	public Integer getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Timestamp getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	

	public PropertyAccommodation getPropertyAccommodation() {
		return this.propertyAccommodation;
	}

	public void setPropertyAccommodation(PropertyAccommodation propertyAccommodation) {
		this.propertyAccommodation = propertyAccommodation;
	}
	
	/*public Time getBlockedTime() {
		return blockedTime;
	}

	public void setBlockedTime(Time blockedTime) {
		this.blockedTime = blockedTime;
	}*/
	
	public List<BookingDetail> getBookingDetail() {
		return bookingDetail;
	}
	
	public void setBookingDetail(List<BookingDetail> bookingDetail) {
		this.bookingDetail = bookingDetail;
	}


	public String getInventoryRemarks() {
		return inventoryRemarks;
	}


	public void setInventoryRemarks(String inventoryRemarks) {
		this.inventoryRemarks = inventoryRemarks;
	}
	
	
	

}
