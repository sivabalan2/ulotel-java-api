package com.ulopms.model;

import java.io.Serializable;

import java.sql.Timestamp;
import java.util.Date;


public class OtaBookingDetails implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Integer bookingId;
	
	private long hotelCode;
	
	private String customerName;
	
	private long noOfRooms;
	
	private long noofNights;
	
	private String roomTypeName;
	
	private Date checkInDate;
	
	private String status;
	
	private boolean payAtHotelFlag;
	
	private Timestamp bookingTime;
	
	private String bookingVendorName;
	
	private long cancellationId;
	
	private Date cancellationDate;
	
	private Double bookingAmount;
	
	private Double cancellationCharges;
	
	private Double refundAmount;
	
	private String cancellationStatus;
	
	public OtaBookingDetails(){
		
	}
	public long getCancellationId() {
		return cancellationId;
	}

	public void setCancellationId(long cancellationId) {
		this.cancellationId = cancellationId;
	}

	public Date getCancellationDate() {
		return cancellationDate;
	}

	public void setCancellationDate(Date cancellationDate) {
		this.cancellationDate = cancellationDate;
	}

	public Double getBookingAmount() {
		return bookingAmount;
	}

	public void setBookingAmount(Double bookingAmount) {
		this.bookingAmount = bookingAmount;
	}

	public Double getCancellationCharges() {
		return cancellationCharges;
	}

	public void setCancellationCharges(Double cancellationCharges) {
		this.cancellationCharges = cancellationCharges;
	}

	public Double getRefundAmount() {
		return refundAmount;
	}

	public void setRefundAmount(Double refundAmount) {
		this.refundAmount = refundAmount;
	}

	public String getCancellationStatus() {
		return cancellationStatus;
	}

	public void setCancellationStatus(String cancellationStatus) {
		this.cancellationStatus = cancellationStatus;
	}

	public Integer getBookingId() {
		return bookingId;
	}

	public void setBookingId(Integer bookingId) {
		this.bookingId = bookingId;
	}

	public long getHotelCode() {
		return hotelCode;
	}

	public void setHotelCode(long hotelCode) {
		this.hotelCode = hotelCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public long getNoOfRooms() {
		return noOfRooms;
	}

	public void setNoOfRooms(long noOfRooms) {
		this.noOfRooms = noOfRooms;
	}

	public long getNoofNights() {
		return noofNights;
	}

	public void setNoofNights(long noofNights) {
		this.noofNights = noofNights;
	}

	public String getRoomTypeName() {
		return roomTypeName;
	}

	public void setRoomTypeName(String roomTypeName) {
		this.roomTypeName = roomTypeName;
	}

	public Date getCheckInDate() {
		return checkInDate;
	}

	public void setCheckInDate(Date checkInDate) {
		this.checkInDate = checkInDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public boolean isPayAtHotelFlag() {
		return payAtHotelFlag;
	}

	public void setPayAtHotelFlag(boolean payAtHotelFlag) {
		this.payAtHotelFlag = payAtHotelFlag;
	}

	public Timestamp getBookingTime() {
		return bookingTime;
	}

	public void setBookingTime(Timestamp bookingTime) {
		this.bookingTime = bookingTime;
	}

	public String getBookingVendorName() {
		return bookingVendorName;
	}

	public void setBookingVendorName(String bookingVendorName) {
		this.bookingVendorName = bookingVendorName;
	}

	
	
	
}