package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;


/**
 * The persistent class for the property_payment_details database table.
 * 
 */
@Entity
@Table(name="property_payment_details")
@NamedQuery(name="PropertyPaymentDetails.findAll", query="SELECT p FROM PropertyPaymentDetails p")
public class PropertyPaymentDetails implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="id",sequenceName="property_payment_details_property_payment_id_seq")
	@GeneratedValue(generator="id")
	@Column(name="property_payment_id", unique=true, nullable=false)
	private Integer propertyPaymentId;

	@Column(name="created_by")
	private Integer createdBy;

	@Column(name="created_date")
	private Timestamp createdDate;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_deleted")
	private Boolean isDeleted;

	@Column(name="modified_by")
	private Integer modifiedBy;

	@Column(name="modified_date")
	private Timestamp modifiedDate;
	
	@Column(name="from_date")
	private Timestamp fromDate;
	
	@Column(name="to_date")
	private Timestamp toDate;
	
	@Column(name="payment_booking_id")
	private String paymentBookingId;
	
	@Column(name="payment_amount")
	private Double paymentAmount;
	
	@Column(name="upload_file_name")
	private String uploadFileName;

	//bi-directional many-to-one association to PmsProperty
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="property_id")
	private PmsProperty pmsProperty;
	
	
	public PropertyPaymentDetails() {
	}

	public Integer getPropertyPaymentId() {
		return propertyPaymentId;
	}

	public void setPropertyPaymentId(Integer propertyPaymentId) {
		this.propertyPaymentId = propertyPaymentId;
	}

	public Integer getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Integer getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Timestamp getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public PmsProperty getPmsProperty() {
		return this.pmsProperty;
	}

	public void setPmsProperty(PmsProperty pmsProperty) {
		this.pmsProperty = pmsProperty;
	}

	public Timestamp getFromDate() {
		return fromDate;
	}

	public void setFromDate(Timestamp fromDate) {
		this.fromDate = fromDate;
	}

	public Timestamp getToDate() {
		return toDate;
	}

	public void setToDate(Timestamp toDate) {
		this.toDate = toDate;
	}

	public String getPaymentBookingId() {
		return paymentBookingId;
	}

	public void setPaymentBookingId(String paymentBookingId) {
		this.paymentBookingId = paymentBookingId;
	}

	public Double getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(Double paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public String getUploadFileName() {
		return uploadFileName;
	}

	public void setUploadFileName(String uploadFileName) {
		this.uploadFileName = uploadFileName;
	}
	


}