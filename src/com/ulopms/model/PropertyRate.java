package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the property_rate database table.
 * 
 */
@Entity
@Table(name="property_rate")
@NamedQuery(name="PropertyRate.findAll", query="SELECT p FROM PropertyRate p")
public class PropertyRate implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="id",sequenceName="property_rate_property_rate_id_seq")
	@GeneratedValue(generator="id")
	@Column(name="property_rate_id")
	private Integer propertyRateId;

	@Column(name="created_by")
	private Integer createdBy;

	@Column(name="created_date")
	private Timestamp createdDate;

	@Column(name="end_date")
	private Timestamp endDate;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_deleted")
	private Boolean isDeleted;

	@Column(name="modified_by")
	private Integer modifiedBy;

	@Column(name="modified_date")
	private Timestamp modifiedDate;

	@Column(name="order_by")
	private Integer orderBy;

	@Column(name="rate_name")
	private String rateName;

	@Column(name="start_date")
	private Timestamp startDate;
	
	@Column(name="smart_price_is_active")
	private Boolean smartPriceIsActive;
	
	//bi-directional many-to-one association to PmsSourceType
	@ManyToOne
	@JoinColumn(name="source_type_id")
	private PmsSourceType pmsSourceType;
	
	//bi-directional many-to-one association to PmsProperty
	@ManyToOne
	@JoinColumn(name="property_id")
	private PmsProperty pmsProperty;

	//bi-directional many-to-one association to PmsSource
	@ManyToOne
	@JoinColumn(name="source_id")
	private PmsSource pmsSource;

	//bi-directional many-to-one association to PropertyAccommodation
	@ManyToOne
	@JoinColumn(name="accommodation_id")
	private PropertyAccommodation propertyAccommodation;

	//bi-directional many-to-one association to PropertyRateDetail
	@OneToMany(mappedBy="propertyRate")
	private List<PropertyRateDetail> propertyRateDetails;

	public PropertyRate() {
	}

	public Integer getPropertyRateId() {
		return this.propertyRateId;
	}

	public void setPropertyRateId(Integer propertyRateId) {
		this.propertyRateId = propertyRateId;
	}

	public Integer getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public Timestamp getEndDate() {
		return this.endDate;
	}

	public void setEndDate(Timestamp endDate) {
		this.endDate = endDate;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	public Boolean getSmartPriceIsActive() {
		return this.smartPriceIsActive;
	}

	public void setSmartPriceIsActive(Boolean smartPriceIsActive) {
		this.smartPriceIsActive = smartPriceIsActive;
	}

	public Integer getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Timestamp getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public Integer getOrderBy() {
		return this.orderBy;
	}

	public void setOrderBy(Integer orderBy) {
		this.orderBy = orderBy;
	}

	public String getRateName() {
		return this.rateName;
	}

	public void setRateName(String rateName) {
		this.rateName = rateName;
	}

	public Timestamp getStartDate() {
		return this.startDate;
	}

	public void setStartDate(Timestamp startDate) {
		this.startDate = startDate;
	}

	public PmsProperty getPmsProperty() {
		return this.pmsProperty;
	}

	public void setPmsProperty(PmsProperty pmsProperty) {
		this.pmsProperty = pmsProperty;
	}

	public PmsSource getPmsSource() {
		return this.pmsSource;
	}

	public void setPmsSource(PmsSource pmsSource) {
		this.pmsSource = pmsSource;
	}

	public PropertyAccommodation getPropertyAccommodation() {
		return this.propertyAccommodation;
	}

	public void setPropertyAccommodation(PropertyAccommodation propertyAccommodation) {
		this.propertyAccommodation = propertyAccommodation;
	}

	/*public PropertyRateDetail getPropertyRatesDetails(){
		return this.propertyRatesDetails;
	}
	
	public void setPropertyRatesDetails(PropertyRateDetail propertyRatesDetails)
	{
		this.propertyRatesDetails=propertyRatesDetails;
	}*/
	
	public List<PropertyRateDetail> getPropertyRateDetails() {
		return this.propertyRateDetails;
	}

	public void setPropertyRateDetails(List<PropertyRateDetail> propertyRateDetails) {
		this.propertyRateDetails = propertyRateDetails;
	}

	public PmsSourceType getPmsSourceType() {
		return pmsSourceType;
	}

	public void setPmsSourceType(PmsSourceType pmsSourceType) {
		this.pmsSourceType = pmsSourceType;
	}
	
	public PropertyRateDetail addPropertyRateDetail(PropertyRateDetail propertyRateDetail) {
		getPropertyRateDetails().add(propertyRateDetail);
		propertyRateDetail.setPropertyRate(this);

		return propertyRateDetail;
	}

	public PropertyRateDetail removePropertyRateDetail(PropertyRateDetail propertyRateDetail) {
		getPropertyRateDetails().remove(propertyRateDetail);
		propertyRateDetail.setPropertyRate(null);

		return propertyRateDetail;
	}

}