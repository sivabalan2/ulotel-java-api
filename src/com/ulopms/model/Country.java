package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import java.util.List;


/**
 * The persistent class for the country database table.
 * 
 */
@Entity
@Table(name="country")
@NamedQuery(name="Country.findAll", query="SELECT c FROM Country c")
public class Country implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="country_code", unique=true, nullable=false, length=10)
	private String countryCode;

	@Column(length=100)
	private String country;

	//bi-directional many-to-one association to State
	@OneToMany(mappedBy="country")
	private List<State> states;
	
	//bi-directional many-to-one association to PmsProperty
			@OneToMany(mappedBy="country")
			private List<PmsProperty> pmsProperties;

	public Country() {
	}

	public String getCountryCode() {
		return this.countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public List<State> getStates() {
		return this.states;
	}

	public void setStates(List<State> states) {
		this.states = states;
	}

	public State addState(State state) {
		getStates().add(state);
		state.setCountry(this);

		return state;
	}

	public State removeState(State state) {
		getStates().remove(state);
		state.setCountry(null);

		return state;
	}
	
	public List<PmsProperty> getPmsProperties() {
		return pmsProperties;
	}

	public void setPmsProperties(List<PmsProperty> pmsProperties) {
		this.pmsProperties = pmsProperties;
	}

}