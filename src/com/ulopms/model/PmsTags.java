package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the property_taxes database table.
 * 
 */
@Entity
@Table(name="pms_tags")
@NamedQuery(name="PmsTags.findAll", query="SELECT p FROM PmsTags p")
public class PmsTags implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="id",sequenceName="pms_tags_tag_id_seq")
	@GeneratedValue(generator="id")
	@Column(name="tag_id", unique=true, nullable=false)
	private Integer tagId;
	
	@Column(name="tag_name")
	private String tagName;

	@Column(name="created_by")
	private Integer createdBy;

	@Column(name="created_date")
	private Timestamp createdDate;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_deleted")
	private Boolean isDeleted;

	@Column(name="modified_by")
	private Integer modifiedBy;

	@Column(name="modified_date")
	private Timestamp modifiedDate;

	@Column(name="tag_description")
	private String tagDescription;

	@OneToMany(mappedBy="pmsTags")
	private List<PropertyTags> propertyTags;

	
	public PmsTags() {
	}

	public Integer getTagId() {
		return tagId;
	}


	public void setTagId(Integer tagId) {
		this.tagId = tagId;
	}


	public String getTagName() {
		return tagName;
	}


	public void setTagName(String tagName) {
		this.tagName = tagName;
	}


	public Integer getCreatedBy() {
		return createdBy;
	}


	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}


	public Timestamp getCreatedDate() {
		return createdDate;
	}


	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}


	public Boolean getIsActive() {
		return isActive;
	}


	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}


	public Boolean getIsDeleted() {
		return isDeleted;
	}


	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}


	public Integer getModifiedBy() {
		return modifiedBy;
	}


	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}


	public Timestamp getModifiedDate() {
		return modifiedDate;
	}


	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}


	public String getTagDescription() {
		return tagDescription;
	}


	public void setTagDescription(String tagDescription) {
		this.tagDescription = tagDescription;
	}

	public List<PropertyTags> getPropertyTags() {
		return propertyTags;
	}

	public void setPropertyTags(List<PropertyTags> propertyTags) {
		this.propertyTags = propertyTags;
	}
	

}