package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

/**
 * The persistent class for the property_accommodation database table.
 * 
 */
@Entity
@Table(name = "property_accommodation")
@NamedQuery(name = "PropertyAccommodation.findAll", query = "SELECT p FROM PropertyAccommodation p")
public class PropertyAccommodation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "accommodation_id", unique = true, nullable = false)
	private Integer accommodationId;

	@Column(length = 5)
	private String abbreviation;

	@Column(name = "accommodation_description", length = 1000)
	private String accommodationDescription;

	@Column(name = "accommodation_type", length = 10)
	private String accommodationType;

	@Column(name = "no_of_adults")
	private Integer noOfAdults;

	@Column(name = "base_amount", precision = 131089)
	private Double baseAmount;

	@Column(name = "no_of_child")
	private Integer noOfChild;

	@Column(name = "no_of_infant")
	private Integer noOfInfant;

	@Column(name = "created_by")
	private Integer createdBy;

	@Column(name = "created_date")
	private Timestamp createdDate;

	@Column(name = "extra_adult", precision = 131089)
	private Double extraAdult;

	@Column(name = "extra_child", precision = 131089)
	private Double extraChild;

	@Column(name = "extra_infant", precision = 131089)
	private Double extraInfant;

	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "is_deleted")
	private Boolean isDeleted;

	@Column(name = "min_occupancy")
	private Integer minOccupancy;

	@Column(name = "max_occupancy")
	private Integer maxOccupancy;

	@Column(name = "modified_by")
	private Integer modifiedBy;

	@Column(name = "modified_date")
	private Timestamp modifiedDate;

	@Column(name = "no_of_units")
	private Integer noOfUnits;

	@Column(name = "accommodation_path", length = 250)
	private String accommodationImgPath;

	@Column(name = "net_rate_revenue")
	private Double netRateRevenue;

	@Column(name = "minimum_base_amount")
	private Double minimumBaseAmount;

	@Column(name = "maximum_base_amount")
	private Double maximumBaseAmount;
	
	@Column(name = "net_rate")
	private Double netRate;
	
	@Column(name = "tax_type")
	private Integer taxType;
	
	@Column(name = "tariff")
	private Double tariff;
	
	@Column(name = "tax")
	private Double tax;
	
	@Column(name = "selling_rate")
	private Double sellingRate;
	
	@Column(name = "purchase_rate")
	private Double purchaseRate;
	
	@Column(name = "weekday_purchase_rate")
	private Double weekdayPurchaseRate;
	
	@Column(name = "weekend_purchase_rate")
	private Double weekendPurchaseRate;
	
	@Column(name = "weekday_net_rate")
	private Double weekdayNetRate;
	
	@Column(name = "weekend_net_rate")
	private Double weekendNetRate;
	
	@Column(name = "weekday_base_amount")
	private Double weekdayBaseAmount;
	
	@Column(name = "weekend_base_amount")
	private Double weekendBaseAmount;
	
	@Column(name = "weekday_tariff")
	private Double weekdayTariff;
	
	@Column(name = "weekend_tariff")
	private Double weekendTariff;
	
	@Column(name = "weekday_tax")
	private Double weekdayTax;
	
	@Column(name = "weekend_tax")
	private Double weekendTax;
	
	@Column(name = "weekday_selling_rate")
	private Double weekdaySellingRate;
	
	@Column(name = "weekend_selling_rate")
	private Double weekendSellingRate;
	
	@Column(name = "accommodation_commission")
	private Double accommodationCommission;

	// bi-directional many-to-one association to PropertyAccommodationRoom
	@OneToMany(mappedBy = "propertyAccommodation")
	private List<PropertyAccommodationRoom> propertyAccommodationRooms;

	// bi-directional many-to-one association to propertyAccommodationInventory
	@OneToMany(mappedBy = "propertyAccommodation")
	private List<PropertyAccommodationInventory> propertyAccommodationInventory;

	// bi-directional many-to-one association to propertyAccommodationInventory
	@OneToMany(mappedBy = "propertyAccommodation")
	private List<OtaHotelDetails> otaHotelDetails;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "property_id")
	private PmsProperty pmsProperty;
	
	//bi-directional many-to-one association to PropertyContractType
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="contract_type_id")
	private PropertyContractType propertyContractType;

	public PropertyAccommodation() {
	}

	public Integer getAccommodationId() {
		return this.accommodationId;
	}

	public void setAccommodationId(Integer accommodationId) {
		this.accommodationId = accommodationId;
	}

	public String getAbbreviation() {
		return this.abbreviation;
	}

	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}

	public String getAccommodationDescription() {
		return this.accommodationDescription;
	}

	public void setAccommodationDescription(String accommodationDescription) {
		this.accommodationDescription = accommodationDescription;
	}

	public String getAccommodationType() {
		return this.accommodationType;
	}

	public void setAccommodationType(String accommodationType) {
		this.accommodationType = accommodationType;
	}

	public Integer getNoOfAdults() {
		return this.noOfAdults;
	}

	public void setNoOfAdults(Integer noOfAdults) {
		this.noOfAdults = noOfAdults;
	}

	public Double getBaseAmount() {
		return this.baseAmount;
	}

	public void setBaseAmount(Double baseAmount) {
		this.baseAmount = baseAmount;
	}

	public Integer getNoOfChild() {
		return this.noOfChild;
	}

	public Integer getNoOfInfant() {
		return this.noOfInfant;
	}

	public void setNoOfInfant(Integer noOfInfant) {
		this.noOfInfant = noOfInfant;
	}

	public void setNoOfChild(Integer noOfChild) {
		this.noOfChild = noOfChild;
	}

	public Integer getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public Double getExtraAdult() {
		return this.extraAdult;
	}

	public void setExtraAdult(Double extraAdult) {
		this.extraAdult = extraAdult;
	}

	public Double getExtraInfant() {
		return this.extraInfant;
	}

	public void setExtraInfant(Double extraInfant) {
		this.extraInfant = extraInfant;
	}

	public Double getExtraChild() {
		return this.extraChild;
	}

	public void setExtraChild(Double extraChild) {
		this.extraChild = extraChild;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Integer getMinOccupancy() {
		return this.minOccupancy;
	}

	public void setMinOccupancy(Integer minOccupancy) {
		this.minOccupancy = minOccupancy;
	}

	public Integer getMaxOccupancy() {
		return this.maxOccupancy;
	}

	public void setMaxOccupancy(Integer maxOccupancy) {
		this.maxOccupancy = maxOccupancy;
	}

	public Integer getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Timestamp getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public Integer getNoOfUnits() {
		return this.noOfUnits;
	}

	public void setNoOfUnits(Integer noOfUnits) {
		this.noOfUnits = noOfUnits;
	}

	public String getAccommodationImgPath() {
		return this.accommodationImgPath;
	}

	public void setAccommodationImgPath(String accommodationImgPath) {
		this.accommodationImgPath = accommodationImgPath;
	}

	public PmsProperty getPmsProperty() {
		return this.pmsProperty;
	}

	public void setPmsProperty(PmsProperty pmsProperty) {
		this.pmsProperty = pmsProperty;
	}

	public PropertyContractType getPropertyContractType() {
		return propertyContractType;
	}

	public void setPropertyContractType(PropertyContractType propertyContractType) {
		this.propertyContractType = propertyContractType;
	}

	/*
	 * public Integer getPropertyId() { return this.propertyId; }
	 * 
	 * public void setPropertyId(Integer propertyId) { this.propertyId =
	 * propertyId; }
	 */
	public List<PropertyAccommodationRoom> getPropertyAccommodationRooms() {
		return this.propertyAccommodationRooms;
	}

	public void setPropertyAccommodationRooms(
			List<PropertyAccommodationRoom> propertyAccommodationRooms) {
		this.propertyAccommodationRooms = propertyAccommodationRooms;
	}

	public List<PropertyAccommodationInventory> getPropertyAccommodationInventory() {
		return propertyAccommodationInventory;
	}

	public void setPropertyAccommodationInventory(
			List<PropertyAccommodationInventory> propertyAccommodationInventory) {
		this.propertyAccommodationInventory = propertyAccommodationInventory;
	}

	public List<OtaHotelDetails> getOtaHotelDetails() {
		return otaHotelDetails;
	}

	public void setOtaHotelDetails(List<OtaHotelDetails> otaHotelDetails) {
		this.otaHotelDetails = otaHotelDetails;
	}

	public Double getNetRateRevenue() {
		return netRateRevenue;
	}

	public void setNetRateRevenue(Double netRateRevenue) {
		this.netRateRevenue = netRateRevenue;
	}

	public Double getMinimumBaseAmount() {
		return minimumBaseAmount;
	}

	public void setMinimumBaseAmount(Double minimumBaseAmount) {
		this.minimumBaseAmount = minimumBaseAmount;
	}

	public Double getMaximumBaseAmount() {
		return maximumBaseAmount;
	}

	public void setMaximumBaseAmount(Double maximumBaseAmount) {
		this.maximumBaseAmount = maximumBaseAmount;
	}

	public Double getNetRate() {
		return netRate;
	}

	public void setNetRate(Double netRate) {
		this.netRate = netRate;
	}

	public Integer getTaxType() {
		return taxType;
	}

	public void setTaxType(Integer taxType) {
		this.taxType = taxType;
	}

	public Double getTariff() {
		return tariff;
	}

	public void setTariff(Double tariff) {
		this.tariff = tariff;
	}

	public Double getTax() {
		return tax;
	}

	public void setTax(Double tax) {
		this.tax = tax;
	}

	public Double getSellingRate() {
		return sellingRate;
	}

	public void setSellingRate(Double sellingRate) {
		this.sellingRate = sellingRate;
	}

	public Double getPurchaseRate() {
		return purchaseRate;
	}

	public void setPurchaseRate(Double purchaseRate) {
		this.purchaseRate = purchaseRate;
	}

	public Double getWeekdayPurchaseRate() {
		return weekdayPurchaseRate;
	}

	public void setWeekdayPurchaseRate(Double weekdayPurchaseRate) {
		this.weekdayPurchaseRate = weekdayPurchaseRate;
	}

	public Double getWeekendPurchaseRate() {
		return weekendPurchaseRate;
	}

	public void setWeekendPurchaseRate(Double weekendPurchaseRate) {
		this.weekendPurchaseRate = weekendPurchaseRate;
	}

	public Double getWeekdayNetRate() {
		return weekdayNetRate;
	}

	public void setWeekdayNetRate(Double weekdayNetRate) {
		this.weekdayNetRate = weekdayNetRate;
	}

	public Double getWeekendNetRate() {
		return weekendNetRate;
	}

	public void setWeekendNetRate(Double weekendNetRate) {
		this.weekendNetRate = weekendNetRate;
	}

	public Double getWeekdayBaseAmount() {
		return weekdayBaseAmount;
	}

	public void setWeekdayBaseAmount(Double weekdayBaseAmount) {
		this.weekdayBaseAmount = weekdayBaseAmount;
	}

	public Double getWeekendBaseAmount() {
		return weekendBaseAmount;
	}

	public void setWeekendBaseAmount(Double weekendBaseAmount) {
		this.weekendBaseAmount = weekendBaseAmount;
	}

	public Double getWeekdayTariff() {
		return weekdayTariff;
	}

	public void setWeekdayTariff(Double weekdayTariff) {
		this.weekdayTariff = weekdayTariff;
	}

	public Double getWeekendTariff() {
		return weekendTariff;
	}

	public void setWeekendTariff(Double weekendTariff) {
		this.weekendTariff = weekendTariff;
	}

	public Double getWeekdayTax() {
		return weekdayTax;
	}

	public void setWeekdayTax(Double weekdayTax) {
		this.weekdayTax = weekdayTax;
	}

	public Double getWeekendTax() {
		return weekendTax;
	}

	public void setWeekendTax(Double weekendTax) {
		this.weekendTax = weekendTax;
	}

	public Double getWeekdaySellingRate() {
		return weekdaySellingRate;
	}

	public void setWeekdaySellingRate(Double weekdaySellingRate) {
		this.weekdaySellingRate = weekdaySellingRate;
	}

	public Double getWeekendSellingRate() {
		return weekendSellingRate;
	}

	public void setWeekendSellingRate(Double weekendSellingRate) {
		this.weekendSellingRate = weekendSellingRate;
	}

	public Double getAccommodationCommission() {
		return accommodationCommission;
	}

	public void setAccommodationCommission(Double accommodationCommission) {
		this.accommodationCommission = accommodationCommission;
	}

	public PropertyAccommodationRoom addPropertyAccommodationRoom(
			PropertyAccommodationRoom propertyAccommodationRoom) {
		getPropertyAccommodationRooms().add(propertyAccommodationRoom);
		propertyAccommodationRoom.setPropertyAccommodation(this);

		return propertyAccommodationRoom;
	}

	public PropertyAccommodationRoom removePropertyAccommodationRoom(
			PropertyAccommodationRoom propertyAccommodationRoom) {
		getPropertyAccommodationRooms().remove(propertyAccommodationRoom);
		propertyAccommodationRoom.setPropertyAccommodation(null);

		return propertyAccommodationRoom;
	}

}