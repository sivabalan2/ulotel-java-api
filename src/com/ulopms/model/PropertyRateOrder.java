package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the pms_booking database table.
 * 
 */

public class PropertyRateOrder implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer rateId;
	
	private String rateName;
	
	private Integer accommodationId;
    
	private String accommodationType;
	
	private Boolean isActive;

	private Boolean isDeleted;


	

	//
	
	
	
	public Integer getAccommodationId() {
		return this.accommodationId;
	}

	public void setAccommodationId(Integer accommodationId) {
		this.accommodationId = accommodationId;
	}
	
	public String getAccommodationType() {
			return this.accommodationType;
	}

	public void setAccommodationType(String accommodationType) {
			this.accommodationType = accommodationType;
	}
	
    public Integer getRateId() {
		return rateId;
	}

	public void setRateId(Integer rateId) {
		this.rateId = rateId;
	}

	public String getRateName() {
		return rateName;
	}

	public void setRateName(String rateName) {
		this.rateName = rateName;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}


}