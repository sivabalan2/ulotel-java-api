package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the property_taxes database table.
 * 
 */
@Entity
@Table(name="property_tags")
@NamedQuery(name="PropertyTags.findAll", query="SELECT p FROM PropertyTags p")
public class PropertyTags implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="id",sequenceName="property_tags_property_tag_id_seq")
	@GeneratedValue(generator="id")
	@Column(name="property_tag_id", unique=true, nullable=false)
	private Integer propertyTagId;
	
	@Column(name="created_by")
	private Integer createdBy;

	@Column(name="created_date")
	private Timestamp createdDate;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_deleted")
	private Boolean isDeleted;

	@Column(name="modified_by")
	private Integer modifiedBy;

	@Column(name="modified_date")
	private Timestamp modifiedDate;

	//bi-directional many-to-one association to PmsProperty
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="property_id")
	private PmsProperty pmsProperty;

	//bi-directional many-to-one association to PmsTags
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="tag_id")
	private PmsTags pmsTags;
	
	public PropertyTags() {
	}

	
	public Integer getPropertyTagId() {
		return propertyTagId;
	}


	public void setPropertyTagId(Integer propertyTagId) {
		this.propertyTagId = propertyTagId;
	}


	public Integer getCreatedBy() {
		return createdBy;
	}


	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}


	public Timestamp getCreatedDate() {
		return createdDate;
	}


	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}


	public Boolean getIsActive() {
		return isActive;
	}


	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}


	public Boolean getIsDeleted() {
		return isDeleted;
	}


	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}


	public Integer getModifiedBy() {
		return modifiedBy;
	}


	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}


	public Timestamp getModifiedDate() {
		return modifiedDate;
	}


	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public PmsProperty getPmsProperty() {
		return pmsProperty;
	}

	public void setPmsProperty(PmsProperty pmsProperty) {
		this.pmsProperty = pmsProperty;
	}

	public PmsTags getPmsTags() {
		return pmsTags;
	}

	public void setPmsTags(PmsTags pmsTags) {
		this.pmsTags = pmsTags;
	}

}