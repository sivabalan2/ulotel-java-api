package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the location_type database table.
 * 
 */
@Entity
@Table(name="location_type")
@NamedQuery(name="LocationType.findAll", query="SELECT p FROM LocationType p")
public class LocationType implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="id",sequenceName="location_type_location_type_id_seq")
	@GeneratedValue(generator="id")
	@Column(name="location_type_id", unique=true, nullable=false)
	private Integer locationTypeId;
	
	@Column(name="location_type_name")
	private String locationTypeName;

	@Column(name="created_by")
	private Integer createdBy;

	@Column(name="created_date")
	private Timestamp createdDate;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_deleted")
	private Boolean isDeleted;

	@Column(name="modified_by")
	private Integer modifiedBy;

	@Column(name="modified_date")
	private Timestamp modifiedDate;

	//bi-directional many-to-one association to PmsProperty
	@OneToMany(mappedBy="locationType")
	private List<PmsProperty> pmsProperties;

	public LocationType() {
	}

	public Integer getLocationTypeId() {
		return this.locationTypeId;
	}

	public void setLocationTypeId(Integer locationTypeId) {
		this.locationTypeId = locationTypeId;
	}

	public Integer getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Integer getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Timestamp getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getLocationTypeName() {
		return this.locationTypeName;
	}

	public void setLocationTypeName(String locationTypeName) {
		this.locationTypeName = locationTypeName;
	}

	public List<PmsProperty> getPmsProperties() {
		return this.pmsProperties;
	}

	public void setPmsProperties(List<PmsProperty> pmsProperties) {
		this.pmsProperties = pmsProperties;
	}

	public PmsProperty addPmsProperty(PmsProperty pmsProperty) {
		getPmsProperties().add(pmsProperty);
		pmsProperty.setLocationType(this);

		return pmsProperty;
	}

	public PmsProperty removePmsProperty(PmsProperty pmsProperty) {
		getPmsProperties().remove(pmsProperty);
		pmsProperty.setPropertyType(null);

		return pmsProperty;
	}

}