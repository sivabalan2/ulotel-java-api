package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the reward_details database table.
 * 
 */
@Entity
@Table(name="reward_details")
@NamedQuery(name="RewardDetails.findAll", query="SELECT p FROM RewardDetails p")
public class RewardDetails implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="id",sequenceName="reward_details_reward__detail_id_seq")
	@GeneratedValue(generator="id")
	@Column(name="reward_detail_id")
	private Integer rewardDetailId;

	@Column(name="created_by")
	private Integer createdBy;

	@Column(name="created_date")
	private Timestamp createdDate;
	
	@Column(name="modified_by")
	private Integer modifiedBy;

	@Column(name="modified_date")
	private Timestamp modifiedDate;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_deleted")
	private Boolean isDeleted;	

	@Column(name="reward_detail_name")
	private String rewardDetailName;
	
	@Column(name="reward_points")
	private Integer rewardPoints;
	
	//bi-directional many-to-one association to PropertyRate
	@OneToMany(mappedBy="rewardDetails")
	private List<RewardUserDetails> rewardUserDetails;
	

	//bi-directional many-to-one association to PmsStatus
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="reward_id")
	private PmsRewards pmsRewards;
	

	public RewardDetails() {
	}

	public Integer getRewardDetailId() {
		return rewardDetailId;
	}

	public void setRewardDetailId(Integer rewardDetailId) {
		this.rewardDetailId = rewardDetailId;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public Integer getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Timestamp getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public Integer getRewardPoints() {
		return rewardPoints;
	}

	public void setRewardPoints(Integer rewardPoints) {
		this.rewardPoints = rewardPoints;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getRewardDetailName() {
		return rewardDetailName;
	}

	public void setRewardDetailName(String rewardDetailName) {
		this.rewardDetailName = rewardDetailName;
	}			
	
	public PmsRewards getPmsRewards() {
		return pmsRewards;
	}

	public void setPmsRewards(PmsRewards pmsRewards) {
		this.pmsRewards = pmsRewards;
	}
	
	public List<RewardUserDetails> getRewardUserDetails() {
		return rewardUserDetails;
	}

	public void setRewardUserDetails(List<RewardUserDetails> rewardUserDetails) {
		this.rewardUserDetails = rewardUserDetails;
	}

	
}