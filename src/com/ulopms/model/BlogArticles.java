package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the blog_articles database table.
 * 
 */
@Entity
@Table(name="blog_articles")
@NamedQuery(name="BlogArticles.findAll", query="SELECT p FROM BlogArticles p")
public class BlogArticles implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="id",sequenceName="blog_articles_blog_article_id_seq")
	@GeneratedValue(generator="id")
	@Column(name="blog_article_id", unique=true, nullable=false)
	private Integer blogArticleId;
	
	@Column(name="blog_article_title", length=1000)
	private String blogArticleTitle;
	
	@Column(name="blog_article_description", length=10485760)
	private String blogArticleDescription;

	@Column(name="created_by")
	private Integer createdBy;

	@Column(name="created_date")
	private Timestamp createdDate;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_deleted")
	private Boolean isDeleted;
	
	@Column(name="modified_by")
	private Integer modifiedBy;

	@Column(name="modified_date")
	private Timestamp modifiedDate;
	
	@Column(name="blog_article_title_image", length=100)
	private String blogArticleTitleImage;	
			
	@Column(name="blog_article_url", length=100)
	private String blogArticleUrl;	
	
			//bi-directional many-to-one association to BlogLocations
			@ManyToOne(fetch=FetchType.EAGER)
			@JoinColumn(name="blog_location_id")
			private BlogLocations blogLocations;
			
			//bi-directional many-to-one association to Blogcategories
			@ManyToOne(fetch=FetchType.EAGER)
			@JoinColumn(name="blog_category_id")
			private BlogCategories blogCategories;
			
			//bi-directional many-to-one association to BlogArticleImages
			@OneToMany(mappedBy="blogArticles")
			private List<BlogArticleImages> blogArticleImages;
			
			//bi-directional many-to-one association to BlogCategories
			@OneToMany(mappedBy="blogArticles")
			private List<BlogUserComments> blogUserComments;
			
			/*private Integer blogLocationId;
			
			private Integer blogCategoryId;
			
			

	public Integer getBlogLocationId() {
				return blogLocationId;
			}

			public void setBlogLocationId(Integer blogLocationId) {
				this.blogLocationId = blogLocationId;
			}

			public Integer getBlogCategoryId() {
				return blogCategoryId;
			}

			public void setBlogCategoryId(Integer blogCategoryId) {
				this.blogCategoryId = blogCategoryId;
			} */

	

	public BlogArticles() {
	}	

	public Integer getBlogArticleId() {
		return blogArticleId;
	}

	public void setBlogArticleId(Integer blogArticleId) {
		this.blogArticleId = blogArticleId;
	}

	public String getBlogArticleTitle() {
		return blogArticleTitle;
	}

	public void setBlogArticleTitle(String blogArticleTitle) {
		this.blogArticleTitle = blogArticleTitle;
	}

	public String getBlogArticleDescription() {
		return blogArticleDescription;
	}

	public void setBlogArticleDescription(String blogArticleDescription) {
		this.blogArticleDescription = blogArticleDescription;
	}	

	public Integer getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}


	public Integer getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Timestamp getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	
	public String getBlogArticleTitleImage() {
		return blogArticleTitleImage;
	}

	public void setBlogArticleTitleImage(String blogArticleTitleImage) {
		this.blogArticleTitleImage = blogArticleTitleImage;
	}
	
	public BlogLocations getBlogLocations() {
		return blogLocations;
	}

	public void setBlogLocations(BlogLocations blogLocations) {
		this.blogLocations = blogLocations;
	}

	public BlogCategories getBlogCategories() {
		return blogCategories;
	}

	public void setBlogCategories(BlogCategories blogCategories) {
		this.blogCategories = blogCategories;
	}
	
	public List<BlogArticleImages> getBlogArticleImages() {
		return blogArticleImages;
	}

	public void setBlogArticleImages(List<BlogArticleImages> blogArticleImages) {
		this.blogArticleImages = blogArticleImages;
	}
	
	public List<BlogUserComments> getBlogUserComments() {
		return blogUserComments;
	}

	public void setBlogUserComments(List<BlogUserComments> blogUserComments) {
		this.blogUserComments = blogUserComments;
	}

	public String getBlogArticleUrl() {
		return blogArticleUrl;
	}

	public void setBlogArticleUrl(String blogArticleUrl) {
		this.blogArticleUrl = blogArticleUrl;
	}

}