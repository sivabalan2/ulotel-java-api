package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the property_rate_details database table.
 * 
 */
@Entity
@Table(name="property_rate_details")
@NamedQuery(name="PropertyRateDetail.findAll", query="SELECT p FROM PropertyRateDetail p")
public class PropertyRateDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="id",sequenceName="property_rate_details_property_rate_details_id_seq")
	@GeneratedValue(generator="id")
	@Column(name="property_rate_details_id")
	private Integer propertyRateDetailsId;

	@Column(name="base_amount")
	private double baseAmount;

	@Column(name="created_by")
	private Integer createdBy;

	@Column(name="created_date")
	private Timestamp createdDate;

	@Column(name="day_part")
	private Integer dayPart;

	@Column(name="days_of_week")
	private String daysOfWeek;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_deleted")
	private Boolean isDeleted;
	
	@Column(name="extra_adult", precision=131089)
	private double extraAdult;

	@Column(name="extra_child", precision=131089)
	private double extraChild;
	
	@Column(name="extra_infant", precision=131089)
	private double extraInfant;

	//bi-directional many-to-one association to PropertyRate
	@ManyToOne
	@JoinColumn(name="property_rate_id")
	private PropertyRate propertyRate;

	@Column(name="minimum_base_amount")
	private Double minimumBaseAmount;
	
	@Column(name="maximum_base_amount")
	private Double maximumBaseAmount;
	

	public PropertyRateDetail() {
	}

	public Integer getPropertyRateDetailsId() {
		return this.propertyRateDetailsId;
	}

	public void setPropertyRateDetailsId(Integer propertyRateDetailsId) {
		this.propertyRateDetailsId = propertyRateDetailsId;
	}

	public double getBaseAmount() {
		return this.baseAmount;
	}

	public void setBaseAmount(double baseAmount) {
		this.baseAmount = baseAmount;
	}

	public Integer getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public Integer getDayPart() {
		return this.dayPart;
	}

	public void setDayPart(Integer dayPart) {
		this.dayPart = dayPart;
	}

	public String getDaysOfWeek() {
		return this.daysOfWeek;
	}

	public void setDaysOfWeek(String daysOfWeek) {
		this.daysOfWeek = daysOfWeek;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public PropertyRate getPropertyRate() {
		return this.propertyRate;
	}

	public void setPropertyRate(PropertyRate propertyRate) {
		this.propertyRate = propertyRate;
	}

	public double getExtraAdult() {
		return this.extraAdult;
	}

	public void setExtraAdult(double extraAdult) {
		this.extraAdult = extraAdult;
	}
	
	public double getExtraInfant() {
		return this.extraInfant;
	}

	public void setExtraInfant(double extraInfant) {
		this.extraInfant = extraInfant;
	}

	public double getExtraChild() {
		return this.extraChild;
	}

	public void setExtraChild(double extraChild) {
		this.extraChild = extraChild;
	}

	public Double getMinimumBaseAmount() {
		return minimumBaseAmount;
	}

	public void setMinimumBaseAmount(Double minimumBaseAmount) {
		this.minimumBaseAmount = minimumBaseAmount;
	}

	public Double getMaximumBaseAmount() {
		return maximumBaseAmount;
	}

	public void setMaximumBaseAmount(Double maximumBaseAmount) {
		this.maximumBaseAmount = maximumBaseAmount;
	}
	

}