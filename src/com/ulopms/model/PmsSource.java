package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the pms_source database table.
 * 
 */
@Entity
@Table(name="pms_source")
@NamedQuery(name="PmsSource.findAll", query="SELECT p FROM PmsSource p")
public class PmsSource implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="id",sequenceName="pms_source_source_id_seq")
	@GeneratedValue(generator="id")
	@Column(name="source_id")
	private Integer sourceId;

	@Column(name="created_by")
	private Integer createdBy;

	@Column(name="created_date")
	private Timestamp createdDate;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_deleted")
	private Boolean isDeleted;
	
	@Column(name="resort_is_active")
	private Boolean resortIsActive;

	

	@Column(name="source_name")
	private String sourceName;

	//bi-directional many-to-one association to PropertyRate
	@OneToMany(mappedBy="pmsSource")
	private List<PropertyRate> propertyRates;
	
	//bi-directional many-to-one association to otaHotels
	@OneToMany(mappedBy="pmsSource")
	private List<OtaHotels> otaHotels;	

	//bi-directional many-to-one association to PmsBooking
	@OneToMany(mappedBy="pmsSource")
	private List<PmsBooking> pmsBooking;

	//bi-directional one-to-many association to PmsSourceType
	@ManyToOne
	@JoinColumn(name="source_type_id")
	private PmsSourceType pmsSourceType;
	
	public PmsSourceType getPmsSourceType() {
		return pmsSourceType;
	}

	public void setPmsSourceType(PmsSourceType pmsSourceType) {
		this.pmsSourceType = pmsSourceType;
	}

	public List<PmsBooking> getPmsBooking() {
		return pmsBooking;
	}

	public void setPmsBooking(List<PmsBooking> pmsBooking) {
		this.pmsBooking = pmsBooking;
	}

	public PmsSource() {
	}

	public Integer getSourceId() {
		return this.sourceId;
	}

	public void setSourceId(Integer sourceId) {
		this.sourceId = sourceId;
	}

	public Integer getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getSourceName() {
		return this.sourceName;
	}

	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}

	public List<PropertyRate> getPropertyRates() {
		return this.propertyRates;
	}

	public void setPropertyRates(List<PropertyRate> propertyRates) {
		this.propertyRates = propertyRates;
	}

	public PropertyRate addPropertyRate(PropertyRate propertyRate) {
		getPropertyRates().add(propertyRate);
		propertyRate.setPmsSource(this);

		return propertyRate;
	}

	public PropertyRate removePropertyRate(PropertyRate propertyRate) {
		getPropertyRates().remove(propertyRate);
		propertyRate.setPmsSource(null);

		return propertyRate;
	}
	
	public Boolean getResortIsActive() {
		return resortIsActive;
	}

	public void setResortIsActive(Boolean resortIsActive) {
		this.resortIsActive = resortIsActive;
	}

	public List<OtaHotels> getOtaHotels() {
		return otaHotels;
	}

	public void setOtaHotels(List<OtaHotels> otaHotels) {
		this.otaHotels = otaHotels;
	}

}