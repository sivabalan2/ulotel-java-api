package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the blog_user_comments database table.
 * 
 */
@Entity
@Table(name="blog_user_comments")
@NamedQuery(name="BlogUserComments.findAll", query="SELECT p FROM BlogUserComments p")
public class BlogUserComments implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="id",sequenceName="blog_user_comments_blog_user_comment_id_seq")
	@GeneratedValue(generator="id")
	@Column(name="blog_user_comment_id", unique=true, nullable=false)
	private Integer blogUserCommentId;
	
	@Column(name="blog_user_name", length=100)
	private String blogUserName;
	
	@Column(name="blog_user_email", length=100)
	private String blogUserEmail;
	
	@Column(name="blog_user_url", length=1000)
	private String blogUserUrl;
	
	@Column(name="blog_user_comment", length=10485760)
	private String blogUserComment;

	@Column(name="created_by")
	private Integer createdBy;

	@Column(name="created_date")
	private Timestamp createdDate;

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_deleted")
	private Boolean isDeleted;
	
	@Column(name="modified_by")
	private Integer modifiedBy;

	@Column(name="modified_date")
	private Timestamp modifiedDate;
	
	@Column(name="is_display")
	private Boolean isDisplay;	
	
	//bi-directional many-to-one association to BlogArticles
			@ManyToOne(fetch=FetchType.EAGER)
			@JoinColumn(name="blog_article_id")
			private BlogArticles blogArticles;		



	public BlogUserComments() {
	}	



	public Integer getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}


	public Integer getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Timestamp getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public Integer getBlogUserCommentId() {
		return blogUserCommentId;
	}

	public void setBlogUserCommentId(Integer blogUserCommentId) {
		this.blogUserCommentId = blogUserCommentId;
	}

	public String getBlogUserName() {
		return blogUserName;
	}

	public void setBlogUserName(String blogUserName) {
		this.blogUserName = blogUserName;
	}

	public String getBlogUserEmail() {
		return blogUserEmail;
	}

	public void setBlogUserEmail(String blogUserEmail) {
		this.blogUserEmail = blogUserEmail;
	}

	public String getBlogUserUrl() {
		return blogUserUrl;
	}

	public void setBlogUserUrl(String blogUserUrl) {
		this.blogUserUrl = blogUserUrl;
	}

	public String getBlogUserComment() {
		return blogUserComment;
	}

	public void setBlogUserComment(String blogUserComment) {
		this.blogUserComment = blogUserComment;
	}

	public Boolean getIsDisplay() {
		return isDisplay;
	}

	public void setIsDisplay(Boolean isDisplay) {
		this.isDisplay = isDisplay;
	}	
	
	public BlogArticles getBlogArticles() {
		return blogArticles;
	}

	public void setBlogArticles(BlogArticles blogArticles) {
		this.blogArticles = blogArticles;
	}


}