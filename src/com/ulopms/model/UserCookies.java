package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the user_cookies database table.
 * 
 */
@Entity
@Table(name="user_cookies")
@NamedQuery(name="UserCookies.findAll", query="SELECT p FROM UserCookies p")
public class UserCookies implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="id",sequenceName="user_cookies_cookie_id_seq")
	@GeneratedValue(generator="id")
	@Column(name="cookie_id")
	private Integer cookieId;

	@Column(name="user_key", length=500)
	private String userKey;

	@Column(name="user_key_value" , length=10000)
	private String userKeyValue;
	
	@Column(name="user_search_date")
	private Timestamp userSearchDate;
	
	@Column(name="user_start_date")
	private Timestamp userStartDate;
	
	@Column(name="user_end_date")
	private Timestamp userEndDate;
	
	@Column(name="user_search_value")
	private String userSearchValue;
	
	@Column(name="user_search_id")
	private Integer userSearchId;
	
	@Column(name="is_active")
	private Boolean isActive;
	
	@Column(name="is_deleted")
	private Boolean isDeleted;
	
	@Column(name="is_cookies_enable")
	private Boolean isCookiesEnable;
	
	@Column(name="is_search_enable")
	private Boolean isSearchEnable;

	@Column(name="search_user_id") 
	private Integer searchUserId;

	@Column(name="searchable_date")
	private Timestamp searchableDate;

	@Column(name="user_ip_address")
	private String userIpAddress;
	
	public UserCookies() {
	}
	
	public Integer getSearchUserId() {
		return searchUserId;
	}


	public void setSearchUserId(Integer searchUserId) {
		this.searchUserId = searchUserId;
	}
	
	
	public Timestamp getUserSearchDate() {
		return userSearchDate;
	}


	public void setUserSearchDate(Timestamp userSearchDate) {
		this.userSearchDate = userSearchDate;
	}


	public Timestamp getUserStartDate() {
		return userStartDate;
	}


	public void setUserStartDate(Timestamp userStartDate) {
		this.userStartDate = userStartDate;
	}


	public Timestamp getUserEndDate() {
		return userEndDate;
	}


	public void setUserEndDate(Timestamp userEndDate) {
		this.userEndDate = userEndDate;
	}


	public String getUserSearchValue() {
		return userSearchValue;
	}


	public void setUserSearchValue(String userSearchValue) {
		this.userSearchValue = userSearchValue;
	}


	public Integer getUserSearchId() {
		return userSearchId;
	}


	public void setUserSearchId(Integer userSearchId) {
		this.userSearchId = userSearchId;
	}


	public Boolean getIsActive() {
		return isActive;
	}


	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}


	public Boolean getIsDeleted() {
		return isDeleted;
	}


	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}


	public Boolean getIsCookiesEnable() {
		return isCookiesEnable;
	}


	public void setIsCookiesEnable(Boolean isCookiesEnable) {
		this.isCookiesEnable = isCookiesEnable;
	}


	public Boolean getIsSearchEnable() {
		return isSearchEnable;
	}


	public void setIsSearchEnable(Boolean isSearchEnable) {
		this.isSearchEnable = isSearchEnable;
	}



	public Integer getCookieId() {
		return cookieId;
	}

	public void setCookieId(Integer cookieId) {
		this.cookieId = cookieId;
	}

	public String getUserKey() {
		return userKey;
	}

	public void setUserKey(String userKey) {
		this.userKey = userKey;
	}

	public String getUserKeyValue() {
		return userKeyValue;
	}
	
	public void setUserKeyValue(String userKeyValue) {
		this.userKeyValue = userKeyValue;
	}

	public Timestamp getSearchableDate() {
		return searchableDate;
	}

	public void setSearchableDate(Timestamp searchableDate) {
		this.searchableDate = searchableDate;
	}

	public String getUserIpAddress() {
		return userIpAddress;
	}

	public void setUserIpAddress(String userIpAddress) {
		this.userIpAddress = userIpAddress;
	}
	
}