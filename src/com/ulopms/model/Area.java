package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the area database table.
 * 
 */
@Entity
@Table(name="area")
@NamedQuery(name="Area.findAll", query="SELECT a FROM Area a")
public class Area implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="id",sequenceName="area_area_id_seq")
	@GeneratedValue(generator="id")
	@Column(name="area_id", unique=true, nullable=false)
	private Integer areaId;

	@Column(name="created_by")
	private Integer createdBy;

	@Column(name="created_on")
	private Timestamp createdOn;	

	@Column(name="is_active")
	private Boolean isActive;

	@Column(name="is_deleted")
	private Boolean isDeleted;

	@Column(name="area_name", length=50)
	private String areaName;
	
	@Column(name="display_area_name", length=100)
	private String displayAreaName;

	@Column(name="modified_by")
	private Integer modifiedBy;

	@Column(name="modified_on")
	private Timestamp modifiedOn;
	
	@Column(name="area_thumbimg", length=10)
	private String areaThumbPath;

	@Column(name="area_url", length=50)
	private String areaUrl;
    
	@Column(name="area_description", length=50)
	private String areaDescription;
	

	@Column(name="redirect_url", length=50)
	private String redirectUrl;
	
	//bi-directional many-to-one association to PmsProperty
	@OneToMany(mappedBy="area")
	private List<PmsProperty> pmsProperties;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="location_id")
	private Location location;

   
	public Area() {
	}

	public Integer getAreaId() {
		return areaId;
	}

	public void setAreaId(Integer areaId) {
		this.areaId = areaId;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public String getDisplayAreaName() {
		return displayAreaName;
	}

	public void setDisplayAreaName(String displayAreaName) {
		this.displayAreaName = displayAreaName;
	}

	public Integer getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Timestamp getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Timestamp modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public List<PmsProperty> getPmsProperties() {
		return pmsProperties;
	}

	public void setPmsProperties(List<PmsProperty> pmsProperties) {
		this.pmsProperties = pmsProperties;
	}
	
	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}
	
	public String getAreaThumbPath() {
		return areaThumbPath;
	}

	public void setAreaThumbPath(String areaThumbPath) {
		this.areaThumbPath = areaThumbPath;
	}
	
	public String getAreaUrl() {
		return areaUrl;
	}

	public void setAreaUrl(String areaUrl) {
		this.areaUrl = areaUrl;
	}

	public String getAreaDescription() {
		return areaDescription;
	}

	public void setAreaDescription(String areaDescription) {
		this.areaDescription = areaDescription;
	}

	public String getRedirectUrl() {
		return redirectUrl;
	}

	public void setRedirectUrl(String redirectUrl) {
		this.redirectUrl = redirectUrl;
	}
	

	
}