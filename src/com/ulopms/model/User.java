package com.ulopms.model;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;
import java.util.List;
import java.util.Set;


/**
 * The persistent class for the mst_users database table.
 * 
 */
@Entity
@Table(name="user",schema="public")
@NamedQuery(name="User.findAll", query="SELECT p FROM User p")
public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="id",sequenceName="mast_user_user_id_seq")
	@GeneratedValue(generator="id")
	@Column(name="user_id", unique=true, nullable=false)
	private Integer userId;

	@Column(name="address_1", length=50)
	private String address1;

	@Column(name="address_2", length=50)
	private String address2;
	
	@Column(name="address_3", length=50)
	private String address3;

	@Column(name="created_date")
	private Timestamp createdDate;

	@Column(name="email_id", length=50)
	private String emailId;
	
	@Column(name="email_id1", length=50)
	private String emailId1;
	
	@Column(name="login_type", length=10)
	private String loginType;
	
	@Column(name="login_otp", length=10)
	private String loginOtp;

	

	@Column(length=500)
	private String hashPassword;

	@Column(name="is_active", nullable=false)
	private Boolean isActive;

	@Column(name="is_deleted", nullable=false)
	private Boolean isDeleted;

	
    @Column(name="modified_by")
	private Integer modifiedBy;

	@Column(name="modified_date")
	private Timestamp modifiedDate;

	@Column(name="phone",length=50)
	private String phone;
	
	@Column(name="phone1",length=50)
	private String phone1;

	@Column(name="saltkey",length=150)
	private String saltkey;
	
	@Column(name="traveler_type",length=50)
	private String travelerType;

	@Column(name="user_name", length=50)
	private String userName;
	
	@Column(name="profile_pic_name", length=50)
	private String ImgPath;

	@OneToMany(mappedBy="user")
	private List<RewardUserDetails> rewardUserDetails;
	
	//bi-directional many-to-one association to ForgetPassword
	@OneToMany(mappedBy="user")
	private List<ForgetPassword> forgetPasswords;

	//bi-directional many-to-one association to Notification
	@OneToMany(mappedBy="user")
	private List<Notification> notifications;

	//bi-directional many-to-one association to PmsProperty
	@OneToMany(mappedBy="user")
	private List<PmsProperty> pmsProperties;

	//bi-directional many-to-one association to PropertyUser
	@OneToMany(mappedBy="user")
	private List<PropertyUser> propertyUsers;

	
	//bi-directional many-to-one association to Role
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="role_id", nullable=false)
	private Role role;

	//bi-directional many-to-one association to User
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="created_by")
	private User user;

	//bi-directional many-to-one association to User
	@OneToMany(mappedBy="user")
	private List<User> users;

	//bi-directional many-to-one association to UserAccessRight
	@OneToMany(mappedBy="user")
	private List<UserAccessRight> userAccessRights;

	//bi-directional many-to-one association to UserClientToken
	@OneToMany(mappedBy="user")
	private List<UserClientToken> userClientTokens;

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="tag_id")
	private PmsTags pmsTags;
	
	public User() {
	}

	public Integer getUserId() {
		return this.userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getAddress1() {
		return this.address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return this.address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	
	public Timestamp getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	

	public String getEmailId() {
		return this.emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getHashpassword() {
		return this.hashPassword;
	}

	public void setHashpassword(String hashPassword) {
		this.hashPassword = hashPassword;
	}

	public Boolean getIsActive() {
		return this.isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	
    public Integer getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(Integer modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Timestamp getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getLoginType() {
		return loginType;
	}

	public void setLoginType(String loginType) {
		this.loginType = loginType;
	}

	public String getLoginOtp() {
		return loginOtp;
	}

	public void setLoginOtp(String loginOtp) {
		this.loginOtp = loginOtp;
	}

	public String getImgPath() {
		return this. ImgPath;
	}

	public void setImgPath(String ImgPath) {
		this.ImgPath = ImgPath;
	}
	
	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	
	public String getEmailId1() {
		return emailId1;
	}

	public void setEmailId1(String emailId1) {
		this.emailId1 = emailId1;
	}

	public String getPhone1() {
		return phone1;
	}

	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}

	public String getSaltkey() {
		return this.saltkey;
	}

	public void setSaltkey(String saltkey) {
		this.saltkey = saltkey;
	}

	public String getAddress3() {
		return address3;
	}

	public void setAddress3(String address3) {
		this.address3 = address3;
	}

	public String getTravelerType() {
		return travelerType;
	}

	public void setTravelerType(String travelerType) {
		this.travelerType = travelerType;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	


	public List<ForgetPassword> getForgetPasswords() {
		return this.forgetPasswords;
	}

	public void setForgetPasswords(List<ForgetPassword> forgetPasswords) {
		this.forgetPasswords = forgetPasswords;
	}

	public List<RewardUserDetails> getRewardUserDetails() {
		return rewardUserDetails;
	}

	public void setRewardUserDetails(List<RewardUserDetails> rewardUserDetails) {
		this.rewardUserDetails = rewardUserDetails;
	}

	public ForgetPassword addForgetPassword(ForgetPassword forgetPassword) {
		getForgetPasswords().add(forgetPassword);
		forgetPassword.setUser(this);

		return forgetPassword;
	}

	public ForgetPassword removeForgetPassword(ForgetPassword forgetPassword) {
		getForgetPasswords().remove(forgetPassword);
		forgetPassword.setUser(null);

		return forgetPassword;
	}

	public List<Notification> getNotifications() {
		return this.notifications;
	}

	public void setNotifications(List<Notification> notifications) {
		this.notifications = notifications;
	}

	public Notification addNotification(Notification notification) {
		getNotifications().add(notification);
		notification.setUser(this);

		return notification;
	}

	public Notification removeNotification(Notification notification) {
		getNotifications().remove(notification);
		notification.setUser(null);

		return notification;
	}

	public List<PmsProperty> getPmsProperties() {
		return this.pmsProperties;
	}

	public void setPmsProperties(List<PmsProperty> pmsProperties) {
		this.pmsProperties = pmsProperties;
	}

	public PmsProperty addPmsProperty(PmsProperty pmsProperty) {
		getPmsProperties().add(pmsProperty);
		pmsProperty.setUser(this);

		return pmsProperty;
	}

	public PmsProperty removePmsProperty(PmsProperty pmsProperty) {
		getPmsProperties().remove(pmsProperty);
		pmsProperty.setUser(null);

		return pmsProperty;
	}

	public List<PropertyUser> getPropertyUsers() {
		return this.propertyUsers;
	}

	public void setPropertyUsers(List<PropertyUser> propertyUsers) {
		this.propertyUsers = propertyUsers;
	}

	public PropertyUser addPropertyUser(PropertyUser propertyUser) {
		getPropertyUsers().add(propertyUser);
		propertyUser.setUser(this);

		return propertyUser;
	}

	public PropertyUser removePropertyUser(PropertyUser propertyUser) {
		getPropertyUsers().remove(propertyUser);
		propertyUser.setUser(null);

		return propertyUser;
	}

	

	public Role getRole() {
		return this.role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<User> getUsers() {
		return this.users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public User addUser(User user) {
		getUsers().add(user);
		user.setUser(this);

		return user;
	}

	public User removeUser(User user) {
		getUsers().remove(user);
		user.setUser(null);

		return user;
	}

	public List<UserAccessRight> getUserAccessRights() {
		return this.userAccessRights;
	}

	public void setUserAccessRights(List<UserAccessRight> userAccessRights) {
		this.userAccessRights = userAccessRights;
	}

	public UserAccessRight addUserAccessRight(UserAccessRight userAccessRight) {
		getUserAccessRights().add(userAccessRight);
		userAccessRight.setUser(this);

		return userAccessRight;
	}

	public UserAccessRight removeUserAccessRight(UserAccessRight userAccessRight) {
		getUserAccessRights().remove(userAccessRight);
		userAccessRight.setUser(null);

		return userAccessRight;
	}

	public List<UserClientToken> getUserClientTokens() {
		return this.userClientTokens;
	}

	public void setUserClientTokens(List<UserClientToken> userClientTokens) {
		this.userClientTokens = userClientTokens;
	}

	public UserClientToken addUserClientToken(UserClientToken userClientToken) {
		getUserClientTokens().add(userClientToken);
		userClientToken.setUser(this);

		return userClientToken;
	}

	public UserClientToken removeUserClientToken(UserClientToken userClientToken) {
		getUserClientTokens().remove(userClientToken);
		userClientToken.setUser(null);

		return userClientToken;
	}

	public PmsTags getPmsTags() {
		return pmsTags;
	}

	public void setPmsTags(PmsTags pmsTags) {
		this.pmsTags = pmsTags;
	}

		
}