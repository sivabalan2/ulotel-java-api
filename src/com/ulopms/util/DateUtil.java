package com.ulopms.util;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class DateUtil {
	
	public static List<DateTime> getDateRange(DateTime start, DateTime end) {

        List<DateTime> ret = new ArrayList<DateTime>();
        DateTime tmp = start;
        while(tmp.isBefore(end) || tmp.equals(end)) {
            ret.add(tmp);
            tmp = tmp.plusDays(1);
        }
        return ret;
    }
	
	public static List<String> getDayRange(DateTime start, DateTime end) {

        List<String> ret = new ArrayList<String>();
        DateTime tmp = start;
        while(tmp.isBefore(end) || tmp.equals(end)) {
        	
        	DateTimeFormatter fmt = DateTimeFormat.forPattern("EEEE");
	    	String dtStr = fmt.print(tmp);
            ret.add(dtStr);
            tmp = tmp.plusDays(1);
        }
        return ret;
    }
	
	public static int getDay(String day)
	 {
		
	  
	   
	  if(day.equalsIgnoreCase("Monday"))
	   return 1;
	  if(day.equalsIgnoreCase("Tuesday"))
	   return 2;
	  if(day.equalsIgnoreCase("Wednesday"))
	   return 3;
	  if(day.equalsIgnoreCase("Thursday"))
	   return 4;
	  if(day.equalsIgnoreCase("Friday"))
	   return 5;
	  if(day.equalsIgnoreCase("Saturday"))
	   return 6;
	  if(day.equalsIgnoreCase("Sunday"))
	   return 7;
	  return 0; 
	 }
	 

}
