package com.ulopms.util;

import java.awt.image.BufferedImage;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

public class Image {

	public byte[] getCustomImageInBytes(String imagePath) {

		byte[] imageInByte = null;
		File file = new File(imagePath);

		BufferedImage originalImage;
		try {
			originalImage = ImageIO.read(file);
			// convert BufferedImage to byte array
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ImageIO.write(originalImage, "jpg", baos);
			baos.flush();
			imageInByte = baos.toByteArray();
			baos.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return imageInByte;
	}

}
