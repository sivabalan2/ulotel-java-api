package com.ulopms.util;

import com.sendgrid.*;

import java.io.*;
import java.util.*;

import javax.mail.*;
import javax.mail.internet.*;
import javax.activation.*;
import javax.servlet.http.*;
import javax.servlet.*;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;




public class Email {
	
	private String _to;
	private String _from;
	private String _username;
	private String _password;
	private String _host;
	private String _port;
	private String _subject;
	private String _body;
	private Writer _bodyContent;
	private String _cc;
	private String _cc2;
	private String _cc3;
	private String _cc4;
	private String _cc5;
	private String _cc6;
	private String _cc7;
	private String _cc8;
	private String _cc9;
	private String _cc10;
	private String _cc11;
	private String _cc12;
	private String _cc13;
	private String _cc14;
	private String _fileName;
	private Integer _emailSize;
	private String emailArray[];
	private ArrayList<String> _bcc;
	
	
	public ArrayList<String> get_bcc() {
		return _bcc;
	}
	public void set_bcc(ArrayList<String> _bcc) {
		this._bcc = _bcc;
	}
	
	
	
	public void sendEmailWithAttachment()
	{

		try {
			
			final String username = "reservations@ulohotels.com";
			final String password = "Ulohotels@1234";
			
			//final String username = "techsupport@ulohotels.com";
			//final String password = "Ulo#2017";
			
			String  from = _from;
			String  to = _to;
		    String subject = _subject;
		    Content content = new Content("text/html", _bodyContent.toString());
		    //InternetAddress[] myCcList = InternetAddress.parse("NEHA.SIVA@xyz.com");
		    String[] cc14={ _cc,_cc2,_cc3,_cc4,_cc5,_cc6,_cc7,_cc8,_cc9,_cc10,_cc11,_cc12,_cc13,_cc14 };
		    String[] cc4={ _cc,_cc2,_cc3,_cc4 };
		    String[] cc5={ _cc,_cc2,_cc3,_cc4,_cc5 };
		    String[] cc6={ _cc,_cc2,_cc3,_cc4,_cc5,_cc6 };
		    String[] cc7={ _cc,_cc2,_cc3,_cc4,_cc5,_cc6,_cc7 };
		    String[] cc8={ _cc,_cc2,_cc3,_cc4,_cc5,_cc6,_cc7,_cc8};  
		    String[] cc9={ _cc,_cc2,_cc3,_cc4,_cc5,_cc6,_cc7,_cc8,_cc9};
		    String[] cc10={ _cc,_cc2,_cc3,_cc4,_cc5,_cc6,_cc7,_cc8,_cc9,_cc10};
		    String[] cc11={ _cc,_cc2,_cc3,_cc4,_cc5,_cc6,_cc7,_cc8,_cc9,_cc10,_cc11};
		    String[] cc12={ _cc,_cc2,_cc3,_cc4,_cc5,_cc6,_cc7,_cc8,_cc9,_cc10,_cc11,_cc12};
		    
			Properties props = new Properties();
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.smtp.host", "smtp.gmail.com");
			props.put("mail.smtp.port", "587");
//			props.put("mail.debug", "true");

			Session session = Session.getInstance(props,
			  new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(username, password);
				}
			  });
            
			
		    //Mail mail = new Mail(from, subject, to, content);
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(from));
			message.setRecipients(Message.RecipientType.TO,
				InternetAddress.parse(to));
			message.setSubject(subject);
			message.setContent(_bodyContent.toString(), "text/html; charset=utf-8");
			
			if(get_emailSize()==4){
				 
	            InternetAddress[] ccAddress = new InternetAddress[cc4.length];
	            
	            // To get the array of ccaddresses
	            for( int i = 0; i < cc4.length; i++ ) {
	                ccAddress[i] = new InternetAddress(cc4[i]);
	            }
	            
	            // Set cc: header field of the header.
	            for( int i = 0; i < ccAddress.length; i++) {
	                message.addRecipient(Message.RecipientType.CC, ccAddress[i]);
	            }
	            Multipart multipart = new MimeMultipart();
		           
	            MimeBodyPart textPart = new MimeBodyPart();
	            textPart.setText(_bodyContent.toString(), "utf-8");

	            MimeBodyPart htmlPart = new MimeBodyPart();
	            htmlPart.setContent(_bodyContent.toString(), "text/html; charset=utf-8");
	            
				MimeBodyPart attachPart = new MimeBodyPart();
				attachPart.attachFile(get_fileName());
				attachPart.setHeader("Content-Type", "application/PDF; Content-Disposition: attachment; charset=\"us-ascii\"; name=\"'"+get_fileName()+"'\"");
				multipart.addBodyPart(attachPart);
				
	            multipart.addBodyPart(htmlPart); // <-- second
				message.setContent(multipart);      
			}else if(get_emailSize()==5){
	            InternetAddress[] ccAddress = new InternetAddress[cc5.length];
	            for( int i = 0; i < cc5.length; i++ ) {	            // To get the array of ccaddresses
	                ccAddress[i] = new InternetAddress(cc5[i]);
	            }
	            for( int i = 0; i < ccAddress.length; i++) {// Set cc: header field of the header.
	                message.addRecipient(Message.RecipientType.CC, ccAddress[i]);
	            }
	            
	            Multipart multipart = new MimeMultipart();
		           
	            MimeBodyPart textPart = new MimeBodyPart();
	            textPart.setText(_bodyContent.toString(), "utf-8");

	            MimeBodyPart htmlPart = new MimeBodyPart();
	            htmlPart.setContent(_bodyContent.toString(), "text/html; charset=utf-8");
	            
				MimeBodyPart attachPart = new MimeBodyPart();
				attachPart.attachFile(get_fileName());
				attachPart.setHeader("Content-Type", "application/PDF; Content-Disposition: attachment; charset=\"us-ascii\"; name=\"'"+get_fileName()+"'\"");
				multipart.addBodyPart(attachPart);
				
	            multipart.addBodyPart(htmlPart); // <-- second
				message.setContent(multipart);
			}else if(get_emailSize()==6){
	            InternetAddress[] ccAddress = new InternetAddress[cc6.length];
	            for( int i = 0; i < cc6.length; i++ ) {	            // To get the array of ccaddresses
	                ccAddress[i] = new InternetAddress(cc6[i]);
	            }
	            for( int i = 0; i < ccAddress.length; i++) {// Set cc: header field of the header.
	                message.addRecipient(Message.RecipientType.CC, ccAddress[i]);
	            }
	            
	            Multipart multipart = new MimeMultipart();
		           
	            MimeBodyPart textPart = new MimeBodyPart();
	            textPart.setText(_bodyContent.toString(), "utf-8");

	            MimeBodyPart htmlPart = new MimeBodyPart();
	            htmlPart.setContent(_bodyContent.toString(), "text/html; charset=utf-8");
	            
				MimeBodyPart attachPart = new MimeBodyPart();
				attachPart.attachFile(get_fileName());
				attachPart.setHeader("Content-Type", "application/PDF; Content-Disposition: attachment; charset=\"us-ascii\"; name=\"'"+get_fileName()+"'\"");
				multipart.addBodyPart(attachPart);
				//
	            multipart.addBodyPart(htmlPart); // <-- second
				message.setContent(multipart);
			}else if(get_emailSize()==7){
	            InternetAddress[] ccAddress = new InternetAddress[cc7.length];
	            for( int i = 0; i < cc7.length; i++ ) {	            // To get the array of ccaddresses
	                ccAddress[i] = new InternetAddress(cc7[i]);
	            }
	            for( int i = 0; i < ccAddress.length; i++) {// Set cc: header field of the header.
	                message.addRecipient(Message.RecipientType.CC, ccAddress[i]);
	            }
	            
	            Multipart multipart = new MimeMultipart();
		           
	            MimeBodyPart textPart = new MimeBodyPart();
	            textPart.setText(_bodyContent.toString(), "utf-8");

	            MimeBodyPart htmlPart = new MimeBodyPart();
	            htmlPart.setContent(_bodyContent.toString(), "text/html; charset=utf-8");
	            
				MimeBodyPart attachPart = new MimeBodyPart();
				attachPart.attachFile(get_fileName());
				attachPart.setHeader("Content-Type", "application/PDF; Content-Disposition: attachment; charset=\"us-ascii\"; name=\"'"+get_fileName()+"'\"");
				multipart.addBodyPart(attachPart);
				//
	            multipart.addBodyPart(htmlPart); // <-- second
				message.setContent(multipart);
			}else if(get_emailSize()==8){
	            InternetAddress[] ccAddress = new InternetAddress[cc8.length];
	            for( int i = 0; i < cc8.length; i++ ) {	            // To get the array of ccaddresses
	                ccAddress[i] = new InternetAddress(cc8[i]);
	            }
	            for( int i = 0; i < ccAddress.length; i++) {// Set cc: header field of the header.
	                message.addRecipient(Message.RecipientType.CC, ccAddress[i]);
	            }
	            
	            Multipart multipart = new MimeMultipart();
		           
	            MimeBodyPart textPart = new MimeBodyPart();
	            textPart.setText(_bodyContent.toString(), "utf-8");

	            MimeBodyPart htmlPart = new MimeBodyPart();
	            htmlPart.setContent(_bodyContent.toString(), "text/html; charset=utf-8");
	            
				MimeBodyPart attachPart = new MimeBodyPart();
				attachPart.attachFile(get_fileName());
				attachPart.setHeader("Content-Type", "application/PDF; Content-Disposition: attachment; charset=\"us-ascii\"; name=\"'"+get_fileName()+"'\"");
				multipart.addBodyPart(attachPart);
				//
	            multipart.addBodyPart(htmlPart); // <-- second
				message.setContent(multipart);
			}else if(get_emailSize()==9){
	            InternetAddress[] ccAddress = new InternetAddress[cc9.length];
	            for( int i = 0; i < cc9.length; i++ ) {	            // To get the array of ccaddresses
	                ccAddress[i] = new InternetAddress(cc9[i]);
	            }
	            for( int i = 0; i < ccAddress.length; i++) {// Set cc: header field of the header.
	                message.addRecipient(Message.RecipientType.CC, ccAddress[i]);
	            }
	            Multipart multipart = new MimeMultipart();
		           
	            MimeBodyPart textPart = new MimeBodyPart();
	            textPart.setText(_bodyContent.toString(), "utf-8");

	            MimeBodyPart htmlPart = new MimeBodyPart();
	            htmlPart.setContent(_bodyContent.toString(), "text/html; charset=utf-8");
	            
				MimeBodyPart attachPart = new MimeBodyPart();
				attachPart.attachFile(get_fileName());
				attachPart.setHeader("Content-Type", "application/PDF; Content-Disposition: attachment; charset=\"us-ascii\"; name=\"'"+get_fileName()+"'\"");
				multipart.addBodyPart(attachPart);
				//
	            multipart.addBodyPart(htmlPart); // <-- second
				message.setContent(multipart);
			}else if(get_emailSize()==10){
	            InternetAddress[] ccAddress = new InternetAddress[cc10.length];
	            for( int i = 0; i < cc10.length; i++ ) {	            // To get the array of ccaddresses
	                ccAddress[i] = new InternetAddress(cc7[i]);
	            }
	            for( int i = 0; i < ccAddress.length; i++) {// Set cc: header field of the header.
	                message.addRecipient(Message.RecipientType.CC, ccAddress[i]);
	            }
	            Multipart multipart = new MimeMultipart();
		           
	            MimeBodyPart textPart = new MimeBodyPart();
	            textPart.setText(_bodyContent.toString(), "utf-8");

	            MimeBodyPart htmlPart = new MimeBodyPart();
	            htmlPart.setContent(_bodyContent.toString(), "text/html; charset=utf-8");
	            
				MimeBodyPart attachPart = new MimeBodyPart();
				attachPart.attachFile(get_fileName());
				attachPart.setHeader("Content-Type", "application/PDF; Content-Disposition: attachment; charset=\"us-ascii\"; name=\"'"+get_fileName()+"'\"");
				multipart.addBodyPart(attachPart);
				//
	            multipart.addBodyPart(htmlPart); // <-- second
				message.setContent(multipart);
			}else if(get_emailSize()==11){
	            InternetAddress[] ccAddress = new InternetAddress[cc11.length];
	            for( int i = 0; i < cc11.length; i++ ) {	            // To get the array of ccaddresses
	                ccAddress[i] = new InternetAddress(cc7[i]);
	            }
	            for( int i = 0; i < ccAddress.length; i++) {// Set cc: header field of the header.
	                message.addRecipient(Message.RecipientType.CC, ccAddress[i]);
	            }
	            
	            Multipart multipart = new MimeMultipart();
		           
	            MimeBodyPart textPart = new MimeBodyPart();
	            textPart.setText(_bodyContent.toString(), "utf-8");

	            MimeBodyPart htmlPart = new MimeBodyPart();
	            htmlPart.setContent(_bodyContent.toString(), "text/html; charset=utf-8");
	            
				MimeBodyPart attachPart = new MimeBodyPart();
				attachPart.attachFile(get_fileName());
				attachPart.setHeader("Content-Type", "application/PDF; Content-Disposition: attachment; charset=\"us-ascii\"; name=\"'"+get_fileName()+"'\"");
				multipart.addBodyPart(attachPart);
				//
	            multipart.addBodyPart(htmlPart); // <-- second
				message.setContent(multipart);
			}else if(get_emailSize()==12){
	            InternetAddress[] ccAddress = new InternetAddress[cc12.length];
	            for( int i = 0; i < cc12.length; i++ ) {	            // To get the array of ccaddresses
	                ccAddress[i] = new InternetAddress(cc12[i]);
	            }
	            for( int i = 0; i < ccAddress.length; i++) {// Set cc: header field of the header.
	                message.addRecipient(Message.RecipientType.CC, ccAddress[i]);
	            }
	            
	            Multipart multipart = new MimeMultipart();
		           
	            MimeBodyPart textPart = new MimeBodyPart();
	            textPart.setText(_bodyContent.toString(), "utf-8");

	            MimeBodyPart htmlPart = new MimeBodyPart();
	            htmlPart.setContent(_bodyContent.toString(), "text/html; charset=utf-8");
	            
				MimeBodyPart attachPart = new MimeBodyPart();
				attachPart.attachFile(get_fileName());
				attachPart.setHeader("Content-Type", "application/PDF; Content-Disposition: attachment; charset=\"us-ascii\"; name=\"'"+get_fileName()+"'\"");
				multipart.addBodyPart(attachPart);
				//
	            multipart.addBodyPart(htmlPart); // <-- second
				message.setContent(multipart);
			}


			Transport.send(message);
			System.out.println("Done");

		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	
	}
	
	public void sendEmailWithoutAttachment(){


		try {
			
			final String username = "reservations@ulohotels.com";
			final String password = "Ulohotels@1234";
			
//			final String username = "techsupport@ulohotels.com";
//			final String password = "Ulo#2017";
			
			String  from = _from;
			String  to = _to;
		    String subject = _subject;
		    Content content = new Content("text/html", _bodyContent.toString());
		    //InternetAddress[] myCcList = InternetAddress.parse("NEHA.SIVA@xyz.com");
		    String[] cc14={ _cc,_cc2,_cc3,_cc4,_cc5,_cc6,_cc7,_cc8,_cc9,_cc10,_cc11,_cc12,_cc13,_cc14 };
		    String[] cc4={ _cc,_cc2,_cc3,_cc4 };
		    String[] cc5={ _cc,_cc2,_cc3,_cc4,_cc5 };
		    String[] cc6={ _cc,_cc2,_cc3,_cc4,_cc5,_cc6 };
		    String[] cc7={ _cc,_cc2,_cc3,_cc4,_cc5,_cc6,_cc7 };
		    String[] cc8={ _cc,_cc2,_cc3,_cc4,_cc5,_cc6,_cc7,_cc8};  
		    String[] cc9={ _cc,_cc2,_cc3,_cc4,_cc5,_cc6,_cc7,_cc8,_cc9};
		    String[] cc10={ _cc,_cc2,_cc3,_cc4,_cc5,_cc6,_cc7,_cc8,_cc9,_cc10};
		    String[] cc11={ _cc,_cc2,_cc3,_cc4,_cc5,_cc6,_cc7,_cc8,_cc9,_cc10,_cc11};
		    String[] cc12={ _cc,_cc2,_cc3,_cc4,_cc5,_cc6,_cc7,_cc8,_cc9,_cc10,_cc11,_cc12};
		    
			Properties props = new Properties();
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.smtp.host", "smtp.gmail.com");
			props.put("mail.smtp.port", "587");
//			props.put("mail.debug", "true");

			Session session = Session.getInstance(props,
			  new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(username, password);
				}
			  });
            
			
		    //Mail mail = new Mail(from, subject, to, content);
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(from));
			message.setRecipients(Message.RecipientType.TO,
				InternetAddress.parse(to));
			message.setSubject(subject);
			message.setContent(_bodyContent.toString(), "text/html; charset=utf-8");
			
			if(get_emailSize()==4){
				 
	            InternetAddress[] ccAddress = new InternetAddress[cc4.length];
	            
	            // To get the array of ccaddresses
	            for( int i = 0; i < cc4.length; i++ ) {
	                ccAddress[i] = new InternetAddress(cc4[i]);
	            }
	            
	            // Set cc: header field of the header.
	            for( int i = 0; i < ccAddress.length; i++) {
	                message.addRecipient(Message.RecipientType.CC, ccAddress[i]);
	            }
			          
			}else if(get_emailSize()==5){
				 
	            InternetAddress[] ccAddress = new InternetAddress[cc5.length];
	            
	            // To get the array of ccaddresses
	            for( int i = 0; i < cc5.length; i++ ) {
	                ccAddress[i] = new InternetAddress(cc5[i]);
	            }
	            
	            // Set cc: header field of the header.
	            for( int i = 0; i < ccAddress.length; i++) {
	                message.addRecipient(Message.RecipientType.CC, ccAddress[i]);
	            }
			          
			}else if(get_emailSize()==6){
				 
	            InternetAddress[] ccAddress = new InternetAddress[cc6.length];
	            
	            // To get the array of ccaddresses
	            for( int i = 0; i < cc6.length; i++ ) {
	                ccAddress[i] = new InternetAddress(cc6[i]);
	            }
	            
	            // Set cc: header field of the header.
	            for( int i = 0; i < ccAddress.length; i++) {
	                message.addRecipient(Message.RecipientType.CC, ccAddress[i]);
	            }
			          
				
			}else if(get_emailSize()==7){

				 
	            InternetAddress[] ccAddress = new InternetAddress[cc7.length];
	            
	            // To get the array of ccaddresses
	            for( int i = 0; i < cc7.length; i++ ) {
	                ccAddress[i] = new InternetAddress(cc7[i]);
	            }
	            
	            // Set cc: header field of the header.
	            for( int i = 0; i < ccAddress.length; i++) {
	                message.addRecipient(Message.RecipientType.CC, ccAddress[i]);
	            }
			          
				
			}else if(get_emailSize()==8){

				 
	            InternetAddress[] ccAddress = new InternetAddress[cc8.length];
	            
	            // To get the array of ccaddresses
	            for( int i = 0; i < cc8.length; i++ ) {
	                ccAddress[i] = new InternetAddress(cc8[i]);
	            }
	            
	            // Set cc: header field of the header.
	            for( int i = 0; i < ccAddress.length; i++) {
	                message.addRecipient(Message.RecipientType.CC, ccAddress[i]);
	            }
			          
				
			}else if(get_emailSize()==9){

				 
	            InternetAddress[] ccAddress = new InternetAddress[cc9.length];
	            
	            // To get the array of ccaddresses
	            for( int i = 0; i < cc9.length; i++ ) {
	                ccAddress[i] = new InternetAddress(cc9[i]);
	            }
	            
	            // Set cc: header field of the header.
	            for( int i = 0; i < ccAddress.length; i++) {
	                message.addRecipient(Message.RecipientType.CC, ccAddress[i]);
	            }
			          
				
			}else if(get_emailSize()==10){

				 
	            InternetAddress[] ccAddress = new InternetAddress[cc10.length];
	            
	            // To get the array of ccaddresses
	            for( int i = 0; i < cc10.length; i++ ) {
	                ccAddress[i] = new InternetAddress(cc10[i]);
	            }
	            
	            // Set cc: header field of the header.
	            for( int i = 0; i < ccAddress.length; i++) {
	                message.addRecipient(Message.RecipientType.CC, ccAddress[i]);
	            }
			          
				
			}else if(get_emailSize()==11){

				 
	            InternetAddress[] ccAddress = new InternetAddress[cc11.length];
	            
	            // To get the array of ccaddresses
	            for( int i = 0; i < cc11.length; i++ ) {
	                ccAddress[i] = new InternetAddress(cc11[i]);
	            }
	            
	            // Set cc: header field of the header.
	            for( int i = 0; i < ccAddress.length; i++) {
	                message.addRecipient(Message.RecipientType.CC, ccAddress[i]);
	            }
			          
				
			}else if(get_emailSize()==12){

				 
	            InternetAddress[] ccAddress = new InternetAddress[cc12.length];
	            
	            // To get the array of ccaddresses
	            for( int i = 0; i < cc12.length; i++ ) {
	                ccAddress[i] = new InternetAddress(cc12[i]);
	            }
	            
	            // Set cc: header field of the header.
	            for( int i = 0; i < ccAddress.length; i++) {
	                message.addRecipient(Message.RecipientType.CC, ccAddress[i]);
	            }
			          
				
			}


			Transport.send(message);
			System.out.println("Done");

		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	
	
	}
	
	public void sendBookingVoucher(){



		try {
			
			final String username = "reservations@ulohotels.com";
			final String password = "Ulohotels@1234";
			
//			final String username = "techsupport@ulohotels.com";
//			final String password = "Ulo#2017";
			
			String  from = _from;
			String  to=null;
			if(get_to()!=null){
				to= _to;
			}else{
				to= emailArray[0];	
			}
		    String subject = _subject;
		    Content content = new Content("text/html", _bodyContent.toString());
		    //InternetAddress[] myCcList = InternetAddress.parse("NEHA.SIVA@xyz.com");
		   		    
			Properties props = new Properties();
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.smtp.host", "smtp.gmail.com");
			props.put("mail.smtp.port", "587");
//			props.put("mail.debug", "true");

			Session session = Session.getInstance(props,
			  new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(username, password);
				}
			  });
            
			
		    //Mail mail = new Mail(from, subject, to, content);
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(from));
			message.setRecipients(Message.RecipientType.TO,
				InternetAddress.parse(to));
			message.setSubject(subject);
			message.setContent(_bodyContent.toString(), "text/html; charset=utf-8");
			
            
            this.emailArray=getEmailArray();
			InternetAddress[] ccAddress = new InternetAddress[emailArray.length];
            
            // To get the array of ccaddresses
            for( int i = 0; i < emailArray.length; i++ ) {
            	ccAddress[i] = new InternetAddress(emailArray[i]);	
            }
            // Set cc: header field of the header.
            for( int i = 0; i < ccAddress.length; i++) {
                message.addRecipient(Message.RecipientType.CC, ccAddress[i]);
            }
            
			Transport.send(message);
			System.out.println("Done");

		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	
	
	
	}
	public void send() {
		try {
			
			final String username = "reservations@ulohotels.com";
			final String password = "Ulohotels@1234";
			
			//final String username = "techsupport@ulohotels.com";
			//final String password = "Ulo#2017";
			
			String  from = _from;
			String  to = _to;
		    String subject = _subject;
		    Content content = new Content("text/html", _bodyContent.toString());
		    //InternetAddress[] myCcList = InternetAddress.parse("NEHA.SIVA@xyz.com");
		    String[] cc={ _cc,_cc2,_cc3,_cc4,_cc5,_cc6,_cc7,_cc8,_cc9,_cc10,_cc11,_cc12,_cc13,_cc14 };
		    
		      
		     
			Properties props = new Properties();
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.smtp.host", "smtp.gmail.com");
			props.put("mail.smtp.port", "587");
//			props.put("mail.debug", "true");

			Session session = Session.getInstance(props,
			  new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(username, password);
				}
			  });
            
			
		    //Mail mail = new Mail(from, subject, to, content);
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(from));
			message.setRecipients(Message.RecipientType.TO,
				InternetAddress.parse(to));
			message.setSubject(subject);
			message.setContent(_bodyContent.toString(), "text/html; charset=utf-8");
			 if((get_cc() != null) && (get_cc2() != null) && (get_cc3() != null) && (get_cc4() != null) && (get_cc5() != null) && (get_cc6() != null) && (get_cc7() != null) && (get_cc8() != null) && (get_cc9() != null) && (get_cc10() != null) && (get_cc11() != null) && (get_cc12() != null) && (get_cc13() != null) && (get_cc14() != null)){
				 
            InternetAddress[] ccAddress = new InternetAddress[cc.length];
            
            // To get the array of ccaddresses
            for( int i = 0; i < cc.length; i++ ) {
                ccAddress[i] = new InternetAddress(cc[i]);
            }
            
            // Set cc: header field of the header.
            for( int i = 0; i < ccAddress.length; i++) {
                message.addRecipient(Message.RecipientType.CC, ccAddress[i]);
            }
		          
			}

			Transport.send(message);
			System.out.println("Done");

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}
	
	public void sendPayment() {
		try {

			final String username = "finance@ulohotels.com";
			final String password = "Accounts@1225";			
			
//			final String username = "techsupport@ulohotels.com";
//			final String password = "Ulo#2017";
			
			String  from = _from;
			String  to=null;
			if(get_to()!=null){
				to= _to;
			}else{
				to= emailArray[0];	
			}
			
		    String subject = _subject;
		    Content content = new Content("text/html", _bodyContent.toString());
		    //InternetAddress[] myCcList = InternetAddress.parse("NEHA.SIVA@xyz.com");
		    
			Properties props = new Properties();
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.smtp.host", "smtp.gmail.com");
			props.put("mail.smtp.port", "587");
//			props.put("mail.debug", "true");

			Session session = Session.getInstance(props,
			  new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(username, password);
				}
			  });
            
			String filename = get_fileName();
		    //Mail mail = new Mail(from, subject, to, content);
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(from));
			message.setRecipients(Message.RecipientType.TO,
				InternetAddress.parse(to));
			message.setSubject(subject);
			message.setContent(_bodyContent.toString(), "text/html; charset=utf-8");
			

			 
            InternetAddress[] ccAddress = new InternetAddress[emailArray.length];
            
            // To get the array of ccaddresses
            for( int i = 0; i < emailArray.length; i++ ) {
                ccAddress[i] = new InternetAddress(emailArray[i]);
            }
            
            // Set cc: header field of the header.
            for( int i = 0; i < ccAddress.length; i++) {
                message.addRecipient(Message.RecipientType.CC, ccAddress[i]);
            }
            Multipart multipart = new MimeMultipart();
	           
            MimeBodyPart textPart = new MimeBodyPart();
            textPart.setText(_bodyContent.toString(), "utf-8");

            MimeBodyPart htmlPart = new MimeBodyPart();
            htmlPart.setContent(_bodyContent.toString(), "text/html; charset=utf-8");
            
			MimeBodyPart attachPart = new MimeBodyPart();
			DataSource source = new FileDataSource(filename);
		    attachPart.setDataHandler(new DataHandler(source));
		    attachPart.setFileName(new File(get_fileName()).getName());
			attachPart.setHeader("Content-Type", "application/PDF; Content-Disposition: attachment; charset=\"us-ascii\"; name=\"'"+get_fileName()+"'\"");
			multipart.addBodyPart(attachPart);
			
            multipart.addBodyPart(htmlPart); // <-- second
			message.setContent(multipart);      
		
			
			Transport.send(message);
			System.out.println("Done");

		
					
			
/*			  // Create the message part
	          BodyPart messageBodyPart = new MimeBodyPart();
	          messageBodyPart.setText("This is message body from"+fromDate+"to"+toDate+"payment details below attachment");
	          // Create a multipar message
	          MimeBodyPart messageBodyPart2 = new MimeBodyPart(); 
	          String filename = filePath;
		      DataSource source = new FileDataSource(filename);
		      messageBodyPart2.setDataHandler(new DataHandler(source));
		      messageBodyPart2.setFileName(filename);
		      
	          Multipart multipart = new MimeMultipart();
			  //messageBodyPart = new MimeBodyPart();
		      multipart.addBodyPart(messageBodyPart);
		      multipart.addBodyPart(messageBodyPart2);
		      message.setContent(multipart);

			Transport.send(message);
			System.out.println("Done");
*/
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}
	
	public void sendPartnerBlock() {
		try {
			
			final String username = "reservations@ulohotels.com";
			final String password = "Ulohotels@1234";
			
			//final String username = "techsupport@ulohotels.com";
			//final String password = "Ulo#2017";
			
			String  from = _from;
			String  to = _to;
		    String subject = _subject;
		    Content content = new Content("text/html", _bodyContent.toString());
		    //InternetAddress[] myCcList = InternetAddress.parse("NEHA.SIVA@xyz.com");
		    String[] cc={ _cc,_cc2,_cc3,_cc4,_cc5,_cc6,_cc7,_cc8 };
		    
		      
		     
			Properties props = new Properties();
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.smtp.host", "smtp.gmail.com");
			props.put("mail.smtp.port", "587");
//			props.put("mail.debug", "true");

			Session session = Session.getInstance(props,
			  new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(username, password);
				}
			  });
            
			
		    //Mail mail = new Mail(from, subject, to, content);
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(from));
			message.setRecipients(Message.RecipientType.TO,
				InternetAddress.parse(to));
			message.setSubject(subject);
			message.setContent(_bodyContent.toString(), "text/html; charset=utf-8");
			 if((get_cc() != null) && (get_cc2() != null) && (get_cc3() != null) && (get_cc4() != null) && (get_cc5() != null) && (get_cc6() != null) && (get_cc7() != null) && (get_cc8() != null)){
				 
            InternetAddress[] ccAddress = new InternetAddress[cc.length];
            
            // To get the array of ccaddresses
            for( int i = 0; i < cc.length; i++ ) {
                ccAddress[i] = new InternetAddress(cc[i]);
            }
            
            // Set cc: header field of the header.
            for( int i = 0; i < ccAddress.length; i++) {
                message.addRecipient(Message.RecipientType.CC, ccAddress[i]);
            }
		          
			}

			Transport.send(message);
			System.out.println("Done");

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}
	
	public void sendPartnerNotification() {
		try {

			
			final String username = "reservations@ulohotels.com";
			final String password = "Ulohotels@1234";
			
//			final String username = "techsupport@ulohotels.com";
//			final String password = "Ulo#2017";
			
			String  from = _from;
			String  to = _to;
		    String subject = _subject;
		    Content content = new Content("text/html", _bodyContent.toString());
		    //InternetAddress[] myCcList = InternetAddress.parse("NEHA.SIVA@xyz.com");
		    String[] cc14={ _cc,_cc2,_cc3,_cc4,_cc5,_cc6,_cc7,_cc8,_cc9,_cc10,_cc11,_cc12,_cc13,_cc14 };
		    String[] cc4={ _cc,_cc2,_cc3,_cc4 };
		    String[] cc5={ _cc,_cc2,_cc3,_cc4,_cc5 };
		    String[] cc6={ _cc,_cc2,_cc3,_cc4,_cc5,_cc6 };
		    String[] cc7={ _cc,_cc2,_cc3,_cc4,_cc5,_cc6,_cc7 };
		    String[] cc8={ _cc,_cc2,_cc3,_cc4,_cc5,_cc6,_cc7,_cc8};  
		    String[] cc9={ _cc,_cc2,_cc3,_cc4,_cc5,_cc6,_cc7,_cc8,_cc9};
		    String[] cc10={ _cc,_cc2,_cc3,_cc4,_cc5,_cc6,_cc7,_cc8,_cc9,_cc10};
		    String[] cc11={ _cc,_cc2,_cc3,_cc4,_cc5,_cc6,_cc7,_cc8,_cc9,_cc10,_cc11};
		    String[] cc12={ _cc,_cc2,_cc3,_cc4,_cc5,_cc6,_cc7,_cc8,_cc9,_cc10,_cc11,_cc12};
		    
			Properties props = new Properties();
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.smtp.host", "smtp.gmail.com");
			props.put("mail.smtp.port", "587");
//			props.put("mail.debug", "true");

			Session session = Session.getInstance(props,
			  new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(username, password);
				}
			  });
            
			
		    //Mail mail = new Mail(from, subject, to, content);
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(from));
			message.setRecipients(Message.RecipientType.TO,
				InternetAddress.parse(to));
			message.setSubject(subject);
			message.setContent(_bodyContent.toString(), "text/html; charset=utf-8");
			
			if(get_emailSize()==4){
				 
	            InternetAddress[] ccAddress = new InternetAddress[cc4.length];
	            
	            // To get the array of ccaddresses
	            for( int i = 0; i < cc4.length; i++ ) {
	                ccAddress[i] = new InternetAddress(cc4[i]);
	            }
	            
	            // Set cc: header field of the header.
	            for( int i = 0; i < ccAddress.length; i++) {
	                message.addRecipient(Message.RecipientType.CC, ccAddress[i]);
	            }
			          
			}else if(get_emailSize()==5){
				 
	            InternetAddress[] ccAddress = new InternetAddress[cc5.length];
	            
	            // To get the array of ccaddresses
	            for( int i = 0; i < cc5.length; i++ ) {
	                ccAddress[i] = new InternetAddress(cc5[i]);
	            }
	            
	            // Set cc: header field of the header.
	            for( int i = 0; i < ccAddress.length; i++) {
	                message.addRecipient(Message.RecipientType.CC, ccAddress[i]);
	            }
			          
			}else if(get_emailSize()==6){
				 
	            InternetAddress[] ccAddress = new InternetAddress[cc6.length];
	            
	            // To get the array of ccaddresses
	            for( int i = 0; i < cc6.length; i++ ) {
	                ccAddress[i] = new InternetAddress(cc6[i]);
	            }
	            
	            // Set cc: header field of the header.
	            for( int i = 0; i < ccAddress.length; i++) {
	                message.addRecipient(Message.RecipientType.CC, ccAddress[i]);
	            }
			          
				
			}else if(get_emailSize()==7){

				 
	            InternetAddress[] ccAddress = new InternetAddress[cc7.length];
	            
	            // To get the array of ccaddresses
	            for( int i = 0; i < cc7.length; i++ ) {
	                ccAddress[i] = new InternetAddress(cc7[i]);
	            }
	            
	            // Set cc: header field of the header.
	            for( int i = 0; i < ccAddress.length; i++) {
	                message.addRecipient(Message.RecipientType.CC, ccAddress[i]);
	            }
			          
				
			}else if(get_emailSize()==8){

				 
	            InternetAddress[] ccAddress = new InternetAddress[cc8.length];
	            
	            // To get the array of ccaddresses
	            for( int i = 0; i < cc8.length; i++ ) {
	                ccAddress[i] = new InternetAddress(cc8[i]);
	            }
	            
	            // Set cc: header field of the header.
	            for( int i = 0; i < ccAddress.length; i++) {
	                message.addRecipient(Message.RecipientType.CC, ccAddress[i]);
	            }
			          
				
			}else if(get_emailSize()==9){

				 
	            InternetAddress[] ccAddress = new InternetAddress[cc9.length];
	            
	            // To get the array of ccaddresses
	            for( int i = 0; i < cc9.length; i++ ) {
	                ccAddress[i] = new InternetAddress(cc9[i]);
	            }
	            
	            // Set cc: header field of the header.
	            for( int i = 0; i < ccAddress.length; i++) {
	                message.addRecipient(Message.RecipientType.CC, ccAddress[i]);
	            }
			          
				
			}else if(get_emailSize()==10){

				 
	            InternetAddress[] ccAddress = new InternetAddress[cc10.length];
	            
	            // To get the array of ccaddresses
	            for( int i = 0; i < cc10.length; i++ ) {
	                ccAddress[i] = new InternetAddress(cc10[i]);
	            }
	            
	            // Set cc: header field of the header.
	            for( int i = 0; i < ccAddress.length; i++) {
	                message.addRecipient(Message.RecipientType.CC, ccAddress[i]);
	            }
			          
				
			}else if(get_emailSize()==11){

				 
	            InternetAddress[] ccAddress = new InternetAddress[cc11.length];
	            
	            // To get the array of ccaddresses
	            for( int i = 0; i < cc11.length; i++ ) {
	                ccAddress[i] = new InternetAddress(cc11[i]);
	            }
	            
	            // Set cc: header field of the header.
	            for( int i = 0; i < ccAddress.length; i++) {
	                message.addRecipient(Message.RecipientType.CC, ccAddress[i]);
	            }
			          
				
			}else if(get_emailSize()==12){

				 
	            InternetAddress[] ccAddress = new InternetAddress[cc12.length];
	            
	            // To get the array of ccaddresses
	            for( int i = 0; i < cc12.length; i++ ) {
	                ccAddress[i] = new InternetAddress(cc12[i]);
	            }
	            
	            // Set cc: header field of the header.
	            for( int i = 0; i < ccAddress.length; i++) {
	                message.addRecipient(Message.RecipientType.CC, ccAddress[i]);
	            }
			          
				
			}


			Transport.send(message);
			System.out.println("Done");

		
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		} catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void sendPartnerVouchers(){

		try {

			
			final String username = "reservations@ulohotels.com";
			final String password = "Ulohotels@1234";
			
//			final String username = "techsupport@ulohotels.com";
//			final String password = "Ulo#2017";
			
			String  from = _from;
			String  to=null;
			if(get_to()!=null){
				to= _to;
			}else{
				to= emailArray[0];	
			}
		    String subject = _subject;
		    Content content = new Content("text/html", _bodyContent.toString());
		    //InternetAddress[] myCcList = InternetAddress.parse("NEHA.SIVA@xyz.com");
		    
			Properties props = new Properties();
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.smtp.host", "smtp.gmail.com");
			props.put("mail.smtp.port", "587");
//			props.put("mail.debug", "true");

			Session session = Session.getInstance(props,
			  new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(username, password);
				}
			  });
            
			
		    //Mail mail = new Mail(from, subject, to, content);
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(from));
			message.setRecipients(Message.RecipientType.TO,
				InternetAddress.parse(to));
			message.setSubject(subject);
			message.setContent(_bodyContent.toString(), "text/html; charset=utf-8");
			this.emailArray=getEmailArray();
			InternetAddress[] ccAddress = new InternetAddress[emailArray.length];
            
            // To get the array of ccaddresses
            for( int i = 0; i < emailArray.length; i++ ) {
            	ccAddress[i] = new InternetAddress(emailArray[i]);	
            }
            // Set cc: header field of the header.
            for( int i = 0; i < ccAddress.length; i++) {
                message.addRecipient(Message.RecipientType.CC, ccAddress[i]);
            }
            
			Transport.send(message);
			System.out.println("Done");

		
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		} catch(Exception e){
			e.printStackTrace();
		}
	
	}
	public void sendPartnerWithUs() {
		try {
			

			final String username = "reservations@ulohotels.com";
			final String password = "Ulohotels@1234";
			
			//final String username = "techsupport@ulohotels.com";
			//final String password = "Ulo#2017";
			
			String  from = _from;
			String  to = _to;
		    String subject = _subject;
		    Content content = new Content("text/html", _bodyContent.toString());
		    //InternetAddress[] myCcList = InternetAddress.parse("NEHA.SIVA@xyz.com");
		    String[] cc={ _cc,_cc2};
		    
		      
		     
			Properties props = new Properties();
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.smtp.host", "smtp.gmail.com");
			props.put("mail.smtp.port", "587");
//			props.put("mail.debug", "true");

			Session session = Session.getInstance(props,
			  new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(username, password);
				}
			  });
            
			
		    //Mail mail = new Mail(from, subject, to, content);
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(from));
			message.setRecipients(Message.RecipientType.TO,
				InternetAddress.parse(to));
			message.setSubject(subject);
			message.setContent(_bodyContent.toString(), "text/html; charset=utf-8");
			 if((get_cc() != null) && (get_cc2() != null)){
				 
            InternetAddress[] ccAddress = new InternetAddress[cc.length];
            
            // To get the array of ccaddresses
            for( int i = 0; i < cc.length; i++ ) {
                ccAddress[i] = new InternetAddress(cc[i]);
            }
            
            // Set cc: header field of the header.
            for( int i = 0; i < ccAddress.length; i++) {
                message.addRecipient(Message.RecipientType.CC, ccAddress[i]);
            }
		          
			}

			Transport.send(message);

			System.out.println("Done");

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}
	
	public void sendVoucher() {
		try {
			
			final String username = "finance@ulohotels.com";
			final String password = "Accounts@1225";
			

			//final String username = "techsupport@ulohotels.com";
			//final String password = "Ulo#2017";
			
			String  from = _from;
			String  to = _to;
		    String subject = _subject;
		    Content content = new Content("text/html", _bodyContent.toString());
		    //InternetAddress[] myCcList = InternetAddress.parse("NEHA.SIVA@xyz.com");
		    String[] cc={ _cc,_cc2,_cc3,_cc4,_cc5,_cc6 };
		    
		      
		     
			Properties props = new Properties();
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.smtp.host", "smtp.gmail.com");
			props.put("mail.smtp.port", "587");
//			props.put("mail.debug", "true");

			Session session = Session.getInstance(props,
			  new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(username, password);
				}
			  });
            
			
		    //Mail mail = new Mail(from, subject, to, content);
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(from));
			message.setRecipients(Message.RecipientType.TO,
				InternetAddress.parse(to));
			message.setSubject(subject);
			
			
			message.setContent(_bodyContent.toString(), "text/html; charset=utf-8");
			 if((get_cc() != null) && (get_cc2() != null) && (get_cc3() != null) && (get_cc4() != null) && (get_cc5() != null) && (get_cc6() != null)){
				 
	            InternetAddress[] ccAddress = new InternetAddress[cc.length];
	            
	            // To get the array of ccaddresses
	            for( int i = 0; i < cc.length; i++ ) {
	                ccAddress[i] = new InternetAddress(cc[i]);
	            }
	            
	            // Set cc: header field of the header.
	            for( int i = 0; i < ccAddress.length; i++) {
	                message.addRecipient(Message.RecipientType.CC, ccAddress[i]);
	            }
	            Multipart multipart = new MimeMultipart();
		           
	            MimeBodyPart textPart = new MimeBodyPart();
	            textPart.setText(_bodyContent.toString(), "utf-8");

	            MimeBodyPart htmlPart = new MimeBodyPart();
	            htmlPart.setContent(_bodyContent.toString(), "text/html; charset=utf-8");
	            
	            /*MimeBodyPart messageBodyPart = new MimeBodyPart();
	            messageBodyPart.setText(_bodyContent.toString());
	            multipart.addBodyPart(messageBodyPart);*/
				
				/*DataSource source = new FileDataSource(_fileName);
				messageBodyPart.setDataHandler(new DataHandler(source));
				messageBodyPart.setFileName(new File(_fileName).getName());*/
				
				MimeBodyPart attachPart = new MimeBodyPart();
				attachPart.attachFile(get_fileName());
				attachPart.setHeader("Content-Type", "application/PDF; Content-Disposition: attachment; charset=\"us-ascii\"; name=\"'"+get_fileName()+"'\"");
				multipart.addBodyPart(attachPart);
				//
	           // multipart.addBodyPart(textPart); // <-- first
	            multipart.addBodyPart(htmlPart); // <-- second
				message.setContent(multipart);
				
		          
			}

			 
			Transport.send(message);

			System.out.println("Done");

		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void sendCanceledRequest(){

		try {
			

			final String username = "reservations@ulohotels.com";
			final String password = "Ulohotels@1234";
			

			//final String username = "techsupport@ulohotels.com";
			//final String password = "Ulo#2017";
			
			String  from = _from;
			String  to = _to;
		    String subject = _subject;
		    Content content = new Content("text/html", _bodyContent.toString());
		    //InternetAddress[] myCcList = InternetAddress.parse("NEHA.SIVA@xyz.com");
		    String[] cc={ _cc,_cc2,_cc3,_cc4,_cc5,_cc6 };
		    
		      
		     
			Properties props = new Properties();
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.smtp.host", "smtp.gmail.com");
			props.put("mail.smtp.port", "587");
//			props.put("mail.debug", "true");

			Session session = Session.getInstance(props,
			  new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(username, password);
				}
			  });
            
			
		    //Mail mail = new Mail(from, subject, to, content);
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(from));
			message.setRecipients(Message.RecipientType.TO,
			InternetAddress.parse(to));
			message.setSubject(subject);
			
			message.setContent(_bodyContent.toString(), "text/html; charset=utf-8");
			 if((get_cc() != null) && (get_cc2() != null) && (get_cc3() != null) && (get_cc4() != null) && (get_cc5() != null) && (get_cc6() != null)){
				 
	            InternetAddress[] ccAddress = new InternetAddress[cc.length];
	            
	            // To get the array of ccaddresses
	            for( int i = 0; i < cc.length; i++ ) {
	                ccAddress[i] = new InternetAddress(cc[i]);
	            }
	            
	            // Set cc: header field of the header.
	            for( int i = 0; i < ccAddress.length; i++) {
	                message.addRecipient(Message.RecipientType.CC, ccAddress[i]);
	            }
			}

			 
			Transport.send(message);

			System.out.println("Done");

		}catch(Exception e){
			e.printStackTrace();
		}
	
	}
	
	public void sendResumes(){

		try {

			final String username = "reservations@ulohotels.com";
			final String password = "Ulohotels@1234";
			
			//final String username = "techsupport@ulohotels.com";
			//final String password = "Ulo#2017";
			
			String  from = _from;
			String  to = _to;
		    String subject = _subject;
//		    Content content = new Content("text/html", _bodyContent.toString());
		    //InternetAddress[] myCcList = InternetAddress.parse("NEHA.SIVA@xyz.com");
		    //String[] cc={ _cc,_cc2,_cc3,_cc4,_cc5,_cc6,_cc7,_cc8 };
		    
		      
		     
			Properties props = new Properties();
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.smtp.host", "smtp.gmail.com");
			props.put("mail.smtp.port", "587");
//			props.put("mail.debug", "true");

			Session session = Session.getInstance(props,
			  new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(username, password);
				}
			  });
            
			
		    //Mail mail = new Mail(from, subject, to, content);
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(from));
			message.setRecipients(Message.RecipientType.TO,
				InternetAddress.parse(to));
			message.setSubject(subject);
//			message.setContent(_bodyContent.toString(), "text/html; charset=utf-8");
			
			MimeBodyPart textPart = new MimeBodyPart();
            textPart.setText(_bodyContent.toString(), "utf-8");

            MimeBodyPart htmlPart = new MimeBodyPart();
            htmlPart.setContent(_bodyContent.toString(), "text/html; charset=utf-8");
            
            
			Multipart multipart = new MimeMultipart();
			MimeBodyPart attachPart = new MimeBodyPart();
			DataSource source = new FileDataSource(get_fileName());
		    attachPart.setDataHandler(new DataHandler(source));
		    attachPart.setFileName(new File(get_fileName()).getName());
		    
			/*File attachFile=new File(new File(get_fileName()),"resume.doc");
			attachPart.attachFile(attachFile);*/
		    
			attachPart.setHeader("Content-Type", "application/word; Content-Disposition: attachment; charset=\"us-ascii\"; name=\"'"+get_fileName()+"'\"");
			multipart.addBodyPart(attachPart);
			multipart.addBodyPart(htmlPart);
			message.setContent(multipart);
			Transport.send(message);
			System.out.println("Done");

		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public void sendCorporate(){

		try {
			
			final String username = "reservations@ulohotels.com";
			final String password = "Ulohotels@1234";
			
			//final String username = "techsupport@ulohotels.com";
			//final String password = "Ulo#2017";
			
			String  from = _from;
			String  to = _to;
		    String subject = _subject;
		    Content content = new Content("text/html", _bodyContent.toString());
		    //InternetAddress[] myCcList = InternetAddress.parse("NEHA.SIVA@xyz.com");
		    String[] cc={ _cc,_cc2};
		    
		      
		     
			Properties props = new Properties();
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.smtp.host", "smtp.gmail.com");
			props.put("mail.smtp.port", "587");
//			props.put("mail.debug", "true");

			Session session = Session.getInstance(props,
			  new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(username, password);
				}
			  });
            
			
		    //Mail mail = new Mail(from, subject, to, content);
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(from));
			message.setRecipients(Message.RecipientType.TO,
				InternetAddress.parse(to));
			message.setSubject(subject);
			InternetAddress[] ccAddress = new InternetAddress[cc.length];
            
            // To get the array of ccaddresses
            for( int i = 0; i < cc.length; i++ ) {
                ccAddress[i] = new InternetAddress(cc[i]);
            }
            
            // Set cc: header field of the header.
            for( int i = 0; i < ccAddress.length; i++) {
                message.addRecipient(Message.RecipientType.CC, ccAddress[i]);
            }
			message.setContent(_bodyContent.toString(), "text/html; charset=utf-8");
			Transport.send(message);
			System.out.println("Done");

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	
	}
	public String get_to() {
		return _to;
	}

	public void set_to(String _to) {
		this._to = _to;
	}

	public String get_from() {
		return _from;
	}

	public void set_from(String _from) {
		this._from = _from;
	}

	public String get_username() {
		return _username;
	}

	public void set_username(String _username) {
		this._username = _username;
	}

	public String get_password() {
		return _password;
	}

	public void set_password(String _password) {
		this._password = _password;
	}

	public String get_host() {
		return _host;
	}

	public void set_host(String _host) {
		this._host = _host;
	}

	public String get_port() {
		return _port;
	}

	public void set_port(String _port) {
		this._port = _port;
	}

	public String get_subject() {
		return _subject;
	}

	public void set_subject(String _subject) {
		this._subject = _subject;
	}

	public String get_body() {
		return _body;
	}

	public void set_body(String _body) {
		this._body = _body;
	}
	
	public Writer get_bodyContent() {
		return _bodyContent;
	}
	public void set_bodyContent(Writer _bodyContent) {
		this._bodyContent = _bodyContent;
	}
	public String get_cc() {
		return _cc;
	}
	public void set_cc(String _cc) {
		this._cc = _cc;
	}
	
	public String get_cc2() {
		return _cc2;
	}
	public void set_cc2(String _cc2) {
		this._cc2 = _cc2;
	}
	
	public String get_cc3() {
		return _cc3;
	}
	public void set_cc3(String _cc3) {
		this._cc3 = _cc3;
	}
	public String get_cc4() {
		return _cc4;
	}
	public void set_cc4(String _cc4) {
		this._cc4 = _cc4;
	}
	
	public String get_cc5() {
		return _cc5;
	}
	public void set_cc5(String _cc5) {
		this._cc5 = _cc5;
	}
	public String get_cc6() {
		return _cc6;
	}
	public void set_cc6(String _cc6) {
		this._cc6 = _cc6;
	}
	public String get_cc7() {
		return _cc7;
	}
	public void set_cc7(String _cc7) {
		this._cc7 = _cc7;
	}
	public String get_cc8() {
		return _cc8;
	}
	public void set_cc8(String _cc8) {
		this._cc8 = _cc8;
	}
	
	public String get_cc9() {
		return _cc9;
	}
	public void set_cc9(String _cc9) {
		this._cc9 = _cc9;
	}
	public String get_cc10() {
		return _cc10;
	}
	public void set_cc10(String _cc10) {
		this._cc10 = _cc10;
	}
	public String get_cc11() {
		return _cc11;
	}
	public void set_cc11(String _cc11) {
		this._cc11 = _cc11;
	}
	public String get_cc12() {
		return _cc12;
	}
	public void set_cc12(String _cc12) {
		this._cc12 = _cc12;
	}
	public String get_cc13() {
		return _cc13;
	}
	public void set_cc13(String _cc13) {
		this._cc13 = _cc13;
	}
	public String get_cc14() {
		return _cc14;
	}
	public void set_cc14(String _cc14) {
		this._cc14 = _cc14;
	}
	public String get_fileName() {
		return _fileName;
	}
	public void set_fileName(String _filename) {
		this._fileName = _filename;
	}
	public Integer get_emailSize() {
		return _emailSize;
	}
	public void set_emailSize(Integer _emailSize) {
		this._emailSize = _emailSize;
	}
	public String[] getEmailArray() {
		return emailArray;
	}
	public void setEmailArray(String[] emailArray) {
		this.emailArray = emailArray;
	}
	
}

