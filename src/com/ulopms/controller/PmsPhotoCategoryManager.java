package com.ulopms.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;

import com.ulopms.model.PmsPhotoCategory;
import com.ulopms.util.HibernateUtil;


public class PmsPhotoCategoryManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(PmsPhotoCategoryManager.class);

	public PmsPhotoCategory add(PmsPhotoCategory PmsSource) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(PmsSource);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return PmsSource;
	}

	public PmsPhotoCategory edit(PmsPhotoCategory PmsSource) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(PmsSource);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return PmsSource;
	}

	public PmsPhotoCategory delete(int id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		PmsPhotoCategory PmsSource = (PmsPhotoCategory) session.load(PmsPhotoCategory.class, id);
		if (null != PmsSource) {
			session.delete(PmsSource);
		}
		session.getTransaction().commit();
		return PmsSource;
	}

	public PmsPhotoCategory find(int id) {
		PmsPhotoCategory PmsSource = new PmsPhotoCategory();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			PmsSource = (PmsPhotoCategory) session.get(PmsPhotoCategory.class, id);
			// session.getTransaction().commit();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return PmsSource;
	}

	@SuppressWarnings("unchecked")
	public List<PmsPhotoCategory> list() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsPhotoCategory> PmsPhotoCategory = null;
		try {

			PmsPhotoCategory = (List<PmsPhotoCategory>) session.createQuery("from PmsPhotoCategory where isActive = 'true' and isDeleted = 'false' and accommodationTagIsActive = 'false'").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return PmsPhotoCategory;
	}
	
	@SuppressWarnings("unchecked")
	public List<PmsPhotoCategory> listRoomTag() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsPhotoCategory> PmsPhotoCategory = null;
		try {

			PmsPhotoCategory = (List<PmsPhotoCategory>) session.createQuery("from PmsPhotoCategory where isActive = 'true' and isDeleted = 'false' and accommodationTagIsActive = 'true'").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return PmsPhotoCategory;
	}

	
}
