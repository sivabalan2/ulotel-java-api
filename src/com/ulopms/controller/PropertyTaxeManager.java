package com.ulopms.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;

import com.ulopms.model.PropertyTaxe;
import com.ulopms.util.HibernateUtil;


public class PropertyTaxeManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(PropertyTaxeManager.class);

	public PropertyTaxe add(PropertyTaxe propertyTaxe) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(propertyTaxe);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyTaxe;
	}

	public PropertyTaxe edit(PropertyTaxe propertyTaxe) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(propertyTaxe);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyTaxe;
	}

	public PropertyTaxe delete(Long id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		PropertyTaxe propertyTaxe = (PropertyTaxe) session.load(PropertyTaxe.class, id);
		if (null != propertyTaxe) {
			session.delete(propertyTaxe);
		}
		session.getTransaction().commit();
		return propertyTaxe;
	}

	public PropertyTaxe find(Integer id) {
		PropertyTaxe propertyTaxe = new PropertyTaxe();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			propertyTaxe = (PropertyTaxe) session.load(PropertyTaxe.class, id);
			// session.getTransaction().commit();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return propertyTaxe;
	}
	
	

   public PropertyTaxe find(double amount) {
		PropertyTaxe propertyTaxe = new PropertyTaxe();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			propertyTaxe = (PropertyTaxe) session.createQuery("from PropertyTaxe where isActive = 'true' and isDeleted = 'false' and  :amount between taxAmountFrom and taxAmountTo")
            		.setParameter("amount", amount).uniqueResult();
			// session.getTransaction().commit();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return propertyTaxe;
	}

	
	
	public PropertyTaxe findAccommodationTax(Integer accommodationId) {

		PropertyTaxe propertyTaxe = new PropertyTaxe();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();

        try {
        	session.beginTransaction();
            //propertyTaxe = (PropertyTaxe) session.load(PropertyTaxe.class, accommodationId);
        	propertyTaxe = (PropertyTaxe) session.createQuery("from PropertyTaxe where propertyAccommodation.accommodationId=:accommodationId")
            		.setParameter("accommodationId", accommodationId).uniqueResult();
            // session.getTransaction().commit();
        } 
        catch (Exception e1) {
            logger.error(e1);
       } finally {
            session = null;
        }
       return propertyTaxe;
    }
	
	@SuppressWarnings("unchecked")
	public List<PropertyTaxe> list() {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyTaxe> propertyTaxes = null;
		try {  

			propertyTaxes = (List<PropertyTaxe>) session.createQuery("from PropertyTaxe where isActive = 'true' and isDeleted = 'false'").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyTaxes;
	}

	
}
