package com.ulopms.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;

import com.ulopms.model.LocationPhoto;
import com.ulopms.util.HibernateUtil;


public class LocationPhotoManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(LocationPhotoManager.class);

	public LocationPhoto add(LocationPhoto locationPhoto) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(locationPhoto);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return locationPhoto;
	}

	public LocationPhoto edit(LocationPhoto locationPhoto) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(locationPhoto);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return locationPhoto;
	}

	public LocationPhoto delete(Long id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		LocationPhoto locationPhoto = (LocationPhoto) session.load(LocationPhoto.class, id);
		if (null != locationPhoto) {
			session.delete(locationPhoto);
		}
		session.getTransaction().commit();
		return locationPhoto;
	}

	public LocationPhoto find(Long id) {
		LocationPhoto locationPhoto = new LocationPhoto();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			locationPhoto = (LocationPhoto) session.load(LocationPhoto.class, id);
			// session.getTransaction().commit();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return locationPhoto;
	}

	@SuppressWarnings("unchecked")
	public List<LocationPhoto> list() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<LocationPhoto> locationPhotos = null;
		try {

			locationPhotos = (List<LocationPhoto>) session.createQuery("from LocationPhoto").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return locationPhotos;
	}

	
}
