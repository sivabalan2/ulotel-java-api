package com.ulopms.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;
import org.hibernate.transform.Transformers;

import com.ulopms.model.PmsSourceType;
import com.ulopms.util.HibernateUtil;


public class PmsSourceTypeManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(PmsSourceTypeManager.class);

	public PmsSourceType add(PmsSourceType sourceType) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(sourceType);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return sourceType;
	}

	public PmsSourceType edit(PmsSourceType sourceType) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(sourceType);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return sourceType;
	}

	public PmsSourceType delete(int id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		PmsSourceType sourceType = (PmsSourceType) session.load(PmsSourceType.class, id);
		if (null != sourceType) {
			session.delete(sourceType);
		}
		session.getTransaction().commit();
		return sourceType;
	}

	public PmsSourceType find(int id) {
		PmsSourceType sourceType = new PmsSourceType();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			sourceType = (PmsSourceType) session.get(PmsSourceType.class, id);
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return sourceType;
	}
	
	@SuppressWarnings("unchecked")
	public List<PmsSourceType> list() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsSourceType> sourceType = null;
		try {

			sourceType = (List<PmsSourceType>) session.createQuery("from PmsSourceType where isActive = 'true' "
					+ "and isDeleted = 'false' ").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return sourceType;
	}

}
