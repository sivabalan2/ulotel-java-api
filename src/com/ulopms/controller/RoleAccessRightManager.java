package com.ulopms.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;

import com.ulopms.model.RoleAccessRight;
import com.ulopms.model.UserAccessRight;
import com.ulopms.util.HibernateUtil;


public class RoleAccessRightManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(RoleAccessRightManager.class);

	public RoleAccessRight add(RoleAccessRight roleAccessRight) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(roleAccessRight);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return roleAccessRight;
	}

	public RoleAccessRight edit(RoleAccessRight roleAccessRight) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(roleAccessRight);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return roleAccessRight;
	}

	public RoleAccessRight delete(Long id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		RoleAccessRight roleAccessRight = (RoleAccessRight) session.load(RoleAccessRight.class, id);
		if (null != roleAccessRight) {
			session.delete(roleAccessRight);
		}
		session.getTransaction().commit();
		return roleAccessRight;
	}

	public RoleAccessRight find(Long id) {
		RoleAccessRight roleAccessRight = new RoleAccessRight();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			roleAccessRight = (RoleAccessRight) session.load(RoleAccessRight.class, id);
			// session.getTransaction().commit();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return roleAccessRight;
	}

	@SuppressWarnings("unchecked")
	public List<RoleAccessRight> list() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<RoleAccessRight> roleAccessRights = null;
		try {

			roleAccessRights = (List<RoleAccessRight>) session.createQuery("from RoleAccessRight").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return roleAccessRights;
	}
	
	@SuppressWarnings("unchecked")
	public List<RoleAccessRight> list(int roleId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<RoleAccessRight> roleAccessRights = null;
		try {

			roleAccessRights = (List<RoleAccessRight>) session.createQuery("from RoleAccessRight where role.roleId =:roleId and accessRightsPermission = true")
		    .setParameter("roleId", roleId).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return roleAccessRights;
	}
	
		@SuppressWarnings("unchecked")
		public List<RoleAccessRight> list(int roleId, int accessRightsId) {
						Session session = HibernateUtil.getSessionFactory().getCurrentSession();
						session.beginTransaction();
						List<RoleAccessRight> roleAccessRights = null;
						try {

								roleAccessRights = (List<RoleAccessRight>) session.createQuery("from RoleAccessRight where role.roleId =:roleId "
											+ "and accessRight.accessRightsId =:accessRightsId and accessRightsPermission = true" )
											.setParameter("roleId", roleId)
											.setParameter("accessRightsId", accessRightsId).list();

							} catch (HibernateException e) {
									logger.error(e);
									session.getTransaction().rollback();
							} finally {
									session.getTransaction().commit();
									session = null;
							}
						return roleAccessRights;
					}
		
		@SuppressWarnings("unchecked")
		public List<RoleAccessRight> listReports(int roleId) {

			Session session = HibernateUtil.getSessionFactory().getCurrentSession();
			session.beginTransaction();
			List<RoleAccessRight> roleAccessRights = null;
			try {

				roleAccessRights = (List<RoleAccessRight>) session.createQuery("from RoleAccessRight where role.roleId =:roleId "
						+ "and accessRightsPermission = true ")
			    .setParameter("roleId", roleId).list();

			} catch (HibernateException e) {
				logger.error(e);
				session.getTransaction().rollback();
			} finally {
				session.getTransaction().commit();
				session = null;
			}
			return roleAccessRights;
		}

	
}
