package com.ulopms.controller;

import java.util.List;
import java.util.Date;
import java.util.Optional;
//import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;
import org.hibernate.transform.Transformers;
import org.joda.time.DateTime;

import com.ulopms.model.PmsAvailableRooms;
import com.ulopms.model.PmsRoomDetail;
import com.ulopms.model.PropertyAccommodationRoom;
import com.ulopms.model.PmsBooking;
import com.ulopms.model.DashBoard;
import com.ulopms.model.PmsProperty;
import com.ulopms.util.HibernateUtil;


public class PmsBookingManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(PmsBookingManager.class);

	public PmsBooking add(PmsBooking pmsBooking) {
		
		Integer bookingId;
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(pmsBooking);
			
			//bookingId = pmsBooking.getBookingId();
			
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			
			session = null;
		}
		return pmsBooking;
	}

	public PmsBooking edit(PmsBooking pmsBooking) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(pmsBooking);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return pmsBooking;
	}

	public PmsBooking delete(int id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		PmsBooking pmsBooking = (PmsBooking) session.load(PmsBooking.class, id);
		if (null != pmsBooking) {
			session.delete(pmsBooking);
		}
		session.getTransaction().commit();
		return pmsBooking;
	}

	public PmsBooking find(int id) {
		PmsBooking pmsBooking = new PmsBooking();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			pmsBooking = (PmsBooking) session.load(PmsBooking.class, id);
			// session.getTransaction().commit();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return pmsBooking;
	}
	
	@SuppressWarnings("unchecked")
	public DashBoard findPartnerCheckIn(int propertyId, String today) throws ParseException {
		Date date=new SimpleDateFormat("yyyy-MM-dd").parse(today);
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		DashBoard dashboard = null;
		try {
			
			 dashboard = (DashBoard)session.createQuery("select count(pb.arrivalDate) as todayArrival "
			+ "from PmsBooking pb where pb.pmsProperty.propertyId=:propertyId and pb.arrivalDate =:date"
			+ " and pb.pmsStatus.statusId=:statusId")
			 		.setParameter("propertyId", propertyId)
					.setParameter("statusId", 2)
					.setParameter("date", date)
					.setResultTransformer(Transformers.aliasToBean(DashBoard.class))
					.uniqueResult();
					
			
			
			
		} catch (HibernateException e) {
			logger.error(e);  
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return dashboard;
	}
	
	@SuppressWarnings("unchecked")
	public DashBoard findPartnerCheckOut(int propertyId, String today) throws ParseException {
		Date date=new SimpleDateFormat("yyyy-MM-dd").parse(today); 
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		DashBoard dashboard = null;
		try {
			
			dashboard = (DashBoard) session.createQuery("select count(pb.departureDate) as todayDeparture"
			+ " from PmsBooking pb where pb.pmsProperty.propertyId=:propertyId and pb.departureDate =:date "
			+ " and pb.pmsStatus.statusId=:statusId ")
			.setParameter("propertyId", propertyId)
			.setParameter("date", date)
			.setParameter("statusId", 2)
			.setResultTransformer(Transformers.aliasToBean(DashBoard.class))
			.uniqueResult();
			
			
			
		} catch (HibernateException e) {
			logger.error(e);  
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return dashboard;
	}
	
	
	@SuppressWarnings("unchecked")
	public DashBoard findArrivalCount(int propertyId, String today) throws ParseException {
		Date date=new SimpleDateFormat("yyyy-MM-dd").parse(today); 
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		DashBoard dashboard = null;
		try {
			/*
			 dashboard = (DashBoard)session.createQuery("select count(pb.arrivalDate) as todayArrival "
			+ "from PmsBooking pb where pb.pmsProperty.propertyId=:propertyId and pb.arrivalDate =:date"
			+ " and pb.pmsStatus.statusId=:statusId")
			 		.setParameter("propertyId", propertyId)
					.setParameter("statusId", 3)
					.setParameter("date", date)
					.setResultTransformer(Transformers.aliasToBean(DashBoard.class))
					.uniqueResult();
					*/
			
			dashboard = (DashBoard)session.createQuery("select COALESCE(sum(bd.roomCount),0) as todayArrival "
					+ "from PmsBooking pb,BookingDetail bd where pb.bookingId=bd.pmsBooking.bookingId and "
					+ " pb.pmsProperty.propertyId=:propertyId and :date between pb.arrivalDate and pb.departureDate"
					+ " and bd.pmsStatus.statusId=:statusId and bd.isActive='true' and bd.isDeleted='false' and bd.bookingDate=:date " )
					.setParameter("propertyId", propertyId)
					.setParameter("statusId", 3)
					.setParameter("date", date)
					.setResultTransformer(Transformers.aliasToBean(DashBoard.class))
					.uniqueResult();
			
		} catch (HibernateException e) {
			logger.error(e);  
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return dashboard;
	}
	
	@SuppressWarnings("unchecked")
	public DashBoard findDepartureCount(int propertyId, String today) throws ParseException {
		Date date=new SimpleDateFormat("yyyy-MM-dd").parse(today); 
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		DashBoard dashboard = null;
		try {

			/*dashDoard = (DashBoard) session.createQuery("select count(pb.departureDate) as todayDeparture"
					+ " from PmsBooking pb where pb.pmsProperty.propertyId=:propertyId and pb.departureDate =:date "
					+ " and pb.pmsStatus.statusId=:statusId ")
					.setParameter("propertyId", propertyId)
					.setParameter("date", date)
					.setParameter("statusId", 5)
					.setResultTransformer(Transformers.aliasToBean(DashBoard.class))
					.uniqueResult();*/
			
			dashboard = (DashBoard)session.createQuery("select COALESCE(sum(bd.roomCount),0) as todayDeparture "
					+ "from PmsBooking pb,BookingDetail bd where pb.bookingId=bd.pmsBooking.bookingId and "
					+ " pb.pmsProperty.propertyId=:propertyId and :date between pb.arrivalDate and pb.departureDate"
					+ " and bd.pmsStatus.statusId=:statusId and bd.isActive='true' and bd.isDeleted='false' and bd.bookingDate=:date")
					.setParameter("propertyId", propertyId)
					.setParameter("date", date)
					.setParameter("statusId", 5)
					.setResultTransformer(Transformers.aliasToBean(DashBoard.class))
					.uniqueResult();
			
		} catch (HibernateException e) {
			logger.error(e);  
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return dashboard;
	}
	
	public PmsAvailableRooms findChartCount(String fromDate,String toDate ,Integer accommodationId) throws ParseException {
	    PmsAvailableRooms roomCount = new PmsAvailableRooms();
	    DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Date start = df.parse(fromDate);
		Date end = df.parse(toDate); 
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		
		try {
		
			
			session.beginTransaction();
			roomCount = (PmsAvailableRooms)session.createQuery("select  sum(bd.roomCount) as roomCount from BookingDetail bd ,PmsBooking pb "
					+ "where  bd.bookingDate between :arrivalDate and :departureDate and pb.bookingId = bd.pmsBooking.bookingId and bd.propertyAccommodation.accommodationId = :accommodationId "
					+ "and bd.pmsStatus.statusId <> :statusId and bd.pmsStatus.statusId <> 1 and bd.isActive='true' and bd.isDeleted='false' ")
		    .setParameter("arrivalDate", start)
		    .setParameter("departureDate", end)
		    .setParameter("accommodationId", accommodationId)
		    .setParameter("statusId", 4)
		    .setResultTransformer(Transformers.aliasToBean(PmsAvailableRooms.class)).uniqueResult();

		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		
		return Optional.ofNullable(roomCount).orElse(roomCount);
		//return roomCount;
	}
	
	@SuppressWarnings("unchecked")
	public DashBoard findOccupancyCount(int propertyId, String today) throws ParseException {
		Date date=new SimpleDateFormat("yyyy-MM-dd").parse(today);
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		DashBoard dashDoard = null;
		try {

			dashDoard = (DashBoard)  session.createQuery("select COALESCE(sum(bd.roomCount),0) as todayOccupancy from PmsBooking pb,BookingDetail bd where "
					+ "pb.pmsProperty.propertyId=:propertyId and pb.bookingId = bd.pmsBooking.bookingId and :date between pb.arrivalDate and pb.departureDate and "
					+ " pb.isActive = 'true' and pb.isDeleted = 'false' and bd.isActive = 'true' and bd.isDeleted = 'false' "
					+ " and bd.pmsStatus.statusId <> 4 and bd.pmsStatus.statusId <> 1  and bd.bookingDate=:date ")
					.setParameter("propertyId", propertyId)
					.setParameter("date", date)
					.setResultTransformer(Transformers.aliasToBean(DashBoard.class))
					.uniqueResult();
		} catch (HibernateException e) {
			logger.error(e);  
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return dashDoard;
	}
	
	@SuppressWarnings("unchecked")
	public DashBoard findTodayRevenue(int propertyId, String today) throws ParseException {
		Date date=new SimpleDateFormat("yyyy-MM-dd").parse(today); 
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		DashBoard dashDoard = null;
		try {

			dashDoard = (DashBoard)  session.createQuery("select COALESCE(sum(bd.amount),0) as todayRevenue,COALESCE(sum(bd.otaCommission),0) as otaCommission,"
					+ "COALESCE(sum(bd.otaTax),0) as otaTax from PmsBooking pb,BookingDetail bd where "
					+ "pb.pmsProperty.propertyId=:propertyId and pb.bookingId = bd.pmsBooking.bookingId and :date between pb.arrivalDate and pb.departureDate and "
					+ " pb.isActive = 'true' and pb.isDeleted = 'false' and bd.isActive = 'true' and bd.isDeleted = 'false' "
					+ " and bd.pmsStatus.statusId <> 4 and bd.pmsStatus.statusId <> 1  and bd.bookingDate=:date ")
					.setParameter("propertyId", propertyId)
					.setParameter("date", date)
					.setResultTransformer(Transformers.aliasToBean(DashBoard.class))
					.uniqueResult();
		} catch (HibernateException e) {
			logger.error(e);  
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return dashDoard;
	}
	
	
	
	/*@SuppressWarnings("unchecked")
	public List<PmsAvailableRooms> listAllBooking(int propertyId,String todayDate) throws ParseException {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Date today = df.parse(todayDate);
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		//Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		List<PmsAvailableRooms> pmsAvailableRooms = null;
		try {

			pmsAvailableRooms = (List<PmsAvailableRooms>) session.createQuery("select pb.bookingId as bookingId,pb.arrivalDate as arrivalDate,"
					+ "pb.departureDate as departureDate,pb.rooms as rooms,pb.pmsSource.sourceId as sourceId,pb.pmsStatus.statusId as statusId"
					+ " from PmsBooking pb where pb.pmsProperty.propertyId = :propertyId and (:today between pb.arrivalDate and pb.departureDate)")
					.setParameter("propertyId", propertyId)
					.setParameter("today", today)
					.setResultTransformer(Transformers.aliasToBean(PmsAvailableRooms.class))
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return pmsAvailableRooms;
	}*/
		
	@SuppressWarnings("unchecked")
	public List<DashBoard> listRevenue(int propertyId,String start,String end) throws ParseException {
      
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Date fromDate = df.parse(start);
		Date toDate = df.parse(end); 
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<DashBoard> dashBoard = null;
		try {

			dashBoard = (List<DashBoard>) session.createQuery("select COALESCE(sum(pb.totalAmount),0) as totalRevenue,COALESCE(sum(pb.totalTax),0) as totalTax,"
					+ " COALESCE(sum(pb.rooms),0) as totalRooms from PmsBooking pb where pb.pmsProperty.propertyId =:propertyId "
					+ "and pb.arrivalDate between :fromDate and :toDate "
					+ " and pb.pmsStatus.statusId <>:statusId and pb.pmsStatus.statusId <> 1 group by pb.pmsProperty.propertyId")
					.setParameter("propertyId", propertyId)
					.setParameter("fromDate", fromDate)
					.setParameter("toDate", toDate)
					.setParameter("statusId", 4)
					.setResultTransformer(Transformers.aliasToBean(DashBoard.class))
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return dashBoard;
	}
	
	@SuppressWarnings("unchecked")
	public List<DashBoard> listPartnerRevenue(int propertyId,String start,String end) throws ParseException {
      
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Date fromDate = df.parse(start);
		Date toDate = df.parse(end); 
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<DashBoard> dashBoard = null;
		try {

			dashBoard = (List<DashBoard>) session.createQuery("select COALESCE(sum(bd.amount),0) as totalRevenue,COALESCE(sum(bd.tax),0) as totalTax,"
					+ " COALESCE(sum(bd.roomCount),0) as totalRooms,COALESCE(sum(bd.otaCommission),0) as otaCommission,"
					+ "COALESCE(sum(bd.otaTax),0) as otaTax from PmsBooking pb,BookingDetail bd where pb.bookingId = bd.pmsBooking.bookingId and "
					+ "pb.pmsProperty.propertyId =:propertyId "
					+ "and bd.bookingDate between :startDate and :endDate"
					+ " and pb.pmsStatus.statusId <>:statusId and pb.pmsStatus.statusId <> 1")
					.setParameter("propertyId", propertyId)
					.setParameter("startDate", fromDate)
					.setParameter("endDate", toDate)
					.setParameter("statusId", 4)
					.setResultTransformer(Transformers.aliasToBean(DashBoard.class))
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return dashBoard;
	}
		
	
	 public PmsAvailableRooms findCount(Date arrivalDate ,Integer accommodationId) {
		    PmsAvailableRooms roomCount = new PmsAvailableRooms();
			Session session = HibernateUtil.getSessionFactory().getCurrentSession();
			
			try {
				/*
				 
				 SELECT * FROM property_accommodation_rooms WHERE NOT EXISTS(SELECT 1 FROM booking_details WHERE booking_details.room_id=property_accommodation_rooms.room_id and accommodation_id='16' and booking_date between '2017-06-17' and '2017-06-18')and accommodation_id='16' limit 1
				 
				 select roomId as roomId from propertyAcccommodationRoom where not exists(select 1 from bookingDetail where bookingDetail.propertyAccommodationRoom.roomId = propertyAccommodationRoom.roomId and bookingDetail.propertyAccommodation.accommodationId = :accommodationId and bookingDate between :arrivalDate and :departureDate) and propertyAcccommodationRoom.accommodationId =:accommodationId
				 */
				
				session.beginTransaction();
				roomCount = (PmsAvailableRooms)session.createQuery("select  sum(bd.roomCount) as roomCount from BookingDetail bd  "
						+ "where  bd.bookingDate = :arrivalDate and bd.propertyAccommodation.accommodationId = :accommodationId "
						+ "and bd.pmsStatus.statusId <> :statusId and bd.pmsStatus.statusId <> 1 and bd.isActive='true' and bd.isDeleted='false' ")
			    .setParameter("arrivalDate", arrivalDate).setParameter("accommodationId", accommodationId).setParameter("statusId", 4)
			    .setResultTransformer(Transformers.aliasToBean(PmsAvailableRooms.class)).uniqueResult();
				// session.getTransaction().commit();
				
			   /*	if(roomCount == null){
		            
		           return null;
		                
		          
		          }  */
			} catch (Exception e1) {
				logger.error(e1);
			} finally {
				session = null;
			}
			
			return Optional.ofNullable(roomCount).orElse(roomCount);
			//return roomCount;
		}
	 
	 public PmsAvailableRooms findCancelledCount(Date arrivalDate ,Integer accommodationId) {
		    PmsAvailableRooms roomCount = new PmsAvailableRooms();
			Session session = HibernateUtil.getSessionFactory().getCurrentSession();
			
			try {
				/*
				 
				 SELECT * FROM property_accommodation_rooms WHERE NOT EXISTS(SELECT 1 FROM booking_details WHERE booking_details.room_id=property_accommodation_rooms.room_id and accommodation_id='16' and booking_date between '2017-06-17' and '2017-06-18')and accommodation_id='16' limit 1
				 
				 select roomId as roomId from propertyAcccommodationRoom where not exists(select 1 from bookingDetail where bookingDetail.propertyAccommodationRoom.roomId = propertyAccommodationRoom.roomId and bookingDetail.propertyAccommodation.accommodationId = :accommodationId and bookingDate between :arrivalDate and :departureDate) and propertyAcccommodationRoom.accommodationId =:accommodationId
				 */
				
				session.beginTransaction();
				roomCount = (PmsAvailableRooms)session.createQuery("select  sum(bd.roomCount) as roomCount from BookingDetail bd  "
						+ "where  bd.bookingDate = :arrivalDate and bd.propertyAccommodation.accommodationId = :accommodationId "
						+ "and bd.pmsStatus.statusId = :statusId and bd.pmsStatus.statusId = 1 and bd.isActive='true' and bd.isDeleted='false' ")
			    .setParameter("arrivalDate", arrivalDate).setParameter("accommodationId", accommodationId).setParameter("statusId", 4)
			    .setResultTransformer(Transformers.aliasToBean(PmsAvailableRooms.class)).uniqueResult();
				// session.getTransaction().commit();
				
			   /*	if(roomCount == null){
		            
		           return null;
		                
		          
		          }  */
			} catch (Exception e1) {
				logger.error(e1);
			} finally {
				session = null;
			}
			
			return Optional.ofNullable(roomCount).orElse(roomCount);
			//return roomCount;
		}
	 
	 public PmsAvailableRooms findBookingCount(Date arrivalDate ,Integer accommodationId,Integer inventoryId) {
		    PmsAvailableRooms roomCount = new PmsAvailableRooms();
			Session session = HibernateUtil.getSessionFactory().getCurrentSession();
			
			try {
				/*
				 
				 SELECT * FROM property_accommodation_rooms WHERE NOT EXISTS(SELECT 1 FROM booking_details WHERE booking_details.room_id=property_accommodation_rooms.room_id and accommodation_id='16' and booking_date between '2017-06-17' and '2017-06-18')and accommodation_id='16' limit 1
				 
				 select roomId as roomId from propertyAcccommodationRoom where not exists(select 1 from bookingDetail where bookingDetail.propertyAccommodationRoom.roomId = propertyAccommodationRoom.roomId and bookingDetail.propertyAccommodation.accommodationId = :accommodationId and bookingDate between :arrivalDate and :departureDate) and propertyAcccommodationRoom.accommodationId =:accommodationId
				 */
				
				session.beginTransaction();
				roomCount = (PmsAvailableRooms)session.createQuery("select  sum(bd.roomCount) as roomCount from BookingDetail bd  "
						+ "where  bd.bookingDate = :arrivalDate and bd.propertyAccommodation.accommodationId = :accommodationId "
						+ "and bd.pmsStatus.statusId <> :statusId and bd.pmsStatus.statusId <> 1 "
						+ "and bd.propertyAccommodationInventory.inventoryId = :inventoryId and bd.isActive='true' and bd.isDeleted='false' ")
			    .setParameter("arrivalDate", arrivalDate).setParameter("accommodationId", accommodationId).setParameter("statusId", 4)
			    .setParameter("inventoryId", inventoryId)
			    .setResultTransformer(Transformers.aliasToBean(PmsAvailableRooms.class)).uniqueResult();
				// session.getTransaction().commit();
				
			   	if(roomCount == null){
		            
		           return null;	                
		          
		          }  
			} catch (Exception e1) {
				logger.error(e1);
			} finally {
				session = null;
			}
			
			return Optional.ofNullable(roomCount).orElse(roomCount);
			//return roomCount;
		}
	 
	/* public PmsAvailableRooms findCount(Date arrivalDate ,Date departureDate,Integer accommodationId) {
		    PmsAvailableRooms roomCount = new PmsAvailableRooms();
			Session session = HibernateUtil.getSessionFactory().getCurrentSession();
			
			try {
				
				 
				 SELECT * FROM property_accommodation_rooms WHERE NOT EXISTS(SELECT 1 FROM booking_details WHERE booking_details.room_id=property_accommodation_rooms.room_id and accommodation_id='16' and booking_date between '2017-06-17' and '2017-06-18')and accommodation_id='16' limit 1
				 
				 select roomId as roomId from propertyAcccommodationRoom where not exists(select 1 from bookingDetail where bookingDetail.propertyAccommodationRoom.roomId = propertyAccommodationRoom.roomId and bookingDetail.propertyAccommodation.accommodationId = :accommodationId and bookingDate between :arrivalDate and :departureDate) and propertyAcccommodationRoom.accommodationId =:accommodationId
				 
				
				session.beginTransaction();
				roomCount = (PmsAvailableRooms)session.createQuery("select bd.propertyAccommodation.accommodationId as accommodationId , sum(bd.roomCount) as roomCount from BookingDetail bd ,PmsBooking pb where bd.bookingDate between :arrivalDate and :departureDate and bd.propertyAccommodation.accommodationId = :accommodationId group by bd.propertyAccommodation.accommodationId")
			    .setParameter("arrivalDate", arrivalDate).setParameter("departureDate", departureDate) .setParameter("accommodationId", accommodationId)
			    .setResultTransformer(Transformers.aliasToBean(PmsAvailableRooms.class)).uniqueResult();
				// session.getTransaction().commit();
				
			   	if(roomCount == null){
		            
		           return null;
		                
		          
		          }  
			} catch (Exception e1) {
				logger.error(e1);
			} finally {
				session = null;
			}
			
			return Optional.ofNullable(roomCount).orElse(roomCount);
			//return roomCount;
		}*/
	 
	 
	@SuppressWarnings("unchecked")
    public List<PmsBooking> list(int propertyId) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        List<PmsBooking> pmsBookings = null;
        try {

            pmsBookings = (List<PmsBooking>) session.createQuery("from PmsBooking where pmsProperty.propertyId = :propertyId").setParameter("propertyId", propertyId).list();

        } catch (HibernateException e) {
            logger.error(e);
            session.getTransaction().rollback();
        } finally {
            session.getTransaction().commit();
            session = null;
        }
        return pmsBookings;
    }
	
	@SuppressWarnings("unchecked")
	public PmsBooking findOtaBookingId(String otaBookingId) throws ParseException {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		PmsBooking pmsBooking = null;
		try {

			pmsBooking = (PmsBooking)session.createQuery("select bookingId as bookingId from PmsBooking  where otaBookingId =:otaBookingId "
					+ "and isActive = 'true' and isDeleted = 'false' ")
					.setParameter("otaBookingId", otaBookingId)
					.setResultTransformer(Transformers.aliasToBean(PmsBooking.class))
					.uniqueResult();
		} catch (HibernateException e) {
			logger.error(e);  
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return pmsBooking;
	}

	
	
	@SuppressWarnings("unchecked")
    public List<PmsBooking> listBookingreport(int propertyId,Date arrivaldate,Date departuredate) {
		 Session session = HibernateUtil.getSessionFactory().getCurrentSession();
	        session.beginTransaction();

        List<PmsBooking> pmsBookings = null;
        try {

            pmsBookings = (List<PmsBooking>) session.createQuery("select pb.bookingId as bookingId ,pb.arrivalDate as arrivalDate,"
            		+ "pb.departureDate as departureDate,pb.totalAmount as totalAmount,pb.rooms as rooms from PmsBooking pb "
            		+ "where pb.pmsProperty.propertyId = :propertyId and (:arrivaldate <= pb.arrivalDate "
            		+ "and :departuredate >= pb.departureDate)")
            		.setParameter("propertyId", propertyId)
            		.setParameter("arrivaldate", arrivaldate)
            		.setParameter("departuredate", departuredate)
            		.setResultTransformer(Transformers.aliasToBean(PmsBooking.class)).list();
          
        } catch (HibernateException e) {
            logger.error(e);
            session.getTransaction().rollback();
        } finally {
            session.getTransaction().commit();
            session = null;
        }
        return pmsBookings;
        
    }
	
	

	@SuppressWarnings("unchecked")
    public List<PmsAvailableRooms> list(int propertyId , Timestamp arrivalDate , Timestamp departureDate) {
       
		String startDate = new SimpleDateFormat("yyyy-MM-dd").format(arrivalDate);
		String endDate = new SimpleDateFormat("yyyy-MM-dd").format(departureDate);
		
		DateTime start = DateTime.parse(startDate);
		DateTime end = DateTime.parse(endDate);
		
		java.util.Date arrival = start.toDate();
		java.util.Date departure = end.toDate();
		
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        List<PmsAvailableRooms> pmsAvailableRooms = null;
        try {
            
        	/* sql query */
        	/* select pb.property_id ,sum(pb.rooms)as roomcount,pa.accommodation_id ,pa.accommodation_type ,pa.max_occupancy
             ,pa.adults_included_rate,pa.child_included_rate,pa.extra_child,pa.extra_adult,
            pa.no_of_units ,(pa.no_of_units- sum(case when pa.accommodation_id = bd.accommodation_id  then pb.rooms else 0 end)) as available
            from pms_booking pb ,booking_details bd ,property_accommodation pa 
            where pa.property_id = '1' and pa.is_active= true and pa.is_deleted= false  and pb.booking_id = bd.booking_id 
            and (
             '2017-06-17 00:00:00.0' BETWEEN pb.arrival_date AND pb.departure_date OR
             '2017-06-18 00:00:00.0' BETWEEN pb.arrival_date AND pb.departure_date OR 
             ('2017-06-17 00:00:00.0' <= pb.arrival_date AND '2017-06-18 00:00:00.0' >= pb.departure_date)
             ) 
             group by pb.property_id, pa.accommodation_id ,pa.accommodation_type ,pa.no_of_units ,pa.max_occupancy
             ,pa.adults_included_rate,pa.child_included_rate,pa.extra_child,pa.extra_adult
             */
        	
            
            
            //select pb.pmsProperty.propertyId as propertyId ,sum(case when pa.accommodationId = bd.propertyAccommodation.accommodationId and pb.bookingId = bd.pmsBooking.bookingId then pb.rooms else 0 end)as roomCount,pa.accommodationId as accommodationId ,pa.accommodationType as accommodationType ,pa.maxOccupancy as maxOccupancy ,pa.adultsIncludedRate as adultsIncludedRate,pa.childIncludedRate as childIncludedRate,pa.extraChild as extraChild,pa.extraAdult as extraAdult, pa.baseAmount as baseAmount, pa.noOfUnits as noOfUnits  ,(pa.noOfUnits- sum(case when pa.accommodationId = bd.propertyAccommodation.accommodationId and pb.bookingId = bd.pmsBooking.bookingId then pb.rooms else 0 end)) as available from PmsBooking pb ,BookingDetail bd ,PropertyAccommodation pa where pb.pmsProperty.propertyId = :propertyId  and pa.pmsProperty.propertyId = :propertyId and pa.isActive= true and pa.isDeleted = false  and  ( :arrivalDate BETWEEN pb.arrivalDate AND pb.departureDate OR  :departureDate BETWEEN pb.arrivalDate AND pb.departureDate OR   (:arrivalDate <= pb.arrivalDate AND :departureDate >= pb.departureDate)) group by pb.pmsProperty.propertyId, pa.accommodationId ,pa.accommodationType,pa.maxOccupancy,pa.adultsIncludedRate,pa.childIncludedRate,pa.extraChild,pa.extraAdult ,pa.baseAmount ,pa.noOfUnits
             
        	
        	
        	//select b.accommodation_id ,count(room_id) as available from  property_accommodation b, property_accommodation_rooms a
        	//where a.accommodation_id = b.accommodation_id and b.property_id = '1' and b.is_active=true
        	//group by b.accommodation_type,b.accommodation_id
        	
        	
        	//select pa.accommodationId as accommodationId ,pa.accommodationType as accommodationType ,pa.minOccupancy as minOccupancy,pa.maxOccupancy as maxOccupancy ,pa.noOfAdults as noOfAdults,pa.noOfChild as noOfChild,pa.extraChild as extraChild,pa.extraInfant as extraInfant,pa.extraAdult as extraAdult, pa.baseAmount as baseAmount, pa.noOfUnits as noOfUnits , par.areaSqft as areaSqft, count(par.roomId) as available from PropertyAccommodationRoom par,PropertyAccommodation pa  where  pa.accommodationId = par.propertyAccommodation.accommodationId and pa.pmsProperty.propertyId = :propertyId  and pa.isActive = true and pa.isDeleted =false and par.isActive = true and par.isDeleted =false group by pa.accommodationId ,pa.accommodationType,pa.minOccupancy,pa.maxOccupancy,pa.noOfAdults,pa.noOfChild,pa.extraChild,pa.extraAdult ,pa.baseAmount ,pa.noOfUnits,par.areaSqft order by pa.baseAmount asc
        	 
        	pmsAvailableRooms = (List<PmsAvailableRooms>) session.createQuery("select pa.accommodationId as accommodationId ,pa.accommodationType as accommodationType ,"
        			+ "pa.minOccupancy as minOccupancy,pa.maxOccupancy as maxOccupancy ,pa.noOfAdults as noOfAdults,pa.noOfChild as noOfChild,pa.extraChild as extraChild,"
        			+ "pa.extraInfant as extraInfant,pa.extraAdult as extraAdult, pa.baseAmount as baseAmount, pa.noOfUnits as noOfUnits , "
        			+ " pa.noOfUnits as roomavailable from PropertyAccommodationRoom par,PropertyAccommodation pa "
        			+ " where  pa.accommodationId = par.propertyAccommodation.accommodationId and pa.pmsProperty.propertyId = :propertyId  "
        			+ "and pa.isActive = true and pa.isDeleted =false and par.isActive = true and par.isDeleted =false"
        			+ " group by pa.accommodationId ,pa.accommodationType,pa.minOccupancy,pa.maxOccupancy,pa.noOfAdults,pa.noOfChild,pa.extraChild,"
        			+ "pa.extraAdult ,pa.baseAmount ,pa.noOfUnits order by pa.baseAmount asc ")
            .setParameter("propertyId", propertyId)
            .setResultTransformer(Transformers.aliasToBean(PmsAvailableRooms.class)).list();
            
           /* if(pmsAvailableRooms.size() == 0){
            
            //select accommodation_id , accommodation_type, no_of_units as available, base_amount from property_accommodation where
            //property_id = 1 and is_active='true' and is_deleted ='false'
                
            pmsAvailableRooms = (List<PmsAvailableRooms>) session.createQuery("select pa.accommodationId as accommodationId , pa.accommodationType as accommodationType ,pa.noOfUnits as noOfUnits , pa.baseAmount as baseAmount ,pa.maxOccupancy as maxOccupancy ,pa.adultsIncludedRate as adultsIncludedRate,pa.childIncludedRate as childIncludedRate,pa.extraChild as extraChild,pa.extraAdult as extraAdult from PropertyAccommodation pa where pa.pmsProperty.propertyId = :propertyId and isActive= true and isDeleted = false")
            .setParameter("propertyId", propertyId)
            .setResultTransformer(Transformers.aliasToBean(PmsAvailableRooms.class)).list();
            
            
            
            } */
            
            
        
        } catch (HibernateException e) {
            logger.error(e);
            session.getTransaction().rollback();
        } finally {
            session.getTransaction().commit();
            session = null;
        }
        return pmsAvailableRooms;
    }
	
	@SuppressWarnings("unchecked")
    public List<PmsAvailableRooms> list(int propertyId , Timestamp arrivalDate , Timestamp departureDate,int accommodationId) {
       
		String startDate = new SimpleDateFormat("yyyy-MM-dd").format(arrivalDate);
		String endDate = new SimpleDateFormat("yyyy-MM-dd").format(departureDate);
		
		DateTime start = DateTime.parse(startDate);
		DateTime end = DateTime.parse(endDate);
		
		java.util.Date arrival = start.toDate();
		java.util.Date departure = end.toDate();
		
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        List<PmsAvailableRooms> pmsAvailableRooms = null;
        try {
            
        	/* sql query */
        	/* select pb.property_id ,sum(pb.rooms)as roomcount,pa.accommodation_id ,pa.accommodation_type ,pa.max_occupancy
             ,pa.adults_included_rate,pa.child_included_rate,pa.extra_child,pa.extra_adult,
            pa.no_of_units ,(pa.no_of_units- sum(case when pa.accommodation_id = bd.accommodation_id  then pb.rooms else 0 end)) as available
            from pms_booking pb ,booking_details bd ,property_accommodation pa 
            where pa.property_id = '1' and pa.is_active= true and pa.is_deleted= false  and pb.booking_id = bd.booking_id 
            and (
             '2017-06-17 00:00:00.0' BETWEEN pb.arrival_date AND pb.departure_date OR
             '2017-06-18 00:00:00.0' BETWEEN pb.arrival_date AND pb.departure_date OR 
             ('2017-06-17 00:00:00.0' <= pb.arrival_date AND '2017-06-18 00:00:00.0' >= pb.departure_date)
             ) 
             group by pb.property_id, pa.accommodation_id ,pa.accommodation_type ,pa.no_of_units ,pa.max_occupancy
             ,pa.adults_included_rate,pa.child_included_rate,pa.extra_child,pa.extra_adult
             */
        	
            
            
            //select pb.pmsProperty.propertyId as propertyId ,sum(case when pa.accommodationId = bd.propertyAccommodation.accommodationId and pb.bookingId = bd.pmsBooking.bookingId then pb.rooms else 0 end)as roomCount,pa.accommodationId as accommodationId ,pa.accommodationType as accommodationType ,pa.maxOccupancy as maxOccupancy ,pa.adultsIncludedRate as adultsIncludedRate,pa.childIncludedRate as childIncludedRate,pa.extraChild as extraChild,pa.extraAdult as extraAdult, pa.baseAmount as baseAmount, pa.noOfUnits as noOfUnits  ,(pa.noOfUnits- sum(case when pa.accommodationId = bd.propertyAccommodation.accommodationId and pb.bookingId = bd.pmsBooking.bookingId then pb.rooms else 0 end)) as available from PmsBooking pb ,BookingDetail bd ,PropertyAccommodation pa where pb.pmsProperty.propertyId = :propertyId  and pa.pmsProperty.propertyId = :propertyId and pa.isActive= true and pa.isDeleted = false  and  ( :arrivalDate BETWEEN pb.arrivalDate AND pb.departureDate OR  :departureDate BETWEEN pb.arrivalDate AND pb.departureDate OR   (:arrivalDate <= pb.arrivalDate AND :departureDate >= pb.departureDate)) group by pb.pmsProperty.propertyId, pa.accommodationId ,pa.accommodationType,pa.maxOccupancy,pa.adultsIncludedRate,pa.childIncludedRate,pa.extraChild,pa.extraAdult ,pa.baseAmount ,pa.noOfUnits
             
        	
        	
        	//select b.accommodation_id ,count(room_id) as available from  property_accommodation b, property_accommodation_rooms a
        	//where a.accommodation_id = b.accommodation_id and b.property_id = '1' and b.is_active=true
        	//group by b.accommodation_type,b.accommodation_id
        	
        	 pmsAvailableRooms = (List<PmsAvailableRooms>) session.createQuery("select pa.accommodationId as accommodationId ,pa.accommodationType as accommodationType ,"
        	 		+ "pa.minOccupancy as minOccupancy,pa.maxOccupancy as maxOccupancy ,pa.noOfAdults as noOfAdults,pa.noOfChild as noOfChild,"
        	 		+ "pa.extraChild as extraChild,pa.extraAdult as extraAdult, pa.baseAmount as baseAmount, pa.noOfUnits as noOfUnits , pa.noOfUnits as roomavailable "
        	 		+ "from PropertyAccommodationRoom par,PropertyAccommodation pa  where  pa.accommodationId =:accommodationId and pa.pmsProperty.propertyId = :propertyId "
        	 		+ " and pa.isActive = true and pa.isDeleted =false and par.isActive = true and par.isDeleted =false group by pa.accommodationId ,pa.accommodationType,"
        	 		+ "pa.minOccupancy,pa.maxOccupancy,pa.noOfAdults,pa.noOfChild,pa.extraChild,pa.extraAdult ,pa.baseAmount ,pa.noOfUnits order by pa.baseAmount asc ")
            .setParameter("propertyId", propertyId)
            .setParameter("accommodationId", accommodationId)
            .setResultTransformer(Transformers.aliasToBean(PmsAvailableRooms.class)).list();
            
           /* if(pmsAvailableRooms.size() == 0){
            
            //select accommodation_id , accommodation_type, no_of_units as available, base_amount from property_accommodation where
            //property_id = 1 and is_active='true' and is_deleted ='false'
                
            pmsAvailableRooms = (List<PmsAvailableRooms>) session.createQuery("select pa.accommodationId as accommodationId , pa.accommodationType as accommodationType ,pa.noOfUnits as noOfUnits , pa.baseAmount as baseAmount ,pa.maxOccupancy as maxOccupancy ,pa.adultsIncludedRate as adultsIncludedRate,pa.childIncludedRate as childIncludedRate,pa.extraChild as extraChild,pa.extraAdult as extraAdult from PropertyAccommodation pa where pa.pmsProperty.propertyId = :propertyId and isActive= true and isDeleted = false")
            .setParameter("propertyId", propertyId)
            .setResultTransformer(Transformers.aliasToBean(PmsAvailableRooms.class)).list();
            
            
            
            } */
            
            
        
        } catch (HibernateException e) {
            logger.error(e);
            session.getTransaction().rollback();
        } finally {
            session.getTransaction().commit();
            session = null;
        }
        return pmsAvailableRooms;
    }
	
	@SuppressWarnings("unchecked")
	 public PmsAvailableRooms findAvailable(Integer propertyId,Date arrivalDate) {
		    PmsAvailableRooms roomCount = new PmsAvailableRooms();
			Session session = HibernateUtil.getSessionFactory().getCurrentSession();
			try {
				session.beginTransaction();
				roomCount = (PmsAvailableRooms)session.createQuery("select  coalesce(sum(pb.rooms),0) as roomCount from PmsBooking pb  "
						+ "where  :arrivalDate between pb.arrivalDate and pb.departureDate and pb.pmsProperty.propertyId= :propertyId "
						+ "and pb.pmsStatus.statusId <> :statusId")
			    .setParameter("arrivalDate", arrivalDate).setParameter("propertyId", propertyId).setParameter("statusId", 4)
			    .setResultTransformer(Transformers.aliasToBean(PmsAvailableRooms.class)).uniqueResult();
			 
			} catch (HibernateException e) {
				logger.error(e);  
				session.getTransaction().rollback();
			} finally {
				session.getTransaction().commit();
				session = null;
				
			}
			return Optional.ofNullable(roomCount).orElse(roomCount);
		}
	
	@SuppressWarnings("unchecked")
    public List<PmsBooking> findSmsId(String date) {
		 Session session = HibernateUtil.getSessionFactory().getCurrentSession();
	        session.beginTransaction();
	        DateTime start = DateTime.parse(date);
			
			java.util.Date arrival = start.toDate();
			
        List<PmsBooking> pmsBookings = null;
        try {

            pmsBookings = (List<PmsBooking>) session.createQuery("from PmsBooking pb "
            		+ "where pb.departureDate =:arrival")
            		.setParameter("arrival", arrival)
            		.list();
          
        } catch (HibernateException e) {
            logger.error(e);
            session.getTransaction().rollback();
        } finally {
            session.getTransaction().commit();
            session = null;
        }
        return pmsBookings;
        
    }
	
	@SuppressWarnings("unchecked")
    public List<PmsBooking> findInvoiceId(String invoiceId) {
		 Session session = HibernateUtil.getSessionFactory().getCurrentSession();
	        session.beginTransaction();
	        
        List<PmsBooking> pmsBookings = null;
        try {

            pmsBookings = (List<PmsBooking>) session.createQuery("from PmsBooking pb "
            		+ "where pb.invoiceId =:invoiceId")
            		.setParameter("invoiceId", invoiceId)
            		.list();
          
        } catch (HibernateException e) {
            logger.error(e);
            session.getTransaction().rollback();
        } finally {
            session.getTransaction().commit();
            session = null;
        }
        return pmsBookings;
        
    }
	
	@SuppressWarnings("unchecked")
    public PmsBooking findGuestInvoiceId(String invoiceId) {
		 Session session = HibernateUtil.getSessionFactory().getCurrentSession();
	        session.beginTransaction();
	        
        PmsBooking pmsBookings = new PmsBooking();
        try {

            pmsBookings = (PmsBooking) session.createQuery("select pb.bookingId as bookingId from PmsBooking pb "
            		+ "where pb.invoiceId =:invoiceId and isActive= false and isDeleted = true")
            		.setParameter("invoiceId", invoiceId)
            		.setResultTransformer(Transformers.aliasToBean(PmsBooking.class)).uniqueResult();
          
        } catch (HibernateException e) {
            logger.error(e);
            session.getTransaction().rollback();
        } finally {
            session.getTransaction().commit();
            session = null;
        }
        return pmsBookings;
        
    }
	
}
