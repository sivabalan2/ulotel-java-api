package com.ulopms.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;

import com.ulopms.model.BookingDetail;
import com.ulopms.model.PromotionImages;
import com.ulopms.util.HibernateUtil;


public class PromotionImageManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(PromotionImageManager.class);

	public PromotionImages add(PromotionImages promotionImages) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(promotionImages);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return promotionImages;
	}

	public PromotionImages edit(PromotionImages propertyPhoto) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(propertyPhoto);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyPhoto;
	}

	public PromotionImages delete(int id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		PromotionImages propertyPhoto = (PromotionImages) session.load(PromotionImages.class, id);
		if (null != propertyPhoto) {
			session.delete(propertyPhoto);
		}
		session.getTransaction().commit();
		return propertyPhoto;
	}

	public PromotionImages find(int id) {
		PromotionImages propertyPhoto = new PromotionImages();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			propertyPhoto = (PromotionImages) session.load(PromotionImages.class, id);
			// session.getTransaction().commit();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return propertyPhoto;
	}
	
	@SuppressWarnings("unchecked")
	public List<PromotionImages> findId(int promotionImageId) {
		List<PromotionImages> promotionImages = null;
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			//bookingDetails = (List<BookingDetail>) session.createQuery("from BookingDetail").list();
			promotionImages = (List<PromotionImages>)session.createQuery("from PromotionImages where promotionImageId =:promotionImageId")
				    .setParameter("promotionImageId",promotionImageId)
				    .list();
			// session.getTransaction().commit();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return promotionImages;
	}

	@SuppressWarnings("unchecked")
	public List<PromotionImages> list() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PromotionImages> promotionImages = null;
		try {

			promotionImages = (List<PromotionImages>) session.createQuery("from PromotionImages where isActive = true and isDeleted =false ").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return promotionImages;
	}	
	

	
}
