package com.ulopms.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;

import com.ulopms.model.PropertyAmenity;
import com.ulopms.model.PropertyUser;
import com.ulopms.util.HibernateUtil;

public class PropertyUserManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(PropertyUserManager.class);

	public PropertyUser add(PropertyUser propertyUser) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(propertyUser);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyUser;
	}

	public PropertyUser edit(PropertyUser propertyUser) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(propertyUser);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyUser;
	}

	public PropertyUser delete(int id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		PropertyUser propertyUser = (PropertyUser) session.load(PropertyUser.class, id);
		if (null != propertyUser) {
			session.delete(propertyUser);
		}
		session.getTransaction().commit();
		return propertyUser;
	}

	public PropertyUser find(int id) {
		PropertyUser propertyUser = new PropertyUser();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			propertyUser = (PropertyUser) session.load(PropertyUser.class, id);
			// session.getTransaction().commit();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return propertyUser;
	}

	@SuppressWarnings("unchecked")
	public List<PropertyUser> list(int propertyId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		//Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		List<PropertyUser> propertyUsers = null;
		try {

			propertyUsers = (List<PropertyUser>) session.createQuery("from PropertyUser where isActive = 'true' "
					+ "and isDeleted = 'false' and pmsProperty.propertyId=:propertyId")
					.setParameter("propertyId", propertyId)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return propertyUsers;
	}
	
	@SuppressWarnings("unchecked")
	public List<PropertyUser> listProperty(int userId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		//Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		List<PropertyUser> userProperty = null;
		try {

			userProperty = (List<PropertyUser>) session.createQuery("from PropertyUser where  user.userId=:userId")
					.setParameter("userId", userId)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return userProperty;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<PropertyUser> list(int propertyId,int userId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyUser> propertyUsers = null;
		try {

			propertyUsers = (List<PropertyUser>) session.createQuery("from PropertyUser where pmsProperty.propertyId=:propertyId "
					+ " and user.userId=:userId"
					+ " and isActive = 'true' and isDeleted = 'false'")
					.setParameter("propertyId", propertyId)
					.setParameter("userId", userId).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyUsers;
	}

	@SuppressWarnings("unchecked")
	public List<PropertyUser> listUserId(int userId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyUser> propertyUsers = null;
		try {

			propertyUsers = (List<PropertyUser>) session.createQuery("from PropertyUser where user.userId=:userId"
					+ " and isActive = 'true' and isDeleted = 'false'")					
					.setParameter("userId", userId).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyUsers;
	}
	
}
