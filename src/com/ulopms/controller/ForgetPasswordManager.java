package com.ulopms.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;

import com.ulopms.model.ForgetPassword;
import com.ulopms.util.HibernateUtil;


public class ForgetPasswordManager extends HibernateUtil {

	private static final Logger logger = Logger
			.getLogger(ForgetPasswordManager.class);

	public ForgetPassword add(ForgetPassword forgetPassworda) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(forgetPassworda);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return forgetPassworda;
	}

	public ForgetPassword edit(ForgetPassword forgetPassworda) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(forgetPassworda);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return forgetPassworda;
	}

	public ForgetPassword delete(int id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		ForgetPassword forgetPassworda = (ForgetPassword) session.load(
				ForgetPassword.class, id);
		if (null != forgetPassworda) {
			session.delete(forgetPassworda);
		}
		session.getTransaction().commit();
		return forgetPassworda;
	}

	public ForgetPassword find(int id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		ForgetPassword forgetPassworda = (ForgetPassword) session.load(
				ForgetPassword.class, id);
		// session.getTransaction().commit();
		// /session.clear();
		return forgetPassworda;
	}

	@SuppressWarnings("unchecked")
	public List<ForgetPassword> list() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<ForgetPassword> forgetPasswordas = null;
		try {

			forgetPasswordas = (List<ForgetPassword>) session.createQuery(
					"from ForgetPassword").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return forgetPasswordas;
	}

	@SuppressWarnings("unchecked")
	public List<ForgetPassword> listByUser(int userId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<ForgetPassword> forgetPasswordas = null;
		try {

			forgetPasswordas = (List<ForgetPassword>) session
					.createQuery(
							"from ForgetPassword where user.userId=:userId")
							.setParameter("userId", userId).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return forgetPasswordas;
	}

	@SuppressWarnings("unchecked")
	public List<ForgetPassword> listByPasswordLink(String passwordLink) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<ForgetPassword> forgetPasswordas = null;
		try {
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			java.sql.Timestamp createdstamp = new java.sql.Timestamp(time);

			forgetPasswordas = (List<ForgetPassword>) session
					.createQuery(
							"from ForgetPassword where passwordLink=:passwordLink and isActive=true and expiryOn > :createdstamp")
							.setParameter("passwordLink", passwordLink)
							.setTimestamp("createdstamp", createdstamp).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return forgetPasswordas;
	}
}
