package com.ulopms.controller;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;

import com.ulopms.model.PmsSource;
import com.ulopms.model.UserCookies;
import com.ulopms.util.HibernateUtil;


public class UserCookiesManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(UserCookiesManager.class);

	public UserCookies add(UserCookies uerCookies) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(uerCookies);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return uerCookies;
	}

	public PmsSource edit(PmsSource PmsSource) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(PmsSource);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return PmsSource;
	}

	public PmsSource delete(int id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		PmsSource PmsSource = (PmsSource) session.load(PmsSource.class, id);
		if (null != PmsSource) {
			session.delete(PmsSource);
		}
		session.getTransaction().commit();
		return PmsSource;
	}

	public PmsSource find(int id) {
		PmsSource PmsSource = new PmsSource();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			PmsSource = (PmsSource) session.get(PmsSource.class, id);
			// session.getTransaction().commit();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return PmsSource;
	}

	@SuppressWarnings("unchecked")
	public List<UserCookies> list() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<UserCookies> userCookies = null;
		try {

			userCookies = (List<UserCookies>) session.createQuery("from UserCookies where userKey =:key")
					.setParameter("key", "cookie").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return userCookies;
	}
	
	@SuppressWarnings("unchecked")
	public List<PmsSource> list1() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsSource> PmsSources = null;
		try {

			PmsSources = (List<PmsSource>) session.createQuery("from PmsSource where isActive = 'true' and isDeleted = 'false' and "
					+ "resortIsActive = 'true'").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return PmsSources;
	}

	
	@SuppressWarnings("unchecked")
	public List<PmsSource> listAll() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsSource> PmsSources = null;
		try {

			PmsSources = (List<PmsSource>) session.createQuery("from PmsSource where isActive = 'true' and isDeleted = 'false'")
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return PmsSources;
	}
	
	@SuppressWarnings("unchecked")
	 public List<UserCookies> listSearch() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<UserCookies> userCookies = null;
		try {

			userCookies = (List<UserCookies>) session.createQuery("from UserCookies where isActive='true' and isDeleted='false' "
					+ "and isSearchEnable='true' order by cookieId desc")
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return userCookies;
	}

	@SuppressWarnings("unchecked")
	 public List<UserCookies> listSearch(Date startDate, Date endDate,String strFilterDateName) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<UserCookies> userCookies = null;
		try {
			String strBookingQuery=null;
			strBookingQuery="from UserCookies where isActive='true' and isDeleted='false' "
					+ "and isSearchEnable='true' and userIpAddress!='192.168.1.16' and userIpAddress!='192.168.1.9' "
					+ " and userIpAddress!='192.168.1.8' and userIpAddress!='192.168.0.108' and userIpAddress is not null ";
			
					if(strFilterDateName.equalsIgnoreCase("BookingDate")){
						strBookingQuery+=" and searchableDate between :startDate and :endDate ";
					}else if(strFilterDateName.equalsIgnoreCase("CheckInDate")){
						strBookingQuery+=" and userStartDate between :startDate and :endDate ";
					}else if(strFilterDateName.equalsIgnoreCase("CheckOutDate")){
						strBookingQuery+=" and userEndDate between :startDate and :endDate ";
					}else if(strFilterDateName.equalsIgnoreCase("currentDateName")){
						strBookingQuery+=" and searchableDate between :startDate and :endDate  ";
					}
					strBookingQuery+="order by cookieId desc";
					userCookies = (List<UserCookies>) session.createQuery(strBookingQuery)
					.setParameter("startDate", startDate).setParameter("endDate", endDate).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return userCookies;
	}
}
