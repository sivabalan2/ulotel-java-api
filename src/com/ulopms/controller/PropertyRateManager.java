package com.ulopms.controller;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.classic.Session;
import org.hibernate.transform.Transformers;
import org.joda.time.DateTime;

import com.ulopms.model.BookingDetail;
import com.ulopms.model.PmsAvailableRooms;
import com.ulopms.model.PmsPromotionDetails;
import com.ulopms.model.PropertyAccommodation;
import com.ulopms.model.PropertyDiscount;
import com.ulopms.model.PropertyRate;
import com.ulopms.model.PropertyRateDetail;
import com.ulopms.util.HibernateUtil;


public class PropertyRateManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(PropertyRateManager.class);

	public PropertyRate add(PropertyRate propertyRate) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(propertyRate);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyRate;
	}

	public PropertyRate edit(PropertyRate propertyRate) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(propertyRate);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyRate;
	}

	public PropertyRate delete(Long id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		PropertyRate propertyRate = (PropertyRate) session.load(PropertyRate.class, id);
		if (null != propertyRate) {
			session.delete(propertyRate);
		}
		session.getTransaction().commit();
		return propertyRate;
	}

	public PropertyRate find(int id) {
		PropertyRate propertyRate = new PropertyRate();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			propertyRate = (PropertyRate) session.load(PropertyRate.class, id);
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return propertyRate;
	}

	@SuppressWarnings("unchecked")
	public List<PropertyRate> list(int propertyId) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyRate> propertyRate = null;
		try {
              
			propertyRate = (List<PropertyRate>) session.createQuery("from PropertyRate where pmsProperty.propertyId=:propertyId"
					+ " and isActive = 'true' and isDeleted = 'false'").setParameter("propertyId", propertyId).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return propertyRate;
	}
	
	@SuppressWarnings("unchecked")
	public List<PropertyRate> list1(int propertyRateId) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyRate> propertyRate = null;
		try {
              
			propertyRate = (List<PropertyRate>) session.createQuery("from PropertyRate where propertyRateId=:propertyRateId").setParameter("propertyRateId", propertyRateId).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return propertyRate;
	}
	
	@SuppressWarnings("unchecked")
	   public List<PropertyRate> list1(int accommodationId,int sourceId,Date date) {
		   Session session = HibernateUtil.getSessionFactory().getCurrentSession();
	       session.beginTransaction();
	       List<PropertyRate> propertyRate = null;
	       try {
	           
	           propertyRate = (List<PropertyRate>) session.createQuery("from PropertyRate where propertyAccommodation.accommodationId=:accommodationId and pmsSource.sourceId=:sourceId and  (:date between startDate and endDate) and isActive = 'true' and isDeleted = 'false'")
	                 
	                   .setParameter("accommodationId", accommodationId)
	                   .setParameter("sourceId", sourceId)
	                    .setParameter("date", date)
	                    .setMaxResults(1)
	                     .list();        } catch (HibernateException e) {
	           logger.error(e);
	           session.getTransaction().rollback();
	       } finally {
	           session.getTransaction().commit();
	           session=null;
	       }
	       return propertyRate;
	   }
	
	@SuppressWarnings("unchecked")
    public List<PropertyRate> list(int propertyId,int accommodationId) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        List<PropertyRate> propertyRate = null;
        try {
             
            propertyRate = (List<PropertyRate>) session.createQuery("from PropertyRate where pmsProperty.propertyId=:propertyId and"
            		+ " propertyAccommodation.accommodationId=:accommodationId and isActive = 'true' and isDeleted = 'false'")
                    .setParameter("propertyId", propertyId)
                    .setParameter("accommodationId", accommodationId).list();

        } catch (HibernateException e) {
            logger.error(e);
            session.getTransaction().rollback();
        } finally {
            session.getTransaction().commit();
            session=null;
        }
        return propertyRate;
    }
	
	@SuppressWarnings("unchecked")
	public List<PropertyRate> list(int accommodationId, int sourceId,Date date) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyRate> propertyRates = null;
		try {
			propertyRates = (List<PropertyRate>) session.createQuery("select a from PropertyRate a join a.propertyAccommodation b join a.pmsSource c "
					+ "where b.accommodationId=:accommodationId and c.sourceId=:sourceId and :date between a.startDate and a.endDate"
					+ " and a.isActive = true order by a.propertyRateId DESC")
					.setParameter("accommodationId", accommodationId)
					.setParameter("sourceId", sourceId)
					.setParameter("date", date)
					.setMaxResults(1)
					.list();
			

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyRates;
	}

	@SuppressWarnings("unchecked")
	public List<PropertyRate> list(int propertyId,int accommodationId,int sourceId,Date start,Date end) {
			Session session = HibernateUtil.getSessionFactory().getCurrentSession();
	        session.beginTransaction();
	        List<PropertyRate> propertyDate = null;
	        try {
	             
	        	propertyDate=(List<PropertyRate>)session.createQuery("from PropertyRate propertyRate"
		        			+ " where propertyRate.pmsProperty.propertyId=:propertyId and propertyRate.propertyAccommodation.accommodationId=:accommodationId "
		        			+ " and pmsSource.sourceId=:sourceId and propertyRate.startDate>=:startDate and propertyRate.endDate<=:endDate "
		        			+ " and propertyRate.isActive = 'true' and propertyRate.isDeleted = 'false' order by propertyRate.propertyRateId desc")
		        			.setParameter("propertyId", propertyId)
		        			.setParameter("accommodationId", accommodationId)
		        			.setParameter("sourceId", sourceId)
		        			.setParameter("startDate", start)
		        			.setParameter("endDate", end).list();

	        } catch (HibernateException e) {
	            logger.error(e);
	            session.getTransaction().rollback();
	        } finally {
	            session.getTransaction().commit();
	            session=null;
	        }
	        return propertyDate;
	    }
	
	@SuppressWarnings("unchecked")
	public List<PropertyAccommodation> list(int propertyId, String datesa) throws ParseException {
		Date dates=new SimpleDateFormat("yyyy-MM-dd").parse(datesa); 
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyAccommodation> accommodationRates = null;
		try {

			accommodationRates = (List<PropertyAccommodation>) session.createQuery("select bd.bookingDate from BookingDetail bd,PropertyAccommodation pa "
					+ "where bd.bookingDate=:dates and pa.pmsProperty.propertyId=:propertyId and bd.propertyAccommodation.accommodationId = pa.accommodationId "
					+ "group by bd.bookingDate,bd.bookingDetailsId")
					.setParameter("propertyId", propertyId)
					.setParameter("dates", dates)
					.list();
		} catch (HibernateException e) {
			logger.error(e);  
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return accommodationRates;
	}
	
	@SuppressWarnings("unchecked")
	public List<PropertyRate> listDateDetails(int propertyId,int accommodationId,Date checkedDate) {
			Session session = HibernateUtil.getSessionFactory().getCurrentSession();
	        session.beginTransaction();
	        List<PropertyRate> propertyDate = null;
	        try {
	             
	        	propertyDate=(List<PropertyRate>)session.createQuery("from PropertyRate propertyRate"
		        			+ " where propertyRate.pmsProperty.propertyId=:propertyId and propertyRate.propertyAccommodation.accommodationId=:accommodationId "
		        			+ " and :date between propertyRate.startDate and propertyRate.endDate and smartPriceIsActive='false'"
		        			+ " and propertyRate.isActive = 'true' and propertyRate.isDeleted = 'false' order by propertyRate.propertyRateId desc")
		        			.setParameter("propertyId", propertyId)
		        			.setParameter("accommodationId", accommodationId)
		        			.setParameter("date", checkedDate)
		        			.list();

	        } catch (HibernateException e) {
	            logger.error(e);
	            session.getTransaction().rollback();
	        } finally {
	            session.getTransaction().commit();
	            session=null;
	        }
	        return propertyDate;
	    }
	
	@SuppressWarnings("unchecked")
	public List<PropertyRate> listSmartPriceCheckDetails(int propertyId,int accommodationId,Date checkedDate) {
	        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
	        session.beginTransaction();
	        List<PropertyRate> propertySmartPriceCheck = null;
	        try {
	             
	        	propertySmartPriceCheck=(List<PropertyRate>)session.createQuery("from PropertyRate propertyRate"
		        			+ " where propertyRate.pmsProperty.propertyId=:propertyId and propertyRate.propertyAccommodation.accommodationId=:accommodationId "
		        			+ " and :date between propertyRate.startDate and propertyRate.endDate and propertyRate.isActive = 'true' and "
		        			+ " propertyRate.isDeleted = 'false' and propertyRate.smartPriceIsActive='true' order by propertyRate.propertyRateId desc")
		        			.setParameter("propertyId", propertyId)
		        			.setParameter("accommodationId", accommodationId)
		        			.setParameter("date", checkedDate)
		        			.list();

	        } catch (HibernateException e) {
	            logger.error(e);
	            session.getTransaction().rollback();
	        } finally {
	            session.getTransaction().commit();
	            session=null;
	        }
	        return propertySmartPriceCheck;
	    }
	
	
	
	@SuppressWarnings("unchecked")
	public List<PropertyRate> listSmartPrices(int propertyId,Date curdate) {
			Session session = HibernateUtil.getSessionFactory().getCurrentSession();
	        session.beginTransaction();
	        
	        List<PropertyRate> propertySmartPriceCheck = null;
	        try {
	             
	        	propertySmartPriceCheck=(List<PropertyRate>)session.createQuery("from PropertyRate propertyRate"
		        			+ " where propertyRate.pmsProperty.propertyId=:propertyId and propertyRate.isActive = 'true' and "
		        			+ " propertyRate.isDeleted = 'false' and propertyRate.smartPriceIsActive='true' "
		        			+ " and (propertyRate.startDate>=:startDate or propertyRate.endDate>=:startDate) order by propertyRate.propertyRateId desc")
		        			.setParameter("propertyId", propertyId)
		        			.setParameter("startDate", curdate)
		        			.list();

	        } catch (HibernateException e) {
	            logger.error(e);
	            session.getTransaction().rollback();
	        } finally {
	            session.getTransaction().commit();
	            session=null;
	        }
	        return propertySmartPriceCheck;
	    }
	
	@SuppressWarnings("unchecked")
	public List<PropertyRate> listPropertyRates(int propertyId,Date curdate) {
			Session session = HibernateUtil.getSessionFactory().getCurrentSession();
	        session.beginTransaction();
	        List<PropertyRate> propertySmartPriceCheck = null;
	        try {
	             
	        	propertySmartPriceCheck=(List<PropertyRate>)session.createQuery("from PropertyRate propertyRate"
		        			+ " where propertyRate.pmsProperty.propertyId=:propertyId and propertyRate.isActive = 'true' "
		        			+ " and propertyRate.isDeleted = 'false' and propertyRate.smartPriceIsActive='false' and "
		        			+ " (propertyRate.startDate>=:startDate or propertyRate.endDate>=:endDate) "
		        			+ " order by propertyRate.propertyRateId desc")
		        			.setParameter("propertyId", propertyId)
		        			.setParameter("startDate", curdate)
		        			.setParameter("endDate", curdate)
		        			.list();

	        } catch (HibernateException e) {
	            logger.error(e);
	            session.getTransaction().rollback();
	        } finally {
	            session.getTransaction().commit();
	            session=null;
	        }
	        return propertySmartPriceCheck;
	    }
	
	@SuppressWarnings("unchecked")
	public List<PropertyRate> listActiveSmartPricesCheck(int propertyRateId) {
			Session session = HibernateUtil.getSessionFactory().getCurrentSession();
	        session.beginTransaction();
	        List<PropertyRate> propertySmartPriceCheck = null;
	        try {
	             
	        	propertySmartPriceCheck=(List<PropertyRate>)session.createQuery("from PropertyRate propertyRate"
		        			+ " where propertyRate.propertyRateId=:propertyRateId and propertyRate.isActive = 'true' and "
		        			+ " propertyRate.isDeleted = 'false' and propertyRate.smartPriceIsActive='true' order by propertyRate.propertyRateId desc")
		        			.setParameter("propertyRateId", propertyRateId)
		        			.list();

	        } catch (HibernateException e) {
	            logger.error(e);
	            session.getTransaction().rollback();
	        } finally {
	            session.getTransaction().commit();
	            session=null;
	        }
	        return propertySmartPriceCheck;
	    }
	
	public PropertyRate listRates(int propertyRateId) {
		PropertyRate rates = new PropertyRate();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			rates = (PropertyRate) session.createQuery(" from PropertyRate pr  where pr.propertyRateId=:propertyRateId "
					+ "and isActive = 'true' and isDeleted = 'false' ").setParameter("propertyRateId", propertyRateId)
			.setFirstResult(0)
			.setMaxResults(1)
			.uniqueResult();
			
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return rates;
	}
	
	@SuppressWarnings("unchecked")
	public List<PropertyRate> listSmartPriceActive(int propertyId,Date fromdate,Date enddate) {
			Session session = HibernateUtil.getSessionFactory().getCurrentSession();
	        session.beginTransaction();
	        List<PropertyRate> propertySmartPriceCheck = null;
	        try {
	             
	        	propertySmartPriceCheck=(List<PropertyRate>)session.createQuery("from PropertyRate propertyRate"
		        			+ " where propertyRate.pmsProperty.propertyId=:propertyId and propertyRate.isActive = 'true' "
		        			+ " and propertyRate.isDeleted = 'false' and propertyRate.smartPriceIsActive='true' and "
		        			+ " (propertyRate.startDate>=:startDate or propertyRate.endDate>=:endDate) ")
		        			.setParameter("propertyId", propertyId)
		        			.setParameter("startDate", fromdate)
		        			.setParameter("endDate", enddate)
		        			.list();

	        } catch (HibernateException e) {
	            logger.error(e);
	            session.getTransaction().rollback();
	        } finally {
	            session.getTransaction().commit();
	            session=null;
	        }
	        return propertySmartPriceCheck;
	    }

	@SuppressWarnings("unchecked")
	public List<PropertyRate> listAllDates(int propertyId,int accommodationId,int sourceTypeId,Date start) {
	        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
	        session.beginTransaction();
	        List<PropertyRate> propertyDate = null;
	        try {
	             
	        	propertyDate=(List<PropertyRate>)session.createQuery("from PropertyRate propertyRate"
		        			+ " where propertyRate.pmsProperty.propertyId=:propertyId and propertyRate.propertyAccommodation.accommodationId=:accommodationId "
		        			+ " and pmsSourceType.sourceTypeId=:sourceTypeId and :startDate between propertyRate.startDate and propertyRate.endDate "
		        			+ " and propertyRate.isActive = 'true' and propertyRate.isDeleted = 'false' order by propertyRate.propertyRateId desc")
		        			.setParameter("propertyId", propertyId)
		        			.setParameter("accommodationId", accommodationId)
		        			.setParameter("sourceTypeId", sourceTypeId)
		        			.setParameter("startDate", start)
		        			.list();

	        } catch (HibernateException e) {
	            logger.error(e);
	            session.getTransaction().rollback();
	        } finally {
	            session.getTransaction().commit();
	            session = null;
	        }
	        return propertyDate;
	    }

	@SuppressWarnings("unchecked")
	public List<PropertyRate> listSmartPriceActive(int propertyId,Date date) {
	        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
	        session.beginTransaction();
	        List<PropertyRate> propertySmartPriceCheck = null;
	        try {
	             
	        	propertySmartPriceCheck=(List<PropertyRate>)session.createQuery("from PropertyRate propertyRate"
		        			+ " where propertyRate.pmsProperty.propertyId=:propertyId and propertyRate.isActive = 'true' "
		        			+ " and propertyRate.isDeleted = 'false' and propertyRate.smartPriceIsActive='true' and "
		        			+ " :date between propertyRate.startDate and propertyRate.endDate ")
		        			.setParameter("propertyId", propertyId)
		        			.setParameter("date", date)
		        			.list();

	        } catch (HibernateException e) {
	            logger.error(e);
	            session.getTransaction().rollback();
	        } finally {
	            session.getTransaction().commit();
	            session = null;
	        }
	        return propertySmartPriceCheck;
	    }

	@SuppressWarnings("unchecked")
	public List<PropertyRate> listCurrentSmartPriceActive(int propertyId,Date date) {
	        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
	        session.beginTransaction();
	        List<PropertyRate> propertySmartPriceCheck = null;
	        try {
	             
	        	propertySmartPriceCheck=(List<PropertyRate>)session.createQuery("from PropertyRate propertyRate"
		        			+ " where propertyRate.pmsProperty.propertyId=:propertyId and propertyRate.isActive = 'true' "
		        			+ " and propertyRate.isDeleted = 'false' and propertyRate.smartPriceIsActive='true' and "
		        			+ " :date between propertyRate.startDate and propertyRate.endDate ")
		        			.setParameter("propertyId", propertyId)
		        			.setParameter("date", date)
		        			.list();

	        } catch (HibernateException e) {
	            logger.error(e);
	            session.getTransaction().rollback();
	        } finally {
	            session.getTransaction().commit();
	            session = null;
	        }
	        return propertySmartPriceCheck;
	    }
	@SuppressWarnings("unchecked")
	public List<PropertyRate> listCurrentSmartPriceActive(int propertyId,int accommId,Date date) {
	        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
	        session.beginTransaction();
	        List<PropertyRate> propertySmartPriceCheck = null;
	        try {
	             
	        	propertySmartPriceCheck=(List<PropertyRate>)session.createQuery("from PropertyRate propertyRate"
		        			+ " where propertyRate.pmsProperty.propertyId=:propertyId and propertyRate.isActive = 'true' "
		        			+ " and propertyRate.isDeleted = 'false' and propertyRate.smartPriceIsActive='true' and "
		        			+ " :date between propertyRate.startDate and propertyRate.endDate and propertyRate.propertyAccommodation.accommodationId=:accommId ")
		        			.setParameter("propertyId", propertyId)
		        			.setParameter("date", date)
		        			.setParameter("accommId", accommId)
		        			.list();

	        } catch (HibernateException e) {
	            logger.error(e);
	            session.getTransaction().rollback();
	        } finally {
	            session.getTransaction().commit();
	            session = null;
	        }
	        return propertySmartPriceCheck;
	    }
	
	@SuppressWarnings("unchecked")
	public List<PropertyRate> listAllDates(int propertyId) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        List<PropertyRate> propertyDate = null;
        try {
             
        	propertyDate=(List<PropertyRate>)session.createQuery("from PropertyRate propertyRate"
	        			+ " where propertyRate.pmsProperty.propertyId=:propertyId "
	        			+ " and propertyRate.isActive = 'true' and propertyRate.isDeleted = 'false' "
	        			+ "order by propertyRate.propertyRateId desc")
	        			.setParameter("propertyId", propertyId)
	        			.list();

        } catch (HibernateException e) {
            logger.error(e);
            session.getTransaction().rollback();
        } finally {
            session.getTransaction().commit();
            session = null;
        }
        return propertyDate;
    }
	
	@SuppressWarnings("unchecked")
	public List<PropertyRate> listRates(int propertyId,int accommodationId,int sourceId,Date start,Date end) {
			Session session = HibernateUtil.getSessionFactory().getCurrentSession();
	        session.beginTransaction();
	        List<PropertyRate> propertyDate = null;
	        try {
	             
	        	propertyDate=(List<PropertyRate>)session.createQuery("from PropertyRate propertyRate"
		        			+ " where propertyRate.pmsProperty.propertyId=:propertyId and propertyRate.propertyAccommodation.accommodationId=:accommodationId "
		        			+ " and pmsSource.sourceId=:sourceId and :startDate between propertyRate.startDate and propertyRate.endDate and :endDate between propertyRate.startDate and propertyRate.endDate "
		        			+ " and propertyRate.isActive = 'true' and propertyRate.isDeleted = 'false' order by propertyRate.propertyRateId desc")
		        			.setParameter("propertyId", propertyId)
		        			.setParameter("accommodationId", accommodationId)
		        			.setParameter("sourceId", sourceId)
		        			.setParameter("startDate", start)
		        			.setParameter("endDate", end).list();

	        } catch (HibernateException e) {
	            logger.error(e);
	            session.getTransaction().rollback();
	        } finally {
	            session.getTransaction().commit();
	            session=null;
	        }
	        return propertyDate;
	    }
}
