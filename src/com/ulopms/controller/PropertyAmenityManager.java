package com.ulopms.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;

import com.ulopms.model.PmsAmenity;
import com.ulopms.model.PropertyAmenity;
import com.ulopms.util.HibernateUtil;


public class PropertyAmenityManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(PropertyAmenityManager.class);

	public PropertyAmenity add(PropertyAmenity propertyAmenity) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(propertyAmenity);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyAmenity;
	}

	public PropertyAmenity edit(PropertyAmenity propertyAmenity) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(propertyAmenity);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyAmenity;
	}

	public PropertyAmenity delete(Long id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		PropertyAmenity propertyAmenity = (PropertyAmenity) session.load(PropertyAmenity.class, id);
		if (null != propertyAmenity) {
			session.delete(propertyAmenity);
		}
		session.getTransaction().commit();
		return propertyAmenity;
	}

	public PropertyAmenity find(Integer id) {
		PropertyAmenity propertyAmenity = new PropertyAmenity();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			propertyAmenity = (PropertyAmenity) session.load(PropertyAmenity.class, id);
			// session.getTransaction().commit();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return propertyAmenity;
	}

	@SuppressWarnings("unchecked")
	public List<PropertyAmenity> list() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyAmenity> propertyAmenitys = null;
		try {

			propertyAmenitys = (List<PropertyAmenity>) session.createQuery("from PropertyAmenity").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyAmenitys;
	}
	
	@SuppressWarnings("unchecked")
	public List<PropertyAmenity> list(int propertyId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyAmenity> propertyAmenitys = null;
		try {

			propertyAmenitys = (List<PropertyAmenity>) session.createQuery("from PropertyAmenity where pmsProperty.propertyId=:propertyId and "
					+ "isDeleted = 'false' and isActive = 'true' and isTagAmenity='false' ").setParameter("propertyId", propertyId).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyAmenitys;
	}
	
	@SuppressWarnings("unchecked")
	public List<PropertyAmenity> listTwo(int propertyId,int amenityId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyAmenity> propertyAmenitys = null;
		try {

			propertyAmenitys = (List<PropertyAmenity>) session.createQuery("from PropertyAmenity where pmsProperty.propertyId=:propertyId "
					+ "and pmsAmenity.amenityId=:amenityId and isDeleted = 'false' and isActive = 'true' and isTagAmenity='false' ")
					.setParameter("propertyId", propertyId)
					.setParameter("amenityId", amenityId).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyAmenitys;
	}
	
	@SuppressWarnings("unchecked")
	public List<PropertyAmenity> list(int propertyId,int amenityId,int tagId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyAmenity> propertyAmenitys = null;
		try {

			propertyAmenitys = (List<PropertyAmenity>) session.createQuery("from PropertyAmenity where pmsProperty.propertyId=:propertyId "
					+ "and pmsAmenity.amenityId=:amenityId and isDeleted = 'false' and isActive = 'true' and pmsTags.tagId=:tagId and isTagAmenity='true'")
					.setParameter("propertyId", propertyId).setParameter("tagId", tagId)
					.setParameter("amenityId", amenityId).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyAmenitys;
	}
	
	@SuppressWarnings("unchecked")
	public List<PropertyAmenity> list(int propertyId,int tagId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyAmenity> propertyAmenitys = null;
		try {

			propertyAmenitys = (List<PropertyAmenity>) session.createQuery("from PropertyAmenity where pmsProperty.propertyId=:propertyId and "
					+ "isDeleted = 'false' and isActive = 'true' and pmsTags.tagId=:tagId and isTagAmenity= 'true' ")
					.setParameter("tagId", tagId).setParameter("propertyId", propertyId).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyAmenitys;
	}
	
	@SuppressWarnings("unchecked")
	public List<PropertyAmenity> listTags(int tagId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyAmenity> propertyAmenitys = null;
		try {

			propertyAmenitys = (List<PropertyAmenity>) session.createQuery("from PropertyAmenity where "
					+ "isDeleted = 'false' and isActive = 'true' and pmsTags.tagId=:tagId and isTagAmenity= 'true' ")
					.setParameter("tagId", tagId).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyAmenitys;
	}
}
