package com.ulopms.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;

import com.ulopms.model.BlogCategories;
import com.ulopms.model.PmsProperty;
import com.ulopms.util.HibernateUtil;


public class BlogCategoriesManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(BlogCategoriesManager.class);

	public BlogCategories add(BlogCategories blogCategories) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(blogCategories);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return blogCategories;
	}

	public BlogCategories edit(BlogCategories blogCategories) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(blogCategories);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return blogCategories;
	}

	public BlogCategories delete(int id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		BlogCategories blogCategories = (BlogCategories) session.load(BlogCategories.class, id);
		if (null != blogCategories) {
			session.delete(blogCategories);
		}
		session.getTransaction().commit();
		return blogCategories;
	}

	public BlogCategories find(int id) {
		BlogCategories blogCategories = new BlogCategories();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			blogCategories = (BlogCategories) session.load(BlogCategories.class, id);
			// session.getTransaction().commit();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return blogCategories;
	}
	
	@SuppressWarnings("unchecked")
	public List<BlogCategories> list(int limit,int offset) {

		//Session session = HibernateUtil.getSessionFactory().openSession();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<BlogCategories> blogCategories = null;
		try {

			blogCategories = (List<BlogCategories>) session.createQuery("from BlogCategories where isActive = 'true' "
					+ "and isDeleted = 'false' ")
					.setFirstResult(offset)
					.setMaxResults(limit)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return blogCategories;
	}
	
	@SuppressWarnings("unchecked")
	public List<BlogCategories> list() {

		//Session session = HibernateUtil.getSessionFactory().openSession();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<BlogCategories> blogCategories = null;
		try {

			blogCategories = (List<BlogCategories>) session.createQuery("from BlogCategories where isActive = 'true' "
					+ "and isDeleted = 'false' ")					
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return blogCategories;
	}
	
	@SuppressWarnings("unchecked")
	public List<BlogCategories> listBlogCategoryUrl(String categoryUrl) {

		//Session session = HibernateUtil.getSessionFactory().openSession();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<BlogCategories> blogCategories = null;
		try {

			blogCategories = (List<BlogCategories>) session.createQuery("from BlogCategories where isActive = 'true' "
					+ "and isDeleted = 'false' and blogCategoryUrl=:categoryUrl")					
					.setParameter("categoryUrl", categoryUrl).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return blogCategories;
	}
	
	/*@SuppressWarnings("unchecked")
	public List<PromotionImages> findId(int promotionImageId) {
		List<PromotionImages> promotionImages = null;
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			//bookingDetails = (List<BookingDetail>) session.createQuery("from BookingDetail").list();
			promotionImages = (List<PromotionImages>)session.createQuery("from PromotionImages where promotionImageId =:promotionImageId")
				    .setParameter("promotionImageId",promotionImageId)
				    .list();
			// session.getTransaction().commit();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return promotionImages;
	} 

	@SuppressWarnings("unchecked")
	public List<PromotionImages> list() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PromotionImages> promotionImages = null;
		try {

			promotionImages = (List<PromotionImages>) session.createQuery("from PromotionImages where isActive = true and isDeleted =false ").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return promotionImages;
	}	
	*/
	
	

	
}
