package com.ulopms.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;

import com.ulopms.model.Area;
import com.ulopms.model.GoogleArea;
import com.ulopms.model.GoogleLocation;
import com.ulopms.model.GooglePropertyAreas;
import com.ulopms.model.Location;
import com.ulopms.model.PropertyAreas;
import com.ulopms.util.HibernateUtil;


public class GooglePlaceManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(GooglePlaceManager.class);

	
	public GoogleLocation add(GoogleLocation googleLocation) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(googleLocation);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return googleLocation;
	}
	
	public GoogleArea add(GoogleArea googleArea) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(googleArea);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return googleArea;
	}
	
	public GooglePropertyAreas add(GooglePropertyAreas googlePropertyAreas) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(googlePropertyAreas);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return googlePropertyAreas;
	}

	
	public GoogleLocation edit(GoogleLocation googleLocation) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(googleLocation);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return googleLocation;
	}
	
	public GoogleArea edit(GoogleArea googleArea) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(googleArea);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return googleArea;
	}
	
	public GooglePropertyAreas edit(GooglePropertyAreas googlePropertyAreas) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(googlePropertyAreas);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return googlePropertyAreas;
	}

	public Location delete(int id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Location location = (Location) session.load(Location.class, id);
		if (null != location) {
			session.delete(location);
		}
		session.getTransaction().commit();
		return location;
	}

	public GoogleLocation find(int id) {
		GoogleLocation googleLocation = new GoogleLocation();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			googleLocation = (GoogleLocation) session.load(GoogleLocation.class, id);
			// session.getTransaction().commit();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return googleLocation;
	}
	
	public GoogleArea findArea(int id) {
		GoogleArea googleArea = new GoogleArea();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			googleArea = (GoogleArea) session.load(GoogleArea.class, id);
			// session.getTransaction().commit();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return googleArea;
	}
	
	public GooglePropertyAreas findProperty(int id) {
		GooglePropertyAreas googlePropertyArea = new GooglePropertyAreas();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			googlePropertyArea = (GooglePropertyAreas) session.load(GooglePropertyAreas.class, id);
			// session.getTransaction().commit();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return googlePropertyArea;
	}

	
	public GoogleLocation findLocation(String placeId) {
//		double laat = Double.parseDouble(lat);
//		double loog = Double.parseDouble(log);
     	GoogleLocation googleLocation = new GoogleLocation();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			googleLocation = (GoogleLocation) session.createQuery("from GoogleLocation where googleLocationPlaceId =:placeId "
					+ " and isActive = 'true' and isDeleted = 'false'")
					.setParameter("placeId", placeId)
					.uniqueResult();
			// session.getTransaction().commit();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return googleLocation;
	}
	
	public GoogleArea findArea(String placeId) {

		GoogleArea googleArea = new GoogleArea();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			googleArea = (GoogleArea) session.createQuery("from GoogleArea where googleAreaPlaceId =:placeId "
					+ " and isActive = 'true' and isDeleted = 'false'")
					.setParameter("placeId", placeId)
					.uniqueResult();
			// session.getTransaction().commit();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return googleArea;
	}
	
	@SuppressWarnings("unchecked")
	public List<GoogleLocation> list() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<GoogleLocation> googleLocation = null;
		try {

			googleLocation = (List<GoogleLocation>) session.createQuery("from GoogleLocation where isActive = 'true' and isDeleted = 'false'").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return googleLocation;
	}
	
	@SuppressWarnings("unchecked")
	public List<GoogleArea> listArea() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<GoogleArea> googleArea = null;
		try {

			googleArea = (List<GoogleArea>) session.createQuery("from GoogleArea where isActive = 'true' and isDeleted = 'false'").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return googleArea;
	}
	
	@SuppressWarnings("unchecked")
	public List<GooglePropertyAreas> listAreaProperties(int googleAreaId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<GooglePropertyAreas> googlePropertyAreas = null;
		try {

			googlePropertyAreas = (List<GooglePropertyAreas>) session.createQuery("from GooglePropertyAreas "
					+ "where googleArea.googleAreaId=:googleAreaId and isActive = 'true' and isDeleted = 'false'")
					.setParameter("googleAreaId", googleAreaId).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return googlePropertyAreas;
	}
	
	@SuppressWarnings("unchecked")
	public List<GoogleArea> listInactivaArea() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<GoogleArea> googleArea = null;
		try {

			googleArea = (List<GoogleArea>) session.createQuery("from GoogleArea where isActive = 'false' and isDeleted = 'false'").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return googleArea;
	}
	
	@SuppressWarnings("unchecked")
	public List<GoogleArea> listInactiveArea(int limit,int offset) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<GoogleArea> googleArea = null;
		try {

			googleArea = (List<GoogleArea>) session.createQuery("from GoogleArea where isActive = 'false'"
					+ " and isDeleted = 'false'")
					.setFirstResult(offset)
					.setMaxResults(limit)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return googleArea;
	}
	
	@SuppressWarnings("unchecked")
	public List<GoogleLocation> list(int googleLocationId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<GoogleLocation> googleLocation = null;
		try {

			googleLocation = (List<GoogleLocation>) session.createQuery("from GoogleLocation where googleLocationId=:googleLocationId and "
					+ " isActive = 'true' and isDeleted = 'false'")
					.setParameter("googleLocationId", googleLocationId)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return googleLocation;
	}
	
	@SuppressWarnings("unchecked")
	public List<GooglePropertyAreas> listArea(int propertyId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<GooglePropertyAreas> googlePropertyAreas = null;
		try {

			googlePropertyAreas = (List<GooglePropertyAreas>) session.createQuery("from GooglePropertyAreas "
					+ "where pmsProperty.propertyId=:propertyId and isDeleted = 'false' "
					+ "and isActive = 'true'")
					.setParameter("propertyId", propertyId).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return googlePropertyAreas;
	}
	
	@SuppressWarnings("unchecked")
	public List<GoogleLocation> list(int limit,int offset) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<GoogleLocation> googleLocation = null;
		try {

			googleLocation = (List<GoogleLocation>) session.createQuery("from GoogleLocation where isActive = 'true'"
					+ " and isDeleted = 'false'")
					.setFirstResult(offset)
					.setMaxResults(limit)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return googleLocation;
	}

	@SuppressWarnings("unchecked")
	public List<GoogleArea> listAreas(int limit,int offset) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<GoogleArea> googleArea = null;
		try {

			googleArea = (List<GoogleArea>) session.createQuery("from GoogleArea where isActive = 'true'"
					+ " and isDeleted = 'false'")
					.setFirstResult(offset)
					.setMaxResults(limit)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return googleArea;
	}

	
	@SuppressWarnings("unchecked")
	public List<GoogleLocation> listLocationUrl(String locationUrl) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<GoogleLocation> googleLocation = null;
		try {

			googleLocation = (List<GoogleLocation>) session.createQuery("from GoogleLocation where isActive = 'true' "
					+ "and isDeleted = 'false' and googleLocationUrl=:locationUrl")
					.setParameter("locationUrl", locationUrl).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return googleLocation;
	}
	
	@SuppressWarnings("unchecked")
	public List<GoogleArea> listAreaUrl(String areaUrl) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<GoogleArea> googleArea = null;
		try {

			googleArea = (List<GoogleArea>) session.createQuery("from GoogleArea where isActive = 'true' "
					+ "and isDeleted = 'false' and googleAreaUrl=:areaUrl")
					.setParameter("areaUrl", areaUrl).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return googleArea;
	}
	
	@SuppressWarnings("unchecked")
	public List<GoogleArea> listGoogleLocationArea(int googleLocationId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<GoogleArea> googleArea = null;
		try {

			googleArea = (List<GoogleArea>) session.createQuery("from GoogleArea where isActive = 'true'"
					+ " and isDeleted = 'false' and googleLocation.googleLocationId=:googleLocationId ")
					.setParameter("googleLocationId", googleLocationId).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return googleArea;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<GooglePropertyAreas> listPropertyArea(int propertyId,int areaId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<GooglePropertyAreas> googlePropertyAreas = null;
		try {

			googlePropertyAreas = (List<GooglePropertyAreas>) session.createQuery("from GooglePropertyAreas "
					+ "where pmsProperty.propertyId=:propertyId and googleArea.googleAreaId=:areaId and isDeleted = 'false' "
					+ "and isActive = 'true'").setParameter("propertyId", propertyId)
					.setParameter("areaId", areaId).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return googlePropertyAreas;
	}
	
	@SuppressWarnings("unchecked")
	public List<GoogleArea> listAreas() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<GoogleArea> googleArea = null;
		try {

			googleArea = (List<GoogleArea>) session.createQuery("from GoogleArea where isActive = 'true'"
					+ " and isDeleted = 'false'").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return googleArea;
	}
	
	@SuppressWarnings("unchecked")
	public List<GooglePropertyAreas> listPropertyArea(int areaId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<GooglePropertyAreas> googlePropertyAreas = null;
		try {

			googlePropertyAreas = (List<GooglePropertyAreas>) session.createQuery("from GooglePropertyAreas "
					+ "where googleArea.googleAreaId=:areaId and isDeleted = 'false' "
					+ "and isActive = 'true'")
					.setParameter("areaId", areaId).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return googlePropertyAreas;
	}
}
