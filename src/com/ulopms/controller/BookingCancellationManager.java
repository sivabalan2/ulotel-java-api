package com.ulopms.controller;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;
import org.hibernate.transform.Transformers;
import org.joda.time.DateTime;

import com.ulopms.model.BookingDetail;
import com.ulopms.model.BookingDetailReport;
import com.ulopms.model.BookingGuestDetail;
import com.ulopms.model.DashBoard;
import com.ulopms.model.Revenue;
import com.ulopms.model.RevenueReport;
import com.ulopms.model.Screen;
import com.ulopms.model.UserGuestDetailReport;
import com.ulopms.util.HibernateUtil;


public class BookingCancellationManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(BookingCancellationManager.class);

	
	@SuppressWarnings("unchecked")
	public List<Screen> list() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<Screen> screens = null;
		try {

			screens = (List<Screen>) session.createQuery("from Screen").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return screens;
	}

	@SuppressWarnings("unchecked")
	public List<BookingDetail> BookingDetails() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<BookingDetail> bookingDetails = null;
		try {
			bookingDetails = (List<BookingDetail>)session.createQuery("from BookingDetail").list();
			//screens = (List<Screen>) session.createSQLQuery(sql).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return bookingDetails;
	}
	@SuppressWarnings("unchecked")
	public List<BookingDetail> BookingDetailsByProperty(int accommodationId, Date fromDate, Date toDate, int propertyId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<BookingDetail> bookingDetails = null;
		try {
			if(accommodationId>0)
			{
				bookingDetails = (List<BookingDetail>)session.createQuery("select a from BookingDetail a join a.pmsBooking b"
						+ " where a.bookingDate between :fromDate and :toDate and "
					+ "a.propertyAccommodation.accommodationId=:accommodationId and b.pmsProperty.propertyId=:propertyId")
					.setParameter("accommodationId", accommodationId)
					.setParameter("fromDate", fromDate)
					.setParameter("toDate", toDate)
					.setParameter("propertyId", propertyId)
					.list();
			}
			else
			{
				bookingDetails = (List<BookingDetail>)session.createQuery("select a from BookingDetail a join a.pmsBooking b "
						+ "where a.bookingDate between :fromDate and :toDate and "
					+ "b.pmsProperty.propertyId=:propertyId")
						.setParameter("fromDate", fromDate)
						.setParameter("toDate", toDate)
						.setParameter("propertyId", propertyId)
						.list();
			}
			//screens = (List<Screen>) session.createSQLQuery(sql).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return bookingDetails;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<BookingDetailReport> listBooking(int propertyId,Date start,Date end,int accommodationId) throws ParseException {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<BookingDetailReport> bookingDetails = null;
		try {
			String strBookingQuery=null;
			strBookingQuery="select pb.bookingId as bookingId,bd.pmsStatus.statusId as statusId, sum(bd.roomCount) as roomCount, pb.arrivalDate as arrivalDate,"
					+ "pb.departureDate as departureDate,pg.firstName as firstName,pg.lastName as lastName,coalesce(sum(bd.amount),0) as amount,"
					+ "coalesce(sum(bd.tax),0) as taxAmount, pg.city as city,"
					+ "pg.phone as mobileNumber,pg.landline as phoneNumber,pg.emailId as emailId,bd.propertyAccommodation.accommodationId as accommodationId "
					+ "from PmsBooking pb,BookingDetail bd,BookingGuestDetail bgd,PmsGuest pg  "
					+ " where pb.pmsProperty.propertyId=:propertyId "
					+ " and pb.bookingId=bd.pmsBooking.bookingId "
					+ " and pb.bookingId=bgd.pmsBooking.bookingId "
					+ " and pg.guestId=bgd.pmsGuest.guestId and bd.pmsStatus.statusId <> :statusId ";
					
					if(accommodationId>0){
						strBookingQuery+=" and bd.propertyAccommodation.accommodationId=:accommodationId "
								+ " and pb.arrivalDate>=:arrivalDate and pb.departureDate<=:departureDate ";
					}else{
						strBookingQuery+=" and pb.arrivalDate>=:arrivalDate and pb.departureDate<=:departureDate ";
					}
					strBookingQuery+= " group by pb.bookingId,bd.pmsStatus.statusId, bd.roomCount, pb.arrivalDate,pb.departureDate,"
					+ "pg.firstName,pg.lastName,bd.amount,bd.tax,pg.city,pg.phone,pg.landline,pg.emailId,bd.propertyAccommodation.accommodationId  "
					+ " order by pb.bookingId DESC";
					
							
					if(accommodationId>0){
						bookingDetails = (List<BookingDetailReport>) session.createQuery(strBookingQuery)
								.setParameter("propertyId", propertyId)
								.setParameter("arrivalDate", start)
								.setParameter("departureDate", end)
								.setParameter("statusId", 4)
								.setParameter("accommodationId", accommodationId)
								.setResultTransformer(Transformers.aliasToBean(BookingDetailReport.class)).list();
					}
					else{
						bookingDetails = (List<BookingDetailReport>) session.createQuery(strBookingQuery)
								.setParameter("propertyId", propertyId)
								.setParameter("arrivalDate", start)
								.setParameter("departureDate", end)
								.setParameter("statusId", 4)
								.setResultTransformer(Transformers.aliasToBean(BookingDetailReport.class)).list();
					}
			
		} catch (HibernateException e) {
			logger.error(e);  
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return bookingDetails;
	}
	
	@SuppressWarnings("unchecked")
	public List<BookingDetailReport> listBooking(int propertyId,Date start,Date end,int accommodationId,String strFilterDateName,Integer filterBookingId) throws ParseException {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<BookingDetailReport> bookingDetails = null;
		try {
			String strBookingQuery=null;
			strBookingQuery="select pb.bookingId as bookingId,bd.pmsStatus.statusId as statusId, sum(bd.roomCount) as roomCount, pb.arrivalDate as arrivalDate,"
					+ "pb.departureDate as departureDate,pg.firstName as firstName,pg.lastName as lastName,coalesce(sum(bd.amount),0) as amount,"
					+ "coalesce(sum(bd.tax),0) as taxAmount, pg.city as city,"
					+ "pg.phone as mobileNumber,pg.landline as phoneNumber,pg.emailId as emailId,bd.propertyAccommodation.accommodationId as accommodationId "
					+ "from PmsBooking pb,BookingDetail bd,BookingGuestDetail bgd,PmsGuest pg  "
					+ " where pb.pmsProperty.propertyId=:propertyId "
					+ " and pb.bookingId=bd.pmsBooking.bookingId "
					+ " and pb.bookingId=bgd.pmsBooking.bookingId "
					+ " and pg.guestId=bgd.pmsGuest.guestId and bd.pmsStatus.statusId <> :statusId and bd.isActive= 'true' and bd.isDeleted = 'false' ";
					
					if(accommodationId>0){
						strBookingQuery+=" and bd.propertyAccommodation.accommodationId=:accommodationId ";
						if(strFilterDateName.equalsIgnoreCase("BookingDate")){
							strBookingQuery+=" and pb.createdDate between :startDate and :endDate ";
						}else if(strFilterDateName.equalsIgnoreCase("CheckInDate")){
							strBookingQuery+=" and pb.arrivalDate between :startDate and :endDate ";
						}else if(strFilterDateName.equalsIgnoreCase("CheckOutDate")){
							strBookingQuery+=" and pb.departureDate between :startDate and :endDate ";
						}else if(strFilterDateName.equalsIgnoreCase("StayInfo")){
							strBookingQuery+=" and :startDate between pb.arrivalDate and pb.departureDate and :endDate between pb.arrivalDate and pb.departureDate ";
						}
						
					}else{
						if(strFilterDateName.equalsIgnoreCase("BookingDate")){
							strBookingQuery+=" and pb.createdDate between :startDate and :endDate ";
						}else if(strFilterDateName.equalsIgnoreCase("CheckInDate")){
							strBookingQuery+=" and pb.arrivalDate between :startDate and :endDate ";
						}else if(strFilterDateName.equalsIgnoreCase("CheckOutDate")){
							strBookingQuery+=" and pb.departureDate between :startDate and :endDate ";
						}else if(strFilterDateName.equalsIgnoreCase("StayInfo")){
							strBookingQuery+=" and :startDate between pb.arrivalDate and pb.departureDate and :endDate between pb.arrivalDate and pb.departureDate ";
						}
					}
			
					
					strBookingQuery+= " group by pb.bookingId,bd.pmsStatus.statusId, bd.roomCount, pb.arrivalDate,pb.departureDate,"
					+ "pg.firstName,pg.lastName,bd.amount,bd.tax,pg.city,pg.phone,pg.landline,pg.emailId,bd.propertyAccommodation.accommodationId  "
					+ " order by pb.bookingId DESC";
					
							
					if(accommodationId>0){
						bookingDetails = (List<BookingDetailReport>) session.createQuery(strBookingQuery)
								.setParameter("propertyId", propertyId)
								.setParameter("startDate", start)
								.setParameter("endDate", end)
								.setParameter("statusId", 4)
								.setParameter("accommodationId", accommodationId)
								.setResultTransformer(Transformers.aliasToBean(BookingDetailReport.class)).list();
					}
					else{
						bookingDetails = (List<BookingDetailReport>) session.createQuery(strBookingQuery)
								.setParameter("propertyId", propertyId)
								.setParameter("startDate", start)
								.setParameter("endDate", end)
								.setParameter("statusId", 4)
								.setResultTransformer(Transformers.aliasToBean(BookingDetailReport.class)).list();
					}
			
		} catch (HibernateException e) {
			logger.error(e);  
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return bookingDetails;
	}
	
	@SuppressWarnings("unchecked")
	public List<BookingDetailReport> listBooking(int propertyId,int filterBookingId) throws ParseException {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<BookingDetailReport> bookingDetails = null;
		try {
			String strBookingQuery=null;
			strBookingQuery="select pb.bookingId as bookingId,bd.pmsStatus.statusId as statusId, sum(bd.roomCount) as roomCount, pb.arrivalDate as arrivalDate,"
					+ "pb.departureDate as departureDate,pg.firstName as firstName,pg.lastName as lastName,coalesce(sum(bd.amount),0) as amount,"
					+ "coalesce(sum(bd.tax),0) as taxAmount, pg.city as city,"
					+ "pg.phone as mobileNumber,pg.landline as phoneNumber,pg.emailId as emailId,bd.propertyAccommodation.accommodationId as accommodationId "
					+ "from PmsBooking pb,BookingDetail bd,BookingGuestDetail bgd,PmsGuest pg  "
					+ " where pb.pmsProperty.propertyId=:propertyId "
					+ " and pb.bookingId=bd.pmsBooking.bookingId "
					+ " and pb.bookingId=bgd.pmsBooking.bookingId "
					+ " and pg.guestId=bgd.pmsGuest.guestId and bd.pmsStatus.statusId <> :statusId and bd.isActive= 'true' and bd.isDeleted = 'false' ";
					
					strBookingQuery+=" and pb.bookingId =:bookingId ";
			
					strBookingQuery+= " group by pb.bookingId,bd.pmsStatus.statusId, bd.roomCount, pb.arrivalDate,pb.departureDate,"
					+ "pg.firstName,pg.lastName,bd.amount,bd.tax,pg.city,pg.phone,pg.landline,pg.emailId,bd.propertyAccommodation.accommodationId  "
					+ " order by pb.bookingId DESC";
					
					bookingDetails = (List<BookingDetailReport>) session.createQuery(strBookingQuery)
							.setParameter("propertyId", propertyId)
							.setParameter("statusId", 4)
							.setParameter("bookingId", filterBookingId)
							.setResultTransformer(Transformers.aliasToBean(BookingDetailReport.class)).list();
					
		} catch (HibernateException e) {
			logger.error(e);  
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return bookingDetails;
	}
	
	@SuppressWarnings("unchecked")
	public List<BookingDetailReport> listBookingDetail(int filterBookingId,int propertyId) throws ParseException {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<BookingDetailReport> bookingDetails = null;
		try {
			String strBookingQuery=null;
			strBookingQuery="select pb.bookingId as bookingId, coalesce(sum(bd.roomCount),0) as roomCount, pb.arrivalDate as arrivalDate,"
					+ "pb.departureDate as departureDate,pg.firstName as firstName,pg.lastName as lastName,coalesce(sum(bd.amount),0) as amount,"
					+ "pb.createdDate as createdDate,coalesce(sum(bd.tax),0) as taxAmount,coalesce(sum(bd.advanceAmount),0) as advanceAmount,"
					+ "pg.guestId as guestId, pg.city as city,pg.phone as mobileNumber,pg.landline as phoneNumber,pg.emailId as emailId,"
					+ "pb.pmsSource.sourceId as sourceId, bd.propertyAccommodation.accommodationId as accommodationId,"
					+ "bd.adultCount as adultCount,pb.pmsProperty.propertyId as propertyId,pb.promotionAmount as promotionAmount,pb.couponAmount as couponAmount,"
					+ "bd.childCount as childCount,bd.pmsStatus.statusId as statusId,bd.checkInTime as checkInTime, bd.checkOutTime as checkOutTime "
					+ "from PmsBooking pb,BookingDetail bd,BookingGuestDetail bgd,PmsGuest pg "
					+ " where pb.bookingId=bd.pmsBooking.bookingId "
					+ " and pb.bookingId=bgd.pmsBooking.bookingId "
					+ " and pg.guestId=bgd.pmsGuest.guestId and bd.isActive= 'true' and bd.isDeleted = 'false' ";
					
					if(filterBookingId>0){
						strBookingQuery+=" and pb.bookingId =:bookingId and pb.pmsProperty.propertyId=:propertyId ";
					}else{
						strBookingQuery+="";
					}
					
					strBookingQuery+= " group by pb.pmsProperty.propertyId,bd.roomCount,pb.bookingId,pb.arrivalDate,"
							+ "pb.departureDate,pb.pmsSource.sourceId,pb.createdDate,pg.guestId,pg.firstName,pg.lastName,pg.city,"
							+ "pg.phone,pg.landline,pg.emailId,bd.propertyAccommodation.accommodationId, bd.advanceAmount,pb.promotionAmount,pb.couponAmount,"
							+ "bd.adultCount,bd.childCount,bd.pmsStatus.statusId,bd.checkInTime,bd.checkOutTime order by pb.bookingId DESC";
					
			
					if(filterBookingId>0){
						bookingDetails = (List<BookingDetailReport>) session.createQuery(strBookingQuery)
								.setParameter("bookingId", filterBookingId)
								.setParameter("propertyId", propertyId)
								.setResultTransformer(Transformers.aliasToBean(BookingDetailReport.class)).list();
					}
			
		} catch (HibernateException e) {
			logger.error(e);  
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
			
		}
		return bookingDetails;
	}
	
}
