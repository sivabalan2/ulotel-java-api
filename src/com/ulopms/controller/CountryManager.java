package com.ulopms.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;

import com.ulopms.model.Country;
import com.ulopms.util.HibernateUtil;


public class CountryManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(CountryManager.class);

	public Country add(Country country) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(country);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return country;
	}

	public Country edit(Country country) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(country);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return country;
	}

	public Country delete(Long id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Country country = (Country) session.load(Country.class, id);
		if (null != country) {
			session.delete(country);
		}
		session.getTransaction().commit();
		return country;
	}

	public Country find(String id) {
		Country country = new Country();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			country = (Country) session.load(Country.class, id);
			// session.getTransaction().commit();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return country;
	}

	@SuppressWarnings("unchecked")
	public List<Country> list() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<Country> countrys = null;
		try {

			countrys = (List<Country>) session.createQuery("from Country").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return countrys;
	}

	
}
