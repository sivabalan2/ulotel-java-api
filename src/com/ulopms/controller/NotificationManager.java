package com.ulopms.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;

import com.ulopms.model.Notification;
import com.ulopms.util.HibernateUtil;

public class NotificationManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(NotificationManager.class);

	public Notification add(Notification notification) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(notification);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return notification;
	}

	public Notification edit(Notification notification) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(notification);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return notification;
	}

	public Notification delete(Long id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Notification notification = (Notification) session.load(Notification.class, id);
		if (null != notification) {
			session.delete(notification);
		}
		session.getTransaction().commit();
		return notification;
	}

	public Notification find(Long id) {
		Notification notification = new Notification();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			notification = (Notification) session.load(Notification.class, id);
			// session.getTransaction().commit();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return notification;
	}

	@SuppressWarnings("unchecked")
	public List<Notification> list() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<Notification> notifications = null;
		try {

			notifications = (List<Notification>) session.createQuery("from Notification").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return notifications;
	}
	@SuppressWarnings("unchecked")
	public List<Notification> list(int userId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<Notification> notifications = null;
		try {

			notifications = (List<Notification>) session.createQuery("from Notification where userId=:userId")
					.setParameter("userId", userId)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return notifications;
	}
	@SuppressWarnings("unchecked")
	public List<Notification> unReadList(int userId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<Notification> notifications = null;
		try {

			notifications = (List<Notification>) session.createQuery("from Notification where userId=:userId")
					.setParameter("userId", userId)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return notifications;
	}
	
}
