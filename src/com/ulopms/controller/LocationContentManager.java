package com.ulopms.controller;

import java.util.List;

import org.apache.log4j.Logger;

import java.text.ParseException;

import org.hibernate.HibernateException;
import org.hibernate.classic.Session;
import org.hibernate.transform.Transformers;

import com.ulopms.model.AccommodationAmenity;
import com.ulopms.model.DashBoard;
import com.ulopms.model.LocationContent;
import com.ulopms.model.PmsAmenity;
import com.ulopms.model.PmsDays;
import com.ulopms.model.PropertyAccommodation;
import com.ulopms.util.HibernateUtil;


public class LocationContentManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(LocationContentManager.class);

	

	public LocationContent edit(LocationContent locationContent) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(locationContent);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return locationContent;
	}

	public LocationContent delete(int id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		LocationContent locationContent = (LocationContent) session.load(LocationContent.class, id);
		if (null != locationContent) {
			session.delete(locationContent);
		}
		session.getTransaction().commit();
		return locationContent;
	}
	
	

	/*public LocationContent find(int locationId) throws ParseException {
		LocationContent locationContent = null;
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			locationContent = (LocationContent) session.createQuery("select lc.isActive as isActive,lc.isDeleted as isDeleted from LocationContent lc where lc.location.locationId =:locationId and isDeleted = 'false' and isActive = 'true'")
					.setParameter("locationId", locationId)
					.setResultTransformer(Transformers.aliasToBean(LocationContent.class)).uniqueResult();
			// session.getTransaction().commit();
			dashBoard = (DashBoard) session.createQuery("select count(bd.propertyAccommodation.accommodationId) as roomCount from BookingDetail bd where (bd.bookingDate >=:fromDate and bd.bookingDate <=:toDate) and bd.propertyAccommodation.accommodationId =:accommodationId group by bd.propertyAccommodation.accommodationId")
					 .setParameter("accommodationId", accommodationId)
					 .setParameter("fromDate", fromDate)
					  .setParameter("toDate", toDate)
					  .setResultTransformer(Transformers.aliasToBean(DashBoard.class)).uniqueResult();
		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return locationContent;

	}*/
	
	public LocationContent find(int id) {
		LocationContent locationContent = new LocationContent();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			locationContent = (LocationContent) session.load(LocationContent.class, id);
			// session.getTransaction().commit();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return locationContent;
	}
	
	public LocationContent findId(int locationId) throws ParseException {
		LocationContent locationContent = null;
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			locationContent = (LocationContent) session.createQuery("select lc.locationContentId as locationContentId,lc.metaDescription as metaDescription,lc.schemaContent as schemaContent,lc.locationDescription as locationDescription,lc.scriptContent as scriptContent from LocationContent lc where lc.location.locationId =:locationId and isDeleted = 'false' and isActive = 'true'")
					.setParameter("locationId", locationId)
					.setResultTransformer(Transformers.aliasToBean(LocationContent.class)).uniqueResult();
			// session.getTransaction().commit();
			/*dashBoard = (DashBoard) session.createQuery("select count(bd.propertyAccommodation.accommodationId) as roomCount from BookingDetail bd where (bd.bookingDate >=:fromDate and bd.bookingDate <=:toDate) and bd.propertyAccommodation.accommodationId =:accommodationId group by bd.propertyAccommodation.accommodationId")
					 .setParameter("accommodationId", accommodationId)
					 .setParameter("fromDate", fromDate)
					  .setParameter("toDate", toDate)
					  .setResultTransformer(Transformers.aliasToBean(DashBoard.class)).uniqueResult();*/
		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return locationContent;

	}
	
	
	@SuppressWarnings("unchecked")
	public List<LocationContent> list(int locationId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<LocationContent> locationContent = null;
		try {

			locationContent = (List<LocationContent>) session.createQuery("from LocationContent lc  where lc.location.locationId =:locationId and isActive = 'true' and isDeleted = 'false'")
					.setParameter("locationId", locationId)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return locationContent;
	}

	
}
