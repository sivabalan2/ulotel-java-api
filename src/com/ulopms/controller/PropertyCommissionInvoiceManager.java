package com.ulopms.controller;


import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;
import org.hibernate.transform.Transformers;

import com.ulopms.model.BookingDetailReport;
import com.ulopms.model.PropertyCommissionInvoice;
import com.ulopms.util.HibernateUtil;


public class PropertyCommissionInvoiceManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(PropertyCommissionInvoiceManager.class);

	public PropertyCommissionInvoice add(PropertyCommissionInvoice propertyCommissionInvoice) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(propertyCommissionInvoice);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyCommissionInvoice;
	}

	public PropertyCommissionInvoice edit(PropertyCommissionInvoice propertyCommissionInvoice) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(propertyCommissionInvoice);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyCommissionInvoice;
	}

	public PropertyCommissionInvoice delete(int id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		PropertyCommissionInvoice propertyCommissionInvoice = (PropertyCommissionInvoice) session.load(PropertyCommissionInvoice.class, id);
		if (null != propertyCommissionInvoice) {
			session.delete(propertyCommissionInvoice);
		}
		session.getTransaction().commit();
		return propertyCommissionInvoice;
	}

	public PropertyCommissionInvoice find(int id) {
		PropertyCommissionInvoice propertyCommissionInvoice = new PropertyCommissionInvoice();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			propertyCommissionInvoice = (PropertyCommissionInvoice) session.get(PropertyCommissionInvoice.class, id);
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return propertyCommissionInvoice;
	}
	
	@SuppressWarnings("unchecked")
	public List<BookingDetailReport> list() {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<BookingDetailReport> commissionInvoice = null;
		try {
			commissionInvoice = (List<BookingDetailReport>) session.createQuery("select max(invoiceId) as invoiceNumber from PropertyCommissionInvoice ")
					.setResultTransformer(Transformers.aliasToBean(BookingDetailReport.class)).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return commissionInvoice;
	}
	
}
