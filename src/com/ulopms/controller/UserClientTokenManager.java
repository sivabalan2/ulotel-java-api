package com.ulopms.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;

import com.ulopms.model.UserClientToken;
import com.ulopms.util.HibernateUtil;


public class UserClientTokenManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(UserClientTokenManager.class);

	public UserClientToken add(UserClientToken userClientToken) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(userClientToken);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return userClientToken;
	}

	public UserClientToken edit(UserClientToken userClientToken) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(userClientToken);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return userClientToken;
	}

	public UserClientToken delete(Long id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		UserClientToken userClientToken = (UserClientToken) session.load(UserClientToken.class, id);
		if (null != userClientToken) {
			session.delete(userClientToken);
		}
		session.getTransaction().commit();
		return userClientToken;
	}

	public UserClientToken find(Long id) {
		UserClientToken userClientToken = new UserClientToken();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			userClientToken = (UserClientToken) session.load(UserClientToken.class, id);
			// session.getTransaction().commit();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return userClientToken;
	}

	@SuppressWarnings("unchecked")
	public List<UserClientToken> list() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<UserClientToken> userClientTokens = null;
		try {

			userClientTokens = (List<UserClientToken>) session.createQuery("from UserClientToken").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return userClientTokens;
	}

	
}
