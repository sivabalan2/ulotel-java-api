package com.ulopms.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;

import com.ulopms.model.AccessRight;
import com.ulopms.util.HibernateUtil;

public class AccessRightManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(AccessRightManager.class);

	public AccessRight add(AccessRight accessRight) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(accessRight);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return accessRight;
	}

	public AccessRight edit(AccessRight accessRight) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(accessRight);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return accessRight;
	}

	public AccessRight delete(int id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		AccessRight accessRight = (AccessRight) session.load(AccessRight.class, id);
		if (null != accessRight) {
			session.delete(accessRight);
		}
		session.getTransaction().commit();
		return accessRight;
	}

	public AccessRight find(int id) {
		AccessRight accessRight = new AccessRight();
//		Session session = HibernateUtil.getSessionFactory().openSession();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			accessRight = (AccessRight) session.load(AccessRight.class, id);
			// session.getTransaction().commit();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return accessRight;
	}

	@SuppressWarnings("unchecked")
	public List<AccessRight> list() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<AccessRight> accessRights = null;
		try {

			accessRights = (List<AccessRight>) session.createQuery("from AccessRight").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return accessRights;
	}
	@SuppressWarnings("unchecked")
	public List<AccessRight> list(String rightsType) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<AccessRight> accessRights = null;
		try {

			accessRights = (List<AccessRight>) session.createQuery("from AccessRight where rightsType=:rightsType")
					.setParameter("rightsType", rightsType)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return accessRights;
	}

	
}
