package com.ulopms.controller;

import java.util.List;

import org.apache.log4j.Logger;

import java.text.ParseException;

import org.hibernate.HibernateException;
import org.hibernate.classic.Session;
import org.hibernate.transform.Transformers;

import com.ulopms.model.AccommodationAmenity;
import com.ulopms.model.DashBoard;
import com.ulopms.model.LocationContent;
import com.ulopms.model.PmsAmenity;
import com.ulopms.model.PmsDays;
import com.ulopms.model.PropertyAccommodation;
import com.ulopms.model.SeoContent;
import com.ulopms.util.HibernateUtil;


public class SeoContentManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(SeoContentManager.class);

	

	public SeoContent edit(SeoContent seoContent) {
		//Session session = HibernateUtil.getSessionFactory().openSession();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(seoContent);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return seoContent;
	}

	public LocationContent delete(int id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		LocationContent locationContent = (LocationContent) session.load(LocationContent.class, id);
		if (null != locationContent) {
			session.delete(locationContent);
		}
		session.getTransaction().commit();
		return locationContent;
	}	
	
	public LocationContent findLocationContent(int id) {
		LocationContent locationContent = new LocationContent();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			locationContent = (LocationContent) session.load(LocationContent.class, id);
			
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return locationContent;
	}
	
/*	public SeoContent findSeoContent(int id){
		 SeoContent seoContent = new SeoContent();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			seoContent = (SeoContent) session.load(SeoContent.class, id);
			
		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return seoContent;

	}*/
	
	public SeoContent findSeoContent(int googleLocationId){
		 SeoContent seoContent = null;
//		Session session = HibernateUtil.getSessionFactory().openSession();
		 Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			seoContent = (SeoContent) session.createQuery("select sc.seoContentId as seoContentId,sc.title as title,sc.description as description,"
					+ "sc.content as content,sc.keywords as keywords,sc.h1 as h1,sc.h2 as h2,sc.h3 as h3,sc.h4 as h4,sc.shortDescription as shortDescription,"
					+ "sc.socialMetaTag as socialMetaTag  from SeoContent sc where sc.googleLocation.googleLocationId =:googleLocationId and isDeleted = 'false' and isActive = 'true'")
					.setParameter("googleLocationId", googleLocationId)
					.setResultTransformer(Transformers.aliasToBean(SeoContent.class)).uniqueResult();
			
		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return seoContent;

	}
	
	public SeoContent findId1(int propertyId){
		 SeoContent seoContent = null;
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			seoContent = (SeoContent) session.createQuery("select sc.seoContentId as seoContentId,sc.title as title,sc.description as description,"
					+ "sc.content as content,sc.keywords as keywords,sc.h1 as h1,sc.h2 as h2,sc.h3 as h3,sc.h4 as h4,sc.socialMetaTag as socialMetaTag,sc.shortDescription as shortDescription"
					+ " from SeoContent sc where sc.pmsProperty.propertyId =:propertyId and isDeleted = 'false' and isActive = 'true'")
					.setParameter("propertyId", propertyId)
					.setResultTransformer(Transformers.aliasToBean(SeoContent.class)).uniqueResult();
			
		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return seoContent;

	}
	
	
	@SuppressWarnings("unchecked")
	public List<SeoContent> list(int googleLocationId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<SeoContent> seoContent = null;
		try {

			seoContent = (List<SeoContent>) session.createQuery("from SeoContent sc  where sc.googleLocation.googleLocationId =:googleLocationId"
					+ " and isActive = 'true' and isDeleted = 'false'")
					.setParameter("googleLocationId", googleLocationId)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return seoContent;
	}
	
	@SuppressWarnings("unchecked")
	public List<SeoContent> list1(int propertyId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<SeoContent> seoContent = null;
		try {

			seoContent = (List<SeoContent>) session.createQuery("from SeoContent sc  where sc.pmsProperty.propertyId =:propertyId"
					+ " and isActive = 'true' and isDeleted = 'false'")
					.setParameter("propertyId", propertyId)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return seoContent;
	}

	@SuppressWarnings("unchecked")
	public List<SeoContent> listArea(int googleAreaId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<SeoContent> seoContent = null;
		try {

			seoContent = (List<SeoContent>) session.createQuery("from SeoContent sc  where sc.googleArea.googleAreaId =:googleAreaId "
					+ "and isActive = 'true' and isDeleted = 'false'")
					.setParameter("googleAreaId", googleAreaId)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return seoContent;
	}

	public SeoContent findId(int googleAreaId){
		 SeoContent seoContent = null;
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			seoContent = (SeoContent) session.createQuery("select sc.seoContentId as seoContentId,sc.title as title,sc.description as description,"
					+ "sc.content as content,sc.keywords as keywords,sc.h1 as h1,sc.h2 as h2,sc.h3 as h3,"
					+ "sc.h4 as h4,sc.socialMetaTag as socialMetaTag,sc.shortDescription as shortDescription"
					+ " from SeoContent sc where sc.googleArea.googleAreaId =:googleAreaId and isDeleted = 'false' and isActive = 'true'")
					.setParameter("googleAreaId", googleAreaId)
					.setResultTransformer(Transformers.aliasToBean(SeoContent.class)).uniqueResult();
			
		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return seoContent;

	}
	
	public SeoContent findSeoBlogArticleContent(int blogArticleId){
		 SeoContent seoContent = null;
//		Session session = HibernateUtil.getSessionFactory().openSession();
		 Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			seoContent = (SeoContent) session.createQuery("select sc.seoContentId as seoContentId,sc.title as title,sc.description as description,"
					+ "sc.content as content,sc.keywords as keywords,sc.h1 as h1,sc.h2 as h2,sc.h3 as h3,sc.h4 as h4,sc.shortDescription as shortDescription,"
					+ "sc.socialMetaTag as socialMetaTag  from SeoContent sc where sc.blogArticles.blogArticleId =:blogArticleId and isDeleted = 'false' and isActive = 'true'")
					.setParameter("blogArticleId", blogArticleId)
					.setResultTransformer(Transformers.aliasToBean(SeoContent.class)).uniqueResult();
			
		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return seoContent;

	}
	
	@SuppressWarnings("unchecked")
	public List<SeoContent> listBlogArticles(int blogArticleId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<SeoContent> seoContent = null;
		try {

			seoContent = (List<SeoContent>) session.createQuery("from SeoContent sc  where sc.blogArticles.blogArticleId =:blogArticleId"
					+ " and isActive = 'true' and isDeleted = 'false'")
					.setParameter("blogArticleId", blogArticleId)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return seoContent;
	}
}
