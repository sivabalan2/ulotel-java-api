package com.ulopms.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;
import org.hibernate.transform.Transformers;

import com.ulopms.model.BookingDetailReport;
import com.ulopms.model.PmsProperty;
import com.ulopms.model.PropertyReviews;
import com.ulopms.model.Revenue;
import com.ulopms.model.Reviews;
import com.ulopms.util.HibernateUtil;


public class ReviewsManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(ReviewsManager.class);

	public Reviews add(Reviews reviews) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(reviews);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return reviews;
	}

	public Reviews edit(Reviews reviews) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(reviews);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return reviews;
	}

	public Reviews delete(int id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Reviews reviews = (Reviews) session.load(Reviews.class, id);
		if (null != reviews) {
			session.delete(reviews);
		}
		session.getTransaction().commit();
		return reviews;
	}

	public Reviews find(int id) {
		Reviews reviews = new Reviews();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			reviews = (Reviews) session.load(Reviews.class, id);
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return reviews;
	}

	@SuppressWarnings("unchecked")
	public List<Reviews> list() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<Reviews> reviews = null;
		try {

			reviews = (List<Reviews>) session.createQuery("from Reviews where isActive = 'true'"
					+ " and isDeleted = 'false' ").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return reviews;
	}
	

}
