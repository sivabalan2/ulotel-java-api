package com.ulopms.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;

import com.ulopms.model.AccommodationAmenity;
import com.ulopms.model.PmsAmenity;
import com.ulopms.model.PropertyAccommodation;
import com.ulopms.model.PropertyAmenity;
import com.ulopms.util.HibernateUtil;


public class AccommodationAmenityManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(AccommodationAmenityManager.class);

	public AccommodationAmenity add(AccommodationAmenity accommodationAmenity) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(accommodationAmenity);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return accommodationAmenity;
	}

	public AccommodationAmenity edit(AccommodationAmenity accommodationAmenity) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(accommodationAmenity);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return accommodationAmenity;
	}

	public AccommodationAmenity delete(Long id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		AccommodationAmenity accommodationAmenity = (AccommodationAmenity) session.load(AccommodationAmenity.class, id);
		if (null != accommodationAmenity) {
			session.delete(accommodationAmenity);
		}
		session.getTransaction().commit();
		return accommodationAmenity;
	}

	public AccommodationAmenity find(Long id) {
		AccommodationAmenity accommodationAmenity = new AccommodationAmenity();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			accommodationAmenity = (AccommodationAmenity) session.load(AccommodationAmenity.class, id);
			// session.getTransaction().commit();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return accommodationAmenity;
	}
	
	@SuppressWarnings("unchecked")
	public List<AccommodationAmenity> list() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<AccommodationAmenity> accommodationAmenitys = null;
		try {

			accommodationAmenitys = (List<AccommodationAmenity>) session.createQuery("from AccommodationAmenity").list();			
				

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return accommodationAmenitys;
	}

	@SuppressWarnings("unchecked")
	public List<AccommodationAmenity> list(int accommodationId,int amenityId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<AccommodationAmenity> accommodationAmenitys = null;
		try {

			accommodationAmenitys = (List<AccommodationAmenity>) session.createQuery("from AccommodationAmenity  where propertyAccommodation.accommodationId=:accommodationId and pmsAmenity.amenityId=:amenityId and isDeleted = 'false' and isActive = 'true' ")
					.setParameter("accommodationId", accommodationId)
					.setParameter("amenityId", amenityId).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return accommodationAmenitys;
	}
	@SuppressWarnings("unchecked")
	public List<AccommodationAmenity> list(int accommodationId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<AccommodationAmenity> accommodationAmenitys = null;
		try {

			accommodationAmenitys = (List<AccommodationAmenity>)session.createQuery("from AccommodationAmenity  where propertyAccommodation.accommodationId=:accommodationId and isDeleted = 'false' and isActive = 'true' ")
					.setParameter("accommodationId", accommodationId)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return accommodationAmenitys;
	}
	
	@SuppressWarnings("unchecked")
	public List<AccommodationAmenity> listTwo(int accommodationId,int amenityId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<AccommodationAmenity> accommodationAmenitys = null;
		try {

			accommodationAmenitys = (List<AccommodationAmenity>) session.createQuery("from AccommodationAmenity where propertyAccommodation.accommodationId=:accommodationId and pmsAmenity.amenityId=:amenityId and isDeleted = 'false' and isActive = 'true' ")
					
					.setParameter("accommodationId", accommodationId)
					.setParameter("amenityId", amenityId).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return accommodationAmenitys;
	}
	
	
}
