package com.ulopms.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;

import com.ulopms.model.HelpTopic;
import com.ulopms.util.HibernateUtil;


public class HelpTopicManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(HelpTopicManager.class);

	public HelpTopic add(HelpTopic helpTopic) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(helpTopic);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return helpTopic;
	}

	public HelpTopic edit(HelpTopic helpTopic) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(helpTopic);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return helpTopic;
	}

	public HelpTopic delete(Long id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		HelpTopic helpTopic = (HelpTopic) session.load(HelpTopic.class, id);
		if (null != helpTopic) {
			session.delete(helpTopic);
		}
		session.getTransaction().commit();
		return helpTopic;
	}

	public HelpTopic find(Long id) {
		HelpTopic helpTopic = new HelpTopic();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			helpTopic = (HelpTopic) session.load(HelpTopic.class, id);
			// session.getTransaction().commit();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return helpTopic;
	}

	@SuppressWarnings("unchecked")
	public List<HelpTopic> list() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<HelpTopic> helpTopics = null;
		try {

			helpTopics = (List<HelpTopic>) session.createQuery("from HelpTopic").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return helpTopics;
	}

	
}
