package com.ulopms.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;

import com.ulopms.model.Location;
import com.ulopms.model.LocationType;
import com.ulopms.util.HibernateUtil;


public class LocationManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(LocationManager.class);

	public Location add(Location location) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(location);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return location;
	}

	public Location edit(Location location) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(location);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return location;
	}

	public Location delete(int id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Location location = (Location) session.load(Location.class, id);
		if (null != location) {
			session.delete(location);
		}
		session.getTransaction().commit();
		return location;
	}

	public Location find(int id) {
		Location location = new Location();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			location = (Location) session.load(Location.class, id);
			// session.getTransaction().commit();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return location;
	}
	
	public LocationType findType(int id) {
		LocationType locationType = new LocationType();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			locationType = (LocationType) session.load(LocationType.class, id);
			// session.getTransaction().commit();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return locationType;
	}

	@SuppressWarnings("unchecked")
	public List<Location> list() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<Location> locations = null;
		try {

			locations = (List<Location>) session.createQuery("from Location where isActive = 'true' and isDeleted = 'false'").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return locations;
	}
	
	@SuppressWarnings("unchecked")
	public List<LocationType> listTypes() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<LocationType> locationType = null;
		try {

			locationType = (List<LocationType>) session.createQuery("from LocationType where isActive = 'true' and isDeleted = 'false'").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return locationType;
	}
	
	@SuppressWarnings("unchecked")
	public List<Location> list(int locationId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<Location> locations = null;
		try {

			locations = (List<Location>) session.createQuery("from Location where locationId=:locationId and "
					+ " isActive = 'true' and isDeleted = 'false'").setParameter("locationId", locationId).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return locations;
	}
	
	@SuppressWarnings("unchecked")
	public List<Location> list(int limit,int offset) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<Location> locations = null;
		try {

			locations = (List<Location>) session.createQuery("from Location where isActive = 'true'"
					+ " and isDeleted = 'false'")
					.setFirstResult(offset)
					.setMaxResults(limit)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return locations;
	}

	@SuppressWarnings("unchecked")
	public List<Location> listLocationUrl(String locationUrl) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<Location> locations = null;
		try {

			locations = (List<Location>) session.createQuery("from Location where isActive = 'true' "
					+ "and isDeleted = 'false' and locationUrl=:locationUrl")
					.setParameter("locationUrl", locationUrl).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return locations;
	}
	
	@SuppressWarnings("unchecked")
	public List<Location> listSelectLocation() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<Location> locations = null;
		try {

			locations = (List<Location>) session.createQuery("from Location where isActive = 'true' "
					+ "and isDeleted = 'false' and locationName like '%I%' ").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return locations;
	}
}
