package com.ulopms.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;
import org.hibernate.transform.Transformers;

import com.ulopms.model.PmsSource;
import com.ulopms.util.HibernateUtil;


public class PmsSourceManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(PmsSourceManager.class);

	public PmsSource add(PmsSource PmsSource) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(PmsSource);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return PmsSource;
	}

	public PmsSource edit(PmsSource PmsSource) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(PmsSource);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return PmsSource;
	}

	public PmsSource delete(int id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		PmsSource PmsSource = (PmsSource) session.load(PmsSource.class, id);
		if (null != PmsSource) {
			session.delete(PmsSource);
		}
		session.getTransaction().commit();
		return PmsSource;
	}

	public PmsSource find(int id) {
		PmsSource PmsSource = new PmsSource();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			PmsSource = (PmsSource) session.load(PmsSource.class, id);
			// session.getTransaction().commit();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return PmsSource;
	}
	
	public PmsSource find(String sourceName) {
		 PmsSource pmsSource = new PmsSource();
			Session session = HibernateUtil.getSessionFactory().getCurrentSession();
			try {
				
				
				session.beginTransaction();
				pmsSource = (PmsSource)session.createQuery("select sourceId as sourceId from PmsSource where "
						+ "sourceName =:sourceName and isActive = 'true' and isDeleted = 'false'")						
			    .setParameter("sourceName", sourceName)
			    .setResultTransformer(Transformers.aliasToBean(PmsSource.class)).uniqueResult();
				
			   	  
			} catch (Exception e1) {
				logger.error(e1);
			} finally {
				session = null;
			}
			
			return pmsSource;
		}
	@SuppressWarnings("unchecked")
	public List<PmsSource> list() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsSource> PmsSources = null;
		try {

			PmsSources = (List<PmsSource>) session.createQuery("from PmsSource where isActive = 'true' and isDeleted = 'false' and "
					+ "resortIsActive = 'false' ").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return PmsSources;
	}
	
	@SuppressWarnings("unchecked")
	public List<PmsSource> list1() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsSource> PmsSources = null;
		try {

			PmsSources = (List<PmsSource>) session.createQuery("from PmsSource where isActive = 'true' and isDeleted = 'false' and "
					+ "resortIsActive = 'true'").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return PmsSources;
	}

	
	@SuppressWarnings("unchecked")
	public List<PmsSource> listAll() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsSource> PmsSources = null;
		try {

			PmsSources = (List<PmsSource>) session.createQuery("from PmsSource where isActive = 'true' and isDeleted = 'false'")
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return PmsSources;
	}
	
}
