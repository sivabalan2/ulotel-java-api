package com.ulopms.controller;

import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;
import org.hibernate.transform.Transformers;
import org.joda.time.DateTime;

import com.ulopms.model.BookingDetail;
import com.ulopms.model.BookingDetailReport;
import com.ulopms.model.BookingGuestDetail;
import com.ulopms.model.DashBoard;
import com.ulopms.model.Revenue;
import com.ulopms.model.RevenueReport;
import com.ulopms.model.Screen;
import com.ulopms.model.UserGuestDetailReport;
import com.ulopms.util.HibernateUtil;


public class PropertyVoucherManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(PropertyVoucherManager.class);

	@SuppressWarnings("unchecked")
	public List<BookingDetailReport> listBookingDetail(int propertyId,Date start,Date end) throws ParseException {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<BookingDetailReport> bookingDetails = null;
		try {
			String strBookingQuery=null;
			strBookingQuery="select pb.bookingId as bookingId, pb.arrivalDate as arrivalDate,pb.departureDate as departureDate,coalesce(sum(bd.amount),0) as amount,"
					+ "coalesce(sum(bd.tax),0) as taxAmount,coalesce(sum(bd.otaCommission),0) as otaCommission,pb.pmsSource.sourceId as sourceId,"
					+ "coalesce(sum(bd.advanceAmount),0) as advanceAmount,coalesce(sum(bd.otaTax),0) as otaTax  "
					+ "from PmsBooking pb,BookingDetail bd "
					+ " where pb.pmsProperty.propertyId=:propertyId and bd.isActive='true' and bd.isDeleted='false' "
					+ " and pb.bookingId=bd.pmsBooking.bookingId and bd.pmsStatus.statusId <> 4 and bd.pmsStatus.statusId <> 1";
			
					strBookingQuery+=" and pb.arrivalDate between :startDate and :endDate  "
							+ " group by pb.bookingId,pb.arrivalDate,pb.departureDate,pb.pmsSource.sourceId order by pb.bookingId DESC";
					
					bookingDetails = (List<BookingDetailReport>) session.createQuery(strBookingQuery)
							.setParameter("propertyId", propertyId)
							.setParameter("startDate", start)
							.setParameter("endDate", end)
							.setResultTransformer(Transformers.aliasToBean(BookingDetailReport.class)).list();
					
			
		} catch (HibernateException e) {
			logger.error(e);  
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
			
		}
		return bookingDetails;
	}
	
	
}
