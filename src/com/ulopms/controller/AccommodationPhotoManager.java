package com.ulopms.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;

import com.ulopms.model.AccommodationPhoto;
import com.ulopms.util.HibernateUtil;

public class AccommodationPhotoManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(AccommodationPhotoManager.class);

	public AccommodationPhoto add(AccommodationPhoto accommodationPhoto) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(accommodationPhoto);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return accommodationPhoto;
	}

	public AccommodationPhoto edit(AccommodationPhoto accommodationPhoto) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(accommodationPhoto);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return accommodationPhoto;
	}

	public AccommodationPhoto delete(int id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		AccommodationPhoto accommodationPhoto = (AccommodationPhoto) session.load(AccommodationPhoto.class, id);
		if (null != accommodationPhoto) {
			session.delete(accommodationPhoto);
		}
		session.getTransaction().commit();
		return accommodationPhoto;
	}

	public AccommodationPhoto find(int id) {
		AccommodationPhoto accommodationPhoto = new AccommodationPhoto();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			accommodationPhoto = (AccommodationPhoto) session.load(AccommodationPhoto.class, id);
			// session.getTransaction().commit();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return accommodationPhoto;
	}

	@SuppressWarnings("unchecked")
	public List<AccommodationPhoto> list() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<AccommodationPhoto> accommodationPhotos = null;
		try {

			accommodationPhotos = (List<AccommodationPhoto>) session.createQuery("from AccommodationPhoto").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return accommodationPhotos;
	}

	
}
