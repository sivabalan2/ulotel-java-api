package com.ulopms.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;



import com.ulopms.model.PmsProperty;
import com.ulopms.model.PropertyPaymentDetails;
import com.ulopms.util.HibernateUtil;

public class PropertyPaymentDetailManager extends HibernateUtil{
	
	private static final Logger logger = Logger.getLogger(PropertyPaymentDetailManager.class);
	
	public PropertyPaymentDetails add(PropertyPaymentDetails propertyPaymentDetails) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(propertyPaymentDetails);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyPaymentDetails;
	}

	public PropertyPaymentDetails edit(PropertyPaymentDetails propertyPaymentDetails) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(propertyPaymentDetails);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyPaymentDetails;
	}

	public PropertyPaymentDetails delete(Long id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		PropertyPaymentDetails propertyPaymentDetails = (PropertyPaymentDetails) session.load(PropertyPaymentDetails.class, id);
		if (null != propertyPaymentDetails) {
			session.delete(propertyPaymentDetails);
		}
		session.getTransaction().commit();
		return propertyPaymentDetails;
	}

	public PropertyPaymentDetails find(int id) {
		PropertyPaymentDetails propertyPaymentDetails = new PropertyPaymentDetails();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			propertyPaymentDetails = (PropertyPaymentDetails) session.load(PropertyPaymentDetails.class, id);
			// session.getTransaction().commit();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return propertyPaymentDetails;
	}
	
	@SuppressWarnings("unchecked")
	public List<PropertyPaymentDetails> list(int propertyId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyPaymentDetails> propertyPaymentDetails = null;
		try {

			propertyPaymentDetails = (List<PropertyPaymentDetails>) session.createQuery("from PropertyPaymentDetails ppa where ppa.isActive = 'true'"
					+ " and ppa.isDeleted = 'false' and ppa.pmsProperty.propertyId =:propertyId ORDER BY createdDate DESC")
					.setParameter("propertyId", propertyId)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyPaymentDetails;
	}
	
	@SuppressWarnings("unchecked")
	public List<PropertyPaymentDetails> list(int propertyId,int offset,int limit) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyPaymentDetails> propertyPaymentDetails = null;
		try {

			propertyPaymentDetails = (List<PropertyPaymentDetails>) session.createQuery("from PropertyPaymentDetails ppa where ppa.isActive = 'true'"
					+ " and ppa.isDeleted = 'false' and ppa.pmsProperty.propertyId =:propertyId ORDER BY createdDate DESC")
					.setParameter("propertyId", propertyId)
					.setFirstResult(offset)
					.setMaxResults(limit)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyPaymentDetails;
	}

}
