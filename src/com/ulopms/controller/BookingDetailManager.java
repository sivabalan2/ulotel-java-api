package com.ulopms.controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;
import org.hibernate.transform.Transformers;

import com.ulopms.model.BookedDetail;
import com.ulopms.model.BookingDetail;
import com.ulopms.model.DashBoard;
import com.ulopms.model.PmsAvailableRooms;
import com.ulopms.model.PmsBookedDetails;
import com.ulopms.model.PmsRoomDetail;
import com.ulopms.model.PropertyAccommodationRoom;
import com.ulopms.util.HibernateUtil;


public class BookingDetailManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(BookingDetailManager.class);

	public BookingDetail add(BookingDetail bookingDetail) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(bookingDetail);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return bookingDetail;
	}

	public BookingDetail edit(BookingDetail bookingDetail) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(bookingDetail);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return bookingDetail;
	}

	public BookingDetail delete(Long id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		BookingDetail bookingDetail = (BookingDetail) session.load(BookingDetail.class, id);
		if (null != bookingDetail) {
			session.delete(bookingDetail);
		}
		session.getTransaction().commit();
		return bookingDetail;
	}

	public BookingDetail find(int id) {
		BookingDetail bookingDetail = new BookingDetail();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			bookingDetail = (BookingDetail) session.load(BookingDetail.class, id);
			// session.getTransaction().commit();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return bookingDetail;
	}
	
	@SuppressWarnings("unchecked")
	public List<BookingDetail> findId(int bookingId) {
		List<BookingDetail> BookingDetail = null;
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			//bookingDetails = (List<BookingDetail>) session.createQuery("from BookingDetail").list();
			BookingDetail = (List<BookingDetail>)session.createQuery("from BookingDetail where pmsBooking.bookingId =:bookingId")
				    .setParameter("bookingId",bookingId)
				    .list();
			// session.getTransaction().commit();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return BookingDetail;
	}
	
	@SuppressWarnings("unchecked")
	public List<BookingDetail> findId(int bookingId,int statusId) {
		List<BookingDetail> BookingDetail = null;
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			//bookingDetails = (List<BookingDetail>) session.createQuery("from BookingDetail").list();
			BookingDetail = (List<BookingDetail>)session.createQuery("from BookingDetail where"
					+ " pmsBooking.bookingId =:bookingId"
					+ " and pmsStatus.statusId=:statusId ")
				    .setParameter("bookingId",bookingId).setParameter("statusId",statusId)
				    .list();
			// session.getTransaction().commit();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return BookingDetail;
	}
	
	@SuppressWarnings("unchecked")
	public List<BookedDetail> list(int bookingId,int accommodationId) {

	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
	session.beginTransaction();
	List<BookedDetail> BookedDetail = null;
	try {

	BookedDetail = (List<BookedDetail>) session.createQuery("select distinct propertyAccommodation.accommodationId as accommodationId,"
			+ "amount as amount from BookingDetail where pmsBooking.bookingId =:bookingId and propertyAccommodation.accommodationId =:accommodationId ")
	.setParameter("bookingId", bookingId)
	.setParameter("accommodationId", accommodationId)
	.setResultTransformer(Transformers.aliasToBean(BookedDetail.class)).list();

	} catch (HibernateException e) {
	logger.error(e);
	session.getTransaction().rollback();
	} finally {
	session.getTransaction().commit();
	session = null;
	}
	return BookedDetail;
	}
	
	 public PmsRoomDetail findRoom(Date arrivalDate ,Date departureDate,int accommodationId) {
		PmsRoomDetail roomDetail = new PmsRoomDetail();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		
		try {
			/*
			 
			 SELECT * FROM property_accommodation_rooms WHERE NOT EXISTS(SELECT 1 FROM booking_details WHERE booking_details.room_id=property_accommodation_rooms.room_id and accommodation_id='16' and booking_date between '2017-06-17' and '2017-06-18')and accommodation_id='16' limit 1
			 
			 select roomId as roomId from propertyAcccommodationRoom where not exists(select 1 from bookingDetail where bookingDetail.propertyAccommodationRoom.roomId = propertyAccommodationRoom.roomId and bookingDetail.propertyAccommodation.accommodationId = :accommodationId and bookingDate between :arrivalDate and :departureDate) and propertyAcccommodationRoom.accommodationId =:accommodationId
			 */
			
			session.beginTransaction();
			roomDetail = (PmsRoomDetail)session.createQuery(" select par.roomId as roomId from PropertyAccommodationRoom par where not "
					+ "exists (select bd.propertyAccommodationRoom.roomId as roomId from BookingDetail bd where "
					+ "bd.propertyAccommodationRoom.roomId = par.roomId and bd.bookingDate between :arrivalDate and :departureDate) "
					+ "and par.propertyAccommodation.accommodationId =:accommodationId")
		    .setParameter("arrivalDate", arrivalDate).setParameter("departureDate", departureDate) .setParameter("accommodationId", accommodationId)
		    .setResultTransformer(Transformers.aliasToBean(PmsRoomDetail.class)).setMaxResults(1).uniqueResult();
			// session.getTransaction().commit();
			
			
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return roomDetail;
	}
	
	@SuppressWarnings("unchecked")
	public List<PmsRoomDetail> roomList(Date arrivalDate ,Date departureDate,int accommodationId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsRoomDetail> roomDetails = null;
		try {
           
			
			roomDetails = (List<PmsRoomDetail>) session.createQuery(" select par.roomId as roomId from PropertyAccommodationRoom par "
					+ "where not exists (select bd.propertyAccommodationRoom.roomId as roomId from BookingDetail bd "
					+ "where bd.propertyAccommodationRoom.roomId = par.roomId and bd.bookingDate between :arrivalDate "
					+ "and :departureDate) and par.propertyAccommodation.accommodationId =:accommodationId")
				    .setParameter("arrivalDate", arrivalDate).setParameter("departureDate", departureDate) .setParameter("accommodationId", accommodationId)
				    .setResultTransformer(Transformers.aliasToBean(PmsRoomDetail.class)).list();
			 
			//bookedDetails = (List<PmsBookedDetails>) session.createQuery("from BookingDetail").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return roomDetails;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<BookedDetail> listAccommodation(int bookingId,int accommodationId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<BookedDetail> BookedDetail = null;
		try {
	
			BookedDetail = (List<BookedDetail>) session.createQuery("select propertyAccommodation.accommodationId as accommodationId,"
			+ "sum(roomCount) as roomCount,coalesce(sum(tax),0) as tax,coalesce(sum(amount),0) as amount , adultCount as adultCount,childCount as childCount,"
			+ "coalesce(sum(otaCommission),0) as otaCommission,coalesce(sum(otaTax),0) as otaTax"
			+ "  from BookingDetail where pmsBooking.bookingId =:bookingId and propertyAccommodation.accommodationId =:accommodationId "
					+ "group by propertyAccommodation.accommodationId,adultCount,childCount ")
			.setParameter("bookingId", bookingId)
			.setParameter("accommodationId", accommodationId)
			.setResultTransformer(Transformers.aliasToBean(BookedDetail.class)).list();
		
			} catch (HibernateException e) {
				logger.error(e);
				session.getTransaction().rollback();
			} finally {
				session.getTransaction().commit();
				session = null;
		}
		return BookedDetail;
	}
	
	@SuppressWarnings("unchecked")
	public List<PmsBookedDetails> bookedList(int bookingId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsBookedDetails> bookedDetails = null;
		try {
             
			
			 bookedDetails = (List<PmsBookedDetails>) session.createQuery("select distinct bd.propertyAccommodation.accommodationId as "
			 		+ "accommodationId, sum(bd.adultCount) as adults, sum(bd.childCount) as child, gd.pmsGuest.guestId as guestId,"
			 		+ "pb.pmsProperty.propertyId as propertyId,pb.arrivalDate as arrivalDate ,pb.departureDate as departureDate,"
			     	+ "pb.rooms as rooms ,pb.advanceAmount as advanceAmount,pb.pmsSource.sourceId as sourceId,pb.pmsTags.tagId as tagId,"
			     	+ "bd.propertyDiscount.propertyDiscountId as propertyDiscountId ,bd.pmsPromotions.promotionId as promotionId,pb.totalAmount as amount,pb.totalTax as tax,"
			     	+ " pb.otaCommission as otaCommission,pb.otaTax as otaTax,pb.createdDate as createdDate,pb.promotionFirstName as promotionFirstName,"
			     	+ "pb.promotionSecondName as promotionSecondName,pb.couponName as couponName,pb.promotionAmount as promotionAmount,pb.couponAmount as couponAmount,"
			     	+ "gd.specialRequest as specialRequest,bd.checkInTime as checkInTime,bd.checkOutTime as checkOutTime,"
			     	+ "sum(bd.purchaseRate) as purchaseRate,pb.totalActualAmount as actualAmount,"
			     	+ "sum(bd.extraAdultRate) as extraAdultRate,sum(bd.extraChildRate) as extraChildRate,pb.addonAmount as addonAmount "
			     	+ "from BookingDetail bd ,PmsBooking pb ,"
			 		+ "BookingGuestDetail gd where bd.pmsBooking.bookingId =:bookingId and pb.bookingId = :bookingId and "
			 		+ "gd.pmsBooking.bookingId =:bookingId group by bd.propertyAccommodation.accommodationId,bd.adultCount,"
			 		+ "bd.childCount,gd.pmsGuest.guestId,pb.pmsProperty.propertyId ,pb.advanceAmount,pb.pmsSource.sourceId,"
			 		+ "bd.propertyDiscount.propertyDiscountId,bd.pmsPromotions.promotionId,gd.specialRequest,pb.arrivalDate ,pb.departureDate,pb.rooms,"
			 		+ "pb.totalAmount,pb.totalTax,pb.otaCommission,pb.otaTax,pb.createdDate,pb.promotionFirstName,pb.promotionSecondName,pb.couponAmount,"
			 		+ "pb.couponName,pb.promotionAmount,pb.pmsTags.tagId,bd.checkInTime,bd.checkOutTime,bd.purchaseRate,pb.totalActualAmount,"
			 		+ "bd.extraAdultRate,bd.extraChildRate,pb.addonAmount")
			.setParameter("bookingId", bookingId)
			.setResultTransformer(Transformers.aliasToBean(PmsBookedDetails.class)).list();
			 
			//bookedDetails = (List<PmsBookedDetails>) session.createQuery("from BookingDetail").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return bookedDetails;
	}
	
	@SuppressWarnings("unchecked")
	public List<PmsBookedDetails> userBookedList(int bookingId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsBookedDetails> bookedDetails = null;
		//Date date=new SimpleDateFormat("yyyy-MM-dd").parse(todayDate); 
		try {
             
			
			
			 bookedDetails = (List<PmsBookedDetails>) session.createQuery("select pb.pmsProperty.propertyId as propertyId,"
			 		+ "pb.arrivalDate as arrivalDate ,pb.departureDate as departureDate,pb.bookingId as bookingId,"
			 		+ "pb.rooms as rooms ,pb.totalAmount as amount,pb.totalTax as tax,bd.adultCount as adults,bd.childCount as child,"
			 		+ "bd.propertyAccommodation.accommodationId as accommodationId from PmsBooking pb,BookingDetail bd "
			 		+ " where pb.bookingId=bd.pmsBooking.bookingId and pb.bookingId = :bookingId  group by "			 	
			 		+ "pb.pmsProperty.propertyId ,pb.arrivalDate ,pb.departureDate,pb.rooms,pb.bookingId,"
			 		+ "pb.totalAmount,pb.totalTax,bd.adultCount,bd.childCount,bd.propertyAccommodation.accommodationId")
			.setParameter("bookingId", bookingId)			
			.setResultTransformer(Transformers.aliasToBean(PmsBookedDetails.class)).list();
			 
			//bookedDetails = (List<PmsBookedDetails>) session.createQuery("from BookingDetail").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return bookedDetails;
	}
	

	@SuppressWarnings("unchecked")
	public List<BookingDetail> list() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<BookingDetail> bookingDetails = null;
		try {

			bookingDetails = (List<BookingDetail>) session.createQuery("from BookingDetail").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return bookingDetails;
	}
	
	@SuppressWarnings("unchecked")
	public List<BookingDetail> listAllBooking(int propertyId,String todayDate) throws ParseException {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<BookingDetail> bookingDetails = null;
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Date today = df.parse(todayDate);
		try {
			/*if(accommodationId>0)
			{
				bookingDetails = (List<BookingDetail>)session.createQuery("select a from BookingDetail a join a.pmsBooking b where a.bookingDate between :fromDate and :toDate and "
					+ "a.propertyAccommodation.accommodationId=:accommodationId and b.pmsProperty.propertyId=:propertyId")
					
					.setParameter("fromDate", fromDate)
					.setParameter("toDate", toDate)
					.setParameter("propertyId", propertyId)
					.list();
			}
			else
			{*/
				bookingDetails = (List<BookingDetail>)session.createQuery("select a from BookingDetail a join a.pmsBooking"
						+ " b where  (:today between b.arrivalDate and b.departureDate) and "
					+ "b.pmsProperty.propertyId=:propertyId")
						.setParameter("today", today)						
						.setParameter("propertyId", propertyId)
						.list();
			//}
			

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return bookingDetails;
	}
	
	
     @SuppressWarnings("unchecked")
	public List<BookingDetail> detailList(int bookingId,int accommodationId) throws ParseException {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<BookingDetail> bookingDetails = null;
		
		
		try {
			
				bookingDetails = (List<BookingDetail>)session.createQuery("from BookingDetail where pmsBooking.bookingId =:bookingId and "
						+ "propertyAccommodation.accommodationId =:accommodationId")
						.setParameter("bookingId", bookingId)						
						.setParameter("accommodationId", accommodationId)
						.list();
			
			

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return bookingDetails;
	}
	
	public DashBoard findCountAccommodation(String start,String end,int accommodationId) throws ParseException {
		
		
		DashBoard dashBoard = null;
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		
		Date fromDate = df.parse(start);
		Date toDate = df.parse(end); 

	
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		//session.beginTransaction();
		
		try {
			session.beginTransaction();

			dashBoard = (DashBoard) session.createQuery("select sum(bd.roomCount) as roomCount from BookingDetail bd where "
					+ "(bd.bookingDate >=:fromDate and bd.bookingDate <=:toDate) and "
					+ "bd.propertyAccommodation.accommodationId =:accommodationId group by bd.propertyAccommodation.accommodationId")
					 .setParameter("accommodationId", accommodationId)
					 .setParameter("fromDate", fromDate)
					  .setParameter("toDate", toDate)
					  .setResultTransformer(Transformers.aliasToBean(DashBoard.class)).uniqueResult();
					
		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return dashBoard;
	}
	
	
	@SuppressWarnings("unchecked")
	public DashBoard findDiscountCount(int discountId) throws ParseException {
		
		
		DashBoard dashBoard = null;
		

	
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		//session.beginTransaction();
		
		try {
			session.beginTransaction();
			//count(distinct bd.booking_id) from booking_details where property_discount_id =16
			
			dashBoard = (DashBoard) session.createQuery("select count(distinct bd.pmsBooking.bookingId) as discountCount "
					+ "from BookingDetail bd where bd.propertyDiscount.propertyDiscountId =:discountId")
					 .setParameter("discountId", discountId)
					 .setResultTransformer(Transformers.aliasToBean(DashBoard.class)).uniqueResult();
					
		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return dashBoard;
	}   
	
	@SuppressWarnings("unchecked")
	public List<BookingDetail> list(int propertyId, String datesa) throws ParseException {
		Date dates=new SimpleDateFormat("yyyy-MM-dd").parse(datesa); 
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<BookingDetail> bookingDetails = null;
		try {

			bookingDetails = (List<BookingDetail>) session.createQuery("select bd.bookingDate from BookingDetail bd,PropertyAccommodation pa "
					+ "where bd.bookingDate=:dates and pa.pmsProperty.propertyId=:propertyId and"
					+ " bd.propertyAccommodation.accommodationId = pa.accommodationId group by bd.bookingDate,bd.bookingDetailsId")
					.setParameter("propertyId", propertyId)
					.setParameter("dates", dates)
					.list();
		} catch (HibernateException e) {
			logger.error(e);  
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return bookingDetails;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<BookingDetail> list(int propertyId, Date chartedDate) throws ParseException {
		
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<BookingDetail> bookingDetails = null;
		try {

			bookingDetails = (List<BookingDetail>) session.createQuery("select bd.bookingDate from BookingDetail bd,PropertyAccommodation pa"
					+ " where bd.bookingDate=:dates and pa.pmsProperty.propertyId=:propertyId and "
					+ "bd.propertyAccommodation.accommodationId = pa.accommodationId "
					+ "group by bd.bookingDate,bd.bookingDetailsId")
					.setParameter("propertyId", propertyId)
					.setParameter("dates", chartedDate)
					.list();
			
		} catch (HibernateException e) {
			logger.error(e);  
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return bookingDetails;
	}


    @SuppressWarnings("unchecked")
	public List<BookingDetail> bookingList(int bookingId) throws ParseException {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<BookingDetail> bookingDetails = null;
		
		
		try {
			
				bookingDetails = (List<BookingDetail>)session.createQuery("from BookingDetail where pmsBooking.bookingId =:bookingId ")
						.setParameter("bookingId", bookingId)						
						.list();
			
			

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return bookingDetails;
	}
    
    @SuppressWarnings("unchecked")
	public List<PmsBookedDetails> getBookingList(Integer bookingId){


		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsBookedDetails> bookedDetails = null;
		try {
             
			
			 bookedDetails = (List<PmsBookedDetails>) session.createQuery("select distinct pb.bookingId as bookingId, bd.propertyAccommodation.accommodationId as "
			 		+ "accommodationId, sum(bd.adultCount) as adults, sum(bd.childCount) as child, gd.pmsGuest.guestId as guestId,"
			 		+ "pb.pmsProperty.propertyId as propertyId,pb.arrivalDate as arrivalDate ,pb.departureDate as departureDate,"
			     	+ "pb.rooms as rooms ,pb.advanceAmount as advanceAmount,pb.pmsSource.sourceId as sourceId,"
			     	+ "pb.totalAmount as amount,pb.totalTax as tax,pb.pmsTags.tagId as tagId,pb.totalActualAmount as actualAmount,"
			     	+ " pb.createdDate as createdDate,gd.specialRequest as specialRequest,pb.promotionFirstName as promotionFirstName,"
			     	+ "pb.promotionSecondName as promotionSecondName,pb.couponName as couponName,pb.promotionAmount as promotionAmount, "
			     	+ "pb.couponAmount as couponAmount,bd.extraAdultRate as extraAdultRate,bd.extraChildRate as extraChildRate "
			     	+ " from BookingDetail bd ,PmsBooking pb ,"
			 		+ "BookingGuestDetail gd where bd.pmsBooking.bookingId =:bookingId and pb.bookingId = :bookingId and "
			 		+ "gd.pmsBooking.bookingId =:bookingId group by pb.bookingId,bd.propertyAccommodation.accommodationId,bd.adultCount,"
			 		+ "bd.childCount,gd.pmsGuest.guestId,pb.pmsProperty.propertyId ,pb.advanceAmount,pb.pmsSource.sourceId,"
			 		+ "gd.specialRequest,pb.arrivalDate ,pb.departureDate,pb.rooms,pb.couponName,pb.promotionAmount,pb.totalActualAmount,"
			 		+ "pb.totalAmount,pb.totalTax,pb.createdDate,pb.promotionFirstName,pb.promotionSecondName,pb.couponAmount,bd.extraAdultRate,bd.extraChildRate")
			.setParameter("bookingId", bookingId)
			.setResultTransformer(Transformers.aliasToBean(PmsBookedDetails.class)).list();
			 

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return bookedDetails;
	
    }
    
}
