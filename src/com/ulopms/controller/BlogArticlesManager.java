package com.ulopms.controller;

import java.text.ParseException;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;
import org.hibernate.transform.Transformers;

import com.ulopms.model.AccommodationRoom;
import com.ulopms.model.Area;
import com.ulopms.model.BlogArticles;
import com.ulopms.model.DashBoard;
import com.ulopms.util.HibernateUtil;


public class BlogArticlesManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(BlogArticlesManager.class);

	public BlogArticles add(BlogArticles blogArticles) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(blogArticles);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return blogArticles;
	}

	public BlogArticles edit(BlogArticles blogArticles) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(blogArticles);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return blogArticles;
	}

	public BlogArticles delete(int id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		BlogArticles blogArticles = (BlogArticles) session.load(BlogArticles.class, id);
		if (null != blogArticles) {
			session.delete(blogArticles);
		}
		session.getTransaction().commit();
		return blogArticles;
	}

	public BlogArticles find(int id) {
		BlogArticles blogArticles = new BlogArticles();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			blogArticles = (BlogArticles) session.load(BlogArticles.class, id);
			if(blogArticles.getBlogLocations().getBlogLocationId() == null){
				
			}
			// session.getTransaction().commit();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return blogArticles;
	}
	
	@SuppressWarnings("unchecked")
	public BlogArticles findDiscountCount(int blogArticleId) throws ParseException {
		
		
		BlogArticles blogArticles = new BlogArticles();
	
		Session session = HibernateUtil.getSessionFactory().openSession();
		//session.beginTransaction();
		
		try {
			session.beginTransaction();
			
			 blogArticles = (BlogArticles) session.createQuery("select ba.blogArticleId as blogArticleId, "
                    + "blogCategories.blogCategoryId as blogCategoryId,ba.blogArticleTitle as blogArticleTitle,ba.blogArticleDescription as blogArticleDescription from "
			 		+ "BlogArticles ba where ba.blogArticleId =:blogArticleId "
			 		+ "and ba.isActive = true and ba.isDeleted = false")
					 .setParameter("blogArticleId", blogArticleId)
					 .setResultTransformer(Transformers.aliasToBean(BlogArticles.class))
					 .setMaxResults(1)
					 .uniqueResult();
		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return blogArticles;
	}   
	
	public AccommodationRoom findArticleId(int blogArticleId) {
		AccommodationRoom accommodationRoom = new AccommodationRoom();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		
		//select count(distinct room_id) from property_accommodation_rooms par ,property_accommodation pa where par.accommodation_id =16 and
        //pa.property_id = 1
        
		try {
			session.beginTransaction();
			accommodationRoom = (AccommodationRoom)session.createQuery("select ba.blogArticleId as blogArticleId,ba.blogLocations.blogLocationId as blogLocationId, "
                    + "ba.blogCategories.blogCategoryId as blogCategoryId,ba.blogArticleTitle as blogArticleTitle,"
                    + "ba.blogArticleDescription as blogArticleDescription,ba.blogArticleUrl as blogArticleUrl "
                    + " from "
                    + "BlogArticles ba where ba.blogArticleId =:blogArticleId "
                    + "and ba.isActive = true and ba.isDeleted =false")
                     .setParameter("blogArticleId", blogArticleId)
					.setResultTransformer(Transformers.aliasToBean(AccommodationRoom.class))	
					.setMaxResults(1)
					.uniqueResult();
			//accommodationRoom = (AccommodationRoom) session.load(AccommodationRoom.class, id);
			// session.getTransaction().commit();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return accommodationRoom;
	}
	
/*	@SuppressWarnings("unchecked")
	public List<PromotionImages> findId(int promotionImageId) {
		List<PromotionImages> promotionImages = null;
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			//bookingDetails = (List<BookingDetail>) session.createQuery("from BookingDetail").list();
			promotionImages = (List<PromotionImages>)session.createQuery("from PromotionImages where promotionImageId =:promotionImageId")
				    .setParameter("promotionImageId",promotionImageId)
				    .list();
			// session.getTransaction().commit();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return promotionImages;
	}  */
	
	@SuppressWarnings("unchecked")
	public List<BlogArticles> list() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<BlogArticles> blogArticles = null;
		try {

			blogArticles = (List<BlogArticles>) session.createQuery("from BlogArticles where isActive = true and isDeleted =false ")
					
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return blogArticles;
	}	

	@SuppressWarnings("unchecked")
	public List<BlogArticles> list(int limit,int offset) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<BlogArticles> blogArticles = null;
		try {

			blogArticles = (List<BlogArticles>) session.createQuery("from BlogArticles where isActive = true and isDeleted =false ORDER BY  blogArticleId DESC")
					.setFirstResult(offset)
					.setMaxResults(limit)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return blogArticles;
	}	

	
	@SuppressWarnings("unchecked")
	public List<BlogArticles> list(int blogCategoryId) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<BlogArticles> blogArticles = null;
		try {

			blogArticles = (List<BlogArticles>) session.createQuery("from BlogArticles where blogCategories.blogCategoryId=:blogCategoryId"
					+ " and isActive = true and isDeleted =false ")
					.setParameter("blogCategoryId", blogCategoryId).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return blogArticles;
	}	
	
	@SuppressWarnings("unchecked")
	public List<BlogArticles> listLocationArticle(int blogLocationId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<BlogArticles> blogArticles = null;
		try {

			blogArticles = (List<BlogArticles>) session.createQuery("from BlogArticles where blogLocations.blogLocationId=:blogLocationId "
					+ "and isActive = true and isDeleted =false ")
					.setParameter("blogLocationId", blogLocationId).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return blogArticles;
	}	
	
	@SuppressWarnings("unchecked")
	public List<BlogArticles> listBlogArticleUrl(String articleUrl) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<BlogArticles> articles = null;
		try {

			articles = (List<BlogArticles>) session.createQuery("select a from BlogArticles a where blogArticleUrl=:articleUrl "
					+ "and a.isActive = 'true' and a.isDeleted = 'false'")
			.setParameter("articleUrl", articleUrl).list();
			
			

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return articles;
	}
	
	
	/*@SuppressWarnings("unchecked")
	 * from PropertyAccommodation where pmsProperty.propertyId=:propertyId "
					+ "and isActive = 'true' and isDeleted = 'false' order by baseAmount asc")
					.setParameter("propertyId", propertyId).list();
					
	public List<BlogArticles> list(String blogArticleTitle) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<BlogArticles> blogArticles = null;
		try {

			blogArticles = (List<BlogArticles>) session.createQuery("from BlogArticles where blogArticleTitle LIKE :blogArticleTitle and isActive = true and isDeleted =false ")
					.setParameter("blogArticleTitle","%"+blogArticleTitle+"%")
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return blogArticles;
	}	*/
	

	
}
