package com.ulopms.controller;

import java.text.ParseException;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;
import org.hibernate.transform.Transformers;

import com.ulopms.model.LocationContent;
import com.ulopms.model.PmsAmenity;
import com.ulopms.model.PmsDays;
import com.ulopms.model.PropertyContent;
import com.ulopms.util.HibernateUtil;


public class PropertyContentManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(PropertyContentManager.class);

	

	public PropertyContent edit(PropertyContent propertyContent) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(propertyContent);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyContent;
	}

	public PmsAmenity delete(int id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		PmsAmenity pmsAmenity = (PmsAmenity) session.load(PmsAmenity.class, id);
		if (null != pmsAmenity) {
			session.delete(pmsAmenity);
		}
		session.getTransaction().commit();
		return pmsAmenity;
	}

	public PmsDays find(int id) {
		PmsDays pmsDays = new PmsDays();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			pmsDays = (PmsDays) session.get(PmsDays.class, id);
			// session.getTransaction().commit();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return pmsDays;
	}
	
	public PropertyContent findId(int propertyId) throws ParseException {
		 PropertyContent propertyContent = null;
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			propertyContent = (PropertyContent) session.createQuery("select pc.propertyContentId as propertyContentId,pc.metaDescription as metaDescription,pc.schemaContent as schemaContent,pc.propertyDescription as propertyDescription,pc.scriptContent as scriptContent from PropertyContent pc where pc.pmsProperty.propertyId=:propertyId and isDeleted = 'false' and isActive = 'true'")
					.setParameter("propertyId", propertyId)
					.setResultTransformer(Transformers.aliasToBean(PropertyContent.class)).uniqueResult();
			// session.getTransaction().commit();
			/*dashBoard = (DashBoard) session.createQuery("select count(bd.propertyAccommodation.accommodationId) as roomCount from BookingDetail bd where (bd.bookingDate >=:fromDate and bd.bookingDate <=:toDate) and bd.propertyAccommodation.accommodationId =:accommodationId group by bd.propertyAccommodation.accommodationId")
					 .setParameter("accommodationId", accommodationId)
					 .setParameter("fromDate", fromDate)
					  .setParameter("toDate", toDate)
					  .setResultTransformer(Transformers.aliasToBean(DashBoard.class)).uniqueResult();*/
		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyContent;

	}
	
	public PmsAmenity find(String id) {
		PmsAmenity pmsAmenity = new PmsAmenity();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			pmsAmenity = (PmsAmenity) session.load(PmsAmenity.class, id);
			// session.getTransaction().commit();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return pmsAmenity;
	}

	@SuppressWarnings("unchecked")
	public List<PmsDays> list() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsDays> pmsdays = null;
		try {

			pmsdays = (List<PmsDays>) session.createQuery("from PmsDays where isActive = 'true' and isDeleted = 'false'").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return pmsdays;
	}
	
	@SuppressWarnings("unchecked")
	public List<PropertyContent> list(int propertyId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyContent> propertyContent = null;
		try {

			propertyContent = (List<PropertyContent>) session.createQuery("from PropertyContent pc  where pc.pmsProperty.propertyId=:propertyId and isActive = 'true' and isDeleted = 'false'")
					.setParameter("propertyId", propertyId)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyContent;
	}
	
}
