package com.ulopms.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;

import com.ulopms.model.PropertyPhoto;
import com.ulopms.util.HibernateUtil;


public class PropertyPhotoManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(PropertyPhotoManager.class);

	public PropertyPhoto add(PropertyPhoto propertyPhoto) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(propertyPhoto);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyPhoto;
	}

	public PropertyPhoto edit(PropertyPhoto propertyPhoto) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(propertyPhoto);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyPhoto;
	}

	public PropertyPhoto delete(int id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		PropertyPhoto propertyPhoto = (PropertyPhoto) session.load(PropertyPhoto.class, id);
		if (null != propertyPhoto) {
			session.delete(propertyPhoto);
		}
		session.getTransaction().commit();
		return propertyPhoto;
	}

	public PropertyPhoto find(int id) {
		PropertyPhoto propertyPhoto = new PropertyPhoto();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			propertyPhoto = (PropertyPhoto) session.load(PropertyPhoto.class, id);
			// session.getTransaction().commit();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return propertyPhoto;
	}

	@SuppressWarnings("unchecked")
	public List<PropertyPhoto> list() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyPhoto> propertyPhotos = null;
		try {

			propertyPhotos = (List<PropertyPhoto>) session.createQuery("from PropertyPhoto").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyPhotos;
	}
	@SuppressWarnings("unchecked")
	public List<PropertyPhoto> list(int propertyId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyPhoto> propertyPhotos = null;
		try {

			propertyPhotos = (List<PropertyPhoto>) session.createQuery("from PropertyPhoto "
					+ "where pmsProperty.propertyId=:propertyId and isActive = 'true' and isDeleted = 'false'")
					.setParameter("propertyId", propertyId)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyPhotos;
	}
	
	@SuppressWarnings("unchecked")
    public List<PropertyPhoto> commonPhotosList(int propertyId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyPhoto> propertyPhotos = null;
		try {

			propertyPhotos = (List<PropertyPhoto>) session.createQuery("from PropertyPhoto where pmsProperty.propertyId=:propertyId and pmsPhotoCategory.photoCategoryId !=2 and isActive = 'true' and isDeleted = 'false'")
					.setParameter("propertyId", propertyId)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyPhotos;
	}
	
	@SuppressWarnings("unchecked")
	public List<PropertyPhoto> accommodationPhotosList(int accommodationId) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyPhoto> propertyPhotos = null;
		try {

			propertyPhotos = (List<PropertyPhoto>) session.createQuery("from PropertyPhoto where  propertyAccommodation.accommodationId=:accommodationId and isActive = 'true' and isDeleted = 'false'")
					.setParameter("accommodationId", accommodationId)
					.list();
         
		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyPhotos;
	}
	
	    @SuppressWarnings("unchecked")
        public List<PropertyPhoto> list1(int propertyId,int photoCategoryId) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyPhoto> propertyPhotos = null;
		try {

			propertyPhotos = (List<PropertyPhoto>) session.createQuery("from PropertyPhoto where pmsProperty.propertyId=:propertyId and"
					+ " pmsPhotoCategory.photoCategoryId =:photoCategoryId and isActive = 'true' and isDeleted = 'false'")
					.setParameter("propertyId", propertyId)
					.setParameter("photoCategoryId", photoCategoryId)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyPhotos;
	}

	    @SuppressWarnings("unchecked")
        public List<PropertyPhoto> listAccommodation(int propertyId,int photoCategoryId,int accommodationId) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyPhoto> propertyPhotos = null;
		try {

			propertyPhotos = (List<PropertyPhoto>) session.createQuery("from PropertyPhoto where pmsProperty.propertyId=:propertyId and"
					+ " propertyAccommodation.accommodationId=:accommodationId and pmsPhotoCategory.photoCategoryId=:photoCategoryId "
					+ "and isActive = 'true' and isDeleted = 'false'")
					.setParameter("propertyId", propertyId)
					.setParameter("photoCategoryId", photoCategoryId)
					.setParameter("accommodationId", accommodationId)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyPhotos;
	}
}
