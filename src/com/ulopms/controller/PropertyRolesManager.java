package com.ulopms.controller;


import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;

import com.ulopms.model.PmsProperty;
import com.ulopms.model.PropertyRatePlan;
import com.ulopms.model.PmsSmartPrice;
import com.ulopms.model.PropertyRate;
import com.ulopms.model.PropertyRoles;
import com.ulopms.util.HibernateUtil;


public class PropertyRolesManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(PropertyRolesManager.class);

	public PropertyRoles add(PropertyRoles propertyRoles) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(propertyRoles);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyRoles;
	}

	public PropertyRoles edit(PropertyRoles propertyRoles) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(propertyRoles);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyRoles;
	}

	public PropertyRoles delete(Long id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		PropertyRoles propertyRoles = (PropertyRoles) session.load(PropertyRoles.class, id);
		if (null != propertyRoles) {
			session.delete(propertyRoles);
		}
		session.getTransaction().commit();
		return propertyRoles;
	}

	public PropertyRoles find(int id) {
		PropertyRoles propertyRoles = new PropertyRoles();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			propertyRoles = (PropertyRoles) session.load(PropertyRoles.class, id);
			
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return propertyRoles;
	}

	@SuppressWarnings("unchecked")
	public List<PropertyRoles> list() {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyRoles> roles = null;
		try {
              
			roles = (List<PropertyRoles>) session.createQuery("from PropertyRoles where "
					+ " isActive = 'true' and isDeleted = 'false'").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return roles;
	}
	
}
