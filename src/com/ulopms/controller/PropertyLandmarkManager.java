package com.ulopms.controller;

import java.text.ParseException;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;
import org.hibernate.transform.Transformers;

import com.ulopms.model.LocationContent;
import com.ulopms.model.PmsAmenity;
import com.ulopms.model.PmsDays;
import com.ulopms.model.PmsProperty;
import com.ulopms.model.PropertyContent;
import com.ulopms.model.PropertyLandmark;
import com.ulopms.util.HibernateUtil;


public class PropertyLandmarkManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(PropertyLandmarkManager.class);

	

	public PropertyLandmark edit(PropertyLandmark propertyLandmark) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(propertyLandmark);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyLandmark;
	}

	public PmsAmenity delete(int id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		PmsAmenity pmsAmenity = (PmsAmenity) session.load(PmsAmenity.class, id);
		if (null != pmsAmenity) {
			session.delete(pmsAmenity);
		}
		session.getTransaction().commit();
		return pmsAmenity;
	}

	public PropertyLandmark find(int propertyId) {
		PropertyLandmark propertyLandmark = new PropertyLandmark();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			propertyLandmark = (PropertyLandmark) session.createQuery("select pl.propertyLandmarkId as propertyLandmarkId, pl.propertyLandmark1 as propertyLandmark1,pl.propertyLandmark2 as propertyLandmark2,pl.propertyLandmark3 as propertyLandmark3,"
					+ "pl.propertyLandmark4 as propertyLandmark4,pl.propertyLandmark5 as propertyLandmark5,pl.propertyLandmark6 as propertyLandmark6,"
					+ "pl.propertyLandmark7 as propertyLandmark7,pl.propertyLandmark8 as propertyLandmark8,pl.propertyLandmark9 as propertyLandmark9,"
					+ "pl.propertyLandmark10 as propertyLandmark10,pl.propertyAttraction1 as propertyAttraction1,pl.propertyAttraction2 as propertyAttraction2,"
					+ "pl.propertyAttraction3 as propertyAttraction3,pl.propertyAttraction4 as propertyAttraction4,pl.propertyAttraction5 as propertyAttraction5,pl.kilometers1 as kilometers1,pl.kilometers2 as kilometers2,pl.kilometers3 as kilometers3,"
					+ "pl.kilometers4 as kilometers4,pl.kilometers5 as kilometers5,pl.kilometers6 as kilometers6,pl.kilometers7 as kilometers7,"
					+ "pl.kilometers8 as kilometers8,pl.kilometers9 as kilometers9,pl.kilometers10 as kilometers10,pl.distance1 as distance1,pl.distance2 as distance2,"
					+ "pl.distance3 as distance3,pl.distance4 as distance4,pl.distance5 as distance5 from PropertyLandmark pl where pl.pmsProperty.propertyId=:propertyId and isDeleted = 'false' and isActive = 'true'")
					.setParameter("propertyId", propertyId)
					.setResultTransformer(Transformers.aliasToBean(PropertyLandmark.class)).uniqueResult();
			//propertyLandmark = (PropertyLandmark) session.load(PropertyLandmark.class, id);
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return propertyLandmark;
	}
	
	public PropertyContent findId(int propertyId) throws ParseException {
		 PropertyContent propertyContent = null;
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			propertyContent = (PropertyContent) session.createQuery("select pc.propertyContentId as propertyContentId,pc.metaDescription as metaDescription,"
					+ "pc.schemaContent as schemaContent,pc.propertyDescription as propertyDescription,pc.scriptContent as scriptContent from PropertyContent pc "
					+ "where pc.pmsProperty.propertyId=:propertyId and isDeleted = 'false' and isActive = 'true'")
					.setParameter("propertyId", propertyId)
					.setResultTransformer(Transformers.aliasToBean(PropertyContent.class)).uniqueResult();
			
		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyContent;

	}
	
	public PmsAmenity find(String id) {
		PmsAmenity pmsAmenity = new PmsAmenity();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			pmsAmenity = (PmsAmenity) session.load(PmsAmenity.class, id);
			// session.getTransaction().commit();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return pmsAmenity;
	}

	@SuppressWarnings("unchecked")
	public List<PmsDays> list() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsDays> pmsdays = null;
		try {

			pmsdays = (List<PmsDays>) session.createQuery("from PmsDays where isActive = 'true' and isDeleted = 'false'").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return pmsdays;
	}
	
	@SuppressWarnings("unchecked")
	public List<PropertyLandmark> list(int propertyId) {
		
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyLandmark> propertyLandmark = null;
		try {

			propertyLandmark = (List<PropertyLandmark>) session.createQuery("from PropertyLandmark pl  where pl.pmsProperty.propertyId=:propertyId and isActive = 'true' "
					+ "and isDeleted = 'false'")
					.setParameter("propertyId", propertyId)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyLandmark;
	}
	
}
