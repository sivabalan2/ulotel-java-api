package com.ulopms.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;

import com.ulopms.model.BlogCategories;
import com.ulopms.model.BlogLocations;
import com.ulopms.util.HibernateUtil;


public class BlogLocationsManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(BlogLocationsManager.class);

	public BlogLocations add(BlogLocations blogLocations) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(blogLocations);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return blogLocations;
	}

	public BlogLocations edit(BlogLocations blogLocations) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(blogLocations);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return blogLocations;
	}

	public BlogLocations delete(int id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		BlogLocations blogLocations = (BlogLocations) session.load(BlogLocations.class, id);
		if (null != blogLocations) {
			session.delete(blogLocations);
		}
		session.getTransaction().commit();
		return blogLocations;
	}

	public BlogLocations find(int id) {
		BlogLocations blogLocations = new BlogLocations();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			blogLocations = (BlogLocations) session.load(BlogLocations.class, id);
			// session.getTransaction().commit();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return blogLocations;
	}
	
	@SuppressWarnings("unchecked")
	public List<BlogLocations> list(int limit,int offset) {

		//Session session = HibernateUtil.getSessionFactory().openSession();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<BlogLocations> blogLocations = null;
		try {

			blogLocations = (List<BlogLocations>) session.createQuery("from BlogLocations where isActive = 'true' "
					+ "and isDeleted = 'false' ")
					.setFirstResult(offset)
					.setMaxResults(limit)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return blogLocations;
	}
	
	@SuppressWarnings("unchecked")
	public List<BlogLocations> list() {

		//Session session = HibernateUtil.getSessionFactory().openSession();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<BlogLocations> blogLocations = null;
		try {

			blogLocations = (List<BlogLocations>) session.createQuery("from BlogLocations where isActive = 'true' "
					+ "and isDeleted = 'false' ")					
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return blogLocations;
	}
	
	@SuppressWarnings("unchecked")
	public List<BlogLocations> listBlogLocationUrl(String blogLocationUrl) {

		//Session session = HibernateUtil.getSessionFactory().openSession();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<BlogLocations> blogLocations = null;
		try {

			blogLocations = (List<BlogLocations>) session.createQuery("from BlogLocations where isActive = 'true' "
					+ "and isDeleted = 'false' and blogLocationUrl=:blogLocationUrl ")					
					.setParameter("blogLocationUrl", blogLocationUrl).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return blogLocations;
	}
	
/*	@SuppressWarnings("unchecked")
	public List<PromotionImages> findId(int promotionImageId) {
		List<PromotionImages> promotionImages = null;
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			//bookingDetails = (List<BookingDetail>) session.createQuery("from BookingDetail").list();
			promotionImages = (List<PromotionImages>)session.createQuery("from PromotionImages where promotionImageId =:promotionImageId")
				    .setParameter("promotionImageId",promotionImageId)
				    .list();
			// session.getTransaction().commit();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return promotionImages;
	}

	@SuppressWarnings("unchecked")
	public List<PromotionImages> list() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PromotionImages> promotionImages = null;
		try {

			promotionImages = (List<PromotionImages>) session.createQuery("from PromotionImages where isActive = true and isDeleted =false ").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return promotionImages;
	}	*/
	

	
}
