package com.ulopms.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;

import com.ulopms.model.PmsStatus;
import com.ulopms.util.HibernateUtil;


public class PmsStatusManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(PmsStatusManager.class);

	public PmsStatus add(PmsStatus pmsStatus) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(pmsStatus);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return pmsStatus;
	}

	public PmsStatus edit(PmsStatus pmsStatus) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(pmsStatus);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return pmsStatus;
	}

	public PmsStatus delete(Long id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		PmsStatus pmsStatus = (PmsStatus) session.load(PmsStatus.class, id);
		if (null != pmsStatus) {
			session.delete(pmsStatus);
		}
		session.getTransaction().commit();
		return pmsStatus;
	}

	public PmsStatus find(int id) {
		PmsStatus pmsStatus = new PmsStatus();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			pmsStatus = (PmsStatus) session.load(PmsStatus.class, id);
			// session.getTransaction().commit();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return pmsStatus;
	}

	@SuppressWarnings("unchecked")
	public List<PmsStatus> list() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsStatus> pmsStatuss = null;
		try {

			pmsStatuss = (List<PmsStatus>) session.createQuery("from PmsStatus").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return pmsStatuss;
	}

	
}
