package com.ulopms.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;

import com.ulopms.model.PropertyItemCategory;
import com.ulopms.util.HibernateUtil;


public class PropertyItemCategoryManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(PropertyItemCategoryManager.class);

	public PropertyItemCategory add(PropertyItemCategory propertyItemCategory) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(propertyItemCategory);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyItemCategory;
	}

	public PropertyItemCategory edit(PropertyItemCategory propertyItemCategory) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(propertyItemCategory);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyItemCategory;
	}

	public PropertyItemCategory delete(int id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		PropertyItemCategory propertyItemCategory = (PropertyItemCategory) session.load(PropertyItemCategory.class, id);
		if (null != propertyItemCategory) {
			session.delete(propertyItemCategory);
		}
		session.getTransaction().commit();
		return propertyItemCategory;
	}

	public PropertyItemCategory find(int id) {
		PropertyItemCategory propertyItemCategory = new PropertyItemCategory();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			propertyItemCategory = (PropertyItemCategory) session.load(PropertyItemCategory.class, id);
			// session.getTransaction().commit();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return propertyItemCategory;
	}

	@SuppressWarnings("unchecked")
	public List<PropertyItemCategory> list(int propertyId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyItemCategory> propertyItemCategorys = null;
		try {

			propertyItemCategorys = (List<PropertyItemCategory>) session.createQuery("from PropertyItemCategory where pmsProperty.propertyId=:propertyId and isActive = 'true' and isDeleted = 'false'")
					.setParameter("propertyId", propertyId).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyItemCategorys;
	}

	
}
