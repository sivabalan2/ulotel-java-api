package com.ulopms.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;

import com.ulopms.model.PropertyGuestReview;
import com.ulopms.util.HibernateUtil;


public class PropertyGuestReviewManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(PropertyGuestReviewManager.class);

	public PropertyGuestReview add(PropertyGuestReview guestReviews) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(guestReviews);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return guestReviews;
	}

	public PropertyGuestReview edit(PropertyGuestReview guestReviews) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(guestReviews);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return guestReviews;
	}

	public PropertyGuestReview delete(int id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		PropertyGuestReview guestReviews = (PropertyGuestReview) session.load(PropertyGuestReview.class, id);
		if (null != guestReviews) {
			session.delete(guestReviews);
		}
		session.getTransaction().commit();
		return guestReviews;
	}

	public PropertyGuestReview find(int id) {
		PropertyGuestReview guestReviews = new PropertyGuestReview();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			guestReviews = (PropertyGuestReview) session.load(PropertyGuestReview.class, id);
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return guestReviews;
	}

	@SuppressWarnings("unchecked")
	public List<PropertyGuestReview> list() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyGuestReview> guestReviews = null;
		try {

			guestReviews = (List<PropertyGuestReview>) session.createQuery("from PropertyGuestReview where isActive = 'true'"
					+ " and isDeleted = 'false' ").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return guestReviews;
	}
	

	@SuppressWarnings("unchecked")
	public List<PropertyGuestReview> list(int limit, int offset) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyGuestReview> guestReviews = null;
		try {

			guestReviews = (List<PropertyGuestReview>) session.createQuery("from PropertyGuestReview pr "
					+ " where  pr.isActive = 'true' and pr.isDeleted = 'false' ")
					.setFirstResult(offset)
					.setMaxResults(limit)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return guestReviews;
	}
	
	@SuppressWarnings("unchecked")
	public List<PropertyGuestReview> listReview(int guestReviewId, int reviewPropertyId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyGuestReview> guestReviews = null;
		try {

			guestReviews = (List<PropertyGuestReview>) session.createQuery("from PropertyGuestReview gr "
					+ " where  gr.isActive = 'true' and gr.isDeleted = 'false' and gr.guestReviewId=:guestReviewId"
					+ " and gr.pmsProperty.propertyId=:reviewPropertyId")
					.setParameter("guestReviewId",guestReviewId)
					.setParameter("reviewPropertyId",reviewPropertyId)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return guestReviews;
	}
	
	@SuppressWarnings("unchecked")
	public List<PropertyGuestReview> listReview(int reviewPropertyId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyGuestReview> guestReviews = null;
		try {

			guestReviews = (List<PropertyGuestReview>) session.createQuery("from PropertyGuestReview gr "
					+ " where  gr.isActive = 'true' and gr.isDeleted = 'false' "
					+ " and gr.pmsProperty.propertyId=:reviewPropertyId")
					.setParameter("reviewPropertyId",reviewPropertyId)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return guestReviews;
	}
}
