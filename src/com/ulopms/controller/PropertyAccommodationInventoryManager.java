package com.ulopms.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;
import org.hibernate.transform.Transformers;

import com.ulopms.model.PmsAvailableRooms;
import com.ulopms.model.PropertyAccommodationInventory;
import com.ulopms.model.State;
import com.ulopms.util.HibernateUtil;
import com.ulopms.view.BookingListAction;


public class PropertyAccommodationInventoryManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(PropertyAccommodationInventoryManager.class);

	public PropertyAccommodationInventory add(PropertyAccommodationInventory propertyAccommodationInventory) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(propertyAccommodationInventory);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyAccommodationInventory;
	}

	public PropertyAccommodationInventory edit(PropertyAccommodationInventory propertyAccommodationInventory) {
		
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(propertyAccommodationInventory);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyAccommodationInventory;
	}

	public PropertyAccommodationInventory delete(Long id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		PropertyAccommodationInventory propertyAccommodationInventory = (PropertyAccommodationInventory) session.load(State.class, id);
		if (null != propertyAccommodationInventory) {
			session.delete(propertyAccommodationInventory);
		}
		session.getTransaction().commit();
		return propertyAccommodationInventory;
	}

	public PropertyAccommodationInventory find(Integer id) {
		PropertyAccommodationInventory propertyAccommodationInventory = new PropertyAccommodationInventory();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			propertyAccommodationInventory = (PropertyAccommodationInventory) session.load(PropertyAccommodationInventory.class, id);
			// session.getTransaction().commit();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return propertyAccommodationInventory;
	}
	
	 public PropertyAccommodationInventory findInventoryCount(Date arrivalDate ,Integer accommodationId) {
		 PropertyAccommodationInventory propertyAccommodationInventory = new PropertyAccommodationInventory();
			Session session = HibernateUtil.getSessionFactory().getCurrentSession();
			try {
				/*
				 
				 SELECT * FROM property_accommodation_rooms WHERE NOT EXISTS(SELECT 1 FROM booking_details WHERE booking_details.room_id=property_accommodation_rooms.room_id and accommodation_id='16' and booking_date between '2017-06-17' and '2017-06-18')and accommodation_id='16' limit 1
				 
				 select roomId as roomId from propertyAcccommodationRoom where not exists(select 1 from bookingDetail 
					 where bookingDetail.propertyAccommodationRoom.roomId = propertyAccommodationRoom.roomId 
					 and bookingDetail.propertyAccommodation.accommodationId = :accommodationId and bookingDate between :arrivalDate and :departureDate)
					  and propertyAcccommodationRoom.accommodationId =:accommodationId
				 */
				
				session.beginTransaction();
				propertyAccommodationInventory = (PropertyAccommodationInventory)session.createQuery("select inventoryCount as inventoryCount,"
						+ "inventoryId as inventoryId,createdDate as createdDate from PropertyAccommodationInventory pai  "
						+ "where  pai.blockedDate = :arrivalDate and pai.propertyAccommodation.accommodationId = :accommodationId "
						+ "and pai.isActive = 'true' and pai.isDeleted = 'false'")						
			    .setParameter("arrivalDate", arrivalDate).setParameter("accommodationId", accommodationId)
			    .setResultTransformer(Transformers.aliasToBean(PropertyAccommodationInventory.class)).uniqueResult();
				// session.getTransaction().commit();
				
			   	if(propertyAccommodationInventory == null){
		           return null;
		                
		          
		          }  
			} catch (Exception e1) {
				logger.error(e1);
			} finally {
				session = null;
			}
			
			//return Optional.ofNullable(propertyAccommodationInventory).orElse(propertyAccommodationInventory);
			return propertyAccommodationInventory;
		}
	 
	 public PropertyAccommodationInventory findId(Date arrivalDate ,Integer accommodationId) {
		 PropertyAccommodationInventory propertyAccommodationInventory = new PropertyAccommodationInventory();
			Session session = HibernateUtil.getSessionFactory().getCurrentSession();
			try {
				/*
				 
				 SELECT * FROM property_accommodation_rooms WHERE NOT EXISTS(SELECT 1 FROM booking_details WHERE booking_details.room_id=property_accommodation_rooms.room_id and accommodation_id='16' and booking_date between '2017-06-17' and '2017-06-18')and accommodation_id='16' limit 1
				 
				 select roomId as roomId from propertyAcccommodationRoom where not exists(select 1 from bookingDetail where bookingDetail.propertyAccommodationRoom.roomId = propertyAccommodationRoom.roomId and bookingDetail.propertyAccommodation.accommodationId = :accommodationId and bookingDate between :arrivalDate and :departureDate) and propertyAcccommodationRoom.accommodationId =:accommodationId
				 */
				
				session.beginTransaction();
				propertyAccommodationInventory = (PropertyAccommodationInventory)session.createQuery("select inventoryId as inventoryId "
						+ "from PropertyAccommodationInventory pai  "
						+ "where  pai.blockedDate = :arrivalDate and pai.propertyAccommodation.accommodationId = :accommodationId "
						+ "and pai.isActive = 'true' and pai.isDeleted = 'false' ")						
			    .setParameter("arrivalDate", arrivalDate).setParameter("accommodationId", accommodationId)
			    .setResultTransformer(Transformers.aliasToBean(PropertyAccommodationInventory.class)).uniqueResult();
				// session.getTransaction().commit();
				
			   /*	if(roomCount == null){
		            
		           return null;
		                
		          
		          }  */
			} catch (Exception e1) {
				logger.error(e1);
			} finally {
				session = null;
			}
			
			return Optional.ofNullable(propertyAccommodationInventory).orElse(propertyAccommodationInventory);
			//return roomCount;
		}

	@SuppressWarnings("unchecked")
	public List<PropertyAccommodationInventory> list() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyAccommodationInventory> propertyAccommodationInventory = null;
		try {

			propertyAccommodationInventory = (List<PropertyAccommodationInventory>) session.createQuery("from PropertyAccommodationInventory").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyAccommodationInventory;
	}
	
	@SuppressWarnings("unchecked")
	public List<PropertyAccommodationInventory> listInventory(int accommodationId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyAccommodationInventory> propertyAccommodationInventory = null;
		try {

			propertyAccommodationInventory = (List<PropertyAccommodationInventory>) session.createQuery("from PropertyAccommodationInventory pai where "
					+ "pai.propertyAccommodation.accommodationId =:accommodationId")
					.setParameter("accommodationId", accommodationId)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyAccommodationInventory;
	}

	@SuppressWarnings("unchecked")
	public List<BookingListAction> listAccommodationInventory(int propertyId,Date date ){
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<BookingListAction> accommodationInventory=null;
		
		try {

			accommodationInventory = (List<BookingListAction>) session.createQuery("select pai.inventoryId as inventoryId, pai.inventoryCount as inventoryCount,"
					+ "pai.blockedDate as  blockedDate, pai.propertyAccommodation.accommodationId as accommodationId, pai.createdDate as createdDate,pai.createdBy as createdBy "
					+ "from PropertyAccommodation pa,PropertyAccommodationInventory pai where pa.accommodationId = pai.propertyAccommodation.accommodationId "
					+ " and pa.pmsProperty.propertyId =:propertyId and pai.isActive='true' and pai.isDeleted='false' and pa.isActive='true' and pa.isDeleted='false' order by pai.inventoryId desc ")
					.setParameter("propertyId", propertyId)
					.setResultTransformer(Transformers.aliasToBean(BookingListAction.class)).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		
		return accommodationInventory;
	}
	
}
