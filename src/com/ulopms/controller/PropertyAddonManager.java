package com.ulopms.controller;


import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;

import com.ulopms.model.PmsProperty;
import com.ulopms.model.PropertyAddon;
import com.ulopms.model.PropertyRatePlan;
import com.ulopms.model.PmsSmartPrice;
import com.ulopms.model.PropertyRate;
import com.ulopms.model.PropertyRoles;
import com.ulopms.util.HibernateUtil;


public class PropertyAddonManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(PropertyAddonManager.class);

	public PropertyAddon add(PropertyAddon propertyAddon) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(propertyAddon);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyAddon;
	}

	public PropertyAddon edit(PropertyAddon propertyAddon) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(propertyAddon);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyAddon;
	}

	public PropertyAddon delete(Long id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		PropertyAddon propertyAddon = (PropertyAddon) session.load(PropertyAddon.class, id);
		if (null != propertyAddon) {
			session.delete(propertyAddon);
		}
		session.getTransaction().commit();
		return propertyAddon;
	}

	public PropertyAddon find(int id) {
		PropertyAddon propertyAddon = new PropertyAddon();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			propertyAddon = (PropertyAddon) session.load(PropertyAddon.class, id);
			
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return propertyAddon;
	}

	@SuppressWarnings("unchecked")
	public List<PropertyAddon> list() {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyAddon> listAddon = null;
		try {
              
			listAddon = (List<PropertyAddon>) session.createQuery("from PropertyAddon where "
					+ " isActive = 'true' and isDeleted = 'false'").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return listAddon;
	}
	
}
