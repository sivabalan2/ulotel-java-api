package com.ulopms.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;
import org.hibernate.transform.Transformers;

import com.ulopms.model.DashBoard;
import com.ulopms.model.PmsSmartPrice;
import com.ulopms.model.PropertyRateDetail;
import com.ulopms.util.HibernateUtil;


public class PropertyRateDetailManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(PropertyRateDetailManager.class);

	public PropertyRateDetail add(PropertyRateDetail propertyRateDetail) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(propertyRateDetail);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyRateDetail;
	}

	public PropertyRateDetail edit(PropertyRateDetail propertyRateDetail) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(propertyRateDetail);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyRateDetail;
	}

	public PropertyRateDetail delete(Long id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		PropertyRateDetail PropertyRateDetail = (PropertyRateDetail) session.load(PropertyRateDetail.class, id);
		if (null != PropertyRateDetail) {
			session.delete(PropertyRateDetail);
		}
		session.getTransaction().commit();
		return PropertyRateDetail;
	}

	public PropertyRateDetail find(Long id) {
		PropertyRateDetail propertyRateDetail = new PropertyRateDetail();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			propertyRateDetail = (PropertyRateDetail) session.load(PropertyRateDetail.class, id);
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return propertyRateDetail;
	}
	
	public PropertyRateDetail find1(int propertyRateId) {
		PropertyRateDetail propertyRateDetail = new PropertyRateDetail();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			propertyRateDetail = (PropertyRateDetail) session.createQuery(" from PropertyRateDetail pr  where pr.propertyRate.propertyRateId=:propertyRateId "
					+ "and isActive = 'true' and isDeleted = 'false'").setParameter("propertyRateId", propertyRateId)
			.setFirstResult(0)
			.setMaxResults(1)
			.uniqueResult();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return propertyRateDetail;
	}
	
	public PropertyRateDetail find(int propertyRateDetailsId) {
		PropertyRateDetail propertyRateDetail = new PropertyRateDetail();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			propertyRateDetail = (PropertyRateDetail) session.load(PropertyRateDetail.class, propertyRateDetailsId);
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return propertyRateDetail;
	}

	@SuppressWarnings("unchecked")
	public List<PropertyRateDetail> list(int propertyRateId,int dayPart) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyRateDetail> propertyRateDetails = null;
		try {

			propertyRateDetails = (List<PropertyRateDetail>) session.createQuery("from PropertyRateDetail  where dayPart=:dayPart "
					+ "and propertyRate.propertyRateId= :propertyRateId and  isActive = 'true' and isDeleted = 'false'")
					.setParameter("propertyRateId", propertyRateId).setParameter("dayPart", dayPart).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyRateDetails;
	}
	
	@SuppressWarnings("unchecked")
	public List<PropertyRateDetail> list(int propertyRateId) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyRateDetail> propertyRateDetails = null;
		try {

			propertyRateDetails = (List<PropertyRateDetail>) session.createQuery("from PropertyRateDetail  where "
					+ " propertyRate.propertyRateId= :propertyRateId").setParameter("propertyRateId", propertyRateId).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyRateDetails;
	}

	public PropertyRateDetail listDetails(int propertyRateId) {
		PropertyRateDetail rateDetails = new PropertyRateDetail();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			rateDetails = (PropertyRateDetail) session.createQuery(" from PropertyRateDetail pr  where propertyRate.propertyRateId=:propertyRateId "
					+ "and isActive = 'true' and isDeleted = 'false'").setParameter("propertyRateId", propertyRateId)
			.setFirstResult(0)
			.setMaxResults(1)
			.uniqueResult();
			
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return rateDetails;
	}

	
	@SuppressWarnings("unchecked")
	public List<PropertyRateDetail> list(int propertyRateId, String daysOfWeek) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyRateDetail> propertyRateDetails = null;
		try {

			propertyRateDetails = (List<PropertyRateDetail>) session.createQuery("from PropertyRateDetail where daysOfWeek=:daysOfWeek and"
					+ " propertyRate.propertyRateId=:propertyRateId and  isActive = 'true' and isDeleted = 'false'")
					.setParameter("daysOfWeek", daysOfWeek)
					.setParameter("propertyRateId", propertyRateId)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyRateDetails;
	}

	@SuppressWarnings("unchecked")
	public List<PropertyRateDetail> listRateDetail(int propertyRateId) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        List<PropertyRateDetail> propertyRateDetail = null;
        try {
             
        	propertyRateDetail = (List<PropertyRateDetail>) session.createQuery("from PropertyRateDetail propertyRateDetails "
        			+ "where  propertyRateDetails.propertyRate.propertyRateId=:propertyRateId and propertyRateDetails.isActive = 'true' and propertyRateDetails.isDeleted = 'false'")
        			.setParameter("propertyRateId", propertyRateId).list();

        } catch (HibernateException e) {
            logger.error(e);
            session.getTransaction().rollback();
        } finally {
            session.getTransaction().commit();
            session=null;
        }
        return propertyRateDetail;
    }
	
	@SuppressWarnings("unchecked")
	public List<PmsSmartPrice> listSmartPriceDetail(int bookingRange) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        List<PmsSmartPrice> smartPriceDetail = null;
        try {
             
        	smartPriceDetail = (List<PmsSmartPrice>) session.createQuery("from PmsSmartPrice smartPrice where "
        			+ " :bookingRange between smartPrice.percentFrom and smartPrice.percentTo and smartPrice.isActive = 'true' and smartPrice.isDeleted = 'false'")
        			.setParameter("bookingRange", (double)bookingRange).list();

        } catch (HibernateException e) {
            logger.error(e);
            session.getTransaction().rollback();
        } finally {
            session.getTransaction().commit();
            session=null;
        }
        return smartPriceDetail;
    }
	
	@SuppressWarnings("unchecked")
	public List<PropertyRateDetail> listRateDetail(int propertyRateId,String weekday) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        List<PropertyRateDetail> propertyRateDetail = null;
        try {
             
        	propertyRateDetail = (List<PropertyRateDetail>) session.createQuery("from PropertyRateDetail propertyRateDetails "
        			+ "where  propertyRateDetails.propertyRate.propertyRateId=:propertyRateId and daysOfWeek=:weekday "
        			+ "and propertyRateDetails.isActive = 'true' and propertyRateDetails.isDeleted = 'false'")
        			.setParameter("propertyRateId", propertyRateId)
        			.setParameter("weekday", weekday).list();

        } catch (HibernateException e) {
            logger.error(e);
            session.getTransaction().rollback();
        } finally {
            session.getTransaction().commit();
            session = null;
        }
        return propertyRateDetail;
    }
	
	@SuppressWarnings("unchecked")
	public List<PropertyRateDetail> listMinMaxRateDetail(int propertyRateId,String weekday) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        session.beginTransaction();
        List<PropertyRateDetail> propertyRateDetail = null;
        try {
             
        	propertyRateDetail = (List<PropertyRateDetail>) session.createQuery("from PropertyRateDetail propertyRateDetails "
        			+ "where  propertyRateDetails.propertyRate.propertyRateId=:propertyRateId and daysOfWeek=:weekday "
        			+ "and propertyRateDetails.isActive = 'true' and propertyRateDetails.isDeleted = 'false' and "
        			+ " (propertyRateDetails.minimumBaseAmount is not null and propertyRateDetails.minimumBaseAmount >0 ) "
        			+ " and (propertyRateDetails.maximumBaseAmount is not null and propertyRateDetails.maximumBaseAmount >0)")
        			.setParameter("propertyRateId", propertyRateId)
        			.setParameter("weekday", weekday).list();

        } catch (HibernateException e) {
            logger.error(e);
            session.getTransaction().rollback();
        } finally {
            session.getTransaction().commit();
            session = null;
        }
        return propertyRateDetail;
    }
}
