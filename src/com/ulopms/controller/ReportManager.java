package com.ulopms.controller;

import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;
import org.hibernate.transform.Transformers;
import org.joda.time.DateTime;

import com.ulopms.model.BookingDetail;
import com.ulopms.model.BookingDetailReport;
import com.ulopms.model.BookingGuestDetail;
import com.ulopms.model.DashBoard;
import com.ulopms.model.Revenue;
import com.ulopms.model.RevenueReport;
import com.ulopms.model.Screen;
import com.ulopms.model.UserGuestDetailReport;
import com.ulopms.util.HibernateUtil;


public class ReportManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(ReportManager.class);

	
	@SuppressWarnings("unchecked")
	public List<Screen> list() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<Screen> screens = null;
		try {

			screens = (List<Screen>) session.createQuery("from Screen").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return screens;
	}

	@SuppressWarnings("unchecked")
	public List<BookingDetail> BookingDetails() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<BookingDetail> bookingDetails = null;
		try {
			bookingDetails = (List<BookingDetail>)session.createQuery("from BookingDetail").list();
			//screens = (List<Screen>) session.createSQLQuery(sql).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return bookingDetails;
	}
	@SuppressWarnings("unchecked")
	public List<BookingDetail> BookingDetailsByProperty(int accommodationId, Date fromDate, Date toDate, int propertyId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<BookingDetail> bookingDetails = null;
		try {
			if(accommodationId>0)
			{
				bookingDetails = (List<BookingDetail>)session.createQuery("select a from BookingDetail a join a.pmsBooking b "
						+ "where a.bookingDate between :fromDate and :toDate and and a.isActive='true' and a.isDeleted='false' and "
					+ "a.propertyAccommodation.accommodationId=:accommodationId and b.pmsProperty.propertyId=:propertyId")
					.setParameter("accommodationId", accommodationId)
					.setParameter("fromDate", fromDate)
					.setParameter("toDate", toDate)
					.setParameter("propertyId", propertyId)
					.list();
			}
			else
			{
				bookingDetails = (List<BookingDetail>)session.createQuery("select a from BookingDetail a join a.pmsBooking b "
						+ "where a.bookingDate between :fromDate and :toDate and a.isActive='true' and a.isDeleted='false' and "
					+ "b.pmsProperty.propertyId=:propertyId")
						.setParameter("fromDate", fromDate)
						.setParameter("toDate", toDate)
						.setParameter("propertyId", propertyId)
						.list();
			}
			//screens = (List<Screen>) session.createSQLQuery(sql).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return bookingDetails;
	}
	@SuppressWarnings("unchecked")
	public List<Revenue> RevenueByProperty(int accommodationId, Date fromDate, Date toDate, int propertyId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<Revenue> revenues = null;
		try {
			/*if(accommodationId>0)
			{
				revenues = (List<Revenue>)session.createQuery("select sum(a.amount) as amount,count(a) as nights from BookingDetail 
						a join a.pmsBooking b where a.bookingDate between :fromDate and :toDate and "
					+ "a.propertyAccommodation.accommodationId=:accommodationId and b.pmsProperty.propertyId=:propertyId")
					.setParameter("accommodationId", accommodationId)
					.setParameter("fromDate", fromDate)
					.setParameter("toDate", toDate)
					.setParameter("propertyId", propertyId)
					.setResultTransformer(Transformers.aliasToBean(Revenue.class))
					.list();
			}
			else
			{
				revenues = (List<Revenue>)session.createQuery("select sum(a.amount) as amount,count(a) as nights from BookingDetail a join a.pmsBooking b 
				where a.bookingDate between :fromDate and :toDate and "
					+ "b.pmsProperty.propertyId=:propertyId")
						.setParameter("fromDate", fromDate)
						.setParameter("toDate", toDate)
						.setParameter("propertyId", propertyId)
						.setResultTransformer(Transformers.aliasToBean(Revenue.class))
						.list();
			}*/
			
			if(accommodationId>0){
				revenues=(List<Revenue>)session.createQuery("select coalesce(sum(bd.amount),0) as amount, coalesce(sum(bd.roomCount),0) as nights, "
						+ " coalesce(sum(bd.tax),0) as taxAmount from BookingDetail bd,PmsBooking pb"
						+ " where bd.propertyAccommodation.accommodationId=:accommodationId and bd.pmsBooking.bookingId=pb.bookingId "
						+ " and pb.arrivalDate between :fromDate and :toDate and bd.isActive='true' and bd.isDeleted='false' "
						+ " and pb.pmsProperty.propertyId=:propertyId and pb.pmsStatus.statusId <> 4 and pb.pmsStatus.statusId <> 1")
						.setParameter("propertyId", propertyId)
						.setParameter("accommodationId", accommodationId)
						.setParameter("fromDate", fromDate)
						.setParameter("toDate", toDate)
						.setResultTransformer(Transformers.aliasToBean(Revenue.class))
						.list();
			}else{
				revenues=(List<Revenue>)session.createQuery("select coalesce(sum(bd.amount),0) as amount, coalesce(sum(bd.roomCount),0) as nights,"
						+ " coalesce(sum(bd.tax),0) as taxAmount from PmsBooking pb,BookingDetail bd where bd.pmsBooking.bookingId=pb.bookingId and "
						+ " pb.arrivalDate between :fromDate and :toDate and bd.isActive='true' and bd.isDeleted='false' "
						+ "and pb.pmsProperty.propertyId=:propertyId and pb.pmsStatus.statusId <> 4 and pb.pmsStatus.statusId <> 1")
						.setParameter("propertyId", propertyId)
						.setParameter("fromDate", fromDate)
						.setParameter("toDate", toDate)
						.setResultTransformer(Transformers.aliasToBean(Revenue.class))
						.list();
			}
		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return revenues;
	}
	@SuppressWarnings("unchecked")
	public List<BookingGuestDetail> BookingGuestDetails(int bookingId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<BookingGuestDetail> bookingGuestDetails = null;
		try {
			bookingGuestDetails = (List<BookingGuestDetail>)session.createQuery("select a from BookingGuestDetail a join a.pmsBooking"
					+ " b where a.isPrimary=true and b.bookingId=:bookingId")
					.setParameter("bookingId", bookingId)
					.list();
			//screens = (List<Screen>) session.createSQLQuery(sql).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return bookingGuestDetails;
	}
	
	@SuppressWarnings("unchecked")
	public List<BookingDetailReport> listTodayBookingDetail(int propertyId,String todayDate) throws ParseException {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<BookingDetailReport> bookingDetails = null;
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		java.util.Date today = df.parse(todayDate);
		try {
			String strBookingQuery=null;
			strBookingQuery="select pb.bookingId as bookingId, coalesce(sum(bd.roomCount),0) as roomCount, pb.arrivalDate as arrivalDate,pb.departureDate as departureDate,"
					+ "pg.firstName as firstName,pg.lastName as lastName,coalesce(sum(bd.amount),0) as amount,pb.createdDate as createdDate,"
					+ "coalesce(sum(bd.tax),0) as taxAmount, pg.city as city,pg.phone as mobileNumber,pg.landline as phoneNumber,pg.emailId as emailId, "
					+ " bd.propertyAccommodation.accommodationId as accommodationId,bd.pmsStatus.statusId as statusId, "
					+ " coalesce(sum(bd.advanceAmount),0) as advanceAmount,pb.pmsSource.sourceId as sourceId "
					+ "from PmsBooking pb,BookingDetail bd,BookingGuestDetail bgd,PmsGuest pg "
					+ " where pb.pmsProperty.propertyId=:propertyId "
					+ " and pb.bookingId=bd.pmsBooking.bookingId "
					+ " and pb.bookingId=bgd.pmsBooking.bookingId "
					+ " and pg.guestId=bgd.pmsGuest.guestId and bd.isActive='true' and bd.isDeleted='false' ";
					
					strBookingQuery+= " and :todayDate between pb.arrivalDate and pb.departureDate ";
					
					strBookingQuery+= "group by bd.roomCount, pb.bookingId,pb.arrivalDate,pb.departureDate,pb.createdDate,pg.firstName,pg.lastName,pg.city,"
							+ "pg.phone,pg.landline,pg.emailId,bd.propertyAccommodation.accommodationId,pb.pmsSource.sourceId,"
							+ "bd.pmsStatus.statusId,pb.advanceAmount order by pb.bookingId DESC";
					
					bookingDetails = (List<BookingDetailReport>) session.createQuery(strBookingQuery)
							.setParameter("propertyId", propertyId)
							.setParameter("todayDate", today)
							.setResultTransformer(Transformers.aliasToBean(BookingDetailReport.class)).list();
			
		} catch (HibernateException e) {
			logger.error(e);  
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
			
		}
		return bookingDetails;
	}
	
	@SuppressWarnings("unchecked")
	public List<BookingDetailReport> listBookingDetail(int propertyId,Date start,Date end,int accommodationId,String strFilterDateName,int filterBookingId) throws ParseException {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<BookingDetailReport> bookingDetails = null;
		try {
			String strBookingQuery=null;
			strBookingQuery="select pb.bookingId as bookingId, coalesce(sum(bd.roomCount),0) as roomCount, pb.arrivalDate as arrivalDate,pb.departureDate as departureDate,"
					+ "pg.firstName as firstName,pg.lastName as lastName,coalesce(sum(bd.amount),0) as amount,pb.createdDate as createdDate,"
					+ "coalesce(sum(bd.tax),0) as taxAmount, pg.city as city,pg.phone as mobileNumber,pg.landline as phoneNumber,pg.emailId as emailId, "
					+ " bd.propertyAccommodation.accommodationId as accommodationId,bd.adultCount as adultCount,bd.childCount as childCount,"
					+ " bd.pmsStatus.statusId as statusId,coalesce(sum(bd.otaCommission),0) as otaCommission,coalesce(sum(bd.otaTax),0) as otaTax,"
					+ "pb.pmsSource.sourceId as sourceId,coalesce(sum(bd.advanceAmount),0) as advanceAmount,pb.addonAmount as addonAmount,"
					+ "coalesce(sum(bd.purchaseRate),0) as purchaseRate,pb.promotionAmount as promotionAmount,pb.couponAmount as couponAmount,"
					+ "sum(bd.extraAdultRate) as extraAdultRate,sum(bd.extraChildRate) as extraChildRate  "
					+ "from PmsBooking pb,BookingDetail bd,BookingGuestDetail bgd,PmsGuest pg "
					+ " where pb.pmsProperty.propertyId=:propertyId "
					+ " and pb.bookingId=bd.pmsBooking.bookingId "
					+ " and pb.bookingId=bgd.pmsBooking.bookingId "
					+ " and pg.guestId=bgd.pmsGuest.guestId and bd.isActive='true' and bd.isDeleted='false' ";
					
					if(accommodationId>0){
						strBookingQuery+=" and bd.propertyAccommodation.accommodationId=:accommodationId ";
						if(strFilterDateName.equalsIgnoreCase("BookingDate")){
							strBookingQuery+=" and pb.createdDate between :startDate and :endDate ";
						}else if(strFilterDateName.equalsIgnoreCase("CheckInDate")){
							strBookingQuery+=" and pb.arrivalDate between :startDate and :endDate ";
						}else if(strFilterDateName.equalsIgnoreCase("CheckOutDate")){
							strBookingQuery+=" and pb.departureDate between :startDate and :endDate ";
						}else if(strFilterDateName.equalsIgnoreCase("StayInfo")){
							strBookingQuery+=" and :startDate between pb.arrivalDate and pb.departureDate and :endDate between pb.arrivalDate and pb.departureDate ";
						}
						
					}else{
						if(strFilterDateName.equalsIgnoreCase("BookingDate")){
							strBookingQuery+=" and pb.createdDate between :startDate and :endDate ";
						}else if(strFilterDateName.equalsIgnoreCase("CheckInDate")){
							strBookingQuery+=" and pb.arrivalDate between :startDate and :endDate ";
						}else if(strFilterDateName.equalsIgnoreCase("CheckOutDate")){
							strBookingQuery+=" and pb.departureDate between :startDate and :endDate ";
						}else if(strFilterDateName.equalsIgnoreCase("StayInfo")){
							strBookingQuery+=" and :startDate between pb.arrivalDate and pb.departureDate and :endDate between pb.arrivalDate and pb.departureDate ";
						}
					}
					strBookingQuery+= "group by bd.roomCount, pb.bookingId,pb.arrivalDate,pb.departureDate,pb.createdDate,"
							+ "pg.firstName,pg.lastName,pg.city,bd.purchaseRate,pb.addonAmount,"
							+ "pg.phone,pg.landline,pg.emailId,bd.propertyAccommodation.accommodationId,"
							+ "bd.adultCount,bd.childCount,bd.pmsStatus.statusId,pb.promotionAmount,pb.couponAmount,"
							+ "bd.extraAdultRate,bd.extraChildRate ";
					
					if(strFilterDateName.equalsIgnoreCase("BookingDate")){
						strBookingQuery+=" order by pb.createdDate DESC";
					}else if(strFilterDateName.equalsIgnoreCase("CheckInDate")){
						strBookingQuery+=" order by pb.arrivalDate DESC";
					}else if(strFilterDateName.equalsIgnoreCase("CheckOutDate")){
						strBookingQuery+=" order by pb.departureDate DESC";
					}else if(strFilterDateName.equalsIgnoreCase("StayInfo")){
						strBookingQuery+=" order by pb.bookingId DESC";
					}
					
					if(accommodationId>0){
						bookingDetails = (List<BookingDetailReport>) session.createQuery(strBookingQuery)
								.setParameter("propertyId", propertyId)
								.setParameter("startDate", start)
								.setParameter("endDate", end)
								.setParameter("accommodationId", accommodationId)
								.setResultTransformer(Transformers.aliasToBean(BookingDetailReport.class)).list();
					}
					else{
						bookingDetails = (List<BookingDetailReport>) session.createQuery(strBookingQuery)
								.setParameter("propertyId", propertyId)
								.setParameter("startDate", start)
								.setParameter("endDate", end)
								.setResultTransformer(Transformers.aliasToBean(BookingDetailReport.class)).list();
						
					}
			
		} catch (HibernateException e) {
			logger.error(e);  
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
			
		}
		return bookingDetails;
	}
	
	@SuppressWarnings("unchecked")
	public List<BookingDetailReport> listBookingDetail(int propertyId,int filterBookingId) throws ParseException {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<BookingDetailReport> bookingDetails = null;
		try {
			String strBookingQuery=null;
			strBookingQuery="select pb.bookingId as bookingId, coalesce(sum(bd.roomCount),0) as roomCount, pb.arrivalDate as arrivalDate,pb.departureDate as departureDate,"
					+ "pg.firstName as firstName,pg.lastName as lastName,coalesce(sum(bd.amount),0) as amount,pb.createdDate as createdDate,"
					+ "coalesce(sum(bd.tax),0) as taxAmount, pg.city as city,pg.phone as mobileNumber,pg.landline as phoneNumber,pg.emailId as emailId, "
					+ " bd.propertyAccommodation.accommodationId as accommodationId,bd.adultCount as adultCount,bd.childCount as childCount,"
					+ " bd.pmsStatus.statusId as statusId,coalesce(sum(bd.otaCommission),0) as otaCommission,coalesce(sum(bd.otaTax),0) as otaTax,"
					+ " pb.pmsSource.sourceId as sourceId,coalesce(sum(bd.advanceAmount),0) as advanceAmount,"
					+ "coalesce(sum(bd.purchaseRate),0) as purchaseRate,pb.pmsProperty.propertyId as propertyId,"
					+ "pb.promotionAmount as promotionAmount,pb.couponAmount as couponAmount,pb.addonAmount as addonAmount,"
					+ "sum(bd.extraAdultRate) as extraAdultRate,sum(bd.extraChildRate) as extraChildRate  "
					+ "from PmsBooking pb,BookingDetail bd,BookingGuestDetail bgd,PmsGuest pg "
					+ " where pb.pmsProperty.propertyId=:propertyId "
					+ " and pb.bookingId=bd.pmsBooking.bookingId "
					+ " and pb.bookingId=bgd.pmsBooking.bookingId "
					+ " and pg.guestId=bgd.pmsGuest.guestId and bd.isActive='true' and bd.isDeleted='false' ";
					
					strBookingQuery+=" and pb.bookingId =:bookingId ";
			
					strBookingQuery+= "group by bd.roomCount, pb.bookingId,pb.arrivalDate,pb.departureDate,"
							+ "pb.createdDate,pg.firstName,pg.lastName,pg.city,pb.addonAmount,"
							+ "pg.phone,pg.landline,pg.emailId,bd.propertyAccommodation.accommodationId,pb.pmsProperty.propertyId,"
							+ "bd.purchaseRate,bd.adultCount,bd.childCount,bd.pmsStatus.statusId"
							+ ",pb.promotionAmount,pb.couponAmount,bd.extraAdultRate, bd.extraChildRate order by pb.bookingId DESC";
					
					bookingDetails = (List<BookingDetailReport>) session.createQuery(strBookingQuery)
							.setParameter("propertyId", propertyId)
							.setParameter("bookingId", filterBookingId)
							.setResultTransformer(Transformers.aliasToBean(BookingDetailReport.class)).list();
					
			
		} catch (HibernateException e) {
			logger.error(e);  
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
			
		}
		return bookingDetails;
	}
	
	@SuppressWarnings("unchecked")
	public List<BookingDetailReport> listBookingDetail(int propertyId,Date start,Date end,int accommodationId) throws ParseException {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<BookingDetailReport> bookingDetails = null;
		try {
			String strBookingQuery=null;
			strBookingQuery="select pb.bookingId as bookingId, coalesce(sum(bd.roomCount),0) as roomCount, pb.arrivalDate as arrivalDate,pb.departureDate as departureDate,"
					+ "pg.firstName as firstName,pg.lastName as lastName,coalesce(sum(bd.amount),0) as amount,pb.createdDate as createdDate,"
					+ "coalesce(sum(bd.tax),0) as taxAmount, pg.city as city,pg.phone as mobileNumber,pg.landline as phoneNumber,pg.emailId as emailId, "
					+ " bd.propertyAccommodation.accommodationId as accommodationId,bd.adultCount as adultCount,bd.childCount as childCount,"
					+ "coalesce(sum(bd.otaCommission),0) as otaCommission,coalesce(sum(bd.otaTax),0) as otaTax,coalesce(sum(bd.purchaseRate),0) as purchaseRate,"
					+ "pb.pmsStatus.statusId as statusId,pb.promotionAmount as promotionAmount,pb.couponAmount as couponAmount,"
					+ " sum(bd.extraAdultRate) as extraAdultRate,sum(bd.extraChildRate) as extraChildRate,pb.addonAmount as addonAmount "
					+ "from PmsBooking pb,BookingDetail bd,BookingGuestDetail bgd,PmsGuest pg "
					+ " where pb.pmsProperty.propertyId=:propertyId "
					+ " and pb.bookingId=bd.pmsBooking.bookingId "
					+ " and pb.bookingId=bgd.pmsBooking.bookingId and bd.isActive='true' and bd.isDeleted='false' "
					+ " and pg.guestId=bgd.pmsGuest.guestId and bd.pmsStatus.statusId <> 4 and  bd.pmsStatus.statusId <> 1";
					
					if(accommodationId>0){
						strBookingQuery+=" and bd.propertyAccommodation.accommodationId=:accommodationId "
								+ " and pb.arrivalDate between :arrivalDate and :departureDate  ";
					}else{
						strBookingQuery+=" and pb.arrivalDate between :arrivalDate and :departureDate ";
					}
					strBookingQuery+= "group by bd.roomCount, pb.bookingId,pb.arrivalDate,pb.departureDate,pb.createdDate,pg.firstName,pg.lastName,pg.city,"
							+ "pg.phone,pg.landline,pg.emailId,bd.propertyAccommodation.accommodationId,bd.adultCount,bd.childCount,pb.addonAmount,"
							+ "pb.promotionAmount,pb.couponAmount,bd.purchaseRate,pb.pmsStatus.statusId,bd.extraAdultRate,bd.extraChildRate order by pb.bookingId DESC";
					
					if(accommodationId>0){
						bookingDetails = (List<BookingDetailReport>) session.createQuery(strBookingQuery)
								.setParameter("propertyId", propertyId)
								.setParameter("arrivalDate", start)
								.setParameter("departureDate", end)
								.setParameter("accommodationId", accommodationId)
								.setResultTransformer(Transformers.aliasToBean(BookingDetailReport.class)).list();
					}else{
						bookingDetails = (List<BookingDetailReport>) session.createQuery(strBookingQuery)
								.setParameter("propertyId", propertyId)
								.setParameter("arrivalDate", start)
								.setParameter("departureDate", end)
								.setResultTransformer(Transformers.aliasToBean(BookingDetailReport.class)).list();
					}
			
		} catch (HibernateException e) {
			logger.error(e);  
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
			
		}
		return bookingDetails;
	}
	
	@SuppressWarnings("unchecked")
	public List<BookingDetailReport> listBookingDetail(int propertyId,Date start,Date end,String filterDate) throws ParseException {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<BookingDetailReport> bookingDetails = null;
		try {
			String strBookingQuery=null;
			strBookingQuery="select pb.bookingId as bookingId, coalesce(sum(bd.roomCount),0) as roomCount, pb.arrivalDate as arrivalDate,pb.departureDate as departureDate,"
					+ "pg.firstName as firstName,pg.lastName as lastName,coalesce(sum(bd.amount),0) as amount,pb.createdDate as createdDate,"
					+ "coalesce(sum(bd.tax),0) as taxAmount, pg.city as city,pg.phone as mobileNumber,pg.landline as phoneNumber,pg.emailId as emailId, "
					+ " bd.propertyAccommodation.accommodationId as accommodationId,bd.adultCount as adultCount,bd.childCount as childCount,"
					+ "coalesce(sum(bd.otaCommission),0) as otaCommission,coalesce(sum(bd.otaTax),0) as otaTax,pb.pmsStatus.statusId as statusId,"
					+ "coalesce(sum(bd.purchaseRate),0) as purchaseRate,pb.promotionAmount as promotionAmount,pb.couponAmount as couponAmount,"
					+ "sum(bd.extraAdultRate) as extraAdultRate,sum(bd.extraChildRate) as extraChildRate,pb.addonAmount as addonAmount "
					+ "from PmsBooking pb,BookingDetail bd,BookingGuestDetail bgd,PmsGuest pg "
					+ " where pb.pmsProperty.propertyId=:propertyId "
					+ " and pb.bookingId=bd.pmsBooking.bookingId "
					+ " and pb.bookingId=bgd.pmsBooking.bookingId and bd.isActive='true' and bd.isDeleted='false' "
					+ " and pg.guestId=bgd.pmsGuest.guestId and bd.pmsStatus.statusId <> 4 and  bd.pmsStatus.statusId <> 1";
					
					if(filterDate.equalsIgnoreCase("BookingDate")){
						strBookingQuery+=" and pb.createdDate between :arrivalDate and :departureDate ";
					}else if(filterDate.equalsIgnoreCase("CheckInDate")){
						strBookingQuery+=" and pb.arrivalDate between :arrivalDate and :departureDate ";
					}else if(filterDate.equalsIgnoreCase("StayInfo")){
						strBookingQuery+=" and :arrivalDate between pb.arrivalDate and pb.departureDate"
								+ " and :departureDate between pb.arrivalDate and pb.departureDate ";
					}
					
					strBookingQuery+= "group by bd.roomCount, pb.bookingId,pb.arrivalDate,pb.departureDate,pb.createdDate,pg.firstName,pg.lastName,pg.city,"
							+ "pg.phone,pg.landline,pg.emailId,bd.propertyAccommodation.accommodationId,bd.adultCount,bd.childCount,pb.addonAmount,"
							+ "pb.pmsStatus.statusId,bd.purchaseRate,pb.promotionAmount,pb.couponAmount,bd.extraAdultRate,bd.extraChildRate order by pb.bookingId DESC";
					
					bookingDetails = (List<BookingDetailReport>) session.createQuery(strBookingQuery)
								.setParameter("propertyId", propertyId)
								.setParameter("arrivalDate", start)
								.setParameter("departureDate", end)
								.setResultTransformer(Transformers.aliasToBean(BookingDetailReport.class)).list();
					
			
		} catch (HibernateException e) {
			logger.error(e);  
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
			
		}
		return bookingDetails;
	}
	
	@SuppressWarnings("unchecked")
	public List<BookingDetailReport> listBookingDetail(Integer propertyId,Date start,Date end) throws ParseException {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<BookingDetailReport> bookingDetails = null;
		try {
			String strBookingQuery=null;
			strBookingQuery="select pb.pmsProperty.propertyId as propertyId, pb.bookingId as bookingId, coalesce(sum(bd.roomCount),0) as roomCount, pb.arrivalDate as arrivalDate,pb.departureDate as departureDate,"
					+ "pg.firstName as firstName,sum(bd.advanceAmount) as advanceAmount,pg.lastName as lastName,coalesce(sum(bd.amount),0) as amount,pb.createdDate as createdDate,"
					+ "coalesce(sum(bd.tax),0) as taxAmount,pg.guestId as guestId, pg.city as city,pg.phone as mobileNumber,pg.landline as phoneNumber,pg.emailId as emailId, "
					+ " bd.propertyAccommodation.accommodationId as accommodationId,bd.adultCount as adultCount,bd.childCount as childCount,pb.pmsSource.sourceId as sourceId,"
					+ "coalesce(sum(bd.otaCommission),0) as otaCommission,coalesce(sum(bd.otaTax),0) as otaTax,"
					+ "pb.pmsStatus.statusId as statusId,coalesce(sum(bd.purchaseRate),0) as purchaseRate,"
					+ "sum(bd.extraAdultRate) as extraAdultRate,sum(bd.extraChildRate) as extraChildRate "
					+ "from PmsBooking pb,BookingDetail bd,BookingGuestDetail bgd,PmsGuest pg "
					+ " where pb.pmsProperty.propertyId=:propertyId "
					+ " and pb.bookingId=bd.pmsBooking.bookingId "
					+ " and pb.bookingId=bgd.pmsBooking.bookingId and bd.isActive='true' and bd.isDeleted='false' "
					+ " and pg.guestId=bgd.pmsGuest.guestId and bd.pmsStatus.statusId <> 4 and  bd.pmsStatus.statusId <> 1";
					
						strBookingQuery+=" and (pb.arrivalDate between :arrivalDate   and :departureDate"
								+ " or pb.departureDate between :arrivalDate   and :departureDate) ";
					
					strBookingQuery+= "group by bd.roomCount, pb.bookingId,pb.arrivalDate,pb.departureDate,pb.createdDate,"
							+ "pg.firstName,pg.lastName,pg.city,bd.purchaseRate,pb.promotionAmount,pb.couponAmount"
							+ "pg.phone,pg.landline,pg.emailId,pg.guestId,bd.propertyAccommodation.accommodationId,"
							+ "bd.adultCount,bd.childCount,pb.pmsStatus.statusId,bd.extraAdultRate,bd.extraChildRate order by pb.bookingId DESC";
					
					bookingDetails = (List<BookingDetailReport>) session.createQuery(strBookingQuery)
								.setParameter("propertyId", propertyId)
								.setParameter("arrivalDate", start)
								.setParameter("departureDate", end)
								.setResultTransformer(Transformers.aliasToBean(BookingDetailReport.class)).list();
					
			
		} catch (HibernateException e) {
			logger.error(e);  
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
			
		}
		return bookingDetails;
	}
	
	@SuppressWarnings("unchecked")
	public List<BookingDetailReport> listStatusBookingDetail(Integer propertyId,Date start,Date end,String statusId) throws ParseException {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Integer statusIds;
		if(statusId.isEmpty()) {
			statusIds = 0;	
		}
		else {
			statusIds = Integer.parseInt(statusId);
		}
		List<BookingDetailReport> bookingDetails = null;
		try {
			String strBookingQuery=null;
			strBookingQuery="select pb.pmsProperty.propertyId as propertyId, pb.bookingId as bookingId, coalesce(sum(bd.roomCount),0) as roomCount, pb.arrivalDate as arrivalDate,pb.departureDate as departureDate,"
					+ "pg.firstName as firstName,sum(bd.advanceAmount) as advanceAmount,pg.lastName as lastName,coalesce(sum(bd.amount),0) as amount,pb.createdDate as createdDate,"
					+ "coalesce(sum(bd.tax),0) as taxAmount,pg.guestId as guestId, pg.city as city,pg.phone as mobileNumber,pg.landline as phoneNumber,pg.emailId as emailId, "
					+ " bd.propertyAccommodation.accommodationId as accommodationId,bd.adultCount as adultCount,bd.childCount as childCount,pb.pmsSource.sourceId as sourceId,"
					+ "coalesce(sum(bd.otaCommission),0) as otaCommission,coalesce(sum(bd.otaTax),0) as otaTax,"
					+ "pb.pmsStatus.statusId as statusId,coalesce(sum(bd.purchaseRate),0) as purchaseRate,pb.promotionAmount as promotionAmount,"
					+ "pb.couponAmount as couponAmount,sum(bd.extraAdultRate) as extraAdultRate,sum(bd.extraChildRate) as extraChildRate "
					+ "from PmsBooking pb,BookingDetail bd,BookingGuestDetail bgd,PmsGuest pg "
					+ " where pb.pmsProperty.propertyId=:propertyId "
					+ " and pb.bookingId=bd.pmsBooking.bookingId "
					+ " and pb.bookingId=bgd.pmsBooking.bookingId and bd.isActive='true' and bd.isDeleted='false' "
					+ " and pg.guestId=bgd.pmsGuest.guestId";
					if(statusIds==1) {
						strBookingQuery+=" and (bd.pmsStatus.statusId = 4 and  bd.pmsStatus.statusId = 1)";	
					}else if(statusIds==2) {
						strBookingQuery+=" and (bd.pmsStatus.statusId <> 4 and  bd.pmsStatus.statusId <> 1)";	
					}else if(statusIds==0){
						strBookingQuery+=" and (bd.pmsStatus.statusId <> 4 and  bd.pmsStatus.statusId <> 1)";
					}else {
						strBookingQuery+=" and (bd.pmsStatus.statusId = 1 or bd.pmsStatus.statusId = 2"
								+ " or bd.pmsStatus.statusId = 3 or bd.pmsStatus.statusId = 4"
								+ " or bd.pmsStatus.statusId = 5)";
					}
						strBookingQuery+=" and (pb.arrivalDate between :arrivalDate   and :departureDate"
								+ " or pb.departureDate between :arrivalDate   and :departureDate) ";
					
					strBookingQuery+= "group by bd.roomCount, pb.bookingId,pb.arrivalDate,pb.departureDate,pb.createdDate,"
							+ "pg.firstName,pg.lastName,pg.city,bd.purchaseRate,pb.promotionAmount,pb.couponAmount,"
							+ "pg.phone,pg.landline,pg.emailId,pg.guestId,bd.propertyAccommodation.accommodationId,"
							+ "bd.adultCount,bd.childCount,pb.pmsStatus.statusId,bd.extraAdultRate,bd.extraChildRate order by pb.bookingId DESC";
					bookingDetails = (List<BookingDetailReport>) session.createQuery(strBookingQuery)
								.setParameter("propertyId", propertyId)
								.setParameter("arrivalDate", start)
								.setParameter("departureDate", end)
								.setResultTransformer(Transformers.aliasToBean(BookingDetailReport.class)).list();
					
			
		} catch (HibernateException e) {
			logger.error(e);  
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
			
		}
		return bookingDetails;
	}

	
	@SuppressWarnings("unchecked")
	public List<BookingDetailReport> listGuestBookingDetail(Integer propertyId,Date start,Date end,int userId) throws ParseException {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<BookingDetailReport> bookingDetails = null;
		try {
			String strBookingQuery=null;
			strBookingQuery="select pb.pmsProperty.propertyId as propertyId, pb.bookingId as bookingId, coalesce(sum(bd.roomCount),0) as roomCount, pb.arrivalDate as arrivalDate,pb.departureDate as departureDate,"
					+ "pg.firstName as firstName,sum(bd.advanceAmount) as advanceAmount,pg.lastName as lastName,coalesce(sum(bd.amount),0) as amount,pb.createdDate as createdDate,"
					+ "coalesce(sum(bd.tax),0) as taxAmount,pg.guestId as guestId, pg.city as city,pg.phone as mobileNumber,pg.landline as phoneNumber,pg.emailId as emailId, "
					+ " bd.propertyAccommodation.accommodationId as accommodationId,bd.adultCount as adultCount,bd.childCount as childCount,pb.pmsSource.sourceId as sourceId,"
					+ "coalesce(sum(bd.otaCommission),0) as otaCommission,coalesce(sum(bd.otaTax),0) as otaTax,"
					+ "pb.pmsStatus.statusId as statusId,coalesce(sum(bd.purchaseRate),0) as purchaseRate,"
					+ "pb.promotionAmount as promotionAmount,pb.couponAmount as couponAmount,sum(bd.extraAdultRate) as extraAdultRate,sum(bd.extraChildRate) as extraChildRate "
					+ "from PmsBooking pb,BookingDetail bd,BookingGuestDetail bgd,PmsGuest pg "
					+ " where pb.pmsProperty.propertyId=:propertyId "
					+ " and pb.bookingId=bd.pmsBooking.bookingId and bd.createdBy =:userId"
					+ " and pb.bookingId=bgd.pmsBooking.bookingId and bd.isActive='true' and bd.isDeleted='false' "
					+ " and pg.guestId=bgd.pmsGuest.guestId and bd.pmsStatus.statusId <> 4 and  bd.pmsStatus.statusId <> 1";
					
					
						strBookingQuery+=" and (pb.arrivalDate between :arrivalDate   and :departureDate"
								+ " or pb.departureDate between :arrivalDate   and :departureDate) ";
					
					strBookingQuery+= "group by bd.roomCount, pb.bookingId,pb.arrivalDate,pb.departureDate,pb.createdDate,"
							+ "pg.firstName,pg.lastName,pg.city,bd.purchaseRate,pb.promotionAmount,pb.couponAmount,"
							+ "pg.phone,pg.landline,pg.emailId,pg.guestId,bd.propertyAccommodation.accommodationId,"
							+ "bd.adultCount,bd.childCount,pb.pmsStatus.statusId,bd.extraAdultRate,bd.extraChildRate order by pb.bookingId DESC";
					
					bookingDetails = (List<BookingDetailReport>) session.createQuery(strBookingQuery)
								.setParameter("propertyId", propertyId)
								.setParameter("arrivalDate", start)
								.setParameter("departureDate", end)
								.setParameter("userId", userId)
								.setResultTransformer(Transformers.aliasToBean(BookingDetailReport.class)).list();
					
			
		} catch (HibernateException e) {
			logger.error(e);  
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
			
		}
		return bookingDetails;
	}
	
	@SuppressWarnings("unchecked")
     

	public List<BookingDetailReport> listBookingDetail(int filterBookingId) throws ParseException {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<BookingDetailReport> bookingDetails = null;
		try {
			String strBookingQuery=null;
			strBookingQuery="select pb.pmsProperty.propertyId as propertyId,pb.bookingId as bookingId,"
					+ "bgd.specialRequest as specialRequest,pb.otaBookingId as otaBookingId, "
					+ "coalesce(sum(bd.roomCount),0) as roomCount, pb.arrivalDate as arrivalDate,pb.departureDate as departureDate,"
					+ "pg.firstName as firstName,pg.lastName as lastName,coalesce(sum(bd.amount),0) as amount,pb.createdDate as createdDate,"
					+ "coalesce(sum(bd.tax),0) as taxAmount,sum(bd.advanceAmount) as advanceAmount,pg.guestId as guestId, pg.city as city,pg.phone as mobileNumber,pg.landline as phoneNumber,pg.emailId as emailId, "
					+ " bd.propertyAccommodation.accommodationId as accommodationId,bd.adultCount as adultCount,pb.pmsSource.sourceId as sourceId,"
					+ "bd.childCount as childCount,pb.pmsStatus.statusId as statusId,coalesce(sum(bd.purchaseRate),0) as purchaseRate,"
					+ "pb.promotionAmount as promotionAmount,pb.couponAmount as couponAmount,pb.addonAmount as addonAmount,pb.addonDescription as addonDescription,"
					+ "sum(bd.extraAdultRate) as extraAdultRate,sum(bd.extraChildRate) as extraChildRate "
					+ "from PmsBooking pb,BookingDetail bd,BookingGuestDetail bgd,PmsGuest pg"
					+ " where pb.bookingId=bd.pmsBooking.bookingId "
					+ " and pb.bookingId=bgd.pmsBooking.bookingId "
					+ " and pg.guestId=bgd.pmsGuest.guestId and bd.isActive='true' and bd.isDeleted='false' ";
					
					if(filterBookingId>0){
						strBookingQuery+=" and pb.bookingId =:bookingId";
					}else{
						strBookingQuery+="";
					}
					
					strBookingQuery+= " group by bd.roomCount,bgd.specialRequest,pb.bookingId,pb.arrivalDate,pb.departureDate,pb.createdDate,"
							+ "pg.guestId,pg.firstName,pg.lastName,pg.city,bd.purchaseRate,pb.addonDescription,pb.addonAmount,"
							+ "pg.phone,pg.landline,pg.emailId,bd.propertyAccommodation.accommodationId,pb.promotionAmount ,pb.couponAmount, "
							+ "bd.advanceAmount,bd.adultCount,bd.childCount,pb.pmsStatus.statusId,bd.extraAdultRate,bd.extraChildRate order by pb.bookingId DESC";
					
			
					if(filterBookingId>0){
						bookingDetails = (List<BookingDetailReport>) session.createQuery(strBookingQuery)
								.setParameter("bookingId", filterBookingId)
								.setResultTransformer(Transformers.aliasToBean(BookingDetailReport.class)).list();
					}
			
		} catch (HibernateException e) {
			logger.error(e);  
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
			
		}
		return bookingDetails;
	}

	@SuppressWarnings("unchecked")
	public List<UserGuestDetailReport> UserDetails() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<UserGuestDetailReport> userDetails = null;
		try {
			userDetails = (List<UserGuestDetailReport>)session.createQuery("select distinct(emailId) as emailId, userName as userName,"
					+ "phone as phoneNumber,address1 as address1,address2 as address2"
					+ " from User where role.roleId = '4' group by userId")
					.setResultTransformer(Transformers.aliasToBean(UserGuestDetailReport.class)).list();
			

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return userDetails;
	}
	
	@SuppressWarnings("unchecked")
	public List<UserGuestDetailReport> UserDetails(Date start,Date end) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<UserGuestDetailReport> userDetails = null;
		try {
			userDetails = (List<UserGuestDetailReport>)session.createQuery("select distinct(emailId) as emailId, userName as userName,"
					+ "phone as phoneNumber,address1 as address1,address2 as address2 from User where role.roleId = '4' "
					+ " and( createdDate between :startDate and :endDate or modifiedDate between :startDate and :endDate) group by userId")
					.setParameter("startDate", start)
					.setParameter("endDate", end)
					.setResultTransformer(Transformers.aliasToBean(UserGuestDetailReport.class)).list();
			

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return userDetails;
	}
	
	@SuppressWarnings("unchecked")
	public List<UserGuestDetailReport> GuestDetails() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<UserGuestDetailReport> guestDetails = null;
		try {
			guestDetails = (List<UserGuestDetailReport>)session.createQuery("select distinct(emailId) as emailId, firstName as userName,"
					+ "phone as phoneNumber,address1 as address1,address2 as address2 "
					+ " from PmsGuest group by guestId ")
					.setResultTransformer(Transformers.aliasToBean(UserGuestDetailReport.class)).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return guestDetails;
	}
	
	@SuppressWarnings("unchecked")
	public List<UserGuestDetailReport> GuestDetails(Date start,Date end) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<UserGuestDetailReport> guestDetails = null;
		try {
			guestDetails = (List<UserGuestDetailReport>)session.createQuery("select distinct(emailId) as emailId, firstName as userName,"
					+ "phone as phoneNumber,address1 as address1,address2 as address2  from PmsGuest where "
					+ " (createdDate between :startDate and :endDate or modifiedDate between :startDate and :endDate) group by guestId ")
					.setParameter("startDate", start)
					.setParameter("endDate", end)
					.setResultTransformer(Transformers.aliasToBean(UserGuestDetailReport.class)).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return guestDetails;
	}
	
	@SuppressWarnings("unchecked")
	public List<BookingDetailReport> listCancellation(int propertyId,Date start,Date end,int accommodationId) throws ParseException {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<BookingDetailReport> bookingDetails = null;
		try {
			String strBookingQuery=null;
			strBookingQuery="select pb.bookingId as bookingId,bd.pmsStatus.statusId as statusId, coalesce(sum(bd.roomCount),0) as roomCount, pb.arrivalDate as arrivalDate,"
					+ "pb.departureDate as departureDate,pg.firstName as firstName,pg.lastName as lastName,coalesce(sum(bd.amount),0) as amount,pb.createdDate as createdDate,"
					+ "coalesce(sum(bd.tax),0) as taxAmount, pg.city as city,"
					+ "pg.phone as mobileNumber,pg.landline as phoneNumber,pg.emailId as emailId,"
					+ "bd.propertyAccommodation.accommodationId as accommodationId,coalesce(sum(bd.purchaseRate),0) as purchaseRate,"
					+ "pb.promotionAmount as promotionAmount,pb.couponAmount as couponAmount,sum(bd.extraAdultRate) as extraAdultRate,sum(bd.extraChildRate) as extraChildRate "
					+ "from PmsBooking pb,BookingDetail bd,BookingGuestDetail bgd,PmsGuest pg  "
					+ " where pb.pmsProperty.propertyId=:propertyId "
					+ " and pb.bookingId=bd.pmsBooking.bookingId "
					+ " and pb.bookingId=bgd.pmsBooking.bookingId "
					+ " and pg.guestId=bgd.pmsGuest.guestId and bd.pmsStatus.statusId = :statusId and bd.isActive='true' and bd.isDeleted='false' ";
					
					if(accommodationId>0){
						strBookingQuery+=" and bd.propertyAccommodation.accommodationId=:accommodationId "
								+ " and pb.arrivalDate>=:arrivalDate and pb.departureDate<=:departureDate ";
					}else{
						strBookingQuery+=" and pb.arrivalDate>=:arrivalDate and pb.departureDate<=:departureDate ";
					}
					strBookingQuery+= " group by pb.bookingId,bd.pmsStatus.statusId, bd.roomCount, pb.arrivalDate,pb.departureDate,"
					+ "pg.firstName,pg.lastName,bd.amount,bd.tax,pg.city,pg.phone,pg.landline,pg.emailId,"
					+ "bd.propertyAccommodation.accommodationId,pb.createdDate,bd.purchaseRate,pb.promotionAmount,pb.couponAmount,  "
					+ "bd.extraAdultRate,bd.extraChildRate order by pb.bookingId DESC";
					
							
					if(accommodationId>0){
						bookingDetails = (List<BookingDetailReport>) session.createQuery(strBookingQuery)
								.setParameter("propertyId", propertyId)
								.setParameter("arrivalDate", start)
								.setParameter("departureDate", end)
								.setParameter("statusId", 4)
								.setParameter("accommodationId", accommodationId)
								.setResultTransformer(Transformers.aliasToBean(BookingDetailReport.class)).list();
					}
					else{
						bookingDetails = (List<BookingDetailReport>) session.createQuery(strBookingQuery)
								.setParameter("propertyId", propertyId)
								.setParameter("arrivalDate", start)
								.setParameter("departureDate", end)
								.setParameter("statusId", 4)
								.setResultTransformer(Transformers.aliasToBean(BookingDetailReport.class)).list();
					}
			
		} catch (HibernateException e) {
			logger.error(e);  
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return bookingDetails;
	}
	
	@SuppressWarnings("unchecked")
	public List<BookingDetailReport> listCancellation(int propertyId,Date start,Date end,int accommodationId, String strFilterDateName,int bookingId) throws ParseException {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<BookingDetailReport> bookingDetails = null;
		try {
			String strBookingQuery=null;
			strBookingQuery="select pb.bookingId as bookingId,bd.pmsStatus.statusId as statusId, coalesce(sum(bd.roomCount),0) as roomCount, pb.arrivalDate as arrivalDate,"
					+ "pb.departureDate as departureDate,pg.firstName as firstName,pg.lastName as lastName,coalesce(sum(bd.amount),0) as amount,pb.createdDate as createdDate,"
					+ "coalesce(sum(bd.tax),0) as taxAmount, pg.city as city,pb.modifiedBy as modifiedBy,bd.modifiedDate as modifiedDate,"
					+ "pg.phone as mobileNumber,pg.landline as phoneNumber,pg.emailId as emailId,"
					+ "bd.propertyAccommodation.accommodationId as accommodationId,coalesce(sum(bd.purchaseRate),0) as purchaseRate,"
					+ "pb.promotionAmount as promotionAmount,pb.couponAmount as couponAmount,sum(bd.extraAdultRate) as extraAdultRate,sum(bd.extraChildRate) as extraChildRate "
					+ "from PmsBooking pb,BookingDetail bd,BookingGuestDetail bgd,PmsGuest pg  "
					+ " where pb.pmsProperty.propertyId=:propertyId "
					+ " and pb.bookingId=bd.pmsBooking.bookingId "
					+ " and pb.bookingId=bgd.pmsBooking.bookingId "
					+ " and pg.guestId=bgd.pmsGuest.guestId and bd.pmsStatus.statusId = :statusId and bd.isActive='true' and bd.isDeleted='false' ";
					
					if(accommodationId>0){
						strBookingQuery+=" and bd.propertyAccommodation.accommodationId=:accommodationId ";
						if(strFilterDateName.equalsIgnoreCase("BookingDate")){
							strBookingQuery+=" and pb.createdDate between :startDate and :endDate ";
						}else if(strFilterDateName.equalsIgnoreCase("CheckInDate")){
							strBookingQuery+=" and pb.arrivalDate between :startDate and :endDate ";
						}else if(strFilterDateName.equalsIgnoreCase("CheckOutDate")){
							strBookingQuery+=" and pb.departureDate between :startDate and :endDate ";
						}else if(strFilterDateName.equalsIgnoreCase("StayInfo")){
							strBookingQuery+=" and :startDate between pb.arrivalDate and pb.departureDate and :endDate between pb.arrivalDate and pb.departureDate ";
						}		
					}else{
						
						if(strFilterDateName.equalsIgnoreCase("BookingDate")){
							strBookingQuery+=" and pb.createdDate between :startDate and :endDate ";
						}else if(strFilterDateName.equalsIgnoreCase("CheckInDate")){
							strBookingQuery+=" and pb.arrivalDate between :startDate and :endDate ";
						}else if(strFilterDateName.equalsIgnoreCase("CheckOutDate")){
							strBookingQuery+=" and pb.departureDate between :startDate and :endDate ";
						}else if(strFilterDateName.equalsIgnoreCase("StayInfo")){
							strBookingQuery+=" and :startDate between pb.arrivalDate and pb.departureDate and :endDate between pb.arrivalDate and pb.departureDate ";
						}
					}
					strBookingQuery+= " group by pb.bookingId,bd.pmsStatus.statusId, bd.roomCount, pb.arrivalDate,pb.departureDate,bd.modifiedDate,"
					+ "pg.firstName,pg.lastName,bd.amount,bd.tax,pg.city,pg.phone,pg.landline,pg.emailId,bd.propertyAccommodation.accommodationId,  "
					+ "bd.purchaseRate,pb.promotionAmount,pb.couponAmount,bd.extraAdultRate,bd.extraChildRate ";
					
					if(strFilterDateName.equalsIgnoreCase("BookingDate")){
						strBookingQuery+=" order by pb.createdDate DESC";
					}else if(strFilterDateName.equalsIgnoreCase("CheckInDate")){
						strBookingQuery+=" order by pb.arrivalDate DESC";
					}else if(strFilterDateName.equalsIgnoreCase("CheckOutDate")){
						strBookingQuery+=" order by pb.departureDate DESC";
					}else if(strFilterDateName.equalsIgnoreCase("StayInfo")){
						strBookingQuery+=" order by pb.bookingId DESC";
					}
							
					if(accommodationId>0){
						bookingDetails = (List<BookingDetailReport>) session.createQuery(strBookingQuery)
								.setParameter("propertyId", propertyId)
								.setParameter("startDate", start)
								.setParameter("endDate", end)
								.setParameter("statusId", 4)
								.setParameter("accommodationId", accommodationId)
								.setResultTransformer(Transformers.aliasToBean(BookingDetailReport.class)).list();
					}
					else{
						bookingDetails = (List<BookingDetailReport>) session.createQuery(strBookingQuery)
								.setParameter("propertyId", propertyId)
								.setParameter("startDate", start)
								.setParameter("endDate", end)
								.setParameter("statusId", 4)
								.setResultTransformer(Transformers.aliasToBean(BookingDetailReport.class)).list();
					}
			
		} catch (HibernateException e) {
			logger.error(e);  
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return bookingDetails;
	}
	
	@SuppressWarnings("unchecked")
	public List<BookingDetailReport> listCancellation(int propertyId,int bookingId) throws ParseException {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<BookingDetailReport> bookingDetails = null;
		try {
			String strBookingQuery=null;
			strBookingQuery="select pb.bookingId as bookingId,bd.pmsStatus.statusId as statusId, coalesce(sum(bd.roomCount),0) as roomCount, pb.arrivalDate as arrivalDate,"
					+ "pb.departureDate as departureDate,pg.firstName as firstName,pg.lastName as lastName,coalesce(sum(bd.amount),0) as amount,pb.createdDate as createdDate,"
					+ "coalesce(sum(bd.tax),0) as taxAmount, pg.city as city,pb.modifiedBy as modifiedBy,bd.modifiedDate as modifiedDate,"
					+ "pg.phone as mobileNumber,pg.landline as phoneNumber,pg.emailId as emailId,"
					+ "bd.propertyAccommodation.accommodationId as accommodationId,coalesce(sum(bd.purchaseRate),0) as purchaseRate,"
					+ "pb.promotionAmount as promotionAmount,pb.couponAmount as couponAmount,sum(bd.extraAdultRate) as extraAdultRate,sum(bd.extraChildRate) as extraChildRate "
					+ "from PmsBooking pb,BookingDetail bd,BookingGuestDetail bgd,PmsGuest pg  "
					+ " where pb.pmsProperty.propertyId=:propertyId "
					+ " and pb.bookingId=bd.pmsBooking.bookingId "
					+ " and pb.bookingId=bgd.pmsBooking.bookingId "
					+ " and pg.guestId=bgd.pmsGuest.guestId and bd.pmsStatus.statusId = :statusId ";
					strBookingQuery+=" and pb.bookingId =:bookingId and bd.isActive='true' and bd.isDeleted='false' ";
					
					strBookingQuery+= " group by pb.bookingId,bd.pmsStatus.statusId, bd.roomCount, pb.arrivalDate,pb.departureDate,bd.modifiedDate,"
					+ "pg.firstName,pg.lastName,bd.amount,bd.tax,pg.city,pg.phone,pg.landline,pg.emailId,bd.propertyAccommodation.accommodationId,  "
					+ "bd.purchaseRate,pb.promotionAmount,pb.couponAmount,bd.extraAdultRate,bd.extraChildRate order by pb.bookingId DESC";
					
						
					bookingDetails = (List<BookingDetailReport>) session.createQuery(strBookingQuery)
							.setParameter("propertyId", propertyId)
							.setParameter("statusId", 4)
							.setParameter("bookingId", bookingId)
							.setResultTransformer(Transformers.aliasToBean(BookingDetailReport.class)).list();
					
			
		} catch (HibernateException e) {
			logger.error(e);  
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return bookingDetails;
	}
	
	@SuppressWarnings("unchecked")
	public List<BookingDetailReport> listResortBookingDetail(int propertyId,Date start,Date end,int accommodationId) throws ParseException {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<BookingDetailReport> bookingDetails = null;
		try {
			String strBookingQuery=null;
			strBookingQuery="select pb.bookingId as bookingId, coalesce(sum(bd.roomCount),0) as roomCount, pb.arrivalDate as arrivalDate,"
					+ "pb.departureDate as departureDate,pb.createdDate as createdDate,"
					+ "pg.firstName as firstName,pg.lastName as lastName,coalesce(sum(bd.amount),0) as amount,"
					+ "bd.adultCount as adultCount,bd.childCount as childCount,"
					+ "coalesce(sum(bd.tax),0) as taxAmount, pg.city as city,pg.phone as mobileNumber,"
					+ "pg.landline as phoneNumber,pg.emailId as emailId, "
					+ " bd.propertyAccommodation.accommodationId as accommodationId,coalesce(sum(bd.purchaseRate),0) as purchaseRate,"
					+ "pb.promotionAmount as promotionAmount,pb.couponAmount as couponAmount,sum(bd.extraAdultRate) as extraAdultRate,sum(bd.extraChildRate) as extraChildRate,"
					+ "pb.pmsStatus.statusId as statusId from PmsBooking pb,BookingDetail bd,BookingGuestDetail bgd,PmsGuest pg "
					+ " where pb.pmsProperty.propertyId=:propertyId "
					+ " and pb.bookingId=bd.pmsBooking.bookingId "
					+ " and pb.bookingId=bgd.pmsBooking.bookingId "
					+ " and pg.guestId=bgd.pmsGuest.guestId and bd.isActive='true' and bd.isDeleted='false' ";
					
					if(accommodationId>0){
						strBookingQuery+=" and bd.propertyAccommodation.accommodationId=:accommodationId "
								+ "and pb.arrivalDate>=:arrivalDate and pb.departureDate<=:departureDate and pb.pmsSource.resortIsActive='true'  ";
					}else{
						strBookingQuery+=" and pb.arrivalDate>=:arrivalDate and pb.departureDate<=:departureDate and pb.pmsSource.resortIsActive='true' ";
					}
					strBookingQuery+= "group by bd.roomCount, pb.bookingId,pb.arrivalDate,pb.departureDate,pb.createdDate,pg.firstName,pg.lastName,pg.city,"
							+ "pg.phone,pg.landline,pg.emailId,bd.propertyAccommodation.accommodationId,bd.adultCount,bd.childCount,"
							+ "pb.pmsStatus.statusId,bd.purchaseRate,pb.promotionAmount,pb.couponAmount,bd.extraAdultRate,bd.extraChildRate order by pb.bookingId DESC";
					
					if(accommodationId>0){
						bookingDetails = (List<BookingDetailReport>) session.createQuery(strBookingQuery)
								.setParameter("propertyId", propertyId)
								.setParameter("arrivalDate", start)
								.setParameter("departureDate", end)
								.setParameter("accommodationId", accommodationId)
								.setResultTransformer(Transformers.aliasToBean(BookingDetailReport.class)).list();
					}
					else{
						bookingDetails = (List<BookingDetailReport>) session.createQuery(strBookingQuery)
								.setParameter("propertyId", propertyId)
								.setParameter("arrivalDate", start)
								.setParameter("departureDate", end)
								.setResultTransformer(Transformers.aliasToBean(BookingDetailReport.class)).list();
					}
			
		} catch (HibernateException e) {
			logger.error(e);  
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
			
		}
		return bookingDetails;
	}
	
	@SuppressWarnings("unchecked")
	public List<BookingDetailReport> listResortBookingDetail(int propertyId,Date start,Date end,int accommodationId,String strFilterDateName,int filterBookingId) throws ParseException {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<BookingDetailReport> bookingDetails = null;
		try {
			String strBookingQuery=null;
			strBookingQuery="select pb.bookingId as bookingId, coalesce(sum(bd.roomCount),0) as roomCount, pb.arrivalDate as arrivalDate,"
					+ "pb.departureDate as departureDate,pb.createdDate as createdDate,"
					+ "pg.firstName as firstName,pg.lastName as lastName,coalesce(sum(bd.amount),0) as amount,"
					+ "bd.adultCount as adultCount,bd.childCount as childCount,"
					+ "coalesce(sum(bd.tax),0) as taxAmount, pg.city as city,pg.phone as mobileNumber,"
					+ "pg.landline as phoneNumber,pg.emailId as emailId, "
					+ " bd.propertyAccommodation.accommodationId as accommodationId,coalesce(sum(bd.actualAmount),0) as actualAmount,"
					+ "pb.pmsStatus.statusId as statusId,coalesce(sum(bd.purchaseRate),0) as purchaseRate,sum(bd.extraAdultRate) as extraAdultRate,sum(bd.extraChildRate) as extraChildRate,"
					+ "pb.promotionAmount as promotionAmount,pb.couponAmount as couponAmount from PmsBooking pb,BookingDetail bd,BookingGuestDetail bgd,PmsGuest pg "
					+ " where pb.pmsProperty.propertyId=:propertyId "
					+ " and pb.bookingId=bd.pmsBooking.bookingId "
					+ " and pb.bookingId=bgd.pmsBooking.bookingId "
					+ " and pg.guestId=bgd.pmsGuest.guestId and bd.isActive='true' and bd.isDeleted='false' ";
					
					if(accommodationId>0){
						strBookingQuery+=" and bd.propertyAccommodation.accommodationId=:accommodationId  and pb.pmsSource.resortIsActive='true' ";
						if(strFilterDateName.equalsIgnoreCase("BookingDate")){
							strBookingQuery+=" and pb.createdDate between :startDate and :endDate ";
						}else if(strFilterDateName.equalsIgnoreCase("CheckInDate")){
							strBookingQuery+=" and pb.arrivalDate between :startDate and :endDate ";
						}else if(strFilterDateName.equalsIgnoreCase("CheckOutDate")){
							strBookingQuery+=" and pb.departureDate between :startDate and :endDate ";
						}else if(strFilterDateName.equalsIgnoreCase("StayInfo")){
							strBookingQuery+=" and :startDate between pb.arrivalDate and pb.departureDate and :endDate between pb.arrivalDate and pb.departureDate ";
						}		
					}else{
						strBookingQuery+=" and pb.pmsSource.resortIsActive='true' ";
						if(strFilterDateName.equalsIgnoreCase("BookingDate")){
							strBookingQuery+=" and pb.createdDate between :startDate and :endDate ";
						}else if(strFilterDateName.equalsIgnoreCase("CheckInDate")){
							strBookingQuery+=" and pb.arrivalDate between :startDate and :endDate ";
						}else if(strFilterDateName.equalsIgnoreCase("CheckOutDate")){
							strBookingQuery+=" and pb.departureDate between :startDate and :endDate ";
						}else if(strFilterDateName.equalsIgnoreCase("StayInfo")){
							strBookingQuery+=" and :startDate between pb.arrivalDate and pb.departureDate and :endDate between pb.arrivalDate and pb.departureDate ";
						}		
					}
					strBookingQuery+= "group by bd.roomCount, pb.bookingId,pb.arrivalDate,pb.departureDate,pb.createdDate,pg.firstName,pg.lastName,pg.city,"
							+ "pg.phone,pg.landline,pg.emailId,bd.propertyAccommodation.accommodationId,bd.adultCount,bd.childCount,"
							+ "pb.pmsStatus.statusId,bd.purchaseRate,pb.promotionAmount,pb.couponAmount,bd.extraAdultRate,bd.extraChildRate ";
					
					if(strFilterDateName.equalsIgnoreCase("BookingDate")){
						strBookingQuery+=" order by pb.createdDate DESC";
					}else if(strFilterDateName.equalsIgnoreCase("CheckInDate")){
						strBookingQuery+=" order by pb.arrivalDate DESC";
					}else if(strFilterDateName.equalsIgnoreCase("CheckOutDate")){
						strBookingQuery+=" order by pb.departureDate DESC";
					}else if(strFilterDateName.equalsIgnoreCase("StayInfo")){
						strBookingQuery+=" order by pb.bookingId DESC";
					}
					
					if(accommodationId>0){
						bookingDetails = (List<BookingDetailReport>) session.createQuery(strBookingQuery)
								.setParameter("propertyId", propertyId)
								.setParameter("startDate", start)
								.setParameter("endDate", end)
								.setParameter("accommodationId", accommodationId)
								.setResultTransformer(Transformers.aliasToBean(BookingDetailReport.class)).list();
					}
					else{
						bookingDetails = (List<BookingDetailReport>) session.createQuery(strBookingQuery)
								.setParameter("propertyId", propertyId)
								.setParameter("startDate", start)
								.setParameter("endDate", end)
								.setResultTransformer(Transformers.aliasToBean(BookingDetailReport.class)).list();
					}
			
		} catch (HibernateException e) {
			logger.error(e);  
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
			
		}
		return bookingDetails;
	}
	
	@SuppressWarnings("unchecked")
	public List<BookingDetailReport> listResortBookingDetail(int propertyId,int filterBookingId) throws ParseException {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<BookingDetailReport> bookingDetails = null;
		try {
			String strBookingQuery=null;
			strBookingQuery="select pb.bookingId as bookingId, coalesce(sum(bd.roomCount),0) as roomCount, pb.arrivalDate as arrivalDate,"
					+ "pb.departureDate as departureDate,pb.createdDate as createdDate,"
					+ "pg.firstName as firstName,pg.lastName as lastName,coalesce(sum(bd.amount),0) as amount,"
					+ "bd.adultCount as adultCount,bd.childCount as childCount,"
					+ "coalesce(sum(bd.tax),0) as taxAmount, pg.city as city,pg.phone as mobileNumber,"
					+ "pg.landline as phoneNumber,pg.emailId as emailId, "
					+ " bd.propertyAccommodation.accommodationId as accommodationId,coalesce(sum(bd.actualAmount),0) as actualAmount,"
					+ "pb.pmsStatus.statusId as statusId,coalesce(sum(bd.purchaseRate),0) as purchaseRate,sum(bd.extraAdultRate) as extraAdultRate,sum(bd.extraChildRate) as extraChildRate,"
					+ "pb.promotionAmount as promotionAmount,pb.couponAmount as couponAmount from PmsBooking pb,BookingDetail bd,BookingGuestDetail bgd,PmsGuest pg "
					+ " where pb.pmsProperty.propertyId=:propertyId "
					+ " and pb.bookingId=bd.pmsBooking.bookingId "
					+ " and pb.bookingId=bgd.pmsBooking.bookingId "
					+ " and pg.guestId=bgd.pmsGuest.guestId and bd.isActive='true' and bd.isDeleted='false' ";
					
					strBookingQuery+=" and  pb.pmsSource.resortIsActive='true' and pb.bookingId =:bookingId ";
					
					strBookingQuery+= "group by bd.roomCount, pb.bookingId,pb.arrivalDate,pb.departureDate,pb.createdDate,pg.firstName,pg.lastName,pg.city,"
							+ "pg.phone,pg.landline,pg.emailId,bd.propertyAccommodation.accommodationId,bd.adultCount,bd.childCount,"
							+ "pb.pmsStatus.statusId,bd.purchaseRate,pb.promotionAmount,pb.couponAmount,bd.extraAdultRate,bd.extraChildRate order by pb.bookingId DESC";
					
					bookingDetails = (List<BookingDetailReport>) session.createQuery(strBookingQuery)
							.setParameter("propertyId", propertyId)
							.setParameter("bookingId", filterBookingId)
							.setResultTransformer(Transformers.aliasToBean(BookingDetailReport.class)).list();
			
		} catch (HibernateException e) {
			logger.error(e);  
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
			
		}
		return bookingDetails;
	}
	
	@SuppressWarnings("unchecked")
	public List<BookingDetailReport> listUloBookingDetail(int propertyId,Date start,Date end,int accommodationId) throws ParseException {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<BookingDetailReport> bookingDetails = null;
		try {
			String strBookingQuery=null;
			strBookingQuery="select pb.bookingId as bookingId, coalesce(sum(bd.roomCount),0) as roomCount, pb.arrivalDate as arrivalDate,pb.departureDate as departureDate,"
					+ "pg.firstName as firstName,pg.lastName as lastName,coalesce(sum(bd.amount),0) as amount,pb.createdDate as createdDate,"
					+ "coalesce(sum(bd.tax),0) as taxAmount, pg.city as city,pg.phone as mobileNumber,pg.landline as phoneNumber,pg.emailId as emailId, "
					+ " bd.propertyAccommodation.accommodationId as accommodationId,bd.adultCount as adultCount,bd.childCount as childCount,"
					+ " bd.pmsStatus.statusId as statusId,pb.pmsSource.sourceId as sourceId,"
					+ " coalesce(sum(bd.otaCommission),0) as otaCommission,coalesce(sum(bd.otaTax),0) as otaTax,coalesce(sum(bd.purchaseRate),0) as purchaseRate,"
					+ "pb.promotionAmount as promotionAmount,pb.couponAmount as couponAmount,sum(bd.extraAdultRate) as extraAdultRate,sum(bd.extraChildRate) as extraChildRate "
					+ "from PmsBooking pb,BookingDetail bd,BookingGuestDetail bgd,PmsGuest pg "
					+ " where pb.pmsProperty.propertyId=:propertyId "
					+ " and pb.bookingId=bd.pmsBooking.bookingId "
					+ " and pb.bookingId=bgd.pmsBooking.bookingId "
					+ " and pg.guestId=bgd.pmsGuest.guestId and bd.isActive='true' and bd.isDeleted='false' ";
					
					if(accommodationId>0){
						strBookingQuery+=" and bd.propertyAccommodation.accommodationId=:accommodationId "
								+ "and pb.arrivalDate>=:arrivalDate and pb.departureDate<=:departureDate ";
					}else{
						strBookingQuery+=" and pb.arrivalDate>=:arrivalDate and pb.departureDate<=:departureDate ";
					}
					strBookingQuery+= "group by bd.roomCount, pb.bookingId,pb.arrivalDate,pb.departureDate,pb.createdDate,pg.firstName,pg.lastName,pg.city,pb.addonAmount,pb.addonDescription,"
							+ "pg.phone,pg.landline,pg.emailId,bd.propertyAccommodation.accommodationId,bd.adultCount,bd.childCount,"
							+ "bd.pmsStatus.statusId,pb.pmsSource.sourceId,bd.purchaseRate,pb.promotionAmount,pb.couponAmount,"
							+ "bd.extraAdultRate,bd.extraChildRate order by pb.bookingId DESC";
					
					if(accommodationId>0){
						bookingDetails = (List<BookingDetailReport>) session.createQuery(strBookingQuery)
								.setParameter("propertyId", propertyId)
								.setParameter("arrivalDate", start)
								.setParameter("departureDate", end)
								.setParameter("accommodationId", accommodationId)
								.setResultTransformer(Transformers.aliasToBean(BookingDetailReport.class)).list();
					}
					else{
						bookingDetails = (List<BookingDetailReport>) session.createQuery(strBookingQuery)
								.setParameter("propertyId", propertyId)
								.setParameter("arrivalDate", start)
								.setParameter("departureDate", end)
								.setResultTransformer(Transformers.aliasToBean(BookingDetailReport.class)).list();
					}
			
		} catch (HibernateException e) {
			logger.error(e);  
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
			
		}
		return bookingDetails;
	}
	
	@SuppressWarnings("unchecked")
	public List<BookingDetailReport> listUloBookingDetail(int propertyId,Date start,Date end,int accommodationId,String strFilterDateName,int filterBookingId) throws ParseException {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<BookingDetailReport> bookingDetails = null;
		try {
			String strBookingQuery=null;
			strBookingQuery="select pb.bookingId as bookingId,pb.otaBookingId as otaBookingId, coalesce(sum(bd.roomCount),0) as roomCount, pb.arrivalDate as arrivalDate,pb.departureDate as departureDate,"
					+ "pg.firstName as firstName,pg.lastName as lastName,coalesce(sum(bd.amount),0) as amount,pb.createdDate as createdDate,pb.createdBy as createdBy,"
					+ "coalesce(sum(bd.tax),0) as taxAmount, pg.city as city,pg.phone as mobileNumber,pg.landline as phoneNumber,pg.emailId as emailId, "
					+ " bd.propertyAccommodation.accommodationId as accommodationId,bd.adultCount as adultCount,bd.childCount as childCount,"
					+ " bd.pmsStatus.statusId as statusId,pb.pmsSource.sourceId as sourceId,coalesce(sum(bd.advanceAmount),0) as advanceAmount,"
					+ " coalesce(sum(bd.otaCommission),0) as otaCommission,coalesce(sum(bd.otaTax),0) as otaTax,"
					+ "pb.addonAmount as addonAmount,pb.addonDescription as addonDescription,coalesce(sum(bd.purchaseRate),0) as purchaseRate,"
					+ "pb.promotionAmount as promotionAmount,pb.couponAmount as couponAmount,sum(bd.extraAdultRate) as extraAdultRate,sum(bd.extraChildRate) as extraChildRate  "
					+ "from PmsBooking pb,BookingDetail bd,BookingGuestDetail bgd,PmsGuest pg "
					+ " where pb.pmsProperty.propertyId=:propertyId "
					+ " and pb.bookingId=bd.pmsBooking.bookingId "
					+ " and pb.bookingId=bgd.pmsBooking.bookingId "
					+ " and pg.guestId=bgd.pmsGuest.guestId and bd.isActive='true' and bd.isDeleted='false' ";
					
					if(accommodationId>0){
						strBookingQuery+=" and bd.propertyAccommodation.accommodationId=:accommodationId ";
						if(strFilterDateName.equalsIgnoreCase("BookingDate")){
							strBookingQuery+=" and pb.createdDate between :startDate and :endDate ";
						}else if(strFilterDateName.equalsIgnoreCase("CheckInDate")){
							strBookingQuery+=" and pb.arrivalDate between :startDate and :endDate ";
						}else if(strFilterDateName.equalsIgnoreCase("CheckOutDate")){
							strBookingQuery+=" and pb.departureDate between :startDate and :endDate ";
						}else if(strFilterDateName.equalsIgnoreCase("StayInfo")){
							strBookingQuery+=" and :startDate between pb.arrivalDate and pb.departureDate and :endDate between pb.arrivalDate and pb.departureDate ";
						}
						
					}else{
						if(strFilterDateName.equalsIgnoreCase("BookingDate")){
							strBookingQuery+=" and pb.createdDate between :startDate and :endDate ";
						}else if(strFilterDateName.equalsIgnoreCase("CheckInDate")){
							strBookingQuery+=" and pb.arrivalDate between :startDate and :endDate ";
						}else if(strFilterDateName.equalsIgnoreCase("CheckOutDate")){
							strBookingQuery+=" and pb.departureDate between :startDate and :endDate ";
						}else if(strFilterDateName.equalsIgnoreCase("StayInfo")){
							strBookingQuery+=" and :startDate between pb.arrivalDate and pb.departureDate and :endDate between pb.arrivalDate and pb.departureDate ";
						}
					}
					strBookingQuery+= "group by bd.roomCount,pb.otaBookingId, pb.bookingId,pb.arrivalDate,pb.departureDate,pb.createdDate,pg.firstName,pg.lastName,pg.city,pb.addonAmount,pb.addonDescription,"
							+ "pg.phone,pg.landline,pg.emailId,bd.propertyAccommodation.accommodationId,bd.adultCount,bd.childCount,"
							+ "bd.pmsStatus.statusId,pb.pmsSource.sourceId,bd.purchaseRate,pb.promotionAmount,pb.couponAmount,bd.extraAdultRate,bd.extraChildRate ";
					
					if(strFilterDateName.equalsIgnoreCase("BookingDate")){
						strBookingQuery+=" order by pb.createdDate DESC";
					}else if(strFilterDateName.equalsIgnoreCase("CheckInDate")){
						strBookingQuery+=" order by pb.arrivalDate DESC";
					}else if(strFilterDateName.equalsIgnoreCase("CheckOutDate")){
						strBookingQuery+=" order by pb.departureDate DESC";
					}else if(strFilterDateName.equalsIgnoreCase("StayInfo")){
						strBookingQuery+=" order by pb.bookingId DESC";
					}
					
					if(accommodationId>0){
						bookingDetails = (List<BookingDetailReport>) session.createQuery(strBookingQuery)
								.setParameter("propertyId", propertyId)
								.setParameter("startDate", start)
								.setParameter("endDate", end)
								.setParameter("accommodationId", accommodationId)
								.setResultTransformer(Transformers.aliasToBean(BookingDetailReport.class)).list();
					}
					else{
						bookingDetails = (List<BookingDetailReport>) session.createQuery(strBookingQuery)
								.setParameter("propertyId", propertyId)
								.setParameter("startDate", start)
								.setParameter("endDate", end)
								.setResultTransformer(Transformers.aliasToBean(BookingDetailReport.class)).list();
					}
			
		} catch (HibernateException e) {
			logger.error(e);  
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
			
		}
		return bookingDetails;
	}
	
	@SuppressWarnings("unchecked")
	public List<BookingDetailReport> listUloBookingDetail(int propertyId,int filterBookingId) throws ParseException {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<BookingDetailReport> bookingDetails = null;
		try {
			String strBookingQuery=null;
			strBookingQuery="select pb.bookingId as bookingId,pb.otaBookingId as otaBookingId, coalesce(sum(bd.roomCount),0) as roomCount, pb.arrivalDate as arrivalDate,pb.departureDate as departureDate,"
					+ "pg.firstName as firstName,pg.lastName as lastName,coalesce(sum(bd.amount),0) as amount,pb.createdDate as createdDate,pb.createdBy as createdBy,"
					+ "coalesce(sum(bd.tax),0) as taxAmount, pg.city as city,pg.phone as mobileNumber,pg.landline as phoneNumber,pg.emailId as emailId, "
					+ " bd.propertyAccommodation.accommodationId as accommodationId,bd.adultCount as adultCount,bd.childCount as childCount,"
					+ " bd.pmsStatus.statusId as statusId,pb.pmsSource.sourceId as sourceId,coalesce(sum(bd.advanceAmount),0) as advanceAmount,"
					+ " coalesce(sum(bd.otaCommission),0) as otaCommission,coalesce(sum(bd.otaTax),0) as otaTax,pb.addonAmount as addonAmount,"
					+ "pb.addonDescription as addonDescription,coalesce(sum(bd.purchaseRate),0) as purchaseRate,"
					+ "pb.promotionAmount as promotionAmount,pb.couponAmount as couponAmount,sum(bd.extraAdultRate) as extraAdultRate,sum(bd.extraChildRate) as extraChildRate   "
					+ "from PmsBooking pb,BookingDetail bd,BookingGuestDetail bgd,PmsGuest pg "
					+ " where pb.pmsProperty.propertyId=:propertyId "
					+ " and pb.bookingId=bd.pmsBooking.bookingId "
					+ " and pb.bookingId=bgd.pmsBooking.bookingId "
					+ " and pg.guestId=bgd.pmsGuest.guestId and bd.isActive='true' and bd.isDeleted='false' ";
					
					strBookingQuery+=" and pb.bookingId =:bookingId ";
			
					strBookingQuery+= "group by bd.roomCount,pb.otaBookingId, pb.bookingId,pb.arrivalDate,pb.departureDate,pb.createdDate,pg.firstName,pg.lastName,pg.city,pb.addonAmount,pb.addonDescription,"
							+ "pg.phone,pg.landline,pg.emailId,bd.propertyAccommodation.accommodationId,bd.adultCount,bd.childCount,"
							+ "bd.pmsStatus.statusId,pb.pmsSource.sourceId,bd.purchaseRate,pb.promotionAmount,pb.couponAmount,bd.extraAdultRate,bd.extraChildRate order by pb.bookingId DESC";
					
					bookingDetails = (List<BookingDetailReport>) session.createQuery(strBookingQuery)
							.setParameter("propertyId", propertyId)
							.setParameter("bookingId", filterBookingId)
							.setResultTransformer(Transformers.aliasToBean(BookingDetailReport.class)).list();
			
		} catch (HibernateException e) {
			logger.error(e);  
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
			
		}
		return bookingDetails;
	}
	
	@SuppressWarnings("unchecked")
	public List<BookingDetailReport> listBookingDetail(int propertyId,int filterBookingId, int accommId) throws ParseException {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<BookingDetailReport> bookingDetails = null;
		try {
			String strBookingQuery=null;
			strBookingQuery="select pb.bookingId as bookingId, coalesce(sum(bd.roomCount),0) as roomCount, pb.arrivalDate as arrivalDate,pb.departureDate as departureDate,"
					+ "pg.firstName as firstName,pg.lastName as lastName,coalesce(sum(bd.amount),0) as amount,pb.createdDate as createdDate,"
					+ "coalesce(sum(bd.tax),0) as taxAmount, pg.city as city,pg.phone as mobileNumber,pg.landline as phoneNumber,pg.emailId as emailId, "
					+ " bd.propertyAccommodation.accommodationId as accommodationId,bd.adultCount as adultCount,bd.childCount as childCount,"
					+ " bd.pmsStatus.statusId as statusId,coalesce(sum(bd.otaCommission),0) as otaCommission,coalesce(sum(bd.otaTax),0) as otaTax,"
					+ " pb.pmsSource.sourceId as sourceId,coalesce(sum(bd.advanceAmount),0) as advanceAmount,"
					+ "bgd.specialRequest as specialRequest,coalesce(sum(bd.purchaseRate),0) as purchaseRate,"
					+ "pb.promotionAmount as promotionAmount,pb.couponAmount as couponAmount,sum(bd.extraAdultRate) as extraAdultRate,sum(bd.extraChildRate) as extraChildRate "
					+ "from PmsBooking pb,BookingDetail bd,BookingGuestDetail bgd,PmsGuest pg "
					+ " where pb.pmsProperty.propertyId=:propertyId "
					+ " and pb.bookingId=bd.pmsBooking.bookingId "
					+ " and pb.bookingId=bgd.pmsBooking.bookingId "
					+ " and pg.guestId=bgd.pmsGuest.guestId ";
					
					strBookingQuery+=" and pb.bookingId =:bookingId and bd.propertyAccommodation.accommodationId=:accommId ";
			
					strBookingQuery+= "group by bd.roomCount, pb.bookingId,pb.arrivalDate,pb.departureDate,pb.createdDate,pg.firstName,pg.lastName,pg.city,"
							+ "pg.phone,pg.landline,pg.emailId,bd.propertyAccommodation.accommodationId,bd.adultCount,bd.childCount,"
							+ "bd.pmsStatus.statusId,bgd.specialRequest,bd.purchaseRate,pb.promotionAmount,pb.couponAmount,bd.extraAdultRate,bd.extraChildRate order by pb.bookingId DESC";
					
					bookingDetails = (List<BookingDetailReport>) session.createQuery(strBookingQuery)
							.setParameter("propertyId", propertyId)
							.setParameter("bookingId", filterBookingId)
							.setParameter("accommId",accommId)
							.setResultTransformer(Transformers.aliasToBean(BookingDetailReport.class)).list();
					
			
		} catch (HibernateException e) {
			logger.error(e);  
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
			
		}
		return bookingDetails;
	}
	
	@SuppressWarnings("unchecked")
	public List<BookingDetailReport> listRevenueBookingDetail(int propertyId,Date start,Date end,int accommodationId) throws ParseException {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<BookingDetailReport> bookingDetails = null;
		try {
			String strBookingQuery=null;
			strBookingQuery="select pb.bookingId as bookingId,pb.otaBookingId as otaBookingId, coalesce(sum(bd.roomCount),0) as roomCount, pb.arrivalDate as arrivalDate,pb.departureDate as departureDate,"
					+ "pg.firstName as firstName,pg.lastName as lastName,coalesce(sum(bd.amount),0) as amount,pb.createdDate as createdDate,pb.createdBy as createdBy,"
					+ "coalesce(sum(bd.tax),0) as taxAmount, pg.city as city,pg.phone as mobileNumber,pg.landline as phoneNumber,pg.emailId as emailId, "
					+ " bd.propertyAccommodation.accommodationId as accommodationId,bd.adultCount as adultCount,bd.childCount as childCount,"
					+ " bd.pmsStatus.statusId as statusId,pb.pmsSource.sourceId as sourceId,coalesce(sum(bd.advanceAmount),0) as advanceAmount,"
					+ " coalesce(sum(bd.otaCommission),0) as otaCommission,coalesce(sum(bd.otaTax),0) as otaTax,"
					+ "pb.addonAmount as addonAmount,pb.addonDescription as addonDescription,coalesce(sum(bd.purchaseRate),0) as purchaseRate,"
					+ "pb.promotionAmount as promotionAmount,pb.couponAmount as couponAmount,sum(bd.extraAdultRate) as extraAdultRate,sum(bd.extraChildRate) as extraChildRate  "
					+ "from PmsBooking pb,BookingDetail bd,BookingGuestDetail bgd,PmsGuest pg "
					+ " where pb.pmsProperty.propertyId=:propertyId "
					+ " and pb.bookingId=bd.pmsBooking.bookingId "
					+ " and pb.bookingId=bgd.pmsBooking.bookingId "
					+ " and pg.guestId=bgd.pmsGuest.guestId and bd.isActive='true' and bd.isDeleted='false' ";
					
					if(accommodationId>0){
						strBookingQuery+=" and bd.propertyAccommodation.accommodationId=:accommodationId "
								+ "and pb.arrivalDate >= :startDate and pb.arrivalDate<=:endDate ";
						
						
					}else{
						strBookingQuery+=" and pb.arrivalDate >= :startDate and pb.arrivalDate<=:endDate ";
						
					}
					strBookingQuery+= "group by bd.roomCount,pb.otaBookingId, pb.bookingId,pb.arrivalDate,pb.departureDate,pb.createdDate,pg.firstName,pg.lastName,pg.city,pb.addonAmount,pb.addonDescription,"
							+ "pg.phone,pg.landline,pg.emailId,bd.propertyAccommodation.accommodationId,bd.adultCount,bd.childCount,"
							+ "bd.pmsStatus.statusId,pb.pmsSource.sourceId,bd.purchaseRate,pb.promotionAmount,pb.couponAmount,bd.extraAdultRate,bd.extraChildRate ";
					
					
					
					if(accommodationId>0){
						bookingDetails = (List<BookingDetailReport>) session.createQuery(strBookingQuery)
								.setParameter("propertyId", propertyId)
								.setParameter("startDate", start)
								.setParameter("endDate", end)
								.setParameter("accommodationId", accommodationId)
								.setResultTransformer(Transformers.aliasToBean(BookingDetailReport.class)).list();
					}
					else{
						bookingDetails = (List<BookingDetailReport>) session.createQuery(strBookingQuery)
								.setParameter("propertyId", propertyId)
								.setParameter("startDate", start)
								.setParameter("endDate", end)
								.setResultTransformer(Transformers.aliasToBean(BookingDetailReport.class)).list();
					}
			
		} catch (HibernateException e) {
			logger.error(e);  
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
			
		}
		return bookingDetails;
	}
	
	@SuppressWarnings("unchecked")
	public List<BookingDetailReport> getInventoryRates(int propertyId,Date start,int accommodationId) throws ParseException{

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<BookingDetailReport> bookingDetails = null;
		try {
			String strBookingQuery=null;
			strBookingQuery="select pb.bookingId as bookingId,pb.otaBookingId as otaBookingId, coalesce(sum(bd.roomCount),0) as roomCount, pb.arrivalDate as arrivalDate,pb.departureDate as departureDate,"
					+ "pg.firstName as firstName,pg.lastName as lastName,coalesce(sum(bd.amount),0) as amount,pb.createdDate as createdDate,pb.createdBy as createdBy,"
					+ "coalesce(sum(bd.tax),0) as taxAmount, pg.city as city,pg.phone as mobileNumber,pg.landline as phoneNumber,pg.emailId as emailId, "
					+ " bd.propertyAccommodation.accommodationId as accommodationId,bd.adultCount as adultCount,bd.childCount as childCount,"
					+ " bd.pmsStatus.statusId as statusId,pb.pmsSource.sourceId as sourceId,coalesce(sum(bd.advanceAmount),0) as advanceAmount,"
					+ " coalesce(sum(bd.otaCommission),0) as otaCommission,coalesce(sum(bd.otaTax),0) as otaTax,pb.addonAmount as addonAmount,"
					+ "pb.addonDescription as addonDescription,coalesce(sum(bd.purchaseRate),0) as purchaseRate,"
					+ "pb.promotionAmount as promotionAmount,pb.couponAmount as couponAmount,sum(bd.extraAdultRate) as extraAdultRate,sum(bd.extraChildRate) as extraChildRate  "
					+ "from PmsBooking pb,BookingDetail bd,BookingGuestDetail bgd,PmsGuest pg "
					+ " where pb.pmsProperty.propertyId=:propertyId "
					+ " and pb.bookingId=bd.pmsBooking.bookingId "
					+ " and pb.bookingId=bgd.pmsBooking.bookingId "
					+ " and pg.guestId=bgd.pmsGuest.guestId and bd.isActive='true' and bd.isDeleted='false' ";
					
					strBookingQuery+=" and bd.propertyAccommodation.accommodationId=:accommodationId and bd.pmsStatus.statusId <> 1 "
							+ " and bd.pmsStatus.statusId <> 4"
							+ " and bd.bookingDate = :startDate ";
					strBookingQuery+= "group by bd.roomCount,pb.otaBookingId, pb.bookingId,pb.arrivalDate,pb.departureDate,pb.createdDate,pg.firstName,pg.lastName,pg.city,pb.addonAmount,pb.addonDescription,"
							+ "pg.phone,pg.landline,pg.emailId,bd.propertyAccommodation.accommodationId,bd.adultCount,bd.childCount,"
							+ "bd.pmsStatus.statusId,pb.pmsSource.sourceId,bd.purchaseRate,pb.promotionAmount,pb.couponAmount,bd.extraAdultRate,bd.extraChildRate "
							+ "order by pb.arrivalDate DESC";
					
					bookingDetails = (List<BookingDetailReport>) session.createQuery(strBookingQuery)
							.setParameter("propertyId", propertyId)
							.setParameter("startDate", start)
							.setParameter("accommodationId", accommodationId)
							.setResultTransformer(Transformers.aliasToBean(BookingDetailReport.class)).list();
					
					
			
		} catch (HibernateException e) {
			logger.error(e);  
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
			
		}
		return bookingDetails;
	
	}
	
	@SuppressWarnings("unchecked")
	public List<UserGuestDetailReport> subscriberUserDetails(java.util.Date dateStart,java.util.Date dateEnd) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<UserGuestDetailReport> userDetails = null;
		try {
			userDetails = (List<UserGuestDetailReport>)session.createQuery("select distinct(emailId) as emailId, createdDate as createdDate"
					+ " from User where role.roleId = '15' and createdDate between :startDate and :endDate group by userId")
					.setParameter("startDate", dateStart)
					.setParameter("endDate", dateEnd)
					.setResultTransformer(Transformers.aliasToBean(UserGuestDetailReport.class)).list();
			

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return userDetails;
	}
}
