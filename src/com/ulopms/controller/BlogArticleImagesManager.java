package com.ulopms.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;

import com.ulopms.model.BlogArticleImages;
import com.ulopms.util.HibernateUtil;


public class BlogArticleImagesManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(BlogArticleImagesManager.class);

	public BlogArticleImages add(BlogArticleImages blogArticleImages) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(blogArticleImages);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return blogArticleImages;
	}

	public BlogArticleImages edit(BlogArticleImages blogArticleImages) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(blogArticleImages);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return blogArticleImages;
	}

	public BlogArticleImages delete(int id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		BlogArticleImages blogArticleImages = (BlogArticleImages) session.load(BlogArticleImages.class, id);
		if (null != blogArticleImages) {
			session.delete(blogArticleImages);
		}
		session.getTransaction().commit();
		return blogArticleImages;
	}

	public BlogArticleImages find(int id) {
		BlogArticleImages blogArticleImages = new BlogArticleImages();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			blogArticleImages = (BlogArticleImages) session.load(BlogArticleImages.class, id);
			// session.getTransaction().commit();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return blogArticleImages;
	}
	
/*	@SuppressWarnings("unchecked")
	public List<PromotionImages> findId(int promotionImageId) {
		List<PromotionImages> promotionImages = null;
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			//bookingDetails = (List<BookingDetail>) session.createQuery("from BookingDetail").list();
			promotionImages = (List<PromotionImages>)session.createQuery("from PromotionImages where promotionImageId =:promotionImageId")
				    .setParameter("promotionImageId",promotionImageId)
				    .list();
			// session.getTransaction().commit();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return promotionImages;
	}*/

	@SuppressWarnings("unchecked")
	public List<BlogArticleImages> list(int blogArticleId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<BlogArticleImages> blogArticleImages = null;
		try {

			blogArticleImages = (List<BlogArticleImages>) session.createQuery("from BlogArticleImages ba where blogArticles.blogArticleId=:blogArticleId and isActive = true and isDeleted =false ")
					 .setParameter("blogArticleId",blogArticleId)
					 .list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return blogArticleImages;
	}	
	

	
}
