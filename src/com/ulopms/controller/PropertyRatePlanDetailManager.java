package com.ulopms.controller;


import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;

import com.ulopms.model.PropertyRatePlan;
import com.ulopms.model.PmsSmartPrice;
import com.ulopms.model.PropertyRate;
import com.ulopms.model.PropertyRatePlanDetail;
import com.ulopms.util.HibernateUtil;


public class PropertyRatePlanDetailManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(PropertyRatePlanDetailManager.class);

	public PropertyRatePlanDetail add(PropertyRatePlanDetail ratePlanDetails) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(ratePlanDetails);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return ratePlanDetails;
	}

	public PropertyRatePlanDetail edit(PropertyRatePlanDetail ratePlanDetails) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(ratePlanDetails);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return ratePlanDetails;
	}

	public PropertyRatePlanDetail delete(Long id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		PropertyRatePlanDetail ratePlanDetails = (PropertyRatePlanDetail) session.load(PropertyRatePlanDetail.class, id);
		if (null != ratePlanDetails) {
			session.delete(ratePlanDetails);
		}
		session.getTransaction().commit();
		return ratePlanDetails;
	}

	public PropertyRatePlanDetail find(int id) {
		PropertyRatePlanDetail ratePlanDetails = new PropertyRatePlanDetail();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			ratePlanDetails = (PropertyRatePlanDetail) session.load(PropertyRatePlanDetail.class, id);
			// session.getTransaction().commit();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return ratePlanDetails;
	}

	@SuppressWarnings("unchecked")
	public List<PropertyRatePlanDetail> list() {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyRatePlanDetail> ratePlanDetails = null;
		try {
              
			ratePlanDetails = (List<PropertyRatePlanDetail>) session.createQuery("from PropertyRatePlanDetail where "
					+ " isActive = 'true' and isDeleted = 'false'").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return ratePlanDetails;
	}
	
	public PropertyRatePlanDetail listRateDetails(int propertyId,int accommId,int ratePlanId) {
		PropertyRatePlanDetail rateplan = new PropertyRatePlanDetail();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			rateplan = (PropertyRatePlanDetail)session.createQuery("from PropertyRatePlanDetail where "
					+ " isActive = 'true' and isDeleted = 'false' and ratePlan.ratePlanId=:ratePlanId and pmsProperty.propertyId=:propertyId and "
					+ "propertyAccommodation.accommodationId=:accommodationId")
					.setParameter("propertyId", propertyId)
					.setParameter("accommodationId", accommId)
					.setParameter("ratePlanId", ratePlanId)
			         .uniqueResult();
			
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return rateplan;
	}
	
	@SuppressWarnings("unchecked")
	public List<PropertyRatePlanDetail> list(int propertyId,int accommId,int ratePlanId) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyRatePlanDetail> ratePlanDetails = null;
		try {
              
			ratePlanDetails = (List<PropertyRatePlanDetail>) session.createQuery("from PropertyRatePlanDetail where "
					+ " isActive = 'true' and isDeleted = 'false' and ratePlan.ratePlanId=:ratePlanId and pmsProperty.propertyId=:propertyId and "
					+ "propertyAccommodation.accommodationId=:accommodationId")
					.setParameter("propertyId", propertyId)
					.setParameter("accommodationId", accommId)
					.setParameter("ratePlanId", ratePlanId).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return ratePlanDetails;
	}
	
	@SuppressWarnings("unchecked")
	public List<PropertyRatePlanDetail> list(int propertyId,int accommId) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyRatePlanDetail> ratePlanDetails = null;
		try {
              
			ratePlanDetails = (List<PropertyRatePlanDetail>) session.createQuery("from PropertyRatePlanDetail where "
					+ " isActive = 'true' and isDeleted = 'false' and pmsProperty.propertyId=:propertyId and "
					+ "propertyAccommodation.accommodationId=:accommodationId")
					.setParameter("propertyId", propertyId)
					.setParameter("accommodationId", accommId).list();

		} catch (HibernateException e) {
			logger.error(e);
			e.printStackTrace();
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return ratePlanDetails;
	}
	
	@SuppressWarnings("unchecked")
	public List<PropertyRatePlanDetail> listAccommodation(int propertyId,int accommId) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyRatePlanDetail> ratePlanDetails = null;
		try {
              
			ratePlanDetails = (List<PropertyRatePlanDetail>) session.createQuery("from PropertyRatePlanDetail where "
					+ " isActive = 'true' and isDeleted = 'false' and planIsActive='true' and pmsProperty.propertyId=:propertyId and "
					+ "propertyAccommodation.accommodationId=:accommodationId")
					.setParameter("propertyId", propertyId)
					.setParameter("accommodationId", accommId).list();

		} catch (HibernateException e) {
			logger.error(e);
			e.printStackTrace();
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return ratePlanDetails;
	}
}
