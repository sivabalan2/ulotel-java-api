package com.ulopms.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;

import com.ulopms.model.PmsAmenity;
import com.ulopms.model.PropertyAmenity;
import com.ulopms.model.PropertyAreas;
import com.ulopms.util.HibernateUtil;


public class PropertyAreasManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(PropertyAreasManager.class);

	public PropertyAreas add(PropertyAreas propertyAreas) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(propertyAreas);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyAreas;
	}

	public PropertyAreas edit(PropertyAreas propertyAreas) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(propertyAreas);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyAreas;
	}

	public PropertyAreas delete(Long id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		PropertyAreas propertyAreas = (PropertyAreas) session.load(PropertyAreas.class, id);
		if (null != propertyAreas) {
			session.delete(propertyAreas);
		}
		session.getTransaction().commit();
		return propertyAreas;
	}

	public PropertyAreas find(Integer id) {
		PropertyAreas propertyAreas = new PropertyAreas();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			propertyAreas = (PropertyAreas) session.load(PropertyAreas.class, id);
			// session.getTransaction().commit();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return propertyAreas;
	}

	@SuppressWarnings("unchecked")
	public List<PropertyAreas> list() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyAreas> propertyAreas = null;
		try {

			propertyAreas = (List<PropertyAreas>) session.createQuery("from PropertyAreas").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyAreas;
	}
	
	@SuppressWarnings("unchecked")
	public List<PropertyAreas> list(int propertyId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyAreas> propertyAreas = null;
		try {

			propertyAreas = (List<PropertyAreas>) session.createQuery("from PropertyAreas "
					+ "where pmsProperty.propertyId=:propertyId and isDeleted = 'false' "
					+ "and isActive = 'true'").setParameter("propertyId", propertyId).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyAreas;
	}
	
	@SuppressWarnings("unchecked")
	public List<PropertyAreas> list(int propertyId,int areaId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyAreas> propertyAreas = null;
		try {

			propertyAreas = (List<PropertyAreas>) session.createQuery("from PropertyAreas "
					+ "where pmsProperty.propertyId=:propertyId and area.areaId=:areaId and isDeleted = 'false' "
					+ "and isActive = 'true'").setParameter("propertyId", propertyId)
					.setParameter("areaId", areaId).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyAreas;
	}
	
	@SuppressWarnings("unchecked")
	public List<PropertyAreas> listPropertyByArea(int areaId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyAreas> propertyAreas = null;
		try {

			propertyAreas = (List<PropertyAreas>) session.createQuery("from PropertyAreas "
					+ "where area.areaId=:areaId and isDeleted = 'false' "
					+ "and isActive = 'true' and propertyIsControl='true' ")
					.setParameter("areaId", areaId).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyAreas;
	}
	
	@SuppressWarnings("unchecked")
	public List<PropertyAreas> listPropertyByArea(int areaId,int limit) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyAreas> propertyAreas = null;
		try {

			 propertyAreas = (List<PropertyAreas>) session.createQuery("from PropertyAreas pa "
						+ "where area.areaId=:areaId and (pa.pmsProperty.propertyId = pmsProperty.propertyId"
						+ " and pmsProperty.hotelIsActive = 'true' and pmsProperty.isActive = 'true')"
						+ " and isDeleted = 'false' "
						+ "and isActive = 'true' and propertyIsControl='true' ")
						.setParameter("areaId", areaId).setMaxResults(limit).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit(); 
			session = null;
		}
		return propertyAreas;
	}
}
