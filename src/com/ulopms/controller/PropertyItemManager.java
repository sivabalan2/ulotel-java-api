package com.ulopms.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;

import com.ulopms.model.PropertyItem;
import com.ulopms.util.HibernateUtil;


public class PropertyItemManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(PropertyItemManager.class);

	public PropertyItem add(PropertyItem propertyItem) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(propertyItem);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyItem;
	}

	public PropertyItem edit(PropertyItem propertyItem) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(propertyItem);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyItem;
	}

	public PropertyItem delete(Long id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		PropertyItem propertyItem = (PropertyItem) session.load(PropertyItem.class, id);
		if (null != propertyItem) {
			session.delete(propertyItem);
		}
		session.getTransaction().commit();
		return propertyItem;
	}

	public PropertyItem find(int id) {
		PropertyItem propertyItem = new PropertyItem();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			propertyItem = (PropertyItem) session.load(PropertyItem.class, id);
			// session.getTransaction().commit();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return propertyItem;
	}

	@SuppressWarnings("unchecked")
	public List<PropertyItem> list(int propertyId) {
		//Session session = HibernateUtil.getSessionFactory().openSession();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyItem> propertyItems = null;
		try {
              
			//propertyItems = (List<PropertyItem>) session.createQuery("from PropertyItem").list();
			propertyItems = (List<PropertyItem>) session.createQuery("from PropertyItem where pmsProperty.propertyId=:propertyId and isActive = 'true' and isDeleted = 'false'").setParameter("propertyId", propertyId).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return propertyItems;
	}

	
}
