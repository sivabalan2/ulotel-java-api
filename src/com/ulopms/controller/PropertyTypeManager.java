package com.ulopms.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;

import com.ulopms.model.PropertyType;
import com.ulopms.util.HibernateUtil;


public class PropertyTypeManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(PropertyTypeManager.class);

	public PropertyType add(PropertyType propertyType) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(propertyType);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyType;
	}

	public PropertyType edit(PropertyType propertyType) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(propertyType);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyType;
	}

	public PropertyType delete(Long id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		PropertyType propertyType = (PropertyType) session.load(PropertyType.class, id);
		if (null != propertyType) {
			session.delete(propertyType);
		}
		session.getTransaction().commit();
		return propertyType;
	}

	
	public PropertyType find(Integer id) {
		
	        PropertyType propertyType = new PropertyType();
	        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
	        try {
	            session.beginTransaction();
	            propertyType = (PropertyType) session.get(PropertyType.class, id);
	           // session.getTransaction().commit();
	        } catch (Exception e1) {
	            logger.error(e1);
	        } finally {
	            session = null;
	        }
	        return propertyType;
	    }

	@SuppressWarnings("unchecked")
	public List<PropertyType> list() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyType> propertyTypes = null;
		try {

			propertyTypes = (List<PropertyType>) session.createQuery("from PropertyType").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyTypes;
	}

	
}
