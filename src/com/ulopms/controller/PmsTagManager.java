package com.ulopms.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;

import com.ulopms.model.PmsTags;
import com.ulopms.util.HibernateUtil;


public class PmsTagManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(PmsTagManager.class);

	public PmsTags add(PmsTags propertyTag) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(propertyTag);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyTag;
	}

	public PmsTags edit(PmsTags propertyTag) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(propertyTag);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyTag;
	}

	public PmsTags delete(int id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		PmsTags propertyTag = (PmsTags) session.load(PmsTags.class, id);
		if (null != propertyTag) {
			session.delete(propertyTag);
		}
		session.getTransaction().commit();
		return propertyTag;
	}

	public PmsTags find(int id) {
		PmsTags propertyTag = new PmsTags();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			propertyTag = (PmsTags) session.get(PmsTags.class, id);
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return propertyTag;
	}
	
	@SuppressWarnings("unchecked")
	public List<PmsTags> list() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsTags> propertyTag = null;
		try {

			propertyTag = (List<PmsTags>) session.createQuery("from PmsTags where isActive = 'true' "
					+ "and isDeleted = 'false' order by tagId").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyTag;
	}

}
