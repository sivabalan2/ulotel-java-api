package com.ulopms.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;
import org.hibernate.transform.Transformers;

import com.ulopms.model.BookingDetailReport;
import com.ulopms.model.PmsProperty;
import com.ulopms.model.PropertyReviews;
import com.ulopms.model.Revenue;
import com.ulopms.util.HibernateUtil;


public class PropertyReviewsManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(PropertyReviewsManager.class);

	public PropertyReviews add(PropertyReviews propertyReviews) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(propertyReviews);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyReviews;
	}

	public PropertyReviews edit(PropertyReviews propertyReviews) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(propertyReviews);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyReviews;
	}

	public PropertyReviews delete(int id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		PropertyReviews propertyReviews = (PropertyReviews) session.load(PropertyReviews.class, id);
		if (null != propertyReviews) {
			session.delete(propertyReviews);
		}
		session.getTransaction().commit();
		return propertyReviews;
	}

	public PropertyReviews find(int id) {
		PropertyReviews propertyReviews = new PropertyReviews();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			propertyReviews = (PropertyReviews) session.load(PropertyReviews.class, id);
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return propertyReviews;
	}

	@SuppressWarnings("unchecked")
	public List<PropertyReviews> list() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyReviews> propertyReviews = null;
		try {

			propertyReviews = (List<PropertyReviews>) session.createQuery("from PropertyReviews where isActive = 'true'"
					+ " and isDeleted = 'false' ").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyReviews;
	}
	
	@SuppressWarnings("unchecked")
	public List<BookingDetailReport> list(int propertyReviewId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<BookingDetailReport> propertyReviews = null;
		try {

			propertyReviews = (List<BookingDetailReport>) session.createQuery("from PropertyReviews pr "
					+ " where  pr.isActive = 'true' and pr.isDeleted = 'false' and pr.propertyReviewId=:propertyReviewId ")
					.setParameter("propertyReviewId", propertyReviewId)
					.setResultTransformer(Transformers.aliasToBean(BookingDetailReport.class)).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyReviews;
	}
	
	@SuppressWarnings("unchecked")
	public List<PropertyReviews> list(int limit, int offset) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyReviews> propertyReviews = null;
		try {

			propertyReviews = (List<PropertyReviews>) session.createQuery("from PropertyReviews pr "
					+ " where  pr.isActive = 'true' and pr.isDeleted = 'false' ")
					.setFirstResult(offset)
					.setMaxResults(limit)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyReviews;
	}
	
	@SuppressWarnings("unchecked")
	public List<PropertyReviews> listReview(int propertyReviewId, int reviewPropertyId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyReviews> propertyReviews = null;
		try {

			propertyReviews = (List<PropertyReviews>) session.createQuery("from PropertyReviews pr "
					+ " where  pr.isActive = 'true' and pr.isDeleted = 'false' and pr.propertyReviewId=:propertyReviewId and pr.pmsProperty.propertyId=:reviewPropertyId")
					.setParameter("propertyReviewId",propertyReviewId)
					.setParameter("reviewPropertyId",reviewPropertyId)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyReviews;
	}
	
	@SuppressWarnings("unchecked")
	public List<PropertyReviews> listReview(int reviewPropertyId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyReviews> propertyReviews = null;
		try {

			propertyReviews = (List<PropertyReviews>) session.createQuery("from PropertyReviews pr "
					+ " where  pr.isActive = 'true' and pr.isDeleted = 'false' and pr.pmsProperty.propertyId=:reviewPropertyId")
					.setParameter("reviewPropertyId", reviewPropertyId)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyReviews;
	}
	

}
