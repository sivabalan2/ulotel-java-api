package com.ulopms.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;
import org.joda.time.DateTime;

import com.ulopms.model.OtaHotelDetails;
import com.ulopms.model.OtaHotels;
import com.ulopms.model.PmsSource;
import com.ulopms.model.PropertyAccommodation;
import com.ulopms.util.HibernateUtil;


public class OtaHotelsManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(OtaHotelsManager.class);

	public OtaHotels add(OtaHotels OtaHotels) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(OtaHotels);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return OtaHotels;
	}
	
	public OtaHotelDetails add(OtaHotelDetails otaHotelDetails) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(otaHotelDetails);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return otaHotelDetails;
	}

	public OtaHotels edit(OtaHotels OtaHotels) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(OtaHotels);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return OtaHotels;
	}

	public OtaHotelDetails findDetailId(int id) {
		OtaHotelDetails otaHotelDetails = new OtaHotelDetails();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			otaHotelDetails = (OtaHotelDetails) session.get(OtaHotelDetails.class, id);
			// session.getTransaction().commit();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return otaHotelDetails;
	}
	
	public OtaHotelDetails edit(OtaHotelDetails otaHotelDetails) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(otaHotelDetails);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return otaHotelDetails;
	}
	
	public OtaHotels delete(int id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		OtaHotels OtaHotels = (OtaHotels) session.load(OtaHotels.class, id);
		if (null != OtaHotels) {
			session.delete(OtaHotels);
		}
		session.getTransaction().commit();
		return OtaHotels;
	}

	public OtaHotels find(int id) {
		OtaHotels OtaHotels = new OtaHotels();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			OtaHotels = (OtaHotels) session.get(OtaHotels.class, id);
			// session.getTransaction().commit();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return OtaHotels;
	}

	
	@SuppressWarnings("unchecked")
	public OtaHotelDetails getOtaHotelDetails(int propertyAccommodationId,int limit) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		OtaHotelDetails otaHotelDetails = new OtaHotelDetails();
		try {
            
			otaHotelDetails = (OtaHotelDetails) session.createQuery("from OtaHotelDetails where propertyAccommodation.accommodationId=:propertyAccommodationId"					
					+ " and IsActive = 'true'")
					.setParameter("propertyAccommodationId",propertyAccommodationId)
					.setMaxResults(limit)
					.uniqueResult();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return otaHotelDetails;
	}
	
	
	@SuppressWarnings("unchecked")
	public OtaHotels getOtaHotels(int propertyId ,int sourceId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		OtaHotels otaHotels = new OtaHotels();
		try {

			otaHotels = (OtaHotels) session.createQuery("from OtaHotels where pmsProperty.propertyId=:propertyId "
					+ "and pmsSource.sourceId=:sourceId"
					+ " and otaIsActive = 'true'")
					.setParameter("propertyId", propertyId )
					.setParameter("sourceId", sourceId)
					.uniqueResult();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return otaHotels;
	}
	
	@SuppressWarnings("unchecked")
	public OtaHotelDetails findDetailsId(int otaHotelId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		OtaHotelDetails otaHotelDetails = new OtaHotelDetails();
		try {

			otaHotelDetails = (OtaHotelDetails) session.createQuery("from OtaHotelDetails where otaHotels.otaHotelId=:otaHotelId"					
					+ " and IsActive = 'true'")
					.setParameter("otaHotelId", otaHotelId )
					.uniqueResult();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return otaHotelDetails;
	}
	
	@SuppressWarnings("unchecked")
	public OtaHotelDetails findPropertyId(Long HotelCode,Long roomTypeId) {
		String otaHotelCode = Long.toString(HotelCode);
		String otaRoomTypeId = Long.toString(roomTypeId);
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		OtaHotelDetails otaHotelDetails = new OtaHotelDetails();
		try {

			otaHotelDetails = (OtaHotelDetails) session.createQuery("from OtaHotelDetails  where otaHotelCode=:otaHotelCode"					
					+ " and otaRoomTypeId =:otaRoomTypeId and IsActive = 'true'")
					.setParameter("otaHotelCode", otaHotelCode)
					.setParameter("otaRoomTypeId", otaRoomTypeId)
					.setMaxResults(1)
					.uniqueResult();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return otaHotelDetails;
	}	
	
	@SuppressWarnings("unchecked")
	public List<OtaHotels> list(int propertyId ,int sourceId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<OtaHotels> otaHotels = null;
		try {

			otaHotels = (List<OtaHotels>) session.createQuery("from OtaHotels where pmsProperty.propertyId=:propertyId "
					+ "and pmsSource.sourceId=:sourceId and otaIsActive = 'true'") 					
					.setParameter("propertyId", propertyId )
					.setParameter("sourceId", sourceId)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return otaHotels;
	}
	
	@SuppressWarnings("unchecked")
	public List<OtaHotelDetails> list(int propertyId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<OtaHotelDetails> otaHotelDetails = null;
		try {

			otaHotelDetails = (List<OtaHotelDetails>) session.createQuery("from OtaHotelDetails where pmsProperty.propertyId=:propertyId "
					+ "and  IsActive = 'true'") 					
					.setParameter("propertyId", propertyId )
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return otaHotelDetails;
	}
	
	@SuppressWarnings("unchecked")
	public List<OtaHotelDetails> listDetails(String otaHotelCode ,int accommodationId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<OtaHotelDetails> otaHotelDetails = null;
		try {

			otaHotelDetails = (List<OtaHotelDetails>) session.createQuery("from OtaHotelDetails where otaHotelCode=:otaHotelCode "
					+ "and propertyAccommodation.accommodationId=:accommodationId and IsActive = 'true'") 					
					.setParameter("otaHotelCode", otaHotelCode )
					.setParameter("accommodationId", accommodationId)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return otaHotelDetails;
	}

	
	@SuppressWarnings("unchecked")
	public List<OtaHotels> listAll() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<OtaHotels> OtaHotelss = null;
		try {

			OtaHotelss = (List<OtaHotels>) session.createQuery("from OtaHotels where isActive = 'true' and isDeleted = 'false'")
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return OtaHotelss;
	}

	
	@SuppressWarnings("unchecked")
	public OtaHotels getOtaHotelToken(String otaHotelCode) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		OtaHotels otaHotels = new OtaHotels();
		try {

			otaHotels = (OtaHotels) session.createQuery("from OtaHotels where otaHotelCode=:otaHotelCode "
					+ " and otaIsActive = 'true'")
					.setParameter("otaHotelCode", otaHotelCode)
					.uniqueResult();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return otaHotels;
	}
	
}
