package com.ulopms.controller;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;
import org.hibernate.transform.Transformers;

import com.ulopms.model.BlogArticleImages;
import com.ulopms.model.BlogCategories;
import com.ulopms.model.BlogUserComments;
import com.ulopms.model.DashBoard;
import com.ulopms.model.PmsRoomDetail;
import com.ulopms.util.HibernateUtil;


public class BlogUserCommentsManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(BlogUserCommentsManager.class);

	public BlogUserComments add(BlogUserComments blogUserComments) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(blogUserComments);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return blogUserComments;
	}

	public BlogUserComments edit(BlogUserComments blogUserComments) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(blogUserComments);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return blogUserComments;
	}

	public BlogUserComments delete(int id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		BlogUserComments blogUserComments = (BlogUserComments) session.load(BlogUserComments.class, id);
		if (null != blogUserComments) {
			session.delete(blogUserComments);
		}
		session.getTransaction().commit();
		return blogUserComments;
	}

	public BlogUserComments find(int id) {
		BlogUserComments blogUserComments = new BlogUserComments();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			blogUserComments = (BlogUserComments) session.load(BlogUserComments.class, id);
			// session.getTransaction().commit();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return blogUserComments;
	}
	
	@SuppressWarnings("unchecked")
	public List<BlogUserComments> list() {

		//Session session = HibernateUtil.getSessionFactory().openSession();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<BlogUserComments> blogUserComments = null;
		try {

			blogUserComments = (List<BlogUserComments>) session.createQuery("from BlogUserComments where isActive = 'true' "
					+ "and isDeleted = 'false' ")					
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return blogUserComments;
	}
	
	@SuppressWarnings("unchecked")
	public List<BlogUserComments> list(int limit,int offset) {

		//Session session = HibernateUtil.getSessionFactory().openSession();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<BlogUserComments> blogUserComments = null;
		try {

			blogUserComments = (List<BlogUserComments>) session.createQuery("from BlogUserComments where isActive = 'true' "
					+ "and isDeleted = 'false' ")
					.setFirstResult(offset)
					.setMaxResults(limit)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return blogUserComments;
	}

	
	@SuppressWarnings("unchecked")
	public List<BlogUserComments> list(int blogArticleId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<BlogUserComments> blogUserComments = null;
		try {

			blogUserComments = (List<BlogUserComments>) session.createQuery("from BlogUserComments ba where blogArticles.blogArticleId=:blogArticleId and isActive = true and isDeleted =false and isDisplay = true ")
					 .setParameter("blogArticleId",blogArticleId)
					 .list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return blogUserComments;
	}	
	

	
}
