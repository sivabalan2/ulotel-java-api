package com.ulopms.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;

import com.ulopms.model.GooglePropertyAreas;
import com.ulopms.model.OtaHotelDetails;
import com.ulopms.model.PmsProperty;
import com.ulopms.model.PmsTags;
import com.ulopms.model.PropertyTags;
import com.ulopms.model.UlotelHotelDetailTags;
import com.ulopms.util.HibernateUtil;


public class PmsPropertyManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(PmsPropertyManager.class);

	public PmsProperty add(PmsProperty pmsProperty) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(pmsProperty);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return pmsProperty;
	}

	public PmsProperty edit(PmsProperty pmsProperty) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(pmsProperty);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return pmsProperty;
	}
	
	public UlotelHotelDetailTags edit(UlotelHotelDetailTags ulotelHotelDetailTags) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(ulotelHotelDetailTags);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return ulotelHotelDetailTags;
	}


	public PmsProperty delete(int id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		PmsProperty pmsProperty = (PmsProperty) session.load(PmsProperty.class, id);
		if (null != pmsProperty) {
			session.delete(pmsProperty);
		}
		session.getTransaction().commit();
		return pmsProperty;
	}

	public PmsProperty find(int id) {
		PmsProperty pmsProperty = new PmsProperty();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			pmsProperty = (PmsProperty) session.load(PmsProperty.class, id);
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return pmsProperty;
	}
	
	public PmsTags findTagId(int id) {
		PmsTags pmsTag = new PmsTags();
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			session.beginTransaction();
			pmsTag = (PmsTags) session.load(PmsTags.class, id);
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return pmsTag;
	}
	
	public PropertyTags edit(PropertyTags propertyTags) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(propertyTags);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyTags;
	}
	
	public PropertyTags add(PropertyTags propertyTags) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(propertyTags);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyTags;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<PmsProperty> list() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsProperty> pmsPropertys = null;
		try {

			pmsPropertys = (List<PmsProperty>) session.createQuery("from PmsProperty where isActive = 'true'"
					+ " and isDeleted = 'false' ").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return pmsPropertys;
	}
	
	@SuppressWarnings("unchecked")
	public UlotelHotelDetailTags findHotelTag(int propertyId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		UlotelHotelDetailTags ulotelHotelDetailTags = new UlotelHotelDetailTags();
		try {
            
			ulotelHotelDetailTags = (UlotelHotelDetailTags) session.createQuery("from UlotelHotelDetailTags where pmsProperty.propertyId=:propertyId")					
					.setParameter("propertyId",propertyId)
					.uniqueResult();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return ulotelHotelDetailTags;
	}
	
	@SuppressWarnings("unchecked")
	public List<PmsTags> listTag() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsTags> pmsTag = null;
		try {

			pmsTag = (List<PmsTags>) session.createQuery("from PmsTags where isActive = 'true'"
					+ " and isDeleted = 'false' ").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return pmsTag;
	}
	
	@SuppressWarnings("unchecked")
	public List<PropertyTags> findTag(int propertyId,int tagId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyTags> propertyTags = null;
		try {

			propertyTags = (List<PropertyTags>) session.createQuery("from PropertyTags pt where isActive = 'true'"
					+ " and isDeleted = 'false' and pt.pmsTags.tagId =:tagId and pt.pmsProperty.propertyId =:propertyId")
					.setParameter("propertyId", propertyId)
					.setParameter("tagId", tagId)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyTags;
	}
	
	@SuppressWarnings("unchecked")
	public List<PropertyTags> findTag(int propertyId,String tagids) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyTags> propertyTags = null;
		try {

			propertyTags = (List<PropertyTags>) session.createQuery("from PropertyTags pt where isActive = 'true'"
					+ " and isDeleted = 'false' and pt.pmsTags.tagId in ("+tagids+") and pt.pmsProperty.propertyId =:propertyId")
					.setParameter("propertyId", propertyId)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyTags;
	}
	
	@SuppressWarnings("unchecked")
	public List<PropertyTags> findTag(int propertyId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyTags> propertyTags = null;
		try {

			propertyTags = (List<PropertyTags>) session.createQuery("from PropertyTags pt where isActive = 'true'"
					+ " and isDeleted = 'false' and pt.pmsProperty.propertyId =:propertyId")
					.setParameter("propertyId", propertyId)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyTags;
	}

	@SuppressWarnings("unchecked")
	public List<PmsProperty> list(int limit,int offset) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsProperty> pmsPropertys = null;
		try {

			pmsPropertys = (List<PmsProperty>) session.createQuery("from PmsProperty where isActive = 'true' "
					+ "and isDeleted = 'false' ORDER BY propertyId DESC")
					.setFirstResult(offset)
					.setMaxResults(limit)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return pmsPropertys;
	}
	
	@SuppressWarnings("unchecked")
	public List<PmsProperty> list(int propertyId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsProperty> pmsPropertys = null;
		try {

			pmsPropertys = (List<PmsProperty>) session.createQuery("from PmsProperty where propertyId=:propertyId "
					+ " and hotelIsActive='true' and propertyIsControl='true' ")
			.setParameter("propertyId", propertyId).list();
			
			

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return pmsPropertys;
	}
	
	@SuppressWarnings("unchecked")
	public List<PmsProperty> listGooglePlaceProperty(int propertyId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsProperty> pmsPropertys = null;
		try {

			pmsPropertys = (List<PmsProperty>) session.createQuery("from PmsProperty where propertyId=:propertyId "
					+ " and isActive='true' and isDeleted='false' ")
			.setParameter("propertyId", propertyId).list();
			
			

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return pmsPropertys;
	}
	
	@SuppressWarnings("unchecked")
	public List<PmsProperty> listPropertyByUser(int userId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsProperty> pmsPropertys = null;
		try {

			pmsPropertys = (List<PmsProperty>) session.createQuery("select a from PmsProperty a left join a.propertyUsers b "
					+ "where b.isActive = 'true' and b.isDeleted = 'false' and a.propertyId=b.pmsProperty.propertyId and b.user.userId=:userId "
					+ "order by a.propertyId desc")
			.setParameter("userId", userId).list();
			
			

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return pmsPropertys;
	}
	@SuppressWarnings("unchecked")
	public List<PmsProperty> listPropertyByLocation(int locationId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsProperty> pmsPropertys = null;
		try {

			pmsPropertys = (List<PmsProperty>) session.createQuery("select a from PmsProperty a where "
					+ "a.googleLocation.googleLocationId=:locationId and a.isActive = 'true' and a.isDeleted = 'false' and "
					+ " a.propertyIsControl = 'true' and a.hotelIsActive='true' ")
			.setParameter("locationId", locationId).list();
			
			

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return pmsPropertys;
	}
	
	@SuppressWarnings("unchecked")
	public List<PmsProperty> listPropertyByLocation(int locationId,int limit) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsProperty> pmsPropertys = null;
		try {

			pmsPropertys = (List<PmsProperty>) session.createQuery("select a from PmsProperty a where "
					+ "a.location.locationId=:locationId and a.isActive = 'true' and a.isDeleted = 'false' and "
					+ " a.propertyIsControl = 'true' and a.hotelIsActive='true' ")
			.setParameter("locationId", locationId).list();
			
			

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return pmsPropertys;
	}
	
	@SuppressWarnings("unchecked")
	public List<PmsProperty> listFirstPropertyByLocation(int locationId,int limit) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsProperty> pmsPropertys = null;
		try {

			pmsPropertys = (List<PmsProperty>) session.createQuery("select a from PmsProperty a where "
					+ "a.location.locationId=:locationId and a.isActive = 'true' and a.isDeleted = 'false'"
					+ " and a.hotelIsActive='true' and a.propertyIsControl = 'true' ")
			.setParameter("locationId", locationId)
			.setMaxResults(limit)
			.list();
			
			

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return pmsPropertys;
	}
	
	@SuppressWarnings("unchecked")
	public List<PmsProperty> listPropertyByArea(int areaId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsProperty> pmsPropertys = null;
		try {

			pmsPropertys = (List<PmsProperty>) session.createQuery("select a from PmsProperty a where "
					+ "a.area.areaId=:areaId and a.isActive = 'true' and a.isDeleted = 'false'")
			.setParameter("areaId", areaId).list();
			
			

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return pmsPropertys;
	}
	
	@SuppressWarnings("unchecked")
	public List<PmsProperty> listPropertyUrl(String propertyUrl) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsProperty> pmsPropertys = null;
		try {

			pmsPropertys = (List<PmsProperty>) session.createQuery("select a from PmsProperty a where propertyUrl=:propertyUrl "
					+ "and a.isActive = 'true' and a.isDeleted = 'false'")
			.setParameter("propertyUrl", propertyUrl).list();
			
			

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return pmsPropertys;
	}
	
	@SuppressWarnings("unchecked")
	public List<PmsProperty> listInActivePropertyUrl(String propertyUrl) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsProperty> pmsPropertys = null;
		try {

			pmsPropertys = (List<PmsProperty>) session.createQuery("select a from PmsProperty a where propertyUrl=:propertyUrl "
					+ "and a.isActive = 'false' and a.isDeleted = 'true'")
			.setParameter("propertyUrl", propertyUrl).list();
			
			

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return pmsPropertys;
	}

	@SuppressWarnings("unchecked")
	public List<PmsProperty> listProperty() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsProperty> pmsPropertys = null;
		try {

			pmsPropertys = (List<PmsProperty>) session.createQuery("from PmsProperty a where "
					+ " a.isActive = 'true' and a.isDeleted = 'false' and "
					+ " a.propertyIsControl = 'true' and a.hotelIsActive='true' ").list();
			
			

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return pmsPropertys;
	}
	
	@SuppressWarnings("unchecked")
	public List<PmsProperty> listEditProperty(int propertyId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsProperty> pmsPropertys = null;
		try {

			pmsPropertys = (List<PmsProperty>) session.createQuery("from PmsProperty where propertyId=:propertyId ")
			.setParameter("propertyId", propertyId).list();
			
			

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return pmsPropertys;
	}
	
	@SuppressWarnings("unchecked")
	public List<GooglePropertyAreas> listPropertyByGoogleArea(int areaId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<GooglePropertyAreas> propertyAreas = null;
		try {

			propertyAreas = (List<GooglePropertyAreas>) session.createQuery("from GooglePropertyAreas "
					+ "where googleArea.googleAreaId=:areaId and isDeleted = 'false' "
					+ "and isActive = 'true' and propertyIsControl='true' ")
					.setParameter("areaId", areaId).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyAreas;
	}
	
	@SuppressWarnings("unchecked")
	public List<GooglePropertyAreas> listPropertyByGoogleArea(String areaId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<GooglePropertyAreas> propertyAreas = null;
		try {

			propertyAreas = (List<GooglePropertyAreas>) session.createQuery("from GooglePropertyAreas "
					+ "where googleArea.googleAreaId in ("+areaId+") and isDeleted = 'false' "
					+ "and isActive = 'true' and propertyIsControl='true' ")
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyAreas;
	}
}
