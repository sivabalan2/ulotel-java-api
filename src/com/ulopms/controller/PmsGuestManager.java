package com.ulopms.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;
import org.hibernate.transform.Transformers;

import com.ulopms.model.PmsGuest;
import com.ulopms.model.User;
import com.ulopms.util.HibernateUtil;


public class PmsGuestManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(PmsGuestManager.class);

	public PmsGuest add(PmsGuest pmsGuest) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(pmsGuest);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return pmsGuest;
	}

	public PmsGuest edit(PmsGuest pmsGuest) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(pmsGuest);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return pmsGuest;
	}

	public PmsGuest delete(Long id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		PmsGuest pmsGuest = (PmsGuest) session.load(PmsGuest.class, id);
		if (null != pmsGuest) {
			session.delete(pmsGuest);
		}
		session.getTransaction().commit();
		return pmsGuest;
	}

	public PmsGuest find(int id) {
		PmsGuest pmsGuest = new PmsGuest();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			pmsGuest = (PmsGuest) session.load(PmsGuest.class, id);
			// session.getTransaction().commit();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return pmsGuest;
	}
	
	@SuppressWarnings("unchecked")
	public List<PmsGuest> findGuestByEmail(String email) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsGuest> pmsGuests = null;
		try {
			pmsGuests = session.createQuery("from PmsGuest where emailId=:email")
					.setParameter("email", email).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return pmsGuests;
	}

	@SuppressWarnings("unchecked")
	public List<PmsGuest> list() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsGuest> pmsGuests = null;
		try {

			pmsGuests = (List<PmsGuest>) session.createQuery("from PmsGuest").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return pmsGuests;
	}
	
	@SuppressWarnings("unchecked")
	public PmsGuest findSmsId(int id,String smsOtp) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		PmsGuest pmsGuest = new PmsGuest();
		try {
			pmsGuest = (PmsGuest) session.createQuery("select guestId as guestId ,payAtHotelOtp as payAtHotelOtp from PmsGuest "
					+ "where guestId=:id and payAtHotelOtp=:smsOtp")
					.setParameter("id", id)
					.setParameter("smsOtp", smsOtp)
					.setResultTransformer(Transformers.aliasToBean(PmsGuest.class))
					.uniqueResult();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return pmsGuest;
	}
	
	@SuppressWarnings("unchecked")
	public List<PmsGuest> listCheckMobileNumber(String mobileNumber) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsGuest> pmsGuests = null;
		try {

			pmsGuests = (List<PmsGuest>) session.createQuery("from PmsGuest where isActive='true' and"
					+ " isDeleted='false' and phone=:phone").setParameter("phone", mobileNumber).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return pmsGuests;
	}

	@SuppressWarnings("unchecked")
	public List<PmsGuest> listCheckEmail(String emailid) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsGuest> pmsGuests = null;
		try {

			pmsGuests = (List<PmsGuest>) session.createQuery("from PmsGuest where isActive='true' and"
					+ " isDeleted='false' and emailId=:emailid").setParameter("emailid", emailid).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return pmsGuests;
	}
	
}
