package com.ulopms.controller;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.classic.Session;
import org.hibernate.transform.Transformers;
import org.joda.time.DateTime;

import com.ulopms.model.PmsPromotions;
import com.ulopms.model.PmsRewards;
import com.ulopms.model.RewardDetails;
import com.ulopms.model.RewardUserDetails;
import com.ulopms.util.HibernateUtil;


public class RewardsManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(RewardsManager.class);

	public PmsRewards add(PmsRewards pmsRewards) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(pmsRewards);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return pmsRewards;
	}
	
	public RewardUserDetails add(RewardUserDetails rewardUserDetails) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(rewardUserDetails);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return rewardUserDetails;
	}

	public PmsRewards edit(PmsRewards pmsRewards) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(pmsRewards);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return pmsRewards;
	}
	
	public RewardDetails edit(RewardDetails rewardDetails) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(rewardDetails);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return rewardDetails;
	}

	public PmsRewards delete(Long id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		PmsRewards pmsRewards = (PmsRewards) session.load(PmsRewards.class, id);
		if (null != pmsRewards) {
			session.delete(pmsRewards);
		}
		session.getTransaction().commit();
		return pmsRewards;
	}

	public PmsRewards find(int id) {
		PmsRewards pmsRewards = new PmsRewards();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			pmsRewards = (PmsRewards) session.load(PmsRewards.class, id);
			// session.getTransaction().commit();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return pmsRewards;
	}
	
	public RewardDetails findDetailId(int id) {
		RewardDetails rewardDetails = new RewardDetails();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			rewardDetails = (RewardDetails) session.load(RewardDetails.class, id);
			// session.getTransaction().commit();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return rewardDetails;
	}
	
	public RewardDetails findRewardDetailId(int rewardDetailId) {
		RewardDetails rewardDetails = new RewardDetails();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			//rewardDetails = (RewardDetails) session.load(RewardDetails.class, id);
			rewardDetails = (RewardDetails) session.createQuery("from RewardDetails where rewardDetailId =:rewardDetailId")
					.setParameter("rewardDetailId", rewardDetailId)					
					.uniqueResult();
			// session.getTransaction().commit();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return rewardDetails;
	}
	
	public RewardUserDetails findUserId(int userId) {
		RewardUserDetails rewardUserDetails = null;
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			rewardUserDetails = (RewardUserDetails) session.createQuery("select usedRewardPoints as usedRewardPoints,rewardUserDetailId as rewardUserDetailId from "
					+ "RewardUserDetails rud where rud.user.userId =:userId ")
					.setParameter("userId", userId)
					.setResultTransformer(Transformers.aliasToBean(RewardUserDetails.class))
					.uniqueResult();

			// session.getTransaction().commit();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return rewardUserDetails;
	}

	@SuppressWarnings("unchecked")
	public List<PmsRewards> list() {
		//Session session = HibernateUtil.getSessionFactory().openSession();
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsRewards> rewardList = null;
		try {
              
			rewardList = (List<PmsRewards>) session.createQuery("from PmsRewards where isActive = 'true' and isDeleted = 'false'")
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return rewardList;
	}
	
	@SuppressWarnings("unchecked")
	public List<RewardDetails> listDetails() {
		//Session session = HibernateUtil.getSessionFactory().openSession();
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<RewardDetails> rewardDetailList = null;
		try {
              
			rewardDetailList = (List<RewardDetails>) session.createQuery("from RewardDetails where isActive = 'true' and isDeleted = 'false'")
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return rewardDetailList;
	}
	
	@SuppressWarnings("unchecked")
	public List<PmsPromotions> listPromotions(int propertyId,Date date) {
		//Session session = HibernateUtil.getSessionFactory().openSession();
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsPromotions> promotionList = null;
		try {
              
			//propertyItems = (List<PropertyItem>) session.createQuery("from PropertyItem").list();
			promotionList = (List<PmsPromotions>) session.createQuery("from PmsPromotions where pmsProperty.propertyId=:propertyId"
					+ " and :date between startDate and endDate and isActive = 'true' and isDeleted = 'false' and promotionIsActive='true' ")
					.setParameter("propertyId", propertyId)
					.setParameter("date", date)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return promotionList;
	}
	@SuppressWarnings("unchecked")
	public List<PmsPromotions> listActive(int propertyId) {
		//Session session = HibernateUtil.getSessionFactory().openSession();
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsPromotions> promotionList = null;
		try {
              
			//propertyItems = (List<PropertyItem>) session.createQuery("from PropertyItem").list();
			promotionList = (List<PmsPromotions>) session.createQuery("from PmsPromotions where pmsProperty.propertyId=:propertyId"
					+ " and isActive = 'true' and isDeleted = 'false'").setParameter("propertyId", propertyId).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return promotionList;
	}
	
	@SuppressWarnings("unchecked")
	public List<PmsPromotions> listPromotions(int propertyId) {
		//Session session = HibernateUtil.getSessionFactory().openSession();
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsPromotions> promotionList = null;
		try {
              
			//propertyItems = (List<PropertyItem>) session.createQuery("from PropertyItem").list();
			promotionList = (List<PmsPromotions>) session.createQuery("from PmsPromotions where pmsProperty.propertyId=:propertyId"
					+ " and isActive = 'true' and isDeleted = 'false' and promotionIsActive='true' ")
					.setParameter("propertyId", propertyId)
					
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return promotionList;
	}
	
	@SuppressWarnings("unchecked")
	public List<PmsPromotions> listPromotions(int propertyId,Date date,String strPromotionType) {
		//Session session = HibernateUtil.getSessionFactory().openSession();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsPromotions> promotionList = null;
		try {
              
			//propertyItems = (List<PropertyItem>) session.createQuery("from PropertyItem").list();
			promotionList = (List<PmsPromotions>) session.createQuery("from PmsPromotions where pmsProperty.propertyId=:propertyId and promotionType=:promotionType "
					+ " and :date between startDate and endDate and isActive = 'true' and isDeleted = 'false' and promotionIsActive='true' ")
					.setParameter("propertyId", propertyId)
					.setParameter("promotionType", strPromotionType)
					.setParameter("date", date)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return promotionList;
	}
	
	@SuppressWarnings("unchecked")
	public List<PmsPromotions> listPromotions(int propertyId,Date date,int accommodationId) {
//		Session session = HibernateUtil.getSessionFactory().openSession();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsPromotions> promotionList = null;
		try {
              
			//propertyItems = (List<PropertyItem>) session.createQuery("from PropertyItem").list();
			promotionList = (List<PmsPromotions>) session.createQuery("from PmsPromotions where pmsProperty.propertyId=:propertyId "
					+ " and :date between startDate and endDate and isActive = 'true' and isDeleted = 'false' and promotionIsActive='true' "
					+ " and propertyAccommodation.accommodationId=:accommodationId")
					.setParameter("propertyId", propertyId)
					.setParameter("accommodationId", accommodationId)
					.setParameter("date", date)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return promotionList;
	}
	
	@SuppressWarnings("unchecked")
	public List<PmsPromotions> listPromotions(int propertyId,Date date,String strPromotionType,int accommodationId) {
//		Session session = HibernateUtil.getSessionFactory().openSession();
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsPromotions> promotionList = null;
		try {
              
			//propertyItems = (List<PropertyItem>) session.createQuery("from PropertyItem").list();
			promotionList = (List<PmsPromotions>) session.createQuery("from PmsPromotions where pmsProperty.propertyId=:propertyId and promotionType=:promotionType "
					+ " and :date between startDate and endDate and isActive = 'true' and isDeleted = 'false' and promotionIsActive='true' "
					+ " propertyAccommodation.accommodationId=:accommodationId")
					.setParameter("propertyId", propertyId)
					.setParameter("accommodationId", accommodationId)
					.setParameter("promotionType", strPromotionType)
					.setParameter("date", date)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return promotionList;
	}
	@SuppressWarnings("unchecked")
	public List<PmsPromotions> listPromotionsPerAccommodation(int accommodationId,Date date) {
		//Session session = HibernateUtil.getSessionFactory().openSession();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsPromotions> promotionList = null;
		try {
              
			//propertyItems = (List<PropertyItem>) session.createQuery("from PropertyItem").list();
			promotionList = (List<PmsPromotions>) session.createQuery("from PmsPromotions where propertyAccommodation.accommodationId=:accommodationId "
					+ " and :date between startDate and endDate and isActive = 'true' and isDeleted = 'false' and promotionIsActive='true' ")
					.setParameter("accommodationId", accommodationId)
					.setParameter("date", date)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return promotionList;
	}
	
	@SuppressWarnings("unchecked")
	public List<PmsPromotions> listUpdatePromotions(int promotionId) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsPromotions> promotionList = null;
		try {
              
			//propertyItems = (List<PropertyItem>) session.createQuery("from PropertyItem").list();
			promotionList = (List<PmsPromotions>) session.createQuery("from PmsPromotions where promotionId=:promotionId"
					+ " and isActive = 'true' and isDeleted = 'false' ").setParameter("promotionId", promotionId).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return promotionList;
	}
	
	@SuppressWarnings("unchecked")
	public List<PmsPromotions> listPromotionsDetails(int propertyId,Date date) {
		//Session session = HibernateUtil.getSessionFactory().openSession();
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsPromotions> promotionList = null;
		try {
              
			//propertyItems = (List<PropertyItem>) session.createQuery("from PropertyItem").list();
			promotionList = (List<PmsPromotions>) session.createQuery("from PmsPromotions where pmsProperty.propertyId=:propertyId"
					+ " and isActive = 'true' and isDeleted = 'false' and (startDate>=:startDate or endDate>=:endDate) order by promotionId DESC")
					.setParameter("propertyId", propertyId)
					.setParameter("startDate", date)
					.setParameter("endDate", date)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return promotionList;
	}
	
	@SuppressWarnings("unchecked")
	public List<PmsPromotions> listDatePromotions(int propertyId,Date start,Date end) {
		//Session session = HibernateUtil.getSessionFactory().openSession();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsPromotions> promotionList = null;
		try {
              
			//propertyItems = (List<PropertyItem>) session.createQuery("from PropertyItem").list();
			promotionList = (List<PmsPromotions>) session.createQuery("from PmsPromotions where pmsProperty.propertyId=:propertyId"
					+ " and isActive = 'true' and isDeleted = 'false' and promotionIsActive='true' and startDate=:startDate and endDate=:endDate order by promotionId DESC")
					.setParameter("propertyId", propertyId)
					.setParameter("startDate", start)
					.setParameter("endDate", end)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return promotionList;
	}
	
	@SuppressWarnings("unchecked")
	public List<PmsPromotions> listDatePromotions(int propertyId,Date date) {
		//Session session = HibernateUtil.getSessionFactory().openSession();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsPromotions> promotionList = null;
		try {
              
			//propertyItems = (List<PropertyItem>) session.createQuery("from PropertyItem").list();
			promotionList = (List<PmsPromotions>) session.createQuery("from PmsPromotions where pmsProperty.propertyId=:propertyId"
					+ " and isActive = 'true' and isDeleted = 'false' and promotionIsActive='true' and :date between startDate and endDate order by promotionId DESC")
					.setParameter("propertyId", propertyId)
					.setParameter("date", date)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return promotionList;
	}
	@SuppressWarnings("unchecked")
	public List<PmsPromotions> listDatePromotions(int propertyId,int accommId,Date date) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		List<PmsPromotions> promotionList = null;
		try {
              
			//propertyItems = (List<PropertyItem>) session.createQuery("from PropertyItem").list();
			promotionList = (List<PmsPromotions>) session.createQuery("from PmsPromotions where pmsProperty.propertyId=:propertyId"
					+ " and isActive = 'true' and isDeleted = 'false' and promotionIsActive='true' and :date between startDate "
					+ "and endDate and propertyAccommodation.accommodationId=:accommId order by promotionId DESC")
					.setParameter("propertyId", propertyId)
					.setParameter("date", date)
					.setParameter("accommId", accommId)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return promotionList;
	}
}
