package com.ulopms.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;

import com.ulopms.model.UserAccessRight;
import com.ulopms.util.HibernateUtil;


public class UserAccessRightManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(UserAccessRightManager.class);

	public UserAccessRight add(UserAccessRight userAccessRight) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(userAccessRight);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return userAccessRight;
	}

	public UserAccessRight edit(UserAccessRight userAccessRight) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(userAccessRight);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return userAccessRight;
	}

	public UserAccessRight delete(Long id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		UserAccessRight userAccessRight = (UserAccessRight) session.load(UserAccessRight.class, id);
		if (null != userAccessRight) {
			session.delete(userAccessRight);
		}
		session.getTransaction().commit();
		return userAccessRight;
	}

	public UserAccessRight find(Long id) {
		UserAccessRight userAccessRight = new UserAccessRight();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			userAccessRight = (UserAccessRight) session.load(UserAccessRight.class, id);
			// session.getTransaction().commit();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return userAccessRight;
	}

	@SuppressWarnings("unchecked")
	public List<UserAccessRight> list() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<UserAccessRight> userAccessRights = null;
		try {

			userAccessRights = (List<UserAccessRight>) session.createQuery("from UserAccessRight").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return userAccessRights;
	}

	
}
