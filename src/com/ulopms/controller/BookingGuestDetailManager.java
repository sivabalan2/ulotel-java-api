package com.ulopms.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;
import org.hibernate.transform.Transformers;

import com.ulopms.model.BookingDetailReport;
import com.ulopms.model.BookingGuestDetail;
import com.ulopms.model.PmsBooking;
import com.ulopms.model.UserGuestDetailReport;
import com.ulopms.util.HibernateUtil;


public class BookingGuestDetailManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(BookingGuestDetailManager.class);

	public BookingGuestDetail add(BookingGuestDetail bookingGuestDetail) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(bookingGuestDetail);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return bookingGuestDetail;
	}

	public BookingGuestDetail edit(BookingGuestDetail bookingGuestDetail) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(bookingGuestDetail);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return bookingGuestDetail;
	}

	public BookingGuestDetail delete(Long id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		BookingGuestDetail bookingGuestDetail = (BookingGuestDetail) session.load(BookingGuestDetail.class, id);
		if (null != bookingGuestDetail) {
			session.delete(bookingGuestDetail);
		}
		session.getTransaction().commit();
		return bookingGuestDetail;
	}

	public BookingGuestDetail find(int id) {
		BookingGuestDetail bookingGuestDetail = new BookingGuestDetail();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			bookingGuestDetail = (BookingGuestDetail) session.load(BookingGuestDetail.class, id);
			// session.getTransaction().commit();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return bookingGuestDetail;
	}

	@SuppressWarnings("unchecked")
	public List<BookingGuestDetail> list() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<BookingGuestDetail> bookingGuestDetails = null;
		try {

			bookingGuestDetails = (List<BookingGuestDetail>) session.createQuery("from BookingGuestDetail").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return bookingGuestDetails;
	}
	
	@SuppressWarnings("unchecked")
	public List<BookingDetailReport> userBookingList(String emailId,Date todayDate) throws ParseException {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<BookingDetailReport> bookingGuestDetails = null;
//		Date date=new SimpleDateFormat("yyyy-MM-dd").parse(todayDate); 
		try {
			bookingGuestDetails = (List<BookingDetailReport>) session.createQuery("select pb.bookingId as bookingId from BookingGuestDetail bg,"
					+ " PmsGuest pg, PmsBooking pb where bg.pmsBooking.bookingId = pb.bookingId and pb.arrivalDate <=:date and pg.emailId = :emailId "
					+ "and pg.guestId = bg.pmsGuest.guestId")
					.setParameter("emailId", emailId)
					.setParameter("date", todayDate)
					.setResultTransformer(Transformers.aliasToBean(BookingDetailReport.class)).list();
			 
			//bookingGuestDetails = (List<BookingGuestDetail>) session.createQuery("from BookingGuestDetail").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return bookingGuestDetails;
	}
	
	@SuppressWarnings("unchecked")
	public List<BookingDetailReport> upcomingBookingList(String emailId,String todayDate) throws ParseException {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<BookingDetailReport> bookingGuestDetails = null;
		Date date=new SimpleDateFormat("yyyy-MM-dd").parse(todayDate); 
		try {
			bookingGuestDetails = (List<BookingDetailReport>) session.createQuery("select bg.pmsBooking.bookingId as bookingId "
					+ "from BookingGuestDetail bg, PmsGuest pg, PmsBooking pb where bg.pmsBooking.bookingId = pb.bookingId and pb.arrivalDate >:date"
					+ " and pg.emailId = :emailId and pg.guestId = bg.pmsGuest.guestId")
					.setParameter("emailId", emailId)
					.setParameter("date", date)
					.setResultTransformer(Transformers.aliasToBean(BookingDetailReport.class)).list();
			 
			//bookingGuestDetails = (List<BookingGuestDetail>) session.createQuery("from BookingGuestDetail").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return bookingGuestDetails;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<BookingGuestDetail> bookingRewardList(String emailId) throws ParseException {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<BookingGuestDetail> bookingGuestDetail = null;
		try {
			bookingGuestDetail = (List<BookingGuestDetail>) session.createQuery("select bg from BookingGuestDetail bg, PmsGuest pg, PmsBooking pb where bg.pmsBooking.bookingId = pb.bookingId"
					+ " and pg.emailId = :emailId and pg.guestId = bg.pmsGuest.guestId")
					.setParameter("emailId", emailId)
					.list();
			 
			//bookingGuestDetails = (List<BookingGuestDetail>) session.createQuery("from BookingGuestDetail").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return bookingGuestDetail;
	}
	
	@SuppressWarnings("unchecked")
	public List<BookingGuestDetail> findSmsGuestId(int bookingId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<BookingGuestDetail> bookingGuestDetails = null;
		try {
			bookingGuestDetails = (List<BookingGuestDetail>)session.createQuery("select a from BookingGuestDetail a join a.pmsBooking"
					+ " b where a.isPrimary=true and b.bookingId=:bookingId")
					.setParameter("bookingId", bookingId)
					.list();
			//screens = (List<Screen>) session.createSQLQuery(sql).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return bookingGuestDetails;
	}
	
	@SuppressWarnings("unchecked")
	public List<BookingGuestDetail> findListBookingId(int bookingId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<BookingGuestDetail> bookingGuestDetails = null;
		try {
			bookingGuestDetails = (List<BookingGuestDetail>)session.createQuery("from BookingGuestDetail where "
					+ " pmsBooking.bookingId=:bookingId and isActive = true")
					.setParameter("bookingId", bookingId)
					.list();
			//screens = (List<Screen>) session.createSQLQuery(sql).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return bookingGuestDetails;
	}

	public BookingGuestDetail findBookingId(int id) {
		BookingGuestDetail bookingGuestDetail = new BookingGuestDetail();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			bookingGuestDetail = (BookingGuestDetail) session.createQuery(" from BookingGuestDetail where "
					+ " pmsBooking.bookingId=:bookingId").setParameter("bookingId", id).list();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return bookingGuestDetail;
	}
}
