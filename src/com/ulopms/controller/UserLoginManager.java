package com.ulopms.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;
import org.hibernate.transform.Transformers;

import com.ulopms.model.CorporateEnquiry;
import com.ulopms.model.DashBoard;
import com.ulopms.model.User;
import com.ulopms.model.UserClientToken;
import com.ulopms.util.HibernateUtil;


public class UserLoginManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(UserLoginManager.class);

	public User add(User user) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(user);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return user;
	}
	
	public CorporateEnquiry add(CorporateEnquiry corporateEnquiry) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(corporateEnquiry);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return corporateEnquiry;
	}

	public UserClientToken add(UserClientToken userClientToken) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(userClientToken);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return userClientToken;
	}

	public User edit(User user) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(user);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return user;
	}
	
	public CorporateEnquiry edit(CorporateEnquiry corporateEnquiry) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(corporateEnquiry);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return corporateEnquiry;
	}
	

	public User delete(int id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		User user = (User) session.load(User.class, id);
		if (null != user) {
			session.delete(user);
		}
		session.getTransaction().commit();
		return user;
	}

	public User findUser(int id) {
		User user = new User();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			user = (User) session.get(User.class, id);
			// session.getTransaction().commit();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		// session.close();
		return user;
	}

	public User find(int id) {
		
		User user = new User();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			user = (User) session.load(User.class, id);
			// session.getTransaction().commit();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		// session.close();
		return user;
	}
	
	public CorporateEnquiry findCorporate(int id) {
		
		CorporateEnquiry user = new CorporateEnquiry();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			user = (CorporateEnquiry) session.load(CorporateEnquiry.class, id);
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return user;
	}
	
	@SuppressWarnings("unchecked")
	public List<User> list() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<User> users = null;
		try {

			users = (List<User>) session.createQuery("from User").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return users;
	}
	
	@SuppressWarnings("unchecked")
	public List<User> listBookingGuest() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<User> users = null;
		try {

			users = (List<User>)  session.createQuery("from User us where "
					+ " us.isActive = 'true' and us.isDeleted = 'false' "
					+ " and (us.role.roleId = 6 or us.role.roleId = 2)")
					.list();
		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return users;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<User> list(String roleName) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<User> users = null;
		try {

			users = (List<User>) session.createQuery("select a from User a join a.role b where b.roleId=:roleId")
					.setParameter("roleName", roleName)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return users;
	}
	
	public User findId(int id)  {
		
		
		    User user = new User();		
			Session session = HibernateUtil.getSessionFactory().getCurrentSession();
			
			try {
				session.beginTransaction();

				user = (User) session.createQuery("select userName as userName,userId as userId,emailId as emailId"
						+ " from User where userId =:id and isActive=true and isDeleted=false")
						 .setParameter("id", id)
						  .setResultTransformer(Transformers.aliasToBean(User.class)).uniqueResult();
						
			} catch (HibernateException e) {
				logger.error(e);
				session.getTransaction().rollback();
			} finally {
				session.getTransaction().commit();
				session = null;
			}
			return user;
		}
	
	@SuppressWarnings("unchecked")
	public List<User> list(short roleId, int companyId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<User> users = null;
		try {

			users = (List<User>) session.createQuery("select a from User a join a.role b join a.company c where b.roleId=:roleId and c.companyId =:companyId")
					.setParameter("roleId", roleId)
					.setParameter("companyId",companyId)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return users;
	}
	
	

	@SuppressWarnings("unchecked")
	public List<User> listAll() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<User> users = null;
		try {

			users = (List<User>) session.createQuery("from User").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return users;
	}
	
	@SuppressWarnings("unchecked")
	public List<User> listAll(int companyId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<User> users = null;
		try {

			users = (List<User>) session.createQuery("from User where company.companyId=:companyId order by createdOn desc")
					.setParameter("companyId", companyId)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return users;
	}

	
	@SuppressWarnings("unchecked")
	public List<User> verifyUser(String loginName) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<User> user = null;
		try {
			user = session
					.createQuery(
							"from User where emailId=:loginName and isActive=true and isDeleted=false and role.roleId<>4")
							.setParameter("loginName", loginName).list();
							//.setParameter("password", password).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return user;
	}

	@SuppressWarnings("unchecked")
	public List<User> verifyLogin(String loginName, String password) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<User> user = null;
		try {
			user = session
					.createQuery(
							"from User where emailId=:loginName and hashpassword=:password and isActive=true and isDeleted=false and role.roleId <> 4")
							.setParameter("loginName", loginName)
							.setParameter("password", password).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return user;
	}

	@SuppressWarnings("unchecked")
	public List<User> findUserByEmail(String email) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<User> user = null;
		try {
			user = session.createQuery("from User where emailId=:email  and isActive=true and isDeleted = false and role.roleId <> 4")
					.setParameter("email", email.trim()).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return user;
	}
	
	@SuppressWarnings("unchecked")
	public User findUserByEmail(String email,String phone) {

		User user = new User();		
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		
		try {
			session.beginTransaction();

			user = (User) session.createQuery("select userId as userId from User "
					+ "where emailId=:email or phone=:phone and isActive=true and isDeleted = false")
					.setParameter("email", email)
					.setParameter("phone", phone)
					.setResultTransformer(Transformers.aliasToBean(User.class)).uniqueResult();
					
		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return user;
	}
	
	@SuppressWarnings("unchecked")
	public User findUserEmail(String email) {

		User user = new User();		
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		
		try {
			session.beginTransaction();

			user = (User) session.createQuery("select userId as userId from User where emailId=:email and isActive=true and isDeleted = false")
					.setParameter("email", email).setMaxResults(1)
					.setResultTransformer(Transformers.aliasToBean(User.class)).uniqueResult();
					
		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return user;
	}
	
	
	public User verifyUserEmail(String loginName) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		User user = null;
		try {
			user = (User) session
					.createQuery("select userId as userId,userName as userName from User where emailId=:loginName "
							+ "and isActive=true and isDeleted=false ")
							.setParameter("loginName", loginName)
							.setResultTransformer(Transformers.aliasToBean(User.class)).uniqueResult();
							//.setParameter("password", password).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return user;
	}
	
	@SuppressWarnings("unchecked")
	public User verifyUserPhone(String phone) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		User user = null;
		try {
			user = (User) session
					.createQuery("select userId as userId,userName as userName"
							+ " from User where phone=:phone and isActive=true and isDeleted=false ")
							.setParameter("phone", phone)
							.setResultTransformer(Transformers.aliasToBean(User.class)).setMaxResults(1).uniqueResult();
							//.setParameter("password", password).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return user;
	}
	
	@SuppressWarnings("unchecked")
	public User verifyUserOtp(int userId,String loginOtp) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		User user = null;
		try {
			user = (User) session 
					.createQuery("select userId as userId,emailId as emailId,userName as userName,"
							+ "phone as phone, address1 as address1 from User where userId=:userId and loginOtp=:loginOtp "
							+ "and isActive=true and isDeleted=false ")
							.setParameter("userId", userId)
							.setParameter("loginOtp", loginOtp)
							.setResultTransformer(Transformers.aliasToBean(User.class)).uniqueResult();
							//.setParameter("password", password).list();
 
		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return user;
	}

	@SuppressWarnings("unchecked")
	public List<User> findUserByRole(int roleId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<User> user = null;
		try {
			user = session.createQuery("from User where role.roleId=:roleId and isActive=true and isDeleted = false")
					.setParameter("roleId", roleId).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return user;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<User> listUserId(int userId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<User> users = null;
		try {

			users = (List<User>) session.createQuery("from User where userId=:userId")
					.setParameter("userId", userId)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return users;
	}
	
	@SuppressWarnings("unchecked")
	public List<User> findUserByPhone(String phone) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<User> user = null;
		try {
			user = session.createQuery("from User where phone=:phone and isActive=true and isDeleted = false")
					.setParameter("phone", phone).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return user;
	}
	
	@SuppressWarnings("unchecked")
	public List<User> findUserByEmailId(String emailId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<User> user = null;
		try {
			user = session.createQuery("from User where emailId=:emailId and isActive=true and isDeleted = false")
					.setParameter("emailId", emailId).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return user;
	}
	
	@SuppressWarnings("unchecked")
	public List<CorporateEnquiry> findCorporateByEmailId(String emailId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<CorporateEnquiry> user = null;
		try {
			user = session.createQuery("from CorporateEnquiry where corporateEmail=:emailId and isActive=true and isDeleted = false")
					.setParameter("emailId", emailId).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return user;
	}
}
