

package com.ulopms.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;
import org.hibernate.transform.Transformers;

import com.ulopms.model.AccommodationRoom;
import com.ulopms.model.DashBoard;
import com.ulopms.model.PropertyAccommodation;
import com.ulopms.util.HibernateUtil;


public class PropertyAccommodationManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(PropertyAccommodationManager.class);

	public PropertyAccommodation add(PropertyAccommodation propertyAccommodation) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		//Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		try {
			session.save(propertyAccommodation);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyAccommodation;
	}

	public PropertyAccommodation edit(PropertyAccommodation propertyAccommodation) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(propertyAccommodation);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyAccommodation;
	}

	public PropertyAccommodation delete(Long id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		PropertyAccommodation propertyAccommodation = (PropertyAccommodation) session.load(PropertyAccommodation.class, id);
		if (null != propertyAccommodation) {
			session.delete(propertyAccommodation);
		}
		session.getTransaction().commit();
		return propertyAccommodation;
	}

	public PropertyAccommodation find(int id) {
		PropertyAccommodation propertyAccommodation = new PropertyAccommodation();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			propertyAccommodation = (PropertyAccommodation) session.load(PropertyAccommodation.class, id);
			// session.getTransaction().commit();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return propertyAccommodation;
	}

	@SuppressWarnings("unchecked")
	public List<PropertyAccommodation> list(int propertyId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyAccommodation> propertyAccommodations = null;
		try {

			propertyAccommodations = (List<PropertyAccommodation>) session.createQuery("from PropertyAccommodation "
					+ " where pmsProperty.propertyId=:propertyId "
					+ "and isActive = 'true' and isDeleted = 'false' order by baseAmount asc")
					.setParameter("propertyId", propertyId).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyAccommodations;
	}
	
	@SuppressWarnings("unchecked")
	public List<PropertyAccommodation> listSortByPrice(int propertyId,String sellingType) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyAccommodation> propertyAccommodations = null;
		try {
			String query=null;
			query="from PropertyAccommodation "
					+ " where pmsProperty.propertyId=:propertyId "
					+ "and isActive = 'true' and isDeleted = 'false'";
			if(sellingType.equalsIgnoreCase("weekdays")){
				query+=" order by weekdaySellingRate asc";
			}else if(sellingType.equalsIgnoreCase("weekend")){
				query+=" order by weekendSellingRate asc";
			}else if(sellingType.equalsIgnoreCase("metro")){
				query+=" order by sellingRate asc";
			}
			propertyAccommodations = (List<PropertyAccommodation>) session.createQuery(query)
					.setParameter("propertyId", propertyId).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyAccommodations;
	}
	
	@SuppressWarnings("unchecked")
	public List<PropertyAccommodation> listId(int accommodationId,int contractId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyAccommodation> propertyAccommodations = null;
		try {

			propertyAccommodations = (List<PropertyAccommodation>) session.createQuery("from PropertyAccommodation "
					+ " where propertyContractType.contractTypeId=:contractId and accommodationId =:accommodationId "
					+ "and isActive = 'true' and isDeleted = 'false'")
					.setParameter("accommodationId", accommodationId)
					.setParameter("contractId", contractId)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyAccommodations;
	}
	
	@SuppressWarnings("unchecked")
	public List<PropertyAccommodation> list(int propertyId,int accommodationId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyAccommodation> propertyAccommodations = null;
		try {

			propertyAccommodations = (List<PropertyAccommodation>) session.createQuery("from PropertyAccommodation where pmsProperty.propertyId=:propertyId "
					+ " and accommodationId=:accommodationId and isActive = 'true' and isDeleted = 'false' order by baseAmount ASC")
					.setParameter("propertyId", propertyId)
					.setParameter("accommodationId", accommodationId).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyAccommodations;
	}

	
	@SuppressWarnings("unchecked")
	public List<DashBoard> listPropertyAccommodation(int propertyId) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<DashBoard> dashDoard = null;
		try {
			

			dashDoard = (List<DashBoard>) session.createQuery("select pa.accommodationId as accommodationId,pa.accommodationType as accommodationType,"
					+ "count(par.roomId) *30  as totals from PropertyAccommodation pa, PropertyAccommodationRoom par where "
					+ "pa.pmsProperty.propertyId =:propertyId and pa.accommodationId = par.propertyAccommodation.accommodationId "
					+ "and pa.isActive = 'true' and pa.isDeleted = 'false' group by pa.accommodationId,pa.accommodationType")
           .setParameter("propertyId", propertyId)
           .setResultTransformer(Transformers.aliasToBean(DashBoard.class)).list();
           
					
		
		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return dashDoard;
	}

	@SuppressWarnings("unchecked")
	public List<AccommodationRoom> listPropertyTotalRooms(int propertyId, int accommodationId){
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<AccommodationRoom> accommodationRoom = null;
		try{
			accommodationRoom=(List<AccommodationRoom>)session.createQuery("select count(par.roomId) as roomCount,pa.accommodationId as accommodationId"
					+ " from PropertyAccommodation pa, PropertyAccommodationRoom par where pa.pmsProperty.propertyId =:propertyId "
					+ " and pa.accommodationId = par.propertyAccommodation.accommodationId and pa.isActive = 'true' and pa.isDeleted = 'false' "
					+ " and par.isActive = 'true' and par.isDeleted = 'false' and pa.accommodationId=:accommodationId group by pa.accommodationId")
					.setParameter("propertyId", propertyId)
					.setParameter("accommodationId", accommodationId)
					.setResultTransformer(Transformers.aliasToBean(AccommodationRoom.class)).list();
			
			
			
		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return accommodationRoom;
	}

	@SuppressWarnings("unchecked")
	public List<AccommodationRoom> listPropertyTotalRooms(int propertyId){
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<AccommodationRoom> accommodationRoom = null;
		try{
			accommodationRoom=(List<AccommodationRoom>)session.createQuery("select count(par.roomId) as roomCount "
					+ " from PropertyAccommodation pa, PropertyAccommodationRoom par where pa.pmsProperty.propertyId =:propertyId "
					+ " and pa.accommodationId = par.propertyAccommodation.accommodationId and pa.isActive = 'true' and pa.isDeleted = 'false' "
					+ " and par.isActive = 'true' and par.isDeleted = 'false'")
					.setParameter("propertyId", propertyId)
					.setResultTransformer(Transformers.aliasToBean(AccommodationRoom.class)).list();
			
			
			
		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return accommodationRoom;
	}
}
