package com.ulopms.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;
import org.hibernate.transform.Transformers;

import com.ulopms.model.AccommodationRoom;
import com.ulopms.model.PropertyAccommodation;
import com.ulopms.model.PropertyAccommodationRoom;
import com.ulopms.util.HibernateUtil;


public class PropertyAccommodationRoomManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(PropertyAccommodationRoomManager.class);

	public PropertyAccommodationRoom add(PropertyAccommodationRoom propertyAccommodationRoom) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		//Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		try {
			session.save(propertyAccommodationRoom);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyAccommodationRoom;
	}

	public PropertyAccommodationRoom edit(PropertyAccommodationRoom propertyAccommodationRoom) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(propertyAccommodationRoom);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyAccommodationRoom;
	}

	public PropertyAccommodationRoom delete(Long id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		PropertyAccommodationRoom propertyAccommodationRoom = (PropertyAccommodationRoom) session.load(PropertyAccommodationRoom.class, id);
		if (null != propertyAccommodationRoom) {
			session.delete(propertyAccommodationRoom);
		}
		session.getTransaction().commit();
		return propertyAccommodationRoom;
	}

	public PropertyAccommodationRoom find(Integer id) {
		PropertyAccommodationRoom propertyAccommodationRoom = new PropertyAccommodationRoom();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			propertyAccommodationRoom = (PropertyAccommodationRoom) session.load(PropertyAccommodationRoom.class, id);
			// session.getTransaction().commit();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return propertyAccommodationRoom;
	}
	
	public AccommodationRoom findCount(int propertyId, int accommodationId) {
		AccommodationRoom accommodationRoom = new AccommodationRoom();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		
		//select count(distinct room_id) from property_accommodation_rooms par ,property_accommodation pa where par.accommodation_id =16 and
        //pa.property_id = 1
        
		try {
			session.beginTransaction();
			accommodationRoom = (AccommodationRoom)session.createQuery("select count( distinct par.roomId) as roomCount "
					+ "from PropertyAccommodationRoom par,PropertyAccommodation pa  where par.propertyAccommodation.accommodationId=:accommodationId"
					+ " and pa.pmsProperty.propertyId =:propertyId and par.isActive = 'true' and par.isDeleted = 'false'")
					.setParameter("propertyId", propertyId)
					.setParameter("accommodationId", accommodationId)
					.setResultTransformer(Transformers.aliasToBean(AccommodationRoom.class))
					.uniqueResult();
			//accommodationRoom = (AccommodationRoom) session.load(AccommodationRoom.class, id);
			// session.getTransaction().commit();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return accommodationRoom;
	}
	
	@SuppressWarnings("unchecked")
	public List<PropertyAccommodationRoom> pagination(int limit,int offset) {
//		Session session = HibernateUtil.getSessionFactory().openSession();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyAccommodationRoom> propertyAccommodationRooms = null;
		try {

			propertyAccommodationRooms = (List<PropertyAccommodationRoom>) session.createQuery("from PropertyAccommodationRoom where isActive = 'true' and isDeleted = 'false'")
			//.setFetchSize(limit)
			.setFirstResult(offset)
			.setMaxResults(limit)
			.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return propertyAccommodationRooms;
	}
	
	@SuppressWarnings("unchecked")
	public List<PropertyAccommodationRoom> listCount(int propertyId) {
//		Session session = HibernateUtil.getSessionFactory().openSession();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyAccommodationRoom> propertyAccommodationRooms = null;
		try {

			propertyAccommodationRooms = (List<PropertyAccommodationRoom>) session.createQuery("select par.roomId as roomId from PropertyAccommodationRoom par,PropertyAccommodation pa where pa.pmsProperty.propertyId=:propertyId and pa.accommodationId = par.propertyAccommodation.accommodationId and par.isActive = 'true' and par.isDeleted = 'false'")
					.setParameter("propertyId", propertyId)
					.setResultTransformer(Transformers.aliasToBean(PropertyAccommodationRoom.class))
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return propertyAccommodationRooms;
	}
	
	@SuppressWarnings("unchecked")
	public List<AccommodationRoom> list(int propertyId,int limit,int offset) {
//		Session session = HibernateUtil.getSessionFactory().openSession();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<AccommodationRoom> accommodationRooms = null;		
		try {

			accommodationRooms = (List<AccommodationRoom>) session.createQuery("select par.roomId as roomId,par.roomAlias as roomAlias,"
					+ " par.sortKey as sortKey, pa.accommodationId as accommodationId ,pa.accommodationType as accommodationType"
					+ "  from PropertyAccommodationRoom par ,PropertyAccommodation pa where par.isActive = 'true' and"
					+ " par.isDeleted = 'false' and  pa.accommodationId = par.propertyAccommodation.accommodationId and"
					+ " pa.pmsProperty.propertyId = :propertyId  group by par.roomId,pa.accommodationId order by pa.accommodationId desc ")
					.setParameter("propertyId", propertyId)
					.setFirstResult(offset)
					.setMaxResults(limit)
					.setResultTransformer(Transformers.aliasToBean(AccommodationRoom.class))
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return accommodationRooms;
	}
	
	@SuppressWarnings("unchecked")
	public List<PropertyAccommodationRoom> list() {
		//Session session = HibernateUtil.getSessionFactory().openSession();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyAccommodationRoom> propertyAccommodationRooms = null;
		try {

			propertyAccommodationRooms = (List<PropertyAccommodationRoom>) session.createQuery("from PropertyAccommodationRoom where isActive = 'true' and isDeleted = 'false'").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return propertyAccommodationRooms;
	}

	@SuppressWarnings("unchecked")
	public List<AccommodationRoom> list(int propertyId) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
//		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		List<AccommodationRoom> accommodationRooms = null;		
		try {

			accommodationRooms = (List<AccommodationRoom>) session.createQuery("select par.roomId as roomId,par.roomAlias as roomAlias, par.sortKey as sortKey, pa.accommodationId as accommodationId ,pa.accommodationType as accommodationType  from PropertyAccommodationRoom par ,PropertyAccommodation pa where par.isActive = 'true' and par.isDeleted = 'false' and  pa.accommodationId = par.propertyAccommodation.accommodationId and pa.pmsProperty.propertyId = :propertyId  group by par.roomId,pa.accommodationId ")
					.setParameter("propertyId", propertyId)
					.setResultTransformer(Transformers.aliasToBean(AccommodationRoom.class))
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return accommodationRooms;
	}

	@SuppressWarnings("unchecked")
	public List<PropertyAccommodation> list(int accommodationId, int propertyId) throws ParseException {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyAccommodation> accommodationRates = null;
		try {

			accommodationRates = (List<PropertyAccommodation>) session.createQuery("from PropertyAccommodation pa where  "
					+ "pa.pmsProperty.propertyId=:propertyId and pa.accommodationId=:accommodationId")
					.setParameter("propertyId", propertyId)
					.setParameter("accommodationId", accommodationId)
					.list();
		} catch (HibernateException e) {
			logger.error(e);  
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return accommodationRates;
	}
	
	@SuppressWarnings("unchecked")
	public List<PropertyAccommodationRoom> list1(int accommodationId) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyAccommodationRoom> propertyAccommodationRooms = null;
		try {

			propertyAccommodationRooms = (List<PropertyAccommodationRoom>) session.createQuery("from PropertyAccommodationRoom par where par.propertyAccommodation.accommodationId=:accommodationId and par.isActive = 'true' and par.isDeleted = 'false'")
					.setParameter("accommodationId", accommodationId)					
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyAccommodationRooms;
	}
	
	public AccommodationRoom findRoomCount(int accommodationId) {
		AccommodationRoom accommodationRoom = new AccommodationRoom();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		
		//select count(distinct room_id) from property_accommodation_rooms par ,property_accommodation pa where par.accommodation_id =16 and
        //pa.property_id = 1
        
		try {
			session.beginTransaction();
			accommodationRoom = (AccommodationRoom)session.createQuery("select count( distinct par.roomId) as roomCount "
					+ "from PropertyAccommodationRoom par,PropertyAccommodation pa  where par.propertyAccommodation.accommodationId=:accommodationId"
					+ "  and par.isActive = 'true' and par.isDeleted = 'false'")					
					.setParameter("accommodationId", accommodationId)
					.setResultTransformer(Transformers.aliasToBean(AccommodationRoom.class))
					.uniqueResult();
			//accommodationRoom = (AccommodationRoom) session.load(AccommodationRoom.class, id);
			// session.getTransaction().commit();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return accommodationRoom;
	}
	
}
