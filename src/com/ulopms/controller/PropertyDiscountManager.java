package com.ulopms.controller;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;

import com.ulopms.model.PropertyDiscount;
import com.ulopms.util.HibernateUtil;


public class PropertyDiscountManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(PropertyDiscountManager.class);

	public PropertyDiscount add(PropertyDiscount propertyDiscount) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(propertyDiscount);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyDiscount;
	}

 	public PropertyDiscount edit(PropertyDiscount propertyDiscount) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(propertyDiscount);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		
		return propertyDiscount;
	}

 	/* public PropertyItem delete(Long id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		PropertyItem propertyItem = (PropertyItem) session.load(PropertyItem.class, id);
		if (null != propertyItem) {
			session.delete(propertyItem);
		}
		session.getTransaction().commit();
		return propertyItem;
	}   */

 	public PropertyDiscount find(int id) {
		PropertyDiscount propertyDiscount = new PropertyDiscount();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			propertyDiscount = (PropertyDiscount) session.load(PropertyDiscount.class, id);
			
		} catch (Exception e1) {
			logger.error(e1);
			e1.printStackTrace();
		} finally {
			session = null;
		}
		return propertyDiscount;
	}   

	
	@SuppressWarnings("unchecked")
	public List<PropertyDiscount> findCount(String discountName) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		List<PropertyDiscount> propertyDiscount = null;
		try {
			session.beginTransaction();
			propertyDiscount = (List<PropertyDiscount>) session.createQuery("from PropertyDiscount where discountName=:discountName "
					+ "and isActive = 'true' and isDeleted = 'false'")
			.setParameter("discountName", discountName)
			.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return propertyDiscount;
	}   
	
	@SuppressWarnings("unchecked")
	public List<PropertyDiscount> findActiveCount() {
//		Session session = HibernateUtil.getSessionFactory().openSession();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyDiscount> propertyDiscount = null;
		try {
             
			//propertyItems = (List<PropertyItem>) session.createQuery("from PropertyItem").list();
			propertyDiscount = (List<PropertyDiscount>) session.createQuery("from PropertyDiscount where isSignup='true' "
					+ "and isActive = 'true' and isDeleted = 'false'")
			//.setParameter("isSignup", isSignup)
			.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return propertyDiscount;
	}   
	

	public PropertyDiscount findName(String discountName, Timestamp endDate) {
		PropertyDiscount propertyDiscount = new PropertyDiscount();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			propertyDiscount = (PropertyDiscount) session.createQuery("from PropertyDiscount where discountName=:discountName"
					+ " and endDate<=:endDate and isActive = 'true' and isDeleted = 'false'")
					.setParameter("discountName", discountName)
					.setParameter("endDate", endDate).uniqueResult();
			// session.getTransaction().commit();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return propertyDiscount;
	}  
	

	public PropertyDiscount findId(int propertyDiscountId) {
		PropertyDiscount propertyDiscount = new PropertyDiscount();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			propertyDiscount = (PropertyDiscount) session.createQuery("from PropertyDiscount where propertyDiscountId=:propertyDiscountId"
					+ "  and isActive = 'true' and isDeleted = 'false'")
					.setParameter("propertyDiscountId", propertyDiscountId)
					.uniqueResult();
			// session.getTransaction().commit();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return propertyDiscount;
	}  
	
	@SuppressWarnings("unchecked")
	public List<PropertyDiscount> verifyDiscount(String discountName, Timestamp endDate) {
//		Session session = HibernateUtil.getSessionFactory().openSession();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyDiscount> propertyDicount = null;
		try {
              
			//propertyItems = (List<PropertyItem>) session.createQuery("from PropertyItem").list();
			propertyDicount = (List<PropertyDiscount>) session.createQuery("from PropertyDiscount where discountName=:discountName "
					+ "and endDate>=:endDate and isActive = 'true' and isDeleted = 'false'")
			.setParameter("discountName", discountName)
			.setParameter("endDate", endDate)
		    .list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return propertyDicount;
	}
	
	
	
	@SuppressWarnings("unchecked")
	public List<PropertyDiscount> currentDiscounts(Date currentDate) {
		//Session session = HibernateUtil.getSessionFactory().openSession();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyDiscount> propertyDicounts = null;
		try {
              
			//propertyItems = (List<PropertyItem>) session.createQuery("from PropertyItem").list();
			propertyDicounts = (List<PropertyDiscount>) session.createQuery("from PropertyDiscount where :date between "
					+ "startDate and endDate  and isActive = 'true' "
					+ "and isDeleted = 'false' and isResort='false' and discountType='UW'")
					//.setParameter("propertyId", propertyId)
				    .setParameter("date", currentDate)
		        	.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return propertyDicounts;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<PropertyDiscount> allCurrentDiscounts(Date currentDate) {
		//Session session = HibernateUtil.getSessionFactory().openSession();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyDiscount> propertyDicounts = null;
		try {
              
			//propertyItems = (List<PropertyItem>) session.createQuery("from PropertyItem").list();
			propertyDicounts = (List<PropertyDiscount>) session.createQuery("from PropertyDiscount where :date between "
					+ "startDate and endDate  and isActive = 'true' "
					+ "and isDeleted = 'false' and isResort='false' ")
					//.setParameter("propertyId", propertyId)
				    .setParameter("date", currentDate)
		        	.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return propertyDicounts;
	}
	
	@SuppressWarnings("unchecked")
	public List<PropertyDiscount> list(int propertyId) {
		//Session session = HibernateUtil.getSessionFactory().openSession();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyDiscount> propertyDicounts = null;
		try {
              
			//propertyItems = (List<PropertyItem>) session.createQuery("from PropertyItem").list();
			propertyDicounts = (List<PropertyDiscount>) session.createQuery("from PropertyDiscount where isActive = 'true'"
					+ " and isResort='false' and isDeleted = 'false'")
			//.setParameter("propertyId", propertyId)
			.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return propertyDicounts;
	}
  
	
	@SuppressWarnings("unchecked")
	public List<PropertyDiscount> listresort(int propertyId,String date) throws ParseException {
        Date today=new SimpleDateFormat("yyyy-MM-dd").parse(date); 
//		Session session = HibernateUtil.getSessionFactory().openSession();
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyDiscount> propertyDicounts = null;
		try {
              
			//propertyItems = (List<PropertyItem>) session.createQuery("from PropertyItem").list();
			propertyDicounts = (List<PropertyDiscount>) session.createQuery("from PropertyDiscount pd where pd.pmsProperty.propertyId =:propertyId "
					+ " and pd.isActive = 'true' and pd.isDeleted = 'false' and pd.isResort = 'true' and (:today between pd.startDate and pd.endDate)")
			.setParameter("propertyId", propertyId)
			.setParameter("today", today)
			.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return propertyDicounts;
	}
  
	@SuppressWarnings("unchecked")
	public List<PropertyDiscount> listDiscount(int propertyDiscountId) {
		//Session session = HibernateUtil.getSessionFactory().openSession();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyDiscount> propertyDiscounts = null;
		try {
              
			propertyDiscounts = (List<PropertyDiscount>) session.createQuery("from PropertyDiscount where propertyDiscountId=:propertyDiscountId "
					+ "and isActive = 'true' and isDeleted = 'false'")
			.setParameter("propertyDiscountId", propertyDiscountId)
			.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return propertyDiscounts;
	}

	@SuppressWarnings("unchecked")
	public List<PropertyDiscount> listDiscounts(Date date) throws ParseException {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyDiscount> propertyDicounts = null;
		try {
              
			//propertyItems = (List<PropertyItem>) session.createQuery("from PropertyItem").list();
			propertyDicounts = (List<PropertyDiscount>) session.createQuery("from PropertyDiscount pd where "
					+ " pd.isActive = 'true' and pd.isDeleted = 'false'  and :today between pd.startDate "
					+ " and pd.endDate and isResort='false' and discountType='UW'")
			.setParameter("today", date)
			.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return propertyDicounts;
	}
	
	@SuppressWarnings("unchecked")
	public List<PropertyDiscount> listCoupons(Date date) throws ParseException {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyDiscount> propertyDicounts = null;
		try {
              
			propertyDicounts = (List<PropertyDiscount>) session.createQuery("from PropertyDiscount pd where "
					+ " pd.isActive = 'true' and pd.isDeleted = 'false'  and :today between pd.startDate "
					+ " and pd.endDate and isResort='false' order by propertyDiscountId desc")
			.setParameter("today", date)
			.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return propertyDicounts;
	}
	
	@SuppressWarnings("unchecked")
	public List<PropertyDiscount> listPropertyCoupons(int propertyId,Date date) throws ParseException {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyDiscount> propertyDicounts = null;
		try {
              
			propertyDicounts = (List<PropertyDiscount>) session.createQuery("from PropertyDiscount pd where "
					+ " pd.isActive = 'true' and pd.isDeleted = 'false'  and pd.pmsProperty.propertyId=:propertyId and :today between pd.startDate "
					+ " and pd.endDate and isResort='false' order by propertyDiscountId desc")
			.setParameter("today", date).setParameter("propertyId", propertyId)
			.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return propertyDicounts;
	}
	
	@SuppressWarnings("unchecked")
	public List<PropertyDiscount> listPropertyCoupons(Date date) throws ParseException {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyDiscount> propertyDicounts = null;
		try {
              
			propertyDicounts = (List<PropertyDiscount>) session.createQuery("from PropertyDiscount pd where "
					+ " pd.isActive = 'true' and pd.isDeleted = 'false' and :today between pd.startDate "
					+ " and pd.endDate and isResort='false' order by propertyDiscountId desc")
			.setParameter("today", date)
			.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return propertyDicounts;
	}
	
	@SuppressWarnings("unchecked")
	public List<PropertyDiscount> verifyCoupon(String couponCode,Date date,int propertyId) throws ParseException {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyDiscount> propertyDicounts = null;
		try {
              
			propertyDicounts = (List<PropertyDiscount>) session.createQuery("from PropertyDiscount pd where "
					+ " pd.isActive = 'true' and pd.isDeleted = 'false' and :today between pd.startDate "
					+ " and pd.endDate and isResort='false' and pd.pmsProperty.propertyId=:propertyId "
					+ "and pd.discountCode=:discountCode order by propertyDiscountId desc")
			.setParameter("today", date).setParameter("propertyId", propertyId)
			.setParameter("discountCode", couponCode)
			.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return propertyDicounts;
	}
}
