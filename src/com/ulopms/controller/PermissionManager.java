package com.ulopms.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;

import com.ulopms.model.Permission;
import com.ulopms.util.HibernateUtil;


public class PermissionManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(PermissionManager.class);

	public Permission add(Permission permission) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(permission);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return permission;
	}

	public Permission edit(Permission permission) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(permission);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return permission;
	}

	public Permission delete(Long id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Permission permission = (Permission) session.load(Permission.class, id);
		if (null != permission) {
			session.delete(permission);
		}
		session.getTransaction().commit();
		return permission;
	}

	public Permission find(Long id) {
		Permission permission = new Permission();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			permission = (Permission) session.load(Permission.class, id);
			// session.getTransaction().commit();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return permission;
	}

	@SuppressWarnings("unchecked")
	public List<Permission> list() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<Permission> permissions = null;
		try {

			permissions = (List<Permission>) session.createQuery("from Permission").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return permissions;
	}

	
}
