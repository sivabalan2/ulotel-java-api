package com.ulopms.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;

import com.ulopms.model.PmsAmenity;
import com.ulopms.util.HibernateUtil;


public class PmsAmenityManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(PmsAmenityManager.class);

	public PmsAmenity add(PmsAmenity pmsAmenity) {
	    Session session = HibernateUtil.getSessionFactory().getCurrentSession();
//	    Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		try {
			session.save(pmsAmenity);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return pmsAmenity;
	}

	public PmsAmenity edit(PmsAmenity pmsAmenity) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(pmsAmenity);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return pmsAmenity;
	}

	public PmsAmenity delete(int id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		PmsAmenity pmsAmenity = (PmsAmenity) session.load(PmsAmenity.class, id);
		if (null != pmsAmenity) {
			session.delete(pmsAmenity);
		}
		session.getTransaction().commit();
		return pmsAmenity;
	}

	public PmsAmenity find(int id) {
		PmsAmenity pmsAmenity = new PmsAmenity();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			pmsAmenity = (PmsAmenity) session.get(PmsAmenity.class, id);
			// session.getTransaction().commit();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return pmsAmenity;
	}
	
	public PmsAmenity find(String id) {
		PmsAmenity pmsAmenity = new PmsAmenity();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			pmsAmenity = (PmsAmenity) session.load(PmsAmenity.class, id);
			// session.getTransaction().commit();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return pmsAmenity;
	}

	@SuppressWarnings("unchecked")
	public List<PmsAmenity> list() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsAmenity> pmsAmenitys = null;
		try {

			pmsAmenitys = (List<PmsAmenity>) session.createQuery("from PmsAmenity where"
					+ "	 isActive = 'true' and isDeleted = 'false' and amenityIsActive = 'true' "
					+ "and propertyAmenityIsActive='true' ").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return pmsAmenitys;
	}
	
	@SuppressWarnings("unchecked")
	public List<PmsAmenity> listProperty() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsAmenity> pmsAmenitys = null;
		try {

			pmsAmenitys = (List<PmsAmenity>) session.createQuery("from PmsAmenity where"
					+ "	 isActive = 'true' and isDeleted = 'false' and amenityIsActive = 'true' "
					+ "and propertyAmenityIsActive='true'").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return pmsAmenitys;
	}
	
	@SuppressWarnings("unchecked")
	public List<PmsAmenity> listAccommodation() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsAmenity> pmsAmenitys = null;
		try {

			pmsAmenitys = (List<PmsAmenity>) session.createQuery("from PmsAmenity where"
					+ "	 isActive = 'true' and isDeleted = 'false' and amenityIsActive = 'true' "
					+ "and accommodationAmenityIsActive='true' ").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return pmsAmenitys;
	}

	@SuppressWarnings("unchecked")
	public List<PmsAmenity> listTags() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsAmenity> pmsAmenitys = null;
		try {

			pmsAmenitys = (List<PmsAmenity>) session.createQuery("from PmsAmenity where"
					+ "	 isActive = 'true' and isDeleted = 'false' and amenityIsActive = 'true' "
					+ "and propertyAmenityIsActive='false' and accommodationAmenityIsActive='false' ").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return pmsAmenitys;
	}
}
