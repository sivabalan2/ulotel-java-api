package com.ulopms.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;

import com.ulopms.model.Menu;
import com.ulopms.util.HibernateUtil;


public class MenuManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(MenuManager.class);

	public Menu add(Menu menu) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(menu);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return menu;
	}

	public Menu edit(Menu menu) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(menu);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return menu;
	}

	public Menu delete(Long id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Menu menu = (Menu) session.load(Menu.class, id);
		if (null != menu) {
			session.delete(menu);
		}
		session.getTransaction().commit();
		return menu;
	}

	public Menu find(Long id) {
		Menu menu = new Menu();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			menu = (Menu) session.load(Menu.class, id);
			// session.getTransaction().commit();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return menu;
	}

	@SuppressWarnings("unchecked")
	public List<Menu> list() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<Menu> menus = null;
		try {

			menus = (List<Menu>) session.createQuery("from Menu").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return menus;
	}

	
}
