package com.ulopms.controller;


import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;
import com.ulopms.model.PmsSmartPrice;
import com.ulopms.util.HibernateUtil;


public class PmsSmartPriceManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(PmsSmartPriceManager.class);

	public PmsSmartPrice add(PmsSmartPrice pmsSmartPrice) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(pmsSmartPrice);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return pmsSmartPrice;
	}

	public PmsSmartPrice edit(PmsSmartPrice pmsSmartPrice) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(pmsSmartPrice);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return pmsSmartPrice;
	}

	public PmsSmartPrice delete(Long id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		PmsSmartPrice pmsSmartPrice = (PmsSmartPrice) session.load(PmsSmartPrice.class, id);
		if (null != pmsSmartPrice) {
			session.delete(pmsSmartPrice);
		}
		session.getTransaction().commit();
		return pmsSmartPrice;
	}

	public PmsSmartPrice find(int id) {
		PmsSmartPrice pmsSmartPrice = new PmsSmartPrice();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			pmsSmartPrice = (PmsSmartPrice) session.load(PmsSmartPrice.class, id);
			// session.getTransaction().commit();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return pmsSmartPrice;
	}

}
