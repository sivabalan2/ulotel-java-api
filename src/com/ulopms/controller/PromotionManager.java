package com.ulopms.controller;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.classic.Session;
import org.joda.time.DateTime;

import com.ulopms.model.PmsPromotions;
import com.ulopms.util.HibernateUtil;


public class PromotionManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(PromotionManager.class);

	public PmsPromotions add(PmsPromotions pmsPromotions) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(pmsPromotions);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return pmsPromotions;
	}

	public PmsPromotions edit(PmsPromotions pmsPromotions) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(pmsPromotions);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return pmsPromotions;
	}

	public PmsPromotions delete(Long id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		PmsPromotions pmsPromotions = (PmsPromotions) session.load(PmsPromotions.class, id);
		if (null != pmsPromotions) {
			session.delete(pmsPromotions);
		}
		session.getTransaction().commit();
		return pmsPromotions;
	}

	public PmsPromotions find(int id) {
		PmsPromotions pmsPromotions = new PmsPromotions();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			pmsPromotions = (PmsPromotions) session.load(PmsPromotions.class, id);
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return pmsPromotions;
	}

	@SuppressWarnings("unchecked")
	public List<PmsPromotions> list(int propertyId) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsPromotions> promotionList = null;
		try {
              
			promotionList = (List<PmsPromotions>) session.createQuery("from PmsPromotions where pmsProperty.propertyId=:propertyId"
					+ " and isActive = 'true' and isDeleted = 'false' and promotionIsActive='true' ").setParameter("propertyId", propertyId).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return promotionList;
	}
	
	@SuppressWarnings("unchecked")
	public List<PmsPromotions> listPromotions(int propertyId,Date date) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsPromotions> promotionList = null;
		try {
              
			promotionList = (List<PmsPromotions>) session.createQuery("from PmsPromotions where pmsProperty.propertyId=:propertyId"
					+ " and :date between startDate and endDate and isActive = 'true' and isDeleted = 'false' and promotionIsActive='true' ")
					.setParameter("propertyId", propertyId)
					.setParameter("date", date)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return promotionList;
	}
	@SuppressWarnings("unchecked")
	public List<PmsPromotions> listActive(int propertyId) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsPromotions> promotionList = null;
		try {
              
			promotionList = (List<PmsPromotions>) session.createQuery("from PmsPromotions where pmsProperty.propertyId=:propertyId"
					+ " and isActive = 'true' and isDeleted = 'false'").setParameter("propertyId", propertyId).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return promotionList;
	}
	
	@SuppressWarnings("unchecked")
	public List<PmsPromotions> listPromotions(int propertyId) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsPromotions> promotionList = null;
		try {
			promotionList = (List<PmsPromotions>) session.createQuery("from PmsPromotions where pmsProperty.propertyId=:propertyId"
					+ " and isActive = 'true' and isDeleted = 'false' and promotionIsActive='true' ")
					.setParameter("propertyId", propertyId)
					
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return promotionList;
	}
	
	@SuppressWarnings("unchecked")
	public List<PmsPromotions> listPromotions(int propertyId,Date date,String strPromotionType) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsPromotions> promotionList = null;
		try {
			promotionList = (List<PmsPromotions>) session.createQuery("from PmsPromotions where pmsProperty.propertyId=:propertyId and promotionType=:promotionType "
					+ " and :date between startDate and endDate and isActive = 'true' and isDeleted = 'false' and promotionIsActive='true' ")
					.setParameter("propertyId", propertyId)
					.setParameter("promotionType", strPromotionType)
					.setParameter("date", date)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return promotionList;
	}
	
	@SuppressWarnings("unchecked")
	public List<PmsPromotions> listPromotions(int propertyId,Date date,int accommodationId) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsPromotions> promotionList = null;
		try {
              
			promotionList = (List<PmsPromotions>) session.createQuery("from PmsPromotions where pmsProperty.propertyId=:propertyId "
					+ " and :date between startDate and endDate and isActive = 'true' and isDeleted = 'false' and promotionIsActive='true' "
					+ " and propertyAccommodation.accommodationId=:accommodationId")
					.setParameter("propertyId", propertyId)
					.setParameter("accommodationId", accommodationId)
					.setParameter("date", date)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return promotionList;
	}
	
	@SuppressWarnings("unchecked")
	public List<PmsPromotions> listPromotions(int propertyId,Date date,String strPromotionType,int accommodationId) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsPromotions> promotionList = null;
		try {
              
			promotionList = (List<PmsPromotions>) session.createQuery("from PmsPromotions where pmsProperty.propertyId=:propertyId and promotionType=:promotionType "
					+ " and :date between startDate and endDate and isActive = 'true' and isDeleted = 'false' and promotionIsActive='true' "
					+ " propertyAccommodation.accommodationId=:accommodationId")
					.setParameter("propertyId", propertyId)
					.setParameter("accommodationId", accommodationId)
					.setParameter("promotionType", strPromotionType)
					.setParameter("date", date)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return promotionList;
	}
	@SuppressWarnings("unchecked")
	public List<PmsPromotions> listPromotionsPerAccommodation(int accommodationId,Date date) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsPromotions> promotionList = null;
		try {
              
			promotionList = (List<PmsPromotions>) session.createQuery("from PmsPromotions where propertyAccommodation.accommodationId=:accommodationId "
					+ " and :date between startDate and endDate and isActive = 'true' and isDeleted = 'false' and promotionIsActive='true' ")
					.setParameter("accommodationId", accommodationId)
					.setParameter("date", date)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return promotionList;
	}
	
	@SuppressWarnings("unchecked")
	public List<PmsPromotions> listUpdatePromotions(int promotionId) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsPromotions> promotionList = null;
		try {
              
			promotionList = (List<PmsPromotions>) session.createQuery("from PmsPromotions where promotionId=:promotionId"
					+ " and isActive = 'true' and isDeleted = 'false' ").setParameter("promotionId", promotionId).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return promotionList;
	}
	
	@SuppressWarnings("unchecked")
	public List<PmsPromotions> listPromotionsDetails(int propertyId,Date date) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsPromotions> promotionList = null;
		try {
              
			promotionList = (List<PmsPromotions>) session.createQuery("from PmsPromotions where pmsProperty.propertyId=:propertyId"
					+ " and isActive = 'true' and isDeleted = 'false' and (startDate>=:startDate or endDate>=:endDate) order by promotionId DESC")
					.setParameter("propertyId", propertyId)
					.setParameter("startDate", date)
					.setParameter("endDate", date)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return promotionList;
	}
	
	@SuppressWarnings("unchecked")
	public List<PmsPromotions> listDatePromotions(int propertyId,Date start,Date end) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsPromotions> promotionList = null;
		try {
              
			promotionList = (List<PmsPromotions>) session.createQuery("from PmsPromotions where pmsProperty.propertyId=:propertyId"
					+ " and isActive = 'true' and isDeleted = 'false' and promotionIsActive='true'"
					+ " and startDate=:startDate and endDate=:endDate order by promotionId DESC")
					.setParameter("propertyId", propertyId)
					.setParameter("startDate", start)
					.setParameter("endDate", end)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return promotionList;
	}
	
	@SuppressWarnings("unchecked")
	public List<PmsPromotions> listDatePromotions(int propertyId,Date date) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsPromotions> promotionList = null;
		try {
			promotionList = (List<PmsPromotions>) session.createQuery("from PmsPromotions where pmsProperty.propertyId=:propertyId"
					+ " and isActive = 'true' and isDeleted = 'false' and promotionIsActive='true' "
					+ "and :date between startDate and endDate order by promotionId DESC")
					.setParameter("propertyId", propertyId)
					.setParameter("date", date)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return promotionList;
	}
	@SuppressWarnings("unchecked")
	public List<PmsPromotions> listDatePromotions(int propertyId,int accommId,Date date) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		List<PmsPromotions> promotionList = null;
		try {
              
			promotionList = (List<PmsPromotions>) session.createQuery("from PmsPromotions where pmsProperty.propertyId=:propertyId"
					+ " and isActive = 'true' and isDeleted = 'false' and promotionIsActive='true' and :date between startDate "
					+ "and endDate and propertyAccommodation.accommodationId=:accommId order by promotionId DESC")
					.setParameter("propertyId", propertyId)
					.setParameter("date", date)
					.setParameter("accommId", accommId)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return promotionList;
	}
	
	@SuppressWarnings("unchecked")
	public List<PmsPromotions> listPromotions() {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsPromotions> promotionList = null;
		try {
              
			promotionList = (List<PmsPromotions>) session.createQuery("from PmsPromotions where "
					+ " isActive = 'true' and isDeleted = 'false' and promotionIsActive='true' ").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return promotionList;
	}
	
	@SuppressWarnings("unchecked")
	public List<PmsPromotions> listPromotionsDetails(Date date) {
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsPromotions> promotionList = null;
		try {
              
			promotionList = (List<PmsPromotions>) session.createQuery("from PmsPromotions where "
					+ " isActive = 'true' and isDeleted = 'false' and  :date between startDate and endDate order by promotionId DESC")
					.setParameter("date", date).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return promotionList;
	}
	
	@SuppressWarnings("unchecked")
	public List<PmsPromotions> listDatePromotions(int propertyId,int accommId,Date date,String hoursRange) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsPromotions> promotionList = null;
		try {
              
			promotionList = (List<PmsPromotions>) session.createQuery("from PmsPromotions where pmsProperty.propertyId=:propertyId"
					+ " and isActive = 'true' and isDeleted = 'false' and promotionIsActive='true' and :date between startDate "
					+ "and endDate and propertyAccommodation.accommodationId=:accommId and promotionHoursRange=:hoursRange order by promotionId DESC")
					.setParameter("propertyId", propertyId)
					.setParameter("date", date)
					.setParameter("accommId", accommId)
					.setParameter("hoursRange", hoursRange)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return promotionList;
	}
	
	@SuppressWarnings("unchecked")
	public List<PmsPromotions> listBookedDatePromotions(int propertyId,Date date) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsPromotions> promotionList = null;
		try {
              
			promotionList = (List<PmsPromotions>) session.createQuery("from PmsPromotions where pmsProperty.propertyId=:propertyId"
					+ " and isActive = 'true' and isDeleted = 'false' and promotionIsActive='true' and promotionType='E' "
					+ "and promotionBookedDate=:date order by promotionId DESC")
					.setParameter("propertyId", propertyId)
					.setParameter("date", date)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return promotionList;
	}
	
	@SuppressWarnings("unchecked")
	public List<PmsPromotions> listLastBookedPromotions(int propertyId,Date date) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsPromotions> promotionList = null;
		try {
              
			promotionList = (List<PmsPromotions>) session.createQuery("from PmsPromotions where pmsProperty.propertyId=:propertyId"
					+ " and isActive = 'true' and isDeleted = 'false' and promotionIsActive='true' and promotionType='L' "
					+ " and :date between firstRangeStartDate and secondRangeEndDate order by promotionId DESC")
					.setParameter("propertyId", propertyId)
					.setParameter("date", date)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return promotionList;
	}
	
	@SuppressWarnings("unchecked")
	public List<PmsPromotions> listLastDatePromotions(int propertyId,int accommId,Date date) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsPromotions> promotionList = null;
		try {
              
			promotionList = (List<PmsPromotions>) session.createQuery("from PmsPromotions where pmsProperty.propertyId=:propertyId"
					+ " and isActive = 'true' and isDeleted = 'false' and promotionIsActive='true' and"
					+ " :date between firstRangeStartDate and secondRangeEndDate and propertyAccommodation.accommodationId=:accommId order by promotionId DESC")
					.setParameter("propertyId", propertyId)
					.setParameter("date", date)
					.setParameter("accommId", accommId)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return promotionList;
	}
	
	@SuppressWarnings("unchecked")
	public List<PmsPromotions> listLastPromotions(int promotionId) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsPromotions> promotionList = null;
		try {
              
			promotionList = (List<PmsPromotions>) session.createQuery("from PmsPromotions where promotionId=:promotionId"
					+ " and isActive = 'true' and isDeleted = 'false' and promotionIsActive='true' ")
					.setParameter("promotionId", promotionId)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return promotionList;
	}
	
	@SuppressWarnings("unchecked")
	public List<PmsPromotions> listFlatDatePromotions(int propertyId,Date date) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PmsPromotions> promotionList = null;
		try {
              
			promotionList = (List<PmsPromotions>) session.createQuery("from PmsPromotions where pmsProperty.propertyId=:propertyId"
					+ " and isActive = 'true' and isDeleted = 'false' and promotionIsActive='true' and promotionType='F' "
					+ " and :date between startDate and endDate order by promotionId DESC")
					.setParameter("propertyId", propertyId)
					.setParameter("date", date)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return promotionList;
	}
	
	
}
