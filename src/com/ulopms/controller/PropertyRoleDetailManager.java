package com.ulopms.controller;


import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;

import com.ulopms.model.PmsProperty;
import com.ulopms.model.PropertyRatePlan;
import com.ulopms.model.PmsSmartPrice;
import com.ulopms.model.PropertyRate;
import com.ulopms.model.PropertyRoleDetails;
import com.ulopms.model.PropertyRoles;
import com.ulopms.util.HibernateUtil;


public class PropertyRoleDetailManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(PropertyRoleDetailManager.class);

	public PropertyRoleDetails add(PropertyRoleDetails propertyRoleDetails) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(propertyRoleDetails);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyRoleDetails;
	}

	public PropertyRoleDetails edit(PropertyRoleDetails propertyRoleDetails) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(propertyRoleDetails);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return propertyRoleDetails;
	}

	public PropertyRoleDetails delete(Long id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		PropertyRoleDetails propertyRoleDetails = (PropertyRoleDetails) session.load(PropertyRoleDetails.class, id);
		if (null != propertyRoleDetails) {
			session.delete(propertyRoleDetails);
		}
		session.getTransaction().commit();
		return propertyRoleDetails;
	}

	public PropertyRoleDetails find(int id) {
		PropertyRoleDetails propertyRoleDetails = new PropertyRoleDetails();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			propertyRoleDetails = (PropertyRoleDetails) session.load(PropertyRoleDetails.class, id);
			
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return propertyRoleDetails;
	}

	@SuppressWarnings("unchecked")
	public List<PropertyRoleDetails> list() {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyRoleDetails> roleDetails = null;
		try {
              
			roleDetails = (List<PropertyRoleDetails>) session.createQuery("from PropertyRoleDetails where "
					+ " isActive = 'true' and isDeleted = 'false'").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return roleDetails;
	}
	
	@SuppressWarnings("unchecked")
	public List<PropertyRoleDetails> list(int propertyId) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyRoleDetails> roleDetails = null;
		try {
              
			roleDetails = (List<PropertyRoleDetails>) session.createQuery("from PropertyRoleDetails where "
					+ " isActive = 'true' and isDeleted = 'false' and pmsProperty.propertyId=:propertyId ")
					.setParameter("propertyId", propertyId).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return roleDetails;
	}
	
	
}
