package com.ulopms.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;
import org.hibernate.transform.Transformers;

import com.ulopms.model.Area;
import com.ulopms.model.BookingDetailReport;
import com.ulopms.util.HibernateUtil;


public class AreaManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(AreaManager.class);

	public Area add(Area area) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(area);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return area;
	}

	public Area edit(Area area) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(area);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return area;
	}

	public Area delete(int id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Area area = (Area) session.load(Area.class, id);
		if (null != area) {
			session.delete(area);
		}
		session.getTransaction().commit();
		return area;
	}

	public Area find(int id) {
		Area area = new Area();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			area = (Area) session.load(Area.class, id);
			
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return area;
	}

	@SuppressWarnings("unchecked")
	public List<Area> list() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<Area> area = null;
		try {

			area = (List<Area>) session.createQuery("from Area where isActive = 'true'"
					+ " and isDeleted = 'false' ").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return area;
	}
	
	@SuppressWarnings("unchecked")
	public List<Area> inactiveList() {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<Area> area = null;
		try {

			area = (List<Area>) session.createQuery("from Area where isActive = 'false'"
					+ " and isDeleted = 'false' ").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return area;
	}
	
	@SuppressWarnings("unchecked")
	public List<Area> listLocationArea(int locationId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<Area> area = null;
		try {

			area = (List<Area>) session.createQuery("from Area where isActive = 'true'"
					+ " and isDeleted = 'false' and location.locationId=:locationId ").setParameter("locationId", locationId).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return area;
	}
	

	@SuppressWarnings("unchecked")
	public List<Area> list(int limit,int offset) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<Area> area = null;
		try {

			area = (List<Area>) session.createQuery("from Area where isActive = 'true' "
					+ "and isDeleted = 'false' ")
					.setFirstResult(offset)
					.setMaxResults(limit)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return area;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<Area> inactiveList(int limit,int offset) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<Area> area = null;
		try {

			area = (List<Area>) session.createQuery("from Area where isActive = 'false' "
					+ "and isDeleted = 'false' ")
					.setFirstResult(offset)
					.setMaxResults(limit)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return area;
	}
	
	@SuppressWarnings("unchecked")
	public List<Area> list(int areaId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<Area> area = null;
		try {

			area = (List<Area>) session.createQuery("from Area where areaId=:areaId and isActive = 'true' and isDeleted = 'false'")
			.setParameter("areaId", areaId).list();
			
			

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return area;
	}
	
	@SuppressWarnings("unchecked")
	public List<Area> inactiveList(int areaId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<Area> area = null;
		try {

			area = (List<Area>) session.createQuery("from Area where areaId=:areaId and isActive = 'false' and isDeleted = 'false'")
			.setParameter("areaId", areaId).list();
			
			

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return area;
	}
	
	
	
	@SuppressWarnings("unchecked")
	public List<Area> listAreaByLocation(int locationId) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<Area> area = null;
		try {

			area = (List<Area>) session.createQuery("select a from Area a where a.location.locationId=:locationId "
					+ "and a.isActive = 'true' and a.isDeleted = 'false'")
			.setParameter("locationId", locationId).list();
			
			

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return area;
	}
	
	

	@SuppressWarnings("unchecked")
	public List<Area> listAreaUrl(String areaUrl) {

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<Area> area = null;
		try {

			area = (List<Area>) session.createQuery("select a from Area a where areaUrl=:areaUrl "
					+ "and a.isActive = 'true' and a.isDeleted = 'false'")
			.setParameter("areaUrl", areaUrl).list();
			
			

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return area;
	}
	
	@SuppressWarnings("unchecked")
	public List<Area> listInactiveAreaUrl(String areaUrl) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<Area> area = null;
		try {

			area = (List<Area>) session.createQuery("select a from Area a where areaUrl=:areaUrl "
					+ "and a.isActive = 'false' and a.isDeleted = 'false'")
			.setParameter("areaUrl", areaUrl).list();
			
			

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return area;
	}
	
	@SuppressWarnings("unchecked")
	public List<BookingDetailReport> listAreaProperty(int areaId){

		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<BookingDetailReport> areaProperty = null;
		try {

			areaProperty = (List<BookingDetailReport>) session.createQuery("select pp.propertyId as propertyId,pp.propertyName as propertyName from PmsProperty pp,"
					+ "Area a,PropertyAreas pa where a.areaId=pa.area.areaId and pp.propertyId=pa.pmsProperty.propertyId and a.isActive='true' and a.isDeleted='false' "
					+ "and pa.isActive='true' and pa.isDeleted='false' and pp.isActive='true' and pp.isDeleted='false' and a.areaId=:areaId "
					+ " and pp.hotelIsActive='true' and pp.propertyIsControl='true'")
			.setParameter("areaId", areaId).setResultTransformer(Transformers.aliasToBean(BookingDetailReport.class)).list();
			
			

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return areaProperty;
	}
	
}
