package com.ulopms.controller;


import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;

import com.ulopms.model.PmsProperty;
import com.ulopms.model.PropertyContractType;
import com.ulopms.model.PropertyRatePlan;
import com.ulopms.model.PmsSmartPrice;
import com.ulopms.model.PropertyRate;
import com.ulopms.model.PropertyRoles;
import com.ulopms.util.HibernateUtil;


public class PropertyContractTypeManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(PropertyContractTypeManager.class);

	public PropertyContractType add(PropertyContractType contractType) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(contractType);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return contractType;
	}

	public PropertyContractType edit(PropertyContractType contractType) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(contractType);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return contractType;
	}

	public PropertyContractType delete(Long id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		PropertyContractType contractType = (PropertyContractType) session.load(PropertyContractType.class, id);
		if (null != contractType) {
			session.delete(contractType);
		}
		session.getTransaction().commit();
		return contractType;
	}

	public PropertyContractType find(int id) {
		PropertyContractType contractType = new PropertyContractType();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			contractType = (PropertyContractType) session.load(PropertyContractType.class, id);
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return contractType;
	}

	@SuppressWarnings("unchecked")
	public List<PropertyContractType> list() {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyContractType> contractType = null;
		try {
              
			contractType = (List<PropertyContractType>) session.createQuery("from PropertyContractType where "
					+ " isActive = 'true' and isDeleted = 'false'").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return contractType;
	}
	
	@SuppressWarnings("unchecked")
	public List<PropertyContractType> listContract() {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyContractType> contractType = null;
		try {
              
			contractType = (List<PropertyContractType>) session.createQuery("from PropertyContractType where "
					+ " isActive = 'true' and isDeleted = 'false' and typeIsActive = 'true'").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return contractType;
	}
	
}
