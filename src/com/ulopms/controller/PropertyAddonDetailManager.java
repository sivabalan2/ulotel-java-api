package com.ulopms.controller;


import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;

import com.ulopms.model.PmsProperty;
import com.ulopms.model.PropertyAddonDetails;
import com.ulopms.model.PropertyRatePlan;
import com.ulopms.model.PmsSmartPrice;
import com.ulopms.model.PropertyRate;
import com.ulopms.model.PropertyRoleDetails;
import com.ulopms.model.PropertyRoles;
import com.ulopms.util.HibernateUtil;


public class PropertyAddonDetailManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(PropertyAddonDetailManager.class);

	public PropertyAddonDetails add(PropertyAddonDetails addonDetails) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(addonDetails);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return addonDetails;
	}

	public PropertyAddonDetails edit(PropertyAddonDetails addonDetails) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(addonDetails);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return addonDetails;
	}

	public PropertyAddonDetails delete(Long id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		PropertyAddonDetails addonDetails = (PropertyAddonDetails) session.load(PropertyAddonDetails.class, id);
		if (null != addonDetails) {
			session.delete(addonDetails);
		}
		session.getTransaction().commit();
		return addonDetails;
	}

	public PropertyAddonDetails find(int id) {
		PropertyAddonDetails addonDetails = new PropertyAddonDetails();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			addonDetails = (PropertyAddonDetails) session.load(PropertyAddonDetails.class, id);
			
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return addonDetails;
	}

	@SuppressWarnings("unchecked")
	public List<PropertyAddonDetails> list() {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyAddonDetails> addonDetails = null;
		try {
              
			addonDetails = (List<PropertyAddonDetails>) session.createQuery("from PropertyAddonDetails where "
					+ " isActive = 'true' and isDeleted = 'false'").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return addonDetails;
	}
	
	@SuppressWarnings("unchecked")
	public List<PropertyAddonDetails> list(int propertyId) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyAddonDetails> addonDetails = null;
		try {
              
			addonDetails = (List<PropertyAddonDetails>) session.createQuery("from PropertyAddonDetails where "
					+ " isActive = 'true' and isDeleted = 'false' and pmsProperty.propertyId=:propertyId ")
					.setParameter("propertyId", propertyId).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return addonDetails;
	}
	
	@SuppressWarnings("unchecked")
	public List<PropertyAddonDetails> propertyAddonList(int propertyId) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyAddonDetails> addonDetails = null;
		try {
              
			addonDetails = (List<PropertyAddonDetails>) session.createQuery("from PropertyAddonDetails where "
					+ " isActive = 'true' and isDeleted = 'false' and addonIsActive = 'true' and pmsProperty.propertyId=:propertyId")
					.setParameter("propertyId", propertyId).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return addonDetails;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<PropertyAddonDetails> list(int addonId,int propertyId) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyAddonDetails> addonDetails = null;
		try {
              
			addonDetails = (List<PropertyAddonDetails>) session.createQuery("from PropertyAddonDetails where "
					+ " isActive = 'true' and isDeleted = 'false' and pmsProperty.propertyId=:propertyId and propertyAddon.propertyAddonId =:propertyAddonId")
					.setParameter("propertyId", propertyId)
					.setParameter("propertyAddonId", addonId).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return addonDetails;
	}
	
}
