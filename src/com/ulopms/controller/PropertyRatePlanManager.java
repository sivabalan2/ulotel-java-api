package com.ulopms.controller;


import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.classic.Session;

import com.ulopms.model.PmsProperty;
import com.ulopms.model.PropertyRatePlan;
import com.ulopms.model.PmsSmartPrice;
import com.ulopms.model.PropertyRate;
import com.ulopms.util.HibernateUtil;


public class PropertyRatePlanManager extends HibernateUtil {

	private static final Logger logger = Logger.getLogger(PropertyRatePlanManager.class);

	public PropertyRatePlan add(PropertyRatePlan pmsRatePlan) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.save(pmsRatePlan);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return pmsRatePlan;
	}

	public PropertyRatePlan edit(PropertyRatePlan pmsRatePlan) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		try {
			session.saveOrUpdate(pmsRatePlan);
		} catch (HibernateException e1) {
			logger.error(e1);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return pmsRatePlan;
	}

	public PropertyRatePlan delete(Long id) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		PropertyRatePlan pmsRatePlan = (PropertyRatePlan) session.load(PropertyRatePlan.class, id);
		if (null != pmsRatePlan) {
			session.delete(pmsRatePlan);
		}
		session.getTransaction().commit();
		return pmsRatePlan;
	}

	public PropertyRatePlan find(int id) {
		PropertyRatePlan pmsRatePlan = new PropertyRatePlan();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try {
			session.beginTransaction();
			pmsRatePlan = (PropertyRatePlan) session.load(PropertyRatePlan.class, id);
			// session.getTransaction().commit();
		} catch (Exception e1) {
			logger.error(e1);
		} finally {
			session = null;
		}
		return pmsRatePlan;
	}

	@SuppressWarnings("unchecked")
	public List<PropertyRatePlan> list() {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyRatePlan> ratePlan = null;
		try {
              
			ratePlan = (List<PropertyRatePlan>) session.createQuery("from PropertyRatePlan where "
					+ " isActive = 'true' and isDeleted = 'false'").list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return ratePlan;
	}
	
	@SuppressWarnings("unchecked")
	public List<PropertyRatePlan> list(int ratePlanId) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyRatePlan> ratePlan = null;
		try {
              
			ratePlan = (List<PropertyRatePlan>) session.createQuery("from PropertyRatePlan where "
					+ " isActive = 'true' and isDeleted = 'false' and ratePlanId=:ratePlanId")
					.setParameter("ratePlanId", ratePlanId).list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session=null;
		}
		return ratePlan;
	}
	
	@SuppressWarnings("unchecked")
	public List<PropertyRatePlan> list(int limit,int offset) {
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		List<PropertyRatePlan> rateplan = null;
		try {

			rateplan = (List<PropertyRatePlan>) session.createQuery("from PropertyRatePlan where isActive = 'true' "
					+ "and isDeleted = 'false' ")
					.setFirstResult(offset)
					.setMaxResults(limit)
					.list();

		} catch (HibernateException e) {
			logger.error(e);
			session.getTransaction().rollback();
		} finally {
			session.getTransaction().commit();
			session = null;
		}
		return rateplan;
	}
	
}
