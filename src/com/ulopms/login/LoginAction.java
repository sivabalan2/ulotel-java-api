package com.ulopms.login;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.apache.catalina.startup.UserConfig;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.SessionAware;

import java.util.Map;  
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.dispatcher.SessionMap;  

//import com.medwecare.controller.AccessRightsManager;
//import com.medwecare.controller.AwsAPIManager;
//import com.medwecare.controller.CompanyManager;
//import com.medwecare.controller.RoleManager;

//import com.medwecare.controller.UserRightsManager;







import org.hibernate.Hibernate;

import antlr.StringUtils;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.ulopms.api.Login;
import com.ulopms.api.Search;
import com.ulopms.controller.AccessRightManager;
import com.ulopms.controller.PmsGuestManager;
import com.ulopms.controller.PmsPropertyManager;
import com.ulopms.controller.RoleAccessRightManager;
import com.ulopms.controller.RoleManager;
import com.ulopms.controller.UserLoginManager;
import com.ulopms.model.AccessRight;
import com.ulopms.model.PmsBooking;
import com.ulopms.model.PmsGuest;
import com.ulopms.model.PmsProperty;
import com.ulopms.model.Role;
import com.ulopms.model.RoleAccessRight;
import com.ulopms.model.User;
import com.ulopms.model.UserClientToken;
import com.ulopms.security.passwordDecrypt;
import com.ulopms.util.Email;
import com.ulopms.view.ForgetPasswordAction;
import com.ulopms.view.UserAddAction;

import freemarker.template.Configuration;
import freemarker.template.Template;

public class LoginAction extends ActionSupport implements SessionAware, ModelDriven<User> {
	private String username;
	private String password;
	private String userid;
	private String emailid;
	private String mobileNumber;
    private UserLoginManager linkController = new UserLoginManager();
	private List<User> get_users;
	private List<User> users;
	private List<RoleAccessRight> roleAccessList;
	private User user;
	private String loginOtp;
	private int userId;
	List<PmsProperty> propertyList=null;
	private HttpSession session;
	
	private static final Logger logger = Logger.getLogger(LoginAction.class);
	
	
	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}

	@Override
    public User getModel() {
        return user;
    }
	private SessionMap<String,Object> sessionMap;  
	  
	//getters and setters  
	  
	@Override  
	public void setSession(Map<String, Object> map) {  
	    sessionMap=(SessionMap)map;  
	}  
	
	
	public String execute() {

		String ret = "input";
		try
		{
			int count=0;
			if(ActionContext.getContext().getSession().get("countcap")!=null)
			{
				if(ActionContext.getContext().getSession().get("countcap")!="0")
				{
					count=(int) ActionContext.getContext().getSession().get("countcap");
				}
				
			}
			count=Integer.parseInt(String.valueOf((sessionMap.get("countcap")!=null?sessionMap.get("countcap"):"0")));
			RoleManager roleController = new RoleManager();
			AccessRightManager accessRightController = new AccessRightManager();
			get_users=linkController.verifyUser(username);
			if(get_users.size()>0)
			{
				String pass=passwordDecrypt.getDigestvalid(password, get_users.get(0).getSaltkey());
				 
				users =  linkController.verifyLogin(username, pass);
				String roleName="NA";
				if(users.size()>0){
					user = users.get(0);
					int roleId=user.getRole().getRoleId();
					Role role1=roleController.find(roleId);
					roleName=role1.getRoleName();
				}
				
				if(roleName.equalsIgnoreCase("FOM") || roleName.equalsIgnoreCase("NA")){
					addActionError(getText("error.invalid.login"));
					ActionContext.getContext().getSession().put("countcap", ++count);
					ret= "error";
				}else{
					if(users.size()>0)
					{
						 count=0;
						user = users.get(0);
						User user1 =linkController.findUser(user.getUserId());
						String userPermissions ="";
						/*List<UserAccessRights> userAccessRightsList =  userRightsController.list(user.getUserId());
						for(UserAccessRights userAccessRights : userAccessRightsList)
						{
							AccessRights accessRights =  accessRightsController.find(userAccessRights.getAccessRights().getAccessRightsId());
							if(!userPermissions.equalsIgnoreCase(""))
								userPermissions += "*" +accessRights.getAccessRights();
							else
								userPermissions = accessRights.getAccessRights();
						}
						
						
						*/
						//short r = 4;
						Role role =  roleController.find(user1.getRole().getRoleId());
						sessionMap.put("loginName",user.getUserName());
						sessionMap.put("userId",user.getUserId());
						sessionMap.put("emailId",user.getEmailId());
						sessionMap.put("role",role.getRoleName());
						sessionMap.put("userName",user.getUserName());
						sessionMap.put("userStatus","1");
						sessionMap.put("USER", user);
						sessionMap.put("countcap", count);
						//role id based access rights with true, comma separated
						RoleAccessRightManager roleAccessController = new RoleAccessRightManager();
						
						this.roleAccessList = roleAccessController.list(user1.getRole().getRoleId());
						StringBuilder displayNames = new StringBuilder();
						for (RoleAccessRight roleAccessRight : roleAccessList) {
						//Hibernate.initialize(roleAccessRight.getAccessRight());
						AccessRight accessRight = accessRightController.find(roleAccessRight.getAccessRight().getAccessRightsId());
						accessRight.getDisplayName();
						displayNames.append(accessRight.getDisplayName());
						displayNames.append(",");
						
						}
						
						sessionMap.put("userRights", displayNames);
						
						ret= "success";
					} else {
					
						if(getUsername()!=null && getPassword()!=null){
							addActionError(getText("error.login"));
							ActionContext.getContext().getSession().put("countcap", ++count);
							ret= "error";	
						}
					}	
				}
				
			}else {
			
				if(getUsername()!=null && getPassword()!=null){
					addActionError(getText("error.login"));
					ActionContext.getContext().getSession().put("countcap", ++count);
					ret= "error";	
				}
				
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return ret;
	}
	
	public String partnerLogin(){
		String ret = "error";
		try{
			Map mapsession = null;
			PmsPropertyManager propertyController=new PmsPropertyManager();
			int count=0;
			if(ActionContext.getContext().getSession().get("countcap")!=null)
			{
				if(ActionContext.getContext().getSession().get("countcap")!="0")
				{
					count=(int) ActionContext.getContext().getSession().get("countcap");
				}
				
			}
			count=Integer.parseInt(String.valueOf((sessionMap.get("countcap")!=null?sessionMap.get("countcap"):"0")));
			RoleManager roleController = new RoleManager();
			AccessRightManager accessRightController = new AccessRightManager();
			Boolean status=false;
			if(username==null){
				mapsession = (Map) ActionContext.getContext().getSession(); // it returns null
				status=(Boolean)mapsession.get("logined");
				if(status==null){
					status=true;
					ret= "error";
				}else{
					status=true;
					ret= "success";
				}
				
			}
			if(!status){
				get_users=linkController.verifyUser(username);
				if(get_users.size()>0)
				{
					String pass=passwordDecrypt.getDigestvalid(password, get_users.get(0).getSaltkey());
					users =  linkController.verifyLogin(username, pass);
					String roleName="NA";
					if(users.size()>0){
						user = users.get(0);
						int roleId=user.getRole().getRoleId();
						Role role1=roleController.find(roleId);
						roleName=role1.getRoleName();
					}
					mapsession = ActionContext.getContext().getSession();
	                mapsession.put("logined",true);
					if(roleName.equalsIgnoreCase("FOM") || roleName.equalsIgnoreCase("admin")){
						if(users.size()>0)
						{
							count=0;
							user = users.get(0);
							User user1 =linkController.findUser(user.getUserId());
							String userPermissions ="";
							
							Role role =  roleController.find(user1.getRole().getRoleId());
							sessionMap.put("partnerLoginName",user.getUserName());
							sessionMap.put("partnerUserId",user.getUserId());
							sessionMap.put("partnerEmailId",user.getEmailId());
							sessionMap.put("partnerRole",role.getRoleName());
							sessionMap.put("partnerUserName",user.getUserName());
							sessionMap.put("partnerUserStatus","1");
							sessionMap.put("partnerUSER", user);
							sessionMap.put("partnerCountcap", count);
							int intCount=0;
							Integer selectedId=(Integer)sessionMap.get("selectedPropertyId");
							if(selectedId == null){
							this.propertyList =propertyController.listPropertyByUser(user.getUserId());
							for (PmsProperty property : propertyList) {
								intCount++;
								if(intCount==1){
									sessionMap.put("partnerPropertyId", property.getPropertyId());
								}
							}
							
							}
							else{
								sessionMap.put("partnerPropertyId",selectedId);
							}
							
							/*String jsonOutput = "";
							HttpServletResponse response = ServletActionContext.getResponse();
							response.setContentType("application/json");
							
							if (!jsonOutput.equalsIgnoreCase(""))
								jsonOutput += ",{";
							else
								jsonOutput += "{";
							jsonOutput += "\"partnerUserName\":\"" + user.getUserName() + "\"";
							jsonOutput += ",\"partnerEmailId\":\"" + user.getEmailId()+ "\"";
							jsonOutput += ",\"partnerPhone\":\"" + user.getPhone()+ "\"";
						    jsonOutput += "}";
		                    
						    response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
							*/
							
							
							ret= "success";
						}else{
							addActionError(getText("error.login"));
							ActionContext.getContext().getSession().put("countcap", ++count);
							ret= "error";
						}
					}else{
						addActionError(getText("error.invalid.login"));
						ActionContext.getContext().getSession().put("countcap", ++count);
						ret= "error";
					}
					
				}else{
					addActionError(getText("error.login"));
					ActionContext.getContext().getSession().put("countcap", ++count);
					ret= "error";
				}
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return ret;
	}
	
	public String corporateLogin(){

		String ret = "error";
		try{
			Map mapsession = null;
			PmsPropertyManager propertyController=new PmsPropertyManager();
			int count=0;
			if(ActionContext.getContext().getSession().get("countcap")!=null)
			{
				if(ActionContext.getContext().getSession().get("countcap")!="0")
				{
					count=(int) ActionContext.getContext().getSession().get("countcap");
				}
				
			}
			count=Integer.parseInt(String.valueOf((sessionMap.get("countcap")!=null?sessionMap.get("countcap"):"0")));
			RoleManager roleController = new RoleManager();
			AccessRightManager accessRightController = new AccessRightManager();
			Boolean status=false;
			if(username==null){
				mapsession = (Map) ActionContext.getContext().getSession(); // it returns null
				status=(Boolean)mapsession.get("logined");
				if(status==null){
					status=true;
					ret= "error";
				}else{
					status=true;
					ret= "success";
				}
				
			}
			if(!status){
				get_users=linkController.verifyUser(username);
				if(get_users.size()>0)
				{
					String pass=passwordDecrypt.getDigestvalid(password, get_users.get(0).getSaltkey());
					users =  linkController.verifyLogin(username, pass);
					String roleName="NA";
					if(users.size()>0){
						user = users.get(0);
						int roleId=user.getRole().getRoleId();
						Role role1=roleController.find(roleId);
						roleName=role1.getRoleName();
					}
					mapsession = ActionContext.getContext().getSession();
	                mapsession.put("logined",true);
					if(roleName.equalsIgnoreCase("Corporate Manger") || roleName.equalsIgnoreCase("admin")){
						if(users.size()>0)
						{
							count=0;
							user = users.get(0);
							User user1 =linkController.findUser(user.getUserId());
							String userPermissions ="";
							
							Role role =  roleController.find(user1.getRole().getRoleId());
							sessionMap.put("corporateLoginName",user.getUserName());
							sessionMap.put("corporateUserId",user.getUserId());
							sessionMap.put("corporateEmailId",user.getEmailId());
							sessionMap.put("corporateRole",role.getRoleName());
							sessionMap.put("corporateUserName",user.getUserName());
							sessionMap.put("corporateUserStatus","1");
							sessionMap.put("corporateUSER", user);
							sessionMap.put("corporateCountcap", count);
							int intCount=0;
							Integer selectedId=(Integer)sessionMap.get("selectedPropertyId");
							if(selectedId == null){
							this.propertyList =propertyController.listPropertyByUser(user.getUserId());
							for (PmsProperty property : propertyList) {
								intCount++;
								if(intCount==1){
									sessionMap.put("corporatePropertyId", property.getPropertyId());
								}
							}
							
							}
							else{
								sessionMap.put("corporatePropertyId",selectedId);
							}
							
							/*String jsonOutput = "";
							HttpServletResponse response = ServletActionContext.getResponse();
							response.setContentType("application/json");
							
							if (!jsonOutput.equalsIgnoreCase(""))
								jsonOutput += ",{";
							else
								jsonOutput += "{";
							jsonOutput += "\"partnerUserName\":\"" + user.getUserName() + "\"";
							jsonOutput += ",\"partnerEmailId\":\"" + user.getEmailId()+ "\"";
							jsonOutput += ",\"partnerPhone\":\"" + user.getPhone()+ "\"";
						    jsonOutput += "}";
		                    
						    response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
							*/
							
							
							ret= "success";
						}else{
							addActionError(getText("error.login"));
							ActionContext.getContext().getSession().put("countcap", ++count);
							ret= "error";
						}
					}else{
						addActionError(getText("error.invalid.login"));
						ActionContext.getContext().getSession().put("countcap", ++count);
						ret= "error";
					}
					
				}else{
					addActionError(getText("error.login"));
					ActionContext.getContext().getSession().put("countcap", ++count);
					ret= "error";
				}
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return ret;
	
	}
	public String uloLogin() {

		String ret = "error";
		try
		{
			int count=0;
			if(ActionContext.getContext().getSession().get("countcap")!=null)
			{
				if(ActionContext.getContext().getSession().get("countcap")!="0")
				{
					count=(int) ActionContext.getContext().getSession().get("countcap");
				}
				
			}
			count=Integer.parseInt(String.valueOf((sessionMap.get("countcap")!=null?sessionMap.get("countcap"):"0")));
			RoleManager roleController = new RoleManager();
			AccessRightManager accessRightController = new AccessRightManager();
			get_users=linkController.verifyUser(username);
			if(get_users.size()>0)
			{
				String pass=passwordDecrypt.getDigestvalid(password, get_users.get(0).getSaltkey());
				users =  linkController.verifyLogin(username, pass);
				
				if(users.size()>0)
				{
					 count=0;
					user = users.get(0);
					User user1 =linkController.findUser(user.getUserId());
					String userPermissions ="";
					/*List<UserAccessRights> userAccessRightsList =  userRightsController.list(user.getUserId());
					for(UserAccessRights userAccessRights : userAccessRightsList)
					{
						AccessRights accessRights =  accessRightsController.find(userAccessRights.getAccessRights().getAccessRightsId());
						if(!userPermissions.equalsIgnoreCase(""))
							userPermissions += "*" +accessRights.getAccessRights();
						else
							userPermissions = accessRights.getAccessRights();
					}
					
					
					*/
					//short r = 4;
					Role role =  roleController.find(user1.getRole().getRoleId());
					sessionMap.put("uloLoginName",user.getUserName());
					sessionMap.put("uloUserId",user.getUserId());
					sessionMap.put("uloEmailId",user.getEmailId());
					sessionMap.put("uloRole",role.getRoleName());
					sessionMap.put("uloUserName",user.getUserName());
					sessionMap.put("uloUserStatus","1");
					sessionMap.put("uloUSER", user);
					sessionMap.put("ulocountcap", count);
					//role id based access rights with true, comma separated
					RoleAccessRightManager roleAccessController = new RoleAccessRightManager();
					
					this.roleAccessList = roleAccessController.list(user1.getRole().getRoleId());
					StringBuilder displayNames = new StringBuilder();
					for (RoleAccessRight roleAccessRight : roleAccessList) {
					//Hibernate.initialize(roleAccessRight.getAccessRight());
					AccessRight accessRight = accessRightController.find(roleAccessRight.getAccessRight().getAccessRightsId());
					accessRight.getDisplayName();
					
					displayNames.append(accessRight.getDisplayName());
					displayNames.append(",");
					
					
					
					}
					
					sessionMap.put("userRights", displayNames);
					
					String jsonOutput = "";
					HttpServletResponse response = ServletActionContext.getResponse();
					response.setContentType("application/json");
					
					if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += ",{";
					else
						jsonOutput += "{";
					jsonOutput += "\"uloUserName\":\"" + user.getUserName() + "\"";
					jsonOutput += ",\"uloEmailId\":\"" + user.getEmailId()+ "\"";
					jsonOutput += ",\"uloPhone\":\"" + user.getPhone()+ "\"";
				    jsonOutput += "}";
                    
				    response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
					
					
					
					ret= null;
					
				} else {
				
					addActionError(getText("error.login"));
					ActionContext.getContext().getSession().put("countcap", ++count);
					ret= "error";
				}
			}else {
				
				addActionError(getText("error.login"));
				ActionContext.getContext().getSession().put("countcap", ++count);
				ret= "error";
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return ret;
	}
	
	public String sendUloLoginOtp() {

		String ret = "error";
		try {
			
			HttpServletResponse response = ServletActionContext.getResponse();
			//response.reset();
			response.flushBuffer();
			response.setContentType("application/json");
			
			int count = 0;
			if (ActionContext.getContext().getSession().get("countcap") != null) {
				if (ActionContext.getContext().getSession().get("countcap") != "0") {
					count = (int) ActionContext.getContext().getSession()
							.get("countcap");
				}

			}

			String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."
					+ "[a-zA-Z0-9_+&*-]+)*@" + "(?:[a-zA-Z0-9-]+\\.)+[a-z"
					+ "A-Z]{2,7}$";

			Pattern pat = Pattern.compile(emailRegex);
			Boolean bool = pat.matcher(username).matches();
			int userId = 0;
			String userName=null;
			if (bool == true) {
				User email = linkController.verifyUserEmail(username);
				if(email != null){
				userId = email.getUserId();
				userName=email.getUserName();
				User userByEmail = linkController.find(email.getUserId());
				int randomPin = (int) (Math.random() * 900000) + 100000;
				String otp = String.valueOf(randomPin);
				userByEmail.setLoginOtp(otp);
				linkController.add(userByEmail);
				String strSubject = "LOGIN OTP FOR ULOHOTELS";

				// email template
				Configuration cfg = new Configuration();
				cfg.setClassForTemplateLoading(ForgetPasswordAction.class,
						"../../../");
				Template template = cfg
						.getTemplate(getText("ulopasswordotp.template"));
				Map<String, String> rootMap = new HashMap<String, String>();
				User user = linkController.find(email.getUserId());
				rootMap.put("to", username);
				rootMap.put("otp", user.getLoginOtp());
				rootMap.put("from", getText("notification.from"));
				Writer out = new StringWriter();
				template.process(rootMap, out);

				// send email
				Email em = new Email();
				if(userName!=null){
					em.set_to(userName);
				}else{
					em.set_to(username);	
				}
//				em.set_to(username);
				em.set_from(getText("email.from"));
				em.set_username(getText("email.username"));
				em.set_subject(strSubject);
				em.set_bodyContent(out);
				em.send();
				ret = null;
				}
				
			} else {
				User phone = linkController.verifyUserPhone(username);
				if(phone != null){
				userId = phone.getUserId();
				User userbyphone = linkController.find(phone.getUserId());
				int randomPin = (int) (Math.random() * 900000) + 100000;
				String otp = String.valueOf(randomPin);
				userbyphone.setLoginOtp(otp);
				linkController.add(userbyphone);
				String message = "&sms_text="+"Dear Guest, Your OTP code for Ulohotels is "+ otp;
				//String sender = "&sender=" + "TXTLCL";
				String numbers = "&sms_to=" + "+91"+username;
				String from = "&sms_from=" + "ULOHTL";
				String type = "&sms_type=" + "trans"; 
				// Send data
				HttpURLConnection conn = (HttpURLConnection) new URL("https://secureets.knowlarity.com/ets/dom/clients/ulohotels/api/ulohotels_sms_api.py").openConnection();
				//"http://etsrds.kapps.in/webapi/ulohotels/api/ulohotels_sms_api.py?sms_text=hi%20this%20gopinath%20from%20ulo&sms_to=%20918220211068&sms_from=ULOHTL&sms_type=promo"
				String data = numbers + message +  from + type;
				conn.setDoOutput(true);
				conn.setRequestMethod("POST");
				conn.addRequestProperty("auth_key", "Basic dWxvaG90ZWxzX1Q6VUxPSFRMQDIwMTg=");
				conn.setRequestProperty("Content-Length", Integer.toString(data.length()));
				conn.getOutputStream().write(data.getBytes("UTF-8"));
				final BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
				final StringBuffer stringBuffer = new StringBuffer();
				/*String line;
				
				while ((line = rd.readLine()) != null) {
					stringBuffer.append(line);
					
				}*/
				rd.close();
				ret = null;
				}

			}

			String jsonOutput = "";
			
			if (!jsonOutput.equalsIgnoreCase(""))
				jsonOutput += ",{";
			else
				jsonOutput += "{";
			
			jsonOutput += "\"uloUserId\":\"" + userId + "\"";
			jsonOutput += ",\"userName\":\"" + username + "\"";

			jsonOutput += "}";

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");

			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		}
		return ret;
	}
	
	public String resendUloLoginOtp() {

		String ret = "error";
		try {
			int count = 0;
			if (ActionContext.getContext().getSession().get("countcap") != null) {
				if (ActionContext.getContext().getSession().get("countcap") != "0") {
					count = (int) ActionContext.getContext().getSession()
							.get("countcap");
				}

			}

			String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."
					+ "[a-zA-Z0-9_+&*-]+)*@" + "(?:[a-zA-Z0-9-]+\\.)+[a-z"
					+ "A-Z]{2,7}$";

			Pattern pat = Pattern.compile(emailRegex);
			Boolean bool = pat.matcher(username).matches();
			int userId = 0;
			if (bool == true) {
				User email = linkController.verifyUserEmail(username);
				userId = email.getUserId();
				User userByEmail = linkController.find(email.getUserId());
				int randomPin = (int) (Math.random() * 900000) + 100000;
				String otp = String.valueOf(randomPin);
				userByEmail.setLoginOtp(otp);
				linkController.edit(userByEmail);
				String strSubject = "YOUR OTP FOR ULOHOTELS";

				// email template
				Configuration cfg = new Configuration();
				cfg.setClassForTemplateLoading(ForgetPasswordAction.class,
						"../../../");
				Template template = cfg
						.getTemplate(getText("ulopasswordotp.template"));
				Map<String, String> rootMap = new HashMap<String, String>();
				rootMap.put("to", username);
				rootMap.put("otp", otp);
				rootMap.put("from", getText("notification.from"));
				Writer out = new StringWriter();
				template.process(rootMap, out);

				// send email
				Email em = new Email();
				em.set_to(username);
				em.set_from(getText("email.from"));
				em.set_username(getText("email.username"));
				em.set_subject(strSubject);
				em.set_bodyContent(out);
				em.send();
			} else {
				User phone = linkController.verifyUserPhone(username);
				userId = phone.getUserId();
				User userbyphone = linkController.find(phone.getUserId());
				int randomPin = (int) (Math.random() * 900000) + 100000;
				String otp = String.valueOf(randomPin);
				userbyphone.setLoginOtp(otp);
				// linkController.add(user);
				String message = "&sms_text="+"Dear Guest, Your OTP code for Ulohotels is"+ otp;
				//String sender = "&sender=" + "TXTLCL";
				String numbers = "&sms_to=" + "+91"+username;
				String from = "&sms_from=" + "ULOHTL";
				String type = "&sms_type=" + "trans"; 
				// Send data
				HttpURLConnection conn = (HttpURLConnection) new URL("https://secureets.knowlarity.com/ets/dom/clients/ulohotels/api/ulohotels_sms_api.py").openConnection();
				//"http://etsrds.kapps.in/webapi/ulohotels/api/ulohotels_sms_api.py?sms_text=hi%20this%20gopinath%20from%20ulo&sms_to=%20918220211068&sms_from=ULOHTL&sms_type=promo"
				String data = numbers + message +  from + type;
				conn.setDoOutput(true);
				conn.setRequestMethod("POST");
				conn.addRequestProperty("auth_key", "Basic dWxvaG90ZWxzX1Q6VUxPSFRMQDIwMTg=");
				conn.setRequestProperty("Content-Length", Integer.toString(data.length()));
				conn.getOutputStream().write(data.getBytes("UTF-8"));
				final BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
				final StringBuffer stringBuffer = new StringBuffer();
				/*String line;
				
				while ((line = rd.readLine()) != null) {
					stringBuffer.append(line);
					
				}*/
				rd.close();

			}

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("application/json");
			if (!jsonOutput.equalsIgnoreCase(""))
				jsonOutput += ",{";
			else
				jsonOutput += "{";
			jsonOutput += "\"uloUserId\":\"" + userId + "\"";
			jsonOutput += ",\"userName\":\"" + username + "\"";

			jsonOutput += "}";

			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");

			ret = null;

		} catch (Exception e) {
			e.printStackTrace();
			ret = "error";
		}

		return ret;
	}

	public String verifyUloLoginOtp() {


		try {			
				User user = linkController.verifyUserOtp(getUserId(),getLoginOtp());
				sessionMap.put("uloUserName",user.getUserName());
				sessionMap.put("uloUserId",user.getUserId());
				sessionMap.put("uloEmailId",user.getEmailId());
			    sessionMap.put("uloUserStatus","1");
				sessionMap.put("uloUSER", user);
				sessionMap.put("uloSignup","true");
				sessionMap.put("uloPhone",user.getPhone());


				String jsonOutput = "";
				HttpServletResponse response = ServletActionContext.getResponse();
				response.setContentType("application/json");
				
				if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				else
					jsonOutput += "{";
				jsonOutput += "\"uloUserName\":\"" + user.getUserName() + "\"";
				jsonOutput += ",\"userId\":\"" + user.getUserId() + "\"";
				jsonOutput += ",\"uloEmailId\":\"" + user.getEmailId()+ "\"";
				jsonOutput += ",\"uloPhone\":\"" + user.getPhone()+ "\"";
			    jsonOutput += "}";
                
			    response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
				

				

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}
	@SuppressWarnings("unused")
	public String verifyLoginOtp(Login login) {

		String output=null;
		try {			
			String token=null,jsonOutput="";
			 int count=0,errorcount=1;
			 PmsGuestManager guestController=new PmsGuestManager();
			 
			 Algorithm algorithm = Algorithm.HMAC256("secret");
				User user = linkController.verifyUserOtp(Integer.parseInt(login.getUserId()),login.getLoginOtp());
				HttpServletResponse response = ServletActionContext.getResponse();
				response.setContentType("application/json");
				if(user!=null){
					
					token = JWT.create().withClaim("email", user.getEmailId())
		        		  	.withClaim("userId", user.getUserId())
							.withClaim("userName", user.getUserName())
							.sign(algorithm);
				 
					if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += ",{";
					else
						jsonOutput += "{";
					jsonOutput += "\"userName\":\"" + user.getUserName() + "\"";
					jsonOutput += ",\"userId\":\"" + user.getUserId() + "\"";
					jsonOutput += ",\"emailId\":\"" + (user.getEmailId()==null?"None":user.getEmailId())+ "\"";
					jsonOutput += ",\"mobileNumber\":\"" + (user.getPhone()==null?"None":user.getPhone())+ "\"";
					jsonOutput +=",\"address\" : \""+(user.getAddress1()==null?"NA":user.getAddress1())+"\"";
					User findUser=linkController.find(user.getUserId());
					if(findUser!=null){
						if(findUser.getPmsTags()==null){
							jsonOutput += ",\"tripTypeId\":\"" + 0 + "\"";
						}else{
							jsonOutput += ",\"tripTypeId\":\"" + findUser.getPmsTags().getTagId()+ "\"";	
						}
					}else{
						jsonOutput += ",\"tripTypeId\":\"" + 0 + "\"";
					}
					this.mobileNumber=user.getPhone()==null?"NA":user.getPhone();
					this.emailid=user.getEmailId()==null?"NA":user.getEmailId();
					boolean blnMobile=false,blnEmail=false;
					List<PmsGuest> finduserMobile=guestController.listCheckMobileNumber(this.mobileNumber);
					 if(finduserMobile.size()>0 && !finduserMobile.isEmpty()){
						 blnMobile=true;
					 }
					 
					 List<PmsGuest> finduser=guestController.listCheckEmail(this.emailid);
					 if(finduser.size()>0 && !finduser.isEmpty()){
						 blnEmail=true;
					 }
					 
					 if(blnMobile || blnEmail){
						 jsonOutput+=",\"firstTimeUser\" : \""+(false)+"\"";	 
					 }else{
						 jsonOutput+=",\"firstTimeUser\" : \""+(true)+"\"";
					 }
					
					jsonOutput += ",\"token\":\"" + (token)+ "\"";
				    jsonOutput += "}";
				    
				    findUser.setLoginOtp("NA");
				    linkController.edit(findUser);
				}
				
                
			    //response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
	 			String data="\"data\":[" + jsonOutput + "]";
	 			String strData=data.replaceAll("\"", "\"");
	 			
	 			if(user == null){
					//response.getWriter().write("{\"error\":\"1\",\"message\":\"Sorry No Results Found\"}");
	 				output="{\"error\":\"1\",\"message\":\"Invalid OTP\"}";
				}else{
					output="{\"error\":\"0\"," + strData + "}";					
				}

	 			
		} catch (Exception e) {
			output="{\"error\":\"2\",\"message\":\"Some technical error on verify login otp api\"}";
			logger.error(e);
			e.printStackTrace();
		}

		return output;
	}
	
	public String sendOtp(Login login){


		String ret = "error",output=null;
		try {
			
			this.mobileNumber=login.getMobileNumber();
			this.emailid=login.getEmailId();
			if(this.emailid==null){
				this.emailid="NA";
			}
			HttpServletResponse response = ServletActionContext.getResponse();
			//response.reset();
			response.flushBuffer();
			response.setContentType("application/json");
			
			int count = 0,errorcount=0;
			if (ActionContext.getContext().getSession().get("countcap") != null) {
				if (ActionContext.getContext().getSession().get("countcap") != "0") {
					count = (int) ActionContext.getContext().getSession()
							.get("countcap");
				}

			}

			String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."
					+ "[a-zA-Z0-9_+&*-]+)*@" + "(?:[a-zA-Z0-9-]+\\.)+[a-z"
					+ "A-Z]{2,7}$";
			String imagePath="";
			
			Pattern pat = Pattern.compile(emailRegex);
			Boolean bool = pat.matcher(this.emailid).matches();
			int userId = 0;
			String userName=null;
			if (bool == true) {
				User email = linkController.verifyUserEmail(this.emailid);
					if(email != null){
					errorcount++;	
					userId = email.getUserId();
					userName=email.getUserName();
					
	            	if ( email.getImgPath() != null) {
						imagePath = getText("storage.aws.property.photo") + "/users/"
								+ userId+"/"+email.getImgPath();
					}
					User userByEmail = linkController.find(email.getUserId());
					int randomPin = (int) (Math.random() * 900000) + 100000;
					String otp = String.valueOf(randomPin);
					userByEmail.setLoginOtp(otp);
					linkController.edit(userByEmail);
					String strSubject = "LOGIN OTP FOR ULOHOTELS";
	
					// email template
					Configuration cfg = new Configuration();
					cfg.setClassForTemplateLoading(ForgetPasswordAction.class,
							"../../../");
					Template template = cfg
							.getTemplate(getText("ulopasswordotp.template"));
					Map<String, String> rootMap = new HashMap<String, String>();
					User user = linkController.find(email.getUserId());
					rootMap.put("to", this.emailid);
					rootMap.put("otp", user.getLoginOtp());
					rootMap.put("from", getText("notification.from"));
					Writer out = new StringWriter();
					template.process(rootMap, out);
	
					// send email
					Email em = new Email();
					if(userName!=null){
						em.set_to(this.emailid);
					}else{
						em.set_to(this.emailid);	
					}
	//				em.set_to(username);
					em.set_from(getText("email.from"));
					em.set_username(getText("email.username"));
					em.set_subject(strSubject);
					em.set_bodyContent(out);
					em.send();
					ret = null;
				}
				
			} else {
				User phone = linkController.verifyUserPhone(this.mobileNumber);
				if(phone != null){
					errorcount++;
					userId = phone.getUserId();
					userName =phone.getUserName();
					if ( phone.getImgPath() != null) {
						imagePath = getText("storage.aws.property.photo") + "/users/"
								+ userId+"/"+phone.getImgPath();
					}
					User userbyphone = linkController.find(phone.getUserId());
					int randomPin = (int) (Math.random() * 900000) + 100000;
					String otp = String.valueOf(randomPin);
					userbyphone.setLoginOtp(otp);
					linkController.edit(userbyphone);
					String message = "&sms_text="+"Dear Guest, Your OTP code for Ulohotels is "+ otp;
					//String sender = "&sender=" + "TXTLCL";
					String numbers = "&sms_to=" + "+91"+this.mobileNumber;
					String from = "&sms_from=" + "ULOHTL";
					String type = "&sms_type=" + "trans"; 
					// Send data
					HttpURLConnection conn = (HttpURLConnection) new URL("https://secureets.knowlarity.com/ets/dom/clients/ulohotels/api/ulohotels_sms_api.py").openConnection();
					//"http://etsrds.kapps.in/webapi/ulohotels/api/ulohotels_sms_api.py?sms_text=hi%20this%20gopinath%20from%20ulo&sms_to=%20918220211068&sms_from=ULOHTL&sms_type=promo"
					String data = numbers + message +  from + type;
					conn.setDoOutput(true);
					conn.setRequestMethod("POST");
					conn.addRequestProperty("auth_key", "Basic dWxvaG90ZWxzX1Q6VUxPSFRMQDIwMTg=");
					conn.setRequestProperty("Content-Length", Integer.toString(data.length()));
					conn.getOutputStream().write(data.getBytes("UTF-8"));
					final BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
					final StringBuffer stringBuffer = new StringBuffer();
					/*String line;
					
					while ((line = rd.readLine()) != null) {
						stringBuffer.append(line);
						
					}*/
					rd.close();
					ret = null;
				}

			}

			
			String jsonOutput = "";
			
			if (!jsonOutput.equalsIgnoreCase(""))
				jsonOutput += ",{";
			else
				jsonOutput += "{";
			
			jsonOutput += "\"userId\":\"" + userId + "\"";
			jsonOutput += ",\"userName\":\"" + userName + "\"";
			jsonOutput += ",\"imagePath\":\"" + imagePath + "\"";
			jsonOutput += "}";
			
			//response.getWriter().write("{\"data\":[" + jsonOutput + "]}");

 			String data="\"data\":[" + jsonOutput + "]";
 			String strData=data.replaceAll("\"", "\"");
 			
 			if(errorcount == 0){
 				output="{\"error\":\"1\",\"message\":\"You are not registered with us, Please signup\"}";
			}else{
				output="{\"error\":\"0\"," + strData + "}";
				
			}
		} catch (Exception e) {
			output="{\"error\":\"2\",\"message\":\"Some technical error on send login otp api\"}";
			logger.error(e);
			e.printStackTrace();
		}
		return output;
	
	}
	
	public String resendOtp(Login login){



		String ret = "error",output=null;
		try {
			this.mobileNumber=login.getMobileNumber();
			this.emailid=login.getEmailId();
			if(this.emailid==null){
				this.emailid="NA";
			}
			HttpServletResponse response = ServletActionContext.getResponse();
			//response.reset();
			response.flushBuffer();
			response.setContentType("application/json");
			
			int count = 0,errorcount=0;
			if (ActionContext.getContext().getSession().get("countcap") != null) {
				if (ActionContext.getContext().getSession().get("countcap") != "0") {
					count = (int) ActionContext.getContext().getSession()
							.get("countcap");
				}

			}

			String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."
					+ "[a-zA-Z0-9_+&*-]+)*@" + "(?:[a-zA-Z0-9-]+\\.)+[a-z"
					+ "A-Z]{2,7}$";

			Pattern pat = Pattern.compile(emailRegex);
			Boolean bool = pat.matcher(this.emailid).matches();
			int userId = 0;
			String userName=null;
			if (bool == true) {
				User email = linkController.verifyUserEmail(this.emailid);
				if(email != null){
				errorcount++;
				this.userId = email.getUserId();
				this.username=email.getUserName();
				this.emailid=email.getEmailId();
				User userByEmail = linkController.find(email.getUserId());
				int randomPin = (int) (Math.random() * 900000) + 100000;
				String otp = String.valueOf(randomPin);
				userByEmail.setLoginOtp(otp);
				linkController.add(userByEmail);
				String strSubject = "LOGIN OTP FOR ULOHOTELS";

				// email template
				Configuration cfg = new Configuration();
				cfg.setClassForTemplateLoading(ForgetPasswordAction.class,
						"../../../");
				Template template = cfg
						.getTemplate(getText("ulopasswordotp.template"));
				Map<String, String> rootMap = new HashMap<String, String>();
				User user = linkController.find(email.getUserId());
				rootMap.put("to", username);
				rootMap.put("otp", user.getLoginOtp());
				rootMap.put("from", getText("notification.from"));
				Writer out = new StringWriter();
				template.process(rootMap, out);

				// send email
				Email em = new Email();
				if(this.emailid!=null){
					em.set_to(this.emailid);
				}else{
					em.set_to(this.emailid);	
				}
//				em.set_to(username);
				em.set_from(getText("email.from"));
				em.set_username(getText("email.username"));
				em.set_subject(strSubject);
				em.set_bodyContent(out);
				em.send();
				ret = null;
				}
				
			} else {
				User phone = linkController.verifyUserPhone(this.mobileNumber);
				if(phone != null){
				errorcount++;	
				this.userId = phone.getUserId();
				this.username =phone.getUserName();
				User userbyphone = linkController.find(phone.getUserId());
				int randomPin = (int) (Math.random() * 900000) + 100000;
				String otp = String.valueOf(randomPin);
				userbyphone.setLoginOtp(otp);
				linkController.add(userbyphone);
				String message = "&sms_text="+"Dear Guest, Your OTP code for Ulohotels is "+ otp;
				//String sender = "&sender=" + "TXTLCL";
				String numbers = "&sms_to=" + "+91"+username;
				String from = "&sms_from=" + "ULOHTL";
				String type = "&sms_type=" + "trans"; 
				// Send data
				HttpURLConnection conn = (HttpURLConnection) new URL("https://secureets.knowlarity.com/ets/dom/clients/ulohotels/api/ulohotels_sms_api.py").openConnection();
				//"http://etsrds.kapps.in/webapi/ulohotels/api/ulohotels_sms_api.py?sms_text=hi%20this%20gopinath%20from%20ulo&sms_to=%20918220211068&sms_from=ULOHTL&sms_type=promo"
				String data = numbers + message +  from + type;
				conn.setDoOutput(true);
				conn.setRequestMethod("POST");
				conn.addRequestProperty("auth_key", "Basic dWxvaG90ZWxzX1Q6VUxPSFRMQDIwMTg=");
				conn.setRequestProperty("Content-Length", Integer.toString(data.length()));
				conn.getOutputStream().write(data.getBytes("UTF-8"));
				final BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
				final StringBuffer stringBuffer = new StringBuffer();
				/*String line;
				
				while ((line = rd.readLine()) != null) {
					stringBuffer.append(line);
					
				}*/
				rd.close();
				ret = null;
				}

			}

			String jsonOutput = "";
			
			if (!jsonOutput.equalsIgnoreCase(""))
				jsonOutput += ",{";
			else
				jsonOutput += "{";
			
			jsonOutput += "\"userId\":\"" + this.userId + "\"";
			jsonOutput += ",\"userName\":\"" + this.username + "\"";

			jsonOutput += "}";

			String data="\"data\":[" + jsonOutput + "]";
 			String strData=data.replaceAll("\"", "\"");
 			
 			if(errorcount == 0){
 				output="{\"error\":\"1\",\"message\":\"Invalid User\"}";
			}else{
				output="{\"error\":\"0\"," + strData + "}";
			}
			

		} catch (Exception e) {
			output="{\"error\":\"2\",\"message\":\"Some technical error on resend login otp api\"}";
			logger.error(e);
			e.printStackTrace();
		}
		return output;
	
	
	}
	
	public String socialLogin() throws IOException {
		
		try {
			
		    sessionMap.put("uloLoginName",getUsername());
			sessionMap.put("uloUserId",getUserid());
			sessionMap.put("uloEmailId",getEmailid());
			sessionMap.put("uloUserName",getUsername());
			sessionMap.put("uloUserStatus","1");
			//sessionMap.put("USER", user);
			
			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("application/json");
			
			if (!jsonOutput.equalsIgnoreCase(""))
				jsonOutput += ",{";
			else
				jsonOutput += "{";
			jsonOutput += "\"uloUserName\":\"" + getUsername() + "\"";
			jsonOutput += ",\"userId\":\"" + getUserid() + "\"";
			jsonOutput += ",\"uloEmailId\":\"" + getEmailid()+ "\"";
		    jsonOutput += "}";
            
		    response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			
			

		} catch (Exception e) {
			
			e.printStackTrace();
		} finally {

		}

		return null;

	}

	
	
	

	

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getEmailid() {
		return emailid;
	}

	public void setEmailid(String emailid) {
		this.emailid = emailid;
	}
	

	public String getLoginOtp() {
		return loginOtp;
	}

	public void setLoginOtp(String loginOtp) {
		this.loginOtp = loginOtp;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	
    public List<RoleAccessRight> getRoleAccessList() {
		return roleAccessList;
	}

	public void setRoleAccessList(List<RoleAccessRight> roleAccessList) {
		this.roleAccessList = roleAccessList;
	}
	
	public String logout()
	{
		if(sessionMap!=null){
			sessionMap.remove("userName");
			sessionMap.remove("userId");
			sessionMap.remove("emailId");
			sessionMap.remove("role");
			sessionMap.remove("userName");
			sessionMap.remove("userStatus");
			sessionMap.remove("USER");
			sessionMap.remove("countcap");
			//sessionMap.invalidate();
		}
			
		if(session!=null) {
			session.removeAttribute("userName");
			session.removeAttribute("userId");
			session.removeAttribute("emailId");
			session.removeAttribute("role");
			session.removeAttribute("userName");
			session.removeAttribute("userStatus");
			session.removeAttribute("USER");
			session.removeAttribute("countcap");
			//session.invalidate();
		}
		
		
		HttpServletResponse response = ServletActionContext.getResponse();
		response.setHeader("Pragma","no-cache");
		response.setHeader("Cache-Control","no-store");
		response.setHeader("Cache-Control","no-cache");
		response.setHeader("Expires","0");
		response.setDateHeader("Expires",0);
		return "success";
	}
	
	public String uloLogout()
	{
		if(sessionMap!=null){
			sessionMap.remove("uloUserName");
			sessionMap.remove("uloUserId");
			sessionMap.remove("uloEmailId");
			sessionMap.remove("uloRole");
			sessionMap.remove("uloUserName");
			sessionMap.remove("uloUserStatus");
			sessionMap.remove("uloUSER");
			sessionMap.remove("ulocountcap");
			sessionMap.remove("uloRewardPoints");
			//sessionMap.invalidate();
		}
			
		if(session!=null) {
			session.removeAttribute("uloUserName");
			session.removeAttribute("uloUserId");
			session.removeAttribute("uloEmailId");
			session.removeAttribute("uloRole");
			session.removeAttribute("uloUserName");
			session.removeAttribute("uloUserStatus");
			session.removeAttribute("uloUSER");
			session.removeAttribute("ulocountcap");
			session.removeAttribute("uloRewardPoints");
			//session.invalidate();
		}
			
		HttpServletResponse response = ServletActionContext.getResponse();
		response.setHeader("Pragma","no-cache");
		response.setHeader("Cache-Control","no-store");
		response.setHeader("Cache-Control","no-cache");
		response.setHeader("Expires","0");
		response.setDateHeader("Expires",0);
		return "success";
	}

	public String partnerLogout()
	{
		if(sessionMap!=null){
			sessionMap.remove("userName");
			sessionMap.remove("userId");
			sessionMap.remove("emailId");
			sessionMap.remove("role");
			sessionMap.remove("userName");
			sessionMap.remove("userStatus");
			sessionMap.remove("USER");
			sessionMap.remove("countcap");
			//sessionMap.invalidate();
		}
			
		if(session!=null) {
			session.removeAttribute("userName");
			session.removeAttribute("userId");
			session.removeAttribute("emailId");
			session.removeAttribute("role");
			session.removeAttribute("userName");
			session.removeAttribute("userStatus");
			session.removeAttribute("USER");
			session.removeAttribute("countcap");
			//session.invalidate();
		}
		
		HttpServletResponse response = ServletActionContext.getResponse();
		response.setHeader("Pragma","no-cache");
		response.setHeader("Cache-Control","no-store");
		response.setHeader("Cache-Control","no-cache");
		response.setHeader("Expires","0");
		response.setDateHeader("Expires",0);
		return SUCCESS;
	}
	
	public String corporateLogout(){

		if(sessionMap!=null){
			sessionMap.remove("corporateUserName");
			sessionMap.remove("corporateUserId");
			sessionMap.remove("corporateEmailId");
			sessionMap.remove("corporateRole");
			sessionMap.remove("corporateUserName");
			sessionMap.remove("corporateUserStatus");
			sessionMap.remove("corporateUSER");
			sessionMap.remove("corporateCountcap");
			sessionMap.remove("corporatePropertyId");
			//sessionMap.invalidate();
		}
			
		if(session!=null) {
			session.removeAttribute("corporateUserName");
			session.removeAttribute("corporateUserId");
			session.removeAttribute("corporateEmailId");
			session.removeAttribute("corporateRole");
			session.removeAttribute("corporateUserName");
			session.removeAttribute("corporateUserStatus");
			session.removeAttribute("corporateUSER");
			session.removeAttribute("corporateCountcap");
			session.removeAttribute("corporatePropertyId");
			//session.invalidate();
		}
		
		
		HttpServletResponse response = ServletActionContext.getResponse();
		response.setHeader("Pragma","no-cache");
		response.setHeader("Cache-Control","no-store");
		response.setHeader("Cache-Control","no-cache");
		response.setHeader("Expires","0");
		response.setDateHeader("Expires",0);
		return "success";
	
	}

	/*public String logout()
	{
		if(sessionMap!=null) sessionMap.invalidate();
		if(session!=null) session.invalidate();
		HttpServletResponse response = ServletActionContext.getResponse();
		response.setHeader("Pragma","no-cache");
		response.setHeader("Cache-Control","no-store");
		response.setHeader("Cache-Control","no-cache");
		response.setHeader("Expires","0");
		response.setDateHeader("Expires",0);
		return "success";
	}*/
	
	public String partnerlogout()
	{
		if(sessionMap!=null){
			sessionMap.remove("partnerUserName");
			sessionMap.remove("partnerUserId");
			sessionMap.remove("partnerEmailId");
			sessionMap.remove("partnerRole");
			sessionMap.remove("partnerUserName");
			sessionMap.remove("partnerUserStatus");
			sessionMap.remove("partnerUSER");
			sessionMap.remove("partnerCountcap");
			sessionMap.remove("partnerPropertyId");
			sessionMap.remove("selectedPropertyId");
			//sessionMap.invalidate();
		}
			
		if(session!=null) {
			session.removeAttribute("partnerUserName");
			session.removeAttribute("partnerUserId");
			session.removeAttribute("partnerEmailId");
			session.removeAttribute("partnerRole");
			session.removeAttribute("partnerUserName");
			session.removeAttribute("partnerUserStatus");
			session.removeAttribute("partnerUSER");
			session.removeAttribute("partnerCountcap");
			session.removeAttribute("partnerPropertyId");
			session.removeAttribute("selectedPropertyId");
			//session.invalidate();
		}
		
		
		HttpServletResponse response = ServletActionContext.getResponse();
		response.setHeader("Pragma","no-cache");
		response.setHeader("Cache-Control","no-store");
		response.setHeader("Cache-Control","no-cache");
		response.setHeader("Expires","0");
		response.setDateHeader("Expires",0);
		return "success";
	}
}
