package com.ulopms.security;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;
import java.security.SecureRandom;


public class passwordDecrypt {
	
	private static String ALGORITHM = "SHA-512";
	//private static String ALGORITHM = "scrypt";
	private static String ENCODING ="UTF-8";
	
	private static int TEMP_PASSWORD_LENGTH = 8;
	
	/**
	 * Another layer of protection against brute force attacks
	 * Will be useful if the DB is comprised but not the app server
	 */
	//private static String PUBLIC_SALT = "(asd80097ahsdfa9{";
	
	private static String PUBLIC_SALT = "(asd80097ahsdfa9{";
	//private static String PUBLIC_SALT = secrandom();
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String str=getDigest("test#123","sathish");
		
		
	}
	
	private static String getHashSaltFromUserName(String userName) {
		int length = userName.length();
		if(length == 1) {
			return userName; //return username
		} else {
			return userName.substring(1); //return everything but the 1st character
		}
	}
	
	
	  public static String getDigest(String pwd, String userName)
	    {
	    	
	    	MessageDigest md;
	    	
	    	
	    	//String privateSalt = passwordDecrypt.getHashSaltFromUserName(userName);
	    	
	    	//MessageDigest is not thread safe. Get a new instance for every request
	        try {
	        	md = MessageDigest.getInstance(ALGORITHM);
	        } catch (NoSuchAlgorithmException ex) {
	        	throw new RuntimeException("Password hashing failed: NoSuchAlgorithmException " + ALGORITHM);
	        }
	        
	        //add salt to defeat rainbow table attacks & birthday paradox attacks
	        //platform specific encoding is bad, use UTF-8 instead
	        try {
				md.update(PUBLIC_SALT.getBytes(ENCODING));
				md.update(pwd.getBytes(ENCODING));
			  //  md.update(privateSalt.getBytes(ENCODING));
			} catch (UnsupportedEncodingException e) {
				throw new RuntimeException("Password hashing failed: UnsupportedEncodingException " + ENCODING);
			}
	       
	 
	        byte byteData[] = md.digest();
	        //harden it more by hashing it a few more times
	        for (int i = 0; i < 10; i++) {
	        	md.reset();
	        	byteData = md.digest(byteData);
	        }
	        
	        //convert the byte to hex format method 1
	        StringBuffer sb = new StringBuffer();
	        for (int i = 0; i < byteData.length; i++) {
	         sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
	        }

	    	return sb.toString();
	    }
	  
	  public static String getDigestvalid(String pwd,String salt)
	    {
		  
		 
	    	
	    	MessageDigest md;
	    	
	    	
	    	//String privateSalt = passwordDecrypt.getHashSaltFromUserName(userName);
	    	
	    	//MessageDigest is not thread safe. Get a new instance for every request
	        try {
	        	md = MessageDigest.getInstance(ALGORITHM);
	        } catch (NoSuchAlgorithmException ex) {
	        	throw new RuntimeException("Password hashing failed: NoSuchAlgorithmException " + ALGORITHM);
	        }
	        
	        //add salt to defeat rainbow table attacks & birthday paradox attacks
	        //platform specific encoding is bad, use UTF-8 instead
	        try {
				md.update(salt.getBytes(ENCODING));
				md.update(pwd.getBytes(ENCODING));
			  //  md.update(privateSalt.getBytes(ENCODING));
			} catch (UnsupportedEncodingException e) {
				throw new RuntimeException("Password hashing failed: UnsupportedEncodingException " + ENCODING);
			}
	       
	 
	        byte byteData[] = md.digest();
	        //harden it more by hashing it a few more times
	        for (int i = 0; i < 10; i++) {
	        	md.reset();
	        	byteData = md.digest(byteData);
	        }
	        
	        //convert the byte to hex format method 1
	        StringBuffer sb = new StringBuffer();
	        for (int i = 0; i < byteData.length; i++) {
	         sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
	        }

	    	return sb.toString();
	    }
	  
	  
	  public static String getTempPassword() {
	    	return UUID.randomUUID().toString().substring(0, TEMP_PASSWORD_LENGTH);
	    }
	  
	  public static String secrandom()
	  {
		  String s ="";
		  try {
				 
	 	         
				 
		 	    // Create a secure random number generator using the SHA1PRNG algorithm
		 
		 	    SecureRandom secureRandomGenerator = SecureRandom.getInstance("SHA1PRNG");
		
		 	     
		
		 	    // Get 128 random bytes
		
		 	    byte[] randomBytes = new byte[128];
		
		 	    secureRandomGenerator.nextBytes(randomBytes);
		
		 	 
		 
		 	    // Create two secure number generators with the same seed
		 
		 	    int seedByteCount = 5;
		 
		 	    byte[] seed = secureRandomGenerator.generateSeed(seedByteCount);
		 
		 	 
		 
		 	    SecureRandom secureRandom1 = SecureRandom.getInstance("SHA1PRNG");
		 
		 	    secureRandom1.setSeed(seed);
		
		 	  /*SecureRandom secureRandom2 = SecureRandom.getInstance("SHA1PRNG");
		 
		 	    secureRandom2.setSeed(seed);
				*/
		 	   s = seed.toString();
		 	   
	 }
	 catch(NoSuchAlgorithmException  e)
	 {
		 
	 }
		  return s;
		  
	  }
	 
}