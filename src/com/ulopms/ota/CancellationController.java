package com.ulopms.ota;

import java.util.Collection;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.rest.DefaultHttpHeaders;
import org.apache.struts2.rest.HttpHeaders;

import com.opensymphony.xwork2.ModelDriven;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;

@Results({
    @Result(name="success", type="redirectAction", params = {"actionName" , "cancellation"})
})

public class CancellationController implements ModelDriven<Object> {
	
	 //private Collection<CancelledListing> list;
	 
	 

	private CancelledListing model = new CancelledListing();
	
	private List<CancelledListing> list;
	
    /* private Collection<CancelledItem> list1;*/
	 
	 private CancellationService cancellationService = new CancellationService();
	 
	 Integer statusCode;
	
	    
	 
	 
	/* @XStreamAlias("CancelledItem")
	    private CancelledItem model1 = new CancelledItem();*/
	 
	
	 
	/* @XStreamAlias("CancelledItem")
	    private CancelledItem cancelledItem;*/
	 
	// GET /orders/1
	    public HttpHeaders show() {
	        return new DefaultHttpHeaders("show");
	    }
	    
	    
	    
	    public HttpHeaders create() {
	    	 
	    	try{
	    		String strStatus=null;
	    		HttpServletResponse response = ServletActionContext.getResponse();
	        	//Booking booking = new Booking();
	        	/*XStream xstream = new XStream();
	        	xstream.processAnnotations(CancelledListing.class);
	            xstream.processAnnotations(CancelledItem.class);
	            xstream.addImplicitCollection(CancelledListing.class, "CancelledItem");*/
	            
	            
	            //CancelledListing b = (CancelledListing) xstream.fromXML(xml);
	            
	        	statusCode=cancellationService.add(model);
	        	response.setStatus(statusCode);
	        	
//	        	strStatus=bookingService.getOtaBookedDetails();
	    	}catch(Exception e){
	    		e.printStackTrace();
	    	}
	    	
	        return null;
	    }
	 
	 
	 public Object getModel() {
			// TODO Auto-generated method stub
			return (list != null ? list : model);
		}
	 
	/* public Object getModel1() {
			// TODO Auto-generated method stub
			return (list1 != null ? list1 : model1);
		}*/
	 
	 
	    public List<CancelledListing> getList() {
			return list;
		}

        public void setList(List<CancelledListing> list) {
			this.list = list;
		}

        public void setModel(CancelledListing model) {
			this.model = model;
		}


}
