package com.ulopms.ota;


import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

//import javax.xml.bind.annotation.XmlElement;
//import javax.xml.bind.annotation.XmlRootElement;

//import com.thoughtworks.xstream.annotations.XStreamAlias;

//@XStreamAlias("Booking")
//@XmlRootElement(name ="Booking")
public class Booking implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@JacksonXmlProperty(isAttribute = false, localName = "BookingId")
	private String BookingId;
	
	@JacksonXmlProperty(isAttribute = true, localName = "HotelCode")
	private String HotelCode;
	
	@JacksonXmlProperty(localName = "CustomerName")
	private String CustomerName;
	
	@JacksonXmlProperty(localName = "NoOfRooms")
	private long NoOfRooms;
	
	@JacksonXmlProperty(localName = "NoOfNights")
	private long NoOfNights;
	
	@JacksonXmlProperty(localName = "RoomTypeName")
	private String RoomTypeName;
	
	@JacksonXmlProperty(localName = "CheckInDate")
	private String CheckInDate;
	
	@JacksonXmlProperty(localName = "CheckIn")
	private String CheckIn;
	
	@JacksonXmlProperty(localName = "BookingTimeDate")
	private String BookingTimeDate;
	
	@JacksonXmlProperty(localName = "Status")
	private String Status;
	
	@JacksonXmlProperty(localName = "PayAtHotelFlag")
	private boolean PayAtHotelFlag;
	
	@JacksonXmlProperty(localName = "BookingTime")
	private String BookingTime;
	
	@JacksonXmlProperty(localName = "ArrivalDate")
	private Timestamp ArrivalDate;
	
	@JacksonXmlProperty(localName = "BookingVendorName")
	private String BookingVendorName;
	
	@JacksonXmlProperty(localName = "CancellationId")
	private long CancellationId;

	@JacksonXmlProperty(localName = "CancellationDate")
	private Date CancellationDate;
	
	@JacksonXmlProperty(localName = "BookingAmount")
	private Double BookingAmount;
	
	@JacksonXmlProperty(localName = "CancellationCharges")
	private Double CancellationCharges;
    
	@JacksonXmlProperty(localName = "RefundAmount")
	private Double RefundAmount;
	
	@JacksonXmlProperty(localName = "CancellationStatus")
	private String CancellationStatus;
	
	@JacksonXmlProperty(localName = "Rooms")
	private Integer Rooms;
	
	@JacksonXmlProperty(localName = "Nights")
	private Integer Nights;
	
	@JacksonXmlProperty(localName = "OtaName")
	private String OtaName;
	
	private List books = new ArrayList();
	
	public Booking(){	
		
		
	}
	
	
	public Booking(String customerName,String roomTypeName, Integer bookingId){
		
	}


	public String getBookingId() {
		return BookingId;
	}


	public void setBookingId(String BookingId) {
		BookingId = BookingId;
	}


	public String getHotelCode() {
		return HotelCode;
	}


	public void setHotelCode(String HotelCode) {
		HotelCode = HotelCode;
	}


	public String getCustomerName() {
		return CustomerName;
	}


	public void setCustomerName(String CustomerName) {
		CustomerName = CustomerName;
	}


	public long getNoOfRooms() {
		return NoOfRooms;
	}


	public void setNoOfRooms(long NoOfRooms) {
		NoOfRooms = NoOfRooms;
	}


	public long getNoOfNights() {
		return NoOfNights;
	}


	public void setNoOfNights(long NoOfNights) {
		NoOfNights = NoOfNights;
	}


	public String getRoomTypeName() {
		return RoomTypeName;
	}


	public void setRoomTypeName(String RoomTypeName) {
		RoomTypeName = RoomTypeName;
	}


	public String getCheckInDate() {
		return CheckInDate;
	}


	public void setCheckInDate(String CheckInDate) {
		CheckInDate = CheckInDate;
	}


	public String getCheckIn() {
		return CheckIn;
	}


	public void setCheckIn(String CheckIn) {
		CheckIn = CheckIn;
	}


	public String getBookingTimeDate() {
		return BookingTimeDate;
	}


	public void setBookingTimeDate(String BookingTimeDate) {
		BookingTimeDate = BookingTimeDate;
	}


	public String getStatus() {
		return Status;
	}


	public void setStatus(String Status) {
		Status = Status;
	}


	public boolean getPayAtHotelFlag() {
		return PayAtHotelFlag;
	}


	public void setPayAtHotelFlag(boolean PayAtHotelFlag) {
		PayAtHotelFlag = PayAtHotelFlag;
	}


	public String getBookingTime() {
		return BookingTime;
	}


	public void setBookingTime(String BookingTime) {
		BookingTime = BookingTime;
	}


	public Timestamp getArrivalDate() {
		return ArrivalDate;
	}


	public void setArrivalDate(Timestamp ArrivalDate) {
		ArrivalDate = ArrivalDate;
	}


	public String getBookingVendorName() {
		return BookingVendorName;
	}


	public void setBookingVendorName(String BookingVendorName) {
		BookingVendorName = BookingVendorName;
	}


	public long getCancellationId() {
		return CancellationId;
	}


	public void setCancellationId(long CancellationId) {
		CancellationId = CancellationId;
	}


	public Date getCancellationDate() {
		return CancellationDate;
	}


	public void setCancellationDate(Date CancellationDate) {
		CancellationDate = CancellationDate;
	}


	public Double getBookingAmount() {
		return BookingAmount;
	}


	public void setBookingAmount(Double BookingAmount) {
		BookingAmount = BookingAmount;
	}


	public Double getCancellationCharges() {
		return CancellationCharges;
	}


	public void setCancellationCharges(Double CancellationCharges) {
		CancellationCharges = CancellationCharges;
	}


	public Double getRefundAmount() {
		return RefundAmount;
	}


	public void setRefundAmount(Double RefundAmount) {
		RefundAmount = RefundAmount;
	}


	public String getCancellationStatus() {
		return CancellationStatus;
	}


	public void setCancellationStatus(String CancellationStatus) {
		CancellationStatus = CancellationStatus;
	}


	public Integer getRooms() {
		return Rooms;
	}


	public void setRooms(Integer Rooms) {
		Rooms = Rooms;
	}


	public Integer getNights() {
		return Nights;
	}


	public void setNights(Integer Nights) {
		Nights = Nights;
	}


	public String getOtaName() {
		return OtaName;
	}


	public void setOtaName(String OtaName) {
		OtaName = OtaName;
	}
	
	
	
	
	}