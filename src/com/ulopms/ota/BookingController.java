package com.ulopms.ota;

import java.util.Collection;

import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.rest.DefaultHttpHeaders;
import org.apache.struts2.rest.HttpHeaders;
import com.opensymphony.xwork2.ModelDriven;
//import com.sun.mail.iap.Response;
import com.thoughtworks.xstream.annotations.XStreamAlias;




@Results({
    @Result(name="success", type="redirectAction", params = {"actionName" , "booking"})
})




public class BookingController implements ModelDriven<Object>{

	
	private OtaBooking otaBooking = new OtaBooking();
    private String id;
    private Collection<Booking> list;
    private BookingService bookingService = new BookingService();
    Integer statusCode;
    
   /* static{
    	
    }*/
    
    @XStreamAlias("Booking")   
       
    private Booking model = new Booking();
   
    
    //xstream.aliasField("CancelledItem", Booking.class, "writer");
    
    /*@XStreamAlias("CancelledListing")
    private CancelledListing model1 = new CancelledListing(); */  
   
	

	// GET /orders/1
    public HttpHeaders show() {
        return new DefaultHttpHeaders("show");
    }

    // GET /orders
   /*public HttpHeaders index() {
        list = bookingService.getAll();
        return new DefaultHttpHeaders("index")
            .disableCaching();
    }*/
    
   /* @POST
    @Consumes(MediaType.APPLICATION_XML)
	 public HttpHeaders create(){
		 try{
			 HttpServletResponse response = ServletActionContext.getResponse();
			 response.setContentType("application/json");
			 Booking booking = new Booking();
			 
			 statusCode=bookingService.add(model);
			 
			 response.setStatus(200);
    	}
    	catch (Exception e) {
			e.printStackTrace();
			return null;
			
		}
		 return null;
	}*/
    
    
  
   
   public HttpHeaders create() {
    	 
    	try{
    		String strStatus=null;
    		HttpServletResponse response = ServletActionContext.getResponse();
        	Booking booking = new Booking();
        	statusCode=bookingService.add(model);
        	response.setStatus(statusCode);
        	
//        	strStatus=bookingService.getOtaBookedDetails();
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    	
        return null;
    }
   
   /*public HttpHeaders create1() {
  	 
   	try{
   		String strStatus=null;
   		HttpServletResponse response = ServletActionContext.getResponse();
   		CancelledListing cancel = new CancelledListing();
       	statusCode=bookingService.add1(model1);
       	response.setStatus(statusCode);
       	
//       	strStatus=bookingService.getOtaBookedDetails();
   	}catch(Exception e){
   		e.printStackTrace();
   	}
   	
       return null;
   }*/
	 
	
	 
	//@Override
	
	public Object getModel() {
		// TODO Auto-generated method stub
		return (list != null ? list : model);
	}
	
/*	public Object getModel1() {
		// TODO Auto-generated method stub
		return (list != null ? list : model1);
	}*/
	
	
	
}
