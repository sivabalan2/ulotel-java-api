package com.ulopms.ota;



import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the ota_cancellation database table.
 * 
 */
@Entity
@Table(name="ota_cancellation")
@NamedQuery(name="OtaCancellation.findAll", query="SELECT p FROM OtaCancellation p")
public class OtaCancellation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="id",sequenceName="ota_cancellation_ota_cancellation_details_id_seq")
	@GeneratedValue(generator="id")
	@Column(name="ota_cancellation_details_id", unique=true, nullable=false)
	private Integer otaCancellationDetailsId;

	@Column(name="ota_booking_id")
	private String otaBookingId;
	
	@Column(name="ota_cancellation_id")
	private String otaCancellationId;
		
	@Column(name="cancellation_date")
	private Date CancellationDate;

	@Column(name="cancellation_status")
	private String cancellationStatus;
	
	

	public Integer getOtaCancellationDetailsId() {
		return otaCancellationDetailsId;
	}

	public void setOtaCancellationDetailsId(Integer otaCancellationDetailsId) {
		this.otaCancellationDetailsId = otaCancellationDetailsId;
	}

	public String getOtaBookingId() {
		return otaBookingId;
	}

	public void setOtaBookingId(String otaBookingId) {
		this.otaBookingId = otaBookingId;
	}

	public String getOtaCancellationId() {
		return otaCancellationId;
	}

	public void setOtaCancellationId(String otaCancellationId) {
		this.otaCancellationId = otaCancellationId;
	}

	public Date getCancellationDate() {
		return CancellationDate;
	}

	public void setCancellationDate(Date cancellationDate) {
		CancellationDate = cancellationDate;
	}

	public String getCancellationStatus() {
		return cancellationStatus;
	}

	public void setCancellationStatus(String cancellationStatus) {
		this.cancellationStatus = cancellationStatus;
	}
	
	
	
	

	
	
}
