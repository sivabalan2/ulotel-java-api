package com.ulopms.ota;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.json.JSONObject;
import org.json.XML;






















import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.ulopms.controller.BookingDetailManager;
import com.ulopms.controller.BookingGuestDetailManager;
import com.ulopms.controller.OtaHotelsManager;
import com.ulopms.controller.PmsBookingManager;
import com.ulopms.controller.PmsDayManager;
import com.ulopms.controller.PmsGuestManager;
import com.ulopms.controller.PmsPropertyManager;
import com.ulopms.controller.PmsSourceManager;
import com.ulopms.controller.PmsStatusManager;
import com.ulopms.controller.PropertyAccommodationInventoryManager;
import com.ulopms.controller.PropertyAccommodationManager;
import com.ulopms.controller.PropertyDiscountManager;
import com.ulopms.controller.PropertyRateDetailManager;
import com.ulopms.controller.PropertyRateManager;
import com.ulopms.controller.PropertyRatePlanDetailManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.BookedDetail;
import com.ulopms.model.BookingDetail;
import com.ulopms.model.BookingGuestDetail;
import com.ulopms.model.OtaHotelDetails;
import com.ulopms.model.OtaHotels;
import com.ulopms.model.PmsAvailableRooms;
import com.ulopms.model.PmsBookedDetails;
import com.ulopms.model.PmsBooking;
import com.ulopms.model.PmsDays;
import com.ulopms.model.PmsGuest;
import com.ulopms.model.PmsProperty;
import com.ulopms.model.PmsSource;
import com.ulopms.model.PmsStatus;
import com.ulopms.model.PropertyAccommodation;
import com.ulopms.model.PropertyAccommodationInventory;
import com.ulopms.model.PropertyDiscount;
import com.ulopms.model.PropertyRate;
import com.ulopms.model.PropertyRateDetail;
import com.ulopms.model.PropertyRatePlanDetail;
import com.ulopms.model.User;
import com.ulopms.util.Email;
import com.ulopms.view.BookingDetailAction;

import freemarker.template.Configuration;
import freemarker.template.Template;



public class BookingService extends ActionSupport implements
ServletRequestAware,SessionAware, UserAware,ModelDriven<Object> {

	/*private static Map<String,Booking> booking = new HashMap<String,Booking>();
    private static int nextId = 6;
    static {
    	booking.put("33", new Booking("33", "Bob", 33));
    	booking.put("43", new Booking("43", "Sarah", 44));
    	booking.put("53", new Booking("53", "Jim", 66));
    }*/
    
    private List<PmsBookedDetails> bookedList;
    
    private List<BookedDetail> bookList;
    
    private static final Logger logger = Logger.getLogger(BookingService.class);
   

	/*public Booking get(String id) {
        return booking.get(id);
    }

    public List<Booking> getAll() {
        return new ArrayList<Booking>(booking.values());
    }*/
    
    private SessionMap<String,Object> sessionMap;
    
    private Long hotelCode;
	private int guestId;
	private Integer bookingId;
	private Integer accommodationId;
	private Integer propertyDiscountId;
	private Object propertyDiscountName;
	private String propertyName;
	private Integer propertyId;
	private String latitude;
	private String longitude;
	private String address1;
	private String address2;
	private String phoneNumber;
	private String propertyEmail;
	private String locationName;
	private String reservationManagerEmail;
	private String reservationManagerContact;
	private String routeMap;
	private String timeHours;
	private String sourceName;
	private Object arrivalDate;
	private Object departureDate;
	private String firstName;
	private String lastName;
	private String phone;
	private String emailId;
	private String specialRequest;
	private Integer days;
	private long roomCount;
	private Integer rooms;
	private long adults;
	private long child;
	private double taxGST;
	private double tax;
	private double amount;
	private double totalAmount;
	private double totalAdvanceAmount;
	private String paymentStatus;
	private double hotelPay;
	private double otaCommission;
	private double otaTax;
	private double sellAmount;
	private double nettAmount;
	private double payAtHotelAmount;
	private String checkedDays;
	int totalAdultCount = 0;
	int totalChildCount = 0;
	int totalInfantCount = 0;
	private double dblOtaCommission=0.0;
	private double dblOtaTax=0.0;
	private double dblTotalOtaAmout=0.0;
	private double dblRevenueAmount=0.0;
	private double dblTotalRevenueTaxAmount=0.0;
	private double dblCommission=0.0;
	private double dblUloCommission=0.0;
	private double dblUloTaxAmount=0.0;
	private double dblTotalUloCommission=0.0;
	private double dblHotelPayable=0.0;
	private double dblHotelTaxPayable=0.0;
	private double dblTax=0.0;

	private double txtGrandTotal;
	private boolean mailSource;
	private boolean mailFlag;
	private Integer sourceId;
	private long infant;
	private long roomCnt;
	  
	//getters and setters  
	  
	public void setSession(Map<String, Object> map) {  
	    sessionMap=(SessionMap)map;  
	}  
    
    public Integer add(Booking booking){
    	
    	
    	try{
    		SimpleDateFormat format1=new SimpleDateFormat("yyyy-MM-dd");
    		DateFormat format2=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    		OtaBookingManager otaBookingController=new OtaBookingManager();
        	OtaBooking otaBooking=new OtaBooking();
        	otaBooking.setOtaBookingId(booking.getBookingId());
        	otaBooking.setOtaCustomerName(booking.getCustomerName());
        	otaBooking.setOtaRooms(booking.getNights());
        	otaBooking.setOtaNights(booking.getNights());
        	otaBooking.setOtaRoomType(booking.getRoomTypeName());
        	otaBooking.setOtaHotelCode(booking.getHotelCode());
        	java.util.Date dateCheckIn = format1.parse(booking.getCheckIn());
		    java.util.Date dateBookingTime = format2.parse(booking.getBookingTimeDate());
		   
		    
		    Calendar calCheckStart=Calendar.getInstance();
		    calCheckStart.setTime(dateCheckIn);
		    java.util.Date checkInDate = calCheckStart.getTime();
		   
		   
		    Calendar calCheckEnd=Calendar.getInstance();
		    calCheckEnd.setTime(dateBookingTime);
		    java.util.Date checkOutDate = calCheckEnd.getTime();
		    Timestamp tsBookingTime=new Timestamp(checkOutDate.getTime());
		    
        	
        	otaBooking.setOtaArrivalDate(checkInDate);
        	otaBooking.setOtaBookingDate(tsBookingTime);
        	otaBooking.setOtaName(booking.getBookingVendorName());
        	otaBooking.setOtaFlag(booking.getPayAtHotelFlag());
        	otaBooking.setOtaStatus(booking.getStatus());
        	otaBookingController.add(otaBooking);
        	getOtaBookedDetails(booking.getBookingId(),booking.getHotelCode());
    	}catch(Exception e){
    		e.printStackTrace();
    	}finally{
    		
    	}
    	return 200;
    }   
 
    
    public void getOtaBookedDetails(String otaBookingId,String OtaHotelId){ 
		try{
			OtaHotelsManager otaController = new OtaHotelsManager(); 
//			HttpServletResponse response = ServletActionContext.getResponse();
			OtaHotels otaHotels =  otaController.getOtaHotelToken(OtaHotelId);
//			response.setContentType("application/json");
			HttpClient httpClient = new DefaultHttpClient();
			HttpPost postRequest = new HttpPost("https://in.goibibo.com/api/chmv2/getbookingdetail/?bearer_token="+otaHotels.getOtaBearerToken()+"&channel_token="+otaHotels.getOtaChannelToken()+"");
			String strXMLBodyFormat="<?xml version='1.0' encoding='UTF-8' ?>"+
					" <Website Name='ingoibibo' HotelCode='"+OtaHotelId+"' Version='2'>"+
						"<BookingId>"+otaBookingId+"</BookingId>"+
						"<PaymentDetailsRequired>True</PaymentDetailsRequired>"+
						"<FetchBookingAddOns>True</FetchBookingAddOns>"+
					"</Website>";
			
			
			StringEntity input = new StringEntity(strXMLBodyFormat);
			input.setContentType("text/json");
			postRequest.setEntity(input);
			HttpResponse httpResponse = httpClient.execute(postRequest);
			HttpEntity entity = httpResponse.getEntity();
            // Read the contents of an entity and return it as a String.
            String content = EntityUtils.toString(entity);
            
            JSONObject bookingDetailJsonObject = XML.toJSONObject(content);
            //response.getWriter().write("{\"data\":[" + bookingDetailJsonObject + "]}");
            
            
            JSONObject jsonValues = (JSONObject) bookingDetailJsonObject.get("BookingDetail");
            JSONObject jsonValues1 = (JSONObject) jsonValues.get("Booking");
            JSONObject jsonValues2 = (JSONObject) jsonValues1.get("PriceDetails");
            JSONObject jsonValues3 = (JSONObject) jsonValues1.get("RoomStay");
            JSONObject jsonValues4 = (JSONObject) jsonValues3.get("Room");
            //JSONObject jsonBookingId = (JSONObject) jsonValues2.get("SellAmount");
			String val = jsonValues1.getString("CheckInDate");
			DateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
			
			java.util.Date arrivalDate = outputFormat.parse(jsonValues1.getString("CheckInDate"));
			java.util.Date departureDate = outputFormat.parse(jsonValues1.getString("CheckoutDate"));
			Timestamp aDate =new Timestamp(arrivalDate.getTime());
			Timestamp dDate=new Timestamp(departureDate.getTime());
			PmsPropertyManager propertyController = new PmsPropertyManager();
			PmsBookingManager bookingController = new PmsBookingManager();
			OtaHotelsManager otaHotelsController = new OtaHotelsManager();
			PropertyAccommodationInventoryManager accommodationInventoryController = new PropertyAccommodationInventoryManager();
			PmsGuestManager guestController = new PmsGuestManager();
			BookingDetailManager bookingDetailController = new BookingDetailManager();
			BookingGuestDetailManager bookingGuestDetailController = new BookingGuestDetailManager();
			PmsStatusManager statusController = new PmsStatusManager();
			PmsSourceManager sourceController = new PmsSourceManager();
			PropertyDiscountManager discountController = new PropertyDiscountManager();
			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
			PmsBooking booking = new PmsBooking();
			OtaHotelDetails otahotelDetail = otaHotelsController.findPropertyId(jsonValues1.getLong("HotelCode"),jsonValues1.getLong("RoomTypeCode"));
			nettAmount=jsonValues2.getDouble("NettAmount");
			sellAmount=jsonValues2.getDouble("SellAmount");
			otaCommission=sellAmount-nettAmount;
			otaTax=otaCommission*18/100;
			payAtHotelAmount=jsonValues2.getDouble("TotalPayAtHotelAmount");
			tax=jsonValues2.getDouble("GST");
			amount=sellAmount-tax;
			totalAdvanceAmount=sellAmount-payAtHotelAmount;
			SimpleDateFormat formattime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			java.util.Date otaBookingDate = formattime.parse(jsonValues1.getString("BookingDate"));
		    Timestamp currentTS = new Timestamp(otaBookingDate.getTime());
		    
		    
			booking.setArrivalDate(aDate);
			booking.setDepartureDate(dDate);
			booking.setCreatedDate(currentTS);
			booking.setRooms(jsonValues1.getInt("NumberofRooms"));
			booking.setIsActive(true);
			booking.setIsDeleted(false);
			booking.setTotalAmount(amount);
			booking.setTotalTax(tax);
			booking.setOtaCommission(otaCommission);
			booking.setOtaTax(otaTax);
			booking.setAdvanceAmount(totalAdvanceAmount);
	 		booking.setOtaBookingId(otaBookingId);
			booking.setTotalActualAmount(jsonValues2.getDouble("SellAmount"));
			
			PmsSource source = sourceController.find(jsonValues1.getString("BookingVendorName"));
			
			booking.setPmsSource(source);
			PmsProperty property = propertyController.find(otahotelDetail.getPmsProperty().getPropertyId());
			booking.setPmsProperty(property);
			PmsStatus status = statusController.find(2);
			booking.setPmsStatus(status);
			booking.setPaynowDiscount(0);
			//booking.setInvoiceId();
			PmsBooking book = bookingController.add(booking);
			this.bookingId = book.getBookingId();
			/*sessionMap.put("bookingId",bookingId);
			
			this.bookingId = (Integer) sessionMap.get("bookingId");*/
			
			/*add guest table*/
			
			PmsGuest guest = new PmsGuest();
			
			
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			String emailId=null,phoneNo=null;
			/*emailId=jsonValues1.getString("GuestEmail");
			phoneNo=jsonValues1.getString("GuestPhoneNo");
			if(emailId.equals("NA")){
				emailId="guestsupport@ulohotels.com";
			}
			if(phoneNo.equals("NA")){
				phoneNo="9543592593";
			} */
			
			emailId="vmohankpm@gmail.com";
			phoneNo="9543592593";
			
			guest.setEmailId(emailId);  /*jsonValues1.getString("GuestEmail")*/
			guest.setFirstName(jsonValues1.getString("GuestName")); 
			guest.setIsActive(true);
			guest.setIsDeleted(false);
			guest.setPhone(phoneNo); /*jsonValues1.getString("GuestPhoneNo")*/
			
		    guest.setCreatedDate(currentTS);
		    
		    PmsGuest guestli = guestController.add(guest);
			
			this.guestId = guestli.getGuestId();
			/*sessionMap.put("guestId",guestId);*/	
			
			/*add booking detail table*/ 
			
			
			 BookingGuestDetail bookingGuestDetail = new BookingGuestDetail();
             PmsBooking bookingg = bookingController.find(getBookingId());
             bookingGuestDetail.setPmsBooking(bookingg);
             PmsGuest guestId = guestController.find(getGuestId());
             bookingGuestDetail.setPmsGuest(guestId);
             bookingGuestDetail.setIsPrimary(true);
             bookingGuestDetail.setIsActive(true);
             bookingGuestDetail.setIsDeleted(false);
            /* if(preBookFlag){
             	bookingGuestDetail.setIsActive(false);
                 bookingGuestDetail.setIsDeleted(true);
             }else{
             	bookingGuestDetail.setIsActive(true);
                 bookingGuestDetail.setIsDeleted(false);
             }*/
             
             bookingGuestDetail.setSpecialRequest(jsonValues1.getString("SpecialRequest"));
             bookingGuestDetailController.add(bookingGuestDetail);
             
             Calendar cal = Calendar.getInstance();    
             cal.setTime(departureDate);
             cal.add(Calendar.DATE, -1);
            // String startDate = new SimpleDateFormat("yyyy-MM-dd").format(this.arrivalDate);
             String strEndDate = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
			
			DateTime start = DateTime.parse(jsonValues1.getString("CheckInDate"));
            DateTime end = DateTime.parse(strEndDate);
            DateTime startDate = DateTime.parse(jsonValues1.getString("CheckInDate"));
            DateTime endDate = DateTime.parse(jsonValues1.getString("CheckoutDate"));
	        java.util.Date fromdate = startDate.toDate();
	 		java.util.Date todate = endDate.toDate();
	 		
			this.days = (int) ((todate.getTime() - fromdate.getTime()) / (1000 * 60 * 60 * 24));
            
             List<DateTime> between = getDateRange(start, end);
             for (DateTime d : between)
             {
            
            	java.util.Date bookingDate=  d.toDate();              
                 BookingDetail bookingDetail = new BookingDetail();
                
                
                 nettAmount=jsonValues2.getDouble("NettAmount");
     			 sellAmount=jsonValues2.getDouble("SellAmount");
     			 otaCommission=sellAmount-nettAmount;
     			 otaTax=otaCommission*18/100;
     			 payAtHotelAmount=jsonValues2.getDouble("TotalPayAtHotelAmount");
     			 tax=jsonValues2.getDouble("GST");
     			 amount=sellAmount-tax;
     			 totalAdvanceAmount=sellAmount-payAtHotelAmount;
     			
     			 amount=amount/days;
     			 
                 bookingDetail.setAdultCount(jsonValues4.getInt("Adult"));
                 bookingDetail.setChildCount(jsonValues4.getInt("Child"));
                 bookingDetail.setRefund(0);
                 bookingDetail.setAdvanceAmount(totalAdvanceAmount); 	
                 bookingDetail.setAmount(amount);
                 bookingDetail.setTax(tax);  	
                 bookingDetail.setOtaCommission(otaCommission);
                 bookingDetail.setOtaTax(otaTax);
                 /*if(this.userId!=null){
                 	bookingDetail.setCreatedBy(this.userId);
                 }*/
                 bookingDetail.setCreatedDate(currentTS);
                 bookingDetail.setBookingDate(bookingDate);
                 bookingDetail.setIsActive(true);
                 bookingDetail.setIsDeleted(false);
                 PmsStatus statusId = statusController.find(2);
                 bookingDetail.setPmsStatus(statusId);
                 PropertyAccommodation propertyAccommodation = accommodationController.find(otahotelDetail.getPropertyAccommodation().getAccommodationId());

                 bookingDetail.setPropertyAccommodation(propertyAccommodation);
                 PmsBooking booking1 = bookingController.find(getBookingId());
                 bookingDetail.setPmsBooking(booking1);
                 //bookingDetail.setRandomNo(array.get(i).getRandomNo());
                 //bookingDetail.setInfantCount(array.get(i).getInfantCount());
                 bookingDetail.setRoomCount(jsonValues1.getInt("NumberofRooms"));
                 bookingDetail.setActualAmount(Math.round(jsonValues2.getDouble("SellAmount")));
                 
                
                 	
                 	
                 	PropertyAccommodationInventory inventoryCount = accommodationInventoryController.findInventoryCount(bookingDate,otahotelDetail.getPropertyAccommodation().getAccommodationId());
                 	 if(inventoryCount != null){ /*inventoryCount.getInventoryId()*/
                 	PropertyAccommodationInventory inventory = accommodationInventoryController.find(inventoryCount.getInventoryId());
                     bookingDetail.setPropertyAccommodationInventory(inventory);
                 	
                 	}
                
                 // for promotion
                 
                               	
                 	int frontDiscountId = 35;
                 	
                 		PropertyDiscount discount = discountController.find(frontDiscountId);
                        bookingDetail.setPropertyDiscount(discount);

                        bookingDetailController.add(bookingDetail);                
             }
             getBookedDetails(this.bookingId);
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
    
    public void getBookedDetails(int bookingId) throws IOException {
		
		 try {

			String jsonOutput = "";
			//HttpServletResponse response = ServletActionContext.getResponse();
			DecimalFormat df = new DecimalFormat("###.##");
			PmsPropertyManager propertyController = new PmsPropertyManager();
			PropertyAccommodationManager propertyAccommodationController = new PropertyAccommodationManager();
			BookingDetailManager bookingDetailController = new BookingDetailManager();
			BookingGuestDetailManager bookingGuestDetailController = new BookingGuestDetailManager();
			PmsGuestManager guestController = new PmsGuestManager();
			PmsStatusManager statusController = new PmsStatusManager();
			PropertyDiscountManager propertyDiscountController = new PropertyDiscountManager();  
			PmsBookingManager bookingController = new PmsBookingManager();
			
			//response.setContentType("application/json");
			this.bookedList = bookingDetailController.bookedList(bookingId);
			
			StringBuilder accommodationType = new StringBuilder();
			
			StringBuilder accommodationRate = new StringBuilder();
			
			
			//this.totalInfantCount = (Integer) sessionMap.get("totalInfantCount");
			
			
			StringBuilder smsAccommodationTypes = new StringBuilder();
			List accommodationBooked = new ArrayList();
            for (PmsBookedDetails booked : bookedList) {
            	this.bookingId = getBookingId();
            	this.accommodationId=booked.getAccommodationId();
            	this.propertyDiscountName = "NA";
            	PmsProperty pmsProperty = propertyController.find(booked.getPropertyId());
            	this.propertyName = pmsProperty.getPropertyName();//resort name and location name is missing
            	this.accommodationId = booked.getAccommodationId();
            	this.propertyId = pmsProperty.getPropertyId();
            	this.latitude = pmsProperty.getLatitude();
            	this.longitude = pmsProperty.getLongitude();
            	this.address1=pmsProperty.getAddress1();//property address1
            	this.address2=pmsProperty.getAddress2();//property address2
            	this.phoneNumber = pmsProperty.getPropertyContact();//property phone number
            	this.propertyEmail = pmsProperty.getPropertyEmail();
            	this.locationName=pmsProperty.getLocation().getLocationName();//property location
            	this.reservationManagerEmail=pmsProperty.getReservationManagerEmail();
            	this.reservationManagerContact = pmsProperty.getReservationManagerContact();
            	this.routeMap = pmsProperty.getRouteMap();
            	java.util.Date date=new java.util.Date();
            	Calendar calDate=Calendar.getInstance();
            	calDate.setTime(booked.getCreatedDate());
            	calDate.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
            	int bookedHours=calDate.get(Calendar.HOUR);
            	int bookedMinute=calDate.get(Calendar.MINUTE);
            	int bookedSecond=calDate.get(Calendar.SECOND);
            	int AMPM=calDate.get(Calendar.AM_PM);
            	
            	String strBookingDate=new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH).format(calDate.getTime());
            	String hours="",minutes="",seconds="";
            	if(bookedHours<10){
            		hours=String.valueOf(bookedHours);
            		hours="0"+hours;
            	}else{
            		hours=String.valueOf(bookedHours);
            	}
            	if(bookedMinute<10){
            		minutes=String.valueOf(bookedMinute);
            		minutes="0"+minutes;
            	}else{
            		minutes=String.valueOf(bookedMinute);
            	}
            	
            	if(bookedSecond<10){
            		seconds=String.valueOf(bookedSecond);
            		seconds="0"+seconds;
            	}else{
            		seconds=String.valueOf(bookedSecond);
            	}
            	if(AMPM==0){//If the current time is AM
            		this.timeHours=strBookingDate+" "+hours+" : "+minutes+" : "+seconds+" AM ";
            	}else if(AMPM==1){//If the current time is PM
            		this.timeHours=strBookingDate+" "+hours+" : "+minutes+" : "+seconds+" PM ";
            	}
            	
       			this.sourceName = "Online";
            	this.arrivalDate = booked.getArrivalDate();
            	this.departureDate = booked.getDepartureDate();
            	PmsGuest guest = guestController.find(booked.getGuestId());
            	this.firstName = guest.getFirstName();
            	this.lastName = guest.getLastName();
            	this.phone = guest.getPhone();
            	this.emailId = guest.getEmailId();
            	this.specialRequest = booked.getSpecialRequest();
            	
            	String checkIn = new SimpleDateFormat("MM/dd/yyyy").format(this.arrivalDate);
				String checkOut = new SimpleDateFormat("MM/dd/yyyy").format(this.departureDate);
				
				LocalDate date1 = LocalDate.parse(checkIn, DateTimeFormat.forPattern("MM/dd/yyyy"));
                LocalDate date2 = LocalDate.parse(checkOut, DateTimeFormat.forPattern("MM/dd/yyyy"));
                this.days = Days.daysBetween(date1, date2).getDays();
            	
             this.bookList =  bookingDetailController.listAccommodation(getBookingId(),booked.getAccommodationId());
             if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				
				else
					jsonOutput += "{";
             
             	String jsonTypes="";
         		jsonOutput += "\"bookingid\":\"" + getBookingId().toString() + "\"";
         	
         		for (BookedDetail bookedDetail : bookList) {
            		
	            	PropertyAccommodation accommodation = propertyAccommodationController.find(bookedDetail.getAccommodationId());
	            	this.roomCount = bookedDetail.getRoomCount()/this.days;
	            	accommodationBooked.add(new BookedDetail(accommodation.getAccommodationType(),bookedDetail.getTax(),
	            			this.roomCount,bookedDetail.getAdultCount(),bookedDetail.getChildCount() ,bookedDetail.getAmount())); 
	            	 
	            	if (!jsonTypes.equalsIgnoreCase(""))
						jsonTypes += ",{";
	            	
					else
						jsonTypes += "{";
            		
	            	
	            	smsAccommodationTypes.append(accommodation.getAccommodationType().trim());
	            	smsAccommodationTypes.append(",");
            		jsonTypes += "\"accommodationType\":\"" + accommodation.getAccommodationType().trim()+ "\"";
            		jsonTypes += ",\"rooms\":\"" + this.roomCount+ "\"";
            		jsonTypes += ",\"adultCount\":\"" + bookedDetail.getAdultCount()+ "\"";
            		jsonTypes += ",\"childCount\":\"" + bookedDetail.getChildCount()+ "\"";
            		jsonTypes += ",\"amount\":\"" + bookedDetail.getAmount()+ "\"";
            		jsonTypes += ",\"tax\":\"" + bookedDetail.getTax()+ "\"";
            		jsonTypes += ",\"total\":\"" + (bookedDetail.getTax() + bookedDetail.getAmount())  + "\"";
            		jsonTypes += "}";
            		
         	    }
         		jsonOutput += ",\"types\":[" + jsonTypes+ "]";
            	
            	
            	this.rooms = booked.getRooms();
            	this.adults = booked.getAdults();
            	//this.infant = this.totalInfantCount;
            	this.child = booked.getChild();
            	this.tax = booked.getTax();
            	this.amount = booked.getAmount();
            	this.totalAmount = this.tax + this.amount;
            	String strTotal=df.format(this.totalAmount);
            	this.totalAmount=Double.valueOf(strTotal);
            	
                this.totalAdvanceAmount = booked.getAdvanceAmount();
                if(this.totalAdvanceAmount == 0){
                	this.hotelPay = 0;
            	}
            	else{
            		this.hotelPay = (this.totalAmount-this.totalAdvanceAmount);
            	}
                this.hotelPay = (this.totalAmount-this.totalAdvanceAmount);
                if(this.totalAmount == booked.getAdvanceAmount()){
               		this.paymentStatus = "Paid";
               	}
               	else{
               		if(this.mailSource == true){
               			if(this.sourceId==12){
               				if(this.totalAmount == booked.getAdvanceAmount()){
           	            		this.paymentStatus = "Paid";
           	            	}else{
           	            		this.paymentStatus = "Partially Paid";
           	            	}
               			}else{
               				this.paymentStatus = "Paid";
               			}
    	            	}else{
    	            		this.paymentStatus = "Partially Paid";
    	            	}
               	}
                jsonOutput += ",\"propertyName\":\"" + this.propertyName+ "\"";
	            jsonOutput += ",\"locationName\":\"" + this.locationName+ "\"";
				jsonOutput += ",\"address1\":\"" + this.address1+ "\"";
				jsonOutput += ",\"address2\":\"" + this.address2+ "\"";
				jsonOutput += ",\"phoneNumber\":\"" + this.phoneNumber+ "\"";
				jsonOutput += ",\"propertyEmail\":\"" + this.propertyEmail+ "\"";
				jsonOutput += ",\"reservationManagerEmail\":\"" + this.reservationManagerEmail+ "\"";
				jsonOutput += ",\"reservationManagerContact\":\"" + this.reservationManagerContact+ "\"";
				
				
            	String CheckIn = new SimpleDateFormat("MMM d,yyyy").format(this.arrivalDate);// check in 
                String CheckOut = new SimpleDateFormat("MMM d,yyyy").format(this.departureDate);//check out
                
                jsonOutput += ",\"checkIn\":\"" + CheckIn+ "\"";
				jsonOutput += ",\"checkOut\":\"" + CheckOut+ "\"";
				jsonOutput += ",\"timeHours\":\"" + this.timeHours+ "\"";
				jsonOutput += ",\"days\":\"" + days + "\"";
				jsonOutput += ",\"guestname\":\"" + this.firstName+ "\"";
                 String strFirstName="",firstNameLetterUpperCase="";
				String strLastName="",lastNameLetterUpperCase="",strFullName="";
				strFirstName=this.firstName;
				strLastName=this.lastName;
				if(strFirstName!=null && strFirstName!="" && !strFirstName.equalsIgnoreCase("undefined")){
					strFirstName=strFirstName.trim();
					firstNameLetterUpperCase=strFirstName.substring(0, 1).toUpperCase()+strFirstName.substring(1);
					strFirstName=firstNameLetterUpperCase;
				}else{
					strFirstName="-";
				}
				if(strLastName!=null && strLastName!=""  && !strLastName.equalsIgnoreCase("undefined")){
					strLastName=strLastName.trim();
					lastNameLetterUpperCase=strLastName.substring(0, 1).toUpperCase() +  strLastName.substring(1);
					strLastName=lastNameLetterUpperCase;
				}else{
					strLastName="";
				}
				strFullName=strFirstName+" "+strLastName;
				
				 String promotionName = null;
				    if(promotionName==null || promotionName==""){
				    	promotionName="NA";
				    }
				    /*if(this.propertyDiscountId == 35){
				    	this.propertyDiscountName = "NA";
				    }*/
				
				jsonOutput += ",\"guestname\":\"" + strFullName+ "\"";
				jsonOutput += ",\"sourcename\":\"" + this.sourceName+ "\"";
				jsonOutput += ",\"paymentstatus\":\"" + this.paymentStatus+ "\"";
				jsonOutput += ",\"specialrequest\":\"" + this.specialRequest+ "\"";
				jsonOutput += ",\"promotionName\":\"" + promotionName + "\"";
				jsonOutput += ",\"propertyDiscountName\":\"" + "50%" + "\"";
				jsonOutput += ",\"mobile\":\"" + this.phone+ "\"";
				jsonOutput += ",\"emailId\":\"" + this.emailId+ "\"";
				jsonOutput += ",\"rooms\":\"" + this.rooms.toString()+ "\"";
				jsonOutput += ",\"adults\":\"" + String.valueOf(this.adults)+ "\"";
				jsonOutput += ",\"child\":\"" + String.valueOf(this.child)+ "\"";
				jsonOutput += ",\"tax\":\"" + df.format(this.tax) + "\"";
				jsonOutput += ",\"amount\":\"" + df.format(this.amount)+ "\"";
				jsonOutput += ",\"totalamount\":\"" + df.format(this.totalAmount)+ "\"";
				//jsonOutput += ",\"totaladvanceamount\":\"" + Double.valueOf(this.totalAdvanceAmount)+ "\"";
				
				/*if(getDiscountId()!=null){
					jsonOutput += ",\"totalamount\":\"" + String.valueOf(this.txtGrandTotal)+ "\"";
				}else{
					jsonOutput += ",\"totalamount\":\"" + String.valueOf(this.totalAmount)+ "\"";	
				}*/
				
				
				
				
				jsonOutput += "}";

			}
           
                String checkIn = new SimpleDateFormat("MM/dd/yyyy").format(this.arrivalDate);
			    String checkOut = new SimpleDateFormat("MM/dd/yyyy").format(this.departureDate);
			    String promotionName = "NA";
			   
			    String strFullName="";
			    this.mailFlag=true; 
            if(this.mailFlag == true)
            {

 				//email template
 				Configuration cfg = new Configuration();
 				cfg.setClassForTemplateLoading(BookingService.class, "../../../");
 				Template template = cfg.getTemplate(getText("reservation.template"));
 				Map<String, Object> rootMap = new HashMap<String, Object>();
 				//Map<String, String> rootMap = new HashMap<String, String>();	 				
 				rootMap.put("bookingid",getBookingId().toString());
 				rootMap.put("propertyname",this.propertyName);
 				rootMap.put("propertyid",this.propertyId);
 				rootMap.put("latitude",this.latitude.trim());
 				rootMap.put("longitude",this.longitude.trim());
                rootMap.put("sourcename",this.sourceName);
 				rootMap.put("paymentstatus",this.paymentStatus);
 				rootMap.put("propertyDiscountName",this.propertyDiscountName);
 				rootMap.put("reservationManagerEmail",this.reservationManagerEmail);
 				rootMap.put("reservationManagerContact",this.reservationManagerContact);
 				rootMap.put("propertyEmail",this.propertyEmail);
 				rootMap.put("specialrequest",this.specialRequest);
 				rootMap.put("guestname",this.firstName);
 				rootMap.put("locationName",this.locationName);
 				rootMap.put("address1",this.address1);
 				rootMap.put("address2",this.address2);
 				rootMap.put("hotelpay",0);
 				rootMap.put("phoneNumber",this.phoneNumber);
 				rootMap.put("checkIn", checkIn);
 				rootMap.put("checkOut",checkOut);
 				rootMap.put("timeHours",this.timeHours);
 				
 				String strFirstName="",firstNameLetterUpperCase="";
				String strLastName="",lastNameLetterUpperCase="";
				
				strFirstName=this.firstName;
				strLastName=this.lastName;
				if(strFirstName!=null && strFirstName!="" && !strFirstName.equalsIgnoreCase("undefined")){
					strFirstName=strFirstName.trim();
					firstNameLetterUpperCase=strFirstName.substring(0, 1).toUpperCase()+strFirstName.substring(1);
					strFirstName=firstNameLetterUpperCase;
				}else{
					strFirstName="-";
				}
				if(strLastName!=null && strLastName!=""  && !strLastName.equalsIgnoreCase("undefined")){
					strLastName=strLastName.trim();
					lastNameLetterUpperCase=strLastName.substring(0, 1).toUpperCase() +  strLastName.substring(1);
					strLastName=lastNameLetterUpperCase;
				}else{
					strLastName="";
				}
				strFullName=strFirstName+" "+strLastName;
 				rootMap.put("guestname",strFullName);
 				//rootMap.put("guestname",this.firstName + " " + this.lastName);
 				rootMap.put("mobile",this.phone);
 				rootMap.put("emailId",this.emailId);	 				
 				rootMap.put("checkin",checkIn);
 				rootMap.put("days",this.days.toString());
 				rootMap.put("checkout",checkOut);
 				rootMap.put("promotionName",promotionName);	 				
 				
            	
 				rootMap.put("accommodationBooked", accommodationBooked);
 				//rootMap.put("rooms", this.roomCount);
 				rootMap.put("adults", String.valueOf(this.adults));
 				rootMap.put("child",String.valueOf(this.child));
 				rootMap.put("infant",2);
 				rootMap.put("tax",df.format(this.tax));
 				rootMap.put("amount",df.format(this.amount));
 				rootMap.put("totalamount",df.format(this.totalAmount));
 				rootMap.put("totaladvanceamount",df.format(this.totalAdvanceAmount));
 				
 				//rootMap.put("totalamount",String.valueOf(this.txtGrandTotal));
 				
 				rootMap.put("from", getText("notification.from"));
 				Writer out = new StringWriter();
 				template.process(rootMap, out);

 				String strReservationMailId=null,strPropertyMailId=null,strResortMailId=null,strContractMailId=null,strRevenueMailId=null;
 				PmsPropertyManager pmsPropertyController=new PmsPropertyManager();
 				for (PmsBookedDetails booked : bookedList) {
 					PmsProperty pmsProperty=pmsPropertyController.find(booked.getPropertyId());
 					
 					strReservationMailId=pmsProperty.getReservationManagerEmail();
 					strResortMailId=pmsProperty.getResortManagerEmail();
 					strPropertyMailId=pmsProperty.getPropertyEmail();
 					strRevenueMailId=pmsProperty.getRevenueManagerEmail();
 					strContractMailId=pmsProperty.getContractManagerEmail();
 				}
 				String strSubject="Hotel Confirmation for Booking Id "+getBookingId().toString()+" "+this.propertyName;
 				//send email
 				Email em = new Email();
 				em.set_to(this.emailId);
 				if(strReservationMailId!=null && strContractMailId!=null && strRevenueMailId!=null){
 					em.set_cc(strContractMailId);
	 				em.set_cc2(strRevenueMailId);
	 				em.set_cc3(strReservationMailId);
	 				em.set_cc4(getText("bookings.notification.email"));
	 				em.set_cc5(getText("operations.notification.email"));
	 				em.set_cc6(getText("finance.notification.email"));
 				}else{
 					em.set_cc(strContractMailId);
	 				em.set_cc2(strRevenueMailId);
	 				em.set_cc3(strReservationMailId);
	 				em.set_cc4(getText("bookings.notification.email"));
	 				em.set_cc5(getText("operations.notification.email"));
	 				em.set_cc6(getText("finance.notification.email"));
 				}
 				
 				em.set_from(getText("email.from"));
 				//em.set_host(getText("email.host"));
 				//em.set_password(getText("email.password"));
 				//em.set_port(getText("email.port"));
 				em.set_username(getText("email.username"));
 				em.set_subject(strSubject);
 				em.set_bodyContent(out);
 				em.send();		
 				
            	
            	
            	getHotelierBookedDetails();
            	
            	String message = "&sms_text=" + "Confirmation from Ulo Hotels"+
    					
    					"Booking ID - "+ getBookingId().toString() +
    					 "Guest Name � "+ strFullName  + 
    					 "Property Name �"+ this.propertyName +
    					"Room Category/s �"+  smsAccommodationTypes.toString()  +
    					 "No. of Adults �"+ this.adults +
    					 "No. of Kids �"+ this.child +
    					 "Check-in Date-"+ checkIn + 
    					"Check out Date-"+ checkOut +
    				   "Contact Number � "+ this.phone;

    			//String sender = "&sender=" + "TXTLCL";
    			String numbers = "&sms_to=" + "+91"+this.phone;
    			String from = "&sms_from=" + "ULOHTL";
    			String type = "&sms_type=" + "trans"; 
    			// Send data
    			HttpURLConnection conn = (HttpURLConnection) new URL("http://etsrds.kapps.in/webapi/ulohotels/api/ulohotels_sms_api.py").openConnection();
    			//"http://etsrds.kapps.in/webapi/ulohotels/api/ulohotels_sms_api.py?sms_text=hi%20this%20gopinath%20from%20ulo&sms_to=%20918220211068&sms_from=ULOHTL&sms_type=promo"
    			String data = numbers + message +  from + type;
    			conn.setDoOutput(true);
    			conn.setRequestMethod("POST");
    			conn.addRequestProperty("auth_key", "Basic dWxvaG90ZWxzX1Q6VUxPSFRMQDIwMTg=");
    			conn.setRequestProperty("Content-Length", Integer.toString(data.length()));
    			conn.getOutputStream().write(data.getBytes("UTF-8"));
    			final BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
    			final StringBuffer stringBuffer = new StringBuffer();
    			String line;
    			
//    			while ((line = rd.readLine()) != null) {
//    				stringBuffer.append(line);
//    				
//    			}
    			rd.close();
    			
    			getRouteMapSms(this.routeMap,this.phone);
    			
    			updateOtaInventory(this.bookingId,this.accommodationId,this.propertyId);
 		     
            }
            
			//response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

//		return null;
	}
    
    public void  updateOtaInventory(int bookingid,int accommodationId,int propertyId) throws Exception{
    	
    	try{
    		
    		OtaHotelsManager otaHotelsController = new OtaHotelsManager();
    		PmsBookingManager bookingController = new PmsBookingManager();
    		PropertyRateManager rateController=new PropertyRateManager();
    		PropertyRateDetailManager rateDetailController= new PropertyRateDetailManager();
    		PropertyAccommodationInventoryManager inventoryController = new PropertyAccommodationInventoryManager();
    		PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();  		
       	 	PropertyAccommodation accommodation1 = accommodationController.find(accommodationId);
       	 	PmsBooking booking = bookingController.find(bookingid);
       	 	DateFormat f = new SimpleDateFormat("EEEE");
       	 	String startDate = new SimpleDateFormat("yyyy-MM-dd").format(booking.getArrivalDate());
       	 	String endDate = new SimpleDateFormat("yyyy-MM-dd").format(booking.getDepartureDate());
         
       	 	DateTime start = DateTime.parse(startDate);
       	 	DateTime end = DateTime.parse(endDate);
       	 	List<DateTime> between = getDateRange(start, end);
       	 	for (DateTime d : between)
       	 	{
       	 		 java.util.Date availDate = d.toDate();
				 PropertyAccommodationInventory inventoryDetails = inventoryController.findInventoryCount(availDate,accommodationId);
				 long rmc = 0;
	   			 this.roomCnt = rmc;
	        	 int sold=0;
				 if(inventoryDetails != null){
				 PmsAvailableRooms roomCount1 = bookingController.findBookingCount(availDate,accommodationId,inventoryDetails.getInventoryId());
				 if(roomCount1.getRoomCount() == null) {
				 	String num1 = inventoryDetails.getInventoryCount();
					PmsAvailableRooms bookingRoomCount=bookingController.findCount(availDate, accommodationId);
					Integer totavailable=0;
					if(accommodation1!=null){
						totavailable=accommodation1.getNoOfUnits();
					}
					if(bookingRoomCount.getRoomCount()!=null){
						long lngRooms=bookingRoomCount.getRoomCount();
						long num2 = Long.parseLong(num1);
						this.roomCnt = num2;
						sold=(int) (lngRooms);
						int availRooms=0;
						availRooms=totavailable-sold;
						if(Integer.parseInt(num1)>availRooms){
							this.roomCnt = availRooms;	
						}else{
							this.roomCnt = num2;
						}
								
					}else{
						 long num2 = Long.parseLong(num1);
						 this.roomCnt = num2;
						 sold=(int) (rmc);
					}
				}else{		
					PmsAvailableRooms bookingRoomCount=bookingController.findCount(availDate, accommodationId);
					int intRooms=0;
					
					Integer totavailable=0,availableRooms=0;
					if(accommodation1!=null){
						totavailable=accommodation1.getNoOfUnits();
					}
					if(bookingRoomCount.getRoomCount()!=null){
						long lngRooms=bookingRoomCount.getRoomCount();
						long num = roomCount1.getRoomCount();	
						intRooms=(int)lngRooms;
						String num1 = inventoryDetails.getInventoryCount();
	 					long num2 = Long.parseLong(num1);
	 					availableRooms=totavailable-intRooms;
	 					if(num2>availableRooms){
	 						this.roomCnt = availableRooms;	 
	 					}else{
	 						this.roomCnt = num2-num;
	 					}
	 					long lngSold=bookingRoomCount.getRoomCount();
	   					sold=(int)lngSold;
					}else{
						long num = roomCount1.getRoomCount();	
						String num1 = inventoryDetails.getInventoryCount();
						long num2 = Long.parseLong(num1);
	  					this.roomCnt = (num2-num);
	  					long lngSold=roomCount1.getRoomCount();
	   					sold=(int)lngSold;
					}

				}
								
			}else{
				PmsAvailableRooms inventoryCounts = bookingController.findCount(availDate, accommodationId);							
				if(inventoryCounts.getRoomCount() == null){								
					this.roomCnt = (accommodation1.getNoOfUnits()-rmc);
					sold=(int)rmc;
				}
				else{
					this.roomCnt = (accommodation1.getNoOfUnits()-inventoryCounts.getRoomCount());	
					long lngSold=inventoryCounts.getRoomCount();
						sold=(int)lngSold;
				}
			}
				 double totalRooms=0,intSold=0;
				 totalRooms=(double)accommodation1.getNoOfUnits();
				 intSold=(double)sold;
				 double occupancy=0.0;
				 occupancy=intSold/totalRooms;
				 double sellrate=0.0,otarate=0.0;
				 Integer occupancyRating=(int) Math.round(occupancy * 100);
				 int rateCount=0,rateIdCount=0;
				 double minAmount=0,maxAmount=0,extraAdultAmount=0,extraChildAmount=0,baseAmount=0;
				    
				 int sourceTypeId=1;
				 List<PropertyRate> dateList =  rateController.listAllDates(getPropertyId(),accommodationId,sourceTypeId,d.toDate());
					if(!dateList.isEmpty()){
						for(PropertyRate rates : dateList) {
				    		int propertyRateId = rates.getPropertyRateId();
			    			List<PropertyRateDetail> rateDetailList =   rateDetailController.listMinMaxRateDetail(propertyRateId,f.format(d.toDate()).toLowerCase());
   				    		 if(rateDetailList.size()>0 && !rateDetailList.isEmpty()){
   				    			 if(rateCount<1){
   				    				 for (PropertyRateDetail rate : rateDetailList){
   				    					 minAmount=rate.getMinimumBaseAmount();
   				    					 maxAmount=rate.getMaximumBaseAmount();
   				    					 extraAdultAmount=rate.getExtraAdult();
   				    					 extraChildAmount=rate.getExtraChild();
   				    				    if(occupancyRating==0){
	   				 						sellrate = minAmount;
	   				 					}else if(occupancyRating>0 && occupancyRating<=30){
	   				 			        	sellrate = minAmount;
	   				 			        }else if(occupancyRating>=31 && occupancyRating<=60){
	   				 			        	sellrate = minAmount+(minAmount*10/100);
	   				 			        }else if(occupancyRating>=61 && occupancyRating<=80){
	   				 			        	sellrate = minAmount+(minAmount*15/100);
	   				 			        }else if(occupancyRating>=81 && occupancyRating<=100){
	   				 			        	sellrate = maxAmount;
	   				 			        }
   				    				    baseAmount+=  sellrate;
       				    			 }
   				    				 rateCount++;
   				    				rateIdCount++;
   				    			 }
   				    		 }else{
   				    			 if(rateIdCount<1){
   				    				PropertyAccommodation accommodations=accommodationController.find(accommodationId); 
   				    				if(accommodations.getMinimumBaseAmount()!=null){
   				    					minAmount=accommodations.getMinimumBaseAmount();	 
   				    				 }
   				    				 if(accommodations.getMaximumBaseAmount()!=null){
   				    					maxAmount=accommodations.getMaximumBaseAmount();	 
   				    				 }
   				    				 extraAdultAmount=accommodations.getExtraAdult();
			    					 extraChildAmount=accommodations.getExtraChild();
   				    				if(occupancyRating==0){
   				 						sellrate = minAmount;
   				 					}else if(occupancyRating>0 && occupancyRating<=30){
   				 			        	sellrate = minAmount;
   				 			        }else if(occupancyRating>=31 && occupancyRating<=60){
   				 			        	sellrate = minAmount+(minAmount*10/100);
   				 			        }else if(occupancyRating>=61 && occupancyRating<=80){
   				 			        	sellrate = minAmount+(minAmount*15/100);
   				 			        }else if(occupancyRating>=81 && occupancyRating<=100){
   				 			        	sellrate = maxAmount;
   				 			        }
   				    				baseAmount+= sellrate;
   				    				rateIdCount++;
   				    				rateCount++;
   				    			 }
       				    	 }
				    	}
				     }else{
				    	 PropertyAccommodation accommodations=accommodationController.find(accommodationId);
				    	 if(accommodations.getMinimumBaseAmount()!=null){
	    					minAmount=accommodations.getMinimumBaseAmount();	 
	    				 }
	    				 if(accommodations.getMaximumBaseAmount()!=null){
	    					maxAmount=accommodations.getMaximumBaseAmount();	 
	    				 }
	    				 extraAdultAmount=accommodations.getExtraAdult();
	    				 extraChildAmount=accommodations.getExtraChild();
	    				 
	    				if(occupancyRating==0){
	 						sellrate = minAmount;
	 					}else if(occupancyRating>0 && occupancyRating<=30){
	 			        	sellrate = minAmount;
	 			        }else if(occupancyRating>=31 && occupancyRating<=60){
	 			        	sellrate = minAmount+(minAmount*10/100);
	 			        }else if(occupancyRating>=61 && occupancyRating<=80){
	 			        	sellrate = minAmount+(minAmount*15/100);
	 			        }else if(occupancyRating>=81 && occupancyRating<=100){
	 			        	sellrate = maxAmount;
	 			        }
	    				baseAmount+= sellrate;
	    			 
				     }
					checkedDays=f.format(d.toDate());
					checkedDays=checkedDays.toLowerCase();
					double otaAmount=sellrate+(sellrate*15/100);
/*        			OtaHotelsManager otaController=new OtaHotelsManager();
        			PropertyRatePlanDetailManager ratePlanDetailsController=new PropertyRatePlanDetailManager();
        			OtaHotels otaHotels =  otaController.getOtaHotels(this.propertyId,sourceId);
        			DefaultHttpClient httpClient = new DefaultHttpClient();
        			HttpPost postRequest = new HttpPost("https://in.goibibo.com/api/chmv2/updateroomrates/?bearer_token="+otaHotels.getOtaBearerToken()+"&channel_token="+otaHotels.getOtaChannelToken()+"");
        			
        			List <OtaHotelDetails> otaDetailList = otaController.listDetails(otaHotels.getOtaHotelCode(),accommodationId);
        			
        			for(OtaHotelDetails otaDetail : otaDetailList){

        				if(otaDetail.getPropertyRatePlan().getRatePlanId() == 1){
        					
        					int rateid = 1;
        					PropertyRatePlanDetail ratePlanDetails=ratePlanDetailsController.listRateDetails(propertyId,accommodationId,rateid);
        					double epTotal = otaAmount - ratePlanDetails.getTariffAmount();
        				String xmlBody = "<?xml version='1.0' encoding='UTF-8' ?>"+
        			    "<Website Name='ingoibibo' HotelCode='"+otaHotels.getOtaHotelCode()+"' Version='1'>"+
        			            "<RatePlan>"+
        			            "<RatePlanCode>"+otaDetail.getOtaRateplanId()+"</RatePlanCode>"+
        			            "<StartDate Format='yyyy-mm-dd'>"+startDate+"</StartDate>"+
        			            "<EndDate Format='yyyy-mm-dd'>"+endDate+"</EndDate>"+
        			            "<SingleOccupancyRates>"+				            
        			            "<SellRate>"+epTotal+"</SellRate>"+
        			            "</SingleOccupancyRates>"+
        			            "<DoubleOccupancyRates>"+				            
        			            "<SellRate>"+epTotal+"</SellRate>"+
        			            "</DoubleOccupancyRates>"+
        			            "<TripleOccupancyRates>"+				            
        			            "<SellRate>"+epTotal+"</SellRate>"+
        			            "</TripleOccupancyRates>"+
        			            "<ExtraChildCharge>"+extraChildAmount+"</ExtraChildCharge>"+
        			            "<ExtraAdultCharge>"+extraAdultAmount+"</ExtraAdultCharge>"+
        			            "<InfantCharge>"+0+"</InfantCharge>"+
        			            "<DaysOfWeek Mon='"+getDaysOfWeek(checkedDays)+"' Tue='"+getDaysOfWeek(checkedDays)+"' Wed='"+getDaysOfWeek(checkedDays)+"' Thu='"+getDaysOfWeek(checkedDays)+"' Fri='"+getDaysOfWeek(checkedDays)+"' Sat='"+getDaysOfWeek(checkedDays)+"' Sun='"+getDaysOfWeek(checkedDays)+"'></DaysOfWeek>"+
        			            "<ContractType b2b='False' b2c='True' mobile='False' fph='False'></ContractType>"+
        			            "<Currency>INR</Currency>"+
        			            "</RatePlan>"+
        			      "</Website>";
        					
        					StringEntity input = new StringEntity(xmlBody);
        					input.setContentType("text/xml");
        					postRequest.setEntity(input);
        					HttpResponse httpResponse = httpClient.execute(postRequest);
        					HttpEntity entity = httpResponse.getEntity();
        		            // Read the contents of an entity and return it as a String.
        		            String content = EntityUtils.toString(entity);
        				}else if(otaDetail.getPropertyRatePlan().getRatePlanId() == 2){
        					
        					String xmlBody = "<?xml version='1.0' encoding='UTF-8' ?>"+
        						    "<Website Name='ingoibibo' HotelCode='"+otaHotels.getOtaHotelCode()+"' Version='1'>"+
        						            "<RatePlan>"+
        						            "<RatePlanCode>"+otaDetail.getOtaRateplanId()+"</RatePlanCode>"+
        						            "<StartDate Format='yyyy-mm-dd'>"+startDate+"</StartDate>"+
        						            "<EndDate Format='yyyy-mm-dd'>"+endDate+"</EndDate>"+
        						            "<SingleOccupancyRates>"+				            
        						            "<SellRate>"+otaAmount+"</SellRate>"+
        						            "</SingleOccupancyRates>"+
        						            "<DoubleOccupancyRates>"+				            
        						            "<SellRate>"+otaAmount+"</SellRate>"+
        						            "</DoubleOccupancyRates>"+
        						            "<TripleOccupancyRates>"+				            
        						            "<SellRate>"+otaAmount+"</SellRate>"+
        						            "</TripleOccupancyRates>"+
        						            "<ExtraChildCharge>"+extraChildAmount+"</ExtraChildCharge>"+
        						            "<ExtraAdultCharge>"+extraAdultAmount+"</ExtraAdultCharge>"+
        						            "<InfantCharge>"+0+"</InfantCharge>"+
        						            "<DaysOfWeek Mon='"+getDaysOfWeek(checkedDays)+"' Tue='"+getDaysOfWeek(checkedDays)+"' Wed='"+getDaysOfWeek(checkedDays)+"' Thu='"+getDaysOfWeek(checkedDays)+"' Fri='"+getDaysOfWeek(checkedDays)+"' Sat='"+getDaysOfWeek(checkedDays)+"' Sun='"+getDaysOfWeek(checkedDays)+"'></DaysOfWeek>"+
        						            "<ContractType b2b='False' b2c='True' mobile='False' fph='False'></ContractType>"+
        						            "<Currency>INR</Currency>"+
        						            "</RatePlan>"+
        						      "</Website>";
        								
        								
        						StringEntity input = new StringEntity(xmlBody);
        						input.setContentType("text/xml");
        						postRequest.setEntity(input);
        						HttpResponse httpResponse = httpClient.execute(postRequest);
        						HttpEntity entity = httpResponse.getEntity();
        			            // Read the contents of an entity and return it as a String.
        			            String content = EntityUtils.toString(entity);
        				}
        		    	
        			}*/
	     	}
         
    		int limit = 1;
  			OtaHotelDetails otaHotelDetails = otaHotelsController.getOtaHotelDetails(accommodationId,limit);
  			if(otaHotelDetails!=null){
    		
	  			int sourceId = 3;
	  			OtaHotels otaHotel = otaHotelsController.getOtaHotels(otaHotelDetails.getPmsProperty().getPropertyId(),sourceId);
	  			DefaultHttpClient httpClient = new DefaultHttpClient();
				HttpPost postRequest = new HttpPost("https://in.goibibo.com/api/chmv2/updateroominventory/?bearer_token="+otaHotel.getOtaBearerToken()+"&channel_token="+otaHotel.getOtaChannelToken()+"");
				
				String xmlBody = "<?xml version='1.0' encoding='UTF-8' ?>"+
					    "<Website Name='ingoibibo' HotelCode='"+otaHotelDetails.getOtaHotelCode()+"' Version='1'>"+
					            "<Room>"+
					            "<RoomTypeCode>"+ otaHotelDetails.getOtaRoomTypeId() +"</RoomTypeCode>"+
					            "<StartDate Format='yyyy-mm-dd'>"+startDate+"</StartDate>"+
					            "<EndDate Format='yyyy-mm-dd'>"+endDate+"</EndDate>"+						            
					            "<DaysOfWeek Mon='"+"true"+"' Tue='"+"true"+"' Wed='"+"true"+"' Thu='"+"true"+"' Fri='"+"true"+"' Sat='"+"true"+"' Sun='"+"true"+"'></DaysOfWeek>"+
//					            "<MinLOS>"+"1"+"</MinLOS>"+
//					            "<CutOff>"+"1h"+"</CutOff>"+
					            "<Available> "+ this.roomCnt +"</Available>"+
					            "<StopSell>"+"False"+"</StopSell>"+
					            "</Room>"+
					     
					     "</Website>";
				
			    
				StringEntity input = new StringEntity(xmlBody);
				input.setContentType("text/xml");
				postRequest.setEntity(input);
				HttpResponse httpResponse = httpClient.execute(postRequest);
				HttpEntity entity = httpResponse.getEntity();
              // Read the contents of an entity and return it as a String.
	            String content = EntityUtils.toString(entity);

  			}
  			
    	}
    	catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}
    }
    
    public void getRouteMapSms(String routeMap, String phone)throws Exception{
		
 		
		
		String number = phone;
		String message = "&sms_text=" + routeMap ;


		//String sender = "&sender=" + "TXTLCL";
		String numbers = "&sms_to=" + "+91"+number ;
		String from = "&sms_from=" + "ULOHTL";
		String type = "&sms_type=" + "trans"; 
		// Send data
		HttpURLConnection conn = (HttpURLConnection) new URL("http://etsrds.kapps.in/webapi/ulohotels/api/ulohotels_sms_api.py").openConnection();
		//"http://etsrds.kapps.in/webapi/ulohotels/api/ulohotels_sms_api.py?sms_text=hi%20this%20gopinath%20from%20ulo&sms_to=%20918220211068&sms_from=ULOHTL&sms_type=promo"
		String data = numbers + message +  from + type;
		conn.setDoOutput(true);
		conn.setRequestMethod("POST");
		conn.addRequestProperty("auth_key", "Basic dWxvaG90ZWxzX1Q6VUxPSFRMQDIwMTg=");
		conn.setRequestProperty("Content-Length", Integer.toString(data.length()));
		conn.getOutputStream().write(data.getBytes("UTF-8"));
		final BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		final StringBuffer stringBuffer = new StringBuffer();
		/*String line;
		
		while ((line = rd.readLine()) != null) {
			stringBuffer.append(line);
			
		}*/
		rd.close();
		
		//return null;
		
	
	}
    
    
    public void  getHotelierBookedDetails() throws IOException {
		
		
		 try {
			DecimalFormat df = new DecimalFormat("###.##");
			String jsonOutput = "";
			//HttpServletResponse response = ServletActionContext.getResponse();
		
			PmsPropertyManager propertyController = new PmsPropertyManager();
			PropertyAccommodationManager propertyAccommodationController = new PropertyAccommodationManager();
			BookingDetailManager bookingDetailController = new BookingDetailManager();
			BookingGuestDetailManager bookingGuestDetailController = new BookingGuestDetailManager();
			PmsGuestManager guestController = new PmsGuestManager();
			PmsStatusManager statusController = new PmsStatusManager();
			PropertyDiscountManager propertyDiscountController = new PropertyDiscountManager();  
			PmsBookingManager bookingController = new PmsBookingManager();
			
			//response.setContentType("application/json");
			this.bookedList = bookingDetailController.bookedList(getBookingId());
			StringBuilder accommodationType = new StringBuilder();
			
			StringBuilder accommodationRate = new StringBuilder();
			
			//this.totalAdultCount = (Integer) sessionMap.get("totalAdultCount");
			
			//this.totalInfantCount = (Integer) sessionMap.get("totalInfantCount");
			
			//this.totalChildCount = (Integer) sessionMap.get("totalChildCount");
			//this.txtGrandTotal=(Double)sessionMap.get("txtGrandTotal");
			
			
			List accommodationBooked = new ArrayList();
            for (PmsBookedDetails booked : bookedList) {
           	this.bookingId = getBookingId();
           	this.sourceId=booked.getSourceId();
           	this.propertyDiscountId = booked.getPropertyDiscountId();
           	this.totalAdultCount = (int)booked.getAdults();
           	this.totalChildCount = (int)booked.getChild();
           	PropertyDiscount discount = propertyDiscountController.findId(this.propertyDiscountId);
           	this.propertyDiscountName = discount.getDiscountName();
           	PmsProperty pmsProperty = propertyController.find(booked.getPropertyId());
           	this.propertyName = pmsProperty.getPropertyName();//resort name and location name is missing
           	this.propertyId = pmsProperty.getPropertyId();
           	this.latitude = pmsProperty.getLatitude();
           	this.longitude = pmsProperty.getLongitude();
           	this.address1=pmsProperty.getAddress1();//property address1
           	this.address2=pmsProperty.getAddress2();//property address2
           	this.phoneNumber = pmsProperty.getPropertyContact();//property phone number
           	this.propertyEmail = pmsProperty.getPropertyEmail();
           	this.locationName=pmsProperty.getLocation().getLocationName();//property location
           	this.reservationManagerEmail=pmsProperty.getReservationManagerEmail();
           	this.reservationManagerContact = pmsProperty.getReservationManagerContact();
           	Calendar calDate=Calendar.getInstance();
           	calDate.setTime(booked.getCreatedDate());
           	calDate.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
           	int bookedHours=calDate.get(Calendar.HOUR);
           	int bookedMinute=calDate.get(Calendar.MINUTE);
           	int bookedSecond=calDate.get(Calendar.SECOND);
           	int AMPM=calDate.get(Calendar.AM_PM);
           	String strBookingDate=new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH).format(calDate.getTime());
           	String hours="",minutes="",seconds="";
           	if(bookedHours<10){
           		hours=String.valueOf(bookedHours);
           		hours="0"+hours;
           	}else{
           		hours=String.valueOf(bookedHours);
           	}
           	if(bookedMinute<10){
           		minutes=String.valueOf(bookedMinute);
           		minutes="0"+minutes;
           	}else{
           		minutes=String.valueOf(bookedMinute);
           	}
           	
           	if(bookedSecond<10){
           		seconds=String.valueOf(bookedSecond);
           		seconds="0"+seconds;
           	}else{
           		seconds=String.valueOf(bookedSecond);
           	}
           	
           	if(AMPM==0){//If the current time is AM
           		this.timeHours=strBookingDate+" "+hours+" : "+minutes+" : "+seconds+" AM ";
           	}else if(AMPM==1){//If the current time is PM
           		this.timeHours=strBookingDate+" "+hours+" : "+minutes+" : "+seconds+" PM ";
           	}
           	
           /*	if(this.mailSource == true){
           		if(this.sourceId==12){
           			this.sourceName = "offline";
           		}else{
           			this.sourceName = "resort";
           		}
           	}else{
           		if(this.sourceId==12){
           			this.sourceName = "offline";
           		}else{
           			this.sourceName = "online";
           		}
           		
           	}*/
           	
           	
           	this.sourceName = "online";   
           	this.arrivalDate = booked.getArrivalDate();
           	this.departureDate = booked.getDepartureDate();
           	PmsGuest guest = guestController.find(booked.getGuestId());
           	this.firstName = guest.getFirstName();
           	this.lastName = guest.getLastName();
           	this.phone = guest.getPhone();
           	this.emailId = guest.getEmailId();
           	this.specialRequest = booked.getSpecialRequest();
           	
           	String checkIn = new SimpleDateFormat("MM/dd/yyyy").format(this.arrivalDate);
			String checkOut = new SimpleDateFormat("MM/dd/yyyy").format(this.departureDate);
				
			   LocalDate date1 = LocalDate.parse(checkIn, DateTimeFormat.forPattern("MM/dd/yyyy"));
               LocalDate date2 = LocalDate.parse(checkOut, DateTimeFormat.forPattern("MM/dd/yyyy"));
               this.days = Days.daysBetween(date1, date2).getDays();
           	
             this.bookList =  bookingDetailController.listAccommodation(getBookingId(),booked.getAccommodationId());
             if (!jsonOutput.equalsIgnoreCase(""))
						jsonOutput += ",{";
					
					else
						jsonOutput += "{";
             
             	String jsonTypes="";
         		jsonOutput += "\"bookingid\":\"" + getBookingId().toString() + "\"";
     	
     		for (BookedDetail bookedDetail : bookList) {
           		
	               PropertyAccommodation accommodation = propertyAccommodationController.find(bookedDetail.getAccommodationId());
	               this.roomCount = bookedDetail.getRoomCount()/this.days;
	               this.dblOtaCommission=bookedDetail.getOtaCommission();
                   this.dblOtaTax=bookedDetail.getOtaTax();
                   this.amount=bookedDetail.getAmount();
                   this.dblTotalOtaAmout=this.dblOtaCommission+this.dblOtaTax;
                   
                   this.dblRevenueAmount=this.amount-this.dblTotalOtaAmout;
                   String strRevenueAmount=df.format(this.dblRevenueAmount);
                   this.dblRevenueAmount=Double.valueOf(strRevenueAmount);
                   
                   this.dblTax=bookedDetail.getTax();
                   String strTax=df.format(this.dblTax);
                   this.dblTax=Double.valueOf(strTax);
	            	
	            	accommodationBooked.add(new BookedDetail(accommodation.getAccommodationType(),this.dblTax,this.roomCount,
	            			bookedDetail.getAdultCount(),bookedDetail.getChildCount() ,this.dblRevenueAmount)); 
	            	 
	            	if (!jsonTypes.equalsIgnoreCase(""))
						jsonTypes += ",{";
	            	
					else
						jsonTypes += "{";
           		
           		 
           		jsonTypes += "\"accommodationType\":\"" + accommodation.getAccommodationType().trim()+ "\"";
           		jsonTypes += ",\"rooms\":\"" + this.roomCount + "\"";
           		jsonTypes += ",\"adultCount\":\"" + bookedDetail.getAdultCount()+ "\"";
           		jsonTypes += ",\"childCount\":\"" + bookedDetail.getChildCount()+ "\"";
           		jsonTypes += ",\"amount\":\"" + bookedDetail.getAmount()+ "\"";
           		jsonTypes += ",\"tax\":\"" + bookedDetail.getTax()+ "\"";
           		jsonTypes += ",\"total\":\"" + (bookedDetail.getTax() + bookedDetail.getAmount())  + "\"";
           		jsonTypes += "}";
           		
        	 }
     		jsonOutput += ",\"types\":[" + jsonTypes+ "]";
           	
           	
           	this.rooms = booked.getRooms();
           	this.adults = this.totalAdultCount;
           	this.infant = this.totalInfantCount;
           	this.child = this.totalChildCount;
           	this.tax = booked.getTax();
           	this.amount = booked.getAmount();
           	this.totalAmount = this.tax + this.amount;
           	
           	String strTotal=df.format(this.totalAmount);
           	this.totalAmount=Double.valueOf(strTotal);
           	
               this.totalAdvanceAmount = booked.getAdvanceAmount();
               
             //OTA Share
               this.dblOtaCommission=booked.getOtaCommission();
               this.dblOtaTax=booked.getOtaTax();
				
               this.dblTotalOtaAmout=this.dblOtaCommission+this.dblOtaTax;
				
               this.dblRevenueAmount=this.amount-this.dblTotalOtaAmout;
               this.dblTotalRevenueTaxAmount=this.dblRevenueAmount+this.tax;
				
            //ULO Share
               this.dblCommission=pmsProperty.getPropertyCommission();
               this.dblUloCommission=this.dblRevenueAmount*this.dblCommission/100;
               this.dblUloTaxAmount=this.dblUloCommission*18/100;
               this.dblTotalUloCommission=this.dblUloCommission+this.dblUloTaxAmount; //Payable to Ulo
               this.dblHotelPayable=this.dblRevenueAmount-this.dblTotalUloCommission;
               this.dblHotelTaxPayable=this.dblHotelPayable+this.tax; //Payable to Hotelier
				
            /*if(this.totalAdvanceAmount == 0){
            	this.hotelPay = 0;
           	}
           	else{
           		this.hotelPay = (this.totalAmount-this.totalAdvanceAmount);
           	}*/
            this.hotelPay = (this.totalAmount-this.totalAdvanceAmount); 
           	if(this.totalAmount == booked.getAdvanceAmount()){
           		this.paymentStatus = "Paid";
           	}
           	else{
           		if(this.mailSource == true){
           			if(this.sourceId==12){
           				if(this.totalAmount == booked.getAdvanceAmount()){
       	            		this.paymentStatus = "Paid";
       	            	}else{
       	            		this.paymentStatus = "Partially Paid";
       	            	}
           			}else{
           				this.paymentStatus = "Paid";
           			}
	            	}else{
	            		this.paymentStatus = "Partially Paid";
	            	}
           	}
              
                jsonOutput += ",\"propertyName\":\"" + this.propertyName+ "\"";
	            jsonOutput += ",\"locationName\":\"" + this.locationName+ "\"";
				jsonOutput += ",\"address1\":\"" + this.address1+ "\"";
				jsonOutput += ",\"address2\":\"" + this.address2+ "\"";
				jsonOutput += ",\"phoneNumber\":\"" + this.phoneNumber+ "\"";
				jsonOutput += ",\"propertyEmail\":\"" + this.propertyEmail+ "\"";
				jsonOutput += ",\"reservationManagerEmail\":\"" + this.reservationManagerEmail+ "\"";
				jsonOutput += ",\"reservationManagerContact\":\"" + this.reservationManagerContact+ "\"";
				
				
           	    String CheckIn = new SimpleDateFormat("MMM d,yyyy").format(this.arrivalDate);// check in 
                String CheckOut = new SimpleDateFormat("MMM d,yyyy").format(this.departureDate);//check out
               
                jsonOutput += ",\"checkIn\":\"" + CheckIn+ "\"";
				jsonOutput += ",\"checkOut\":\"" + CheckOut+ "\"";
				jsonOutput += ",\"timeHours\":\"" + this.timeHours+ "\"";
				jsonOutput += ",\"days\":\"" + this.days.toString()+ "\"";
				jsonOutput += ",\"guestname\":\"" + this.firstName+ "\"";
                String strFirstName="",firstNameLetterUpperCase="";
				String strLastName="",lastNameLetterUpperCase="",strFullName="";
				strFirstName=this.firstName;
				strLastName=this.lastName;
				if(strFirstName!=null && strFirstName!="" && !strFirstName.equalsIgnoreCase("undefined")){
					strFirstName=strFirstName.trim();
					firstNameLetterUpperCase=strFirstName.substring(0, 1).toUpperCase()+strFirstName.substring(1);
					strFirstName=firstNameLetterUpperCase;
				}else{
					strFirstName="-";
				}
				if(strLastName!=null && strLastName!=""  && !strLastName.equalsIgnoreCase("undefined")){
					strLastName=strLastName.trim();
					lastNameLetterUpperCase=strLastName.substring(0, 1).toUpperCase() +  strLastName.substring(1);
					strLastName=lastNameLetterUpperCase;
				}else{
					strLastName="";
				}
				strFullName=strFirstName+" "+strLastName;
				
				    String promotionName="NA";
				    
				    if(this.propertyDiscountId == 35){
				    	this.propertyDiscountName = "NA";
				    }
				
				jsonOutput += ",\"guestname\":\"" + strFullName+ "\"";
				jsonOutput += ",\"sourcename\":\"" + this.sourceName+ "\"";
				jsonOutput += ",\"paymentstatus\":\"" + this.paymentStatus+ "\"";
				jsonOutput += ",\"specialrequest\":\"" + this.specialRequest+ "\"";
				jsonOutput += ",\"promotionName\":\"" + promotionName + "\"";
				jsonOutput += ",\"propertyDiscountName\":\"" + this.propertyDiscountName + "\"";
				jsonOutput += ",\"mobile\":\"" + this.phone+ "\"";
				jsonOutput += ",\"emailId\":\"" + this.emailId+ "\"";
				jsonOutput += ",\"rooms\":\"" + this.rooms.toString()+ "\"";
				jsonOutput += ",\"adults\":\"" + String.valueOf(this.adults)+ "\"";
				jsonOutput += ",\"child\":\"" + String.valueOf(this.child)+ "\"";
				jsonOutput += ",\"tax\":\"" + String.valueOf(this.tax) + "\"";
				jsonOutput += ",\"amount\":\"" + String.valueOf(this.amount)+ "\"";
				jsonOutput += ",\"totalamount\":\"" + Double.valueOf(this.totalAmount)+ "\"";
				//jsonOutput += ",\"totaladvanceamount\":\"" + Double.valueOf(this.totalAdvanceAmount)+ "\"";
				
				/*if(getDiscountId()!=null){
					jsonOutput += ",\"totalamount\":\"" + String.valueOf(this.txtGrandTotal)+ "\"";
				}else{
					jsonOutput += ",\"totalamount\":\"" + String.valueOf(this.totalAmount)+ "\"";	
				}*/
				
				
				
				
				jsonOutput += "}";

			}
          
               String checkIn = new SimpleDateFormat("MM/dd/yyyy").format(this.arrivalDate);
			   String checkOut = new SimpleDateFormat("MM/dd/yyyy").format(this.departureDate);
			  // String promotionName = (String) sessionMap.get("promotionName");

			   String promotionName="NA";
			    

				//email template
				Configuration cfg = new Configuration();
				cfg.setClassForTemplateLoading(BookingDetailAction.class, "../../../");
				Template template = cfg.getTemplate(getText("hotelier.template"));
				Map<String, Object> rootMap = new HashMap<String, Object>();
				//Map<String, String> rootMap = new HashMap<String, String>();	 				
				rootMap.put("bookingid",getBookingId().toString());
				rootMap.put("propertyname",this.propertyName);
				rootMap.put("propertyid",this.propertyId);
				rootMap.put("latitude",this.latitude.trim());
				rootMap.put("longitude",this.longitude.trim());
				rootMap.put("sourcename",this.sourceName);
				rootMap.put("paymentstatus",this.paymentStatus);
				rootMap.put("propertyDiscountName",this.propertyDiscountName);
				rootMap.put("reservationManagerEmail",this.reservationManagerEmail);
				rootMap.put("reservationManagerContact",this.reservationManagerContact);
				rootMap.put("propertyEmail",this.propertyEmail);
				rootMap.put("specialrequest",this.specialRequest);
				rootMap.put("guestname",this.firstName);
				rootMap.put("locationName",this.locationName);
				rootMap.put("address1",this.address1);
				rootMap.put("address2",this.address2);
				rootMap.put("hotelpay",this.hotelPay);
				rootMap.put("phoneNumber",this.phoneNumber);
				rootMap.put("checkIn", checkIn);
				rootMap.put("checkOut",checkOut);
				rootMap.put("timeHours",this.timeHours);
				
				String strFirstName="",firstNameLetterUpperCase="";
				String strLastName="",lastNameLetterUpperCase="",strFullName="";
				strFirstName=this.firstName;
				strLastName=this.lastName;
				if(strFirstName!=null && strFirstName!="" && !strFirstName.equalsIgnoreCase("undefined")){
					strFirstName=strFirstName.trim();
					firstNameLetterUpperCase=strFirstName.substring(0, 1).toUpperCase()+strFirstName.substring(1);
					strFirstName=firstNameLetterUpperCase;
				}else{
					strFirstName="-";
				}
				if(strLastName!=null && strLastName!=""  && !strLastName.equalsIgnoreCase("undefined")){
					strLastName=strLastName.trim();
					lastNameLetterUpperCase=strLastName.substring(0, 1).toUpperCase() +  strLastName.substring(1);
					strLastName=lastNameLetterUpperCase;
				}else{
					strLastName="";
				}
				
				
				strFullName=strFirstName+" "+strLastName;
				rootMap.put("guestname",strFullName);
				//rootMap.put("guestname",this.firstName + " " + this.lastName);
				rootMap.put("mobile",this.phone);
				rootMap.put("emailId",this.emailId);	 				
				rootMap.put("checkin",checkIn);
				rootMap.put("days",this.days.toString());
				rootMap.put("checkout",checkOut);
				rootMap.put("promotionName",promotionName);
				
				
				
				rootMap.put("accommodationBooked", accommodationBooked);
				//rootMap.put("rooms", this.roomCount);
				rootMap.put("adults", String.valueOf(this.adults));
				rootMap.put("child",String.valueOf(this.child));
				rootMap.put("infant", String.valueOf(this.infant));
				rootMap.put("tax",df.format(this.tax));
				rootMap.put("amount",df.format(this.dblRevenueAmount));
				rootMap.put("totalamount",df.format(this.dblTotalRevenueTaxAmount));
				rootMap.put("totaladvanceamount",df.format(this.totalAdvanceAmount));
				
				rootMap.put("otaCommission",df.format(this.dblOtaCommission));
				rootMap.put("otaTax",df.format(this.dblOtaTax));
				rootMap.put("totalOtaRevenue",df.format(this.dblTotalOtaAmout));
				rootMap.put("totalRevenueAmount",df.format(this.dblRevenueAmount));
				rootMap.put("uloCommission",df.format(this.dblUloCommission));
				rootMap.put("uloTax",df.format(this.dblUloTaxAmount));
				rootMap.put("totalUloRevenue",df.format(this.dblTotalUloCommission));
				rootMap.put("hotelPayable",df.format(this.dblHotelPayable));
				
				rootMap.put("totalHotelRevenue",df.format(this.dblHotelTaxPayable));
				
				//rootMap.put("totalamount",String.valueOf(this.txtGrandTotal));
				
				
				
				rootMap.put("from", getText("notification.from"));
				Writer out = new StringWriter();
				template.process(rootMap, out);

				String strReservationMailId=null,strPropertyMailId=null,strResortMailId=null,strContractMailId=null,strRevenueMailId=null;
				PmsPropertyManager pmsPropertyController=new PmsPropertyManager();
				for (PmsBookedDetails booked : bookedList) {
					PmsProperty pmsProperty=pmsPropertyController.find(booked.getPropertyId());
					
					strReservationMailId=pmsProperty.getReservationManagerEmail();
					strResortMailId=pmsProperty.getResortManagerEmail();
					strPropertyMailId=pmsProperty.getPropertyEmail();
					strRevenueMailId=pmsProperty.getRevenueManagerEmail();
					strContractMailId=pmsProperty.getContractManagerEmail();
				}
				String strSubject="Hotel Confirmation for Booking Id "+getBookingId().toString()+" "+this.propertyName;
				//send email
				Email em = new Email();
				
				if(strPropertyMailId!=null && strResortMailId!=null && strContractMailId!=null){
					em.set_to(strPropertyMailId);
					em.set_cc(strResortMailId);
	 				em.set_cc2(getText("bookings.notification.email"));
	 				em.set_cc3(getText("operations.notification.email"));
	 				em.set_cc4(getText("finance.notification.email"));
	 				em.set_cc5(strContractMailId);
	 				em.set_cc6(strContractMailId);
	 				em.set_cc7(strContractMailId);
	 				em.set_cc8(strContractMailId);
	 				em.set_cc9(strContractMailId);
	 				em.set_cc10(strContractMailId);
	 				em.set_cc11(strContractMailId);
	 				em.set_cc12(strContractMailId);
	 				em.set_cc13(strContractMailId);
	 				em.set_cc14(strContractMailId);
				}else{
					em.set_to(strPropertyMailId);
					em.set_cc(strResortMailId);
	 				em.set_cc2(getText("bookings.notification.email"));
	 				em.set_cc3(getText("operations.notification.email"));
	 				em.set_cc4(getText("finance.notification.email"));
	 				em.set_cc5(strContractMailId);
	 				em.set_cc6(strContractMailId);
	 				em.set_cc7(strContractMailId);
	 				em.set_cc8(strContractMailId);
	 				em.set_cc9(strContractMailId);
	 				em.set_cc10(strContractMailId);
	 				em.set_cc11(strContractMailId);
	 				em.set_cc12(strContractMailId);
	 				em.set_cc13(strContractMailId);
	 				em.set_cc14(strContractMailId);
				}
				
				
				em.set_from(getText("email.from"));
				//em.set_host(getText("email.host"));
				//em.set_password(getText("email.password"));
				//em.set_port(getText("email.port"));
				em.set_username(getText("email.username"));
				em.set_subject(strSubject);
				em.set_bodyContent(out);
				em.send();		
				
		     
           
           
          // return null;
			//response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

     // return null;
	}
   

	public static List<DateTime> getDateRange(DateTime start, DateTime end) {

        List<DateTime> ret = new ArrayList<DateTime>();
        DateTime tmp = start;
        while(tmp.isBefore(end) || tmp.equals(end)) {
            ret.add(tmp);
            tmp = tmp.plusDays(1);
        }
        return ret;
    }

	public Long getHotelCode() {
		return hotelCode;
	}

	public void setHotelCode(Long hotelCode) {
		this.hotelCode = hotelCode;
	}

	public int getGuestId() {
		return guestId;
	}

	public void setGuestId(int guestId) {
		this.guestId = guestId;
	}

	public Integer getBookingId() {
		return bookingId;
	}

	public void setBookingId(Integer bookingId) {
		this.bookingId = bookingId;
	}
	
	 public Integer getAccommodationId() {
		return accommodationId;
	}

	public void setAccommodationId(Integer accommodationId) {
		this.accommodationId = accommodationId;
	}

	public List<BookedDetail> getBookList() {
		return bookList;
	}

	public void setBookList(List<BookedDetail> bookList) {
		this.bookList = bookList;
	}

	public Integer getPropertyDiscountId() {
		return propertyDiscountId;
	}

	public void setPropertyDiscountId(Integer propertyDiscountId) {
		this.propertyDiscountId = propertyDiscountId;
	}

	public Object getPropertyDiscountName() {
		return propertyDiscountName;
	}

	public void setPropertyDiscountName(Object propertyDiscountName) {
		this.propertyDiscountName = propertyDiscountName;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public Integer getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(Integer propertyId) {
		this.propertyId = propertyId;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getPropertyEmail() {
		return propertyEmail;
	}

	public void setPropertyEmail(String propertyEmail) {
		this.propertyEmail = propertyEmail;
	}

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public String getReservationManagerEmail() {
		return reservationManagerEmail;
	}

	public void setReservationManagerEmail(String reservationManagerEmail) {
		this.reservationManagerEmail = reservationManagerEmail;
	}

	public String getReservationManagerContact() {
		return reservationManagerContact;
	}

	public void setReservationManagerContact(String reservationManagerContact) {
		this.reservationManagerContact = reservationManagerContact;
	}

	public String getRouteMap() {
		return routeMap;
	}

	public void setRouteMap(String routeMap) {
		this.routeMap = routeMap;
	}

	public String getTimeHours() {
		return timeHours;
	}

	public void setTimeHours(String timeHours) {
		this.timeHours = timeHours;
	}

	public String getSourceName() {
		return sourceName;
	}

	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}

	public Object getArrivalDate() {
		return arrivalDate;
	}

	public void setArrivalDate(Object arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getSpecialRequest() {
		return specialRequest;
	}

	public void setSpecialRequest(String specialRequest) {
		this.specialRequest = specialRequest;
	}

	public Integer getDays() {
		return days;
	}

	public void setDays(Integer days) {
		this.days = days;
	}

	public long getRoomCount() {
		return roomCount;
	}

	public void setRoomCount(long roomCount) {
		this.roomCount = roomCount;
	}

	public Integer getRooms() {
		return rooms;
	}

	public void setRooms(Integer rooms) {
		this.rooms = rooms;
	}

	public long getAdults() {
		return adults;
	}

	public void setAdults(long adults) {
		this.adults = adults;
	}

	public long getChild() {
		return child;
	}

	public void setChild(long child) {
		this.child = child;
	}

	public double getTax() {
		return tax;
	}

	public void setTax(double tax) {
		this.tax = tax;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public double getTotalAdvanceAmount() {
		return totalAdvanceAmount;
	}

	public void setTotalAdvanceAmount(double totalAdvanceAmount) {
		this.totalAdvanceAmount = totalAdvanceAmount;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public double getHotelPay() {
		return hotelPay;
	}

	public void setHotelPay(double hotelPay) {
		this.hotelPay = hotelPay;
	}

	public int getTotalAdultCount() {
		return totalAdultCount;
	}

	public void setTotalAdultCount(int totalAdultCount) {
		this.totalAdultCount = totalAdultCount;
	}

	public int getTotalChildCount() {
		return totalChildCount;
	}

	public void setTotalChildCount(int totalChildCount) {
		this.totalChildCount = totalChildCount;
	}

	public int getTotalInfantCount() {
		return totalInfantCount;
	}

	public void setTotalInfantCount(int totalInfantCount) {
		this.totalInfantCount = totalInfantCount;
	}

	public double getTxtGrandTotal() {
		return txtGrandTotal;
	}

	public void setTxtGrandTotal(double txtGrandTotal) {
		this.txtGrandTotal = txtGrandTotal;
	}

	public boolean isMailSource() {
		return mailSource;
	}

	public void setMailSource(boolean mailSource) {
		this.mailSource = mailSource;
	}

	public boolean isMailFlag() {
		return mailFlag;
	}

	public void setMailFlag(boolean mailFlag) {
		this.mailFlag = mailFlag;
	}

	public Integer getSourceId() {
		return sourceId;
	}

	public void setSourceId(Integer sourceId) {
		this.sourceId = sourceId;
	}

	public long getInfant() {
		return infant;
	}

	public void setInfant(long infant) {
		this.infant = infant;
	}

	public long getRoomCnt() {
		return roomCnt;
	}

	public void setRoomCnt(long roomCnt) {
		this.roomCnt = roomCnt;
	}

	public Object getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Object departureDate) {
		this.departureDate = departureDate;
	}

	public List<PmsBookedDetails> getBookedList() {
			return bookedList;
		}

		public void setBookedList(List<PmsBookedDetails> bookedList) {
			this.bookedList = bookedList;
		}

		@Override
		public Object getModel() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public void setUser(User user) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void setServletRequest(HttpServletRequest arg0) {
			// TODO Auto-generated method stub
			
		}
		
		public String getDaysOfWeek(String checkedDays) {	    	
	    	
			PmsDayManager dayController=new PmsDayManager();
			List<PmsDays> listDays=dayController.list();
			ArrayList<String> arlDaysList=new ArrayList<String>();
			if(listDays.size()>0 && !listDays.isEmpty())
			{
				for(PmsDays days:listDays){
					arlDaysList.add(days.getDaysOfWeek());	
				}
			}
		   	 if(arlDaysList.contains(checkedDays)){    		
		   		 return "True";    		 
		   	 }    	 
		   	 else{
		   		 
		   		 return "False"; 
		   		 
		   	 }
		   	
		}
    
}
