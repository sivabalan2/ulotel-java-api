package com.ulopms.ota;

import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the pms_booking database table.
 * 
 */
@Entity
@Table(name="ota_booking")
@NamedQuery(name="OtaBooking.findAll", query="SELECT p FROM OtaBooking p")
public class OtaBooking implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="id",sequenceName="ota_booking_ota_booking_details_id_seq")
	@GeneratedValue(generator="id")
	@Column(name="ota_booking_details_id", unique=true, nullable=false)
	private Integer otaBookingDetailsId;

	@Column(name="ota_booking_id")
	private String otaBookingId;
	
	@Column(name="ota_customer_name")
	private String otaCustomerName;
	
	@Column(name="ota_rooms")
	private long otaRooms;
	
	@Column(name="ota_nights")
	private long otaNights;
	
	@Column(name="ota_room_type")
	private String otaRoomType;
		
	@Column(name="ota_arrival_date")
	private Date otaArrivalDate;

	@Column(name="ota_status")
	private String otaStatus;
	
	@Column(name="ota_flag")
	private Boolean otaFlag;
	
	@Column(name="ota_booking_date")
	private Timestamp otaBookingDate;

	@Column(name="ota_name")
	private String otaName;

	@Column(name="ota_hotel_code")
	private String otaHotelCode;
	
	

	public Integer getOtaBookingDetailsId() {
		return otaBookingDetailsId;
	}

	public void setOtaBookingDetailsId(Integer otaBookingDetailsId) {
		this.otaBookingDetailsId = otaBookingDetailsId;
	}

	public String getOtaBookingId() {
		return otaBookingId;
	}

	public void setOtaBookingId(String otaBookingId) {
		this.otaBookingId = otaBookingId;
	}

	public String getOtaCustomerName() {
		return otaCustomerName;
	}

	public void setOtaCustomerName(String otaCustomerName) {
		this.otaCustomerName = otaCustomerName;
	}

	public long getOtaRooms() {
		return otaRooms;
	}

	public void setOtaRooms(long otaRooms) {
		this.otaRooms = otaRooms;
	}

	public long getOtaNights() {
		return otaNights;
	}

	public void setOtaNights(long otaNights) {
		this.otaNights = otaNights;
	}

	public String getOtaRoomType() {
		return otaRoomType;
	}

	public void setOtaRoomType(String otaRoomType) {
		this.otaRoomType = otaRoomType;
	}

	public Date getOtaArrivalDate() {
		return otaArrivalDate;
	}

	public void setOtaArrivalDate(Date otaArrivalDate) {
		this.otaArrivalDate = otaArrivalDate;
	}

	public String getOtaStatus() {
		return otaStatus;
	}

	public void setOtaStatus(String otaStatus) {
		this.otaStatus = otaStatus;
	}

	public Boolean getOtaFlag() {
		return otaFlag;
	}

	public void setOtaFlag(Boolean otaFlag) {
		this.otaFlag = otaFlag;
	}

	public Timestamp getOtaBookingDate() {
		return otaBookingDate;
	}

	public void setOtaBookingDate(Timestamp otaBookingDate) {
		this.otaBookingDate = otaBookingDate;
	}

	public String getOtaName() {
		return otaName;
	}

	public void setOtaName(String otaName) {
		this.otaName = otaName;
	}
	
	public String getOtaHotelCode() {
		return otaHotelCode;
	}

	public void setOtaHotelCode(String otaHotelCode) {
		this.otaHotelCode = otaHotelCode;
	}
	
}