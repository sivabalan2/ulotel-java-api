package com.ulopms.ota;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import com.ulopms.controller.BookingCancellationManager;
import com.ulopms.controller.BookingDetailManager;
import com.ulopms.controller.PmsBookingManager;
import com.ulopms.controller.PmsPropertyManager;
import com.ulopms.controller.PmsSourceManager;
import com.ulopms.controller.PmsStatusManager;
import com.ulopms.controller.PropertyAccommodationManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.BookingDetail;
import com.ulopms.model.BookingDetailReport;
import com.ulopms.model.PmsBooking;
import com.ulopms.model.PmsProperty;
import com.ulopms.model.PmsStatus;
import com.ulopms.model.User;
import com.ulopms.util.Email;

import freemarker.template.Configuration;
import freemarker.template.Template;

public class CancellationService extends ActionSupport implements
ServletRequestAware,SessionAware, UserAware,ModelDriven<Object>{
	
	
	private static final Logger logger = Logger.getLogger(CancellationService.class);
	private List<BookingDetail> bookingDetailList;
	private List<BookingDetailReport> listBookingDetails;	
	//@XStreamImplicit(itemFieldName = "CancelledItem")
	//public List<CancelledItem> array ;

	private Integer accommodationId;
	public Integer statusId;
	private String timeHours; 
	private boolean mailFlag;
	private long roomCount;
	private Integer adultCount;
	private Integer childCount;
	private double totalBaseAmount=0.0;
	private double totalBaseTaxAmount=0.0;
	private double totalTaxAmount=0.0;
	private double totalAdvanceAmount=0.0;
	private double reducedAmount=0.0;
	private double refundAmount=0.0;
	private String strAccommodationType;
	private String diffDays;
	private String diffInsDays;
	private String strGuestName;
	private String strEmailId;
	private String strMobileNo;
	private String propertyName;
	private String sourceName;
	private Integer sourceId;
	private String paymentStatus;
	private String tariffStatus;
	private String checkIn;
	private String checkOut;
	private Integer bookingPropertyId;
	private Integer diffInDays;
	private String strRemarks;
	
	public Integer add(CancelledListing cancelledListing){
    	
    	try{
    		
    		SimpleDateFormat format1=new SimpleDateFormat("yyyy-MM-dd");
    		DateFormat format2=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    		OtaCancellationManager otaCancellationController=new OtaCancellationManager();
    		PmsBookingManager bookingController = new PmsBookingManager();
    		
        	java.util.Date dateCheckIn = format1.parse(cancelledListing.getCancelledItem().getCancellationDate());
        	OtaCancellation otaCancel =new OtaCancellation();
        	PmsStatusManager statusController = new PmsStatusManager();
        	BookingDetailManager detailController = new BookingDetailManager();
        	otaCancel.setOtaBookingId(cancelledListing.getCancelledItem().getBookingId());
        	otaCancel.setCancellationStatus(cancelledListing.getCancelledItem().getCancellationStatus());
        	otaCancel.setOtaCancellationId(cancelledListing.getCancelledItem().getCancellationId());
        	otaCancel.setCancellationDate(dateCheckIn);
        	otaCancellationController.add(otaCancel);
        	PmsBooking otaBooking = bookingController.findOtaBookingId(cancelledListing.getCancelledItem().getBookingId());
    		PmsBooking booking = bookingController.find(otaBooking.getBookingId());
    		
    		long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			booking.setIsActive(false);
			booking.setIsDeleted(true);
			booking.setModifiedDate(new java.sql.Timestamp(date.getTime()));
			PmsStatus status = statusController.find(4);
			booking.setPmsStatus(status);
			bookingController.edit(booking);
			
			
			this.bookingDetailList = detailController.findId(otaBooking.getBookingId());
			for (BookingDetail bookingDetails : bookingDetailList){	
				int accommId=bookingDetails.getPropertyAccommodation().getAccommodationId();
				if(accommId==(int)bookingDetails.getPropertyAccommodation().getAccommodationId()){
					bookingDetails.setIsActive(false);
					bookingDetails.setIsDeleted(true);
					
					bookingDetails.setModifiedDate(new java.sql.Timestamp(date.getTime()));
					bookingDetails.setPmsStatus(status);
					detailController.edit(bookingDetails);
		   
				}
				
			}
			getCancellationVoucher(otaBooking.getBookingId(),booking.getPmsProperty().getPropertyId());
    	}catch(Exception e){
    		e.printStackTrace();
    	}finally{
    		
    	}
    	return 200;
    }
	
	public String getCancellationVoucher(Integer bookingId,Integer propertyId) throws IOException{

		try{
			DecimalFormat df = new DecimalFormat("###.##");
			BookingDetailManager bookingDetailController=new BookingDetailManager();
			PmsPropertyManager propertyController=new PmsPropertyManager();
			ArrayList<Integer> listAccommId=new ArrayList<Integer>();
			
			BookingCancellationManager cancelController=new BookingCancellationManager();
			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
			PmsSourceManager sourceController=new PmsSourceManager();
			PmsStatusManager statusController=new PmsStatusManager();
			String strAccommType="";
			SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
			this.listBookingDetails=cancelController.listBookingDetail(bookingId,propertyId);
			if(listBookingDetails.size()>0 && !listBookingDetails.isEmpty()){
			
				for(BookingDetailReport bookingDetails:listBookingDetails){

					accommodationId=bookingDetails.getAccommodationId();
					totalBaseAmount+=bookingDetails.getBaseAmount();
					totalTaxAmount+=bookingDetails.getTaxAmount();
					totalAdvanceAmount+=bookingDetails.getAdvanceAmount();
//					reducedAmount+=listCancelBookingDetails.get(i).getReducedAmount();
//					refundAmount+=listCancelBookingDetails.get(i).getRefundAmount();
					strAccommType=bookingDetails.getAccommodationType()+","+strAccommType;
					
					roomCount=bookingDetails.getRooms();
					adultCount=bookingDetails.getAdultCount();
					childCount=bookingDetails.getChildCount();
					checkIn=bookingDetails.getCheckIn();
					checkOut=bookingDetails.getCheckOut();
					diffInsDays=bookingDetails.getDiffInDays();
					diffDays=bookingDetails.getDiffDays();
					bookingId=bookingDetails.getBookingId();
					strGuestName=bookingDetails.getGuestName();
					strEmailId=bookingDetails.getEmailId();
					strMobileNo=bookingDetails.getMobileNumber();
					strAccommodationType=strAccommType;
					totalBaseTaxAmount=totalBaseAmount+totalTaxAmount;
					sourceId=bookingDetails.getSourceId();
					bookingPropertyId=bookingDetails.getPropertyId();
				
				}
			}
			
			
			java.util.Date date=new java.util.Date();
        	Calendar calDate=Calendar.getInstance();
        	calDate.setTime(date);
        	calDate.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
        	
		    String strCurrentDate = new SimpleDateFormat("yyyy-MM-dd").format(calDate.getTime());
		    java.util.Date currentDate = format1.parse(strCurrentDate);
		    Timestamp currentTS = new Timestamp(currentDate.getTime());
        	int bookedHours=calDate.get(Calendar.HOUR);
        	int bookedMinute=calDate.get(Calendar.MINUTE);
        	int AMPM=calDate.get(Calendar.AM_PM);
        	String strBookingDate=new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH).format(date);
        	String hours="",minutes="";
        	if(bookedHours<10){
        		hours=String.valueOf(bookedHours);
        		hours="0"+hours;
        	}else{
        		hours=String.valueOf(bookedHours);
        	}
        	if(bookedMinute<10){
        		minutes=String.valueOf(bookedMinute);
        		minutes="0"+minutes;
        	}else{
        		minutes=String.valueOf(bookedMinute);
        	}
        	if(AMPM==0){//If the current time is AM
        		this.timeHours=strBookingDate+" "+hours+" : "+minutes+" AM ";
        	}else if(AMPM==1){//If the current time is PM
        		this.timeHours=strBookingDate+" "+hours+" : "+minutes+" PM ";
        	}
        	
        	if(this.sourceId==12){
    			this.sourceName = "Offline";
    		}else if(this.sourceId==1){
    			this.sourceName = "Online";
    		}else{
    			this.sourceName = "Online";
    		}
        	
        	if(totalBaseTaxAmount == totalAdvanceAmount){
        		this.paymentStatus = "Paid";
        		this.tariffStatus="";
        	}
        	else{
        		this.paymentStatus = "Partially Paid";
        		this.tariffStatus= "Hotel Pay ("+String.valueOf(totalBaseTaxAmount-totalAdvanceAmount)+")";
        	}
        	
        	diffInDays=Integer.parseInt(diffDays);
        	
			
        	if(diffInDays<=1){
				strRemarks="No Refund";
				String strReduced=df.format(totalAdvanceAmount);
				reducedAmount=Double.parseDouble(strReduced);
				refundAmount=totalAdvanceAmount-totalAdvanceAmount;
				String strRefund=df.format(refundAmount);
				refundAmount=Double.parseDouble(strRefund);
			
			}else if(diffInDays<=2){
				Double dblDiscountPer=25.0; 
				Double dblDiscountAmt=0.0;
				strRemarks="Cancellation Fee 25 %";
				dblDiscountAmt=totalBaseAmount*dblDiscountPer/100;
				String strReduced=df.format(dblDiscountAmt);
				reducedAmount=Double.parseDouble(strReduced);
				
				refundAmount=totalBaseAmount-dblDiscountAmt;
				String strRefund=df.format(refundAmount);
				refundAmount=Double.parseDouble(strRefund);
				
			}else if(diffInDays>=3){
				strRemarks="Cancellation Fee 300 Rs";
				reducedAmount=300 ;
				String strReduced=df.format(reducedAmount);
				reducedAmount=Double.parseDouble(strReduced);
				refundAmount=totalAdvanceAmount-300;
				String strRefund=df.format(refundAmount);
				refundAmount=Double.parseDouble(strRefund);
			}
        	
        	
			/*String strPropertyMailId1=null,strPropertyMailId2=null,strPropertyMailId3=null,strPropertyMailId4=null,strPropertyMailId5=null;
			PmsProperty pmsproperty=propertyController.find((Integer) sessionMap.get("propertyId"));
			strPropertyMailId1=pmsproperty.getPropertyEmail();
			strPropertyMailId2=pmsproperty.getResortManagerEmail();
			strPropertyMailId3=pmsproperty.getContractManagerEmail();
			strPropertyMailId4=pmsproperty.getReservationManagerEmail();*/
			
				String strReservationMailId=null,strPropertyMailId=null,strResortMailId=null,strContractMailId=null,strRevenueMailId=null;
				PmsPropertyManager pmsPropertyController=new PmsPropertyManager();
				PmsProperty pmsProperty=pmsPropertyController.find(propertyId);
				
				strReservationMailId=pmsProperty.getReservationManagerEmail();
				strResortMailId=pmsProperty.getResortManagerEmail();
				strPropertyMailId=pmsProperty.getPropertyEmail();
				strRevenueMailId=pmsProperty.getRevenueManagerEmail();
				strContractMailId=pmsProperty.getContractManagerEmail();
				this.propertyName=pmsProperty.getPropertyName();
				
				String[] arrString=null;
				ArrayList<String> arlToken=new ArrayList<String>();
				StringTokenizer stoken=new StringTokenizer(strReservationMailId,",");
				String strReservationMailId1=null,strReservationMailId2=null,strReservationMailId3=null,strReservationMailId4=null,strReservationMailId5=null;
				while(stoken.hasMoreElements()){
					arlToken.add(stoken.nextToken());
				}
				
				int intCount=0, arlSize=0;
				arrString=new String[arlToken.size()];
				Iterator<String> iterListValues=arlToken.iterator();
			    while(iterListValues.hasNext()){
		    		String strTemp=iterListValues.next();
		    		arrString[intCount]=strTemp.trim();
		    		intCount++;		
			    }
			    
				int i=0;
				arlSize=arlToken.size();
				for(String s : arrString) {
			          String[] s2 = s.split(" ");
			          for(String results : s2) {
			        	  if(i==0){
			        		 strReservationMailId1=results;
			        	  }else if(i==1){
			        		 strReservationMailId2=results;
			        	  }else if(i==2){
			        		 strReservationMailId3=results;
			        	  }else if(i==3){
			        		 strReservationMailId4=results;
			        	  }else if(i==4){
			        		 strReservationMailId5=results;
			        	  }else if(i==5){
			        		  break;
			        	  }
			        	
			        	 i++;
			        	 
			        	
			          }
			      }
				
				if(strReservationMailId2==null){
					strReservationMailId2=strReservationMailId1;
				}
				if(strReservationMailId3==null){
					strReservationMailId3=strReservationMailId1;
				}
				if(strReservationMailId4==null){
					strReservationMailId4=strReservationMailId1;
				}
				if(strReservationMailId5==null){
					strReservationMailId5=strReservationMailId1;
				}
				
				
				String[] arrString1=null;
				ArrayList<String> arlToken1=new ArrayList<String>();
				StringTokenizer stoken1=new StringTokenizer(strRevenueMailId,",");
				String strRevenueMailId1=null,strRevenueMailId2=null,strRevenueMailId3=null,strRevenueMailId4=null,strRevenueMailId5=null;
				while(stoken1.hasMoreElements()){
					arlToken1.add(stoken1.nextToken());
				}
				
				int intCount1=0, arlSize1=0;
				arrString1=new String[arlToken1.size()];
				Iterator<String> iterListValues1=arlToken1.iterator();
			    while(iterListValues1.hasNext()){
		    		String strTemp=iterListValues1.next();
		    		arrString1[intCount1]=strTemp.trim();
		    		intCount1++;		
			    }
			    
				int j=0;
				arlSize1=arlToken1.size();
				for(String s1 : arrString1) {
			          String[] s3 = s1.split(" ");
			          for(String results1 : s3) {
			        	  if(j==0){
			        		 strRevenueMailId1=results1;
			        	  }else if(j==1){
			        		 strRevenueMailId2=results1;
			        	  }else if(j==2){
			        		 strRevenueMailId3=results1;
			        	  }else if(j==3){
			        		 strRevenueMailId4=results1;
			        	  }else if(j==4){
			        		 strRevenueMailId5=results1;
			        	  }else if(j==5){
			        		  break;
			        	  }
			        	
			        	 j++;
			        	 
			        	
			          }
			      }
				
				if(strRevenueMailId2==null){
					strRevenueMailId2=strRevenueMailId1;
				}
				if(strRevenueMailId3==null){
					strRevenueMailId3=strRevenueMailId1;
				}
				if(strRevenueMailId4==null){
					strRevenueMailId4=strRevenueMailId1;
				}
				if(strRevenueMailId5==null){
					strRevenueMailId5=strRevenueMailId1;
				}
				
			
				this.mailFlag=true;	
			if(this.mailFlag == true)
            {

 				//email template
 				Configuration cfg = new Configuration();
 				cfg.setClassForTemplateLoading(BookingService.class, "../../../");
 				Template template = cfg.getTemplate(getText("booking.cancellation.voucher.template"));
 				Map<String, Object> rootMap = new HashMap<String, Object>();
 				rootMap.put("voucherDate",this.timeHours);
 				rootMap.put("rooms",roomCount);
 				rootMap.put("adults",adultCount);
 				rootMap.put("child",childCount);
 				rootMap.put("checkIn",checkIn);
                rootMap.put("checkOut",checkOut);
 				rootMap.put("diffDays",diffInsDays);
 				rootMap.put("bookingId",bookingId.toString());
 				rootMap.put("guestName",strGuestName);
 				rootMap.put("emailId",strEmailId);
 				rootMap.put("mobile",strMobileNo);
 				rootMap.put("propertyName",this.propertyName);
 				rootMap.put("paymentStatus",paymentStatus);
 				rootMap.put("tariffStatus", this.tariffStatus);
 				rootMap.put("sourceName",sourceName);
 				
 				rootMap.put("accommodationType",strAccommodationType);
 				rootMap.put("totalBaseTaxAmount",totalAdvanceAmount);
 				rootMap.put("reducedAmount",reducedAmount);
 				rootMap.put("refundAmount",refundAmount);
 				rootMap.put("remarks",strRemarks);
 				
 				rootMap.put("from", getText("notification.from"));
 				Writer out = new StringWriter();
 				template.process(rootMap, out);
 				
 				
 				
 				
 				String strSubject="Hotel Cancellation for Booking Id "+bookingId.toString()+" "+this.propertyName;
 				//send email
 				/*Email em = new Email();
 				em.set_to(strEmailId);
 				em.set_cc(getText("bookings.notification.email"));
 				em.set_cc2(getText("operations.notification.email"));
 				em.set_cc3(getText("finance.notification.email"));
 				em.set_cc4(strPropertyMailId1);
 				em.set_cc5(strPropertyMailId2);
 				em.set_cc6(strPropertyMailId3);*/
 				Email em = new Email();
 				em.set_to(strEmailId);
 				
 				if(strReservationMailId1!=null && strContractMailId!=null && strRevenueMailId1!=null){
 					em.set_cc(strContractMailId);
	 				em.set_cc2(strPropertyMailId);
	 				em.set_cc3(strResortMailId);
	 				em.set_cc4(getText("bookings.notification.email"));
	 				em.set_cc5(getText("operations.notification.email"));
	 				em.set_cc6(getText("finance.notification.email"));
	 				em.set_cc7(strRevenueMailId1);
	 				em.set_cc8(strReservationMailId1);
	 				em.set_cc9(strRevenueMailId2);
	 				em.set_cc10(strReservationMailId2);
	 				em.set_cc11(strRevenueMailId3);
	 				em.set_cc12(strReservationMailId3);
	 				em.set_cc13(strRevenueMailId4);
	 				em.set_cc14(strReservationMailId4);
 				}else{
 					em.set_cc(strContractMailId);
	 				em.set_cc2(strPropertyMailId);
	 				em.set_cc3(strResortMailId);
	 				em.set_cc4(getText("bookings.notification.email"));
	 				em.set_cc5(getText("operations.notification.email"));
	 				em.set_cc6(getText("finance.notification.email"));
	 				em.set_cc7(strRevenueMailId1);
	 				em.set_cc8(strReservationMailId1);
	 				em.set_cc9(strRevenueMailId2);
	 				em.set_cc10(strReservationMailId2);
	 				em.set_cc11(strRevenueMailId3);
	 				em.set_cc12(strReservationMailId3);
	 				em.set_cc13(strRevenueMailId4);
	 				em.set_cc14(strReservationMailId4);
 				}
 				
 				
 				em.set_from(getText("email.from"));
 				em.set_username(getText("email.username"));
 				em.set_subject(strSubject);
 				em.set_bodyContent(out);
 				em.send();		
 				
            }
			
		}catch(Exception e){
			logger.error(e);
			e.printStackTrace();
		}finally{
			
		}
		return null;
	
	}

	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setUser(User user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setSession(Map<String, Object> arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setServletRequest(HttpServletRequest arg0) {
		// TODO Auto-generated method stub
		
	}

}
