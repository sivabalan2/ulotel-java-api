package com.ulopms.api;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Config implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String propertyTagId;
	
	private String propertyTagName;
	
	
	
	public Config(){	
		
		
		
		
	}
	
	public Config(String propertyTagId, String propertyTagName) {
		this.propertyTagId = propertyTagId;
		this.propertyTagName = propertyTagName;
	}

	public String getPropertyTagId() {
		return propertyTagId;
	}

	public void setPropertyTagId(String propertyTagId) {
		this.propertyTagId = propertyTagId;
	}

	public String getPropertyTagName() {
		return propertyTagName;
	}

	public void setPropertyTagName(String propertyTagName) {
		this.propertyTagName = propertyTagName;
	}


}