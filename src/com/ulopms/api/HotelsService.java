package com.ulopms.api;
import com.ulopms.view.LocationAction;
import com.ulopms.view.PropertyAction;



public class HotelsService {


	public HotelsService() {
	}
	
	public String listProperties(Hotels hotels) {
		String str=null;
		try{
			LocationAction properties = new LocationAction();
			str=properties.getListPropertiesApi(hotels);
			
		}
		catch(Exception e){
			e.printStackTrace();
			
		}
		
		return str;
	}

	public String listFilterData(Hotels hotels){
		String data=null;
		
		try{
			PropertyAction filterAction=new PropertyAction();
			data=filterAction.getFilterAPI(hotels);
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return data;
	}

}
