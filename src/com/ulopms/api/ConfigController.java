package com.ulopms.api;


import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.rest.DefaultHttpHeaders;
import org.apache.struts2.rest.HttpHeaders;

import com.opensymphony.xwork2.ModelDriven;
import com.ulopms.model.PmsTags;





@Results({
    @Result(name="success", type="redirectAction", params = {"actionName" , "config"})
})


public class ConfigController implements ModelDriven<Object>{

	
	
   
   private List<PmsTags> propertyTagList;
   /* private Collection<Locations> list;*/
	private String id;
	
	public Config configure = new Config();
	 
	private Object config;
	
    private ConfigService configService = new ConfigService();
    Integer statusCode;
    
 // GET	/api/users
 	public HttpHeaders index() {
 		try{
 			
 			HttpServletResponse response = ServletActionContext.getResponse();
 			response.flushBuffer();
 			response.setContentType("application/json");
 			
 	 		String data = null;
 	 		
 	 		data = configService.findAll();
 	 		
 	 		response.getWriter().write(data);
 	 		
 		}catch(Exception e){
 			e.printStackTrace();
 		}
 		return new DefaultHttpHeaders("index").disableCaching();
 		
 	}
 	
 	
	//@Override
	
    public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	
	public Integer getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}
	
    public Object getModel() {
		if(config == null) {
			return configure;	
		} else {
			return config;
		}
	}
    
	
}
