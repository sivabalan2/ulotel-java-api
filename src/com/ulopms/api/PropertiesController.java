package com.ulopms.api;


import java.util.Collection;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.rest.DefaultHttpHeaders;
import org.apache.struts2.rest.HttpHeaders;

import com.opensymphony.xwork2.ModelDriven;
//import com.sun.mail.iap.Response;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.ulopms.controller.LocationManager;
import com.ulopms.controller.UserLoginManager;
import com.ulopms.model.Location;
import com.ulopms.model.User;





@Results({
    @Result(name="success", type="redirectAction", params = {"actionName" , "properties"})
})


public class PropertiesController implements ModelDriven<Object>{

	
	
   
   private List<Location> locationList;
   /* private Collection<Locations> list;*/
	private String id;
	
	public Property property = new Property();
    private PropertiesService propertyService = new PropertiesService();
    Integer statusCode;
    
 	public HttpHeaders index() {
 		try{
 			HttpServletResponse response = ServletActionContext.getResponse();
 			response.flushBuffer();
 			response.setContentType("application/json");
 	 		String data = null;
 	 		data = propertyService.findAll(property);
 	 		response.getWriter().write(data);
 		}catch(Exception e){
 			e.printStackTrace();
 		}
 		return new DefaultHttpHeaders("index").disableCaching();
 	}
 	
	
 	public HttpHeaders show() {
 		try{
 			HttpServletResponse response = ServletActionContext.getResponse();
 			response.flushBuffer();
 			response.setContentType("application/json");
 	 		String data = null;
 	 		
 	 		if(getId().equalsIgnoreCase("coupons")){
 	 			data =  propertyService.findCoupons(property);
 	 		}else if(getId().equalsIgnoreCase("couponsData")){
 	 			data =  propertyService.findAllCoupons(property);
 	 		}else if(getId().equalsIgnoreCase("verifyCoupon")){
 	 			data =  propertyService.verifyCoupon(property);
 	 		}
 	 		response.getWriter().write(data);
 		}catch(Exception e){
 			e.printStackTrace();
 		}
 		
 		return new DefaultHttpHeaders("show").disableCaching();
 	}
     
 	
 	
 	public HttpHeaders create() {
 		HttpServletResponse response = ServletActionContext.getResponse();
		response.setStatus(200);
		return new DefaultHttpHeaders("create").disableCaching();
	}
 	
  
   
	 
	//@Override
	
    public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}


	public Integer getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}
	
    public Object getModel() {
		if(property == null) {
			return property;	
		} else {
			return property;
		}
	}
    
}
