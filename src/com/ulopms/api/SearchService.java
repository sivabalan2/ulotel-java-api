package com.ulopms.api;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.ulopms.controller.LocationManager;
import com.ulopms.controller.UserLoginManager;
import com.ulopms.model.Location;
import com.ulopms.model.User;
import com.ulopms.view.JoinUloAction;
import com.ulopms.view.LocationAction;
import com.ulopms.view.PropertyAction;
import com.ulopms.view.PropertyPhotoAction;
import com.ulopms.view.SeoUrlAction;
import com.ulopms.view.UserAction;

import java.util.HashMap;
import java.util.Map;


public class SearchService {
	private static Map<String, Search> userSearch = new HashMap<String,Search>(); 
	private static final Logger logger = Logger.getLogger(SearchService.class);
	
	
	public SearchService() {
	}

	public String findAll() {
		String str=null;
		try{
			
		}
		catch(Exception e){
			e.printStackTrace();
			logger.error(e);
		}
		
		return str;
	}
 
	public String findByLocation(Search search) {
		String str=null;
		try{
			
		}
		catch(Exception e){
			e.printStackTrace();
			logger.error(e);
		}
		
		return str;
	}
	
	public String findSeoContent(Search search) {
		String data=null;
		try{
			SeoUrlAction seoSearch = new SeoUrlAction();
			data=seoSearch.getSeoContentApi(search);
			
		}
		catch(Exception e){
			e.printStackTrace();
			logger.error(e);
		}
		
		return data;
	}
	
	public String addSubcriberMail(Search search){
		String data=null;
		try{
			UserAction adduser=new UserAction();
			data=adduser.addSubcriberMail(search);
		}catch(Exception e){
			e.printStackTrace();
			logger.error(e);
		}
		return data;
	}
	
	public String addPartnerWithUS(Search search){
		String data=null;
		try{
			JoinUloAction joinulo=new JoinUloAction();
			data=joinulo.partnerWithusAPI(search);
		}catch(Exception e){
			e.printStackTrace();
			logger.error(e);
		}
		return data;
	}
	
	public String uploadResume(Search search){
		String data=null;
		try{
			PropertyPhotoAction upload=new PropertyPhotoAction();
			data=upload.uploadResume(search);
		}catch(Exception e){
			e.printStackTrace();
			logger.error(e);
		}
		return data;
	}
	public String findPromotionImages(Search search) {
		String data="";
		try{
			PropertyPhotoAction imageSearch = new PropertyPhotoAction();
			data=imageSearch.getPromotionImagesAPI();
			
		}
		catch(Exception e){
			e.printStackTrace();
			logger.error(e);
		}
		
		return data;
	}
	
	public String addCorporateEnquiry(Search search){
		String data=null;
		try{
			UserAction addcorporate=new UserAction();
			data=addcorporate.sendCorporateEnquiryApi(search);
		}catch(Exception e){
			e.printStackTrace();
			logger.error(e);
		}
		return data;
		
	}
}
