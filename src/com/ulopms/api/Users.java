package com.ulopms.api;

import java.io.File;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Users implements Serializable {
	private static final long serialVersionUID = 1L;
	private String id;
	private String fullName;
	private String userId;
	private String mobileNumber;
	private String alternateNumber;
	private String emailId;
	private String userName;
	private String address;
	private String guestId;
	private String guestOtp;
	private File myFile;
	private Integer tripTypeId;
	
	public Users(){	
		
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getAlternateNumber() {
		return alternateNumber;
	}

	public void setAlternateNumber(String alternateNumber) {
		this.alternateNumber = alternateNumber;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getGuestId() {
		return guestId;
	}

	public void setGuestId(String guestId) {
		this.guestId = guestId;
	}

	public String getGuestOtp() {
		return guestOtp;
	}

	public void setGuestOtp(String guestOtp) {
		this.guestOtp = guestOtp;
	}

	public File getMyFile() {
		return myFile;
	}

	public void setMyFile(File myFile) {
		this.myFile = myFile;
	}

	public Integer getTripTypeId() {
		return tripTypeId;
	}

	public void setTripTypeId(Integer tripTypeId) {
		this.tripTypeId = tripTypeId;
	}

	
	
}