package com.ulopms.api;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

//import javax.xml.bind.annotation.XmlElement;
//import javax.xml.bind.annotation.XmlRootElement;

//import com.thoughtworks.xstream.annotations.XStreamAlias;

//@XStreamAlias("Booking")
//@XmlRootElement(name ="Booking")
public class Locations implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String locationId;
	
	private String locationName;
	
	private String locationDescription;
	
	private String locationUrl;
	
	private String photoPath;
	
	private Boolean isDeleted;
	
		
	
	
	public Locations(){	
		
		
		
		
	}
	
	public Locations(String locationId, String locationName, String locationDescription) {
		this.locationId = locationId;
		this.locationName = locationName;
		this.locationDescription = locationDescription; 
	}




	public String getLocationId() {
		return locationId;
	}




	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}




	public String getLocationName() {
		return locationName;
	}




	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}




	public String getLocationDescription() {
		return locationDescription;
	}




	public void setLocationDescription(String locationDescription) {
		this.locationDescription = locationDescription;
	}




	public String getLocationUrl() {
		return locationUrl;
	}




	public void setLocationUrl(String locationUrl) {
		this.locationUrl = locationUrl;
	}




	public String getPhotoPath() {
		return photoPath;
	}




	public void setPhotoPath(String photoPath) {
		this.photoPath = photoPath;
	}




	public Boolean getIsDeleted() {
		return isDeleted;
	}




	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	
	
	


	
	
	
	
	
	}