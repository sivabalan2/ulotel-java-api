package com.ulopms.api;


import java.util.Collection;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.rest.DefaultHttpHeaders;
import org.apache.struts2.rest.HttpHeaders;

import com.opensymphony.xwork2.ModelDriven;





@Results({
    @Result(name="success", type="redirectAction", params = {"actionName" , "users"})
})


public class UsersController implements ModelDriven<Object>{

	private String id;
	
	public Users configure = new Users();
	
	private Collection<Search> list;
	 
    private Users model = new Users();
    
	
    private UsersService userService = new UsersService();
    Integer statusCode;
    
 // GET	/api/id/search
    
    public HttpHeaders show(){
    	try{
    		HttpServletResponse response = ServletActionContext.getResponse();
 			response.flushBuffer();
    		response.setContentType("application/json");
 			
 	 		String data = null;
 	 		if(getId().equalsIgnoreCase("userprofile")){
 	 			data = userService.userProfile(model);	
 	 		}else if(getId().equalsIgnoreCase("upcomingbooking")){
 	 			data = userService.upcomingBooking(model);
 	 		}else if(getId().equalsIgnoreCase("pastbooking")){
 	 			data = userService.pastBooking(model);
 	 		}else if(getId().equalsIgnoreCase("sendpayathotelotp")){
 	 			data = userService.sendPayatHotelOtp(model);
 	 		}else if(getId().equalsIgnoreCase("resendpayathotelotp")){
 	 			data = userService.resendPayatHotelOtp(model);
 	 		}else if(getId().equalsIgnoreCase("verifypayathotelotp")){
 	 			data = userService.verifyPayatHotelOtp(model);
 	 		}
 	 		
 	 		
 	 		response.getWriter().write(data);
 	 		
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    	return null;
    }
    
    //GET /api/search
 	public HttpHeaders index() {
 		try{
 	 		
 		}catch(Exception e){
 			e.printStackTrace();
 		}
 		return null;
 		
 	}
 	//POST /api/users
 	public HttpHeaders create() {
   	 
    	try{
    		HttpServletResponse response = ServletActionContext.getResponse();
    		response.flushBuffer();
 			response.setContentType("application/json");
 	 		String data = null;
// 	 		data = userService.uloUserProfile(model);
 	 		//data = userService.updateUserProfile(model);
 	 		response.setStatus(200);
 	 		
 	 		response.getWriter().write(data);
 			
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    	
        return null;
    }
 	
 	public HttpHeaders update(){
 		try{
    		HttpServletResponse response = ServletActionContext.getResponse();
    		response.flushBuffer();
 			response.setContentType("application/json");
 	 		String data = null;
 	 		if(getId().equalsIgnoreCase("addUserLogin")){
 	 			data = userService.uloUserProfile(model);
 	 		}else if(getId().equalsIgnoreCase("updateprofile")){
 	 			data = userService.updateUserProfile(model);
 	 		}else if(getId().equalsIgnoreCase("addSocialLogin")){
 	 			data = userService.socialLogin(model); 	 			
 	 		}else if(getId().equalsIgnoreCase("updateProfileImage")){
 	 			data = userService.updateProfileImage(model);			
 	 		}
 	 		
 	 		response.setStatus(200);
 	 		response.getWriter().write(data);
 			
    	}catch(Exception e){
    		e.printStackTrace();
    	}
 		return null;
 	}
 	
	//@Override
	
    public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	
	public Integer getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}
	
    public Object getModel() {
    	return (list != null ? list : model);
	}
    
	
}
