package com.ulopms.api;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Property implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Integer propertyId;
	
	private String propertyName;
	
	private String propertyDescription;
	
	private String propertyUrl;
	
	private String photoPath;
	
	private Boolean isDeleted;
	
	private String checkIn;
	
	private String checkOut;
		
	private String bookingDate;
	
	private String couponCode;
	
	private Integer tripTypeId;
	
	public Property(){	
		
		
	}
	
	public Property(Integer propertyId, String propertyName, String propertyDescription) {
		this.propertyId = propertyId;
		this.propertyName = propertyName;
		this.propertyDescription = propertyDescription; 
	}

	public Integer getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(Integer propertyId) {
		this.propertyId = propertyId;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public String getPropertyDescription() {
		return propertyDescription;
	}

	public void setPropertyDescription(String propertyDescription) {
		this.propertyDescription = propertyDescription;
	}

	public String getPropertyUrl() {
		return propertyUrl;
	}

	public void setPropertyUrl(String propertyUrl) {
		this.propertyUrl = propertyUrl;
	}

	public String getPhotoPath() {
		return photoPath;
	}

	public void setPhotoPath(String photoPath) {
		this.photoPath = photoPath;
	}

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getCheckIn() {
		return checkIn;
	}

	public void setCheckIn(String checkIn) {
		this.checkIn = checkIn;
	}

	public String getCheckOut() {
		return checkOut;
	}

	public void setCheckOut(String checkOut) {
		this.checkOut = checkOut;
	}

	public String getBookingDate() {
		return bookingDate;
	}

	public void setBookingDate(String bookingDate) {
		this.bookingDate = bookingDate;
	}

	public String getCouponCode() {
		return couponCode;
	}

	public void setCouponCode(String couponCode) {
		this.couponCode = couponCode;
	}

	public Integer getTripTypeId() {
		return tripTypeId;
	}

	public void setTripTypeId(Integer tripTypeId) {
		this.tripTypeId = tripTypeId;
	}

	
}