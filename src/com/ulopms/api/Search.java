package com.ulopms.api;

import java.io.File;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Search implements Serializable {
	private static final long serialVersionUID = 1L;
	private String id;
	private String propertyTagId;
	private String locationId;
	private String propertyTagName;
	private String checkIn;
	private String checkOut;
	private String propertyId;
	private String url;
	private String type;
	private String locationName;
	private String metaTag;
	private String description;
	private String content;
	private String emailId;
	private String roleId;
	private String roleName;
	private String mobileNumber;
	private String corporateName;
	private String employeeName;
	private String corporateEmail;
	private String corporateNumber;
	private String partnerMessage;
	private String partnerName;
	private String partnerNumber;
	private String partnerEmail;
	private String partnerLocation;
	private String partnerHotel;
	private File myFile;
	private String message;
	
	public Search(){	
		
		
		
		
	}
	
	public Search(String propertyTagId,String locationId, String propertyTagName, String checkIn,String checkOut,String propertyId) {
		this.propertyTagId = propertyTagId;
		this.propertyTagName = propertyTagName;
		this.locationId=locationId;
		this.checkIn=checkIn;
		this.checkOut=checkOut;
		this.propertyId=propertyId;
	}

	public String getPropertyTagId() {
		return propertyTagId;
	}

	public void setPropertyTagId(String propertyTagId) {
		this.propertyTagId = propertyTagId;
	}

	public String getPropertyTagName() {
		return propertyTagName;
	}

	public void setPropertyTagName(String propertyTagName) {
		this.propertyTagName = propertyTagName;
	}

	public String getLocationId() {
		return locationId;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	public String getCheckIn() {
		return checkIn;
	}

	public void setCheckIn(String checkIn) {
		this.checkIn = checkIn;
	}

	public String getCheckOut() {
		return checkOut;
	}

	public void setCheckOut(String checkOut) {
		this.checkOut = checkOut;
	}

	public String getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(String propertyId) {
		this.propertyId = propertyId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public String getMetaTag() {
		return metaTag;
	}

	public void setMetaTag(String metaTag) {
		this.metaTag = metaTag;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getCorporateName() {
		return corporateName;
	}

	public void setCorporateName(String corporateName) {
		this.corporateName = corporateName;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public String getCorporateEmail() {
		return corporateEmail;
	}

	public void setCorporateEmail(String corporateEmail) {
		this.corporateEmail = corporateEmail;
	}

	public String getCorporateNumber() {
		return corporateNumber;
	}

	public void setCorporateNumber(String corporateNumber) {
		this.corporateNumber = corporateNumber;
	}

	public String getPartnerMessage() {
		return partnerMessage;
	}

	public void setPartnerMessage(String partnerMessage) {
		this.partnerMessage = partnerMessage;
	}

	public String getPartnerName() {
		return partnerName;
	}

	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}

	public String getPartnerNumber() {
		return partnerNumber;
	}

	public void setPartnerNumber(String partnerNumber) {
		this.partnerNumber = partnerNumber;
	}

	public String getPartnerEmail() {
		return partnerEmail;
	}

	public void setPartnerEmail(String partnerEmail) {
		this.partnerEmail = partnerEmail;
	}

	public String getPartnerLocation() {
		return partnerLocation;
	}

	public void setPartnerLocation(String partnerLocation) {
		this.partnerLocation = partnerLocation;
	}

	public String getPartnerHotel() {
		return partnerHotel;
	}

	public void setPartnerHotel(String partnerHotel) {
		this.partnerHotel = partnerHotel;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public File getMyFile() {
		return myFile;
	}

	public void setMyFile(File myFile) {
		this.myFile = myFile;
	}

		
}