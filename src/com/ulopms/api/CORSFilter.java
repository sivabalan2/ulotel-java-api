package com.ulopms.api;
import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
 
/**
 * Servlet Filter implementation class CORSFilter
 */
// Enable it for Servlet 3.x implementations
/* @ WebFilter(asyncSupported = true, urlPatterns = { "/*" }) */
public class CORSFilter implements Filter {
 
    /**
     * Default constructor.
     */
	private TokenService tokenService;
	private String data;
    public CORSFilter() {
        // TODO Auto-generated constructor stub
    	this.tokenService = new TokenService();
    }
 
    /**
     * @see Filter#destroy()
     */
    public void destroy() {
        // TODO Auto-generated method stub
    }
 
    /**
     * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
     */
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain)
            throws IOException, ServletException {
 
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        String token = request.getHeader("token");
        // Authorize (allow) all domains to consume the content
        ((HttpServletResponse) servletResponse).addHeader("Access-Control-Allow-Origin", "*");
        ((HttpServletResponse) servletResponse).addHeader("Access-Control-Allow-Methods","GET, OPTIONS, HEAD, PUT, POST");
        ((HttpServletResponse) servletResponse).addHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
        ((HttpServletResponse) servletResponse).addHeader("Content-Type", "application/json");
//        ((HttpServletResponse) servletResponse).addHeader("Access-Control-Allow-Credentials" ,true);
        HttpServletResponse response = (HttpServletResponse) servletResponse;
 
        // For HTTP OPTIONS verb/method reply with ACCEPTED status code
        if (request.getMethod().equals("OPTIONS")) {
            response.setStatus(HttpServletResponse.SC_ACCEPTED);
            return;
        }
 
        if (allowRequestWithoutToken(request)) {

	        response.setStatus(HttpServletResponse.SC_OK);
	
	        chain.doFilter(request, response);
	 
        } else {
	
        	if (token == null || !tokenService.isTokenValid(token)) {

	            response.sendError(HttpServletResponse.SC_UNAUTHORIZED);

	        } else {

	            String email = tokenService.getUserIdFromToken(token,"email");
	            
	            request.setAttribute("emailId", email);
	            
	            this.data=tokenService.getUserDetails(email);
	            response.getWriter().write(this.data);	
	            
	            chain.doFilter(request, response);
	  
	        }
         }
        // pass the request along the filter chain
        //chain.doFilter(request, servletResponse);
    }
 
    public boolean allowRequestWithoutToken(HttpServletRequest request) {
        if (request.getRequestURI().contains("/login") || request.getRequestURI().contains("/users")
        		|| request.getRequestURI().contains("/hotels") || request.getRequestURI().contains("/properties") 
        		|| request.getRequestURI().contains("/locations") || request.getRequestURI().contains("/config")
        		|| request.getRequestURI().contains("/search") || request.getRequestURI().contains("/booking")) {
      
          return true;
    
        }
          
        return false;
    
    }
    
    /**
     * @see Filter#init(FilterConfig)
     */
    public void init(FilterConfig fConfig) throws ServletException {
        // TODO Auto-generated method stub
    }
 
}