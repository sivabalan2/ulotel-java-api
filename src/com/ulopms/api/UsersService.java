package com.ulopms.api;

import com.ulopms.view.GuestAction;
import com.ulopms.view.UserAction;
import com.ulopms.view.UserAddAction;



public class UsersService {
	
	public UsersService() {
	}

	public String userProfile(Users user) {
		String str="";
		try{
			UserAddAction userAction=new UserAddAction();
			str=userAction.getUserProfile(user);
		}
		catch(Exception e){
			e.printStackTrace();
			
		}
		
		return str;
	}
	
	public String upcomingBooking(Users user) {
		String str="";
		try{
			UserAction userAction=new UserAction();
			str=userAction.getUpcomingBooking(user);
		}
		catch(Exception e){
			e.printStackTrace();
			
		}
		
		return str;
	}
	
	public String pastBooking(Users user) {
		String str="";
		try{
			UserAction userAction=new UserAction();
			str=userAction.getPastBooking(user);
		}
		catch(Exception e){
			e.printStackTrace();
			
		}
		
		return str;
	}
	
	public String uloUserProfile(Users user){
		String data="";
		try{
			UserAction users=new UserAction();
			data=users.addUserProfile(user);
		}catch(Exception e){
			e.printStackTrace();
		}
		return data;
	}
	
	public String updateUserProfile(Users user){
		String data="";
		try{
			UserAddAction users=new UserAddAction();
			data=users.updateProfile(user);
		}catch(Exception e){
			e.printStackTrace();
		}
		return data;
	}
	
	public String socialLogin(Users users){
		String data="";
		try{
			UserAction user=new UserAction();
			data=user.addSocialUser(users);
		}catch(Exception e){
			e.printStackTrace();
		}
		return data;
	}
	
	public String updateProfileImage(Users users){
		String data="";
		try{
			UserAction user=new UserAction();
			data=user.updateProfileImage(users);
		}catch(Exception e){
			e.printStackTrace();
		}
		return data;
	}
	
	public String sendPayatHotelOtp(Users users){
		String data="";
		try{
			GuestAction guest=new GuestAction();
			data=guest.sendPayAtHotelOtpApi(users);
		}catch(Exception e){
			e.printStackTrace();
		}
		return data;
	}
	
	public String resendPayatHotelOtp(Users users){
		String data="";
		try{
			GuestAction guest=new GuestAction();
			data=guest.resendPayAtHotelApi(users);
		}catch(Exception e){
			e.printStackTrace();
		}
		return data;
	}
	
	public String verifyPayatHotelOtp(Users users){
		String data="";
		try{
			GuestAction guest=new GuestAction();
			data=guest.verifyPayatHotelOtp(users);
		}catch(Exception e){
			e.printStackTrace();
		}
		return data;
	}
}
