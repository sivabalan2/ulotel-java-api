package com.ulopms.api;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.ulopms.view.LocationAction;
import com.ulopms.view.UserAction;



public class TokenService {


	public TokenService() {
	}
	
	public String getJwtTokenVerifier() {
		String str=null;
		try{
			Hotels hotels=new Hotels();
			LocationAction properties = new LocationAction();
			str=properties.getListPropertiesApi(hotels);
			
		}
		catch(Exception e){
			e.printStackTrace();
			
		}
		
		return str;
	}

	public String getUserIdFromToken(String token,String key) {

	    try {
	    	//token="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyTmFtZSI6Ik1vaGFuIiwicm9sZVR5cGUiOiJDdXN0b21lciIsImVtYWlsIjoidm1vaGFua3BtQGdtYWlsLmNvbSJ9.QXRxic3CMqF0O4SsqIMC6CWYkXa2zpMrTX3oG_jZG2g";
	        Algorithm algorithm = Algorithm.HMAC256("secret");

	        JWTVerifier verifier = JWT.require(algorithm).build();

	        DecodedJWT jwt = verifier.verify(token);

	        return jwt.getClaim(key).asString();

	    } catch (JWTVerificationException e) {
	
	    	e.printStackTrace();
	    	return null;
	    }
	}

	public String getUserDetails(String email){
		String data="";
		try{
			
			UserAction user=new UserAction();
			data=user.getUserDetails(email);
		}catch(Exception e){
			
		}
		return data;
	}
   
	public boolean isTokenValid(String token) {

	    String email = getUserIdFromToken(token,"email");

	    return email != null;

	}


}
