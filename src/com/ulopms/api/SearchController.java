package com.ulopms.api;


import java.util.Collection;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.rest.DefaultHttpHeaders;
import org.apache.struts2.rest.HttpHeaders;

import com.opensymphony.xwork2.ModelDriven;





@Results({
    @Result(name="success", type="redirectAction", params = {"actionName" , "search"})
})


public class SearchController implements ModelDriven<Object>{

	private String id;
	
	public Search configure = new Search();
	
	private Collection<Search> list;
	 
    private Search model = new Search();
    
	
    private SearchService userSearchService = new SearchService();
    Integer statusCode;
    
 // GET	/api/id/search
    
    //http://localhost:8080/ulopms/api/search/usersearch.json
    //{"locationId":"6","propertyTagId":"1","checkIn":"April 20,2019","checkOut":"April 21,2019"}
    public HttpHeaders show(){
    	try{
    		HttpServletResponse response = ServletActionContext.getResponse();
    		response.flushBuffer();
 			response.setContentType("application/json");
 	 		String data = null;
 	 		if(getId().equalsIgnoreCase("usersearch")){
 	 			data = userSearchService.findByLocation(model);	
 	 		}else if(getId().equalsIgnoreCase("seocontent")){
 	 			data = userSearchService.findSeoContent(model);
 	 		}else if(getId().equalsIgnoreCase("promotionImages")){
 	 			data = userSearchService.findPromotionImages(model);
 	 		}
 	 		
 	 		
 	 		response.getWriter().write(data);
 	 		
    	}catch(Exception e){
    		
    	}
    	return null;
    }
    
    //GET /api/search
 	public HttpHeaders index() {
 		try{
 			HttpServletResponse response = ServletActionContext.getResponse();
 			response.flushBuffer();
 			response.setContentType("application/json");
 	 		String data = null;
 	 		
 	 		data = userSearchService.findAll();
 	 		
 	 		response.getWriter().write(data);
 	 		
 		}catch(Exception e){
 			e.printStackTrace();
 		}
 		return null;
 		
 	}
 	//POST /api/search
 	public HttpHeaders create() {
   	 
    	try{
    		String data=null;
    		HttpServletResponse response = ServletActionContext.getResponse();
    		response.flushBuffer();
    		response.setContentType("application/json");
    		/*data=userSearchService.addSubcriberMail(model);
    		response.getWriter().write("{\"status\" : \""+data+"\"}");*/
 			
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    	
        return null;
    }
 	
 	public HttpHeaders update(){
 		try{
 			String data=null;
    		HttpServletResponse response = ServletActionContext.getResponse();
    		response.flushBuffer();
    		response.setContentType("application/json");
    		if(getId().equalsIgnoreCase("corporateEnquiry")){
    			data=userSearchService.addCorporateEnquiry(model);	
    		}else if(getId().equalsIgnoreCase("subcriberMail")){
    			data=userSearchService.addSubcriberMail(model);
    		}else if(getId().equalsIgnoreCase("partnerEnquiry")){
    			data=userSearchService.addPartnerWithUS(model);
    		}else if(getId().equalsIgnoreCase("uploadResume") ){
    			data=userSearchService.uploadResume(model);
    		}
    		
    		response.getWriter().write("{\"status\" : \""+data+"\"}");
 		}catch(Exception e){
 			e.printStackTrace();
 		}
 		return null;
 	}
	//@Override
	
    public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	
	public Integer getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}
	
    public Object getModel() {
    	return (list != null ? list : model);
	}
    
	
}
