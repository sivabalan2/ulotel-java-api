package com.ulopms.api;


import java.util.Collection;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.rest.DefaultHttpHeaders;
import org.apache.struts2.rest.HttpHeaders;

import com.opensymphony.xwork2.ModelDriven;
//import com.sun.mail.iap.Response;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.ulopms.controller.LocationManager;
import com.ulopms.controller.UserLoginManager;
import com.ulopms.model.Location;
import com.ulopms.model.User;





@Results({
    @Result(name="success", type="redirectAction", params = {"actionName" , "locations"})
})


public class LocationsController implements ModelDriven<Object>{

	
	
   
   private List<Location> locationList;
   /* private Collection<Locations> list;*/
	private String id;
	
	public Locations location = new Locations();
	 
	private Object locations;
	
    private LocationsService locationService = new LocationsService();
    
    
	
	
   
    Integer statusCode;
    

 // GET	/api/locations
 	public HttpHeaders index() {
 		try{
 			HttpServletResponse response = ServletActionContext.getResponse();
 			response.flushBuffer();
 			response.setContentType("application/json");
 	 		String data = null;
 	 		data = locationService.findAll();
 	 		response.getWriter().write(data);
 	 		
 		}catch(Exception e){
 			e.printStackTrace();
 		}
 		return new DefaultHttpHeaders("index").disableCaching();
 		
 	}
 	
	
 // GET	/api/users/1
 	public HttpHeaders show() {
 		try{
 			HttpServletResponse response = ServletActionContext.getResponse();
 			response.setContentType("application/json");
 	 		
 		}catch(Exception e){
 			e.printStackTrace();
 		}
 		return new DefaultHttpHeaders("show")
        .disableCaching();
 	}
     
 	
 	
 	public HttpHeaders create() {
		return new DefaultHttpHeaders("create").disableCaching();
	}
      
	 
	//@Override
	
    public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Locations getLocation() {
		return location;
	}

	public void setLocation(Locations location) {
		this.location = location;
	}
	

	public LocationsService getLocationService() {
		return locationService;
	}

	public void setLocationService(LocationsService locationService) {
		this.locationService = locationService;
	}

	public Integer getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}
	
    public Object getModel() {
		if(locations == null) {
			return location;	
		} else {
			return locations;
		}
	}
    
	/*public Object getModel() {
		// TODO Auto-generated method stub
		return (list != null ? list : model);
	}*/
	
	/*public Object getModel1() {
		// TODO Auto-generated method stub
		return (list != null ? list : model1);
	}*/
	
}
