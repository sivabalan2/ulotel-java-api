package com.ulopms.api;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Hotels implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String propertyId;
	
	private String propertyName;
	
	private String propertyDescription;
	
	private String propertyUrl;
	
	private String locationUrl;
	
	private String photoPath;
	
	private Integer locationId;
	
	private String locationName;
	
	private Boolean payAtHotelStatus;
	
	private Boolean coupleStatus;
	
	private Boolean breakfastStatus;
	
	private Timestamp arrivalDate;
	
	private Timestamp departureDate;
	
	private String startDate;
	
	private String endDate;
	
	private Integer diffDays;
	
	private Integer photoId;
	
	private Boolean isPrimary;
	
	private Integer amenityId;
	
	private String amenityName;
	
	private String amenityIcon;
	
	private String photos;
	
	private String amenities;
	
	private Integer accommodationId;
	
	private String accommodationName;
	
	private long available;
	
	private double minimumAmount;
	
	private double maximumAmount;
	
	private double baseAmount;
	
	private String promotionName;
	
	private String promotionType;
	
	private Integer tripTypeId;

	private Integer hotelTypeId;
	
	private String tripType;
	
	private String hotelType;
	
	private String offerType;
	
	private String areaType;
	
	
	
	public Hotels(){	
		
		
		
	}
	

	public String getPropertyId() {
		return propertyId;
	}

	public void setPropertyId(String propertyId) {
		this.propertyId = propertyId;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public String getPropertyDescription() {
		return propertyDescription;
	}

	public void setPropertyDescription(String propertyDescription) {
		this.propertyDescription = propertyDescription;
	}

	public String getPropertyUrl() {
		return propertyUrl;
	}

	public void setPropertyUrl(String propertyUrl) {
		this.propertyUrl = propertyUrl;
	}

	public String getPhotoPath() {
		return photoPath;
	}

	public void setPhotoPath(String photoPath) {
		this.photoPath = photoPath;
	}


	public Integer getLocationId() {
		return locationId;
	}


	public void setLocationId(Integer locationId) {
		this.locationId = locationId;
	}


	public String getLocationName() {
		return locationName;
	}


	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}


	public Boolean getPayAtHotelStatus() {
		return payAtHotelStatus;
	}


	public void setPayAtHotelStatus(Boolean payAtHotelStatus) {
		this.payAtHotelStatus = payAtHotelStatus;
	}


	public Boolean getCoupleStatus() {
		return coupleStatus;
	}


	public void setCoupleStatus(Boolean coupleStatus) {
		this.coupleStatus = coupleStatus;
	}


	public Boolean getBreakfastStatus() {
		return breakfastStatus;
	}


	public void setBreakfastStatus(Boolean breakfastStatus) {
		this.breakfastStatus = breakfastStatus;
	}


	public Timestamp getArrivalDate() {
		return arrivalDate;
	}


	public void setArrivalDate(Timestamp arrivalDate) {
		this.arrivalDate = arrivalDate;
	}


	public Timestamp getDepartureDate() {
		return departureDate;
	}


	public void setDepartureDate(Timestamp departureDate) {
		this.departureDate = departureDate;
	}


	public Integer getDiffDays() {
		return diffDays;
	}


	public void setDiffDays(Integer diffDays) {
		this.diffDays = diffDays;
	}


	public Integer getPhotoId() {
		return photoId;
	}


	public void setPhotoId(Integer photoId) {
		this.photoId = photoId;
	}


	public Boolean getIsPrimary() {
		return isPrimary;
	}


	public void setIsPrimary(Boolean isPrimary) {
		this.isPrimary = isPrimary;
	}


	public Integer getAmenityId() {
		return amenityId;
	}


	public void setAmenityId(Integer amenityId) {
		this.amenityId = amenityId;
	}


	public String getAmenityName() {
		return amenityName;
	}


	public void setAmenityName(String amenityName) {
		this.amenityName = amenityName;
	}


	public String getAmenityIcon() {
		return amenityIcon;
	}


	public void setAmenityIcon(String amenityIcon) {
		this.amenityIcon = amenityIcon;
	}


	public String getPhotos() {
		return photos;
	}


	public void setPhotos(String photos) {
		this.photos = photos;
	}


	public String getAmenities() {
		return amenities;
	}


	public void setAmenities(String amenities) {
		this.amenities = amenities;
	}


	public Integer getAccommodationId() {
		return accommodationId;
	}


	public void setAccommodationId(Integer accommodationId) {
		this.accommodationId = accommodationId;
	}


	public String getAccommodationName() {
		return accommodationName;
	}


	public void setAccommodationName(String accommodationName) {
		this.accommodationName = accommodationName;
	}


	public long getAvailable() {
		return available;
	}


	public void setAvailable(long available) {
		this.available = available;
	}


	public double getMinimumAmount() {
		return minimumAmount;
	}


	public void setMinimumAmount(double minimumAmount) {
		this.minimumAmount = minimumAmount;
	}


	public double getMaximumAmount() {
		return maximumAmount;
	}


	public void setMaximumAmount(double maximumAmount) {
		this.maximumAmount = maximumAmount;
	}


	public double getBaseAmount() {
		return baseAmount;
	}


	public void setBaseAmount(double baseAmount) {
		this.baseAmount = baseAmount;
	}


	public String getPromotionName() {
		return promotionName;
	}


	public void setPromotionName(String promotionName) {
		this.promotionName = promotionName;
	}


	public String getPromotionType() {
		return promotionType;
	}


	public void setPromotionType(String promotionType) {
		this.promotionType = promotionType;
	}

	public Integer getTripTypeId() {
		return tripTypeId;
	}


	public void setTripTypeId(Integer tripTypeId) {
		this.tripTypeId = tripTypeId;
	}


	public String getStartDate() {
		return startDate;
	}


	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}


	public String getEndDate() {
		return endDate;
	}


	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}


	public String getLocationUrl() {
		return locationUrl;
	}


	public void setLocationUrl(String locationUrl) {
		this.locationUrl = locationUrl;
	}


	public Integer getHotelTypeId() {
		return hotelTypeId;
	}


	public void setHotelTypeId(Integer hotelTypeId) {
		this.hotelTypeId = hotelTypeId;
	}

	public String getTripType() {
		return tripType;
	}

	public void setTripType(String tripType) {
		this.tripType = tripType;
	}

	public String getHotelType() {
		return hotelType;
	}

	public void setHotelType(String hotelType) {
		this.hotelType = hotelType;
	}

	public String getOfferType() {
		return offerType;
	}

	public void setOfferType(String offerType) {
		this.offerType = offerType;
	}
	
	public String getAreaType(){
		return areaType;
	}
	
	public void setAreaType(String areaType){
		this.areaType=areaType;
	}

}