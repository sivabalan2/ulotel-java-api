package com.ulopms.api;


import java.util.HashMap;
import java.util.Map;

import com.ulopms.view.BookingAction;
import com.ulopms.view.BookingCancelAction;
import com.ulopms.view.UserAction;




public class BookingService {

	private static Map<String,Booking> booking = new HashMap<String,Booking>();


	public String addBooking(Booking booking){
		String data=null;
		try{
			BookingAction action=new BookingAction();
			data=action.addBookingAPI(booking);
		}catch(Exception e){
			e.printStackTrace();
		}
		return data;
	}
	
	public String cancelledRequest(Booking booking){
		String data=null;
		try{
			BookingCancelAction action=new BookingCancelAction();
			data=action.cancelledRequestVoucher(booking);
		}catch(Exception e){
			e.printStackTrace();
		}
		return data;
	}

	public String bookingConfirmation(Booking booking){
		String data=null;
		try{
			BookingAction action=new BookingAction();
			data=action.getBookingAPI(booking);
		}catch(Exception e){
			e.printStackTrace();
		}
		return data;
	}
}
