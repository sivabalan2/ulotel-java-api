package com.ulopms.api;
import com.ulopms.view.BookingAction;
import com.ulopms.view.PropertyDiscountAction;


public class PropertiesService {

	public PropertiesService() {
	}
	
	public String findAll(Property property) {
		String str=null;
		try{
			BookingAction properties = new BookingAction();
			str=properties.getSelectedPropertyAPI(property);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return str;
	}
	
	public String findById(Property property) {
		String data=null;
		try{
			BookingAction prop = new BookingAction();
			data=prop.getSelectedPropertyAPI(property);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return data;
	}

	public String findCoupons(Property property) {
		String data=null;
		try{
			PropertyDiscountAction discount= new PropertyDiscountAction();
			data=discount.getCouponsAPI(property);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return data;
	}
	
	public String findAllCoupons(Property property) {
		String data=null;
		try{
			PropertyDiscountAction discount= new PropertyDiscountAction();
			data=discount.getAllCouponsAPI(property);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return data;
	}
	
	public String verifyCoupon(Property property) {
		String data=null;
		try{
			PropertyDiscountAction discount= new PropertyDiscountAction();
			data=discount.getVerifyCouponsAPI(property);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return data;
	}

}
