package com.ulopms.api;


import java.util.Collection;
import java.util.List;

import javax.servlet.Servlet;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.rest.DefaultHttpHeaders;
import org.apache.struts2.rest.HttpHeaders;

import com.opensymphony.xwork2.ModelDriven;
//import com.sun.mail.iap.Response;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.ulopms.model.PmsTags;
import com.ulopms.ota.CancelledListing;




@Results({
    @Result(name="success", type="redirectAction", params = {"actionName" , "booking"})
})


public class BookingController implements ModelDriven<Object>{
	
	   
		private String id;
		
		public Booking model = new Booking();
		 
		private Object booking;
		
	    private BookingService bookingService = new BookingService();
	    Integer statusCode;
	    
	    public HttpHeaders show(){
	    	try{
	    		String data="";
	    		HttpServletResponse response=ServletActionContext.getResponse();
	    		response.flushBuffer();
	    		response.setContentType("application/json");
	    		if(getId().equalsIgnoreCase("cancelledRequest")){
	    			data=bookingService.cancelledRequest(model);
	    		}else if(getId().equalsIgnoreCase("bookingConfirmation")){
	    			data=bookingService.bookingConfirmation(model);
	    		}
	    		response.setStatus(200);
	    		response.getWriter().write(data);
	    	}catch(Exception e){
	    		e.printStackTrace();
	    	}
	    	return null;
	    }
	    
	  //POST /api/booking
	 	public HttpHeaders create() {
	   	 
	    	try{
	    		HttpServletResponse response = ServletActionContext.getResponse();
	    		response.flushBuffer();
	 			response.setContentType("application/json");
	 	 		String data = null;
	 	 		data = bookingService.addBooking(model);
	 	 		response.setStatus(200);
	 	 		
	 	 		
	 	 		response.getWriter().write(data);
	 			
	    	}catch(Exception e){
	    		e.printStackTrace();
	    	}
	    	
	        return null;
	    }
	 	
		//@Override
		
	    public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		
		public Integer getStatusCode() {
			return statusCode;
		}

		public void setStatusCode(Integer statusCode) {
			this.statusCode = statusCode;
		}
		
	    public Object getModel() {
			if(booking == null) {
				return model;	
			} else {
				return booking;
			}
		}
	
}
