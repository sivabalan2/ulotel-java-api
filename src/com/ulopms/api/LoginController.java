package com.ulopms.api;


import java.util.Collection;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.rest.DefaultHttpHeaders;
import org.apache.struts2.rest.HttpHeaders;

import com.opensymphony.xwork2.ModelDriven;
//import com.sun.mail.iap.Response;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.ulopms.controller.LocationManager;
import com.ulopms.controller.UserLoginManager;
import com.ulopms.model.Location;
import com.ulopms.model.User;





@Results({
    @Result(name="success", type="redirectAction", params = {"actionName" , "login"})
})


public class LoginController implements ModelDriven<Object>{

	private String id;
	
	public Login configure = new Login();
	
	private Collection<Search> list;
	 
    private Login model = new Login();
    
	
    private LoginService loginService = new LoginService();
    Integer statusCode;
    
    public HttpHeaders show(){
    	try{
    		HttpServletResponse response = ServletActionContext.getResponse();
    		response.flushBuffer();
 			response.setContentType("application/json");
 	 		String data = null;
 	 		if(getId().equalsIgnoreCase("sendOtp")){
 	 			data = loginService.sendOtp(model);	
 	 		}else if(getId().equalsIgnoreCase("resendOtp")){
 	 			data = loginService.resendOtp(model);
 	 		}else if(getId().equalsIgnoreCase("verifyLoginOtp")){
 	 			data = loginService.verifyLoginOtp(model);
 	 		}
 	 		
 	 		
 	 		response.getWriter().write(data);
 	 		
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    	return null;
    }
    
    //GET /api/search
 	public HttpHeaders index() {
 		try{
 			HttpServletResponse response = ServletActionContext.getResponse();
 			response.flushBuffer();
 			response.setContentType("application/json");
 	 		String data = null;
 	 		
 	 		data = loginService.findAll();
 	 		
 	 		response.getWriter().write(data);
 	 		
 		}catch(Exception e){
 			e.printStackTrace();
 		}
 		return null;
 		
 	}
 	//POST /api/search
 	public HttpHeaders create() {
   	 
    	try{
    		HttpServletResponse response = ServletActionContext.getResponse();
    		response.flushBuffer();
 			response.setContentType("application/json");
 	 		String data = "";
 	 		response.setStatus(200);
 	 		
 	 		response.getWriter().write(data);
 			
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    	
        return null;
    }
 	
	//@Override
	
    public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	
	public Integer getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}
	
    public Object getModel() {
    	return (list != null ? list : model);
	}
    
	
}
