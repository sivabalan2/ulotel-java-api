
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

//import javax.xml.bind.annotation.XmlElement;
//import javax.xml.bind.annotation.XmlRootElement;

//import com.thoughtworks.xstream.annotations.XStreamAlias;

//@XStreamAlias("Booking")
//@XmlRootElement(name ="Booking")
public class Booking implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String BookingId;
	
	private String HotelCode;
	
	private String CustomerName;
	
	private long NoOfRooms;
	
	private long NoOfNights;
	
	private String RoomTypeName;
	
	private String CheckInDate;
	
	private String CheckIn;
	
	private String BookingTimeDate;
	
	private String Status;
	
	private boolean PayAtHotelFlag;
	
	private String BookingTime;
	
	private Timestamp ArrivalDate;
	

	private String BookingVendorName;
	
	private long CancellationId;

	
	private Date CancellationDate;
	
	private Double BookingAmount;
	
	private Double CancellationCharges;

	private Double RefundAmount;
	
	private String CancellationStatus;
	
	private Integer Rooms;
	
	private Integer Nights;
	
	private String OtaName;
	
	private List books = new ArrayList();
	
	public Booking(){	
		
		
	}
	
	
	public Booking(String customerName,String roomTypeName, Integer bookingId){
		
	}


	public String getBookingId() {
		return BookingId;
	}


	public void setBookingId(String BookingId) {
		BookingId = BookingId;
	}


	public String getHotelCode() {
		return HotelCode;
	}


	public void setHotelCode(String HotelCode) {
		HotelCode = HotelCode;
	}


	public String getCustomerName() {
		return CustomerName;
	}


	public void setCustomerName(String CustomerName) {
		CustomerName = CustomerName;
	}


	public long getNoOfRooms() {
		return NoOfRooms;
	}


	public void setNoOfRooms(long NoOfRooms) {
		NoOfRooms = NoOfRooms;
	}


	public long getNoOfNights() {
		return NoOfNights;
	}


	public void setNoOfNights(long NoOfNights) {
		NoOfNights = NoOfNights;
	}


	public String getRoomTypeName() {
		return RoomTypeName;
	}


	public void setRoomTypeName(String RoomTypeName) {
		RoomTypeName = RoomTypeName;
	}


	public String getCheckInDate() {
		return CheckInDate;
	}


	public void setCheckInDate(String CheckInDate) {
		CheckInDate = CheckInDate;
	}


	public String getCheckIn() {
		return CheckIn;
	}


	public void setCheckIn(String CheckIn) {
		CheckIn = CheckIn;
	}


	public String getBookingTimeDate() {
		return BookingTimeDate;
	}


	public void setBookingTimeDate(String BookingTimeDate) {
		BookingTimeDate = BookingTimeDate;
	}


	public String getStatus() {
		return Status;
	}


	public void setStatus(String Status) {
		Status = Status;
	}


	public boolean getPayAtHotelFlag() {
		return PayAtHotelFlag;
	}


	public void setPayAtHotelFlag(boolean PayAtHotelFlag) {
		PayAtHotelFlag = PayAtHotelFlag;
	}


	public String getBookingTime() {
		return BookingTime;
	}


	public void setBookingTime(String BookingTime) {
		BookingTime = BookingTime;
	}


	public Timestamp getArrivalDate() {
		return ArrivalDate;
	}


	public void setArrivalDate(Timestamp ArrivalDate) {
		ArrivalDate = ArrivalDate;
	}


	public String getBookingVendorName() {
		return BookingVendorName;
	}


	public void setBookingVendorName(String BookingVendorName) {
		BookingVendorName = BookingVendorName;
	}


	public long getCancellationId() {
		return CancellationId;
	}


	public void setCancellationId(long CancellationId) {
		CancellationId = CancellationId;
	}


	public Date getCancellationDate() {
		return CancellationDate;
	}


	public void setCancellationDate(Date CancellationDate) {
		CancellationDate = CancellationDate;
	}


	public Double getBookingAmount() {
		return BookingAmount;
	}


	public void setBookingAmount(Double BookingAmount) {
		BookingAmount = BookingAmount;
	}


	public Double getCancellationCharges() {
		return CancellationCharges;
	}


	public void setCancellationCharges(Double CancellationCharges) {
		CancellationCharges = CancellationCharges;
	}


	public Double getRefundAmount() {
		return RefundAmount;
	}


	public void setRefundAmount(Double RefundAmount) {
		RefundAmount = RefundAmount;
	}


	public String getCancellationStatus() {
		return CancellationStatus;
	}


	public void setCancellationStatus(String CancellationStatus) {
		CancellationStatus = CancellationStatus;
	}


	public Integer getRooms() {
		return Rooms;
	}


	public void setRooms(Integer Rooms) {
		Rooms = Rooms;
	}


	public Integer getNights() {
		return Nights;
	}


	public void setNights(Integer Nights) {
		Nights = Nights;
	}


	public String getOtaName() {
		return OtaName;
	}


	public void setOtaName(String OtaName) {
		OtaName = OtaName;
	}
	
	
	
	
	}