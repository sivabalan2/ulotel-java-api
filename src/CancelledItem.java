
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.sun.xml.internal.txw2.annotation.XmlElement;
import com.thoughtworks.xstream.annotations.XStreamAlias;



public  class CancelledItem {
	
		
	@JacksonXmlProperty(localName = "CancellationId")
	private String CancellationId;
	
	@JacksonXmlProperty(localName = "CancellationDate")
	private String CancellationDate;
	
	@JacksonXmlProperty(localName = "BookingAmount")
	private Double BookingAmount;
	
	@JacksonXmlProperty(localName = "CancellationCharges")
	private Double CancellationCharges;

	@JacksonXmlProperty(localName = "RefundAmount")
	private Double RefundAmount;
	
	@JacksonXmlProperty(localName = "CancellationStatus")
	private String CancellationStatus;
	
	@JacksonXmlProperty(localName = "BookingId")
	private String BookingId;
	
	
public CancelledItem(){	
		
		
		
	}

     
	public String getCancellationId() {
		return CancellationId;
	}


	public void setCancellationId(String CancellationId) {
		CancellationId = CancellationId;
	}


	public  String getCancellationDate() {
		return CancellationDate;
	}


	public void setCancellationDate(String CancellationDate) {
		CancellationDate = CancellationDate;
	}


	public Double getBookingAmount() {
		return BookingAmount;
	}


	public void setBookingAmount(Double BookingAmount) {
		BookingAmount = BookingAmount;
	}


	public Double getCancellationCharges() {
		return CancellationCharges;
	}


	public void setCancellationCharges(Double CancellationCharges) {
		CancellationCharges = CancellationCharges;
	}


	public Double getRefundAmount() {
		return RefundAmount;
	}


	public void setRefundAmount(Double RefundAmount) {
		RefundAmount = RefundAmount;
	}


	public String getCancellationStatus() {
		return CancellationStatus;
	}


	public void setCancellationStatus(String CancellationStatus) {
		CancellationStatus = CancellationStatus;
	}

	public String getBookingId() {
		return BookingId;
	}

	public void setBookingId(String BookingId) {
		BookingId = BookingId;
	}

}
