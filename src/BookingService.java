

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.json.JSONObject;
import org.json.XML;





import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.ulopms.controller.BookingDetailManager;
import com.ulopms.controller.BookingGuestDetailManager;
import com.ulopms.controller.OtaHotelsManager;
import com.ulopms.controller.PmsBookingManager;
import com.ulopms.controller.PmsGuestManager;
import com.ulopms.controller.PmsPropertyManager;
import com.ulopms.controller.PmsSourceManager;
import com.ulopms.controller.PmsStatusManager;
import com.ulopms.controller.PropertyAccommodationInventoryManager;
import com.ulopms.controller.PropertyAccommodationManager;
import com.ulopms.controller.PropertyDiscountManager;
import com.ulopms.interceptor.UserAware;
import com.ulopms.model.BookedDetail;
import com.ulopms.model.BookingDetail;
import com.ulopms.model.BookingGuestDetail;
import com.ulopms.model.OtaHotelDetails;
import com.ulopms.model.PmsBookedDetails;
import com.ulopms.model.PmsBooking;
import com.ulopms.model.PmsGuest;
import com.ulopms.model.PmsProperty;
import com.ulopms.model.PmsSource;
import com.ulopms.model.PmsStatus;
import com.ulopms.model.PropertyAccommodation;
import com.ulopms.model.PropertyAccommodationInventory;
import com.ulopms.model.PropertyDiscount;
import com.ulopms.model.User;
import com.ulopms.util.Email;
import com.ulopms.view.BookingDetailAction;

import freemarker.template.Configuration;
import freemarker.template.Template;



public class BookingService extends ActionSupport implements
ServletRequestAware,SessionAware, UserAware,ModelDriven<Object> {

	private static Map<String,Booking> booking = new HashMap<String,Booking>();
    private static int nextId = 6;
    static {
    	booking.put("33", new Booking("33", "Bob", 33));
    	booking.put("43", new Booking("43", "Sarah", 44));
    	booking.put("53", new Booking("53", "Jim", 66));
    }
    
    private List<PmsBookedDetails> bookedList;
    
    private List<BookedDetail> bookList;
    
    private static final Logger logger = Logger.getLogger(BookingService.class);
   

	public Booking get(String id) {
        return booking.get(id);
    }

    public List<Booking> getAll() {
        return new ArrayList<Booking>(booking.values());
    }
    
    private SessionMap<String,Object> sessionMap;
    
    private Long hotelCode;
	private int guestId;
	private Integer bookingId;
	private Integer propertyDiscountId;
	private Object propertyDiscountName;
	private String propertyName;
	private Integer propertyId;
	private String latitude;
	private String longitude;
	private String address1;
	private String address2;
	private String phoneNumber;
	private String propertyEmail;
	private String locationName;
	private String reservationManagerEmail;
	private String reservationManagerContact;
	private String routeMap;
	private String timeHours;
	private String sourceName;
	private Object arrivalDate;
	private Object departureDate;
	private String firstName;
	private String lastName;
	private String phone;
	private String emailId;
	private String specialRequest;
	private int days;
	private long roomCount;
	private Integer rooms;
	private long adults;
	private long child;
	private double tax;
	private double amount;
	private double totalAmount;
	private double totalAdvanceAmount;
	private String paymentStatus;
	private int hotelPay;
	  
	//getters and setters  
	  
	public void setSession(Map<String, Object> map) {  
	    sessionMap=(SessionMap)map;  
	}  
    
    public Integer add(Booking booking){
    	
    	
    	try{
    		SimpleDateFormat format1=new SimpleDateFormat("yyyy-MM-dd");
    		DateFormat format2=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    		OtaBookingManager otaBookingController=new OtaBookingManager();
        	OtaBooking otaBooking=new OtaBooking();
        	otaBooking.setOtaBookingId(booking.getBookingId());
        	otaBooking.setOtaCustomerName(booking.getCustomerName());
        	otaBooking.setOtaRooms(booking.getRooms());
        	otaBooking.setOtaNights(booking.getNights());
        	otaBooking.setOtaRoomType(booking.getRoomTypeName());
        	otaBooking.setOtaHotelCode("1000100943");
        	java.util.Date dateCheckIn = format1.parse(booking.getCheckInDate());
		    java.util.Date dateBookingTime = format2.parse(booking.getBookingTime());
		   
		    
		    Calendar calCheckStart=Calendar.getInstance();
		    calCheckStart.setTime(dateCheckIn);
		    java.util.Date checkInDate = calCheckStart.getTime();
		   
		   
		    Calendar calCheckEnd=Calendar.getInstance();
		    calCheckEnd.setTime(dateBookingTime);
		    java.util.Date checkOutDate = calCheckEnd.getTime();
		    Timestamp tsBookingTime=new Timestamp(checkOutDate.getTime());
		    
        	
        	otaBooking.setOtaArrivalDate(checkInDate);
        	otaBooking.setOtaBookingDate(tsBookingTime);
        	otaBooking.setOtaName(booking.getBookingVendorName());
        	otaBooking.setOtaFlag(booking.getPayAtHotelFlag());
        	otaBooking.setOtaStatus(booking.getStatus());
        	otaBookingController.add(otaBooking);
        	getOtaBookedDetails(booking.getBookingId(),booking.getHotelCode());
    	}catch(Exception e){
    		e.printStackTrace();
    	}finally{
    		
    	}
    	return 200;
    }
    
    public String getOtaBookedDetails(String bookingId,String OtaHotelId ){ 
		try{
			HttpServletResponse response = ServletActionContext.getResponse();
			
			response.setContentType("application/json");
			HttpClient httpClient = new DefaultHttpClient();
			HttpPost postRequest = new HttpPost("https://ppin.goibibo.com/api/chmv2/getbookingdetail/?bearer_token=31ff186ee4&channel_token=i3jiqz44rb");
			String strXMLBodyFormat="<?xml version='1.0' encoding='UTF-8' ?>"+
					" <Website Name='ingoibibo' HotelCode='"+OtaHotelId+"' Version='2'>"+
						"<BookingId>"+bookingId+"</BookingId>"+
						"<PaymentDetailsRequired>True</PaymentDetailsRequired>"+
						"<FetchBookingAddOns>True</FetchBookingAddOns>"+
					"</Website>";
			
			
			StringEntity input = new StringEntity(strXMLBodyFormat);
			input.setContentType("text/json");
			postRequest.setEntity(input);
			HttpResponse httpResponse = httpClient.execute(postRequest);
			HttpEntity entity = httpResponse.getEntity();
            // Read the contents of an entity and return it as a String.
            String content = EntityUtils.toString(entity);
            
            JSONObject bookingDetailJsonObject = XML.toJSONObject(content);
            response.getWriter().write("{\"data\":[" + bookingDetailJsonObject + "]}");
            
            
            JSONObject jsonValues = (JSONObject) bookingDetailJsonObject.get("BookingDetail");
            JSONObject jsonValues1 = (JSONObject) jsonValues.get("Booking");
            JSONObject jsonValues2 = (JSONObject) jsonValues1.get("PriceDetails");
            JSONObject jsonValues3 = (JSONObject) jsonValues1.get("RoomStay");
            JSONObject jsonValues4 = (JSONObject) jsonValues3.get("Room");
            //JSONObject jsonBookingId = (JSONObject) jsonValues2.get("SellAmount");
			String val = jsonValues1.getString("CheckInDate");
			DateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
			
			java.util.Date arrivalDate = outputFormat.parse(jsonValues1.getString("CheckInDate"));
			java.util.Date departureDate = outputFormat.parse(jsonValues1.getString("CheckoutDate"));
			Timestamp aDate =new Timestamp(arrivalDate.getTime());
			Timestamp dDate=new Timestamp(departureDate.getTime());
			PmsPropertyManager propertyController = new PmsPropertyManager();
			PmsBookingManager bookingController = new PmsBookingManager();
			OtaHotelsManager otaHotelsController = new OtaHotelsManager();
			PropertyAccommodationInventoryManager accommodationInventoryController = new PropertyAccommodationInventoryManager();
			PmsGuestManager guestController = new PmsGuestManager();
			BookingDetailManager bookingDetailController = new BookingDetailManager();
			BookingGuestDetailManager bookingGuestDetailController = new BookingGuestDetailManager();
			PmsStatusManager statusController = new PmsStatusManager();
			PmsSourceManager sourceController = new PmsSourceManager();
			PropertyAccommodationManager accommodationController = new PropertyAccommodationManager();
			PmsBooking booking = new PmsBooking();
//			OtaHotelDetails otahotelDetail = otaHotelsController.findPropertyId(jsonValues1.getInt("HotelCode"),jsonValues1.getInt("RoomTypeCode"));
			OtaHotelDetails otahotelDetail = null;
			booking.setArrivalDate(aDate);
			booking.setDepartureDate(dDate);
			booking.setRooms(jsonValues1.getInt("NumberofRooms"));
			booking.setIsActive(true);
			booking.setIsDeleted(false);
			booking.setTotalAmount(jsonValues2.getInt("TotalPayAtHotelAmount"));
			booking.setTotalTax(jsonValues2.getInt("GST"));
			booking.setOtaCommission(100);
			booking.setOtaTax(10);
			booking.setAdvanceAmount(100);
			booking.setTotalActualAmount(jsonValues2.getDouble("SellAmount"));
			
			PmsSource source = sourceController.find(jsonValues1.getString("BookingVendorName"));
			
			booking.setPmsSource(source);
			PmsProperty property = propertyController.find(otahotelDetail.getPmsProperty().getPropertyId());
			booking.setPmsProperty(property);
			PmsStatus status = statusController.find(2);
			booking.setPmsStatus(status);
			PmsBooking book = bookingController.add(booking);
			this.bookingId = book.getBookingId();
			/*sessionMap.put("bookingId",bookingId);
			
			this.bookingId = (Integer) sessionMap.get("bookingId");*/
			
			/*add guest table*/
			
			PmsGuest guest = new PmsGuest();
			
			
			long time = System.currentTimeMillis();
			java.sql.Date date = new java.sql.Date(time);
			
			
			guest.setEmailId("sivabalan@ulohotels.com");  /*jsonValues1.getString("GuestEmail")*/
			guest.setFirstName(jsonValues1.getString("GuestName")); 
			guest.setIsActive(true);
			guest.setIsDeleted(false);
			guest.setPhone("9944668918"); /*jsonValues1.getString("GuestPhoneNo")*/
			
			
			SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
			Calendar today=Calendar.getInstance();
		    String strCurrentDate = new SimpleDateFormat("yyyy-MM-dd").format(today.getTime());
		    java.util.Date currentDate = format1.parse(strCurrentDate);
		    Timestamp currentTS = new Timestamp(currentDate.getTime());
		    guest.setCreatedDate(currentTS);
		    
		    PmsGuest guestli = guestController.add(guest);
			
			this.guestId = guestli.getGuestId();
			/*sessionMap.put("guestId",guestId);*/	
			
			/*add booking detail table*/ 
			
			
			 BookingGuestDetail bookingGuestDetail = new BookingGuestDetail();
             PmsBooking bookingg = bookingController.find(getBookingId());
             bookingGuestDetail.setPmsBooking(bookingg);
             PmsGuest guestId = guestController.find(getGuestId());
             bookingGuestDetail.setPmsGuest(guestId);
             bookingGuestDetail.setIsPrimary(true);
             bookingGuestDetail.setIsActive(true);
             bookingGuestDetail.setIsDeleted(false);
            /* if(preBookFlag){
             	bookingGuestDetail.setIsActive(false);
                 bookingGuestDetail.setIsDeleted(true);
             }else{
             	bookingGuestDetail.setIsActive(true);
                 bookingGuestDetail.setIsDeleted(false);
             }*/
             
             bookingGuestDetail.setSpecialRequest(jsonValues1.getString("SpecialRequest"));
             bookingGuestDetailController.add(bookingGuestDetail);
			
			DateTime start = DateTime.parse(jsonValues1.getString("CheckInDate"));
            DateTime end = DateTime.parse(jsonValues1.getString("CheckoutDate"));
          
            
           	

             List<DateTime> between = getDateRange(start, end);
             for (DateTime d : between)
             {
            
            	java.util.Date bookingDate=  d.toDate();              
                 BookingDetail bookingDetail = new BookingDetail();
                
                
                 
                 bookingDetail.setAdultCount(jsonValues4.getInt("Adult"));
                 bookingDetail.setChildCount(jsonValues4.getInt("Child"));
                 //bookingDetail.setRefund(array.get(i).getRefund());
                 bookingDetail.setAdvanceAmount(100); 	
                 bookingDetail.setAmount(jsonValues2.getDouble("TotalPayAtHotelAmount"));
                 bookingDetail.setTax(jsonValues2.getDouble("GST"));  	
                 bookingDetail.setOtaCommission(100);
                 bookingDetail.setOtaTax(10);
                 /*if(this.userId!=null){
                 	bookingDetail.setCreatedBy(this.userId);
                 }*/
                 bookingDetail.setCreatedDate(currentTS);
                 bookingDetail.setBookingDate(bookingDate);
                 bookingDetail.setIsActive(true);
                 bookingDetail.setIsDeleted(false);
                 PmsStatus statusId = statusController.find(2);
                 bookingDetail.setPmsStatus(statusId);
                 PropertyAccommodation propertyAccommodation = accommodationController.find(otahotelDetail.getPropertyAccommodation().getAccommodationId());

                 bookingDetail.setPropertyAccommodation(propertyAccommodation);
                 PmsBooking booking1 = bookingController.find(getBookingId());
                 bookingDetail.setPmsBooking(booking1);
                 //bookingDetail.setRandomNo(array.get(i).getRandomNo());
                 //bookingDetail.setInfantCount(array.get(i).getInfantCount());
                 bookingDetail.setRoomCount(jsonValues1.getInt("NumberofRooms"));
                 bookingDetail.setActualAmount(Math.round(jsonValues2.getDouble("SellAmount")));
                 
                
                 	
                 	
                 	PropertyAccommodationInventory inventoryCount = accommodationInventoryController.findInventoryCount(bookingDate,otahotelDetail.getPropertyAccommodation().getAccommodationId());
                 	 if(inventoryCount != null){ /*inventoryCount.getInventoryId()*/
                 	PropertyAccommodationInventory inventory = accommodationInventoryController.find(inventoryCount.getInventoryId());
                     bookingDetail.setPropertyAccommodationInventory(inventory);
                 	
                 	}
                
                 // for promotion
                /* sessionMap.put("promotionName", array.get(i).getPromotionName());
                 
                 if(array.get(i).getDiscountId() != null){
                     PropertyDiscount discount = discountController.find(array.get(i).getDiscountId());
                     bookingDetail.setPropertyDiscount(discount);
                 }else{
                    	
                 	//bookingDetail.setPmsBooking(booking);
                 	Integer frontDiscountId = null,frontSmartPriceId=null,frontPromotionId=null;
                 	frontSmartPriceId= array.get(0).getSmartPriceId();
                 	frontPromotionId= array.get(0).getPromotionId();
                 	frontDiscountId = array.get(0).getDiscountId();                   	
                 	
                 	
                 	if(frontDiscountId != null && frontDiscountId!=0){  
                 		//frontDiscountId = array.get(0).getDiscountId();	
                 		PropertyDiscount discount = discountController.find(frontDiscountId);
                         bookingDetail.setPropertyDiscount(discount);
                 	
                 	}
                 	
                 	if(frontSmartPriceId != null && frontSmartPriceId!=0){
                 		//frontDiscountId = array.get(0).getDiscountId();	
                 		PmsSmartPrice pmsSmartPrice = smartPriceController.find(frontSmartPriceId);
                         bookingDetail.setPmsSmartPrice(pmsSmartPrice);
                 	
                 	}
                 	
                 	if(frontPromotionId != null && frontPromotionId!=0){
                 		//frontDiscountId = array.get(0).getDiscountId();	
                 		PmsPromotions pmsPromotions = promotionController.find(frontPromotionId);
                         bookingDetail.setPmsPromotions(pmsPromotions);
                 	
                 	}
                 }*/

                
                 //PmsRoomDetail roomDetail = bookingDetailController.findRoom(arrival, departure,array.get(i).getAccommodationId());
                 //PropertyAccommodationRoom accommodationRoom = propertyAccommodationRoomController.find(roomDetail.getRoomId());
                 //bookingDetail.setPropertyAccommodationRoom(accommodationRoom);
                 //bookingDetail.set(roomDetail.getRoomId());
                 bookingDetailController.add(bookingDetail);
                 getBookedDetails(this.bookingId);
             }
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return null;
	}
    
    public void getBookedDetails(int bookingId) throws IOException {
		
	    
	
		 try {

			String jsonOutput = "";
			HttpServletResponse response = ServletActionContext.getResponse();
			DecimalFormat df = new DecimalFormat("###.##");
			PmsPropertyManager propertyController = new PmsPropertyManager();
			PropertyAccommodationManager propertyAccommodationController = new PropertyAccommodationManager();
			BookingDetailManager bookingDetailController = new BookingDetailManager();
			BookingGuestDetailManager bookingGuestDetailController = new BookingGuestDetailManager();
			PmsGuestManager guestController = new PmsGuestManager();
			PmsStatusManager statusController = new PmsStatusManager();
			PropertyDiscountManager propertyDiscountController = new PropertyDiscountManager();  
			PmsBookingManager bookingController = new PmsBookingManager();
			
			response.setContentType("application/json");
			this.bookedList = bookingDetailController.bookedList(getBookingId());
			
			StringBuilder accommodationType = new StringBuilder();
			
			StringBuilder accommodationRate = new StringBuilder();
			
			
			//this.totalInfantCount = (Integer) sessionMap.get("totalInfantCount");
			
			
			StringBuilder smsAccommodationTypes = new StringBuilder();
			List accommodationBooked = new ArrayList();
            for (PmsBookedDetails booked : bookedList) {
            	this.bookingId = getBookingId();
            	this.propertyDiscountName = "50";
            	PmsProperty pmsProperty = propertyController.find(booked.getPropertyId());
            	this.propertyName = pmsProperty.getPropertyName();//resort name and location name is missing
            	this.propertyId = pmsProperty.getPropertyId();
            	this.latitude = pmsProperty.getLatitude();
            	this.longitude = pmsProperty.getLongitude();
            	this.address1=pmsProperty.getAddress1();//property address1
            	this.address2=pmsProperty.getAddress2();//property address2
            	this.phoneNumber = pmsProperty.getPropertyContact();//property phone number
            	this.propertyEmail = pmsProperty.getPropertyEmail();
            	this.locationName=pmsProperty.getLocation().getLocationName();//property location
            	this.reservationManagerEmail=pmsProperty.getReservationManagerEmail();
            	this.reservationManagerContact = pmsProperty.getReservationManagerContact();
            	this.routeMap = pmsProperty.getRouteMap();
            	java.util.Date date=new java.util.Date();
            	Calendar calDate=Calendar.getInstance();
            	calDate.setTime(date);
            	calDate.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
            	int bookedHours=calDate.get(Calendar.HOUR);
            	int bookedMinute=calDate.get(Calendar.MINUTE);
            	int bookedSecond=calDate.get(Calendar.SECOND);
            	int AMPM=calDate.get(Calendar.AM_PM);
            	
            	String strBookingDate=new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH).format(calDate.getTime());
            	String hours="",minutes="",seconds="";
            	if(bookedHours<10){
            		hours=String.valueOf(bookedHours);
            		hours="0"+hours;
            	}else{
            		hours=String.valueOf(bookedHours);
            	}
            	if(bookedMinute<10){
            		minutes=String.valueOf(bookedMinute);
            		minutes="0"+minutes;
            	}else{
            		minutes=String.valueOf(bookedMinute);
            	}
            	
            	if(bookedMinute<10){
            		seconds=String.valueOf(bookedSecond);
            		seconds="0"+seconds;
            	}else{
            		seconds=String.valueOf(bookedSecond);
            	}
            	if(AMPM==0){//If the current time is AM
            		this.timeHours=strBookingDate+" "+hours+" : "+minutes+" : "+seconds+" AM ";
            	}else if(AMPM==1){//If the current time is PM
            		this.timeHours=strBookingDate+" "+hours+" : "+minutes+" : "+seconds+" PM ";
            	}
            	
            			this.sourceName = "offline";
            		
            	
            	
            	
                
            	this.arrivalDate = booked.getArrivalDate();
            	this.departureDate = booked.getDepartureDate();
            	PmsGuest guest = guestController.find(booked.getGuestId());
            	this.firstName = guest.getFirstName();
            	this.lastName = guest.getLastName();
            	this.phone = guest.getPhone();
            	this.emailId = guest.getEmailId();
            	this.specialRequest = booked.getSpecialRequest();
            	
            	String checkIn = new SimpleDateFormat("MM/dd/yyyy").format(this.arrivalDate);
				String checkOut = new SimpleDateFormat("MM/dd/yyyy").format(this.departureDate);
				
				LocalDate date1 = LocalDate.parse(checkIn, DateTimeFormat.forPattern("MM/dd/yyyy"));
                LocalDate date2 = LocalDate.parse(checkOut, DateTimeFormat.forPattern("MM/dd/yyyy"));
                this.days = Days.daysBetween(date1, date2).getDays();
            	
             this.bookList =  bookingDetailController.listAccommodation(getBookingId(),booked.getAccommodationId());
             if (!jsonOutput.equalsIgnoreCase(""))
					jsonOutput += ",{";
				
				else
					jsonOutput += "{";
             
             	String jsonTypes="";
         		jsonOutput += "\"bookingid\":\"" + getBookingId().toString() + "\"";
         	
         		for (BookedDetail bookedDetail : bookList) {
            		
	            	PropertyAccommodation accommodation = propertyAccommodationController.find(bookedDetail.getAccommodationId());
	            	this.roomCount = bookedDetail.getRoomCount()/this.days;
	            	accommodationBooked.add(new BookedDetail(accommodation.getAccommodationType(),bookedDetail.getTax(),
	            			this.roomCount,bookedDetail.getAdultCount(),bookedDetail.getChildCount() ,bookedDetail.getAmount())); 
	            	 
	            	if (!jsonTypes.equalsIgnoreCase(""))
						jsonTypes += ",{";
	            	
					else
						jsonTypes += "{";
            		
	            	
	            	smsAccommodationTypes.append(accommodation.getAccommodationType().trim());
	            	smsAccommodationTypes.append(",");
            		jsonTypes += "\"accommodationType\":\"" + accommodation.getAccommodationType().trim()+ "\"";
            		jsonTypes += ",\"rooms\":\"" + this.roomCount+ "\"";
            		jsonTypes += ",\"adultCount\":\"" + bookedDetail.getAdultCount()+ "\"";
            		jsonTypes += ",\"childCount\":\"" + bookedDetail.getChildCount()+ "\"";
            		jsonTypes += ",\"amount\":\"" + bookedDetail.getAmount()+ "\"";
            		jsonTypes += ",\"tax\":\"" + bookedDetail.getTax()+ "\"";
            		jsonTypes += ",\"total\":\"" + (bookedDetail.getTax() + bookedDetail.getAmount())  + "\"";
            		jsonTypes += "}";
            		
         	    }
         		jsonOutput += ",\"types\":[" + jsonTypes+ "]";
            	
            	
            	this.rooms = booked.getRooms();
            	this.adults = booked.getAdults();
            	//this.infant = this.totalInfantCount;
            	this.child = booked.getChild();
            	this.tax = booked.getTax();
            	this.amount = booked.getAmount();
            	this.totalAmount = this.tax + this.amount;
            	String strTotal=df.format(this.totalAmount);
            	this.totalAmount=Double.valueOf(strTotal);
            	
                this.totalAdvanceAmount = booked.getAdvanceAmount();
                /*if(this.totalAdvanceAmount == 0){
                	this.hotelPay = 0;
            	}
            	else{
            		this.hotelPay = (this.totalAmount-this.totalAdvanceAmount);
            	}*/
               
                		this.hotelPay = 0;
	            		this.paymentStatus = "Partially Paid";
            		
            		
               
                jsonOutput += ",\"propertyName\":\"" + this.propertyName+ "\"";
	            jsonOutput += ",\"locationName\":\"" + this.locationName+ "\"";
				jsonOutput += ",\"address1\":\"" + this.address1+ "\"";
				jsonOutput += ",\"address2\":\"" + this.address2+ "\"";
				jsonOutput += ",\"phoneNumber\":\"" + this.phoneNumber+ "\"";
				jsonOutput += ",\"propertyEmail\":\"" + this.propertyEmail+ "\"";
				jsonOutput += ",\"reservationManagerEmail\":\"" + this.reservationManagerEmail+ "\"";
				jsonOutput += ",\"reservationManagerContact\":\"" + this.reservationManagerContact+ "\"";
				
				
            	String CheckIn = new SimpleDateFormat("MMM d,yyyy").format(this.arrivalDate);// check in 
                String CheckOut = new SimpleDateFormat("MMM d,yyyy").format(this.departureDate);//check out
                
                jsonOutput += ",\"checkIn\":\"" + CheckIn+ "\"";
				jsonOutput += ",\"checkOut\":\"" + CheckOut+ "\"";
				jsonOutput += ",\"timeHours\":\"" + this.timeHours+ "\"";
				jsonOutput += ",\"days\":\"" + 2 + "\"";
				jsonOutput += ",\"guestname\":\"" + this.firstName+ "\"";
                 String strFirstName="",firstNameLetterUpperCase="";
				String strLastName="",lastNameLetterUpperCase="",strFullName="";
				strFirstName=this.firstName;
				strLastName=this.lastName;
				if(strFirstName!=null && strFirstName!="" && !strFirstName.equalsIgnoreCase("undefined")){
					strFirstName=strFirstName.trim();
					firstNameLetterUpperCase=strFirstName.substring(0, 1).toUpperCase()+strFirstName.substring(1);
					strFirstName=firstNameLetterUpperCase;
				}else{
					strFirstName="-";
				}
				if(strLastName!=null && strLastName!=""  && !strLastName.equalsIgnoreCase("undefined")){
					strLastName=strLastName.trim();
					lastNameLetterUpperCase=strLastName.substring(0, 1).toUpperCase() +  strLastName.substring(1);
					strLastName=lastNameLetterUpperCase;
				}else{
					strLastName="";
				}
				strFullName=strFirstName+" "+strLastName;
				
				 String promotionName = "50";
				    if(promotionName==null || promotionName==""){
				    	promotionName="Nil";
				    }
				    /*if(this.propertyDiscountId == 35){
				    	this.propertyDiscountName = "Nil";
				    }*/
				
				jsonOutput += ",\"guestname\":\"" + strFullName+ "\"";
				jsonOutput += ",\"sourcename\":\"" + this.sourceName+ "\"";
				jsonOutput += ",\"paymentstatus\":\"" + this.paymentStatus+ "\"";
				jsonOutput += ",\"specialrequest\":\"" + this.specialRequest+ "\"";
				jsonOutput += ",\"promotionName\":\"" + promotionName + "\"";
				jsonOutput += ",\"propertyDiscountName\":\"" + "50%" + "\"";
				jsonOutput += ",\"mobile\":\"" + this.phone+ "\"";
				jsonOutput += ",\"emailId\":\"" + this.emailId+ "\"";
				jsonOutput += ",\"rooms\":\"" + this.rooms.toString()+ "\"";
				jsonOutput += ",\"adults\":\"" + String.valueOf(this.adults)+ "\"";
				jsonOutput += ",\"child\":\"" + String.valueOf(this.child)+ "\"";
				jsonOutput += ",\"tax\":\"" + df.format(this.tax) + "\"";
				jsonOutput += ",\"amount\":\"" + df.format(this.amount)+ "\"";
				jsonOutput += ",\"totalamount\":\"" + df.format(this.totalAmount)+ "\"";
				//jsonOutput += ",\"totaladvanceamount\":\"" + Double.valueOf(this.totalAdvanceAmount)+ "\"";
				
				/*if(getDiscountId()!=null){
					jsonOutput += ",\"totalamount\":\"" + String.valueOf(this.txtGrandTotal)+ "\"";
				}else{
					jsonOutput += ",\"totalamount\":\"" + String.valueOf(this.totalAmount)+ "\"";	
				}*/
				
				
				
				
				jsonOutput += "}";

			}
           
                String checkIn = new SimpleDateFormat("MM/dd/yyyy").format(this.arrivalDate);
			    String checkOut = new SimpleDateFormat("MM/dd/yyyy").format(this.departureDate);
			    String promotionName = "get1";
			   
			    String strFullName="";
			    
            /*if(this.mailFlag == true)
            {*/
            	try
 				{
 				//email template
 				Configuration cfg = new Configuration();
 				cfg.setClassForTemplateLoading(BookingService.class, "../../../");
 				Template template = cfg.getTemplate(getText("reservation.template"));
 				Map<String, Object> rootMap = new HashMap<String, Object>();
 				//Map<String, String> rootMap = new HashMap<String, String>();	 				
 				rootMap.put("bookingid",getBookingId().toString());
 				rootMap.put("propertyname",this.propertyName);
 				rootMap.put("propertyid",this.propertyId);
 				rootMap.put("latitude",this.latitude.trim());
 				rootMap.put("longitude",this.longitude.trim());
                rootMap.put("sourcename",this.sourceName);
 				rootMap.put("paymentstatus",this.paymentStatus);
 				rootMap.put("propertyDiscountName",this.propertyDiscountName);
 				rootMap.put("reservationManagerEmail",this.reservationManagerEmail);
 				rootMap.put("reservationManagerContact",this.reservationManagerContact);
 				rootMap.put("propertyEmail",this.propertyEmail);
 				rootMap.put("specialrequest",this.specialRequest);
 				rootMap.put("guestname",this.firstName);
 				rootMap.put("locationName",this.locationName);
 				rootMap.put("address1",this.address1);
 				rootMap.put("address2",this.address2);
 				rootMap.put("hotelpay",0);
 				rootMap.put("phoneNumber",this.phoneNumber);
 				rootMap.put("checkIn", checkIn);
 				rootMap.put("checkOut",checkOut);
 				rootMap.put("timeHours",this.timeHours);
 				
 				String strFirstName="",firstNameLetterUpperCase="";
				String strLastName="",lastNameLetterUpperCase="";
				
				strFirstName=this.firstName;
				strLastName=this.lastName;
				if(strFirstName!=null && strFirstName!="" && !strFirstName.equalsIgnoreCase("undefined")){
					strFirstName=strFirstName.trim();
					firstNameLetterUpperCase=strFirstName.substring(0, 1).toUpperCase()+strFirstName.substring(1);
					strFirstName=firstNameLetterUpperCase;
				}else{
					strFirstName="-";
				}
				if(strLastName!=null && strLastName!=""  && !strLastName.equalsIgnoreCase("undefined")){
					strLastName=strLastName.trim();
					lastNameLetterUpperCase=strLastName.substring(0, 1).toUpperCase() +  strLastName.substring(1);
					strLastName=lastNameLetterUpperCase;
				}else{
					strLastName="";
				}
				strFullName=strFirstName+" "+strLastName;
 				rootMap.put("guestname",strFullName);
 				//rootMap.put("guestname",this.firstName + " " + this.lastName);
 				rootMap.put("mobile",this.phone);
 				rootMap.put("emailId",this.emailId);	 				
 				rootMap.put("checkin",checkIn);
 				rootMap.put("days",2);
 				rootMap.put("checkout",checkOut);
 				rootMap.put("promotionName",promotionName);	 				
 				
            	
 				rootMap.put("accommodationBooked", accommodationBooked);
 				//rootMap.put("rooms", this.roomCount);
 				rootMap.put("adults", String.valueOf(this.adults));
 				rootMap.put("child",String.valueOf(this.child));
 				rootMap.put("infant",2);
 				rootMap.put("tax",df.format(this.tax));
 				rootMap.put("amount",df.format(this.amount));
 				rootMap.put("totalamount",df.format(this.totalAmount));
 				rootMap.put("totaladvanceamount",df.format(this.totalAdvanceAmount));
 				
 				//rootMap.put("totalamount",String.valueOf(this.txtGrandTotal));
 				
 				rootMap.put("from", getText("notification.from"));
 				Writer out = new StringWriter();
 				template.process(rootMap, out);

 				String strReservationMailId=null,strPropertyMailId=null,strResortMailId=null,strContractMailId=null,strRevenueMailId=null;
 				PmsPropertyManager pmsPropertyController=new PmsPropertyManager();
 				for (PmsBookedDetails booked : bookedList) {
 					PmsProperty pmsProperty=pmsPropertyController.find(booked.getPropertyId());
 					
 					strReservationMailId=pmsProperty.getReservationManagerEmail();
 					strResortMailId=pmsProperty.getResortManagerEmail();
 					strPropertyMailId=pmsProperty.getPropertyEmail();
 					strRevenueMailId=pmsProperty.getRevenueManagerEmail();
 					strContractMailId=pmsProperty.getContractManagerEmail();
 				}
 				String strSubject="Hotel Confirmation for Booking Id "+getBookingId().toString()+" "+this.propertyName;
 				//send email
 				Email em = new Email();
 				em.set_to(this.emailId);
 				if(strReservationMailId!=null && strContractMailId!=null && strRevenueMailId!=null){
 					em.set_cc(strContractMailId);
	 				em.set_cc2(strRevenueMailId);
	 				em.set_cc3(strReservationMailId);
	 				em.set_cc4(getText("bookings.notification.email"));
	 				em.set_cc5(getText("operations.notification.email"));
	 				em.set_cc6(getText("finance.notification.email"));
 				}else{
 					em.set_cc(strContractMailId);
	 				em.set_cc2(strRevenueMailId);
	 				em.set_cc3(strReservationMailId);
	 				em.set_cc4(getText("bookings.notification.email"));
	 				em.set_cc5(getText("operations.notification.email"));
	 				em.set_cc6(getText("finance.notification.email"));
 				}
 				
 				em.set_from(getText("email.from"));
 				//em.set_host(getText("email.host"));
 				//em.set_password(getText("email.password"));
 				//em.set_port(getText("email.port"));
 				em.set_username(getText("email.username"));
 				em.set_subject(strSubject);
 				em.set_bodyContent(out);
 				em.send();		
 				}
 				catch(Exception e1)
 				{
 					e1.printStackTrace();
 				}
            	
            	
            	//getHotelierBookedDetails();
            	
            	String message = "&sms_text=" + "Confirmation from Ulo Hotels"+
    					
    					"Booking ID - "+ getBookingId().toString() +
    					 "Guest Name � "+ strFullName  + 
    					 "Property Name �"+ this.propertyName +
    					"Room Category/s �"+  smsAccommodationTypes.toString()  +
    					 "No. of Adults �"+ this.adults +
    					 "No. of Kids �"+ this.child +
    					 "Check-in Date-"+ checkIn + 
    					"Check out Date-"+ checkOut +
    				   "Contact Number � "+ this.phone;

    			//String sender = "&sender=" + "TXTLCL";
    			String numbers = "&sms_to=" + "+91"+this.phone;
    			String from = "&sms_from=" + "ULOHTL";
    			String type = "&sms_type=" + "trans"; 
    			// Send data
    			HttpURLConnection conn = (HttpURLConnection) new URL("https://secureets.knowlarity.com/ets/dom/clients/ulohotels/api/ulohotels_sms_api.py").openConnection();
    			//"http://etsrds.kapps.in/webapi/ulohotels/api/ulohotels_sms_api.py?sms_text=hi%20this%20gopinath%20from%20ulo&sms_to=%20918220211068&sms_from=ULOHTL&sms_type=promo"
    			String data = numbers + message +  from + type;
    			conn.setDoOutput(true);
    			conn.setRequestMethod("POST");
    			conn.addRequestProperty("auth_key", "Basic dWxvaG90ZWxzX1Q6VUxPSFRMQDIwMTg=");
    			conn.setRequestProperty("Content-Length", Integer.toString(data.length()));
    			conn.getOutputStream().write(data.getBytes("UTF-8"));
    			final BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
    			final StringBuffer stringBuffer = new StringBuffer();
    			/*String line;
    			
    			while ((line = rd.readLine()) != null) {
    				stringBuffer.append(line);
    				
    			}*/
    			rd.close();
    			
    			//getRouteMapSms(this.routeMap,this.phone);
 		     
            //}
            
			response.getWriter().write("{\"data\":[" + jsonOutput + "]}");
			

		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {

		}

//		return null;
	}
    
   

	public static List<DateTime> getDateRange(DateTime start, DateTime end) {

        List<DateTime> ret = new ArrayList<DateTime>();
        DateTime tmp = start;
        while(tmp.isBefore(end) || tmp.equals(end)) {
            ret.add(tmp);
            tmp = tmp.plusDays(1);
        }
        return ret;
    }

	public Long getHotelCode() {
		return hotelCode;
	}

	public void setHotelCode(Long hotelCode) {
		this.hotelCode = hotelCode;
	}

	public int getGuestId() {
		return guestId;
	}

	public void setGuestId(int guestId) {
		this.guestId = guestId;
	}

	public Integer getBookingId() {
		return bookingId;
	}

	public void setBookingId(Integer bookingId) {
		this.bookingId = bookingId;
	}
	
	 public List<PmsBookedDetails> getBookedList() {
			return bookedList;
		}

		public void setBookedList(List<PmsBookedDetails> bookedList) {
			this.bookedList = bookedList;
		}

		@Override
		public Object getModel() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public void setUser(User user) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void setServletRequest(HttpServletRequest arg0) {
			// TODO Auto-generated method stub
			
		}
    
}
