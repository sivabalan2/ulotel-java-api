<html>
<body>
Hi ${to},

<p>You recently requested to reset your password. Please click the button below to start the password reset process.</p>
<p><a href=${passworkLink}>Reset Password</a></p>
<p>If you did not request a password change, you may ignore this message and your password will remain unchanged.</p>

Regards,<br/>
${from}.
</body>
</html>