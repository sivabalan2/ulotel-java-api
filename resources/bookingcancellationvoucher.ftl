<!doctype html>
<html>
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible">
      <title>ULO HOTELS BOOKING CANCELLATION VOUCHER</title>
      <style>
         @media screen and (max-width:600px) {
         .invoice-box{
         display:block;
         margin:0,auto;
         padding:10px;
         }
         }
         @media only screen and (max-width:480px) {
         @-ms-viewport { width:320px; }
         @viewport { width:320px; }
         }
         /** RTL **/
         .rtl {
         direction: rtl;
         font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
         }
         .rtl table {
         text-align: right;
         }
         .rtl table tr td:nth-child(2) {
         text-align: left;
         }
      </style>
   </head>
   <body>
      <div class="invoice-box" style="max-width: 800px; margin: auto; padding: 30px; border: 1px solid #E5E5E5; box-shadow: 0 0 10px rgba(0, 0, 0, .15); font-size: 16px; line-height: 24px; font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; color: #555;">
         <table style="width: 100%; line-height: inherit;text-align: left;" cellpadding="0" cellspacing="0">
            <tr>
               <td class="title">
                  <a href="https://www.ulohotels.com" target="_blank">
                  <img alt="Logo" src="https://www.ulohotels.com/ulowebsite/images/ulo_logo2.png" style="display:block;font-family:Helvetica,Arial,sans-serif;color:#fff;font-size:16px;float:left;" border="0">
                  </a>
                  </br>
               </td>
               <td>
                  <h3 style="text-align:right;padding:5px;"><span style="color:#ff0228">Cancellation</span><span style="color:#145E8F"> Voucher</span><br>
                     <span  style="color:#000;font-size:12px;">Voucher No&nbsp;:&nbsp;${bookingId}<br>
                     Booking Id&nbsp;:&nbsp;${bookingId}<br>
                    </span>
                  </h3>
               </td>
            </tr>
         </table>
         <table>
            <span >
               <hr style="height:2px; border:none; color:#000; background-color:#115E92;margin-top:10px;">
            </span>
            <h4 style="color:#000;font-style:italic;line-height: 0px;font-size:14px;">Dear ${guestName},</h4>
            <span style="text-align:justify;font-style:bold;font-size:13px;">This is to inform that the booking for <span style="color:#000;font-weight:600;" >${guestName},</span>under Booking ID <span style="color:#000;font-weight:600;" >${bookingId}</span> has been cancelled. Kindly acknowledge the same from your end.In case of any queries, Please revert back to <a href="mailto:support@ulohotels.com" target="_top">support@ulohotels.com</a> or call our toll free number, if there are any concerns.</span>
            <h4 style="color:#125E90;font-style:italic;line-height: 0px;font-size:16px;">Booking Details</h4>
            <span >
               <hr style="height:2px; border:none; color:#000; background-color:#115E92;margin-top:10px;">
            </span>
         </table>
         <table  class="responsive-table"  style="border:1px solid #d6d4d4;padding:5px;" cellpadding="0" cellspacing="0"  >
            <tr >
               <td style="font-size:14px;font-family:Helvetica,Arial,sans-serif;color:#727272;padding-bottom:0px;font-weight:bold;">Booking Id</td>
               <td  style="text-align:left;"><b>:</b>&nbsp;${bookingId}</td>
            </tr>
            <tr>
               <td style="font-size:14px;font-family:Helvetica,Arial,sans-serif;color:#727272;padding-bottom:0px;font-weight:bold;">Name Of Guest</td>
               <td  style="text-align:left;"><b>:</b>&nbsp;${guestName}</td>
            </tr>
            <tr >
               <td style="font-size:14px;font-family:Helvetica,Arial,sans-serif;color:#727272;padding-bottom:0px;font-weight:bold;">Email</td>
               <td  style="text-align:left;"><b>:</b>&nbsp;${emailId}</td>
            </tr>
            <tr>
               <td style="font-size:14px;font-family:Helvetica,Arial,sans-serif;color:#727272;padding-bottom:0px;font-weight:bold;">Mobile</td>
               <td  style="text-align:left;"><b>:</b>&nbsp;${mobile}</td>
            </tr>
            <tr>
               <td colspan="2" style="font-size:15px;font-family:Helvetica,Arial,sans-serif;color:#000;padding-bottom:0px;font-weight:600;text-decoration:underline;">Room Details</td>
            </tr>
			   <tr >
               <td style="font-size:14px;font-family:Helvetica,Arial,sans-serif;color:#727272;padding-bottom:0px;font-weight:bold;">Hotel Name</td>
               <td  style="text-align:left;"><b>:</b>&nbsp;${propertyName}</td>
            </tr>
            <tr >
               <td style="font-size:14px;font-family:Helvetica,Arial,sans-serif;color:#727272;padding-bottom:0px;font-weight:bold;">Source</td>
               <td  style="text-align:left;"><b>:</b>&nbsp;${sourceName}</td>
            </tr>
            <tr>
               <td style="font-size:14px;font-family:Helvetica,Arial,sans-serif;color:#727272;padding-bottom:0px;font-weight:bold;">Check-In</td>
               <td  style="text-align:left;"><b>:</b>&nbsp;${checkIn}</td>
            </tr>
            <tr>
               <td style="font-size:14px;font-family:Helvetica,Arial,sans-serif;color:#727272;padding-bottom:0px;font-weight:bold;">Check-Out</td>
               <td  style="text-align:left;"><b>:</b>&nbsp;${checkOut}</td>
            </tr>
            <tr>
               <td style="font-size:14px;font-family:Helvetica,Arial,sans-serif;color:#727272;padding-bottom:0px;font-weight:bold;">No-Of-Nights</td>
               <td  style="text-align:left;"><b>:</b>&nbsp;${diffDays} Nights</td>
            </tr>
            <tr>
               <td style="font-size:14px;font-family:Helvetica,Arial,sans-serif;color:#727272;padding-bottom:0px;font-weight:bold;white-space: nowrap; overflow: hidden;">Room Category</td>
               <td  style="text-align:left;"><b>:</b>&nbsp;${accommodationType}</td>
            </tr>
            <tr>
               <td style="font-size:14px;font-family:Helvetica,Arial,sans-serif;color:#727272;padding-bottom:0px;font-weight:bold;">No-Of-Rooms</td>
               <td  style="text-align:left;"><b>:</b>&nbsp;${rooms}</td>
            </tr>
            <tr>
               <td style="font-size:14px;font-family:Helvetica,Arial,sans-serif;color:#727272;padding-bottom:0px;font-weight:bold;">Adult</td>
               <td  style="text-align:left;"><b>:</b>&nbsp;${adults}</td>
            </tr>
            <tr>
               <td style="font-size:14px;font-family:Helvetica,Arial,sans-serif;color:#727272;padding-bottom:0px;font-weight:bold;">Child</td>
               <td  style="text-align:left;"><b>:</b>&nbsp;${child}</td>
            </tr>
            <tr>
               <td style="font-size:14px;font-family:Helvetica,Arial,sans-serif;color:#727272;padding-bottom:0px;font-weight:bold;white-space: nowrap; overflow: hidden;">Payment Status</td>
               <td  style="text-align:left;"><b>:</b>&nbsp;${paymentStatus}</td>
            </tr>
            <tr>
               <td style="font-size:14px;font-family:Helvetica,Arial,sans-serif;color:#727272;padding-bottom:0px;font-weight:bold;">Total Paid</td>
               <td  style="text-align:left;"><b>:</b>&nbsp;${totalBaseTaxAmount}&nbsp;&nbsp;&nbsp;${tariffStatus}</td>
            </tr>
            <tr>
               <td style="font-size:14px;font-family:Helvetica,Arial,sans-serif;color:#727272;padding-bottom:0px;font-weight:bold;">Reduced Amount</td>
               <td  style="text-align:left;"><b>:</b>&nbsp;${reducedAmount}</td>
            </tr>
			<tr  style="border-top:1px solid #d6d4d4;">
               <td style="font-size:14px;font-family:Helvetica,Arial,sans-serif;color:#e50000;padding-bottom:0px;font-weight:bold;">Amount To Be Refunded</td>
               <td  style="text-align:left;color:#e50000;"><b>:</b>&nbsp;${refundAmount}</td>
            </tr>
         </table>
         <h4 style="color:#125E90;font-style:italic;line-height: 0px;font-size:15px;padding:5px;">Cancellation Policy:</h4>
         <span >
            <hr style="height:2px; border:none; color:#000; background-color:#115E92;margin-top:10px;">
         </span>
		 <p style="font-size:12px;color:#e57520;font-weight:bold;">* If the booking is cancelled 3 days prior to the check-in date, Rs.500 will be charged from the total amount and the remaining amount will be refunded within 10 to 12 working days.</p>
		 <p style="font-size:12px;color:#e57520;font-weight:bold;">* If the booking is cancelled 2 days prior to the check-in date, 25% from the total amount will be charged as a cancellation fee.</p>
		 <p style="font-size:12px;color:#e57520;font-weight:bold;">* If the booking is cancelled one day prior to the check-in date, no refund is applicable. No refund is applicable for cancellations done on long Weekends, Festival and On-Season times.</p>
         <p style="font-size:12px;color:#e57520;font-weight:bold;">* To know more read the hotel policies to click <a href="https://www.ulohotels.com/guest-policy" target="_blank" >Terms and Conditions</a> and <a href="https://www.ulohotels.com/details?propertyId=18" target="_blank" >Hotel Policy</a></p>
		      <span >
            <hr style="height:1px; border:none; color:#000; background-color:#727272;margin-top:10px;">
         </span>
		 <h4 style="color:#125E90;font-style:italic;line-height: 0px;font-size:15px;padding:5px;">Corporate Office:</h4>
          <span style="font-size:12px;"><img src="https://www.ulohotels.com/ulowebsite/images/invoice/footmap.png" align="top"><span style="color:#125E90;font-weight:bold;">Ulo Hotels Private Limited </span><br>1/4, Plot No: 251, Second Floor, <br> Rajamannar Salai, Vijayaraghavapuram ,<br> Saligramam, Chennai, <br>Tamil Nadu 600093 <br><img src="https://www.ulohotels.com/ulowebsite/images/invoice/footcall.png" align="top"> 1800-3000-8686</span>
         <p style="font-size:12px;text-align:center;font-weight:bold;color:#727272;">* Incase of any queries, Please revert back to <a href="mailto:support@ulohotels.com" target="_top">support@ulohotels.com</a></p>
         <p style="font-size:12px;background-color:#E5E5E5;padding:5px;"><img src="https://www.ulohotels.com/ulowebsite/images/invoice/footmap.png" align="top">1/4, Plot No: 251, Second Floor, Rajamannar Salai, Vijayaraghavapuram, Saligramam, Chennai, Tamil Nadu 600093 <br><span><img src="https://www.ulohotels.com/ulowebsite/images/invoice/footmail.png" align="top"> support@ulohotels.com</span><img src="https://www.ulohotels.com/ulowebsite/images/invoice/footcall.png" align="top"> 1800-3000-8686<img src="https://www.ulohotels.com/ulowebsite/images/invoice/footweb.png" align="top" >www.ulohotels.com</p>
      </div>
   </body>
</html>