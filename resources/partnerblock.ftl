<!doctype html>
<html>
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible">
      <title>PARTNER INVENTORY BLOCK VOUCHER</title>
      <style>
         @media screen and (max-width:600px) {
         .invoice-box{
         display:block;
         margin:0,auto;
         padding:10px;
         }
         }
         @media only screen and (max-width:480px) {
         @-ms-viewport { width:320px; }
         @viewport { width:320px; }
         }
         /** RTL **/
         .rtl {
         direction: rtl;
         font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
         }
         .rtl table {
         text-align: right;
         }
         .rtl table tr td:nth-child(2) {
         text-align: left;
         }
      </style>
   </head>
   <body>
      <div class="invoice-box" style="max-width: 800px; margin: auto; padding: 30px; border: 1px solid #E5E5E5; box-shadow: 0 0 10px rgba(0, 0, 0, .15); font-size: 16px; line-height: 24px; font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; color: #555;">
         <table style="width: 100%; line-height: inherit;text-align: left;" cellpadding="0" cellspacing="0">
            <tr>
               <td class="title">
                  <a href="https://www.ulohotels.com" target="_blank">
                  <img alt="Logo" src="https://ulotel.ulohotels.com/ulowebsite/images/ulo_logo2.png" style="display:block;font-family:Helvetica,Arial,sans-serif;color:#fff;font-size:16px;float:left;" border="0">
                  </a>
                  </br>
               </td>
               <td>
                  <h3 style="text-align:right;padding:5px;"><span style="color:#86B940"></span><span style="color:#145E8F"> Room Inventory Notification</span><br>
               </td>
            </tr>
         </table>
		 <span >
               <hr style="height:5px; border:none; color:#000; background-color:#0f467c;">
            </span>
		 <div style="border:10px solid #75a849;border-top:60px solid #75a849;padding:5px;">
            <table>
            
                <h4 style="color:#000;font-style:italic;line-height: 0px;font-size:14px;">Dear Valued Partner,</h4>
                <span style="text-align:justify;font-size:13px;">Thank you for partnering with ULO hotels. Your room inventory change has been confirmed. We hope that the aspect which made you block the inventory will be rectified soon so that mutual business may not be affected.  Kindly overview the below provided details and please feel free to contact if you have any queries.
</span> 
            
                <h4 style="color:#125E90;font-style:italic;line-height: 0px;font-size:16px;">Hotel Details</h4>
                <span >
                   <hr style="height:2px; border:none; color:#000; background-color:#115E92;margin-top:10px;">
                </span>
             </table>
         <div class="invoice" >
            <table  style=" border-collapse: collapse;border:1px solid #d6d4d4;" cellpadding="0" cellspacing="0" class="responsive-table">
            <tr  style="border-bottom:1px solid #d6d4d4;">
               <td style="padding:5px;font-size:13px;font-family:Helvetica,Arial,sans-serif;color:#434343;padding-bottom:0px;font-weight:bold;">Hotel name</td>
               <td  style="padding:5px;text-align:left;font-size:13px;"><b>:</b>&nbsp;${propertyName}</td>
                
            </tr>
			<tr  style="border-bottom:1px solid #d6d4d4;">
               <td style="padding:5px;font-size:13px;font-family:Helvetica,Arial,sans-serif;color:#434343;padding-bottom:0px;font-weight:bold;">Blocking date & time</td>
               <td  style="padding:5px;text-align:left;font-size:13px;"><b>:</b>&nbsp;${createdDate}</td>
            </tr>
			 <tr style="border-bottom:1px solid #d6d4d4;">
               <td style="padding:5px;font-size:13px;font-family:Helvetica,Arial,sans-serif;color:#434343;padding-bottom:0px;font-weight:bold;">Blocked by</td>
               <td  style="padding:5px;text-align:left;font-size:13px;"><b>:</b>&nbsp;${createdBy}</td>
            </tr>
            <tr >
                <td style="padding:5px;font-size:13px;font-family:Helvetica,Arial,sans-serif;color:#434343;padding-bottom:0px;font-weight:bold;">Reason for blocking</td>
                <td  style="padding:5px;text-align:left;font-size:13px;"><b>:</b>&nbsp;${blockReason}</td>
             </tr>
		
            </table>

         </div>
         <h4 style="color:#125E90;font-style:italic;line-height: 0px;font-size:16px;">Room Inventory Details</h4>
         <span >
            <hr style="height:2px; border:none; color:#000; background-color:#115E92;margin-top:10px;">
         </span>
            <div class="invoice" style="padding:10px;">
                <table width="100%"  style=" border-collapse: collapse;border:1px solid #d6d4d4;" cellpadding="0" cellspacing="0" class="responsive-table">
                   <tr style="border-bottom:1px solid #d6d4d4;padding:5px;" >
                      <td style="text-align:center;font-size:13px;font-weight:600;padding:2px;">Block Date</td>
                      <td style="text-align:center;font-size:13px;font-weight:600;padding:2px;">Categories</td>
                      <td style="text-align:center;font-size:13px;font-weight:600;padding:2px;">Total Room</td>
                      <td style="text-align:center;font-size:13px;font-weight:600;padding:2px;">Blocked Room</td>
                      <td style="text-align:center;font-size:13px;font-weight:600;padding:2px;">Available Room</td>
                   </tr>
               <#list json as data>  
               <tr>
                  <td style="text-align:center;">${data.blockDate}</td>
                  <td style="text-align:center;">${data.accommodationType}</td>
                  <td style="text-align:center;">${data.available}</td>
                  <td style="text-align:center;">${data.blockedRoom}</td>
                  <td style="text-align:center;">${data.inventory}</td>
                  
               </tr >
                </#list>
             
                  
             
                </table>
          
             </div>
         <span >
            <hr style="height:2px; border:none; color:#000; background-color:#d6d4d4;margin-top:5px;">
         </span>
    </div>
		 <p style="font-size:12px;background-color:#E5E5E5;padding:5px;"><img src="https://ulotel.ulohotels.com/ulowebsite/images/invoice/footmap.png" align="top">1/4, Plot No: 251, Second Floor, Rajamannar Salai, Vijayaraghavapuram, Saligramam, Chennai, Tamil Nadu 600093 <br> <span><img src="https://ulotel.ulohotels.com/ulowebsite/images/invoice/footmail.png" align="top"> support@ulohotels.com</span><img src="https://ulotel.ulohotels.com/ulowebsite/images/invoice/footcall.png" align="top">9790 999 222<img src="https://ulotel.ulohotels.com/ulowebsite/images/invoice/footweb.png" align="top" >www.ulohotels.com</p>
      </div>
   </body>
</html>
