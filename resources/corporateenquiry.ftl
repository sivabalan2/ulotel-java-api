<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

    
    <style>
@media screen and (min-width: 601px) {
	.container {
		width: 600px!important;
	}
}
	@media (max-width: 991px) {
    .mobile-only {
        display:block !important;
		font-size:10px;
    }
 
    .desktop-only {
        display:none !important;
    }
}
    </style>

</head>
<body>

  <div class="invoice-box"  style="font-size:14px;max-width:800px;margin:auto;padding:30px;border:1px solid #000;max-width:800px;margin:auto;padding:30px;border:1px solid #eee;box-shadow:0 0 10px rgba(0, 0, 0, .15);font-size:16px;line-height:24px;font-family:'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;color:#555;">
        <table cellpadding="0" cellspacing="0" style="width:100%;line-height:inherit;text-align:left;" >
            <tr class="top">
                <td colspan="2">
                    <table  style="width:100%;line-height:inherit;text-align:left; background-color:#88ba41">
                        <tr>
                            <td class="title">
                                <img src="https://ulohotels.com/ulowebsite/images/logo.png" >
                            </td>
                        
                          
                        </tr>
					
                    </table>			
                </td>
            </tr>
        </table>
	         
			        <table  style="width:100%;line-height:inherit;background-color:#88ba41">
                        <tr>
                            <td class="title" style="text-align:center;border:1px solid #fff;">
                            <h2 style="color:#fff;text-transform:uppercase;word-wrap: break-word;vertical-align: middle;">WELCOME TO ULO CORPORATE ENQUIRY</h2>
                            </td>

                        </tr>
					
                    </table>
	
							<table  style="width:100%;line-height:inherit;background-color:#88ba41">
                        <tr>
                            <td class="title" style="text-align:center;border:1px solid #fff;font-size:14px;">
                              <h2 style="color:#fff;text-transform:uppercase;word-wrap: break-word;vertical-align: middle;font-size:14px;">Name</h2>
                            </td>
							 <td class="title" style="text-align:center;border:1px solid #fff;font-size:14px;">
                              <h2 style="color:#fff;word-wrap: break-word;vertical-align: middle;font-size:14px;">${employeeName}</h2>
                            </td>

                        </tr>
						    <tr>
                            <td class="title" style="text-align:center;border:1px solid #fff;font-size:14px;">
                              <h2 style="color:#fff;text-transform:uppercase;word-wrap: break-word;vertical-align: middle;font-size:14px;">Mobile</h2>
                            </td>
							 <td class="title" style="text-align:center;border:1px solid #fff;font-size:14px;">
                              <h2 style="color:#fff;word-wrap: break-word;vertical-align: middle;font-size:14px;">${corporateNumber}</h2>
                            </td>

                        </tr>
							    <tr>
                            <td class="title" style="text-align:center;border:1px solid #fff;font-size:14px;">
                              <h2 style="color:#fff;text-transform:uppercase;word-wrap: break-word;vertical-align: middle;font-size:14px;">Email:</h2>
                            </td>
							 <td class="title" style="text-align:center;border:1px solid #fff;font-size:14px;">
                              <h2 style="color:#fff;word-wrap: break-word;vertical-align: middle;font-size:14px;">${corporateEmail}</h2>
                            </td>

                        </tr>
							    <tr>
                            <td class="title" style="text-align:center;border:1px solid #fff;">
                              <h2 style="color:#fff;word-wrap: break-word;vertical-align: middle;font-size:14px;">COMPANY NAME:</h2>
                            </td>
							 <td class="title" style="text-align:center;border:1px solid #fff;font-size:14px;">
                              <h2 style="color:#fff;word-wrap: break-word;vertical-align: middle;font-size:14px;">${corporateName}</h2>
                            </td>

                        </tr>
				
					
                    </table>
															 <table  style="width:100%;line-height:inherit;text-align:left;  ">
						   <tr>
					<td style="text-align: center;">
							  </td>
							  </tr>
                  
							
					
						</table>
			 <table  style="width:100%;line-height:inherit;text-align:left; border:1px solid #124c71; padding:10px; "  >
					
                
						<tr>
						  <td align="right" style="font-size:14px;color:#000000;">
                                   <p style="float:right;text-align: center;">Looking Forward To A Fruitful Association.
						   <br>Thanks And Regards, <br> Ulo Hotels <br>
						   
						   </p>
                            </td>
						</tr>
						</table>
			<br>
									 <table  style="width:100%;line-height:inherit;text-align:left; background-color:#88ba41;padding:5px; ">
						 
                          <tr>
                            <td align="middle" style="font-size:14px;color:#000000;">
                                <b><span style="color:#fff" >Mobile:</span> 1800-3000-8686<span style="color:#fff"> Email:</span> <span style="color:#155d90">Support@ulohotels.com </span><span style="color:#fff"> Website:</span> <span style="color:#155d90">www.ulohotels.com </span></b>
                            </td>
                              </tr>
					
						</table>
															 <table  style="width:100%;line-height:inherit;text-align:left; background-color:#FFFFE0;padding:5px; ">
						   <tr>
					<td style="text-align: center;">
							  <img src="https://ulohotels.com/ulowebsite/images/logo.png" align="center" style="margin: 0 auto;dispaly:block;" >
							  </td>
							  </tr>
                          <tr>
						    
                              <td align="middle" style="font-size:14px;color:#000000;">
							  
                              CORPORATE OFFICE: 1/4, Plot No: 251, Second Floor, Rajamannar Salai, Vijayaraghavapuram, Saligramam, Chennai, Tamil Nadu 600093
                            </td>
                              </tr>
							
					
						</table>
			
						 
    </div>



</body>
</html>