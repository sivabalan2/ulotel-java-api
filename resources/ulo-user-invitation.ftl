<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

    
    <style>
@media screen and (min-width: 601px) {
	.container {
		width: 600px!important;
	}
}
	@media (max-width: 991px) {
    .mobile-only {
        display:block !important;
		font-size:10px;
    }
 
    .desktop-only {
        display:none !important;
    }
}
    </style>

</head>
<body>

  <div class="invoice-box"  style="font-size:14px;max-width:800px;margin:auto;padding:30px;border:1px solid #000;max-width:800px;margin:auto;padding:30px;border:1px solid #eee;box-shadow:0 0 10px rgba(0, 0, 0, .15);font-size:16px;line-height:24px;font-family:'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;color:#555;">
        <table cellpadding="0" cellspacing="0" style="width:100%;line-height:inherit;text-align:left;" >
            <tr class="top">
                <td colspan="2">
                    <table  style="width:100%;line-height:inherit;text-align:left; background-color:#88ba41">
                        <tr>
                            <td class="title">
                                <img src="https://ulohotels.com/ulowebsite/images/email/logoemail.png" style="width:100px;">
                            </td>
                            <td style="text-align:middle;">
                               <h2 style="color:#fff;text-transform:uppercase;word-wrap: break-word;vertical-align: middle;">WELCOME</h2>
                            </td>
                          
                        </tr>
					
                    </table>
					
                </td>
            </tr>
            
       <table  style="width:100%;line-height:inherit;text-align:left;">
						
                        <tr> 
						<tr><h2 align="middle" style="color:#092f53;text-transform:uppercase;word-wrap: break-word;">Thank You for joining the ulo family!</h2></tr>
						<p align="middle" style="color:#000;font-size:14px;">Thank you for becoming a part of Ulo Hotels. We are the Next Generation Quality budget hotel chain that you don't have to burn your savings for. Ulo was born because we felt that quality shouldn't be a compromise no matter the budget. We wish to show you the places we call home, through our famed hospitality and service.</p>
						</tr>
						</table>

        </table>
	         
		         <table class="container" width="100%" cellpadding="0" cellspacing="0" style="max-width: 800px;">
			<tr >
		
			
			<td style=" vertical-align: top; font-size: 0;" class="mobile-only">
			<div style="width: 800px; display: inline-block; vertical-align: top;padding:5px;">
				   <ul style="list-style-type:none;background-color:#88ba41;border:2px solid #124c71; text-align:center; " width="100%" >
						   <li><p style="font-size:14px;color:#092f53;padding:5px;"><b>The Ulo promoises Guarantees The Following</b><p></li>
                           <li ><img src="https://ulohotels.com/ulowebsite/images/email/bestguest.png" height="60px" width="60px" alt="Ulo hotels confortable stay" style="vertical-align: middle;"><span style="display: inline-block;vertical-align: middle;color:#ffffff;font-size:12px;">BEST IN GUEST SERVICE & CARE</span></li>
                           <li><img src="https://ulohotels.com/ulowebsite/images/email/breakfast.png" height="60px" width="60px"  alt="Ulo hotels confortable stay" style="vertical-align: middle;"><span style="display: inline-block;vertical-align: middle;color:#ffffff;font-size:12px;">BREAKFAST HEALTHY & REGIONAL</span></li>
						   <li><img src="https://ulohotels.com/ulowebsite/images/email/bedclean.png" height="60px" width="60px" alt="Ulo hotels confortable stay" style="vertical-align: middle;"><span style="display: inline-block;vertical-align: middle;color:#ffffff;font-size:12px;">BED CLEAN & COMFORTABLE</span></li>
                           <li><img src="https://ulohotels.com/ulowebsite/images/email/budget.png" height="60px" width="60px" alt="Ulo hotels confortable stay" style="vertical-align: middle;"><span style="display: inline-block;vertical-align: middle;color:#ffffff;font-size:12px;">BUDGET FRIENDLY & VALUE</span></li>
						   <li><img src="https://ulohotels.com/ulowebsite/images/email/bathroom.png" height="60px" width="60px"  alt="Ulo hotels confortable stay" style="vertical-align: middle;"><span style="display: inline-block;vertical-align: middle;color:#ffffff;font-size:12px;">BATHROOM HYGIENIC & FUNCTIONAL</span></li>
                         
                        </ul>
			
			</div>
		
						</td>
		
						
		
	
			</tr>
                   
             </table>
		
			 <table  style="width:100%;line-height:inherit;text-align:left; border:1px solid #124c71; padding:10px; "  >
					
                        <tr>    <td>  <p style="text-align: center;word-wrap: break-word; color:#000;font-size:14px;">I can't wait to hear about your experience with Ulo. I love getting feedback about our service. 
               Whether you have suggestions, queries, or just want to say hi, you can reach us on Facebook 
          or send an email to <span style="color:#124c71">feedback@ulohotels.com</span></p><td>
		     
						</tr>
						<tr>
						  <td align="right" style="font-size:14px;color:#000000;">
                                   <p style="float:right;text-align: center;">Looking Forward To A Fruitful Association.
						   <br>Thanks And Regards,<br>Viswanathan, <br> CEO, <br> Ulo Hotels <br>
						   
						   </p>
                            </td>
						</tr>
						</table>
			
									 <table  style="width:100%;line-height:inherit;text-align:left; background-color:#88ba41;padding:5px; ">
						 
                          <tr>
                            <td align="middle" style="font-size:14px;color:#000000;">
                                <b><span style="color:#fff" >Mobile:</span> 1800-3000-8686<span style="color:#fff"> Email:</span> <span style="color:#155d90">Support@ulohotels.com </span><span style="color:#fff"> Website:</span> <span style="color:#155d90">www.ulohotels.com </span></b>
                            </td>
                              </tr>
					
						</table>
															 <table  style="width:100%;line-height:inherit;text-align:left; background-color:#FFFFE0;padding:5px; ">
						   <tr>
					<td style="text-align: center;">
							  <img src="https://ulohotels.com/ulowebsite/images/logo.png" align="center" style="margin: 0 auto;dispaly:block;" >
							  </td>
							  </tr>
                          <tr>
						    
                              <td align="middle" style="font-size:14px;color:#000000;">
							  
                              CORPORATE OFFICE: 1/4, Plot No: 251, Second Floor, Rajamannar Salai, Vijayaraghavapuram, Saligramam, Chennai, Tamil Nadu 600093
                            </td>
                              </tr>
							
					
						</table>
			
						 
    </div>



</body>
</html>