<!doctype html>
<html>
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible">
      <title>ULO HOTELS</title>
      <style>
         @media screen and (max-width:600px) {
         .invoice-box{
         display:block;
         margin:0,auto;
         padding:10px;
         }
         }
         @media only screen and (max-width:480px) {
         @-ms-viewport { width:320px; }
         @viewport { width:320px; }
         }
         /** RTL **/
         <!-- .rtl { -->
         direction: rtl;
         font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
         }
         .rtl table {
         text-align: right;
         }
         .rtl table tr td:nth-child(2) {
         text-align: left;
         }
      </style>
   </head>
   <body>
      <div class="invoice-box" style="max-width: 800px; margin: auto; padding: 30px; border: 1px solid #E5E5E5; box-shadow: 0 0 10px rgba(0, 0, 0, .15); font-size: 16px; line-height: 24px; font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; color: #555;">
         <table style="width: 100%; line-height: inherit;text-align: left;" cellpadding="0" cellspacing="0">
            <tr>
               <td class="title">
                  <a href="https://www.ulohotels.com" target="_blank">
                  <img alt="Logo" src="https://www.ulohotels.com/ulowebsite/images/ulo_logo2.png" style="display:block;font-family:Helvetica,Arial,sans-serif;color:#fff;font-size:16px;float:left;" border="0">
                  </a>
                  </br>
               </td>
               <td>
                  <h3 style="text-align:right;padding:5px;"><span style="color:#86B940">Booking</span><span style="color:#145E8F"> Voucher</span><br>
                     <span  style="color:#000;font-size:12px;">Voucher No&nbsp;:&nbsp;${bookingid}<br>
                     Booking Id&nbsp;:&nbsp;${bookingid}<br>
                     Booking Date&nbsp;:&nbsp;${timeHours}</span>
                  </h3>
               </td>
            </tr>
         </table>
         <table>
            <span >
               <hr style="height:2px; border:none; color:#000; background-color:#115E92;margin-top:10px;">
            </span>
            <h4 style="color:#000;font-style:italic;line-height: 0px;font-size:14px;">Dear Partner</h4>
            <span style="text-align:justify;font-style:italic;font-size:13px;">Thank you for Partnering Ulo Hotels, We are happy to associate with you. Our mutual goal is to offer the best quality Service to all our guest.You can find the complete guest and booking details below. Incase of any queries, Please revert back to <a href="mailto:support@ulohotels.com" target="_top">support@ulohotels.com</a></span>
            <h4 style="color:#125E90;font-style:italic;line-height: 0px;font-size:16px;">Booking Details</h4>
            <span >
               <hr style="height:2px; border:none; color:#000; background-color:#115E92;margin-top:10px;">
            </span>
         </table>
         <table  class="responsive-table"  style="border:1px solid #d6d4d4;padding:5px;" >
            <tr >
               <td style="font-size:14px;font-family:Helvetica,Arial,sans-serif;color:#727272;padding-bottom:0px;font-weight:bold;">Booking Id</td>
               <td  style="text-align:left;"><b>:</b>&nbsp;${bookingid}</td>
            </tr>
            <tr>
               <td style="font-size:14px;font-family:Helvetica,Arial,sans-serif;color:#727272;padding-bottom:0px;font-weight:bold;">Name Of Guest</td>
               <td  style="text-align:left;"><b>:</b>&nbsp;${guestname}</td>
            </tr>
            <tr >
               <td style="font-size:14px;font-family:Helvetica,Arial,sans-serif;color:#727272;padding-bottom:0px;font-weight:bold;">Email</td>
               <td  style="text-align:left;"><b>:</b>&nbsp;${emailId}</td>
            </tr>
            <tr>
               <td style="font-size:14px;font-family:Helvetica,Arial,sans-serif;color:#727272;padding-bottom:0px;font-weight:bold;">Mobile</td>
               <td  style="text-align:left;"><b>:</b>&nbsp;${mobile}</td>
            </tr>
            <tr>
               <td colspan="2" style="font-size:15px;font-family:Helvetica,Arial,sans-serif;color:#000;padding-bottom:0px;font-weight:600;text-decoration:underline;">Room Details</td>
            </tr>
			   <tr >
               <td style="font-size:14px;font-family:Helvetica,Arial,sans-serif;color:#727272;padding-bottom:0px;font-weight:bold;">Hotel Name</td>
               <td  style="text-align:left;"><b>:</b>&nbsp;${propertyname}</td>
            </tr>
            <tr >
               <td style="font-size:14px;font-family:Helvetica,Arial,sans-serif;color:#727272;padding-bottom:0px;font-weight:bold;">Source</td>
               <td  style="text-align:left;"><b>:</b>&nbsp;${sourcename}</td>
            </tr>
            <tr>
               <td style="font-size:14px;font-family:Helvetica,Arial,sans-serif;color:#727272;padding-bottom:0px;font-weight:bold;">Check-In</td>
               <td  style="text-align:left;"><b>:</b>&nbsp;${checkin}</td>
            </tr>
            <tr>
               <td style="font-size:14px;font-family:Helvetica,Arial,sans-serif;color:#727272;padding-bottom:0px;font-weight:bold;">Check-Out</td>
               <td  style="text-align:left;"><b>:</b>&nbsp;${checkOut}</td>
            </tr>
            <tr>
               <td style="font-size:14px;font-family:Helvetica,Arial,sans-serif;color:#727272;padding-bottom:0px;font-weight:bold;">No-Of-Nights</td>
               <td  style="text-align:left;"><b>:</b>&nbsp;${days} Nights</td>
            </tr>   
            <tr>
               <td style="font-size:14px;font-family:Helvetica,Arial,sans-serif;color:#727272;padding-bottom:0px;font-weight:bold;white-space: nowrap; overflow: hidden;">Offers Applied</td>
               <td  style="text-align:left;"><b>:</b>&nbsp;${promotionName}</td>
            </tr>
            <tr>
               <td style="font-size:14px;font-family:Helvetica,Arial,sans-serif;color:#727272;padding-bottom:0px;font-weight:bold;white-space: nowrap; overflow: hidden;">Coupon Applied</td>
               <td style="text-align:left;"><b>:</b>&nbsp;${propertyDiscountName}</td>
            </tr>
            <tr>
               <td style="font-size:14px;font-family:Helvetica,Arial,sans-serif;color:#727272;padding-bottom:0px;font-weight:bold;white-space: nowrap; overflow: hidden;">Payment Status</td>
               <td  style="text-align:left;"><b>:</b>&nbsp;${paymentstatus}</td>
            </tr>
            <tr>
               <td style="font-size:14px;font-family:Helvetica,Arial,sans-serif;color:#727272;padding-bottom:0px;font-weight:bold;">Special Request</td>
               <td  style="text-align:left;"><b>:</b>&nbsp;${specialrequest}</td>
            </tr>
         </table>
         <h4 style="color:#125E90;font-style:italic;line-height: 0px;font-size:16px;">Payment Details</h4>
         <span >
            <hr style="height:2px; border:none; color:#000; background-color:#115E92;margin-top:10px;">
         </span>
         <div class="invoice" style="padding:10px;">
            <table width="100%"  style=" border-collapse: collapse;border:1px solid #d6d4d4;border-bottom:none;" cellpadding="0" cellspacing="0" class="responsive-table">
               <tr style="border-bottom:1px solid #d6d4d4;padding:5px;" >
                  <td style="text-align:center;font-size:13px;font-weight:600;padding:2px;">S.NO</td>
                  <td style="text-align:center;font-size:13px;font-weight:600;padding:2px;">Categories</td>
                  <td style="text-align:center;font-size:13px;font-weight:600;padding:2px;">Price</td>
                  <td style="text-align:center;font-size:13px;font-weight:600;padding:2px;">Tax</td>
                  <td style="text-align:center;font-size:13px;font-weight:600;padding:2px;">Total Amount</td>
               </tr>
			   <#list accommodationBooked as booked>
               <tr>
                  <td style="text-align:center;">${booked_index + 1}</td>
                  <td style="text-align:center;color:#000;font-weight:600;padding:2px;"><span >${booked.roomCount} ${booked.accommodationType} </span><span style="font-size:12px;color:#727272;">(${booked.adultCount} Adult / ${booked.childCount} child)</span></td>
                  <td style="text-align:center;color:#000;font-weight:600;padding:2px;">${booked.amount}</td>
                  <td style="text-align:center;color:#000;font-weight:600;padding:2px;">${booked.tax}</td>
                  <td style="text-align:center;color:#000;font-weight:600;padding:2px;">${booked.amount+booked.tax}</td>
               </tr >
               </#list>
               <tbody>
                  <tr  style="border-top:1px solid #d6d4d4;">
                     <td>&nbsp;</td>
                     <td>&nbsp;</td>
                     <td>&nbsp;</td>
                     <td>&nbsp;</td>
                     <td style="text-align:center;color:#000;font-weight:600;background-color:#F7F183;font-size:15px;padding:5px;">Total Amount:${totalamount}</td>
                  </tr>
               </tbody>
            </table>

         </div>
        <#-- <h4 style="color:#125E90;font-style:italic;line-height: 0px;font-size:15px;padding:5px;">ROOM TARIFF</h4>
         <span >
            <hr style="height:2px; border:none; color:#000; background-color:#115E92;margin-top:10px;">
         </span>
         <table width="100%"  style=" border-collapse: collapse;border:0px solid #d6d4d4;" cellpadding="0" cellspacing="0" class="responsive-table">
            <tr>
		
               <td style="color:#000;font-weight:600;text-align:left;font-size:14px;padding:5px;" width="50%" >Room Amount</td>
               <td style="color:#000;font-weight:600;text-align:right;font-size:14px;padding:5px; " width="50%" >${amount}</td>
             
            </tr>
			    <tr>
				  
               <td style="color:#000;font-weight:600;text-align:left;font-size:14px;padding:5px;" width="50%" >Tax</td>
               <td style="color:#000;font-weight:600;text-align:right;font-size:14px;padding:5px; " width="50%" >${tax}</td>
             
            </tr>
				    <tr style=" border-top:1px solid #d6d4d4;border-bottom:1px solid #d6d4d4;">
					<td style="color:#125E90;font-weight:600;text-align:right;font-size:14px;padding:5px;" width="80%" >Total Room Tariff </td>
               <td style="color:#125E90;font-weight:600;text-align:right;font-size:14px;padding:5px; " width="20%" >${totalamount}</td>
             
            </tr>
         </table>
		   <span >
               <h4 style="color:#125E90;font-style:italic;line-height: 0px;font-size:15px;padding:5px;">ULO SHARE</h4>
         <span >
            <hr style="height:2px; border:none; color:#000; background-color:#115E92;margin-top:10px;">
         </span>
         <table width="100%"  style=" border-collapse: collapse;border:0px solid #d6d4d4;" cellpadding="0" cellspacing="0" class="responsive-table">
            <tr>
		
               <td style="color:#000;font-weight:600;text-align:left;font-size:14px;padding:5px;" width="50%" >Ulo Commision</td>
               <td style="color:#000;font-weight:600;text-align:right;font-size:14px;padding:5px; " width="50%" >${uloCommission}</td>
             
            </tr>
			    <tr>
				  
               <td style="color:#000;font-weight:600;text-align:left;font-size:14px;padding:5px;" width="50%" >Ulo Tax</td>
               <td style="color:#000;font-weight:600;text-align:right;font-size:14px;padding:5px; " width="50%" >${uloTax}</td>
             
            </tr>
				    <tr style=" border-top:1px solid #d6d4d4;border-bottom:1px solid #d6d4d4;">
					<td style="color:#125E90;font-weight:600;text-align:right;font-size:14px;padding:5px;" width="80%" >Total Pay To Ulo </td>
               <td style="color:#125E90;font-weight:600;text-align:right;font-size:14px;padding:5px; " width="20%" >${totalUloRevenue}</td>
             
            </tr>
         </table>
		 <span >
               <h4 style="color:#125E90;font-style:italic;line-height: 0px;font-size:15px;padding:5px;">HOTEL REVENUE</h4>
         <span >
            <hr style="height:2px; border:none; color:#000; background-color:#115E92;margin-top:10px;">
         </span>
         <table width="100%"  style=" border-collapse: collapse;border:0px solid #d6d4d4;" cellpadding="0" cellspacing="0" class="responsive-table">
            <tr>
		
               <td style="color:#000;font-weight:600;text-align:left;font-size:14px;padding:5px;" width="50%" >Hotel Revenue</td>
               <td style="color:#000;font-weight:600;text-align:right;font-size:14px;padding:5px; " width="50%" >${hotelPayable}</td>
             
            </tr>
			    <tr>
				  
               <td style="color:#000;font-weight:600;text-align:left;font-size:14px;padding:5px;" width="50%" >Tax</td>
               <td style="color:#000;font-weight:600;text-align:right;font-size:14px;padding:5px; " width="50%" >${tax}</td>
             
            </tr>
				    <tr style=" border-top:1px solid #d6d4d4;border-bottom:1px solid #d6d4d4;">
					<td style="color:#125E90;font-weight:600;text-align:right;font-size:14px;padding:5px;" width="80%" >Hotel Revenue - Total </td>
               <td style="color:#125E90;font-weight:600;text-align:right;font-size:14px;padding:5px; " width="20%" >${totalHotelRevenue}</td>
             
            </tr>
         </table>
         <span >
            <hr style="height:2px; border:none; color:#000; background-color:#115E92;margin-top:5px;">
         </span>-->
             <table width="100%"  style=" border-collapse:collapse;border:1px solid #d6d4d4;" cellpadding="0" cellspacing="0" class="responsive-table">
               <tbody>
                  <tr >
                     <td style="text-align:left;color:#000;font-size:13px;background-color:#E5E5E5;font-weight:normal;padding:5px">GST : 33AABCU9979Q2Z8 <br> CIN : U55100TN2016PTC113073 <br> PAN : AABCU9979Q <br> Service Category-Hotel Service</td>
                     <td >&nbsp;</td>
                     <td >&nbsp;</td>
                     <td >&nbsp;</td>
                     <td style="text-align:center;color:#145E8F;font-weight:600;padding:5px;font-size:15px;">Advance:${totaladvanceamount} <br> <span style="text-align:center;color:#FF0000;font-weight:600;white-space: nowrap; overflow: hidden;font-size:15px;">Hotel Pay:${hotelpay}</span></td>
                  </tr>
               </tbody>
            </table>
       <p style="font-size:12px;text-align:center;font-weight:bold;">* This Computer Generated Voucher,Here No Signature Required.</p>
         <p style="font-size:12px;background-color:#E5E5E5;padding:5px;"><img src="https://www.ulohotels.com/ulowebsite/images/invoice/footmap.png" align="top">1/4, Plot No: 251, Second Floor, Rajamannar Salai, Vijayaraghavapuram, Saligramam, Chennai, Tamil Nadu 600093<br> <span><img src="https://www.ulohotels.com/ulowebsite/images/invoice/footmail.png" align="top"> support@ulohotels.com</span><img src="https://www.ulohotels.com/ulowebsite/images/invoice/footcall.png" align="top"> 1800-3000-8686<img src="https://www.ulohotels.com/ulowebsite/images/invoice/footweb.png" align="top" >www.ulohotels.com</p>
      </div>
   </body>
</html>