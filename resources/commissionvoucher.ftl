<!doctype html>
<html>
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible">
      <title>ULO HOTELS COMMISSION INVOICE</title>
      <style>
         @media screen and (max-width:600px) {
         .invoice-box{
         display:block;
         margin:0,auto;
         padding:10px;
         }
         }
         @media only screen and (max-width:480px) {
         @-ms-viewport { width:320px; }
         @viewport { width:320px; }
         }
         /** RTL **/
         .rtl {
         direction: rtl;
         font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
         }
         .rtl table {
         text-align: right;
         }
         .rtl table tr td:nth-child(2) {
         text-align: left;
         }
      </style>
   </head>
   <body>
      <div class="invoice-box" style="max-width: 800px; margin: auto; padding: 30px; border: 1px solid #E5E5E5; box-shadow: 0 0 10px rgba(0, 0, 0, .15); font-size: 16px; line-height: 24px; font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; color: #555;">
         <table style="width: 100%; line-height: inherit;text-align: left;" cellpadding="0" cellspacing="0">
            <tr>
               <td class="title">
                  <a href="https://www.ulohotels.com" target="_blank">
                  <img alt="Logo" src="https://www.ulohotels.com/ulowebsite/images/ulo_logo2.png" style="display:block;font-family:Helvetica,Arial,sans-serif;color:#fff;font-size:16px;float:left;" border="0">
                  </a>
                  </br>
               </td>
               <td>
                   <td>
                  <h3 style="text-align:right;padding:5px;"><span style="color:#86B940"></span><span style="color:#145E8F"> COMMISION INVOICE</span><br>
                     <span  style="color:#000;font-size:12px;">
                     Invoice Date&nbsp;:&nbsp;${voucherDate}<br>
                     Invoice No&nbsp;:&nbsp;${invoiceNumber}</span>
                  </h3>
               </td>
               </td>
            </tr>
         </table>
		 <span >
               <hr style="height:5px; border:none; color:#000; background-color:#0f467c;">
            </span>
		 <div >
         <table>
            
            <h4 style="color:#000;font-style:italic;line-height: 0px;font-size:14px;">Dear Partner,</h4>
            <span style="text-align:justify;font-size:13px;">Thanks for partnering with Ulo Hotels. We are happy to be associated with you. Our mutual goal is to offer best quality services to all our guests.
 			You can find the complete commission invoice details for the property below. In case of any queries, Please revert to <a href="mailto:finance@ulohotels.com" target="_top">finance@ulohotels.com</a>.</span> 
        
            <span >
               <hr style="height:2px; border:none;margin-top:10px;">
            </span>
         </table>
		 
         <div class="invoice" >
            <table  style=" border-collapse: collapse;border:1px solid #d6d4d4;" cellpadding="0" cellspacing="0" class="responsive-table">
            <tr  style="border-bottom:1px solid #d6d4d4;">
               <td style="padding:5px;font-size:13px;font-family:Helvetica,Arial,sans-serif;color:#434343;padding-bottom:0px;font-weight:bold;">Property Name</td>
               <td  style="padding:5px;text-align:left;font-size:13px;"><b>:</b>&nbsp;${propertyName}</td>

            </tr>
			 <tr style="border-bottom:1px solid #d6d4d4;" >
               <td style="padding:5px;font-size:13px;font-family:Helvetica,Arial,sans-serif;color:#434343;padding-bottom:0px;font-weight:bold;">Address</td>
               <td  style="padding:5px;text-align:left;font-size:13px;"><b>:</b>&nbsp;${propertyAddress}</td>
            </tr>
			<tr  style="border-bottom:1px solid #d6d4d4;">
               <td style="padding:5px;font-size:13px;font-family:Helvetica,Arial,sans-serif;color:#434343;padding-bottom:0px;font-weight:bold;">GST</td>
               <td  style="padding:5px;text-align:left;font-size:13px;"><b>:</b>&nbsp;${propertyGstNo}</td>
            </tr>
			
		
            </table>

         </div>
		        <h4 style="color:#125E90;font-style:italic;line-height: 0px;font-size:16px;">Commision Details</h4>
         <span >
            <hr style="height:2px; border:none; color:#000; background-color:#115E92;margin-top:10px;">
         </span>
           <div class="invoice" style="padding:10px;">
            <table width="100%"  style=" border-collapse: collapse;border:1px solid #d6d4d4;border-bottom:none;" cellpadding="0" cellspacing="0" class="responsive-table">
               <tr style="border-bottom:1px solid #d6d4d4;padding:5px;" >
                  <td style="text-align:center;font-size:13px;font-weight:600;padding:2px;">Time Period</td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
			
                  <td style="text-align:center;font-size:13px;font-weight:600;padding:2px;">Amount</td>
               </tr>
			 
               <tr>
                  <td style="text-align:center;">Commission Amount ( ${durationPeriod} )</td>
				  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				  
                  <td style="text-align:center;color:#000;font-weight:600;padding:2px;">${uloCommission}</td>
               </tr >
             <tr style="border:1px solid #E5E5E5;">
                  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				  <td style="padding:2px;">SUB TOTAL</td>
				  
                  <td style="text-align:center;color:#000;font-weight:600;padding:2px;">${uloCommission}</td>
               </tr >
			     <tr>
                  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				 
				  <td>CGST (9%)</td>
                  <td style="text-align:center;color:#000;font-weight:600;padding:2px;">${centralGstTax}</td>
               </tr >
              <tr>
                  <td>&nbsp;</td>
				  <td>&nbsp;</td>
				  
				  <td>SGST (9%)</td>
                  <td style="text-align:center;color:#000;font-weight:600;padding:2px;">${stateGstTax}</td>
               </tr >
                 <tr style="border:1px solid #E5E5E5;">
                     <td style="text-align:left;color:#000;font-size:13px;background-color:#E5E5E5;font-weight:normal;padding:5px">GST : 33AABCU9979Q2Z8 <br> CIN : U55100TN2016PTC113073 <br> PAN : AABCU9979Q</td>
                     <td >&nbsp;</td>
                     <td style="color:#145E8F;font-weight:600;font-size:15px;">Total Amount:</td>
                    
                     <td style="text-align:center;color:#145E8F;font-weight:600;padding:5px;font-size:15px;">${totalUloCommission}</td>
                  </tr>

            </table>
   
         </div>
         <span >
            <hr style="height:2px; border:none; color:#000; background-color:#d6d4d4;margin-top:5px;">
         </span>
         <span style="font-size:12px;"><span style="color:#0d467b;font-weight:bold;">Regards </span><br>Finance Team ,<br> Ulo Hotels Private Limited ,<br>Chennai.</span>
     <p style="font-size:12px;text-align:center;font-weight:bold; border-top:1px solid rgb(217, 217, 217);">In case of any queries, Please revert to <a href="mailto:finance@ulohotels.com" target="_top">finance@ulohotels.com</a>.</p>
         </div>
		 <p style="font-size:12px;background-color:#E5E5E5;padding:5px;"><img src="https://www.ulohotels.com/ulowebsite/images/invoice/footmap.png" align="top">1/4, Plot No: 251, Second Floor, Rajamannar Salai, Vijayaraghavapuram, Saligramam, Chennai, Tamil Nadu 600093 <br> <span><img src="https://www.ulohotels.com/ulowebsite/images/invoice/footmail.png" align="top"> support@ulohotels.com</span><img src="https://www.ulohotels.com/ulowebsite/images/invoice/footcall.png" align="top"> 1800-3000-8686<img src="https://www.ulohotels.com/ulowebsite/images/invoice/footweb.png" align="top" >www.ulohotels.com</p>
      </div>
   </body>
</html>