<html>
<body>
Hi ${userName},

<p>You recently requested to reset your password. Please click the button below to start the password reset process.</p>
<p>your password ${password} </p>
<p>If you did not request a password change, you may ignore this message and your password will remain unchanged.</p>
 <p><a href="https://ulotel.ulohotels.com/">Click Here For Ulotel</a></p>
 
Regards,<br/>
${from}.
</body>
</html>