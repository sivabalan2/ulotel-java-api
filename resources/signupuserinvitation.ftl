<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Signup User Invitation</title>
    <style type="text/css">
        html {
            width: 100%;
        }
        
        body {
            margin: 0;
            padding: 0;
            width: 100%;
            -webkit-text-size-adjust: none;
            -ms-text-size-adjust: none;
        }
        
        @media only screen and (max-width: 597px) {
            *[class].container {
                width: 100% !important;
                max-width: 100% !important;
                clear: both !important;
            }
            td[class="headerlogo"] {
                padding: 15px 10px !important;
                text-align: center;
            }
            .inner-mob td {
                width: 100% !important;
                display: block;
                margin: 0 auto;
                padding: 10px 0 !important;
            }
            td.inner-mob {
                padding: 10px 15px !important;
            }
            td.inner-footer {
                padding-top: 10px !important;
            }
            td.padb1 {
                padding: 0 !important;
            }
        }
    </style>
</head>

<body style="background:#FFFFFF; padding:0; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; font-size:13px; color:#4D4D4D;line-height: 20px;">
    <table class="container" bgcolor="#f5f8fb" width="650" border="0" align="center" cellpadding="0" cellspacing="0" style="width:650px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;">
        <tr>
            <td style="display:block;" bgcolor="#f5f8fb">
                <table border="0" align="center" cellpadding="0" cellspacing="0" width="100%" style="width:100%;">
                    <tr>
                        <td valign="top" style="padding:0px;">
                            <table width="100%" align="left" border="0" cellpadding="0" cellspacing="0" class="layout">
                                <tr>
                                    <td valign="top" style="padding: 20px 30px; border-bottom: 1px solid #b6cbe1;">
                                        <table border="0" width="100%" align="left" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td class="headerlogo" align="left" valign="top" width="50%" style="padding-bottom: 10px;">
                                                    <a href="https://www.ulohotels.com/" target="_blank" title="Welcome Ulo Logo"><img src="https://ulotel.ulohotels.com/contents/images/voucher-icon/ulo-logo.png" width="134" alt="Welcome Ulo Logo" style="border:0;" /></a>
                                                </td>
                                              
                                            </tr>
                                        

                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" style="padding: 20px 30px 0 30px;">
                                        
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" style="padding:0 30px 0 30px;">
                                        <table bgColor="#FFFFFF" cellpadding="0" cellspacing="0" border="0" width="100%" style="padding: 30px 40px;border-bottom: 1px solid #b6cbe1;">
                                            <tr>
                                                <td colspan="2" align="left" valign="top">
                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                        <tr>
                                                            <td valign="middle" class="padb1" align="justify" style="line-height:20px; font-size:20px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; margin:0px;padding-bottom: 10px;">
                                                                <table cellpadding="0" cellspacing="0" border="0" style="margin: auto">
                                                                    <tr>
                                                                        
                                                                        <td valign="middle" class="padb1" align="center" style="padding-left: 10px;">
                                                                        	<img width="43" height="46" src="https://ulotel.ulohotels.com/contents/images/voucher-icon/bird-icon.png" alt="brid" />
                                                                            <font color="#568419" style="line-height:20px;font-size:20px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;letter-spacing: 1px;">
                                                                                <b style="color:#042649">WELCOME TO ULO HOTELS</b>
                                                                            </font>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <table cellpadding="0" cellspacing="0" border="0" style="text-align: center; margin: auto">
                                                                            <tr>
                                                                                <td valign="top" class="padb1" align="center" style="line-height:24px; font-size:14px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; margin:0px;padding-bottom: 20px;">
                                                                                    <font color="#333333" style="line-height:24px;font-size:14px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;"><b>THANK YOU FOR JOINING THE ULO FAMILY!</b></font>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </tr>
                                                                    <tr>
                                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                            <tr>
                                                                                <td valign="top" class="padb1" align="left" style="line-height:24px; font-size:14px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; margin:0px;padding-bottom: 10px;">
                                                                                    <font color="#333333" style="line-height:24px;font-size:14px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;"><b>Dear ${guestName},</b></font>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td valign="top" class="padb1" align="left" style="line-height:20px; font-size:13px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; margin:0px;">
                                                                                    <font color="#777777" style="line-height:24px;font-size:13px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;">
																					Thanks for signing up to keep in touch with Ulo hotels. From now you will get regular updates on fare prices and special offers from Ulo hotels. 
																					Since you will be the first to know, you can make your stay comfortable and cost effective.
																					</font>
                                                                                </td>
                                                                            </tr>
																			   <tr>
                                                                                <td valign="top" class="padb1" align="left" style="line-height:24px; font-size:14px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; margin:0px;padding-bottom: 10px;text-align:center;">
                                                                                    <font align="center" style="line-height:24px; font-size:14px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; margin:0px;padding-bottom: 20px;"><b>The Ulo promoises Guarantees The Following</b></font>
                                                                                </td>
                                                                            </tr>
																		
                                                                        </table>
																		<table cellpadding="0" cellspacing="0" border="0" width="100%">
																			<tr>
																			  <td  class="padb1"  style="line-height:24px; font-size:14px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; margin:0px;padding-bottom: 10px;text-align:center;">
                                                                                    <img src="http://ulotel.ulohotels.com/contents/images/fivebs/gs100.png" height="60px" width="60px" alt="Ulo hotels comfortable stay" style="vertical-align:middle;display:block;margin:0 auto;">
																					<font color="#333333" style="line-height:24px;font-size:12px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;"><b>Budget Friendly & Value</b></font>
                                                                                </td>
																			  <td  class="padb1"  style="line-height:24px; font-size:14px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; margin:0px;padding-bottom: 10px;text-align:center;">
                                                                                    <img src="http://ulotel.ulohotels.com/contents/images/fivebs/gs101.png" height="60px" width="60px" alt="Ulo hotels comfortable stay" style="vertical-align:middle;display:block;margin:0 auto;">
																					<font color="#333333" style="line-height:24px;font-size:12px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;"><b>Breakfast Healthy & Regional</b></font>
                                                                                </td>
																				  <td  class="padb1"  style="line-height:24px; font-size:14px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; margin:0px;padding-bottom: 10px;text-align:center;">
                                                                                    <img src="http://ulotel.ulohotels.com/contents/images/fivebs/gs102.png" height="60px" width="60px" alt="Ulo hotels comfortable stay" style="vertical-align:middle;display:block;margin:0 auto;">
																					<font color="#333333" style="line-height:24px;font-size:12px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;"><b>Bed Clean & Comfort</b></font>
                                                                                </td>
																			
																			</tr>
																	
																		</table>
																		<table cellpadding="0" cellspacing="0" border="0" width="100%">
																				<tr  >
																		
																			  <td  class="padb1"  style="line-height:24px; font-size:14px; width:50%;font-family: Helvetica Neue, Helvetica, Arial, sans-serif; margin:0px;padding-bottom: 10px;text-align:center;">
                                                                                    <img src="http://ulotel.ulohotels.com/contents/images/fivebs/gs104.png" height="60px" width="60px" alt="Ulo hotels comfortable stay" style="vertical-align:middle;display:block;margin:0 auto;">                                                                                   
																				   <font color="#333333" style="line-height:24px;font-size:12px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;"><b>Best Guest Service & Care</b></font>
                                                                                </td>
																				  <td  class="padb1"  style="line-height:24px; width:50%;font-size:14px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; margin:0px;padding-bottom: 10px;text-align:center;">
                                                                                    <img src="http://ulotel.ulohotels.com/contents/images/fivebs/gs103.png" height="60px" width="60px" alt="Ulo hotels comfortable stay" style="vertical-align:middle;display:block;margin:0 auto;">
																					<font color="#333333" style="line-height:24px;font-size:12px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;"><b>Bathroom Hygienic & Functional</b></font>
                                                                                </td>
																			
																			</tr>
																		</table>
                                                                    </tr>
																	
                                                                </table>

                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            <tr>
                      
                                    <tr>
                                        <td class="inner-footer" style="padding-top:5px;padding-left:30px;padding-right:30px;text-align:center;">
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tr>
                                                    <td style="font-family: Helvetica Neue, Helvetica, Arial, sans-serif;line-height:26px;font-size: 15px;color:#898989; padding:0 40px 10px 40px;">
                                                        <font color="#898989" style="line-height:24px;font-size:15px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;">1/4, Plot No: 251, Second Floor, Rajamannar Salai, Vijayaraghavapuram, Saligramam, Chennai, Tamil Nadu 600093</font>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="font-family: Helvetica Neue, Helvetica, Arial, sans-serif;line-height:26px;font-size: 16px;color:#011830;padding:0 40px 10px 40px;">
                                                        <font color="#011830" style="line-height:24px;font-size:16px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;">
                                                            <a href="mailto:support@ulohotels.com" style="color:#011830; text-decoration: none;" title="support@ulohotels.com">support@ulohotels.com</a> | <a style="color:#011830; text-decoration: none;"
                                                                title="9790 999 222" href="tel:9790 999 222">9790 999 222</a> | <a style="color:#011830; text-decoration: none;" href="https://www.ulohotels.com/" title="www.ulohotels.com">www.ulohotels.com</a>
                                                        </font>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" style="padding:10px 0 40px 0;">
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tr>
                                                    <td class="footer-width" width="38%"></td>
                                                    <td class="footer-widthcenter" width="24%">
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                                <td align="center" style="padding:0 5px;">
                                                                    <a href="https://www.facebook.com/Ulohotels/"><img src="https://ulotel.ulohotels.com/contents/images/voucher-icon/icon-facebook.png" style="display:inline-block;border:0;" alt="Facebook" /></a>
                                                                </td>
                                                                <td align="center" style="padding:0 5px;">
                                                                    <a href="https://twitter.com/ulohotels"><img src="https://ulotel.ulohotels.com/contents/images/voucher-icon/icon-twitter.png" style="display:inline-block;border:0;" alt="Twitter" /></a>
                                                                </td>
                                                                <td align="center" style="padding:0 5px;">
                                                                    <a href="https://plus.google.com/+ULOHOTELS"><img src="https://ulotel.ulohotels.com/contents/images/voucher-icon/icon-google.png" style="display:inline-block;border:0;" alt="Google Plus" /></a>
                                                                </td>
                                                                <td align="center" style="padding:0 5px;">
                                                                    <a href="https://www.pinterest.com/ulohotels/"><img src="https://ulotel.ulohotels.com/contents/images/voucher-icon/icon-pintrest.png" style="display:inline-block;border:0;" alt="Pintrest" /></a>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td class="footer-width" width="38%"></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                            </table>
                        </td>
                        </tr>
                </table>
            </td>
            </tr>
    </table>
</body>

</html>