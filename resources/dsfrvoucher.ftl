<!doctype html>
<html>
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible">
      <title>ULO HOTELS DSFR VOUCHER</title>
      <style>
         @media screen and (max-width:600px) {
         .invoice-box{
         display:block;
         margin:0,auto;
         padding:10px;
         }
         }
         @media only screen and (max-width:480px) {
         @-ms-viewport { width:320px; }
         @viewport { width:320px; }
         }
         /** RTL **/
         .rtl {
         direction: rtl;
         font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
         }
         .rtl table {
         text-align: right;
         }
         .rtl table tr td:nth-child(2) {
         text-align: left;
         }
      </style>
   </head>
   <body>
      <div class="invoice-box" style="max-width: 800px; margin: auto; padding: 30px; border: 1px solid #E5E5E5; box-shadow: 0 0 10px rgba(0, 0, 0, .15); font-size: 16px; line-height: 24px; font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; color: #555;">
         <table style="width: 100%; line-height: inherit;text-align: left;" cellpadding="0" cellspacing="0">
            <tr>
               <td class="title">
                  <a href="https://www.ulohotels.com" target="_blank">
                  <img alt="Logo" src="https://www.ulohotels.com/ulowebsite/images/ulo_logo2.png" style="display:block;font-family:Helvetica,Arial,sans-serif;color:#fff;font-size:16px;float:left;" border="0">
                  </a>
                  </br>
               </td>
               <td>
                  <h3 style="text-align:right;padding:5px;"><span style="color:#86B940"></span><span style="color:#145E8F"> DSFR REPORT</span><br>
               </td>
            </tr>
         </table>
		 <span >
               <hr style="height:5px; border:none; color:#000; background-color:#0f467c;">
            </span>
		 <div style="border:10px solid #75a849;border-top:60px solid #75a849;padding:5px;">
         <table>
            
            <h4 style="color:#000;font-style:italic;line-height: 0px;font-size:14px;">Dear Partner,</h4>
            <span style="text-align:justify;font-size:13px;">Thanks for partnering with Ulo Hotels.We are happy to be associated with you.Our mutual goal is to offer best quality service to all our guests. You can find the 15 days DSFR details for the property below. In case of any queries, Please revert to <a href="mailto:finance@ulohotels.com" target="_top">finance@ulohotels.com</a></span> 
        
            <span >
               <hr style="height:2px; border:none;margin-top:10px;">
            </span>
         </table>
		 
         <div class="invoice" >
            <table  style=" border-collapse: collapse;border:1px solid #d6d4d4;" cellpadding="0" cellspacing="0" class="responsive-table">
            <tr  style="border-bottom:1px solid #d6d4d4;">
               <td style="padding:5px;font-size:13px;font-family:Helvetica,Arial,sans-serif;color:#434343;padding-bottom:0px;font-weight:bold;">Property Name</td>
               <td  style="padding:5px;text-align:left;font-size:13px;"><b>:</b>&nbsp;${propertyName}</td>

            </tr>
			 <tr style="border-bottom:1px solid #d6d4d4;" >
               <td style="padding:5px;font-size:13px;font-family:Helvetica,Arial,sans-serif;color:#434343;padding-bottom:0px;font-weight:bold;">Address</td>
               <td  style="padding:5px;text-align:left;font-size:13px;"><b>:</b>&nbsp;${propertyAddress}</td>
            </tr>
			<tr  style="border-bottom:1px solid #d6d4d4;">
               <td style="padding:5px;font-size:13px;font-family:Helvetica,Arial,sans-serif;color:#434343;padding-bottom:0px;font-weight:bold;">GST</td>
               <td  style="padding:5px;text-align:left;font-size:13px;"><b>:</b>&nbsp;${propertyGstNo}</td>
            </tr>
			 <tr >
               <td style="padding:5px;font-size:13px;font-family:Helvetica,Arial,sans-serif;color:#434343;padding-bottom:0px;font-weight:bold;">Time Period</td>
               <td  style="padding:5px;text-align:left;font-size:13px;"><b>:</b>&nbsp;${durationPeriod}</td>
            </tr>
		
            </table>

         </div>
		       <span >
               <hr style="height:2px; border:none; margin-top:10px;">
            </span>
         <table  class="responsive-table"  style=" border-collapse: collapse;border:1px solid #d6d4d4;" cellpadding="0" cellspacing="0" >
		     <tr  >
               <td colspan="2" style="border-bottom:5px solid #0f467c;background-color:#dde6ed;font-size:15px;font-family:Helvetica,Arial,sans-serif;color:#125e90;padding:2px;font-weight:normal;">DSFR Details</td>
            </tr>
            <tr style="border-bottom:1px solid #d6d4d4;" >
               <td style="padding:5px;font-size:13px;font-family:Helvetica,Arial,sans-serif;color:#434343;padding-bottom:0px;font-weight:bold;">Total Amount</td>
               <td  style="padding:5px;text-align:left;"><b>:</b>&nbsp;${revenueTaxAmount}</td>
            </tr>
            <tr style="border-bottom:1px solid #d6d4d4;">
               <td style="padding:5px;font-size:13px;font-family:Helvetica,Arial,sans-serif;color:#434343;padding-bottom:0px;font-weight:bold;">Total Tax</td>
               <td  style="padding:5px;text-align:left;"><b>:</b>&nbsp;${totalTaxAmount}</td>
            </tr>
            <tr style="border-bottom:1px solid #d6d4d4;">
               <td style="padding:5px;font-size:13px;font-family:Helvetica,Arial,sans-serif;color:#4b9a25;padding-bottom:0px;font-weight:bold;">Revenue <span style="text-align:center;font-size:10px;font-weight:600;padding:2px;color:#b4b4b4;"> (A)</span></td>
               <td  style="padding:5px;text-align:left;"><b>:</b>&nbsp;${revenueAmount}</td>
            </tr>
            <tr style="border-bottom:1px solid #d6d4d4;">
               <td style="padding:5px;font-size:13px;font-family:Helvetica,Arial,sans-serif;color:#434343;padding-bottom:0px;font-weight:bold;">Ulo Service Fee</td>
               <td  style="padding:5px;text-align:left;"><b>:</b>&nbsp;${uloCommission}</td>
            </tr>
			   <tr style="border-bottom:1px solid #d6d4d4;" >
               <td style="padding:5px;font-size:13px;font-family:Helvetica,Arial,sans-serif;color:#434343;padding-bottom:0px;font-weight:bold;">Ulo Tax</td>
               <td  style="padding:5px;text-align:left;"><b>:</b>&nbsp;${uloTaxAmount}</td>
            </tr>
            <tr style="border-bottom:1px solid #d6d4d4;" >
               <td style="padding:5px;font-size:13px;font-family:Helvetica,Arial,sans-serif;color:#ea1d02;padding-bottom:0px;font-weight:bold;">Invoice Ulo <span style="text-align:center;font-size:10px;font-weight:600;padding:2px;color:#b4b4b4;">(B)</span></td>
               <td  style="padding:5px;text-align:left;"><b>:</b>&nbsp;${uloRevenue}</td>
            </tr>
            <tr style="border-bottom:1px solid #d6d4d4;">
               <td style="padding:5px;font-size:13px;font-family:Helvetica,Arial,sans-serif;color:#ea1d02;padding-bottom:0px;font-weight:bold;">Resort Pay <span style="text-align:center;font-size:10px;font-weight:600;padding:2px;color:#b4b4b4;">(c)</span></td>
               <td  style="padding:5px;text-align:left;"><b>:</b>&nbsp;${hotelDueRevenue}</td>
            </tr>
            <tr >
               <td style="padding:5px;font-size:13px;font-family:Helvetica,Arial,sans-serif;color:#434343;padding-bottom:0px;font-weight:bold;">Net Payable <span style="text-align:center;font-size:10px;font-weight:600;padding:2px;color:#b4b4b4;"> A - ( B+C ) </span></td>
               <td  style="padding:5px;text-align:left;"><b>:</b>&nbsp;${netHotelPayable}</td>
            </tr>
        
         </table>
         <span >
            <hr style="height:2px; border:none; color:#000; background-color:#d6d4d4;margin-top:5px;">
         </span>
         <span style="font-size:12px;"><span style="color:#0d467b;font-weight:bold;">Regards </span><br>Finance Team ,<br> Ulo Hotels Private Limited ,<br>Chennai.</span>
     <p style="font-size:12px;text-align:center;font-weight:bold; border-top:1px solid rgb(217, 217, 217);">In case of any queries, Please revert to <a href="mailto:finance@ulohotels.com" target="_top">finance@ulohotels.com</a></p>
         </div>
		 <p style="font-size:12px;background-color:#E5E5E5;padding:5px;"><img src="https://www.ulohotels.com/ulowebsite/images/invoice/footmap.png" align="top">1/4, Plot No: 251, Second Floor, Rajamannar Salai, Vijayaraghavapuram, Saligramam, Chennai, Tamil Nadu 600093 <br> <span><img src="https://www.ulohotels.com/ulowebsite/images/invoice/footmail.png" align="top"> support@ulohotels.com</span><img src="https://www.ulohotels.com/ulowebsite/images/invoice/footcall.png" align="top"> 1800-3000-8686<img src="https://www.ulohotels.com/ulowebsite/images/invoice/footweb.png" align="top" >www.ulohotels.com</p>
      </div>
   </body>
</html>