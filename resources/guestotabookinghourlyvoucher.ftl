<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>ULO Booking Voucher</title>
    <style type="text/css">
        html {
            width: 100%;
        }
        
        body {
            margin: 0;
            padding: 0;
            width: 100%;
            -webkit-text-size-adjust: none;
            -ms-text-size-adjust: none;
        }
        
        @media only screen and (max-width: 597px) {
            *[class].container {
                width: 100% !important;
                max-width: 100% !important;
                clear: both !important;
            }
            td[class="headerlogo"] {
                padding: 15px 10px !important;
                text-align: center;
            }
            .inner-mob td {
                width: 100% !important;
                display: block;
                margin: 0 auto;
                padding: 10px 0 !important;
            }
            td.inner-mob {
                padding: 10px 15px !important;
            }
            td.inner-footer {
                padding-top: 10px !important;
            }
            td.padb1 {
                padding: 0 !important;
            }
        }
    </style>
</head>

<body style="background:#FFFFFF; padding:0; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; font-size:13px; color:#4D4D4D;line-height: 20px;">
    <table class="container" bgcolor="#f5f8fb" width="650" border="0" align="center" cellpadding="0" cellspacing="0" style="width:650px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;">
        <tr>
            <td style="display:block;" bgcolor="#f5f8fb">
                <table border="0" align="center" cellpadding="0" cellspacing="0" width="100%" style="width:100%;">
                    <tr>
                        <td valign="top" style="padding:0px;">
                            <table width="100%" align="left" border="0" cellpadding="0" cellspacing="0" class="layout">
                                <tr>
                                    <td valign="top" style="padding: 20px 30px; border-bottom: 1px solid #b6cbe1;">
                                        <table border="0" width="100%" align="left" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td class="headerlogo" align="left" valign="top" width="50%" style="padding-bottom: 10px;">
                                                    <a href="https://www.ulohotels.com/" target="_blank" title="Ulo Hotels"><img src="https://ulotel.ulohotels.com/contents/images/voucher-icon/ulo-logo.png" width="134" alt="Ulo Hotels" style="border:0;" /></a>
                                                </td>
                                                <td align="right" valign="middle" width="50%">
                                                    ${timeHours}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" style="font-size: 14px;">
                                                    Hotel Confirmation for Booking Id &nbsp;&nbsp;${bookingId} &nbsp;&nbsp;${propertyName}
                                                </td>
                                            </tr>

                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" style="padding: 20px 30px 0 30px;">
                                        <table bgColor="#FFFFFF" cellpadding="0" cellspacing="0" border="0" width="100%" style="padding: 30px 40px;border-bottom: 1px solid #b6cbe1;">
                                            <tr>
                                                <td colspan="2" align="left" valign="top">
                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                        <tr>
                                                            <td valign="top" class="padb1" align="justify" style="line-height:20px; font-size:18px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; margin:0px;padding-bottom: 10px;">
                                                                <font color="#568419" style="line-height:20px;font-size:18px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;letter-spacing: 1px;"><b>Booking Voucher</b></font>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top" width="50%">
                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                        <tr>
                                                            <td valign="top" class="padb1" align="left" style="line-height:24px; font-size:14px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; margin:0px;">
                                                                <font color="#555555" style="line-height:24px;font-size:14px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;">Voucher No : ${bookingId}</font>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top" class="padb1" align="left" style="line-height:24px; font-size:14px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; margin:0px;">
                                                                <font color="#555555" style="line-height:24px;font-size:14px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;">Booking Id : ${bookingId}</font>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top" class="padb1" align="left" style="line-height:24px; font-size:14px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; margin:0px;">
                                                                <font color="#555555" style="line-height:24px;font-size:14px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;">ULO GST : 33AABCU9979Q2Z8</font>
                                                            </td>
                                                        </tr>
                                                        
                                                    </table>
                                                </td>
                                                <td align="right" valign="top" width="50%">
                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                        <tr>
                                                            <td valign="top" class="padb1" align="right" style="line-height:24px; font-size:14px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; margin:0px;">
                                                                <font color="#555555" style="line-height:24px;font-size:14px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;">Booking Date : ${bookingDate}</font>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top" class="padb1" align="right" style="line-height:24px; font-size:14px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; margin:0px;">
                                                                <font color="#555555" style="line-height:24px;font-size:14px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;">Booking Time : ${bookingTime}</font>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top" class="padb1" align="right" style="line-height:24px; font-size:14px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; margin:0px;">
                                                                <font color="#555555" style="line-height:24px;font-size:14px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;">ULO PAN Number: AABCU9979Q</font>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" style="padding:0 30px 0 30px;">
                                        <table bgColor="#FFFFFF" cellpadding="0" cellspacing="0" border="0" width="100%" style="padding: 30px 40px;border-bottom: 1px solid #b6cbe1;">
                                            <tr>
                                                <td colspan="2" align="left" valign="top">
                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                        <tr>
                                                            <td valign="middle" class="padb1" align="justify" style="line-height:20px; font-size:20px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; margin:0px;padding-bottom: 10px;">
                                                                <table cellpadding="0" cellspacing="0" border="0" style="margin: auto">
                                                                    <tr>
                                                                        
                                                                        <td valign="middle" class="padb1" align="center" style="padding-left: 10px;">
                                                                        	<img width="43" height="46" src="https://ulotel.ulohotels.com/contents/images/voucher-icon/bird-icon.png" alt="brid" />
                                                                            <font color="#568419" style="line-height:20px;font-size:20px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;letter-spacing: 1px;">
                                                                                <b>Great!</b><b style="color:#042649">Your Stay Is Confirmed</b>
                                                                            </font>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <table cellpadding="0" cellspacing="0" border="0" style="text-align: center; margin: auto">
                                                                            <tr>
                                                                                <td valign="top" class="padb1" align="center" style="line-height:24px; font-size:14px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; margin:0px;padding-bottom: 20px;">
                                                                                    <font color="#333333" style="line-height:24px;font-size:14px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;"><b>Thank you for booking with us</b></font>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </tr>
                                                                    <tr>
                                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                                            <tr>
                                                                                <td valign="top" class="padb1" align="left" style="line-height:24px; font-size:14px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; margin:0px;padding-bottom: 10px;">
                                                                                    <font color="#333333" style="line-height:24px;font-size:14px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;"><b>Dear ${guestName},</b></font>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td valign="top" class="padb1" align="left" style="line-height:20px; font-size:13px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; margin:0px;padding-bottom: 20px;">
                                                                                    <font color="#777777" style="line-height:24px;font-size:13px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;">
                                                                                    We, Ulo Hotels thank you for giving us an opportunity to serve you in the coming date. On behalf of the hotel, we would like to express our gratitude to you for choosing our services. Please find all the details regarding the confirmation of the reservation listed below,  In case of any query please feel free to write us at <a style="color:#042649;" href="mailto:reservations@ulohotels.com" target="_blank">reservations@ulohotels.com</a></font>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </tr>
                                                                </table>

                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr>
                                    <td valign="top" style="padding:0 30px 0 30px;">
                                        <table bgColor="#FFFFFF" cellpadding="0" cellspacing="0" border="0" width="100%" style="padding: 30px 40px;border-bottom: 1px solid #b6cbe1;">
                                            <tr>
                                                <td align="left" valign="top" width="50%" style="padding-bottom: 20px">
                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                        <tr>
                                                            <td valign="top" class="padb1" align="left" style="line-height:24px; font-size:16px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; margin:0px;">
                                                                <font color="#042649" style="line-height:24px;font-size:16px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;"><b>Name of Guest</b></font>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top" class="padb1" align="left" style="line-height:24px; font-size:14px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; margin:0px;">
                                                                <font color="#333333" style="line-height:24px;font-size:14px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;">${guestName}</font>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td align="right" valign="top" width="50%" style="padding-bottom: 20px">
                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                        <tr>
                                                            <td valign="top" class="padb1" align="left" style="line-height:24px; font-size:16px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; margin:0px;">
                                                                <font color="#042649" style="line-height:24px;font-size:16px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;"><b>Email Id</b></font>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top" class="padb1" align="left" style="line-height:24px; font-size:14px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; margin:0px;">
                                                                <font color="#333333" style="line-height:24px;font-size:14px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;">${emailId}</font>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top" width="50%" style="padding-bottom: 20px">
                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                        <tr>
                                                            <td valign="top" class="padb1" align="left" style="line-height:24px; font-size:16px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; margin:0px;">
                                                                <font color="#042649" style="line-height:24px;font-size:16px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;"><b>Mobile Number</b></font>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top" class="padb1" align="left" style="line-height:24px; font-size:14px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; margin:0px;">
                                                                <font color="#333333" style="line-height:24px;font-size:14px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;">${mobile}</font>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td align="right" valign="top" width="50%" style="padding-bottom: 20px">
                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                        <tr>
                                                            <td valign="top" class="padb1" align="left" style="line-height:24px; font-size:16px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; margin:0px;">
                                                                <font color="#042649" style="line-height:24px;font-size:16px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;"><b>GST Number</b></font>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top" class="padb1" align="left" style="line-height:24px; font-size:14px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; margin:0px;">
                                                                <font color="#333333" style="line-height:24px;font-size:14px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;">${gstno}</font>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr>
                                    <td valign="top" style="padding:0 30px 0 30px;">
                                        <table bgColor="#FFFFFF" cellpadding="0" cellspacing="0" border="0" width="100%" style="padding: 30px 40px;border-bottom: 1px solid #b6cbe1;">
                                            <tr>
                                                <td align="left" valign="top" width="40%" style="padding-bottom: 20px">
                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                        <tr>
                                                            <td valign="top" class="padb1" align="left" style="line-height:24px; font-size:16px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; margin:0px;">
                                                                <font color="#042649" style="line-height:24px;font-size:16px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;"><b>${checkIn}</b></font>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top" class="padb1" align="left" style="line-height:24px; font-size:14px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; margin:0px;">
                                                                <font color="#333333" style="line-height:24px;font-size:14px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;">${checkInDays}, ${bookingInTime} </font>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td align="center" valign="justify" width="20%" style="padding-bottom: 20px">
                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                        <tr>
                                                            <td valign="top" class="padb1" align="center" style="line-height:24px; font-size:12px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; margin:0px;">
                                                                <img src="https://ulotel.ulohotels.com/contents/images/voucher-icon/icon-time.png" width="22" height="22" style="display: block; width: 22px; height: 22px;" />
                                                                <font color="#777777" style="line-height:24px;font-size:12px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;">
                                                                    <b>${hours} Hours</b></font>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td align="right" valign="top" width="40%" style="padding-bottom: 20px">
                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                        <tr>
                                                            <td valign="top" class="padb1" align="right" style="line-height:24px; font-size:16px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; margin:0px;">
                                                                <font color="#042649" style="line-height:24px;font-size:16px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;">
                                                                    <b>${checkOut}</b></font>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top" class="padb1" align="right" style="line-height:24px; font-size:14px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; margin:0px;">
                                                                <font color="#333333" style="line-height:24px;font-size:14px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;">${checkOutDays}, ${bookingOutTime}</font>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td align="left" valign="top" width="40%" style="padding-bottom: 30px">
                                                    <table cellpadding="0" cellspacing="0" border="0" width="100" style="border-right: 1px solid #b6cbe1">
                                                        <tr>
                                                            <td valign="top" class="padb1" align="left" style="line-height:24px; font-size:16px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; margin:0px;">
                                                                <font color="#042649" style="line-height:24px;font-size:16px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;"><b>${guests} Guest,${roomType}</b></font>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top" class="padb1" align="left" style="line-height:24px; font-size:14px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; margin:0px;">
                                                                <font color="#333333" style="line-height:24px;font-size:14px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;">${adults} Adult ${child} Child</font>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td align="center" valign="justify" width="20%" style="padding-bottom: 30px">
                                                    &nbsp;
                                                </td>
                                                <td align="right" valign="middle" width="40%" style="padding-bottom: 30px">
                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                        <tr>
                                                            <td valign="top" class="padb1" align="right" style="line-height:24px; font-size:16px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; margin:0px;">
                                                                <font color="#042649" style="line-height:24px;font-size:16px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;">
                                                                    <b>${propertyName}</b></font>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top" width="40%">
                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">

                                                        <tr>
                                                            <td valign="top" class="padb1" align="left" style="line-height:24px; font-size:16px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; margin:0px;">
                                                                <font color="#042649" style="line-height:24px;font-size:16px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;"><b>Category</b></font>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top" class="padb1" align="left" style="line-height:24px; font-size:14px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; margin:0px;">
                                                                <font color="#333333" style="line-height:24px;font-size:14px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;">${tagName}</font>
                                                            </td>
                                                        </tr>

                                                    </table>
                                                </td>
                                                <td align="center" valign="justify" width="20%">
                                                    &nbsp;
                                                </td>
                                                <td align="right" valign="top" width="40%">
                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                        <tr>
                                                            <td valign="top" class="padb1" align="right" style="line-height:24px; font-size:16px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; margin:0px;">
                                                                <font color="#042649" style="line-height:24px;font-size:16px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;">
                                                                    <b>Accommodation Type</b></font>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top" class="padb1" align="right" style="line-height:24px; font-size:14px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; margin:0px;">
                                                                <font color="#333333" style="line-height:24px;font-size:14px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;">${accommodationType}</font>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top" width="40%">
                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">

                                                        <tr>
                                                            <td valign="top" class="padb1" align="left" style="line-height:24px; font-size:16px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; margin:0px;">
                                                                <font color="#042649" style="line-height:24px;font-size:16px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;"><b>Source</b></font>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top" class="padb1" align="left" style="line-height:24px; font-size:14px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; margin:0px;">
                                                                <font color="#333333" style="line-height:24px;font-size:14px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;">${sourceName}</font>
                                                            </td>
                                                        </tr>

                                                    </table>
                                                </td>
                                                <td align="center" valign="justify" width="20%">
                                                    &nbsp;
                                                </td>
                                                <td align="right" valign="top" width="40%">
                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                        <tr>
                                                            <td valign="top" class="padb1" align="right" style="line-height:24px; font-size:16px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; margin:0px;">
                                                                <font color="#042649" style="line-height:24px;font-size:16px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;">
                                                                    <b>Payment Status</b></font>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top" class="padb1" align="right" style="line-height:24px; font-size:14px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; margin:0px;">
                                                                <font color="#f54245" style="line-height:24px;font-size:14px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;">${paymentStatus}</font>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top" width="40%">
                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">

                                                        <tr>
                                                            <td valign="top" class="padb1" align="left" style="line-height:24px; font-size:16px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; margin:0px;">
                                                                <font color="#042649" style="line-height:24px;font-size:16px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;"><b>Special Request</b></font>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top" class="padb1" align="left" style="line-height:24px; font-size:14px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; margin:0px;">
                                                                <font color="#333333" style="line-height:24px;font-size:14px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;">${specialrequest}</font>
                                                            </td>
                                                        </tr>

                                                    </table>
                                                </td>
                                                <td align="center" valign="justify" width="20%">
                                                    &nbsp;
                                                </td>
                                                <td align="right" valign="top" width="40%">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" style="padding:0 30px 0 30px;">
                                        <table bgcolor="#edf1f6" cellpadding="0" cellspacing="0" border="0" width="100%" style="padding: 30px 40px 10px 40px;border-bottom: 1px solid #b6cbe1;">
                                            <tr>
                                                <td>
                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                        <tr>
                                                            <td valign="top" class="padb1" align="left" style="line-height:24px; font-size:15px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; margin:0px;padding-bottom: 5px;">
                                                                <font color="#333333" style="line-height:24px;font-size:15px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;">Tariff</font>
                                                            </td>
                                                            <td valign="top" class="padb1" align="right" style="line-height:24px; font-size:15px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; margin:0px;padding-bottom: 5px;">
                                                                <font color="#042649" style="line-height:24px;font-size:15px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;"><b style="color:#042649"><span style="padding-right: 15px;display: inline-block;vertical-align:middle;">(A)</span><img src="https://ulotel.ulohotels.com/contents/images/voucher-icon/rupee-symbol.png" width="8" height="10"/>${amount}</b></font>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                        	<td valign="top" class="padb1" align="left" style="line-height:24px; font-size:15px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; margin:0px;padding-bottom: 5px;">
                                                                <font color="#333333" style="line-height:15px;font-size:10px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;">(${roomnighttype})</font>
                                                            </td>
                                                            <td valign="top" class="padb1" align="right" style="line-height:24px; font-size:15px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; margin:0px;padding-bottom: 5px;">
                                                                &nbsp;
                                                            </td>
                                                        	
                                                        </tr>
                                                        <tr>
                                                            <td valign="top" class="padb1" align="left" style="line-height:24px; font-size:15px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; margin:0px;padding-bottom: 5px;">
                                                                <font color="#333333" style="line-height:24px;font-size:15px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;">Tax</font>
                                                            </td>
                                                            <td valign="top" class="padb1" align="right" style="line-height:24px; font-size:15px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; margin:0px;padding-bottom: 5px;">
                                                                <font color="#042649" style="line-height:24px;font-size:15px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;"><b style="color:#042649"><span style="padding-right: 15px;display: inline-block;vertical-align:middle;">(B)</span><img src="https://ulotel.ulohotels.com/contents/images/voucher-icon/rupee-symbol.png" width="8" height="10"/>${tax}</b></font>
                                                            </td>
                                                        </tr>
                                                        
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <table bgcolor="#edf1f6" cellpadding="0" cellspacing="0" border="0" width="100%" style="padding: 20px 40px 20px 40px;">
                                            <tr>
                                                <td>
                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                        <tr>
                                                            <td valign="top" class="padb1" align="left" style="line-height:24px; font-size:15px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; margin:0px;">
                                                                <font color="#333333" style="line-height:24px;font-size:15px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;"><b style="color:#333333">Total Pay (A+B)</b></font>
                                                            </td>
                                                            <td valign="top" class="padb1" align="right" style="line-height:24px; font-size:15px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; margin:0px;">
                                                                <font color="#333333" style="line-height:24px;font-size:15px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;"><b style="color:#042649"><span style="padding-right: 15px;display: inline-block;vertical-align:middle;"></span><img src="https://ulotel.ulohotels.com/contents/images/voucher-icon/rupee-symbol.png" width="8" height="10" />${totalamount}</b></font>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top" class="padb1" align="left" style="line-height:24px; font-size:15px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; margin:0px;">
                                                                <font color="#333333" style="line-height:24px;font-size:15px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;"><b style="color:#333333">Advance</b></font>
                                                            </td>
                                                            <td valign="top" class="padb1" align="right" style="line-height:24px; font-size:15px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; margin:0px;">
                                                                <font color="#333333" style="line-height:24px;font-size:15px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;"><b style="color:#042649"><span style="padding-right: 15px;display: inline-block;vertical-align:middle;"></span><img src="https://ulotel.ulohotels.com/contents/images/voucher-icon/rupee-symbol.png" width="8" height="10" />${totaladvanceamount}</b></font>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top" class="padb1" align="left" style="line-height:24px; font-size:15px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; margin:0px;">
                                                                <font color="#f54245" style="line-height:24px;font-size:15px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;"><b style="color:#f54245">Payment at Hotel</b></font>
                                                            </td>
                                                            <td valign="top" class="padb1" align="right" style="line-height:24px; font-size:15px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; margin:0px;">
                                                                <font color="#f54245" style="line-height:24px;font-size:15px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;"><b style="color:#f54245"><span style="padding-right: 15px;display: inline-block;vertical-align:middle;"></span><img src="https://ulotel.ulohotels.com/contents/images/voucher-icon/rupee-symbol.png" width="8" height="10" />${hotelpay}</b></font>
                                                            </td>
                                                        </tr>
                                                        
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <table bgcolor="#FFFFFF" cellpadding="0" cellspacing="0" border="0" width="100%" style="padding: 20px 40px 20px 40px;">
                                            <tr>
                                                <td valign="top" class="padb1" align="center" style="line-height:24px; font-size:15px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; margin:0px;">
                                                    <font color="#333333" style="line-height:24px;font-size:15px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;">Something is not right? <b style="color:#011830">Cancel the booking</b> or Call 9790 999 222</font>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>


                                <tr>
                                    <td valign="top" style="padding:40px 30px 20px 30px;">
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                            <tr>
                                                <td>
                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                        <tr>
                                                            <td valign="top" class="padb1" align="left" style="line-height:24px; font-size:16px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; margin:0px;padding-bottom: 10px;">
                                                                <font color="#333333" style="line-height:24px;font-size:16px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;"><b style="color:#333333">Hotel Policy</b></font>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top" class="padb1" align="left" style="line-height:24px; font-size:14px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; margin:0px;padding-bottom: 10px;">
                                                                <font color="#333333" style="line-height:24px;font-size:14px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;">The Check-In Time is 12:00 PM and Check-out Time is 11:00 AM.</font>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top" class="padb1" align="left" style="line-height:24px; font-size:14px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; margin:0px;padding-bottom: 10px;">
                                                                <font color="#333333" style="line-height:24px;font-size:14px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;">If the booking is cancelled 3 days prior to the check-in date, Rs.500 will be charged from the total amount and the remaining amount will be refunded within 10 to 12 working days.</font>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top" class="padb1" align="left" style="line-height:24px; font-size:14px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; margin:0px;padding-bottom: 10px;">
                                                                <font color="#333333" style="line-height:24px;font-size:14px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;">If the booking is cancelled 2 days prior to the check-in date, 25% from the total amount will be charged as a cancellation fee.</font>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top" class="padb1" align="left" style="line-height:24px; font-size:14px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; margin:0px;padding-bottom: 10px;">
                                                                <font color="#333333" style="line-height:24px;font-size:14px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;">If the booking is cancelled one day prior to the check-in date, no refund is applicable. No refund is applicable for cancellations done on Long Weekends, Festival And On-Season Times.
                                                                </font>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td valign="top" class="padb1" align="left" style="line-height:24px; font-size:14px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; margin:0px;padding-bottom: 10px;">
                                                                <font color="#333333" style="line-height:24px;font-size:14px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;">To know more read the hotel policies to click <a style="color:#042649" href="https://ulotel.ulohotels.com/terms-conditions" target="_blank" title="Terms And Conditions">Terms And Conditions</a>
                                                                </font>
                                                            </td>
                                                        </tr>

                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr>
                                    <td valign="top" style="padding:0 30px 5px 30px;">
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                            <tr>
                                                <td>
                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                        <tr>
                                                            <td valign="top" class="padb1" align="center" style="line-height:24px; font-size:16px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; margin:0px;padding-bottom: 10px;">
                                                                <font color="#011830" style="line-height:24px;font-size:16px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;"><b style="color:#011830">Hotel Details</b></font>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top" class="padb1" align="center" style="line-height:24px; font-size:15px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; margin:0px;padding-bottom: 10px;">
                                                                <font color="#333333" style="line-height:24px;font-size:15px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;">${propertyName} ${address1} ${address2}</font>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="top" class="padb1" align="center" style="line-height:24px; font-size:14px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; margin:0px;padding-bottom: 30px;">
                                                                <font color="#011830" style="line-height:24px;font-size:14px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;"><a style="color: #042649; text-decoration: none;" href="mailto:${propertyEmail}">${propertyEmail}</a> | <a style="color: #042649; text-decoration: none;" href="tel:${phoneNumber}">${phoneNumber}</a><a href="https://www.google.com/maps/search/?api=1&query=${latitude},${longitude}" target="_blank"><img src="https://ulotel.ulohotels.com/ulowebsite/images/invoice/invoicemap.png" alt="gallery" />&nbsp;View Map</a><a href="https://www.ulohotels.com/${propertyUrl}" target="_blank"><span style="white-space: nowrap; overflow: hidden;padding:3px;">&nbsp; Photo Gallery</span></a></font>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                        <tr>
                                                            <td valign="top" class="padb1" align="center" style="line-height:24px; font-size:16px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; margin:0px;padding-bottom: 10px;">
                                                                <font color="#011830" style="line-height:24px;font-size:16px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;"><b style="color:#011830">Customer Support Details</b></font>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td valign="top" class="padb1" align="center" style="line-height:24px; font-size:14px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; margin:0px;padding-bottom: 10px;">
                                                                <font color="#011830" style="line-height:24px;font-size:14px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;"><a style="color: #042649; text-decoration: none;" href="mailto:${reservationEmail}">${reservationEmail}</a> | <a style="color: #042649; text-decoration: none;" href="tel:${reservationContact}">${reservationContact}</a></font>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr>
                                    <td valign="top" style="padding:0 30px 5px 30px;">
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                            <tr>
                                                <td>
                                                    <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                                        <tr>
                                                            <td valign="top" class="padb1" align="center" style="line-height:24px; font-size:13px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif; margin:0px;padding-bottom: 10px;">
                                                                <font color="#d83c3c" style="line-height:24px;font-size:13px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;"><b>This Computer Generated Voucher,Here No Signature Required.</b></font>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td></tr>
                                    <tr>
                                        <td class="inner-footer" style="padding-top:5px;padding-left:30px;padding-right:30px;text-align:center;">
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tr>
                                                    <td style="font-family: Helvetica Neue, Helvetica, Arial, sans-serif;line-height:26px;font-size: 15px;color:#898989; padding:0 40px 10px 40px;">
                                                        <font color="#898989" style="line-height:24px;font-size:15px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;">1/4, Plot No: 251, Second Floor, Rajamannar Salai, Vijayaraghavapuram, Saligramam, Chennai, Tamil Nadu 600093</font>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="font-family: Helvetica Neue, Helvetica, Arial, sans-serif;line-height:26px;font-size: 16px;color:#011830;padding:0 40px 10px 40px;">
                                                        <font color="#011830" style="line-height:24px;font-size:16px; font-family: Helvetica Neue, Helvetica, Arial, sans-serif;">
                                                            <a href="mailto:support@ulohotels.com" style="color:#011830; text-decoration: none;" title="support@ulohotels.com">support@ulohotels.com</a> | <a style="color:#011830; text-decoration: none;"
                                                                title="9790 999 222" href="tel:9790 999 222">9790 999 222</a> | <a style="color:#011830; text-decoration: none;" href="https://www.ulohotels.com/" title="www.ulohotels.com">www.ulohotels.com</a>
                                                        </font>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" style="padding:10px 0 40px 0;">
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tr>
                                                    <td class="footer-width" width="38%"></td>
                                                    <td class="footer-widthcenter" width="24%">
                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                                <td align="center" style="padding:0 5px;">
                                                                    <a href="https://www.facebook.com/Ulohotels/"><img src="https://ulotel.ulohotels.com/contents/images/voucher-icon/icon-facebook.png" style="display:inline-block;border:0;" alt="Facebook" /></a>
                                                                </td>
                                                                <td align="center" style="padding:0 5px;">
                                                                    <a href="https://twitter.com/ulohotels"><img src="https://ulotel.ulohotels.com/contents/images/voucher-icon/icon-twitter.png" style="display:inline-block;border:0;" alt="Twitter" /></a>
                                                                </td>
                                                                <td align="center" style="padding:0 5px;">
                                                                    <a href="https://plus.google.com/+ULOHOTELS"><img src="https://ulotel.ulohotels.com/contents/images/voucher-icon/icon-google.png" style="display:inline-block;border:0;" alt="Google Plus" /></a>
                                                                </td>
                                                                <td align="center" style="padding:0 5px;">
                                                                    <a href="https://www.pinterest.com/ulohotels/"><img src="https://ulotel.ulohotels.com/contents/images/voucher-icon/icon-pintrest.png" style="display:inline-block;border:0;" alt="Pintrest" /></a>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td class="footer-width" width="38%"></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                            </table>
                        </td>
                     </tr>
                </table>
            </td>
         </tr>
    </table>
</body>

</html>